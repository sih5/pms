﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        public bool ExportPartsBranch(int? branchId, string partCode, string partName, int? partAbc, string partsWarrantyCategoryName, int? productLifeCycle, int? status, int? partsSalesCategoryId, string referenceCode, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName) {
            fileName = GetExportFilePath("配件分品牌商务信息.xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated) {
                    throw new Exception(ErrorStrings.File_Validation_User);
                }
                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {

                    conn.Open();
                    var sql = new StringBuilder();
                    sql.Append(@"Select Extent2.Name,
                               a.Partcode,
                               a.Partname,
                               a.Branchname,
                               a.Referencecode,
                               b.Feature,
                               b.EnglishName,
                               (select value from keyvalueitem where NAME = 'IsOrNot'and key=a.Isorderable) As Isorderable,
                               (select value from keyvalueitem where NAME = 'IsOrNot'and key=a.Issalable) As Issalable,
                               (select value from keyvalueitem where NAME = 'IsOrNot'and key=a.Isdirectsupply) As Isdirectsupply,
                               (select value from keyvalueitem where NAME = 'ABCStrategy_Category'and key=a.Partabc) As Partabc,
                               a.Minsalequantity,
                               (select value from keyvalueitem where NAME = 'PartsSalesOrder_SecondLevelOrderType'and key=a.Purchasecycle) As Purchasecycle,
                               (select value from keyvalueitem where NAME = 'SparePart_LossType'and key=a.Losstype) As Losstype,
                               (select value from keyvalueitem where NAME = 'PartsAttribution'and key=a.PartsAttribution) As PartsAttribution,
                               a.Partswarrantycategoryname,
                               (select value from keyvalueitem where NAME = 'PartsWarrantyTerm_ReturnPolicy'and key=a.Partsreturnpolicy) As Partsreturnpolicy,
                               a.Stockmaximum,
                               a.Stockminimum,
                               (select value from keyvalueitem where NAME = 'MasterData_Status'and key=a.Status) As Status,
                               a.AutoApproveUpLimit,
                               a.Remark,
                               a.Creatorname,
                               a.Createtime,
                               a.Modifiername,
                               a.Modifytime,
                               a.Abandonername,
                               a.Abandontime
                          From Partsbranch a
                            inner join sparepart b
                            on a.PartId=b.id
                          Left Outer Join Partssalescategory Extent2
                            On a.Partssalescategoryid = Extent2.Id where 1=1 ");
                    var dbParameters = new List<DbParameter>();
                    if(branchId.HasValue) {
                        sql.Append(@" and a.Branchid={0}branchId");
                        dbParameters.Add(db.CreateDbParameter("branchId", branchId.Value));
                    }
                    if(!string.IsNullOrEmpty(partCode)) {
                        sql.Append(@" and LOWER(a.partCode) like {0}partCode ");
                        dbParameters.Add(db.CreateDbParameter("partCode", "%" + partCode.ToLower() + "%"));
                    }
                    if(!string.IsNullOrEmpty(partName)) {
                        sql.Append(@" and LOWER(a.partName) like {0}partName ");
                        dbParameters.Add(db.CreateDbParameter("partName", "%" + partName.ToLower() + "%"));
                    }
                    if(!string.IsNullOrEmpty(referenceCode)) {
                        sql.Append(@" and LOWER(a.referenceCode) like {0}referenceCode ");
                        dbParameters.Add(db.CreateDbParameter("referenceCode", "%" + referenceCode.ToLower() + "%"));
                    }
                    if(partAbc.HasValue) {
                        sql.Append(@" and a.partABC={0}partABC");
                        dbParameters.Add(db.CreateDbParameter("partABC", partAbc.Value));
                    }

                    if(!string.IsNullOrEmpty(partsWarrantyCategoryName)) {
                        sql.Append(@" and LOWER(a.partsWarrantyCategoryName) like {0}partsWarrantyCategoryName ");
                        dbParameters.Add(db.CreateDbParameter("partsWarrantyCategoryName", "%" + partsWarrantyCategoryName.ToLower() + "%"));
                    }

                    if(productLifeCycle.HasValue) {
                        sql.Append(@" and a.productLifeCycle ={0}productLifeCycle");
                        dbParameters.Add(db.CreateDbParameter("productLifeCycle", productLifeCycle.Value));
                    }

                    if(status.HasValue) {
                        sql.Append(@" and a.status ={0}status");
                        dbParameters.Add(db.CreateDbParameter("status", status.Value));
                    }

                    if(partsSalesCategoryId.HasValue) {
                        sql.Append(@" and a.partsSalesCategoryId ={0}partsSalesCategoryId");
                        dbParameters.Add(db.CreateDbParameter("partsSalesCategoryId", partsSalesCategoryId.Value));
                    }

                    if(createTimeBegin.HasValue) {
                        sql.Append(" and a.createTime>=to_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                        var tempValue = createTimeBegin.Value;
                        var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                        dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                    }
                    if(createTimeEnd.HasValue) {
                        sql.Append(" and a.createTime<=to_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                        var tempValue = createTimeEnd.Value;
                        var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                        dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                    }
                    var sqlStr = sql.ToString();
                    if(sqlStr.IndexOf(" where 1=1  and ") > 0)
                        sqlStr = sqlStr.Replace(" where 1=1  and ", " where ");
                    else
                        sqlStr = sqlStr.Replace(" where 1=1 ", "");

                    var command = db.CreateDbCommand(string.Format(sqlStr, db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    ErrorStrings.Export_Title_Partssalescategory_Name, ErrorStrings.Export_Title_PartsBranch_Code, ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName, ErrorStrings.Export_Title_PartsBranch_BranchName, ErrorStrings.Export_Title_PartsBranch_Referencecode,
                                    ErrorStrings.Export_Title_PartsBranch_Feature, ErrorStrings.Export_Title_PartsBranch_EnglishName, ErrorStrings.Export_Title_PartsBranch_IsOrderable, ErrorStrings.Export_Title_PartsBranch_IsSalable, ErrorStrings.Export_Title_PartsBranch_IsDirectSupply,
                                    ErrorStrings.Export_Title_PartsBranch_PartABC, ErrorStrings.Export_Title_PartsBranch_Minsalequantity, ErrorStrings.Export_Title_PartsBranch_ProductLifeCycle, ErrorStrings.Export_Title_PartsBranch_LossType, ErrorStrings.Export_Title_PartsBranch_PartsAttribution, ErrorStrings.Export_Title_PartsBranch_PartsWarrantyCategoryName, ErrorStrings.Export_Title_PartsBranch_PartsReturnPolicy, 
                                    ErrorStrings.Export_Title_PartsBranch_StockMaximum, ErrorStrings.Export_Title_PartsBranch_StockMinimum, ErrorStrings.Export_Title_AccountPeriod_Status, ErrorStrings.Export_Title_PartsBranch_AutoApproveUpLimit, ErrorStrings.Export_Title_PartsBranch_Remark, ErrorStrings.Export_Title_AccountPeriod_CreatorName, ErrorStrings.Export_Title_AccountPeriod_CreateTime,
                                    ErrorStrings.Export_Title_AccountPeriod_ModifierName, ErrorStrings.Export_Title_AccountPeriod_ModifyTime, ErrorStrings.Export_Title_PartsBranch_AbandonerName, ErrorStrings.Export_Title_PartsBranch_AbandonerTime
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }

                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }

        public bool ExportForPartsBranch(int[] ids, int? branchId, string partCode, string partName, int? partAbc, string partsWarrantyCategoryName, int? productLifeCycle, int? status, int? partsSalesCategoryId, int? plannedPriceCategory, string referenceCode, DateTime? createTimeBegin, DateTime? createTimeEnd, string partsSalePriceIncreaseRateGroupCode, string partsSalePriceIncreaseRateGroupName, int? partsReturnPolicy, bool? isSalable, bool? isOrderable, bool? isDirectSupply, string partsEnglishName, DateTime? modifyTimeBegin, DateTime? modifyTimeEnd, out string fileName)
        {
            fileName = GetExportFilePath("配件分品牌商务信息.xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated) {
                    throw new Exception(ErrorStrings.File_Validation_User);
                }
                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {

                    conn.Open();
                    var sql = new StringBuilder();
                    sql.Append(@"Select (select value from keyvalueitem where NAME = 'MasterData_Status'and key=a.Status) As Status,
                                       a.Partcode,
                                       a.Partname,
                                       b.Referencecode,
                                       b.EnglishName,
                                       (select value from keyvalueitem where NAME = 'IsOrNot'and key=a.Isorderable) As Isorderable,
                                       (select value from keyvalueitem where NAME = 'IsOrNot'and key=a.Issalable) As Issalable,
                                       (select value from keyvalueitem where NAME = 'IsOrNot'and key=a.Isdirectsupply) As Isdirectsupply,
                                       (select value from keyvalueitem where NAME = 'IsOrNot'and key=a.IsAccreditPack) As IsAccreditPack,
                                       --a.Minsalequantity,
                                       Extent3.GroupName,
                                      (select value from keyvalueitem where NAME = 'IsOrNot'and key=a.IsAutoCaclSaleProperty) As IsAutoCaclSaleProperty,
                                       (select value from keyvalueitem where NAME = 'PackingUnitType'and key=a.MainPackingType) As MainPackingType,
                                       c.PackingCoefficient AS PackingCoefficient1,
                                     CASE WHEN c.IsBoxStandardPrint = 1 THEN cast('大标' as varchar2(100)) WHEN c.IsPrintPartStandard = 1 THEN cast('小标' as varchar2(100)) ELSE '' END AS C2, 
                                        c.PackingMaterial as FirPackingMaterial,                    
                                       d.PackingCoefficient AS PackingCoefficient2,
                                      CASE WHEN d.IsBoxStandardPrint = 1 THEN cast('大标' as varchar2(100)) WHEN d.IsPrintPartStandard = 1 THEN cast('小标' as varchar2(100)) ELSE '' END AS C3,
                                       d.PackingMaterial as SecPackingMaterial,  
                                        e.PackingCoefficient AS PackingCoefficient3, 
                                     CASE WHEN e.IsBoxStandardPrint = 1 THEN cast('大标' as varchar2(100)) WHEN e.IsPrintPartStandard = 1 THEN cast('小标' as varchar2(100)) ELSE '' END AS C4,                                      
                                       e.PackingMaterial as ThidPackingMaterial,  
                                       a.Stockmaximum,
                                       a.Stockminimum,
                                       a.PartsWarrantyLong,                                                                            
                                       a.AutoApproveUpLimit,   
                                       a.BreakTime,
                                       (select value from keyvalueitem where NAME = 'PartsBranch_PurchaseRoute'and key=a.PurchaseRoute) As PurchaseRoute,
                                        (select value from keyvalueitem where NAME = 'ABCStrategy_Category'and key=a.Partabc) As Partabc,                                   
                                       a.Remark,
                                       a.Creatorname,
                                       a.Createtime,
                                       a.Modifiername,
                                       a.Modifytime,
                                       a.Abandonername,
                                       a.Abandontime,
                                       Extent2.Name
                                       
                                  From Partsbranch a
                                    inner join sparepart b
                                    on a.PartId=b.id
                                  Left Outer Join Partssalescategory Extent2
                                    On a.Partssalescategoryid = Extent2.Id
                                Left Outer Join PartsSalePriceIncreaseRate Extent3
                                       on  a.IncreaseRateGroupId = Extent3.id 
                                  LEFT OUTER JOIN PartsBranchPackingProp c ON c.PackingType = 1 AND a.Id = c.PartsBranchId
                                  LEFT OUTER JOIN PartsBranchPackingProp d ON d.PackingType = 2 AND a.Id = d.PartsBranchId
                                  LEFT OUTER JOIN PartsBranchPackingProp e ON e.PackingType = 3 AND a.Id = e.PartsBranchId");
                    var dbParameters = new List<DbParameter>();
                    var userInfo = Utils.GetCurrentUserInfo();
                    sql.Append(string.Format(" where a.branchId={0}", userInfo.EnterpriseId));

                    if(!string.IsNullOrEmpty(partsEnglishName)) {
                        sql.Append(@" and b.EnglishName like {0}EnglishName ");
                        dbParameters.Add(db.CreateDbParameter("EnglishName", "%" + partsEnglishName + "%"));
                    }
                    //if(branchId.HasValue) {
                    //    sql.Append(@" and a.branchId={0}branchId");
                    //    dbParameters.Add(db.CreateDbParameter("branchId", branchId.Value));
                    //}
                    if(ids != null && ids.Length > 0) {
                        var idStr = string.Join(",", ids);
                        sql.Append(" and a.id in (" + idStr + ")");
                    } else {
                        if(!string.IsNullOrEmpty(partCode)) {
                            sql.Append(@" and LOWER(a.partCode) like {0}partCode ");
                            dbParameters.Add(db.CreateDbParameter("partCode", "%" + partCode.ToLower() + "%"));
                        }
                        if(!string.IsNullOrEmpty(partName)) {
                            sql.Append(@" and LOWER(a.partName) like {0}partName ");
                            dbParameters.Add(db.CreateDbParameter("partName", "%" + partName.ToLower() + "%"));
                        }
                        if(!string.IsNullOrEmpty(referenceCode)) {
                            sql.Append(@" and LOWER(b.referenceCode) like {0}referenceCode ");
                            dbParameters.Add(db.CreateDbParameter("referenceCode", "%" + referenceCode.ToLower() + "%"));
                        }
                        if(partsReturnPolicy.HasValue) {
                            sql.Append(@" and a.partsReturnPolicy={0}partsReturnPolicy");
                            dbParameters.Add(db.CreateDbParameter("partsReturnPolicy", partsReturnPolicy.Value));
                        }

                        if(!string.IsNullOrEmpty(partsSalePriceIncreaseRateGroupName)) {
                            sql.Append(@" and LOWER(Extent3.GroupName) like {0}GroupName ");
                            dbParameters.Add(db.CreateDbParameter("GroupName", "%" + partsSalePriceIncreaseRateGroupName.ToLower() + "%"));
                        }
                        if(!string.IsNullOrEmpty(partsSalePriceIncreaseRateGroupCode)) {
                            sql.Append(@" and LOWER(Extent3.GroupCode) like {0}GroupCode ");
                            dbParameters.Add(db.CreateDbParameter("GroupCode", "%" + partsSalePriceIncreaseRateGroupCode.ToLower() + "%"));
                        }
                        if(isSalable.HasValue) {
                            sql.Append(@" and a.isSalable={0}isSalable");
                            dbParameters.Add(db.CreateDbParameter("isSalable", isSalable.Value ? 1 : 0));
                        }
                        if(isOrderable.HasValue) {
                            sql.Append(@" and a.isOrderable={0}isOrderable");
                            dbParameters.Add(db.CreateDbParameter("isOrderable", isOrderable.Value ? 1 : 0));
                        }
                        if(isDirectSupply.HasValue) {
                            sql.Append(@" and a.isDirectSupply={0}isDirectSupply");
                            dbParameters.Add(db.CreateDbParameter("isDirectSupply", isDirectSupply.Value ? 1 : 0));
                        }
                        if(partAbc.HasValue) {
                            sql.Append(@" and a.partABC={0}partABC");
                            dbParameters.Add(db.CreateDbParameter("partABC", partAbc.Value));
                        }
                        if(!string.IsNullOrEmpty(partsWarrantyCategoryName)) {
                            sql.Append(@" and LOWER(a.partsWarrantyCategoryName) like {0}partsWarrantyCategoryName ");
                            dbParameters.Add(db.CreateDbParameter("partsWarrantyCategoryName", "%" + partsWarrantyCategoryName.ToLower() + "%"));
                        }

                        if(productLifeCycle.HasValue) {
                            sql.Append(@" and a.productLifeCycle ={0}productLifeCycle");
                            dbParameters.Add(db.CreateDbParameter("productLifeCycle", productLifeCycle.Value));
                        }

                        if(status.HasValue) {
                            sql.Append(@" and a.status ={0}status");
                            dbParameters.Add(db.CreateDbParameter("status", status.Value));
                        }
                        if(plannedPriceCategory.HasValue) {
                            sql.Append(@" and a.PlannedPriceCategory ={0}plannedPriceCategory");
                            dbParameters.Add(db.CreateDbParameter("plannedPriceCategory", plannedPriceCategory.Value));
                        }

                        if(partsSalesCategoryId.HasValue) {
                            sql.Append(@" and a.partsSalesCategoryId ={0}partsSalesCategoryId");
                            dbParameters.Add(db.CreateDbParameter("partsSalesCategoryId", partsSalesCategoryId.Value));
                        }

                        if(createTimeBegin.HasValue) {
                            sql.Append(" and a.createTime>=to_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(" and a.createTime<=to_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                    }
                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                     ErrorStrings.Export_Title_AccountPeriod_Status, ErrorStrings.Export_Title_PartsBranch_Code, ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName, ErrorStrings.Export_Title_Accessinfo_Tmp_ReferenceCode,ErrorStrings.Export_Title_PartsBranch_EnglishName,
                                     ErrorStrings.Export_Title_PartsBranch_IsOrderableNew, ErrorStrings.Export_Title_PartsBranch_IsSalableNew, ErrorStrings.Export_Title_PartsBranch_IsDirectSupplyNew,
                                     ErrorStrings.Export_Title_PartsBranch_IsAccreditPack, ErrorStrings.Export_Title_PartsBranch_PriceType,ErrorStrings.Export_Title_PartsBranch_IsAutoCaclSaleProperty,
                                     ErrorStrings.Export_Title_PartsBranch_MinPacking,ErrorStrings.Export_Title_PartsBranch_FirPacking,ErrorStrings.Export_Title_PartsBranch_FirPrint,ErrorStrings.Export_Title_FirPackingMaterial,ErrorStrings.Export_Title_PartsBranch_SecPacking,ErrorStrings.Export_Title_PartsBranch_SecPrint,ErrorStrings.Export_Title_SecPackingMaterial,
                                     ErrorStrings.Export_Title_PartsBranch_ThidPacking, ErrorStrings.Export_Title_PartsBranch_ThidPrint,ErrorStrings.Export_Title_ThidPackingMaterial,ErrorStrings.Export_Title_PartsBranch_StockMaximum, ErrorStrings.Export_Title_PartsBranch_StockMinimum,
                                     ErrorStrings.Export_Title_PartsBranch_PartsWarrantyLong, ErrorStrings.Export_Title_PartsBranch_AutoApproveUpLimit,ErrorStrings.Export_Title_PartsBranch_BreakTime,
                                     ErrorStrings.Export_Title_PartsBranch_PurchaseRoute,ErrorStrings.Export_Title_PartsBranch_PartABC, ErrorStrings.Export_Title_PartsBranch_Remark,ErrorStrings.Export_Title_AccountPeriod_CreatorName, ErrorStrings.Export_Title_AccountPeriod_CreateTime, ErrorStrings.Export_Title_AccountPeriod_ModifierName, ErrorStrings.Export_Title_AccountPeriod_ModifyTime, ErrorStrings.Export_Title_PartsBranch_AbandonerName, ErrorStrings.Export_Title_PartsBranch_AbandonerTime,ErrorStrings.Export_Title_Partssalescategory_Name
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }

                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }

        public bool ImportPartsBranch(string fileName, out int excelImportNum, out List<PartsBranchExtend> rightData, out List<PartsBranchExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<PartsBranchExtend>();
            var rightList = new List<PartsBranchExtend>();
            var allList = new List<PartsBranchExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("PartsBranch", out notNullableFields, out fieldLenght);
                List<object> excelColumns;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Partssalescategory_Name, "PartsSalesCategoryName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Code, "PartCode");
                    //excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName, "PartName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_BranchName, "BranchName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_IsOrderableNew, "IsOrderable");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_IsSalableNew, "IsSalable");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_IsDirectSupplyNew, "IsDirectSupply");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Minsalequantity, "MinSaleQuantity");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_ProductLifeCycle, "ProductLifeCycle");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_LossType, "LossType");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_PartsAttribution, "PartsAttribution");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_PartsReturnPolicy, "PartsReturnPolicy");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_StockMaximum, "StockMaximum");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_StockMinimum, "StockMinimum");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_PartsMaterialManageCost, "PartsMaterialManageCost");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_PartsWarrantyLong, "PartsWarrantyLong");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_IncreaseRateGroupCode, "IncreaseRateGroupCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_PriceType, "IncreaseRateGroupName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_AutoApproveUpLimit, "AutoApproveUpLimit");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Remark, "Remark");

                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;

                    ////单次最大导入条数不能大于500！
                    //if(excelOperator.LastRowNum > 500) {
                    //    throw new Exception("单次最大导入条数不能大于500！");
                    //}

                    var keyValuePairs = new[] {
                        //new KeyValuePair<string, string>("PartABC", "ABCStrategy_Category"),
                        new KeyValuePair<string, string>("ProductLifeCycle", "PartsBranch_ProductLifeCycle"), new KeyValuePair<string, string>("LossType", "SparePart_LossType"), new KeyValuePair<string, string>("Status", "BaseData_Status"), new KeyValuePair<string, string>("PartsReturnPolicy", "PartsWarrantyTerm_ReturnPolicy"), new KeyValuePair<string, string>("PartsAttribution", "PartsAttribution")
                    };
                    tempExcelOperator.ImportHelper.LoadEnumFromDb(null, null, keyValuePairs);
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var tempImportObj = new PartsBranchExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);

                        #region
                        tempImportObj.PartsSalesCategoryNameStr = newRow["PartsSalesCategoryName"];
                        tempImportObj.PartCodeStr = newRow["PartCode"];
                        //tempImportObj.PartNameStr = newRow["PartName"];
                        tempImportObj.BranchNameStr = newRow["BranchName"];
                        tempImportObj.IsOrderableStr = newRow["IsOrderable"];
                        tempImportObj.IsSalableStr = newRow["IsSalable"];
                        tempImportObj.IsDirectSupplyStr = newRow["IsDirectSupply"];
                        tempImportObj.MinSaleQuantityStr = newRow["MinSaleQuantity"];
                        tempImportObj.ProductLifeCycleStr = newRow["ProductLifeCycle"];
                        tempImportObj.LossTypeStr = newRow["LossType"];
                        tempImportObj.PartsAttributionStr = newRow["PartsAttribution"];
                        tempImportObj.PartsReturnPolicyStr = newRow["PartsReturnPolicy"];
                        tempImportObj.StockMaximumStr = newRow["StockMaximum"];
                        tempImportObj.StockMinimumStr = newRow["StockMinimum"];
                        tempImportObj.PartsMaterialManageCostStr = newRow["PartsMaterialManageCost"];
                        tempImportObj.PartsWarrantyLongStr = newRow["PartsWarrantyLong"];
                        tempImportObj.IncreaseRateGroupCode = newRow["IncreaseRateGroupCode"];
                        tempImportObj.IncreaseRateGroupName = newRow["IncreaseRateGroupName"];
                        tempImportObj.AutoApproveUpLimitStr = newRow["AutoApproveUpLimit"];
                        tempImportObj.RemarkStr = newRow["Remark"];

                        #endregion

                        var tempErrorMessage = new List<string>();

                        #region 导入的内容基本检查

                        //品牌检查
                        //var fieldIndex = notNullableFields.IndexOf("PartsSalesCategoryName".ToUpper());
                        //if(string.IsNullOrEmpty(tempImportObj.PartsSalesCategoryNameStr)) {
                        //    if(fieldIndex > -1)
                        //        tempErrorMessage.Add(ErrorStrings.Export_Validation_MarketingDepartment_PartsSalesCategoryIsNull);
                        //} else {
                        //    if(Encoding.Default.GetByteCount(tempImportObj.PartsSalesCategoryNameStr) > fieldLenght["PartsSalesCategoryName".ToUpper()])
                        //        tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_PartsSalescategoryNameIsLong);
                        //}
                        //配件编号
                        var fieldIndex = notNullableFields.IndexOf("PartCode".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.PartCodeStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.OverstockPartsAppDetail__Validation1);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.PartCodeStr) > fieldLenght["PartCode".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.ImpPartsSupplierRelation_Validation8);
                        }
                        ////配件名称检查
                        //fieldIndex = notNullableFields.IndexOf("PartName".ToUpper());
                        //if(string.IsNullOrEmpty(tempImportObj.PartNameStr)) {
                        //    if(fieldIndex > -1)
                        //        tempErrorMessage.Add(ErrorStrings.Export_Validation_Sparepart_Name);
                        //} else {
                        //    if(Encoding.Default.GetByteCount(tempImportObj.PartNameStr) > fieldLenght["PartName".ToUpper()])
                        //        tempErrorMessage.Add(ErrorStrings.Export_Validation_Sparepart_NameIsLong);
                        //}
                        //营销分公司检查
                        fieldIndex = notNullableFields.IndexOf("BranchName".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.BranchNameStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_BranchIsNull);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.BranchNameStr) > fieldLenght["BranchName".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_BranchIsLong);
                        }
                        ////配件参图号
                        //fieldIndex = notNullableFields.IndexOf("ReferenceCode".ToUpper());
                        //if(string.IsNullOrEmpty(tempImportObj.ReferenceCodeStr)) {
                        //    if(fieldIndex > -1)
                        //        tempErrorMessage.Add("零部件图号不能为空");
                        //} else {
                        //    if(Encoding.Default.GetByteCount(tempImportObj.ReferenceCodeStr) > fieldLenght["ReferenceCode".ToUpper()])
                        //        tempErrorMessage.Add("零部件图号号过长");
                        //}
                        //是否可采购
                        fieldIndex = notNullableFields.IndexOf("IsOrderable".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.IsOrderableStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_IsOrderableIsNull);
                        } else {
                            if(tempImportObj.IsOrderableStr != ErrorStrings.Export_Title_PartsBranch_Yes && tempImportObj.IsOrderableStr != ErrorStrings.Export_Title_PartsBranch_No)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_IsOrderableValueError);
                        }
                        //是否可销售
                        fieldIndex = notNullableFields.IndexOf("IsSalable".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.IsSalableStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_IsSalableIsNull);
                        } else {
                            if(tempImportObj.IsSalableStr != ErrorStrings.Export_Title_PartsBranch_Yes && tempImportObj.IsSalableStr != ErrorStrings.Export_Title_PartsBranch_No)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_IsSalableValueError);
                        }
                        //是否直供
                        fieldIndex = notNullableFields.IndexOf("IsDirectSupply".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.IsDirectSupplyStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_IsDirectSupplyIsNull);
                        } else {
                            if(tempImportObj.IsDirectSupplyStr != ErrorStrings.Export_Title_PartsBranch_Yes && tempImportObj.IsDirectSupplyStr != ErrorStrings.Export_Title_PartsBranch_No)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_IsDirectSupplyValueError);
                        }
                        //最小销售批量
                        fieldIndex = notNullableFields.IndexOf("MinSaleQuantity".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.MinSaleQuantityStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_MinSaleQuantityIsNull);
                        } else {
                            int checkValue;
                            if(int.TryParse(tempImportObj.MinSaleQuantityStr, out checkValue)) {
                                if(checkValue <= 0) {
                                    tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_MinSaleQuantityIsOverZero);
                                }
                            } else {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_MinSaleQuantityIsNumber);
                            }
                        }

                        //产品生命周期
                        if(!string.IsNullOrEmpty(tempImportObj.ProductLifeCycleStr)) {
                            var tempEnumValue = tempExcelOperator.ImportHelper.GetEnumValue("ProductLifeCycle", tempImportObj.ProductLifeCycleStr);
                            if(!tempEnumValue.HasValue) {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_ProductLifeCycleError);
                            }
                        }

                        //损耗类型
                        fieldIndex = notNullableFields.IndexOf("LossType".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.LossTypeStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_LossTypeIsNull);
                        } else {
                            var tempEnumValue = tempExcelOperator.ImportHelper.GetEnumValue("LossType", tempImportObj.LossTypeStr);
                            if(!tempEnumValue.HasValue) {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_LossTypeValueError);
                            }
                        }
                        //配件所属分类
                        fieldIndex = notNullableFields.IndexOf("PartsAttribution".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.PartsAttributionStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_PartsAttributionIsNull);
                        } else {
                            var tempEnumValue = tempExcelOperator.ImportHelper.GetEnumValue("PartsAttribution", tempImportObj.PartsAttributionStr);
                            if(!tempEnumValue.HasValue) {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_PartsAttributionValueError);
                            }
                        }
                        //配件返回政策 
                        fieldIndex = notNullableFields.IndexOf("PartsReturnPolicy".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.PartsReturnPolicyStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_PartsReturnPolicyIsNull);
                        } else {
                            var tempEnumValue = tempExcelOperator.ImportHelper.GetEnumValue("PartsReturnPolicy", tempImportObj.PartsReturnPolicyStr);
                            if(!tempEnumValue.HasValue) {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_PartsReturnPolicyIsValueError);
                            }
                        }

                        //库存高限StockMaximum
                        fieldIndex = notNullableFields.IndexOf("StockMaximum".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.StockMaximumStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_StockMaximumIsNull);
                        } else {
                            int checkValue;
                            if(int.TryParse(tempImportObj.StockMaximumStr, out checkValue)) {
                                if(checkValue < 0) {
                                    tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_StockMaximumIsOverZero);
                                }
                            } else {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_StockMaximumIsNumber);
                            }
                        }
                        //库存低限StockMinimum
                        fieldIndex = notNullableFields.IndexOf("StockMinimum".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.StockMinimumStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_StockMinimum);
                        } else {
                            int checkValue;
                            if(int.TryParse(tempImportObj.StockMinimumStr, out checkValue)) {
                                if(checkValue < 0) {
                                    tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_StockMinimumOverZero);
                                }
                            } else {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_StockMinimumIsNumber);
                            }
                        }

                        //配件材料费 PartsMaterialManageCost
                        fieldIndex = notNullableFields.IndexOf("PartsMaterialManageCost".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.PartsMaterialManageCostStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_PartsMaterialManageCostIsNull);
                        } else {
                            decimal checkValue;
                            if(decimal.TryParse(tempImportObj.PartsMaterialManageCostStr, out checkValue)) {
                                if(checkValue <= 0) {
                                    tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_PartsMaterialManageCostOverZero);
                                } else {
                                    tempImportObj.PartsMaterialManageCost = checkValue;
                                }
                            } else {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_PartsMaterialManageCostIsNumber);
                            }
                        }

                        //配件保修时长 PartsWarrantyLong
                        fieldIndex = notNullableFields.IndexOf("PartsWarrantyLong".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.PartsWarrantyLongStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_PartsWarrantyLongIsNull);
                        } else {
                            int checkValue;
                            if(int.TryParse(tempImportObj.PartsWarrantyLongStr, out checkValue)) {
                                if(checkValue <= 0) {
                                    tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_PartsWarrantyLongOverZero);
                                } else {
                                    tempImportObj.PartsWarrantyLong = checkValue;
                                }
                            } else {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_PartsWarrantyLongIsNumber);
                            }
                        }

                        //批量审核上限
                        if(!string.IsNullOrEmpty(tempImportObj.AutoApproveUpLimitStr)) {
                            int checkValue;
                            if(int.TryParse(tempImportObj.AutoApproveUpLimitStr, out checkValue)) {
                                if(checkValue < 0) {
                                    tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_AutoApproveUpLimiOverZero);
                                } else
                                    tempImportObj.AutoApproveUpLimit = checkValue;
                            } else {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_AutoApproveUpLimiIsNumber);
                            }
                        } else
                            tempImportObj.AutoApproveUpLimit = 0;

                        //备注Remark
                        fieldIndex = notNullableFields.IndexOf("Remark".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.RemarkStr)) {
                            if(fieldIndex > -1) {
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchasePricingDetail_Validation15);
                            }

                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.RemarkStr) > fieldLenght["Remark".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchasePricingDetail_Validation16);
                        }

                        #endregion

                        if(tempErrorMessage.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });
                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    #region 导入内容业务检查
                    var groups = tempRightList.GroupBy(r => new {
                        r.BranchNameStr,
                        r.PartCodeStr,
                        //r.PartsSalesCategoryNameStr
                    }).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        groupItem.ErrorMsg = ErrorStrings.ImpPartsSupplierRelation_Validation31;
                    }
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();

                    var partCodesNeedCheck = tempRightList.Select(r => r.PartCodeStr.ToUpper()).Distinct().ToArray();
                    var dbSparePartCodes = new List<PartsBranchExtend>();
                    Func<string[], bool> getDbSparePartCodes = value => {
                        var dbObj = new PartsBranchExtend {
                            PartId = Convert.ToInt32(value[0]),
                            PartCode = value[1],
                            PartName = value[2],
                            OverseasPartsFigure = value[3]
                        };
                        dbSparePartCodes.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select id,Trim(Code),Trim(Name),Trim(OverseasPartsFigure) from sparePart where status=1 ", "Code", true, partCodesNeedCheck, getDbSparePartCodes);
                    foreach(var errorItem in tempRightList) {
                        var oldSparePart = dbSparePartCodes.FirstOrDefault(v => v.PartCode == errorItem.PartCodeStr);
                        if(oldSparePart == null) {
                            errorItem.ErrorMsg = String.Format(ErrorStrings.Export_Validation_PartsBranch_SparePartIsNull, errorItem.PartCodeStr);
                        } else {
                            errorItem.PartId = oldSparePart.PartId;
                            errorItem.PartName = oldSparePart.PartName;

                            //海外配件图号检查
                            if(errorItem.PartsSalesCategoryNameStr == "海外乘用车" || errorItem.PartsSalesCategoryNameStr == "海外轻卡" || errorItem.PartsSalesCategoryNameStr == "海外中重卡") {

                                //if(string.IsNullOrEmpty(oldSparePart.OverseasPartsFigure)) {
                                //    errorItem.ErrorMsg = String.Format("海外配件图号为空", errorItem.PartCodeStr);
                                //} else
                                if(!string.IsNullOrEmpty(oldSparePart.OverseasPartsFigure)) {
                                    if(Encoding.Default.GetByteCount(oldSparePart.OverseasPartsFigure) > 18) {
                                        errorItem.ErrorMsg = String.Format(ErrorStrings.Export_Validation_PartsBranch_OverseasPartsFigureIsLong, errorItem.PartCodeStr);
                                    } else {
                                        errorItem.PartId = oldSparePart.PartId;
                                        errorItem.PartName = oldSparePart.PartName;
                                    }
                                }
                            }
                        }
                    }
                    ////海外配件图号检查
                    //tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();
                    //var overseasPartsFigureNeedCheck = tempRightList.Select(r => r.PartCodeStr.ToUpper()).Distinct().ToArray();
                    //var dbOverseasPartsFigures = new List<PartsBranchExtend>();
                    //Func<string[], bool> getDbOverseasPartsFigures = value => {
                    //    var dbObj = new PartsBranchExtend {
                    //        PartId = Convert.ToInt32(value[0]),
                    //        PartCode = value[1],
                    //        OverseasPartsFigure = value[2]

                    //    };
                    //    dbOverseasPartsFigures.Add(dbObj);
                    //    return false;
                    //};
                    //db.QueryDataWithInOperator("select id,Trim(Code),Trim(OverseasPartsFigure) from sparePart where status=1 ", "Code", true, overseasPartsFigureNeedCheck, getDbOverseasPartsFigures);
                    //foreach(var errorItem in tempRightList) {
                    //    var oldSparePart = dbOverseasPartsFigures.FirstOrDefault(v => v.PartCode == errorItem.PartCodeStr && (errorItem.PartsSalesCategoryNameStr == "海外乘用车" || errorItem.PartsSalesCategoryNameStr == "海外轻卡" || errorItem.PartsSalesCategoryNameStr == "海外中重卡"));
                    //    if(oldSparePart != null) {

                    //        if(string.IsNullOrEmpty(oldSparePart.OverseasPartsFigure)) {
                    //            errorItem.ErrorMsg = String.Format("海外配件图号为空", errorItem.PartCodeStr);
                    //        } else if(Encoding.Default.GetByteCount(oldSparePart.OverseasPartsFigure) > 18) {
                    //            errorItem.ErrorMsg = String.Format(ErrorStrings.Export_Validation_PartsBranch_OverseasPartsFigureIsLong, errorItem.PartCodeStr);
                    //        } else {
                    //            errorItem.PartId = oldSparePart.PartId;
                    //            errorItem.PartName = oldSparePart.PartName;
                    //        }
                    //    }
                    //}
                    //var errorSparePartCode = tempRightList.Where(r => dbSparePartCodes.All(v => v.PartCode != r.PartCodeStr)).ToList();
                    //foreach(var errorItem in errorSparePartCode) {
                    //    errorItem.ErrorMsg = "配件图号在系统中不存在";
                    //}
                    //tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();
                    //var errorSpareParts = tempRightList.Where(r => dbSparePartCodes.Any(v => v.PartCode == r.PartCodeStr && v.PartName != r.PartNameStr)).ToList();
                    //foreach(var errorItem in errorSpareParts) {
                    //    errorItem.ErrorMsg = "配件名称与图号不匹配";
                    //}

                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();

                    var branchNamesNeedCheck = tempRightList.Select(r => r.BranchNameStr).Distinct().ToArray();
                    var dbBranches = new List<PartsBranchExtend>();
                    Func<string[], bool> getDbBranches = value => {
                        var dbObj = new PartsBranchExtend {
                            BranchId = Convert.ToInt32(value[0]),
                            BranchCode = value[1],
                            BranchName = value[2]
                        };
                        dbBranches.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select Id,Code,Name from Branch where status=1 ", "Name", true, branchNamesNeedCheck, getDbBranches);
                    var errorBranches = tempRightList.Where(r => dbBranches.Any(v => v.BranchName != r.BranchNameStr)).ToList();
                    foreach(var errorItem in errorBranches) {
                        errorItem.ErrorMsg = ErrorStrings.Export_Validation_PartsBranch_ErrorBranches;
                    }
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();
                    var IncreaseRateGroupCodeCheck = tempRightList.Where(r => r.IncreaseRateGroupCode != null).Select(r => r.IncreaseRateGroupCode).Distinct().ToArray();
                    var dbIncreaseRateGroupCode = new List<PartsBranchExtend>();
                    Func<string[], bool> getIncreaseRateGroupCodes = value => {
                        var dbObj = new PartsBranchExtend {
                            IncreaseRateGroupId = Convert.ToInt32(value[0]),
                            IncreaseRateGroupCode = value[1],
                            IncreaseRateGroupName = value[2]
                        };
                        dbIncreaseRateGroupCode.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select * from PartsSalePriceIncreaseRate where STATUS=1", "GroupCode", true, IncreaseRateGroupCodeCheck, getIncreaseRateGroupCodes);
                    var errorIncreaseRateGroupCodeCheck = tempRightList.Where(r => dbIncreaseRateGroupCode.Any(v => v.IncreaseRateGroupCode != r.IncreaseRateGroupCode) && r.IncreaseRateGroupCode != null).ToList();
                    foreach(var errorItem in errorIncreaseRateGroupCodeCheck) {
                        errorItem.ErrorMsg = ErrorStrings.Export_Validation_PartsBranch_IncreaseRateError;
                    }
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();

                    var branchCodes = dbBranches.Select(r => r.BranchCode).Distinct().ToArray();
                    var dbPartsSalesCategories = new List<PartsBranchExtend>();
                    Func<string[], bool> getDbPartsSalesCategories = value => {
                        var dbObj = new PartsBranchExtend {
                            PartsSalesCategoryId = Convert.ToInt32(value[0]),
                            PartsSalesCategoryName = value[1],
                            BranchName = value[2]
                        };
                        dbPartsSalesCategories.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select id,name,branchName,branchCode from PartsSalesCategory where status=1 ", "branchCode", true, branchCodes, getDbPartsSalesCategories);
                    foreach(var item in tempRightList) {
                        var oldPtsCategory = dbPartsSalesCategories.FirstOrDefault(v => v.BranchName == item.BranchNameStr);
                        if(oldPtsCategory != null) {
                            item.PartsSalesCategoryNameStr = oldPtsCategory.PartsSalesCategoryName;
                        } else {
                            item.ErrorMsg = ErrorStrings.Export_Title_PartsBranch_BranchName + item.BranchNameStr + ErrorStrings.Export_Validation_PartsBranch_PartssalescategoryName;
                        }
                    }
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();

                    var partsBranchNeedCheck = tempRightList.Select(r => r.PartCodeStr).Distinct().ToArray();
                    var dbPartsBranches = new List<PartsBranchExtend>();
                    Func<string[], bool> getDbPartsBranches = value => {
                        var dbObj = new PartsBranchExtend {
                            PartsSalesCategoryName = value[0],
                            BranchName = value[1],
                            PartCode = value[2]
                        };
                        dbPartsBranches.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select partssalescategoryName,branchName,partcode from partsbranch where status=1 ", "PartCode", true, partsBranchNeedCheck, getDbPartsBranches);
                    var errorPartsBranches = tempRightList.Where(r => dbPartsBranches.Any(v => v.PartsSalesCategoryName == r.PartsSalesCategoryNameStr && v.BranchName == r.BranchNameStr && v.PartCode == r.PartCodeStr)).ToList();
                    foreach(var errorItem in errorPartsBranches) {
                        errorItem.ErrorMsg = ErrorStrings.Export_Validation_PartsBranch_Report;
                    }

                    var dbSalesCenterstrategies = new List<PartsBranchExtend>();
                    var dbSpareParts = new List<PartsBranchExtend>();
                    var partsSalesCenterstrategies = tempRightList.Select(r => r.PartsSalesCategoryNameStr).Distinct().ToArray();
                    var partsSpareParts = tempRightList.Select(r => r.PartCodeStr.ToUpper()).Distinct().ToArray();
                    Func<string[], bool> getDbsSalesCenterstrategies = value => {
                        var dbObj = new PartsBranchExtend {
                            PartsSalesCategoryId = Convert.ToInt32(value[0]),
                            PartsSalesCategoryName = value[1],
                            IsEngNameRequired = value[2]
                        };
                        dbSalesCenterstrategies.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select p.PartsSalesCategoryId,p.PartsSalesCategoryName,s.IsEngNameRequired from PartsBranch p left join SalesCenterstrategy s on p.PartsSalesCategoryId=s.PartsSalesCategoryId where p.status=1 ", "p.PartsSalescategoryName", true, partsSalesCenterstrategies, getDbsSalesCenterstrategies);
                    dbSalesCenterstrategies = dbSalesCenterstrategies.Where((x, i) => dbSalesCenterstrategies.FindIndex(z => z.PartsSalesCategoryId == x.PartsSalesCategoryId) == i).ToList();
                    foreach(var item in tempRightList) {
                        if(dbSalesCenterstrategies.FirstOrDefault(r => r.PartsSalesCategoryName == item.PartsSalesCategoryNameStr) != null) {
                            item.IsEngNameRequired = dbSalesCenterstrategies.FirstOrDefault(r => r.PartsSalesCategoryName == item.PartsSalesCategoryNameStr).IsEngNameRequired;
                        }
                    }

                    Func<string[], bool> getDbsSpareparts = value => {
                        var dbObj = new PartsBranchExtend {
                            PartCode = value[0],
                            EnglishName = value[1],
                            PartName = value[2]
                        };
                        dbSpareParts.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select Code,EnglishName,Name from SparePart where status=1", "Code", true, partsSpareParts, getDbsSpareparts);
                    var errorEnglishNames = tempRightList.Where(r => dbSpareParts.Where(s => s.PartCode == r.PartCodeStr && string.IsNullOrEmpty(s.EnglishName)).Any() && r.IsEngNameRequired == "1");
                    foreach(var errorItem in errorEnglishNames) {
                        errorItem.ErrorMsg = errorItem.PartNameStr + ErrorStrings.Export_Validation_PartsBranch_EnglishNameIsNull;
                    }


                    #endregion

                    //获取所有不合格数据
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    //获取所有合格数据
                    rightList = allList.Except(errorList).ToList();
                    //将合格数据的值填上
                    foreach(var rightItem in rightList) {
                        rightItem.PartsSalesCategoryName = rightItem.PartsSalesCategoryNameStr;
                        rightItem.PartCode = rightItem.PartCodeStr;
                        //rightItem.PartName = rightItem.PartNameStr;
                        rightItem.BranchName = rightItem.BranchNameStr;
                        rightItem.ReferenceCode = rightItem.ReferenceCodeStr;
                        rightItem.IsOrderable = rightItem.IsOrderableStr == ErrorStrings.Export_Title_PartsBranch_Yes;
                        rightItem.IsSalable = rightItem.IsSalableStr == ErrorStrings.Export_Title_PartsBranch_Yes;
                        rightItem.IsDirectSupply = rightItem.IsDirectSupplyStr == ErrorStrings.Export_Title_PartsBranch_Yes;
                        rightItem.IncreaseRateGroupCode = rightItem.IncreaseRateGroupCode;

                        try {
                            rightItem.MinSaleQuantity = Convert.ToInt32(rightItem.MinSaleQuantityStr);
                        } catch {
                            rightItem.MinSaleQuantity = null;
                        }

                        rightItem.ProductLifeCycle = tempExcelOperator.ImportHelper.GetEnumValue("ProductLifeCycle", rightItem.ProductLifeCycleStr) ?? 0;
                        rightItem.LossType = tempExcelOperator.ImportHelper.GetEnumValue("LossType", rightItem.LossTypeStr) ?? 0;
                        rightItem.PartsAttribution = tempExcelOperator.ImportHelper.GetEnumValue("PartsAttribution", rightItem.PartsAttributionStr) ?? 0;
                        rightItem.PartsReturnPolicy = tempExcelOperator.ImportHelper.GetEnumValue("PartsReturnPolicy", rightItem.PartsReturnPolicyStr) ?? 0;
                        //rightItem.PartABC = tempExcelOperator.ImportHelper.GetEnumValue("PartABC", rightItem.PartABCStr == null ? rightItem.PartABCStr : rightItem.PartABCStr.ToLower()) ?? 0;
                        try {
                            rightItem.StockMaximum = Convert.ToInt32(rightItem.StockMaximumStr);
                        } catch {
                            rightItem.StockMaximum = null;
                        }
                        try {
                            rightItem.StockMinimum = Convert.ToInt32(rightItem.StockMinimumStr);
                        } catch {
                            rightItem.StockMinimum = null;
                        }
                        rightItem.Remark = rightItem.RemarkStr;
                        rightItem.Status = tempExcelOperator.ImportHelper.GetEnumValue("Status", ErrorStrings.Export_Validation_PartsBranch_Used) ?? 0;
                        var tempDbSparePartCode = dbSparePartCodes.Single(r => r.PartCode == rightItem.PartCode);
                        rightItem.PartId = tempDbSparePartCode.PartId;
                        var tempDbBranch = dbBranches.Single(r => r.BranchName == rightItem.BranchNameStr);
                        rightItem.BranchId = tempDbBranch.BranchId;
                        var tempdbPartsSalesCategory = dbPartsSalesCategories.FirstOrDefault(r => r.PartsSalesCategoryName == rightItem.PartsSalesCategoryNameStr);
                        if(tempdbPartsSalesCategory != null) {
                            rightItem.PartsSalesCategoryId = tempdbPartsSalesCategory.PartsSalesCategoryId;
                        }
                        var tempIncreaseRateGroup = dbIncreaseRateGroupCode.FirstOrDefault(r => r.IncreaseRateGroupCode == rightItem.IncreaseRateGroupCode);
                        if(tempIncreaseRateGroup != null) {
                            rightItem.IncreaseRateGroupId = tempIncreaseRateGroup.IncreaseRateGroupId;
                            rightItem.IncreaseRateGroupName = tempIncreaseRateGroup.IncreaseRateGroupName;
                        } else {
                            rightItem.IncreaseRateGroupId = default(int);
                            rightItem.IncreaseRateGroupName = null;
                        }
                    }
                }

                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempObj = list[index - 1];
                            var values = new object[] {
                                tempObj.PartsSalesCategoryNameStr, tempObj.PartCodeStr, tempObj.BranchNameStr, tempObj.IsOrderableStr, tempObj.IsSalableStr, tempObj.IsDirectSupplyStr, tempObj.MinSaleQuantityStr, tempObj.ProductLifeCycleStr, tempObj.LossTypeStr, tempObj.PartsAttributionStr, tempObj.PartsReturnPolicyStr, tempObj.StockMaximumStr, tempObj.StockMinimumStr, tempObj.PartsMaterialManageCostStr, tempObj.PartsWarrantyLongStr, tempObj.IncreaseRateGroupCode, tempObj.IncreaseRateGroupName, tempObj.AutoApproveUpLimitStr, tempObj.RemarkStr, tempObj.ErrorMsg
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }

                //导入所有合格数据
                if(!rightList.Any())
                    return true;
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    //开启事务，新增更新配件在一个事务内
                    var ts = conn.BeginTransaction();
                    try {
                        //新增配件
                        if(rightList.Any()) {
                            //获取新增数据的sql语句，Id为主键
                            var sqlInsert = db.GetInsertSql("PartsBranch", "Id", new[] {
                                "PartId", "PartCode", "PartName", "ProductLifeCycle", "StockMaximum", "StockMinimum", "PartsMaterialManageCost", "PartsWarrantyLong", "BranchId", "BranchName", "PartsSalesCategoryId", "PartsSalesCategoryName", "LossType", "PartsAttribution", "IsOrderable", "IsSalable", "PartsReturnPolicy", "INCREASERATEGROUPID", "IsDirectSupply", "MinSaleQuantity", "AutoApproveUpLimit", "Status", "CreatorId", "CreatorName", "CreateTime","OMSparePartMark"
                            });
                            var sqlHistoryInsert = db.GetInsertSql("PartsBranchHistory", "Id", new[] {
                                "PartsBranchId", "PartId", "PartCode", "PartName", "ProductLifeCycle", "StockMaximum", "StockMinimum", "PartsMaterialManageCost", "PartsWarrantyLong", "BranchId", "BranchName", "PartsSalesCategoryId", "PartsSalesCategoryName", "LossType", "PartsAttribution", "IsOrderable", "IsSalable", "PartsReturnPolicy", "INCREASERATEGROUPID", "IsDirectSupply", "MinSaleQuantity", "AutoApproveUpLimit", "Status", "CreatorId", "CreatorName", "CreateTime"
                            });
                            var userInfo = Utils.GetCurrentUserInfo();
                            //往数据库增加配件信息
                            foreach(var item in rightList) {
                                var command = db.CreateDbCommand(sqlInsert, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("PartId", item.PartId));
                                command.Parameters.Add(db.CreateDbParameter("PartCode", item.PartCode));
                                command.Parameters.Add(db.CreateDbParameter("PartName", item.PartName));
                                //command.Parameters.Add(db.CreateDbParameter("ReferenceCode", item.ReferenceCode));
                                command.Parameters.Add(db.CreateDbParameter("StockMaximum", item.StockMaximum));
                                command.Parameters.Add(db.CreateDbParameter("StockMinimum", item.StockMinimum));
                                command.Parameters.Add(db.CreateDbParameter("PartsMaterialManageCost", item.PartsMaterialManageCost));
                                command.Parameters.Add(db.CreateDbParameter("PartsWarrantyLong", item.PartsWarrantyLong));
                                command.Parameters.Add(db.CreateDbParameter("BranchId", item.BranchId));
                                command.Parameters.Add(db.CreateDbParameter("BranchName", item.BranchName));
                                command.Parameters.Add(db.CreateDbParameter("PartsSalesCategoryId", item.PartsSalesCategoryId));
                                command.Parameters.Add(db.CreateDbParameter("PartsSalesCategoryName", item.PartsSalesCategoryName));
                                command.Parameters.Add(db.CreateDbParameter("ProductLifeCycle", item.ProductLifeCycle));
                                command.Parameters.Add(db.CreateDbParameter("LossType", item.LossType));
                                command.Parameters.Add(db.CreateDbParameter("PartsAttribution", item.PartsAttribution));
                                command.Parameters.Add(db.CreateDbParameter("IsOrderable", item.IsOrderable));
                                command.Parameters.Add(db.CreateDbParameter("IsSalable", item.IsSalable));
                                command.Parameters.Add(db.CreateDbParameter("PartsReturnPolicy", item.PartsReturnPolicy));
                                command.Parameters.Add(db.CreateDbParameter("INCREASERATEGROUPID", item.IncreaseRateGroupId));
                                command.Parameters.Add(db.CreateDbParameter("IsDirectSupply", item.IsDirectSupply));
                                command.Parameters.Add(db.CreateDbParameter("MinSaleQuantity", item.MinSaleQuantity));
                                command.Parameters.Add(db.CreateDbParameter("AutoApproveUpLimit", item.AutoApproveUpLimit));
                                //command.Parameters.Add(db.CreateDbParameter("PartABC", item.PartABC));
                                command.Parameters.Add(db.CreateDbParameter("Status", item.Status));
                                command.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                command.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                command.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                command.Parameters.Add(db.CreateDbParameter("OMSparePartMark", 0));
                                var tempId = db.ExecuteInsert(command, "Id");
                                同步配件营销信息(db, item, conn, ts, rightList);
                                //加履历
                                var commandHistory = db.CreateDbCommand(sqlHistoryInsert, conn, ts);
                                commandHistory.Parameters.Add(db.CreateDbParameter("PartsBranchId", tempId));
                                commandHistory.Parameters.Add(db.CreateDbParameter("PartId", item.PartId));
                                commandHistory.Parameters.Add(db.CreateDbParameter("PartCode", item.PartCode));
                                commandHistory.Parameters.Add(db.CreateDbParameter("PartName", item.PartName));
                                //commandHistory.Parameters.Add(db.CreateDbParameter("ReferenceCode", item.ReferenceCode));
                                commandHistory.Parameters.Add(db.CreateDbParameter("StockMaximum", item.StockMaximum));
                                commandHistory.Parameters.Add(db.CreateDbParameter("StockMinimum", item.StockMinimum));
                                commandHistory.Parameters.Add(db.CreateDbParameter("PartsMaterialManageCost", item.PartsMaterialManageCost));
                                commandHistory.Parameters.Add(db.CreateDbParameter("PartsWarrantyLong", item.PartsWarrantyLong));
                                commandHistory.Parameters.Add(db.CreateDbParameter("BranchId", item.BranchId));
                                commandHistory.Parameters.Add(db.CreateDbParameter("BranchName", item.BranchName));
                                commandHistory.Parameters.Add(db.CreateDbParameter("PartsSalesCategoryId", item.PartsSalesCategoryId));
                                commandHistory.Parameters.Add(db.CreateDbParameter("PartsSalesCategoryName", item.PartsSalesCategoryName));
                                commandHistory.Parameters.Add(db.CreateDbParameter("ProductLifeCycle", item.ProductLifeCycle));
                                commandHistory.Parameters.Add(db.CreateDbParameter("LossType", item.LossType));
                                commandHistory.Parameters.Add(db.CreateDbParameter("PartsAttribution", item.PartsAttribution));
                                commandHistory.Parameters.Add(db.CreateDbParameter("IsOrderable", item.IsOrderable));
                                commandHistory.Parameters.Add(db.CreateDbParameter("IsSalable", item.IsSalable));
                                commandHistory.Parameters.Add(db.CreateDbParameter("PartsReturnPolicy", item.PartsReturnPolicy));
                                commandHistory.Parameters.Add(db.CreateDbParameter("INCREASERATEGROUPID", item.IncreaseRateGroupId));
                                commandHistory.Parameters.Add(db.CreateDbParameter("IsDirectSupply", item.IsDirectSupply));
                                commandHistory.Parameters.Add(db.CreateDbParameter("MinSaleQuantity", item.MinSaleQuantity));
                                commandHistory.Parameters.Add(db.CreateDbParameter("AutoApproveUpLimit", item.AutoApproveUpLimit));
                                //commandHistory.Parameters.Add(db.CreateDbParameter("PartABC", item.PartABC));
                                commandHistory.Parameters.Add(db.CreateDbParameter("Status", item.Status));
                                commandHistory.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                commandHistory.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                commandHistory.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                commandHistory.ExecuteNonQuery();
                            }

                            #region 当配件生成 奥铃、欧马可、蒙派克风景、拓陆者萨普、集团配件、红岩配件 中任意一种品牌的分品牌信息时，系统自动生成该配件 随车行 品牌的分品牌信息
                            rightList = rightList.Where(r => r.PartsSalesCategoryName == "奥铃" || r.PartsSalesCategoryName == "欧马可" || r.PartsSalesCategoryName == "蒙派克风景" || r.PartsSalesCategoryName == "拓陆者萨普" || r.PartsSalesCategoryName == "集团配件" || r.PartsSalesCategoryName == "红岩配件").ToList();
                            if(rightList.Any()) {
                                rightList = rightList.Where((x, i) => rightList.FindIndex(z => z.PartCode == x.PartCode) == i).ToList();
                                var sqlPartsSalesCategory = string.Format("select Id,Name from PartsSalesCategory where Name='随车行'");
                                var cmdPartsSalesCategory = db.CreateDbCommand(sqlPartsSalesCategory, conn, null);
                                DataTable dbPartsSalesCategory = new DataTable();
                                dbPartsSalesCategory.Load(cmdPartsSalesCategory.ExecuteReader());


                                //查询是否有重复的随车行品牌信息

                                var objIds = new List<string>();
                                foreach(var item in rightList) {
                                    objIds.Add(item.PartCode);
                                }
                                string reqparts = getOracleSQLIn(objIds, "PartCode");
                                //var SpareCodestr = string.Join("','", objIds.ToArray());
                                var sqlhaverepeat = string.Format("select Id,PartCode from PartsBranch where PartsSalesCategoryName='随车行' and " + reqparts);
                                var cmdhaverepeat = db.CreateDbCommand(sqlhaverepeat, conn, null);
                                DataTable dbhaverepeat = new DataTable();
                                dbhaverepeat.Load(cmdhaverepeat.ExecuteReader());
                                foreach(var item in rightList) {
                                    var drhaverepeat = dbhaverepeat.Select(" PartCode='" + item.PartCode + "'");
                                    if(drhaverepeat.Length == 0) {
                                        var sqlInsertSuiCheXing = db.GetInsertSql("PartsBranch", "Id", new[] {
                                            "PartId", "PartCode", "PartName", "BranchId", "BranchName", "MinSaleQuantity", "PartsSalesCategoryId", "PartsSalesCategoryName", "IsOrderable", "IsSalable", "PartsReturnPolicy", "IsDirectSupply", "Status", "CreatorId", "CreatorName", "CreateTime", "AutoApproveUpLimit"
                                        });
                                        var sqlHistorySuiCheXingInsert = db.GetInsertSql("PartsBranchHistory", "Id", new[] {
                                            "PartsBranchId", "PartId", "PartCode", "PartName", "MinSaleQuantity", "BranchId", "BranchName", "PartsSalesCategoryId", "PartsSalesCategoryName", "IsOrderable", "IsSalable", "PartsReturnPolicy", "IsDirectSupply", "Status", "CreatorId", "CreatorName", "CreateTime", "AutoApproveUpLimit"
                                        });
                                        var command = db.CreateDbCommand(sqlInsertSuiCheXing, conn, ts);
                                        command.Parameters.Add(db.CreateDbParameter("PartId", item.PartId));
                                        command.Parameters.Add(db.CreateDbParameter("PartCode", item.PartCode));
                                        command.Parameters.Add(db.CreateDbParameter("PartName", item.PartName));
                                        command.Parameters.Add(db.CreateDbParameter("BranchId", item.BranchId));
                                        command.Parameters.Add(db.CreateDbParameter("BranchName", item.BranchName));
                                        command.Parameters.Add(db.CreateDbParameter("PartsSalesCategoryId", dbPartsSalesCategory.Rows[0]["Id"]));
                                        command.Parameters.Add(db.CreateDbParameter("PartsSalesCategoryName", dbPartsSalesCategory.Rows[0]["Name"]));
                                        command.Parameters.Add(db.CreateDbParameter("IsOrderable", 1));
                                        command.Parameters.Add(db.CreateDbParameter("IsSalable", 1));
                                        command.Parameters.Add(db.CreateDbParameter("MinSaleQuantity", 1));
                                        command.Parameters.Add(db.CreateDbParameter("PartsReturnPolicy", (int)DcsPartsWarrantyTermReturnPolicy.返回本部));
                                        command.Parameters.Add(db.CreateDbParameter("IsDirectSupply", 0));
                                        command.Parameters.Add(db.CreateDbParameter("Status", item.Status));
                                        command.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                        command.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                        command.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                        command.Parameters.Add(db.CreateDbParameter("AutoApproveUpLimit", 100));
                                        var branchId = db.ExecuteInsert(command, "Id");
                                        var commandHistory = db.CreateDbCommand(sqlHistorySuiCheXingInsert, conn, ts);
                                        commandHistory.Parameters.Add(db.CreateDbParameter("PartsBranchId", branchId));
                                        commandHistory.Parameters.Add(db.CreateDbParameter("PartId", item.PartId));
                                        commandHistory.Parameters.Add(db.CreateDbParameter("PartCode", item.PartCode));
                                        commandHistory.Parameters.Add(db.CreateDbParameter("PartName", item.PartName));
                                        commandHistory.Parameters.Add(db.CreateDbParameter("BranchId", item.BranchId));
                                        commandHistory.Parameters.Add(db.CreateDbParameter("BranchName", item.BranchName));
                                        commandHistory.Parameters.Add(db.CreateDbParameter("PartsSalesCategoryId", dbPartsSalesCategory.Rows[0]["Id"]));
                                        commandHistory.Parameters.Add(db.CreateDbParameter("PartsSalesCategoryName", dbPartsSalesCategory.Rows[0]["Name"]));
                                        commandHistory.Parameters.Add(db.CreateDbParameter("IsOrderable", 1));
                                        commandHistory.Parameters.Add(db.CreateDbParameter("IsSalable", 1));
                                        commandHistory.Parameters.Add(db.CreateDbParameter("MinSaleQuantity", 1));
                                        commandHistory.Parameters.Add(db.CreateDbParameter("PartsReturnPolicy", (int)DcsPartsWarrantyTermReturnPolicy.返回本部));
                                        commandHistory.Parameters.Add(db.CreateDbParameter("IsDirectSupply", 0));
                                        commandHistory.Parameters.Add(db.CreateDbParameter("Status", item.Status));
                                        commandHistory.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                        commandHistory.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                        commandHistory.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                        commandHistory.Parameters.Add(db.CreateDbParameter("AutoApproveUpLimit", 100));
                                        commandHistory.ExecuteNonQuery();
                                    }
                                }
                            }
                            #endregion
                        }
                        //无异常提交
                        ts.Commit();
                    } catch(Exception) {
                        //报错回滚
                        ts.Rollback();
                        throw;
                    } finally {
                        if(conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }

        private void 同步配件营销信息(DbHelper db, PartsBranchExtend partsBranch, DbConnection conn, DbTransaction ts, List<PartsBranchExtend> importRightList) {
            var code1 = new[] {
                "999998", "999999", "999990"
            };
            //1. 新增   奥铃、蒙派克风景、拓路者   要新增  999998  999990  999999 3个品牌的份品牌信息
            var sqlPartsSalesCategory = string.Format("select count(*) from PartsSalesCategory where status=1 and code in ('104011','102012','102112','102111') and id={0}", partsBranch.PartsSalesCategoryId);
            var commandPartsSalesCategory = db.CreateDbCommand(sqlPartsSalesCategory, conn, null);
            var readerPartsSalesCategory = Convert.ToInt32(commandPartsSalesCategory.ExecuteScalar());
            if(readerPartsSalesCategory > 0) {
                var dbCode1 = new List<PartsSalesCategoryExtend>();
                Func<string[], bool> getDbCode1 = value => {
                    var dbObj = new PartsSalesCategoryExtend {
                        Id = Convert.ToInt32(value[0]),
                        Name = value[1],
                        Code = value[2],
                    };
                    dbCode1.Add(dbObj);
                    return false;
                };
                db.QueryDataWithInOperator("select Id,Name,code from PartsSalesCategory where status=1 ", "Code", true, code1, getDbCode1);
                foreach(var partsSalesCategoryExtend in dbCode1) {
                    var sql = string.Format("select count(*) from PartsBranch where Status=1  and PartId={0} and PartsSalesCategoryId={1}", partsBranch.PartId, partsSalesCategoryExtend.Id);
                    var command = db.CreateDbCommand(sql, conn, null);
                    var reader = Convert.ToInt32(command.ExecuteScalar());
                    if(reader == 0) {
                        //没有数据时候
                        if(!importRightList.Any(r => r.PartsSalesCategoryId == partsSalesCategoryExtend.Id && r.PartId == partsBranch.PartId)) //判断同步数据中是否与导入数据重复。 如果重复则不做同步
                        {
                            if(partsSalesCategoryExtend.Code == "999999" || partsSalesCategoryExtend.Code == "999991") //集团配件
                            {
                                partsBranch.IsSalable = true;
                                partsBranch.IsOrderable = true;
                                partsBranch.IsDirectSupply = false;
                                partsBranch.AutoApproveUpLimit = 100;
                            } else if(partsSalesCategoryExtend.Code == "999998" || partsSalesCategoryExtend.Code == "999990") //电子商务 红岩配件
                            {
                                partsBranch.IsSalable = true;
                                partsBranch.IsOrderable = false;
                                partsBranch.IsDirectSupply = false;
                                partsBranch.AutoApproveUpLimit = 100;
                            }
                            CreatePartsBranch(partsBranch, partsSalesCategoryExtend.Id, partsSalesCategoryExtend.Name, db, conn, ts);
                        }
                    }

                }
            }
            //2. 新增 999998  要同步给   999990  999999 2个品牌
            var sql2 = string.Format("select count(*) from PartsSalesCategory where Status=1  and  Code ='999998' and id={0}", partsBranch.PartsSalesCategoryId);
            var command2 = db.CreateDbCommand(sql2, conn, null);
            var reader2 = Convert.ToInt32(command2.ExecuteScalar());
            if(reader2 > 0) {
                var command3 = db.CreateDbCommand("select * from PartsSalesCategory where Status=1  and  Code in ('999990','999999') ", conn, null);
                var reader3 = command3.ExecuteReader();
                this.VerifyPartsBrancheExistence(db, reader3, conn, partsBranch, ts, importRightList);

            }
            //3.新增  999999  要同步给  999998  999990 2个品牌
            var sql4 = string.Format("select count(*) from PartsSalesCategory where Status=1  and  Code ='999999' and id={0}", partsBranch.PartsSalesCategoryId);
            var command4 = db.CreateDbCommand(sql4, conn, null);
            var reader4 = Convert.ToInt32(command4.ExecuteScalar());
            if(reader4 > 0) {
                var command3 = db.CreateDbCommand("select * from PartsSalesCategory where Status=1  and  Code in ('999990','999998') ", conn, null);
                var reader3 = command3.ExecuteReader();
                this.VerifyPartsBrancheExistence(db, reader3, conn, partsBranch, ts, importRightList);
            }
        }

        private void VerifyPartsBrancheExistence(DbHelper db, DbDataReader reader, DbConnection conn, PartsBranchExtend partsBranch, DbTransaction ts, List<PartsBranchExtend> importRightList) {
            while(reader.Read()) {
                var sql = string.Format("select count(*) from PartsBranch where Status=1  and PartId={0} and PartsSalesCategoryId={1}", partsBranch.PartId, Convert.ToInt32(reader["Id"]));
                var command = db.CreateDbCommand(sql, conn, null);
                var reader1 = Convert.ToInt32(command.ExecuteScalar());
                if(reader1 == 0) {
                    //没有数据时候新增
                    if(!importRightList.Any(r => r.PartsSalesCategoryId == Convert.ToInt32(reader["Id"]) && r.PartId == partsBranch.PartId)) //判断同步数据中是否与导入数据重复。 如果重复则不做同步
                        CreatePartsBranch(partsBranch, Convert.ToInt32(reader["Id"]), reader["Name"].ToString(), db, conn, ts);
                }
            }
        }
        private void CreatePartsBranch(PartsBranchExtend partsBranch, int id, string name, DbHelper db, DbConnection conn, DbTransaction ts) {
            //获取新增数据的sql语句，Id为主键
            var sqlInsert = db.GetInsertSql("PartsBranch", "Id", new[] {
                "PartId", "PartCode", "PartName", "ReferenceCode", "StockMaximum", "StockMinimum", "PartsMaterialManageCost", "PartsWarrantyLong", "BranchId", "BranchName", "PartsSalesCategoryId", "PartsSalesCategoryName", "LossType", "PartsAttribution", "IsOrderable", "IsSalable", "PartsReturnPolicy", "INCREASERATEGROUPID", "IsDirectSupply", "MinSaleQuantity", "PartsWarrantyCategoryId", "PartsWarrantyCategoryCode", "PartsWarrantyCategoryName", "Status", "CreatorId", "CreatorName", "CreateTime", "AutoApproveUpLimit","OMSparePartMark"
            });
            var userInfo = Utils.GetCurrentUserInfo();
            var command = db.CreateDbCommand(sqlInsert, conn, ts);
            command.Parameters.Add(db.CreateDbParameter("PartId", partsBranch.PartId));
            command.Parameters.Add(db.CreateDbParameter("PartCode", partsBranch.PartCode));
            command.Parameters.Add(db.CreateDbParameter("PartName", partsBranch.PartName));
            command.Parameters.Add(db.CreateDbParameter("ReferenceCode", partsBranch.ReferenceCode));
            command.Parameters.Add(db.CreateDbParameter("StockMaximum", partsBranch.StockMaximum));
            command.Parameters.Add(db.CreateDbParameter("StockMinimum", partsBranch.StockMinimum));
            command.Parameters.Add(db.CreateDbParameter("PartsMaterialManageCost", partsBranch.PartsMaterialManageCost));
            command.Parameters.Add(db.CreateDbParameter("PartsWarrantyLong", partsBranch.PartsWarrantyLong));
            command.Parameters.Add(db.CreateDbParameter("BranchId", partsBranch.BranchId));
            command.Parameters.Add(db.CreateDbParameter("BranchName", partsBranch.BranchName));
            command.Parameters.Add(db.CreateDbParameter("PartsSalesCategoryId", id));
            command.Parameters.Add(db.CreateDbParameter("PartsSalesCategoryName", name));
            command.Parameters.Add(db.CreateDbParameter("LossType", partsBranch.LossType));
            command.Parameters.Add(db.CreateDbParameter("PartsAttribution", partsBranch.PartsAttribution));
            command.Parameters.Add(db.CreateDbParameter("IsOrderable", partsBranch.IsOrderable));
            command.Parameters.Add(db.CreateDbParameter("IsSalable", partsBranch.IsSalable));
            command.Parameters.Add(db.CreateDbParameter("PartsReturnPolicy", partsBranch.PartsReturnPolicy));
            command.Parameters.Add(db.CreateDbParameter("INCREASERATEGROUPID", partsBranch.IncreaseRateGroupId));
            command.Parameters.Add(db.CreateDbParameter("IsDirectSupply", partsBranch.IsDirectSupply));
            command.Parameters.Add(db.CreateDbParameter("MinSaleQuantity", partsBranch.MinSaleQuantity));
            command.Parameters.Add(db.CreateDbParameter("PartsWarrantyCategoryId", partsBranch.PartsWarrantyCategoryId));
            command.Parameters.Add(db.CreateDbParameter("PartsWarrantyCategoryCode", partsBranch.PartsWarrantyCategoryCode));
            command.Parameters.Add(db.CreateDbParameter("PartsWarrantyCategoryName", partsBranch.PartsWarrantyCategoryName));
            command.Parameters.Add(db.CreateDbParameter("AutoApproveUpLimit", partsBranch.AutoApproveUpLimit));
            command.Parameters.Add(db.CreateDbParameter("Status", partsBranch.Status));
            command.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
            command.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
            command.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
            command.Parameters.Add(db.CreateDbParameter("OMSparePartMark", 0));
            partsBranch.Id = db.ExecuteInsert(command, "Id");
            //增加营销信息履历
            InsertPartsBranchHistory(partsBranch, db, conn, ts);
        }

        /// <summary>
        /// 校验导入修改配件营销信息
        /// </summary>
        public bool ImportUpdatePartsBranch(string fileName, out int excelImportNum, out List<PartsBranchExtend> rightData, out List<PartsBranchExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<PartsBranchExtend>();
            var rightList = new List<PartsBranchExtend>();
            var allList = new List<PartsBranchExtend>();
            var dbPartsBranches = new List<PartsBranchExtend>();
            var dbSparePartCodes = new List<PartsBranchExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("PartsBranch", out notNullableFields, out fieldLenght);
                List<object> excelColumns;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Code, "PartCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Partssalescategory_Name, "PartsSalesCategoryName");
                    //excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName, "PartName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_BranchName, "BranchName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_IsOrderableNew, "IsOrderable");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_IsSalableNew, "IsSalable");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_IsDirectSupplyNew, "IsDirectSupply");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Minsalequantity, "MinSaleQuantity");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_ProductLifeCycle, "ProductLifeCycle");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_LossType, "LossType");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_PartsAttribution, "PartsAttribution");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_PartsReturnPolicy, "PartsReturnPolicy");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_StockMaximum, "StockMaximum");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_StockMinimum, "StockMinimum");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_PartsMaterialManageCost, "PartsMaterialManageCost");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_PartsWarrantyLong, "PartsWarrantyLong");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_IncreaseRateGroupCode, "IncreaseRateGroupCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_PriceType, "IncreaseRateGroupName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_AutoApproveUpLimit, "AutoApproveUpLimit");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Remark, "Remark");
                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;

                    ////单次最大导入条数不能大于500！
                    //if(excelOperator.LastRowNum > 500) {
                    //    throw new Exception("单次最大导入条数不能大于500！");
                    //}

                    var keyValuePairs = new[] {
                        new KeyValuePair<string, string>("PartABC", "ABCStrategy_Category"), new KeyValuePair<string, string>("ProductLifeCycle", "PartsBranch_ProductLifeCycle"), new KeyValuePair<string, string>("LossType", "SparePart_LossType"), new KeyValuePair<string, string>("PartsAttribution", "PartsAttribution"), new KeyValuePair<string, string>("Status", "BaseData_Status"), new KeyValuePair<string, string>("PartsReturnPolicy", "PartsWarrantyTerm_ReturnPolicy")
                    };
                    tempExcelOperator.ImportHelper.LoadEnumFromDb(null, null, keyValuePairs);
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var tempImportObj = new PartsBranchExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);

                        #region
                        tempImportObj.PartsSalesCategoryNameStr = newRow["PartsSalesCategoryName"];
                        tempImportObj.PartCodeStr = newRow["PartCode"];
                        //tempImportObj.PartNameStr = newRow["PartName"];
                        tempImportObj.BranchNameStr = newRow["BranchName"];
                        tempImportObj.IsOrderableStr = newRow["IsOrderable"];
                        tempImportObj.IsSalableStr = newRow["IsSalable"];
                        tempImportObj.IsDirectSupplyStr = newRow["IsDirectSupply"];
                        tempImportObj.MinSaleQuantityStr = newRow["MinSaleQuantity"];
                        tempImportObj.ProductLifeCycleStr = newRow["ProductLifeCycle"];
                        tempImportObj.LossTypeStr = newRow["LossType"];
                        tempImportObj.PartsAttributionStr = newRow["PartsAttribution"];
                        tempImportObj.PartsReturnPolicyStr = newRow["PartsReturnPolicy"];
                        tempImportObj.StockMaximumStr = newRow["StockMaximum"];
                        tempImportObj.StockMinimumStr = newRow["StockMinimum"];
                        tempImportObj.PartsMaterialManageCostStr = newRow["PartsMaterialManageCost"];
                        tempImportObj.PartsWarrantyLongStr = newRow["PartsWarrantyLong"];
                        tempImportObj.IncreaseRateGroupCode = newRow["IncreaseRateGroupCode"];
                        tempImportObj.IncreaseRateGroupName = newRow["IncreaseRateGroupName"];
                        tempImportObj.AutoApproveUpLimitStr = newRow["AutoApproveUpLimit"];
                        tempImportObj.RemarkStr = newRow["Remark"];
                        #endregion

                        var tempErrorMessage = new List<string>();

                        #region 导入的内容基本检查

                        //品牌检查
                        //var fieldIndex = notNullableFields.IndexOf("PartsSalesCategoryName".ToUpper());
                        //if(string.IsNullOrEmpty(tempImportObj.PartsSalesCategoryNameStr)) {
                        //    if(fieldIndex > -1)
                        //        tempErrorMessage.Add(ErrorStrings.Export_Validation_MarketingDepartment_PartsSalesCategoryIsNull);
                        //} else {
                        //    if(Encoding.Default.GetByteCount(tempImportObj.PartsSalesCategoryNameStr) > fieldLenght["PartsSalesCategoryName".ToUpper()])
                        //        tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_PartsSalescategoryNameIsLong);
                        //}
                        //配件编号
                        var fieldIndex = notNullableFields.IndexOf("PartCode".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.PartCodeStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.OverstockPartsAppDetail__Validation1);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.PartCodeStr) > fieldLenght["PartCode".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.ImpPartsSupplierRelation_Validation8);
                        }
                        ////配件名称检查
                        //fieldIndex = notNullableFields.IndexOf("PartName".ToUpper());
                        //if(string.IsNullOrEmpty(tempImportObj.PartNameStr)) {
                        //    ;
                        //    //if(fieldIndex > -1)
                        //    //    tempErrorMessage.Add(ErrorStrings.Export_Validation_Sparepart_Name);
                        //} else {
                        //    if(Encoding.Default.GetByteCount(tempImportObj.PartNameStr) > fieldLenght["PartName".ToUpper()])
                        //        tempErrorMessage.Add(ErrorStrings.Export_Validation_Sparepart_NameIsLong);
                        //}
                        //营销分公司检查
                        fieldIndex = notNullableFields.IndexOf("BranchName".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.BranchNameStr)) {
                            //if(fieldIndex > -1)
                            //    tempErrorMessage.Add("营销分公司不能为空");
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.BranchNameStr) > fieldLenght["BranchName".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_ErrorBranchesIsLong);
                        }
                        ////配件参图号
                        //fieldIndex = notNullableFields.IndexOf("ReferenceCode".ToUpper());
                        //if(string.IsNullOrEmpty(tempImportObj.ReferenceCodeStr)) {
                        //    if(fieldIndex > -1)
                        //        tempErrorMessage.Add("零部件图号不能为空");
                        //} else {
                        //    if(Encoding.Default.GetByteCount(tempImportObj.ReferenceCodeStr) > fieldLenght["ReferenceCode".ToUpper()])
                        //        tempErrorMessage.Add(ErrorStrings.Export_Validation_Sparepart_OverseasPartsFigureOverLong);
                        //}
                        //是否可采购
                        fieldIndex = notNullableFields.IndexOf("IsOrderable".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.IsOrderableStr)) {
                            //if(fieldIndex > -1)
                            //    tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_IsOrderableIsNull);
                        } else {
                            if(tempImportObj.IsOrderableStr != ErrorStrings.Export_Title_PartsBranch_Yes && tempImportObj.IsOrderableStr != ErrorStrings.Export_Title_PartsBranch_No)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_IsOrderableValueError);
                        }
                        //是否可销售
                        fieldIndex = notNullableFields.IndexOf("IsSalable".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.IsSalableStr)) {
                            //if(fieldIndex > -1)
                            //    tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_IsSalableIsNull);
                        } else {
                            if(tempImportObj.IsSalableStr != ErrorStrings.Export_Title_PartsBranch_Yes && tempImportObj.IsSalableStr != ErrorStrings.Export_Title_PartsBranch_No)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_IsSalableValueError);
                        }
                        //是否直供
                        fieldIndex = notNullableFields.IndexOf("IsDirectSupply".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.IsDirectSupplyStr)) {
                            //if(fieldIndex > -1)
                            //    tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_IsDirectSupplyIsNull);
                        } else {
                            if(tempImportObj.IsDirectSupplyStr != ErrorStrings.Export_Title_PartsBranch_Yes && tempImportObj.IsDirectSupplyStr != ErrorStrings.Export_Title_PartsBranch_No)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_IsDirectSupplyValueError);
                        }
                        //最小销售批量
                        fieldIndex = notNullableFields.IndexOf("MinSaleQuantity".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.MinSaleQuantityStr)) {
                            //if(fieldIndex > -1)
                            //    tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_MinSaleQuantityIsNull);
                        } else {
                            int checkValue;
                            if(int.TryParse(tempImportObj.MinSaleQuantityStr, out checkValue)) {
                                if(checkValue <= 0) {
                                    tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_MinSaleQuantityIsOverZero);
                                }
                            } else {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_MinSaleQuantityIsNumber);
                            }
                        }
                        //产品生命周期
                        fieldIndex = notNullableFields.IndexOf("ProductLifeCycle".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.ProductLifeCycleStr)) {
                            //if(fieldIndex > -1)
                            //    tempErrorMessage.Add("产品生命周期不能为空");
                        } else {
                            var tempEnumValue = tempExcelOperator.ImportHelper.GetEnumValue("ProductLifeCycle", tempImportObj.ProductLifeCycleStr);
                            if(!tempEnumValue.HasValue) {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_ProductLifeCycleError);
                            }
                        }
                        //损耗类型
                        fieldIndex = notNullableFields.IndexOf("LossType".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.LossTypeStr)) {
                            //if(fieldIndex > -1)
                            //    tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_LossTypeIsNull);
                        } else {
                            var tempEnumValue = tempExcelOperator.ImportHelper.GetEnumValue("LossType", tempImportObj.LossTypeStr);
                            if(!tempEnumValue.HasValue) {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_LossTypeValueError);
                            }
                        }

                        //配件所属分类
                        fieldIndex = notNullableFields.IndexOf("PartsAttribution".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.PartsAttributionStr)) {
                            //if(fieldIndex > -1)
                            //    tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_PartsAttributionIsNull);
                        } else {
                            var tempEnumValue = tempExcelOperator.ImportHelper.GetEnumValue("PartsAttribution", tempImportObj.PartsAttributionStr);
                            if(!tempEnumValue.HasValue) {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_PartsAttributionValueError);
                            }
                        }
                        //配件返回政策 
                        fieldIndex = notNullableFields.IndexOf("PartsReturnPolicy".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.PartsReturnPolicyStr)) {
                            //if(fieldIndex > -1)
                            //    tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_PartsReturnPolicyIsNull);
                        } else {
                            var tempEnumValue = tempExcelOperator.ImportHelper.GetEnumValue("PartsReturnPolicy", tempImportObj.PartsReturnPolicyStr);
                            if(!tempEnumValue.HasValue) {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_PartsReturnPolicyIsValueError);
                            }
                        }

                        ////配件ABC分类 
                        //fieldIndex = notNullableFields.IndexOf("PartABC".ToUpper());
                        //if(string.IsNullOrEmpty(tempImportObj.PartABCStr)) {
                        //    if(fieldIndex > -1)
                        //        tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_PartsABCIsNull);
                        //} else {
                        //    var tempEnumValue = tempExcelOperator.ImportHelper.GetEnumValue("PartABC", tempImportObj.PartABCStr.ToLower());
                        //    if(!tempEnumValue.HasValue) {
                        //        tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_PartsABCValueError);
                        //    }
                        //}
                        //库存高限StockMaximum
                        fieldIndex = notNullableFields.IndexOf("StockMaximum".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.StockMaximumStr)) {
                            //if(fieldIndex > -1)
                            //    tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_StockMaximumIsNull);
                        } else {
                            int checkValue;
                            if(int.TryParse(tempImportObj.StockMaximumStr, out checkValue)) {
                                if(checkValue <= 0) {
                                    tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_StockMaximumIsOverZeroNew);
                                }
                            } else {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_StockMaximumIsNumber);
                            }
                        }
                        //库存低限StockMinimum
                        fieldIndex = notNullableFields.IndexOf("StockMinimum".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.StockMinimumStr)) {
                            //if(fieldIndex > -1)
                            //    tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_StockMinimum);
                        } else {
                            int checkValue;
                            if(int.TryParse(tempImportObj.StockMinimumStr, out checkValue)) {
                                if(checkValue <= 0) {
                                    tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_StockMinimumIsOverZero);
                                }
                            } else {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_StockMinimumIsNumber);
                            }
                        }

                        //批量审核上限
                        if(!string.IsNullOrEmpty(tempImportObj.AutoApproveUpLimitStr)) {
                            int checkValue;
                            if(int.TryParse(tempImportObj.AutoApproveUpLimitStr, out checkValue)) {
                                if(checkValue < 0) {
                                    tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_AutoApproveUpLimiOverZero);
                                } else
                                    tempImportObj.AutoApproveUpLimit = checkValue;
                            } else {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_AutoApproveUpLimiIsNumber);
                            }
                        }
                        //else
                        //    tempImportObj.AutoApproveUpLimit = 0;


                        //配件材料费 PartsMaterialManageCost
                        fieldIndex = notNullableFields.IndexOf("PartsMaterialManageCost".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.PartsMaterialManageCostStr)) {
                            //if(fieldIndex > -1)
                            //    tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_PartsMaterialManageCostIsNull);
                        } else {
                            decimal checkValue;
                            if(decimal.TryParse(tempImportObj.PartsMaterialManageCostStr, out checkValue)) {
                                if(checkValue <= 0) {
                                    tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_PartsMaterialManageCostOverZero);
                                } else {
                                    tempImportObj.PartsMaterialManageCost = checkValue;
                                }
                            } else {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_PartsMaterialManageCostIsNumber);
                            }
                        }

                        //配件保修时长 PartsWarrantyLong
                        fieldIndex = notNullableFields.IndexOf("PartsWarrantyLong".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.PartsWarrantyLongStr)) {
                            //if(fieldIndex > -1)
                            //    tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_PartsWarrantyLongIsNull);
                        } else {
                            int checkValue;
                            if(int.TryParse(tempImportObj.PartsWarrantyLongStr, out checkValue)) {
                                if(checkValue <= 0) {
                                    tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_PartsWarrantyLongOverZero);
                                } else {
                                    tempImportObj.PartsWarrantyLong = checkValue;
                                }
                            } else {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_PartsWarrantyLongIsNumber);
                            }
                        }

                        //备注Remark
                        fieldIndex = notNullableFields.IndexOf("Remark".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.RemarkStr)) {
                            //if(fieldIndex > -1) {
                            //    tempErrorMessage.Add(ErrorStrings.ImpPartsPurchasePricingDetail_Validation15);
                            //}

                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.RemarkStr) > fieldLenght["Remark".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchasePricingDetail_Validation16);
                        }

                        #endregion

                        if(tempErrorMessage.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });
                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    #region 导入内容业务检查
                    var groups = tempRightList.GroupBy(r => new {
                        r.BranchNameStr,
                        r.PartCodeStr,
                        //r.PartsSalesCategoryNameStr
                    }).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        groupItem.ErrorMsg = ErrorStrings.ImpPartsSupplierRelation_Validation31;
                    }
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();

                    var partCodesNeedCheck = tempRightList.Select(r => r.PartCodeStr.ToUpper()).Distinct().ToArray();
                    Func<string[], bool> getDbSparePartCodes = value => {
                        var dbObj = new PartsBranchExtend {
                            PartId = Convert.ToInt32(value[0]),
                            PartCode = value[1],
                            PartName = value[2],
                            OverseasPartsFigure = value[3],
                            ReferenceCode = value[4]
                        };
                        dbSparePartCodes.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select id,Trim(Code),Trim(Name),Trim(OverseasPartsFigure),Trim(ReferenceCode) from sparePart where status=1 ", "Code", true, partCodesNeedCheck, getDbSparePartCodes);
                    foreach(var errorItem in tempRightList) {
                        var oldSparePart = dbSparePartCodes.FirstOrDefault(v => v.PartCode == errorItem.PartCodeStr);
                        if(oldSparePart == null) {
                            errorItem.ErrorMsg = String.Format(ErrorStrings.Export_Validation_PartsBranch_SparePartIsNull, errorItem.PartCodeStr);
                        } else {
                            errorItem.PartId = oldSparePart.PartId;
                            errorItem.PartName = oldSparePart.PartName;

                            //海外配件图号检查
                            if(errorItem.PartsSalesCategoryNameStr == "海外乘用车" || errorItem.PartsSalesCategoryNameStr == "海外轻卡" || errorItem.PartsSalesCategoryNameStr == "海外中重卡") {
                                if(!string.IsNullOrEmpty(oldSparePart.OverseasPartsFigure)) {
                                    if(Encoding.Default.GetByteCount(oldSparePart.OverseasPartsFigure) > 18) {
                                        errorItem.ErrorMsg = String.Format(ErrorStrings.Export_Validation_PartsBranch_OverseasPartsFigureIsLong, errorItem.PartCodeStr);
                                    } else {
                                        errorItem.PartId = oldSparePart.PartId;
                                        errorItem.PartName = oldSparePart.PartName;
                                    }
                                }

                            }
                        }
                    }
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();

                    var branchNamesNeedCheck = tempRightList.Select(r => r.BranchNameStr).Distinct().ToArray();
                    var dbBranches = new List<PartsBranchExtend>();
                    Func<string[], bool> getDbBranches = value => {
                        var dbObj = new PartsBranchExtend {
                            BranchId = Convert.ToInt32(value[0]),
                            BranchCode = value[1],
                            BranchName = value[2]
                        };
                        dbBranches.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select Id,Code,Name from Branch where status=1 ", "Name", true, branchNamesNeedCheck, getDbBranches);
                    var errorBranches = tempRightList.Where(r => dbBranches.Any(v => v.BranchName != r.BranchNameStr)).ToList();
                    foreach(var errorItem in errorBranches) {
                        errorItem.ErrorMsg = ErrorStrings.Export_Validation_PartsBranch_ErrorBranches;
                    }
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();

                    var IncreaseRateGroupCodeCheck = tempRightList.Where(r => r.IncreaseRateGroupCode != null).Select(r => r.IncreaseRateGroupCode).Distinct().ToArray();
                    var dbIncreaseRateGroupCode = new List<PartsBranchExtend>();
                    Func<string[], bool> getIncreaseRateGroupCodes = value => {
                        var dbObj = new PartsBranchExtend {
                            IncreaseRateGroupId = Convert.ToInt32(value[0]),
                            IncreaseRateGroupCode = value[1],
                            IncreaseRateGroupName = value[2]
                        };
                        dbIncreaseRateGroupCode.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select * from PartsSalePriceIncreaseRate where STATUS=1", "GroupCode", true, IncreaseRateGroupCodeCheck, getIncreaseRateGroupCodes);
                    var errorIncreaseRateGroupCodeCheck = tempRightList.Where(r => dbIncreaseRateGroupCode.Any(v => v.IncreaseRateGroupCode != r.IncreaseRateGroupCode) && r.IncreaseRateGroupCode != null).ToList();
                    foreach(var errorItem in errorIncreaseRateGroupCodeCheck) {
                        errorItem.ErrorMsg = ErrorStrings.Export_Validation_PartsBranch_IncreaseRateError;
                    }
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();

                    var branchCodes = dbBranches.Select(r => r.BranchCode).Distinct().ToArray();
                    var dbPartsSalesCategories = new List<PartsBranchExtend>();
                    Func<string[], bool> getDbPartsSalesCategories = value => {
                        var dbObj = new PartsBranchExtend {
                            PartsSalesCategoryId = Convert.ToInt32(value[0]),
                            PartsSalesCategoryName = value[1],
                            BranchName = value[2]
                        };
                        dbPartsSalesCategories.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select id,name,branchName,branchCode from PartsSalesCategory where status=1 ", "branchCode", true, branchCodes, getDbPartsSalesCategories);
                    foreach(var item in tempRightList) {
                        var oldPtsCategory = dbPartsSalesCategories.FirstOrDefault(v => v.BranchName == item.BranchNameStr);
                        if(oldPtsCategory != null) {
                            item.PartsSalesCategoryNameStr = oldPtsCategory.PartsSalesCategoryName;
                        } else {
                            item.ErrorMsg = ErrorStrings.Export_Title_PartsBranch_BranchName + item.BranchNameStr + ErrorStrings.Export_Validation_PartsBranch_PartssalescategoryName;
                        }
                    }
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();

                    var partsBranchNeedCheck = tempRightList.Select(r => r.PartCodeStr.ToUpper()).Distinct().ToArray();

                    Func<string[], bool> getDbPartsBranches = value => {
                        var dbObj = new PartsBranchExtend {
                            Id = int.Parse(value[0]),
                            PartsSalesCategoryName = value[1],
                            //BranchName = value[2],
                            PartCode = value[2],
                            //StockMaximum = string.IsNullOrEmpty(value[4].ToString()) ? 0 : Convert.ToInt32(value[4]),
                            //StockMinimum = string.IsNullOrEmpty(value[5].ToString()) ? 0 : Convert.ToInt32(value[5]),
                            //PartsMaterialManageCost = string.IsNullOrEmpty(value[6].ToString()) ? 0 : Convert.ToDecimal(value[6]),
                            //PartsWarrantyLong = string.IsNullOrEmpty(value[7].ToString()) ? 0 : Convert.ToInt32(value[7]),
                            //BranchId = string.IsNullOrEmpty(value[8].ToString()) ? 0 : Convert.ToInt32(value[8]),
                            //PartsSalesCategoryId = string.IsNullOrEmpty(value[9].ToString()) ? 0 : Convert.ToInt32(value[9]),
                            //ProductLifeCycle = string.IsNullOrEmpty(value[10].ToString()) ? 0 : Convert.ToInt32(value[10]),
                            //LossType = string.IsNullOrEmpty(value[11].ToString()) ? 0 : Convert.ToInt32(value[11]),
                            //PartsAttribution = string.IsNullOrEmpty(value[12].ToString()) ? 0 : Convert.ToInt32(value[12]),
                            //IsOrderable = Convert.ToBoolean(string.IsNullOrEmpty(value[13].ToString()) ? 0 : Convert.ToInt32(value[13])),
                            //IsSalable = Convert.ToBoolean(string.IsNullOrEmpty(value[14].ToString()) ? 0 : Convert.ToInt32(value[14])),
                            //PartsReturnPolicy = string.IsNullOrEmpty(value[15].ToString()) ? 0 : Convert.ToInt32(value[15]),
                            //IncreaseRateGroupId = string.IsNullOrEmpty(value[16].ToString()) ? 0 : Convert.ToInt32(value[16]),
                            //MinSaleQuantity = string.IsNullOrEmpty(value[17].ToString()) ? 0 : Convert.ToInt32(value[17]),
                            //PartsWarrantyCategoryId = string.IsNullOrEmpty(value[18].ToString()) ? 0 : Convert.ToInt32(value[18]),
                            //PartsWarrantyCategoryCode = value[19],
                            //PartsWarrantyCategoryName = value[20],
                            //IsDirectSupply = Convert.ToBoolean(Convert.ToInt32(value[21])),
                            //PartName = value[22],
                            //Remark = value[23],
                            //AutoApproveUpLimit = string.IsNullOrEmpty(value[24].ToString()) ? 0 : Convert.ToInt32(value[24]),
                        };
                        dbPartsBranches.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator(@"select Id,partssalescategoryName,partcode from PartsBranch where status=1 ", "PartCode", true, partsBranchNeedCheck, getDbPartsBranches);
                    var rightPartsBranches = tempRightList.Where(r => dbPartsBranches.Any(v => v.PartsSalesCategoryName == r.PartsSalesCategoryNameStr && v.PartCode == r.PartCodeStr)).ToList();
                    foreach(var rightItem in rightPartsBranches) {
                        rightItem.Id = dbPartsBranches.First(v => v.PartsSalesCategoryName == rightItem.PartsSalesCategoryNameStr && v.PartCode == rightItem.PartCodeStr).Id;
                    }
                    var errorPartsBranches = tempRightList.Where(r => !dbPartsBranches.Any(v => v.PartsSalesCategoryName == r.PartsSalesCategoryNameStr && v.PartCode == r.PartCodeStr)).ToList();
                    foreach(var errorItem in errorPartsBranches) {
                        errorItem.ErrorMsg = ErrorStrings.Export_Validation_PartsBranch_PartsSalescategoryIsNull;
                    }

                    var dbSalesCenterstrategies = new List<PartsBranchExtend>();
                    var dbSpareParts = new List<PartsBranchExtend>();
                    var partsSalesCenterstrategies = tempRightList.Select(r => r.PartsSalesCategoryNameStr).Distinct().ToArray();
                    var partsSpareParts = tempRightList.Select(r => r.PartCodeStr.ToUpper()).Distinct().ToArray();
                    Func<string[], bool> getDbsSalesCenterstrategies = value => {
                        var dbObj = new PartsBranchExtend {
                            PartsSalesCategoryId = Convert.ToInt32(value[0]),
                            PartsSalesCategoryName = value[1],
                            IsEngNameRequired = value[2]
                        };
                        dbSalesCenterstrategies.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select p.PartsSalesCategoryId,p.PartsSalesCategoryName,s.IsEngNameRequired from PartsBranch p left join SalesCenterstrategy s on p.PartsSalesCategoryId=s.PartsSalesCategoryId where p.status=1 ", "p.PartsSalescategoryName", true, partsSalesCenterstrategies, getDbsSalesCenterstrategies);
                    dbSalesCenterstrategies = dbSalesCenterstrategies.Where((x, i) => dbSalesCenterstrategies.FindIndex(z => z.PartsSalesCategoryId == x.PartsSalesCategoryId) == i).ToList();
                    foreach(var item in tempRightList) {
                        item.IsEngNameRequired = dbSalesCenterstrategies.FirstOrDefault(r => r.PartsSalesCategoryName == item.PartsSalesCategoryNameStr).IsEngNameRequired;
                    }

                    Func<string[], bool> getDbsSpareparts = value => {
                        var dbObj = new PartsBranchExtend {
                            PartCode = value[0],
                            EnglishName = value[1]
                        };
                        dbSpareParts.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select Code,EnglishName from SparePart where status=1", "Code", true, partsSpareParts, getDbsSpareparts);
                    var errorEnglishNames = tempRightList.Where(r => dbSpareParts.Where(s => s.PartCode == r.PartCodeStr && string.IsNullOrEmpty(s.EnglishName)).Any() && r.IsEngNameRequired == "1");
                    foreach(var errorItem in errorEnglishNames) {
                        errorItem.ErrorMsg = errorItem.PartNameStr + ErrorStrings.Export_Validation_PartsBranch_EnglishNameIsNull;
                    }

                    #endregion

                    //获取所有不合格数据
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    //获取所有合格数据
                    rightList = allList.Except(errorList).ToList();
                    //将合格数据的值填上
                    foreach(var rightItem in rightList) {
                        rightItem.PartsSalesCategoryName = rightItem.PartsSalesCategoryNameStr;
                        rightItem.PartCode = rightItem.PartCodeStr;
                        //if(!string.IsNullOrEmpty(rightItem.PartNameStr))
                        //    rightItem.PartName = rightItem.PartNameStr;
                        rightItem.BranchName = rightItem.BranchNameStr;
                        rightItem.ReferenceCode = rightItem.ReferenceCodeStr;
                        rightItem.IsOrderable = rightItem.IsOrderableStr == ErrorStrings.Export_Title_PartsBranch_Yes;
                        rightItem.IsSalable = rightItem.IsSalableStr == ErrorStrings.Export_Title_PartsBranch_Yes;
                        rightItem.IsDirectSupply = rightItem.IsDirectSupplyStr == ErrorStrings.Export_Title_PartsBranch_Yes;
                        rightItem.IncreaseRateGroupCode = rightItem.IncreaseRateGroupCode;
                        try {
                            rightItem.MinSaleQuantity = Convert.ToInt32(rightItem.MinSaleQuantityStr);
                        } catch {
                            rightItem.MinSaleQuantity = null;
                        }

                        rightItem.ProductLifeCycle = tempExcelOperator.ImportHelper.GetEnumValue("ProductLifeCycle", rightItem.ProductLifeCycleStr);
                        rightItem.LossType = tempExcelOperator.ImportHelper.GetEnumValue("LossType", rightItem.LossTypeStr);
                        rightItem.PartsAttribution = tempExcelOperator.ImportHelper.GetEnumValue("PartsAttribution", rightItem.PartsAttributionStr);
                        rightItem.PartsReturnPolicy = tempExcelOperator.ImportHelper.GetEnumValue("PartsReturnPolicy", rightItem.PartsReturnPolicyStr) ?? 0;
                        //rightItem.PartABC = tempExcelOperator.ImportHelper.GetEnumValue("PartABC", rightItem.PartABCStr == null ? rightItem.PartABCStr : (rightItem.PartABCStr.ToLower()) ?? 0);
                        try {
                            rightItem.StockMaximum = Convert.ToInt32(rightItem.StockMaximumStr);
                        } catch {
                            rightItem.StockMaximum = null;
                        }
                        try {
                            rightItem.StockMinimum = Convert.ToInt32(rightItem.StockMinimumStr);
                        } catch {
                            rightItem.StockMinimum = null;
                        }
                        rightItem.Remark = rightItem.RemarkStr;
                        rightItem.Status = tempExcelOperator.ImportHelper.GetEnumValue("Status", ErrorStrings.Export_Validation_PartsBranch_Used) ?? 0;
                        var tempDbSparePartCode = dbSparePartCodes.Single(r => r.PartCode == rightItem.PartCode);
                        rightItem.PartId = tempDbSparePartCode.PartId;
                        var tempDbBranch = dbBranches.FirstOrDefault(r => r.BranchName == rightItem.BranchNameStr);
                        if(tempDbBranch != null) {
                            rightItem.BranchId = tempDbBranch.BranchId;
                        } else {
                            rightItem.BranchId = null;
                        }
                        var tempdbPartsSalesCategory = dbPartsSalesCategories.FirstOrDefault(r => r.PartsSalesCategoryName == rightItem.PartsSalesCategoryNameStr);
                        if(tempdbPartsSalesCategory != null) {
                            rightItem.PartsSalesCategoryId = tempdbPartsSalesCategory.PartsSalesCategoryId;
                        }
                        var tempIncreaseRateGroup = dbIncreaseRateGroupCode.FirstOrDefault(r => r.IncreaseRateGroupCode == rightItem.IncreaseRateGroupCode);
                        if(tempIncreaseRateGroup != null) {
                            rightItem.IncreaseRateGroupId = tempIncreaseRateGroup.IncreaseRateGroupId;
                            rightItem.IncreaseRateGroupName = tempIncreaseRateGroup.IncreaseRateGroupName;
                        } else {
                            rightItem.IncreaseRateGroupId = default(int);
                            rightItem.IncreaseRateGroupName = null;
                        }
                    }
                }

                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempObj = list[index - 1];
                            var values = new object[] {
                                tempObj.PartsSalesCategoryNameStr, tempObj.PartCodeStr, tempObj.BranchNameStr, tempObj.IsOrderableStr, tempObj.IsSalableStr, tempObj.IsDirectSupplyStr, tempObj.MinSaleQuantityStr, tempObj.ProductLifeCycleStr, tempObj.LossTypeStr, tempObj.PartsAttributionStr, tempObj.PartsReturnPolicyStr, tempObj.StockMaximumStr, tempObj.StockMinimumStr, tempObj.PartsMaterialManageCostStr, tempObj.PartsWarrantyLongStr, tempObj.IncreaseRateGroupCode, tempObj.IncreaseRateGroupName, tempObj.AutoApproveUpLimitStr, tempObj.RemarkStr, tempObj.ErrorMsg
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }

                //更新导入所有合格数据
                if(!rightList.Any())
                    return true;
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    //开启事务，新增更新配件在一个事务内
                    var ts = conn.BeginTransaction();
                    try {
                        //新增配件
                        if(rightList.Any()) {
                            var userInfo = Utils.GetCurrentUserInfo();
                            //往数据库增加配件信息
                            foreach(var item in rightList) {
                                List<string> fields = new List<string>();
                                fields.Add("PartsSalesCategoryId");
                                fields.Add("PartsSalesCategoryName");
                                fields.Add("PartId");
                                fields.Add("PartCode");
                                fields.Add("ModifyTime");
                                fields.Add("ModifierId");
                                fields.Add("ModifierName");
                                fields.Add("OMSparePartMark");
                                string sqlUpdate = null;
                                var command = db.CreateDbCommand(sqlUpdate, conn, ts);

                                //if(!string.IsNullOrEmpty(item.PartName)) {
                                //    fields.Add("PartName");
                                //    command.Parameters.Add(db.CreateDbParameter("PartName", item.PartName));
                                //}
                                if(item.StockMaximum.HasValue && item.StockMaximum != 0) {
                                    fields.Add("StockMaximum");
                                    command.Parameters.Add(db.CreateDbParameter("StockMaximum", item.StockMaximum));
                                }
                                if(item.StockMinimum.HasValue && item.StockMinimum != 0) {
                                    fields.Add("StockMinimum");
                                    command.Parameters.Add(db.CreateDbParameter("StockMinimum", item.StockMinimum));
                                }
                                if(item.PartsMaterialManageCost.HasValue) {
                                    fields.Add("PartsMaterialManageCost");
                                    command.Parameters.Add(db.CreateDbParameter("PartsMaterialManageCost", item.PartsMaterialManageCost));
                                }
                                if(item.PartsWarrantyLong.HasValue) {
                                    fields.Add("PartsWarrantyLong");
                                    command.Parameters.Add(db.CreateDbParameter("PartsWarrantyLong", item.PartsWarrantyLong));
                                }
                                if(item.ProductLifeCycle.HasValue) {
                                    fields.Add("ProductLifeCycle");
                                    command.Parameters.Add(db.CreateDbParameter("ProductLifeCycle", item.ProductLifeCycle));
                                }
                                if(item.LossType.HasValue) {
                                    fields.Add("LossType");
                                    command.Parameters.Add(db.CreateDbParameter("LossType", item.LossType));
                                }
                                if(!string.IsNullOrEmpty(item.IsOrderableStr)) {
                                    fields.Add("IsOrderable");
                                    item.IsOrderable = item.IsOrderableStr == ErrorStrings.Export_Title_PartsBranch_Yes;
                                    command.Parameters.Add(db.CreateDbParameter("IsOrderable", item.IsOrderable));
                                }
                                if(!string.IsNullOrEmpty(item.IsSalableStr)) {
                                    fields.Add("IsSalable");
                                    item.IsSalable = item.IsSalableStr == ErrorStrings.Export_Title_PartsBranch_Yes;
                                    command.Parameters.Add(db.CreateDbParameter("IsSalable", item.IsSalable));
                                }
                                if(item.PartsReturnPolicy != 0) {
                                    fields.Add("PartsReturnPolicy");
                                    command.Parameters.Add(db.CreateDbParameter("PartsReturnPolicy", item.PartsReturnPolicy));
                                }
                                if(item.PartsAttribution.HasValue) {
                                    fields.Add("PartsAttribution");
                                    command.Parameters.Add(db.CreateDbParameter("PartsAttribution", item.PartsAttribution));
                                }
                                if(item.IncreaseRateGroupId.HasValue && item.IncreaseRateGroupId != 0) {
                                    fields.Add("INCREASERATEGROUPID");
                                    command.Parameters.Add(db.CreateDbParameter("INCREASERATEGROUPID", item.IncreaseRateGroupId));
                                }

                                if(!string.IsNullOrEmpty(item.IsDirectSupplyStr)) {
                                    fields.Add("IsDirectSupply");
                                    item.IsDirectSupply = item.IsDirectSupplyStr == ErrorStrings.Export_Title_PartsBranch_Yes;
                                    command.Parameters.Add(db.CreateDbParameter("IsDirectSupply", item.IsDirectSupply));
                                }
                                if(item.MinSaleQuantity != 0 && item.MinSaleQuantity.HasValue) {
                                    fields.Add("MinSaleQuantity");
                                    command.Parameters.Add(db.CreateDbParameter("MinSaleQuantity", item.MinSaleQuantity));
                                }
                                if(item.PartsWarrantyCategoryId.HasValue) {
                                    fields.Add("PartsWarrantyCategoryId");
                                    command.Parameters.Add(db.CreateDbParameter("PartsWarrantyCategoryId", item.PartsWarrantyCategoryId));
                                }

                                if(!string.IsNullOrEmpty(item.PartsWarrantyCategoryCode)) {
                                    fields.Add("PartsWarrantyCategoryCode");
                                    command.Parameters.Add(db.CreateDbParameter("PartsWarrantyCategoryCode", item.PartsWarrantyCategoryCode));
                                }
                                if(item.Status != 0) {
                                    fields.Add("Status");
                                    command.Parameters.Add(db.CreateDbParameter("Status", item.Status));
                                }
                                if(!string.IsNullOrEmpty(item.Remark)) {
                                    fields.Add("Remark");
                                    command.Parameters.Add(db.CreateDbParameter("Remark", item.Remark));
                                }
                                if(item.AutoApproveUpLimit.HasValue) {
                                    fields.Add("AutoApproveUpLimit");
                                    command.Parameters.Add(db.CreateDbParameter("AutoApproveUpLimit", item.AutoApproveUpLimit));
                                }
                                sqlUpdate = db.GetUpdateSql("PartsBranch", fields.ToArray(), new[] {
                                    "Id"
                                });
                                command.Parameters.Add(db.CreateDbParameter("Id", item.Id));
                                command.Parameters.Add(db.CreateDbParameter("PartsSalesCategoryId", item.PartsSalesCategoryId));
                                command.Parameters.Add(db.CreateDbParameter("PartsSalesCategoryName", item.PartsSalesCategoryName));
                                command.Parameters.Add(db.CreateDbParameter("PartId", item.PartId));
                                command.Parameters.Add(db.CreateDbParameter("PartCode", item.PartCode));
                                command.Parameters.Add(db.CreateDbParameter("ModifierId", userInfo.Id));
                                command.Parameters.Add(db.CreateDbParameter("ModifierName", userInfo.Name));
                                command.Parameters.Add(db.CreateDbParameter("ModifyTime", DateTime.Now));
                                command.Parameters.Add(db.CreateDbParameter("OMSparePartMark", 0));
                                command.CommandText = sqlUpdate;
                                command.ExecuteNonQuery();
                            }
                            InsertPartsBranchHistoryByPartsIdOrList(db, rightList.Select(r => r.Id.ToString()).ToArray(), null, conn, ts);
                        }
                        //无异常提交
                        ts.Commit();
                    } catch(Exception) {
                        //报错回滚
                        ts.Rollback();
                        throw;
                    } finally {
                        if(conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }

        /// <summary>
        /// 批量替换可否采购
        /// </summary>
        public bool 批量替换可否采购(string fileName, out int excelImportNum, out List<PartsBranchExtend> rightData, out List<PartsBranchExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<PartsBranchExtend>();
            var rightList = new List<PartsBranchExtend>();
            var allList = new List<PartsBranchExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("PartsBranch", out notNullableFields, out fieldLenght);
                List<object> excelColumns;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Code, "PartCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Partssalescategory_Name, "PartsSalesCategoryName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_IsOrderableNew, "IsOrderable");
                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;

                    ////单次最大导入条数不能大于500！
                    //if(excelOperator.LastRowNum > 500) {
                    //    throw new Exception("单次最大导入条数不能大于500！");
                    //}

                    var keyValuePairs = new[] {
                        new KeyValuePair<string, string>("Status", "BaseData_Status")
                    };
                    tempExcelOperator.ImportHelper.LoadEnumFromDb(null, null, keyValuePairs);
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var tempImportObj = new PartsBranchExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);

                        #region
                        tempImportObj.PartsSalesCategoryNameStr = newRow["PartsSalesCategoryName"];
                        tempImportObj.PartsSalesCategoryName = newRow["PartsSalesCategoryName"];
                        tempImportObj.PartCodeStr = newRow["PartCode"];
                        tempImportObj.PartCode = newRow["PartCode"];
                        tempImportObj.IsOrderableStr = newRow["IsOrderable"];

                        #endregion

                        var tempErrorMessage = new List<string>();

                        #region 导入的内容基本检查

                        //品牌检查
                        var fieldIndex = notNullableFields.IndexOf("PartsSalesCategoryName".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.PartsSalesCategoryNameStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_PartsSalescategoryNameIsNull);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.PartsSalesCategoryNameStr) > fieldLenght["PartsSalesCategoryName".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_PartsSalescategoryNameIsLong);
                        }
                        //配件编号
                        fieldIndex = notNullableFields.IndexOf("PartCode".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.PartCodeStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.OverstockPartsAppDetail__Validation1);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.PartCodeStr) > fieldLenght["PartCode".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.ImpPartsSupplierRelation_Validation8);
                        }
                        //是否可采购
                        fieldIndex = notNullableFields.IndexOf("IsOrderable".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.IsOrderableStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_IsOrderableIsNull);
                        } else {
                            if(tempImportObj.IsOrderableStr == ErrorStrings.Export_Title_PartsBranch_Yes) {
                                tempImportObj.IsOrderable = true;
                            } else if(tempImportObj.IsOrderableStr == ErrorStrings.Export_Title_PartsBranch_No) {
                                tempImportObj.IsOrderable = false;
                            } else {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_IsOrderableValueError);
                            }
                        }

                        #endregion

                        if(tempErrorMessage.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });
                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    #region 导入内容业务检查
                    var groups = tempRightList.GroupBy(r => new {
                        r.PartCodeStr,
                        r.PartsSalesCategoryNameStr
                    }).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        groupItem.ErrorMsg = ErrorStrings.Export_Validation_PartsBranch_PartsSalescategoryReport;
                    }
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();

                    var partCodesNeedCheck = tempRightList.Select(r => r.PartCodeStr.ToUpper()).Distinct().ToArray();
                    var dbSparePartCodes = new List<PartsBranchExtend>();
                    Func<string[], bool> getDbSparePartCodes = value => {
                        var dbObj = new PartsBranchExtend {
                            PartId = Convert.ToInt32(value[0]),
                            PartCode = value[1],
                            PartName = value[2]
                        };
                        dbSparePartCodes.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator(@"select id,Trim(Code),Trim(Name) from sparePart where status=1 ", "Code", true, partCodesNeedCheck, getDbSparePartCodes);
                    foreach(var errorItem in tempRightList) {
                        var oldSparePart = dbSparePartCodes.FirstOrDefault(v => v.PartCode == errorItem.PartCodeStr);
                        if(oldSparePart == null) {
                            errorItem.ErrorMsg = String.Format(ErrorStrings.Export_Validation_PartsBranch_SparePartIsNull, errorItem.PartCodeStr);
                        } else {
                            errorItem.PartId = oldSparePart.PartId;
                            errorItem.PartName = oldSparePart.PartName;
                        }
                    }
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();

                    var partsSalesCategoryNeedCheck = tempRightList.Select(r => r.PartsSalesCategoryNameStr).Distinct().ToArray();
                    var dbPartsSalesCategories = new List<PartsBranchExtend>();
                    Func<string[], bool> getDbPartsSalesCategories = value => {
                        var dbObj = new PartsBranchExtend {
                            PartsSalesCategoryId = Convert.ToInt32(value[0]),
                            PartsSalesCategoryName = value[1],
                            BranchName = value[2],
                            BranchId = Convert.ToInt32(value[3])
                        };
                        dbPartsSalesCategories.Add(dbObj);
                        return false;
                    };
                    var userInfo = Utils.GetCurrentUserInfo();
                    db.QueryDataWithInOperator("select id,name,branchName,branchId from PartsSalesCategory where status=1 and BranchId = " + userInfo.EnterpriseId, "Name", true, partsSalesCategoryNeedCheck, getDbPartsSalesCategories);
                    var errorPartsSalesCategories = tempRightList.Where(r => !dbPartsSalesCategories.Any(v => v.PartsSalesCategoryName == r.PartsSalesCategoryName && v.BranchId == userInfo.EnterpriseId)).ToList();
                    foreach(var errorItem in errorPartsSalesCategories) {
                        errorItem.ErrorMsg = ErrorStrings.Export_Title_PartsBranch_BranchName + errorItem.BranchNameStr + ErrorStrings.Export_Validation_PartsBranch_Validation1 + errorItem.PartsSalesCategoryNameStr + ErrorStrings.Export_Validation_PartsBranch_Validation2;
                    }
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();

                    var partsBranchNeedCheck = tempRightList.Select(r => r.PartCodeStr.ToUpper()).Distinct().ToArray();
                    var dbPartsBranches = new List<PartsBranchExtend>();
                    Func<string[], bool> getDbPartsBranches = value => {
                        var dbObj = new PartsBranchExtend {
                            Id = int.Parse(value[0]),
                            PartsSalesCategoryName = value[1],
                            BranchName = value[2],
                            BranchId = int.Parse(value[3]),
                            PartCode = value[4],
                            IsOrderable = Convert.ToBoolean(int.Parse(value[5]))
                        };
                        dbPartsBranches.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator(@"select Id, partssalescategoryName, branchName,branchId, partcode,IsOrderable from partsbranch  where status = 1 ", "PartCode", true, partsBranchNeedCheck, getDbPartsBranches);

                    var dbPartsPurchasePlanDetails = new List<PartsPurchasePlanDetailExtend>();
                    Func<string[], bool> getDbPartsPurchasePlanDetails = value => {
                        var dbObj = new PartsPurchasePlanDetailExtend {
                            Id = int.Parse(value[0]),
                            SparePartCode = value[1]
                        };
                        dbPartsPurchasePlanDetails.Add(dbObj);
                        return false;
                    };

                    db.QueryDataWithInOperator(@"select id,sparepartcode from PartsPurchasePlanDetail a where exists ( select 1 from partspurchaseplan b where b.id = a.purchaseplanid and b.status <> 0 and b.status <> 4) ", "SparePartCode", true, partsBranchNeedCheck, getDbPartsPurchasePlanDetails);
                   
                    foreach(var Item in tempRightList) {
                        var dbitem = dbPartsBranches.Where(v => v.PartsSalesCategoryName == Item.PartsSalesCategoryNameStr && v.BranchId == userInfo.EnterpriseId && v.PartCode == Item.PartCodeStr).FirstOrDefault();
                        if(dbitem == null) {
                            Item.ErrorMsg = ErrorStrings.Export_Validation_PartsBranch_PartsSalescategoryIsNull;
                        } else {
                            Item.Id = dbitem.Id;
                            if (!dbitem.IsOrderable && Item.IsOrderable) {
                                Item.ErrorMsg = ErrorStrings.Export_Validation_PartsBranch_IsSalableError;
                            }
                            if (dbitem.IsOrderable && !Item.IsOrderable) {
                                var dbPurchasePlan = dbPartsPurchasePlanDetails.Any(r => r.SparePartCode == Item.PartCode);
                                if(dbPurchasePlan){
                                    Item.ErrorMsg = ErrorStrings.Export_Validation_PartsBranch_HasPurchasePlan;
                                }
                            }
                        }
                    }
                    #endregion

                    //获取所有不合格数据
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    //获取所有合格数据
                    rightList = allList.Except(errorList).ToList();
                }


                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempObj = list[index - 1];
                            var values = new object[] {
                                tempObj.PartCodeStr, tempObj.PartsSalesCategoryNameStr, tempObj.IsOrderableStr, tempObj.ErrorMsg
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }
                rightList = rightList.Where(r => r.IsOrderable == false).ToList();
                //更新导入所有合格数据
                if(!rightList.Any())
                    return true;
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    //开启事务，新增更新配件在一个事务内
                    var ts = conn.BeginTransaction();
                    try {
                        //修改配件
                        if(rightList.Any()) {
                            //获取修改数据的sql语句，Id为主键
                            var sqlUpdate = db.GetUpdateSql("PartsBranch", new[] {
                                "IsOrderable", "ModifierId", "ModifierName", "ModifyTime","OMSparePartMark"
                            }, new[] {
                                "Id"
                            });
                            var userInfo = Utils.GetCurrentUserInfo();
                            //往数据库修改配件信息
                            foreach(var item in rightList) {
                                var command = db.CreateDbCommand(sqlUpdate, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("IsOrderable", item.IsOrderable));
                                command.Parameters.Add(db.CreateDbParameter("ModifierId", userInfo.Id));
                                command.Parameters.Add(db.CreateDbParameter("ModifierName", userInfo.Name));
                                command.Parameters.Add(db.CreateDbParameter("ModifyTime", DateTime.Now));
                                command.Parameters.Add(db.CreateDbParameter("Id", item.Id));
                                command.Parameters.Add(db.CreateDbParameter("OMSparePartMark", 0));
                                command.ExecuteNonQuery();
                            }
                            //增加履历
                            InsertPartsBranchHistoryByPartsIdOrList(db, rightList.Select(r => r.Id.ToString()).Distinct().ToArray(), null, conn, ts);
                        }
                        //无异常提交
                        ts.Commit();
                    } catch(Exception) {
                        //报错回滚
                        ts.Rollback();
                        throw;
                    } finally {
                        if(conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }

        /// <summary>
        /// 批量替换可否销售及直供
        /// </summary>
        public bool 批量替换可否销售及直供(string fileName, out int excelImportNum, out List<PartsBranchExtend> rightData, out List<PartsBranchExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<PartsBranchExtend>();
            var rightList = new List<PartsBranchExtend>();
            var allList = new List<PartsBranchExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("PartsBranch", out notNullableFields, out fieldLenght);
                List<object> excelColumns;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Code, "PartCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Partssalescategory_Name, "PartsSalesCategoryName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_IsSalableNew, "IsSalable");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_IsDirectSupplyNew, "IsDirectSupply");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_AutoApproveUpLimit, "AutoApproveUpLimit");
                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;

                    ////单次最大导入条数不能大于500！
                    //if(excelOperator.LastRowNum > 500) {
                    //    throw new Exception("单次最大导入条数不能大于500！");
                    //}

                    var keyValuePairs = new[] {
                        new KeyValuePair<string, string>("Status", "BaseData_Status")
                    };
                    tempExcelOperator.ImportHelper.LoadEnumFromDb(null, null, keyValuePairs);
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var tempImportObj = new PartsBranchExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);

                        #region
                        tempImportObj.PartsSalesCategoryNameStr = newRow["PartsSalesCategoryName"];
                        tempImportObj.PartsSalesCategoryName = newRow["PartsSalesCategoryName"];
                        tempImportObj.PartCodeStr = newRow["PartCode"];
                        tempImportObj.PartCode = newRow["PartCode"];
                        tempImportObj.IsSalableStr = newRow["IsSalable"];
                        tempImportObj.IsDirectSupplyStr = newRow["IsDirectSupply"];
                        tempImportObj.AutoApproveUpLimitStr = newRow["AutoApproveUpLimit"];
                        #endregion

                        var tempErrorMessage = new List<string>();

                        #region 导入的内容基本检查

                        //品牌检查
                        var fieldIndex = notNullableFields.IndexOf("PartsSalesCategoryName".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.PartsSalesCategoryNameStr)) {
                            //if(fieldIndex > -1)
                            //    tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_PartsSalescategoryNameIsNull);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.PartsSalesCategoryNameStr) > fieldLenght["PartsSalesCategoryName".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_PartsSalescategoryNameIsLong);
                        }
                        //配件编号
                        fieldIndex = notNullableFields.IndexOf("PartCode".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.PartCodeStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.OverstockPartsAppDetail__Validation1);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.PartCodeStr) > fieldLenght["PartCode".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.ImpPartsSupplierRelation_Validation8);
                        }
                        //是否可直供
                        fieldIndex = notNullableFields.IndexOf("IsDirectSupply".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.IsDirectSupplyStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_IsDirectSupplyIsNull);
                        } else {
                            if(tempImportObj.IsDirectSupplyStr == ErrorStrings.Export_Title_PartsBranch_Yes) {
                                tempImportObj.IsDirectSupply = true;
                            } else if(tempImportObj.IsDirectSupplyStr == ErrorStrings.Export_Title_PartsBranch_No) {
                                tempImportObj.IsDirectSupply = false;
                            } else {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_IsDirectSupplyValueError);
                            }
                        }

                        //是否可销售
                        fieldIndex = notNullableFields.IndexOf("IsSalable".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.IsSalableStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_IsSalableIsNull);
                        } else {
                            if(tempImportObj.IsSalableStr == ErrorStrings.Export_Title_PartsBranch_Yes) {
                                tempImportObj.IsSalable = true;
                            } else if(tempImportObj.IsSalableStr == ErrorStrings.Export_Title_PartsBranch_No) {
                                tempImportObj.IsSalable = false;
                            } else {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_IsSalableValueError);
                            }
                        }

                        //批量审核上限
                        if(!string.IsNullOrEmpty(tempImportObj.AutoApproveUpLimitStr)) {
                            int checkValue;
                            if(int.TryParse(tempImportObj.AutoApproveUpLimitStr, out checkValue)) {
                                if(checkValue < 0) {
                                    tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_AutoApproveUpLimiOverZero);
                                } else
                                    tempImportObj.AutoApproveUpLimit = checkValue;
                            } else {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_AutoApproveUpLimiIsNumber);
                            }
                        }
                        //else {
                        //    tempErrorMessage.Add("批量审核上限不能为空");
                        //}

                        #endregion

                        if(tempErrorMessage.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });
                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    #region 导入内容业务检查
                    var groups = tempRightList.GroupBy(r => new {
                        r.PartCodeStr,
                        r.PartsSalesCategoryNameStr
                    }).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        groupItem.ErrorMsg = ErrorStrings.Export_Validation_PartsBranch_PartsSalescategoryReport;
                    }
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();

                    var partCodesNeedCheck = tempRightList.Select(r => r.PartCodeStr.ToUpper()).Distinct().ToArray();
                    var dbSparePartCodes = new List<PartsBranchExtend>();
                    Func<string[], bool> getDbSparePartCodes = value => {
                        var dbObj = new PartsBranchExtend {
                            PartId = Convert.ToInt32(value[0]),
                            PartCode = value[1],
                            PartName = value[2]
                        };
                        dbSparePartCodes.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator(@"select id,Trim(Code),Trim(Name) from sparePart where status=1 ", "Code", true, partCodesNeedCheck, getDbSparePartCodes);
                    foreach(var errorItem in tempRightList) {
                        var oldSparePart = dbSparePartCodes.FirstOrDefault(v => v.PartCode == errorItem.PartCodeStr);
                        if(oldSparePart == null) {
                            errorItem.ErrorMsg = String.Format(ErrorStrings.Export_Validation_PartsBranch_SparePartIsNull, errorItem.PartCodeStr);
                        } else {
                            errorItem.PartId = oldSparePart.PartId;
                            errorItem.PartName = oldSparePart.PartName;
                        }
                    }
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();

                    var partsSalesCategoryNeedCheck = tempRightList.Select(r => r.PartsSalesCategoryNameStr).Distinct().ToArray();
                    var dbPartsSalesCategories = new List<PartsBranchExtend>();
                    Func<string[], bool> getDbPartsSalesCategories = value => {
                        var dbObj = new PartsBranchExtend {
                            PartsSalesCategoryId = Convert.ToInt32(value[0]),
                            PartsSalesCategoryName = value[1],
                            BranchName = value[2],
                            BranchId = Convert.ToInt32(value[3])
                        };
                        dbPartsSalesCategories.Add(dbObj);
                        return false;
                    };
                    var userInfo = Utils.GetCurrentUserInfo();
                    db.QueryDataWithInOperator("select id,name,branchName,branchId from PartsSalesCategory where status=1 and BranchId = " + userInfo.EnterpriseId, "Name", true, partsSalesCategoryNeedCheck, getDbPartsSalesCategories);
                    var errorPartsSalesCategories = tempRightList.Where(r => !dbPartsSalesCategories.Any(v => v.PartsSalesCategoryName == r.PartsSalesCategoryName && v.BranchId == userInfo.EnterpriseId)).ToList();
                    foreach(var errorItem in errorPartsSalesCategories) {
                        if(!string.IsNullOrEmpty(errorItem.PartsSalesCategoryNameStr))
                        errorItem.ErrorMsg = ErrorStrings.Export_Title_PartsBranch_BranchName + errorItem.BranchNameStr + ErrorStrings.Export_Validation_PartsBranch_Validation1 + errorItem.PartsSalesCategoryNameStr + ErrorStrings.Export_Validation_PartsBranch_Validation2;
                    }
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();

                    var partsBranchNeedCheck = tempRightList.Select(r => r.PartCodeStr.ToUpper()).Distinct().ToArray();
                    var dbPartsBranches = new List<PartsBranchExtend>();
                    Func<string[], bool> getDbPartsBranches = value => {
                        var dbObj = new PartsBranchExtend {
                            Id = int.Parse(value[0]),
                            PartsSalesCategoryName = value[1],
                            BranchName = value[2],
                            BranchId = int.Parse(value[3]),
                            PartCode = value[4]
                        };
                        dbPartsBranches.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator(@"select Id, partssalescategoryName, branchName,branchId, partcode from partsbranch  where status = 1 ", "PartCode", true, partsBranchNeedCheck, getDbPartsBranches);

                    //var errorPartsBranches = tempRightList.Where(r => !dbPartsBranches.Any(v => v.PartsSalesCategoryName == r.PartsSalesCategoryNameStr && v.BranchId == userInfo.EnterpriseId && v.PartCode == r.PartCodeStr)).ToList();
                    foreach(var Item in tempRightList) {
                        PartsBranchExtend dbitem;
                        if (!string.IsNullOrEmpty(Item.PartsSalesCategoryNameStr)) {
                            dbitem = dbPartsBranches.Where(v => v.PartsSalesCategoryName == Item.PartsSalesCategoryNameStr && v.BranchId == userInfo.EnterpriseId && v.PartCode == Item.PartCodeStr).FirstOrDefault();
                        } else { 
                            dbitem = dbPartsBranches.Where(v =>  v.BranchId == userInfo.EnterpriseId && v.PartCode == Item.PartCodeStr).FirstOrDefault();
                        }
                        if(dbitem == null) {
                            if(!string.IsNullOrEmpty(Item.PartsSalesCategoryNameStr))
                            Item.ErrorMsg = ErrorStrings.Export_Validation_PartsBranch_PartsSalescategoryIsNull;
                        } else {
                            Item.Id = dbitem.Id;
                        }
                    }
                    #endregion

                    //获取所有不合格数据
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    //获取所有合格数据
                    rightList = allList.Except(errorList).ToList();
                }

                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempObj = list[index - 1];
                            var values = new object[] {
                                tempObj.PartCodeStr, tempObj.PartsSalesCategoryNameStr, tempObj.IsSalableStr, tempObj.IsDirectSupplyStr, tempObj.AutoApproveUpLimitStr, tempObj.ErrorMsg
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }

                //更新导入所有合格数据
                if(!rightList.Any())
                    return true;
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    //开启事务，新增更新配件在一个事务内
                    var ts = conn.BeginTransaction();
                    try {
                        //修改配件
                        if(rightList.Any()) {
                            //获取修改数据的sql语句，Id为主键
                            var sqlUpdate = db.GetUpdateSql("PartsBranch", new[] {
                                "IsSalable", "IsDirectSupply", "ModifierId", "ModifierName", "ModifyTime", "AutoApproveUpLimit","OMSparePartMark"
                            }, new[] {
                                "Id"
                            });
                            var userInfo = Utils.GetCurrentUserInfo();
                            //往数据库修改配件信息
                            foreach(var item in rightList) {
                                var command = db.CreateDbCommand(sqlUpdate, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("IsDirectSupply", item.IsDirectSupply));
                                command.Parameters.Add(db.CreateDbParameter("AutoApproveUpLimit", item.AutoApproveUpLimit));
                                command.Parameters.Add(db.CreateDbParameter("IsSalable", item.IsSalable));
                                command.Parameters.Add(db.CreateDbParameter("ModifierId", userInfo.Id));
                                command.Parameters.Add(db.CreateDbParameter("ModifierName", userInfo.Name));
                                command.Parameters.Add(db.CreateDbParameter("ModifyTime", DateTime.Now));
                                command.Parameters.Add(db.CreateDbParameter("Id", item.Id));
                                command.Parameters.Add(db.CreateDbParameter("OMSparePartMark", 0));
                                command.ExecuteNonQuery();
                            }
                            //增加履历
                            InsertPartsBranchHistoryByPartsIdOrList(db, rightList.Select(r => r.Id.ToString()).Distinct().ToArray(), null, conn, ts);
                        }
                        //无异常提交
                        ts.Commit();
                    } catch(Exception) {
                        //报错回滚
                        ts.Rollback();
                        throw;
                    } finally {
                        if(conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }

        /// <summary>
        /// 批量替换加价率分组
        /// </summary>
        public bool 批量替换加价率分组(string fileName, out int excelImportNum, out List<PartsBranchExtend> rightData, out List<PartsBranchExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<PartsBranchExtend>();
            var rightList = new List<PartsBranchExtend>();
            var allList = new List<PartsBranchExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("PartsBranch", out notNullableFields, out fieldLenght);
                List<object> excelColumns;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Code, "PartCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Partssalescategory_Name, "PartsSalesCategoryName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_IncreaseRateGroupCode, "IncreaseRateGroupCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_PriceType, "IncreaseRateGroupName");
                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;

                    ////单次最大导入条数不能大于500！
                    //if(excelOperator.LastRowNum > 500) {
                    //    throw new Exception("单次最大导入条数不能大于500！");
                    //}

                    var keyValuePairs = new[] {
                        new KeyValuePair<string, string>("Status", "BaseData_Status")
                    };
                    tempExcelOperator.ImportHelper.LoadEnumFromDb(null, null, keyValuePairs);
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var tempImportObj = new PartsBranchExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);

                        #region
                        tempImportObj.PartsSalesCategoryNameStr = newRow["PartsSalesCategoryName"];
                        tempImportObj.PartsSalesCategoryName = newRow["PartsSalesCategoryName"];
                        tempImportObj.PartCodeStr = newRow["PartCode"];
                        tempImportObj.PartCode = newRow["PartCode"];
                        tempImportObj.IncreaseRateGroupCode = newRow["IncreaseRateGroupCode"];
                        tempImportObj.IncreaseRateGroupName = newRow["IncreaseRateGroupName"];

                        #endregion

                        var tempErrorMessage = new List<string>();

                        #region 导入的内容基本检查

                        //品牌检查
                        var fieldIndex = notNullableFields.IndexOf("PartsSalesCategoryName".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.PartsSalesCategoryNameStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_PartsSalescategoryNameIsNull);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.PartsSalesCategoryNameStr) > fieldLenght["PartsSalesCategoryName".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_PartsSalescategoryNameIsLong);
                        }
                        //配件编号
                        fieldIndex = notNullableFields.IndexOf("PartCode".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.PartCodeStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.OverstockPartsAppDetail__Validation1);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.PartCodeStr) > fieldLenght["PartCode".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.ImpPartsSupplierRelation_Validation8);
                        }

                        //加价比率编号
                        //if(string.IsNullOrEmpty(tempImportObj.IncreaseRateGroupCode)) {
                        //    tempErrorMessage.Add("加价比率编号不能为空");
                        //}

                        #endregion

                        if(tempErrorMessage.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });
                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    #region 导入内容业务检查
                    var groups = tempRightList.GroupBy(r => new {
                        r.PartCodeStr,
                        r.PartsSalesCategoryNameStr
                    }).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        groupItem.ErrorMsg = ErrorStrings.Export_Validation_PartsBranch_PartsSalescategoryReport;
                    }
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();

                    var partCodesNeedCheck = tempRightList.Select(r => r.PartCodeStr.ToUpper()).Distinct().ToArray();
                    var dbSparePartCodes = new List<PartsBranchExtend>();
                    Func<string[], bool> getDbSparePartCodes = value => {
                        var dbObj = new PartsBranchExtend {
                            PartId = Convert.ToInt32(value[0]),
                            PartCode = value[1],
                            PartName = value[2]
                        };
                        dbSparePartCodes.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator(@"select id,Trim(Code),Trim(Name) from sparePart where status=1 ", "Code", true, partCodesNeedCheck, getDbSparePartCodes);
                    foreach(var errorItem in tempRightList) {
                        var oldSparePart = dbSparePartCodes.FirstOrDefault(v => v.PartCode == errorItem.PartCodeStr);
                        if(oldSparePart == null) {
                            errorItem.ErrorMsg = String.Format(ErrorStrings.Export_Validation_PartsBranch_SparePartIsNull, errorItem.PartCodeStr);
                        } else {
                            errorItem.PartId = oldSparePart.PartId;
                            errorItem.PartName = oldSparePart.PartName;
                        }
                    }
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();

                    var increaseCodesNeedCheck = tempRightList.Where(r => r.IncreaseRateGroupCode != null).Select(r => r.IncreaseRateGroupCode.ToUpper()).Distinct().ToArray();
                    var dbIncreaseCodes = new List<PartsBranchExtend>();
                    Func<string[], bool> getDbPartsSalePriceIncreaseRateCodes = value => {
                        var dbObj = new PartsBranchExtend {
                            IncreaseRateGroupId = Convert.ToInt32(value[0]),
                            IncreaseRateGroupCode = value[1],
                            IncreaseRateGroupName = value[2]
                        };
                        dbIncreaseCodes.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator(@" select Id,GroupCode,GroupName,IncreaseRate from PartsSalePriceIncreaseRate where status = 1 ", "GroupCode", true, increaseCodesNeedCheck, getDbPartsSalePriceIncreaseRateCodes);
                    foreach(var errorItem in tempRightList) {
                        if(errorItem.IncreaseRateGroupCode != null) {
                            var oldIncreaseGroup = dbIncreaseCodes.FirstOrDefault(v => v.IncreaseRateGroupCode == errorItem.IncreaseRateGroupCode);
                            if(oldIncreaseGroup == null) {
                                errorItem.ErrorMsg = String.Format(ErrorStrings.Export_Validation_PartsBranch_IncreaseRateGroupCodeNotExist, errorItem.IncreaseRateGroupCode);
                            } else {
                                errorItem.IncreaseRateGroupId = oldIncreaseGroup.IncreaseRateGroupId;
                                errorItem.IncreaseRateGroupCode = oldIncreaseGroup.IncreaseRateGroupCode;
                                errorItem.IncreaseRateGroupName = oldIncreaseGroup.IncreaseRateGroupName;
                            }
                        }
                    }
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();

                    var partsSalesCategoryNeedCheck = tempRightList.Select(r => r.PartsSalesCategoryNameStr).Distinct().ToArray();
                    var dbPartsSalesCategories = new List<PartsBranchExtend>();
                    Func<string[], bool> getDbPartsSalesCategories = value => {
                        var dbObj = new PartsBranchExtend {
                            PartsSalesCategoryId = Convert.ToInt32(value[0]),
                            PartsSalesCategoryName = value[1],
                            BranchName = value[2],
                            BranchId = Convert.ToInt32(value[3])
                        };
                        dbPartsSalesCategories.Add(dbObj);
                        return false;
                    };
                    var userInfo = Utils.GetCurrentUserInfo();
                    db.QueryDataWithInOperator("select id,name,branchName,branchId from PartsSalesCategory where status=1 and BranchId = " + userInfo.EnterpriseId, "Name", true, partsSalesCategoryNeedCheck, getDbPartsSalesCategories);
                    var errorPartsSalesCategories = tempRightList.Where(r => !dbPartsSalesCategories.Any(v => v.PartsSalesCategoryName == r.PartsSalesCategoryName && v.BranchId == userInfo.EnterpriseId)).ToList();
                    foreach(var errorItem in errorPartsSalesCategories) {
                        errorItem.ErrorMsg = ErrorStrings.Export_Title_PartsBranch_BranchName + errorItem.BranchNameStr + ErrorStrings.Export_Validation_PartsBranch_Validation1 + errorItem.PartsSalesCategoryNameStr + ErrorStrings.Export_Validation_PartsBranch_Validation2;
                    }
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();
                    var partsBranchNeedCheck = tempRightList.Select(r => r.PartCodeStr.ToUpper()).Distinct().ToArray();
                    var dbPartsBranches = new List<PartsBranchExtend>();
                    Func<string[], bool> getDbPartsBranches = value => {
                        var dbObj = new PartsBranchExtend {
                            Id = int.Parse(value[0]),
                            PartsSalesCategoryName = value[1],
                            BranchName = value[2],
                            BranchId = int.Parse(value[3]),
                            PartCode = value[4]
                        };
                        dbPartsBranches.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator(@"select Id, partssalescategoryName, branchName,branchId, partcode from partsbranch  where status = 1 ", "PartCode", true, partsBranchNeedCheck, getDbPartsBranches);

                    foreach(var Item in tempRightList) {
                        var dbitem = dbPartsBranches.Where(v => v.PartsSalesCategoryName == Item.PartsSalesCategoryNameStr && v.BranchId == userInfo.EnterpriseId && v.PartCode == Item.PartCodeStr).FirstOrDefault();
                        if(dbitem == null) {
                            Item.ErrorMsg = ErrorStrings.Export_Validation_PartsBranch_PartsSalescategoryIsNull;
                        } else {
                            Item.Id = dbitem.Id;
                        }
                    }
                    //获取所有不合格数据
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    //获取所有合格数据
                    rightList = allList.Except(errorList).ToList();
                    #endregion
                }

                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempObj = list[index - 1];
                            var values = new object[] {
                                tempObj.PartCodeStr, tempObj.PartsSalesCategoryNameStr, tempObj.IncreaseRateGroupCode, tempObj.IncreaseRateGroupName, tempObj.ErrorMsg
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }

                //更新导入所有合格数据
                if(!rightList.Any())
                    return true;
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    //开启事务，新增更新配件在一个事务内
                    var ts = conn.BeginTransaction();
                    try {
                        //修改配件
                        if(rightList.Any()) {
                            //获取修改数据的sql语句，Id为主键
                            var sqlUpdate = db.GetUpdateSql("PartsBranch", new[] {
                                "IncreaseRateGroupId", "ModifierId", "ModifierName", "ModifyTime","OMSparePartMark"
                            }, new[] {
                                "Id"
                            });
                            var userInfo = Utils.GetCurrentUserInfo();
                            //往数据库修改配件信息
                            foreach(var item in rightList) {
                                var command = db.CreateDbCommand(sqlUpdate, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("IncreaseRateGroupId", item.IncreaseRateGroupId));
                                command.Parameters.Add(db.CreateDbParameter("ModifierId", userInfo.Id));
                                command.Parameters.Add(db.CreateDbParameter("ModifierName", userInfo.Name));
                                command.Parameters.Add(db.CreateDbParameter("ModifyTime", DateTime.Now));
                                command.Parameters.Add(db.CreateDbParameter("Id", item.Id));
                                command.Parameters.Add(db.CreateDbParameter("OMSparePartMark", 0));
                                command.ExecuteNonQuery();
                            }
                            //增加履历
                            InsertPartsBranchHistoryByPartsIdOrList(db, rightList.Select(r => r.Id.ToString()).Distinct().ToArray(), null, conn, ts);
                        }
                        //无异常提交
                        ts.Commit();
                    } catch(Exception) {
                        //报错回滚
                        ts.Rollback();
                        throw;
                    } finally {
                        if(conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }

        public bool 批量替换审核上限(string fileName, out int excelImportNum, out List<PartsBranchExtend> rightData, out List<PartsBranchExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<PartsBranchExtend>();
            var rightList = new List<PartsBranchExtend>();
            var allList = new List<PartsBranchExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("PartsBranch", out notNullableFields, out fieldLenght);
                List<object> excelColumns;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Code, "PartCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Partssalescategory_Name, "PartsSalesCategoryName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_AutoApproveUpLimit, "AutoApproveUpLimit");
                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;

                    ////单次最大导入条数不能大于500！
                    //if(excelOperator.LastRowNum > 500) {
                    //    throw new Exception("单次最大导入条数不能大于500！");
                    //}

                    var keyValuePairs = new[] {
                        new KeyValuePair<string, string>("Status", "BaseData_Status")
                    };
                    tempExcelOperator.ImportHelper.LoadEnumFromDb(null, null, keyValuePairs);
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var tempImportObj = new PartsBranchExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);

                        #region
                        tempImportObj.PartsSalesCategoryNameStr = newRow["PartsSalesCategoryName"];
                        tempImportObj.PartsSalesCategoryName = newRow["PartsSalesCategoryName"];
                        tempImportObj.PartCodeStr = newRow["PartCode"];
                        tempImportObj.PartCode = newRow["PartCode"];
                        tempImportObj.AutoApproveUpLimitStr = newRow["AutoApproveUpLimit"];
                        #endregion

                        var tempErrorMessage = new List<string>();

                        #region 导入的内容基本检查

                        //品牌检查
                        var fieldIndex = notNullableFields.IndexOf("PartsSalesCategoryName".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.PartsSalesCategoryNameStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_PartsSalescategoryNameIsNull);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.PartsSalesCategoryNameStr) > fieldLenght["PartsSalesCategoryName".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_PartsSalescategoryNameIsLong);
                        }
                        //配件编号
                        fieldIndex = notNullableFields.IndexOf("PartCode".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.PartCodeStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.OverstockPartsAppDetail__Validation1);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.PartCodeStr) > fieldLenght["PartCode".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.ImpPartsSupplierRelation_Validation8);
                        }

                        //批量审核上限
                        if(!string.IsNullOrEmpty(tempImportObj.AutoApproveUpLimitStr)) {
                            int checkValue;
                            if(int.TryParse(tempImportObj.AutoApproveUpLimitStr, out checkValue)) {
                                if(checkValue < 0) {
                                    tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_AutoApproveUpLimiOverZero);
                                } else
                                    tempImportObj.AutoApproveUpLimit = checkValue;
                            } else {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_AutoApproveUpLimiIsNumber);
                            }
                        }

                        #endregion

                        if(tempErrorMessage.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });
                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    #region 导入内容业务检查
                    var groups = tempRightList.GroupBy(r => new {
                        r.PartCodeStr,
                        r.PartsSalesCategoryNameStr
                    }).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        groupItem.ErrorMsg = ErrorStrings.Export_Validation_PartsBranch_PartsSalescategoryReport;
                    }
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();

                    var partCodesNeedCheck = tempRightList.Select(r => r.PartCodeStr.ToUpper()).Distinct().ToArray();
                    var dbSparePartCodes = new List<PartsBranchExtend>();
                    Func<string[], bool> getDbSparePartCodes = value => {
                        var dbObj = new PartsBranchExtend {
                            PartId = Convert.ToInt32(value[0]),
                            PartCode = value[1],
                            PartName = value[2]
                        };
                        dbSparePartCodes.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator(@"select id,Trim(Code),Trim(Name) from sparePart where status=1 ", "Code", true, partCodesNeedCheck, getDbSparePartCodes);
                    foreach(var errorItem in tempRightList) {
                        var oldSparePart = dbSparePartCodes.FirstOrDefault(v => v.PartCode == errorItem.PartCodeStr);
                        if(oldSparePart == null) {
                            errorItem.ErrorMsg = String.Format(ErrorStrings.Export_Validation_PartsBranch_SparePartIsNull, errorItem.PartCodeStr);
                        } else {
                            errorItem.PartId = oldSparePart.PartId;
                            errorItem.PartName = oldSparePart.PartName;
                        }
                    }
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();

                    var partsSalesCategoryNeedCheck = tempRightList.Select(r => r.PartsSalesCategoryNameStr).Distinct().ToArray();
                    var dbPartsSalesCategories = new List<PartsBranchExtend>();
                    Func<string[], bool> getDbPartsSalesCategories = value => {
                        var dbObj = new PartsBranchExtend {
                            PartsSalesCategoryId = Convert.ToInt32(value[0]),
                            PartsSalesCategoryName = value[1],
                            BranchName = value[2],
                            BranchId = Convert.ToInt32(value[3])
                        };
                        dbPartsSalesCategories.Add(dbObj);
                        return false;
                    };
                    var userInfo = Utils.GetCurrentUserInfo();
                    db.QueryDataWithInOperator("select id,name,branchName,branchId from PartsSalesCategory where status=1 and BranchId = " + userInfo.EnterpriseId, "Name", true, partsSalesCategoryNeedCheck, getDbPartsSalesCategories);
                    var errorPartsSalesCategories = tempRightList.Where(r => !dbPartsSalesCategories.Any(v => v.PartsSalesCategoryName == r.PartsSalesCategoryName && v.BranchId == userInfo.EnterpriseId)).ToList();
                    foreach(var errorItem in errorPartsSalesCategories) {
                        errorItem.ErrorMsg = ErrorStrings.Export_Title_PartsBranch_BranchName + errorItem.BranchNameStr + ErrorStrings.Export_Validation_PartsBranch_Validation1 + errorItem.PartsSalesCategoryNameStr + ErrorStrings.Export_Validation_Sparepart_NotExist;
                    }
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();

                    var partsBranchNeedCheck = tempRightList.Select(r => r.PartCodeStr.ToUpper()).Distinct().ToArray();
                    var dbPartsBranches = new List<PartsBranchExtend>();
                    Func<string[], bool> getDbPartsBranches = value => {
                        var dbObj = new PartsBranchExtend {
                            Id = int.Parse(value[0]),
                            PartsSalesCategoryName = value[1],
                            BranchName = value[2],
                            BranchId = int.Parse(value[3]),
                            PartCode = value[4]
                        };
                        dbPartsBranches.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator(@"select Id, partssalescategoryName, branchName,branchId, partcode from partsbranch  where status = 1 ", "PartCode", true, partsBranchNeedCheck, getDbPartsBranches);

                    //var errorPartsBranches = tempRightList.Where(r => !dbPartsBranches.Any(v => v.PartsSalesCategoryName == r.PartsSalesCategoryNameStr && v.BranchId == userInfo.EnterpriseId && v.PartCode == r.PartCodeStr)).ToList();
                    foreach(var Item in tempRightList) {
                        var dbitem = dbPartsBranches.Where(v => v.PartsSalesCategoryName == Item.PartsSalesCategoryNameStr && v.BranchId == userInfo.EnterpriseId && v.PartCode == Item.PartCodeStr).FirstOrDefault();
                        if(dbitem == null) {
                            Item.ErrorMsg = ErrorStrings.Export_Validation_PartsBranch_PartsSalescategoryIsNull;
                        } else {
                            Item.Id = dbitem.Id;
                        }
                    }
                    #endregion

                    //获取所有不合格数据
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    //获取所有合格数据
                    rightList = allList.Except(errorList).ToList();
                }

                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempObj = list[index - 1];
                            var values = new object[] {
                                tempObj.PartCodeStr, tempObj.PartsSalesCategoryNameStr, tempObj.AutoApproveUpLimitStr, tempObj.ErrorMsg
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }

                //更新导入所有合格数据
                if(!rightList.Any())
                    return true;
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    //开启事务，新增更新配件在一个事务内
                    var ts = conn.BeginTransaction();
                    try {
                        //修改配件
                        if(rightList.Any()) {
                            //获取修改数据的sql语句，Id为主键
                            var sqlUpdate = db.GetUpdateSql("PartsBranch", new[] {
                                "ModifierId", "ModifierName", "ModifyTime", "AutoApproveUpLimit","OMSparePartMark"
                            }, new[] {
                                "Id"
                            });
                            var userInfo = Utils.GetCurrentUserInfo();
                            //往数据库修改配件信息
                            foreach(var item in rightList) {
                                var command = db.CreateDbCommand(sqlUpdate, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("AutoApproveUpLimit", item.AutoApproveUpLimit));
                                command.Parameters.Add(db.CreateDbParameter("ModifierId", userInfo.Id));
                                command.Parameters.Add(db.CreateDbParameter("ModifierName", userInfo.Name));
                                command.Parameters.Add(db.CreateDbParameter("ModifyTime", DateTime.Now));
                                command.Parameters.Add(db.CreateDbParameter("Id", item.Id));
                                command.Parameters.Add(db.CreateDbParameter("OMSparePartMark", 0));
                                command.ExecuteNonQuery();
                            }
                            //增加履历
                            InsertPartsBranchHistoryByPartsIdOrList(db, rightList.Select(r => r.Id.ToString()).Distinct().ToArray(), null, conn, ts);
                        }
                        //无异常提交
                        ts.Commit();
                    } catch(Exception) {
                        //报错回滚
                        ts.Rollback();
                        throw;
                    } finally {
                        if(conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }

        /// <summary>
        /// 批量替换保修分类
        /// </summary>
        public bool 批量替换保修分类(string fileName, out int excelImportNum, out List<PartsBranchExtend> rightData, out List<PartsBranchExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<PartsBranchExtend>();
            var rightList = new List<PartsBranchExtend>();
            var allList = new List<PartsBranchExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("PartsBranch", out notNullableFields, out fieldLenght);
                List<object> excelColumns;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Code, "PartCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Partssalescategory_Name, "PartsSalesCategoryName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_PartsWarrantyCategoryName, "PartsWarrantyCategoryName");
                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;

                    ////单次最大导入条数不能大于500！
                    //if(excelOperator.LastRowNum > 500) {
                    //    throw new Exception("单次最大导入条数不能大于500！");
                    //}

                    var keyValuePairs = new[] {
                        new KeyValuePair<string, string>("Status", "BaseData_Status")
                    };
                    tempExcelOperator.ImportHelper.LoadEnumFromDb(null, null, keyValuePairs);
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var tempImportObj = new PartsBranchExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);

                        #region
                        tempImportObj.PartsSalesCategoryNameStr = newRow["PartsSalesCategoryName"];
                        tempImportObj.PartsSalesCategoryName = newRow["PartsSalesCategoryName"];
                        tempImportObj.PartCodeStr = newRow["PartCode"];
                        tempImportObj.PartCode = newRow["PartCode"];
                        tempImportObj.PartsWarrantyCategoryName = newRow["PartsWarrantyCategoryName"];
                        tempImportObj.PartsWarrantyCategoryNameStr = newRow["PartsWarrantyCategoryName"];

                        #endregion

                        var tempErrorMessage = new List<string>();

                        #region 导入的内容基本检查

                        //品牌检查
                        var fieldIndex = notNullableFields.IndexOf("PartsSalesCategoryName".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.PartsSalesCategoryNameStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_PartsSalescategoryNameIsNull);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.PartsSalesCategoryNameStr) > fieldLenght["PartsSalesCategoryName".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_PartsSalescategoryNameIsLong);
                        }
                        //配件编号
                        fieldIndex = notNullableFields.IndexOf("PartCode".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.PartCodeStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.OverstockPartsAppDetail__Validation1);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.PartCodeStr) > fieldLenght["PartCode".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.ImpPartsSupplierRelation_Validation8);
                        }
                        //配件保修分类名称
                        //if(string.IsNullOrEmpty(tempImportObj.PartsWarrantyCategoryNameStr)) {
                        //    tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_PartsWarrantyCategoryNamesIsNull);
                        //}

                        #endregion

                        if(tempErrorMessage.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });
                    rightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    #region 导入内容业务检查
                    var groups = rightList.GroupBy(r => new {
                        r.PartCodeStr,
                        r.PartsSalesCategoryNameStr
                    }).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        groupItem.ErrorMsg = ErrorStrings.Export_Validation_PartsBranch_PartsSalescategoryReport;
                    }
                    rightList = rightList.Where(r => r.ErrorMsg == null).ToList();

                    var partCodesNeedCheck = rightList.Select(r => r.PartCodeStr.ToUpper()).Distinct().ToArray();
                    var dbSparePartCodes = new List<PartsBranchExtend>();
                    Func<string[], bool> getDbSparePartCodes = value => {
                        var dbObj = new PartsBranchExtend {
                            PartId = Convert.ToInt32(value[0]),
                            PartCode = value[1],
                            PartName = value[2]
                        };
                        dbSparePartCodes.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator(@"select id,Trim(Code),Trim(Name) from sparePart where status=1 ", "Code", true, partCodesNeedCheck, getDbSparePartCodes);
                    foreach(var errorItem in rightList) {
                        var oldSparePart = dbSparePartCodes.FirstOrDefault(v => v.PartCode == errorItem.PartCodeStr);
                        if(oldSparePart == null) {
                            errorItem.ErrorMsg = String.Format(ErrorStrings.Export_Validation_PartsBranch_SparePartIsNull, errorItem.PartCodeStr);
                        } else {
                            errorItem.PartId = oldSparePart.PartId;
                            errorItem.PartName = oldSparePart.PartName;
                        }
                    }
                    rightList = rightList.Where(r => r.ErrorMsg == null).ToList();

                    var partsWarrantyNameNeedCheck = rightList.Where(r => r.PartsWarrantyCategoryName != null).Select(r => r.PartsWarrantyCategoryName.ToUpper()).Distinct().ToArray();
                    var dbPartsWarrantyNames = new List<PartsBranchExtend>();
                    Func<string[], bool> getDbPartsWarrantyCodes = value => {
                        var dbObj = new PartsBranchExtend {
                            PartsWarrantyCategoryId = Convert.ToInt32(value[0]),
                            PartsWarrantyCategoryCode = value[1],
                            PartsWarrantyCategoryName = value[2]
                        };
                        dbPartsWarrantyNames.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator(@" select Id,Code,Name  from PartsWarrantyCategory where status = 1 ", "Name", true, partsWarrantyNameNeedCheck, getDbPartsWarrantyCodes);
                    foreach(var errorItem in rightList) {
                        if(errorItem.PartsWarrantyCategoryName != null) {
                            var oldPartsWarranty = dbPartsWarrantyNames.FirstOrDefault(v => v.PartsWarrantyCategoryName == errorItem.PartsWarrantyCategoryName);
                            if(oldPartsWarranty == null) {
                                errorItem.ErrorMsg = String.Format(ErrorStrings.Export_Validation_PartsBranch_PartsWarrantyCategoryNameNotExist,errorItem.PartsWarrantyCategoryName);
                            } else {
                                errorItem.PartsWarrantyCategoryId = oldPartsWarranty.PartsWarrantyCategoryId;
                                errorItem.PartsWarrantyCategoryCode = oldPartsWarranty.PartsWarrantyCategoryCode;
                                errorItem.PartsWarrantyCategoryName = oldPartsWarranty.PartsWarrantyCategoryName;
                                errorItem.PartsWarrantyCategoryNameStr = oldPartsWarranty.PartsWarrantyCategoryName;
                            }
                        }
                    }
                    rightList = rightList.Where(r => r.ErrorMsg == null).ToList();

                    var partsSalesCategoryNeedCheck = rightList.Select(r => r.PartsSalesCategoryNameStr).Distinct().ToArray();
                    var dbPartsSalesCategories = new List<PartsBranchExtend>();
                    Func<string[], bool> getDbPartsSalesCategories = value => {
                        var dbObj = new PartsBranchExtend {
                            PartsSalesCategoryId = Convert.ToInt32(value[0]),
                            PartsSalesCategoryName = value[1],
                            BranchName = value[2],
                            BranchId = Convert.ToInt32(value[3])
                        };
                        dbPartsSalesCategories.Add(dbObj);
                        return false;
                    };
                    var userInfo = Utils.GetCurrentUserInfo();
                    db.QueryDataWithInOperator("select id,name,branchName,branchId from PartsSalesCategory where status=1 and BranchId = " + userInfo.EnterpriseId, "Name", true, partsSalesCategoryNeedCheck, getDbPartsSalesCategories);

                    foreach(var errorItem in rightList) {
                        var partsSalesCategory = dbPartsSalesCategories.FirstOrDefault(v => v.PartsSalesCategoryName == errorItem.PartsSalesCategoryNameStr);
                        if(partsSalesCategory == null) {
                            errorItem.ErrorMsg = ErrorStrings.Export_Title_PartsBranch_BranchName + errorItem.BranchNameStr + ErrorStrings.Export_Validation_PartsBranch_Validation1 + errorItem.PartsSalesCategoryNameStr + ErrorStrings.Export_Validation_PartsBranch_Validation2;
                        } else {
                            errorItem.PartsSalesCategoryId = partsSalesCategory.PartsSalesCategoryId;
                            errorItem.PartsSalesCategoryName = partsSalesCategory.PartsSalesCategoryName;
                        }
                    }
                    rightList = rightList.Where(r => r.ErrorMsg == null).ToList();
                    var partsBranchNeedCheck = rightList.Select(r => r.PartCodeStr.ToUpper()).Distinct().ToArray();
                    var dbPartsBranches = new List<PartsBranchExtend>();
                    Func<string[], bool> getDbPartsBranches = value => {
                        var dbObj = new PartsBranchExtend {
                            Id = int.Parse(value[0]),
                            PartsSalesCategoryName = value[1],
                            BranchName = value[2],
                            BranchId = int.Parse(value[3]),
                            PartCode = value[4]
                        };
                        dbPartsBranches.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator(@"select Id, partssalescategoryName, branchName,branchId, partcode from partsbranch  where status = 1 ", "PartCode", true, partsBranchNeedCheck, getDbPartsBranches);

                    foreach(var Item in rightList) {
                        var dbitem = dbPartsBranches.Where(v => v.PartsSalesCategoryName == Item.PartsSalesCategoryNameStr && v.BranchId == userInfo.EnterpriseId && v.PartCode == Item.PartCodeStr).FirstOrDefault();
                        if(dbitem == null) {
                            Item.ErrorMsg = ErrorStrings.Export_Validation_PartsBranch_PartsSalescategoryIsNull;
                        } else {
                            Item.Id = dbitem.Id;
                        }
                    }

                    //获取所有不合格数据
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    //获取所有合格数据
                    rightList = allList.Except(errorList).ToList();
                    #endregion
                }

                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempObj = list[index - 1];
                            var values = new object[] {
                                tempObj.PartCodeStr, tempObj.PartsSalesCategoryNameStr, tempObj.PartsWarrantyCategoryName, tempObj.ErrorMsg
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }

                //更新导入所有合格数据
                if(!rightList.Any())
                    return true;
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    //开启事务，新增更新配件在一个事务内
                    var ts = conn.BeginTransaction();
                    try {
                        //修改配件
                        if(rightList.Any()) {
                            //获取修改数据的sql语句，Id为主键
                            var sqlUpdate = db.GetUpdateSql("PartsBranch", new[] {
                                "PartsWarrantyCategoryId", "PartsWarrantyCategoryCode", "PartsWarrantyCategoryName", "ModifierId", "ModifierName", "ModifyTime","OMSparePartMark"
                            }, new[] {
                                "Id"
                            });
                            var userInfo = Utils.GetCurrentUserInfo();
                            //往数据库修改配件信息
                            foreach(var item in rightList) {
                                var command = db.CreateDbCommand(sqlUpdate, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("PartsWarrantyCategoryId", item.PartsWarrantyCategoryId));
                                command.Parameters.Add(db.CreateDbParameter("PartsWarrantyCategoryCode", item.PartsWarrantyCategoryCode));
                                command.Parameters.Add(db.CreateDbParameter("PartsWarrantyCategoryName", item.PartsWarrantyCategoryName));
                                command.Parameters.Add(db.CreateDbParameter("ModifierId", userInfo.Id));
                                command.Parameters.Add(db.CreateDbParameter("ModifierName", userInfo.Name));
                                command.Parameters.Add(db.CreateDbParameter("ModifyTime", DateTime.Now));
                                command.Parameters.Add(db.CreateDbParameter("Id", item.Id));
                                command.Parameters.Add(db.CreateDbParameter("OMSparePartMark", 0));
                                command.ExecuteNonQuery();
                            }
                            //增加履历
                            InsertPartsBranchHistoryByPartsIdOrList(db, rightList.Select(r => r.Id.ToString()).Distinct().ToArray(), null, conn, ts);
                        }
                        //无异常提交
                        ts.Commit();
                    } catch(Exception) {
                        //报错回滚
                        ts.Rollback();
                        throw;
                    } finally {
                        if(conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }

        /// <summary>
        /// 批量替换旧件返回政策
        /// </summary>
        public bool 批量替换旧件返回政策(string fileName, out int excelImportNum, out List<PartsBranchExtend> rightData, out List<PartsBranchExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<PartsBranchExtend>();
            var rightList = new List<PartsBranchExtend>();
            var allList = new List<PartsBranchExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("PartsBranch", out notNullableFields, out fieldLenght);
                List<object> excelColumns;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Code, "PartCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Partssalescategory_Name, "PartsSalesCategoryName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_PartsReturnPolicy, "PartsReturnPolicy");
                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;

                    ////单次最大导入条数不能大于500！
                    //if(excelOperator.LastRowNum > 500) {
                    //    throw new Exception("单次最大导入条数不能大于500！");
                    //}

                    var keyValuePairs = new[] {
                        new KeyValuePair<string, string>("Status", "BaseData_Status"), new KeyValuePair<string, string>("PartsReturnPolicy", "PartsWarrantyTerm_ReturnPolicy")
                    };
                    tempExcelOperator.ImportHelper.LoadEnumFromDb(null, null, keyValuePairs);
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var tempImportObj = new PartsBranchExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);

                        #region
                        tempImportObj.PartsSalesCategoryNameStr = newRow["PartsSalesCategoryName"];
                        tempImportObj.PartsSalesCategoryName = newRow["PartsSalesCategoryName"];
                        tempImportObj.PartCodeStr = newRow["PartCode"];
                        tempImportObj.PartCode = newRow["PartCode"];
                        tempImportObj.PartsReturnPolicyStr = newRow["PartsReturnPolicy"];

                        #endregion

                        var tempErrorMessage = new List<string>();

                        #region 导入的内容基本检查

                        //品牌检查
                        var fieldIndex = notNullableFields.IndexOf("PartsSalesCategoryName".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.PartsSalesCategoryNameStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_PartsSalescategoryNameIsNull);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.PartsSalesCategoryNameStr) > fieldLenght["PartsSalesCategoryName".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_PartsSalescategoryNameIsLong);
                        }
                        //配件编号
                        fieldIndex = notNullableFields.IndexOf("PartCode".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.PartCodeStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.OverstockPartsAppDetail__Validation1);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.PartCodeStr) > fieldLenght["PartCode".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.ImpPartsSupplierRelation_Validation8);
                        }
                        //配件返回政策 
                        fieldIndex = notNullableFields.IndexOf("PartsReturnPolicy".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.PartsReturnPolicyStr)) {
                            //if(fieldIndex > -1)
                            //    tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_PartsReturnPolicyIsNull);
                        } else {
                            var tempEnumValue = tempExcelOperator.ImportHelper.GetEnumValue("PartsReturnPolicy", tempImportObj.PartsReturnPolicyStr);
                            if(!tempEnumValue.HasValue) {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_PartsReturnPolicyIsValueError);
                            } else {
                                tempImportObj.PartsReturnPolicy = tempEnumValue.Value;
                            }
                        }


                        #endregion

                        if(tempErrorMessage.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });
                    rightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    #region 导入内容业务检查
                    var groups = rightList.GroupBy(r => new {
                        r.PartCodeStr,
                        r.PartsSalesCategoryNameStr
                    }).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        groupItem.ErrorMsg = ErrorStrings.Export_Validation_PartsBranch_PartsSalescategoryReport;
                    }
                    rightList = rightList.Where(r => r.ErrorMsg == null).ToList();

                    var partCodesNeedCheck = rightList.Select(r => r.PartCodeStr.ToUpper()).Distinct().ToArray();
                    var dbSparePartCodes = new List<PartsBranchExtend>();
                    Func<string[], bool> getDbSparePartCodes = value => {
                        var dbObj = new PartsBranchExtend {
                            PartId = Convert.ToInt32(value[0]),
                            PartCode = value[1],
                            PartName = value[2]
                        };
                        dbSparePartCodes.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator(@"select id,Trim(Code),Trim(Name) from sparePart where status=1 ", "Code", true, partCodesNeedCheck, getDbSparePartCodes);
                    foreach(var errorItem in rightList) {
                        var oldSparePart = dbSparePartCodes.FirstOrDefault(v => v.PartCode == errorItem.PartCodeStr);
                        if(oldSparePart == null) {
                            errorItem.ErrorMsg = String.Format(ErrorStrings.Export_Validation_PartsBranch_SparePartIsNull, errorItem.PartCodeStr);
                        } else {
                            errorItem.PartId = oldSparePart.PartId;
                            errorItem.PartName = oldSparePart.PartName;
                        }
                    }
                    rightList = rightList.Where(r => r.ErrorMsg == null).ToList();

                    var partsSalesCategoryNeedCheck = rightList.Select(r => r.PartsSalesCategoryNameStr).Distinct().ToArray();
                    var dbPartsSalesCategories = new List<PartsBranchExtend>();
                    Func<string[], bool> getDbPartsSalesCategories = value => {
                        var dbObj = new PartsBranchExtend {
                            PartsSalesCategoryId = Convert.ToInt32(value[0]),
                            PartsSalesCategoryName = value[1],
                            BranchName = value[2],
                            BranchId = Convert.ToInt32(value[3])
                        };
                        dbPartsSalesCategories.Add(dbObj);
                        return false;
                    };
                    var userInfo = Utils.GetCurrentUserInfo();
                    db.QueryDataWithInOperator("select id,name,branchName,branchId from PartsSalesCategory where status=1 and BranchId = " + userInfo.EnterpriseId, "Name", true, partsSalesCategoryNeedCheck, getDbPartsSalesCategories);
                    var errorPartsSalesCategories = rightList.Where(r => !dbPartsSalesCategories.Any(v => v.PartsSalesCategoryName == r.PartsSalesCategoryName && v.BranchId == userInfo.EnterpriseId)).ToList();
                    foreach(var errorItem in errorPartsSalesCategories) {
                        errorItem.ErrorMsg = ErrorStrings.Export_Title_PartsBranch_BranchName + errorItem.BranchNameStr + ErrorStrings.Export_Validation_PartsBranch_Validation1 + errorItem.PartsSalesCategoryNameStr + ErrorStrings.Export_Validation_PartsBranch_Validation2;
                    }
                    rightList = rightList.Where(r => r.ErrorMsg == null).ToList();

                    var partsBranchNeedCheck = rightList.Select(r => r.PartCodeStr.ToUpper()).Distinct().ToArray();
                    var dbPartsBranches = new List<PartsBranchExtend>();
                    Func<string[], bool> getDbPartsBranches = value => {
                        var dbObj = new PartsBranchExtend {
                            Id = int.Parse(value[0]),
                            PartsSalesCategoryName = value[1],
                            BranchName = value[2],
                            BranchId = int.Parse(value[3]),
                            PartCode = value[4]
                        };
                        dbPartsBranches.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator(@"select Id, partssalescategoryName, branchName,branchId, partcode from partsbranch  where status = 1 ", "PartCode", true, partsBranchNeedCheck, getDbPartsBranches);

                    foreach(var Item in rightList) {
                        var dbitem = dbPartsBranches.Where(v => v.PartsSalesCategoryName == Item.PartsSalesCategoryNameStr && v.BranchId == userInfo.EnterpriseId && v.PartCode == Item.PartCodeStr).FirstOrDefault();
                        if(dbitem == null) {
                            Item.ErrorMsg = ErrorStrings.Export_Validation_PartsBranch_PartsSalescategoryIsNull;
                        } else {
                            Item.Id = dbitem.Id;
                        }
                    }
                    //获取所有不合格数据
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    #endregion
                }


                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempObj = list[index - 1];
                            var values = new object[] {
                                tempObj.PartCodeStr, tempObj.PartsSalesCategoryNameStr, tempObj.PartsReturnPolicy, tempObj.ErrorMsg
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }

                //更新导入所有合格数据
                if(!rightList.Any())
                    return true;
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    //开启事务，新增更新配件在一个事务内
                    var ts = conn.BeginTransaction();
                    try {
                        //修改配件
                        if(rightList.Any()) {
                            //获取修改数据的sql语句，Id为主键
                            var sqlUpdate = db.GetUpdateSql("PartsBranch", new[] {
                                "PartsReturnPolicy", "ModifierId", "ModifierName", "ModifyTime","OMSparePartMark"
                            }, new[] {
                                "Id"
                            });
                            var userInfo = Utils.GetCurrentUserInfo();
                            //往数据库修改配件信息
                            foreach(var item in rightList) {
                                var command = db.CreateDbCommand(sqlUpdate, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("PartsReturnPolicy", item.PartsReturnPolicy));
                                command.Parameters.Add(db.CreateDbParameter("ModifierId", userInfo.Id));
                                command.Parameters.Add(db.CreateDbParameter("ModifierName", userInfo.Name));
                                command.Parameters.Add(db.CreateDbParameter("ModifyTime", DateTime.Now));
                                command.Parameters.Add(db.CreateDbParameter("Id", item.Id));
                                command.Parameters.Add(db.CreateDbParameter("OMSparePartMark", 0));
                                command.ExecuteNonQuery();
                            }
                            //增加履历
                            InsertPartsBranchHistoryByPartsIdOrList(db, rightList.Select(r => r.Id.ToString()).Distinct().ToArray(), null, conn, ts);
                        }
                        //无异常提交
                        ts.Commit();
                    } catch(Exception) {
                        //报错回滚
                        ts.Rollback();
                        throw;
                    } finally {
                        if(conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }

        /// <summary>
        /// 批量替换损耗类型及高低限
        /// </summary>
        public bool 批量替换损耗类型及高低限(string fileName, out int excelImportNum, out List<PartsBranchExtend> rightData, out List<PartsBranchExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<PartsBranchExtend>();
            var rightList = new List<PartsBranchExtend>();
            var allList = new List<PartsBranchExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("PartsBranch", out notNullableFields, out fieldLenght);
                List<object> excelColumns;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Code, "PartCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Partssalescategory_Name, "PartsSalesCategoryName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_LossType, "LossType");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_StockMaximum, "StockMaximum");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_StockMinimum, "StockMinimum");
                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;

                    ////单次最大导入条数不能大于500！
                    //if(excelOperator.LastRowNum > 500) {
                    //    throw new Exception("单次最大导入条数不能大于500！");
                    //}

                    var keyValuePairs = new[] {
                        new KeyValuePair<string, string>("LossType", "SparePart_LossType"), new KeyValuePair<string, string>("Status", "BaseData_Status")
                    };
                    tempExcelOperator.ImportHelper.LoadEnumFromDb(null, null, keyValuePairs);
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var tempImportObj = new PartsBranchExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);

                        #region
                        tempImportObj.PartsSalesCategoryNameStr = newRow["PartsSalesCategoryName"];
                        tempImportObj.PartsSalesCategoryName = newRow["PartsSalesCategoryName"];
                        tempImportObj.PartCodeStr = newRow["PartCode"];
                        tempImportObj.PartCode = newRow["PartCode"];
                        tempImportObj.LossTypeStr = newRow["LossType"];
                        tempImportObj.StockMaximumStr = newRow["StockMaximum"];
                        tempImportObj.StockMinimumStr = newRow["StockMinimum"];

                        #endregion

                        var tempErrorMessage = new List<string>();

                        #region 导入的内容基本检查

                        //品牌检查
                        var fieldIndex = notNullableFields.IndexOf("PartsSalesCategoryName".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.PartsSalesCategoryNameStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_PartsSalescategoryNameIsNull);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.PartsSalesCategoryNameStr) > fieldLenght["PartsSalesCategoryName".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_PartsSalescategoryNameIsLong);
                        }
                        //配件编号
                        fieldIndex = notNullableFields.IndexOf("PartCode".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.PartCodeStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.OverstockPartsAppDetail__Validation1);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.PartCodeStr) > fieldLenght["PartCode".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.ImpPartsSupplierRelation_Validation8);
                        }

                        //损耗类型
                        fieldIndex = notNullableFields.IndexOf("LossType".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.LossTypeStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_LossTypeIsNull);
                        } else {
                            var tempEnumValue = tempExcelOperator.ImportHelper.GetEnumValue("LossType", tempImportObj.LossTypeStr);
                            if(!tempEnumValue.HasValue) {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_LossTypeValueError);
                            } else {
                                tempImportObj.LossType = tempEnumValue;

                            }
                        }


                        //库存高限StockMaximum
                        fieldIndex = notNullableFields.IndexOf("StockMaximum".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.StockMaximumStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_StockMaximumIsNull);
                        } else {
                            if(tempImportObj.StockMaximumStr != null) {
                                int checkValue;
                                if(int.TryParse(tempImportObj.StockMaximumStr, out checkValue)) {
                                    if(checkValue <= 0) {
                                        tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_StockMaximumIsOverZeroNew);
                                    }
                                    tempImportObj.StockMaximum = int.Parse(tempImportObj.StockMaximumStr);
                                } else {
                                    tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_Validation3);
                                }
                            }
                        }
                        //库存低限StockMinimum
                        fieldIndex = notNullableFields.IndexOf("StockMinimum".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.StockMinimumStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_StockMinimum);
                        } else {
                            if(tempImportObj.StockMinimumStr != null) {
                                int checkValue;
                                if(int.TryParse(tempImportObj.StockMinimumStr, out checkValue)) {
                                    if(checkValue <= 0) {
                                        tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_StockMinimumIsOverZero);
                                    }
                                    tempImportObj.StockMinimum = int.Parse(tempImportObj.StockMinimumStr);
                                } else {
                                    tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_StockMinimumIsNumber);
                                }
                            }
                        }

                        #endregion

                        if(tempErrorMessage.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });
                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    #region 导入内容业务检查
                    var groups = tempRightList.GroupBy(r => new {
                        r.PartCodeStr,
                        r.PartsSalesCategoryNameStr
                    }).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        groupItem.ErrorMsg = ErrorStrings.Export_Validation_PartsBranch_PartsSalescategoryReport;
                    }
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();

                    var partCodesNeedCheck = tempRightList.Select(r => r.PartCodeStr.ToUpper()).Distinct().ToArray();
                    var dbSparePartCodes = new List<PartsBranchExtend>();
                    Func<string[], bool> getDbSparePartCodes = value => {
                        var dbObj = new PartsBranchExtend {
                            PartId = Convert.ToInt32(value[0]),
                            PartCode = value[1],
                            PartName = value[2]
                        };
                        dbSparePartCodes.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator(@"select id,Trim(Code),Trim(Name) from sparePart where status=1 ", "Code", true, partCodesNeedCheck, getDbSparePartCodes);
                    foreach(var errorItem in tempRightList) {
                        var oldSparePart = dbSparePartCodes.FirstOrDefault(v => v.PartCode == errorItem.PartCodeStr);
                        if(oldSparePart == null) {
                            errorItem.ErrorMsg = String.Format(ErrorStrings.Export_Validation_PartsBranch_SparePartIsNull, errorItem.PartCodeStr);
                        } else {
                            errorItem.PartId = oldSparePart.PartId;
                            errorItem.PartName = oldSparePart.PartName;
                        }
                    }
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();

                    var partsSalesCategoryNeedCheck = tempRightList.Select(r => r.PartsSalesCategoryNameStr).Distinct().ToArray();
                    var dbPartsSalesCategories = new List<PartsBranchExtend>();
                    Func<string[], bool> getDbPartsSalesCategories = value => {
                        var dbObj = new PartsBranchExtend {
                            PartsSalesCategoryId = Convert.ToInt32(value[0]),
                            PartsSalesCategoryName = value[1],
                            BranchName = value[2],
                            BranchId = Convert.ToInt32(value[3])
                        };
                        dbPartsSalesCategories.Add(dbObj);
                        return false;
                    };
                    var userInfo = Utils.GetCurrentUserInfo();
                    db.QueryDataWithInOperator("select id,name,branchName,branchId from PartsSalesCategory where status=1 and BranchId = " + userInfo.EnterpriseId, "Name", true, partsSalesCategoryNeedCheck, getDbPartsSalesCategories);
                    var errorPartsSalesCategories = tempRightList.Where(r => !dbPartsSalesCategories.Any(v => v.PartsSalesCategoryName == r.PartsSalesCategoryName && v.BranchId == userInfo.EnterpriseId)).ToList();
                    foreach(var errorItem in errorPartsSalesCategories) {
                        errorItem.ErrorMsg = ErrorStrings.Export_Title_PartsBranch_BranchName + errorItem.BranchNameStr + ErrorStrings.Export_Validation_PartsBranch_Validation1 + errorItem.PartsSalesCategoryNameStr + ErrorStrings.Export_Validation_PartsBranch_Validation2;
                    }
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();

                    var partsBranchNeedCheck = tempRightList.Select(r => r.PartCodeStr.ToUpper()).Distinct().ToArray();
                    var dbPartsBranches = new List<PartsBranchExtend>();
                    Func<string[], bool> getDbPartsBranches = value => {
                        var dbObj = new PartsBranchExtend {
                            Id = int.Parse(value[0]),
                            PartsSalesCategoryName = value[1],
                            BranchName = value[2],
                            BranchId = int.Parse(value[3]),
                            PartCode = value[4]
                        };
                        dbPartsBranches.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator(@"select Id, partssalescategoryName, branchName,branchId, partcode from partsbranch  where status = 1 ", "PartCode", true, partsBranchNeedCheck, getDbPartsBranches);

                    foreach(var Item in tempRightList) {
                        var dbitem = dbPartsBranches.Where(v => v.PartsSalesCategoryName == Item.PartsSalesCategoryNameStr && v.BranchId == userInfo.EnterpriseId && v.PartCode == Item.PartCodeStr).FirstOrDefault();
                        if(dbitem == null) {
                            Item.ErrorMsg = ErrorStrings.Export_Validation_PartsBranch_PartsSalescategoryIsNull;
                        } else {
                            Item.Id = dbitem.Id;
                        }
                    }
                    //获取所有不合格数据
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    //获取所有合格数据
                    rightList = allList.Except(errorList).ToList();
                    #endregion
                }

                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempObj = list[index - 1];
                            var values = new object[] {
                                tempObj.PartCodeStr, tempObj.PartsSalesCategoryNameStr, tempObj.LossTypeStr, tempObj.StockMaximumStr, tempObj.StockMinimumStr, tempObj.ErrorMsg
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }

                //更新导入所有合格数据
                if(!rightList.Any())
                    return true;
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    //开启事务，新增更新配件在一个事务内
                    var ts = conn.BeginTransaction();
                    try {
                        //修改配件
                        if(rightList.Any()) {
                            //获取修改数据的sql语句，Id为主键
                            var sqlUpdate = db.GetUpdateSql("PartsBranch", new[] {
                                "LossType", "StockMaximum", "StockMinimum", "ModifierId", "ModifierName", "ModifyTime","OMSparePartMark"
                            }, new[] {
                                "Id"
                            });
                            var userInfo = Utils.GetCurrentUserInfo();
                            //往数据库修改配件信息
                            foreach(var item in rightList) {
                                var command = db.CreateDbCommand(sqlUpdate, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("LossType", item.LossType));
                                command.Parameters.Add(db.CreateDbParameter("StockMaximum", item.StockMaximum));
                                command.Parameters.Add(db.CreateDbParameter("StockMinimum", item.StockMinimum));
                                command.Parameters.Add(db.CreateDbParameter("ModifierId", userInfo.Id));
                                command.Parameters.Add(db.CreateDbParameter("ModifierName", userInfo.Name));
                                command.Parameters.Add(db.CreateDbParameter("ModifyTime", DateTime.Now));
                                command.Parameters.Add(db.CreateDbParameter("Id", item.Id));
                                command.Parameters.Add(db.CreateDbParameter("OMSparePartMark", 0));
                                command.ExecuteNonQuery();
                            }
                            //增加履历
                            InsertPartsBranchHistoryByPartsIdOrList(db, rightList.Select(r => r.Id.ToString()).Distinct().ToArray(), null, conn, ts);
                        }
                        //无异常提交
                        ts.Commit();
                    } catch(Exception) {
                        //报错回滚
                        ts.Rollback();
                        throw;
                    } finally {
                        if(conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }

        /// <summary>
        /// 批量替换备注
        /// </summary>
        public bool 批量替换备注(string fileName, out int excelImportNum, out List<PartsBranchExtend> rightData, out List<PartsBranchExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<PartsBranchExtend>();
            var rightList = new List<PartsBranchExtend>();
            var allList = new List<PartsBranchExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("PartsBranch", out notNullableFields, out fieldLenght);
                List<object> excelColumns;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Code, "PartCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Partssalescategory_Name, "PartsSalesCategoryName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Remark, "Remark");
                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;

                    ////单次最大导入条数不能大于500！
                    //if(excelOperator.LastRowNum > 500) {
                    //    throw new Exception("单次最大导入条数不能大于500！");
                    //}

                    var keyValuePairs = new[] {
                        new KeyValuePair<string, string>("Status", "BaseData_Status")
                    };
                    tempExcelOperator.ImportHelper.LoadEnumFromDb(null, null, keyValuePairs);
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var tempImportObj = new PartsBranchExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);

                        #region
                        tempImportObj.PartsSalesCategoryNameStr = newRow["PartsSalesCategoryName"];
                        tempImportObj.PartsSalesCategoryName = newRow["PartsSalesCategoryName"];
                        tempImportObj.PartCodeStr = newRow["PartCode"];
                        tempImportObj.PartCode = newRow["PartCode"];
                        tempImportObj.RemarkStr = newRow["Remark"];
                        tempImportObj.Remark = newRow["Remark"];

                        #endregion

                        var tempErrorMessage = new List<string>();

                        #region 导入的内容基本检查

                        //品牌检查
                        var fieldIndex = notNullableFields.IndexOf("PartsSalesCategoryName".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.PartsSalesCategoryNameStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_PartsSalescategoryNameIsNull);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.PartsSalesCategoryNameStr) > fieldLenght["PartsSalesCategoryName".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_PartsSalescategoryNameIsLong);
                        }
                        //配件编号
                        fieldIndex = notNullableFields.IndexOf("PartCode".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.PartCodeStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.OverstockPartsAppDetail__Validation1);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.PartCodeStr) > fieldLenght["PartCode".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.ImpPartsSupplierRelation_Validation8);
                        }
                        //备注
                        if(tempImportObj.RemarkStr != null && Encoding.Default.GetByteCount(tempImportObj.RemarkStr) > 200)
                            tempErrorMessage.Add(ErrorStrings.ImpPartsPurchasePricingDetail_Validation16);

                        #endregion

                        if(tempErrorMessage.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });
                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    #region 导入内容业务检查
                    var groups = tempRightList.GroupBy(r => new {
                        r.PartCodeStr,
                        r.PartsSalesCategoryNameStr
                    }).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        groupItem.ErrorMsg = ErrorStrings.Export_Validation_PartsBranch_PartsSalescategoryReport;
                    }
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();

                    var partCodesNeedCheck = tempRightList.Select(r => r.PartCodeStr.ToUpper()).Distinct().ToArray();
                    var dbSparePartCodes = new List<PartsBranchExtend>();
                    Func<string[], bool> getDbSparePartCodes = value => {
                        var dbObj = new PartsBranchExtend {
                            PartId = Convert.ToInt32(value[0]),
                            PartCode = value[1],
                            PartName = value[2]
                        };
                        dbSparePartCodes.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator(@"select id,Trim(Code),Trim(Name) from sparePart where status=1 ", "Code", true, partCodesNeedCheck, getDbSparePartCodes);
                    foreach(var errorItem in tempRightList) {
                        var oldSparePart = dbSparePartCodes.FirstOrDefault(v => v.PartCode == errorItem.PartCodeStr);
                        if(oldSparePart == null) {
                            errorItem.ErrorMsg = String.Format(ErrorStrings.Export_Validation_PartsBranch_SparePartIsNull, errorItem.PartCodeStr);
                        } else {
                            errorItem.PartId = oldSparePart.PartId;
                            errorItem.PartName = oldSparePart.PartName;
                        }
                    }
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();

                    var partsSalesCategoryNeedCheck = tempRightList.Select(r => r.PartsSalesCategoryNameStr).Distinct().ToArray();
                    var dbPartsSalesCategories = new List<PartsBranchExtend>();
                    Func<string[], bool> getDbPartsSalesCategories = value => {
                        var dbObj = new PartsBranchExtend {
                            PartsSalesCategoryId = Convert.ToInt32(value[0]),
                            PartsSalesCategoryName = value[1],
                            BranchName = value[2],
                            BranchId = Convert.ToInt32(value[3])
                        };
                        dbPartsSalesCategories.Add(dbObj);
                        return false;
                    };
                    var userInfo = Utils.GetCurrentUserInfo();
                    db.QueryDataWithInOperator("select id,name,branchName,branchId from PartsSalesCategory where status=1 and BranchId = " + userInfo.EnterpriseId, "Name", true, partsSalesCategoryNeedCheck, getDbPartsSalesCategories);
                    var errorPartsSalesCategories = tempRightList.Where(r => !dbPartsSalesCategories.Any(v => v.PartsSalesCategoryName == r.PartsSalesCategoryName && v.BranchId == userInfo.EnterpriseId)).ToList();
                    foreach(var errorItem in errorPartsSalesCategories) {
                        errorItem.ErrorMsg = ErrorStrings.Export_Title_PartsBranch_BranchName + errorItem.BranchNameStr + ErrorStrings.Export_Validation_PartsBranch_Validation1 + errorItem.PartsSalesCategoryNameStr + ErrorStrings.Export_Validation_PartsBranch_Validation2;
                    }
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();

                    var partsBranchNeedCheck = tempRightList.Select(r => r.PartCodeStr.ToUpper()).Distinct().ToArray();
                    var dbPartsBranches = new List<PartsBranchExtend>();
                    Func<string[], bool> getDbPartsBranches = value => {
                        var dbObj = new PartsBranchExtend {
                            Id = int.Parse(value[0]),
                            PartsSalesCategoryName = value[1],
                            BranchName = value[2],
                            BranchId = int.Parse(value[3]),
                            PartCode = value[4]
                        };
                        dbPartsBranches.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator(@"select Id, partssalescategoryName, branchName,branchId, partcode from partsbranch  where status = 1 ", "PartCode", true, partsBranchNeedCheck, getDbPartsBranches);

                    foreach(var Item in tempRightList) {
                        var dbitem = dbPartsBranches.Where(v => v.PartsSalesCategoryName == Item.PartsSalesCategoryNameStr && v.BranchId == userInfo.EnterpriseId && v.PartCode == Item.PartCodeStr).FirstOrDefault();
                        if(dbitem == null) {
                            Item.ErrorMsg = ErrorStrings.Export_Validation_PartsBranch_PartsSalescategoryIsNull;
                        } else {
                            Item.Id = dbitem.Id;
                        }
                    }
                    //获取所有不合格数据
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    //获取所有合格数据
                    rightList = allList.Except(errorList).ToList();
                    #endregion
                }

                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempObj = list[index - 1];
                            var values = new object[] {
                                tempObj.PartCodeStr, tempObj.PartsSalesCategoryNameStr, tempObj.Remark, tempObj.ErrorMsg
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }

                //更新导入所有合格数据
                if(!rightList.Any())
                    return true;
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    //开启事务，新增更新配件在一个事务内
                    var ts = conn.BeginTransaction();
                    try {
                        //修改配件
                        if(rightList.Any()) {
                            //获取修改数据的sql语句，Id为主键
                            var sqlUpdate = db.GetUpdateSql("PartsBranch", new[] {
                                "Remark", "ModifierId", "ModifierName", "ModifyTime","OMSparePartMark"
                            }, new[] {
                                "Id"
                            });
                            var userInfo = Utils.GetCurrentUserInfo();
                            //往数据库修改配件信息
                            foreach(var item in rightList) {
                                var command = db.CreateDbCommand(sqlUpdate, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("Remark", item.Remark));
                                command.Parameters.Add(db.CreateDbParameter("ModifierId", userInfo.Id));
                                command.Parameters.Add(db.CreateDbParameter("ModifierName", userInfo.Name));
                                command.Parameters.Add(db.CreateDbParameter("ModifyTime", DateTime.Now));
                                command.Parameters.Add(db.CreateDbParameter("Id", item.Id));
                                command.Parameters.Add(db.CreateDbParameter("OMSparePartMark", 0));
                                command.ExecuteNonQuery();
                            }
                            //增加履历
                            InsertPartsBranchHistoryByPartsIdOrList(db, rightList.Select(r => r.Id.ToString()).Distinct().ToArray(), null, conn, ts);
                        }
                        //无异常提交
                        ts.Commit();
                    } catch(Exception) {
                        //报错回滚
                        ts.Rollback();
                        throw;
                    } finally {
                        if(conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }

        /// <summary>
        /// 批量导入修改配件保修分类
        /// </summary>
        public bool 批量导入修改配件保修分类(string fileName, out int excelImportNum, out List<PartsBranchExtend> rightData, out List<PartsBranchExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<PartsBranchExtend>();
            var rightList = new List<PartsBranchExtend>();
            var allList = new List<PartsBranchExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("PartsBranch", out notNullableFields, out fieldLenght);
                List<object> excelColumns;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Code, "PartCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Partssalescategory_Name, "PartsSalesCategoryName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_PartsWarrantyCategoryNames, "PartsWarrantyCategoryName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_PartsWarrantyCategoryCode, "PartsWarrantyCategoryCode");
                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;

                    ////单次最大导入条数不能大于500！
                    //if(excelOperator.LastRowNum > 500) {
                    //    throw new Exception("单次最大导入条数不能大于500！");
                    //}

                    var keyValuePairs = new[] {
                        new KeyValuePair<string, string>("Status", "BaseData_Status")
                    };
                    tempExcelOperator.ImportHelper.LoadEnumFromDb(null, null, keyValuePairs);
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var tempImportObj = new PartsBranchExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);

                        #region
                        tempImportObj.PartsSalesCategoryNameStr = newRow["PartsSalesCategoryName"];
                        tempImportObj.PartsSalesCategoryName = newRow["PartsSalesCategoryName"];
                        tempImportObj.PartCodeStr = newRow["PartCode"];
                        tempImportObj.PartCode = newRow["PartCode"];
                        tempImportObj.PartsWarrantyCategoryName = newRow["PartsWarrantyCategoryName"];
                        tempImportObj.PartsWarrantyCategoryNameStr = newRow["PartsWarrantyCategoryName"];
                        tempImportObj.PartsWarrantyCategoryCode = newRow["PartsWarrantyCategoryCode"];

                        #endregion

                        var tempErrorMessage = new List<string>();

                        #region 导入的内容基本检查

                        //品牌检查
                        var fieldIndex = notNullableFields.IndexOf("PartsSalesCategoryName".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.PartsSalesCategoryNameStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_PartsSalescategoryNameIsNull);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.PartsSalesCategoryNameStr) > fieldLenght["PartsSalesCategoryName".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_PartsSalescategoryNameIsLong);
                        }
                        //配件编号
                        fieldIndex = notNullableFields.IndexOf("PartCode".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.PartCodeStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.OverstockPartsAppDetail__Validation1);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.PartCodeStr) > fieldLenght["PartCode".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.ImpPartsSupplierRelation_Validation8);
                        }
                        //配件保修分类名称
                        if(string.IsNullOrEmpty(tempImportObj.PartsWarrantyCategoryNameStr)) {
                            tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_PartsWarrantyCategoryNamesIsNull);
                        }
                        //配件保修分类编号
                        if(string.IsNullOrEmpty(tempImportObj.PartsWarrantyCategoryCode)) {
                            tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_PartsWarrantyCategoryCodeIsNull);
                        }

                        #endregion

                        if(tempErrorMessage.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });
                    rightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    #region 导入内容业务检查
                    var groups = rightList.GroupBy(r => new {
                        r.PartCodeStr,
                        r.PartsSalesCategoryNameStr
                    }).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        groupItem.ErrorMsg = ErrorStrings.Export_Validation_PartsBranch_PartsSalescategoryReport;
                    }
                    rightList = rightList.Where(r => r.ErrorMsg == null).ToList();

                    var partCodesNeedCheck = rightList.Select(r => r.PartCodeStr.ToUpper()).Distinct().ToArray();
                    var dbSparePartCodes = new List<PartsBranchExtend>();
                    Func<string[], bool> getDbSparePartCodes = value => {
                        var dbObj = new PartsBranchExtend {
                            PartId = Convert.ToInt32(value[0]),
                            PartCode = value[1],
                            PartName = value[2]
                        };
                        dbSparePartCodes.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator(@"select id,Trim(Code),Trim(Name) from sparePart where status=1 ", "Code", true, partCodesNeedCheck, getDbSparePartCodes);
                    foreach(var errorItem in rightList) {
                        var oldSparePart = dbSparePartCodes.FirstOrDefault(v => v.PartCode == errorItem.PartCodeStr);
                        if(oldSparePart == null) {
                            errorItem.ErrorMsg = String.Format(ErrorStrings.Export_Validation_PartsBranch_SparePartIsNull, errorItem.PartCodeStr);
                        } else {
                            errorItem.PartId = oldSparePart.PartId;
                            errorItem.PartName = oldSparePart.PartName;
                        }
                    }
                    rightList = rightList.Where(r => r.ErrorMsg == null).ToList();

                    var partsWarrantyNameNeedCheck = rightList.Where(r => r.PartsWarrantyCategoryName != null).Select(r => r.PartsWarrantyCategoryName.ToUpper()).Distinct().ToArray();
                    var dbPartsWarrantyNames = new List<PartsBranchExtend>();
                    Func<string[], bool> getDbPartsWarrantyCodes = value => {
                        var dbObj = new PartsBranchExtend {
                            PartsWarrantyCategoryId = Convert.ToInt32(value[0]),
                            PartsWarrantyCategoryCode = value[1],
                            PartsWarrantyCategoryName = value[2]
                        };
                        dbPartsWarrantyNames.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator(@" select Id,Code,Name  from PartsWarrantyCategory where status = 1 ", "Name", true, partsWarrantyNameNeedCheck, getDbPartsWarrantyCodes);
                    foreach(var errorItem in rightList) {
                        if(errorItem.PartsWarrantyCategoryName != null) {
                            var dbPartsWarranty = dbPartsWarrantyNames.FirstOrDefault(v => v.PartsWarrantyCategoryName == errorItem.PartsWarrantyCategoryName);
                            if(dbPartsWarranty == null) {
                                errorItem.ErrorMsg = String.Format(ErrorStrings.Export_Validation_PartsBranch_PartsWarrantyCategoryNameNotExist,errorItem.PartsWarrantyCategoryName);
                            } else {
                                if(errorItem.PartsWarrantyCategoryCode == dbPartsWarranty.PartsWarrantyCategoryCode) {
                                    errorItem.PartsWarrantyCategoryId = dbPartsWarranty.PartsWarrantyCategoryId;
                                    errorItem.PartsWarrantyCategoryCode = dbPartsWarranty.PartsWarrantyCategoryCode;
                                    errorItem.PartsWarrantyCategoryName = dbPartsWarranty.PartsWarrantyCategoryName;
                                    errorItem.PartsWarrantyCategoryNameStr = dbPartsWarranty.PartsWarrantyCategoryName;
                                } else {
                                    errorItem.ErrorMsg = String.Format(ErrorStrings.Export_Validation_PartsBranch_PartsWarrantyCategoryMatch, errorItem.PartsWarrantyCategoryName);
                                }

                            }
                        }
                    }
                    rightList = rightList.Where(r => r.ErrorMsg == null).ToList();

                    var partsSalesCategoryNeedCheck = rightList.Select(r => r.PartsSalesCategoryNameStr).Distinct().ToArray();
                    var dbPartsSalesCategories = new List<PartsBranchExtend>();
                    Func<string[], bool> getDbPartsSalesCategories = value => {
                        var dbObj = new PartsBranchExtend {
                            PartsSalesCategoryId = Convert.ToInt32(value[0]),
                            PartsSalesCategoryName = value[1],
                            BranchName = value[2],
                            BranchId = Convert.ToInt32(value[3])
                        };
                        dbPartsSalesCategories.Add(dbObj);
                        return false;
                    };
                    var userInfo = Utils.GetCurrentUserInfo();
                    db.QueryDataWithInOperator("select id,name,branchName,branchId from PartsSalesCategory where status=1 and BranchId = " + userInfo.EnterpriseId, "Name", true, partsSalesCategoryNeedCheck, getDbPartsSalesCategories);

                    foreach(var errorItem in rightList) {
                        var partsSalesCategory = dbPartsSalesCategories.FirstOrDefault(v => v.PartsSalesCategoryName == errorItem.PartsSalesCategoryNameStr);
                        if(partsSalesCategory == null) {
                            errorItem.ErrorMsg = ErrorStrings.Export_Title_PartsBranch_BranchName + errorItem.BranchNameStr + ErrorStrings.Export_Validation_PartsBranch_Validation1 + errorItem.PartsSalesCategoryNameStr + ErrorStrings.Export_Validation_PartsBranch_Validation2;
                        } else {
                            errorItem.PartsSalesCategoryId = partsSalesCategory.PartsSalesCategoryId;
                            errorItem.PartsSalesCategoryName = partsSalesCategory.PartsSalesCategoryName;
                        }
                    }
                    rightList = rightList.Where(r => r.ErrorMsg == null).ToList();
                    var partsBranchNeedCheck = rightList.Select(r => r.PartCodeStr.ToUpper()).Distinct().ToArray();
                    var dbPartsBranches = new List<PartsBranchExtend>();
                    Func<string[], bool> getDbPartsBranches = value => {
                        var dbObj = new PartsBranchExtend {
                            Id = int.Parse(value[0]),
                            PartsSalesCategoryName = value[1],
                            BranchName = value[2],
                            BranchId = int.Parse(value[3]),
                            PartCode = value[4]
                        };
                        dbPartsBranches.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator(@"select Id, partssalescategoryName, branchName,branchId, partcode from partsbranch  where status = 1 ", "PartCode", true, partsBranchNeedCheck, getDbPartsBranches);

                    foreach(var Item in rightList) {
                        var dbitem = dbPartsBranches.Where(v => v.PartsSalesCategoryName == Item.PartsSalesCategoryNameStr && v.BranchId == userInfo.EnterpriseId && v.PartCode == Item.PartCodeStr).FirstOrDefault();
                        if(dbitem == null) {
                            Item.ErrorMsg = ErrorStrings.Export_Validation_PartsBranch_Validation4;
                        } else {
                            Item.Id = dbitem.Id;
                        }
                    }

                    //获取所有不合格数据
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    //获取所有合格数据
                    rightList = allList.Except(errorList).ToList();
                    #endregion
                }

                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempObj = list[index - 1];
                            var values = new object[] {
                                tempObj.PartCodeStr, tempObj.PartsSalesCategoryNameStr, tempObj.PartsWarrantyCategoryCode,tempObj.PartsWarrantyCategoryName, tempObj.ErrorMsg
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }

                //更新导入所有合格数据
                if(!rightList.Any())
                    return true;
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    //开启事务，新增更新配件在一个事务内
                    var ts = conn.BeginTransaction();
                    try {
                        //修改配件
                        if(rightList.Any()) {
                            //获取修改数据的sql语句，Id为主键
                            var sqlUpdate = db.GetUpdateSql("PartsBranch", new[] {
                                "PartsWarrantyCategoryId", "PartsWarrantyCategoryCode", "PartsWarrantyCategoryName", "ModifierId", "ModifierName", "ModifyTime"
                            }, new[] {
                                "Id"
                            });
                            var userInfo = Utils.GetCurrentUserInfo();
                            //往数据库修改配件信息
                            foreach(var item in rightList) {
                                var command = db.CreateDbCommand(sqlUpdate, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("PartsWarrantyCategoryId", item.PartsWarrantyCategoryId));
                                command.Parameters.Add(db.CreateDbParameter("PartsWarrantyCategoryCode", item.PartsWarrantyCategoryCode));
                                command.Parameters.Add(db.CreateDbParameter("PartsWarrantyCategoryName", item.PartsWarrantyCategoryName));
                                command.Parameters.Add(db.CreateDbParameter("ModifierId", userInfo.Id));
                                command.Parameters.Add(db.CreateDbParameter("ModifierName", userInfo.Name));
                                command.Parameters.Add(db.CreateDbParameter("ModifyTime", DateTime.Now));
                                command.Parameters.Add(db.CreateDbParameter("Id", item.Id));
                                command.ExecuteNonQuery();
                            }
                            //增加履历
                            InsertPartsBranchHistoryByPartsIdOrList(db, rightList.Select(r => r.Id.ToString()).Distinct().ToArray(), null, conn, ts);
                        }
                        //无异常提交
                        ts.Commit();
                    } catch(Exception) {
                        //报错回滚
                        ts.Rollback();
                        throw;
                    } finally {
                        if(conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }

        //批量停用
        public bool 批量停用配件营销信息(string fileName, out int excelImportNum, out List<PartsBranchExtend> rightData, out List<PartsBranchExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<PartsBranchExtend>();
            var rightList = new List<PartsBranchExtend>();
            var allList = new List<PartsBranchExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("PartsBranch", out notNullableFields, out fieldLenght);
                List<object> excelColumns;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Code, "PartCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName, "PartName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Partssalescategory_Name, "PartsSalesCategoryName");
                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;

                    ////单次最大导入条数不能大于500！
                    //if(excelOperator.LastRowNum > 500) {
                    //    throw new Exception("单次最大导入条数不能大于500！");
                    //}

                    var keyValuePairs = new[] {
                        new KeyValuePair<string, string>("Status", "BaseData_Status")
                    };
                    tempExcelOperator.ImportHelper.LoadEnumFromDb(null, null, keyValuePairs);
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var tempImportObj = new PartsBranchExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);

                        #region
                        tempImportObj.PartCodeStr = newRow["PartCode"];
                        tempImportObj.PartCode = newRow["PartCode"];
                        tempImportObj.PartNameStr = newRow["PartName"];
                        tempImportObj.PartName = newRow["PartName"];
                        tempImportObj.PartsSalesCategoryNameStr = newRow["PartsSalesCategoryName"];
                        tempImportObj.PartsSalesCategoryName = newRow["PartsSalesCategoryName"];


                        #endregion

                        var tempErrorMessage = new List<string>();

                        #region 导入的内容基本检查

                        //品牌检查
                        var fieldIndex = notNullableFields.IndexOf("PartsSalesCategoryName".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.PartsSalesCategoryNameStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_PartsSalescategoryNameIsNull);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.PartsSalesCategoryNameStr) > fieldLenght["PartsSalesCategoryName".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_PartsSalescategoryNameIsLong);
                        }
                        //配件编号
                        fieldIndex = notNullableFields.IndexOf("PartCode".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.PartCodeStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.OverstockPartsAppDetail__Validation1);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.PartCodeStr) > fieldLenght["PartCode".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.ImpPartsSupplierRelation_Validation8);
                        }

                        #endregion

                        if(tempErrorMessage.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });
                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    #region 导入内容业务检查
                    var groups = tempRightList.GroupBy(r => new {
                        r.PartCodeStr,
                        r.PartsSalesCategoryNameStr
                    }).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        groupItem.ErrorMsg = ErrorStrings.Export_Validation_PartsBranch_PartsSalescategoryReport;
                    }
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();

                    var partCodesNeedCheck = tempRightList.Select(r => r.PartCodeStr.ToUpper()).Distinct().ToArray();
                    var dbSparePartCodes = new List<PartsBranchExtend>();
                    Func<string[], bool> getDbSparePartCodes = value => {
                        var dbObj = new PartsBranchExtend {
                            PartId = Convert.ToInt32(value[0]),
                            PartCode = value[1],
                            PartName = value[2]
                        };
                        dbSparePartCodes.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator(@"select id,Trim(Code),Trim(Name) from sparePart where status=1 ", "Code", true, partCodesNeedCheck, getDbSparePartCodes);
                    foreach(var errorItem in tempRightList) {
                        var oldSparePart = dbSparePartCodes.FirstOrDefault(v => v.PartCode == errorItem.PartCodeStr);
                        if(oldSparePart == null) {
                            errorItem.ErrorMsg = String.Format(ErrorStrings.Export_Validation_PartsBranch_SparePartIsNull, errorItem.PartCodeStr);
                        } else {
                            errorItem.PartId = oldSparePart.PartId;
                            errorItem.PartName = oldSparePart.PartName;
                        }
                    }
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();

                    var partsSalesCategoryNeedCheck = tempRightList.Select(r => r.PartsSalesCategoryNameStr).Distinct().ToArray();
                    var dbPartsSalesCategories = new List<PartsBranchExtend>();
                    Func<string[], bool> getDbPartsSalesCategories = value => {
                        var dbObj = new PartsBranchExtend {
                            PartsSalesCategoryId = Convert.ToInt32(value[0]),
                            PartsSalesCategoryName = value[1],
                            BranchName = value[2],
                            BranchId = Convert.ToInt32(value[3])
                        };
                        dbPartsSalesCategories.Add(dbObj);
                        return false;
                    };
                    var userInfo = Utils.GetCurrentUserInfo();
                    db.QueryDataWithInOperator("select id,name,branchName,branchId from PartsSalesCategory where status=1 and BranchId = " + userInfo.EnterpriseId, "Name", true, partsSalesCategoryNeedCheck, getDbPartsSalesCategories);
                    var errorPartsSalesCategories = tempRightList.Where(r => !dbPartsSalesCategories.Any(v => v.PartsSalesCategoryName == r.PartsSalesCategoryName && v.BranchId == userInfo.EnterpriseId)).ToList();
                    foreach(var errorItem in errorPartsSalesCategories) {
                        errorItem.ErrorMsg = ErrorStrings.Export_Title_PartsBranch_BranchName + errorItem.BranchNameStr + ErrorStrings.Export_Validation_PartsBranch_Validation1 + errorItem.PartsSalesCategoryNameStr + ErrorStrings.Export_Validation_PartsBranch_Validation2;
                    }
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();

                    var partsBranchNeedCheck = tempRightList.Select(r => r.PartCodeStr).Distinct().ToArray();
                    var dbPartsBranches = new List<PartsBranchExtend>();
                    Func<string[], bool> getDbPartsBranches = value => {
                        var dbObj = new PartsBranchExtend {
                            Id = int.Parse(value[0]),
                            PartsSalesCategoryName = value[1],
                            BranchName = value[2],
                            BranchId = int.Parse(value[3]),
                            PartCode = value[4],
                            Status = Convert.ToInt32(value[5])
                        };
                        dbPartsBranches.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator(@"select Id, partssalescategoryName, branchName,branchId, partcode,status from partsbranch  ", "PartCode", true, partsBranchNeedCheck, getDbPartsBranches);

                    foreach(var Item in tempRightList) {
                        var dbitem = dbPartsBranches.Where(v => v.PartsSalesCategoryName == Item.PartsSalesCategoryNameStr && v.BranchId == userInfo.EnterpriseId && v.PartCode == Item.PartCodeStr).FirstOrDefault();
                        if(dbitem == null) {
                            Item.ErrorMsg = ErrorStrings.Export_Validation_PartsBranch_PartsSalescategoryIsNull;
                        } else {
                            Item.Id = dbitem.Id;
                        }
                    }

                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();
                    var errorPartsBranches1 = tempRightList.Where(r => dbPartsBranches.Any(v => v.Status == (int)DcsMasterDataStatus.作废 && v.PartsSalesCategoryName == r.PartsSalesCategoryNameStr && v.BranchId == userInfo.EnterpriseId && v.PartCode == r.PartCodeStr)).ToList();
                    foreach(var errorItem in errorPartsBranches1) {
                        errorItem.ErrorMsg = ErrorStrings.Export_Validation_PartsBranch_Abandon;
                    }

                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();
                    var errorPartsBranches2 = tempRightList.Where(r => dbPartsBranches.Any(v => v.Status == (int)DcsMasterDataStatus.停用 && v.PartsSalesCategoryName == r.PartsSalesCategoryNameStr && v.BranchId == userInfo.EnterpriseId && v.PartCode == r.PartCodeStr)).ToList();
                    foreach(var errorItem in errorPartsBranches2) {
                        errorItem.ErrorMsg = ErrorStrings.Export_Validation_PartsBranch_Stop;
                    }



                    #endregion

                    //获取所有不合格数据
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    //获取所有合格数据
                    rightList = allList.Except(errorList).ToList();
                }

                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempObj = list[index - 1];
                            var values = new object[] {
                                tempObj.PartCodeStr, tempObj.PartNameStr, tempObj.PartsSalesCategoryNameStr, tempObj.ErrorMsg
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }

                //更新导入所有合格数据
                if(!rightList.Any())
                    return true;
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    //开启事务，新增更新配件在一个事务内
                    var ts = conn.BeginTransaction();
                    try {
                        //修改配件
                        if(rightList.Any()) {
                            //获取修改数据的sql语句，Id为主键
                            var sqlUpdate = db.GetUpdateSql("PartsBranch", new[] {
                                "Status", "ModifierId", "ModifierName", "ModifyTime","OMSparePartMark"
                            }, new[] {
                                "Id"
                            });
                            var userInfo = Utils.GetCurrentUserInfo();
                            //往数据库修改配件信息
                            foreach(var item in rightList) {
                                var command = db.CreateDbCommand(sqlUpdate, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("Status", (int)DcsMasterDataStatus.停用));
                                command.Parameters.Add(db.CreateDbParameter("ModifierId", userInfo.Id));
                                command.Parameters.Add(db.CreateDbParameter("ModifierName", userInfo.Name));
                                command.Parameters.Add(db.CreateDbParameter("ModifyTime", DateTime.Now));
                                command.Parameters.Add(db.CreateDbParameter("Id", item.Id));
                                command.Parameters.Add(db.CreateDbParameter("OMSparePartMark", 0));
                                command.ExecuteNonQuery();
                            }
                            //增加履历
                            InsertPartsBranchHistoryByPartsIdOrList(db, rightList.Select(r => r.Id.ToString()).Distinct().ToArray(), null, conn, ts);
                        }
                        //无异常提交
                        ts.Commit();
                    } catch(Exception) {
                        //报错回滚
                        ts.Rollback();
                        throw;
                    } finally {
                        if(conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }

        //批量恢复
        public bool 批量恢复配件营销信息(string fileName, out int excelImportNum, out List<PartsBranchExtend> rightData, out List<PartsBranchExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<PartsBranchExtend>();
            var rightList = new List<PartsBranchExtend>();
            var allList = new List<PartsBranchExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("PartsBranch", out notNullableFields, out fieldLenght);
                List<object> excelColumns;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Code, "PartCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName, "PartName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Partssalescategory_Name, "PartsSalesCategoryName");
                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;

                    ////单次最大导入条数不能大于500！
                    //if(excelOperator.LastRowNum > 500) {
                    //    throw new Exception("单次最大导入条数不能大于500！");
                    //}

                    var keyValuePairs = new[] {
                        new KeyValuePair<string, string>("Status", "MasterData_Status")
                    };
                    tempExcelOperator.ImportHelper.LoadEnumFromDb(null, null, keyValuePairs);
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var tempImportObj = new PartsBranchExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);

                        #region
                        tempImportObj.PartCodeStr = newRow["PartCode"];
                        tempImportObj.PartCode = newRow["PartCode"];
                        tempImportObj.PartNameStr = newRow["PartName"];
                        tempImportObj.PartName = newRow["PartName"];
                        tempImportObj.PartsSalesCategoryNameStr = newRow["PartsSalesCategoryName"];
                        tempImportObj.PartsSalesCategoryName = newRow["PartsSalesCategoryName"];


                        #endregion

                        var tempErrorMessage = new List<string>();

                        #region 导入的内容基本检查

                        //品牌检查
                        var fieldIndex = notNullableFields.IndexOf("PartsSalesCategoryName".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.PartsSalesCategoryNameStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_PartsSalescategoryNameIsNull);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.PartsSalesCategoryNameStr) > fieldLenght["PartsSalesCategoryName".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_PartsSalescategoryNameIsLong);
                        }
                        //配件编号
                        fieldIndex = notNullableFields.IndexOf("PartCode".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.PartCodeStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.OverstockPartsAppDetail__Validation1);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.PartCodeStr) > fieldLenght["PartCode".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.ImpPartsSupplierRelation_Validation8);
                        }

                        #endregion

                        if(tempErrorMessage.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });
                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    #region 导入内容业务检查
                    var groups = tempRightList.GroupBy(r => new {
                        r.PartCodeStr,
                        r.PartsSalesCategoryNameStr
                    }).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        groupItem.ErrorMsg = ErrorStrings.Export_Validation_PartsBranch_PartsSalescategoryReport;
                    }
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();

                    var partCodesNeedCheck = tempRightList.Select(r => r.PartCodeStr.ToUpper()).Distinct().ToArray();
                    var dbSparePartCodes = new List<PartsBranchExtend>();
                    Func<string[], bool> getDbSparePartCodes = value => {
                        var dbObj = new PartsBranchExtend {
                            PartId = Convert.ToInt32(value[0]),
                            PartCode = value[1],
                            PartName = value[2]
                        };
                        dbSparePartCodes.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator(@"select id,Trim(Code),Trim(Name) from sparePart where status=1 ", "Code", true, partCodesNeedCheck, getDbSparePartCodes);
                    foreach(var errorItem in tempRightList) {
                        var oldSparePart = dbSparePartCodes.FirstOrDefault(v => v.PartCode == errorItem.PartCodeStr);
                        if(oldSparePart == null) {
                            errorItem.ErrorMsg = String.Format(ErrorStrings.Export_Validation_PartsBranch_SparePartIsNull, errorItem.PartCodeStr);
                        } else {
                            errorItem.PartId = oldSparePart.PartId;
                            errorItem.PartName = oldSparePart.PartName;
                        }
                    }
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();

                    var partsSalesCategoryNeedCheck = tempRightList.Select(r => r.PartsSalesCategoryNameStr).Distinct().ToArray();
                    var dbPartsSalesCategories = new List<PartsBranchExtend>();
                    Func<string[], bool> getDbPartsSalesCategories = value => {
                        var dbObj = new PartsBranchExtend {
                            PartsSalesCategoryId = Convert.ToInt32(value[0]),
                            PartsSalesCategoryName = value[1],
                            BranchName = value[2],
                            BranchId = Convert.ToInt32(value[3])
                        };
                        dbPartsSalesCategories.Add(dbObj);
                        return false;
                    };
                    var userInfo = Utils.GetCurrentUserInfo();
                    db.QueryDataWithInOperator("select id,name,branchName,branchId from PartsSalesCategory where status=1 and BranchId = " + userInfo.EnterpriseId, "Name", true, partsSalesCategoryNeedCheck, getDbPartsSalesCategories);
                    var errorPartsSalesCategories = tempRightList.Where(r => !dbPartsSalesCategories.Any(v => v.PartsSalesCategoryName == r.PartsSalesCategoryName && v.BranchId == userInfo.EnterpriseId)).ToList();
                    foreach(var errorItem in errorPartsSalesCategories) {
                        errorItem.ErrorMsg = ErrorStrings.Export_Title_PartsBranch_BranchName + errorItem.BranchNameStr + ErrorStrings.Export_Validation_PartsBranch_Validation1 + errorItem.PartsSalesCategoryNameStr + ErrorStrings.Export_Validation_PartsBranch_Validation2;
                    }
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();

                    var partsBranchNeedCheck = tempRightList.Select(r => r.PartCodeStr).Distinct().ToArray();
                    var dbPartsBranches = new List<PartsBranchExtend>();
                    Func<string[], bool> getDbPartsBranches = value => {
                        var dbObj = new PartsBranchExtend {
                            Id = int.Parse(value[0]),
                            PartsSalesCategoryName = value[1],
                            BranchName = value[2],
                            BranchId = int.Parse(value[3]),
                            PartCode = value[4],
                            Status = Convert.ToInt32(value[5])
                        };
                        dbPartsBranches.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator(@"select Id, partssalescategoryName, branchName,branchId, partcode,status from partsbranch  ", "PartCode", true, partsBranchNeedCheck, getDbPartsBranches);

                    foreach(var Item in tempRightList) {
                        var dbitem = dbPartsBranches.Where(v => v.PartsSalesCategoryName == Item.PartsSalesCategoryNameStr && v.BranchId == userInfo.EnterpriseId && v.PartCode == Item.PartCodeStr).FirstOrDefault();
                        if(dbitem == null) {
                            Item.ErrorMsg = ErrorStrings.Export_Validation_PartsBranch_PartsSalescategoryIsNull;
                        } else {
                            Item.Id = dbitem.Id;
                        }
                    }

                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();
                    var errorPartsBranches1 = tempRightList.Where(r => dbPartsBranches.Any(v => v.Status == (int)DcsMasterDataStatus.作废 && v.PartsSalesCategoryName == r.PartsSalesCategoryNameStr && v.BranchId == userInfo.EnterpriseId && v.PartCode == r.PartCodeStr)).ToList();
                    foreach(var errorItem in errorPartsBranches1) {
                        errorItem.ErrorMsg = ErrorStrings.Export_Validation_PartsBranch_Abandon;
                    }

                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();
                    var errorPartsBranches2 = tempRightList.Where(r => dbPartsBranches.Any(v => v.Status == (int)DcsMasterDataStatus.有效 && v.PartsSalesCategoryName == r.PartsSalesCategoryNameStr && v.BranchId == userInfo.EnterpriseId && v.PartCode == r.PartCodeStr)).ToList();
                    foreach(var errorItem in errorPartsBranches2) {
                        errorItem.ErrorMsg = ErrorStrings.Export_Validation_PartsBranch_TakeEffect;
                    }



                    #endregion

                    //获取所有不合格数据
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    //获取所有合格数据
                    rightList = allList.Except(errorList).ToList();
                }

                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempObj = list[index - 1];
                            var values = new object[] {
                                tempObj.PartCodeStr, tempObj.PartNameStr, tempObj.PartsSalesCategoryNameStr, tempObj.ErrorMsg
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }

                //更新导入所有合格数据
                if(!rightList.Any())
                    return true;
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    //开启事务，新增更新配件在一个事务内
                    var ts = conn.BeginTransaction();
                    try {
                        //修改配件
                        if(rightList.Any()) {
                            //获取修改数据的sql语句，Id为主键
                            var sqlUpdate = db.GetUpdateSql("PartsBranch", new[] {
                                "Status", "ModifierId", "ModifierName", "ModifyTime","OMSparePartMark"
                            }, new[] {
                                "Id"
                            });
                            var userInfo = Utils.GetCurrentUserInfo();
                            //往数据库修改配件信息
                            foreach(var item in rightList) {
                                var command = db.CreateDbCommand(sqlUpdate, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("Status", (int)DcsMasterDataStatus.有效));
                                command.Parameters.Add(db.CreateDbParameter("ModifierId", userInfo.Id));
                                command.Parameters.Add(db.CreateDbParameter("ModifierName", userInfo.Name));
                                command.Parameters.Add(db.CreateDbParameter("ModifyTime", DateTime.Now));
                                command.Parameters.Add(db.CreateDbParameter("Id", item.Id));
                                command.Parameters.Add(db.CreateDbParameter("OMSparePartMark", 0));
                                command.ExecuteNonQuery();
                            }
                            //增加履历
                            InsertPartsBranchHistoryByPartsIdOrList(db, rightList.Select(r => r.Id.ToString()).Distinct().ToArray(), null, conn, ts);
                        }
                        //无异常提交
                        ts.Commit();
                    } catch(Exception) {
                        //报错回滚
                        ts.Rollback();
                        throw;
                    } finally {
                        if(conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }

        //批量作废
        public bool 批量作废配件营销信息(string fileName, out int excelImportNum, out List<PartsBranchExtend> rightData, out List<PartsBranchExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<PartsBranchExtend>();
            var rightList = new List<PartsBranchExtend>();
            var allList = new List<PartsBranchExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("PartsBranch", out notNullableFields, out fieldLenght);
                List<object> excelColumns;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Code, "PartCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName, "PartName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Partssalescategory_Name, "PartsSalesCategoryName");
                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;
                    var keyValuePairs = new[] {
                        new KeyValuePair<string, string>("Status", "BaseData_Status")
                    };
                    tempExcelOperator.ImportHelper.LoadEnumFromDb(null, null, keyValuePairs);
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var tempImportObj = new PartsBranchExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);

                        #region
                        tempImportObj.PartCodeStr = newRow["PartCode"];
                        tempImportObj.PartCode = newRow["PartCode"];
                        tempImportObj.PartNameStr = newRow["PartName"];
                        tempImportObj.PartName = newRow["PartName"];
                        tempImportObj.PartsSalesCategoryNameStr = newRow["PartsSalesCategoryName"];
                        tempImportObj.PartsSalesCategoryName = newRow["PartsSalesCategoryName"];


                        #endregion

                        var tempErrorMessage = new List<string>();

                        #region 导入的内容基本检查

                        //品牌检查
                        var fieldIndex = notNullableFields.IndexOf("PartsSalesCategoryName".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.PartsSalesCategoryNameStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_PartsSalescategoryNameIsNull);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.PartsSalesCategoryNameStr) > fieldLenght["PartsSalesCategoryName".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_PartsSalescategoryNameIsLong);
                        }
                        //配件编号
                        fieldIndex = notNullableFields.IndexOf("PartCode".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.PartCodeStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.OverstockPartsAppDetail__Validation1);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.PartCodeStr) > fieldLenght["PartCode".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.ImpPartsSupplierRelation_Validation8);
                        }

                        #endregion

                        if(tempErrorMessage.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });
                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    #region 导入内容业务检查
                    var groups = tempRightList.GroupBy(r => new {
                        r.PartCodeStr,
                        r.PartsSalesCategoryNameStr
                    }).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        groupItem.ErrorMsg = ErrorStrings.Export_Validation_PartsBranch_PartsSalescategoryReport;
                    }
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();

                    var partCodesNeedCheck = tempRightList.Select(r => r.PartCodeStr.ToUpper()).Distinct().ToArray();
                    var dbSparePartCodes = new List<PartsBranchExtend>();
                    Func<string[], bool> getDbSparePartCodes = value => {
                        var dbObj = new PartsBranchExtend {
                            PartId = Convert.ToInt32(value[0]),
                            PartCode = value[1],
                            PartName = value[2]
                        };
                        dbSparePartCodes.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator(@"select id,Trim(Code),Trim(Name) from sparePart where status=1 ", "Code", true, partCodesNeedCheck, getDbSparePartCodes);
                    foreach(var errorItem in tempRightList) {
                        var oldSparePart = dbSparePartCodes.FirstOrDefault(v => v.PartCode == errorItem.PartCodeStr);
                        if(oldSparePart == null) {
                            errorItem.ErrorMsg = String.Format(ErrorStrings.Export_Validation_PartsBranch_SparePartIsNull, errorItem.PartCodeStr);
                        } else {
                            errorItem.PartId = oldSparePart.PartId;
                            errorItem.PartName = oldSparePart.PartName;
                        }
                    }
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();

                    var partsSalesCategoryNeedCheck = tempRightList.Select(r => r.PartsSalesCategoryNameStr).Distinct().ToArray();
                    var dbPartsSalesCategories = new List<PartsBranchExtend>();
                    Func<string[], bool> getDbPartsSalesCategories = value => {
                        var dbObj = new PartsBranchExtend {
                            PartsSalesCategoryId = Convert.ToInt32(value[0]),
                            PartsSalesCategoryName = value[1],
                            BranchName = value[2],
                            BranchId = Convert.ToInt32(value[3])
                        };
                        dbPartsSalesCategories.Add(dbObj);
                        return false;
                    };
                    var userInfo = Utils.GetCurrentUserInfo();
                    db.QueryDataWithInOperator("select id,name,branchName,branchId from PartsSalesCategory where status=1 and BranchId = " + userInfo.EnterpriseId, "Name", true, partsSalesCategoryNeedCheck, getDbPartsSalesCategories);
                    var errorPartsSalesCategories = tempRightList.Where(r => !dbPartsSalesCategories.Any(v => v.PartsSalesCategoryName == r.PartsSalesCategoryName && v.BranchId == userInfo.EnterpriseId)).ToList();
                    foreach(var errorItem in errorPartsSalesCategories) {
                        errorItem.ErrorMsg = ErrorStrings.Export_Title_PartsBranch_BranchName + errorItem.BranchNameStr + ErrorStrings.Export_Validation_PartsBranch_Validation1 + errorItem.PartsSalesCategoryNameStr + ErrorStrings.Export_Validation_PartsBranch_Validation2;
                    }
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();

                    var partsBranchNeedCheck = tempRightList.Select(r => r.PartCodeStr).Distinct().ToArray();
                    var dbPartsBranches = new List<PartsBranchExtend>();
                    Func<string[], bool> getDbPartsBranches = value => {
                        var dbObj = new PartsBranchExtend {
                            Id = int.Parse(value[0]),
                            PartsSalesCategoryName = value[1],
                            BranchName = value[2],
                            BranchId = int.Parse(value[3]),
                            PartCode = value[4],
                            Status = Convert.ToInt32(value[5])
                        };
                        dbPartsBranches.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator(@"select Id, partssalescategoryName, branchName,branchId, partcode,status from partsbranch  ", "PartCode", true, partsBranchNeedCheck, getDbPartsBranches);

                    foreach(var Item in tempRightList) {
                        var dbitem = dbPartsBranches.Where(v => v.PartsSalesCategoryName == Item.PartsSalesCategoryNameStr && v.BranchId == userInfo.EnterpriseId && v.PartCode == Item.PartCodeStr).FirstOrDefault();
                        if(dbitem == null) {
                            Item.ErrorMsg = ErrorStrings.Export_Validation_PartsBranch_PartsSalescategoryIsNull;
                        } else {
                            Item.Id = dbitem.Id;
                        }
                    }

                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();
                    var errorPartsBranches1 = tempRightList.Where(r => dbPartsBranches.Any(v => v.Status == (int)DcsMasterDataStatus.作废 && v.PartsSalesCategoryName == r.PartsSalesCategoryNameStr && v.BranchId == userInfo.EnterpriseId && v.PartCode == r.PartCodeStr)).ToList();
                    foreach(var errorItem in errorPartsBranches1) {
                        errorItem.ErrorMsg = ErrorStrings.Export_Validation_PartsBranch_Abandon;
                    }

                    #endregion

                    //获取所有不合格数据
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    //获取所有合格数据
                    rightList = allList.Except(errorList).ToList();
                }

                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempObj = list[index - 1];
                            var values = new object[] {
                                tempObj.PartCodeStr, tempObj.PartNameStr, tempObj.PartsSalesCategoryNameStr, tempObj.ErrorMsg
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }

                //更新导入所有合格数据
                if(!rightList.Any())
                    return true;
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    //开启事务，新增更新配件在一个事务内
                    var ts = conn.BeginTransaction();
                    try {
                        //修改配件
                        if(rightList.Any()) {
                            //获取修改数据的sql语句，Id为主键
                            var sqlUpdate = db.GetUpdateSql("PartsBranch", new[] {
                                "Status", "ModifierId", "ModifierName", "ModifyTime","OMSparePartMark"
                            }, new[] {
                                "Id"
                            });
                            var userInfo = Utils.GetCurrentUserInfo();
                            //往数据库修改配件信息
                            foreach(var item in rightList) {
                                var command = db.CreateDbCommand(sqlUpdate, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("Status", (int)DcsMasterDataStatus.作废));
                                command.Parameters.Add(db.CreateDbParameter("ModifierId", userInfo.Id));
                                command.Parameters.Add(db.CreateDbParameter("ModifierName", userInfo.Name));
                                command.Parameters.Add(db.CreateDbParameter("ModifyTime", DateTime.Now));
                                command.Parameters.Add(db.CreateDbParameter("OMSparePartMark", 0));
                                command.Parameters.Add(db.CreateDbParameter("Id", item.Id));
                                command.ExecuteNonQuery();
                            }
                            //增加履历
                            InsertPartsBranchHistoryByPartsIdOrList(db, rightList.Select(r => r.Id.ToString()).Distinct().ToArray(), null, conn, ts);
                        }
                        //无异常提交
                        ts.Commit();

                    } catch(Exception) {
                        //报错回滚
                        ts.Rollback();
                        throw;
                    } finally {
                        if(conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }

        /// <summary>
        /// 配件营销信息加履历
        /// </summary>
        /// <param name="db"></param>
        /// <param name="partsIds">需要增加履历的营销信息Id</param>
        /// <param name="rightData">需要增加履历的营销信息集合</param>
        /// <param name="conn">当前事物数据库连接</param>
        /// <param name="ts">当前事物对象</param>
        private void InsertPartsBranchHistoryByPartsIdOrList(DbHelper db, string[] partsIds, List<PartsBranchExtend> rightData, DbConnection conn, DbTransaction ts) {
            if(partsIds != null) {
                rightData = new List<PartsBranchExtend>();
                Func<string[], bool> fillPartsBranches = value => {
                    var dbObj = new PartsBranchExtend {
                        Id = int.Parse(value[0]),
                        PartId = int.Parse(value[1]),
                        PartCode = value[2],
                        PartName = value[3],
                        ReferenceCode = value[4],
                        StockMaximum = string.IsNullOrEmpty(value[5]) ? 0 : Convert.ToInt32(value[5]),
                        StockMinimum = string.IsNullOrEmpty(value[6]) ? 0 : Convert.ToInt32(value[6]),
                        BranchId = string.IsNullOrEmpty(value[7].ToString()) ? 0 : Convert.ToInt32(value[7]),
                        BranchName = value[8],
                        PartsSalesCategoryId = string.IsNullOrEmpty(value[9]) ? 0 : Convert.ToInt32(value[9]),
                        PartsSalesCategoryName = value[10],
                        ABCStrategyId = string.IsNullOrEmpty(value[11]) ? 0 : Convert.ToInt32(value[11]),
                        ProductLifeCycle = string.IsNullOrEmpty(value[12]) ? 0 : Convert.ToInt32(value[12]),
                        LossType = string.IsNullOrEmpty(value[13]) ? 0 : Convert.ToInt32(value[13]),
                        IsOrderable = string.IsNullOrEmpty(value[14]) || value[14] == "0" ? false : true,
                        PurchaseCycle = string.IsNullOrEmpty(value[15]) ? 0 : Convert.ToInt32(value[15]),
                        IsService = string.IsNullOrEmpty(value[16]) || value[16] == "0" ? false : true,
                        IsSalable = string.IsNullOrEmpty(value[17]) || value[17] == "0" ? false : true,
                        PartsReturnPolicy = string.IsNullOrEmpty(value[18]) ? 0 : Convert.ToInt32(value[18]),
                        IsDirectSupply = string.IsNullOrEmpty(value[19]) || value[19] == "0" ? false : true,
                        WarrantySupplyStatus = string.IsNullOrEmpty(value[20]) ? 0 : Convert.ToInt32(value[20]),
                        MinSaleQuantity = string.IsNullOrEmpty(value[21]) ? 0 : Convert.ToInt32(value[21]),
                        PartABC = string.IsNullOrEmpty(value[22]) ? 0 : Convert.ToInt32(value[22]),
                        PurchaseRoute = string.IsNullOrEmpty(value[23]) ? 0 : Convert.ToInt32(value[23]),
                        RepairMatMinUnit = string.IsNullOrEmpty(value[24]) || value[24] == "0" ? false : true,
                        PartsWarhouseManageGranularity = string.IsNullOrEmpty(value[25]) ? 0 : Convert.ToInt32(value[25]),
                        Status = string.IsNullOrEmpty(value[26]) ? 0 : Convert.ToInt32(value[26]),
                        Remark = value[27],
                        IncreaseRateGroupId = string.IsNullOrEmpty(value[28]) ? 0 : Convert.ToInt32(value[28]),
                        PartsAttribution = string.IsNullOrEmpty(value[29]) ? 0 : Convert.ToInt32(value[29]),
                        PartsMaterialManageCost = string.IsNullOrEmpty(value[30]) ? 0 : Convert.ToDecimal(value[30]),
                        PartsWarrantyLong = string.IsNullOrEmpty(value[31]) ? 0 : Convert.ToInt32(value[31]),
                        AutoApproveUpLimit = string.IsNullOrEmpty(value[32]) ? 0 : Convert.ToInt32(value[32]),
                    };
                    rightData.Add(dbObj);
                    return false;
                };
                db.QueryDataWithInOperator(@"select ID,PARTID,PARTCODE,PARTNAME,REFERENCECODE,STOCKMAXIMUM,STOCKMINIMUM,BRANCHID,BRANCHNAME,PARTSSALESCATEGORYID,PARTSSALESCATEGORYNAME,ABCSTRATEGYID,PRODUCTLIFECYCLE,LOSSTYPE,ISORDERABLE,PURCHASECYCLE,ISSERVICE,ISSALABLE,PARTSRETURNPOLICY,ISDIRECTSUPPLY,WARRANTYSUPPLYSTATUS,MINSALEQUANTITY,PARTABC,PURCHASEROUTE,REPAIRMATMINUNIT,PARTSWARHOUSEMANAGEGRANULARITY,
                                                         STATUS,REMARK,INCREASERATEGROUPID,PARTSATTRIBUTION,PartsMaterialManageCost,PartsWarrantyLong,AutoApproveUpLimit from partsbranch where 1 = 1 ", "Id", true, partsIds, fillPartsBranches, ts, conn);
            }
            foreach(var partsBranchExtend in rightData) {
                //加履历
                InsertPartsBranchHistory(partsBranchExtend, db, conn, ts);
            }
        }

        /// <summary>
        /// 配件营销信息加履历
        /// </summary>
        /// <param name="partsBranchExtend">需要生成履历的营销信息对象</param>
        /// <param name="db"></param>
        /// <param name="conn">当前事物数据库连接</param>
        /// <param name="ts">当前事物对象</param>
        private void InsertPartsBranchHistory(PartsBranchExtend partsBranchExtend, DbHelper db, DbConnection conn, DbTransaction ts) {
            var sqlHistoryInsert = db.GetInsertSql("PartsBranchHistory", "Id", new[] {
                "PartsBranchId", "PartId", "PartCode", "PartName", "ReferenceCode", "StockMaximum", "StockMinimum", "BranchId", "BranchName", "PartsSalesCategoryId", "PartsSalesCategoryName", "ProductLifeCycle", "LossType", "PartsAttribution", "IsOrderable", "IsSalable", "PartsReturnPolicy", "INCREASERATEGROUPID", "IsDirectSupply", "MinSaleQuantity", "PartABC", "PartsMaterialManageCost", "PartsWarrantyLong", "AutoApproveUpLimit", "Status", "ABCStrategyId", "PurchaseCycle", "IsService", "WarrantySupplyStatus", "PurchaseRoute", "PartsWarhouseManageGranularity", "RepairMatMinUnit", "Remark", "CreatorId", "CreatorName", "CreateTime"
            });
            var userInfo = Utils.GetCurrentUserInfo();
            var commandHistory = db.CreateDbCommand(sqlHistoryInsert, conn, ts);
            commandHistory.Parameters.Add(db.CreateDbParameter("PartsBranchId", partsBranchExtend.Id));
            commandHistory.Parameters.Add(db.CreateDbParameter("PartId", partsBranchExtend.PartId));
            commandHistory.Parameters.Add(db.CreateDbParameter("PartCode", partsBranchExtend.PartCode));
            commandHistory.Parameters.Add(db.CreateDbParameter("PartName", partsBranchExtend.PartName));
            commandHistory.Parameters.Add(db.CreateDbParameter("ReferenceCode", partsBranchExtend.ReferenceCode));
            commandHistory.Parameters.Add(db.CreateDbParameter("StockMaximum", partsBranchExtend.StockMaximum));
            commandHistory.Parameters.Add(db.CreateDbParameter("StockMinimum", partsBranchExtend.StockMinimum));
            commandHistory.Parameters.Add(db.CreateDbParameter("BranchId", partsBranchExtend.BranchId));
            commandHistory.Parameters.Add(db.CreateDbParameter("BranchName", partsBranchExtend.BranchName));
            commandHistory.Parameters.Add(db.CreateDbParameter("PartsSalesCategoryId", partsBranchExtend.PartsSalesCategoryId));
            commandHistory.Parameters.Add(db.CreateDbParameter("PartsSalesCategoryName", partsBranchExtend.PartsSalesCategoryName));
            commandHistory.Parameters.Add(db.CreateDbParameter("ProductLifeCycle", partsBranchExtend.ProductLifeCycle));
            commandHistory.Parameters.Add(db.CreateDbParameter("LossType", partsBranchExtend.LossType));
            commandHistory.Parameters.Add(db.CreateDbParameter("PartsAttribution", partsBranchExtend.PartsAttribution));
            commandHistory.Parameters.Add(db.CreateDbParameter("IsOrderable", partsBranchExtend.IsOrderable));
            commandHistory.Parameters.Add(db.CreateDbParameter("IsSalable", partsBranchExtend.IsSalable));
            commandHistory.Parameters.Add(db.CreateDbParameter("PartsReturnPolicy", partsBranchExtend.PartsReturnPolicy));
            commandHistory.Parameters.Add(db.CreateDbParameter("INCREASERATEGROUPID", partsBranchExtend.IncreaseRateGroupId));
            commandHistory.Parameters.Add(db.CreateDbParameter("IsDirectSupply", partsBranchExtend.IsDirectSupply));
            commandHistory.Parameters.Add(db.CreateDbParameter("MinSaleQuantity", partsBranchExtend.MinSaleQuantity));
            commandHistory.Parameters.Add(db.CreateDbParameter("PartABC", partsBranchExtend.PartABC));
            commandHistory.Parameters.Add(db.CreateDbParameter("PartsMaterialManageCost", partsBranchExtend.PartsMaterialManageCost));
            commandHistory.Parameters.Add(db.CreateDbParameter("PartsWarrantyLong", partsBranchExtend.PartsWarrantyLong));
            commandHistory.Parameters.Add(db.CreateDbParameter("AutoApproveUpLimit", partsBranchExtend.AutoApproveUpLimit));
            commandHistory.Parameters.Add(db.CreateDbParameter("ABCStrategyId", partsBranchExtend.ABCStrategyId));
            commandHistory.Parameters.Add(db.CreateDbParameter("PurchaseCycle", partsBranchExtend.PurchaseCycle));
            commandHistory.Parameters.Add(db.CreateDbParameter("IsService", partsBranchExtend.IsService));
            commandHistory.Parameters.Add(db.CreateDbParameter("WarrantySupplyStatus", partsBranchExtend.WarrantySupplyStatus));
            commandHistory.Parameters.Add(db.CreateDbParameter("PurchaseRoute", partsBranchExtend.PurchaseRoute));
            commandHistory.Parameters.Add(db.CreateDbParameter("PartsWarhouseManageGranularity", partsBranchExtend.PartsWarhouseManageGranularity));
            commandHistory.Parameters.Add(db.CreateDbParameter("RepairMatMinUnit", partsBranchExtend.RepairMatMinUnit));
            commandHistory.Parameters.Add(db.CreateDbParameter("Remark", partsBranchExtend.Remark));
            commandHistory.Parameters.Add(db.CreateDbParameter("Status", partsBranchExtend.Status));
            commandHistory.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
            commandHistory.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
            commandHistory.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
            commandHistory.ExecuteNonQuery();
        }
        /// <summary>
        /// 批量替换可否采购
        /// </summary>
        public bool 批量导入配件ABC类型(string fileName, out int excelImportNum, out List<PartsBranchExtend> rightData, out List<PartsBranchExtend> errorData, out string errorDataFileName, out string errorMessage)
        {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<PartsBranchExtend>();
            var rightList = new List<PartsBranchExtend>();
            var allList = new List<PartsBranchExtend>();
            try
            {
                if (!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("PartsBranch", out notNullableFields, out fieldLenght);
                List<object> excelColumns;
                using (var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString))
                {
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Code, "PartCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Partssalescategory_Name, "PartsSalesCategoryName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_PartABC, "PartABC");
                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;

                    ////单次最大导入条数不能大于500！
                    //if(excelOperator.LastRowNum > 500) {
                    //    throw new Exception("单次最大导入条数不能大于500！");
                    //}

                    var keyValuePairs = new[] {
                        new KeyValuePair<string, string>("PartABC", "ABCStrategy_Category")
                    };
                    tempExcelOperator.ImportHelper.LoadEnumFromDb(null, null, keyValuePairs);
                    excelImportNum = excelOperator.LoadExcelRow(row =>
                    {
                        var tempImportObj = new PartsBranchExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);

                        #region
                        tempImportObj.PartsSalesCategoryNameStr = newRow["PartsSalesCategoryName"];
                        tempImportObj.PartsSalesCategoryName = newRow["PartsSalesCategoryName"];
                        tempImportObj.PartCodeStr = newRow["PartCode"];
                        tempImportObj.PartCode = newRow["PartCode"];
                        tempImportObj.PartABCStr = newRow["PartABC"];

                        #endregion

                        var tempErrorMessage = new List<string>();

                        #region 导入的内容基本检查

                        //品牌检查
                        var fieldIndex = notNullableFields.IndexOf("PartsSalesCategoryName".ToUpper());
                        if (string.IsNullOrEmpty(tempImportObj.PartsSalesCategoryNameStr))
                        {
                            if (fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_PartsSalescategoryNameIsNull);
                        }
                        else
                        {
                            if (Encoding.Default.GetByteCount(tempImportObj.PartsSalesCategoryNameStr) > fieldLenght["PartsSalesCategoryName".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_PartsSalescategoryNameIsLong);
                        }
                        //配件编号
                        fieldIndex = notNullableFields.IndexOf("PartCode".ToUpper());
                        if (string.IsNullOrEmpty(tempImportObj.PartCodeStr))
                        {
                            if (fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.OverstockPartsAppDetail__Validation1);
                        }
                        else
                        {
                            if (Encoding.Default.GetByteCount(tempImportObj.PartCodeStr) > fieldLenght["PartCode".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.ImpPartsSupplierRelation_Validation8);
                        }
                        //配件ABC分类
                        fieldIndex = notNullableFields.IndexOf("PartABC".ToUpper());
                        if (string.IsNullOrEmpty(tempImportObj.PartABCStr))
                        {
                            if (fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_PartsABCIsNull);
                        }
                        else
                        {
                            var tempEnumValue = tempExcelOperator.ImportHelper.GetEnumValue("PartABC", tempImportObj.PartABCStr.ToLower());
                            if (!tempEnumValue.HasValue)
                            {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_PartsABCValueError);
                            }
                            else
                            {
                                tempImportObj.PartABC = tempEnumValue.Value;
                            }
                        }

                        #endregion

                        if (tempErrorMessage.Count > 0)
                        {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });
                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    #region 导入内容业务检查
                    var groups = tempRightList.GroupBy(r => new
                    {
                        r.PartCodeStr,
                        r.PartsSalesCategoryNameStr
                    }).Where(r => r.Count() > 1);
                    foreach (var groupItem in groups.SelectMany(r => r))
                    {
                        groupItem.ErrorMsg = ErrorStrings.Export_Validation_PartsBranch_PartsSalescategoryReport;
                    }
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();

                    var partCodesNeedCheck = tempRightList.Select(r => r.PartCodeStr.ToUpper()).Distinct().ToArray();
                    var dbSparePartCodes = new List<PartsBranchExtend>();
                    Func<string[], bool> getDbSparePartCodes = value =>
                    {
                        var dbObj = new PartsBranchExtend
                        {
                            PartId = Convert.ToInt32(value[0]),
                            PartCode = value[1],
                            PartName = value[2]
                        };
                        dbSparePartCodes.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator(@"select id,Trim(Code),Trim(Name) from sparePart where status=1 ", "Code", true, partCodesNeedCheck, getDbSparePartCodes);
                    foreach (var errorItem in tempRightList)
                    {
                        var oldSparePart = dbSparePartCodes.FirstOrDefault(v => v.PartCode == errorItem.PartCodeStr);
                        if (oldSparePart == null)
                        {
                            errorItem.ErrorMsg = String.Format(ErrorStrings.Export_Validation_PartsBranch_SparePartIsNull, errorItem.PartCodeStr);
                        }
                        else
                        {
                            errorItem.PartId = oldSparePart.PartId;
                            errorItem.PartName = oldSparePart.PartName;
                        }
                    }
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();

                    var partsSalesCategoryNeedCheck = tempRightList.Select(r => r.PartsSalesCategoryNameStr).Distinct().ToArray();
                    var dbPartsSalesCategories = new List<PartsBranchExtend>();
                    Func<string[], bool> getDbPartsSalesCategories = value =>
                    {
                        var dbObj = new PartsBranchExtend
                        {
                            PartsSalesCategoryId = Convert.ToInt32(value[0]),
                            PartsSalesCategoryName = value[1],
                            BranchName = value[2],
                            BranchId = Convert.ToInt32(value[3])
                        };
                        dbPartsSalesCategories.Add(dbObj);
                        return false;
                    };
                    var userInfo = Utils.GetCurrentUserInfo();
                    db.QueryDataWithInOperator("select id,name,branchName,branchId from PartsSalesCategory where status=1 and BranchId = " + userInfo.EnterpriseId, "Name", true, partsSalesCategoryNeedCheck, getDbPartsSalesCategories);
                    var errorPartsSalesCategories = tempRightList.Where(r => !dbPartsSalesCategories.Any(v => v.PartsSalesCategoryName == r.PartsSalesCategoryName && v.BranchId == userInfo.EnterpriseId)).ToList();
                    foreach (var errorItem in errorPartsSalesCategories)
                    {
                        errorItem.ErrorMsg = ErrorStrings.Export_Title_PartsBranch_BranchName + errorItem.BranchNameStr + ErrorStrings.Export_Validation_PartsBranch_Validation1 + errorItem.PartsSalesCategoryNameStr + ErrorStrings.Export_Validation_PartsBranch_Validation2;
                    }
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();

                    var partsBranchNeedCheck = tempRightList.Select(r => r.PartCodeStr.ToUpper()).Distinct().ToArray();
                    var dbPartsBranches = new List<PartsBranchExtend>();
                    Func<string[], bool> getDbPartsBranches = value =>
                    {
                        var dbObj = new PartsBranchExtend
                        {
                            Id = int.Parse(value[0]),
                            PartsSalesCategoryName = value[1],
                            BranchName = value[2],
                            BranchId = int.Parse(value[3]),
                            PartCode = value[4]
                        };
                        dbPartsBranches.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator(@"select Id, partssalescategoryName, branchName,branchId, partcode from partsbranch  where status = 1 ", "PartCode", true, partsBranchNeedCheck, getDbPartsBranches);

                    foreach (var Item in tempRightList)
                    {
                        var dbitem = dbPartsBranches.Where(v => v.PartsSalesCategoryName == Item.PartsSalesCategoryNameStr && v.BranchId == userInfo.EnterpriseId && v.PartCode == Item.PartCodeStr).FirstOrDefault();
                        if (dbitem == null)
                        {
                            Item.ErrorMsg = ErrorStrings.Export_Validation_PartsBranch_PartsSalescategoryIsNull;
                        }
                        else
                        {
                            Item.Id = dbitem.Id;
                        }
                    }
                    #endregion

                    //获取所有不合格数据
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    //获取所有合格数据
                    rightList = allList.Except(errorList).ToList();
                }


                //导出所有不合格数据
                if (errorList.Any())
                {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using (var excelExport = new ExcelExport(errorDataFileName))
                    {
                        var list = errorList;
                        excelExport.ExportByRow(index =>
                        {
                            if (index == list.Count + 1)
                                return null;
                            if (index == 0)
                                return excelColumns.ToArray();
                            var tempObj = list[index - 1];
                            var values = new object[] {
                                tempObj.PartCodeStr, tempObj.PartsSalesCategoryNameStr, tempObj.PartABCStr, tempObj.ErrorMsg
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }

                //更新导入所有合格数据
                if (!rightList.Any())
                    return true;
                using (var conn = db.CreateDbConnection())
                {
                    conn.Open();
                    //开启事务，新增更新配件在一个事务内
                    var ts = conn.BeginTransaction();
                    try
                    {
                        //修改配件
                        if (rightList.Any())
                        {
                            //获取修改数据的sql语句，Id为主键
                            var sqlUpdate = db.GetUpdateSql("PartsBranch", new[] {
                                "PartABC", "ModifierId", "ModifierName", "ModifyTime","OMSparePartMark"
                            }, new[] {
                                "Id"
                            });
                            var userInfo = Utils.GetCurrentUserInfo();
                            //往数据库修改配件信息
                            foreach (var item in rightList)
                            {
                                var command = db.CreateDbCommand(sqlUpdate, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("PartABC", item.PartABC));
                                command.Parameters.Add(db.CreateDbParameter("ModifierId", userInfo.Id));
                                command.Parameters.Add(db.CreateDbParameter("ModifierName", userInfo.Name));
                                command.Parameters.Add(db.CreateDbParameter("ModifyTime", DateTime.Now));
                                command.Parameters.Add(db.CreateDbParameter("Id", item.Id));
                                command.Parameters.Add(db.CreateDbParameter("OMSparePartMark", 0));
                                command.ExecuteNonQuery();
                            }
                            //增加履历
                            InsertPartsBranchHistoryByPartsIdOrList(db, rightList.Select(r => r.Id.ToString()).Distinct().ToArray(), null, conn, ts);
                        }
                        //无异常提交
                        ts.Commit();
                    }
                    catch (Exception)
                    {
                        //报错回滚
                        ts.Rollback();
                        throw;
                    }
                    finally
                    {
                        if (conn.State == System.Data.ConnectionState.Open)
                        {
                            conn.Close();
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            }
            finally
            {
                errorData = errorList;
                rightData = rightList;
            }
        }
        public bool 批量替换库存低限(string fileName, out int excelImportNum, out List<PartsBranchExtend> rightData, out List<PartsBranchExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<PartsBranchExtend>();
            var rightList = new List<PartsBranchExtend>();
            var allList = new List<PartsBranchExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("PartsBranch", out notNullableFields, out fieldLenght);
                List<object> excelColumns;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Code, "PartCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Name, "PartName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_StockMinimum, "StockMinimum");
                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;

                    ////单次最大导入条数不能大于500！
                    //if(excelOperator.LastRowNum > 500) {
                    //    throw new Exception("单次最大导入条数不能大于500！");
                    //}

                 
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var tempImportObj = new PartsBranchExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);

                        #region
                        tempImportObj.PartCodeStr = newRow["PartCode"];
                        tempImportObj.PartCode = newRow["PartCode"];
                        tempImportObj.PartName = newRow["PartName"];
                        tempImportObj.PartNameStr = newRow["PartName"];
                        tempImportObj.StockMinimumStr = newRow["StockMinimum"];
                        #endregion

                        var tempErrorMessage = new List<string>();

                        #region 导入的内容基本检查

                        //配件编号
                        var fieldIndex = notNullableFields.IndexOf("PartCode".ToUpper());                                              
                        if(string.IsNullOrEmpty(tempImportObj.PartCodeStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.OverstockPartsAppDetail__Validation1);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.PartCodeStr) > fieldLenght["PartCode".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.ImpPartsSupplierRelation_Validation8);
                        }

                        //批量审核上限
                        if(!string.IsNullOrEmpty(tempImportObj.StockMinimumStr)) {
                            int checkValue;
                            if(int.TryParse(tempImportObj.StockMinimumStr, out checkValue)) {
                                if(checkValue < 0) {
                                    tempErrorMessage.Add("库存低限必须大于等于0");
                                } else
                                    tempImportObj.StockMinimum = checkValue;
                            } else {
                                tempErrorMessage.Add("库存低限必须是数字");
                            }
                        } else {
                            tempErrorMessage.Add("库存低限必填");
                        }

                        #endregion

                        if(tempErrorMessage.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });
                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    #region 导入内容业务检查
                   
                    var partCodesNeedCheck = tempRightList.Select(r => r.PartCodeStr.ToUpper()).Distinct().ToArray();
                    var dbSparePartCodes = new List<PartsBranchExtend>();
                    Func<string[], bool> getDbSparePartCodes = value => {
                        var dbObj = new PartsBranchExtend {
                            PartId = Convert.ToInt32(value[0]),
                            PartCode = value[1],
                            PartName = value[2]
                        };
                        dbSparePartCodes.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator(@"select id,Trim(Code),Trim(Name) from sparePart where status=1 ", "Code", true, partCodesNeedCheck, getDbSparePartCodes);
                    foreach(var errorItem in tempRightList) {
                        var oldSparePart = dbSparePartCodes.FirstOrDefault(v => v.PartCode == errorItem.PartCodeStr);
                        if(oldSparePart == null) {
                            errorItem.ErrorMsg = String.Format(ErrorStrings.Export_Validation_PartsBranch_SparePartIsNull, errorItem.PartCodeStr);
                        } else {
                            errorItem.PartId = oldSparePart.PartId;
                            errorItem.PartName = oldSparePart.PartName;
                        }
                    }
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();                
                    var partsBranchNeedCheck = tempRightList.Select(r => r.PartCodeStr.ToUpper()).Distinct().ToArray();
                    var dbPartsBranches = new List<PartsBranchExtend>();
                    Func<string[], bool> getDbPartsBranches = value => {
                        var dbObj = new PartsBranchExtend {
                            Id = int.Parse(value[0]),
                            PartsSalesCategoryName = value[1],
                            BranchName = value[2],
                            BranchId = int.Parse(value[3]),
                            PartCode = value[4]
                        };
                        dbPartsBranches.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator(@"select Id, partssalescategoryName, branchName,branchId, partcode from partsbranch  where status = 1 ", "PartCode", true, partsBranchNeedCheck, getDbPartsBranches);
                    foreach(var Item in tempRightList) {
                        var dbitem = dbPartsBranches.Where(v =>  v.PartCode == Item.PartCodeStr).FirstOrDefault();
                        if(dbitem == null) {
                            Item.ErrorMsg = ErrorStrings.Export_Validation_PartsBranch_PartsSalescategoryIsNull;
                        } else {
                            Item.Id = dbitem.Id;
                        }
                    }
                    #endregion

                    //获取所有不合格数据
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    //获取所有合格数据
                    rightList = allList.Except(errorList).ToList();
                }

                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempObj = list[index - 1];
                            var values = new object[] {
                                tempObj.PartCodeStr, tempObj.PartNameStr, tempObj.StockMinimumStr, tempObj.ErrorMsg
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }

                //更新导入所有合格数据
                if(!rightList.Any())
                    return true;
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    //开启事务，新增更新配件在一个事务内
                    var ts = conn.BeginTransaction();
                    try {
                        //修改配件
                        if(rightList.Any()) {
                            //获取修改数据的sql语句，Id为主键
                            var sqlUpdate = db.GetUpdateSql("PartsBranch", new[] {
                                "ModifierId", "ModifierName", "ModifyTime", "StockMinimum","OMSparePartMark"
                            }, new[] {
                                "Id"
                            });
                            var userInfo = Utils.GetCurrentUserInfo();
                            //往数据库修改配件信息
                            foreach(var item in rightList) {
                                var command = db.CreateDbCommand(sqlUpdate, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("StockMinimum", item.StockMinimum));
                                command.Parameters.Add(db.CreateDbParameter("ModifierId", userInfo.Id));
                                command.Parameters.Add(db.CreateDbParameter("ModifierName", userInfo.Name));
                                command.Parameters.Add(db.CreateDbParameter("ModifyTime", DateTime.Now));
                                command.Parameters.Add(db.CreateDbParameter("Id", item.Id));
                                command.Parameters.Add(db.CreateDbParameter("OMSparePartMark", 0));
                                command.ExecuteNonQuery();
                            }
                            //增加履历
                            InsertPartsBranchHistoryByPartsIdOrList(db, rightList.Select(r => r.Id.ToString()).Distinct().ToArray(), null, conn, ts);
                        }
                        //无异常提交
                        ts.Commit();
                    } catch(Exception) {
                        //报错回滚
                        ts.Rollback();
                        throw;
                    } finally {
                        if(conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }
    }
}