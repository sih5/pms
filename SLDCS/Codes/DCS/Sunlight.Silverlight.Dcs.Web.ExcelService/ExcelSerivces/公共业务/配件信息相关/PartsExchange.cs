﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        public bool ExportPartsExchange(string exchangeCode, string exchangeName, string partCode, string partName, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName) {
            fileName = GetExportFilePath("配件互换信息_" + ".xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated) {
                    throw new Exception(ErrorStrings.File_Validation_User);
                }
                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {

                    conn.Open();
                    var sql = new StringBuilder();
                    sql.Append(@"select    a.ExchangeCode,
                                           a.ExchangeName,
                                           b.code as PartCode,
                                           b.name as PartName,
                                           (select value from keyvalueitem where NAME = 'MasterData_Status'and key=b.Status) As Status,
                                           a.CreatorName,
                                           a.CreateTime,
                                           a.ModifierName,
                                           a.ModifyTime,
                                           a.AbandonerName,
                                           a.AbandonTime
                                      from PartsExchange a
                                     inner join sparepart b
                                        on a.partid = b.id
                                       and b.status = 1
                                     where exists (select 1
                                              from PartsExchange c
                                             inner join sparepart d
                                                on c.partid = d.id
                                             where 1=1
                                               and a.exchangecode = c.exchangecode");
                    var dbParameters = new List<DbParameter>();
                    if(!string.IsNullOrEmpty(exchangeCode)) {
                        sql.Append(@" and c.exchangeCode like {0}exchangeCode ");
                        dbParameters.Add(db.CreateDbParameter("exchangeCode", "%" + exchangeCode + "%"));
                    }
                    if(!string.IsNullOrEmpty(exchangeName)) {
                        sql.Append(@" and c.exchangeName like {0}exchangeName ");
                        dbParameters.Add(db.CreateDbParameter("exchangeName", "%" + exchangeName + "%"));
                    }
                    if(!string.IsNullOrEmpty(partCode)) {
                        sql.Append(@" and d.Code like {0}partCode ");
                        dbParameters.Add(db.CreateDbParameter("partCode", "%" + partCode + "%"));
                    }
                    if(!string.IsNullOrEmpty(partName)) {
                        sql.Append(@" and d.Name like {0}partName ");
                        dbParameters.Add(db.CreateDbParameter("partName", "%" + partName + "%"));
                    }
                    if(status.HasValue) {
                        sql.Append(@" and c.status ={0}Status");
                        dbParameters.Add(db.CreateDbParameter("status", status.Value));
                    }
                    if(createTimeBegin.HasValue) {
                        sql.Append(@" and c.createTime >=to_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                        var tempValue = createTimeBegin.Value;
                        var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                        dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                    }
                    if(createTimeEnd.HasValue) {
                        sql.Append(@" and c.createTime <=to_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                        var tempValue = createTimeEnd.Value;
                        var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                        dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                    }

                    sql.Append(" )");



                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    ErrorStrings.Export_Title_PartsExchangeGroupExtend_ExGroupCode, ErrorStrings.Export_Title_PartsExchangeGroupExtend_ExGroupName, ErrorStrings.Export_Title_PartsBranch_Code, ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName, ErrorStrings.Export_Title_AccountPeriod_Status, ErrorStrings.Export_Title_AccountPeriod_CreatorName, ErrorStrings.Export_Title_AccountPeriod_CreateTime, ErrorStrings.Export_Title_AccountPeriod_ModifierName, ErrorStrings.Export_Title_AccountPeriod_ModifyTime, ErrorStrings.Export_Title_PartsBranch_AbandonerName, ErrorStrings.Export_Title_PartsBranch_AbandonerTime
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }

                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }

        public bool ExportPartsExchangeByPrice(int[] ids, string exchangeCode, string partCode, string partName, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName) {
            fileName = GetExportFilePath("配件互换价格不一致信息_" + ".xlsx");
            try {
                var userinfo = Utils.GetCurrentUserInfo();
                if(!HttpContext.Current.User.Identity.IsAuthenticated) {
                    throw new Exception(ErrorStrings.File_Validation_User);
                }
                var db = DbHelper.GetDbHelp(ConnectionString);


                var sqlString = new StringBuilder();
                var dbParameters = new List<DbParameter>();
                if(ids != null && ids.Length > 0) {
                    sqlString.Append(" and p.id in (");
                    for(var i = 0; i < ids.Length; i++) {
                        if(ids.Length == i + 1) {
                            sqlString.Append("{0}" + ids[i].ToString(CultureInfo.InvariantCulture));
                            dbParameters.Add(db.CreateDbParameter(ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                        } else {
                            sqlString.Append("{0}" + ids[i].ToString(CultureInfo.InvariantCulture) + ",");
                            dbParameters.Add(db.CreateDbParameter(ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                        }
                    }
                    sqlString.Append(")");
                } else {
                    if(!string.IsNullOrEmpty(exchangeCode)) {
                        sqlString.Append(@" and p.ExchangeCode like {0}exchangeCode ");
                        dbParameters.Add(db.CreateDbParameter("exchangeCode", "%" + exchangeCode + "%"));
                    }
                    if(!string.IsNullOrEmpty(partCode)) {
                        sqlString.Append(@" and s.Code like {0}partCode ");
                        dbParameters.Add(db.CreateDbParameter("partCode", "%" + partCode + "%"));
                    }
                    if(!string.IsNullOrEmpty(partName)) {
                        sqlString.Append(@" and s.Name like {0}partName ");
                        dbParameters.Add(db.CreateDbParameter("partName", "%" + partName + "%"));
                    }
                    if(status.HasValue) {
                        sqlString.Append(@" and p.status ={0}Status");
                        dbParameters.Add(db.CreateDbParameter("status", status.Value));
                    }
                    if(createTimeBegin.HasValue) {
                        sqlString.Append(@" and p.createTime >=to_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                        var tempValue = createTimeBegin.Value;
                        var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                        dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                    }
                    if(createTimeEnd.HasValue) {
                        sqlString.Append(@" and p.createTime <=to_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                        var tempValue = createTimeEnd.Value;
                        var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                        dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                    }
                }

                using(var conn = db.CreateDbConnection()) {

                    conn.Open();
                    var sql = new StringBuilder();
                    sql.AppendFormat(@"select a.ExchangeCode,
       s.Code,
       s.Name,
       p.name,
       c.SalesPrice,
       b.RetailGuidePrice,
       (select value from keyvalueitem where NAME = 'MasterData_Status'and key=a.Status) As Status,
       a.CreatorName,
       a.CreateTime,
       a.ModifierName,
       a.ModifyTime,
       a.AbandonerName,
       a.AbandonTime
  From PartsExchange a
 inner join PartsRetailGuidePrice b
    on a.partid = b.SparePartId
 inner join PartsSalesPrice c
    on a.partid = c.SparePartId
 inner join SparePart s
    on s.id = a.partid
 inner join PartsSalesCategory p
    on p.id = b.partssalescategoryid
 inner join (select ExchangeCode, PartsSalesCategoryId
               from (select ExchangeCode, PartsSalesCategoryId, count(*) as c
                       from (select ExchangeCode,
                                    b.RetailGuidePrice,
                                    b.PartsSalesCategoryId,
                                    count(*)
                               from PartsExchange a
                              inner join PartsRetailGuidePrice b
                                 on a.partid = b.SparePartId
                              where b.PartsSalesCategoryId in
                                    (select PartsSalesCategoryId
                                       from PersonSalesCenterLink
                                      where PersonId =  " + userinfo.Id + @"
                                        and status = 1) and a.status=1 and a.ExchangeCode in (  select ExchangeCode from PartsExchange p inner join   SparePart s  on s.id = p.partid where 1=1 {0}) 
                              group by ExchangeCode,
                                       b.PartsSalesCategoryId,
                                       b.RetailGuidePrice) a
                      group by ExchangeCode, PartsSalesCategoryId) a
              where c > 1
             union
             select ExchangeCode, PartsSalesCategoryId
               from (select ExchangeCode, PartsSalesCategoryId, count(*) as c
                       from (select ExchangeCode,
                                    b.RetailGuidePrice,
                                    b.PartsSalesCategoryId,
                                    count(*)
                               from PartsExchange a
                              inner join PartsRetailGuidePrice b
                                 on a.partid = b.SparePartId
                              where b.PartsSalesCategoryId in
                                    (select PartsSalesCategoryId
                                       from PersonSalesCenterLink
                                      where PersonId =  " + userinfo.Id + @"
                                        and status = 1) and a.status=1 and a.ExchangeCode in (  select ExchangeCode from PartsExchange p inner join   SparePart s  on s.id = p.partid where 1=1 {0}) 
                              group by ExchangeCode,
                                       b.PartsSalesCategoryId,
                                       b.RetailGuidePrice) a
                      group by ExchangeCode, PartsSalesCategoryId) a
              where c > 1) d
    on a.ExchangeCode = d.ExchangeCode
 where
    c.partssalescategoryid = b.partssalescategoryid
   and c.partssalescategoryid = d.partssalescategoryid
       and b.PartsSalesCategoryId in
       (select PartsSalesCategoryId
          from PersonSalesCenterLink
         where PersonId = " + userinfo.Id + @"
           and status = 1)
   and c.PartsSalesCategoryId in
       (select PartsSalesCategoryId
          from PersonSalesCenterLink
         where PersonId =  " + userinfo.Id + @"
           and status = 1)  and a.status=1  
    order by ExchangeCode, p.name, s.id  ", sqlString);
                    var sqlStr = sql.ToString();
                    if(sqlStr.IndexOf(" where 1=1  and ") > 0)
                        sqlStr = sqlStr.Replace(" where 1=1  and ", " where ");
                    else
                        sqlStr = sqlStr.Replace(" where 1=1 ", "");

                    var command = db.CreateDbCommand(string.Format(sqlStr, db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    ErrorStrings.Export_Title_PartsExchangeGroupExtend_ExGroupCode, ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode, ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName, ErrorStrings.Export_Title_Partssalescategory_Name,
                                    ErrorStrings.Export_Title_PartsExchangeGroupExtend_SalesPrice,ErrorStrings.Export_Title_PartsExchangeGroupExtend_RetailGuidePrice, ErrorStrings.Export_Title_AccountPeriod_Status, ErrorStrings.Export_Title_AccountPeriod_CreatorName, ErrorStrings.Export_Title_AccountPeriod_CreateTime, ErrorStrings.Export_Title_AccountPeriod_ModifierName, ErrorStrings.Export_Title_AccountPeriod_ModifyTime, ErrorStrings.Export_Title_PartsBranch_AbandonerName, ErrorStrings.Export_Title_PartsBranch_AbandonerTime
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }

                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }


        /// <summary>
        /// 导入配件互换信息
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="excelImportNum"></param>
        /// <param name="errorData"></param>
        /// <param name="errorDataFileName"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public bool ImportPartsExchange(string fileName, out int excelImportNum, out List<PartsExchangeExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<PartsExchangeExtend>();
            var allList = new List<PartsExchangeExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("PartsExchange", out notNullableFields, out fieldLenght);

                List<string> notNullableFieldsSparePart;
                Dictionary<string, int> fieldLenghtSparePart;
                db.GetTableSchema("SparePart", out notNullableFieldsSparePart, out fieldLenghtSparePart);

                List<object> excelColumns;
                List<PartsExchangeExtend> rightList;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    #region 指定文件中的列对应的名称 例如 excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode, "PartCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsExchangeGroupExtend_ExGroupCode, "ExchangeCode");
                    //excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsExchangeGroupExtend_ExGroupName, "ExchangeName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Code, "PartCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName, "PartName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Remark, "Remark");
                    #endregion

                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;

                    #region 获取对应枚举
                    var keyValuePairs = new[] {
                        new KeyValuePair<string, string>("BaseDataStatus", "BaseData_Status")
                    };
                    tempExcelOperator.ImportHelper.LoadEnumFromDb(null, null, keyValuePairs);

                    #endregion

                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var tempImportObj = new PartsExchangeExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);

                        #region 根据导入的字段给对应的实体属性赋值
                        tempImportObj.ExchangeCodeStr = newRow["ExchangeCode"];
                        //tempImportObj.ExchangeNameStr = newRow["ExchangeName"];
                        tempImportObj.RemarkStr = newRow["Remark"];
                        tempImportObj.PartCodeStr = newRow["PartCode"];
                        tempImportObj.PartNameStr = newRow["PartName"];
                        #endregion

                        var tempErrorMessage = new List<string>();

                        #region 导入的数据基本检查
                        //互换号检查
                        if(string.IsNullOrEmpty(tempImportObj.ExchangeCodeStr)) {
                            tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsExchangeGroupExtend_ExGroupCodeIsNull);
                        }

                        ////互换名称检查
                        //fieldIndex = notNullableFields.IndexOf("ExchangeName".ToUpper());
                        //if (string.IsNullOrEmpty(tempImportObj.ExchangeNameStr)) {
                        //    if (fieldIndex > -1)
                        //        tempErrorMessage.Add("互换名称不能为空");
                        //} else {
                        //    if (Encoding.Default.GetByteCount(tempImportObj.ExchangeNameStr) > fieldLenght["ExchangeName".ToUpper()])
                        //        tempErrorMessage.Add("互换名称过长");
                        //}

                        //配件编号检查
                        var fieldIndex = notNullableFieldsSparePart.IndexOf("Code".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.PartCodeStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.OverstockPartsAppDetail__Validation1);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.PartCodeStr) > fieldLenghtSparePart["Code".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.ImpPartsSupplierRelation_Validation8);
                        }

                        //备注检查
                        fieldIndex = notNullableFields.IndexOf("Remark".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.RemarkStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchasePricingDetail_Validation15);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.RemarkStr) > fieldLenght["Remark".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchasePricingDetail_Validation16);
                        }
                        #endregion

                        if(tempErrorMessage.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });
                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    #region 剩下的数据进行业务检查
                    //文件内，互换号、配件编号组合唯一
                    var groups = tempRightList.GroupBy(r => new {
                        r.PartCodeStr
                    }).Where(r => r.Count() > 1);
                    var list = groups.SelectMany(r => r).Select(r => r.ExchangeCodeStr).ToList();
                    var tempWrongList = tempRightList.Where(r => list.Any(v => v == r.ExchangeCodeStr)).ToList();
                    foreach(var item in tempWrongList) {
                        item.ErrorMsg = ErrorStrings.Export_Validation_PartsExchangeGroupExtend_MultipleRecords;
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    //配件编号、配件名称组合存在配件信息中
                    var partCodesNeedCheck = tempRightList.Select(r => r.PartCodeStr.ToUpper()).Distinct().ToArray();
                    var dbSpareParts = new List<SparePartExtend>();
                    Func<string[], bool> getDbSparePartCodes = value => {
                        var dbObj = new SparePartExtend {
                            Id = Convert.ToInt32(value[0]),
                            Code = value[1]
                        };
                        dbSpareParts.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select Id,Trim(Code) as Code from sparePart where status=1 ", "Code", true, partCodesNeedCheck, getDbSparePartCodes);
                    foreach(var tempRight in tempRightList) {
                        var sparePart = dbSpareParts.FirstOrDefault(v => v.Code == tempRight.PartCodeStr);
                        if(sparePart == null) {
                            tempRight.ErrorMsg = String.Format(ErrorStrings.Export_Validation_PartsBranch_SparePartIsNull, tempRight.PartCodeStr);
                            continue;
                        }
                        tempRight.PartId = sparePart.Id;
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    //系统内数据重复校验
                    var exchangeCodeStrsNeedCheck = tempRightList.Select(r => r.ExchangeCodeStr).Distinct().ToArray();
                    var dbPartsExchanges = new List<PartsExchangeExtend>();
                    Func<string[], bool> getDbPartsExchanges = value => {
                        var dbObj = new PartsExchangeExtend {
                            //ExchangeCode = value[0],
                            PartId = Convert.ToInt32(value[0]),
                            PartCode = value[1]
                        };
                        dbPartsExchanges.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select pe.PartId,s.Code from PartsExchange pe inner join Sparepart s on pe.PartId=s.Id where pe.status=1 ", "ExchangeCode", false, exchangeCodeStrsNeedCheck, getDbPartsExchanges);
                    foreach(var tempRight in tempRightList) {
                        var newSparePart = dbPartsExchanges.FirstOrDefault(v => v.PartId == tempRight.PartId);
                        if(newSparePart != null) {
                            tempRight.ErrorMsg = String.Format(ErrorStrings.Export_Validation_PartsExchangeGroupExtend_HasChange, tempRight.PartCode);
                        }
                    }

                    #endregion

                    //获取所有不合格数据
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    //获取所有合格数据
                    rightList = allList.Except(errorList).ToList();

                    #region 将合格数据的值填上
                    foreach(var rightItem in rightList) {
                        //rightItem.ExchangeCode = exchangeCode;
                        //rightItem.ExchangeCodeStr;
                        //rightItem.ExchangeName = rightItem.ExchangeNameStr;
                        rightItem.Status = tempExcelOperator.ImportHelper.GetEnumValue("BaseDataStatus", ErrorStrings.Export_Validation_PartsBranch_Used) ?? 0;
                        rightItem.Remark = rightItem.RemarkStr;
                    }
                    #endregion
                }
                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempObj = list[index - 1];
                            var values = new object[] {
                                #region 设置错误信息导出的列的值
                                tempObj.ExchangeCodeStr, //tempObj.ExchangeNameStr, 
                                tempObj.PartCodeStr, tempObj.PartNameStr, tempObj.RemarkStr, tempObj.ErrorMsg
                                #endregion
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }

                //导入所有合格数据
                if(!rightList.Any())
                    return true;
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    //开启事务，新增更新配件在一个事务内
                    var ts = conn.BeginTransaction();
                    try {
                        //新增配件
                        if(rightList.Any()) {
                            var rightListGroups = rightList.GroupBy(r => r.ExchangeCodeStr);
                            foreach(var group in rightListGroups) {
                                var tempRightList = group.ToList();


                                //创建统一使用的新的互换号
                                string exchangeCode = CodeGenerator.Generate(db, "PartsExchange");

                                #region 获取对应的插入的SQL 通过db.GetInsertSql方法
                                //获取新增数据的sql语句，Id为主键
                                var sqlInsert = db.GetInsertSql("PartsExchange", "Id", new[] {
                                    "ExchangeCode", "PartId", "Status", "CreatorId", "CreatorName", "CreateTime", "Remark"
                                });
                                var sqlHistoryInsert = db.GetInsertSql("PartsExchangeHistory", "Id", new[] {
                                    "PartsExchangeId", "ExchangeCode", "PartId", "Status", "CreatorId", "CreatorName", "CreateTime", "Remark"
                                });
                                #endregion


                                var userInfo = Utils.GetCurrentUserInfo();
                                //查询出数据库中已经存在互换信息的配件
                                var sql = new StringBuilder();
                                sql.Append(@"select Id,PartId,ExchangeCode,Status  from partsexchange where status = 1 and exchangecode in (select exchangecode from partsexchange where status = 1 ");
                                sql.Append(@" and partid in (");
                                var dbParameters = new List<DbParameter>();
                                for(var i = 0; i < tempRightList.Count; i++) {
                                    var tempPartsPurchaseOrderIdWithNum = "partId" + i;
                                    if(i == tempRightList.Count - 1) {
                                        var tempParam = "{0}" + tempPartsPurchaseOrderIdWithNum;
                                        sql.Append(tempParam);
                                    } else {
                                        var tempParam = "{0}" + tempPartsPurchaseOrderIdWithNum + ",";
                                        sql.Append(tempParam);
                                    }
                                    dbParameters.Add(db.CreateDbParameter(tempPartsPurchaseOrderIdWithNum, tempRightList[i].PartId));
                                }
                                sql.Append(@"))");
                                var commandread = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                                commandread.Parameters.AddRange(dbParameters.ToArray());
                                var reader = commandread.ExecuteReader();
                                var tempUpdatelist = new List<PartsExchangeExtend>();
                                while(reader.Read()) {
                                    var tempUpdateObj = new PartsExchangeExtend();
                                    tempUpdateObj.Id = Convert.ToInt32(reader["Id"].ToString());
                                    tempUpdateObj.PartId = Convert.ToInt32(reader["PartId"].ToString());
                                    tempUpdateObj.ExchangeCode = reader["ExchangeCode"].ToString();
                                    tempUpdateObj.Status = Convert.ToInt32(reader["Status"].ToString());
                                    tempUpdatelist.Add(tempUpdateObj);
                                }
                                reader.Close();

                                var sqlUpdate = db.GetUpdateSql("PartsExchange", new[] {
                                    "Status", "ModifierId", "ModifierName", "ModifyTime", "Remark"

                                }, new[] {
                                    "Id"
                                });


                                //与导入的配件信息可以互换的配件信息,因为要全部重新生成,所以先全部删除
                                var deleteRemark = ErrorStrings.Export_Title_PartsExchangeGroupExtend_ExGroupCode + exchangeCode + "的导入而作废";
                                foreach(var item in tempUpdatelist) {
                                    #region 添加Sql的参数
                                    var command = db.CreateDbCommand(sqlUpdate, conn, ts);
                                    command.Parameters.Add(db.CreateDbParameter("Status", (int)DcsBaseDataStatus.作废));
                                    command.Parameters.Add(db.CreateDbParameter("ModifierId", userInfo.Id));
                                    command.Parameters.Add(db.CreateDbParameter("ModifierName", userInfo.Name));
                                    command.Parameters.Add(db.CreateDbParameter("ModifyTime", DateTime.Now));
                                    command.Parameters.Add(db.CreateDbParameter("Id", item.Id));
                                    command.Parameters.Add(db.CreateDbParameter("Remark", deleteRemark));
                                    command.ExecuteNonQuery();

                                    var tempId = db.ExecuteInsert(command, "Id");
                                    //加履历
                                    var commandHistory = db.CreateDbCommand(sqlHistoryInsert, conn, ts);
                                    commandHistory.Parameters.Add(db.CreateDbParameter("PartsExchangeId", tempId));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("ExchangeCode", item.ExchangeCode));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("PartId", item.PartId));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Status", item.Status));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Remark", deleteRemark));
                                    commandHistory.ExecuteNonQuery();
                                    #endregion
                                }

                                //往数据库增加配件信息 第一部分：导入的数据
                                foreach(var item in tempRightList) {
                                    #region 添加Sql的参数
                                    var command = db.CreateDbCommand(sqlInsert, conn, ts);
                                    command.Parameters.Add(db.CreateDbParameter("ExchangeCode", exchangeCode));
                                    command.Parameters.Add(db.CreateDbParameter("PartId", item.PartId));
                                    command.Parameters.Add(db.CreateDbParameter("Status", item.Status));
                                    command.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                    command.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                    command.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                    command.Parameters.Add(db.CreateDbParameter("Remark", item.Remark));
                                    #endregion

                                    var tempId = db.ExecuteInsert(command, "Id");
                                    //加履历
                                    var commandHistory = db.CreateDbCommand(sqlHistoryInsert, conn, ts);
                                    commandHistory.Parameters.Add(db.CreateDbParameter("PartsExchangeId", tempId));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("ExchangeCode", exchangeCode));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("PartId", item.PartId));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Status", item.Status));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Remark", item.Remark));
                                    commandHistory.ExecuteNonQuery();
                                    // }
                                }
                                //第2部分：与导入的配件信息可以互换的配件信息,全部重新生成,状态赋值为新增,备注赋值为由于互换号..导入而而重新增加
                                var listNeedReAdd = tempUpdatelist.Where(r => tempRightList.All(v => v.PartId != r.PartId)).Select(r => r.PartId).Distinct().ToArray();
                                var tempStatus = (int)DcsBaseDataStatus.有效;
                                var tempRemark = ErrorStrings.Export_Title_PartsExchangeGroupExtend_ExGroupCode + exchangeCode + "的导入而重新增加";
                                foreach(var item in listNeedReAdd) {
                                    #region 添加Sql的参数
                                    var command = db.CreateDbCommand(sqlInsert, conn, ts);
                                    command.Parameters.Add(db.CreateDbParameter("ExchangeCode", exchangeCode));
                                    command.Parameters.Add(db.CreateDbParameter("PartId", item));
                                    command.Parameters.Add(db.CreateDbParameter("Status", tempStatus));
                                    command.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                    command.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                    command.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                    command.Parameters.Add(db.CreateDbParameter("Remark", tempRemark));
                                    #endregion

                                    var tempId = db.ExecuteInsert(command, "Id");
                                    //加履历
                                    var commandHistory = db.CreateDbCommand(sqlHistoryInsert, conn, ts);
                                    commandHistory.Parameters.Add(db.CreateDbParameter("PartsExchangeId", tempId));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("ExchangeCode", exchangeCode));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("PartId", item));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Status", tempStatus));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Remark", tempRemark));
                                    commandHistory.ExecuteNonQuery();
                                }


                            }
                        }
                        //无异常提交
                        ts.Commit();
                    } catch(Exception) {
                        //报错回滚
                        ts.Rollback();
                        throw;
                    } finally {
                        if(conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
            }
        }

    }
}
