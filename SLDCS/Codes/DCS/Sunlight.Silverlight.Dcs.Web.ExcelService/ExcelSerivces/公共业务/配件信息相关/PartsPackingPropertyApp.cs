﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService
{
    public partial class ExcelService
    {
        public bool ExportPartsPackingPropertyApp(int[] ids, string code, string spareCode, string spareName, int? status, int? mainPackingType, DateTime? bCreateTime, DateTime? eCreateTime, out string fileName)
        {
            fileName = GetExportFilePath("导出配件包装设置.xlsx");
            try
            {
                if (!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);
                using (var conn = db.CreateDbConnection())
                {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.Append(@"select Code, SpareCode,SpareName,SihCode,
                      (select value from keyvalueitem where name='PackingPropertyAppStatus' and key=a.Status),
                       (select value from keyvalueitem where name='PackingUnitType' and key=a.MainPackingType),MInSalesAmount,OldMInPackingAmount,
                      CreatorName,CreateTime,ModifierName,ModifyTime,ApproverName,ApproveTime ,(select value from keyvalueitem where name='PackingUnitType' and key=p.PackingType)," +
                        "p.MeasureUnit,p.PackingCoefficient,p.PackingMaterial,p.Volume,p.Weight,p.Length,p.Width,p.Height " +
                      "from partspackingpropertyapp a left join PartsPackingPropAppDetail p on a.id=p.PartsPackingPropAppId where 1=1 ");
                    var dbParameters = new List<DbParameter>();
                    if (ids != null && ids.Length > 0)
                    {
                        sql.Append(" and a.id in (");
                        for (var i = 0; i < ids.Length; i++)
                        {
                            if (ids.Length == i + 1)
                            {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                            else
                            {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                        }
                        sql.Append(")");
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(code))
                        {
                            sql.Append("and LOWER(a.Code) like {0}code ");
                            dbParameters.Add(db.CreateDbParameter("code", "%" + code.ToLower() + "%"));
                        }
                        if (!string.IsNullOrEmpty(spareCode))
                        {
                            sql.Append("and LOWER(a.SpareCode) like {0}spareCode ");
                            dbParameters.Add(db.CreateDbParameter("spareCode", "%" + code.ToLower() + "%"));
                        }
                        if (!string.IsNullOrEmpty(spareName))
                        {
                            sql.Append("and LOWER(a.SpareName) like {0}spareName ");
                            dbParameters.Add(db.CreateDbParameter("spareName", "%" + code.ToLower() + "%"));
                        }
                        if (status.HasValue)
                        {
                            sql.Append(@" and a.Status = {0}status ");
                            dbParameters.Add(db.CreateDbParameter("status", status.Value));
                        }
                        if (mainPackingType.HasValue)
                        {
                            sql.Append(@" and a.MainPackingType = {0}mainPackingType ");
                            dbParameters.Add(db.CreateDbParameter("mainPackingType", status.Value));
                        }
                        if (bCreateTime.HasValue)
                        {
                            sql.Append(@" and a.createTime >=To_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = bCreateTime.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if (eCreateTime.HasValue)
                        {
                            sql.Append(@" and a.createTime <=To_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = eCreateTime.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                    }
                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using (var excelExport = new ExcelExport(fileName))
                    {
                        excelExport.ExportByRow(index =>
                        {
                            if (index == 0)
                            {
                                return new object[] {

                                    ErrorStrings.Export_Title_PackingPropertyApp_Code, ErrorStrings.Export_Title_PartsBranch_Code, ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName, ErrorStrings.Export_Title_Accessinfo_Tmp_ReferenceCode, ErrorStrings.Export_Title_AccountPeriod_Status, ErrorStrings.Export_Title_PartsBranch_MinPacking,ErrorStrings.Export_Title_PackingPropertyApp_Minsalequantity,
                                    ErrorStrings.Export_Title_PackingPropertyApp_OldMinsalequantity, ErrorStrings.Export_Title_AccountPeriod_CreatorName, ErrorStrings.Export_Title_AccountPeriod_CreateTime,ErrorStrings.Export_Title_AccountPeriod_ModifierName,ErrorStrings.Export_Title_AccountPeriod_ModifyTime, 
                                    ErrorStrings.Export_Title_PackingPropertyApp_ApproverName,ErrorStrings.Export_Title_PackingPropertyApp_ApproveTime,ErrorStrings.Export_Title_PackingPropertyApp_PackingType,ErrorStrings.Export_Title_PackingPropertyApp_MeasureUnit,
                                    ErrorStrings.Export_Title_PackingPropertyApp_PackingAmount,ErrorStrings.Export_Title_PackingPropertyApp_PackingMaterial,ErrorStrings.Export_Title_PackingPropertyApp_Volume,ErrorStrings.Export_Title_PackingPropertyApp_Weight,
                                    ErrorStrings.Export_Title_PackingPropertyApp_Length,ErrorStrings.Export_Title_PackingPropertyApp_Width,ErrorStrings.Export_Title_PackingPropertyApp_Height
                                };
                            }
                            if (reader.Read())
                            {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if (num != reader.FieldCount)
                                {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();

                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
