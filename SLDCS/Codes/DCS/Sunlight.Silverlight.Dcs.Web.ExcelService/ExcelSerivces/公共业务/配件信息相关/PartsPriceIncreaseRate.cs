﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Entities;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        public bool 导出加价策略信息(int[] ids, string CategoryCode, string CategoryName, int? BrandId, int? Status, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName) {
            fileName = GetExportFilePath(string.Format("配件加价策略信息_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var userinfo = Utils.GetCurrentUserInfo();
                var db = DbHelper.GetDbHelp(ConnectionString);
                using(var conn = db.CreateDbConnection()) {
                    var sql = new StringBuilder();
                    sql.AppendFormat(@"SELECT 
                                       CategoryCode,CategoryName,Brand,SaleIncreaseRate,MaxSalesPriceFloating,MinSalesPriceFloating,retailIncreaseRate,MaxRetailOrderPriceFloating,MinRetailOrderPriceFloating,(select value from keyvalueitem where key=PartsPriceIncreaseRate.Status and name='MasterData_Status') Status,Remark,CreatorName,CreateTime,ModifierName,ModifyTime,AbandonerName,AbandonTime From PartsPriceIncreaseRate where 1=1 ");
                    var dbParameters = new List<DbParameter>();
                    if(ids != null && ids.Length > 0) {
                        sql.Append(" and Id in (");
                        for(var i = 0; i < ids.Length; i++) {
                            if(ids.Length == i + 1) {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            } else {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                        }
                        sql.Append(")");
                    } else {
                        if(!string.IsNullOrEmpty(CategoryCode)) {
                            sql.Append(" and Lower(CategoryCode) like {0}CategoryCode ");
                            dbParameters.Add(db.CreateDbParameter("CategoryCode", "%" + CategoryCode.ToLower() + "%"));
                        }
                        if(!string.IsNullOrEmpty(CategoryName)) {
                            sql.Append(" and Lower(CategoryName) like {0}CategoryName ");
                            dbParameters.Add(db.CreateDbParameter("CategoryName", "%" + CategoryName.ToLower() + "%"));
                        }
                        if(BrandId.HasValue) {
                            sql.Append(" and BrandId = {0}BrandId ");
                            dbParameters.Add(db.CreateDbParameter("BrandId", BrandId));
                        }
                        if(Status.HasValue) {
                            sql.Append(" and Status = {0}Status ");
                            dbParameters.Add(db.CreateDbParameter("Status", Status));
                        }
                        if(createTimeBegin.HasValue) {
                            sql.Append(@" and CreateTime >=to_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(@" and CreateTime <=to_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                    }

                    var sqlStr = sql.ToString();
                    if(sqlStr.IndexOf(" where 1=1  and ") > 0)
                        sqlStr = sqlStr.Replace(" where 1=1  and ", " where ");
                    else
                        sqlStr = sqlStr.Replace(" where 1=1 ", "");

                    var command = db.CreateDbCommand(string.Format(sqlStr, db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    conn.Open();
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    ErrorStrings.Export_Title_Sparepart_CategoryCode, ErrorStrings.Export_Title_Sparepart_CategoryName, ErrorStrings.Export_Title_Partssalescategory_Name, "销售标准加价率", "销售价浮动范围（上限）", "销售价浮动范围（下限）","零售标准加价率","建议售价浮动范围（上限）","建议售价浮动范围（下限）",ErrorStrings.Export_Title_AccountPeriod_Status,ErrorStrings.Export_Title_PartsBranch_Remark,ErrorStrings.Export_Title_AccountPeriod_CreatorName,ErrorStrings.Export_Title_AccountPeriod_CreateTime,ErrorStrings.Export_Title_AccountPeriod_ModifierName,ErrorStrings.Export_Title_AccountPeriod_ModifyTime,ErrorStrings.Export_Title_PartsBranch_AbandonerName,ErrorStrings.Export_Title_PartsBranch_AbandonerTime

                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }

        public bool 加价策略信息导入(string fileName, out int excelImportNum, out List<PartsPriceIncreaseRateExtend> rightData, out List<PartsPriceIncreaseRateExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";

            var allList = new List<PartsPriceIncreaseRateExtend>();
            var errorList = new List<PartsPriceIncreaseRateExtend>();
            var rightList = new List<PartsPriceIncreaseRateExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);
                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("PartsPriceIncreaseRate", out notNullableFields, out fieldLenght);
                List<object> excelColumns;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Sparepart_CategoryCode, "CategoryCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Sparepart_CategoryName, "CategoryName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Partssalescategory_Name, "Brand");
                    excelOperator.AddColumnDataSource("销售标准加价率", "SaleIncreaseRateStr");
                    excelOperator.AddColumnDataSource("销售价浮动范围（上限）", "MaxSalesPriceFloatingStr");
                    excelOperator.AddColumnDataSource("销售价浮动范围（下限）", "MinSalesPriceFloatingStr");
                    excelOperator.AddColumnDataSource("零售标准加价率", "retailIncreaseRateStr");
                    excelOperator.AddColumnDataSource("建议售价浮动范围（上限）", "MaxRetailOrderPriceFloatingStr");
                    excelOperator.AddColumnDataSource("建议售价浮动范围（下限）", "MinRetailOrderPriceFloatingStr");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Remark, "Remark");
                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;
                    //var keyValuePairs = new[] {
                    //            new KeyValuePair<string, string>("Status","MasterData_Status") 
                    //        };
                    //tempExcelOperator.ImportHelper.LoadEnumFromDb(null, null, keyValuePairs);
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var tempImportObj = new PartsPriceIncreaseRateExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        tempImportObj.Brand = newRow["Brand"];
                        tempImportObj.CategoryCode = newRow["CategoryCode"];
                        tempImportObj.CategoryName = newRow["CategoryName"];
                        tempImportObj.MaxRetailOrderPriceFloatingStr = newRow["MaxRetailOrderPriceFloatingStr"];
                        tempImportObj.MaxSalesPriceFloatingStr = newRow["MaxSalesPriceFloatingStr"];
                        tempImportObj.MinRetailOrderPriceFloatingStr = newRow["MinRetailOrderPriceFloatingStr"];
                        tempImportObj.MinSalesPriceFloatingStr = newRow["MinSalesPriceFloatingStr"];
                        tempImportObj.retailIncreaseRateStr = newRow["retailIncreaseRateStr"];
                        tempImportObj.SaleIncreaseRateStr = newRow["SaleIncreaseRateStr"];
                        tempImportObj.Remark = newRow["Remark"];
                        var tempErrorMessage = new List<string>();
                        #region 导入的内容基本检查

                        if(string.IsNullOrEmpty(tempImportObj.CategoryCode)) {
                            tempErrorMessage.Add("分类编码不能为空");
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.CategoryCode) > fieldLenght["CategoryCode".ToUpper()])
                                tempErrorMessage.Add("分类编码过长");
                        }

                        if(string.IsNullOrEmpty(tempImportObj.CategoryName)) {
                            tempErrorMessage.Add("分类名称不能为空");
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.CategoryName) > fieldLenght["CategoryName".ToUpper()])
                                tempErrorMessage.Add("分类名称过长");
                        }

                        if(string.IsNullOrEmpty(tempImportObj.Brand)) {
                            tempErrorMessage.Add(ErrorStrings.Export_Validation_MarketingDepartment_PartsSalesCategoryIsNull);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.Brand) > fieldLenght["Brand".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_MarketingDepartment_PartsSalesCategoryIsLong);
                        }
                        if(!string.IsNullOrEmpty(tempImportObj.MaxRetailOrderPriceFloatingStr)) {
                            double checkValue;
                            if(double.TryParse(tempImportObj.MaxRetailOrderPriceFloatingStr, out checkValue)) {
                                if(checkValue < 0)
                                    tempErrorMessage.Add("建议售价浮动范围（上限）不能为负数");
                                tempImportObj.MaxRetailOrderPriceFloating = checkValue;
                            } else
                                tempErrorMessage.Add("建议售价浮动范围（上限）必须为数字");
                        }
                        if(!string.IsNullOrEmpty(tempImportObj.MaxSalesPriceFloatingStr)) {
                            double checkValue;
                            if(double.TryParse(tempImportObj.MaxSalesPriceFloatingStr, out checkValue)) {
                                if(checkValue < 0)
                                    tempErrorMessage.Add("销售价浮动范围（上限）不能为负数");
                                tempImportObj.MaxSalesPriceFloating = checkValue;
                            } else
                                tempErrorMessage.Add("销售价浮动范围（上限）必须为数字");
                        }
                        if(!string.IsNullOrEmpty(tempImportObj.MinRetailOrderPriceFloatingStr)) {
                            double checkValue;
                            if(double.TryParse(tempImportObj.MinRetailOrderPriceFloatingStr, out checkValue)) {
                                if(checkValue < 0)
                                    tempErrorMessage.Add("建议售价浮动范围（下限）不能为负数");
                                tempImportObj.MinRetailOrderPriceFloating = checkValue;
                            } else
                                tempErrorMessage.Add("建议售价浮动范围（下限）必须为数字");
                        }
                        if(!string.IsNullOrEmpty(tempImportObj.MinSalesPriceFloatingStr)) {
                            double checkValue;
                            if(double.TryParse(tempImportObj.MinSalesPriceFloatingStr, out checkValue)) {
                                if(checkValue < 0)
                                    tempErrorMessage.Add("销售价浮动范围（下限）不能为负数");
                                tempImportObj.MinSalesPriceFloating = checkValue;
                            } else
                                tempErrorMessage.Add("销售价浮动范围（下限）必须为数字");
                        }
                        if(!string.IsNullOrEmpty(tempImportObj.retailIncreaseRateStr)) {
                            double checkValue;
                            if(double.TryParse(tempImportObj.retailIncreaseRateStr, out checkValue)) {
                                if(checkValue < 0)
                                    tempErrorMessage.Add("零售标准加价率不能为负数");
                                tempImportObj.retailIncreaseRate = checkValue;
                            } else
                                tempErrorMessage.Add("零售标准加价率必须为数字");
                        }
                        if(!string.IsNullOrEmpty(tempImportObj.SaleIncreaseRateStr)) {
                            double checkValue;
                            if(double.TryParse(tempImportObj.SaleIncreaseRateStr, out checkValue)) {
                                if(checkValue < 0)
                                    tempErrorMessage.Add("销售标准加价率不能为负数");
                                tempImportObj.SaleIncreaseRate = checkValue;
                            } else
                                tempErrorMessage.Add("销销售标准加价率必须为数字");
                        }
                        #endregion
                        if(tempErrorMessage.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });
                    //校验导入的数据业务校验
                    var groupsNeedCheckUnique = allList.Where(r => r.ErrorMsg == null).GroupBy(r => new {
                        r.CategoryCode,
                        r.Brand
                    }).Where(r => r.Count() > 1);
                    foreach(var group in groupsNeedCheckUnique) {
                        foreach(var groupItem in group) {
                            if(groupItem.ErrorMsg == null) {
                                groupItem.ErrorMsg = ErrorStrings.ImpPartsSupplierRelation_Validation31;
                            } else {
                                groupItem.ErrorMsg = groupItem.ErrorMsg + ";文件内数据重复";
                            }
                        }
                    }
                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    var categoryCode = tempRightList.Select(r => r.CategoryCode).Distinct().ToArray();
                    var dbPartsPriceIncreaseRates = new List<PartsPriceIncreaseRateExtend>();
                    Func<string[], bool> getDbPartsPriceIncreaseRates = value => {
                        var dbObj = new PartsPriceIncreaseRateExtend {
                            Id = int.Parse(value[0]),
                            CategoryCode = value[1],
                            CategoryName = value[2],
                            Brand = value[3]
                        };
                        dbPartsPriceIncreaseRates.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperatorByLower("select id,CategoryCode,CategoryName,Brand from PartsPriceIncreaseRate where status=1 ", "CategoryCode", true, categoryCode, getDbPartsPriceIncreaseRates);
                    var inPartsPriceIncreaseRates = tempRightList.Where(r => dbPartsPriceIncreaseRates.Any(v => v.CategoryCode == r.CategoryCode && v.Brand == r.Brand)).ToList();
                    foreach(var inItem in inPartsPriceIncreaseRates) {
                        var _categoryName = dbPartsPriceIncreaseRates.First(r => r.CategoryCode == inItem.CategoryCode && r.Brand == inItem.Brand).CategoryName;
                        if(inItem.CategoryName == _categoryName) {
                            inItem.StatusStr = "修改";
                            inItem.Id = dbPartsPriceIncreaseRates.First(r => r.CategoryCode == inItem.CategoryCode).Id;
                        } else
                            inItem.ErrorMsg = "当前已存在的分类编码对应的分类名称(" + _categoryName + ")与此分类名称(" + inItem.CategoryName + ")不匹配，请核对";
                    }

                    var categoryName = tempRightList.Where(r => r.StatusStr != "修改").Select(r => r.CategoryName).Distinct().ToArray();
                    dbPartsPriceIncreaseRates = new List<PartsPriceIncreaseRateExtend>();
                    Func<string[], bool> getDbPartsPriceIncreaseRateNames = value => {
                        var dbObj = new PartsPriceIncreaseRateExtend {
                            Id = int.Parse(value[0]),
                            CategoryCode = value[1],
                            CategoryName = value[2],
                            BrandId = int.Parse(value[3]),
                            Brand = value[4]
                        };
                        dbPartsPriceIncreaseRates.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperatorByLower("select id,CategoryCode,CategoryName,BrandId,Brand from PartsPriceIncreaseRate where status=1 ", "CategoryName", true, categoryName, getDbPartsPriceIncreaseRateNames);
                    var inPartsPriceIncreaseRates2 = tempRightList.Where(r => dbPartsPriceIncreaseRates.Any(v => v.CategoryName == r.CategoryName && v.Brand == r.Brand)).ToList();
                    foreach(var inItem in inPartsPriceIncreaseRates2) {
                        var _categoryCode = dbPartsPriceIncreaseRates.First(r => r.CategoryName == inItem.CategoryName && r.Brand == inItem.Brand).CategoryCode;
                        if(inItem.CategoryCode == _categoryCode) {
                            inItem.StatusStr = "修改";
                            inItem.Id = dbPartsPriceIncreaseRates.First(r => r.CategoryCode == inItem.CategoryCode && r.Brand == inItem.Brand).Id;
                        } else
                            inItem.ErrorMsg = "当前已存在的分类名称对应的分类编码(" + _categoryCode + ")与此分类编码(" + inItem.CategoryCode + ")不匹配，请核对";
                    }

                    var partsBranchNamesNeedCheck = tempRightList.Select(r => r.Brand).Distinct().ToArray();
                    var dbBranchs = new List<BranchExtend>();
                    Func<string[], bool> getDbBranchs = value => {
                        var dbObj = new BranchExtend {
                            Id = Convert.ToInt32(value[0]),
                            Name = value[1]
                        };
                        dbBranchs.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select Id,Name from partssalescategory where status=1 ", "Name", false, partsBranchNamesNeedCheck, getDbBranchs);
                    foreach(var tempRight in tempRightList) {
                        var branch = dbBranchs.FirstOrDefault(v => v.Name == tempRight.Brand);
                        if(branch == null) {
                            tempRight.ErrorMsg = String.Format(ErrorStrings.ImpPartsSupplierRelation_Validation28, tempRight.Brand);
                            continue;
                        }
                        tempRight.BrandId = branch.Id;

                    }

                    //获取所有不合格数据
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    rightList = allList.Except(errorList).ToList();
                    //导出所有不合格数据
                    if(errorList.Any()) {
                        excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                        errorDataFileName = GetErrorFilePath(fileName);
                        using(var excelExport = new ExcelExport(errorDataFileName)) {
                            var list = errorList;
                            excelExport.ExportByRow(index => {
                                if(index == list.Count + 1)
                                    return null;
                                if(index == 0)
                                    return excelColumns.ToArray();
                                var tempObj = list[index - 1];
                                var values = new object[] {
                                tempObj.CategoryCode,
                                tempObj.CategoryName,
                                tempObj.Brand,
                                tempObj.SaleIncreaseRateStr,
                                tempObj.MaxSalesPriceFloatingStr,
                                tempObj.MinSalesPriceFloatingStr,
                                tempObj.retailIncreaseRateStr,
                                tempObj.MaxRetailOrderPriceFloatingStr,
                                tempObj.MinRetailOrderPriceFloatingStr,
                                tempObj.Remark,
                                tempObj.ErrorMsg
                            };
                                return values;
                            });
                        }
                        errorList = null;
                    }
                    //更新导入所有合格数据
                    if(!rightList.Any())
                        return true;
                    using(var conn = db.CreateDbConnection()) {
                        conn.Open();
                        //开启事务，新增更新配件在一个事务内
                        var ts = conn.BeginTransaction();
                        try {
                            //新增配件
                            var userInfo = Utils.GetCurrentUserInfo();
                            var inRightList = rightList.Where(r => r.StatusStr == "修改").ToList();
                            var notInRightList = rightList.Where(r => r.StatusStr != "修改").ToList();
                            if(notInRightList.Any()) {
                                var sqlInsertPartsPriceIncreaseRate = db.GetInsertSql("PartsPriceIncreaseRate", "Id", new[] {
                                    "CategoryCode","CategoryName","Brand","BrandId","SaleIncreaseRate","MaxSalesPriceFloating","MinSalesPriceFloating","retailIncreaseRate","MaxRetailOrderPriceFloating","MinRetailOrderPriceFloating","Remark","Status","CreatorId","CreatorName","CreateTime"
                                });
                                //往数据库增加配件信息
                                foreach(var item in notInRightList) {
                                    var command = db.CreateDbCommand(sqlInsertPartsPriceIncreaseRate, conn, ts);
                                    command.Parameters.Add(db.CreateDbParameter("CategoryCode", item.CategoryCode));
                                    command.Parameters.Add(db.CreateDbParameter("CategoryName", item.CategoryName));
                                    command.Parameters.Add(db.CreateDbParameter("Brand", item.Brand));
                                    command.Parameters.Add(db.CreateDbParameter("BrandId", item.BrandId));
                                    command.Parameters.Add(db.CreateDbParameter("SaleIncreaseRate", item.SaleIncreaseRate));
                                    command.Parameters.Add(db.CreateDbParameter("MaxSalesPriceFloating", item.MaxSalesPriceFloating));
                                    command.Parameters.Add(db.CreateDbParameter("MinSalesPriceFloating", item.MinSalesPriceFloating));
                                    command.Parameters.Add(db.CreateDbParameter("retailIncreaseRate", item.retailIncreaseRate));
                                    command.Parameters.Add(db.CreateDbParameter("MaxRetailOrderPriceFloating", item.MaxRetailOrderPriceFloating));
                                    command.Parameters.Add(db.CreateDbParameter("MinRetailOrderPriceFloating", item.MinRetailOrderPriceFloating));
                                    command.Parameters.Add(db.CreateDbParameter("Remark", item.Remark));
                                    command.Parameters.Add(db.CreateDbParameter("Status", (int)DcsMasterDataStatus.有效));
                                    command.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                    command.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                    command.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                    command.ExecuteNonQuery();
                                }
                            }
                            if(inRightList.Any()) {
                                var sqlUpdatePriceIncreaseRate = db.GetUpdateSql("PartsPriceIncreaseRate", new[] {
                                    "Brand","BrandId","SaleIncreaseRate","MaxSalesPriceFloating","MinSalesPriceFloating","retailIncreaseRate","MaxRetailOrderPriceFloating","MinRetailOrderPriceFloating","Remark","ModifierId","ModifierName","ModifyTime"
                                }, new[] { "Id" });
                                //往数据库增加配件信息
                                foreach(var item in inRightList) {
                                    var command = db.CreateDbCommand(sqlUpdatePriceIncreaseRate, conn, ts);
                                    command.Parameters.Add(db.CreateDbParameter("Id", item.Id));
                                    command.Parameters.Add(db.CreateDbParameter("Brand", item.Brand));
                                    command.Parameters.Add(db.CreateDbParameter("BrandId", item.BrandId));
                                    command.Parameters.Add(db.CreateDbParameter("SaleIncreaseRate", item.SaleIncreaseRate));
                                    command.Parameters.Add(db.CreateDbParameter("MaxSalesPriceFloating", item.MaxSalesPriceFloating));
                                    command.Parameters.Add(db.CreateDbParameter("MinSalesPriceFloating", item.MinSalesPriceFloating));
                                    command.Parameters.Add(db.CreateDbParameter("retailIncreaseRate", item.retailIncreaseRate));
                                    command.Parameters.Add(db.CreateDbParameter("MaxRetailOrderPriceFloating", item.MaxRetailOrderPriceFloating));
                                    command.Parameters.Add(db.CreateDbParameter("MinRetailOrderPriceFloating", item.MinRetailOrderPriceFloating));
                                    command.Parameters.Add(db.CreateDbParameter("Remark", item.Remark));
                                    command.Parameters.Add(db.CreateDbParameter("ModifierId", userInfo.Id));
                                    command.Parameters.Add(db.CreateDbParameter("ModifierName", userInfo.Name));
                                    command.Parameters.Add(db.CreateDbParameter("ModifyTime", DateTime.Now));
                                    command.ExecuteNonQuery();
                                }
                            }
                            //无异常提交
                            ts.Commit();
                        } catch(Exception) {
                            //报错回滚
                            ts.Rollback();
                            throw;
                        } finally {
                            if(conn.State == System.Data.ConnectionState.Open) {
                                conn.Close();
                            }
                        }
                    }
                    return true;
                }
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }
    }
}
