﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Entities;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService
{
    public partial class ExcelService
    {
        public bool ExportPartsPurchasePricingForQuery(int[] ids, string code, string name, DateTime? bCreateTime, DateTime? eCreateTime, DateTime? bPartCreateTime, DateTime? ePartCreateTime, bool? isprice,bool? isExactExport,string referenceCode, out string fileName)
        {
            fileName = GetExportFilePath(string.Format("新产品明细_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));

            try
            {
                if (!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var userinfo = Utils.GetCurrentUserInfo();
                var db = DbHelper.GetDbHelp(ConnectionString);
                using (var conn = db.CreateDbConnection())
                {
                    var sql = new StringBuilder();
                    sql.AppendFormat(@"select p.partcreatetime,p.code,p.name,p.ReferenceCode,p.companyCode,p.companyName,p.purchaseprice,p.createtime, cast(case p.isprice    when 1 then '是' when 0 then '否' end as varchar2(100)) as  Isprice,  cast(case p.isprimary    when 1 then '是' when 0 then '否' end as varchar2(100)) as isprimary ,p.ValidTo,p.CreatorName,p.ModifierName,p.ModifyTime
                         from (select  a.createtime as partcreatetime,a.code,a.name,a.ReferenceCode,d.code as companyCode,d.name as companyName,c.purchaseprice,c.createtime,case when c.id is null then 0 else 1 end as isprice,case when b.isprimary is null or b.isprimary=0 then 0 else 1 end as isprimary ,
                            c.ValidTo,c.CreatorName,c.ModifierName,c.ModifyTime
                          from sparepart a  join partsbranch pb on a.id =PartId
                          left join partssupplierrelation b on a.id = b.partid and b.status = 1 and b.isprimary=1
                          left join partspurchasepricing c on a.id = c.partid and c.createtime>=a.createtime and c.partssupplierid = b.supplierid and c.validfrom<=sysdate and c.validto>sysdate
                          left join company d on d.id = b.supplierid where  pb.IsOrderable=1 ");
                    var dbParameters = new List<DbParameter>();
                    if (ids != null && ids.Length > 0)
                    {
                        sql.Append(" and a.Id in (");
                        for (var i = 0; i < ids.Length; i++)
                        {
                            if (ids.Length == i + 1)
                            {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                            else
                            {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                        }
                        sql.Append("))p");
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(code)) {
                            var spareparts = code.Split(',');
                            if (spareparts.Length == 1) {
                                var sparepartcode = spareparts[0];
                                if (isExactExport.HasValue && isExactExport.Value == true) {
                                     sql.Append(" and Lower(a.Code) = {0}Code ");
                                     dbParameters.Add(db.CreateDbParameter("Code", sparepartcode.ToLower()));
                                } else { 
                                     sql.Append(" and Lower(a.Code) like {0}Code ");
                                     dbParameters.Add(db.CreateDbParameter("Code", "%" + sparepartcode.ToLower() + "%"));
                                }
                            } else {
                                for (int i = 0; i < spareparts.Length; i++) {
                                    spareparts[i] = "'" + spareparts[i] + "'";
                                }
                                string codes = string.Join(",", spareparts);
                                sql.Append(" and a.Code in (" + codes + ")");
                            }
                        }
                        if (!string.IsNullOrEmpty(name))
                        {
                            sql.Append(" and Lower(a.Name) like {0}Name ");
                            dbParameters.Add(db.CreateDbParameter("Name", "%" + name.ToLower() + "%"));
                        }
                        if (!string.IsNullOrEmpty(referenceCode))
                        {
                            sql.Append(" and Lower(a.referenceCode) like {0}referenceCode ");
                            dbParameters.Add(db.CreateDbParameter("referenceCode", "%" + referenceCode.ToLower() + "%"));
                        }
                        if (bPartCreateTime.HasValue)
                        {
                            sql.Append(@" and a.CreateTime >=to_date({0}bPartCreateTime,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = bPartCreateTime.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("bPartCreateTime", tempTime.ToString("G")));
                        }
                        if (ePartCreateTime.HasValue)
                        {
                            sql.Append(@" and a.CreateTime <=to_date({0}ePartCreateTime,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = ePartCreateTime.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("ePartCreateTime", tempTime.ToString("G")));
                        }
                        if (bCreateTime.HasValue)
                        {
                            sql.Append(@" and c.CreateTime >=to_date({0}bCreateTime,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = bCreateTime.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("bCreateTime", tempTime.ToString("G")));
                        }
                        if (eCreateTime.HasValue)
                        {
                            sql.Append(@" and c.CreateTime <=to_date({0}eCreateTime,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = eCreateTime.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("eCreateTime", tempTime.ToString("G")));
                        }
                        sql.Append(" )p ");
                        if(isprice.HasValue){
                            sql.Append(" where p.isprice = {0}isprice ");
                            if (isprice.Value)
                            {                                                     
                               dbParameters.Add(db.CreateDbParameter("isprice", 1));                          
                            }
                            else
                                dbParameters.Add(db.CreateDbParameter("isprice", 0));
                        }
                    }
                    var sqlStr = sql.ToString();
                    if (sqlStr.IndexOf(" where 1=1  and ") > 0)
                        sqlStr = sqlStr.Replace(" where 1=1  and ", " where ");
                    else
                        sqlStr = sqlStr.Replace(" where 1=1 ", "");

                    var command = db.CreateDbCommand(string.Format(sqlStr, db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    conn.Open();
                    var reader = command.ExecuteReader();
                    using (var excelExport = new ExcelExport(fileName))
                    {
                        excelExport.ExportByRow(index =>
                        {
                            if (index == 0)
                            {
                                return new object[] {
                                    ErrorStrings.Export_Title_SpareParts_CreateTime,ErrorStrings.Export_Title_PartsBranch_Code, ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName,ErrorStrings.Export_Title_Accessinfo_Tmp_ReferenceCode, ErrorStrings.Export_Title_FactoryPurchacePrice_SupplierCode, ErrorStrings.Export_Title_FactoryPurchacePrice_SupplierName, ErrorStrings.Export_Title_FactoryPurchacePrice_PurchasingPrice, 
                                    ErrorStrings.Export_Title_SpareParts_PurchacePrice,ErrorStrings.Export_Title_SpareParts_IsPrice,ErrorStrings.Export_Title_SpareParts_IsPrimary,ErrorStrings.Export_Title_FactoryPurchacePrice_ValidTo,ErrorStrings.Export_Title_AccountPeriod_CreatorName,ErrorStrings.Export_Title_AccountPeriod_ModifierName,ErrorStrings.Export_Title_AccountPeriod_ModifyTime

                                };
                            }
                            if (reader.Read())
                            {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if (num != reader.FieldCount)
                                {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
