﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        public bool ExportPartsReplacement(bool? isInterFace, bool? isSPM, string oldPartCode, string oldPartName, int? status, out string fileName) {
            fileName = GetExportFilePath("配件替换件信息_" + ".xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated) {
                    throw new Exception(ErrorStrings.File_Validation_User);
                }
                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {

                    conn.Open();
                    var sql = new StringBuilder();
                    sql.Append(@"select OldPartCode,
                                        OldPartName,OldMInAmount,
                                        NewPartCode,
                                        NewPartName,RepMInAmount,
                                        (select value from keyvalueitem where NAME = 'MasterData_Status'and key=Status) As Status,
                                        decode(IsSPM,1,'是','否')as IsSPM,
                                        decode(IsInterFace,1,'是','否')as IsInterFace,
                                        Remark,
                                        CreatorName,
                                        CreateTime,
                                        ModifierName,
                                        ModifyTime,
                                        AbandonerName,
                                        AbandonTime
                                        from PartsReplacement where 1=1 ");
                    var dbParameters = new List<DbParameter>();
                    if(!string.IsNullOrEmpty(oldPartCode)) {
                        sql.Append(@" and (oldPartCode like {0}oldPartCode or NewPartCode like {0}oldPartCode)");
                        dbParameters.Add(db.CreateDbParameter("oldPartCode", "%" + oldPartCode + "%"));

                    }
                    if(!string.IsNullOrEmpty(oldPartName)) {
                        sql.Append(@" and (oldPartName like {0}oldPartName or NewPartName like {0}oldPartName)");
                        dbParameters.Add(db.CreateDbParameter("oldPartName", "%" + oldPartName + "%"));
                    }

                    if(status.HasValue) {
                        sql.Append(@" and status ={0}Status");
                        dbParameters.Add(db.CreateDbParameter("status", status.Value));
                    }
                    if(isSPM.HasValue) {
                        sql.Append(@" and isSPM ={0}isSPM");
                        dbParameters.Add(db.CreateDbParameter("isSPM", isSPM.Value));
                    }
                    if(isInterFace.HasValue) {
                        sql.Append(@" and isInterFace ={0}isInterFace");
                        dbParameters.Add(db.CreateDbParameter("isInterFace", isInterFace.Value));
                    }
                    var sqlStr = sql.ToString();
                    if(sqlStr.IndexOf(" where 1=1  and ") > 0)
                        sqlStr = sqlStr.Replace(" where 1=1  and ", " where ");
                    else
                        sqlStr = sqlStr.Replace(" where 1=1 ", "");

                    var command = db.CreateDbCommand(string.Format(sqlStr, db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    ErrorStrings.Export_Title_PartsReplacement_OldPartCode,ErrorStrings.Export_Title_PartsReplacement_OldPartName,"原件最小销售数量",ErrorStrings.Export_Title_PartsReplacement_NewPartCode,ErrorStrings.Export_Title_PartsReplacement_NewPartName,"替换件最小销售数量",ErrorStrings.Export_Title_AccountPeriod_Status,"SPM是否应用","是否接口传输",ErrorStrings.Export_Title_PartsBranch_Remark,ErrorStrings.Export_Title_AccountPeriod_CreatorName,ErrorStrings.Export_Title_AccountPeriod_CreateTime,ErrorStrings.Export_Title_AccountPeriod_ModifierName,ErrorStrings.Export_Title_AccountPeriod_ModifyTime,ErrorStrings.Export_Title_PartsBranch_AbandonerName,ErrorStrings.Export_Title_PartsBranch_AbandonerTime
                                 };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }

                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }
        /// <summary>
        /// 导入配件替换信息
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="excelImportNum"></param>
        /// <param name="errorData"></param>
        /// <param name="errorDataFileName"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public bool ImportPartsReplacement(string fileName, out int excelImportNum, out List<PartsReplacementExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<PartsReplacementExtend>();
            var allList = new List<PartsReplacementExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("PartsReplacement", out notNullableFields, out fieldLenght);
                List<object> excelColumns;
                List<PartsReplacementExtend> rightList;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    #region 指定文件中的列对应的名称 例如 excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode, "PartCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsReplacement_OldPartCode, "OldPartCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsReplacement_OldPartName, "OldPartName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsReplacement_NewPartCode, "NewPartCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsReplacement_NewPartName, "NewPartName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Remark, "Remark");
                    #endregion
                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;
                    #region 获取对应枚举
                    var keyValuePairs = new[] { new KeyValuePair<string, string>("BaseDataStatus", "BaseData_Status") };
                    tempExcelOperator.ImportHelper.LoadEnumFromDb(null, null, keyValuePairs);

                    #endregion
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var tempImportObj = new PartsReplacementExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        #region 根据导入的字段给对应的实体属性赋值 tempImportObj.PartsSalesCategoryNameStr = row["PartsSalesCategoryName"].Trim();
                        tempImportObj.OldPartCodeStr = newRow["OldPartCode"];
                        tempImportObj.OldPartNameStr = newRow["OldPartName"];
                        tempImportObj.NewPartCodeStr = newRow["NewPartCode"];
                        tempImportObj.NewPartNameStr = newRow["NewPartName"];
                        tempImportObj.RemarkStr = newRow["Remark"];

                        #endregion

                        var tempErrorMessage = new List<string>();
                        #region 导入的数据基本检查
                        //原配件编号 检查
                        var fieldIndex = notNullableFields.IndexOf("OldPartCode".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.OldPartCodeStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsReplacement_OldPartCodeIsNull);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.OldPartCodeStr) > fieldLenght["OldPartCode".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsReplacement_OldPartCodeIsLong);
                        }

                        //原配件名称 检查
                        fieldIndex = notNullableFields.IndexOf("OldPartName".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.OldPartNameStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsReplacement_OldPartNameIsNull);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.OldPartNameStr) > fieldLenght["OldPartName".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsReplacement_OldPartNameIsLong);
                        }

                        //替换配件编号 检查
                        fieldIndex = notNullableFields.IndexOf("NewPartCode".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.NewPartNameStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsReplacement_NewPartCodeIsNull);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.NewPartNameStr) > fieldLenght["NewPartCode".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsReplacement_NewPartCodeIsLong);
                        }

                        //替换配件名称检查
                        fieldIndex = notNullableFields.IndexOf("NewPartName".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.NewPartNameStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsReplacement_NewPartNameIsNull);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.NewPartNameStr) > fieldLenght["NewPartName".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsReplacement_NewPartNameIsLong);
                        }

                        //备注检查
                        fieldIndex = notNullableFields.IndexOf("Remark".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.RemarkStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchasePricingDetail_Validation15);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.RemarkStr) > fieldLenght["Remark".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchasePricingDetail_Validation16);
                        }
                        #endregion
                        if(tempErrorMessage.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });
                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    #region 剩下的数据进行业务检查
                    //校验配件编号、配件名称组合,在配件信息中存在
                    var oldPartCodesNeedCheck = tempRightList.Select(r => r.OldPartCodeStr.ToUpper());
                    var newPartCodesNeedCheck = tempRightList.Select(r => r.NewPartCodeStr.ToUpper());
                    var partCodesNeedCheck = oldPartCodesNeedCheck.Concat(newPartCodesNeedCheck).Distinct().ToArray();
                    var dbSpareParts = new List<SparePartExtend>();
                    Func<string[], bool> getDbSparePartCodes = value => {
                        var dbObj = new SparePartExtend {
                            Id = Convert.ToInt32(value[0]),
                            Code = value[1],
                            Name = value[2],
                            MInPackingAmount = Convert.ToInt32(value[3])
                        };
                        dbSpareParts.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select Id,Trim(Code) as Code,Trim(Name),MInPackingAmount as Name from SparePart where status=1 ", "Code", true, partCodesNeedCheck, getDbSparePartCodes);
                    foreach(var tempRight in tempRightList) {
                        var oldSparePart = dbSpareParts.FirstOrDefault(v => v.Code == tempRight.OldPartCodeStr && v.Name == tempRight.OldPartNameStr);
                        if(oldSparePart == null) {
                            tempRight.ErrorMsg = String.Format(ErrorStrings.Export_Validation_PartsReplacement_SparePartsNotExist, tempRight.OldPartCodeStr, tempRight.OldPartNameStr);
                            continue;
                        }

                        var newSparePart = dbSpareParts.FirstOrDefault(v => v.Code == tempRight.NewPartCodeStr && v.Name == tempRight.NewPartNameStr);
                        if(newSparePart == null) {
                            tempRight.ErrorMsg = String.Format(ErrorStrings.Export_Validation_PartsReplacement_SparePartsNotExist, tempRight.NewPartCodeStr, tempRight.NewPartNameStr);
                            continue;
                        }
                        tempRight.OldPartId = oldSparePart.Id;
                        tempRight.OldMInAmount = oldSparePart.MInPackingAmount;
                        tempRight.NewPartId = newSparePart.Id;
                        tempRight.RepMInAmount = newSparePart.MInPackingAmount;
                    }

                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    //文件内数据重复
                    var groups = tempRightList.GroupBy(r => new {
                        r.OldPartId,
                        r.NewPartId
                    }).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        groupItem.ErrorMsg = ErrorStrings.ImpPartsSupplierRelation_Validation31;
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    //系统内数据重复校验
                    var oldPartIdStrsNeedCheck = tempRightList.Select(r => r.OldPartId.ToString(CultureInfo.InvariantCulture)).Distinct().ToArray();
                    var dbPartsReplacements = new List<PartsReplacementExtend>();
                    Func<string[], bool> getDbPartsReplacements = value => {
                        var dbObj = new PartsReplacementExtend {
                            OldPartId = Convert.ToInt32(value[0]),
                            NewPartId = Convert.ToInt32(value[1])
                        };
                        dbPartsReplacements.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select OldPartId,NewPartId from PartsReplacement where status=1 ", "OldPartId", false, oldPartIdStrsNeedCheck, getDbPartsReplacements);
                    foreach(var tempRight in tempRightList) {
                        var newSparePart = dbPartsReplacements.FirstOrDefault(v => v.OldPartId == tempRight.OldPartId && v.NewPartId == tempRight.NewPartId);
                        if(newSparePart != null) {
                            tempRight.ErrorMsg = String.Format(ErrorStrings.Export_Validation_PartsReplacement_HasExist, tempRight.OldPartId, tempRight.NewPartId);
                        }
                    }

                    #endregion
                    //获取所有不合格数据
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    //获取所有合格数据
                    rightList = allList.Except(errorList).ToList();
                    #region 将合格数据的值填上
                    foreach(var rightItem in rightList) {
                        rightItem.OldPartCode = rightItem.OldPartCodeStr;
                        rightItem.OldPartName = rightItem.OldPartNameStr;
                        rightItem.NewPartCode = rightItem.NewPartCodeStr;
                        rightItem.NewPartName = rightItem.NewPartNameStr;
                        rightItem.Status = tempExcelOperator.ImportHelper.GetEnumValue("BaseDataStatus", ErrorStrings.Export_Validation_PartsBranch_Used) ?? 0;
                        rightItem.Remark = rightItem.RemarkStr;
                    }
                    #endregion
                }
                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempObj = list[index - 1];
                            var values = new object[] {
                                #region 设置错误信息导出的列的值
                                tempObj.OldPartCodeStr,tempObj.OldPartNameStr,
                                tempObj.NewPartCodeStr,tempObj.NewPartNameStr,
                                tempObj.RemarkStr, tempObj.ErrorMsg
                                #endregion                     
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }

                //导入所有合格数据
                if(!rightList.Any())
                    return true;
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    //开启事务,新增更新配件在一个事务内
                    var ts = conn.BeginTransaction();
                    try {
                        //新增配件
                        if(rightList.Any()) {
                            #region 获取对应的插入的SQL 通过db.GetInsertSql方法
                            //获取新增数据的sql语句,Id为主键
                            var sqlInsert = db.GetInsertSql("PartsReplacement", "Id", new[] {
                                "OldPartId","OldPartCode","OldPartName","NewPartId","NewPartCode","NewPartName","Status","CreatorId","CreatorName","CreateTime","Remark","OldMInAmount","RepMInAmount","IsInterFace"
                            });

                            var sqlHistoryInsert = db.GetInsertSql("PartsReplacementHistory", "Id", new[] {
                                "PartsReplacementId","OldPartId","OldPartCode","OldPartName","NewPartId","NewPartCode","NewPartName","Status","CreatorId","CreatorName","CreateTime","Remark","OldMInAmount","RepMInAmount"
                            });
                            #endregion
                            var userInfo = Utils.GetCurrentUserInfo();
                            //往数据库增加配件信息
                            foreach(var item in rightList) {
                                #region 添加Sql的参数
                                var command = db.CreateDbCommand(sqlInsert, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("OldPartId", item.OldPartId));
                                command.Parameters.Add(db.CreateDbParameter("OldPartCode", item.OldPartCode));
                                command.Parameters.Add(db.CreateDbParameter("OldPartName", item.OldPartName));
                                command.Parameters.Add(db.CreateDbParameter("NewPartId", item.NewPartId));
                                command.Parameters.Add(db.CreateDbParameter("NewPartCode", item.NewPartCode));
                                command.Parameters.Add(db.CreateDbParameter("NewPartName", item.NewPartName));
                                command.Parameters.Add(db.CreateDbParameter("Status", item.Status));
                                command.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                command.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                command.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                command.Parameters.Add(db.CreateDbParameter("Remark", item.Remark));
                                command.Parameters.Add(db.CreateDbParameter("OldMInAmount", item.OldMInAmount));
                                command.Parameters.Add(db.CreateDbParameter("RepMInAmount", item.RepMInAmount));
                                command.Parameters.Add(db.CreateDbParameter("IsInterFace", false));
                                #endregion
                                var tempId = db.ExecuteInsert(command, "Id");
                                //加履历
                                var commandHistory = db.CreateDbCommand(sqlHistoryInsert, conn, ts);
                                commandHistory.Parameters.Add(db.CreateDbParameter("PartsReplacementId", tempId));
                                commandHistory.Parameters.Add(db.CreateDbParameter("OldPartId", item.OldPartId));
                                commandHistory.Parameters.Add(db.CreateDbParameter("OldPartCode", item.OldPartCode));
                                commandHistory.Parameters.Add(db.CreateDbParameter("OldPartName", item.OldPartName));
                                commandHistory.Parameters.Add(db.CreateDbParameter("NewPartId", item.NewPartId));
                                commandHistory.Parameters.Add(db.CreateDbParameter("NewPartCode", item.NewPartCode));
                                commandHistory.Parameters.Add(db.CreateDbParameter("NewPartName", item.NewPartName));
                                commandHistory.Parameters.Add(db.CreateDbParameter("Status", item.Status));
                                commandHistory.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                commandHistory.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                commandHistory.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                commandHistory.Parameters.Add(db.CreateDbParameter("Remark", item.Remark));
                                commandHistory.Parameters.Add(db.CreateDbParameter("OldMInAmount", item.OldMInAmount));
                                commandHistory.Parameters.Add(db.CreateDbParameter("RepMInAmount", item.RepMInAmount));
                                commandHistory.ExecuteNonQuery();
                            }
                        }
                        //无异常提交
                        ts.Commit();
                    } catch(Exception) {
                        //报错回滚
                        ts.Rollback();
                        throw;
                    } finally {
                        if(conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
            }
        }
    }
}
