﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Entities;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        public bool 导出储备系数(int[] ids, int? warehouseId, int? status, int? aBCStrategyId, int? reserveCoefficient, DateTime? bCreateTime, DateTime? eCreateTime, DateTime? bModifyTime, DateTime? eModifyTime, out string fileName) {
            fileName = GetExportFilePath("储备系数.xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);
                var userInfo = Utils.GetCurrentUserInfo();
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.Append(string.Format(@" select rf.warehousecode,
                                               rf.warehousename,
                                               (select value from keyvalueitem where name='ABCStrategy_Category' and key=rf.ABCStrategyId) as ABCStrategyId,
                                               rf.reservecoefficient,
                                               rf.status,
                                                (select value from keyvalueitem where name='BaseData_Status' and key=rf.Status) as Status,
                                               rf.creatorname,
                                               rf.createtime,
                                               rf.modifiername,
                                               rf.modifytime,
                                               rf.abandonername,
                                               rf.abandontime,
                                                ro.pricefloor,
                                               ro.pricecap,
                                               ro.lowerlimitcoefficient,
                                               ro.upperlimitcoefficient
                                               
                                          from ReserveFactorMasterOrder rf
                                          join ReserveFactorOrderDetail ro
                                            on rf.id = ro.reservefactormasterorderid
                                         where 1=1 "));

                    var dbParameters = new List<DbParameter>();
                    if(ids != null && ids.Length > 0) {
                        sql.Append(" and rf.id in (");
                        for(var i = 0; i < ids.Length; i++) {
                            if(ids.Length == i + 1) {
                                sql.Append("{0}" + ids[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter(ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            } else {
                                sql.Append("{0}" + ids[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter(ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                        }
                        sql.Append(")");
                    } else {

                        if(warehouseId.HasValue) {
                            sql.Append(" and rf.warehouseId =" + warehouseId);
                        }
                        if(status.HasValue) {
                            sql.Append(" and rf.status =" + status);
                        }
                        if(aBCStrategyId.HasValue) {
                            sql.Append(" and rf.aBCStrategyId =" + aBCStrategyId);
                        }
                        if(reserveCoefficient.HasValue) {
                            sql.Append(" and rf.reserveCoefficient =" + reserveCoefficient);
                        }
                        if(bCreateTime.HasValue) {
                            sql.Append(@" and rf.createtime >=To_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = bCreateTime.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(eCreateTime.HasValue) {
                            sql.Append(@" and rf.createtime <=To_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = eCreateTime.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                        if(bModifyTime.HasValue) {
                            sql.Append(@" and rf.ModifyTime >=To_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = bModifyTime.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(eModifyTime.HasValue) {
                            sql.Append(@" and rf.ModifyTime <=To_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = eModifyTime.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                    }

                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                   "仓库编号","仓库名称","配件属性","储备系数","状态","日期","创建人","创建时间","修改人","修改时间","作废人","作废时间","价格下限(服务站价)","价格上限","下限系数","上限系数"};
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();

                }
                return true;
            } catch(Exception) {
                return false;
            }
        }
        public bool Import储备系数清单(string fileName, out int excelImportNum, out List<ReserveFactorOrderDetailExtend> rightData, out List<ReserveFactorOrderDetailExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<ReserveFactorOrderDetailExtend>();
            var allList = new List<ReserveFactorOrderDetailExtend>();
            var rightList = new List<ReserveFactorOrderDetailExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("ReserveFactorOrderDetail", out notNullableFields, out fieldLenght);


                List<object> excelColumns;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    #region 指定文件中的列对应的名称 例如 excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Code, "PartCode");
                    excelOperator.AddColumnDataSource("价格上限", "PriceCap");
                    excelOperator.AddColumnDataSource("价格下限(服务站价)", "PriceFloor");
                    excelOperator.AddColumnDataSource("上限系数", "UpperLimitCoefficient");
                    excelOperator.AddColumnDataSource("下限系数", "LowerLimitCoefficient");
                    #endregion
                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    #region
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var tempImportObj = new ReserveFactorOrderDetailExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        #region 根据导入的字段给对应的实体属性赋值 tempImportObj.PartsSalesCategoryNameStr = row["PartsSalesCategoryName"].Trim();

                        tempImportObj.PriceCapStr = newRow["PriceCap"];
                        tempImportObj.PriceFloorStr = newRow["PriceFloor"];
                        tempImportObj.UpperLimitCoefficientStr = newRow["UpperLimitCoefficient"];
                        tempImportObj.LowerLimitCoefficientStr = newRow["LowerLimitCoefficient"];

                        #endregion

                        var tempErrorMessage = new List<string>();
                        #region 导入的数据基本检查
                        //价格上限检查
                        var fieldIndex = notNullableFields.IndexOf("PriceCap".ToUpper());
                        if(newRow["PriceCap"] == null) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add("价格上限不能为空");
                        } else {
                            Decimal price;
                            if(Decimal.TryParse(newRow["PriceCap"], out price)) {
                                if(price >= 0)
                                    tempImportObj.PriceCap = price;
                                else
                                    tempErrorMessage.Add("价格上限必须大于等于0");
                            } else {
                                tempErrorMessage.Add("价格上限只能为数字");
                            }
                        }
                        //价格下限(服务站价)检查
                        fieldIndex = notNullableFields.IndexOf("PriceFloor".ToUpper());
                        if(newRow["PriceFloor"] == null) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add("价格下限(服务站价)不能为空");
                        } else {
                            Decimal price;
                            if(Decimal.TryParse(newRow["PriceFloor"], out price)) {
                                if(price >= 0)
                                    tempImportObj.PriceFloor = price;
                                else
                                    tempErrorMessage.Add("价格下限(服务站价)必须大于等于0");
                            } else {
                                tempErrorMessage.Add("价格下限(服务站价)只能为数字");
                            }
                        }
                        //上限系数检查
                        fieldIndex = notNullableFields.IndexOf("UpperLimitCoefficient".ToUpper());
                        if(newRow["UpperLimitCoefficient"] == null) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add("上限系数不能为空");
                        } else {
                            Decimal price;
                            if(Decimal.TryParse(newRow["UpperLimitCoefficient"], out price)) {
                                if(price >= 0)
                                    tempImportObj.UpperLimitCoefficient = price;
                                else
                                    tempErrorMessage.Add("上限系数必须大于等于0");
                            } else {
                                tempErrorMessage.Add("上限系数只能为数字");
                            }
                        }
                        //下限系数检查
                        fieldIndex = notNullableFields.IndexOf("LowerLimitCoefficient".ToUpper());
                        if(newRow["LowerLimitCoefficient"] == null) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add("下限系数不能为空");
                        } else {
                            Decimal price;
                            if(Decimal.TryParse(newRow["LowerLimitCoefficient"], out price)) {
                                if(price >= 0)
                                    tempImportObj.LowerLimitCoefficient = price;
                                else
                                    tempErrorMessage.Add("下限系数必须大于等于0");
                            } else {
                                tempErrorMessage.Add("下限系数只能为数字");
                            }
                        }

                        #endregion
                        if(tempErrorMessage.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });
                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    #region 剩下的数据进行业务检查
                    ////文件内数据重复
                    var groups = tempRightList.GroupBy(r => new {
                        r.PriceCapStr,
                        r.UpperLimitCoefficientStr
                    }).Where(r => r.Count() > 1);
                    if(groups.Any()) {
                        foreach(var groupItem in groups.SelectMany(r => r)) {
                            if(groupItem.ErrorMsg == null) {
                                groupItem.ErrorMsg = ErrorStrings.Export_Validation_PartsPurchaseOrder_Validation2;
                            } else {
                                groupItem.ErrorMsg = groupItem.ErrorMsg + "," + ErrorStrings.Export_Validation_PartsPurchaseOrder_Validation2;
                            }
                        }
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    #endregion
                    //获取所有不合格数据
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    //获取所有合格数据
                    rightList = allList.Except(errorList).ToList();
                }
                    #endregion
                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempObj = list[index - 1];
                            var values = new object[] {
                        #region 设置错误信息导出的列的值
                        tempObj.PriceCapStr,
                        tempObj.PriceFloorStr,
                        tempObj.UpperLimitCoefficientStr,
                        tempObj.LowerLimitCoefficientStr,
                        tempObj.ErrorMsg
                        #endregion                     
                    };
                            return values;
                        });
                    }
                    errorList = null;
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }
    }
}
