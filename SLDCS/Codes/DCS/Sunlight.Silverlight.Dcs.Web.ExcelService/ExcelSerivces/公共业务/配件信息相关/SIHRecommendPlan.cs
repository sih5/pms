﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Entities;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        public bool ImportSIHRecommendPlan(string fileName, out int excelImportNum, int branchId, out List<SIHRecommendPlanExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<SIHRecommendPlanExtend>();
            var allList = new List<SIHRecommendPlanExtend>();
            var userInfo = Utils.GetCurrentUserInfo();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("SIHRecommendPlan", out notNullableFields, out fieldLenght);
                List<object> excelColumns;
                List<SIHRecommendPlanExtend> rightList;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    #region 指定文件中的列对应的名称 例如
                    excelOperator.AddColumnDataSource("结转时间", "CarryTime");
                    excelOperator.AddColumnDataSource("仓库编号", "WarehouseCode");
                    excelOperator.AddColumnDataSource("仓库名称", "WarehouseName");
                    excelOperator.AddColumnDataSource("配件编号", "SparePartCode");
                    excelOperator.AddColumnDataSource("配件名称", "SparePartName");
                    excelOperator.AddColumnDataSource("实际计划数量", "ActQty");
                    #endregion
                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var tempImportObj = new SIHRecommendPlanExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        #region 根据导入的字段给对应的实体属性赋值
						tempImportObj.CarryTimeStr = newRow["CarryTime"];
                        tempImportObj.WarehouseCodeStr = newRow["WarehouseCode"];
                        tempImportObj.WarehouseNameStr = newRow["WarehouseName"];
                        tempImportObj.SparePartCodeStr = newRow["SparePartCode"];
                        tempImportObj.SparePartNameStr = newRow["SparePartName"];
                        tempImportObj.ActQtyStr = newRow["ActQty"];
                        #endregion

                        var tempErrorMessage = new List<string>();
                        #region 导入的数据基本检查

                        //仓库编号检查
                        var fieldIndex = notNullableFields.IndexOf("WarehouseCode".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.WarehouseCodeStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add("仓库编号不能为空");
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.WarehouseCodeStr) > fieldLenght["WarehouseCode".ToUpper()])
                                tempErrorMessage.Add("仓库编号过长");
                        }
						 //配件编号检查
                         fieldIndex = notNullableFields.IndexOf("SparePartCode".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.SparePartCodeStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add("配件编号不能为空");
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.SparePartCodeStr) > fieldLenght["SparePartCode".ToUpper()])
                                tempErrorMessage.Add("配件编号过长");
                            else tempImportObj.SparePartCode = tempImportObj.SparePartCodeStr;
                        }
                        //时间检查
                        fieldIndex = notNullableFields.IndexOf("CarryTime".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.CarryTimeStr)) {
                            tempErrorMessage.Add("结转日期不能为空");
                        }
                        fieldIndex = notNullableFields.IndexOf("CarryTime".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.CarryTimeStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add("结转日期不能为空");
                        } else {
                            try {
                                DateTime checkValue;
                                if(!DateTime.TryParse(tempImportObj.CarryTimeStr, out checkValue)) {
                                    tempErrorMessage.Add("结转日期格式不对");
                                } else {
                                    tempImportObj.CarryTime = DateTime.Parse(tempImportObj.CarryTimeStr);
                                }
                            } catch(Exception) {
                                tempErrorMessage.Add("日期格式不对");
                            }
                        }
						 //实际计划量检查
                         fieldIndex = notNullableFields.IndexOf("ActQty".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.ActQtyStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add("实际计划量不能为空");
                        } else {
                            int qty;
                            if (int.TryParse(tempImportObj.ActQtyStr, out qty)) {
                                if (qty >= 0)
                                    tempImportObj.ActQty = qty;
                                else
                                    tempErrorMessage.Add("实际计划量必须大于等于0");
                            } else {
                                tempErrorMessage.Add("实际计划量必须是数字");
                            }
                        }
                        #endregion
                        if(tempErrorMessage.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });
                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    #region 剩下的数据进行业务检查
                    //4、文件中结转时间，仓库，配件组合唯一，否则提示：数据重复
                    var groups = tempRightList.GroupBy(r => new {
                        r.WarehouseCodeStr,
						r.SparePartCodeStr,
                        r.CarryTimeStr
                    }).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        groupItem.ErrorMsg = ErrorStrings.ImpPartsSupplierRelation_Validation31;
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    //仓库编号存在于仓库表中，否则提示：仓库编号不存在
                    var WarehouseCodesNeedCheck = tempRightList.Select(r => r.WarehouseCodeStr).Distinct().ToArray();
                    var dbWarehouses = new List<WarehouseExtend>();
                    Func<string[], bool> getdbWarehouses = value => {
                        var dbObj = new WarehouseExtend {
                            Id = Convert.ToInt32(value[0]),
                            Code = value[1],
                            Name = value[2]
                        };
                        dbWarehouses.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select Id,Code,Name from Warehouse where status=1 ", "Code", false, WarehouseCodesNeedCheck, getdbWarehouses);
                    foreach(var tempRight in tempRightList) {
                        var repairItem = dbWarehouses.FirstOrDefault(v => v.Code == tempRight.WarehouseCodeStr);
                        if(repairItem == null) {
                            tempRight.ErrorMsg = ErrorStrings.Export_Validation_Company_WarehouseCodeIsNull;
                            continue;
                        }
                        tempRight.WarehouseId = repairItem.Id;
                        tempRight.WarehouseName = repairItem.Name;
                        tempRight.WarehouseCode = repairItem.Code;
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    var carryTimes = tempRightList.Select(t => t.WarehouseCodeStr).Distinct().ToArray();
                    var dbSIHRecommendPlanExtends = new List<SIHRecommendPlanExtend>();
                    Func<string[], bool> getSIHRecommendPlanExtends = value => {
                        var dbObj = new SIHRecommendPlanExtend {
                            Id = Convert.ToInt32(value[0]),
                            SalesPrice =Convert.ToDecimal( value[1]),
                            SparePartCode = value[2],
                            WarehouseId = Convert.ToInt32(value[3]),
                            CarryTime=DateTime.Parse(value[4])
                        };
                        dbSIHRecommendPlanExtends.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select Id,SalesPrice,SparePartCode ,WarehouseId,CarryTime from SIHRecommendPlan where 1=1 ", "WarehouseCode", false, carryTimes, getSIHRecommendPlanExtends);
                    foreach(var tempRight in tempRightList) {
                        var repairItem = dbSIHRecommendPlanExtends.FirstOrDefault(v => v.WarehouseId == tempRight.WarehouseId && tempRight.CarryTime == v.CarryTime && v.SparePartCode == tempRight.SparePartCode);
                        if(repairItem == null) {
                            tempRight.ErrorMsg = "推荐计划信息不存在";
                            continue;
                        }
                        tempRight.Id = repairItem.Id;
                        tempRight.SalesPrice = repairItem.SalesPrice;
                    }
                    #endregion
                    //获取所有不合格数据
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    //获取所有合格数据
                    rightList = allList.Except(errorList).ToList();
                }
                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempObj = list[index - 1];
                            var values = new object[] {
                                #region 设置错误信息导出的列的值
                                tempObj.CarryTimeStr,
                                tempObj.WarehouseCodeStr,tempObj.WarehouseNameStr,
                                tempObj.SparePartCodeStr,tempObj.SparePartNameStr,tempObj.ActQtyStr,tempObj.ErrorMsg
                                
                                #endregion                     
                            };
                            return values;
                        });
                    }
                }


                //导入所有合格数据
                if(!rightList.Any())
                    return true;
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    //开启事务，新增更新配件在一个事务内
                    var ts = conn.BeginTransaction();
                    try {
                        if(rightList.Any()) {
                            var sqlUpdate = db.GetUpdateSql("SIHRecommendPlan", new[] {
                                "ActQty", "ActQtyFee"
                            }, new[] {
                                "Id"
                            });
                           
                            //往数据库增加配件信息
                            foreach(var item in rightList) {
                                var command = db.CreateDbCommand(sqlUpdate, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("ActQty", item.ActQty));
                                command.Parameters.Add(db.CreateDbParameter("ActQtyFee", item.ActQty*item.SalesPrice));
                                command.Parameters.Add(db.CreateDbParameter("Id", item.Id));
                                command.ExecuteNonQuery();
                            }
                        }
                        //无异常提交
                        ts.Commit();
                    } catch(Exception) {
                        //报错回滚
                        ts.Rollback();
                        throw;
                    } finally {
                        if(conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
            }
        }
    }
}
