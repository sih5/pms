﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Configuration;
using Microsoft.VisualBasic;
using NPOI.SS.Formula.Functions;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Entities;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        public bool ExportNoGoldenTaxClassify(int[] ids, string code, string name,string ReferenceCode, int? status, DateTime? createDateTimeStart, DateTime? createDateTimeEnd, DateTime? modifyDateTimeStart, DateTime? modifyDateTimeEnd, out string fileName) {
            fileName = GetExportFilePath("配件信息无金税分类_" + ".xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated) {
                    throw new Exception(ErrorStrings.File_Validation_User);
                }
                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {

                    conn.Open();
                    var sql = new StringBuilder();
                    sql.Append(@"select    Code,
                                           Name,
                                           ReferenceCode,
                                           --ProductBrand,
                                           GoldenTaxClassifyCode,
                                           GoldenTaxClassifyName,
                                           --StandardCode,
                                           --StandardName,
                                           --CADCode,
                                           --Specification,
                                           EnglishName,
                                           (select value from keyvalueitem where NAME = 'SparePart_PartType'and key=PartType) As PartType,
                                           MeasureUnit,
                                           MInPackingAmount,
                                           --LastSubstitute,
                                           --NextSubstitute,
                                           Weight,
                                           Volume,
                                           Feature,
                                           --TotalNumber,
                                           --Factury,
                                           IsOriginal,
                                           --CategoryCode,
                                           --CategoryName,
                                           PinyinCode,
                                           Material,
                                           DeclareElement,
                                           CreatorName,
                                           CreateTime,
                                           ModifierName,
                                           ModifyTime,
                                           AbandonerName,
                                           AbandonTime
                                      from sparepart
                                     where ( GoldenTaxClassifyId is null or  GoldenTaxClassifyCode is null or  GoldenTaxClassifyName is null)");
                    var dbParameters = new List<DbParameter>();
                    if(ids != null && ids.Length > 0) {
                        sql.Append(" and id in (");
                        for(var i = 0; i < ids.Length; i++) {
                            if(ids.Length == i + 1) {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            } else {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                        }
                        sql.Append(")");
                    } else {
                        if(!string.IsNullOrEmpty(code)) {
                            var codes = code.Split(',');
                            if (code.Length == 1) {
                                sql.Append(@" and LOWER(code) like {0}code ");
                                dbParameters.Add(db.CreateDbParameter("code", "%" + code.ToLower() + "%"));
                            } else {
                                for (int i =0;i < codes.Length; i++){
                                    codes[i] = "'"+codes[i]+"'";
                                }
                                sql.Append(@" and code in (" + string.Join(",", codes) + ")");
                            }
                        }
                        if(!string.IsNullOrEmpty(name)) {
                            sql.Append(@" and LOWER(name) like {0}name ");
                            dbParameters.Add(db.CreateDbParameter("name", "%" + name.ToLower() + "%"));
                        }
                        if (!string.IsNullOrEmpty(ReferenceCode))
                        {
                            sql.Append(@" and LOWER(ReferenceCode) like {0}ReferenceCode ");
                            dbParameters.Add(db.CreateDbParameter("ReferenceCode", "%" + ReferenceCode.ToLower() + "%"));
                        }

                        if (status.HasValue) {
                            sql.Append(@" and status ={0}Status");
                            dbParameters.Add(db.CreateDbParameter("status", status.Value));
                        }
                        if(createDateTimeStart.HasValue) {
                            sql.Append(@" and createTime >=To_date({0}createDateTimeStart,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createDateTimeStart.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createDateTimeStart", tempTime.ToString("G")));
                        }
                        if(createDateTimeEnd.HasValue) {
                            sql.Append(@" and createTime <=To_date({0}createDateTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createDateTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createDateTimeEnd", tempTime.ToString("G")));
                        }
                        if(modifyDateTimeStart.HasValue) {
                            sql.Append(@" and ModifyTime >=To_date({0}modifyDateTimeStart,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = modifyDateTimeStart.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("modifyDateTimeStart", tempTime.ToString("G")));
                        }
                        if(modifyDateTimeEnd.HasValue) {
                            sql.Append(@" and ModifyTime <=To_date({0}modifyDateTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = modifyDateTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("modifyDateTimeEnd", tempTime.ToString("G")));
                        }

                    }
                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    ErrorStrings.Export_Title_PartsBranch_Code, ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName, ErrorStrings.Export_Title_Accessinfo_Tmp_ReferenceCode, /*ErrorStrings.Export_Title_Sparepart_ProductBrand,*/ ErrorStrings.Export_Title_GoldenTaxClassifyMsg_GoldenTaxClassifyCode, ErrorStrings.Export_Title_GoldenTaxClassifyMsg_GoldenTaxClassifyName, /*ErrorStrings.Export_Title_Sparepart_StandardCode, "产品执行标准名称 ", ErrorStrings.Export_Title_Sparepart_CADCode, "规格型号 ",*/ 
                                    ErrorStrings.Export_Title_PartsBranch_EnglishName, ErrorStrings.Export_Title_Sparepart_PartsType, ErrorStrings.Export_Title_PackingPropertyApp_MeasureUnit, ErrorStrings.Export_Title_PackingPropertyApp_Minsalequantity, /*ErrorStrings.Export_Title_Sparepart_LastSubstitute, ErrorStrings.Export_Title_Sparepart_NextSubstitute,*/ ErrorStrings.Export_Title_PackingPropertyApp_Weight, ErrorStrings.Export_Title_PackingPropertyApp_Volume, ErrorStrings.Export_Title_PartsBranch_Feature,/* ErrorStrings.Export_Title_Sparepart_TotalNumber, ErrorStrings.Export_Title_Sparepart_Factury,*/ 
                                    ErrorStrings.Export_Title_Sparepart_IsOriginal, /*ErrorStrings.Export_Title_Sparepart_CategoryCode, ErrorStrings.Export_Title_Sparepart_CategoryName,*/ErrorStrings.Export_Title_Sparepart_PinyinCode, ErrorStrings.Export_Title_Sparepart_Material, ErrorStrings.Export_Title_Sparepart_DeclareElement,  ErrorStrings.Export_Title_AccountPeriod_CreatorName, ErrorStrings.Export_Title_AccountPeriod_CreateTime, ErrorStrings.Export_Title_AccountPeriod_ModifierName, ErrorStrings.Export_Title_AccountPeriod_ModifyTime, ErrorStrings.Export_Title_PartsBranch_AbandonerName, ErrorStrings.Export_Title_PartsBranch_AbandonerTime
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }

                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception ex) {
                throw ex;
            }
        }


        public bool ExportSparePart(int[] ids, string code, string name, int? status, DateTime? createDateTimeStart, DateTime? createDateTimeEnd, DateTime? modifyDateTimeStart, DateTime? modifyDateTimeEnd, string ReferenceCode, string OverseasPartsFigure, string exchangeIdentification, string exGroupCode,bool? isExactExport, out string fileName) {
            fileName = GetExportFilePath("配件信息_" + ".xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated) {
                    throw new Exception(ErrorStrings.File_Validation_User);
                }
                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {

                    conn.Open();
                    var sql = new StringBuilder();
                    sql.Append(@"select    Code,
                                           Name,
                                          case when  pb.isorderable = 1 then cast ('是' as varchar2(100)) else cast ('否' as varchar2(100)) end as isorderable,
                                          case when  pb.issalable = 1 then cast ('是' as varchar2(100)) else cast ('否' as varchar2(100)) end as issalable,
                                           s.ReferenceCode,
                                           ProductBrand,
                                           GoldenTaxClassifyCode,
                                           GoldenTaxClassifyName,
                                           --StandardCode,
                                           --StandardName,
                                           --CADCode,
                                           --Specification,
                                           EnglishName,
                                           (select value from keyvalueitem where NAME = 'SparePart_PartType'and key=PartType) As PartType,
                                           MeasureUnit,
                                           MInPackingAmount,
                                           --LastSubstitute,
                                           --NextSubstitute,
                                           Weight,
                                           Volume,
                                           Feature,
                                           --TotalNumber,
                                           --Factury,
                         (select value from keyvalueitem where NAME = 'IsOrNot'and key=IsOriginal) As IsOriginal,
                                           --CategoryCode,
                                            --CategoryName,
                                           PinyinCode,
                                           Material,
                                           DeclareElement,
                                            (select value from keyvalueitem where NAME = 'SparePart_KeyCode' and key=s.AssemblyKeyCode) As AssemblyKeyCode,
                                          (select value from keyvalueitem where NAME = 'TraceProperty' and key=s.TraceProperty) As TraceProperty,
                                           s.SafeDays,s.WarehousDays,s.TemDays,
                                           s.CreatorName,
                                           s.CreateTime,
                                           s.ModifierName,
                                           s.ModifyTime,
                                           s.AbandonerName,
                                           s.AbandonTime,
                                           (select value from keyvalueitem where NAME = 'MasterData_Status' and key=s.Status) As Status,
                                     case when  s.IsSupplierPutIn = 1 then cast ('是' as varchar2(100)) else cast ('否' as varchar2(100)) end as IsSupplierPutIn
                                      from sparepart s left join PartsExchangeGroup p on s.ExchangeIdentification=p.ExchangeCode and p.Status=1 
                                      left join partsbranch pb on pb.partid = s.id and pb.status =1  
                                      where 1=1 ");
                    var dbParameters = new List<DbParameter>();
                    if(ids != null && ids.Length > 0) {
                        sql.Append(" and s.id in (");
                        for(var i = 0; i < ids.Length; i++) {
                            if(ids.Length == i + 1) {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            } else {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                        }
                        sql.Append(")");
                    } else {
                        if(!string.IsNullOrEmpty(code)) {
                            var codes = code.Split(',');
                            if (codes.Length == 1) {
                                if (isExactExport.HasValue && isExactExport.Value == true) {
                                    sql.Append(@" and LOWER(code) ={0}code ");
                                    dbParameters.Add(db.CreateDbParameter("code", code.ToLower()));
                                } else {
                                    sql.Append(@" and LOWER(code) like {0}code ");
                                    dbParameters.Add(db.CreateDbParameter("code", "%" + code.ToLower() + "%"));
                                }
                            } else {
                                for (int i =0;i < codes.Length; i++){
                                    codes[i] = "'"+codes[i]+"'";
                                }
                                sql.Append(@" and code in (" + string.Join(",", codes) + ")");
                            }
                        }
                        if(!string.IsNullOrEmpty(exGroupCode)) {
                            sql.Append(@" and LOWER(p.ExGroupCode) like {0}exGroupCode ");
                            dbParameters.Add(db.CreateDbParameter("exGroupCode", "%" + exGroupCode.ToLower() + "%"));
                        }
                        if(!string.IsNullOrEmpty(exchangeIdentification)) {
                            sql.Append(@" and LOWER(ExchangeIdentification) like {0}exchangeIdentification ");
                            dbParameters.Add(db.CreateDbParameter("exchangeIdentification", "%" + exchangeIdentification.ToLower() + "%"));
                        }
                        if(!string.IsNullOrEmpty(name)) {
                            sql.Append(@" and LOWER(name) like {0}name ");
                            dbParameters.Add(db.CreateDbParameter("name", "%" + name.ToLower() + "%"));
                        }

                        if(status.HasValue) {
                            sql.Append(@" and s.status ={0}Status");
                            dbParameters.Add(db.CreateDbParameter("status", status.Value));
                        }
                        if(createDateTimeStart.HasValue) {
                            sql.Append(@" and s.createTime >=To_date({0}createDateTimeStart,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createDateTimeStart.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createDateTimeStart", tempTime.ToString("G")));
                        }
                        if(createDateTimeEnd.HasValue) {
                            sql.Append(@" and s.createTime <=To_date({0}createDateTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createDateTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createDateTimeEnd", tempTime.ToString("G")));
                        }
                        if(modifyDateTimeStart.HasValue) {
                            sql.Append(@" and s.ModifyTime >=To_date({0}modifyDateTimeStart,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = modifyDateTimeStart.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("modifyDateTimeStart", tempTime.ToString("G")));
                        }
                        if(modifyDateTimeEnd.HasValue) {
                            sql.Append(@" and s.ModifyTime <=To_date({0}modifyDateTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = modifyDateTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("modifyDateTimeEnd", tempTime.ToString("G")));
                        }

                        if(!string.IsNullOrEmpty(ReferenceCode)) {
                            sql.Append(@" and LOWER(ReferenceCode) like {0}ReferenceCode ");
                            dbParameters.Add(db.CreateDbParameter("ReferenceCode", "%" + ReferenceCode.ToLower() + "%"));
                        }
                        if(!string.IsNullOrEmpty(OverseasPartsFigure)) {
                            sql.Append(@" and LOWER(OverseasPartsFigure) like {0}OverseasPartsFigure ");
                            dbParameters.Add(db.CreateDbParameter("OverseasPartsFigure", "%" + OverseasPartsFigure.ToLower() + "%"));
                        }

                    }
                    var sqlStr = sql.ToString();
                    if(sqlStr.IndexOf(" where 1=1  and ") > 0)
                        sqlStr = sqlStr.Replace(" where 1=1  and ", " where ");
                    else
                        sqlStr = sqlStr.Replace(" where 1=1 ", "");
                    var command = db.CreateDbCommand(string.Format(sqlStr, db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    ErrorStrings.Export_Title_PartsBranch_Code, ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName,ErrorStrings.Export_Title_PartsBranch_IsOrderableNew,ErrorStrings.Export_Title_PartsBranch_IsSalableNew, ErrorStrings.Export_Title_Accessinfo_Tmp_ReferenceCode,ErrorStrings.Export_Title_Sparepart_OverseasPartsCode, ErrorStrings.Export_Title_GoldenTaxClassifyMsg_GoldenTaxClassifyCode, ErrorStrings.Export_Title_GoldenTaxClassifyMsg_GoldenTaxClassifyName,/* ErrorStrings.Export_Title_Sparepart_StandardCode, ErrorStrings.Export_Title_Sparepart_StandardName, ErrorStrings.Export_Title_Sparepart_CADCode, "规格型号 ", */ErrorStrings.Export_Title_PartsBranch_EnglishName,ErrorStrings.Export_Title_Sparepart_PartsType, ErrorStrings.Export_Title_PackingPropertyApp_MeasureUnit, ErrorStrings.Export_Title_PackingPropertyApp_Minsalequantity, /*ErrorStrings.Export_Title_Sparepart_LastSubstitute, ErrorStrings.Export_Title_Sparepart_NextSubstitute,*/ ErrorStrings.Export_Title_PackingPropertyApp_Weight, ErrorStrings.Export_Title_PackingPropertyApp_Volume, ErrorStrings.Export_Title_PartsBranch_Feature,/* ErrorStrings.Export_Title_Sparepart_TotalNumber, ErrorStrings.Export_Title_Sparepart_Factury,*/ ErrorStrings.Export_Title_Sparepart_IsOriginal, /*ErrorStrings.Export_Title_Sparepart_CategoryCode, ErrorStrings.Export_Title_Sparepart_CategoryName,*/ErrorStrings.Export_Title_Sparepart_PinyinCode, ErrorStrings.Export_Title_Sparepart_Material, ErrorStrings.Export_Title_Sparepart_DeclareElement,ErrorStrings.Export_Title_Sparepart_AssemblyKeyCode,"追溯属性","安全天数最大值","库房天数","临时天数", ErrorStrings.Export_Title_AccountPeriod_CreatorName, ErrorStrings.Export_Title_AccountPeriod_CreateTime, ErrorStrings.Export_Title_AccountPeriod_ModifierName, ErrorStrings.Export_Title_AccountPeriod_ModifyTime, ErrorStrings.Export_Title_PartsBranch_AbandonerName, ErrorStrings.Export_Title_PartsBranch_AbandonerTime, ErrorStrings.Export_Title_AccountPeriod_Status,ErrorStrings.Export_Title_PartsBranch_IsSupplierPutIn
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }

                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception ex) {
                throw ex;
            }
        }

        private string strconvsparecode(string sparecode) {
            string reslut = sparecode.Replace("—", "-").Replace("【", "[").Replace("￥", "$").Replace("……", "^").Replace("×", "*").Replace("】", "]").Replace("＼", @"\").Replace("《", "<").Replace("》", ">").Replace("＃", "#").Replace("｛", "{").Replace("｝", "}");
            return reslut;
        }

        public bool ImportSparepart(string fileName, out int excelImportNum, out List<SparePartExtend> rightData, out List<SparePartExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<SparePartExtend>();
            var rightList = new List<SparePartExtend>();
            var allList = new List<SparePartExtend>();
            var dbpartsExchangeGroups = new List<PartsExchangeGroupExtend>();
            var dbpartsExchanges = new List<PartsExchangeGroupExtend>();
            var goldenTaxClassifys = new List<GoldenTaxClassifyMsgExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("SparePart", out notNullableFields, out fieldLenght);
                List<object> excelColumns;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Code, "Code");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName, "Name");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Referencecode, "ReferenceCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsExchangeGroupExtend_ExchangeCode, "ExchangeIdentification");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_GoldenTaxClassifyMsg_GoldenTaxClassifyCode, "GoldenTaxClassifyCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_GoldenTaxClassifyMsg_GoldenTaxClassifyName, "GoldenTaxClassifyName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Sparepart_StandardCode, "StandardCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Sparepart_StandardName, "StandardName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Sparepart_OverseasPartsCode, "OverseasPartsFigure");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Sparepart_ProductBrand, "ProductBrand");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Sparepart_IMSCompressionNumber, "IMSCompressionNumber");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Sparepart_IMSManufacturerNumber, "IMSManufacturerNumber");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Sparepart_CADCode, "CADCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Sparepart_Specification, "Specification");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_EnglishName, "EnglishName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Sparepart_PartsType, "PartType");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PackingPropertyApp_MeasureUnit, "MeasureUnit");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Sparepart_MInPackingAmount, "MInPackingAmount");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Sparepart_ShelfLife, "ShelfLife");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Sparepart_GroupABCCategory, "GroupABCCategory");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Sparepart_LastSubstitute, "LastSubstitute");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Sparepart_NextSubstitute, "NextSubstitute");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PackingPropertyApp_Weight, "Weight");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Sparepart_Volume, "Volume");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Feature, "Feature");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Sparepart_TotalNumber, "TotalNumber");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Sparepart_Factury, "Factury");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Sparepart_IsOriginal, "IsOriginal");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Sparepart_IsNotWarrantyTransfer, "IsNotWarrantyTransfer");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Sparepart_CategoryCode, "CategoryCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Sparepart_CategoryName, "CategoryName");

                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;
                    var keyValuePairs = new[] {
                        new KeyValuePair<string, string>("PartType", "SparePart_PartType"), new KeyValuePair<string, string>("IsOriginal", "IsOrNot"), new KeyValuePair<string, string>("MeasureUnit", "SparePart_MeasureUnit"), new KeyValuePair<string, string>("Status", "MasterData_Status")
                    };
                    tempExcelOperator.ImportHelper.LoadEnumFromDb(null, null, keyValuePairs);
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var sparePart = new SparePartExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        sparePart.CodeStr = Strings.StrConv(newRow["Code"], VbStrConv.Narrow).Replace("【", "[");
                        sparePart.CodeStr = strconvsparecode(sparePart.CodeStr);
                        sparePart.Code = Strings.StrConv(newRow["Code"], VbStrConv.Narrow).Replace("【", "[").ToUpper();
                        sparePart.Code = strconvsparecode(sparePart.Code);
                        sparePart.Code = sparePart.Code.ToUpper(); //全部大写
                        sparePart.Name = newRow["Name"];
                        sparePart.ReferenceCode = newRow["ReferenceCode"];
                        sparePart.OverseasPartsFigure = newRow["OverseasPartsFigure"];
                        sparePart.ProductBrand = newRow["ProductBrand"];
                        sparePart.IMSCompressionNumber = newRow["IMSCompressionNumber"];
                        sparePart.IMSManufacturerNumber = newRow["IMSManufacturerNumber"];
                        sparePart.CADCode = newRow["CADCode"];
                        sparePart.Specification = newRow["Specification"];
                        sparePart.EnglishName = newRow["EnglishName"];
                        sparePart.PartTypeStr = newRow["PartType"];
                        sparePart.MeasureUnit = newRow["MeasureUnit"];
                        sparePart.LastSubstitute = newRow["LastSubstitute"];
                        sparePart.IsNotWarrantyTransferStr = newRow["IsNotWarrantyTransfer"];
                        sparePart.NextSubstitute = newRow["NextSubstitute"];
                        sparePart.WeightStr = newRow["Weight"];
                        sparePart.Feature = newRow["Feature"];
                        sparePart.TotalNumber = newRow["TotalNumber"];
                        sparePart.Factury = newRow["Factury"];
                        //sparePart.IsOriginalStr = newRow["IsOriginal"];
                        sparePart.CategoryCode = newRow["CategoryCode"];
                        sparePart.CategoryName = newRow["CategoryName"];

                        sparePart.ExchangeIdentification = newRow["ExchangeIdentification"];
                        sparePart.MInPackingAmountStr = newRow["MInPackingAmount"];
                        sparePart.GroupABCCategoryStr = newRow["GroupABCCategory"];
                        sparePart.VolumeStr = newRow["Volume"];
                        sparePart.CategoryName = newRow["CategoryName"];
                        sparePart.GoldenTaxClassifyCode = newRow["GoldenTaxClassifyCode"];
                        sparePart.GoldenTaxClassifyName = newRow["GoldenTaxClassifyName"];
                        sparePart.StandardCode = newRow["StandardCode"];
                        sparePart.StandardName = newRow["StandardName"];
                        var tempErrorMessage = new List<string>();
                        //配件编号检查
                        var fieldIndex = notNullableFields.IndexOf("CODE");
                        if(string.IsNullOrEmpty(sparePart.Code)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.OverstockPartsAppDetail__Validation1);
                        } else {
                            //配件长度不得超过25
                            if(Encoding.Default.GetByteCount(sparePart.Code) > 25) //fieldLenght["CODE"]
                                tempErrorMessage.Add(ErrorStrings.ImpPartsSupplierRelation_Validation8);
                        }
                        //海外配件图号检查
                        fieldIndex = notNullableFields.IndexOf("OVERSEASPARTSFIGURE");
                        if(!string.IsNullOrEmpty(sparePart.OverseasPartsFigure)) {
                            //配件长度不得超过25
                            if(Encoding.Default.GetByteCount(sparePart.OverseasPartsFigure) > 18) //fieldLenght["CODE"]
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_OverseasPartsFigureIsLong);
                        }
                        //配件名称检查
                        fieldIndex = notNullableFields.IndexOf("NAME");
                        if(string.IsNullOrEmpty(sparePart.Name)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Sparepart_Name);
                        } else {
                            if(Encoding.Default.GetByteCount(sparePart.Name) > fieldLenght["NAME"])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Sparepart_NameIsLong);
                        }
                        //计量单位检查
                        fieldIndex = notNullableFields.IndexOf("MeasureUnit".ToUpper());
                        if(string.IsNullOrEmpty(sparePart.MeasureUnit)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Sparepart_MeasureUnitIsNull);
                        } else {
                            var checkMeasureUnit = tempExcelOperator.ImportHelper.GetEnumValue("MeasureUnit", sparePart.MeasureUnit);
                            if(!checkMeasureUnit.HasValue)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Sparepart_MeasureUnitValueError);
                        }
                        //配件类型检查
                        fieldIndex = notNullableFields.IndexOf("PartType".ToUpper());
                        if(string.IsNullOrEmpty(row["PartType"])) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Sparepart_PartTypeIsNull);
                        } else {
                            sparePart.PartTypeStr = row["PartType"]; //"];
                            var partType = tempExcelOperator.ImportHelper.GetEnumValue("PartType", sparePart.PartTypeStr);
                            if(!partType.HasValue) {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Sparepart_PartTypeValueError);
                            } else {
                                sparePart.PartType = partType.Value;
                            }
                        }
                        //保质期检查
                        fieldIndex = notNullableFields.IndexOf("ShelfLife".ToUpper());
                        if(string.IsNullOrEmpty(row["ShelfLife"])) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Sparepart_ShelfLifeIsNull);
                        } else {
                            try {
                                var shelfLife = Convert.ToInt32(row["ShelfLife"]);
                                sparePart.ShelfLife = shelfLife;
                            } catch(Exception) {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Sparepart_ShelfLifeIsInteger);
                            }
                        }
                        ////最小包装数量默认为1
                        //sparePart.MInPackingAmount = 1;

                        //重量检查
                        fieldIndex = notNullableFields.IndexOf("Weight".ToUpper());
                        if(string.IsNullOrEmpty(row["Weight"])) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Sparepart_WeightIsNull);
                        } else {
                            try {
                                var weight = Convert.ToDecimal(row["Weight"]);
                                sparePart.Weight = weight;
                            } catch(Exception) {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Sparepart_WeightIsNumber);
                            }
                        }

                        fieldIndex = notNullableFields.IndexOf("IsNotWarrantyTransfer".ToUpper());
                        if(string.IsNullOrEmpty(row["IsNotWarrantyTransfer"])) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Sparepart_IsNotWarrantyTransferIsNull);
                        } else {
                            var partType = tempExcelOperator.ImportHelper.GetEnumValue("IsOriginal", sparePart.IsNotWarrantyTransferStr);
                            if(!partType.HasValue) {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Sparepart_IsNotWarrantyTransferValueError);
                            } else {
                                sparePart.PartType = partType.Value;
                            }
                        }


                        //最小包装数量
                        if(!string.IsNullOrEmpty(row["MInPackingAmount"])) {
                            try {
                                var minPackingAmount = Convert.ToInt32(row["MInPackingAmount"]);
                                sparePart.MInPackingAmount = minPackingAmount;
                            } catch(Exception) {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Sparepart_MInPackingAmountIsNumber);
                            }
                        } else {
                            sparePart.MInPackingAmount = 1;
                        }

                        //集团ABC类别
                        if(!string.IsNullOrEmpty(row["GroupABCCategory"])) {
                            try {
                                var groupABCCategory = Convert.ToInt32(row["GroupABCCategory"]);
                                sparePart.GroupABCCategory = groupABCCategory;
                            } catch(Exception) {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Sparepart_GroupABCCategoryIsNumber);
                            }
                        }
                        //体积检查
                        if(!string.IsNullOrEmpty(row["Volume"])) {
                            try {
                                var volume = Convert.ToDecimal(row["Volume"]);
                                sparePart.Volume = volume;
                            } catch(Exception) {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Sparepart_VolumeIsNumber);
                            }
                        }
                        //是否原厂件

                        if(!string.IsNullOrEmpty(row["IsOriginal"])) {
                            var isOriginal = tempExcelOperator.ImportHelper.GetEnumValue("IsOriginal", row["IsOriginal"]);
                            if(!isOriginal.HasValue) {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Sparepart_IsOriginalValueError);
                            } else {
                                sparePart.IsOriginal = Convert.ToBoolean(isOriginal.Value);
                            }
                        }

                        if(!string.IsNullOrEmpty(sparePart.EnglishName)) {
                            //英文名称检查
                            if(Encoding.Default.GetByteCount(sparePart.EnglishName) > 100) {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Sparepart_EnglishNameOverLong);
                            }
                        }

                        if(string.IsNullOrEmpty(sparePart.GoldenTaxClassifyCode)) {
                            tempErrorMessage.Add(ErrorStrings.Export_Validation_Sparepart_GoldenTaxClassifyCodeIsNull);
                        } else {
                            if(Encoding.Default.GetByteCount(sparePart.GoldenTaxClassifyCode) > 50)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Sparepart_GoldenTaxClassifyCodeIsLong);
                        }

                        //if(string.IsNullOrEmpty(sparePart.StandardCode)) {
                        //    tempErrorMessage.Add("产品执行标准代码不能为空");
                        //}

                        if(string.IsNullOrEmpty(sparePart.GoldenTaxClassifyName)) {
                            tempErrorMessage.Add(ErrorStrings.Export_Validation_Sparepart_GoldenTaxClassifyNameIsNull);
                        } else {
                            if(Encoding.Default.GetByteCount(sparePart.GoldenTaxClassifyName) > 50)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Sparepart_GoldenTaxClassifyNameIsLong);
                        }
                        if(tempErrorMessage.Count > 0) {
                            sparePart.ErrorMsg = string.Join("; ", tempErrorMessage);
                        } else {
                            var tempStatus = tempExcelOperator.ImportHelper.GetEnumValue("Status", ErrorStrings.Export_Validation_PartsBranch_Used);
                            if(tempStatus.HasValue) {
                                sparePart.Status = tempStatus.Value;
                            }
                        }
                        allList.Add(sparePart);
                        return false;
                    });
                    //校验导入的数据配件编号重复
                    var groups = allList.GroupBy(r => r.Code).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        if(groupItem.ErrorMsg == null) {
                            groupItem.ErrorMsg = ErrorStrings.Export_Title_PartsBranch_Code + groupItem.Code + ErrorStrings.Export_Validation_PartsBranch_Validation5;
                        } else {
                            groupItem.ErrorMsg = groupItem.ErrorMsg + ErrorStrings.Export_Validation_PartsBranch_Validation6 + groupItem.Code + ErrorStrings.Export_Validation_PartsBranch_Validation5;
                        }
                    }
                    //var sparePartsNeedCheck = allList.Where(r => r.ErrorMsg == null).ToList();//
                    var sparePartsNeedCheck = allList.ToList();
                    var partCodesNeedCheck = sparePartsNeedCheck.Select(r => r.Code).Distinct().ToArray();
                    var dbSparePartCodes = new List<string>();
                    Func<string[], bool> getDbSparePartCodes = vales => {
                        dbSparePartCodes.Add(vales[0]);
                        return false;
                    };
                    //根据SQL查询数据库，将查出来的结果根据定义的委托中的方法（这里是getDbSparePartCodes）放到具体的集合中（这里是dbSparePartCodes）
                    db.QueryDataWithInOperator("select Code from sparePart where 1=1 ", "Code", true, partCodesNeedCheck, getDbSparePartCodes);
                    if(partCodesNeedCheck.Length > 0) {
                        var sparePartsExistsCode = sparePartsNeedCheck.Where(r => dbSparePartCodes.Any(v => v == r.Code)).ToArray();
                        foreach(var item in sparePartsExistsCode) {
                            if(item.ErrorMsg == null) {
                                item.ErrorMsg = ErrorStrings.Export_Validation_PartsBranch_Validation8 + item.Code + ErrorStrings.Export_Validation_PartsBranch_Validation7;
                            } else {
                                item.ErrorMsg = item.ErrorMsg + ErrorStrings.Export_Validation_PartsBranch_Validation9 + item.Code + ErrorStrings.Export_Validation_PartsBranch_Validation7;
                            }
                        }
                    }
                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    //查找对应的互换组
                    var partsExchangeGroups = tempRightList.Select(r => r.ExchangeIdentification).Distinct().ToArray();

                    Func<string[], bool> getDbpartsExchangeGroups = values => {
                        var item = new PartsExchangeGroupExtend {
                            Id = int.Parse(values[0]),
                            ExGroupCode = values[1].ToString(),
                            ExchangeCode = values[2].ToString()
                        };
                        dbpartsExchangeGroups.Add(item);
                        return false;
                    };
                    db.QueryDataWithInOperator("select Id,ExGroupCode,ExchangeCode from PartsExchangeGroup where Status=1", "ExchangeCode", false, partsExchangeGroups, getDbpartsExchangeGroups);


                    //查找对应的互换信息

                    Func<string[], bool> getDbpartsExchanges = values => {
                        var item = new PartsExchangeGroupExtend {
                            Id = int.Parse(values[0]),
                            ExchangeCode = values[1].ToString()
                        };
                        dbpartsExchanges.Add(item);
                        return false;
                    };
                    db.QueryDataWithInOperator("select Id,ExchangeCode from PartsExchange where Status=1", "ExchangeCode", false, partsExchangeGroups, getDbpartsExchanges);
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    if(tempRightList.Count > 0) {
                        //金税编码业务判断
                        using(var conn = db.CreateDbConnection()) {
                            conn.Open();
                            var goldenTaxClassifyCode = string.Join("','", tempRightList.Select(v => v.GoldenTaxClassifyCode).ToArray());
                            var goldenTaxClassifyName = string.Join("','", tempRightList.Select(v => v.GoldenTaxClassifyName).ToArray());
                            var standardCode = string.Join("','", tempRightList.Select(v => v.StandardCode).ToArray());
                            var sql = string.Format(@" select GoldenTaxClassifyCode,GoldenTaxClassifyName,Id from GoldenTaxClassifyMsg where GoldenTaxClassifyCode in ('{0}') and GoldenTaxClassifyName in ('{1}')", goldenTaxClassifyCode, goldenTaxClassifyName);
                            var command = db.CreateDbCommand(sql, conn, null);
                            var goldenTaxClassifyInfos = command.ExecuteReader();

                            while(goldenTaxClassifyInfos.Read()) {
                                goldenTaxClassifys.Add(new GoldenTaxClassifyMsgExtend {
                                    GoldenTaxClassifyCode = goldenTaxClassifyInfos[0].ToString(),
                                    GoldenTaxClassifyName = goldenTaxClassifyInfos[1].ToString(),
                                    Id = int.Parse(goldenTaxClassifyInfos[2].ToString())
                                });
                            }

                            List<SparePartExtend> s = new List<SparePartExtend>();
                            sql = string.Format(@" select StandardCode,StandardName, Id from ProductStandard where StandardCode in ('{0}')", standardCode);
                            var commandsc = db.CreateDbCommand(sql, conn, null);
                            var StandardCode = commandsc.ExecuteReader();

                            while(StandardCode.Read()) {
                                s.Add(new SparePartExtend {
                                    StandardCode = StandardCode[0].ToString(),
                                    StandardName = StandardCode[1].ToString(),
                                });
                            }


                            foreach(var tempRight in from tempRight in tempRightList
                                                     let code = goldenTaxClassifys.FirstOrDefault(v => v.GoldenTaxClassifyCode == tempRight.GoldenTaxClassifyCode && v.GoldenTaxClassifyName == tempRight.GoldenTaxClassifyName)
                                                     where code == null
                                                     select tempRight) {
                                if(tempRight.ErrorMsg == null) {
                                    tempRight.ErrorMsg = ErrorStrings.Export_Validation_Sparepart_GoldenTaxClassifyNotMatch;
                                } else {
                                    tempRight.ErrorMsg = tempRight.ErrorMsg + ";" + ErrorStrings.Export_Validation_Sparepart_GoldenTaxClassifyNotMatch; ;
                                }
                            }


                            foreach(var item in allList.Where(r => r.ErrorMsg == null)) {
                                if(!string.IsNullOrEmpty(item.StandardCode)) {
                                    var standard = s.FirstOrDefault(v => v.StandardCode == item.StandardCode);
                                    if(standard != null) {
                                        item.StandardName = standard.StandardName;
                                    } else {
                                        if(item.ErrorMsg == null) {
                                            item.ErrorMsg = ErrorStrings.Export_Validation_Sparepart_StandardCodeNotMatch;
                                        } else {
                                            item.ErrorMsg = item.ErrorMsg + ";" + ErrorStrings.Export_Validation_Sparepart_StandardCodeNotMatch;
                                        }
                                    }
                                }
                            }


                            conn.Close();
                        }
                    }
                }
                //获取所有不合格数据
                errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                //获取所有合格数据
                rightList = allList.Except(errorList).ToList();



                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempSparePart = list[index - 1];
                            var values = new object[] {
                                tempSparePart.CodeStr, tempSparePart.Name, tempSparePart.ReferenceCode, tempSparePart.ExchangeIdentification, tempSparePart.GoldenTaxClassifyCode, tempSparePart.GoldenTaxClassifyName, tempSparePart.StandardCode, tempSparePart.StandardName, tempSparePart.OverseasPartsFigure, tempSparePart.ProductBrand, tempSparePart.IMSCompressionNumber, tempSparePart.IMSManufacturerNumber, tempSparePart.CADCode, tempSparePart.Specification, tempSparePart.EnglishName, tempSparePart.PartTypeStr, tempSparePart.MeasureUnit, tempSparePart.MInPackingAmountStr, tempSparePart.ShelfLife, tempSparePart.GroupABCCategoryStr, tempSparePart.LastSubstitute, tempSparePart.NextSubstitute, tempSparePart.WeightStr, tempSparePart.VolumeStr, tempSparePart.Feature, tempSparePart.TotalNumber, tempSparePart.Factury, tempSparePart.IsOriginal, tempSparePart.CategoryCode,tempSparePart.IsNotWarrantyTransferStr, tempSparePart.CategoryName, tempSparePart.ErrorMsg
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }

                //导入所有合格数据
                if(!rightList.Any())
                    return true;
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    //开启事务，新增更新配件在一个事务内

                    //新增配件
                    if(rightList.Any()) {

                        //获取新增数据的sql语句，Id为主键
                        var sqlInsert = db.GetInsertSql("SparePart", "Id", new[] {
                          "IsNotWarrantyTransfer", "Code", "Name", "ReferenceCode", "ExchangeIdentification", "OverseasPartsFigure", "ProductBrand", "IMSCompressionNumber", "IMSManufacturerNumber", "CADCode", "Specification", "EnglishName", "PartType", "MeasureUnit", "ShelfLife", "LastSubstitute", "NextSubstitute", "Weight", "Volume", "GroupABCCategory", "Feature", "TotalNumber", "Factury", "IsOriginal", "CategoryCode", "CategoryName", "status", "CreatorId", "CreatorName", "CreateTime", "MInPackingAmount", "GoldenTaxClassifyCode", "GoldenTaxClassifyName", "StandardCode", "StandardName", "GoldenTaxClassifyId","OMSparePartMark"
                        });
                        var sqlInsertHistory = db.GetInsertSql("SparePartHistory", "Id", new[] {
                         "IsNotWarrantyTransfer",   "SparePartId", "Code", "ReferenceCode", "ExchangeIdentification", "OverseasPartsFigure", "IMSCompressionNumber", "IMSManufacturerNumber", "Name", "CADCode", "Specification", "EnglishName", "PartType", "MeasureUnit", "ShelfLife", "LastSubstitute", "NextSubstitute", "Weight", "Volume", "GroupABCCategory", "Feature", "TotalNumber", "Factury", "IsOriginal", "CategoryCode", "CategoryName", "status", "CreatorId", "CreatorName", "CreateTime", "MInPackingAmount", "GoldenTaxClassifyCode", "GoldenTaxClassifyName", "StandardCode", "StandardName", "GoldenTaxClassifyId"
                        });
                        var sqlInsertPartsExchange = db.GetInsertSql("PartsExchange", "Id", new[] {
                            "ExchangeCode", "ExchangeName", "PartId", "Status", "Remark", "CreatorId", "CreatorName", "CreateTime"
                        });
                        var sqlInsertPartsExchangeHistory = db.GetInsertSql("PartsExchangeHistory", "Id", new[] {
                            "PartsExchangeId", "ExchangeCode", "ExchangeName", "PartId", "Status", "Remark", "CreatorId", "CreatorName", "CreateTime"
                        });
                        var sqlInsertPartsExchangeGroup = db.GetInsertSql("PartsExchangeGroup", "Id", new[] {
                            "ExGroupCode", "ExchangeCode", "Status", "CreatorId", "CreatorName", "CreateTime"
                        });
                        var userInfo = Utils.GetCurrentUserInfo();
                        var ts = conn.BeginTransaction();
                        try {
                            //往数据库增加配件信息
                            foreach(var item in rightList) {
                                var goldenTaxClassify = goldenTaxClassifys.Where(r => r.GoldenTaxClassifyCode == item.GoldenTaxClassifyCode && r.GoldenTaxClassifyName == item.GoldenTaxClassifyName).FirstOrDefault();
                                var command = db.CreateDbCommand(sqlInsert, conn, ts);
                                var code = Strings.StrConv(item.Code, VbStrConv.Narrow).Replace("【", "[");
                                command.Parameters.Add(db.CreateDbParameter("Code", code));
                                command.Parameters.Add(db.CreateDbParameter("Name", item.Name));
                                command.Parameters.Add(db.CreateDbParameter("ReferenceCode", item.ReferenceCode));
                                command.Parameters.Add(db.CreateDbParameter("IsNotWarrantyTransfer", item.IsNotWarrantyTransfer));
                                command.Parameters.Add(db.CreateDbParameter("ExchangeIdentification", item.ExchangeIdentification));
                                command.Parameters.Add(db.CreateDbParameter("Volume", item.Volume));
                                command.Parameters.Add(db.CreateDbParameter("GroupABCCategory", item.GroupABCCategory));
                                command.Parameters.Add(db.CreateDbParameter("OverseasPartsFigure", item.OverseasPartsFigure));
                                command.Parameters.Add(db.CreateDbParameter("ProductBrand", item.ProductBrand));
                                command.Parameters.Add(db.CreateDbParameter("IMSManufacturerNumber", item.IMSManufacturerNumber));
                                command.Parameters.Add(db.CreateDbParameter("IMSCompressionNumber", item.IMSCompressionNumber));
                                command.Parameters.Add(db.CreateDbParameter("CADCode", item.CADCode));
                                command.Parameters.Add(db.CreateDbParameter("Specification", item.Specification));
                                command.Parameters.Add(db.CreateDbParameter("EnglishName", item.EnglishName));
                                command.Parameters.Add(db.CreateDbParameter("PartType", item.PartType));
                                command.Parameters.Add(db.CreateDbParameter("MeasureUnit", item.MeasureUnit));
                                command.Parameters.Add(db.CreateDbParameter("ShelfLife", item.ShelfLife));
                                command.Parameters.Add(db.CreateDbParameter("LastSubstitute", item.LastSubstitute));
                                command.Parameters.Add(db.CreateDbParameter("NextSubstitute", item.NextSubstitute));
                                command.Parameters.Add(db.CreateDbParameter("Weight", item.Weight));
                                command.Parameters.Add(db.CreateDbParameter("Feature", item.Feature));
                                command.Parameters.Add(db.CreateDbParameter("TotalNumber", item.TotalNumber));
                                command.Parameters.Add(db.CreateDbParameter("Factury", item.Factury));
                                command.Parameters.Add(db.CreateDbParameter("IsOriginal", item.IsOriginal));
                                command.Parameters.Add(db.CreateDbParameter("CategoryCode", item.CategoryCode));
                                command.Parameters.Add(db.CreateDbParameter("CategoryName", item.CategoryName));
                                command.Parameters.Add(db.CreateDbParameter("Status", item.Status));
                                command.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                command.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                command.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                command.Parameters.Add(db.CreateDbParameter("MInPackingAmount", item.MInPackingAmount));
                                command.Parameters.Add(db.CreateDbParameter("GoldenTaxClassifyId", goldenTaxClassify == null ? 0 : goldenTaxClassify.Id));
                                command.Parameters.Add(db.CreateDbParameter("GoldenTaxClassifyCode", item.GoldenTaxClassifyCode));
                                command.Parameters.Add(db.CreateDbParameter("GoldenTaxClassifyName", item.GoldenTaxClassifyName));
                                command.Parameters.Add(db.CreateDbParameter("StandardCode", item.StandardCode));
                                command.Parameters.Add(db.CreateDbParameter("StandardName", item.StandardName));
                                command.Parameters.Add(db.CreateDbParameter("OMSparePartMark", 0));
                                var tempId = db.ExecuteInsert(command, "Id");
                                //往数据库增加履历
                                var commandHistory = db.CreateDbCommand(sqlInsertHistory, conn, ts);
                                commandHistory.Parameters.Add(db.CreateDbParameter("SparePartId", tempId));
                                commandHistory.Parameters.Add(db.CreateDbParameter("Code", code));
                                commandHistory.Parameters.Add(db.CreateDbParameter("IsNotWarrantyTransfer", item.IsNotWarrantyTransfer));
                                commandHistory.Parameters.Add(db.CreateDbParameter("Name", item.Name));
                                commandHistory.Parameters.Add(db.CreateDbParameter("ReferenceCode", item.ReferenceCode));
                                commandHistory.Parameters.Add(db.CreateDbParameter("ExchangeIdentification", item.ExchangeIdentification));
                                commandHistory.Parameters.Add(db.CreateDbParameter("Volume", item.Volume));
                                commandHistory.Parameters.Add(db.CreateDbParameter("GroupABCCategory", item.GroupABCCategory));
                                commandHistory.Parameters.Add(db.CreateDbParameter("OverseasPartsFigure", item.OverseasPartsFigure));
                                commandHistory.Parameters.Add(db.CreateDbParameter("IMSManufacturerNumber", item.IMSManufacturerNumber));
                                commandHistory.Parameters.Add(db.CreateDbParameter("IMSCompressionNumber", item.IMSCompressionNumber));
                                commandHistory.Parameters.Add(db.CreateDbParameter("CADCode", item.CADCode));
                                commandHistory.Parameters.Add(db.CreateDbParameter("Specification", item.Specification));
                                commandHistory.Parameters.Add(db.CreateDbParameter("EnglishName", item.EnglishName));
                                commandHistory.Parameters.Add(db.CreateDbParameter("PartType", item.PartType));
                                commandHistory.Parameters.Add(db.CreateDbParameter("MeasureUnit", item.MeasureUnit));
                                commandHistory.Parameters.Add(db.CreateDbParameter("ShelfLife", item.ShelfLife));
                                commandHistory.Parameters.Add(db.CreateDbParameter("LastSubstitute", item.LastSubstitute));
                                commandHistory.Parameters.Add(db.CreateDbParameter("NextSubstitute", item.NextSubstitute));
                                commandHistory.Parameters.Add(db.CreateDbParameter("Weight", item.Weight));
                                commandHistory.Parameters.Add(db.CreateDbParameter("Feature", item.Feature));
                                commandHistory.Parameters.Add(db.CreateDbParameter("TotalNumber", item.TotalNumber));
                                commandHistory.Parameters.Add(db.CreateDbParameter("Factury", item.Factury));
                                commandHistory.Parameters.Add(db.CreateDbParameter("IsOriginal", item.IsOriginal));
                                commandHistory.Parameters.Add(db.CreateDbParameter("CategoryCode", item.CategoryCode));
                                commandHistory.Parameters.Add(db.CreateDbParameter("CategoryName", item.CategoryName));
                                commandHistory.Parameters.Add(db.CreateDbParameter("Status", item.Status));
                                commandHistory.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                commandHistory.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                commandHistory.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                commandHistory.Parameters.Add(db.CreateDbParameter("MInPackingAmount", item.MInPackingAmount));
                                commandHistory.Parameters.Add(db.CreateDbParameter("GoldenTaxClassifyId", goldenTaxClassify == null ? 0 : goldenTaxClassify.Id));
                                commandHistory.Parameters.Add(db.CreateDbParameter("GoldenTaxClassifyCode", item.GoldenTaxClassifyCode));
                                commandHistory.Parameters.Add(db.CreateDbParameter("GoldenTaxClassifyName", item.GoldenTaxClassifyName));
                                commandHistory.Parameters.Add(db.CreateDbParameter("StandardCode", item.StandardCode));
                                commandHistory.Parameters.Add(db.CreateDbParameter("StandardName", item.StandardName));
                                commandHistory.ExecuteNonQuery();
                                if(!string.IsNullOrEmpty(item.ExchangeIdentification)) {
                                    var commandPartsExchange = db.CreateDbCommand(sqlInsertPartsExchange, conn, ts);
                                    commandPartsExchange.Parameters.Add(db.CreateDbParameter("ExchangeCode", item.ExchangeIdentification));
                                    commandPartsExchange.Parameters.Add(db.CreateDbParameter("ExchangeName", ""));
                                    commandPartsExchange.Parameters.Add(db.CreateDbParameter("PartId", tempId));
                                    commandPartsExchange.Parameters.Add(db.CreateDbParameter("Status", 1));
                                    commandPartsExchange.Parameters.Add(db.CreateDbParameter("Remark", ErrorStrings.Export_Validation_Sparepart_RemarkDefault));
                                    commandPartsExchange.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                    commandPartsExchange.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                    commandPartsExchange.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                    var exchangeId = db.ExecuteInsert(commandPartsExchange, "Id");
                                    var commandPartsExchangeHistory = db.CreateDbCommand(sqlInsertPartsExchangeHistory, conn, ts);
                                    commandPartsExchangeHistory.Parameters.Add(db.CreateDbParameter("PartsExchangeId", exchangeId));
                                    commandPartsExchangeHistory.Parameters.Add(db.CreateDbParameter("ExchangeCode", item.ExchangeIdentification));
                                    commandPartsExchangeHistory.Parameters.Add(db.CreateDbParameter("ExchangeName", ""));
                                    commandPartsExchangeHistory.Parameters.Add(db.CreateDbParameter("PartId", tempId));
                                    commandPartsExchangeHistory.Parameters.Add(db.CreateDbParameter("Status", 1));
                                    commandPartsExchangeHistory.Parameters.Add(db.CreateDbParameter("Remark", ErrorStrings.Export_Validation_Sparepart_RemarkDefault));
                                    commandPartsExchangeHistory.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                    commandPartsExchangeHistory.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                    commandPartsExchangeHistory.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                    commandPartsExchangeHistory.ExecuteNonQuery();
                                }
                            }
                            if(rightList.Any()) {
                                rightList = rightList.Where(r => !string.IsNullOrEmpty(r.ExchangeIdentification)).ToList();
                                if(rightList.Any()) {
                                    var groups = rightList.GroupBy(r => r.ExchangeIdentification).Where(r => r.Count() > 1);
                                    var distinctMany = groups.SelectMany(r => r).ToList();
                                    distinctMany = distinctMany.Where((x, i) => distinctMany.FindIndex(z => z.ExchangeIdentification == x.ExchangeIdentification) == i).ToList();
                                    foreach(var groupitem in distinctMany) {
                                        var partsExchangeGroup = dbpartsExchangeGroups.Where(r => r.ExchangeCode == groupitem.ExchangeIdentification);
                                        if(!partsExchangeGroup.Any()) {
                                            var commandPartsExchangeGroup = db.CreateDbCommand(sqlInsertPartsExchangeGroup, conn, ts);
                                            var exgroupcode = CodeGenerator.Generate(db, "PartsExchange");
                                            commandPartsExchangeGroup.Parameters.Add(db.CreateDbParameter("ExGroupCode", exgroupcode));
                                            commandPartsExchangeGroup.Parameters.Add(db.CreateDbParameter("ExchangeCode", groupitem.ExchangeIdentification));
                                            commandPartsExchangeGroup.Parameters.Add(db.CreateDbParameter("Status", 1));
                                            commandPartsExchangeGroup.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                            commandPartsExchangeGroup.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                            commandPartsExchangeGroup.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                            commandPartsExchangeGroup.ExecuteNonQuery();
                                        }
                                    }

                                    var groupsOne = rightList.GroupBy(r => r.ExchangeIdentification).Where(r => r.Count() == 1);
                                    var onelist = groupsOne.SelectMany(r => r).ToList();
                                    foreach(var groupitem in onelist) {
                                        var partsExchangeGroup = dbpartsExchangeGroups.Where(r => r.ExchangeCode == groupitem.ExchangeIdentification);
                                        if(!partsExchangeGroup.Any()) {
                                            var partsExchange = dbpartsExchanges.Where(r => r.ExchangeCode == groupitem.ExchangeIdentification);
                                            if(partsExchange.Any()) {
                                                var commandPartsExchangeGroup = db.CreateDbCommand(sqlInsertPartsExchangeGroup, conn, ts);
                                                var exgroupcode = CodeGenerator.Generate(db, "PartsExchange");
                                                commandPartsExchangeGroup.Parameters.Add(db.CreateDbParameter("ExGroupCode", exgroupcode));
                                                commandPartsExchangeGroup.Parameters.Add(db.CreateDbParameter("ExchangeCode", groupitem.ExchangeIdentification));
                                                commandPartsExchangeGroup.Parameters.Add(db.CreateDbParameter("Status", 1));
                                                commandPartsExchangeGroup.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                                commandPartsExchangeGroup.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                                commandPartsExchangeGroup.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                                commandPartsExchangeGroup.ExecuteNonQuery();
                                            }
                                        }
                                    }
                                }
                            }
                            ts.Commit();
                            ts.Dispose();
                        } catch(Exception ex) {
                            //报错回滚
                            ts.Rollback();
                            throw new Exception(ex.Message);
                        }
                    }
                    if(conn.State == System.Data.ConnectionState.Open) {
                        conn.Close();
                    }
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }


        public bool 批量替换保外调拨(string fileName, out int excelImportNum, out List<SparePartExtend> rightData, out List<SparePartExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<SparePartExtend>();
            var rightList = new List<SparePartExtend>();
            var allList = new List<SparePartExtend>();
            var sparePartExtends = new List<SparePartExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("SparePart", out notNullableFields, out fieldLenght);
                List<object> excelColumns;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Code, "Code");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Sparepart_IsNotWarrantyTransfer, "IsNotWarrantyTransfer");
                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;
                    var keyValuePairs = new[] {
                        new KeyValuePair<string, string>("IsNotWarrantyTransfer", "IsOrNot")
                    };
                    tempExcelOperator.ImportHelper.LoadEnumFromDb(null, null, keyValuePairs);

                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var sparePart = new SparePartExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        sparePart.CodeStr = Strings.StrConv(newRow["Code"], VbStrConv.Narrow).Replace("【", "[");
                        sparePart.CodeStr = strconvsparecode(sparePart.CodeStr);
                        sparePart.Code = Strings.StrConv(newRow["Code"], VbStrConv.Narrow).Replace("【", "[");
                        sparePart.Code = strconvsparecode(sparePart.Code);
                        sparePart.Code = newRow["Code"];
                        sparePart.IsNotWarrantyTransferStr = newRow["IsNotWarrantyTransfer"];

                        var tempErrorMessage = new List<string>();
                        //配件编号检查
                        var fieldIndex = notNullableFields.IndexOf("CODE");
                        if(string.IsNullOrEmpty(sparePart.Code)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.OverstockPartsAppDetail__Validation1);
                        } else {
                            //配件长度不得超过25
                            if(Encoding.Default.GetByteCount(sparePart.Code) > 25)
                                tempErrorMessage.Add(ErrorStrings.ImpPartsSupplierRelation_Validation8);
                        }
                        //是否保外调拨
                        fieldIndex = notNullableFields.IndexOf("IsNotWarrantyTransfer".ToUpper());
                        if(string.IsNullOrEmpty(row["IsNotWarrantyTransfer"])) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Sparepart_IsNotWarrantyTransferIsNull);
                        } else {
                            var isNotWarrantyTransfer = tempExcelOperator.ImportHelper.GetEnumValue("IsNotWarrantyTransfer", sparePart.IsNotWarrantyTransferStr);
                            if(!isNotWarrantyTransfer.HasValue) {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Sparepart_IsNotWarrantyTransferValueError);
                            } else {
                                sparePart.IsNotWarrantyTransfer = isNotWarrantyTransfer.Value;
                            }
                        }
                        allList.Add(sparePart);
                        return false;
                    });
                    //校验导入的数据配件编号重复
                    var groups = allList.GroupBy(r => r.Code).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        if(groupItem.ErrorMsg == null) {
                            groupItem.ErrorMsg = ErrorStrings.Export_Title_PartsBranch_Code + groupItem.Code + ErrorStrings.Export_Validation_PartsBranch_Validation5;
                        } else {
                            groupItem.ErrorMsg = groupItem.ErrorMsg + ErrorStrings.Export_Validation_PartsBranch_Validation6 + groupItem.Code + ErrorStrings.Export_Validation_PartsBranch_Validation5;
                        }
                    }
                    var sparePartsNeedCheck = allList.ToList();
                    var partCodesNeedCheck = sparePartsNeedCheck.Select(r => r.Code).Distinct().ToArray();
                    Func<string[], bool> getDbSparePartInfo = vales => {
                        sparePartExtends.Add(new SparePartExtend {
                            Id = Convert.ToInt32(vales[0]),
                            Code = vales[1],
                            ReferenceCode = vales[3],
                            Name = vales[2],
                            LastSubstitute = vales[4],
                            NextSubstitute = vales[5],
                            ShelfLife = string.IsNullOrEmpty(vales[6]) ? 0 : Convert.ToInt32(vales[6]),
                            EnglishName = vales[7],
                            PinyinCode = vales[8],
                            //ReferenceCode = vales[9],
                            ReferenceName = vales[10],
                            CADCode = vales[11],
                            CADName = vales[12],
                            PartType = string.IsNullOrEmpty(vales[13]) ? 0 : Convert.ToInt32(vales[13]),
                            Specification = vales[14],
                            Feature = vales[15],
                            Status = string.IsNullOrEmpty(vales[16]) ? 0 : Convert.ToInt32(vales[16]),
                            Length = string.IsNullOrEmpty(vales[17]) ? 0 : Convert.ToDecimal(vales[17]),
                            Width = string.IsNullOrEmpty(vales[18]) ? 0 : Convert.ToDecimal(vales[18]),
                            Height = string.IsNullOrEmpty(vales[19]) ? 0 : Convert.ToDecimal(vales[19]),
                            Volume = string.IsNullOrEmpty(vales[20]) ? 0 : Convert.ToDecimal(vales[20]),
                            Weight = string.IsNullOrEmpty(vales[21]) ? 0 : Convert.ToDecimal(vales[21]),
                            Material = vales[22],
                            PackingAmount = string.IsNullOrEmpty(vales[23]) ? 0 : Convert.ToInt32(vales[23]),
                            PackingSpecification = vales[24],
                            PartsOutPackingCode = vales[25],
                            PartsInPackingCode = vales[26],
                            MeasureUnit = vales[27],
                            MInPackingAmount = string.IsNullOrEmpty(vales[28]) ? 0 : Convert.ToInt32(vales[28]),
                            IsNotWarrantyTransfer = string.IsNullOrEmpty(vales[29]) ? 0 : Convert.ToInt32(vales[29]),
                            ExchangeIdentification = vales[30],
                        });
                        return false;
                    };
                    db.QueryDataWithInOperator(@"select ID,CODE,NAME,ReferenceCode,LASTSUBSTITUTE,NEXTSUBSTITUTE,SHELFLIFE,ENGLISHNAME,PINYINCODE,REFERENCECODE,REFERENCENAME,CADCODE,CADNAME,PARTTYPE,SPECIFICATION,FEATURE,STATUS,LENGTH,WIDTH,HEIGHT,VOLUME,WEIGHT,MATERIAL,PACKINGAMOUNT,PACKINGSPECIFICATION,PARTSOUTPACKINGCODE,PARTSINPACKINGCODE,MEASUREUNIT,MINPACKINGAMOUNT,IsNotWarrantyTransfer,ExchangeIdentification  from sparepart where status=1 ", "Code", false, partCodesNeedCheck, getDbSparePartInfo);
                    if(partCodesNeedCheck.Length > 0) {
                        var sparePartsExistsCode = sparePartsNeedCheck.Where(r => sparePartExtends.Any(v => v.Code == r.Code)).ToArray();
                        if(sparePartsExistsCode.Count() < sparePartsNeedCheck.Count()) {
                            var notExist = allList.Except(sparePartsExistsCode);
                            foreach(var item in notExist) {
                                if(item.ErrorMsg == null) {
                                    item.ErrorMsg = ErrorStrings.Export_Validation_PartsBranch_Validation8 + item.Code + ErrorStrings.Export_Validation_Sparepart_NotExist;
                                } else {
                                    item.ErrorMsg = item.ErrorMsg + ErrorStrings.Export_Validation_PartsBranch_Validation9 + item.Code + ErrorStrings.Export_Validation_Sparepart_NotExist;
                                }
                                errorList.Add(item);
                            }
                        }
                    }
                }
                //获取所有不合格数据
                errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                //获取所有合格数据
                rightList = allList.Except(errorList).ToList();

                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempSparePart = list[index - 1];
                            var values = new object[] {
                                tempSparePart.CodeStr, tempSparePart.NameStr, tempSparePart.ErrorMsg
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }

                //导入所有合格数据
                if(!rightList.Any())
                    return true;

                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    //开启事务，新增更新配件在一个事务内
                    var ts = conn.BeginTransaction();
                    try {
                        //新增配件
                        if(rightList.Any()) {
                            //获取新增数据的sql语句，Id为主键
                            var sqlUpdate = db.GetUpdateSql("SparePart", new string[] {
                                 "ModifierId", "ModifierName", "ModifyTime","IsNotWarrantyTransfer"
                            }, new string[] {
                                "Code"
                            });
                            var sqlInsertHistory = db.GetInsertSql("SparePartHistory", "Id", new[] {
                               "SparePartId", "Code", "ReferenceCode", "Name", "CADCode", "IsNotWarrantyTransfer", "Specification", "EnglishName", "PartType", "MeasureUnit", "ShelfLife", "LastSubstitute", "NextSubstitute", "Weight", "Feature", "status", "CreatorId", "CreatorName", "CreateTime", "MInPackingAmount","ExchangeIdentification"
                            });

                            var userInfo = Utils.GetCurrentUserInfo();
                            //往数据库增加配件信息
                            foreach(SparePartExtend item in rightList) {
                                var command = db.CreateDbCommand(sqlUpdate, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("Code", Strings.StrConv(item.Code, VbStrConv.Narrow).Replace("【", "[")));
                                command.Parameters.Add(db.CreateDbParameter("IsNotWarrantyTransfer", item.IsNotWarrantyTransfer));
                                command.Parameters.Add(db.CreateDbParameter("ModifierId", userInfo.Id));
                                command.Parameters.Add(db.CreateDbParameter("ModifierName", userInfo.Name));
                                command.Parameters.Add(db.CreateDbParameter("ModifyTime", DateTime.Now));
                                command.ExecuteNonQuery();
                                //往数据库增加履历
                                var sparePartExtend = sparePartExtends.Single(r => r.Code == item.Code);
                                var commandHistory = db.CreateDbCommand(sqlInsertHistory, conn, ts);
                                commandHistory.Parameters.Add(db.CreateDbParameter("SparePartId", sparePartExtend.Id));
                                commandHistory.Parameters.Add(db.CreateDbParameter("Code", Strings.StrConv(sparePartExtend.Code, VbStrConv.Narrow).Replace("【", "[")));
                                commandHistory.Parameters.Add(db.CreateDbParameter("Name", sparePartExtend.Name));
                                commandHistory.Parameters.Add(db.CreateDbParameter("ReferenceCode", sparePartExtend.ReferenceCode));
                                commandHistory.Parameters.Add(db.CreateDbParameter("CADCode", sparePartExtend.CADCode));
                                commandHistory.Parameters.Add(db.CreateDbParameter("Specification", sparePartExtend.Specification));
                                commandHistory.Parameters.Add(db.CreateDbParameter("EnglishName", sparePartExtend.EnglishName));
                                commandHistory.Parameters.Add(db.CreateDbParameter("PartType", sparePartExtend.PartType));
                                commandHistory.Parameters.Add(db.CreateDbParameter("MeasureUnit", sparePartExtend.MeasureUnit));
                                commandHistory.Parameters.Add(db.CreateDbParameter("ShelfLife", sparePartExtend.ShelfLife));
                                commandHistory.Parameters.Add(db.CreateDbParameter("IsNotWarrantyTransfer", item.IsNotWarrantyTransfer));
                                commandHistory.Parameters.Add(db.CreateDbParameter("LastSubstitute", sparePartExtend.LastSubstitute));
                                commandHistory.Parameters.Add(db.CreateDbParameter("NextSubstitute", sparePartExtend.NextSubstitute));
                                commandHistory.Parameters.Add(db.CreateDbParameter("Weight", sparePartExtend.Weight));
                                commandHistory.Parameters.Add(db.CreateDbParameter("Feature", sparePartExtend.Feature));
                                commandHistory.Parameters.Add(db.CreateDbParameter("Status", sparePartExtend.Status));
                                commandHistory.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                commandHistory.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                commandHistory.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                commandHistory.Parameters.Add(db.CreateDbParameter("MInPackingAmount", sparePartExtend.MInPackingAmount));
                                commandHistory.Parameters.Add(db.CreateDbParameter("ExchangeIdentification", sparePartExtend.ExchangeIdentification));
                                commandHistory.ExecuteNonQuery();
                            }

                        }
                        //无异常提交
                        ts.Commit();
                    } catch(Exception ex) {
                        //报错回滚
                        ts.Rollback();
                        throw new Exception(ex.Message);
                    } finally {
                        if(conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }
        public bool 批量替换配件名称(string fileName, out int excelImportNum, out List<SparePartExtend> rightData, out List<SparePartExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<SparePartExtend>();
            var rightList = new List<SparePartExtend>();
            var allList = new List<SparePartExtend>();
            var sparePartExtends = new List<SparePartExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("SparePart", out notNullableFields, out fieldLenght);
                List<object> excelColumns;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Code, "Code");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName, "Name");
                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;
                    var keyValuePairs = new[] {
                                new KeyValuePair<string, string>("Status","MasterData_Status") 
                            };
                    tempExcelOperator.ImportHelper.LoadEnumFromDb(null, null, keyValuePairs);

                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var sparePart = new SparePartExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        sparePart.CodeStr = Strings.StrConv(newRow["Code"], VbStrConv.Narrow).Replace("【", "[");
                        sparePart.CodeStr = strconvsparecode(sparePart.CodeStr);
                        sparePart.Code = Strings.StrConv(newRow["Code"], VbStrConv.Narrow).Replace("【", "[");
                        sparePart.Code = strconvsparecode(sparePart.Code);
                        sparePart.Code = sparePart.Code;//全部大写
                        sparePart.Name = newRow["Name"];
                        sparePart.NameStr = newRow["Name"];

                        var tempErrorMessage = new List<string>();
                        //配件编号检查
                        var fieldIndex = notNullableFields.IndexOf("CODE");
                        if(string.IsNullOrEmpty(sparePart.Code)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.OverstockPartsAppDetail__Validation1);
                        } else {
                            //配件长度不得超过25
                            if(Encoding.Default.GetByteCount(sparePart.Code) > 25)
                                tempErrorMessage.Add(ErrorStrings.ImpPartsSupplierRelation_Validation8);
                        }
                        //配件名称检查
                        fieldIndex = notNullableFields.IndexOf("NAME");
                        if(string.IsNullOrEmpty(sparePart.Name)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Sparepart_Name);
                        } else {
                            if(Encoding.Default.GetByteCount(sparePart.Name) > fieldLenght["NAME"])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Sparepart_NameIsLong);
                        }

                        if(tempErrorMessage.Count > 0) {
                            sparePart.ErrorMsg = string.Join("; ", tempErrorMessage);
                        } else {
                            var tempStatus = tempExcelOperator.ImportHelper.GetEnumValue("Status", ErrorStrings.Export_Validation_PartsBranch_Used);
                            if(tempStatus.HasValue) {
                                sparePart.Status = tempStatus.Value;
                            }
                        }
                        allList.Add(sparePart);
                        return false;
                    });
                    //校验导入的数据配件编号重复
                    var groups = allList.GroupBy(r =>
                        r.Code
                    ).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        if(groupItem.ErrorMsg == null) {
                            groupItem.ErrorMsg = ErrorStrings.Export_Title_PartsBranch_Code + groupItem.Code + ErrorStrings.Export_Validation_PartsBranch_Validation5;
                        } else {
                            groupItem.ErrorMsg = groupItem.ErrorMsg + ErrorStrings.Export_Validation_PartsBranch_Validation6 + groupItem.Code + ErrorStrings.Export_Validation_PartsBranch_Validation5;
                        }
                    }


                    var sparePartsNeedCheck = allList.ToList();
                    var partCodesNeedCheck = sparePartsNeedCheck.Select(r => r.Code).Distinct().ToArray();
                    Func<string[], bool> getDbSparePartInfo = vales => {
                        sparePartExtends.Add(new SparePartExtend {
                            Id = Convert.ToInt32(vales[0]),
                            Code = vales[1],
                            ReferenceCode = vales[3],
                            //m.Name = vales[2],
                            LastSubstitute = vales[4],
                            NextSubstitute = vales[5],
                            ShelfLife = string.IsNullOrEmpty(vales[6]) ? 0 : Convert.ToInt32(vales[6]),
                            EnglishName = vales[7],
                            PinyinCode = vales[8],
                            //ReferenceCode = vales[9],
                            ReferenceName = vales[10],
                            CADCode = vales[11],
                            CADName = vales[12],
                            PartType = string.IsNullOrEmpty(vales[13]) ? 0 : Convert.ToInt32(vales[13]),
                            Specification = vales[14],
                            Feature = vales[15],
                            Status = string.IsNullOrEmpty(vales[16]) ? 0 : Convert.ToInt32(vales[16]),
                            Length = string.IsNullOrEmpty(vales[17]) ? 0 : Convert.ToDecimal(vales[17]),
                            Width = string.IsNullOrEmpty(vales[18]) ? 0 : Convert.ToDecimal(vales[18]),
                            Height = string.IsNullOrEmpty(vales[19]) ? 0 : Convert.ToDecimal(vales[19]),
                            Volume = string.IsNullOrEmpty(vales[20]) ? 0 : Convert.ToDecimal(vales[20]),
                            Weight = string.IsNullOrEmpty(vales[21]) ? 0 : Convert.ToDecimal(vales[21]),
                            Material = vales[22],
                            PackingAmount = string.IsNullOrEmpty(vales[23]) ? 0 : Convert.ToInt32(vales[23]),
                            PackingSpecification = vales[24],
                            PartsOutPackingCode = vales[25],
                            PartsInPackingCode = vales[26],
                            MeasureUnit = vales[27],
                            MInPackingAmount = string.IsNullOrEmpty(vales[28]) ? 0 : Convert.ToInt32(vales[28]),
                            ExchangeIdentification = vales[29],
                        });
                        return false;
                    };
                    db.QueryDataWithInOperator(@"select ID,CODE,NAME,ReferenceCode,LASTSUBSTITUTE,NEXTSUBSTITUTE,SHELFLIFE,ENGLISHNAME,PINYINCODE,REFERENCECODE,REFERENCENAME,CADCODE,CADNAME,PARTTYPE,SPECIFICATION,FEATURE,STATUS,LENGTH,WIDTH,HEIGHT,VOLUME,WEIGHT,MATERIAL,PACKINGAMOUNT,PACKINGSPECIFICATION,PARTSOUTPACKINGCODE,PARTSINPACKINGCODE,MEASUREUNIT,MINPACKINGAMOUNT,ExchangeIdentification  from sparepart where status=1 ", "Code", false, partCodesNeedCheck, getDbSparePartInfo);
                    if(partCodesNeedCheck.Length > 0) {
                        var sparePartsExistsCode = sparePartsNeedCheck.Where(r => sparePartExtends.Any(v => v.Code == r.Code)).ToArray();
                        if(sparePartsExistsCode.Count() < sparePartsNeedCheck.Count()) {
                            var notExist = allList.Except(sparePartsExistsCode);
                            foreach(var item in notExist) {
                                if(item.ErrorMsg == null) {
                                    item.ErrorMsg = ErrorStrings.Export_Validation_PartsBranch_Validation8 + item.Code + ErrorStrings.Export_Validation_Sparepart_NotExist;
                                } else {
                                    item.ErrorMsg = item.ErrorMsg + ErrorStrings.Export_Validation_PartsBranch_Validation9 + item.Code + ErrorStrings.Export_Validation_Sparepart_NotExist;
                                }
                                errorList.Add(item);
                            }
                        }
                    }
                }
                //获取所有不合格数据
                errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                //if(errList.Count > 0) {
                //    errorList.AddRange(errList);
                //}
                //获取所有合格数据
                rightList = allList.Except(errorList).ToList();

                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempSparePart = list[index - 1];
                            var values = new object[] {
                                tempSparePart.CodeStr, tempSparePart.NameStr,
                                tempSparePart.ErrorMsg
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }

                //导入所有合格数据
                if(!rightList.Any())
                    return true;

                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    //开启事务，新增更新配件在一个事务内
                    var ts = conn.BeginTransaction();
                    try {
                        //新增配件
                        if(rightList.Any()) {
                            //获取新增数据的sql语句，Id为主键
                            var sqlUpdate = db.GetUpdateSql("SparePart", new string[] { "Name", "ModifierId", "ModifierName", "ModifyTime", "OMSparePartMark" }, new string[] { "Code" });
                            var sqlInsertHistory = db.GetInsertSql("SparePartHistory", "Id", new[] {
                                "SparePartId","Code","ReferenceCode", "Name", "CADCode", "Specification", "EnglishName", "PartType", "MeasureUnit", "ShelfLife", "LastSubstitute", "NextSubstitute", "Weight", "Feature", "status", "CreatorId", "CreatorName", "CreateTime","MInPackingAmount","ExchangeIdentification"
                            });
                            var sqlUpdatePartsBranch = db.GetUpdateSql("PartsBranch", new string[] { "PartName", "ModifierId", "ModifierName", "ModifyTime", "OMSparePartMark" }, new string[] { "PartCode" });

                            var userInfo = Utils.GetCurrentUserInfo();
                            //往数据库增加配件信息
                            foreach(SparePartExtend item in rightList) {
                                var command = db.CreateDbCommand(sqlUpdate, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("Code", Strings.StrConv(item.Code, VbStrConv.Narrow).Replace("【", "[")));
                                command.Parameters.Add(db.CreateDbParameter("Name", item.Name));
                                command.Parameters.Add(db.CreateDbParameter("ModifierId", userInfo.Id));
                                command.Parameters.Add(db.CreateDbParameter("ModifierName", userInfo.Name));
                                command.Parameters.Add(db.CreateDbParameter("ModifyTime", DateTime.Now));
                                command.Parameters.Add(db.CreateDbParameter("OMSparePartMark", 0));
                                command.ExecuteNonQuery();
                                //往数据库增加履历
                                var sparePartExtend = sparePartExtends.Single(r => r.Code == item.Code);
                                var commandHistory = db.CreateDbCommand(sqlInsertHistory, conn, ts);
                                commandHistory.Parameters.Add(db.CreateDbParameter("SparePartId", sparePartExtend.Id));
                                commandHistory.Parameters.Add(db.CreateDbParameter("Code", Strings.StrConv(sparePartExtend.Code, VbStrConv.Narrow).Replace("【", "[")));
                                commandHistory.Parameters.Add(db.CreateDbParameter("Name", item.Name));
                                commandHistory.Parameters.Add(db.CreateDbParameter("ReferenceCode", sparePartExtend.ReferenceCode));
                                commandHistory.Parameters.Add(db.CreateDbParameter("CADCode", sparePartExtend.CADCode));
                                commandHistory.Parameters.Add(db.CreateDbParameter("Specification", sparePartExtend.Specification));
                                commandHistory.Parameters.Add(db.CreateDbParameter("EnglishName", sparePartExtend.EnglishName));
                                commandHistory.Parameters.Add(db.CreateDbParameter("PartType", sparePartExtend.PartType));
                                commandHistory.Parameters.Add(db.CreateDbParameter("MeasureUnit", sparePartExtend.MeasureUnit));
                                commandHistory.Parameters.Add(db.CreateDbParameter("ShelfLife", sparePartExtend.ShelfLife));
                                commandHistory.Parameters.Add(db.CreateDbParameter("LastSubstitute", sparePartExtend.LastSubstitute));
                                commandHistory.Parameters.Add(db.CreateDbParameter("NextSubstitute", sparePartExtend.NextSubstitute));
                                commandHistory.Parameters.Add(db.CreateDbParameter("Weight", sparePartExtend.Weight));
                                commandHistory.Parameters.Add(db.CreateDbParameter("Feature", sparePartExtend.Feature));
                                commandHistory.Parameters.Add(db.CreateDbParameter("Status", sparePartExtend.Status));
                                commandHistory.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                commandHistory.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                commandHistory.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                commandHistory.Parameters.Add(db.CreateDbParameter("MInPackingAmount", sparePartExtend.MInPackingAmount));
                                commandHistory.Parameters.Add(db.CreateDbParameter("ExchangeIdentification", sparePartExtend.ExchangeIdentification));
                                commandHistory.ExecuteNonQuery();

                                //配件分品牌信息
                                var partsBranchExtend = sparePartExtends.Single(r => r.Code == item.Code);
                                var commandPartsBranch = db.CreateDbCommand(sqlUpdatePartsBranch, conn, ts);
                                commandPartsBranch.Parameters.Add(db.CreateDbParameter("PartCode", Strings.StrConv(partsBranchExtend.Code, VbStrConv.Narrow).Replace("【", "[")));
                                commandPartsBranch.Parameters.Add(db.CreateDbParameter("PartName", item.Name));
                                commandPartsBranch.Parameters.Add(db.CreateDbParameter("ModifierId", userInfo.Id));
                                commandPartsBranch.Parameters.Add(db.CreateDbParameter("ModifierName", userInfo.Name));
                                commandPartsBranch.Parameters.Add(db.CreateDbParameter("ModifyTime", DateTime.Now));
                                commandPartsBranch.Parameters.Add(db.CreateDbParameter("OMSparePartMark", 0));
                                commandPartsBranch.ExecuteNonQuery();
                            }

                        }
                        //无异常提交
                        ts.Commit();
                    } catch(Exception ex) {
                        //报错回滚
                        ts.Rollback();
                        throw new Exception(ex.Message);
                    } finally {
                        if(conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }

        public bool 批量替换规格型号(string fileName, out int excelImportNum, out List<SparePartExtend> rightData, out List<SparePartExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<SparePartExtend>();
            var rightList = new List<SparePartExtend>();
            var allList = new List<SparePartExtend>();
            var dbSparePartCodes = new List<SparePartExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("SparePart", out notNullableFields, out fieldLenght);
                List<object> excelColumns;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Code, "Code");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Sparepart_Specification, "Specification");
                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;
                    var keyValuePairs = new[] {
                                new KeyValuePair<string, string>("Status","MasterData_Status") 
                            };
                    tempExcelOperator.ImportHelper.LoadEnumFromDb(null, null, keyValuePairs);
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var sparePart = new SparePartExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        sparePart.CodeStr = Strings.StrConv(newRow["Code"], VbStrConv.Narrow).Replace("【", "[");
                        sparePart.CodeStr = strconvsparecode(sparePart.CodeStr);
                        sparePart.Code = Strings.StrConv(newRow["Code"], VbStrConv.Narrow).Replace("【", "[");
                        sparePart.Code = strconvsparecode(sparePart.Code);
                        sparePart.Code = sparePart.Code;
                        sparePart.Specification = newRow["Specification"];
                        sparePart.SpecificationStr = newRow["Specification"];

                        var tempErrorMessage = new List<string>();
                        //配件编号检查
                        var fieldIndex = notNullableFields.IndexOf("CODE");
                        if(string.IsNullOrEmpty(sparePart.Code)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.OverstockPartsAppDetail__Validation1);
                        } else {
                            //配件长度不得超过25
                            if(Encoding.Default.GetByteCount(sparePart.Code) > 25)
                                tempErrorMessage.Add(ErrorStrings.ImpPartsSupplierRelation_Validation8);
                        }
                        //规格型号检查
                        if(sparePart.Specification != null && Encoding.Default.GetByteCount(sparePart.Specification) > 100)
                            tempErrorMessage.Add(ErrorStrings.Export_Validation_Sparepart_SpecificationIsLong);
                        if(tempErrorMessage.Count > 0) {
                            sparePart.ErrorMsg = string.Join("; ", tempErrorMessage);
                        } else {
                            var tempStatus = tempExcelOperator.ImportHelper.GetEnumValue("Status", ErrorStrings.Export_Validation_PartsBranch_Used);
                            if(tempStatus.HasValue) {
                                sparePart.Status = tempStatus.Value;
                            }
                        }
                        allList.Add(sparePart);
                        return false;
                    });
                    //校验导入的数据配件编号重复
                    var groups = allList.GroupBy(r =>
                        r.Code
                    ).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        if(groupItem.ErrorMsg == null) {
                            groupItem.ErrorMsg = ErrorStrings.Export_Title_PartsBranch_Code + groupItem.Code + ErrorStrings.Export_Validation_PartsBranch_Validation5;
                        } else {
                            groupItem.ErrorMsg = groupItem.ErrorMsg + ErrorStrings.Export_Validation_PartsBranch_Validation6 + groupItem.Code + ErrorStrings.Export_Validation_PartsBranch_Validation5;
                        }
                    }
                    var sparePartsNeedCheck = allList.ToList();
                    var partCodesNeedCheck = sparePartsNeedCheck.Select(r => r.Code).Distinct().ToArray();

                    Func<string[], bool> getDbSparePartCodes = vales => {
                        var dbSparePart = new SparePartExtend();
                        dbSparePart.Id = Convert.ToInt32(vales[0]);
                        dbSparePart.Code = vales[1];
                        dbSparePart.ReferenceCode = vales[3];
                        dbSparePart.Name = vales[2];
                        dbSparePart.LastSubstitute = vales[4];
                        dbSparePart.NextSubstitute = vales[5];
                        dbSparePart.ShelfLife = string.IsNullOrEmpty(vales[6]) ? 0 : Convert.ToInt32(vales[6]);
                        dbSparePart.EnglishName = vales[7];
                        dbSparePart.PinyinCode = vales[8];
                        dbSparePart.ReferenceCode = vales[9];
                        dbSparePart.ReferenceName = vales[10];
                        dbSparePart.CADCode = vales[11];
                        dbSparePart.CADName = vales[12];
                        dbSparePart.PartType = string.IsNullOrEmpty(vales[13]) ? 0 : Convert.ToInt32(vales[13]);
                        dbSparePart.Specification = vales[14];
                        dbSparePart.Feature = vales[15];
                        dbSparePart.Status = string.IsNullOrEmpty(vales[16]) ? 0 : Convert.ToInt32(vales[16]);
                        dbSparePart.Length = string.IsNullOrEmpty(vales[17]) ? 0 : Convert.ToDecimal(vales[17]);
                        dbSparePart.Width = string.IsNullOrEmpty(vales[18]) ? 0 : Convert.ToDecimal(vales[18]);
                        dbSparePart.Height = string.IsNullOrEmpty(vales[19]) ? 0 : Convert.ToDecimal(vales[19]);
                        dbSparePart.Volume = string.IsNullOrEmpty(vales[20]) ? 0 : Convert.ToDecimal(vales[20]);
                        dbSparePart.Weight = string.IsNullOrEmpty(vales[21]) ? 0 : Convert.ToDecimal(vales[21]);
                        dbSparePart.Material = vales[22];
                        dbSparePart.PackingAmount = string.IsNullOrEmpty(vales[23]) ? 0 : Convert.ToInt32(vales[23]);
                        dbSparePart.PackingSpecification = vales[24];
                        dbSparePart.PartsOutPackingCode = vales[25];
                        dbSparePart.PartsInPackingCode = vales[26];
                        dbSparePart.MeasureUnit = vales[27];
                        dbSparePart.MInPackingAmount = string.IsNullOrEmpty(vales[28]) ? 0 : Convert.ToInt32(vales[28]);
                        dbSparePart.IsNotWarrantyTransfer = string.IsNullOrEmpty(vales[29]) ? 0 : Convert.ToInt32(vales[29]);
                        dbSparePart.ExchangeIdentification = vales[30];
                        dbSparePartCodes.Add(dbSparePart);
                        return false;
                    };
                    //根据SQL查询数据库，将查出来的结果根据定义的委托中的方法（这里是getDbSparePartCodes）放到具体的集合中（这里是dbSparePartCodes）
                    db.QueryDataWithInOperator(@"select ID,
                                                                    CODE,
                                                                    NAME,
                                                                    ReferenceCode,
                                                                    LASTSUBSTITUTE,
                                                                    NEXTSUBSTITUTE,
                                                                    SHELFLIFE,
                                                                    ENGLISHNAME,
                                                                    PINYINCODE,
                                                                    REFERENCECODE,
                                                                    REFERENCENAME,
                                                                    CADCODE,
                                                                    CADNAME,
                                                                    PARTTYPE,
                                                                    SPECIFICATION,
                                                                    FEATURE,
                                                                    STATUS,
                                                                    LENGTH,
                                                                    WIDTH,
                                                                    HEIGHT,
                                                                    VOLUME,
                                                                    WEIGHT,
                                                                    MATERIAL,
                                                                    PACKINGAMOUNT,
                                                                    PACKINGSPECIFICATION,
                                                                    PARTSOUTPACKINGCODE,
                                                                    PARTSINPACKINGCODE,
                                                                    MEASUREUNIT,
                                                                    MINPACKINGAMOUNT,IsNotWarrantyTransfer,ExchangeIdentification
                                                                from sparepart where status=1 ", "Code", false, partCodesNeedCheck, getDbSparePartCodes);
                    if(partCodesNeedCheck.Length > 0) {
                        var sparePartsExistsCode = sparePartsNeedCheck.Where(r => dbSparePartCodes.Any(v => v.Code == r.Code)).ToArray();
                        if(sparePartsExistsCode.Count() < sparePartsNeedCheck.Count()) {
                            var notExist = allList.Except(sparePartsExistsCode);
                            foreach(var item in notExist) {
                                if(item.ErrorMsg == null) {
                                    item.ErrorMsg = ErrorStrings.Export_Validation_PartsBranch_Validation8 + item.Code + ErrorStrings.Export_Validation_Sparepart_NotExistOrStop;
                                } else {
                                    item.ErrorMsg = item.ErrorMsg + ErrorStrings.Export_Validation_PartsBranch_Validation9 + item.Code + ErrorStrings.Export_Validation_Sparepart_NotExistOrStop;
                                }
                                errorList.Add(item);
                            }
                        }
                    }
                }
                //获取所有不合格数据
                errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                //if(errList.Count > 0) {
                //    errorList.AddRange(errList);
                //}
                //获取所有合格数据
                rightList = allList.Except(errorList).ToList();

                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempSparePart = list[index - 1];
                            var values = new object[] {
                                tempSparePart.CodeStr, tempSparePart.SpecificationStr,
                                tempSparePart.ErrorMsg
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }

                //导入所有合格数据
                if(!rightList.Any())
                    return true;
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    //开启事务，新增更新配件在一个事务内
                    var ts = conn.BeginTransaction();
                    try {
                        //新增配件
                        if(rightList.Any()) {
                            //获取新增数据的sql语句，Id为主键
                            var sqlUpdate = db.GetUpdateSql("SparePart", new string[] { "Specification", "ModifierId", "ModifierName", "ModifyTime", "OMSparePartMark" }, new string[] { "Code" });
                            var sqlInsertHistory = db.GetInsertSql("SparePartHistory", "Id", new[] {
                                "IsNotWarrantyTransfer","SparePartId","Code","ReferenceCode", "Name", "CADCode", "Specification", "EnglishName", "PartType", "MeasureUnit", "ShelfLife", "LastSubstitute", "NextSubstitute", "Weight", "Feature", "status", "CreatorId", "CreatorName", "CreateTime","MInPackingAmount","ExchangeIdentification"
                            });

                            var userInfo = Utils.GetCurrentUserInfo();
                            //往数据库增加配件信息
                            foreach(SparePartExtend item in rightList) {
                                var command = db.CreateDbCommand(sqlUpdate, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("Code", Strings.StrConv(item.Code, VbStrConv.Narrow).Replace("【", "[")));
                                command.Parameters.Add(db.CreateDbParameter("Specification", item.Specification));
                                command.Parameters.Add(db.CreateDbParameter("ModifierId", userInfo.Id));
                                command.Parameters.Add(db.CreateDbParameter("ModifierName", userInfo.Name));
                                command.Parameters.Add(db.CreateDbParameter("ModifyTime", DateTime.Now));
                                command.Parameters.Add(db.CreateDbParameter("OMSparePartMark", 0));
                                command.ExecuteNonQuery();

                                //                                //获取数据库的完整备件信息
                                //                                string[] partCode = new string[] { item.Code };

                                //                                Func<string[], bool> getDbSparePartInfo = vales => {
                                //                                    item.Id = Convert.ToInt32(vales[0]);
                                //                                    item.Code = vales[1];
                                //                                    item.ReferenceCode = vales[3];
                                //                                    item.Name = vales[2];
                                //                                    item.LastSubstitute = vales[4];
                                //                                    item.NextSubstitute = vales[5];
                                //                                    item.ShelfLife = string.IsNullOrEmpty(vales[6]) ? 0 : Convert.ToInt32(vales[6]);
                                //                                    item.EnglishName = vales[7];
                                //                                    item.PinyinCode = vales[8];
                                //                                    item.ReferenceCode = vales[9];
                                //                                    item.ReferenceName = vales[10];
                                //                                    item.CADCode = vales[11];
                                //                                    item.CADName = vales[12];
                                //                                    item.PartType = string.IsNullOrEmpty(vales[13]) ? 0 : Convert.ToInt32(vales[13]);
                                //                                    item.Specification = vales[14];
                                //                                    item.Feature = vales[15];
                                //                                    item.Status = string.IsNullOrEmpty(vales[16]) ? 0 : Convert.ToInt32(vales[16]);
                                //                                    item.Length = string.IsNullOrEmpty(vales[17]) ? 0 : Convert.ToDecimal(vales[17]);
                                //                                    item.Width = string.IsNullOrEmpty(vales[18]) ? 0 : Convert.ToDecimal(vales[18]);
                                //                                    item.Height = string.IsNullOrEmpty(vales[19]) ? 0 : Convert.ToDecimal(vales[19]);
                                //                                    item.Volume = string.IsNullOrEmpty(vales[20]) ? 0 : Convert.ToDecimal(vales[20]);
                                //                                    item.Weight = string.IsNullOrEmpty(vales[21]) ? 0 : Convert.ToDecimal(vales[21]);
                                //                                    item.Material = vales[22];
                                //                                    item.PackingAmount = string.IsNullOrEmpty(vales[23]) ? 0 : Convert.ToInt32(vales[23]);
                                //                                    item.PackingSpecification = vales[24];
                                //                                    item.PartsOutPackingCode = vales[25];
                                //                                    item.PartsInPackingCode = vales[26];
                                //                                    item.MeasureUnit = vales[27];
                                //                                    item.MInPackingAmount = string.IsNullOrEmpty(vales[28]) ? 0 : Convert.ToInt32(vales[28]);
                                //                                    return false;
                                //                                };

                                //                                db.QueryDataWithInOperator(@"select ID,
                                //                                                                    CODE,
                                //                                                                    NAME,
                                //                                                                    ReferenceCode,
                                //                                                                    LASTSUBSTITUTE,
                                //                                                                    NEXTSUBSTITUTE,
                                //                                                                    SHELFLIFE,
                                //                                                                    ENGLISHNAME,
                                //                                                                    PINYINCODE,
                                //                                                                    REFERENCECODE,
                                //                                                                    REFERENCENAME,
                                //                                                                    CADCODE,
                                //                                                                    CADNAME,
                                //                                                                    PARTTYPE,
                                //                                                                    SPECIFICATION,
                                //                                                                    FEATURE,
                                //                                                                    STATUS,
                                //                                                                    LENGTH,
                                //                                                                    WIDTH,
                                //                                                                    HEIGHT,
                                //                                                                    VOLUME,
                                //                                                                    WEIGHT,
                                //                                                                    MATERIAL,
                                //                                                                    PACKINGAMOUNT,
                                //                                                                    PACKINGSPECIFICATION,
                                //                                                                    PARTSOUTPACKINGCODE,
                                //                                                                    PARTSINPACKINGCODE,
                                //                                                                    MEASUREUNIT,
                                //                                                                    MINPACKINGAMOUNT
                                //                                                                from sparepart where status=1 ", "Code", true, partCode, getDbSparePartInfo);

                                //往数据库增加履历
                                var sparePart = dbSparePartCodes.Where(r => r.Code == item.Code).FirstOrDefault();
                                if(sparePart != null) {
                                    var commandHistory = db.CreateDbCommand(sqlInsertHistory, conn, ts);
                                    commandHistory.Parameters.Add(db.CreateDbParameter("SparePartId", sparePart.Id));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Code", Strings.StrConv(sparePart.Code, VbStrConv.Narrow).Replace("【", "[")));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Name", sparePart.Name));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("ReferenceCode", sparePart.ReferenceCode));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("CADCode", sparePart.CADCode));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("IsNotWarrantyTransfer", sparePart.IsNotWarrantyTransfer));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Specification", item.Specification));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("EnglishName", sparePart.EnglishName));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("PartType", sparePart.PartType));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("MeasureUnit", sparePart.MeasureUnit));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("ShelfLife", sparePart.ShelfLife));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("LastSubstitute", sparePart.LastSubstitute));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("NextSubstitute", sparePart.NextSubstitute));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Weight", sparePart.Weight));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Feature", sparePart.Feature));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Status", sparePart.Status));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("MInPackingAmount", sparePart.MInPackingAmount));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("ExchangeIdentification", sparePart.ExchangeIdentification));
                                    commandHistory.ExecuteNonQuery();
                                }
                            }

                        }
                        //无异常提交
                        ts.Commit();
                    } catch(Exception ex) {
                        //报错回滚
                        ts.Rollback();
                        throw new Exception(ex.Message);
                    } finally {
                        if(conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }
      public  bool 批量替换是否供应商投放(string fileName, out int excelImportNum, out List<SparePartExtend> rightData, out List<SparePartExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<SparePartExtend>();
            var rightList = new List<SparePartExtend>();
            var allList = new List<SparePartExtend>();
            var dbSparePartCodes = new List<SparePartExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("SparePart", out notNullableFields, out fieldLenght);
                List<object> excelColumns;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Code, "Code");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_IsSupplierPutIn, "IsSupplierPutIn");
                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;
                    var keyValuePairs = new[] {
                                new KeyValuePair<string, string>("Status","MasterData_Status") 
                            };
                    tempExcelOperator.ImportHelper.LoadEnumFromDb(null, null, keyValuePairs);
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var sparePart = new SparePartExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        sparePart.CodeStr = Strings.StrConv(newRow["Code"], VbStrConv.Narrow).Replace("【", "[");
                        sparePart.CodeStr = strconvsparecode(sparePart.CodeStr);
                        sparePart.Code = Strings.StrConv(newRow["Code"], VbStrConv.Narrow).Replace("【", "[");
                        sparePart.Code = strconvsparecode(sparePart.Code);
                        sparePart.Code = sparePart.Code;//全部大写
                        sparePart.IsSupplierPutInStr = newRow["IsSupplierPutIn"];

                        var tempErrorMessage = new List<string>();
                        //配件编号检查
                        var fieldIndex = notNullableFields.IndexOf("CODE");
                        if(string.IsNullOrEmpty(sparePart.Code)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.OverstockPartsAppDetail__Validation1);
                        } else {
                            //配件长度不得超过25
                            if(Encoding.Default.GetByteCount(sparePart.Code) > 25)
                                tempErrorMessage.Add(ErrorStrings.ImpPartsSupplierRelation_Validation8);
                        }
                        //是否供应商投放检查
                        if(sparePart.IsSupplierPutInStr != null ) {
                            if(sparePart.IsSupplierPutInStr == ErrorStrings.Export_Title_PartsBranch_Yes) {
                                sparePart.IsSupplierPutIn = 1;
                            } else if(sparePart.IsSupplierPutInStr == ErrorStrings.Export_Title_PartsBranch_No) {
                                sparePart.IsSupplierPutIn = 0;
                            } else {
                                tempErrorMessage.Add(ErrorStrings.ImpPartsSupplierRelation_Validation36);
                            }
                        } else {
                            tempErrorMessage.Add(ErrorStrings.ImpPartsSupplierRelation_Validation36);
                        }

                        if(tempErrorMessage.Count > 0) {
                            sparePart.ErrorMsg = string.Join("; ", tempErrorMessage);
                        } else {
                            var tempStatus = tempExcelOperator.ImportHelper.GetEnumValue("Status", ErrorStrings.Export_Validation_PartsBranch_Used);
                            if(tempStatus.HasValue) {
                                sparePart.Status = tempStatus.Value;
                            }
                        }
                        allList.Add(sparePart);
                        return false;
                    });
                    //校验导入的数据配件编号重复
                    var groups = allList.GroupBy(r =>
                        r.Code
                    ).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        if(groupItem.ErrorMsg == null) {
                            groupItem.ErrorMsg = ErrorStrings.Export_Title_PartsBranch_Code + groupItem.Code + ErrorStrings.Export_Validation_PartsBranch_Validation5;
                        } else {
                            groupItem.ErrorMsg = groupItem.ErrorMsg + ErrorStrings.Export_Validation_PartsBranch_Validation6 + groupItem.Code + ErrorStrings.Export_Validation_PartsBranch_Validation5;
                        }
                    }
                    var sparePartsNeedCheck = allList.ToList();
                    var partCodesNeedCheck = sparePartsNeedCheck.Select(r => r.Code).Distinct().ToArray();

                    Func<string[], bool> getDbSparePartCodes = vales => {
                        var dbSparePart = new SparePartExtend();
                        dbSparePart.Id = Convert.ToInt32(vales[0]);
                        dbSparePart.Code = vales[1];
                        dbSparePart.ReferenceCode = vales[3];
                        dbSparePart.Name = vales[2];
                        dbSparePart.LastSubstitute = vales[4];
                        dbSparePart.NextSubstitute = vales[5];
                        dbSparePart.ShelfLife = string.IsNullOrEmpty(vales[6]) ? 0 : Convert.ToInt32(vales[6]);
                        dbSparePart.EnglishName = vales[7];
                        dbSparePart.PinyinCode = vales[8];
                        dbSparePart.ReferenceCode = vales[9];
                        dbSparePart.ReferenceName = vales[10];
                        dbSparePart.CADCode = vales[11];
                        dbSparePart.CADName = vales[12];
                        dbSparePart.PartType = string.IsNullOrEmpty(vales[13]) ? 0 : Convert.ToInt32(vales[13]);
                        dbSparePart.Specification = vales[14];
                        dbSparePart.Feature = vales[15];
                        dbSparePart.Status = string.IsNullOrEmpty(vales[16]) ? 0 : Convert.ToInt32(vales[16]);
                        dbSparePart.Length = string.IsNullOrEmpty(vales[17]) ? 0 : Convert.ToDecimal(vales[17]);
                        dbSparePart.Width = string.IsNullOrEmpty(vales[18]) ? 0 : Convert.ToDecimal(vales[18]);
                        dbSparePart.Height = string.IsNullOrEmpty(vales[19]) ? 0 : Convert.ToDecimal(vales[19]);
                        dbSparePart.Volume = string.IsNullOrEmpty(vales[20]) ? 0 : Convert.ToDecimal(vales[20]);
                        dbSparePart.Weight = string.IsNullOrEmpty(vales[21]) ? 0 : Convert.ToDecimal(vales[21]);
                        dbSparePart.Material = vales[22];
                        dbSparePart.PackingAmount = string.IsNullOrEmpty(vales[23]) ? 0 : Convert.ToInt32(vales[23]);
                        dbSparePart.PackingSpecification = vales[24];
                        dbSparePart.PartsOutPackingCode = vales[25];
                        dbSparePart.PartsInPackingCode = vales[26];
                        dbSparePart.MeasureUnit = vales[27];
                        dbSparePart.MInPackingAmount = string.IsNullOrEmpty(vales[28]) ? 0 : Convert.ToInt32(vales[28]);
                        dbSparePart.IsNotWarrantyTransfer = string.IsNullOrEmpty(vales[29]) ? 0 : Convert.ToInt32(vales[29]);
                        dbSparePart.ExchangeIdentification = vales[30];
                        dbSparePart.IsSupplierPutIn = string.IsNullOrEmpty(vales[31]) ? 0 : Convert.ToInt32(vales[29]);
                        dbSparePartCodes.Add(dbSparePart);
                        return false;
                    };
                    //根据SQL查询数据库，将查出来的结果根据定义的委托中的方法（这里是getDbSparePartCodes）放到具体的集合中（这里是dbSparePartCodes）
                    db.QueryDataWithInOperator(@"select ID,
                                                                    CODE,
                                                                    NAME,
                                                                    ReferenceCode,
                                                                    LASTSUBSTITUTE,
                                                                    NEXTSUBSTITUTE,
                                                                    SHELFLIFE,
                                                                    ENGLISHNAME,
                                                                    PINYINCODE,
                                                                    REFERENCECODE,
                                                                    REFERENCENAME,
                                                                    CADCODE,
                                                                    CADNAME,
                                                                    PARTTYPE,
                                                                    SPECIFICATION,
                                                                    FEATURE,
                                                                    STATUS,
                                                                    LENGTH,
                                                                    WIDTH,
                                                                    HEIGHT,
                                                                    VOLUME,
                                                                    WEIGHT,
                                                                    MATERIAL,
                                                                    PACKINGAMOUNT,
                                                                    PACKINGSPECIFICATION,
                                                                    PARTSOUTPACKINGCODE,
                                                                    PARTSINPACKINGCODE,
                                                                    MEASUREUNIT,
                                                                    MINPACKINGAMOUNT,IsNotWarrantyTransfer,ExchangeIdentification,IsSupplierPutIn
                                                                from sparepart where status=1 ", "Code", false, partCodesNeedCheck, getDbSparePartCodes);
                    if(partCodesNeedCheck.Length > 0) {
                        var sparePartsExistsCode = sparePartsNeedCheck.Where(r => dbSparePartCodes.Any(v => v.Code == r.Code)).ToArray();
                        if(sparePartsExistsCode.Count() < sparePartsNeedCheck.Count()) {
                            var notExist = allList.Except(sparePartsExistsCode);
                            foreach(var item in notExist) {
                                if(item.ErrorMsg == null) {
                                    item.ErrorMsg = ErrorStrings.Export_Validation_PartsBranch_Validation8 + item.Code + ErrorStrings.Export_Validation_Sparepart_NotExistOrStop;
                                } else {
                                    item.ErrorMsg = item.ErrorMsg + ErrorStrings.Export_Validation_PartsBranch_Validation9 + item.Code + ErrorStrings.Export_Validation_Sparepart_NotExistOrStop;
                                }
                                errorList.Add(item);
                            }
                        }
                    }
                }
                //获取所有不合格数据
                errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                //if(errList.Count > 0) {
                //    errorList.AddRange(errList);
                //}
                //获取所有合格数据
                rightList = allList.Except(errorList).ToList();

                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempSparePart = list[index - 1];
                            var values = new object[] {
                                tempSparePart.CodeStr, tempSparePart.EnglishNameStr,
                                tempSparePart.ErrorMsg
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }

                //导入所有合格数据
                if(!rightList.Any())
                    return true;
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    //开启事务，新增更新配件在一个事务内
                    var ts = conn.BeginTransaction();
                    try {
                        //新增配件
                        if(rightList.Any()) {
                            //获取新增数据的sql语句，Id为主键
                            var sqlUpdate = db.GetUpdateSql("SparePart", new string[] { "IsSupplierPutIn", "ModifierId", "ModifierName", "ModifyTime"}, new string[] { "Code" });
                            var sqlInsertHistory = db.GetInsertSql("SparePartHistory", "Id", new[] {
                            "IsNotWarrantyTransfer",   "SparePartId","Code","ReferenceCode", "Name", "CADCode", "Specification", "EnglishName", "PartType", "MeasureUnit", "ShelfLife", "LastSubstitute", "NextSubstitute", "Weight", "Feature", "status", "CreatorId", "CreatorName", "CreateTime","MInPackingAmount","ExchangeIdentification"
                            ,"IsSupplierPutIn" });

                            var userInfo = Utils.GetCurrentUserInfo();
                            //往数据库增加配件信息
                            foreach(SparePartExtend item in rightList) {
                                var command = db.CreateDbCommand(sqlUpdate, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("Code", Strings.StrConv(item.Code, VbStrConv.Narrow).Replace("【", "[")));
                                command.Parameters.Add(db.CreateDbParameter("IsSupplierPutIn", item.IsSupplierPutIn));
                                command.Parameters.Add(db.CreateDbParameter("ModifierId", userInfo.Id));
                                command.Parameters.Add(db.CreateDbParameter("ModifierName", userInfo.Name));
                                command.Parameters.Add(db.CreateDbParameter("ModifyTime", DateTime.Now));
                                command.ExecuteNonQuery();                         
                                //往数据库增加履历
                                var sparePart = dbSparePartCodes.Where(r => r.Code == item.Code).FirstOrDefault();
                                if(sparePart != null) {
                                    var commandHistory = db.CreateDbCommand(sqlInsertHistory, conn, ts);
                                    commandHistory.Parameters.Add(db.CreateDbParameter("SparePartId", sparePart.Id));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Code", Strings.StrConv(sparePart.Code, VbStrConv.Narrow).Replace("【", "[")));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Name", sparePart.Name));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("ReferenceCode", sparePart.ReferenceCode));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("CADCode", sparePart.CADCode));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("IsNotWarrantyTransfer", sparePart.IsNotWarrantyTransfer));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Specification", sparePart.Specification));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("EnglishName", item.EnglishName));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("PartType", sparePart.PartType));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("MeasureUnit", sparePart.MeasureUnit));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("ShelfLife", sparePart.ShelfLife));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("LastSubstitute", sparePart.LastSubstitute));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("NextSubstitute", sparePart.NextSubstitute));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Weight", sparePart.Weight));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Feature", sparePart.Feature));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Status", sparePart.Status));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("MInPackingAmount", sparePart.MInPackingAmount));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("ExchangeIdentification", sparePart.ExchangeIdentification));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("IsSupplierPutIn", sparePart.ExchangeIdentification));
                                    commandHistory.ExecuteNonQuery();
                                }
                            }

                        }
                        //无异常提交
                        ts.Commit();
                    } catch(Exception ex) {
                        //报错回滚
                        ts.Rollback();
                        throw new Exception(ex.Message);
                    } finally {
                        if(conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }
        public bool 批量替换英文名称(string fileName, out int excelImportNum, out List<SparePartExtend> rightData, out List<SparePartExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<SparePartExtend>();
            var rightList = new List<SparePartExtend>();
            var allList = new List<SparePartExtend>();
            var dbSparePartCodes = new List<SparePartExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("SparePart", out notNullableFields, out fieldLenght);
                List<object> excelColumns;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Code, "Code");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_EnglishName, "EnglishName");
                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;
                    var keyValuePairs = new[] {
                                new KeyValuePair<string, string>("Status","MasterData_Status") 
                            };
                    tempExcelOperator.ImportHelper.LoadEnumFromDb(null, null, keyValuePairs);
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var sparePart = new SparePartExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        sparePart.CodeStr = Strings.StrConv(newRow["Code"], VbStrConv.Narrow).Replace("【", "[");
                        sparePart.CodeStr = strconvsparecode(sparePart.CodeStr);
                        sparePart.Code = Strings.StrConv(newRow["Code"], VbStrConv.Narrow).Replace("【", "[");
                        sparePart.Code = strconvsparecode(sparePart.Code);
                        sparePart.Code = sparePart.Code;//全部大写
                        sparePart.EnglishName = newRow["EnglishName"];
                        sparePart.EnglishNameStr = newRow["EnglishName"];

                        var tempErrorMessage = new List<string>();
                        //配件编号检查
                        var fieldIndex = notNullableFields.IndexOf("CODE");
                        if(string.IsNullOrEmpty(sparePart.Code)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.OverstockPartsAppDetail__Validation1);
                        } else {
                            //配件长度不得超过25
                            if(Encoding.Default.GetByteCount(sparePart.Code) > 25)
                                tempErrorMessage.Add(ErrorStrings.ImpPartsSupplierRelation_Validation8);
                        }
                        //英文名称检查
                        if(sparePart.EnglishName != null && Encoding.Default.GetByteCount(sparePart.EnglishName) > 100)
                            tempErrorMessage.Add(ErrorStrings.Export_Validation_Sparepart_EnglishNameOverLong);

                        if(tempErrorMessage.Count > 0) {
                            sparePart.ErrorMsg = string.Join("; ", tempErrorMessage);
                        } else {
                            var tempStatus = tempExcelOperator.ImportHelper.GetEnumValue("Status", ErrorStrings.Export_Validation_PartsBranch_Used);
                            if(tempStatus.HasValue) {
                                sparePart.Status = tempStatus.Value;
                            }
                        }
                        allList.Add(sparePart);
                        return false;
                    });
                    //校验导入的数据配件编号重复
                    var groups = allList.GroupBy(r =>
                        r.Code
                    ).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        if(groupItem.ErrorMsg == null) {
                            groupItem.ErrorMsg = ErrorStrings.Export_Title_PartsBranch_Code + groupItem.Code + ErrorStrings.Export_Validation_PartsBranch_Validation5;
                        } else {
                            groupItem.ErrorMsg = groupItem.ErrorMsg + ErrorStrings.Export_Validation_PartsBranch_Validation6 + groupItem.Code + ErrorStrings.Export_Validation_PartsBranch_Validation5;
                        }
                    }
                    var sparePartsNeedCheck = allList.ToList();
                    var partCodesNeedCheck = sparePartsNeedCheck.Select(r => r.Code).Distinct().ToArray();

                    Func<string[], bool> getDbSparePartCodes = vales => {
                        var dbSparePart = new SparePartExtend();
                        dbSparePart.Id = Convert.ToInt32(vales[0]);
                        dbSparePart.Code = vales[1];
                        dbSparePart.ReferenceCode = vales[3];
                        dbSparePart.Name = vales[2];
                        dbSparePart.LastSubstitute = vales[4];
                        dbSparePart.NextSubstitute = vales[5];
                        dbSparePart.ShelfLife = string.IsNullOrEmpty(vales[6]) ? 0 : Convert.ToInt32(vales[6]);
                        dbSparePart.EnglishName = vales[7];
                        dbSparePart.PinyinCode = vales[8];
                        dbSparePart.ReferenceCode = vales[9];
                        dbSparePart.ReferenceName = vales[10];
                        dbSparePart.CADCode = vales[11];
                        dbSparePart.CADName = vales[12];
                        dbSparePart.PartType = string.IsNullOrEmpty(vales[13]) ? 0 : Convert.ToInt32(vales[13]);
                        dbSparePart.Specification = vales[14];
                        dbSparePart.Feature = vales[15];
                        dbSparePart.Status = string.IsNullOrEmpty(vales[16]) ? 0 : Convert.ToInt32(vales[16]);
                        dbSparePart.Length = string.IsNullOrEmpty(vales[17]) ? 0 : Convert.ToDecimal(vales[17]);
                        dbSparePart.Width = string.IsNullOrEmpty(vales[18]) ? 0 : Convert.ToDecimal(vales[18]);
                        dbSparePart.Height = string.IsNullOrEmpty(vales[19]) ? 0 : Convert.ToDecimal(vales[19]);
                        dbSparePart.Volume = string.IsNullOrEmpty(vales[20]) ? 0 : Convert.ToDecimal(vales[20]);
                        dbSparePart.Weight = string.IsNullOrEmpty(vales[21]) ? 0 : Convert.ToDecimal(vales[21]);
                        dbSparePart.Material = vales[22];
                        dbSparePart.PackingAmount = string.IsNullOrEmpty(vales[23]) ? 0 : Convert.ToInt32(vales[23]);
                        dbSparePart.PackingSpecification = vales[24];
                        dbSparePart.PartsOutPackingCode = vales[25];
                        dbSparePart.PartsInPackingCode = vales[26];
                        dbSparePart.MeasureUnit = vales[27];
                        dbSparePart.MInPackingAmount = string.IsNullOrEmpty(vales[28]) ? 0 : Convert.ToInt32(vales[28]);
                        dbSparePart.IsNotWarrantyTransfer = string.IsNullOrEmpty(vales[29]) ? 0 : Convert.ToInt32(vales[29]);
                        dbSparePart.ExchangeIdentification =vales[30];
                        dbSparePartCodes.Add(dbSparePart);
                        return false;
                    };
                    //根据SQL查询数据库，将查出来的结果根据定义的委托中的方法（这里是getDbSparePartCodes）放到具体的集合中（这里是dbSparePartCodes）
                    db.QueryDataWithInOperator(@"select ID,
                                                                    CODE,
                                                                    NAME,
                                                                    ReferenceCode,
                                                                    LASTSUBSTITUTE,
                                                                    NEXTSUBSTITUTE,
                                                                    SHELFLIFE,
                                                                    ENGLISHNAME,
                                                                    PINYINCODE,
                                                                    REFERENCECODE,
                                                                    REFERENCENAME,
                                                                    CADCODE,
                                                                    CADNAME,
                                                                    PARTTYPE,
                                                                    SPECIFICATION,
                                                                    FEATURE,
                                                                    STATUS,
                                                                    LENGTH,
                                                                    WIDTH,
                                                                    HEIGHT,
                                                                    VOLUME,
                                                                    WEIGHT,
                                                                    MATERIAL,
                                                                    PACKINGAMOUNT,
                                                                    PACKINGSPECIFICATION,
                                                                    PARTSOUTPACKINGCODE,
                                                                    PARTSINPACKINGCODE,
                                                                    MEASUREUNIT,
                                                                    MINPACKINGAMOUNT,IsNotWarrantyTransfer,ExchangeIdentification
                                                                from sparepart where status=1 ", "Code", false, partCodesNeedCheck, getDbSparePartCodes);
                    if(partCodesNeedCheck.Length > 0) {
                        var sparePartsExistsCode = sparePartsNeedCheck.Where(r => dbSparePartCodes.Any(v => v.Code == r.Code)).ToArray();
                        if(sparePartsExistsCode.Count() < sparePartsNeedCheck.Count()) {
                            var notExist = allList.Except(sparePartsExistsCode);
                            foreach(var item in notExist) {
                                if(item.ErrorMsg == null) {
                                    item.ErrorMsg = ErrorStrings.Export_Validation_PartsBranch_Validation8 + item.Code + ErrorStrings.Export_Validation_Sparepart_NotExistOrStop;
                                } else {
                                    item.ErrorMsg = item.ErrorMsg + ErrorStrings.Export_Validation_PartsBranch_Validation9 + item.Code + ErrorStrings.Export_Validation_Sparepart_NotExistOrStop;
                                }
                                errorList.Add(item);
                            }
                        }
                    }
                }
                //获取所有不合格数据
                errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                //if(errList.Count > 0) {
                //    errorList.AddRange(errList);
                //}
                //获取所有合格数据
                rightList = allList.Except(errorList).ToList();

                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempSparePart = list[index - 1];
                            var values = new object[] {
                                tempSparePart.CodeStr, tempSparePart.EnglishNameStr,
                                tempSparePart.ErrorMsg
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }

                //导入所有合格数据
                if(!rightList.Any())
                    return true;
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    //开启事务，新增更新配件在一个事务内
                    var ts = conn.BeginTransaction();
                    try {
                        //新增配件
                        if(rightList.Any()) {
                            //获取新增数据的sql语句，Id为主键
                            var sqlUpdate = db.GetUpdateSql("SparePart", new string[] { "EnglishName", "ModifierId", "ModifierName", "ModifyTime", "OMSparePartMark" }, new string[] { "Code" });
                            var sqlInsertHistory = db.GetInsertSql("SparePartHistory", "Id", new[] {
                            "IsNotWarrantyTransfer",   "SparePartId","Code","ReferenceCode", "Name", "CADCode", "Specification", "EnglishName", "PartType", "MeasureUnit", "ShelfLife", "LastSubstitute", "NextSubstitute", "Weight", "Feature", "status", "CreatorId", "CreatorName", "CreateTime","MInPackingAmount","ExchangeIdentification"
                            });

                            var userInfo = Utils.GetCurrentUserInfo();
                            //往数据库增加配件信息
                            foreach(SparePartExtend item in rightList) {
                                var command = db.CreateDbCommand(sqlUpdate, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("Code", Strings.StrConv(item.Code, VbStrConv.Narrow).Replace("【", "[")));
                                command.Parameters.Add(db.CreateDbParameter("EnglishName", item.EnglishName));
                                command.Parameters.Add(db.CreateDbParameter("ModifierId", userInfo.Id));
                                command.Parameters.Add(db.CreateDbParameter("ModifierName", userInfo.Name));
                                command.Parameters.Add(db.CreateDbParameter("ModifyTime", DateTime.Now));
                                command.Parameters.Add(db.CreateDbParameter("OMSparePartMark", 0));
                                command.ExecuteNonQuery();

                                //                                //获取数据库的完整备件信息
                                //                                string[] partCode = new string[] { item.Code };

                                //                                Func<string[], bool> getDbSparePartInfo = vales => {
                                //                                    item.Id = Convert.ToInt32(vales[0]);
                                //                                    //item.Code = vales[1];
                                //                                    item.ReferenceCode = vales[3];
                                //                                    item.Name = vales[2];
                                //                                    item.LastSubstitute = vales[4];
                                //                                    item.NextSubstitute = vales[5];
                                //                                    item.ShelfLife = string.IsNullOrEmpty(vales[6]) ? 0 : Convert.ToInt32(vales[6]);
                                //                                    //item.EnglishName = vales[7];
                                //                                    item.PinyinCode = vales[8];
                                //                                    item.ReferenceCode = vales[9];
                                //                                    item.ReferenceName = vales[10];
                                //                                    item.CADCode = vales[11];
                                //                                    item.CADName = vales[12];
                                //                                    item.PartType = string.IsNullOrEmpty(vales[13]) ? 0 : Convert.ToInt32(vales[13]);
                                //                                    item.Specification = vales[14];
                                //                                    item.Feature = vales[15];
                                //                                    item.Status = string.IsNullOrEmpty(vales[16]) ? 0 : Convert.ToInt32(vales[16]);
                                //                                    item.Length = string.IsNullOrEmpty(vales[17]) ? 0 : Convert.ToDecimal(vales[17]);
                                //                                    item.Width = string.IsNullOrEmpty(vales[18]) ? 0 : Convert.ToDecimal(vales[18]);
                                //                                    item.Height = string.IsNullOrEmpty(vales[19]) ? 0 : Convert.ToDecimal(vales[19]);
                                //                                    item.Volume = string.IsNullOrEmpty(vales[20]) ? 0 : Convert.ToDecimal(vales[20]);
                                //                                    item.Weight = string.IsNullOrEmpty(vales[21]) ? 0 : Convert.ToDecimal(vales[21]);
                                //                                    item.Material = vales[22];
                                //                                    item.PackingAmount = string.IsNullOrEmpty(vales[23]) ? 0 : Convert.ToInt32(vales[23]);
                                //                                    item.PackingSpecification = vales[24];
                                //                                    item.PartsOutPackingCode = vales[25];
                                //                                    item.PartsInPackingCode = vales[26];
                                //                                    item.MeasureUnit = vales[27];
                                //                                    item.MInPackingAmount = string.IsNullOrEmpty(vales[28]) ? 0 : Convert.ToInt32(vales[28]);
                                //                                    return false;
                                //                                };

                                //                                db.QueryDataWithInOperator(@"select ID,
                                //                                                                    CODE,
                                //                                                                    NAME,
                                //                                                                    ReferenceCode,
                                //                                                                    LASTSUBSTITUTE,
                                //                                                                    NEXTSUBSTITUTE,
                                //                                                                    SHELFLIFE,
                                //                                                                    ENGLISHNAME,
                                //                                                                    PINYINCODE,
                                //                                                                    REFERENCECODE,
                                //                                                                    REFERENCENAME,
                                //                                                                    CADCODE,
                                //                                                                    CADNAME,
                                //                                                                    PARTTYPE,
                                //                                                                    SPECIFICATION,
                                //                                                                    FEATURE,
                                //                                                                    STATUS,
                                //                                                                    LENGTH,
                                //                                                                    WIDTH,
                                //                                                                    HEIGHT,
                                //                                                                    VOLUME,
                                //                                                                    WEIGHT,
                                //                                                                    MATERIAL,
                                //                                                                    PACKINGAMOUNT,
                                //                                                                    PACKINGSPECIFICATION,
                                //                                                                    PARTSOUTPACKINGCODE,
                                //                                                                    PARTSINPACKINGCODE,
                                //                                                                    MEASUREUNIT,
                                //                                                                    MINPACKINGAMOUNT
                                //                                                                from sparepart where status=1 ", "Code", true, partCode, getDbSparePartInfo);

                                //往数据库增加履历
                                var sparePart = dbSparePartCodes.Where(r => r.Code == item.Code).FirstOrDefault();
                                if(sparePart != null) {
                                    var commandHistory = db.CreateDbCommand(sqlInsertHistory, conn, ts);
                                    commandHistory.Parameters.Add(db.CreateDbParameter("SparePartId", sparePart.Id));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Code", Strings.StrConv(sparePart.Code, VbStrConv.Narrow).Replace("【", "[")));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Name", sparePart.Name));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("ReferenceCode", sparePart.ReferenceCode));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("CADCode", sparePart.CADCode));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("IsNotWarrantyTransfer", sparePart.IsNotWarrantyTransfer));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Specification", sparePart.Specification));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("EnglishName", item.EnglishName));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("PartType", sparePart.PartType));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("MeasureUnit", sparePart.MeasureUnit));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("ShelfLife", sparePart.ShelfLife));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("LastSubstitute", sparePart.LastSubstitute));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("NextSubstitute", sparePart.NextSubstitute));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Weight", sparePart.Weight));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Feature", sparePart.Feature));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Status", sparePart.Status));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("MInPackingAmount", sparePart.MInPackingAmount));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("ExchangeIdentification", sparePart.ExchangeIdentification));
                                    commandHistory.ExecuteNonQuery();
                                }
                            }

                        }
                        //无异常提交
                        ts.Commit();
                    } catch(Exception ex) {
                        //报错回滚
                        ts.Rollback();
                        throw new Exception(ex.Message);
                    } finally {
                        if(conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }

        public bool 批量替换配件类型(string fileName, out int excelImportNum, out List<SparePartExtend> rightData, out List<SparePartExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<SparePartExtend>();
            var rightList = new List<SparePartExtend>();
            var allList = new List<SparePartExtend>();
            var dbSparePartCodes = new List<SparePartExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("SparePart", out notNullableFields, out fieldLenght);
                List<object> excelColumns;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Code, "Code");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Sparepart_PartsType, "PartType");
                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;
                    var keyValuePairs = new[] {
                        new KeyValuePair<string, string>("PartType", "SparePart_PartType"),
                        new KeyValuePair<string, string>("Status","MasterData_Status") 
                            };
                    tempExcelOperator.ImportHelper.LoadEnumFromDb(null, null, keyValuePairs);
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var sparePart = new SparePartExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        sparePart.CodeStr = Strings.StrConv(newRow["Code"], VbStrConv.Narrow).Replace("【", "[");
                        sparePart.CodeStr = strconvsparecode(sparePart.CodeStr);
                        sparePart.Code = Strings.StrConv(newRow["Code"], VbStrConv.Narrow).Replace("【", "[");
                        sparePart.Code = strconvsparecode(sparePart.Code);
                        sparePart.Code = sparePart.Code;//全部大写
                        var tempErrorMessage = new List<string>();
                        //配件编号检查
                        var fieldIndex = notNullableFields.IndexOf("CODE");
                        if(string.IsNullOrEmpty(sparePart.Code)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.OverstockPartsAppDetail__Validation1);
                        } else {
                            //配件长度不得超过25
                            if(Encoding.Default.GetByteCount(sparePart.Code) > 25)
                                tempErrorMessage.Add(ErrorStrings.ImpPartsSupplierRelation_Validation8);
                        }

                        //配件类型检查
                        fieldIndex = notNullableFields.IndexOf("PartType".ToUpper());
                        if(string.IsNullOrEmpty(row["PartType"])) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Sparepart_PartTypeIsNull);
                        } else {
                            sparePart.PartTypeStr = row["PartType"]; //"];
                            var partType = tempExcelOperator.ImportHelper.GetEnumValue("PartType", sparePart.PartTypeStr);
                            if(sparePart.PartTypeStr != null && !partType.HasValue) {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Sparepart_PartTypeValueError);
                            } else {
                                sparePart.PartType = partType.HasValue ? partType.Value : 0;
                            }
                        }

                        if(tempErrorMessage.Count > 0) {
                            sparePart.ErrorMsg = string.Join("; ", tempErrorMessage);
                        } else {
                            var tempStatus = tempExcelOperator.ImportHelper.GetEnumValue("Status", ErrorStrings.Export_Validation_PartsBranch_Used);
                            if(tempStatus.HasValue) {
                                sparePart.Status = tempStatus.Value;
                            }
                        }
                        allList.Add(sparePart);
                        return false;
                    });
                    //校验导入的数据配件编号重复
                    var groups = allList.GroupBy(r =>
                        r.Code
                    ).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        if(groupItem.ErrorMsg == null) {
                            groupItem.ErrorMsg = ErrorStrings.Export_Title_PartsBranch_Code + groupItem.Code + ErrorStrings.Export_Validation_PartsBranch_Validation5;
                        } else {
                            groupItem.ErrorMsg = groupItem.ErrorMsg + ErrorStrings.Export_Validation_PartsBranch_Validation6 + groupItem.Code + ErrorStrings.Export_Validation_PartsBranch_Validation5;
                        }
                    }
                    var sparePartsNeedCheck = allList.ToList();
                    var partCodesNeedCheck = sparePartsNeedCheck.Select(r => r.Code).Distinct().ToArray();

                    Func<string[], bool> getDbSparePartCodes = vales => {
                        var dbSparePart = new SparePartExtend();
                        dbSparePart.Id = Convert.ToInt32(vales[0]);
                        dbSparePart.Code = vales[1];
                        dbSparePart.ReferenceCode = vales[3];
                        dbSparePart.Name = vales[2];
                        dbSparePart.LastSubstitute = vales[4];
                        dbSparePart.NextSubstitute = vales[5];
                        dbSparePart.ShelfLife = string.IsNullOrEmpty(vales[6]) ? 0 : Convert.ToInt32(vales[6]);
                        dbSparePart.EnglishName = vales[7];
                        dbSparePart.PinyinCode = vales[8];
                        dbSparePart.ReferenceCode = vales[9];
                        dbSparePart.ReferenceName = vales[10];
                        dbSparePart.CADCode = vales[11];
                        dbSparePart.CADName = vales[12];
                        dbSparePart.PartType = string.IsNullOrEmpty(vales[13]) ? 0 : Convert.ToInt32(vales[13]);
                        dbSparePart.Specification = vales[14];
                        dbSparePart.Feature = vales[15];
                        dbSparePart.Status = string.IsNullOrEmpty(vales[16]) ? 0 : Convert.ToInt32(vales[16]);
                        dbSparePart.Length = string.IsNullOrEmpty(vales[17]) ? 0 : Convert.ToDecimal(vales[17]);
                        dbSparePart.Width = string.IsNullOrEmpty(vales[18]) ? 0 : Convert.ToDecimal(vales[18]);
                        dbSparePart.Height = string.IsNullOrEmpty(vales[19]) ? 0 : Convert.ToDecimal(vales[19]);
                        dbSparePart.Volume = string.IsNullOrEmpty(vales[20]) ? 0 : Convert.ToDecimal(vales[20]);
                        dbSparePart.Weight = string.IsNullOrEmpty(vales[21]) ? 0 : Convert.ToDecimal(vales[21]);
                        dbSparePart.Material = vales[22];
                        dbSparePart.PackingAmount = string.IsNullOrEmpty(vales[23]) ? 0 : Convert.ToInt32(vales[23]);
                        dbSparePart.PackingSpecification = vales[24];
                        dbSparePart.PartsOutPackingCode = vales[25];
                        dbSparePart.PartsInPackingCode = vales[26];
                        dbSparePart.MeasureUnit = vales[27];
                        dbSparePart.MInPackingAmount = string.IsNullOrEmpty(vales[28]) ? 0 : Convert.ToInt32(vales[28]);
                        dbSparePart.IsNotWarrantyTransfer = string.IsNullOrEmpty(vales[29]) ? 0 : Convert.ToInt32(vales[29]);
                        dbSparePart.ExchangeIdentification = vales[30];
                        dbSparePartCodes.Add(dbSparePart);
                        return false;
                    };
                    //根据SQL查询数据库，将查出来的结果根据定义的委托中的方法（这里是getDbSparePartCodes）放到具体的集合中（这里是dbSparePartCodes）
                    db.QueryDataWithInOperator(@"select ID,
                                                                    CODE,
                                                                    NAME,
                                                                    ReferenceCode,
                                                                    LASTSUBSTITUTE,
                                                                    NEXTSUBSTITUTE,
                                                                    SHELFLIFE,
                                                                    ENGLISHNAME,
                                                                    PINYINCODE,
                                                                    REFERENCECODE,
                                                                    REFERENCENAME,
                                                                    CADCODE,
                                                                    CADNAME,
                                                                    PARTTYPE,
                                                                    SPECIFICATION,
                                                                    FEATURE,
                                                                    STATUS,
                                                                    LENGTH,
                                                                    WIDTH,
                                                                    HEIGHT,
                                                                    VOLUME,
                                                                    WEIGHT,
                                                                    MATERIAL,
                                                                    PACKINGAMOUNT,
                                                                    PACKINGSPECIFICATION,
                                                                    PARTSOUTPACKINGCODE,
                                                                    PARTSINPACKINGCODE,
                                                                    MEASUREUNIT,
                                                                    MINPACKINGAMOUNT,IsNotWarrantyTransfer,ExchangeIdentification
                                                                from sparepart where status=1 ", "Code", false, partCodesNeedCheck, getDbSparePartCodes);
                    if(partCodesNeedCheck.Length > 0) {
                        var sparePartsExistsCode = sparePartsNeedCheck.Where(r => dbSparePartCodes.Any(v => v.Code == r.Code)).ToArray();
                        if(sparePartsExistsCode.Count() < sparePartsNeedCheck.Count()) {
                            var notExist = allList.Except(sparePartsExistsCode);
                            foreach(var item in notExist) {
                                if(item.ErrorMsg == null) {
                                    item.ErrorMsg = ErrorStrings.Export_Validation_PartsBranch_Validation8 + item.Code + ErrorStrings.Export_Validation_Sparepart_NotExistOrStop;
                                } else {
                                    item.ErrorMsg = item.ErrorMsg + ErrorStrings.Export_Validation_PartsBranch_Validation9 + item.Code + ErrorStrings.Export_Validation_Sparepart_NotExistOrStop;
                                }
                                errorList.Add(item);
                            }
                        }
                    }
                }
                //获取所有不合格数据
                errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                //if(errList.Count > 0) {
                //    errorList.AddRange(errList);
                //}
                //获取所有合格数据
                rightList = allList.Except(errorList).ToList();

                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempSparePart = list[index - 1];
                            var values = new object[] {
                                tempSparePart.CodeStr, tempSparePart.PartTypeStr,
                                tempSparePart.ErrorMsg
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }

                //导入所有合格数据
                if(!rightList.Any())
                    return true;
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    //开启事务，新增更新配件在一个事务内
                    var ts = conn.BeginTransaction();
                    try {
                        //新增配件
                        if(rightList.Any()) {
                            //获取新增数据的sql语句，Id为主键
                            var sqlUpdate = db.GetUpdateSql("SparePart", new string[] { "PartType", "ModifierId", "ModifierName", "ModifyTime", "OMSparePartMark" }, new string[] { "Code" });
                            var sqlInsertHistory = db.GetInsertSql("SparePartHistory", "Id", new[] {
                              "IsNotWarrantyTransfer", "SparePartId","Code","ReferenceCode", "Name", "CADCode", "Specification", "EnglishName", "PartType", "MeasureUnit", "ShelfLife", "LastSubstitute", "NextSubstitute", "Weight", "Feature", "status", "CreatorId", "CreatorName", "CreateTime","MInPackingAmount","ExchangeIdentification"
                            });

                            var userInfo = Utils.GetCurrentUserInfo();
                            //往数据库增加配件信息
                            foreach(SparePartExtend item in rightList) {
                                var command = db.CreateDbCommand(sqlUpdate, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("Code", Strings.StrConv(item.Code, VbStrConv.Narrow).Replace("【", "[")));
                                command.Parameters.Add(db.CreateDbParameter("PartType", item.PartType));
                                command.Parameters.Add(db.CreateDbParameter("ModifierId", userInfo.Id));
                                command.Parameters.Add(db.CreateDbParameter("ModifierName", userInfo.Name));
                                command.Parameters.Add(db.CreateDbParameter("ModifyTime", DateTime.Now));
                                command.Parameters.Add(db.CreateDbParameter("OMSparePartMark", 0));
                                command.ExecuteNonQuery();

                                //                                //获取数据库的完整备件信息
                                //                                string[] partCode = new string[] { item.Code };

                                //                                Func<string[], bool> getDbSparePartInfo = vales => {
                                //                                    item.Id = Convert.ToInt32(vales[0]);
                                //                                    //item.Code = vales[1];
                                //                                    item.ReferenceCode = vales[3];
                                //                                    item.Name = vales[2];
                                //                                    item.LastSubstitute = vales[4];
                                //                                    item.NextSubstitute = vales[5];
                                //                                    item.ShelfLife = string.IsNullOrEmpty(vales[6]) ? 0 : Convert.ToInt32(vales[6]);
                                //                                    item.EnglishName = vales[7];
                                //                                    item.PinyinCode = vales[8];
                                //                                    item.ReferenceCode = vales[9];
                                //                                    item.ReferenceName = vales[10];
                                //                                    item.CADCode = vales[11];
                                //                                    item.CADName = vales[12];
                                //                                    //item.PartType = string.IsNullOrEmpty(vales[13]) ? 0 : Convert.ToInt32(vales[13]);
                                //                                    item.Specification = vales[14];
                                //                                    item.Feature = vales[15];
                                //                                    item.Status = string.IsNullOrEmpty(vales[16]) ? 0 : Convert.ToInt32(vales[16]);
                                //                                    item.Length = string.IsNullOrEmpty(vales[17]) ? 0 : Convert.ToDecimal(vales[17]);
                                //                                    item.Width = string.IsNullOrEmpty(vales[18]) ? 0 : Convert.ToDecimal(vales[18]);
                                //                                    item.Height = string.IsNullOrEmpty(vales[19]) ? 0 : Convert.ToDecimal(vales[19]);
                                //                                    item.Volume = string.IsNullOrEmpty(vales[20]) ? 0 : Convert.ToDecimal(vales[20]);
                                //                                    item.Weight = string.IsNullOrEmpty(vales[21]) ? 0 : Convert.ToDecimal(vales[21]);
                                //                                    item.Material = vales[22];
                                //                                    item.PackingAmount = string.IsNullOrEmpty(vales[23]) ? 0 : Convert.ToInt32(vales[23]);
                                //                                    item.PackingSpecification = vales[24];
                                //                                    item.PartsOutPackingCode = vales[25];
                                //                                    item.PartsInPackingCode = vales[26];
                                //                                    item.MeasureUnit = vales[27];
                                //                                    item.MInPackingAmount = string.IsNullOrEmpty(vales[28]) ? 0 : Convert.ToInt32(vales[28]);
                                //                                    return false;
                                //                                };

                                //                                db.QueryDataWithInOperator(@"select ID,
                                //                                                                    CODE,
                                //                                                                    NAME,
                                //                                                                    ReferenceCode,
                                //                                                                    LASTSUBSTITUTE,
                                //                                                                    NEXTSUBSTITUTE,
                                //                                                                    SHELFLIFE,
                                //                                                                    ENGLISHNAME,
                                //                                                                    PINYINCODE,
                                //                                                                    REFERENCECODE,
                                //                                                                    REFERENCENAME,
                                //                                                                    CADCODE,
                                //                                                                    CADNAME,
                                //                                                                    PARTTYPE,
                                //                                                                    SPECIFICATION,
                                //                                                                    FEATURE,
                                //                                                                    STATUS,
                                //                                                                    LENGTH,
                                //                                                                    WIDTH,
                                //                                                                    HEIGHT,
                                //                                                                    VOLUME,
                                //                                                                    WEIGHT,
                                //                                                                    MATERIAL,
                                //                                                                    PACKINGAMOUNT,
                                //                                                                    PACKINGSPECIFICATION,
                                //                                                                    PARTSOUTPACKINGCODE,
                                //                                                                    PARTSINPACKINGCODE,
                                //                                                                    MEASUREUNIT,
                                //                                                                    MINPACKINGAMOUNT
                                //                                                                from sparepart where status=1 ", "Code", true, partCode, getDbSparePartInfo);

                                //往数据库增加履历
                                var sparePart = dbSparePartCodes.Where(r => r.Code == item.Code).FirstOrDefault();
                                if(sparePart != null) {
                                    var commandHistory = db.CreateDbCommand(sqlInsertHistory, conn, ts);
                                    commandHistory.Parameters.Add(db.CreateDbParameter("SparePartId", sparePart.Id));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Code", Strings.StrConv(sparePart.Code, VbStrConv.Narrow).Replace("【", "[")));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Name", sparePart.Name));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("ReferenceCode", sparePart.ReferenceCode));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("CADCode", sparePart.CADCode));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Specification", sparePart.Specification));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("EnglishName", sparePart.EnglishName));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("PartType", item.PartType));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("IsNotWarrantyTransfer", sparePart.IsNotWarrantyTransfer));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("MeasureUnit", sparePart.MeasureUnit));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("ShelfLife", sparePart.ShelfLife));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("LastSubstitute", sparePart.LastSubstitute));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("NextSubstitute", sparePart.NextSubstitute));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Weight", sparePart.Weight));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Feature", sparePart.Feature));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Status", sparePart.Status));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("MInPackingAmount", sparePart.MInPackingAmount));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("ExchangeIdentification", sparePart.ExchangeIdentification));
                                    commandHistory.ExecuteNonQuery();
                                }
                            }

                        }
                        //无异常提交
                        ts.Commit();
                    } catch(Exception ex) {
                        //报错回滚
                        ts.Rollback();
                        throw new Exception(ex.Message);
                    } finally {
                        if(conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }
        public bool 批量修改追溯属性(string fileName, out int excelImportNum, out List<SparePartExtend> rightData, out List<SparePartExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<SparePartExtend>();
            var rightList = new List<SparePartExtend>();
            var allList = new List<SparePartExtend>();
            var dbSparePartCodes = new List<SparePartExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("SparePart", out notNullableFields, out fieldLenght);
                List<object> excelColumns;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Code, "Code");
                    excelOperator.AddColumnDataSource("追溯属性", "TraceProperty");
                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;
                    var keyValuePairs = new[] {
                        new KeyValuePair<string, string>("TraceProperty", "TraceProperty"),
                            };
                    tempExcelOperator.ImportHelper.LoadEnumFromDb(null, null, keyValuePairs);
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var sparePart = new SparePartExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        sparePart.CodeStr = Strings.StrConv(newRow["Code"], VbStrConv.Narrow).Replace("【", "[");
                        sparePart.CodeStr = strconvsparecode(sparePart.CodeStr);
                        sparePart.Code = Strings.StrConv(newRow["Code"], VbStrConv.Narrow).Replace("【", "[");
                        sparePart.Code = strconvsparecode(sparePart.Code);
                        sparePart.Code = sparePart.Code;//全部大写
                        var tempErrorMessage = new List<string>();
                        //配件编号检查
                        var fieldIndex = notNullableFields.IndexOf("CODE");
                        if(string.IsNullOrEmpty(sparePart.Code)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.OverstockPartsAppDetail__Validation1);
                        } else {
                            //配件长度不得超过25
                            if(Encoding.Default.GetByteCount(sparePart.Code) > 25)
                                tempErrorMessage.Add(ErrorStrings.ImpPartsSupplierRelation_Validation8);
                        }

                        //配件类型检查
                        fieldIndex = notNullableFields.IndexOf("TraceProperty".ToUpper());
                        if(string.IsNullOrEmpty(row["TraceProperty"])) {
                            //if (fieldIndex > -1)
                            //tempErrorMessage.Add(ErrorStrings.Export_Validation_Sparepart_PartTypeIsNull);
                        } else {
                            sparePart.TracePropertyStr = row["TraceProperty"]; //"];
                            var traceProperty = tempExcelOperator.ImportHelper.GetEnumValue("TraceProperty", sparePart.TracePropertyStr);
                            if(sparePart.TracePropertyStr != null && !traceProperty.HasValue) {
                                tempErrorMessage.Add("追溯类型不正确");
                            } else {
                                sparePart.TraceProperty = traceProperty.HasValue ? traceProperty.Value : 0;
                            }
                        }

                        if(tempErrorMessage.Count > 0) {
                            sparePart.ErrorMsg = string.Join("; ", tempErrorMessage);
                        } 
                        allList.Add(sparePart);
                        return false;
                    });
                    //校验导入的数据配件编号重复
                    var groups = allList.GroupBy(r =>
                        r.Code
                    ).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        if(groupItem.ErrorMsg == null) {
                            groupItem.ErrorMsg = ErrorStrings.Export_Title_PartsBranch_Code + groupItem.Code + ErrorStrings.Export_Validation_PartsBranch_Validation5;
                        } else {
                            groupItem.ErrorMsg = groupItem.ErrorMsg + ErrorStrings.Export_Validation_PartsBranch_Validation6 + groupItem.Code + ErrorStrings.Export_Validation_PartsBranch_Validation5;
                        }
                    }
                    var sparePartsNeedCheck = allList.ToList();
                    var partCodesNeedCheck = sparePartsNeedCheck.Select(r => r.Code).Distinct().ToArray();

                    Func<string[], bool> getDbSparePartCodes = vales => {
                        var dbSparePart = new SparePartExtend();
                        dbSparePart.Id = Convert.ToInt32(vales[0]);
                        dbSparePart.Code = vales[1];
                        dbSparePart.ReferenceCode = vales[3];
                        dbSparePart.Name = vales[2];
                        dbSparePart.LastSubstitute = vales[4];
                        dbSparePart.NextSubstitute = vales[5];
                        dbSparePart.ShelfLife = string.IsNullOrEmpty(vales[6]) ? 0 : Convert.ToInt32(vales[6]);
                        dbSparePart.EnglishName = vales[7];
                        dbSparePart.PinyinCode = vales[8];
                        dbSparePart.ReferenceCode = vales[9];
                        dbSparePart.ReferenceName = vales[10];
                        dbSparePart.CADCode = vales[11];
                        dbSparePart.CADName = vales[12];
                        dbSparePart.PartType = string.IsNullOrEmpty(vales[13]) ? 0 : Convert.ToInt32(vales[13]);
                        dbSparePart.Specification = vales[14];
                        dbSparePart.Feature = vales[15];
                        dbSparePart.Status = string.IsNullOrEmpty(vales[16]) ? 0 : Convert.ToInt32(vales[16]);
                        dbSparePart.Length = string.IsNullOrEmpty(vales[17]) ? 0 : Convert.ToDecimal(vales[17]);
                        dbSparePart.Width = string.IsNullOrEmpty(vales[18]) ? 0 : Convert.ToDecimal(vales[18]);
                        dbSparePart.Height = string.IsNullOrEmpty(vales[19]) ? 0 : Convert.ToDecimal(vales[19]);
                        dbSparePart.Volume = string.IsNullOrEmpty(vales[20]) ? 0 : Convert.ToDecimal(vales[20]);
                        dbSparePart.Weight = string.IsNullOrEmpty(vales[21]) ? 0 : Convert.ToDecimal(vales[21]);
                        dbSparePart.Material = vales[22];
                        dbSparePart.PackingAmount = string.IsNullOrEmpty(vales[23]) ? 0 : Convert.ToInt32(vales[23]);
                        dbSparePart.PackingSpecification = vales[24];
                        dbSparePart.PartsOutPackingCode = vales[25];
                        dbSparePart.PartsInPackingCode = vales[26];
                        dbSparePart.MeasureUnit = vales[27];
                        dbSparePart.MInPackingAmount = string.IsNullOrEmpty(vales[28]) ? 0 : Convert.ToInt32(vales[28]);
                        dbSparePart.IsNotWarrantyTransfer = string.IsNullOrEmpty(vales[29]) ? 0 : Convert.ToInt32(vales[29]);
                        dbSparePart.ExchangeIdentification = vales[30];
                        dbSparePart.TraceProperty = string.IsNullOrEmpty(vales[31]) ? 0 : Convert.ToInt32(vales[31]);
                        dbSparePartCodes.Add(dbSparePart);
                        return false;
                    };
                    //根据SQL查询数据库，将查出来的结果根据定义的委托中的方法（这里是getDbSparePartCodes）放到具体的集合中（这里是dbSparePartCodes）
                    db.QueryDataWithInOperator(@"select ID,
                                                                    CODE,
                                                                    NAME,
                                                                    ReferenceCode,
                                                                    LASTSUBSTITUTE,
                                                                    NEXTSUBSTITUTE,
                                                                    SHELFLIFE,
                                                                    ENGLISHNAME,
                                                                    PINYINCODE,
                                                                    REFERENCECODE,
                                                                    REFERENCENAME,
                                                                    CADCODE,
                                                                    CADNAME,
                                                                    PARTTYPE,
                                                                    SPECIFICATION,
                                                                    FEATURE,
                                                                    STATUS,
                                                                    LENGTH,
                                                                    WIDTH,
                                                                    HEIGHT,
                                                                    VOLUME,
                                                                    WEIGHT,
                                                                    MATERIAL,
                                                                    PACKINGAMOUNT,
                                                                    PACKINGSPECIFICATION,
                                                                    PARTSOUTPACKINGCODE,
                                                                    PARTSINPACKINGCODE,
                                                                    MEASUREUNIT,
                                                                    MINPACKINGAMOUNT,
                                                                   IsNotWarrantyTransfer,
                                                                   ExchangeIdentification,TraceProperty
                                                                from sparepart where status=1 ", "Code", false, partCodesNeedCheck, getDbSparePartCodes);
                    if(partCodesNeedCheck.Length > 0) {
                        var sparePartsExistsCode = sparePartsNeedCheck.Where(r => dbSparePartCodes.Any(v => v.Code == r.Code)).ToArray();
                        if(sparePartsExistsCode.Count() < sparePartsNeedCheck.Count()) {
                            var notExist = allList.Except(sparePartsExistsCode);
                            foreach(var item in notExist) {
                                if(item.ErrorMsg == null) {
                                    item.ErrorMsg = ErrorStrings.Export_Validation_PartsBranch_Validation8 + item.Code + ErrorStrings.Export_Validation_Sparepart_NotExistOrStop;
                                } else {
                                    item.ErrorMsg = item.ErrorMsg + ErrorStrings.Export_Validation_PartsBranch_Validation9 + item.Code + ErrorStrings.Export_Validation_Sparepart_NotExistOrStop;
                                }
                                errorList.Add(item);
                            }
                        }
                    }
                }
                //获取所有不合格数据
                errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                //if(errList.Count > 0) {
                //    errorList.AddRange(errList);
                //}
                //获取所有合格数据
                rightList = allList.Except(errorList).ToList();

                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempSparePart = list[index - 1];
                            var values = new object[] {
                                tempSparePart.CodeStr, tempSparePart.TracePropertyStr,
                                tempSparePart.ErrorMsg
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }

                //导入所有合格数据
                if(!rightList.Any())
                    return true;
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    //开启事务，新增更新配件在一个事务内
                    var ts = conn.BeginTransaction();
                    try {
                        //新增配件
                        if(rightList.Any()) {
                            //获取新增数据的sql语句，Id为主键
                            var sqlUpdate = db.GetUpdateSql("SparePart", new string[] { "TraceProperty", "ModifierId", "ModifierName", "ModifyTime", "OMSparePartMark" }, new string[] { "Code" });                          
                            var userInfo = Utils.GetCurrentUserInfo();
                            //往数据库增加配件信息
                            foreach(SparePartExtend item in rightList) {
                                var command = db.CreateDbCommand(sqlUpdate, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("Code", Strings.StrConv(item.Code, VbStrConv.Narrow).Replace("【", "[")));
                                command.Parameters.Add(db.CreateDbParameter("TraceProperty", item.TraceProperty));
                                command.Parameters.Add(db.CreateDbParameter("ModifierId", userInfo.Id));
                                command.Parameters.Add(db.CreateDbParameter("ModifierName", userInfo.Name));
                                command.Parameters.Add(db.CreateDbParameter("ModifyTime", DateTime.Now));
                                command.Parameters.Add(db.CreateDbParameter("OMSparePartMark", 0));
                                command.ExecuteNonQuery();                              
                               
                            }

                        }
                        //无异常提交
                        ts.Commit();
                    } catch(Exception ex) {
                        //报错回滚
                        ts.Rollback();
                        throw new Exception(ex.Message);
                    } finally {
                        if(conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }
        public bool 批量修改安全天数最大值(string fileName, out int excelImportNum, out List<SparePartExtend> rightData, out List<SparePartExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<SparePartExtend>();
            var rightList = new List<SparePartExtend>();
            var allList = new List<SparePartExtend>();
            var dbSparePartCodes = new List<SparePartExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("SparePart", out notNullableFields, out fieldLenght);
                List<object> excelColumns;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Code, "Code");
                    excelOperator.AddColumnDataSource("安全天数最大值", "SafeDays");
                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var sparePart = new SparePartExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        sparePart.CodeStr = Strings.StrConv(newRow["Code"], VbStrConv.Narrow).Replace("【", "[");
                        sparePart.CodeStr = strconvsparecode(sparePart.CodeStr);
                        sparePart.Code = Strings.StrConv(newRow["Code"], VbStrConv.Narrow).Replace("【", "[");
                        sparePart.Code = strconvsparecode(sparePart.Code);
                        sparePart.Code = sparePart.Code;//全部大写
                        var tempErrorMessage = new List<string>();
                        //配件编号检查
                        var fieldIndex = notNullableFields.IndexOf("CODE");
                        if(string.IsNullOrEmpty(sparePart.Code)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.OverstockPartsAppDetail__Validation1);
                        } else {
                            //配件长度不得超过25
                            if(Encoding.Default.GetByteCount(sparePart.Code) > 25)
                                tempErrorMessage.Add(ErrorStrings.ImpPartsSupplierRelation_Validation8);
                        }

                        //配件类型检查
                        fieldIndex = notNullableFields.IndexOf("SafeDays".ToUpper());
                        if(string.IsNullOrEmpty(row["SafeDays"])) {
                            //if (fieldIndex > -1)
                            //tempErrorMessage.Add(ErrorStrings.Export_Validation_Sparepart_PartTypeIsNull);
                        } else {
                            sparePart.SafeDaysStr = row["SafeDays"]; //"];
                            var safeDays =Convert.ToInt32(sparePart.SafeDaysStr);
                            if(sparePart.SafeDaysStr != null  && safeDays<0) {
                                tempErrorMessage.Add("安全天数最大值不能小于0");
                            } else {
                                sparePart.SafeDays = safeDays;
                            }
                        }

                        if(tempErrorMessage.Count > 0) {
                            sparePart.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(sparePart);
                        return false;
                    });
                    //校验导入的数据配件编号重复
                    var groups = allList.GroupBy(r =>
                        r.Code
                    ).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        if(groupItem.ErrorMsg == null) {
                            groupItem.ErrorMsg = ErrorStrings.Export_Title_PartsBranch_Code + groupItem.Code + ErrorStrings.Export_Validation_PartsBranch_Validation5;
                        } else {
                            groupItem.ErrorMsg = groupItem.ErrorMsg + ErrorStrings.Export_Validation_PartsBranch_Validation6 + groupItem.Code + ErrorStrings.Export_Validation_PartsBranch_Validation5;
                        }
                    }
                    var sparePartsNeedCheck = allList.ToList();
                    var partCodesNeedCheck = sparePartsNeedCheck.Select(r => r.Code).Distinct().ToArray();

                    Func<string[], bool> getDbSparePartCodes = vales => {
                        var dbSparePart = new SparePartExtend();
                        dbSparePart.Id = Convert.ToInt32(vales[0]);
                        dbSparePart.Code = vales[1];
                        dbSparePart.ReferenceCode = vales[3];
                        dbSparePart.Name = vales[2];
                        dbSparePart.LastSubstitute = vales[4];
                        dbSparePart.NextSubstitute = vales[5];
                        dbSparePart.ShelfLife = string.IsNullOrEmpty(vales[6]) ? 0 : Convert.ToInt32(vales[6]);
                        dbSparePart.EnglishName = vales[7];
                        dbSparePart.PinyinCode = vales[8];
                        dbSparePart.ReferenceCode = vales[9];
                        dbSparePart.ReferenceName = vales[10];
                        dbSparePart.CADCode = vales[11];
                        dbSparePart.CADName = vales[12];
                        dbSparePart.PartType = string.IsNullOrEmpty(vales[13]) ? 0 : Convert.ToInt32(vales[13]);
                        dbSparePart.Specification = vales[14];
                        dbSparePart.Feature = vales[15];
                        dbSparePart.Status = string.IsNullOrEmpty(vales[16]) ? 0 : Convert.ToInt32(vales[16]);
                        dbSparePart.Length = string.IsNullOrEmpty(vales[17]) ? 0 : Convert.ToDecimal(vales[17]);
                        dbSparePart.Width = string.IsNullOrEmpty(vales[18]) ? 0 : Convert.ToDecimal(vales[18]);
                        dbSparePart.Height = string.IsNullOrEmpty(vales[19]) ? 0 : Convert.ToDecimal(vales[19]);
                        dbSparePart.Volume = string.IsNullOrEmpty(vales[20]) ? 0 : Convert.ToDecimal(vales[20]);
                        dbSparePart.Weight = string.IsNullOrEmpty(vales[21]) ? 0 : Convert.ToDecimal(vales[21]);
                        dbSparePart.Material = vales[22];
                        dbSparePart.PackingAmount = string.IsNullOrEmpty(vales[23]) ? 0 : Convert.ToInt32(vales[23]);
                        dbSparePart.PackingSpecification = vales[24];
                        dbSparePart.PartsOutPackingCode = vales[25];
                        dbSparePart.PartsInPackingCode = vales[26];
                        dbSparePart.MeasureUnit = vales[27];
                        dbSparePart.MInPackingAmount = string.IsNullOrEmpty(vales[28]) ? 0 : Convert.ToInt32(vales[28]);
                        dbSparePart.IsNotWarrantyTransfer = string.IsNullOrEmpty(vales[29]) ? 0 : Convert.ToInt32(vales[29]);
                        dbSparePart.ExchangeIdentification = vales[30];
                        dbSparePart.TraceProperty = string.IsNullOrEmpty(vales[31]) ? 0 : Convert.ToInt32(vales[31]);
                        dbSparePart.SafeDays = string.IsNullOrEmpty(vales[32]) ? 0 : Convert.ToInt32(vales[32]);
                        dbSparePartCodes.Add(dbSparePart);
                        return false;
                    };
                    //根据SQL查询数据库，将查出来的结果根据定义的委托中的方法（这里是getDbSparePartCodes）放到具体的集合中（这里是dbSparePartCodes）
                    db.QueryDataWithInOperator(@"select ID,
                                                                    CODE,
                                                                    NAME,
                                                                    ReferenceCode,
                                                                    LASTSUBSTITUTE,
                                                                    NEXTSUBSTITUTE,
                                                                    SHELFLIFE,
                                                                    ENGLISHNAME,
                                                                    PINYINCODE,
                                                                    REFERENCECODE,
                                                                    REFERENCENAME,
                                                                    CADCODE,
                                                                    CADNAME,
                                                                    PARTTYPE,
                                                                    SPECIFICATION,
                                                                    FEATURE,
                                                                    STATUS,
                                                                    LENGTH,
                                                                    WIDTH,
                                                                    HEIGHT,
                                                                    VOLUME,
                                                                    WEIGHT,
                                                                    MATERIAL,
                                                                    PACKINGAMOUNT,
                                                                    PACKINGSPECIFICATION,
                                                                    PARTSOUTPACKINGCODE,
                                                                    PARTSINPACKINGCODE,
                                                                    MEASUREUNIT,
                                                                    MINPACKINGAMOUNT,
                                                                   IsNotWarrantyTransfer,
                                                                   ExchangeIdentification,TraceProperty,SafeDays
                                                                from sparepart where status=1 ", "Code", false, partCodesNeedCheck, getDbSparePartCodes);
                    if(partCodesNeedCheck.Length > 0) {
                        var sparePartsExistsCode = sparePartsNeedCheck.Where(r => dbSparePartCodes.Any(v => v.Code == r.Code)).ToArray();
                        if(sparePartsExistsCode.Count() < sparePartsNeedCheck.Count()) {
                            var notExist = allList.Except(sparePartsExistsCode);
                            foreach(var item in notExist) {
                                if(item.ErrorMsg == null) {
                                    item.ErrorMsg = ErrorStrings.Export_Validation_PartsBranch_Validation8 + item.Code + ErrorStrings.Export_Validation_Sparepart_NotExistOrStop;
                                } else {
                                    item.ErrorMsg = item.ErrorMsg + ErrorStrings.Export_Validation_PartsBranch_Validation9 + item.Code + ErrorStrings.Export_Validation_Sparepart_NotExistOrStop;
                                }
                                errorList.Add(item);
                            }
                        }
                    }
                }
                //获取所有不合格数据
                errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                //if(errList.Count > 0) {
                //    errorList.AddRange(errList);
                //}
                //获取所有合格数据
                rightList = allList.Except(errorList).ToList();

                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempSparePart = list[index - 1];
                            var values = new object[] {
                                tempSparePart.CodeStr, tempSparePart.SafeDaysStr,
                                tempSparePart.ErrorMsg
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }

                //导入所有合格数据
                if(!rightList.Any())
                    return true;
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    //开启事务，新增更新配件在一个事务内
                    var ts = conn.BeginTransaction();
                    try {
                        //新增配件
                        if(rightList.Any()) {
                            //获取新增数据的sql语句，Id为主键
                            var sqlUpdate = db.GetUpdateSql("SparePart", new string[] { "SafeDays", "ModifierId", "ModifierName", "ModifyTime", "OMSparePartMark" }, new string[] { "Code" });
                            var userInfo = Utils.GetCurrentUserInfo();
                            //往数据库增加配件信息
                            foreach(SparePartExtend item in rightList) {
                                var command = db.CreateDbCommand(sqlUpdate, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("Code", Strings.StrConv(item.Code, VbStrConv.Narrow).Replace("【", "[")));
                                command.Parameters.Add(db.CreateDbParameter("SafeDays", item.SafeDays));
                                command.Parameters.Add(db.CreateDbParameter("ModifierId", userInfo.Id));
                                command.Parameters.Add(db.CreateDbParameter("ModifierName", userInfo.Name));
                                command.Parameters.Add(db.CreateDbParameter("ModifyTime", DateTime.Now));
                                command.Parameters.Add(db.CreateDbParameter("OMSparePartMark", 0));
                                command.ExecuteNonQuery();

                            }

                        }
                        //无异常提交
                        ts.Commit();
                    } catch(Exception ex) {
                        //报错回滚
                        ts.Rollback();
                        throw new Exception(ex.Message);
                    } finally {
                        if(conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }
        public bool 批量修改库房天数(string fileName, out int excelImportNum, out List<SparePartExtend> rightData, out List<SparePartExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<SparePartExtend>();
            var rightList = new List<SparePartExtend>();
            var allList = new List<SparePartExtend>();
            var dbSparePartCodes = new List<SparePartExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("SparePart", out notNullableFields, out fieldLenght);
                List<object> excelColumns;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Code, "Code");
                    excelOperator.AddColumnDataSource("库房天数", "WarehousDays");
                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var sparePart = new SparePartExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        sparePart.CodeStr = Strings.StrConv(newRow["Code"], VbStrConv.Narrow).Replace("【", "[");
                        sparePart.CodeStr = strconvsparecode(sparePart.CodeStr);
                        sparePart.Code = Strings.StrConv(newRow["Code"], VbStrConv.Narrow).Replace("【", "[");
                        sparePart.Code = strconvsparecode(sparePart.Code);
                        sparePart.Code = sparePart.Code;//全部大写
                        var tempErrorMessage = new List<string>();
                        //配件编号检查
                        var fieldIndex = notNullableFields.IndexOf("CODE");
                        if(string.IsNullOrEmpty(sparePart.Code)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.OverstockPartsAppDetail__Validation1);
                        } else {
                            //配件长度不得超过25
                            if(Encoding.Default.GetByteCount(sparePart.Code) > 25)
                                tempErrorMessage.Add(ErrorStrings.ImpPartsSupplierRelation_Validation8);
                        }

                        //配件类型检查
                        fieldIndex = notNullableFields.IndexOf("WarehousDays".ToUpper());
                        if(string.IsNullOrEmpty(row["WarehousDays"])) {
                            //if (fieldIndex > -1)
                            //tempErrorMessage.Add(ErrorStrings.Export_Validation_Sparepart_PartTypeIsNull);
                        } else {
                            sparePart.WarehousDaysStr = row["WarehousDays"]; //"];
                            var warehousDays = Convert.ToInt32(sparePart.WarehousDaysStr);
                            if(sparePart.WarehousDaysStr != null  && warehousDays < 0) {
                                tempErrorMessage.Add("库房天数不能小于0");
                            } else {
                                sparePart.WarehousDays = warehousDays;
                            }
                        }

                        if(tempErrorMessage.Count > 0) {
                            sparePart.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(sparePart);
                        return false;
                    });
                    //校验导入的数据配件编号重复
                    var groups = allList.GroupBy(r =>
                        r.Code
                    ).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        if(groupItem.ErrorMsg == null) {
                            groupItem.ErrorMsg = ErrorStrings.Export_Title_PartsBranch_Code + groupItem.Code + ErrorStrings.Export_Validation_PartsBranch_Validation5;
                        } else {
                            groupItem.ErrorMsg = groupItem.ErrorMsg + ErrorStrings.Export_Validation_PartsBranch_Validation6 + groupItem.Code + ErrorStrings.Export_Validation_PartsBranch_Validation5;
                        }
                    }
                    var sparePartsNeedCheck = allList.ToList();
                    var partCodesNeedCheck = sparePartsNeedCheck.Select(r => r.Code).Distinct().ToArray();

                    Func<string[], bool> getDbSparePartCodes = vales => {
                        var dbSparePart = new SparePartExtend();
                        dbSparePart.Id = Convert.ToInt32(vales[0]);
                        dbSparePart.Code = vales[1];
                        dbSparePart.ReferenceCode = vales[3];
                        dbSparePart.Name = vales[2];
                        dbSparePart.LastSubstitute = vales[4];
                        dbSparePart.NextSubstitute = vales[5];
                        dbSparePart.ShelfLife = string.IsNullOrEmpty(vales[6]) ? 0 : Convert.ToInt32(vales[6]);
                        dbSparePart.EnglishName = vales[7];
                        dbSparePart.PinyinCode = vales[8];
                        dbSparePart.ReferenceCode = vales[9];
                        dbSparePart.ReferenceName = vales[10];
                        dbSparePart.CADCode = vales[11];
                        dbSparePart.CADName = vales[12];
                        dbSparePart.PartType = string.IsNullOrEmpty(vales[13]) ? 0 : Convert.ToInt32(vales[13]);
                        dbSparePart.Specification = vales[14];
                        dbSparePart.Feature = vales[15];
                        dbSparePart.Status = string.IsNullOrEmpty(vales[16]) ? 0 : Convert.ToInt32(vales[16]);
                        dbSparePart.Length = string.IsNullOrEmpty(vales[17]) ? 0 : Convert.ToDecimal(vales[17]);
                        dbSparePart.Width = string.IsNullOrEmpty(vales[18]) ? 0 : Convert.ToDecimal(vales[18]);
                        dbSparePart.Height = string.IsNullOrEmpty(vales[19]) ? 0 : Convert.ToDecimal(vales[19]);
                        dbSparePart.Volume = string.IsNullOrEmpty(vales[20]) ? 0 : Convert.ToDecimal(vales[20]);
                        dbSparePart.Weight = string.IsNullOrEmpty(vales[21]) ? 0 : Convert.ToDecimal(vales[21]);
                        dbSparePart.Material = vales[22];
                        dbSparePart.PackingAmount = string.IsNullOrEmpty(vales[23]) ? 0 : Convert.ToInt32(vales[23]);
                        dbSparePart.PackingSpecification = vales[24];
                        dbSparePart.PartsOutPackingCode = vales[25];
                        dbSparePart.PartsInPackingCode = vales[26];
                        dbSparePart.MeasureUnit = vales[27];
                        dbSparePart.MInPackingAmount = string.IsNullOrEmpty(vales[28]) ? 0 : Convert.ToInt32(vales[28]);
                        dbSparePart.IsNotWarrantyTransfer = string.IsNullOrEmpty(vales[29]) ? 0 : Convert.ToInt32(vales[29]);
                        dbSparePart.ExchangeIdentification = vales[30];
                        dbSparePart.TraceProperty = string.IsNullOrEmpty(vales[31]) ? 0 : Convert.ToInt32(vales[31]);
                        dbSparePart.WarehousDays = string.IsNullOrEmpty(vales[32]) ? 0 : Convert.ToInt32(vales[32]);
                        dbSparePartCodes.Add(dbSparePart);
                        return false;
                    };
                    //根据SQL查询数据库，将查出来的结果根据定义的委托中的方法（这里是getDbSparePartCodes）放到具体的集合中（这里是dbSparePartCodes）
                    db.QueryDataWithInOperator(@"select ID,
                                                                    CODE,
                                                                    NAME,
                                                                    ReferenceCode,
                                                                    LASTSUBSTITUTE,
                                                                    NEXTSUBSTITUTE,
                                                                    SHELFLIFE,
                                                                    ENGLISHNAME,
                                                                    PINYINCODE,
                                                                    REFERENCECODE,
                                                                    REFERENCENAME,
                                                                    CADCODE,
                                                                    CADNAME,
                                                                    PARTTYPE,
                                                                    SPECIFICATION,
                                                                    FEATURE,
                                                                    STATUS,
                                                                    LENGTH,
                                                                    WIDTH,
                                                                    HEIGHT,
                                                                    VOLUME,
                                                                    WEIGHT,
                                                                    MATERIAL,
                                                                    PACKINGAMOUNT,
                                                                    PACKINGSPECIFICATION,
                                                                    PARTSOUTPACKINGCODE,
                                                                    PARTSINPACKINGCODE,
                                                                    MEASUREUNIT,
                                                                    MINPACKINGAMOUNT,
                                                                   IsNotWarrantyTransfer,
                                                                   ExchangeIdentification,TraceProperty,WarehousDays
                                                                from sparepart where status=1 ", "Code", false, partCodesNeedCheck, getDbSparePartCodes);
                    if(partCodesNeedCheck.Length > 0) {
                        var sparePartsExistsCode = sparePartsNeedCheck.Where(r => dbSparePartCodes.Any(v => v.Code == r.Code)).ToArray();
                        if(sparePartsExistsCode.Count() < sparePartsNeedCheck.Count()) {
                            var notExist = allList.Except(sparePartsExistsCode);
                            foreach(var item in notExist) {
                                if(item.ErrorMsg == null) {
                                    item.ErrorMsg = ErrorStrings.Export_Validation_PartsBranch_Validation8 + item.Code + ErrorStrings.Export_Validation_Sparepart_NotExistOrStop;
                                } else {
                                    item.ErrorMsg = item.ErrorMsg + ErrorStrings.Export_Validation_PartsBranch_Validation9 + item.Code + ErrorStrings.Export_Validation_Sparepart_NotExistOrStop;
                                }
                                errorList.Add(item);
                            }
                        }
                    }
                }
                //获取所有不合格数据
                errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                //if(errList.Count > 0) {
                //    errorList.AddRange(errList);
                //}
                //获取所有合格数据
                rightList = allList.Except(errorList).ToList();

                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempSparePart = list[index - 1];
                            var values = new object[] {
                                tempSparePart.CodeStr, tempSparePart.WarehousDaysStr,
                                tempSparePart.ErrorMsg
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }

                //导入所有合格数据
                if(!rightList.Any())
                    return true;
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    //开启事务，新增更新配件在一个事务内
                    var ts = conn.BeginTransaction();
                    try {
                        //新增配件
                        if(rightList.Any()) {
                            //获取新增数据的sql语句，Id为主键
                            var sqlUpdate = db.GetUpdateSql("SparePart", new string[] { "WarehousDays", "ModifierId", "ModifierName", "ModifyTime", "OMSparePartMark" }, new string[] { "Code" });
                            var userInfo = Utils.GetCurrentUserInfo();
                            //往数据库增加配件信息
                            foreach(SparePartExtend item in rightList) {
                                var command = db.CreateDbCommand(sqlUpdate, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("Code", Strings.StrConv(item.Code, VbStrConv.Narrow).Replace("【", "[")));
                                command.Parameters.Add(db.CreateDbParameter("WarehousDays", item.WarehousDays));
                                command.Parameters.Add(db.CreateDbParameter("ModifierId", userInfo.Id));
                                command.Parameters.Add(db.CreateDbParameter("ModifierName", userInfo.Name));
                                command.Parameters.Add(db.CreateDbParameter("ModifyTime", DateTime.Now));
                                command.Parameters.Add(db.CreateDbParameter("OMSparePartMark", 0));
                                command.ExecuteNonQuery();

                            }

                        }
                        //无异常提交
                        ts.Commit();
                    } catch(Exception ex) {
                        //报错回滚
                        ts.Rollback();
                        throw new Exception(ex.Message);
                    } finally {
                        if(conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }
        public bool 批量修改临时天数(string fileName, out int excelImportNum, out List<SparePartExtend> rightData, out List<SparePartExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<SparePartExtend>();
            var rightList = new List<SparePartExtend>();
            var allList = new List<SparePartExtend>();
            var dbSparePartCodes = new List<SparePartExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("SparePart", out notNullableFields, out fieldLenght);
                List<object> excelColumns;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Code, "Code");
                    excelOperator.AddColumnDataSource("临时天数", "TemDays");
                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var sparePart = new SparePartExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        sparePart.CodeStr = Strings.StrConv(newRow["Code"], VbStrConv.Narrow).Replace("【", "[");
                        sparePart.CodeStr = strconvsparecode(sparePart.CodeStr);
                        sparePart.Code = Strings.StrConv(newRow["Code"], VbStrConv.Narrow).Replace("【", "[");
                        sparePart.Code = strconvsparecode(sparePart.Code);
                        sparePart.Code = sparePart.Code;//全部大写
                        var tempErrorMessage = new List<string>();
                        //配件编号检查
                        var fieldIndex = notNullableFields.IndexOf("CODE");
                        if(string.IsNullOrEmpty(sparePart.Code)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.OverstockPartsAppDetail__Validation1);
                        } else {
                            //配件长度不得超过25
                            if(Encoding.Default.GetByteCount(sparePart.Code) > 25)
                                tempErrorMessage.Add(ErrorStrings.ImpPartsSupplierRelation_Validation8);
                        }

                        //配件类型检查
                        fieldIndex = notNullableFields.IndexOf("TemDays".ToUpper());
                        if(string.IsNullOrEmpty(row["TemDays"])) {
                            //if (fieldIndex > -1)
                            //tempErrorMessage.Add(ErrorStrings.Export_Validation_Sparepart_PartTypeIsNull);
                        } else {
                            sparePart.TemDaysStr = row["TemDays"]; //"];
                            var temDays = Convert.ToInt32(sparePart.TemDaysStr);
                            if(sparePart.TemDaysStr != null  && temDays < 0) {
                                tempErrorMessage.Add("临时天数不能小于0");
                            } else {
                                sparePart.TemDays = temDays;
                            }
                        }

                        if(tempErrorMessage.Count > 0) {
                            sparePart.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(sparePart);
                        return false;
                    });
                    //校验导入的数据配件编号重复
                    var groups = allList.GroupBy(r =>
                        r.Code
                    ).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        if(groupItem.ErrorMsg == null) {
                            groupItem.ErrorMsg = ErrorStrings.Export_Title_PartsBranch_Code + groupItem.Code + ErrorStrings.Export_Validation_PartsBranch_Validation5;
                        } else {
                            groupItem.ErrorMsg = groupItem.ErrorMsg + ErrorStrings.Export_Validation_PartsBranch_Validation6 + groupItem.Code + ErrorStrings.Export_Validation_PartsBranch_Validation5;
                        }
                    }
                    var sparePartsNeedCheck = allList.ToList();
                    var partCodesNeedCheck = sparePartsNeedCheck.Select(r => r.Code).Distinct().ToArray();

                    Func<string[], bool> getDbSparePartCodes = vales => {
                        var dbSparePart = new SparePartExtend();
                        dbSparePart.Id = Convert.ToInt32(vales[0]);
                        dbSparePart.Code = vales[1];
                        dbSparePart.ReferenceCode = vales[3];
                        dbSparePart.Name = vales[2];
                        dbSparePart.LastSubstitute = vales[4];
                        dbSparePart.NextSubstitute = vales[5];
                        dbSparePart.ShelfLife = string.IsNullOrEmpty(vales[6]) ? 0 : Convert.ToInt32(vales[6]);
                        dbSparePart.EnglishName = vales[7];
                        dbSparePart.PinyinCode = vales[8];
                        dbSparePart.ReferenceCode = vales[9];
                        dbSparePart.ReferenceName = vales[10];
                        dbSparePart.CADCode = vales[11];
                        dbSparePart.CADName = vales[12];
                        dbSparePart.PartType = string.IsNullOrEmpty(vales[13]) ? 0 : Convert.ToInt32(vales[13]);
                        dbSparePart.Specification = vales[14];
                        dbSparePart.Feature = vales[15];
                        dbSparePart.Status = string.IsNullOrEmpty(vales[16]) ? 0 : Convert.ToInt32(vales[16]);
                        dbSparePart.Length = string.IsNullOrEmpty(vales[17]) ? 0 : Convert.ToDecimal(vales[17]);
                        dbSparePart.Width = string.IsNullOrEmpty(vales[18]) ? 0 : Convert.ToDecimal(vales[18]);
                        dbSparePart.Height = string.IsNullOrEmpty(vales[19]) ? 0 : Convert.ToDecimal(vales[19]);
                        dbSparePart.Volume = string.IsNullOrEmpty(vales[20]) ? 0 : Convert.ToDecimal(vales[20]);
                        dbSparePart.Weight = string.IsNullOrEmpty(vales[21]) ? 0 : Convert.ToDecimal(vales[21]);
                        dbSparePart.Material = vales[22];
                        dbSparePart.PackingAmount = string.IsNullOrEmpty(vales[23]) ? 0 : Convert.ToInt32(vales[23]);
                        dbSparePart.PackingSpecification = vales[24];
                        dbSparePart.PartsOutPackingCode = vales[25];
                        dbSparePart.PartsInPackingCode = vales[26];
                        dbSparePart.MeasureUnit = vales[27];
                        dbSparePart.MInPackingAmount = string.IsNullOrEmpty(vales[28]) ? 0 : Convert.ToInt32(vales[28]);
                        dbSparePart.IsNotWarrantyTransfer = string.IsNullOrEmpty(vales[29]) ? 0 : Convert.ToInt32(vales[29]);
                        dbSparePart.ExchangeIdentification = vales[30];
                        dbSparePart.TraceProperty = string.IsNullOrEmpty(vales[31]) ? 0 : Convert.ToInt32(vales[31]);
                        dbSparePart.TemDays = string.IsNullOrEmpty(vales[32]) ? 0 : Convert.ToInt32(vales[32]);
                        dbSparePartCodes.Add(dbSparePart);
                        return false;
                    };
                    //根据SQL查询数据库，将查出来的结果根据定义的委托中的方法（这里是getDbSparePartCodes）放到具体的集合中（这里是dbSparePartCodes）
                    db.QueryDataWithInOperator(@"select ID,
                                                                    CODE,
                                                                    NAME,
                                                                    ReferenceCode,
                                                                    LASTSUBSTITUTE,
                                                                    NEXTSUBSTITUTE,
                                                                    SHELFLIFE,
                                                                    ENGLISHNAME,
                                                                    PINYINCODE,
                                                                    REFERENCECODE,
                                                                    REFERENCENAME,
                                                                    CADCODE,
                                                                    CADNAME,
                                                                    PARTTYPE,
                                                                    SPECIFICATION,
                                                                    FEATURE,
                                                                    STATUS,
                                                                    LENGTH,
                                                                    WIDTH,
                                                                    HEIGHT,
                                                                    VOLUME,
                                                                    WEIGHT,
                                                                    MATERIAL,
                                                                    PACKINGAMOUNT,
                                                                    PACKINGSPECIFICATION,
                                                                    PARTSOUTPACKINGCODE,
                                                                    PARTSINPACKINGCODE,
                                                                    MEASUREUNIT,
                                                                    MINPACKINGAMOUNT,
                                                                   IsNotWarrantyTransfer,
                                                                   ExchangeIdentification,TraceProperty,TemDays
                                                                from sparepart where status=1 ", "Code", false, partCodesNeedCheck, getDbSparePartCodes);
                    if(partCodesNeedCheck.Length > 0) {
                        var sparePartsExistsCode = sparePartsNeedCheck.Where(r => dbSparePartCodes.Any(v => v.Code == r.Code)).ToArray();
                        if(sparePartsExistsCode.Count() < sparePartsNeedCheck.Count()) {
                            var notExist = allList.Except(sparePartsExistsCode);
                            foreach(var item in notExist) {
                                if(item.ErrorMsg == null) {
                                    item.ErrorMsg = ErrorStrings.Export_Validation_PartsBranch_Validation8 + item.Code + ErrorStrings.Export_Validation_Sparepart_NotExistOrStop;
                                } else {
                                    item.ErrorMsg = item.ErrorMsg + ErrorStrings.Export_Validation_PartsBranch_Validation9 + item.Code + ErrorStrings.Export_Validation_Sparepart_NotExistOrStop;
                                }
                                errorList.Add(item);
                            }
                        }
                    }
                }
                //获取所有不合格数据
                errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                //if(errList.Count > 0) {
                //    errorList.AddRange(errList);
                //}
                //获取所有合格数据
                rightList = allList.Except(errorList).ToList();

                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempSparePart = list[index - 1];
                            var values = new object[] {
                                tempSparePart.CodeStr, tempSparePart.TemDaysStr,
                                tempSparePart.ErrorMsg
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }

                //导入所有合格数据
                if(!rightList.Any())
                    return true;
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    //开启事务，新增更新配件在一个事务内
                    var ts = conn.BeginTransaction();
                    try {
                        //新增配件
                        if(rightList.Any()) {
                            //获取新增数据的sql语句，Id为主键
                            var sqlUpdate = db.GetUpdateSql("SparePart", new string[] { "TemDays", "ModifierId", "ModifierName", "ModifyTime", "OMSparePartMark" }, new string[] { "Code" });
                            var userInfo = Utils.GetCurrentUserInfo();
                            //往数据库增加配件信息
                            foreach(SparePartExtend item in rightList) {
                                var command = db.CreateDbCommand(sqlUpdate, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("Code", Strings.StrConv(item.Code, VbStrConv.Narrow).Replace("【", "[")));
                                command.Parameters.Add(db.CreateDbParameter("TemDays", item.TemDays));
                                command.Parameters.Add(db.CreateDbParameter("ModifierId", userInfo.Id));
                                command.Parameters.Add(db.CreateDbParameter("ModifierName", userInfo.Name));
                                command.Parameters.Add(db.CreateDbParameter("ModifyTime", DateTime.Now));
                                command.Parameters.Add(db.CreateDbParameter("OMSparePartMark", 0));
                                command.ExecuteNonQuery();

                            }

                        }
                        //无异常提交
                        ts.Commit();
                    } catch(Exception ex) {
                        //报错回滚
                        ts.Rollback();
                        throw new Exception(ex.Message);
                    } finally {
                        if(conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }
        public bool 批量替换特征说明(string fileName, out int excelImportNum, out List<SparePartExtend> rightData, out List<SparePartExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<SparePartExtend>();
            var rightList = new List<SparePartExtend>();
            var allList = new List<SparePartExtend>();
            var dbSparePartCodes = new List<SparePartExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("SparePart", out notNullableFields, out fieldLenght);
                List<object> excelColumns;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Code, "Code");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Feature, "Feature");
                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;
                    var keyValuePairs = new[] {
                                new KeyValuePair<string, string>("Status","MasterData_Status") 
                            };
                    tempExcelOperator.ImportHelper.LoadEnumFromDb(null, null, keyValuePairs);
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var sparePart = new SparePartExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        sparePart.CodeStr = Strings.StrConv(newRow["Code"], VbStrConv.Narrow).Replace("【", "[");
                        sparePart.CodeStr = strconvsparecode(sparePart.CodeStr);
                        sparePart.Code = Strings.StrConv(newRow["Code"], VbStrConv.Narrow).Replace("【", "[");
                        sparePart.Code = strconvsparecode(sparePart.Code);
                        sparePart.Code = sparePart.Code;//全部大写
                        sparePart.Feature = newRow["Feature"];
                        sparePart.FeatureStr = newRow["Feature"];

                        var tempErrorMessage = new List<string>();
                        //配件编号检查
                        var fieldIndex = notNullableFields.IndexOf("CODE");
                        if(string.IsNullOrEmpty(sparePart.Code)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.OverstockPartsAppDetail__Validation1);
                        } else {
                            //配件长度不得超过25
                            if(Encoding.Default.GetByteCount(sparePart.Code) > 25)
                                tempErrorMessage.Add(ErrorStrings.ImpPartsSupplierRelation_Validation8);
                        }
                        //特征说明检查
                        if(sparePart.Feature != null && Encoding.Default.GetByteCount(sparePart.Feature) > 200)
                            tempErrorMessage.Add(ErrorStrings.Export_Validation_Sparepart_FeatureIsLong);

                        if(tempErrorMessage.Count > 0) {
                            sparePart.ErrorMsg = string.Join("; ", tempErrorMessage);
                        } else {
                            var tempStatus = tempExcelOperator.ImportHelper.GetEnumValue("Status", ErrorStrings.Export_Validation_PartsBranch_Used);
                            if(tempStatus.HasValue) {
                                sparePart.Status = tempStatus.Value;
                            }
                        }

                        allList.Add(sparePart);
                        return false;
                    });
                    //校验导入的数据配件编号重复
                    var groups = allList.GroupBy(r =>
                        r.Code
                    ).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        if(groupItem.ErrorMsg == null) {
                            groupItem.ErrorMsg = ErrorStrings.Export_Title_PartsBranch_Code + groupItem.Code + ErrorStrings.Export_Validation_PartsBranch_Validation5;
                        } else {
                            groupItem.ErrorMsg = groupItem.ErrorMsg + ErrorStrings.Export_Validation_PartsBranch_Validation6 + groupItem.Code + ErrorStrings.Export_Validation_PartsBranch_Validation5;
                        }
                    }
                    var sparePartsNeedCheck = allList.ToList();
                    var partCodesNeedCheck = sparePartsNeedCheck.Select(r => r.Code).Distinct().ToArray();

                    Func<string[], bool> getDbSparePartCodes = vales => {
                        var dbSparePart = new SparePartExtend();
                        dbSparePart.Id = Convert.ToInt32(vales[0]);
                        dbSparePart.Code = vales[1];
                        dbSparePart.ReferenceCode = vales[3];
                        dbSparePart.Name = vales[2];
                        dbSparePart.LastSubstitute = vales[4];
                        dbSparePart.NextSubstitute = vales[5];
                        dbSparePart.ShelfLife = string.IsNullOrEmpty(vales[6]) ? 0 : Convert.ToInt32(vales[6]);
                        dbSparePart.EnglishName = vales[7];
                        dbSparePart.PinyinCode = vales[8];
                        dbSparePart.ReferenceCode = vales[9];
                        dbSparePart.ReferenceName = vales[10];
                        dbSparePart.CADCode = vales[11];
                        dbSparePart.CADName = vales[12];
                        dbSparePart.PartType = string.IsNullOrEmpty(vales[13]) ? 0 : Convert.ToInt32(vales[13]);
                        dbSparePart.Specification = vales[14];
                        dbSparePart.Feature = vales[15];
                        dbSparePart.Status = string.IsNullOrEmpty(vales[16]) ? 0 : Convert.ToInt32(vales[16]);
                        dbSparePart.Length = string.IsNullOrEmpty(vales[17]) ? 0 : Convert.ToDecimal(vales[17]);
                        dbSparePart.Width = string.IsNullOrEmpty(vales[18]) ? 0 : Convert.ToDecimal(vales[18]);
                        dbSparePart.Height = string.IsNullOrEmpty(vales[19]) ? 0 : Convert.ToDecimal(vales[19]);
                        dbSparePart.Volume = string.IsNullOrEmpty(vales[20]) ? 0 : Convert.ToDecimal(vales[20]);
                        dbSparePart.Weight = string.IsNullOrEmpty(vales[21]) ? 0 : Convert.ToDecimal(vales[21]);
                        dbSparePart.Material = vales[22];
                        dbSparePart.PackingAmount = string.IsNullOrEmpty(vales[23]) ? 0 : Convert.ToInt32(vales[23]);
                        dbSparePart.PackingSpecification = vales[24];
                        dbSparePart.PartsOutPackingCode = vales[25];
                        dbSparePart.PartsInPackingCode = vales[26];
                        dbSparePart.MeasureUnit = vales[27];
                        dbSparePart.MInPackingAmount = string.IsNullOrEmpty(vales[28]) ? 0 : Convert.ToInt32(vales[28]);
                        dbSparePart.ExchangeIdentification = vales[29];
                        dbSparePartCodes.Add(dbSparePart);
                        return false;
                    };
                    //根据SQL查询数据库，将查出来的结果根据定义的委托中的方法（这里是getDbSparePartCodes）放到具体的集合中（这里是dbSparePartCodes）
                    db.QueryDataWithInOperator(@"select ID,
                                                                    CODE,
                                                                    NAME,
                                                                    ReferenceCode,
                                                                    LASTSUBSTITUTE,
                                                                    NEXTSUBSTITUTE,
                                                                    SHELFLIFE,
                                                                    ENGLISHNAME,
                                                                    PINYINCODE,
                                                                    REFERENCECODE,
                                                                    REFERENCENAME,
                                                                    CADCODE,
                                                                    CADNAME,
                                                                    PARTTYPE,
                                                                    SPECIFICATION,
                                                                    FEATURE,
                                                                    STATUS,
                                                                    to_char(LENGTH),
                                                                    to_char(WIDTH),
                                                                    to_char(HEIGHT),
                                                                    to_char(VOLUME),
                                                                    to_char(WEIGHT),
                                                                    MATERIAL,
                                                                    PACKINGAMOUNT,
                                                                    PACKINGSPECIFICATION,
                                                                    PARTSOUTPACKINGCODE,
                                                                    PARTSINPACKINGCODE,
                                                                    MEASUREUNIT,
                                                                    MINPACKINGAMOUNT,ExchangeIdentification
                                                                from sparepart where status=1 ", "Code", false, partCodesNeedCheck, getDbSparePartCodes);
                    if(partCodesNeedCheck.Length > 0) {
                        var sparePartsExistsCode = sparePartsNeedCheck.Where(r => dbSparePartCodes.Any(v => v.Code == r.Code)).ToArray();
                        if(sparePartsExistsCode.Count() < sparePartsNeedCheck.Count()) {
                            var notExist = allList.Except(sparePartsExistsCode);
                            foreach(var item in notExist) {
                                if(item.ErrorMsg == null) {
                                    item.ErrorMsg = ErrorStrings.Export_Validation_PartsBranch_Validation8 + item.Code + ErrorStrings.Export_Validation_Sparepart_NotExistOrStop;
                                } else {
                                    item.ErrorMsg = item.ErrorMsg + ErrorStrings.Export_Validation_PartsBranch_Validation9 + item.Code + ErrorStrings.Export_Validation_Sparepart_NotExistOrStop;
                                }
                                errorList.Add(item);
                            }
                        }
                    }
                }
                //获取所有不合格数据
                errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                //if(errList.Count > 0) {
                //    errorList.AddRange(errList);
                //}
                //获取所有合格数据
                rightList = allList.Except(errorList).ToList();

                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempSparePart = list[index - 1];
                            var values = new object[] {
                                tempSparePart.CodeStr, tempSparePart.FeatureStr,
                                tempSparePart.ErrorMsg
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }

                //导入所有合格数据
                if(!rightList.Any())
                    return true;
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    //开启事务，新增更新配件在一个事务内
                    var ts = conn.BeginTransaction();
                    try {
                        //新增配件
                        if(rightList.Any()) {
                            //获取新增数据的sql语句，Id为主键
                            var sqlUpdate = db.GetUpdateSql("SparePart", new string[] { "Feature", "ModifierId", "ModifierName", "ModifyTime", "OMSparePartMark" }, new string[] { "Code" });
                            var sqlInsertHistory = db.GetInsertSql("SparePartHistory", "Id", new[] {
                                "SparePartId","Code","ReferenceCode", "Name", "CADCode", "Specification", "EnglishName", "PartType", "MeasureUnit", "ShelfLife", "LastSubstitute", "NextSubstitute", "Weight", "Feature", "status", "CreatorId", "CreatorName", "CreateTime","MInPackingAmount","ExchangeIdentification"
                            });

                            var userInfo = Utils.GetCurrentUserInfo();
                            //往数据库增加配件信息
                            foreach(SparePartExtend item in rightList) {
                                var command = db.CreateDbCommand(sqlUpdate, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("Code", Strings.StrConv(item.Code, VbStrConv.Narrow).Replace("【", "[")));
                                command.Parameters.Add(db.CreateDbParameter("Feature", item.Feature));
                                command.Parameters.Add(db.CreateDbParameter("ModifierId", userInfo.Id));
                                command.Parameters.Add(db.CreateDbParameter("ModifierName", userInfo.Name));
                                command.Parameters.Add(db.CreateDbParameter("ModifyTime", DateTime.Now));
                                command.Parameters.Add(db.CreateDbParameter("OMSparePartMark", 0));
                                command.ExecuteNonQuery();

                                //                                //获取数据库的完整备件信息
                                //                                string[] partCode = new string[] { item.Code };

                                //                                Func<string[], bool> getDbSparePartInfo = vales => {
                                //                                    item.Id = Convert.ToInt32(vales[0]);
                                //                                    //item.Code = vales[1];
                                //                                    item.ReferenceCode = vales[3];
                                //                                    item.Name = vales[2];
                                //                                    item.LastSubstitute = vales[4];
                                //                                    item.NextSubstitute = vales[5];
                                //                                    item.ShelfLife = string.IsNullOrEmpty(vales[6]) ? 0 : Convert.ToInt32(vales[6]);
                                //                                    item.EnglishName = vales[7];
                                //                                    item.PinyinCode = vales[8];
                                //                                    item.ReferenceCode = vales[9];
                                //                                    item.ReferenceName = vales[10];
                                //                                    item.CADCode = vales[11];
                                //                                    item.CADName = vales[12];
                                //                                    item.PartType = string.IsNullOrEmpty(vales[13]) ? 0 : Convert.ToInt32(vales[13]);
                                //                                    item.Specification = vales[14];
                                //                                    //item.Feature = vales[15];
                                //                                    item.Status = string.IsNullOrEmpty(vales[16]) ? 0 : Convert.ToInt32(vales[16]);
                                //                                    item.Length = string.IsNullOrEmpty(vales[17]) ? 0 : Convert.ToDecimal(vales[17]);
                                //                                    item.Width = string.IsNullOrEmpty(vales[18]) ? 0 : Convert.ToDecimal(vales[18]);
                                //                                    item.Height = string.IsNullOrEmpty(vales[19]) ? 0 : Convert.ToDecimal(vales[19]);
                                //                                    item.Volume = string.IsNullOrEmpty(vales[20]) ? 0 : Convert.ToDecimal(vales[20]);
                                //                                    item.Weight = string.IsNullOrEmpty(vales[21]) ? 0 : Convert.ToDecimal(vales[21]);
                                //                                    item.Material = vales[22];
                                //                                    item.PackingAmount = string.IsNullOrEmpty(vales[23]) ? 0 : Convert.ToInt32(vales[23]);
                                //                                    item.PackingSpecification = vales[24];
                                //                                    item.PartsOutPackingCode = vales[25];
                                //                                    item.PartsInPackingCode = vales[26];
                                //                                    item.MeasureUnit = vales[27];
                                //                                    item.MInPackingAmount = string.IsNullOrEmpty(vales[28]) ? 0 : Convert.ToInt32(vales[28]);
                                //                                    return false;
                                //                                };

                                //                                db.QueryDataWithInOperator(@"select ID,
                                //                                                                    CODE,
                                //                                                                    NAME,
                                //                                                                    ReferenceCode,
                                //                                                                    LASTSUBSTITUTE,
                                //                                                                    NEXTSUBSTITUTE,
                                //                                                                    SHELFLIFE,
                                //                                                                    ENGLISHNAME,
                                //                                                                    PINYINCODE,
                                //                                                                    REFERENCECODE,
                                //                                                                    REFERENCENAME,
                                //                                                                    CADCODE,
                                //                                                                    CADNAME,
                                //                                                                    PARTTYPE,
                                //                                                                    SPECIFICATION,
                                //                                                                    FEATURE,
                                //                                                                    STATUS,
                                //                                                                    LENGTH,
                                //                                                                    WIDTH,
                                //                                                                    HEIGHT,
                                //                                                                    VOLUME,
                                //                                                                    WEIGHT,
                                //                                                                    MATERIAL,
                                //                                                                    PACKINGAMOUNT,
                                //                                                                    PACKINGSPECIFICATION,
                                //                                                                    PARTSOUTPACKINGCODE,
                                //                                                                    PARTSINPACKINGCODE,
                                //                                                                    MEASUREUNIT,
                                //                                                                    MINPACKINGAMOUNT
                                //                                                                from sparepart where status=1 ", "Code", true, partCode, getDbSparePartInfo);

                                //往数据库增加履历
                                var sparePart = dbSparePartCodes.Where(r => r.Code == item.Code).FirstOrDefault();
                                if(sparePart != null) {
                                    var commandHistory = db.CreateDbCommand(sqlInsertHistory, conn, ts);
                                    commandHistory.Parameters.Add(db.CreateDbParameter("SparePartId", sparePart.Id));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Code", Strings.StrConv(sparePart.Code, VbStrConv.Narrow).Replace("【", "[")));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Name", sparePart.Name));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("ReferenceCode", sparePart.ReferenceCode));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("CADCode", sparePart.CADCode));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Specification", sparePart.Specification));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("EnglishName", sparePart.EnglishName));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("PartType", sparePart.PartType));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("MeasureUnit", sparePart.MeasureUnit));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("ShelfLife", sparePart.ShelfLife));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("LastSubstitute", sparePart.LastSubstitute));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("NextSubstitute", sparePart.NextSubstitute));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Weight", sparePart.Weight));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Feature", item.Feature));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Status", sparePart.Status));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("MInPackingAmount", sparePart.MInPackingAmount));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("ExchangeIdentification", sparePart.ExchangeIdentification));
                                    commandHistory.ExecuteNonQuery();
                                }
                            }

                        }
                        //无异常提交
                        ts.Commit();
                    } catch(Exception ex) {
                        //报错回滚
                        ts.Rollback();
                        throw new Exception(ex.Message);
                    } finally {
                        if(conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }

        public bool 批量替换零部件图号(string fileName, out int excelImportNum, out List<SparePartExtend> rightData, out List<SparePartExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<SparePartExtend>();
            var rightList = new List<SparePartExtend>();
            var allList = new List<SparePartExtend>();
            var dbSparePartCodes = new List<SparePartExtend>();

            var oldspareParts = new List<SparePartExtend>();

            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("SparePart", out notNullableFields, out fieldLenght);
                List<object> excelColumns;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Code, "Code");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_ReferenceCode, "ReferenceCode");

                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;
                    var keyValuePairs = new[] {
                                new KeyValuePair<string, string>("Status","MasterData_Status") 
                            };
                    tempExcelOperator.ImportHelper.LoadEnumFromDb(null, null, keyValuePairs);
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var sparePart = new SparePartExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        sparePart.CodeStr = Strings.StrConv(newRow["Code"], VbStrConv.Narrow).Replace("【", "[");
                        sparePart.CodeStr = strconvsparecode(sparePart.CodeStr);
                        sparePart.Code = Strings.StrConv(newRow["Code"], VbStrConv.Narrow).Replace("【", "[");
                        sparePart.Code = strconvsparecode(sparePart.Code);
                        sparePart.Code = sparePart.Code;//全部大写
                        sparePart.ReferenceCode = newRow["ReferenceCode"];
                        sparePart.ReferenceCodeStr = newRow["ReferenceCode"];

                        var tempErrorMessage = new List<string>();
                        //配件编号检查
                        var fieldIndex = notNullableFields.IndexOf("CODE");
                        if(string.IsNullOrEmpty(sparePart.Code)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.OverstockPartsAppDetail__Validation1);
                        } else {
                            //配件长度不得超过25
                            if(Encoding.Default.GetByteCount(sparePart.Code) > 25)
                                tempErrorMessage.Add(ErrorStrings.ImpPartsSupplierRelation_Validation8);
                        }
                        //零部件图号检查
                        if(string.IsNullOrEmpty(sparePart.ReferenceCode)) {
                            tempErrorMessage.Add(ErrorStrings.Export_Validation_Sparepart_ReferenceCodeIsNull);
                        } else {
                            if(Encoding.Default.GetByteCount(sparePart.ReferenceCode) > 50)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Sparepart_ReferenceCodeIsLong);
                        }
                        if(tempErrorMessage.Count > 0) {
                            sparePart.ErrorMsg = string.Join("; ", tempErrorMessage);
                        } else {
                            var tempStatus = tempExcelOperator.ImportHelper.GetEnumValue("Status", ErrorStrings.Export_Validation_PartsBranch_Used);
                            if(tempStatus.HasValue) {
                                sparePart.Status = tempStatus.Value;
                            }
                        }

                        allList.Add(sparePart);
                        return false;
                    });
                    //校验导入的数据配件编号重复
                    var groups = allList.GroupBy(r =>
                        r.Code
                    ).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        if(groupItem.ErrorMsg == null) {
                            groupItem.ErrorMsg = ErrorStrings.Export_Title_PartsBranch_Code + groupItem.Code + ErrorStrings.Export_Validation_PartsBranch_Validation5;
                        } else {
                            groupItem.ErrorMsg = groupItem.ErrorMsg + ErrorStrings.Export_Validation_PartsBranch_Validation6 + groupItem.Code + ErrorStrings.Export_Validation_PartsBranch_Validation5;
                        }
                    }
                    var sparePartsNeedCheck = allList.ToList();
                    var partCodesNeedCheck = sparePartsNeedCheck.Select(r => r.Code).Distinct().ToArray();

                    Func<string[], bool> getDbSparePartCodes = vales => {
                        var dbSparePart = new SparePartExtend();
                        dbSparePart.Id = Convert.ToInt32(vales[0]);
                        dbSparePart.Code = vales[1];
                        dbSparePart.Name = vales[2];
                        dbSparePart.ReferenceCode = vales[3];                        
                        dbSparePart.LastSubstitute = vales[4];
                        dbSparePart.NextSubstitute = vales[5];
                        dbSparePart.ShelfLife = string.IsNullOrEmpty(vales[6]) ? 0 : Convert.ToInt32(vales[6]);
                        dbSparePart.EnglishName = vales[7];
                        dbSparePart.PinyinCode = vales[8];
                        dbSparePart.ReferenceName = vales[9];
                        dbSparePart.CADCode = vales[10];
                        dbSparePart.CADName = vales[11];
                        dbSparePart.PartType = string.IsNullOrEmpty(vales[13]) ? 0 : Convert.ToInt32(vales[12]);
                        dbSparePart.Specification = vales[13];
                        dbSparePart.Feature = vales[14];
                        dbSparePart.Status = string.IsNullOrEmpty(vales[15]) ? 0 : Convert.ToInt32(vales[15]);
                        dbSparePart.Length = string.IsNullOrEmpty(vales[16]) ? 0 : Convert.ToDecimal(vales[16]);
                        dbSparePart.Width = string.IsNullOrEmpty(vales[17]) ? 0 : Convert.ToDecimal(vales[17]);
                        dbSparePart.Height = string.IsNullOrEmpty(vales[18]) ? 0 : Convert.ToDecimal(vales[18]);
                        dbSparePart.Volume = string.IsNullOrEmpty(vales[19]) ? 0 : Convert.ToDecimal(vales[19]);
                        dbSparePart.Weight = string.IsNullOrEmpty(vales[20]) ? 0 : Convert.ToDecimal(vales[20]);
                        dbSparePart.Material = vales[21];
                        dbSparePart.PackingAmount = string.IsNullOrEmpty(vales[22]) ? 0 : Convert.ToInt32(vales[22]);
                        dbSparePart.PackingSpecification = vales[23];
                        dbSparePart.PartsOutPackingCode = vales[24];
                        dbSparePart.PartsInPackingCode = vales[25];
                        dbSparePart.MeasureUnit = vales[26];
                        dbSparePart.MInPackingAmount = string.IsNullOrEmpty(vales[27]) ? 0 : Convert.ToInt32(vales[27]);
                        dbSparePart.IsNotWarrantyTransfer = string.IsNullOrEmpty(vales[28]) ? 0 : Convert.ToInt32(vales[28]);
                        dbSparePart.ExchangeIdentification = vales[29];
                        dbSparePartCodes.Add(dbSparePart);
                        return false;
                    };
                    //根据SQL查询数据库，将查出来的结果根据定义的委托中的方法（这里是getDbSparePartCodes）放到具体的集合中（这里是dbSparePartCodes）
                    db.QueryDataWithInOperator(@"select ID,
                                                                    CODE,
                                                                    NAME,
                                                                    ReferenceCode,
                                                                    LASTSUBSTITUTE,
                                                                    NEXTSUBSTITUTE,
                                                                    SHELFLIFE,
                                                                    ENGLISHNAME,
                                                                    PINYINCODE,
                                                                    REFERENCENAME,
                                                                    CADCODE,
                                                                    CADNAME,
                                                                    PARTTYPE,
                                                                    SPECIFICATION,
                                                                    FEATURE,
                                                                    STATUS,
                                                                    LENGTH,
                                                                    WIDTH,
                                                                    HEIGHT,
                                                                    VOLUME,
                                                                    WEIGHT,
                                                                    MATERIAL,
                                                                    PACKINGAMOUNT,
                                                                    PACKINGSPECIFICATION,
                                                                    PARTSOUTPACKINGCODE,
                                                                    PARTSINPACKINGCODE,
                                                                    MEASUREUNIT,
                                                                    MINPACKINGAMOUNT,IsNotWarrantyTransfer,ExchangeIdentification
                                                                from sparepart where status=1 ", "Code", false, partCodesNeedCheck, getDbSparePartCodes);
                    if(partCodesNeedCheck.Length > 0) {
                        var sparePartsExistsCode = sparePartsNeedCheck.Where(r => dbSparePartCodes.Any(v => v.Code == r.Code)).ToArray();
                        if(sparePartsExistsCode.Count() < sparePartsNeedCheck.Count()) {
                            var notExist = allList.Except(sparePartsExistsCode);
                            foreach(var item in notExist) {
                                if(item.ErrorMsg == null) {
                                    item.ErrorMsg = ErrorStrings.Export_Validation_PartsBranch_Validation8 + item.Code + ErrorStrings.Export_Validation_Sparepart_NotExistOrStop;
                                } else {
                                    item.ErrorMsg = item.ErrorMsg + ErrorStrings.Export_Validation_PartsBranch_Validation9 + item.Code + ErrorStrings.Export_Validation_Sparepart_NotExistOrStop;
                                }
                                errorList.Add(item);
                            }
                        }
                    }
                }
                //获取所有不合格数据
                errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                //if(errList.Count > 0) {
                //    errorList.AddRange(errList);
                //}
                //获取所有合格数据
                rightList = allList.Except(errorList).ToList();
                var spareParts = rightList.ToList();
                var sparePartCodes = spareParts.Select(r => r.Code).Distinct().ToArray();
                Func<string[], bool> getoldspareParts = values => {
                    oldspareParts.Add(new SparePartExtend {
                        Code = values[0],
                        ExchangeIdentification = values[1],
                        Id = int.Parse(values[2]),
                        ReferenceName = values[3]
                    });
                    return false;
                };
                db.QueryDataWithInOperator("select Code,ExchangeIdentification,Id,ReferenceName from sparePart where status!=99 ", "Code", true, sparePartCodes, getoldspareParts);

                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempSparePart = list[index - 1];
                            var values = new object[] {
                                tempSparePart.CodeStr, tempSparePart.ReferenceCodeStr,
                                tempSparePart.ErrorMsg
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }

                //导入所有合格数据
                if(!rightList.Any())
                    return true;
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    //开启事务，新增更新配件在一个事务内
                    var ts = conn.BeginTransaction();
                    try {
                        //新增配件
                        if(rightList.Any()) {
                            //获取新增数据的sql语句，Id为主键
                            var sqlUpdate = db.GetUpdateSql("SparePart", new string[] { "ReferenceCode", "ModifierId", "ModifierName", "ModifyTime", "OMSparePartMark" }, new string[] { "Code" });

                            var sqlInsertHistory = db.GetInsertSql("SparePartHistory", "Id", new[] {
                             "IsNotWarrantyTransfer","SparePartId","Code","ReferenceCode", "Name", "CADCode", "Specification", "EnglishName", "PartType", "MeasureUnit", "ShelfLife", "LastSubstitute", "NextSubstitute", "Weight", "Feature", "status", "CreatorId", "CreatorName", "CreateTime","MInPackingAmount","ExchangeIdentification"
                            });


                            var userInfo = Utils.GetCurrentUserInfo();
                            //往数据库增加配件信息
                            foreach(SparePartExtend item in rightList) {
                                var command = db.CreateDbCommand(sqlUpdate, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("Code", Strings.StrConv(item.Code, VbStrConv.Narrow).Replace("【", "[")));
                                command.Parameters.Add(db.CreateDbParameter("ReferenceCode", item.ReferenceCode));
                                command.Parameters.Add(db.CreateDbParameter("ModifierId", userInfo.Id));
                                command.Parameters.Add(db.CreateDbParameter("ModifierName", userInfo.Name));
                                command.Parameters.Add(db.CreateDbParameter("ModifyTime", DateTime.Now));
                                command.Parameters.Add(db.CreateDbParameter("OMSparePartMark", 0));
                                command.ExecuteNonQuery();


                                //往数据库增加履历
                                var sparePart = dbSparePartCodes.Where(r => r.Code == item.Code).FirstOrDefault();
                                if(sparePart != null) {
                                    var commandHistory = db.CreateDbCommand(sqlInsertHistory, conn, ts);
                                    commandHistory.Parameters.Add(db.CreateDbParameter("SparePartId", sparePart.Id));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Code", Strings.StrConv(sparePart.Code, VbStrConv.Narrow).Replace("【", "[")));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Name", sparePart.Name));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("ReferenceCode", item.ReferenceCode));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("CADCode", sparePart.CADCode));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("IsNotWarrantyTransfer", sparePart.IsNotWarrantyTransfer));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Specification", sparePart.Specification));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("EnglishName", sparePart.EnglishName));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("PartType", sparePart.PartType));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("MeasureUnit", sparePart.MeasureUnit));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("ShelfLife", sparePart.ShelfLife));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("LastSubstitute", sparePart.LastSubstitute));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("NextSubstitute", sparePart.NextSubstitute));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Weight", sparePart.Weight));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Feature", sparePart.Feature));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Status", sparePart.Status));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("MInPackingAmount", sparePart.MInPackingAmount));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("ExchangeIdentification", sparePart.ExchangeIdentification));
                                    commandHistory.ExecuteNonQuery();
                                }

                            }

                        }
                        //无异常提交
                        ts.Commit();
                    } catch(Exception ex) {
                        //报错回滚
                        ts.Rollback();
                        throw new Exception(ex.Message);
                    } finally {
                        if(conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }

        public bool 批量替换IMS压缩号(string fileName, out int excelImportNum, out List<SparePartExtend> rightData, out List<SparePartExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<SparePartExtend>();
            var rightList = new List<SparePartExtend>();
            var allList = new List<SparePartExtend>();
            var dbSparePartCodes = new List<SparePartExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("SparePart", out notNullableFields, out fieldLenght);
                List<object> excelColumns;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Code, "Code");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Sparepart_IMSCompressionNumber, "IMSCompressionNumber");
                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;
                    var keyValuePairs = new[] {
                                new KeyValuePair<string, string>("Status","MasterData_Status") 
                            };
                    tempExcelOperator.ImportHelper.LoadEnumFromDb(null, null, keyValuePairs);
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var sparePart = new SparePartExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        sparePart.CodeStr = Strings.StrConv(newRow["Code"], VbStrConv.Narrow).Replace("【", "[");
                        sparePart.CodeStr = strconvsparecode(sparePart.CodeStr);
                        sparePart.Code = Strings.StrConv(newRow["Code"], VbStrConv.Narrow).Replace("【", "[");
                        sparePart.Code = strconvsparecode(sparePart.Code);
                        sparePart.Code = sparePart.Code;//全部大写
                        sparePart.IMSCompressionNumber = newRow["IMSCompressionNumber"];
                        sparePart.IMSCompressionNumberStr = newRow["IMSCompressionNumber"];
                        var tempErrorMessage = new List<string>();
                        //配件编号检查
                        var fieldIndex = notNullableFields.IndexOf("CODE");
                        if(string.IsNullOrEmpty(sparePart.Code)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.OverstockPartsAppDetail__Validation1);
                        } else {
                            //配件长度不得超过25
                            if(Encoding.Default.GetByteCount(sparePart.Code) > 25)
                                tempErrorMessage.Add(ErrorStrings.ImpPartsSupplierRelation_Validation8);
                        }
                        //IMS压缩号检查
                        if(sparePart.IMSCompressionNumber != null && Encoding.Default.GetByteCount(sparePart.IMSCompressionNumber) > 50)
                            tempErrorMessage.Add(ErrorStrings.Export_Validation_Sparepart_IMSCompressionNumberIsLong);
                        if(tempErrorMessage.Count > 0) {
                            sparePart.ErrorMsg = string.Join("; ", tempErrorMessage);
                        } else {
                            var tempStatus = tempExcelOperator.ImportHelper.GetEnumValue("Status", ErrorStrings.Export_Validation_PartsBranch_Used);
                            if(tempStatus.HasValue) {
                                sparePart.Status = tempStatus.Value;
                            }
                        }

                        allList.Add(sparePart);
                        return false;
                    });
                    //校验导入的数据配件编号重复
                    var groups = allList.GroupBy(r =>
                        r.Code
                    ).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        if(groupItem.ErrorMsg == null) {
                            groupItem.ErrorMsg = ErrorStrings.Export_Title_PartsBranch_Code + groupItem.Code + ErrorStrings.Export_Validation_PartsBranch_Validation5;
                        } else {
                            groupItem.ErrorMsg = groupItem.ErrorMsg + ErrorStrings.Export_Validation_PartsBranch_Validation6 + groupItem.Code + ErrorStrings.Export_Validation_PartsBranch_Validation5;
                        }
                    }
                    var sparePartsNeedCheck = allList.ToList();
                    var partCodesNeedCheck = sparePartsNeedCheck.Select(r => r.Code).Distinct().ToArray();

                    Func<string[], bool> getDbSparePartCodes = vales => {
                        var dbSparePart = new SparePartExtend();
                        dbSparePart.Id = Convert.ToInt32(vales[0]);
                        dbSparePart.Code = vales[1];
                        dbSparePart.Name = vales[2];
                        dbSparePart.ReferenceCode = vales[3];
                        dbSparePart.LastSubstitute = vales[4];
                        dbSparePart.NextSubstitute = vales[5];
                        dbSparePart.ShelfLife = string.IsNullOrEmpty(vales[6]) ? 0 : Convert.ToInt32(vales[6]);
                        dbSparePart.EnglishName = vales[7];
                        dbSparePart.PinyinCode = vales[8];
                        dbSparePart.IMSManufacturerNumber = vales[9];
                        dbSparePart.ReferenceName = vales[10];
                        dbSparePart.CADCode = vales[11];
                        dbSparePart.CADName = vales[12];
                        dbSparePart.PartType = string.IsNullOrEmpty(vales[13]) ? 0 : Convert.ToInt32(vales[13]);
                        dbSparePart.Specification = vales[14];
                        dbSparePart.Feature = vales[15];
                        dbSparePart.Status = string.IsNullOrEmpty(vales[16]) ? 0 : Convert.ToInt32(vales[16]);
                        dbSparePart.Length = string.IsNullOrEmpty(vales[17]) ? 0 : Convert.ToDecimal(vales[17]);
                        dbSparePart.Width = string.IsNullOrEmpty(vales[18]) ? 0 : Convert.ToDecimal(vales[18]);
                        dbSparePart.Height = string.IsNullOrEmpty(vales[19]) ? 0 : Convert.ToDecimal(vales[19]);
                        dbSparePart.Volume = string.IsNullOrEmpty(vales[20]) ? 0 : Convert.ToDecimal(vales[20]);
                        dbSparePart.Weight = string.IsNullOrEmpty(vales[21]) ? 0 : Convert.ToDecimal(vales[21]);
                        dbSparePart.Material = vales[22];
                        dbSparePart.PackingAmount = string.IsNullOrEmpty(vales[23]) ? 0 : Convert.ToInt32(vales[23]);
                        dbSparePart.PackingSpecification = vales[24];
                        dbSparePart.PartsOutPackingCode = vales[25];
                        dbSparePart.PartsInPackingCode = vales[26];
                        dbSparePart.MeasureUnit = vales[27];
                        dbSparePart.MInPackingAmount = string.IsNullOrEmpty(vales[28]) ? 0 : Convert.ToInt32(vales[28]);
                        dbSparePart.GroupABCCategory = string.IsNullOrEmpty(vales[29]) ? 0 : Convert.ToInt32(vales[29]);
                        dbSparePart.IsNotWarrantyTransfer = string.IsNullOrEmpty(vales[30]) ? 0 : Convert.ToInt32(vales[30]);
                        dbSparePart.ExchangeIdentification = vales[31];
                        dbSparePartCodes.Add(dbSparePart);
                        return false;
                    };
                    //根据SQL查询数据库，将查出来的结果根据定义的委托中的方法（这里是getDbSparePartCodes）放到具体的集合中（这里是dbSparePartCodes）
                    db.QueryDataWithInOperator(@"select ID,
                                                                    CODE,
                                                                    NAME,
                                                                    ReferenceCode,
                                                                    LASTSUBSTITUTE,
                                                                    NEXTSUBSTITUTE,
                                                                    SHELFLIFE,
                                                                    ENGLISHNAME,
                                                                    PINYINCODE,
                                                                    IMSManufacturerNumber,
                                                                    REFERENCENAME,
                                                                    CADCODE,
                                                                    CADNAME,
                                                                    PARTTYPE,
                                                                    SPECIFICATION,
                                                                    FEATURE,
                                                                    STATUS,
                                                                    LENGTH,
                                                                    WIDTH,
                                                                    HEIGHT,
                                                                    VOLUME,
                                                                    WEIGHT,
                                                                    MATERIAL,
                                                                    PACKINGAMOUNT,
                                                                    PACKINGSPECIFICATION,
                                                                    PARTSOUTPACKINGCODE,
                                                                    PARTSINPACKINGCODE,
                                                                    MEASUREUNIT,
                                                                    MINPACKINGAMOUNT,
                                                                    GroupABCCategory,IsNotWarrantyTransfer,ExchangeIdentification
                                                                from sparepart where status=1 ", "Code", false, partCodesNeedCheck, getDbSparePartCodes);
                    if(partCodesNeedCheck.Length > 0) {
                        var sparePartsExistsCode = sparePartsNeedCheck.Where(r => dbSparePartCodes.Any(v => v.Code == r.Code)).ToArray();
                        if(sparePartsExistsCode.Count() < sparePartsNeedCheck.Count()) {
                            var notExist = allList.Except(sparePartsExistsCode);
                            foreach(var item in notExist) {
                                if(item.ErrorMsg == null) {
                                    item.ErrorMsg = ErrorStrings.Export_Validation_PartsBranch_Validation8 + item.Code + ErrorStrings.Export_Validation_Sparepart_NotExistOrStop;
                                } else {
                                    item.ErrorMsg = item.ErrorMsg + ErrorStrings.Export_Validation_PartsBranch_Validation9 + item.Code + ErrorStrings.Export_Validation_Sparepart_NotExistOrStop;
                                }
                                errorList.Add(item);
                            }
                        }
                    }
                }
                //获取所有不合格数据
                errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                //if(errList.Count > 0) {
                //    errorList.AddRange(errList);
                //}
                //获取所有合格数据
                rightList = allList.Except(errorList).ToList();

                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempSparePart = list[index - 1];
                            var values = new object[] {
                                tempSparePart.CodeStr, tempSparePart.ReferenceCodeStr,
                                tempSparePart.ErrorMsg
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }

                //导入所有合格数据
                if(!rightList.Any())
                    return true;
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    //开启事务，新增更新配件在一个事务内
                    var ts = conn.BeginTransaction();
                    try {
                        //新增配件
                        if(rightList.Any()) {
                            //获取新增数据的sql语句，Id为主键
                            var sqlUpdate = db.GetUpdateSql("SparePart", new string[] { "IMSCompressionNumber", "ModifierId", "ModifierName", "ModifyTime", "OMSparePartMark" }, new string[] { "Code" });
                            var sqlInsertHistory = db.GetInsertSql("SparePartHistory", "Id", new[] {
                            "IsNotWarrantyTransfer",    "SparePartId","Code","ReferenceCode","IMSCompressionNumber","IMSManufacturerNumber", "Name", "CADCode", "Specification", "EnglishName", "PartType", "MeasureUnit", "ShelfLife", "LastSubstitute", "NextSubstitute", "Weight","Volume","GroupABCCategory", "Feature", "status", "CreatorId", "CreatorName", "CreateTime","MInPackingAmount","ExchangeIdentification"
                            });

                            var userInfo = Utils.GetCurrentUserInfo();
                            //往数据库增加配件信息
                            foreach(SparePartExtend item in rightList) {
                                var command = db.CreateDbCommand(sqlUpdate, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("Code", Strings.StrConv(item.Code, VbStrConv.Narrow).Replace("【", "[")));
                                command.Parameters.Add(db.CreateDbParameter("IMSCompressionNumber", item.IMSCompressionNumber));
                                command.Parameters.Add(db.CreateDbParameter("ModifierId", userInfo.Id));
                                command.Parameters.Add(db.CreateDbParameter("ModifierName", userInfo.Name));
                                command.Parameters.Add(db.CreateDbParameter("ModifyTime", DateTime.Now));
                                command.Parameters.Add(db.CreateDbParameter("OMSparePartMark", 0));
                                command.ExecuteNonQuery();

                                //                                //获取数据库的完整备件信息
                                //                                string[] partCode = new string[] { item.Code };

                                //                                Func<string[], bool> getDbSparePartInfo = vales => {
                                //                                    item.Id = Convert.ToInt32(vales[0]);
                                //                                    //item.Code = vales[1];
                                //                                    item.Name = vales[2];
                                //                                    item.ReferenceCode = vales[3];
                                //                                    item.LastSubstitute = vales[4];
                                //                                    item.NextSubstitute = vales[5];
                                //                                    item.ShelfLife = string.IsNullOrEmpty(vales[6]) ? 0 : Convert.ToInt32(vales[6]);
                                //                                    item.EnglishName = vales[7];
                                //                                    item.PinyinCode = vales[8];
                                //                                    item.IMSManufacturerNumber = vales[9];
                                //                                    item.ReferenceName = vales[10];
                                //                                    item.CADCode = vales[11];
                                //                                    item.CADName = vales[12];
                                //                                    item.PartType = string.IsNullOrEmpty(vales[13]) ? 0 : Convert.ToInt32(vales[13]);
                                //                                    item.Specification = vales[14];
                                //                                    item.Feature = vales[15];
                                //                                    item.Status = string.IsNullOrEmpty(vales[16]) ? 0 : Convert.ToInt32(vales[16]);
                                //                                    item.Length = string.IsNullOrEmpty(vales[17]) ? 0 : Convert.ToDecimal(vales[17]);
                                //                                    item.Width = string.IsNullOrEmpty(vales[18]) ? 0 : Convert.ToDecimal(vales[18]);
                                //                                    item.Height = string.IsNullOrEmpty(vales[19]) ? 0 : Convert.ToDecimal(vales[19]);
                                //                                    item.Volume = string.IsNullOrEmpty(vales[20]) ? 0 : Convert.ToDecimal(vales[20]);
                                //                                    item.Weight = string.IsNullOrEmpty(vales[21]) ? 0 : Convert.ToDecimal(vales[21]);
                                //                                    item.Material = vales[22];
                                //                                    item.PackingAmount = string.IsNullOrEmpty(vales[23]) ? 0 : Convert.ToInt32(vales[23]);
                                //                                    item.PackingSpecification = vales[24];
                                //                                    item.PartsOutPackingCode = vales[25];
                                //                                    item.PartsInPackingCode = vales[26];
                                //                                    item.MeasureUnit = vales[27];
                                //                                    item.MInPackingAmount = string.IsNullOrEmpty(vales[28]) ? 0 : Convert.ToInt32(vales[28]);
                                //                                    item.GroupABCCategory = string.IsNullOrEmpty(vales[29]) ? 0 : Convert.ToInt32(vales[29]);
                                //                                    return false;
                                //                                };

                                //                                db.QueryDataWithInOperator(@"select ID,
                                //                                                                    CODE,
                                //                                                                    NAME,
                                //                                                                    ReferenceCode,
                                //                                                                    LASTSUBSTITUTE,
                                //                                                                    NEXTSUBSTITUTE,
                                //                                                                    SHELFLIFE,
                                //                                                                    ENGLISHNAME,
                                //                                                                    PINYINCODE,
                                //                                                                    IMSManufacturerNumber,
                                //                                                                    REFERENCENAME,
                                //                                                                    CADCODE,
                                //                                                                    CADNAME,
                                //                                                                    PARTTYPE,
                                //                                                                    SPECIFICATION,
                                //                                                                    FEATURE,
                                //                                                                    STATUS,
                                //                                                                    LENGTH,
                                //                                                                    WIDTH,
                                //                                                                    HEIGHT,
                                //                                                                    VOLUME,
                                //                                                                    WEIGHT,
                                //                                                                    MATERIAL,
                                //                                                                    PACKINGAMOUNT,
                                //                                                                    PACKINGSPECIFICATION,
                                //                                                                    PARTSOUTPACKINGCODE,
                                //                                                                    PARTSINPACKINGCODE,
                                //                                                                    MEASUREUNIT,
                                //                                                                    MINPACKINGAMOUNT,
                                //                                                                    GroupABCCategory
                                //                                                                from sparepart where status=1 ", "Code", true, partCode, getDbSparePartInfo);

                                //往数据库增加履历
                                var sparePart = dbSparePartCodes.Where(r => r.Code == item.Code).FirstOrDefault();
                                if(sparePart != null) {
                                    var commandHistory = db.CreateDbCommand(sqlInsertHistory, conn, ts);
                                    commandHistory.Parameters.Add(db.CreateDbParameter("SparePartId", sparePart.Id));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Code", Strings.StrConv(sparePart.Code, VbStrConv.Narrow).Replace("【", "[")));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Name", sparePart.Name));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("ReferenceCode", sparePart.ReferenceCode));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("IMSCompressionNumber", item.IMSCompressionNumber));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("IMSManufacturerNumber", sparePart.IMSManufacturerNumber));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("CADCode", sparePart.CADCode));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Specification", sparePart.Specification));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("EnglishName", sparePart.EnglishName));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("IsNotWarrantyTransfer", sparePart.IsNotWarrantyTransfer));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("PartType", sparePart.PartType));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("MeasureUnit", sparePart.MeasureUnit));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("ShelfLife", sparePart.ShelfLife));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("LastSubstitute", sparePart.LastSubstitute));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("NextSubstitute", sparePart.NextSubstitute));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Weight", sparePart.Weight));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Feature", sparePart.Feature));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Status", sparePart.Status));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Volume", sparePart.Volume));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("MInPackingAmount", sparePart.MInPackingAmount));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("GroupABCCategory", sparePart.GroupABCCategory));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("ExchangeIdentification", sparePart.ExchangeIdentification));
                                    commandHistory.ExecuteNonQuery();
                                }
                            }

                        }
                        //无异常提交
                        ts.Commit();
                    } catch(Exception ex) {
                        //报错回滚
                        ts.Rollback();
                        throw new Exception(ex.Message);
                    } finally {
                        if(conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }

        public bool 批量替换IMS厂商号(string fileName, out int excelImportNum, out List<SparePartExtend> rightData, out List<SparePartExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<SparePartExtend>();
            var rightList = new List<SparePartExtend>();
            var allList = new List<SparePartExtend>();
            var dbSparePartCodes = new List<SparePartExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("SparePart", out notNullableFields, out fieldLenght);
                List<object> excelColumns;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Code, "Code");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Sparepart_IMSManufacturerNumber, "IMSManufacturerNumber");
                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;
                    var keyValuePairs = new[] {
                                new KeyValuePair<string, string>("Status","MasterData_Status") 
                            };
                    tempExcelOperator.ImportHelper.LoadEnumFromDb(null, null, keyValuePairs);
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var sparePart = new SparePartExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        sparePart.CodeStr = Strings.StrConv(newRow["Code"], VbStrConv.Narrow).Replace("【", "[");
                        sparePart.CodeStr = strconvsparecode(sparePart.CodeStr);
                        sparePart.Code = Strings.StrConv(newRow["Code"], VbStrConv.Narrow).Replace("【", "[");
                        sparePart.Code = strconvsparecode(sparePart.Code);
                        sparePart.Code = sparePart.Code;
                        sparePart.IMSManufacturerNumber = newRow["IMSManufacturerNumber"];
                        sparePart.IMSManufacturerNumberStr = newRow["IMSManufacturerNumber"];
                        var tempErrorMessage = new List<string>();
                        //配件编号检查
                        var fieldIndex = notNullableFields.IndexOf("CODE");
                        if(string.IsNullOrEmpty(sparePart.Code)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.OverstockPartsAppDetail__Validation1);
                        } else {
                            //配件长度不得超过25
                            if(Encoding.Default.GetByteCount(sparePart.Code) > 25)
                                tempErrorMessage.Add(ErrorStrings.ImpPartsSupplierRelation_Validation8);
                        }
                        //IMS厂商号检查
                        if(sparePart.IMSManufacturerNumber != null && Encoding.Default.GetByteCount(sparePart.IMSManufacturerNumber) > 50)
                            tempErrorMessage.Add(ErrorStrings.Export_Validation_Sparepart_IMSManufacturerNumberIsLong);
                        if(tempErrorMessage.Count > 0) {
                            sparePart.ErrorMsg = string.Join("; ", tempErrorMessage);
                        } else {
                            var tempStatus = tempExcelOperator.ImportHelper.GetEnumValue("Status", ErrorStrings.Export_Validation_PartsBranch_Used);
                            if(tempStatus.HasValue) {
                                sparePart.Status = tempStatus.Value;
                            }
                        }

                        allList.Add(sparePart);
                        return false;
                    });
                    //校验导入的数据配件编号重复
                    var groups = allList.GroupBy(r =>
                        r.Code
                    ).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        if(groupItem.ErrorMsg == null) {
                            groupItem.ErrorMsg = ErrorStrings.Export_Title_PartsBranch_Code + groupItem.Code + ErrorStrings.Export_Validation_PartsBranch_Validation5;
                        } else {
                            groupItem.ErrorMsg = groupItem.ErrorMsg + ErrorStrings.Export_Validation_PartsBranch_Validation6 + groupItem.Code + ErrorStrings.Export_Validation_PartsBranch_Validation5;
                        }
                    }
                    var sparePartsNeedCheck = allList.ToList();
                    var partCodesNeedCheck = sparePartsNeedCheck.Select(r => r.Code).Distinct().ToArray();

                    Func<string[], bool> getDbSparePartCodes = vales => {
                        var dbSparePart = new SparePartExtend();
                        dbSparePart.Id = Convert.ToInt32(vales[0]);
                        dbSparePart.Code = vales[1];
                        dbSparePart.Name = vales[2];
                        dbSparePart.ReferenceCode = vales[3];
                        dbSparePart.LastSubstitute = vales[4];
                        dbSparePart.NextSubstitute = vales[5];
                        dbSparePart.ShelfLife = string.IsNullOrEmpty(vales[6]) ? 0 : Convert.ToInt32(vales[6]);
                        dbSparePart.EnglishName = vales[7];
                        dbSparePart.PinyinCode = vales[8];
                        dbSparePart.IMSCompressionNumber = vales[9];
                        dbSparePart.ReferenceName = vales[10];
                        dbSparePart.CADCode = vales[11];
                        dbSparePart.CADName = vales[12];
                        dbSparePart.PartType = string.IsNullOrEmpty(vales[13]) ? 0 : Convert.ToInt32(vales[13]);
                        dbSparePart.Specification = vales[14];
                        dbSparePart.Feature = vales[15];
                        dbSparePart.Status = string.IsNullOrEmpty(vales[16]) ? 0 : Convert.ToInt32(vales[16]);
                        dbSparePart.Length = string.IsNullOrEmpty(vales[17]) ? 0 : Convert.ToDecimal(vales[17]);
                        dbSparePart.Width = string.IsNullOrEmpty(vales[18]) ? 0 : Convert.ToDecimal(vales[18]);
                        dbSparePart.Height = string.IsNullOrEmpty(vales[19]) ? 0 : Convert.ToDecimal(vales[19]);
                        dbSparePart.Volume = string.IsNullOrEmpty(vales[20]) ? 0 : Convert.ToDecimal(vales[20]);
                        dbSparePart.Weight = string.IsNullOrEmpty(vales[21]) ? 0 : Convert.ToDecimal(vales[21]);
                        dbSparePart.Material = vales[22];
                        dbSparePart.PackingAmount = string.IsNullOrEmpty(vales[23]) ? 0 : Convert.ToInt32(vales[23]);
                        dbSparePart.PackingSpecification = vales[24];
                        dbSparePart.PartsOutPackingCode = vales[25];
                        dbSparePart.PartsInPackingCode = vales[26];
                        dbSparePart.MeasureUnit = vales[27];
                        dbSparePart.MInPackingAmount = string.IsNullOrEmpty(vales[28]) ? 0 : Convert.ToInt32(vales[28]);
                        dbSparePart.GroupABCCategory = string.IsNullOrEmpty(vales[29]) ? 0 : Convert.ToInt32(vales[29]);
                        dbSparePart.IsNotWarrantyTransfer = string.IsNullOrEmpty(vales[30]) ? 0 : Convert.ToInt32(vales[30]);
                        dbSparePart.ExchangeIdentification = vales[31];
                        dbSparePartCodes.Add(dbSparePart);
                        return false;
                    };
                    //根据SQL查询数据库，将查出来的结果根据定义的委托中的方法（这里是getDbSparePartCodes）放到具体的集合中（这里是dbSparePartCodes）
                    db.QueryDataWithInOperator(@"select ID,
                                                                    CODE,
                                                                    NAME,
                                                                    ReferenceCode,
                                                                    LASTSUBSTITUTE,
                                                                    NEXTSUBSTITUTE,
                                                                    SHELFLIFE,
                                                                    ENGLISHNAME,
                                                                    PINYINCODE,
                                                                    IMSCompressionNumber,
                                                                    REFERENCENAME,
                                                                    CADCODE,
                                                                    CADNAME,
                                                                    PARTTYPE,
                                                                    SPECIFICATION,
                                                                    FEATURE,
                                                                    STATUS,
                                                                    LENGTH,
                                                                    WIDTH,
                                                                    HEIGHT,
                                                                    VOLUME,
                                                                    WEIGHT,
                                                                    MATERIAL,
                                                                    PACKINGAMOUNT,
                                                                    PACKINGSPECIFICATION,
                                                                    PARTSOUTPACKINGCODE,
                                                                    PARTSINPACKINGCODE,
                                                                    MEASUREUNIT,
                                                                    MINPACKINGAMOUNT,
                                                                    GroupABCCategory,IsNotWarrantyTransfer,ExchangeIdentification
                                                                from sparepart where status=1 ", "Code", false, partCodesNeedCheck, getDbSparePartCodes);
                    if(partCodesNeedCheck.Length > 0) {
                        var sparePartsExistsCode = sparePartsNeedCheck.Where(r => dbSparePartCodes.Any(v => v.Code == r.Code)).ToArray();
                        if(sparePartsExistsCode.Count() < sparePartsNeedCheck.Count()) {
                            var notExist = allList.Except(sparePartsExistsCode);
                            foreach(var item in notExist) {
                                if(item.ErrorMsg == null) {
                                    item.ErrorMsg = ErrorStrings.Export_Validation_PartsBranch_Validation8 + item.Code + ErrorStrings.Export_Validation_Sparepart_NotExistOrStop;
                                } else {
                                    item.ErrorMsg = item.ErrorMsg + ErrorStrings.Export_Validation_PartsBranch_Validation9 + item.Code + ErrorStrings.Export_Validation_Sparepart_NotExistOrStop;
                                }
                                errorList.Add(item);
                            }
                        }
                    }
                }
                //获取所有不合格数据
                errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                //if(errList.Count > 0) {
                //    errorList.AddRange(errList);
                //}
                //获取所有合格数据
                rightList = allList.Except(errorList).ToList();

                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempSparePart = list[index - 1];
                            var values = new object[] {
                                tempSparePart.CodeStr, tempSparePart.ReferenceCodeStr,
                                tempSparePart.ErrorMsg
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }

                //导入所有合格数据
                if(!rightList.Any())
                    return true;
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    //开启事务，新增更新配件在一个事务内
                    var ts = conn.BeginTransaction();
                    try {
                        //新增配件
                        if(rightList.Any()) {
                            //获取新增数据的sql语句，Id为主键
                            var sqlUpdate = db.GetUpdateSql("SparePart", new string[] { "IMSManufacturerNumber", "ModifierId", "ModifierName", "ModifyTime", "OMSparePartMark" }, new string[] { "Code" });
                            var sqlInsertHistory = db.GetInsertSql("SparePartHistory", "Id", new[] {
                                "IsNotWarrantyTransfer","SparePartId","Code","ReferenceCode","IMSCompressionNumber","IMSManufacturerNumber", "Name", "CADCode", "Specification", "EnglishName", "PartType", "MeasureUnit", "ShelfLife", "LastSubstitute", "NextSubstitute", "Weight","Volume","GroupABCCategory", "Feature", "status", "CreatorId", "CreatorName", "CreateTime","MInPackingAmount","ExchangeIdentification"
                            });

                            var userInfo = Utils.GetCurrentUserInfo();
                            //往数据库增加配件信息
                            foreach(SparePartExtend item in rightList) {
                                var command = db.CreateDbCommand(sqlUpdate, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("Code", Strings.StrConv(item.Code, VbStrConv.Narrow).Replace("【", "[")));
                                command.Parameters.Add(db.CreateDbParameter("IMSManufacturerNumber", item.IMSManufacturerNumber));
                                command.Parameters.Add(db.CreateDbParameter("ModifierId", userInfo.Id));
                                command.Parameters.Add(db.CreateDbParameter("ModifierName", userInfo.Name));
                                command.Parameters.Add(db.CreateDbParameter("ModifyTime", DateTime.Now));
                                command.Parameters.Add(db.CreateDbParameter("OMSparePartMark", 0));
                                command.ExecuteNonQuery();

                                //                                //获取数据库的完整备件信息
                                //                                string[] partCode = new string[] { item.Code };

                                //                                Func<string[], bool> getDbSparePartInfo = vales => {
                                //                                    item.Id = Convert.ToInt32(vales[0]);
                                //                                    //item.Code = vales[1];
                                //                                    item.Name = vales[2];
                                //                                    item.ReferenceCode = vales[3];
                                //                                    item.LastSubstitute = vales[4];
                                //                                    item.NextSubstitute = vales[5];
                                //                                    item.ShelfLife = string.IsNullOrEmpty(vales[6]) ? 0 : Convert.ToInt32(vales[6]);
                                //                                    item.EnglishName = vales[7];
                                //                                    item.PinyinCode = vales[8];
                                //                                    item.IMSCompressionNumber = vales[9];
                                //                                    item.ReferenceName = vales[10];
                                //                                    item.CADCode = vales[11];
                                //                                    item.CADName = vales[12];
                                //                                    item.PartType = string.IsNullOrEmpty(vales[13]) ? 0 : Convert.ToInt32(vales[13]);
                                //                                    item.Specification = vales[14];
                                //                                    item.Feature = vales[15];
                                //                                    item.Status = string.IsNullOrEmpty(vales[16]) ? 0 : Convert.ToInt32(vales[16]);
                                //                                    item.Length = string.IsNullOrEmpty(vales[17]) ? 0 : Convert.ToDecimal(vales[17]);
                                //                                    item.Width = string.IsNullOrEmpty(vales[18]) ? 0 : Convert.ToDecimal(vales[18]);
                                //                                    item.Height = string.IsNullOrEmpty(vales[19]) ? 0 : Convert.ToDecimal(vales[19]);
                                //                                    item.Volume = string.IsNullOrEmpty(vales[20]) ? 0 : Convert.ToDecimal(vales[20]);
                                //                                    item.Weight = string.IsNullOrEmpty(vales[21]) ? 0 : Convert.ToDecimal(vales[21]);
                                //                                    item.Material = vales[22];
                                //                                    item.PackingAmount = string.IsNullOrEmpty(vales[23]) ? 0 : Convert.ToInt32(vales[23]);
                                //                                    item.PackingSpecification = vales[24];
                                //                                    item.PartsOutPackingCode = vales[25];
                                //                                    item.PartsInPackingCode = vales[26];
                                //                                    item.MeasureUnit = vales[27];
                                //                                    item.MInPackingAmount = string.IsNullOrEmpty(vales[28]) ? 0 : Convert.ToInt32(vales[28]);
                                //                                    item.GroupABCCategory = string.IsNullOrEmpty(vales[29]) ? 0 : Convert.ToInt32(vales[29]);
                                //                                    return false;
                                //                                };

                                //                                db.QueryDataWithInOperator(@"select ID,
                                //                                                                    CODE,
                                //                                                                    NAME,
                                //                                                                    ReferenceCode,
                                //                                                                    LASTSUBSTITUTE,
                                //                                                                    NEXTSUBSTITUTE,
                                //                                                                    SHELFLIFE,
                                //                                                                    ENGLISHNAME,
                                //                                                                    PINYINCODE,
                                //                                                                    IMSCompressionNumber,
                                //                                                                    REFERENCENAME,
                                //                                                                    CADCODE,
                                //                                                                    CADNAME,
                                //                                                                    PARTTYPE,
                                //                                                                    SPECIFICATION,
                                //                                                                    FEATURE,
                                //                                                                    STATUS,
                                //                                                                    LENGTH,
                                //                                                                    WIDTH,
                                //                                                                    HEIGHT,
                                //                                                                    VOLUME,
                                //                                                                    WEIGHT,
                                //                                                                    MATERIAL,
                                //                                                                    PACKINGAMOUNT,
                                //                                                                    PACKINGSPECIFICATION,
                                //                                                                    PARTSOUTPACKINGCODE,
                                //                                                                    PARTSINPACKINGCODE,
                                //                                                                    MEASUREUNIT,
                                //                                                                    MINPACKINGAMOUNT,
                                //                                                                    GroupABCCategory
                                //                                                                from sparepart where status=1 ", "Code", true, partCode, getDbSparePartInfo);

                                //往数据库增加履历
                                var sparePart = dbSparePartCodes.Where(r => r.Code == item.Code).FirstOrDefault();
                                if(sparePart != null) {
                                    var commandHistory = db.CreateDbCommand(sqlInsertHistory, conn, ts);
                                    commandHistory.Parameters.Add(db.CreateDbParameter("SparePartId", sparePart.Id));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Code", Strings.StrConv(sparePart.Code, VbStrConv.Narrow).Replace("【", "[")));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Name", sparePart.Name));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("ReferenceCode", sparePart.ReferenceCode));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("IMSCompressionNumber", sparePart.IMSCompressionNumber));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("IMSManufacturerNumber", item.IMSManufacturerNumber));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("CADCode", sparePart.CADCode));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Specification", sparePart.Specification));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("EnglishName", sparePart.EnglishName));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("IsNotWarrantyTransfer", sparePart.IsNotWarrantyTransfer));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("PartType", sparePart.PartType));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("MeasureUnit", sparePart.MeasureUnit));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("ShelfLife", sparePart.ShelfLife));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("LastSubstitute", sparePart.LastSubstitute));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("NextSubstitute", sparePart.NextSubstitute));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Weight", sparePart.Weight));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Feature", sparePart.Feature));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Status", sparePart.Status));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Volume", sparePart.Volume));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("MInPackingAmount", sparePart.MInPackingAmount));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("GroupABCCategory", sparePart.GroupABCCategory));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("ExchangeIdentification", sparePart.ExchangeIdentification));
                                    commandHistory.ExecuteNonQuery();
                                }
                            }
                        }
                        //无异常提交
                        ts.Commit();
                    } catch(Exception ex) {
                        //报错回滚
                        ts.Rollback();
                        throw new Exception(ex.Message);
                    } finally {
                        if(conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }

        public bool 批量替换最小包装数量(string fileName, out int excelImportNum, out List<SparePartExtend> rightData, out List<SparePartExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<SparePartExtend>();
            var rightList = new List<SparePartExtend>();
            var allList = new List<SparePartExtend>();
            var dbSparePartCodes = new List<SparePartExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("SparePart", out notNullableFields, out fieldLenght);
                List<object> excelColumns;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Code, "Code");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Sparepart_MInPackingAmount, "MInPackingAmount");
                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;
                    var keyValuePairs = new[] {
                                new KeyValuePair<string, string>("Status","MasterData_Status") 
                            };
                    tempExcelOperator.ImportHelper.LoadEnumFromDb(null, null, keyValuePairs);
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var sparePart = new SparePartExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        sparePart.CodeStr = Strings.StrConv(newRow["Code"], VbStrConv.Narrow).Replace("【", "[");
                        sparePart.CodeStr = strconvsparecode(sparePart.CodeStr);
                        sparePart.Code = Strings.StrConv(newRow["Code"], VbStrConv.Narrow).Replace("【", "[");
                        sparePart.Code = strconvsparecode(sparePart.Code);
                        sparePart.Code = sparePart.Code;//全部大写
                        sparePart.MInPackingAmountStr = newRow["MInPackingAmount"];
                        var tempErrorMessage = new List<string>();
                        //配件编号检查
                        var fieldIndex = notNullableFields.IndexOf("CODE");
                        if(string.IsNullOrEmpty(sparePart.Code)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.OverstockPartsAppDetail__Validation1);
                        } else {
                            //配件长度不得超过25
                            if(Encoding.Default.GetByteCount(sparePart.Code) > 25)
                                tempErrorMessage.Add(ErrorStrings.ImpPartsSupplierRelation_Validation8);
                        }
                        //最小包装数量
                        fieldIndex = notNullableFields.IndexOf("MInPackingAmount");
                        if(string.IsNullOrEmpty(row["MInPackingAmount"])) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Sparepart_MInPackingAmountIsNull);
                        } else {
                            try {
                                var minPackingAmount = Convert.ToInt32(row["MInPackingAmount"]);
                                sparePart.MInPackingAmount = minPackingAmount;
                            } catch(Exception) {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Sparepart_MInPackingAmountIsInteger);
                            }

                            if(sparePart.MInPackingAmount <= 0) {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Sparepart_MInPackingAmountOverZero);
                            }
                        }

                        if(tempErrorMessage.Count > 0) {
                            sparePart.ErrorMsg = string.Join("; ", tempErrorMessage);
                        } else {
                            var tempStatus = tempExcelOperator.ImportHelper.GetEnumValue("Status", ErrorStrings.Export_Validation_PartsBranch_Used);
                            if(tempStatus.HasValue) {
                                sparePart.Status = tempStatus.Value;
                            }
                        }


                        allList.Add(sparePart);
                        return false;
                    });
                    //校验导入的数据配件编号重复
                    var groups = allList.GroupBy(r =>
                        r.Code
                    ).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        if(groupItem.ErrorMsg == null) {
                            groupItem.ErrorMsg = ErrorStrings.Export_Title_PartsBranch_Code + groupItem.Code + ErrorStrings.Export_Validation_PartsBranch_Validation5;
                        } else {
                            groupItem.ErrorMsg = groupItem.ErrorMsg + ErrorStrings.Export_Validation_PartsBranch_Validation6 + groupItem.Code + ErrorStrings.Export_Validation_PartsBranch_Validation5;
                        }
                    }

                    var sparePartsNeedCheck = allList.ToList();
                    var partCodesNeedCheck = sparePartsNeedCheck.Select(r => r.Code).Distinct().ToArray();

                    Func<string[], bool> getDbSparePartCodes = vales => {
                        var dbSparePart = new SparePartExtend();
                        dbSparePart.Id = Convert.ToInt32(vales[0]);
                        dbSparePart.Code = vales[1];
                        dbSparePart.ReferenceCode = vales[3];
                        dbSparePart.Name = vales[2];
                        dbSparePart.LastSubstitute = vales[4];
                        dbSparePart.NextSubstitute = vales[5];
                        dbSparePart.ShelfLife = string.IsNullOrEmpty(vales[6]) ? 0 : Convert.ToInt32(vales[6]);
                        dbSparePart.EnglishName = vales[7];
                        dbSparePart.PinyinCode = vales[8];
                        dbSparePart.ReferenceCode = vales[9];
                        dbSparePart.ReferenceName = vales[10];
                        dbSparePart.CADCode = vales[11];
                        dbSparePart.CADName = vales[12];
                        dbSparePart.PartType = string.IsNullOrEmpty(vales[13]) ? 0 : Convert.ToInt32(vales[13]);
                        dbSparePart.Specification = vales[14];
                        dbSparePart.Feature = vales[15];
                        dbSparePart.Status = string.IsNullOrEmpty(vales[16]) ? 0 : Convert.ToInt32(vales[16]);
                        dbSparePart.Length = string.IsNullOrEmpty(vales[17]) ? 0 : Convert.ToDecimal(vales[17]);
                        dbSparePart.Width = string.IsNullOrEmpty(vales[18]) ? 0 : Convert.ToDecimal(vales[18]);
                        dbSparePart.Height = string.IsNullOrEmpty(vales[19]) ? 0 : Convert.ToDecimal(vales[19]);
                        dbSparePart.Volume = string.IsNullOrEmpty(vales[20]) ? 0 : Convert.ToDecimal(vales[20]);
                        dbSparePart.Weight = string.IsNullOrEmpty(vales[21]) ? 0 : Convert.ToDecimal(vales[21]);
                        dbSparePart.Material = vales[22];
                        dbSparePart.PackingAmount = string.IsNullOrEmpty(vales[23]) ? 0 : Convert.ToInt32(vales[23]);
                        dbSparePart.PackingSpecification = vales[24];
                        dbSparePart.PartsOutPackingCode = vales[25];
                        dbSparePart.PartsInPackingCode = vales[26];
                        dbSparePart.MeasureUnit = vales[27];
                        dbSparePart.MInPackingAmount = string.IsNullOrEmpty(vales[28]) ? 0 : Convert.ToInt32(vales[28]);
                        dbSparePart.IsNotWarrantyTransfer = string.IsNullOrEmpty(vales[29]) ? 0 : Convert.ToInt32(vales[29]);
                        dbSparePart.ExchangeIdentification = vales[30];
                        dbSparePartCodes.Add(dbSparePart);
                        return false;
                    };
                    //根据SQL查询数据库，将查出来的结果根据定义的委托中的方法（这里是getDbSparePartCodes）放到具体的集合中（这里是dbSparePartCodes）
                    db.QueryDataWithInOperator(@"select ID,
                                                                    CODE,
                                                                    NAME,
                                                                    ReferenceCode,
                                                                    LASTSUBSTITUTE,
                                                                    NEXTSUBSTITUTE,
                                                                    SHELFLIFE,
                                                                    ENGLISHNAME,
                                                                    PINYINCODE,
                                                                    REFERENCECODE,
                                                                    REFERENCENAME,
                                                                    CADCODE,
                                                                    CADNAME,
                                                                    PARTTYPE,
                                                                    SPECIFICATION,
                                                                    FEATURE,
                                                                    STATUS,
                                                                    LENGTH,
                                                                    WIDTH,
                                                                    HEIGHT,
                                                                    VOLUME,
                                                                    WEIGHT,
                                                                    MATERIAL,
                                                                    PACKINGAMOUNT,
                                                                    PACKINGSPECIFICATION,
                                                                    PARTSOUTPACKINGCODE,
                                                                    PARTSINPACKINGCODE,
                                                                    MEASUREUNIT,
                                                                    MINPACKINGAMOUNT,IsNotWarrantyTransfer,ExchangeIdentification
                                                                from sparepart where status=1 ", "Code", false, partCodesNeedCheck, getDbSparePartCodes);
                    if(partCodesNeedCheck.Length > 0) {
                        var sparePartsExistsCode = sparePartsNeedCheck.Where(r => dbSparePartCodes.Any(v => v.Code == r.Code)).ToArray();
                        if(sparePartsExistsCode.Count() < sparePartsNeedCheck.Count()) {
                            var notExist = allList.Except(sparePartsExistsCode);
                            foreach(var item in notExist) {
                                if(item.ErrorMsg == null) {
                                    item.ErrorMsg = ErrorStrings.Export_Validation_PartsBranch_Validation8 + item.Code + ErrorStrings.Export_Validation_Sparepart_NotExistOrStop;
                                } else {
                                    item.ErrorMsg = item.ErrorMsg + ErrorStrings.Export_Validation_PartsBranch_Validation9 + item.Code + ErrorStrings.Export_Validation_Sparepart_NotExistOrStop;
                                }

                                errorList.Add(item);
                            }
                        }
                    }
                }
                //获取所有不合格数据
                errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                //if(errList.Count > 0) {
                //    errorList.AddRange(errList);
                //}
                //获取所有合格数据
                rightList = allList.Except(errorList).ToList();

                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempSparePart = list[index - 1];
                            var values = new object[] {
                                tempSparePart.CodeStr, tempSparePart.MInPackingAmountStr,
                                tempSparePart.ErrorMsg
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }

                //导入所有合格数据
                if(!rightList.Any())
                    return true;
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    //开启事务，新增更新配件在一个事务内
                    var ts = conn.BeginTransaction();
                    try {
                        //新增配件
                        if(rightList.Any()) {
                            //获取新增数据的sql语句，Id为主键
                            var sqlUpdate = db.GetUpdateSql("SparePart", new string[] { "MInPackingAmount", "ModifierId", "ModifierName", "ModifyTime", "OMSparePartMark" }, new string[] { "Code" });
                            var sqlInsertHistory = db.GetInsertSql("SparePartHistory", "Id", new[] {
                              "IsNotWarrantyTransfer",  "SparePartId","Code","ReferenceCode", "Name", "CADCode", "Specification", "EnglishName", "PartType", "MeasureUnit", "ShelfLife", "LastSubstitute", "NextSubstitute", "Weight", "Feature", "status", "CreatorId", "CreatorName", "CreateTime","MInPackingAmount","ExchangeIdentification"
                            });

                            var userInfo = Utils.GetCurrentUserInfo();
                            //往数据库增加配件信息
                            foreach(SparePartExtend item in rightList) {
                                var command = db.CreateDbCommand(sqlUpdate, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("Code", Strings.StrConv(item.Code, VbStrConv.Narrow).Replace("【", "[")));
                                command.Parameters.Add(db.CreateDbParameter("MInPackingAmount", item.MInPackingAmount));
                                command.Parameters.Add(db.CreateDbParameter("ModifierId", userInfo.Id));
                                command.Parameters.Add(db.CreateDbParameter("ModifierName", userInfo.Name));
                                command.Parameters.Add(db.CreateDbParameter("ModifyTime", DateTime.Now));
                                command.Parameters.Add(db.CreateDbParameter("OMSparePartMark", 0));
                                command.ExecuteNonQuery();

                                //                                //获取数据库的完整备件信息
                                //                                string[] partCode = new string[] { item.Code };

                                //                                Func<string[], bool> getDbSparePartInfo = vales => {
                                //                                    item.Id = Convert.ToInt32(vales[0]);
                                //                                    //item.Code = vales[1];
                                //                                    item.ReferenceCode = vales[3];
                                //                                    item.Name = vales[2];
                                //                                    item.LastSubstitute = vales[4];
                                //                                    item.NextSubstitute = vales[5];
                                //                                    item.ShelfLife = string.IsNullOrEmpty(vales[6]) ? 0 : Convert.ToInt32(vales[6]);
                                //                                    item.EnglishName = vales[7];
                                //                                    item.PinyinCode = vales[8];
                                //                                    item.ReferenceCode = vales[9];
                                //                                    item.ReferenceName = vales[10];
                                //                                    item.CADCode = vales[11];
                                //                                    item.CADName = vales[12];
                                //                                    item.PartType = string.IsNullOrEmpty(vales[13]) ? 0 : Convert.ToInt32(vales[13]);
                                //                                    item.Specification = vales[14];
                                //                                    item.Feature = vales[15];
                                //                                    item.Status = string.IsNullOrEmpty(vales[16]) ? 0 : Convert.ToInt32(vales[16]);
                                //                                    item.Length = string.IsNullOrEmpty(vales[17]) ? 0 : Convert.ToDecimal(vales[17]);
                                //                                    item.Width = string.IsNullOrEmpty(vales[18]) ? 0 : Convert.ToDecimal(vales[18]);
                                //                                    item.Height = string.IsNullOrEmpty(vales[19]) ? 0 : Convert.ToDecimal(vales[19]);
                                //                                    item.Volume = string.IsNullOrEmpty(vales[20]) ? 0 : Convert.ToDecimal(vales[20]);
                                //                                    item.Weight = string.IsNullOrEmpty(vales[21]) ? 0 : Convert.ToDecimal(vales[21]);
                                //                                    item.Material = vales[22];
                                //                                    item.PackingAmount = string.IsNullOrEmpty(vales[23]) ? 0 : Convert.ToInt32(vales[23]);
                                //                                    item.PackingSpecification = vales[24];
                                //                                    item.PartsOutPackingCode = vales[25];
                                //                                    item.PartsInPackingCode = vales[26];
                                //                                    item.MeasureUnit = vales[27];
                                //                                    //item.MInPackingAmount = string.IsNullOrEmpty(vales[28]) ? 0 : Convert.ToInt32(vales[28]);
                                //                                    return false;
                                //                                };

                                //                                db.QueryDataWithInOperator(@"select ID,
                                //                                                                    CODE,
                                //                                                                    NAME,
                                //                                                                    ReferenceCode,
                                //                                                                    LASTSUBSTITUTE,
                                //                                                                    NEXTSUBSTITUTE,
                                //                                                                    SHELFLIFE,
                                //                                                                    ENGLISHNAME,
                                //                                                                    PINYINCODE,
                                //                                                                    REFERENCECODE,
                                //                                                                    REFERENCENAME,
                                //                                                                    CADCODE,
                                //                                                                    CADNAME,
                                //                                                                    PARTTYPE,
                                //                                                                    SPECIFICATION,
                                //                                                                    FEATURE,
                                //                                                                    STATUS,
                                //                                                                    LENGTH,
                                //                                                                    WIDTH,
                                //                                                                    HEIGHT,
                                //                                                                    VOLUME,
                                //                                                                    WEIGHT,
                                //                                                                    MATERIAL,
                                //                                                                    PACKINGAMOUNT,
                                //                                                                    PACKINGSPECIFICATION,
                                //                                                                    PARTSOUTPACKINGCODE,
                                //                                                                    PARTSINPACKINGCODE,
                                //                                                                    MEASUREUNIT,
                                //                                                                    MINPACKINGAMOUNT
                                //                                                                from sparepart where status=1 ", "Code", true, partCode, getDbSparePartInfo);

                                //往数据库增加履历
                                var sparePart = dbSparePartCodes.Where(r => r.Code == item.Code).FirstOrDefault();
                                if(sparePart != null) {
                                    var commandHistory = db.CreateDbCommand(sqlInsertHistory, conn, ts);
                                    commandHistory.Parameters.Add(db.CreateDbParameter("SparePartId", sparePart.Id));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Code", Strings.StrConv(sparePart.Code, VbStrConv.Narrow).Replace("【", "[")));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Name", sparePart.Name));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("ReferenceCode", sparePart.ReferenceCode));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("IsNotWarrantyTransfer", sparePart.IsNotWarrantyTransfer));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("CADCode", sparePart.CADCode));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Specification", sparePart.Specification));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("EnglishName", sparePart.EnglishName));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("PartType", sparePart.PartType));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("MeasureUnit", sparePart.MeasureUnit));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("ShelfLife", sparePart.ShelfLife));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("LastSubstitute", sparePart.LastSubstitute));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("NextSubstitute", sparePart.NextSubstitute));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Weight", sparePart.Weight));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Feature", sparePart.Feature));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Status", sparePart.Status));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("MInPackingAmount", item.MInPackingAmount));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("ExchangeIdentification", sparePart.ExchangeIdentification));
                                    commandHistory.ExecuteNonQuery();
                                }
                            }

                        }
                        //无异常提交
                        ts.Commit();
                    } catch(Exception ex) {
                        //报错回滚
                        ts.Rollback();
                        throw new Exception(ex.Message);
                    } finally {
                        if(conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }

        public bool 批量替换下一替代件(string fileName, out int excelImportNum, out List<SparePartExtend> rightData, out List<SparePartExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<SparePartExtend>();
            var rightList = new List<SparePartExtend>();
            var allList = new List<SparePartExtend>();
            var dbSparePartCodes = new List<SparePartExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("SparePart", out notNullableFields, out fieldLenght);
                List<object> excelColumns;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Code, "Code");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Sparepart_NextSubstitute, "NextSubstitute");
                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;
                    var keyValuePairs = new[] {
                                new KeyValuePair<string, string>("Status","MasterData_Status") 
                            };
                    tempExcelOperator.ImportHelper.LoadEnumFromDb(null, null, keyValuePairs);
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var sparePart = new SparePartExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        sparePart.CodeStr = Strings.StrConv(newRow["Code"], VbStrConv.Narrow).Replace("【", "[");
                        sparePart.CodeStr = strconvsparecode(sparePart.CodeStr);
                        sparePart.Code = Strings.StrConv(newRow["Code"], VbStrConv.Narrow).Replace("【", "[");
                        sparePart.Code = strconvsparecode(sparePart.Code);
                        sparePart.Code = sparePart.Code;//全部大写
                        sparePart.NextSubstituteStr = newRow["NextSubstitute"];
                        sparePart.NextSubstitute = newRow["NextSubstitute"];
                        var tempErrorMessage = new List<string>();
                        //配件编号检查
                        var fieldIndex = notNullableFields.IndexOf("CODE");
                        if(string.IsNullOrEmpty(sparePart.Code)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.OverstockPartsAppDetail__Validation1);
                        } else {
                            //配件长度不得超过25
                            if(Encoding.Default.GetByteCount(sparePart.Code) > 25)
                                tempErrorMessage.Add(ErrorStrings.ImpPartsSupplierRelation_Validation8);
                        }
                        //下一替代件检查
                        if(sparePart.NextSubstitute != null && Encoding.Default.GetByteCount(sparePart.NextSubstitute) > 50)
                            tempErrorMessage.Add(ErrorStrings.Export_Validation_Sparepart_NextSubstituteIsLong);
                        if(tempErrorMessage.Count > 0) {
                            sparePart.ErrorMsg = string.Join("; ", tempErrorMessage);
                        } else {
                            var tempStatus = tempExcelOperator.ImportHelper.GetEnumValue("Status", ErrorStrings.Export_Validation_PartsBranch_Used);
                            if(tempStatus.HasValue) {
                                sparePart.Status = tempStatus.Value;
                            }
                        }
                        allList.Add(sparePart);
                        return false;
                    });
                    //校验导入的数据配件编号重复
                    var groups = allList.GroupBy(r =>
                        r.Code
                    ).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        if(groupItem.ErrorMsg == null) {
                            groupItem.ErrorMsg = ErrorStrings.Export_Title_PartsBranch_Code + groupItem.Code + ErrorStrings.Export_Validation_PartsBranch_Validation5;
                        } else {
                            groupItem.ErrorMsg = groupItem.ErrorMsg + ErrorStrings.Export_Validation_PartsBranch_Validation6 + groupItem.Code + ErrorStrings.Export_Validation_PartsBranch_Validation5;
                        }
                    }
                    var sparePartsNeedCheck = allList.ToList();
                    var partCodesNeedCheck = sparePartsNeedCheck.Select(r => r.Code).Distinct().ToArray();

                    Func<string[], bool> getDbSparePartCodes = vales => {
                        var dbSparePart = new SparePartExtend {
                            Id = Convert.ToInt32(vales[0]),
                            Code = vales[1],
                            Name = vales[2],
                            ReferenceCode = vales[3],
                            LastSubstitute = vales[4],
                            NextSubstitute = vales[5],
                            ShelfLife = string.IsNullOrEmpty(vales[6]) ? 0 : Convert.ToInt32(vales[6]),
                            EnglishName = vales[7],
                            PinyinCode = vales[8],
                            ReferenceName = vales[9],
                            CADCode = vales[10],
                            CADName = vales[11],
                            PartType = string.IsNullOrEmpty(vales[12]) ? 0 : Convert.ToInt32(vales[12]),
                            Specification = vales[13],
                            Feature = vales[14],
                            Status = string.IsNullOrEmpty(vales[15]) ? 0 : Convert.ToInt32(vales[15]),
                            Length = string.IsNullOrEmpty(vales[16]) ? 0 : Convert.ToDecimal(vales[16]),
                            Width = string.IsNullOrEmpty(vales[17]) ? 0 : Convert.ToDecimal(vales[17]),
                            Height = string.IsNullOrEmpty(vales[18]) ? 0 : Convert.ToDecimal(vales[18]),
                            Volume = string.IsNullOrEmpty(vales[19]) ? 0 : Convert.ToDecimal(vales[19]),
                            Weight = string.IsNullOrEmpty(vales[20]) ? 0 : Convert.ToDecimal(vales[20]),
                            Material = vales[21],
                            PackingAmount = string.IsNullOrEmpty(vales[22]) ? 0 : Convert.ToInt32(vales[22]),
                            PackingSpecification = vales[23],
                            PartsOutPackingCode = vales[24],
                            PartsInPackingCode = vales[25],
                            MeasureUnit = vales[26],
                            MInPackingAmount = string.IsNullOrEmpty(vales[27]) ? 1 : Convert.ToInt32(vales[27]),
                            IsNotWarrantyTransfer = string.IsNullOrEmpty(vales[28]) ? 1 : Convert.ToInt32(vales[28]),
                            ExchangeIdentification = vales[29],
                        };
                        dbSparePartCodes.Add(dbSparePart);
                        return false;
                    };
                    //根据SQL查询数据库，将查出来的结果根据定义的委托中的方法（这里是getDbSparePartCodes）放到具体的集合中（这里是dbSparePartCodes）
                    db.QueryDataWithInOperator(@"Select ID, CODE,NAME,ReferenceCode,LASTSUBSTITUTE,NEXTSUBSTITUTE,SHELFLIFE,ENGLISHNAME,PINYINCODE,REFERENCENAME,CADCODE,CADNAME,PARTTYPE,
                                                                    SPECIFICATION,FEATURE,STATUS,LENGTH,WIDTH,HEIGHT,VOLUME,WEIGHT,MATERIAL,PACKINGAMOUNT,PACKINGSPECIFICATION,PARTSOUTPACKINGCODE,
                                                                    PARTSINPACKINGCODE,MEASUREUNIT,MINPACKINGAMOUNT,IsNotWarrantyTransfer,ExchangeIdentification
                                                                from sparepart where status=1 ", "Code", false, partCodesNeedCheck, getDbSparePartCodes);
                    if(partCodesNeedCheck.Length > 0) {
                        var sparePartsExistsCode = sparePartsNeedCheck.Where(r => dbSparePartCodes.Any(v => v.Code == r.Code)).ToArray();
                        if(sparePartsExistsCode.Count() < sparePartsNeedCheck.Count()) {
                            var notExist = allList.Except(sparePartsExistsCode);
                            foreach(var item in notExist) {
                                if(item.ErrorMsg == null) {
                                    item.ErrorMsg = ErrorStrings.Export_Validation_PartsBranch_Validation8 + item.Code + ErrorStrings.Export_Validation_Sparepart_NotExistOrStop;
                                } else {
                                    item.ErrorMsg = item.ErrorMsg + ErrorStrings.Export_Validation_PartsBranch_Validation9 + item.Code + ErrorStrings.Export_Validation_Sparepart_NotExistOrStop;
                                }
                                errorList.Add(item);
                            }
                        }
                    }
                }
                //获取所有合格数据
                rightList = allList.Except(errorList).ToList();
                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempSparePart = list[index - 1];
                            var values = new object[] {
                                tempSparePart.CodeStr, tempSparePart.NextSubstituteStr,
                                tempSparePart.ErrorMsg
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }

                //导入所有合格数据
                if(!rightList.Any())
                    return true;
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    //开启事务，新增更新配件在一个事务内
                    var ts = conn.BeginTransaction();
                    try {
                        //新增配件
                        if(rightList.Any()) {
                            //获取新增数据的sql语句，Id为主键
                            var sqlUpdate = db.GetUpdateSql("SparePart", new string[] { "NextSubstitute", "ModifierId", "ModifierName", "ModifyTime", "OMSparePartMark" }, new string[] { "Code" });
                            var sqlInsertHistory = db.GetInsertSql("SparePartHistory", "Id", new[] {
                               "IsNotWarrantyTransfer", "SparePartId","Code","ReferenceCode", "Name", "CADCode", "Specification", "EnglishName", "PartType", "MeasureUnit", "ShelfLife", "LastSubstitute", "NextSubstitute", "Weight", "Feature", "status", "CreatorId", "CreatorName", "CreateTime","MInPackingAmount","ExchangeIdentification"
                            });

                            var userInfo = Utils.GetCurrentUserInfo();
                            //往数据库增加配件信息
                            foreach(SparePartExtend item in rightList) {
                                var command = db.CreateDbCommand(sqlUpdate, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("Code", Strings.StrConv(item.Code, VbStrConv.Narrow).Replace("【", "[")));
                                command.Parameters.Add(db.CreateDbParameter("NextSubstitute", item.NextSubstitute));
                                command.Parameters.Add(db.CreateDbParameter("ModifierId", userInfo.Id));
                                command.Parameters.Add(db.CreateDbParameter("ModifierName", userInfo.Name));
                                command.Parameters.Add(db.CreateDbParameter("ModifyTime", DateTime.Now));
                                command.Parameters.Add(db.CreateDbParameter("OMSparePartMark", 0));
                                command.ExecuteNonQuery();

                                #region 注释代码（无用）
                                //                                //获取数据库的完整备件信息
                                //                                string[] partCode = new string[] { item.Code };
                                //                                Func<string[], bool> getDbSparePartInfo = vales => {
                                //                                    item.Id = Convert.ToInt32(vales[0]);
                                //                                    //item.Code = vales[1];
                                //                                    item.ReferenceCode = vales[3];
                                //                                    item.Name = vales[2];
                                //                                    item.LastSubstitute = vales[4];
                                //                                    // item.NextSubstitute = vales[5];
                                //                                    item.ShelfLife = string.IsNullOrEmpty(vales[6]) ? 0 : Convert.ToInt32(vales[6]);
                                //                                    item.EnglishName = vales[7];
                                //                                    item.PinyinCode = vales[8];
                                //                                    item.ReferenceCode = vales[9];
                                //                                    item.ReferenceName = vales[10];
                                //                                    item.CADCode = vales[11];
                                //                                    item.CADName = vales[12];
                                //                                    item.PartType = string.IsNullOrEmpty(vales[13]) ? 0 : Convert.ToInt32(vales[13]);
                                //                                    item.Specification = vales[14];
                                //                                    item.Feature = vales[15];
                                //                                    item.Status = string.IsNullOrEmpty(vales[16]) ? 0 : Convert.ToInt32(vales[16]);
                                //                                    item.Length = string.IsNullOrEmpty(vales[17]) ? 0 : Convert.ToDecimal(vales[17]);
                                //                                    item.Width = string.IsNullOrEmpty(vales[18]) ? 0 : Convert.ToDecimal(vales[18]);
                                //                                    item.Height = string.IsNullOrEmpty(vales[19]) ? 0 : Convert.ToDecimal(vales[19]);
                                //                                    item.Volume = string.IsNullOrEmpty(vales[20]) ? 0 : Convert.ToDecimal(vales[20]);
                                //                                    item.Weight = string.IsNullOrEmpty(vales[21]) ? 0 : Convert.ToDecimal(vales[21]);
                                //                                    item.Material = vales[22];
                                //                                    item.PackingAmount = string.IsNullOrEmpty(vales[23]) ? 0 : Convert.ToInt32(vales[23]);
                                //                                    item.PackingSpecification = vales[24];
                                //                                    item.PartsOutPackingCode = vales[25];
                                //                                    item.PartsInPackingCode = vales[26];
                                //                                    item.MeasureUnit = vales[27];
                                //                                    item.MInPackingAmount = string.IsNullOrEmpty(vales[28]) ? 1 : Convert.ToInt32(vales[28]);
                                //                                    return false;
                                //                                };

                                //                                db.QueryDataWithInOperator(@"select ID,
                                //                                                                    CODE,
                                //                                                                    NAME,
                                //                                                                    ReferenceCode,
                                //                                                                    LASTSUBSTITUTE,
                                //                                                                    NEXTSUBSTITUTE,
                                //                                                                    SHELFLIFE,
                                //                                                                    ENGLISHNAME,
                                //                                                                    PINYINCODE,
                                //                                                                    REFERENCECODE,
                                //                                                                    REFERENCENAME,
                                //                                                                    CADCODE,
                                //                                                                    CADNAME,
                                //                                                                    PARTTYPE,
                                //                                                                    SPECIFICATION,
                                //                                                                    FEATURE,
                                //                                                                    STATUS,
                                //                                                                    LENGTH,
                                //                                                                    WIDTH,
                                //                                                                    HEIGHT,
                                //                                                                    VOLUME,
                                //                                                                    WEIGHT,
                                //                                                                    MATERIAL,
                                //                                                                    PACKINGAMOUNT,
                                //                                                                    PACKINGSPECIFICATION,
                                //                                                                    PARTSOUTPACKINGCODE,
                                //                                                                    PARTSINPACKINGCODE,
                                //                                                                    MEASUREUNIT,
                                //                                                                    MINPACKINGAMOUNT
                                //                                                                from sparepart where status=1 ", "Code", true, partCode, getDbSparePartInfo); 
                                #endregion
                                var sparePart = dbSparePartCodes.Where(r => r.Code == item.Code).FirstOrDefault();
                                if(sparePart != null) {
                                    //往数据库增加履历
                                    var commandHistory = db.CreateDbCommand(sqlInsertHistory, conn, ts);
                                    commandHistory.Parameters.Add(db.CreateDbParameter("SparePartId", sparePart.Id));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Code", Strings.StrConv(sparePart.Code, VbStrConv.Narrow).Replace("【", "[")));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Name", sparePart.Name));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("ReferenceCode", sparePart.ReferenceCode));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("IsNotWarrantyTransfer", sparePart.IsNotWarrantyTransfer));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("CADCode", sparePart.CADCode));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Specification", sparePart.Specification));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("EnglishName", sparePart.EnglishName));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("PartType", sparePart.PartType));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("MeasureUnit", sparePart.MeasureUnit));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("ShelfLife", sparePart.ShelfLife));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("LastSubstitute", sparePart.LastSubstitute));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("NextSubstitute", item.NextSubstitute));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Weight", sparePart.Weight));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Feature", sparePart.Feature));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Status", sparePart.Status));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("MInPackingAmount", sparePart.MInPackingAmount));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("ExchangeIdentification", sparePart.ExchangeIdentification));
                                    commandHistory.ExecuteNonQuery();
                                }
                            }

                        }
                        //无异常提交
                        ts.Commit();
                    } catch(Exception ex) {
                        //报错回滚
                        ts.Rollback();
                        throw new Exception(ex.Message);
                    } finally {
                        if(conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }

        public bool 批量替换产品商标(string fileName, out int excelImportNum, out List<SparePartExtend> rightData, out List<SparePartExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<SparePartExtend>();
            var rightList = new List<SparePartExtend>();
            var allList = new List<SparePartExtend>();
            var dbSparePartCodes = new List<SparePartExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("SparePart", out notNullableFields, out fieldLenght);
                List<object> excelColumns;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Code, "Code");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Sparepart_ProductBrand, "ProductBrand");
                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;
                    var keyValuePairs = new[] { 
                           new KeyValuePair<string,string>("Status","MasterData_Status")
                      };
                    tempExcelOperator.ImportHelper.LoadEnumFromDb(null, null, keyValuePairs);
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var sparePart = new SparePartExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        sparePart.CodeStr = Strings.StrConv(newRow["Code"], VbStrConv.Narrow).Replace("【", "[");
                        sparePart.CodeStr = strconvsparecode(sparePart.CodeStr);
                        sparePart.Code = Strings.StrConv(newRow["Code"], VbStrConv.Narrow).Replace("【", "[");
                        sparePart.Code = strconvsparecode(sparePart.Code);
                        sparePart.Code = sparePart.Code;//全部大写
                        sparePart.ProductBrand = newRow["ProductBrand"];
                        sparePart.ProductBrandStr = newRow["ProductBrand"];
                        var tempErrorMessage = new List<string>();
                        //配件编号检查
                        var fieldIndex = notNullableFields.IndexOf("CODE");
                        if(string.IsNullOrEmpty(sparePart.Code)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.OverstockPartsAppDetail__Validation1);
                        } else {
                            //配件长度不得超过25
                            if(Encoding.Default.GetByteCount(sparePart.Code) > 25)
                                tempErrorMessage.Add(ErrorStrings.ImpPartsSupplierRelation_Validation8);
                        }
                        //产品商标检查
                        if(sparePart.ProductBrand != null && Encoding.Default.GetByteCount(sparePart.ProductBrand) > 50)
                            tempErrorMessage.Add(ErrorStrings.Export_Validation_Sparepart_ProductBrandIsLong);
                        if(tempErrorMessage.Count > 0) {
                            sparePart.ErrorMsg = string.Join("; ", tempErrorMessage);
                        } else {
                            var tempStatus = tempExcelOperator.ImportHelper.GetEnumValue("Status", ErrorStrings.Export_Validation_PartsBranch_Used);
                            if(tempStatus.HasValue) {
                                sparePart.Status = tempStatus.Value;
                            }
                        }
                        allList.Add(sparePart);
                        return false;
                    });

                    //校验导入数据配件编号重复
                    var groups = allList.GroupBy(r =>
                        r.Code
                        ).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        if(groupItem.ErrorMsg == null) {
                            groupItem.ErrorMsg = ErrorStrings.Export_Title_PartsBranch_Code + groupItem.Code + ErrorStrings.Export_Validation_PartsBranch_Validation5;
                        } else {
                            groupItem.ErrorMsg = groupItem.ErrorMsg + ErrorStrings.Export_Validation_PartsBranch_Validation6 + groupItem.Code + ErrorStrings.Export_Validation_PartsBranch_Validation5;
                        }
                    }
                    var sparePartsNeedCheck = allList.ToList();
                    var partCodesNeedCheck = sparePartsNeedCheck.Select(r => r.Code).Distinct().ToArray();

                    Func<string[], bool> getDbSparePartCodes = vales => {
                        var dbSparePart = new SparePartExtend();
                        dbSparePart.Id = Convert.ToInt32(vales[0]);
                        dbSparePart.Code = vales[1];
                        dbSparePart.ReferenceCode = vales[3];
                        dbSparePart.Name = vales[2];
                        dbSparePart.LastSubstitute = vales[4];
                        dbSparePart.NextSubstitute = vales[5];
                        dbSparePart.ShelfLife = string.IsNullOrEmpty(vales[6]) ? 0 : Convert.ToInt32(vales[6]);
                        dbSparePart.EnglishName = vales[7];
                        dbSparePart.PinyinCode = vales[8];
                        dbSparePart.ReferenceCode = vales[9];
                        dbSparePart.ReferenceName = vales[10];
                        dbSparePart.CADCode = vales[11];
                        dbSparePart.CADName = vales[12];
                        dbSparePart.PartType = string.IsNullOrEmpty(vales[13]) ? 0 : Convert.ToInt32(vales[13]);
                        dbSparePart.Specification = vales[14];
                        dbSparePart.Feature = vales[15];
                        dbSparePart.Status = string.IsNullOrEmpty(vales[16]) ? 0 : Convert.ToInt32(vales[16]);
                        dbSparePart.Length = string.IsNullOrEmpty(vales[17]) ? 0 : Convert.ToDecimal(vales[17]);
                        dbSparePart.Width = string.IsNullOrEmpty(vales[18]) ? 0 : Convert.ToDecimal(vales[18]);
                        dbSparePart.Height = string.IsNullOrEmpty(vales[19]) ? 0 : Convert.ToDecimal(vales[19]);
                        dbSparePart.Volume = string.IsNullOrEmpty(vales[20]) ? 0 : Convert.ToDecimal(vales[20]);
                        dbSparePart.Weight = string.IsNullOrEmpty(vales[21]) ? 0 : Convert.ToDecimal(vales[21]);
                        dbSparePart.Material = vales[22];
                        dbSparePart.PackingAmount = string.IsNullOrEmpty(vales[23]) ? 0 : Convert.ToInt32(vales[23]);
                        dbSparePart.PackingSpecification = vales[24];
                        dbSparePart.PartsOutPackingCode = vales[25];
                        dbSparePart.PartsInPackingCode = vales[26];
                        dbSparePart.MeasureUnit = vales[27];
                        dbSparePart.MInPackingAmount = string.IsNullOrEmpty(vales[28]) ? 0 : Convert.ToInt32(vales[28]);
                        dbSparePart.IsNotWarrantyTransfer = string.IsNullOrEmpty(vales[29]) ? 0 : Convert.ToInt32(vales[29]);
                        dbSparePart.ExchangeIdentification = vales[30];
                        dbSparePartCodes.Add(dbSparePart);
                        return false;
                    };

                    //根据SQL查询数据库，将查出来的结果根据定义的委托中的方法（这里是getDbSparePartCodes）放到具体的集合中（这里是dbSparePartCodes）
                    db.QueryDataWithInOperator(@"select ID,
                                                                    CODE,
                                                                    NAME,
                                                                    ReferenceCode,
                                                                    LASTSUBSTITUTE,
                                                                    NEXTSUBSTITUTE,
                                                                    SHELFLIFE,
                                                                    ENGLISHNAME,
                                                                    PINYINCODE,
                                                                    REFERENCECODE,
                                                                    REFERENCENAME,
                                                                    CADCODE,
                                                                    CADNAME,
                                                                    PARTTYPE,
                                                                    SPECIFICATION,
                                                                    FEATURE,
                                                                    STATUS,
                                                                    LENGTH,
                                                                    WIDTH,
                                                                    HEIGHT,
                                                                    VOLUME,
                                                                    WEIGHT,
                                                                    MATERIAL,
                                                                    PACKINGAMOUNT,
                                                                    PACKINGSPECIFICATION,
                                                                    PARTSOUTPACKINGCODE,
                                                                    PARTSINPACKINGCODE,
                                                                    MEASUREUNIT,
                                                                    MINPACKINGAMOUNT,IsNotWarrantyTransfer,ExchangeIdentification
                                                                from sparepart where status=1 ", "Code", false, partCodesNeedCheck, getDbSparePartCodes);
                    if(partCodesNeedCheck.Length > 0) {
                        var sparePartsExistsCode = sparePartsNeedCheck.Where(r => dbSparePartCodes.Any(v => v.Code == r.Code)).ToArray();
                        if(sparePartsExistsCode.Count() < sparePartsNeedCheck.Count()) {
                            var notExist = allList.Except(sparePartsExistsCode);
                            foreach(var item in notExist) {
                                if(item.ErrorMsg == null) {
                                    item.ErrorMsg = ErrorStrings.Export_Validation_PartsBranch_Validation8 + item.Code + ErrorStrings.Export_Validation_Sparepart_NotExistOrStop;
                                } else {
                                    item.ErrorMsg = item.ErrorMsg + ErrorStrings.Export_Validation_PartsBranch_Validation9 + item.Code + ErrorStrings.Export_Validation_Sparepart_NotExistOrStop;
                                }
                                errorList.Add(item);
                            }
                        }
                    }
                }
                //获取所有不合格数据
                errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                //if(errList.Count > 0) {
                //    errorList.AddRange(errList);
                //}
                //获取所有合格数据
                rightList = allList.Except(errorList).ToList();

                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempSparePart = list[index - 1];
                            var values = new object[] {
                                tempSparePart.CodeStr, tempSparePart.NameStr,
                                tempSparePart.ErrorMsg
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }

                //导入所有合格数据
                if(!rightList.Any())
                    return true;
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    //开启事务，新增更新配件在应该事务内
                    var ts = conn.BeginTransaction();
                    try {
                        //新增配件
                        if(rightList.Any()) {
                            var sqlUpdate = db.GetUpdateSql("SparePart", new string[] { "ProductBrand", "ModifierId", "ModifierName", "ModifyTime", "OMSparePartMark" }, new string[] { "Code" });
                            var sqlInsertHistory = db.GetInsertSql("SparePartHistory", "Id", new[] {
                                 "IsNotWarrantyTransfer", "SparePartId","Code","ReferenceCode", "Name", "CADCode", "Specification", "EnglishName", "PartType", "MeasureUnit", "ShelfLife", "LastSubstitute", "NextSubstitute", "Weight", "Feature", "status", "CreatorId", "CreatorName", "CreateTime","MInPackingAmount","ExchangeIdentification","ProductBrand"
                                });

                            var userInfo = Utils.GetCurrentUserInfo();

                            //往数据库增加配件信息
                            foreach(SparePartExtend item in rightList) {
                                var command = db.CreateDbCommand(sqlUpdate, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("Code", Strings.StrConv(item.Code, VbStrConv.Narrow).Replace("【", "[")));
                                command.Parameters.Add(db.CreateDbParameter("ProductBrand", item.ProductBrand));
                                command.Parameters.Add(db.CreateDbParameter("ModifierId", userInfo.Id));
                                command.Parameters.Add(db.CreateDbParameter("ModifierName", userInfo.Name));
                                command.Parameters.Add(db.CreateDbParameter("ModifyTime", DateTime.Now));
                                command.Parameters.Add(db.CreateDbParameter("OMSparePartMark", 0));
                                command.ExecuteNonQuery();

                                //                                //获取数据库的完整备件信息
                                //                                string[] partCode = new string[] { item.Code };

                                //                                Func<string[], bool> getDbSparePartInfo = vales => {
                                //                                    item.Id = Convert.ToInt32(vales[0]);
                                //                                    //item.Code = vales[1];
                                //                                    item.ReferenceCode = vales[3];
                                //                                    item.Name = vales[2];
                                //                                    item.LastSubstitute = vales[4];
                                //                                    item.NextSubstitute = vales[5];
                                //                                    item.ShelfLife = string.IsNullOrEmpty(vales[6]) ? 0 : Convert.ToInt32(vales[6]);
                                //                                    item.EnglishName = vales[7];
                                //                                    item.PinyinCode = vales[8];
                                //                                    item.ReferenceCode = vales[9];
                                //                                    item.ReferenceName = vales[10];
                                //                                    item.CADCode = vales[11];
                                //                                    item.CADName = vales[12];
                                //                                    item.PartType = string.IsNullOrEmpty(vales[13]) ? 0 : Convert.ToInt32(vales[13]);
                                //                                    item.Specification = vales[14];
                                //                                    item.Feature = vales[15];
                                //                                    item.Status = string.IsNullOrEmpty(vales[16]) ? 0 : Convert.ToInt32(vales[16]);
                                //                                    item.Length = string.IsNullOrEmpty(vales[17]) ? 0 : Convert.ToDecimal(vales[17]);
                                //                                    item.Width = string.IsNullOrEmpty(vales[18]) ? 0 : Convert.ToDecimal(vales[18]);
                                //                                    item.Height = string.IsNullOrEmpty(vales[19]) ? 0 : Convert.ToDecimal(vales[19]);
                                //                                    item.Volume = string.IsNullOrEmpty(vales[20]) ? 0 : Convert.ToDecimal(vales[20]);
                                //                                    item.Weight = string.IsNullOrEmpty(vales[21]) ? 0 : Convert.ToDecimal(vales[21]);
                                //                                    item.Material = vales[22];
                                //                                    item.PackingAmount = string.IsNullOrEmpty(vales[23]) ? 0 : Convert.ToInt32(vales[23]);
                                //                                    item.PackingSpecification = vales[24];
                                //                                    item.PartsOutPackingCode = vales[25];
                                //                                    item.PartsInPackingCode = vales[26];
                                //                                    item.MeasureUnit = vales[27];
                                //                                    item.MInPackingAmount = string.IsNullOrEmpty(vales[28]) ? 0 : Convert.ToInt32(vales[28]);
                                //                                    return false;
                                //                                };

                                //                                db.QueryDataWithInOperator(@"select ID,
                                //                                                                    CODE,
                                //                                                                    NAME,
                                //                                                                    ReferenceCode,
                                //                                                                    LASTSUBSTITUTE,
                                //                                                                    NEXTSUBSTITUTE,
                                //                                                                    SHELFLIFE,
                                //                                                                    ENGLISHNAME,
                                //                                                                    PINYINCODE,
                                //                                                                    REFERENCECODE,
                                //                                                                    REFERENCENAME,
                                //                                                                    CADCODE,
                                //                                                                    CADNAME,
                                //                                                                    PARTTYPE,
                                //                                                                    SPECIFICATION,
                                //                                                                    FEATURE,
                                //                                                                    STATUS,
                                //                                                                    LENGTH,
                                //                                                                    WIDTH,
                                //                                                                    HEIGHT,
                                //                                                                    VOLUME,
                                //                                                                    WEIGHT,
                                //                                                                    MATERIAL,
                                //                                                                    PACKINGAMOUNT,
                                //                                                                    PACKINGSPECIFICATION,
                                //                                                                    PARTSOUTPACKINGCODE,
                                //                                                                    PARTSINPACKINGCODE,
                                //                                                                    MEASUREUNIT,
                                //                                                                    MINPACKINGAMOUNT
                                //                                                                from sparepart where status=1 ", "Code", true, partCode, getDbSparePartInfo);
                                //往数据库增加履历
                                var sparePart = dbSparePartCodes.Where(r => r.Code == item.Code).FirstOrDefault();
                                if(sparePart != null) {
                                    var commandHistory = db.CreateDbCommand(sqlInsertHistory, conn, ts);
                                    commandHistory.Parameters.Add(db.CreateDbParameter("SparePartId", sparePart.Id));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Code", Strings.StrConv(sparePart.Code, VbStrConv.Narrow).Replace("【", "[")));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Name", sparePart.Name));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("ReferenceCode", sparePart.ReferenceCode));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("CADCode", sparePart.CADCode));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Specification", sparePart.Specification));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("EnglishName", sparePart.EnglishName));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("IsNotWarrantyTransfer", sparePart.IsNotWarrantyTransfer));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("PartType", sparePart.PartType));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("MeasureUnit", sparePart.MeasureUnit));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("ShelfLife", sparePart.ShelfLife));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("LastSubstitute", sparePart.LastSubstitute));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("NextSubstitute", sparePart.NextSubstitute));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Weight", sparePart.Weight));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Feature", sparePart.Feature));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Status", sparePart.Status));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("MInPackingAmount", sparePart.MInPackingAmount));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("ExchangeIdentification", sparePart.ExchangeIdentification));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("ProductBrand", item.ProductBrand));
                                    commandHistory.ExecuteNonQuery();
                                }
                            }
                        }
                        //无异常提交
                        ts.Commit();
                    } catch(Exception ex) {
                        //报错回滚
                        ts.Rollback();
                        throw new Exception(ex.Message);
                    } finally {
                        if(conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                }

                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }

        public bool 批量替换标准名称(string fileName, out int excelImportNum, out List<SparePartExtend> rightData, out List<SparePartExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<SparePartExtend>();
            var rightList = new List<SparePartExtend>();
            var allList = new List<SparePartExtend>();
            var dbSparePartCodes = new List<SparePartExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("SparePart", out notNullableFields, out fieldLenght);
                List<object> excelColumns;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Code, "Code");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName, "Name");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Validation_Sparepart_SubstandardName, "SubstandardName");
                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;
                    var keyValuePairs = new[] {
                                new KeyValuePair<string, string>("Status","MasterData_Status") 
                            };
                    tempExcelOperator.ImportHelper.LoadEnumFromDb(null, null, keyValuePairs);
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var sparePart = new SparePartExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        sparePart.CodeStr = Strings.StrConv(newRow["Code"], VbStrConv.Narrow).Replace("【", "[");
                        sparePart.CodeStr = strconvsparecode(sparePart.CodeStr);
                        sparePart.Code = Strings.StrConv(newRow["Code"], VbStrConv.Narrow).Replace("【", "[");
                        sparePart.Code = strconvsparecode(sparePart.Code);
                        sparePart.Code = sparePart.Code;//全部大写
                        sparePart.Name = newRow["Name"];
                        sparePart.NameStr = newRow["Name"];
                        //sparePart.SubstandardName = newRow["SubstandardName"];
                        //sparePart.SubstandardNameStr = newRow["SubstandardName"];
                        sparePart.DeclareElement = newRow["SubstandardName"];
                        sparePart.DeclareElementStr = newRow["SubstandardName"];

                        var tempErrorMessage = new List<string>();
                        //配件编号检查
                        var fieldIndex = notNullableFields.IndexOf("CODE");
                        if(string.IsNullOrEmpty(sparePart.Code)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.OverstockPartsAppDetail__Validation1);
                        } else {
                            //配件长度不得超过25
                            if(Encoding.Default.GetByteCount(sparePart.Code) > 25)
                                tempErrorMessage.Add(ErrorStrings.ImpPartsSupplierRelation_Validation8);
                        }
                        //配件名称检查
                        fieldIndex = notNullableFields.IndexOf("NAME");
                        if(string.IsNullOrEmpty(sparePart.Name)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Sparepart_Name);
                        } else {
                            if(Encoding.Default.GetByteCount(sparePart.Name) > fieldLenght["NAME"])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Sparepart_NameIsLong);
                        }
                        //配件申报要素检查
                        if(sparePart.DeclareElement != null && Encoding.Default.GetByteCount(sparePart.DeclareElement) > 1000)
                            tempErrorMessage.Add(ErrorStrings.Export_Validation_Sparepart_SubstandardNameIsLong);

                        if(tempErrorMessage.Count > 0) {
                            sparePart.ErrorMsg = string.Join("; ", tempErrorMessage);
                        } else {
                            var tempStatus = tempExcelOperator.ImportHelper.GetEnumValue("Status", ErrorStrings.Export_Validation_PartsBranch_Used);
                            if(tempStatus.HasValue) {
                                sparePart.Status = tempStatus.Value;
                            }
                        }
                        allList.Add(sparePart);
                        return false;
                    });
                    //校验导入的数据配件编号重复
                    var groups = allList.GroupBy(r =>
                        r.Code
                    ).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        if(groupItem.ErrorMsg == null) {
                            groupItem.ErrorMsg = ErrorStrings.Export_Title_PartsBranch_Code + groupItem.Code + ErrorStrings.Export_Validation_PartsBranch_Validation5;
                        } else {
                            groupItem.ErrorMsg = groupItem.ErrorMsg + ErrorStrings.Export_Validation_PartsBranch_Validation6 + groupItem.Code + ErrorStrings.Export_Validation_PartsBranch_Validation5;
                        }
                    }
                    var sparePartsNeedCheck = allList.ToList();
                    var partCodesNeedCheck = sparePartsNeedCheck.Select(r => r.Code).Distinct().ToArray();

                    Func<string[], bool> getDbSparePartCodes = vales => {
                        var dbSparePart = new SparePartExtend();
                        dbSparePart.Id = Convert.ToInt32(vales[0]);
                        dbSparePart.Code = vales[1];
                        dbSparePart.ReferenceCode = vales[3];
                        dbSparePart.Name = vales[2];
                        dbSparePart.LastSubstitute = vales[4];
                        dbSparePart.NextSubstitute = vales[5];
                        dbSparePart.ShelfLife = string.IsNullOrEmpty(vales[6]) ? 0 : Convert.ToInt32(vales[6]);
                        dbSparePart.EnglishName = vales[7];
                        dbSparePart.PinyinCode = vales[8];
                        dbSparePart.ReferenceCode = vales[9];
                        dbSparePart.ReferenceName = vales[10];
                        dbSparePart.CADCode = vales[11];
                        dbSparePart.CADName = vales[12];
                        dbSparePart.PartType = string.IsNullOrEmpty(vales[13]) ? 0 : Convert.ToInt32(vales[13]);
                        dbSparePart.Specification = vales[14];
                        dbSparePart.Feature = vales[15];
                        dbSparePart.Status = string.IsNullOrEmpty(vales[16]) ? 0 : Convert.ToInt32(vales[16]);
                        dbSparePart.Length = string.IsNullOrEmpty(vales[17]) ? 0 : Convert.ToDecimal(vales[17]);
                        dbSparePart.Width = string.IsNullOrEmpty(vales[18]) ? 0 : Convert.ToDecimal(vales[18]);
                        dbSparePart.Height = string.IsNullOrEmpty(vales[19]) ? 0 : Convert.ToDecimal(vales[19]);
                        dbSparePart.Volume = string.IsNullOrEmpty(vales[20]) ? 0 : Convert.ToDecimal(vales[20]);
                        dbSparePart.Weight = string.IsNullOrEmpty(vales[21]) ? 0 : Convert.ToDecimal(vales[21]);
                        dbSparePart.Material = vales[22];
                        dbSparePart.PackingAmount = string.IsNullOrEmpty(vales[23]) ? 0 : Convert.ToInt32(vales[23]);
                        dbSparePart.PackingSpecification = vales[24];
                        dbSparePart.PartsOutPackingCode = vales[25];
                        dbSparePart.PartsInPackingCode = vales[26];
                        dbSparePart.MeasureUnit = vales[27];
                        dbSparePart.MInPackingAmount = string.IsNullOrEmpty(vales[28]) ? 0 : Convert.ToInt32(vales[28]);
                        dbSparePart.IsNotWarrantyTransfer = string.IsNullOrEmpty(vales[29]) ? 0 : Convert.ToInt32(vales[29]);
                        dbSparePart.ExchangeIdentification = vales[30];
                        dbSparePartCodes.Add(dbSparePart);
                        return false;
                    };
                    //根据SQL查询数据库，将查出来的结果根据定义的委托中的方法（这里是getDbSparePartCodes）放到具体的集合中（这里是dbSparePartCodes）
                    db.QueryDataWithInOperator(@"select ID,
                                                                    CODE,
                                                                    NAME,
                                                                    ReferenceCode,
                                                                    LASTSUBSTITUTE,
                                                                    NEXTSUBSTITUTE,
                                                                    SHELFLIFE,
                                                                    ENGLISHNAME,
                                                                    PINYINCODE,
                                                                    REFERENCECODE,
                                                                    REFERENCENAME,
                                                                    CADCODE,
                                                                    CADNAME,
                                                                    PARTTYPE,
                                                                    SPECIFICATION,
                                                                    FEATURE,
                                                                    STATUS,
                                                                    LENGTH,
                                                                    WIDTH,
                                                                    HEIGHT,
                                                                    VOLUME,
                                                                    WEIGHT,
                                                                    MATERIAL,
                                                                    PACKINGAMOUNT,
                                                                    PACKINGSPECIFICATION,
                                                                    PARTSOUTPACKINGCODE,
                                                                    PARTSINPACKINGCODE,
                                                                    MEASUREUNIT,
                                                                    MINPACKINGAMOUNT,IsNotWarrantyTransfer,ExchangeIdentification
                                                                from sparepart where status=1 ", "Code", false, partCodesNeedCheck, getDbSparePartCodes);
                    if(partCodesNeedCheck.Length > 0) {
                        var sparePartsExistsCode = sparePartsNeedCheck.Where(r => dbSparePartCodes.Any(v => v.Code == r.Code)).ToArray();
                        if(sparePartsExistsCode.Count() < sparePartsNeedCheck.Count()) {
                            var notExist = allList.Except(sparePartsExistsCode);
                            foreach(var item in notExist) {
                                if(item.ErrorMsg == null) {
                                    item.ErrorMsg = ErrorStrings.Export_Validation_PartsBranch_Validation8 + item.Code + ErrorStrings.Export_Validation_Sparepart_NotExistOrStop;
                                } else {
                                    item.ErrorMsg = item.ErrorMsg + ErrorStrings.Export_Validation_PartsBranch_Validation9 + item.Code + ErrorStrings.Export_Validation_Sparepart_NotExistOrStop;
                                }
                                errorList.Add(item);
                            }
                        }
                    }
                }
                //获取所有不合格数据
                errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                //if(errList.Count > 0) {
                //    errorList.AddRange(errList);
                //}
                //获取所有合格数据
                rightList = allList.Except(errorList).ToList();

                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempSparePart = list[index - 1];
                            var values = new object[] {
                                tempSparePart.CodeStr, tempSparePart.NameStr,tempSparePart.SubstandardNameStr,
                                tempSparePart.ErrorMsg
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }

                //导入所有合格数据
                if(!rightList.Any())
                    return true;
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    //开启事务，新增更新配件在一个事务内
                    var ts = conn.BeginTransaction();
                    try {
                        //新增配件
                        if(rightList.Any()) {
                            //获取新增数据的sql语句，Id为主键
                            var sqlUpdate = db.GetUpdateSql("SparePart", new string[] { "DeclareElement", "ModifierId", "ModifierName", "ModifyTime", "OMSparePartMark" }, new string[] { "Code" });
                            var sqlInsertHistory = db.GetInsertSql("SparePartHistory", "Id", new[] {
                              "IsNotWarrantyTransfer",  "SparePartId","Code","ReferenceCode", "Name", "CADCode", "Specification", "EnglishName", "PartType", "MeasureUnit", "ShelfLife", "LastSubstitute", "NextSubstitute", "Weight", "Feature", "status", "CreatorId", "CreatorName", "CreateTime","MInPackingAmount","ExchangeIdentification",
                                "DeclareElement"
                            });

                            var userInfo = Utils.GetCurrentUserInfo();
                            //往数据库增加配件信息
                            foreach(SparePartExtend item in rightList) {
                                var command = db.CreateDbCommand(sqlUpdate, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("Code", Strings.StrConv(item.Code, VbStrConv.Narrow).Replace("【", "[")));
                                command.Parameters.Add(db.CreateDbParameter("DeclareElement", item.DeclareElement));
                                command.Parameters.Add(db.CreateDbParameter("ModifierId", userInfo.Id));
                                command.Parameters.Add(db.CreateDbParameter("ModifierName", userInfo.Name));
                                command.Parameters.Add(db.CreateDbParameter("ModifyTime", DateTime.Now));
                                command.Parameters.Add(db.CreateDbParameter("OMSparePartMark", 0));
                                command.ExecuteNonQuery();

                                //往数据库增加履历
                                var sparePart = dbSparePartCodes.Where(r => r.Code == item.Code).FirstOrDefault();
                                if(sparePart != null) {
                                    var commandHistory = db.CreateDbCommand(sqlInsertHistory, conn, ts);
                                    commandHistory.Parameters.Add(db.CreateDbParameter("SparePartId", sparePart.Id));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Code", Strings.StrConv(sparePart.Code, VbStrConv.Narrow).Replace("【", "[")));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Name", sparePart.Name));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("IsNotWarrantyTransfer", sparePart.IsNotWarrantyTransfer));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("ReferenceCode", sparePart.ReferenceCode));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("CADCode", sparePart.CADCode));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Specification", sparePart.Specification));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("EnglishName", sparePart.EnglishName));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("PartType", sparePart.PartType));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("MeasureUnit", sparePart.MeasureUnit));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("ShelfLife", sparePart.ShelfLife));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("LastSubstitute", sparePart.LastSubstitute));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("NextSubstitute", sparePart.NextSubstitute));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Weight", sparePart.Weight));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Feature", sparePart.Feature));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Status", sparePart.Status));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("MInPackingAmount", sparePart.MInPackingAmount));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("ExchangeIdentification", sparePart.ExchangeIdentification));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("DeclareElement", item.DeclareElement));
                                    commandHistory.ExecuteNonQuery();
                                }
                            }

                        }
                        //无异常提交
                        ts.Commit();
                    } catch(Exception ex) {
                        //报错回滚
                        ts.Rollback();
                        throw new Exception(ex.Message);
                    } finally {
                        if(conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }

        public bool 批量替换产品执行标准代码(string fileName, out int excelImportNum, out List<SparePartExtend> rightData, out List<SparePartExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<SparePartExtend>();
            var rightList = new List<SparePartExtend>();
            var allList = new List<SparePartExtend>();
            var dbSparePartCodes = new List<SparePartExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("SparePart", out notNullableFields, out fieldLenght);
                List<object> excelColumns;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Code, "Code");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName, "Name");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Sparepart_StandardCode, "StandardCode");
                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;
                    var keyValuePairs = new[] {
                                new KeyValuePair<string, string>("Status","MasterData_Status") 
                            };
                    tempExcelOperator.ImportHelper.LoadEnumFromDb(null, null, keyValuePairs);
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var sparePart = new SparePartExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);

                        sparePart.Code = row["Code"];//全部大写
                        sparePart.Name = row["Name"];
                        sparePart.NameStr = row["Name"];
                        sparePart.StandardCode = row["StandardCode"];

                        var tempErrorMessage = new List<string>();
                        //配件编号检查
                        var fieldIndex = notNullableFields.IndexOf("CODE");
                        if(string.IsNullOrEmpty(sparePart.Code)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.OverstockPartsAppDetail__Validation1);
                        } else {
                            //配件长度不得超过25
                            if(Encoding.Default.GetByteCount(sparePart.Code) > 25)
                                tempErrorMessage.Add(ErrorStrings.ImpPartsSupplierRelation_Validation8);
                        }
                        //配件标准名称检查
                        //if(string.IsNullOrEmpty(sparePart.StandardCode))
                        //    tempErrorMessage.Add("产品执行标准代码不能为空");
                        if(sparePart.StandardCode != null && Encoding.Default.GetByteCount(sparePart.StandardCode) > 100)
                            tempErrorMessage.Add(ErrorStrings.Export_Validation_Sparepart_StandardCodeIsLong);

                        if(tempErrorMessage.Count > 0) {
                            sparePart.ErrorMsg = string.Join("; ", tempErrorMessage);
                        } else {
                            var tempStatus = tempExcelOperator.ImportHelper.GetEnumValue("Status", ErrorStrings.Export_Validation_PartsBranch_Used);
                            if(tempStatus.HasValue) {
                                sparePart.Status = tempStatus.Value;
                            }
                        }
                        allList.Add(sparePart);
                        return false;
                    });
                    //校验导入的数据配件编号重复
                    var groups = allList.GroupBy(r =>
                        r.Code
                    ).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        if(groupItem.ErrorMsg == null) {
                            groupItem.ErrorMsg = ErrorStrings.Export_Title_PartsBranch_Code + groupItem.Code + ErrorStrings.Export_Validation_PartsBranch_Validation5;
                        } else {
                            groupItem.ErrorMsg = groupItem.ErrorMsg + ErrorStrings.Export_Validation_PartsBranch_Validation6 + groupItem.Code + ErrorStrings.Export_Validation_PartsBranch_Validation5;
                        }
                    }
                    var sparePartsNeedCheck = allList.ToList();
                    var partCodesNeedCheck = sparePartsNeedCheck.Select(r => r.Code).Distinct().ToArray();
                    var standardCodes = allList.Where(l => !string.IsNullOrEmpty(l.StandardCode)).Select(v => v.StandardCode).ToArray();
                    var standardCode = string.Join("','", standardCodes);

                    Func<string[], bool> getDbSparePartCodes = vales => {
                        var dbSparePart = new SparePartExtend();
                        dbSparePart.Id = Convert.ToInt32(vales[0]);
                        dbSparePart.Code = vales[1];
                        dbSparePart.ReferenceCode = vales[3];
                        dbSparePart.Name = vales[2];
                        dbSparePart.LastSubstitute = vales[4];
                        dbSparePart.NextSubstitute = vales[5];
                        dbSparePart.ShelfLife = string.IsNullOrEmpty(vales[6]) ? 0 : Convert.ToInt32(vales[6]);
                        dbSparePart.EnglishName = vales[7];
                        dbSparePart.PinyinCode = vales[8];
                        dbSparePart.ReferenceCode = vales[9];
                        dbSparePart.ReferenceName = vales[10];
                        dbSparePart.CADCode = vales[11];
                        dbSparePart.CADName = vales[12];
                        dbSparePart.PartType = string.IsNullOrEmpty(vales[13]) ? 0 : Convert.ToInt32(vales[13]);
                        dbSparePart.Specification = vales[14];
                        dbSparePart.Feature = vales[15];
                        dbSparePart.Status = string.IsNullOrEmpty(vales[16]) ? 0 : Convert.ToInt32(vales[16]);
                        dbSparePart.Length = string.IsNullOrEmpty(vales[17]) ? 0 : Convert.ToDecimal(vales[17]);
                        dbSparePart.Width = string.IsNullOrEmpty(vales[18]) ? 0 : Convert.ToDecimal(vales[18]);
                        dbSparePart.Height = string.IsNullOrEmpty(vales[19]) ? 0 : Convert.ToDecimal(vales[19]);
                        dbSparePart.Volume = string.IsNullOrEmpty(vales[20]) ? 0 : Convert.ToDecimal(vales[20]);
                        dbSparePart.Weight = string.IsNullOrEmpty(vales[21]) ? 0 : Convert.ToDecimal(vales[21]);
                        dbSparePart.Material = vales[22];
                        dbSparePart.PackingAmount = string.IsNullOrEmpty(vales[23]) ? 0 : Convert.ToInt32(vales[23]);
                        dbSparePart.PackingSpecification = vales[24];
                        dbSparePart.PartsOutPackingCode = vales[25];
                        dbSparePart.PartsInPackingCode = vales[26];
                        dbSparePart.MeasureUnit = vales[27];
                        dbSparePart.MInPackingAmount = string.IsNullOrEmpty(vales[28]) ? 0 : Convert.ToInt32(vales[28]);
                        dbSparePart.StandardCode = vales[29] as string;
                        dbSparePart.StandardName = vales[30] as string;
                        dbSparePart.IsNotWarrantyTransfer = string.IsNullOrEmpty(vales[31]) ? 0 : Convert.ToInt32(vales[31]);
                        dbSparePart.ExchangeIdentification = vales[32] as string;
                        dbSparePartCodes.Add(dbSparePart);
                        return false;
                    };
                    //根据SQL查询数据库，将查出来的结果根据定义的委托中的方法（这里是getDbSparePartCodes）放到具体的集合中（这里是dbSparePartCodes）
                    db.QueryDataWithInOperator(@"select ID,
                                                                    CODE,
                                                                    NAME,
                                                                    ReferenceCode,
                                                                    LASTSUBSTITUTE,
                                                                    NEXTSUBSTITUTE,
                                                                    SHELFLIFE,
                                                                    ENGLISHNAME,
                                                                    PINYINCODE,
                                                                    REFERENCECODE,
                                                                    REFERENCENAME,
                                                                    CADCODE,
                                                                    CADNAME,
                                                                    PARTTYPE,
                                                                    SPECIFICATION,
                                                                    FEATURE,
                                                                    STATUS,
                                                                    LENGTH,
                                                                    WIDTH,
                                                                    HEIGHT,
                                                                    VOLUME,
                                                                    WEIGHT,
                                                                    MATERIAL,
                                                                    PACKINGAMOUNT,
                                                                    PACKINGSPECIFICATION,
                                                                    PARTSOUTPACKINGCODE,
                                                                    PARTSINPACKINGCODE,
                                                                    MEASUREUNIT,
                                                                    MINPACKINGAMOUNT,
                                                                    STANDARDCODE,
                                                                    STANDARDNAME,IsNotWarrantyTransfer,ExchangeIdentification
                                                                from sparepart where status=1 ", "Code", false, partCodesNeedCheck, getDbSparePartCodes);
                    if(partCodesNeedCheck.Length > 0) {
                        var sparePartsExistsCode = sparePartsNeedCheck.Where(r => dbSparePartCodes.Any(v => v.Code == r.Code)).ToArray();
                        if(sparePartsExistsCode.Count() < sparePartsNeedCheck.Count()) {
                            var notExist = allList.Except(sparePartsExistsCode);
                            foreach(var item in notExist) {
                                if(item.ErrorMsg == null) {
                                    item.ErrorMsg = ErrorStrings.Export_Validation_PartsBranch_Validation8 + item.Code + ErrorStrings.Export_Validation_Sparepart_NotExistOrStop;
                                } else {
                                    item.ErrorMsg = item.ErrorMsg + ErrorStrings.Export_Validation_PartsBranch_Validation9 + item.Code + ErrorStrings.Export_Validation_Sparepart_NotExistOrStop;
                                }
                                errorList.Add(item);
                            }
                        }
                    }
                    using(var conn = db.CreateDbConnection()) {
                        conn.Open();
                        List<SparePartExtend> s = new List<SparePartExtend>();
                        var sql = string.Format(@" select StandardCode,StandardName, Id from ProductStandard where StandardCode in ('{0}')", standardCode);
                        var commandsc = db.CreateDbCommand(sql, conn, null);
                        var StandardCode = commandsc.ExecuteReader();

                        while(StandardCode.Read()) {
                            s.Add(new SparePartExtend {
                                StandardCode = StandardCode[0].ToString(),
                                StandardName = StandardCode[1].ToString(),
                            });
                        }


                        foreach(var item in allList.Where(r => r.ErrorMsg == null)) {
                            if(!string.IsNullOrEmpty(item.StandardCode)) {
                                var standard = s.FirstOrDefault(v => v.StandardCode == item.StandardCode);
                                if(standard != null) {
                                    item.StandardName = standard.StandardName;
                                } else {
                                    if(item.ErrorMsg == null) {
                                        item.ErrorMsg = ErrorStrings.Export_Validation_Sparepart_StandardCodeNotMatch;
                                    } else {
                                        item.ErrorMsg = item.ErrorMsg + ";" + ErrorStrings.Export_Validation_Sparepart_StandardCodeNotMatch;
                                    }
                                }
                            }
                        }
                    }


                }
                //获取所有不合格数据
                errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                //if(errList.Count > 0) {
                //    errorList.AddRange(errList);
                //}
                //获取所有合格数据
                rightList = allList.Except(errorList).ToList();

                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempSparePart = list[index - 1];
                            var values = new object[] {
                                tempSparePart.CodeStr, tempSparePart.NameStr,tempSparePart.SubstandardNameStr,
                                tempSparePart.ErrorMsg
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }

                //导入所有合格数据
                if(!rightList.Any())
                    return true;
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    //开启事务，新增更新配件在一个事务内
                    var ts = conn.BeginTransaction();
                    try {
                        //新增配件
                        if(rightList.Any()) {
                            //获取新增数据的sql语句，Id为主键
                            var sqlUpdate = db.GetUpdateSql("SparePart", new string[] { "StandardCode", "StandardName", "ModifierId", "ModifierName", "ModifyTime", "OMSparePartMark" }, new string[] { "Code" });
                            var sqlInsertHistory = db.GetInsertSql("SparePartHistory", "Id", new[] {
                              "IsNotWarrantyTransfer",  "SparePartId","Code","ReferenceCode", "Name", "CADCode", "Specification", "EnglishName", "PartType", "MeasureUnit", "ShelfLife", "LastSubstitute", "NextSubstitute", "Weight", "Feature", "status", "CreatorId", "CreatorName", "CreateTime","MInPackingAmount","StandardCode","StandardName","ExchangeIdentification"
                            });

                            var userInfo = Utils.GetCurrentUserInfo();
                            //往数据库增加配件信息
                            foreach(SparePartExtend item in rightList) {

                                var command = db.CreateDbCommand(sqlUpdate, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("Code", Strings.StrConv(item.Code, VbStrConv.Narrow).Replace("【", "[")));
                                command.Parameters.Add(db.CreateDbParameter("StandardCode", string.IsNullOrEmpty(item.StandardCode) ? "" : item.StandardCode));
                                command.Parameters.Add(db.CreateDbParameter("StandardName", string.IsNullOrEmpty(item.StandardName) ? "" : item.StandardName));
                                command.Parameters.Add(db.CreateDbParameter("ModifierId", userInfo.Id));
                                command.Parameters.Add(db.CreateDbParameter("ModifierName", userInfo.Name));
                                command.Parameters.Add(db.CreateDbParameter("ModifyTime", DateTime.Now));
                                command.Parameters.Add(db.CreateDbParameter("OMSparePartMark", 0));
                                command.ExecuteNonQuery();


                                //往数据库增加履历
                                var sparePart = dbSparePartCodes.Where(r => r.Code == item.Code).FirstOrDefault();
                                if(sparePart != null) {
                                    var commandHistory = db.CreateDbCommand(sqlInsertHistory, conn, ts);
                                    commandHistory.Parameters.Add(db.CreateDbParameter("SparePartId", sparePart.Id));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Code", Strings.StrConv(sparePart.Code, VbStrConv.Narrow).Replace("【", "[")));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Name", sparePart.Name));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("IsNotWarrantyTransfer", sparePart.IsNotWarrantyTransfer));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("ReferenceCode", sparePart.ReferenceCode));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("CADCode", sparePart.CADCode));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Specification", sparePart.Specification));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("EnglishName", sparePart.EnglishName));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("PartType", sparePart.PartType));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("MeasureUnit", sparePart.MeasureUnit));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("ShelfLife", sparePart.ShelfLife));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("LastSubstitute", sparePart.LastSubstitute));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("NextSubstitute", sparePart.NextSubstitute));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Weight", sparePart.Weight));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Feature", sparePart.Feature));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Status", sparePart.Status));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("MInPackingAmount", sparePart.MInPackingAmount));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("ExchangeIdentification", sparePart.ExchangeIdentification));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("StandardCode", string.IsNullOrEmpty(item.StandardCode) ? "" : item.StandardCode));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("StandardName", string.IsNullOrEmpty(item.StandardName) ? "" : item.StandardName));
                                    commandHistory.ExecuteNonQuery();
                                }
                            }

                        }
                        //无异常提交
                        ts.Commit();
                    } catch(Exception ex) {
                        //报错回滚
                        ts.Rollback();
                        throw new Exception(ex.Message);
                    } finally {
                        if(conn.State == ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }

        public bool 批量导入修改配件基本信息(string fileName, out int excelImportNum, out List<SparePartExtend> rightData, out List<SparePartExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<SparePartExtend>();
            var rightList = new List<SparePartExtend>();
            var allList = new List<SparePartExtend>();
            var oldspareParts = new List<SparePartExtend>();
            var dbpartsExchangeGroups = new List<PartsExchangeGroupExtend>();
            var dbpartsExchanges = new List<PartsExchangeGroupExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                List<object> excelColumns;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Code, "Code");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName, "Name");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_ReferenceCode, "ReferenceCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsExchangeGroupExtend_ExchangeCode, "ExchangeIdentification");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Sparepart_OverseasPartsCode, "OverseasPartsFigure");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Sparepart_ProductBrand, "ProductBrand");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Sparepart_IMSCompressionNumber, "IMSCompressionNumber");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Sparepart_IMSManufacturerNumber, "IMSManufacturerNumber");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Sparepart_CADCode, "CADCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Sparepart_Specification, "Specification");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_EnglishName, "EnglishName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Sparepart_PartsType, "PartType");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PackingPropertyApp_MeasureUnit, "MeasureUnit");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Sparepart_MInPackingAmount, "MInPackingAmount");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Sparepart_ShelfLife, "ShelfLife");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Sparepart_GroupABCCategory, "GroupABCCategory");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Sparepart_LastSubstitute, "LastSubstitute");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Sparepart_NextSubstitute, "NextSubstitute");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PackingPropertyApp_Weight, "Weight");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Sparepart_Volume, "Volume");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Feature, "Feature");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Sparepart_TotalNumber, "TotalNumber");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Sparepart_FacturyNew, "Factury");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Sparepart_IsOriginal, "IsOriginal");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Sparepart_CategoryCode, "CategoryCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Sparepart_CategoryName, "CategoryName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Sparepart_StandardCode, "StandardCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Sparepart_StandardName, "StandardName");
                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;

                    //单次最大导入条数不能大于300
                    if(excelOperator.LastRowNum > 2000) {
                        throw new Exception(ErrorStrings.Export_Validation_Sparepart_Item);
                    }

                    var keyValuePairs = new[] {
                                new KeyValuePair<string, string>("PartType", "SparePart_PartType"),
                                new KeyValuePair<string, string>("MeasureUnit", "SparePart_MeasureUnit"),
                                new KeyValuePair<string, string>("Status","MasterData_Status"),
                                new KeyValuePair<string, string>("IsOriginal","IsOrNot") 
                            };
                    tempExcelOperator.ImportHelper.LoadEnumFromDb(null, null, keyValuePairs);

                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var tempImportObj = new SparePartExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);

                        tempImportObj.Code = newRow["Code"];
                        tempImportObj.Name = newRow["Name"];
                        tempImportObj.ReferenceCode = newRow["ReferenceCode"];
                        tempImportObj.OverseasPartsFigure = newRow["OverseasPartsFigure"];
                        tempImportObj.ProductBrand = newRow["ProductBrand"];
                        tempImportObj.IMSCompressionNumber = newRow["IMSCompressionNumber"];
                        tempImportObj.IMSManufacturerNumber = newRow["IMSManufacturerNumber"];
                        tempImportObj.CADCode = newRow["CADCode"];
                        tempImportObj.Specification = newRow["Specification"];
                        tempImportObj.EnglishName = newRow["EnglishName"];
                        tempImportObj.PartTypeStr = newRow["PartType"];
                        tempImportObj.MeasureUnit = newRow["MeasureUnit"];
                        tempImportObj.MInPackingAmountStr = newRow["MInPackingAmount"];
                        tempImportObj.ShelfLifeStr = newRow["ShelfLife"];
                        tempImportObj.GroupABCCategoryStr = newRow["GroupABCCategory"];
                        tempImportObj.LastSubstitute = newRow["LastSubstitute"];
                        tempImportObj.NextSubstitute = newRow["NextSubstitute"];
                        tempImportObj.WeightStr = newRow["Weight"];
                        tempImportObj.VolumeStr = newRow["Volume"];
                        tempImportObj.Feature = newRow["Feature"];
                        tempImportObj.TotalNumber = newRow["TotalNumber"];
                        tempImportObj.Factury = newRow["Factury"];
                        tempImportObj.IsOriginalStr = newRow["IsOriginal"];
                        tempImportObj.CategoryCode = newRow["CategoryCode"];
                        tempImportObj.CategoryName = newRow["CategoryName"];
                        tempImportObj.StandardCode = newRow["StandardCode"];
                        tempImportObj.StandardName = newRow["StandardName"];
                        tempImportObj.ExchangeIdentification = newRow["ExchangeIdentification"];
                        var tempErrorMessage = new List<string>();

                        //导入模板必填
                        if(string.IsNullOrEmpty(tempImportObj.Code)) {
                            tempErrorMessage.Add(ErrorStrings.Export_Validation_Sparepart_CodeIsNull);
                        }
                        //if(string.IsNullOrEmpty(tempImportObj.StandardCode)) {
                        //    tempErrorMessage.Add("产品执行标准代码不能为空");
                        //}
                        if(!string.IsNullOrEmpty(tempImportObj.Code)) {
                            if(Encoding.Default.GetByteCount(tempImportObj.Code) > 50) {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Sparepart_CodeIsLong);
                            }
                        }

                        if(!string.IsNullOrEmpty(tempImportObj.OverseasPartsFigure)) {
                            if(Encoding.Default.GetByteCount(tempImportObj.OverseasPartsFigure) > 18) {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Sparepart_OverseasPartsFigureIsLong);
                            }
                        }
                        //配件类型检查
                        if(!string.IsNullOrEmpty(row["PartType"])) {
                            var partType = tempExcelOperator.ImportHelper.GetEnumValue("PartType", row["PartType"]);
                            if(!partType.HasValue) {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Sparepart_PartTypeValueError);
                            } else {
                                tempImportObj.PartType = partType.Value;
                            }
                        }
                        //是否原厂件

                        if(!string.IsNullOrEmpty(row["IsOriginal"])) {
                            var isOriginal = tempExcelOperator.ImportHelper.GetEnumValue("IsOriginal", row["IsOriginal"]);
                            if(!isOriginal.HasValue) {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Sparepart_IsOriginalValueError);
                            } else {
                                tempImportObj.IsOriginal = Convert.ToBoolean(isOriginal.Value);
                            }
                        }
                        if(tempErrorMessage.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });
                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    var groups = tempRightList.GroupBy(r => new {
                        r.Code
                    }).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        groupItem.ErrorMsg = ErrorStrings.ImpPartsSupplierRelation_Validation31;
                    }
                    var sparePartsNeedCheck = allList.ToList();
                    var partCodesNeedCheck = sparePartsNeedCheck.Select(r => r.Code).Distinct().ToArray();
                    var dbSparePartCodes = new List<SparePartExtend>();
                    Func<string[], bool> getDbSparePartCodes = vales => {
                        var dbSparePart = new SparePartExtend();
                        dbSparePart.Id = Convert.ToInt32(vales[0]);
                        dbSparePart.Code = vales[1];
                        dbSparePart.ReferenceCode = vales[3];
                        dbSparePart.Name = vales[2];
                        dbSparePart.LastSubstitute = vales[4];
                        dbSparePart.NextSubstitute = vales[5];
                        dbSparePart.ShelfLife = string.IsNullOrEmpty(vales[6]) ? 0 : Convert.ToInt32(vales[6]);
                        dbSparePart.EnglishName = vales[7];
                        dbSparePart.PinyinCode = vales[8];
                        dbSparePart.ReferenceName = vales[10];
                        dbSparePart.CADCode = vales[11];
                        dbSparePart.CADName = vales[12];
                        dbSparePart.PartType = string.IsNullOrEmpty(vales[13]) ? 0 : Convert.ToInt32(vales[13]);
                        dbSparePart.Specification = vales[14];
                        dbSparePart.Feature = vales[15];
                        dbSparePart.Status = string.IsNullOrEmpty(vales[16]) ? 0 : Convert.ToInt32(vales[16]);
                        dbSparePart.Length = string.IsNullOrEmpty(vales[17]) ? 0 : Convert.ToDecimal(vales[17]);
                        dbSparePart.Width = string.IsNullOrEmpty(vales[18]) ? 0 : Convert.ToDecimal(vales[18]);
                        dbSparePart.Height = string.IsNullOrEmpty(vales[19]) ? 0 : Convert.ToDecimal(vales[19]);
                        dbSparePart.Volume = string.IsNullOrEmpty(vales[20]) ? 0 : Convert.ToDecimal(vales[20]);
                        dbSparePart.Weight = string.IsNullOrEmpty(vales[21]) ? 0 : Convert.ToDecimal(vales[21]);
                        dbSparePart.Material = vales[22];
                        dbSparePart.PackingAmount = string.IsNullOrEmpty(vales[23]) ? 0 : Convert.ToInt32(vales[23]);
                        dbSparePart.PackingSpecification = vales[24];
                        dbSparePart.PartsOutPackingCode = vales[25];
                        dbSparePart.PartsInPackingCode = vales[26];
                        dbSparePart.MeasureUnit = vales[27];
                        dbSparePart.MInPackingAmount = string.IsNullOrEmpty(vales[28]) ? 0 : Convert.ToInt32(vales[28]);
                        dbSparePart.IsOriginal = vales[29] == "1" ? true : false;
                        dbSparePartCodes.Add(dbSparePart);
                        return false;
                    };
                    //根据SQL查询数据库，将查出来的结果根据定义的委托中的方法（这里是getDbSparePartCodes）放到具体的集合中（这里是dbSparePartCodes）
                    db.QueryDataWithInOperator(@"select ID,
                                                                    CODE,
                                                                    NAME,
                                                                    ReferenceCode,
                                                                    LASTSUBSTITUTE,
                                                                    NEXTSUBSTITUTE,
                                                                    SHELFLIFE,
                                                                    ENGLISHNAME,
                                                                    PINYINCODE,
                                                                    REFERENCECODE,
                                                                    REFERENCENAME,
                                                                    CADCODE,
                                                                    CADNAME,
                                                                    PARTTYPE,
                                                                    SPECIFICATION,
                                                                    FEATURE,
                                                                    STATUS,
                                                                    LENGTH,
                                                                    WIDTH,
                                                                    HEIGHT,
                                                                    VOLUME,
                                                                    WEIGHT,
                                                                    MATERIAL,
                                                                    PACKINGAMOUNT,
                                                                    PACKINGSPECIFICATION,
                                                                    PARTSOUTPACKINGCODE,
                                                                    PARTSINPACKINGCODE,
                                                                    MEASUREUNIT,
                                                                    MINPACKINGAMOUNT,
                                                                    IsOriginal
                                                                from sparepart where status=1 ", "Code", false, partCodesNeedCheck, getDbSparePartCodes);
                    if(partCodesNeedCheck.Length > 0) {
                        var sparePartsExistsCode = sparePartsNeedCheck.Where(r => dbSparePartCodes.Any(v => v.Code == r.Code)).ToArray();
                        if(sparePartsExistsCode.Count() < sparePartsNeedCheck.Count()) {
                            var notExist = allList.Except(sparePartsExistsCode);
                            foreach(var item in notExist) {
                                if(item.ErrorMsg == null) {
                                    item.ErrorMsg = ErrorStrings.Export_Validation_PartsBranch_Validation8 + item.Code + ErrorStrings.Export_Validation_Sparepart_NotExistOrStop;
                                } else {
                                    item.ErrorMsg = item.ErrorMsg + ErrorStrings.Export_Validation_PartsBranch_Validation9 + item.Code + ErrorStrings.Export_Validation_Sparepart_NotExistOrStop;
                                }
                            }
                        }
                        rightList = allList.Where(r => r.ErrorMsg == null).ToList();
                        rightList.ForEach(i => {
                            var rSparePart = dbSparePartCodes.First(r => r.Code == i.Code);
                            i.MInPackingAmount = rSparePart.MInPackingAmount;
                            i.IsOriginal = rSparePart.IsOriginal;
                        });
                    }

                    //var sparePartsNeedCheck = allList.ToList();
                    var StandardCodeNeedCheck = sparePartsNeedCheck.Select(r => r.StandardCode).Distinct().ToArray();
                    var dbStandardCodes = new List<SparePartExtend>();
                    Func<string[], bool> getDbStandardCodes = vales => {
                        var SparePartExtend = new SparePartExtend();
                        SparePartExtend.StandardCode = vales[0];
                        SparePartExtend.StandardName = vales[1];
                        dbStandardCodes.Add(SparePartExtend);
                        return false;
                    };
                    //根据SQL查询数据库，将查出来的结果根据定义的委托中的方法（这里是getDbSparePartCodes）放到具体的集合中（这里是dbSparePartCodes）
                    db.QueryDataWithInOperator("select StandardCode,StandardName from ProductStandard where status!=99 ", "StandardCode", true, StandardCodeNeedCheck, getDbStandardCodes);
                    if(StandardCodeNeedCheck.Length > 0) {
                        foreach(var item in allList.Where(r => r.ErrorMsg == null)) {
                            if(!string.IsNullOrEmpty(item.StandardCode)) {
                                var standard = dbStandardCodes.FirstOrDefault(v => v.StandardCode == item.StandardCode);
                                if(standard != null) {
                                    item.StandardName = standard.StandardName;
                                } else {
                                    item.ErrorMsg = ErrorStrings.Export_Validation_PartsBranch_Validation10 + item.StandardCode + ErrorStrings.Export_Validation_Sparepart_NotExist;
                                }
                            }
                        }
                    }


                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    rightList = allList.Except(errorList).ToList();
                    var spareParts = rightList.ToList();
                    var sparePartCodes = spareParts.Select(r => r.Code).Distinct().ToArray();
                    Func<string[], bool> getoldspareParts = values => {
                        oldspareParts.Add(new SparePartExtend {
                            Code = values[0],
                            ExchangeIdentification = values[1],
                            Id = int.Parse(values[2]),
                            ReferenceName = values[3],
                            Name = values[4]
                        });
                        return false;
                    };
                    db.QueryDataWithInOperator("select Code,ExchangeIdentification,Id,ReferenceName,Name from sparePart where status!=99 ", "Code", true, sparePartCodes, getoldspareParts);

                    //查找对应的互换组
                    var partsExchangeGroups = rightList.Select(r => r.ExchangeIdentification).Distinct().ToArray();

                    Func<string[], bool> getDbpartsExchangeGroups = values => {
                        var item = new PartsExchangeGroupExtend {
                            Id = int.Parse(values[0]),
                            ExGroupCode = values[1].ToString(),
                            ExchangeCode = values[2].ToString(),
                            PartCode = values[3].ToString()
                        };
                        dbpartsExchangeGroups.Add(item);
                        return false;
                    };
                    db.QueryDataWithInOperator("select p.Id,p.ExGroupCode,p.ExchangeCode,s.Code from PartsExchangeGroup p left join SparePart s on s.ExchangeIdentification=p.ExchangeCode where p.Status=1", "p.ExchangeCode", false, partsExchangeGroups, getDbpartsExchangeGroups);


                    //查找对应的互换信息

                    Func<string[], bool> getDbpartsExchanges = values => {
                        var item = new PartsExchangeGroupExtend {
                            Id = int.Parse(values[0]),
                            ExchangeCode = values[1].ToString(),
                            PartCode = values[2].ToString()
                        };
                        dbpartsExchanges.Add(item);
                        return false;
                    };
                    db.QueryDataWithInOperator("select p.Id,p.ExchangeCode,s.Code from PartsExchange p inner join SparePart s on s.ExchangeIdentification=p.ExchangeCode where p.Status=1", "ExchangeCode", false, partsExchangeGroups, getDbpartsExchanges);
                }
                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempObj = list[index - 1];
                            var values = new object[] {
                                tempObj.Code,
                                tempObj.Name,
                                tempObj.ReferenceCode,
                                tempObj.ExchangeIdentification,
                                tempObj.OverseasPartsFigure,
                                tempObj.ProductBrand,
                                tempObj.IMSCompressionNumber,
                                tempObj.IMSManufacturerNumber,
                                tempObj.CADCode,
                                tempObj.Specification, 
                                tempObj.EnglishName,
                                tempObj.PartTypeStr,
                                tempObj.MeasureUnit,
                                tempObj.MInPackingAmountStr,
                                tempObj.ShelfLife,
                                tempObj.GroupABCCategoryStr,
                                tempObj.LastSubstitute, 
                                tempObj.NextSubstitute,
                                tempObj.WeightStr,
                                tempObj.VolumeStr,
                                tempObj.Feature,
                                tempObj.TotalNumber,
                                tempObj.Factury,
                                tempObj.IsOriginal,
                                tempObj.CategoryCode,
                                tempObj.CategoryName,
                                tempObj.StandardCode,
                                tempObj.StandardName,
                                tempObj.ErrorMsg
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }
                //导入所有合格数据  (替换 name ==null 的为原先（替换前）的值)
                if(!rightList.Any())
                    return true;

                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    if(rightList.Any()) {
                        var sparePartCodeCheck = rightList.Select(r => r.Code).Distinct().ToArray();
                        var dbSpareParts = new List<SparePart>();
                        Func<string[], bool> getDbSpareParts = values => {
                            dbSpareParts.Add(new SparePart {
                                Id = int.Parse(values[0]),
                                Code = values[1],
                                PartType = int.Parse(values[2]),
                                Status = int.Parse(values[3]),
                                MeasureUnit = values[4],
                                Name = values[5]
                            });
                            return false;
                        };
                        db.QueryDataWithInOperator("select Id,Code,PartType,Status,MeasureUnit,Name from SparePart", "Code", false, sparePartCodeCheck, getDbSpareParts);

                        var userInfo = Utils.GetCurrentUserInfo();
                        var sqlInsertHistory = db.GetInsertSql("SparePartHistory", "Id", new[] {
                            "SparePartId", "Code", "Name", "ReferenceCode", "ExchangeIdentification", "OverseasPartsFigure", "IMSCompressionNumber", "IMSManufacturerNumber", "CADCode", "Specification", "EnglishName", "PartType", "MeasureUnit", "MInPackingAmount", "ShelfLife", "GroupABCCategory", "LastSubstitute", "NextSubstitute", "Weight", "Volume", "Feature", "Factury", "IsOriginal", "CategoryCode", "CategoryName", "StandardCode", "StandardName", "Status", "CreatorId", "CreatorName", "CreateTime",
                        });
                        var sqlUpdatePartsExchangeStatus = db.GetUpdateSql("PartsExchange", new string[] {
                            "Status", "AbandonerId", "AbandonerName", "AbandonTime","Remark"
                        }, new string[] {
                            "PartId"
                        });
                        var sqlInsertPartsExchange = db.GetInsertSql("PartsExchange", "Id", new[] {
                            "ExchangeCode", "ExchangeName", "PartId", "Status", "Remark", "CreatorId", "CreatorName", "CreateTime"
                        });
                        var sqlInsertPartsExchangeHistory = db.GetInsertSql("PartsExchangeHistory", "Id", new[] {
                            "PartsExchangeId", "ExchangeCode", "ExchangeName", "PartId", "Status", "Remark", "CreatorId", "CreatorName", "CreateTime"
                        });
                        var sqlInsertPartsExchangeGroup = db.GetInsertSql("PartsExchangeGroup", "Id", new[] {
                            "ExGroupCode", "ExchangeCode", "Status", "CreatorId", "CreatorName", "CreateTime"
                        });
                        var ts = conn.BeginTransaction();
                        try {
                            foreach(var item in rightList) {
                                List<string> fields = new List<string>();
                                fields.Add("ModifyTime");
                                fields.Add("ModifierId");
                                fields.Add("ModifierName");
                                fields.Add("OMSparePartMark");
                                string sqlUpdateSparePart = null;
                                var commandSparePart = db.CreateDbCommand(sqlUpdateSparePart, conn, ts);
                                if(!string.IsNullOrEmpty(item.Name)) {
                                    fields.Add("Name");
                                    commandSparePart.Parameters.Add(db.CreateDbParameter("Name", item.Name));
                                }
                                if(!string.IsNullOrEmpty(item.ReferenceCode)) {
                                    fields.Add("ReferenceCode");
                                    commandSparePart.Parameters.Add(db.CreateDbParameter("ReferenceCode", item.ReferenceCode));
                                }
                                if(!string.IsNullOrEmpty(item.ExchangeIdentification)) {
                                    fields.Add("ExchangeIdentification");
                                    commandSparePart.Parameters.Add(db.CreateDbParameter("ExchangeIdentification", item.ExchangeIdentification));
                                }
                                if(!string.IsNullOrEmpty(item.OverseasPartsFigure)) {
                                    fields.Add("OverseasPartsFigure");
                                    commandSparePart.Parameters.Add(db.CreateDbParameter("OverseasPartsFigure", item.OverseasPartsFigure));
                                }
                                if(!string.IsNullOrEmpty(item.ProductBrand)) {
                                    fields.Add("ProductBrand");
                                    commandSparePart.Parameters.Add(db.CreateDbParameter("ProductBrand", item.ProductBrand));
                                }
                                if(!string.IsNullOrEmpty(item.IMSCompressionNumber)) {
                                    fields.Add("IMSCompressionNumber");
                                    commandSparePart.Parameters.Add(db.CreateDbParameter("IMSCompressionNumber", item.IMSCompressionNumber));
                                }
                                if(!string.IsNullOrEmpty(item.IMSManufacturerNumber)) {
                                    fields.Add("IMSManufacturerNumber");
                                    commandSparePart.Parameters.Add(db.CreateDbParameter("IMSManufacturerNumber", item.IMSManufacturerNumber));
                                }
                                if(!string.IsNullOrEmpty(item.CADCode)) {
                                    fields.Add("CADCode");
                                    commandSparePart.Parameters.Add(db.CreateDbParameter("CADCode", item.CADCode));
                                }
                                if(!string.IsNullOrEmpty(item.Specification)) {
                                    fields.Add("Specification");
                                    commandSparePart.Parameters.Add(db.CreateDbParameter("Specification", item.Specification));
                                }
                                if(!string.IsNullOrEmpty(item.EnglishName)) {
                                    fields.Add("EnglishName");
                                    commandSparePart.Parameters.Add(db.CreateDbParameter("EnglishName", item.EnglishName));
                                }
                                if(!string.IsNullOrEmpty(item.PartTypeStr)) {
                                    fields.Add("PartType");
                                    commandSparePart.Parameters.Add(db.CreateDbParameter("PartType", item.PartType));
                                }
                                if(!string.IsNullOrEmpty(item.MeasureUnit)) {
                                    fields.Add("MeasureUnit");
                                    commandSparePart.Parameters.Add(db.CreateDbParameter("MeasureUnit", item.MeasureUnit));
                                }
                                if(!string.IsNullOrEmpty(item.MInPackingAmountStr)) {
                                    fields.Add("MInPackingAmount");
                                    commandSparePart.Parameters.Add(db.CreateDbParameter("MInPackingAmount", item.MInPackingAmountStr));
                                }
                                if(!string.IsNullOrEmpty(item.ShelfLifeStr)) {
                                    fields.Add("ShelfLife");
                                    commandSparePart.Parameters.Add(db.CreateDbParameter("ShelfLife", item.ShelfLife));
                                }
                                if(!string.IsNullOrEmpty(item.GroupABCCategoryStr)) {
                                    fields.Add("GroupABCCategory");
                                    commandSparePart.Parameters.Add(db.CreateDbParameter("GroupABCCategory", item.GroupABCCategory));
                                }
                                if(!string.IsNullOrEmpty(item.LastSubstitute)) {
                                    fields.Add("LastSubstitute");
                                    commandSparePart.Parameters.Add(db.CreateDbParameter("LastSubstitute", item.LastSubstitute));
                                }
                                if(!string.IsNullOrEmpty(item.NextSubstitute)) {
                                    fields.Add("NextSubstitute");
                                    commandSparePart.Parameters.Add(db.CreateDbParameter("NextSubstitute", item.NextSubstitute));
                                }
                                if(!string.IsNullOrEmpty(item.WeightStr)) {
                                    fields.Add("Weight");
                                    commandSparePart.Parameters.Add(db.CreateDbParameter("Weight", item.WeightStr));
                                }
                                if(!string.IsNullOrEmpty(item.VolumeStr)) {
                                    fields.Add("Volume");
                                    commandSparePart.Parameters.Add(db.CreateDbParameter("Volume", item.VolumeStr));
                                }
                                if(!string.IsNullOrEmpty(item.Feature)) {
                                    fields.Add("Feature");
                                    commandSparePart.Parameters.Add(db.CreateDbParameter("Feature", item.Feature));
                                }
                                if(!string.IsNullOrEmpty(item.TotalNumber)) {
                                    fields.Add("TotalNumber");
                                    commandSparePart.Parameters.Add(db.CreateDbParameter("TotalNumber", item.TotalNumber));
                                }
                                if(!string.IsNullOrEmpty(item.Factury)) {
                                    fields.Add("Factury");
                                    commandSparePart.Parameters.Add(db.CreateDbParameter("Factury", item.Factury));
                                }
                                if(!string.IsNullOrEmpty(item.IsOriginalStr)) {
                                    fields.Add("IsOriginal");
                                    commandSparePart.Parameters.Add(db.CreateDbParameter("IsOriginal", item.IsOriginal));
                                }
                                if(!string.IsNullOrEmpty(item.CategoryCode)) {
                                    fields.Add("CategoryCode");
                                    commandSparePart.Parameters.Add(db.CreateDbParameter("CategoryCode", item.CategoryCode));
                                }
                                if(!string.IsNullOrEmpty(item.CategoryName)) {
                                    fields.Add("CategoryName");
                                    commandSparePart.Parameters.Add(db.CreateDbParameter("CategoryName", item.CategoryName));
                                }

                                if(!string.IsNullOrEmpty(item.StandardCode)) {
                                    fields.Add("StandardCode");
                                    fields.Add("StandardName");
                                    commandSparePart.Parameters.Add(db.CreateDbParameter("StandardCode", item.StandardCode));
                                    commandSparePart.Parameters.Add(db.CreateDbParameter("StandardName", item.StandardName));
                                }
                                var spareParts = dbSpareParts.FirstOrDefault(v => v.Code == item.Code);
                                if(string.IsNullOrEmpty(item.Name))
                                    item.Name = spareParts.Name;

                                sqlUpdateSparePart = db.GetUpdateSql("SparePart", fields.ToArray(), new[] {
                                    "Id"
                                });
                                commandSparePart.Parameters.Add(db.CreateDbParameter("Id", spareParts.Id));
                                commandSparePart.Parameters.Add(db.CreateDbParameter("ModifyTime", DateTime.Now));
                                commandSparePart.Parameters.Add(db.CreateDbParameter("ModifierId", userInfo.Id));
                                commandSparePart.Parameters.Add(db.CreateDbParameter("ModifierName", userInfo.Name));
                                commandSparePart.Parameters.Add(db.CreateDbParameter("OMSparePartMark", 0));
                                commandSparePart.CommandText = sqlUpdateSparePart;
                                commandSparePart.ExecuteNonQuery();

                                var oldsparePart = oldspareParts.Where(r => r.Code == item.Code).FirstOrDefault();
                                if(oldsparePart != null) {
                                    if(oldsparePart.Name != item.Name) {
                                        var sqlUpdatePartsBranchs = db.GetUpdateSql("PartsBranch", new string[] {
                                            "PartName", "ModifierId", "ModifierName", "ModifyTime","OMSparePartMark"
                                        }, new string[] {
                                            "PartCode"
                                        });
                                        var commandpartsbranch = db.CreateDbCommand(sqlUpdatePartsBranchs, conn, ts);
                                        commandpartsbranch.Parameters.Add(db.CreateDbParameter("PartName", item.Name));
                                        commandpartsbranch.Parameters.Add(db.CreateDbParameter("ModifierId", userInfo.Id));
                                        commandpartsbranch.Parameters.Add(db.CreateDbParameter("ModifierName", userInfo.Name));
                                        commandpartsbranch.Parameters.Add(db.CreateDbParameter("ModifyTime", DateTime.Now));
                                        commandpartsbranch.Parameters.Add(db.CreateDbParameter("PartCode", item.Code));
                                        commandpartsbranch.Parameters.Add(db.CreateDbParameter("OMSparePartMark", 0));
                                        commandpartsbranch.ExecuteNonQuery();
                                    }
                                }
                                //新增履历
                                string code = Strings.StrConv(item.Code, VbStrConv.Narrow).Replace("【", "[");
                                var command = db.CreateDbCommand(sqlInsertHistory, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("SparePartId", spareParts.Id));
                                command.Parameters.Add(db.CreateDbParameter("Code", code));
                                command.Parameters.Add(db.CreateDbParameter("Name", item.Name));
                                command.Parameters.Add(db.CreateDbParameter("ReferenceCode", item.ReferenceCode));
                                command.Parameters.Add(db.CreateDbParameter("ExchangeIdentification", item.ExchangeIdentification));
                                command.Parameters.Add(db.CreateDbParameter("OverseasPartsFigure", item.OverseasPartsFigure));
                                command.Parameters.Add(db.CreateDbParameter("IMSCompressionNumber", item.IMSCompressionNumber));
                                command.Parameters.Add(db.CreateDbParameter("IMSManufacturerNumber", item.IMSManufacturerNumber));
                                command.Parameters.Add(db.CreateDbParameter("CADCode", item.CADCode));
                                command.Parameters.Add(db.CreateDbParameter("Specification", item.Specification));
                                command.Parameters.Add(db.CreateDbParameter("EnglishName", item.EnglishName));
                                if(item.PartTypeStr == null) {
                                    command.Parameters.Add(db.CreateDbParameter("PartType", spareParts.PartType));
                                } else {
                                    command.Parameters.Add(db.CreateDbParameter("PartType", item.PartType));
                                }
                                if(item.MeasureUnitStr == null) {
                                    command.Parameters.Add(db.CreateDbParameter("MeasureUnit", spareParts.MeasureUnit));
                                } else {
                                    command.Parameters.Add(db.CreateDbParameter("MeasureUnit", item.MeasureUnit));
                                }
                                if(!string.IsNullOrEmpty(item.MInPackingAmountStr)) {
                                    command.Parameters.Add(db.CreateDbParameter("MInPackingAmount", item.MInPackingAmountStr));
                                } else {
                                    command.Parameters.Add(db.CreateDbParameter("MInPackingAmount", item.MInPackingAmount));
                                }
                                command.Parameters.Add(db.CreateDbParameter("ShelfLife", item.ShelfLife));
                                command.Parameters.Add(db.CreateDbParameter("GroupABCCategory", item.GroupABCCategory));
                                command.Parameters.Add(db.CreateDbParameter("LastSubstitute", item.LastSubstitute));
                                command.Parameters.Add(db.CreateDbParameter("NextSubstitute", item.NextSubstitute));
                                command.Parameters.Add(db.CreateDbParameter("Weight", item.Weight));
                                command.Parameters.Add(db.CreateDbParameter("Volume", item.Volume));
                                command.Parameters.Add(db.CreateDbParameter("Feature", item.Feature));
                                command.Parameters.Add(db.CreateDbParameter("Factury", item.Factury));
                                command.Parameters.Add(db.CreateDbParameter("IsOriginal", item.IsOriginal));
                                command.Parameters.Add(db.CreateDbParameter("CategoryCode", item.CategoryCode));
                                command.Parameters.Add(db.CreateDbParameter("CategoryName", item.CategoryName));
                                command.Parameters.Add(db.CreateDbParameter("StandardCode", item.StandardCode));
                                command.Parameters.Add(db.CreateDbParameter("StandardName", item.StandardName));

                                if(item.StatusStr == null) {
                                    command.Parameters.Add(db.CreateDbParameter("Status", spareParts.Status));
                                } else {
                                    command.Parameters.Add(db.CreateDbParameter("Status", item.Status));
                                }
                                command.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                command.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                command.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                command.ExecuteNonQuery();
                                var commandUpdatePartsExchange = db.CreateDbCommand(sqlUpdatePartsExchangeStatus, conn, ts);
                                commandUpdatePartsExchange.Parameters.Add(db.CreateDbParameter("Status", 99));
                                commandUpdatePartsExchange.Parameters.Add(db.CreateDbParameter("AbandonerId", userInfo.Id));
                                commandUpdatePartsExchange.Parameters.Add(db.CreateDbParameter("AbandonerName", userInfo.Name));
                                commandUpdatePartsExchange.Parameters.Add(db.CreateDbParameter("AbandonTime", DateTime.Now));
                                commandUpdatePartsExchange.Parameters.Add(db.CreateDbParameter("PartId", spareParts.Id));
                                commandUpdatePartsExchange.Parameters.Add(db.CreateDbParameter("Remark", ErrorStrings.Export_Validation_Sparepart_RemarkDefault4));
                                commandUpdatePartsExchange.ExecuteNonQuery();
                                if(!string.IsNullOrEmpty(item.ExchangeIdentification)) {
                                    var commandPartsExchange = db.CreateDbCommand(sqlInsertPartsExchange, conn, ts);
                                    commandPartsExchange.Parameters.Add(db.CreateDbParameter("ExchangeCode", item.ExchangeIdentification));
                                    commandPartsExchange.Parameters.Add(db.CreateDbParameter("ExchangeName", ""));
                                    commandPartsExchange.Parameters.Add(db.CreateDbParameter("PartId", spareParts.Id));
                                    commandPartsExchange.Parameters.Add(db.CreateDbParameter("Status", 1));
                                    commandPartsExchange.Parameters.Add(db.CreateDbParameter("Remark", ErrorStrings.Export_Validation_Sparepart_RemarkDefault5));
                                    commandPartsExchange.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                    commandPartsExchange.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                    commandPartsExchange.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                    var exchangeId = db.ExecuteInsert(commandPartsExchange, "Id");
                                    var commandPartsExchangeHistory = db.CreateDbCommand(sqlInsertPartsExchangeHistory, conn, ts);
                                    commandPartsExchangeHistory.Parameters.Add(db.CreateDbParameter("PartsExchangeId", exchangeId));
                                    commandPartsExchangeHistory.Parameters.Add(db.CreateDbParameter("ExchangeCode", item.ExchangeIdentification));
                                    commandPartsExchangeHistory.Parameters.Add(db.CreateDbParameter("ExchangeName", ""));
                                    commandPartsExchangeHistory.Parameters.Add(db.CreateDbParameter("PartId", spareParts.Id));
                                    commandPartsExchangeHistory.Parameters.Add(db.CreateDbParameter("Status", 1));
                                    commandPartsExchangeHistory.Parameters.Add(db.CreateDbParameter("Remark", ErrorStrings.Export_Validation_Sparepart_RemarkDefault5));
                                    commandPartsExchangeHistory.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                    commandPartsExchangeHistory.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                    commandPartsExchangeHistory.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                    commandPartsExchangeHistory.ExecuteNonQuery();
                                }
                            }
                            if(rightList.Any()) {
                                rightList = rightList.Where(r => !string.IsNullOrEmpty(r.ExchangeIdentification)).ToList();
                                dbpartsExchangeGroups = dbpartsExchangeGroups.Where(r => !rightList.Any(p => p.Code == r.PartCode)).ToList();
                                //dbpartsExchanges = dbpartsExchanges.Where(r => !rightList.Any(p => p.Code == r.PartCode)).ToList();
                                var partsExchanges = dbpartsExchanges.Where(r => !rightList.Any(p => p.Code == r.PartCode)).ToList();
                                if(rightList.Any()) {
                                    var groups = rightList.GroupBy(r => r.ExchangeIdentification).Where(r => r.Count() > 1);
                                    var distinctMany = groups.SelectMany(r => r).ToList();
                                    distinctMany = distinctMany.Where((x, i) => distinctMany.FindIndex(z => z.ExchangeIdentification == x.ExchangeIdentification) == i).ToList();
                                    foreach(var groupitem in distinctMany) {
                                        var exchange = dbpartsExchanges.FirstOrDefault(v => v.PartCode == groupitem.Code && v.ExchangeCode == groupitem.ExchangeIdentification);
                                        if(exchange == null) {
                                            var partsExchangeGroup = dbpartsExchangeGroups.Where(r => r.ExchangeCode == groupitem.ExchangeIdentification);
                                            if(!partsExchangeGroup.Any()) {
                                                var commandPartsExchangeGroup = db.CreateDbCommand(sqlInsertPartsExchangeGroup, conn, ts);
                                                var exgroupcode = CodeGenerator.Generate(db, "PartsExchange");
                                                commandPartsExchangeGroup.Parameters.Add(db.CreateDbParameter("ExGroupCode", exgroupcode));
                                                commandPartsExchangeGroup.Parameters.Add(db.CreateDbParameter("ExchangeCode", groupitem.ExchangeIdentification));
                                                commandPartsExchangeGroup.Parameters.Add(db.CreateDbParameter("Status", 1));
                                                commandPartsExchangeGroup.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                                commandPartsExchangeGroup.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                                commandPartsExchangeGroup.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                                commandPartsExchangeGroup.ExecuteNonQuery();
                                            }
                                        }
                                    }

                                    var groupsOne = rightList.GroupBy(r => r.ExchangeIdentification).Where(r => r.Count() == 1);
                                    var onelist = groupsOne.SelectMany(r => r).ToList();
                                    foreach(var groupitem in onelist) {
                                        var partsExchangeGroup = dbpartsExchangeGroups.Where(r => r.ExchangeCode == groupitem.ExchangeIdentification);
                                        if(!partsExchangeGroup.Any()) {
                                            var partsExchange = partsExchanges.Where(r => r.ExchangeCode == groupitem.ExchangeIdentification);
                                            if(partsExchange.Any()) {
                                                var commandPartsExchangeGroup = db.CreateDbCommand(sqlInsertPartsExchangeGroup, conn, ts);
                                                var exgroupcode = CodeGenerator.Generate(db, "PartsExchange");
                                                commandPartsExchangeGroup.Parameters.Add(db.CreateDbParameter("ExGroupCode", exgroupcode));
                                                commandPartsExchangeGroup.Parameters.Add(db.CreateDbParameter("ExchangeCode", groupitem.ExchangeIdentification));
                                                commandPartsExchangeGroup.Parameters.Add(db.CreateDbParameter("Status", 1));
                                                commandPartsExchangeGroup.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                                commandPartsExchangeGroup.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                                commandPartsExchangeGroup.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                                commandPartsExchangeGroup.ExecuteNonQuery();
                                            }
                                        }
                                    }
                                }
                            }
                            //无异常提交
                            ts.Commit();
                            ts.Dispose();
                        } catch(Exception) {
                            //报错回滚
                            ts.Rollback();
                            ts.Dispose();
                            throw;
                        }
                    }
                    if(conn.State == System.Data.ConnectionState.Open) {
                        conn.Close();
                    }
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }

        public bool 批量停用配件信息(string fileName, out int excelImportNum, out List<SparePartExtend> rightData, out List<SparePartExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<SparePartExtend>();
            var rightList = new List<SparePartExtend>();
            var allList = new List<SparePartExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("SparePart", out notNullableFields, out fieldLenght);
                List<object> excelColumns;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Code, "Code");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName, "Name");
                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;
                    var keyValuePairs = new[] {
                                new KeyValuePair<string, string>("Status","MasterData_Status") 
                            };
                    tempExcelOperator.ImportHelper.LoadEnumFromDb(null, null, keyValuePairs);

                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var sparePart = new SparePartExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        sparePart.CodeStr = Strings.StrConv(newRow["Code"], VbStrConv.Narrow).Replace("【", "[");
                        sparePart.CodeStr = strconvsparecode(sparePart.CodeStr);
                        sparePart.Code = Strings.StrConv(newRow["Code"], VbStrConv.Narrow).Replace("【", "[");
                        sparePart.Code = strconvsparecode(sparePart.Code);
                        sparePart.Code = sparePart.Code;//全部大写
                        sparePart.Name = newRow["Name"];
                        sparePart.NameStr = newRow["Name"];

                        var tempErrorMessage = new List<string>();
                        //配件编号检查
                        var fieldIndex = notNullableFields.IndexOf("CODE");
                        if(string.IsNullOrEmpty(sparePart.Code)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.OverstockPartsAppDetail__Validation1);
                        } else {
                            //配件长度不得超过25
                            if(Encoding.Default.GetByteCount(sparePart.Code) > 25)
                                tempErrorMessage.Add(ErrorStrings.ImpPartsSupplierRelation_Validation8);
                        }
                        if(tempErrorMessage.Count > 0) {
                            sparePart.ErrorMsg = string.Join("; ", tempErrorMessage);
                        } else {
                            var tempStatus = tempExcelOperator.ImportHelper.GetEnumValue("Status", ErrorStrings.Export_Validation_PartsBranch_Used);
                            if(tempStatus.HasValue) {
                                sparePart.Status = tempStatus.Value;
                            }
                        }
                        allList.Add(sparePart);
                        return false;
                    });
                    //校验导入的数据配件编号重复
                    var groups = allList.GroupBy(r =>
                        r.Code
                    ).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        if(groupItem.ErrorMsg == null) {
                            groupItem.ErrorMsg = ErrorStrings.Export_Title_PartsBranch_Code + groupItem.Code + ErrorStrings.Export_Validation_PartsBranch_Validation5;
                        } else {
                            groupItem.ErrorMsg = groupItem.ErrorMsg + ErrorStrings.Export_Validation_PartsBranch_Validation6 + groupItem.Code + ErrorStrings.Export_Validation_PartsBranch_Validation5;
                        }
                    }
                    using(var conn = db.CreateDbConnection()) {
                        conn.Open();
                        var sparePartsNeedCheck = allList.ToList();
                        DataTable dtSparePart = new DataTable();
                        var partCodesNeedCheck = sparePartsNeedCheck.Select(r => r.Code).Distinct().ToArray();
                        var codeStr = string.Join("','", partCodesNeedCheck.ToArray());
                        var SpareParts = string.Format("select Id,Code,status from SparePart where Code in ('{0}')", codeStr);
                        var cmdSpareParts = db.CreateDbCommand(SpareParts, conn, null);
                        dtSparePart.Load(cmdSpareParts.ExecuteReader());

                        if(dtSparePart.Rows.Count > 0) {
                            var spareparts1 = new List<SparePartExtend>();
                            foreach(var item in sparePartsNeedCheck) {
                                var drSparePart = dtSparePart.Select(" Code='" + item.Code + "'");
                                if(drSparePart.Length == 0) {
                                    if(item.ErrorMsg == null) {
                                        item.ErrorMsg = ErrorStrings.Export_Validation_PartsBranch_Validation8 + item.Code + ErrorStrings.Export_Validation_Sparepart_NotExist;
                                    } else {
                                        item.ErrorMsg = item.ErrorMsg + ErrorStrings.Export_Validation_PartsBranch_Validation9 + item.Code + ErrorStrings.Export_Validation_Sparepart_NotExist;
                                    }
                                    errorList.Add(item);
                                } else
                                    spareparts1.Add(item);
                            }
                            foreach(var item in spareparts1) {
                                var drSparePartstatus = dtSparePart.Select(" Code='" + item.Code + "'" + "and status=" + (int)DcsMasterDataStatus.作废);
                                if(drSparePartstatus.Length > 0) {
                                    if(item.ErrorMsg == null)
                                        item.ErrorMsg = ErrorStrings.Export_Validation_PartsBranch_Validation11;
                                    else
                                        item.ErrorMsg = item.ErrorMsg + ";" + ErrorStrings.Export_Validation_PartsBranch_Validation11;
                                    errorList.Add(item);
                                }
                                var drSparePartstatus1 = dtSparePart.Select(" Code='" + item.Code + "'" + "and status=" + (int)DcsMasterDataStatus.停用);
                                if(drSparePartstatus1.Length > 0) {
                                    if(item.ErrorMsg == null)
                                        item.ErrorMsg = ErrorStrings.Export_Validation_PartsBranch_Validation12;
                                    else
                                        item.ErrorMsg = item.ErrorMsg + ";" + ErrorStrings.Export_Validation_PartsBranch_Validation12;
                                    errorList.Add(item);
                                }
                            }
                        } else {
                            foreach(var item in sparePartsNeedCheck) {
                                if(item.ErrorMsg == null) {
                                    item.ErrorMsg = ErrorStrings.Export_Validation_PartsBranch_Validation8 + item.Code + ErrorStrings.Export_Validation_Sparepart_NotExist;
                                } else {
                                    item.ErrorMsg = item.ErrorMsg + ErrorStrings.Export_Validation_PartsBranch_Validation9 + item.Code + ErrorStrings.Export_Validation_Sparepart_NotExist;
                                }
                                errorList.Add(item);

                            }
                        }

                    }

                }
                //获取所有不合格数据
                errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                //if(errList.Count > 0) {
                //    errorList.AddRange(errList);
                //}
                //获取所有合格数据
                rightList = allList.Except(errorList).ToList();

                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempSparePart = list[index - 1];
                            var values = new object[] {
                                tempSparePart.CodeStr, tempSparePart.NameStr,
                                tempSparePart.ErrorMsg
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }

                //导入所有合格数据
                if(!rightList.Any())
                    return true;
                //获取数据库的完整备件信息
                string[] partCode = rightList.Select(r => r.Code).ToArray();
                List<SparePartExtend> sparePartExtends = new List<SparePartExtend>();
                Func<string[], bool> getDbSparePartInfo = vales => {
                    sparePartExtends.Add(new SparePartExtend {
                        Id = Convert.ToInt32(vales[0]),
                        Code = vales[1],
                        ReferenceCode = vales[3],
                        //m.Name = vales[2],
                        LastSubstitute = vales[4],
                        NextSubstitute = vales[5],
                        ShelfLife = string.IsNullOrEmpty(vales[6]) ? 0 : Convert.ToInt32(vales[6]),
                        EnglishName = vales[7],
                        PinyinCode = vales[8],
                        //ReferenceCode = vales[9],
                        ReferenceName = vales[10],
                        CADCode = vales[11],
                        CADName = vales[12],
                        PartType = string.IsNullOrEmpty(vales[13]) ? 0 : Convert.ToInt32(vales[13]),
                        Specification = vales[14],
                        Feature = vales[15],
                        Status = string.IsNullOrEmpty(vales[16]) ? 0 : Convert.ToInt32(vales[16]),
                        Length = string.IsNullOrEmpty(vales[17]) ? 0 : Convert.ToDecimal(vales[17]),
                        Width = string.IsNullOrEmpty(vales[18]) ? 0 : Convert.ToDecimal(vales[18]),
                        Height = string.IsNullOrEmpty(vales[19]) ? 0 : Convert.ToDecimal(vales[19]),
                        Volume = string.IsNullOrEmpty(vales[20]) ? 0 : Convert.ToDecimal(vales[20]),
                        Weight = string.IsNullOrEmpty(vales[21]) ? 0 : Convert.ToDecimal(vales[21]),
                        Material = vales[22],
                        PackingAmount = string.IsNullOrEmpty(vales[23]) ? 0 : Convert.ToInt32(vales[23]),
                        PackingSpecification = vales[24],
                        PartsOutPackingCode = vales[25],
                        PartsInPackingCode = vales[26],
                        MeasureUnit = vales[27],
                        MInPackingAmount = string.IsNullOrEmpty(vales[28]) ? 0 : Convert.ToInt32(vales[28]),
                        ExchangeIdentification = vales[29],
                    });
                    return false;
                };
                db.QueryDataWithInOperator(@"select ID,
                                            CODE,
                                            NAME,
                                            ReferenceCode,
                                            LASTSUBSTITUTE,
                                            NEXTSUBSTITUTE,
                                            SHELFLIFE,
                                            ENGLISHNAME,
                                            PINYINCODE,
                                            REFERENCECODE,
                                            REFERENCENAME,
                                            CADCODE,
                                            CADNAME,
                                            PARTTYPE,
                                            SPECIFICATION,
                                            FEATURE,
                                            STATUS,
                                            LENGTH,
                                            WIDTH,
                                            HEIGHT,
                                            VOLUME,
                                            WEIGHT,
                                            MATERIAL,
                                            PACKINGAMOUNT,
                                            PACKINGSPECIFICATION,
                                            PARTSOUTPACKINGCODE,
                                            PARTSINPACKINGCODE,
                                            MEASUREUNIT,
                                            MINPACKINGAMOUNT,ExchangeIdentification
                                  from sparepart where status=1 ", "Code", false, partCode, getDbSparePartInfo);

                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }

        public bool 批量恢复配件信息(string fileName, out int excelImportNum, out List<SparePartExtend> rightData, out List<SparePartExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<SparePartExtend>();
            var rightList = new List<SparePartExtend>();
            var allList = new List<SparePartExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("SparePart", out notNullableFields, out fieldLenght);
                List<object> excelColumns;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Code, "Code");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName, "Name");
                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;
                    var keyValuePairs = new[] {
                                new KeyValuePair<string, string>("Status","MasterData_Status") 
                            };
                    tempExcelOperator.ImportHelper.LoadEnumFromDb(null, null, keyValuePairs);

                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var sparePart = new SparePartExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        sparePart.CodeStr = Strings.StrConv(newRow["Code"], VbStrConv.Narrow).Replace("【", "[");
                        sparePart.CodeStr = strconvsparecode(sparePart.CodeStr);
                        sparePart.Code = Strings.StrConv(newRow["Code"], VbStrConv.Narrow).Replace("【", "[");
                        sparePart.Code = strconvsparecode(sparePart.Code);
                        sparePart.Code = sparePart.Code;//全部大写
                        sparePart.Name = newRow["Name"];
                        sparePart.NameStr = newRow["Name"];

                        var tempErrorMessage = new List<string>();
                        //配件编号检查
                        var fieldIndex = notNullableFields.IndexOf("CODE");
                        if(string.IsNullOrEmpty(sparePart.Code)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.OverstockPartsAppDetail__Validation1);
                        } else {
                            //配件长度不得超过25
                            if(Encoding.Default.GetByteCount(sparePart.Code) > 25)
                                tempErrorMessage.Add(ErrorStrings.ImpPartsSupplierRelation_Validation8);
                        }
                        if(tempErrorMessage.Count > 0) {
                            sparePart.ErrorMsg = string.Join("; ", tempErrorMessage);
                        } else {
                            var tempStatus = tempExcelOperator.ImportHelper.GetEnumValue("Status", ErrorStrings.Export_Validation_PartsBranch_Used);
                            if(tempStatus.HasValue) {
                                sparePart.Status = tempStatus.Value;
                            }
                        }
                        allList.Add(sparePart);
                        return false;
                    });
                    //校验导入的数据配件编号重复
                    var groups = allList.GroupBy(r =>
                        r.Code
                    ).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        if(groupItem.ErrorMsg == null) {
                            groupItem.ErrorMsg = ErrorStrings.Export_Title_PartsBranch_Code + groupItem.Code + ErrorStrings.Export_Validation_PartsBranch_Validation5;
                        } else {
                            groupItem.ErrorMsg = groupItem.ErrorMsg + ErrorStrings.Export_Validation_PartsBranch_Validation6 + groupItem.Code + ErrorStrings.Export_Validation_PartsBranch_Validation5;
                        }
                    }
                    using(var conn = db.CreateDbConnection()) {
                        conn.Open();
                        var sparePartsNeedCheck = allList.ToList();
                        DataTable dtSparePart = new DataTable();
                        var partCodesNeedCheck = sparePartsNeedCheck.Select(r => r.Code).Distinct().ToArray();
                        var codeStr = string.Join("','", partCodesNeedCheck.ToArray());
                        var SpareParts = string.Format("select Id,Code,status from SparePart where Code in ('{0}')", codeStr);
                        var cmdSpareParts = db.CreateDbCommand(SpareParts, conn, null);
                        dtSparePart.Load(cmdSpareParts.ExecuteReader());
                        if(dtSparePart.Rows.Count > 0) {
                            var spareparts1 = new List<SparePartExtend>();
                            foreach(var item in sparePartsNeedCheck) {
                                var drSparePart = dtSparePart.Select(" Code='" + item.Code + "'");
                                if(drSparePart.Length == 0) {
                                    if(item.ErrorMsg == null) {
                                        item.ErrorMsg = ErrorStrings.Export_Validation_PartsBranch_Validation8 + item.Code + ErrorStrings.Export_Validation_Sparepart_NotExist;
                                    } else {
                                        item.ErrorMsg = item.ErrorMsg + ErrorStrings.Export_Validation_PartsBranch_Validation9 + item.Code + ErrorStrings.Export_Validation_Sparepart_NotExist;
                                    }
                                    errorList.Add(item);
                                } else
                                    spareparts1.Add(item);
                            }
                            foreach(var item in spareparts1) {
                                var drSparePartstatus = dtSparePart.Select(" Code='" + item.Code + "'" + "and status=" + (int)DcsMasterDataStatus.作废);
                                if(drSparePartstatus.Length > 0) {
                                    if(item.ErrorMsg == null)
                                        item.ErrorMsg = ErrorStrings.Export_Validation_PartsBranch_Validation11;
                                    else
                                        item.ErrorMsg = item.ErrorMsg + ";" + ErrorStrings.Export_Validation_PartsBranch_Validation11;
                                    errorList.Add(item);
                                }
                                var drSparePartstatus1 = dtSparePart.Select(" Code='" + item.Code + "'" + "and status=" + (int)DcsMasterDataStatus.有效);
                                if(drSparePartstatus1.Length > 0) {
                                    if(item.ErrorMsg == null)
                                        item.ErrorMsg = ErrorStrings.Export_Validation_PartsBranch_Validation13;
                                    else
                                        item.ErrorMsg = item.ErrorMsg + ";"+ErrorStrings.Export_Validation_PartsBranch_Validation12;
                                    errorList.Add(item);
                                }
                            }
                        } else {
                            foreach(var item in sparePartsNeedCheck) {
                                if(item.ErrorMsg == null) {
                                    item.ErrorMsg = ErrorStrings.Export_Validation_PartsBranch_Validation8 + item.Code + ErrorStrings.Export_Validation_Sparepart_NotExist;
                                } else {
                                    item.ErrorMsg = item.ErrorMsg + ErrorStrings.Export_Validation_PartsBranch_Validation9 + item.Code + ErrorStrings.Export_Validation_Sparepart_NotExist;
                                }
                                errorList.Add(item);

                            }
                        }
                    }
                }
                //获取所有不合格数据
                errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                //if(errList.Count > 0) {
                //    errorList.AddRange(errList);
                //}
                //获取所有合格数据
                rightList = allList.Except(errorList).ToList();

                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempSparePart = list[index - 1];
                            var values = new object[] {
                                tempSparePart.CodeStr, tempSparePart.NameStr,
                                tempSparePart.ErrorMsg
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }

                //导入所有合格数据
                if(!rightList.Any())
                    return true;
                //获取数据库的完整备件信息
                string[] partCode = rightList.Select(r => r.Code).ToArray();
                List<SparePartExtend> sparePartExtends = new List<SparePartExtend>();
                Func<string[], bool> getDbSparePartInfo = vales => {
                    sparePartExtends.Add(new SparePartExtend {
                        Id = Convert.ToInt32(vales[0]),
                        Code = vales[1],
                        ReferenceCode = vales[3],
                        //m.Name = vales[2],
                        LastSubstitute = vales[4],
                        NextSubstitute = vales[5],
                        ShelfLife = string.IsNullOrEmpty(vales[6]) ? 0 : Convert.ToInt32(vales[6]),
                        EnglishName = vales[7],
                        PinyinCode = vales[8],
                        //ReferenceCode = vales[9],
                        ReferenceName = vales[10],
                        CADCode = vales[11],
                        CADName = vales[12],
                        PartType = string.IsNullOrEmpty(vales[13]) ? 0 : Convert.ToInt32(vales[13]),
                        Specification = vales[14],
                        Feature = vales[15],
                        Status = string.IsNullOrEmpty(vales[16]) ? 0 : Convert.ToInt32(vales[16]),
                        Length = string.IsNullOrEmpty(vales[17]) ? 0 : Convert.ToDecimal(vales[17]),
                        Width = string.IsNullOrEmpty(vales[18]) ? 0 : Convert.ToDecimal(vales[18]),
                        Height = string.IsNullOrEmpty(vales[19]) ? 0 : Convert.ToDecimal(vales[19]),
                        Volume = string.IsNullOrEmpty(vales[20]) ? 0 : Convert.ToDecimal(vales[20]),
                        Weight = string.IsNullOrEmpty(vales[21]) ? 0 : Convert.ToDecimal(vales[21]),
                        Material = vales[22],
                        PackingAmount = string.IsNullOrEmpty(vales[23]) ? 0 : Convert.ToInt32(vales[23]),
                        PackingSpecification = vales[24],
                        PartsOutPackingCode = vales[25],
                        PartsInPackingCode = vales[26],
                        MeasureUnit = vales[27],
                        MInPackingAmount = string.IsNullOrEmpty(vales[28]) ? 0 : Convert.ToInt32(vales[28]),
                        ExchangeIdentification = vales[29],
                    });
                    return false;
                };
                db.QueryDataWithInOperator(@"select ID,
                                            CODE,
                                            NAME,
                                            ReferenceCode,
                                            LASTSUBSTITUTE,
                                            NEXTSUBSTITUTE,
                                            SHELFLIFE,
                                            ENGLISHNAME,
                                            PINYINCODE,
                                            REFERENCECODE,
                                            REFERENCENAME,
                                            CADCODE,
                                            CADNAME,
                                            PARTTYPE,
                                            SPECIFICATION,
                                            FEATURE,
                                            STATUS,
                                            LENGTH,
                                            WIDTH,
                                            HEIGHT,
                                            VOLUME,
                                            WEIGHT,
                                            MATERIAL,
                                            PACKINGAMOUNT,
                                            PACKINGSPECIFICATION,
                                            PARTSOUTPACKINGCODE,
                                            PARTSINPACKINGCODE,
                                            MEASUREUNIT,
                                            MINPACKINGAMOUNT,ExchangeIdentification
                                  from sparepart where status=1 ", "Code", false, partCode, getDbSparePartInfo);
                //using(var conn = db.CreateDbConnection()) {
                //    conn.Open();
                //    //开启事务，新增更新配件在一个事务内
                //    var ts = conn.BeginTransaction();
                //    try {
                //        //新增配件
                //        if(rightList.Any()) {
                //            //获取新增数据的sql语句，Id为主键
                //            var sqlUpdate = db.GetUpdateSql("SparePart", new string[] { "Name", "ModifierId", "ModifierName", "ModifyTime" }, new string[] { "Code" });
                //            var sqlInsertHistory = db.GetInsertSql("SparePartHistory", "Id", new[] {
                //                "SparePartId","Code","ReferenceCode", "Name", "CADCode", "Specification", "EnglishName", "PartType", "MeasureUnit", "ShelfLife", "LastSubstitute", "NextSubstitute", "Weight", "Feature", "status", "CreatorId", "CreatorName", "CreateTime","MInPackingAmount"
                //            });
                //            var sqlUpdatePartsBranch = db.GetUpdateSql("PartsBranch", new string[] { "PartName", "ModifierId", "ModifierName", "ModifyTime" }, new string[] { "PartCode" });

                //            var userInfo = Utils.GetCurrentUserInfo();
                //            //往数据库增加配件信息
                //            foreach(SparePartExtend item in rightList) {
                //                var command = db.CreateDbCommand(sqlUpdate, conn, ts);
                //                command.Parameters.Add(db.CreateDbParameter("Code", Strings.StrConv(item.Code, VbStrConv.Narrow).Replace("【", "[")));
                //                command.Parameters.Add(db.CreateDbParameter("Name", item.Name));
                //                command.Parameters.Add(db.CreateDbParameter("ModifierId", userInfo.Id));
                //                command.Parameters.Add(db.CreateDbParameter("ModifierName", userInfo.Name));
                //                command.Parameters.Add(db.CreateDbParameter("ModifyTime", DateTime.Now));
                //                command.ExecuteNonQuery();
                //                //往数据库增加履历
                //                var sparePartExtend = sparePartExtends.Single(r => r.Code == item.Code);
                //                var commandHistory = db.CreateDbCommand(sqlInsertHistory, conn, ts);
                //                commandHistory.Parameters.Add(db.CreateDbParameter("SparePartId", sparePartExtend.Id));
                //                commandHistory.Parameters.Add(db.CreateDbParameter("Code", Strings.StrConv(sparePartExtend.Code, VbStrConv.Narrow).Replace("【", "[")));
                //                commandHistory.Parameters.Add(db.CreateDbParameter("Name", item.Name));
                //                commandHistory.Parameters.Add(db.CreateDbParameter("ReferenceCode", sparePartExtend.ReferenceCode));
                //                commandHistory.Parameters.Add(db.CreateDbParameter("CADCode", sparePartExtend.CADCode));
                //                commandHistory.Parameters.Add(db.CreateDbParameter("Specification", sparePartExtend.Specification));
                //                commandHistory.Parameters.Add(db.CreateDbParameter("EnglishName", sparePartExtend.EnglishName));
                //                commandHistory.Parameters.Add(db.CreateDbParameter("PartType", sparePartExtend.PartType));
                //                commandHistory.Parameters.Add(db.CreateDbParameter("MeasureUnit", sparePartExtend.MeasureUnit));
                //                commandHistory.Parameters.Add(db.CreateDbParameter("ShelfLife", sparePartExtend.ShelfLife));
                //                commandHistory.Parameters.Add(db.CreateDbParameter("LastSubstitute", sparePartExtend.LastSubstitute));
                //                commandHistory.Parameters.Add(db.CreateDbParameter("NextSubstitute", sparePartExtend.NextSubstitute));
                //                commandHistory.Parameters.Add(db.CreateDbParameter("Weight", sparePartExtend.Weight));
                //                commandHistory.Parameters.Add(db.CreateDbParameter("Feature", sparePartExtend.Feature));
                //                commandHistory.Parameters.Add(db.CreateDbParameter("Status", sparePartExtend.Status));
                //                commandHistory.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                //                commandHistory.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                //                commandHistory.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                //                commandHistory.Parameters.Add(db.CreateDbParameter("MInPackingAmount", sparePartExtend.MInPackingAmount));
                //                commandHistory.ExecuteNonQuery();

                //                //配件分品牌信息
                //                var partsBranchExtend = sparePartExtends.Single(r => r.Code == item.Code);
                //                var commandPartsBranch = db.CreateDbCommand(sqlUpdatePartsBranch, conn, ts);
                //                commandPartsBranch.Parameters.Add(db.CreateDbParameter("PartCode", Strings.StrConv(partsBranchExtend.Code, VbStrConv.Narrow).Replace("【", "[")));
                //                commandPartsBranch.Parameters.Add(db.CreateDbParameter("PartName", item.Name));
                //                commandPartsBranch.Parameters.Add(db.CreateDbParameter("ModifierId", userInfo.Id));
                //                commandPartsBranch.Parameters.Add(db.CreateDbParameter("ModifierName", userInfo.Name));
                //                commandPartsBranch.Parameters.Add(db.CreateDbParameter("ModifyTime", DateTime.Now));
                //                commandPartsBranch.ExecuteNonQuery();
                //            }

                //        }
                //        //无异常提交
                //        ts.Commit();
                //    } catch(Exception ex) {
                //        //报错回滚
                //        ts.Rollback();
                //        throw new Exception(ex.Message);
                //    } finally {
                //        if(conn.State == System.Data.ConnectionState.Open) {
                //            conn.Close();
                //        }
                //    }
                //}
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }

        public bool 批量替换海外配件图号(string fileName, out int excelImportNum, out List<SparePartExtend> rightData, out List<SparePartExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<SparePartExtend>();
            var rightList = new List<SparePartExtend>();
            var allList = new List<SparePartExtend>();
            var dbSparePartCodes = new List<SparePartExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("SparePart", out notNullableFields, out fieldLenght);
                List<object> excelColumns;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Code, "Code");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Sparepart_OverseasPartsCode, "OverseasPartsFigure");
                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;
                    var keyValuePairs = new[] {
                                new KeyValuePair<string, string>("Status","MasterData_Status") 
                            };
                    tempExcelOperator.ImportHelper.LoadEnumFromDb(null, null, keyValuePairs);
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var sparePart = new SparePartExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        sparePart.CodeStr = Strings.StrConv(newRow["Code"], VbStrConv.Narrow).Replace("【", "[");
                        sparePart.CodeStr = strconvsparecode(sparePart.CodeStr);
                        sparePart.Code = Strings.StrConv(newRow["Code"], VbStrConv.Narrow).Replace("【", "[");
                        sparePart.Code = strconvsparecode(sparePart.Code);
                        sparePart.Code = sparePart.Code;//全部大写
                        sparePart.OverseasPartsFigure = newRow["OverseasPartsFigure"];
                        sparePart.OverseasPartsFigureStr = newRow["OverseasPartsFigure"];
                        var tempErrorMessage = new List<string>();
                        //配件编号检查
                        var fieldIndex = notNullableFields.IndexOf("CODE");
                        if(string.IsNullOrEmpty(sparePart.Code)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.OverstockPartsAppDetail__Validation1);
                        } else {
                            //配件长度不得超过25
                            if(Encoding.Default.GetByteCount(sparePart.Code) > 25)
                                tempErrorMessage.Add(ErrorStrings.ImpPartsSupplierRelation_Validation8);
                        }
                        //零部件图号检查
                        if(string.IsNullOrEmpty(sparePart.OverseasPartsFigure)) {
                            tempErrorMessage.Add(ErrorStrings.Export_Validation_Sparepart_OverseasPartsFigureIsNull);
                        } else {
                            if(Encoding.Default.GetByteCount(sparePart.OverseasPartsFigure) > 18)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Sparepart_OverseasPartsFigureOverLong);
                        }
                        if(tempErrorMessage.Count > 0) {
                            sparePart.ErrorMsg = string.Join("; ", tempErrorMessage);
                        } else {
                            var tempStatus = tempExcelOperator.ImportHelper.GetEnumValue("Status", ErrorStrings.Export_Validation_PartsBranch_Used);
                            if(tempStatus.HasValue) {
                                sparePart.Status = tempStatus.Value;
                            }
                        }

                        allList.Add(sparePart);
                        return false;
                    });
                    //校验导入的数据配件编号重复
                    var groups = allList.GroupBy(r =>
                        r.Code
                    ).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        if(groupItem.ErrorMsg == null) {
                            groupItem.ErrorMsg = ErrorStrings.Export_Title_PartsBranch_Code + groupItem.Code + ErrorStrings.Export_Validation_PartsBranch_Validation5;
                        } else {
                            groupItem.ErrorMsg = groupItem.ErrorMsg + ErrorStrings.Export_Validation_PartsBranch_Validation6 + groupItem.Code + ErrorStrings.Export_Validation_PartsBranch_Validation5;
                        }
                    }
                    var sparePartsNeedCheck = allList.ToList();
                    var partCodesNeedCheck = sparePartsNeedCheck.Select(r => r.Code).Distinct().ToArray();

                    Func<string[], bool> getDbSparePartCodes = vales => {
                        var dbSparePart = new SparePartExtend();
                        dbSparePart.Id = Convert.ToInt32(vales[0]);
                        dbSparePart.Code = vales[1];
                        dbSparePart.Name = vales[2];
                        dbSparePart.ReferenceCode = vales[3];                        
                        dbSparePart.LastSubstitute = vales[4];
                        dbSparePart.NextSubstitute = vales[5];
                        dbSparePart.ShelfLife = string.IsNullOrEmpty(vales[6]) ? 0 : Convert.ToInt32(vales[6]);
                        dbSparePart.EnglishName = vales[7];
                        dbSparePart.PinyinCode = vales[8];
                        dbSparePart.ReferenceName = vales[9];
                        dbSparePart.CADCode = vales[10];
                        dbSparePart.CADName = vales[11];
                        dbSparePart.PartType = string.IsNullOrEmpty(vales[12]) ? 0 : Convert.ToInt32(vales[12]);
                        dbSparePart.Specification = vales[13];
                        dbSparePart.Feature = vales[14];
                        dbSparePart.Status = string.IsNullOrEmpty(vales[15]) ? 0 : Convert.ToInt32(vales[15]);
                        dbSparePart.Length = string.IsNullOrEmpty(vales[16]) ? 0 : Convert.ToDecimal(vales[16]);
                        dbSparePart.Width = string.IsNullOrEmpty(vales[17]) ? 0 : Convert.ToDecimal(vales[17]);
                        dbSparePart.Height = string.IsNullOrEmpty(vales[18]) ? 0 : Convert.ToDecimal(vales[18]);
                        dbSparePart.Volume = string.IsNullOrEmpty(vales[19]) ? 0 : Convert.ToDecimal(vales[19]);
                        dbSparePart.Weight = string.IsNullOrEmpty(vales[20]) ? 0 : Convert.ToDecimal(vales[20]);
                        dbSparePart.Material = vales[21];
                        dbSparePart.PackingAmount = string.IsNullOrEmpty(vales[22]) ? 0 : Convert.ToInt32(vales[22]);
                        dbSparePart.PackingSpecification = vales[23];
                        dbSparePart.PartsOutPackingCode = vales[24];
                        dbSparePart.PartsInPackingCode = vales[25];
                        dbSparePart.MeasureUnit = vales[26];
                        dbSparePart.MInPackingAmount = string.IsNullOrEmpty(vales[27]) ? 0 : Convert.ToInt32(vales[27]);
                        dbSparePart.IsNotWarrantyTransfer = string.IsNullOrEmpty(vales[28]) ? 0 : Convert.ToInt32(vales[28]);
                        dbSparePart.ExchangeIdentification = vales[29];
                        dbSparePartCodes.Add(dbSparePart);
                        return false;
                    };
                    //根据SQL查询数据库，将查出来的结果根据定义的委托中的方法（这里是getDbSparePartCodes）放到具体的集合中（这里是dbSparePartCodes）
                    db.QueryDataWithInOperator(@"select ID,
                                                                    CODE,
                                                                    NAME,
                                                                    ReferenceCode,
                                                                    LASTSUBSTITUTE,
                                                                    NEXTSUBSTITUTE,
                                                                    SHELFLIFE,
                                                                    ENGLISHNAME,
                                                                    PINYINCODE,                                                                    
                                                                    REFERENCENAME,
                                                                    CADCODE,
                                                                    CADNAME,
                                                                    PARTTYPE,
                                                                    SPECIFICATION,
                                                                    FEATURE,
                                                                    STATUS,
                                                                    LENGTH,
                                                                    WIDTH,
                                                                    HEIGHT,
                                                                    VOLUME,
                                                                    WEIGHT,
                                                                    MATERIAL,
                                                                    PACKINGAMOUNT,
                                                                    PACKINGSPECIFICATION,
                                                                    PARTSOUTPACKINGCODE,
                                                                    PARTSINPACKINGCODE,
                                                                    MEASUREUNIT,
                                                                    MINPACKINGAMOUNT,
                                                                    IsNotWarrantyTransfer,ExchangeIdentification

                                                                from sparepart where status=1 ", "Code", false, partCodesNeedCheck, getDbSparePartCodes);
                    if(partCodesNeedCheck.Length > 0) {
                        var sparePartsExistsCode = sparePartsNeedCheck.Where(r => dbSparePartCodes.Any(v => v.Code == r.Code)).ToArray();
                        if(sparePartsExistsCode.Count() < sparePartsNeedCheck.Count()) {
                            var notExist = allList.Except(sparePartsExistsCode);
                            foreach(var item in notExist) {
                                if(item.ErrorMsg == null) {
                                    item.ErrorMsg = ErrorStrings.Export_Validation_PartsBranch_Validation8 + item.Code + ErrorStrings.Export_Validation_Sparepart_NotExistOrStop;
                                } else {
                                    item.ErrorMsg = item.ErrorMsg + ErrorStrings.Export_Validation_PartsBranch_Validation9 + item.Code + ErrorStrings.Export_Validation_Sparepart_NotExistOrStop;
                                }
                                errorList.Add(item);
                            }
                        }
                    }
                }
                //获取所有不合格数据
                errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                //if(errList.Count > 0) {
                //    errorList.AddRange(errList);
                //}
                //获取所有合格数据
                rightList = allList.Except(errorList).ToList();

                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempSparePart = list[index - 1];
                            var values = new object[] {
                                tempSparePart.CodeStr, tempSparePart.OverseasPartsFigureStr,
                                tempSparePart.ErrorMsg
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }

                //导入所有合格数据
                if(!rightList.Any())
                    return true;
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    //开启事务，新增更新配件在一个事务内
                    var ts = conn.BeginTransaction();
                    try {
                        //新增配件
                        if(rightList.Any()) {
                            //获取新增数据的sql语句，Id为主键
                            var sqlUpdate = db.GetUpdateSql("SparePart", new string[] { "OverseasPartsFigure", "ModifierId", "ModifierName", "ModifyTime", "OMSparePartMark" }, new string[] { "Code" });
                            var sqlInsertHistory = db.GetInsertSql("SparePartHistory", "Id", new[] {
                                "IsNotWarrantyTransfer","SparePartId","Code","ReferenceCode","OverseasPartsFigure", "Name", "CADCode", "Specification", "EnglishName", "PartType", "MeasureUnit", "ShelfLife", "LastSubstitute", "NextSubstitute", "Weight", "Feature", "status", "CreatorId", "CreatorName", "CreateTime","MInPackingAmount","ExchangeIdentification"
                            });
                            var sqlUpdatePartsExchangeStatus = db.GetUpdateSql("PartsExchange", new string[] {
                                "Status"
                            }, new string[] {
                                "ExchangeCode","PartId"
                            });
                            var sqlInsertPartsExchange = db.GetInsertSql("PartsExchange", "Id", new[] {
                                "ExchangeCode","ExchangeName","PartId","Status","Remark","CreatorId","CreatorName","CreateTime"
                            });
                            var sqlInsertPartsExchangeHistory = db.GetInsertSql("PartsExchangeHistory", "Id", new[] {
                                "PartsExchangeId","ExchangeCode","ExchangeName","PartId","Status","Remark","CreatorId","CreatorName","CreateTime"
                            });
                            List<string> listSpares = new List<string>(); //查找配件编码
                            List<string> lisOverseasPartsFigures = new List<string>(); //查找新的零部件图号
                            List<string> listOverseasPartsFigures = new List<string>(); //查找老的零部件图号
                            foreach(var item in rightList) {
                                if(!string.IsNullOrEmpty(item.Code)) {
                                    listSpares.Add(item.Code);
                                }
                                if(!string.IsNullOrEmpty(item.ReferenceCode)) {
                                    lisOverseasPartsFigures.Add(item.ReferenceCode);
                                }
                            }
                            DataTable dtSparePart = new DataTable();
                            DataTable oldpartsExchanges = new DataTable();
                            DataTable newpartsExchanges = new DataTable();
                            if(listSpares.Count > 0) {
                                var codeStr = string.Join("','", listSpares.ToArray());
                                //string reqparts = getOracleSQLIn(listSpares, "Code");
                                var OverseasPartsFigureStr = string.Join("','", lisOverseasPartsFigures.ToArray());
                                var SpareParts = string.Format("select Id,Code,OverseasPartsFigure  from SparePart where Code in ('{0}') and Status=1", codeStr);
                                var cmdSpareParts = db.CreateDbCommand(SpareParts, conn, null);
                                dtSparePart.Load(cmdSpareParts.ExecuteReader());
                                if(lisOverseasPartsFigures.Count > 0) {
                                    //查找新的互换件
                                    var partsExchangesnew = string.Format("select e.Id as PartsExchangeId,e.ExchangeName as ExchangeName,e.PartId as PartId,e.ExchangeCode as ExchangeCode,s.Code as Code from PartsExchange e left join SparePart s on s.Id=e.PartId where e.ExchangeCode in ('{0}') and s.Code in ('{1}') and e.Status=1", OverseasPartsFigureStr, codeStr);
                                    var newcmdpartsExchanges = db.CreateDbCommand(partsExchangesnew, conn, null);
                                    newpartsExchanges.Load(newcmdpartsExchanges.ExecuteReader());
                                }

                                //查找老的互换件
                                foreach(DataRow drSparePart in dtSparePart.Rows) {
                                    if(!string.IsNullOrEmpty(drSparePart["OverseasPartsFigure"].ToString())) {
                                        listOverseasPartsFigures.Add(drSparePart["OverseasPartsFigure"].ToString());
                                    }
                                }
                                if(listOverseasPartsFigures.Count > 0) {
                                    var oldOverseasPartsFigureStr = string.Join("','", listOverseasPartsFigures.ToArray());
                                    var partsExchangesold = string.Format("select e.Id as PartsExchangeId,e.ExchangeName as ExchangeName,e.PartId as PartId,e.ExchangeCode as ExchangeCode,s.Code as Code from PartsExchange e left join SparePart s on s.Id=e.PartId where e.ExchangeCode in ('{0}') and s.Code in ('{1}') and e.Status=1", oldOverseasPartsFigureStr, codeStr);
                                    var oldcmdpartsExchanges = db.CreateDbCommand(partsExchangesold, conn, null);
                                    oldpartsExchanges.Load(oldcmdpartsExchanges.ExecuteReader());
                                }
                            }
                            var userInfo = Utils.GetCurrentUserInfo();
                            //往数据库增加配件信息
                            foreach(SparePartExtend item in rightList) {
                                var command = db.CreateDbCommand(sqlUpdate, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("Code", Strings.StrConv(item.Code, VbStrConv.Narrow).Replace("【", "[")));
                                command.Parameters.Add(db.CreateDbParameter("OverseasPartsFigure", item.OverseasPartsFigure));
                                command.Parameters.Add(db.CreateDbParameter("ModifierId", userInfo.Id));
                                command.Parameters.Add(db.CreateDbParameter("ModifierName", userInfo.Name));
                                command.Parameters.Add(db.CreateDbParameter("ModifyTime", DateTime.Now));
                                command.Parameters.Add(db.CreateDbParameter("OMSparePartMark", 0));
                                command.ExecuteNonQuery();

                                //                                //获取数据库的完整备件信息
                                //                                string[] partCode = new string[] { item.Code };

                                //                                Func<string[], bool> getDbSparePartInfo = vales => {
                                //                                    item.Id = Convert.ToInt32(vales[0]);
                                //                                    //item.Code = vales[1];
                                //                                    item.ReferenceCode = vales[3];
                                //                                    item.Name = vales[2];
                                //                                    item.LastSubstitute = vales[4];
                                //                                    item.NextSubstitute = vales[5];
                                //                                    item.ShelfLife = string.IsNullOrEmpty(vales[6]) ? 0 : Convert.ToInt32(vales[6]);
                                //                                    item.EnglishName = vales[7];
                                //                                    item.PinyinCode = vales[8];
                                //                                    //item.ReferenceCode = vales[9];
                                //                                    item.ReferenceName = vales[10];
                                //                                    item.CADCode = vales[11];
                                //                                    item.CADName = vales[12];
                                //                                    item.PartType = string.IsNullOrEmpty(vales[13]) ? 0 : Convert.ToInt32(vales[13]);
                                //                                    item.Specification = vales[14];
                                //                                    item.Feature = vales[15];
                                //                                    item.Status = string.IsNullOrEmpty(vales[16]) ? 0 : Convert.ToInt32(vales[16]);
                                //                                    item.Length = string.IsNullOrEmpty(vales[17]) ? 0 : Convert.ToDecimal(vales[17]);
                                //                                    item.Width = string.IsNullOrEmpty(vales[18]) ? 0 : Convert.ToDecimal(vales[18]);
                                //                                    item.Height = string.IsNullOrEmpty(vales[19]) ? 0 : Convert.ToDecimal(vales[19]);
                                //                                    item.Volume = string.IsNullOrEmpty(vales[20]) ? 0 : Convert.ToDecimal(vales[20]);
                                //                                    item.Weight = string.IsNullOrEmpty(vales[21]) ? 0 : Convert.ToDecimal(vales[21]);
                                //                                    item.Material = vales[22];
                                //                                    item.PackingAmount = string.IsNullOrEmpty(vales[23]) ? 0 : Convert.ToInt32(vales[23]);
                                //                                    item.PackingSpecification = vales[24];
                                //                                    item.PartsOutPackingCode = vales[25];
                                //                                    item.PartsInPackingCode = vales[26];
                                //                                    item.MeasureUnit = vales[27];
                                //                                    item.MInPackingAmount = string.IsNullOrEmpty(vales[28]) ? 0 : Convert.ToInt32(vales[28]);
                                //                                    return false;
                                //                                };

                                //                                db.QueryDataWithInOperator(@"select ID,
                                //                                                                    CODE,
                                //                                                                    NAME,
                                //                                                                    ReferenceCode,
                                //                                                                    LASTSUBSTITUTE,
                                //                                                                    NEXTSUBSTITUTE,
                                //                                                                    SHELFLIFE,
                                //                                                                    ENGLISHNAME,
                                //                                                                    PINYINCODE,
                                //                                                                    REFERENCECODE,
                                //                                                                    REFERENCENAME,
                                //                                                                    CADCODE,
                                //                                                                    CADNAME,
                                //                                                                    PARTTYPE,
                                //                                                                    SPECIFICATION,
                                //                                                                    FEATURE,
                                //                                                                    STATUS,
                                //                                                                    LENGTH,
                                //                                                                    WIDTH,
                                //                                                                    HEIGHT,
                                //                                                                    VOLUME,
                                //                                                                    WEIGHT,
                                //                                                                    MATERIAL,
                                //                                                                    PACKINGAMOUNT,
                                //                                                                    PACKINGSPECIFICATION,
                                //                                                                    PARTSOUTPACKINGCODE,
                                //                                                                    PARTSINPACKINGCODE,
                                //                                                                    MEASUREUNIT,
                                //                                                                    MINPACKINGAMOUNT,
                                //                                                                    OverseasPartsFigure
                                //
                                //                                                                from sparepart where status=1 ", "Code", true, partCode, getDbSparePartInfo);

                                //往数据库增加履历
                                var sparePart = dbSparePartCodes.Where(r => r.Code == item.Code).FirstOrDefault();
                                if(sparePart != null) {
                                    var commandHistory = db.CreateDbCommand(sqlInsertHistory, conn, ts);
                                    commandHistory.Parameters.Add(db.CreateDbParameter("SparePartId", sparePart.Id));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Code", Strings.StrConv(sparePart.Code, VbStrConv.Narrow).Replace("【", "[")));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Name", sparePart.Name));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("IsNotWarrantyTransfer", sparePart.IsNotWarrantyTransfer));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("ReferenceCode", sparePart.ReferenceCode));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("OverseasPartsFigure", item.OverseasPartsFigure));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("CADCode", sparePart.CADCode));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Specification", sparePart.Specification));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("EnglishName", sparePart.EnglishName));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("PartType", sparePart.PartType));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("MeasureUnit", sparePart.MeasureUnit));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("ShelfLife", sparePart.ShelfLife));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("LastSubstitute", sparePart.LastSubstitute));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("NextSubstitute", sparePart.NextSubstitute));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Weight", sparePart.Weight));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Feature", sparePart.Feature));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Status", sparePart.Status));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("ExchangeIdentification", sparePart.ExchangeIdentification));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("MInPackingAmount", sparePart.MInPackingAmount));
                                    commandHistory.ExecuteNonQuery();
                                }


                                if(dtSparePart.Rows.Count > 0) {
                                    var drSparePart = dtSparePart.Select(" Code='" + item.Code + "'");
                                    if(drSparePart.Length > 0) {
                                        //修改零部件图号校验
                                        if(drSparePart[0]["OverseasPartsFigure"].ToString() != item.OverseasPartsFigure) {
                                            if(string.IsNullOrEmpty(item.OverseasPartsFigure)) {
                                                if(oldpartsExchanges.Rows.Count > 0) {
                                                    var droldpartsExchanges = oldpartsExchanges.Select(" Code='" + item.Code + "' And ExchangeCode='" + drSparePart[0]["OverseasPartsFigure"] + "'");
                                                    if(droldpartsExchanges.Length > 0) {
                                                        foreach(var droldpartsExchange in droldpartsExchanges) {
                                                            var commandpartsExchange = db.CreateDbCommand(sqlUpdatePartsExchangeStatus, conn, ts);
                                                            commandpartsExchange.Parameters.Add(db.CreateDbParameter("Status", 99));
                                                            commandpartsExchange.Parameters.Add(db.CreateDbParameter("ExchangeCode", droldpartsExchange["ExchangeCode"]));
                                                            commandpartsExchange.Parameters.Add(db.CreateDbParameter("PartId", droldpartsExchange["PartId"]));
                                                            commandpartsExchange.ExecuteNonQuery();

                                                            var commandPartsExchangeHistory = db.CreateDbCommand(sqlInsertPartsExchangeHistory, conn, ts);
                                                            commandPartsExchangeHistory.Parameters.Add(db.CreateDbParameter("PartsExchangeId", droldpartsExchange["PartsExchangeId"]));
                                                            commandPartsExchangeHistory.Parameters.Add(db.CreateDbParameter("ExchangeCode", droldpartsExchange["ExchangeCode"]));
                                                            commandPartsExchangeHistory.Parameters.Add(db.CreateDbParameter("ExchangeName", droldpartsExchange["ExchangeName"]));
                                                            commandPartsExchangeHistory.Parameters.Add(db.CreateDbParameter("PartId", droldpartsExchange["PartId"]));
                                                            commandPartsExchangeHistory.Parameters.Add(db.CreateDbParameter("Status", 1));
                                                            commandPartsExchangeHistory.Parameters.Add(db.CreateDbParameter("Remark", ErrorStrings.Export_Validation_Sparepart_RemarkDefault2));
                                                            commandPartsExchangeHistory.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                                            commandPartsExchangeHistory.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                                            commandPartsExchangeHistory.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                                            commandPartsExchangeHistory.ExecuteNonQuery();
                                                        }
                                                    }
                                                }
                                            } else {
                                                if(oldpartsExchanges.Rows.Count > 0) {
                                                    var droldpartsExchanges = oldpartsExchanges.Select(" Code='" + item.Code + "' And ExchangeCode='" + drSparePart[0]["OverseasPartsFigure"] + "'");
                                                    if(droldpartsExchanges.Length > 0) {
                                                        foreach(var droldpartsExchange in droldpartsExchanges) {
                                                            var commandpartsExchange = db.CreateDbCommand(sqlUpdatePartsExchangeStatus, conn, ts);
                                                            commandpartsExchange.Parameters.Add(db.CreateDbParameter("Status", 99));
                                                            commandpartsExchange.Parameters.Add(db.CreateDbParameter("ExchangeCode", droldpartsExchange["ExchangeCode"]));
                                                            commandpartsExchange.Parameters.Add(db.CreateDbParameter("PartId", droldpartsExchange["PartId"]));
                                                            commandpartsExchange.ExecuteNonQuery();

                                                            var commandPartsExchangeHistory = db.CreateDbCommand(sqlInsertPartsExchangeHistory, conn, ts);
                                                            commandPartsExchangeHistory.Parameters.Add(db.CreateDbParameter("PartsExchangeId", droldpartsExchange["PartsExchangeId"]));
                                                            commandPartsExchangeHistory.Parameters.Add(db.CreateDbParameter("ExchangeCode", droldpartsExchange["ExchangeCode"]));
                                                            commandPartsExchangeHistory.Parameters.Add(db.CreateDbParameter("ExchangeName", droldpartsExchange["ExchangeName"]));
                                                            commandPartsExchangeHistory.Parameters.Add(db.CreateDbParameter("PartId", droldpartsExchange["PartId"]));
                                                            commandPartsExchangeHistory.Parameters.Add(db.CreateDbParameter("Status", 1));
                                                            commandPartsExchangeHistory.Parameters.Add(db.CreateDbParameter("Remark", ErrorStrings.Export_Validation_Sparepart_RemarkDefault2));
                                                            commandPartsExchangeHistory.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                                            commandPartsExchangeHistory.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                                            commandPartsExchangeHistory.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                                            commandPartsExchangeHistory.ExecuteNonQuery();
                                                        }
                                                    }
                                                }
                                                if(newpartsExchanges.Rows.Count > 0) {
                                                    var drnewpartsExchanges = newpartsExchanges.Select(" Code='" + item.Code + "' And ExchangeCode='" + item.ReferenceCode + "'");
                                                    if(drnewpartsExchanges.Length > 0) {
                                                        var commandpartsExchange = db.CreateDbCommand(sqlInsertPartsExchange, conn, ts);
                                                        commandpartsExchange.Parameters.Add(db.CreateDbParameter("ExchangeCode", drnewpartsExchanges[0]["ExchangeCode"]));
                                                        commandpartsExchange.Parameters.Add(db.CreateDbParameter("ExchangeName", drnewpartsExchanges[0]["ExchangeName"]));
                                                        commandpartsExchange.Parameters.Add(db.CreateDbParameter("PartId", drnewpartsExchanges[0]["PartId"]));
                                                        commandpartsExchange.Parameters.Add(db.CreateDbParameter("Status", 1));
                                                        commandpartsExchange.Parameters.Add(db.CreateDbParameter("Remark", ErrorStrings.Export_Validation_Sparepart_RemarkDefault3));
                                                        commandpartsExchange.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                                        commandpartsExchange.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                                        commandpartsExchange.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                                        var id = db.ExecuteInsert(commandpartsExchange, "Id");
                                                        var commandPartsExchangeHistory = db.CreateDbCommand(sqlInsertPartsExchangeHistory, conn, ts);
                                                        commandPartsExchangeHistory.Parameters.Add(db.CreateDbParameter("PartsExchangeId", id));
                                                        commandPartsExchangeHistory.Parameters.Add(db.CreateDbParameter("ExchangeCode", drnewpartsExchanges[0]["ExchangeCode"]));
                                                        commandPartsExchangeHistory.Parameters.Add(db.CreateDbParameter("ExchangeName", drnewpartsExchanges[0]["ExchangeName"]));
                                                        commandPartsExchangeHistory.Parameters.Add(db.CreateDbParameter("PartId", drnewpartsExchanges[0]["PartId"]));
                                                        commandPartsExchangeHistory.Parameters.Add(db.CreateDbParameter("Status", 1));
                                                        commandPartsExchangeHistory.Parameters.Add(db.CreateDbParameter("Remark", ErrorStrings.Export_Validation_Sparepart_RemarkDefault3));
                                                        commandPartsExchangeHistory.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                                        commandPartsExchangeHistory.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                                        commandPartsExchangeHistory.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                                        commandPartsExchangeHistory.ExecuteNonQuery();
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                        }
                        //无异常提交
                        ts.Commit();
                    } catch(Exception ex) {
                        //报错回滚
                        ts.Rollback();
                        throw new Exception(ex.Message);
                    } finally {
                        if(conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }

        public bool 批量导入修改金税分类编码(string fileName, out int excelImportNum, out List<SparePartExtend> rightData, out List<SparePartExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<SparePartExtend>();
            var rightList = new List<SparePartExtend>();
            var allList = new List<SparePartExtend>();
            var dbSparePartCodes = new List<SparePartExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("SparePart", out notNullableFields, out fieldLenght);
                List<object> excelColumns;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Code, "Code");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_GoldenTaxClassifyMsg_GoldenTaxClassifyCode, "GoldenTaxClassifyCode");
                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var tempImportObj = new SparePartExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        #region
                        tempImportObj.Code = newRow["Code"];
                        tempImportObj.GoldenTaxClassifyCode = newRow["GoldenTaxClassifyCode"];
                        #endregion

                        var tempErrorMessage = new List<string>();

                        #region 导入的内容基本检查
                        if(string.IsNullOrEmpty(tempImportObj.Code)) {
                            tempErrorMessage.Add(ErrorStrings.OverstockPartsAppDetail__Validation1);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.Code) > fieldLenght["Code".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Sparepart_CodeOverLong);
                        }

                        if(string.IsNullOrEmpty(tempImportObj.GoldenTaxClassifyCode)) {
                            tempErrorMessage.Add(ErrorStrings.Export_Validation_Sparepart_GoldenTaxClassifyCodeIsNull);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.GoldenTaxClassifyCode) > fieldLenght["GoldenTaxClassifyCode".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Sparepart_GoldenTaxClassifyCodeIsLong);
                        }

                        #endregion

                        if(tempErrorMessage.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });
                    //校验导入的数据业务校验
                    var groups = allList.GroupBy(r =>
                        r.Code
                    ).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        if(groupItem.ErrorMsg == null) {
                            groupItem.ErrorMsg = ErrorStrings.Export_Title_PartsBranch_Code + groupItem.Code + ErrorStrings.Export_Validation_PartsBranch_Validation5;
                        } else {
                            groupItem.ErrorMsg = groupItem.ErrorMsg + ErrorStrings.Export_Validation_PartsBranch_Validation6 + groupItem.Code + ErrorStrings.Export_Validation_PartsBranch_Validation5;
                        }
                    }
                    var sparePartsNeedCheck = allList.ToList();
                    var partCodesNeedCheck = sparePartsNeedCheck.Select(r => r.Code).Distinct().ToArray();

                    Func<string[], bool> getDbSparePartCodes = vales => {
                        var dbSparePart = new SparePartExtend();
                        dbSparePart.Id = Convert.ToInt32(vales[0]);
                        dbSparePart.Code = vales[1];
                        dbSparePart.Name = vales[2];
                        dbSparePart.ReferenceCode = vales[3];
                        dbSparePart.LastSubstitute = vales[4];
                        dbSparePart.NextSubstitute = vales[5];
                        dbSparePart.ShelfLife = string.IsNullOrEmpty(vales[6]) ? 0 : Convert.ToInt32(vales[6]);
                        dbSparePart.EnglishName = vales[7];
                        dbSparePart.PinyinCode = vales[8];
                        dbSparePart.IMSManufacturerNumber = vales[9];
                        dbSparePart.ReferenceName = vales[10];
                        dbSparePart.CADCode = vales[11];
                        dbSparePart.CADName = vales[12];
                        dbSparePart.PartType = string.IsNullOrEmpty(vales[13]) ? 0 : Convert.ToInt32(vales[13]);
                        dbSparePart.Specification = vales[14];
                        dbSparePart.Feature = vales[15];
                        dbSparePart.Status = string.IsNullOrEmpty(vales[16]) ? 0 : Convert.ToInt32(vales[16]);
                        dbSparePart.Length = string.IsNullOrEmpty(vales[17]) ? 0 : Convert.ToDecimal(vales[17]);
                        dbSparePart.Width = string.IsNullOrEmpty(vales[18]) ? 0 : Convert.ToDecimal(vales[18]);
                        dbSparePart.Height = string.IsNullOrEmpty(vales[19]) ? 0 : Convert.ToDecimal(vales[19]);
                        dbSparePart.Volume = string.IsNullOrEmpty(vales[20]) ? 0 : Convert.ToDecimal(vales[20]);
                        dbSparePart.Weight = string.IsNullOrEmpty(vales[21]) ? 0 : Convert.ToDecimal(vales[21]);
                        dbSparePart.Material = vales[22];
                        dbSparePart.PackingAmount = string.IsNullOrEmpty(vales[23]) ? 0 : Convert.ToInt32(vales[23]);
                        dbSparePart.PackingSpecification = vales[24];
                        dbSparePart.PartsOutPackingCode = vales[25];
                        dbSparePart.PartsInPackingCode = vales[26];
                        dbSparePart.MeasureUnit = vales[27];
                        dbSparePart.MInPackingAmount = string.IsNullOrEmpty(vales[28]) ? 0 : Convert.ToInt32(vales[28]);
                        dbSparePart.GroupABCCategory = string.IsNullOrEmpty(vales[29]) ? 0 : Convert.ToInt32(vales[29]);
                        dbSparePart.IsNotWarrantyTransfer = string.IsNullOrEmpty(vales[30]) ? 0 : Convert.ToInt32(vales[30]);
                        dbSparePart.ExchangeIdentification = vales[31];
                        dbSparePart.DeclareElement = vales[32];
                        dbSparePartCodes.Add(dbSparePart);
                        return false;
                    };
                    //根据SQL查询数据库，将查出来的结果根据定义的委托中的方法（这里是getDbSparePartCodes）放到具体的集合中（这里是dbSparePartCodes）
                    db.QueryDataWithInOperator(@"select ID,
                                                                    CODE,
                                                                    NAME,
                                                                    ReferenceCode,
                                                                    LASTSUBSTITUTE,
                                                                    NEXTSUBSTITUTE,
                                                                    SHELFLIFE,
                                                                    ENGLISHNAME,
                                                                    PINYINCODE,
                                                                    IMSManufacturerNumber,
                                                                    REFERENCENAME,
                                                                    CADCODE,
                                                                    CADNAME,
                                                                    PARTTYPE,
                                                                    SPECIFICATION,
                                                                    FEATURE,
                                                                    STATUS,
                                                                    LENGTH,
                                                                    WIDTH,
                                                                    HEIGHT,
                                                                    VOLUME,
                                                                    WEIGHT,
                                                                    MATERIAL,
                                                                    PACKINGAMOUNT,
                                                                    PACKINGSPECIFICATION,
                                                                    PARTSOUTPACKINGCODE,
                                                                    PARTSINPACKINGCODE,
                                                                    MEASUREUNIT,
                                                                    MINPACKINGAMOUNT,
                                                                    GroupABCCategory,IsNotWarrantyTransfer,ExchangeIdentification,DeclareElement
                                                                from sparepart where status=1", "Code", false, partCodesNeedCheck, getDbSparePartCodes);
                    if(partCodesNeedCheck.Length > 0) {
                        var sparePartsExistsCode = sparePartsNeedCheck.Where(r => dbSparePartCodes.Any(v => v.Code == r.Code)).ToArray();
                        if(sparePartsExistsCode.Count() < sparePartsNeedCheck.Count()) {
                            var notExist = allList.Except(sparePartsExistsCode);
                            foreach(var item in notExist) {
                                if(item.ErrorMsg == null) {
                                    item.ErrorMsg = ErrorStrings.Export_Validation_PartsBranch_Validation8 + item.Code + ErrorStrings.Export_Validation_Sparepart_NotExistOrStop;
                                } else {
                                    item.ErrorMsg = item.ErrorMsg + ErrorStrings.Export_Validation_PartsBranch_Validation9 + item.Code + ErrorStrings.Export_Validation_Sparepart_NotExistOrStop;
                                }
                                errorList.Add(item);
                            }
                        }
                    }
                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    var goldenTaxClassifyCode = tempRightList.Select(r => r.GoldenTaxClassifyCode).Distinct().ToArray();
                    var dbGoldenTaxClassifyMsgs = new List<GoldenTaxClassifyMsgExtend>();
                    Func<string[], bool> getDbGoldenTaxClassifyMsgs = value => {
                        var dbObj = new GoldenTaxClassifyMsgExtend {
                            Id = Convert.ToInt32(value[0]),
                            GoldenTaxClassifyCode = value[1],
                            GoldenTaxClassifyName = value[2]
                        };
                        dbGoldenTaxClassifyMsgs.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperatorByLower("select Id,GoldenTaxClassifyCode,GoldenTaxClassifyName from GoldenTaxClassifyMsg ", "GoldenTaxClassifyCode", true, goldenTaxClassifyCode, getDbGoldenTaxClassifyMsgs);
                    if(dbGoldenTaxClassifyMsgs.Count == 0) {
                        foreach(var errorItem in tempRightList) {
                            errorItem.ErrorMsg = ErrorStrings.Export_Validation_Sparepart_GoldenTaxClassifyNotExist;
                        }
                    } else {
                        var errorGoldenTaxClassifyMsgs = tempRightList.Where(r => !dbGoldenTaxClassifyMsgs.Any(v => v.GoldenTaxClassifyCode == r.GoldenTaxClassifyCode)).ToList();
                        foreach(var errorItem in errorGoldenTaxClassifyMsgs) {
                            errorItem.ErrorMsg = ErrorStrings.Export_Validation_Sparepart_GoldenTaxClassifyNotExist;
                        }
                    }
                    //获取所有不合格数据
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    rightList = allList.Except(errorList).ToList();
                    //导出所有不合格数据
                    if(errorList.Any()) {
                        excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                        errorDataFileName = GetErrorFilePath(fileName);
                        using(var excelExport = new ExcelExport(errorDataFileName)) {
                            var list = errorList;
                            excelExport.ExportByRow(index => {
                                if(index == list.Count + 1)
                                    return null;
                                if(index == 0)
                                    return excelColumns.ToArray();
                                var tempObj = list[index - 1];
                                var values = new object[] {
                                tempObj.Code,
                                tempObj.GoldenTaxClassifyCode,
                                tempObj.ErrorMsg
                            };
                                return values;
                            });
                        }
                        errorList = null;
                    }
                    //更新导入所有合格数据
                    if(!rightList.Any())
                        return true;
                    using(var conn = db.CreateDbConnection()) {
                        conn.Open();
                        //开启事务，新增更新配件在一个事务内
                        var ts = conn.BeginTransaction();
                        try {
                            //新增配件
                            if(rightList.Any()) {
                                var sqlUpdate = db.GetUpdateSql("SparePart", new string[] { "GoldenTaxClassifyId", "GoldenTaxClassifyCode", "GoldenTaxClassifyName", "ModifierId", "ModifierName", "ModifyTime", "OMSparePartMark" }, new string[] { "Code" });
                                var sqlInsertHistory = db.GetInsertSql("SparePartHistory", "Id", new[] {
                                  "SparePartId","Code","ReferenceCode","IMSCompressionNumber","IsNotWarrantyTransfer","IMSManufacturerNumber", "Name", "CADCode", "Specification", "EnglishName", "PartType", "MeasureUnit", "ShelfLife", "LastSubstitute", "NextSubstitute", "Weight","Volume","GroupABCCategory", "Feature", "status", "CreatorId", "CreatorName", "CreateTime","MInPackingAmount","GoldenTaxClassifyId", "GoldenTaxClassifyCode", "GoldenTaxClassifyName","ExchangeIdentification","DeclareElement"
                                });

                                var userInfo = Utils.GetCurrentUserInfo();
                                //往数据库增加配件信息
                                foreach(SparePartExtend item in rightList) {
                                    var goldenTaxClassify = dbGoldenTaxClassifyMsgs.Where(r => r.GoldenTaxClassifyCode == item.GoldenTaxClassifyCode).FirstOrDefault();
                                    var command = db.CreateDbCommand(sqlUpdate, conn, ts);
                                    command.Parameters.Add(db.CreateDbParameter("Code", Strings.StrConv(item.Code, VbStrConv.Narrow).Replace("【", "[")));
                                    command.Parameters.Add(db.CreateDbParameter("GoldenTaxClassifyId", goldenTaxClassify.Id));
                                    command.Parameters.Add(db.CreateDbParameter("GoldenTaxClassifyCode", goldenTaxClassify.GoldenTaxClassifyCode));
                                    command.Parameters.Add(db.CreateDbParameter("GoldenTaxClassifyName", goldenTaxClassify.GoldenTaxClassifyName));
                                    command.Parameters.Add(db.CreateDbParameter("ModifierId", userInfo.Id));
                                    command.Parameters.Add(db.CreateDbParameter("ModifierName", userInfo.Name));
                                    command.Parameters.Add(db.CreateDbParameter("ModifyTime", DateTime.Now));
                                    command.Parameters.Add(db.CreateDbParameter("OMSparePartMark", 0));
                                    command.ExecuteNonQuery();

                                    #region 注释无用代码
                                    //                                    //获取数据库的完整备件信息
                                    //                                    string[] partCode = new string[] { item.Code };

                                    //                                    Func<string[], bool> getDbSparePartInfo = vales => {
                                    //                                        item.Id = Convert.ToInt32(vales[0]);
                                    //                                        //item.Code = vales[1];
                                    //                                        item.Name = vales[2];
                                    //                                        item.ReferenceCode = vales[3];
                                    //                                        item.LastSubstitute = vales[4];
                                    //                                        item.NextSubstitute = vales[5];
                                    //                                        item.ShelfLife = string.IsNullOrEmpty(vales[6]) ? 0 : Convert.ToInt32(vales[6]);
                                    //                                        item.EnglishName = vales[7];
                                    //                                        item.PinyinCode = vales[8];
                                    //                                        item.IMSManufacturerNumber = vales[9];
                                    //                                        item.ReferenceName = vales[10];
                                    //                                        item.CADCode = vales[11];
                                    //                                        item.CADName = vales[12];
                                    //                                        item.PartType = string.IsNullOrEmpty(vales[13]) ? 0 : Convert.ToInt32(vales[13]);
                                    //                                        item.Specification = vales[14];
                                    //                                        item.Feature = vales[15];
                                    //                                        item.Status = string.IsNullOrEmpty(vales[16]) ? 0 : Convert.ToInt32(vales[16]);
                                    //                                        item.Length = string.IsNullOrEmpty(vales[17]) ? 0 : Convert.ToDecimal(vales[17]);
                                    //                                        item.Width = string.IsNullOrEmpty(vales[18]) ? 0 : Convert.ToDecimal(vales[18]);
                                    //                                        item.Height = string.IsNullOrEmpty(vales[19]) ? 0 : Convert.ToDecimal(vales[19]);
                                    //                                        item.Volume = string.IsNullOrEmpty(vales[20]) ? 0 : Convert.ToDecimal(vales[20]);
                                    //                                        item.Weight = string.IsNullOrEmpty(vales[21]) ? 0 : Convert.ToDecimal(vales[21]);
                                    //                                        item.Material = vales[22];
                                    //                                        item.PackingAmount = string.IsNullOrEmpty(vales[23]) ? 0 : Convert.ToInt32(vales[23]);
                                    //                                        item.PackingSpecification = vales[24];
                                    //                                        item.PartsOutPackingCode = vales[25];
                                    //                                        item.PartsInPackingCode = vales[26];
                                    //                                        item.MeasureUnit = vales[27];
                                    //                                        item.MInPackingAmount = string.IsNullOrEmpty(vales[28]) ? 0 : Convert.ToInt32(vales[28]);
                                    //                                        item.GroupABCCategory = string.IsNullOrEmpty(vales[29]) ? 0 : Convert.ToInt32(vales[29]);
                                    //                                        return false;
                                    //                                    };

                                    //                                    db.QueryDataWithInOperator(@"select ID,
                                    //                                                                    CODE,
                                    //                                                                    NAME,
                                    //                                                                    ReferenceCode,
                                    //                                                                    LASTSUBSTITUTE,
                                    //                                                                    NEXTSUBSTITUTE,
                                    //                                                                    SHELFLIFE,
                                    //                                                                    ENGLISHNAME,
                                    //                                                                    PINYINCODE,
                                    //                                                                    IMSManufacturerNumber,
                                    //                                                                    REFERENCENAME,
                                    //                                                                    CADCODE,
                                    //                                                                    CADNAME,
                                    //                                                                    PARTTYPE,
                                    //                                                                    SPECIFICATION,
                                    //                                                                    FEATURE,
                                    //                                                                    STATUS,
                                    //                                                                    LENGTH,
                                    //                                                                    WIDTH,
                                    //                                                                    HEIGHT,
                                    //                                                                    VOLUME,
                                    //                                                                    WEIGHT,
                                    //                                                                    MATERIAL,
                                    //                                                                    PACKINGAMOUNT,
                                    //                                                                    PACKINGSPECIFICATION,
                                    //                                                                    PARTSOUTPACKINGCODE,
                                    //                                                                    PARTSINPACKINGCODE,
                                    //                                                                    MEASUREUNIT,
                                    //                                                                    MINPACKINGAMOUNT,
                                    //                                                                    GroupABCCategory
                                    //                                                                from sparepart where status=1 ", "Code", true, partCode, getDbSparePartInfo); 
                                    #endregion


                                    //往数据库增加履历
                                    var sparePart = dbSparePartCodes.Where(r => r.Code == item.Code).FirstOrDefault();
                                    if(sparePart != null) {
                                        var commandHistory = db.CreateDbCommand(sqlInsertHistory, conn, ts);
                                        commandHistory.Parameters.Add(db.CreateDbParameter("SparePartId", sparePart.Id));
                                        commandHistory.Parameters.Add(db.CreateDbParameter("Code", Strings.StrConv(sparePart.Code, VbStrConv.Narrow).Replace("【", "[")));
                                        commandHistory.Parameters.Add(db.CreateDbParameter("Name", sparePart.Name));
                                        commandHistory.Parameters.Add(db.CreateDbParameter("IsNotWarrantyTransfer", sparePart.IsNotWarrantyTransfer));
                                        commandHistory.Parameters.Add(db.CreateDbParameter("ReferenceCode", sparePart.ReferenceCode));
                                        commandHistory.Parameters.Add(db.CreateDbParameter("IMSCompressionNumber", sparePart.IMSCompressionNumber));
                                        commandHistory.Parameters.Add(db.CreateDbParameter("IMSManufacturerNumber", sparePart.IMSManufacturerNumber));
                                        commandHistory.Parameters.Add(db.CreateDbParameter("CADCode", sparePart.CADCode));
                                        commandHistory.Parameters.Add(db.CreateDbParameter("Specification", sparePart.Specification));
                                        commandHistory.Parameters.Add(db.CreateDbParameter("EnglishName", sparePart.EnglishName));
                                        commandHistory.Parameters.Add(db.CreateDbParameter("PartType", sparePart.PartType));
                                        commandHistory.Parameters.Add(db.CreateDbParameter("MeasureUnit", sparePart.MeasureUnit));
                                        commandHistory.Parameters.Add(db.CreateDbParameter("ShelfLife", sparePart.ShelfLife));
                                        commandHistory.Parameters.Add(db.CreateDbParameter("LastSubstitute", sparePart.LastSubstitute));
                                        commandHistory.Parameters.Add(db.CreateDbParameter("NextSubstitute", sparePart.NextSubstitute));
                                        commandHistory.Parameters.Add(db.CreateDbParameter("Weight", sparePart.Weight));
                                        commandHistory.Parameters.Add(db.CreateDbParameter("Feature", sparePart.Feature));
                                        commandHistory.Parameters.Add(db.CreateDbParameter("Status", sparePart.Status));
                                        commandHistory.Parameters.Add(db.CreateDbParameter("Volume", sparePart.Volume));
                                        commandHistory.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                        commandHistory.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                        commandHistory.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                        commandHistory.Parameters.Add(db.CreateDbParameter("MInPackingAmount", sparePart.MInPackingAmount));
                                        commandHistory.Parameters.Add(db.CreateDbParameter("GroupABCCategory", sparePart.GroupABCCategory));
                                        commandHistory.Parameters.Add(db.CreateDbParameter("GoldenTaxClassifyId", goldenTaxClassify.Id));
                                        commandHistory.Parameters.Add(db.CreateDbParameter("GoldenTaxClassifyCode", goldenTaxClassify.GoldenTaxClassifyCode));
                                        commandHistory.Parameters.Add(db.CreateDbParameter("GoldenTaxClassifyName", goldenTaxClassify.GoldenTaxClassifyName));
                                        commandHistory.Parameters.Add(db.CreateDbParameter("ExchangeIdentification", sparePart.ExchangeIdentification));
                                        commandHistory.Parameters.Add(db.CreateDbParameter("DeclareElement", sparePart.DeclareElement));
                                        commandHistory.ExecuteNonQuery();
                                    }
                                }
                            }
                            //无异常提交
                            ts.Commit();
                        } catch(Exception) {
                            //报错回滚
                            ts.Rollback();
                            throw;
                        } finally {
                            if(conn.State == System.Data.ConnectionState.Open) {
                                conn.Close();
                            }
                        }
                    }
                    return true;
                }
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }

        }

        public bool 批量替换配件互换识别号(string fileName, out int excelImportNum, out List<SparePartExtend> rightData, out List<SparePartExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<SparePartExtend>();
            var rightList = new List<SparePartExtend>();
            var allList = new List<SparePartExtend>();
            var oldspareParts = new List<SparePartExtend>();
            var dbpartsExchangeGroups = new List<PartsExchangeGroupExtend>();
            var dbpartsExchanges = new List<PartsExchangeGroupExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);
                List<object> excelColumns;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Code, "Code");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName, "Name");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsExchangeGroupExtend_ExchangeCode, "ExchangeIdentification");
                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    //单次最大导入条数不能大于300
                    if(excelOperator.LastRowNum > 2000) {
                        throw new Exception(ErrorStrings.Export_Validation_Sparepart_Item);
                    }

                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var tempImportObj = new SparePartExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        tempImportObj.Code = newRow["Code"];
                        tempImportObj.Name = newRow["Name"];
                        tempImportObj.ExchangeIdentification = newRow["ExchangeIdentification"];
                        var tempErrorMessage = new List<string>();

                        //导入模板必填
                        if(string.IsNullOrEmpty(tempImportObj.Code)) {
                            tempErrorMessage.Add(ErrorStrings.Export_Validation_Sparepart_CodeIsNull);
                        }
                        if(tempErrorMessage.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });
                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    var groups = tempRightList.GroupBy(r => new {
                        r.Code
                    }).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        groupItem.ErrorMsg = ErrorStrings.ImpPartsSupplierRelation_Validation31;
                    }
                    var sparePartsNeedCheck = allList.ToList();
                    var partCodesNeedCheck = sparePartsNeedCheck.Select(r => r.Code).Distinct().ToArray();
                    var dbSparePartCodes = new List<string>();
                    Func<string[], bool> getDbSparePartCodes = vales => {
                        dbSparePartCodes.Add(vales[0]);
                        return false;
                    };
                    rightList = allList.Where(r => r.ErrorMsg != null).ToList();
                    //根据SQL查询数据库，将查出来的结果根据定义的委托中的方法（这里是getDbSparePartCodes）放到具体的集合中（这里是dbSparePartCodes）
                    db.QueryDataWithInOperator("select Code from sparePart where status!=99 ", "Code", false, partCodesNeedCheck, getDbSparePartCodes);

                    foreach(var item in sparePartsNeedCheck) {
                        if(!dbSparePartCodes.Any(r => r == item.Code)) {
                            item.ErrorMsg = ErrorStrings.Export_Validation_PartsBranch_Validation8 + item.Code + ErrorStrings.Export_Validation_Sparepart_NotExist;
                        }
                    }

                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    rightList = allList.Except(errorList).ToList();
                    var spareParts = rightList.ToList();
                    var sparePartCodes = spareParts.Select(r => r.Code).Distinct().ToArray();
                    Func<string[], bool> getoldspareParts = values => {
                        oldspareParts.Add(new SparePartExtend {
                            Code = values[0],
                            ExchangeIdentification = values[1],
                            Id = int.Parse(values[2]),
                            ReferenceName = values[3],
                            Name = values[4]
                        });
                        return false;
                    };
                    db.QueryDataWithInOperator("select Code,ExchangeIdentification,Id,ReferenceName,Name from sparePart where status!=99 ", "Code", false, sparePartCodes, getoldspareParts);

                    //查找对应的互换组
                    var partsExchangeGroups = rightList.Select(r => r.ExchangeIdentification).Distinct().ToArray();

                    Func<string[], bool> getDbpartsExchangeGroups = values => {
                        var item = new PartsExchangeGroupExtend {
                            Id = int.Parse(values[0]),
                            ExGroupCode = values[1].ToString(),
                            ExchangeCode = values[2].ToString(),
                            PartCode = values[3].ToString()
                        };
                        dbpartsExchangeGroups.Add(item);
                        return false;
                    };
                    db.QueryDataWithInOperator("select p.Id,p.ExGroupCode,p.ExchangeCode,s.Code from PartsExchangeGroup p inner join SparePart s on s.ExchangeIdentification=p.ExchangeCode where p.Status=1", "p.ExchangeCode", false, partsExchangeGroups, getDbpartsExchangeGroups);


                    //查找对应的互换信息

                    Func<string[], bool> getDbpartsExchanges = values => {
                        var item = new PartsExchangeGroupExtend {
                            Id = int.Parse(values[0]),
                            ExchangeCode = values[1].ToString(),
                            PartCode = values[2].ToString()
                        };
                        dbpartsExchanges.Add(item);
                        return false;
                    };
                    db.QueryDataWithInOperator("select p.Id,p.ExchangeCode,s.Code from PartsExchange p inner join SparePart s on s.ExchangeIdentification=p.ExchangeCode where p.Status=1", "ExchangeCode", false, partsExchangeGroups, getDbpartsExchanges);

                }

                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempObj = list[index - 1];
                            var values = new object[] {
                                tempObj.Code,
                                tempObj.Name,
                                tempObj.ExchangeIdentification,
                                tempObj.ErrorMsg
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }

                //导入所有合格数据  
                if(!rightList.Any())
                    return true;
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    if(rightList.Any()) {
                        var userInfo = Utils.GetCurrentUserInfo();
                        var sqlUpdatePartsExchangeStatus = db.GetUpdateSql("PartsExchange", new string[] {
                            "Status", "AbandonerId", "AbandonerName", "AbandonTime","Remark"
                        }, new string[] {
                            "PartId"
                        });
                        var sqlInsertPartsExchange = db.GetInsertSql("PartsExchange", "Id", new[] {
                            "ExchangeCode", "ExchangeName", "PartId", "Status", "Remark", "CreatorId", "CreatorName", "CreateTime"
                        });
                        var sqlInsertPartsExchangeHistory = db.GetInsertSql("PartsExchangeHistory", "Id", new[] {
                            "PartsExchangeId", "ExchangeCode", "ExchangeName", "PartId", "Status", "Remark", "CreatorId", "CreatorName", "CreateTime"
                        });
                        var sqlInsertPartsExchangeGroup = db.GetInsertSql("PartsExchangeGroup", "Id", new[] {
                            "ExGroupCode", "ExchangeCode", "Status", "CreatorId", "CreatorName", "CreateTime"
                        });
                        var sqlUpdateSparePart = db.GetUpdateSql("SparePart", new string[] {
                            "ExchangeIdentification","ModifierId","ModifyTime","ModifierName","OMSparePartMark"
                        }, new string[] {
                            "Code"
                        });
                        var ts = conn.BeginTransaction();
                        try {
                            foreach(var item in rightList) {
                                var oldsparePart = oldspareParts.Where(r => r.Code == item.Code).FirstOrDefault();
                                string code = Strings.StrConv(item.Code, VbStrConv.Narrow).Replace("【", "[");
                                var command = db.CreateDbCommand(sqlUpdateSparePart, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("ModifyTime", DateTime.Now));
                                command.Parameters.Add(db.CreateDbParameter("ModifierId", userInfo.Id));
                                command.Parameters.Add(db.CreateDbParameter("ModifierName", userInfo.Name));
                                command.Parameters.Add(db.CreateDbParameter("ExchangeIdentification", item.ExchangeIdentification));
                                command.Parameters.Add(db.CreateDbParameter("Code", code));
                                command.Parameters.Add(db.CreateDbParameter("OMSparePartMark", 0));
                                command.ExecuteNonQuery();
                                var commandUpdatePartsExchange = db.CreateDbCommand(sqlUpdatePartsExchangeStatus, conn, ts);
                                commandUpdatePartsExchange.Parameters.Add(db.CreateDbParameter("Status", 99));
                                commandUpdatePartsExchange.Parameters.Add(db.CreateDbParameter("AbandonerId", userInfo.Id));
                                commandUpdatePartsExchange.Parameters.Add(db.CreateDbParameter("AbandonerName", userInfo.Name));
                                commandUpdatePartsExchange.Parameters.Add(db.CreateDbParameter("AbandonTime", DateTime.Now));
                                commandUpdatePartsExchange.Parameters.Add(db.CreateDbParameter("PartId", oldsparePart.Id));
                                commandUpdatePartsExchange.Parameters.Add(db.CreateDbParameter("Remark", ErrorStrings.Export_Validation_Sparepart_RemarkDefault4));
                                commandUpdatePartsExchange.ExecuteNonQuery();
                                if(!string.IsNullOrEmpty(item.ExchangeIdentification)) {
                                    var commandPartsExchange = db.CreateDbCommand(sqlInsertPartsExchange, conn, ts);
                                    commandPartsExchange.Parameters.Add(db.CreateDbParameter("ExchangeCode", item.ExchangeIdentification));
                                    commandPartsExchange.Parameters.Add(db.CreateDbParameter("ExchangeName", ""));
                                    commandPartsExchange.Parameters.Add(db.CreateDbParameter("PartId", oldsparePart.Id));
                                    commandPartsExchange.Parameters.Add(db.CreateDbParameter("Status", 1));
                                    commandPartsExchange.Parameters.Add(db.CreateDbParameter("Remark", ErrorStrings.Export_Validation_Sparepart_RemarkDefault5));
                                    commandPartsExchange.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                    commandPartsExchange.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                    commandPartsExchange.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                    var exchangeId = db.ExecuteInsert(commandPartsExchange, "Id");
                                    var commandPartsExchangeHistory = db.CreateDbCommand(sqlInsertPartsExchangeHistory, conn, ts);
                                    commandPartsExchangeHistory.Parameters.Add(db.CreateDbParameter("PartsExchangeId", exchangeId));
                                    commandPartsExchangeHistory.Parameters.Add(db.CreateDbParameter("ExchangeCode", item.ExchangeIdentification));
                                    commandPartsExchangeHistory.Parameters.Add(db.CreateDbParameter("ExchangeName", ""));
                                    commandPartsExchangeHistory.Parameters.Add(db.CreateDbParameter("PartId", oldsparePart.Id));
                                    commandPartsExchangeHistory.Parameters.Add(db.CreateDbParameter("Status", 1));
                                    commandPartsExchangeHistory.Parameters.Add(db.CreateDbParameter("Remark", ErrorStrings.Export_Validation_Sparepart_RemarkDefault5));
                                    commandPartsExchangeHistory.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                    commandPartsExchangeHistory.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                    commandPartsExchangeHistory.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                    commandPartsExchangeHistory.ExecuteNonQuery();
                                }
                            }
                            if(rightList.Any()) {
                                rightList = rightList.Where(r => !string.IsNullOrEmpty(r.ExchangeIdentification)).ToList();
                                dbpartsExchangeGroups = dbpartsExchangeGroups.Where(r => !rightList.Any(p => p.Code == r.PartCode)).ToList();
                                //dbpartsExchanges = dbpartsExchanges.Where(r => !rightList.Any(p => p.Code == r.PartCode)).ToList();
                                var partsExchanges = dbpartsExchanges.Where(r => !rightList.Any(p => p.Code == r.PartCode)).ToList();
                                if(rightList.Any()) {
                                    var groups = rightList.GroupBy(r => r.ExchangeIdentification).Where(r => r.Count() > 1);
                                    var distinctMany = groups.SelectMany(r => r).ToList();
                                    distinctMany = distinctMany.Where((x, i) => distinctMany.FindIndex(z => z.ExchangeIdentification == x.ExchangeIdentification) == i).ToList();
                                    foreach(var groupitem in distinctMany) {
                                        var exchange = dbpartsExchanges.FirstOrDefault(v => v.PartCode == groupitem.Code && v.ExchangeCode == groupitem.ExchangeIdentification);
                                        if(exchange == null) {
                                            var partsExchangeGroup = dbpartsExchangeGroups.Where(r => r.ExchangeCode == groupitem.ExchangeIdentification);
                                            if(!partsExchangeGroup.Any()) {
                                                var commandPartsExchangeGroup = db.CreateDbCommand(sqlInsertPartsExchangeGroup, conn, ts);
                                                var exgroupcode = CodeGenerator.Generate(db, "PartsExchange");
                                                commandPartsExchangeGroup.Parameters.Add(db.CreateDbParameter("ExGroupCode", exgroupcode));
                                                commandPartsExchangeGroup.Parameters.Add(db.CreateDbParameter("ExchangeCode", groupitem.ExchangeIdentification));
                                                commandPartsExchangeGroup.Parameters.Add(db.CreateDbParameter("Status", 1));
                                                commandPartsExchangeGroup.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                                commandPartsExchangeGroup.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                                commandPartsExchangeGroup.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                                commandPartsExchangeGroup.ExecuteNonQuery();
                                            }
                                        }
                                    }
                                    var groupsOne = rightList.GroupBy(r => r.ExchangeIdentification).Where(r => r.Count() == 1);
                                    var onelist = groupsOne.SelectMany(r => r).ToList();
                                    foreach(var groupitem in onelist) {
                                        var partsExchangeGroup = dbpartsExchangeGroups.Where(r => r.ExchangeCode == groupitem.ExchangeIdentification);
                                        if(!partsExchangeGroup.Any()) {
                                            var partsExchange = partsExchanges.Where(r => r.ExchangeCode == groupitem.ExchangeIdentification);
                                            if(partsExchange.Any()) {
                                                var commandPartsExchangeGroup = db.CreateDbCommand(sqlInsertPartsExchangeGroup, conn, ts);
                                                var exgroupcode = CodeGenerator.Generate(db, "PartsExchange");
                                                commandPartsExchangeGroup.Parameters.Add(db.CreateDbParameter("ExGroupCode", exgroupcode));
                                                commandPartsExchangeGroup.Parameters.Add(db.CreateDbParameter("ExchangeCode", groupitem.ExchangeIdentification));
                                                commandPartsExchangeGroup.Parameters.Add(db.CreateDbParameter("Status", 1));
                                                commandPartsExchangeGroup.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                                commandPartsExchangeGroup.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                                commandPartsExchangeGroup.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                                commandPartsExchangeGroup.ExecuteNonQuery();
                                            }
                                        }
                                    }
                                }
                            }
                            //无异常提交
                            ts.Commit();
                            ts.Dispose();
                        } catch(Exception) {
                            //报错回滚
                            ts.Rollback();
                            ts.Dispose();
                            throw;
                        }
                        if(conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }
        public bool 批量替换配件厂商(string fileName, out int excelImportNum, out List<SparePartExtend> rightData, out List<SparePartExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<SparePartExtend>();
            var rightList = new List<SparePartExtend>();
            var allList = new List<SparePartExtend>();
            var dbSparePartCodes = new List<SparePartExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);
                List<object> excelColumns;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Code, "Code");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Sparepart_Factury, "Factury");
                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    //单次最大导入条数不能大于300
                    if(excelOperator.LastRowNum > 2000) {
                        throw new Exception(ErrorStrings.Export_Validation_Sparepart_Item);
                    }

                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var tempImportObj = new SparePartExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        tempImportObj.Code = newRow["Code"];
                        tempImportObj.CodeStr = newRow["Code"];
                        tempImportObj.Factury = newRow["Factury"];
                        var tempErrorMessage = new List<string>();

                        //导入模板必填
                        if(string.IsNullOrEmpty(tempImportObj.Code)) {
                            tempErrorMessage.Add(ErrorStrings.Export_Validation_Sparepart_CodeIsNull);
                        }
                        if(tempErrorMessage.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });
                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    var groups = tempRightList.GroupBy(r => new {
                        r.Code
                    }).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        groupItem.ErrorMsg = ErrorStrings.ImpPartsSupplierRelation_Validation31;
                    }
                    var sparePartsNeedCheck = tempRightList.ToList();
                    var partCodesNeedCheck = sparePartsNeedCheck.Select(r => r.Code).Distinct().ToArray();

                    Func<string[], bool> getDbSparePartCodes = vales => {
                        var dbSparePart = new SparePartExtend();
                        dbSparePart.Id = Convert.ToInt32(vales[0]);
                        dbSparePart.Code = vales[1];
                        dbSparePart.Name = vales[2];
                        dbSparePart.ReferenceCode = vales[3];
                        dbSparePart.LastSubstitute = vales[4];
                        dbSparePart.NextSubstitute = vales[5];
                        dbSparePart.ShelfLife = string.IsNullOrEmpty(vales[6]) ? 0 : Convert.ToInt32(vales[6]);
                        dbSparePart.EnglishName = vales[7];
                        dbSparePart.PinyinCode = vales[8];
                        dbSparePart.IMSManufacturerNumber = vales[9];
                        dbSparePart.ReferenceName = vales[10];
                        dbSparePart.CADCode = vales[11];
                        dbSparePart.CADName = vales[12];
                        dbSparePart.PartType = string.IsNullOrEmpty(vales[13]) ? 0 : Convert.ToInt32(vales[13]);
                        dbSparePart.Specification = vales[14];
                        dbSparePart.Feature = vales[15];
                        dbSparePart.Status = string.IsNullOrEmpty(vales[16]) ? 0 : Convert.ToInt32(vales[16]);
                        dbSparePart.Length = string.IsNullOrEmpty(vales[17]) ? 0 : Convert.ToDecimal(vales[17]);
                        dbSparePart.Width = string.IsNullOrEmpty(vales[18]) ? 0 : Convert.ToDecimal(vales[18]);
                        dbSparePart.Height = string.IsNullOrEmpty(vales[19]) ? 0 : Convert.ToDecimal(vales[19]);
                        dbSparePart.Volume = string.IsNullOrEmpty(vales[20]) ? 0 : Convert.ToDecimal(vales[20]);
                        dbSparePart.Weight = string.IsNullOrEmpty(vales[21]) ? 0 : Convert.ToDecimal(vales[21]);
                        dbSparePart.Material = vales[22];
                        dbSparePart.PackingAmount = string.IsNullOrEmpty(vales[23]) ? 0 : Convert.ToInt32(vales[23]);
                        dbSparePart.PackingSpecification = vales[24];
                        dbSparePart.PartsOutPackingCode = vales[25];
                        dbSparePart.PartsInPackingCode = vales[26];
                        dbSparePart.MeasureUnit = vales[27];
                        dbSparePart.MInPackingAmount = string.IsNullOrEmpty(vales[28]) ? 0 : Convert.ToInt32(vales[28]);
                        dbSparePart.GroupABCCategory = string.IsNullOrEmpty(vales[29]) ? 0 : Convert.ToInt32(vales[29]);
                        dbSparePart.Factury = vales[30];
                        dbSparePart.ExchangeIdentification = vales[31];
                        dbSparePartCodes.Add(dbSparePart);
                        return false;
                    };
                    //根据SQL查询数据库，将查出来的结果根据定义的委托中的方法（这里是getDbSparePartCodes）放到具体的集合中（这里是dbSparePartCodes）
                    db.QueryDataWithInOperator(@"select ID,
                                                                    CODE,
                                                                    NAME,
                                                                    ReferenceCode,
                                                                    LASTSUBSTITUTE,
                                                                    NEXTSUBSTITUTE,
                                                                    SHELFLIFE,
                                                                    ENGLISHNAME,
                                                                    PINYINCODE,
                                                                    IMSManufacturerNumber,
                                                                    REFERENCENAME,
                                                                    CADCODE,
                                                                    CADNAME,
                                                                    PARTTYPE,
                                                                    SPECIFICATION,
                                                                    FEATURE,
                                                                    STATUS,
                                                                    LENGTH,
                                                                    WIDTH,
                                                                    HEIGHT,
                                                                    VOLUME,
                                                                    WEIGHT,
                                                                    MATERIAL,
                                                                    PACKINGAMOUNT,
                                                                    PACKINGSPECIFICATION,
                                                                    PARTSOUTPACKINGCODE,
                                                                    PARTSINPACKINGCODE,
                                                                    MEASUREUNIT,
                                                                    MINPACKINGAMOUNT,
                                                                    GroupABCCategory,
                                                                    Factury,ExchangeIdentification
                                                                from sparepart where status != 99", "Code", false, partCodesNeedCheck, getDbSparePartCodes);
                    if(partCodesNeedCheck.Length > 0) {
                        foreach(var item in sparePartsNeedCheck) {
                            var sparePartsExistsCode = dbSparePartCodes.FirstOrDefault(r => r.Code == item.Code);
                            if(sparePartsExistsCode == null) {
                                item.ErrorMsg = ErrorStrings.Export_Validation_PartsBranch_Validation8 + item.Code + ErrorStrings.Export_Validation_Sparepart_NotExist;
                            }
                        }
                    }

                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    rightList = allList.Except(errorList).ToList();


                }

                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempObj = list[index - 1];
                            var values = new object[] {
                                tempObj.CodeStr,
                                tempObj.Factury,
                                tempObj.ErrorMsg
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }

                //导入所有合格数据  
                if(!rightList.Any())
                    return true;
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    if(rightList.Any()) {
                        var userInfo = Utils.GetCurrentUserInfo();
                        var sqlUpdateSparePart = db.GetUpdateSql("SparePart", new string[] {
                            "Factury","ModifierId","ModifyTime","ModifierName","OMSparePartMark"
                        }, new string[] {
                            "Id"
                        });
                        var sqlInsertHistory = db.GetInsertSql("SparePartHistory", "Id", new[] {
                                  "SparePartId","Code","ReferenceCode","IMSCompressionNumber","IMSManufacturerNumber", "Name", "CADCode", "Specification", "EnglishName", "PartType", "MeasureUnit", "ShelfLife", "LastSubstitute", "NextSubstitute", "Weight","Volume","GroupABCCategory", "Feature", "status", "CreatorId", "CreatorName", "CreateTime","MInPackingAmount","Factury","ExchangeIdentification"
                                });
                        var ts = conn.BeginTransaction();
                        try {
                            foreach(var item in rightList) {
                                //string code = Strings.StrConv(item.Code, VbStrConv.Narrow).Replace("【", "[");
                                var sparePart = dbSparePartCodes.Where(r => r.Code == item.Code).FirstOrDefault();
                                var command = db.CreateDbCommand(sqlUpdateSparePart, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("ModifyTime", DateTime.Now));
                                command.Parameters.Add(db.CreateDbParameter("ModifierId", userInfo.Id));
                                command.Parameters.Add(db.CreateDbParameter("ModifierName", userInfo.Name));
                                command.Parameters.Add(db.CreateDbParameter("Factury", item.Factury));
                                command.Parameters.Add(db.CreateDbParameter("Id", sparePart.Id));
                                command.Parameters.Add(db.CreateDbParameter("OMSparePartMark", 0));
                                command.ExecuteNonQuery();

                                if(sparePart != null) {
                                    var commandHistory = db.CreateDbCommand(sqlInsertHistory, conn, ts);
                                    commandHistory.Parameters.Add(db.CreateDbParameter("SparePartId", sparePart.Id));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Code", Strings.StrConv(sparePart.Code, VbStrConv.Narrow).Replace("【", "[")));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Name", sparePart.Name));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("ReferenceCode", sparePart.ReferenceCode));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("IMSCompressionNumber", sparePart.IMSCompressionNumber));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("IMSManufacturerNumber", sparePart.IMSManufacturerNumber));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("CADCode", sparePart.CADCode));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Specification", sparePart.Specification));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("EnglishName", sparePart.EnglishName));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("PartType", sparePart.PartType));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("MeasureUnit", sparePart.MeasureUnit));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("ShelfLife", sparePart.ShelfLife));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("LastSubstitute", sparePart.LastSubstitute));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("NextSubstitute", sparePart.NextSubstitute));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Weight", sparePart.Weight));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Feature", sparePart.Feature));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Status", sparePart.Status));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Volume", sparePart.Volume));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("MInPackingAmount", sparePart.MInPackingAmount));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("GroupABCCategory", sparePart.GroupABCCategory));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Factury", item.Factury));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("ExchangeIdentification", sparePart.ExchangeIdentification));
                                    commandHistory.ExecuteNonQuery();
                                }
                            }

                            //无异常提交
                            ts.Commit();
                            ts.Dispose();
                        } catch(Exception) {
                            //报错回滚
                            ts.Rollback();
                            ts.Dispose();
                            throw;
                        }
                        if(conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }
        public bool 批量替换配件分类编码(string fileName, out int excelImportNum, out List<SparePartExtend> rightData, out List<SparePartExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<SparePartExtend>();
            var rightList = new List<SparePartExtend>();
            var allList = new List<SparePartExtend>();
            var dbSparePartCodes = new List<SparePartExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);
                List<object> excelColumns;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Code, "Code");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Sparepart_CategoryCode, "CategoryCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Sparepart_CategoryName, "CategoryName");
                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    //单次最大导入条数不能大于300
                    if(excelOperator.LastRowNum > 2000) {
                        throw new Exception(ErrorStrings.Export_Validation_Sparepart_Item);
                    }

                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var tempImportObj = new SparePartExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        tempImportObj.Code = newRow["Code"] == null ? null : newRow["Code"];
                        tempImportObj.CategoryCode = newRow["CategoryCode"];
                        tempImportObj.CategoryName = newRow["CategoryName"];
                        var tempErrorMessage = new List<string>();

                        //导入模板必填
                        if(string.IsNullOrEmpty(tempImportObj.Code)) {
                            tempErrorMessage.Add(ErrorStrings.Export_Validation_Sparepart_CodeIsNull);
                        }
                        if(tempErrorMessage.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });
                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    var groups = tempRightList.GroupBy(r => new {
                        r.Code
                    }).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        groupItem.ErrorMsg = ErrorStrings.ImpPartsSupplierRelation_Validation31;
                    }
                    var sparePartsNeedCheck = tempRightList.ToList();
                    var partCodesNeedCheck = sparePartsNeedCheck.Select(r => r.Code).Distinct().ToArray();

                    Func<string[], bool> getDbSparePartCodes = vales => {
                        var dbSparePart = new SparePartExtend();
                        dbSparePart.Id = Convert.ToInt32(vales[0]);
                        dbSparePart.Code = vales[1];
                        dbSparePart.Name = vales[2];
                        dbSparePart.ReferenceCode = vales[3];
                        dbSparePart.LastSubstitute = vales[4];
                        dbSparePart.NextSubstitute = vales[5];
                        dbSparePart.ShelfLife = string.IsNullOrEmpty(vales[6]) ? 0 : Convert.ToInt32(vales[6]);
                        dbSparePart.EnglishName = vales[7];
                        dbSparePart.PinyinCode = vales[8];
                        dbSparePart.IMSManufacturerNumber = vales[9];
                        dbSparePart.ReferenceName = vales[10];
                        dbSparePart.CADCode = vales[11];
                        dbSparePart.CADName = vales[12];
                        dbSparePart.PartType = string.IsNullOrEmpty(vales[13]) ? 0 : Convert.ToInt32(vales[13]);
                        dbSparePart.Specification = vales[14];
                        dbSparePart.Feature = vales[15];
                        dbSparePart.Status = string.IsNullOrEmpty(vales[16]) ? 0 : Convert.ToInt32(vales[16]);
                        dbSparePart.Length = string.IsNullOrEmpty(vales[17]) ? 0 : Convert.ToDecimal(vales[17]);
                        dbSparePart.Width = string.IsNullOrEmpty(vales[18]) ? 0 : Convert.ToDecimal(vales[18]);
                        dbSparePart.Height = string.IsNullOrEmpty(vales[19]) ? 0 : Convert.ToDecimal(vales[19]);
                        dbSparePart.Volume = string.IsNullOrEmpty(vales[20]) ? 0 : Convert.ToDecimal(vales[20]);
                        dbSparePart.Weight = string.IsNullOrEmpty(vales[21]) ? 0 : Convert.ToDecimal(vales[21]);
                        dbSparePart.Material = vales[22];
                        dbSparePart.PackingAmount = string.IsNullOrEmpty(vales[23]) ? 0 : Convert.ToInt32(vales[23]);
                        dbSparePart.PackingSpecification = vales[24];
                        dbSparePart.PartsOutPackingCode = vales[25];
                        dbSparePart.PartsInPackingCode = vales[26];
                        dbSparePart.MeasureUnit = vales[27];
                        dbSparePart.MInPackingAmount = string.IsNullOrEmpty(vales[28]) ? 0 : Convert.ToInt32(vales[28]);
                        dbSparePart.GroupABCCategory = string.IsNullOrEmpty(vales[29]) ? 0 : Convert.ToInt32(vales[29]);
                        dbSparePart.Factury = vales[30];
                        dbSparePart.CategoryCode = vales[31];
                        dbSparePart.CategoryName = vales[32];
                        dbSparePart.CategoryName = vales[33];
                        dbSparePartCodes.Add(dbSparePart);
                        return false;
                    };
                    //根据SQL查询数据库，将查出来的结果根据定义的委托中的方法（这里是getDbSparePartCodes）放到具体的集合中（这里是dbSparePartCodes）
                    db.QueryDataWithInOperator(@"select ID,
                                                                    CODE,
                                                                    NAME,
                                                                    ReferenceCode,
                                                                    LASTSUBSTITUTE,
                                                                    NEXTSUBSTITUTE,
                                                                    SHELFLIFE,
                                                                    ENGLISHNAME,
                                                                    PINYINCODE,
                                                                    IMSManufacturerNumber,
                                                                    REFERENCENAME,
                                                                    CADCODE,
                                                                    CADNAME,
                                                                    PARTTYPE,
                                                                    SPECIFICATION,
                                                                    FEATURE,
                                                                    STATUS,
                                                                    LENGTH,
                                                                    WIDTH,
                                                                    HEIGHT,
                                                                    VOLUME,
                                                                    WEIGHT,
                                                                    MATERIAL,
                                                                    PACKINGAMOUNT,
                                                                    PACKINGSPECIFICATION,
                                                                    PARTSOUTPACKINGCODE,
                                                                    PARTSINPACKINGCODE,
                                                                    MEASUREUNIT,
                                                                    MINPACKINGAMOUNT,
                                                                    GroupABCCategory,
                                                                    Factury,
                                                                    CategoryCode,
                                                                    CategoryName,ExchangeIdentification
                                                                from sparepart where status != 99", "Code", false, partCodesNeedCheck, getDbSparePartCodes);
                    if(partCodesNeedCheck.Length > 0) {
                        foreach(var item in sparePartsNeedCheck) {
                            var sparePartsExistsCode = dbSparePartCodes.FirstOrDefault(r => r.Code == item.Code);
                            if(sparePartsExistsCode == null) {
                                item.ErrorMsg = ErrorStrings.Export_Validation_PartsBranch_Validation8 + item.Code + ErrorStrings.Export_Validation_Sparepart_NotExist;
                            }
                        }
                    }

                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    rightList = allList.Except(errorList).ToList();


                }

                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempObj = list[index - 1];
                            var values = new object[] {
                                tempObj.Code,
                                tempObj.CategoryCode,
                                tempObj.CategoryName,
                                tempObj.ErrorMsg
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }

                //导入所有合格数据  
                if(!rightList.Any())
                    return true;
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    if(rightList.Any()) {
                        var userInfo = Utils.GetCurrentUserInfo();
                        var sqlUpdateSparePart = db.GetUpdateSql("SparePart", new string[] {
                            "CategoryCode","CategoryName","ModifierId","ModifyTime","ModifierName","OMSparePartMark"
                        }, new string[] {
                            "Id"
                        });
                        var sqlInsertHistory = db.GetInsertSql("SparePartHistory", "Id", new[] {
                                  "SparePartId","Code","ReferenceCode","IMSCompressionNumber","IMSManufacturerNumber", "Name", "CADCode", "Specification", "EnglishName", "PartType", "MeasureUnit", "ShelfLife", "LastSubstitute", "NextSubstitute", "Weight","Volume","GroupABCCategory", "Feature", "status", "CreatorId", "CreatorName", "CreateTime","MInPackingAmount","Factury","CategoryName","CategoryCode","ExchangeIdentification"
                                });
                        var ts = conn.BeginTransaction();
                        try {
                            foreach(var item in rightList) {
                                var sparePart = dbSparePartCodes.Where(r => r.Code == item.Code).FirstOrDefault();
                                var command = db.CreateDbCommand(sqlUpdateSparePart, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("ModifyTime", DateTime.Now));
                                command.Parameters.Add(db.CreateDbParameter("ModifierId", userInfo.Id));
                                command.Parameters.Add(db.CreateDbParameter("ModifierName", userInfo.Name));
                                command.Parameters.Add(db.CreateDbParameter("CategoryCode", item.CategoryCode));
                                command.Parameters.Add(db.CreateDbParameter("CategoryName", item.CategoryName));
                                command.Parameters.Add(db.CreateDbParameter("Id", sparePart.Id));
                                command.Parameters.Add(db.CreateDbParameter("OMSparePartMark", 0));
                                command.ExecuteNonQuery();

                                if(sparePart != null) {
                                    var commandHistory = db.CreateDbCommand(sqlInsertHistory, conn, ts);
                                    commandHistory.Parameters.Add(db.CreateDbParameter("SparePartId", sparePart.Id));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Code", Strings.StrConv(sparePart.Code, VbStrConv.Narrow).Replace("【", "[")));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Name", sparePart.Name));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("ReferenceCode", sparePart.ReferenceCode));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("IMSCompressionNumber", sparePart.IMSCompressionNumber));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("IMSManufacturerNumber", sparePart.IMSManufacturerNumber));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("CADCode", sparePart.CADCode));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Specification", sparePart.Specification));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("EnglishName", sparePart.EnglishName));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("PartType", sparePart.PartType));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("MeasureUnit", sparePart.MeasureUnit));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("ShelfLife", sparePart.ShelfLife));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("LastSubstitute", sparePart.LastSubstitute));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("NextSubstitute", sparePart.NextSubstitute));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Weight", sparePart.Weight));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Feature", sparePart.Feature));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Status", sparePart.Status));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Volume", sparePart.Volume));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("MInPackingAmount", sparePart.MInPackingAmount));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("GroupABCCategory", sparePart.GroupABCCategory));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("Factury", sparePart.Factury));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("CategoryCode", item.CategoryCode));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("CategoryName", item.CategoryName));
                                    commandHistory.Parameters.Add(db.CreateDbParameter("ExchangeIdentification", sparePart.ExchangeIdentification));
                                    commandHistory.ExecuteNonQuery();
                                }
                            }

                            //无异常提交
                            ts.Commit();
                            ts.Dispose();
                        } catch(Exception) {
                            //报错回滚
                            ts.Rollback();
                            ts.Dispose();
                            throw;
                        }
                        if(conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }
        public bool 导出EPC接口日志(int[] ids, string code, string name, DateTime? LastProcessTimeStart, DateTime? LastProcessTimeEnd, out string fileName) {
            fileName = GetExportFilePath("EPC接口日志_" + ".xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated) {
                    throw new Exception(ErrorStrings.File_Validation_User);
                }
                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {

                    conn.Open();
                    var sql = new StringBuilder();
                    sql.Append(@"select    Code,
                                           Name,
                                           ReferenceCode,
                                            PartsSalesCategoryName,
                                           Specification,
                                           --(select value from keyvalueitem where NAME = 'SparePart_MeasureUnit'and key=MeasureUnit) As MeasureUnit,
                                           MeasureUnit,
                                           EnglishName,
                                           Feature,
                                           LastSubstitute,
                                           NextSubstitute,
                                           (select value from keyvalueitem where NAME = 'PMSHandleStatus'and key=ProcessStatus) As ProcessStatus,
                                           LastProcessTime,
                                           ErrorMessage,
                                           WarrantyRange
                                      from SEP_SparePartInfo where 1=1 ");
                    var dbParameters = new List<DbParameter>();
                    if(ids != null && ids.Length > 0) {
                        sql.Append(" and id in (");
                        for(var i = 0; i < ids.Length; i++) {
                            if(ids.Length == i + 1) {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            } else {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                        }
                        sql.Append(")");
                    } else {
                        if(!string.IsNullOrEmpty(code)) {
                            sql.Append(@" and LOWER(code) like {0}code ");
                            dbParameters.Add(db.CreateDbParameter("code", "%" + code.ToLower() + "%"));
                        }
                        if(!string.IsNullOrEmpty(name)) {
                            sql.Append(@" and LOWER(name) like {0}name ");
                            dbParameters.Add(db.CreateDbParameter("name", "%" + name.ToLower() + "%"));
                        }

                        if(LastProcessTimeStart.HasValue) {
                            sql.Append(@" and LastProcessTime >=To_date({0}LastProcessTimeStart,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = LastProcessTimeStart.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("LastProcessTimeStart", tempTime.ToString("G")));
                        }
                        if(LastProcessTimeEnd.HasValue) {
                            sql.Append(@" and LastProcessTime <=To_date({0}LastProcessTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = LastProcessTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("LastProcessTimeEnd", tempTime.ToString("G")));
                        }

                    }
                    var sqlStr = sql.ToString();
                    if(sqlStr.IndexOf(" where 1=1  and ") > 0)
                        sqlStr = sqlStr.Replace(" where 1=1  and ", " where ");
                    else
                        sqlStr = sqlStr.Replace(" where 1=1 ", "");
                    var command = db.CreateDbCommand(string.Format(sqlStr, db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                ErrorStrings.Export_Title_PartsBranch_Code,
                                ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName,
                                "参考编号",
                                "配件销售类别名称",
                                "规范",
                                "测量单位",
                                "英文名",
                                "特征",
                                "上个替换配件",
                                "下个替换配件",
                                "过程状态",
                                "上次处理时间",
                                "错误信息",
                                "保修范围"
                                 };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }

                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception ex) {
                throw ex;
            }
        }




        #region oracle 查询条件in超过1000条解决方案
        /// <summary>
        /// oracle 查询条件in超过1000条解决方案
        /// </summary>
        /// <param name="ids">值的集合</param>
        /// <param name="field">字段名</param>
        /// <returns></returns>
        private string getOracleSQLIn(List<string> ids, string field) {
            int count = Math.Min(ids.Count, 1000);
            int len = ids.Count;
            int size = len % count;
            if(size == 0) {
                size = len / count;
            } else {
                size = (len / count) + 1;
            }
            StringBuilder builder = new StringBuilder();
            for(int i = 0; i < size; i++) {
                int fromIndex = i * count;
                int toIndex = Math.Min(fromIndex + count, len);
                string productId = string.Join("','", getArrayValues(fromIndex, toIndex, ids).ToArray());
                if(i != 0) {
                    builder.Append(" or ");
                }
                builder.Append(field).Append(" in ('").Append(productId).Append("')");
            }
            return builder.ToString();
        }
        public List<string> getArrayValues(int fromindex, int toindex, List<string> array) {
            List<string> listret = new List<string>();
            for(int i = fromindex; i < toindex; i++) {
                listret.Add(array[i]);
            }
            return listret;
        }
        #endregion


    }
}