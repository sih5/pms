﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        /// <summary>
        /// 导出服务站旧件库存
        /// </summary>
        public bool ExportSsUsedPartsStorage(int? billId, int? dealerId, int? branchId, string dealerCode, string dealerName, string claimBillCode, string usedPartsBarCode, string usedPartsCode, int? usedPartsReturnPolicy, int? partsSalesCategoryId, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName) {
            fileName = GetExportFilePath("服务站旧件库存_" + ".xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {

                    conn.Open();
                    var sql = new StringBuilder();
                    sql.Append(@"Select a.Branchname, --分公司
                                       b.Code, --服务站编号
                                       b.Name, --服务站名称
                                       a.Claimbillcode, --索赔单编号
                                       c.Name, --品牌
                                       a.Usedpartsbarcode, --旧件条码
                                       cast(Case a.Usedpartsreturnpolicy --旧件返回政策
                                         When 1 Then
                                          '返回本部'
                                         When 2 Then
                                          '供应商自行回收'
                                         When 3 Then
                                          '监督处理'
                                         When 4 Then
                                          '自行处理'
                                         When 5 Then
                                          '总部调件'
                                       End as varchar2(20)),
                                       a.Usedpartscode, --旧件编号
                                       a.Usedpartsname, --旧件名称
                                       cast(Decode(a.Shipped, 1, '是', 0, '否') as varchar2(20)), --已发运
                                       a.Unitprice, --单价
                                       cast(Decode(a.Iffaultyparts, 1, '是', 0, '否') as varchar2(20)), --是否祸首件
                                       a.Usedpartssuppliercode, --旧件供应商编号
                                       a.Usedpartssuppliername, --旧件供应商名称
                                       a.Faultypartscode, --祸首件编号
                                       a.Faultypartsname, --祸首件名称
                                       a.Faultypartssuppliercode, --祸首件供应商编号
                                       a.Faultypartssuppliername, --祸首件供应商名称
                                       a.Claimbillcreatetime, --索赔单生成时间
                                       a.Modifiername, --修改人
                                       a.Modifytime --修改时间
                                  From Ssusedpartsstorage a
                                  Left Join Dealer b
                                    On a.Dealerid = b.Id
                                  Left Join Partssalescategory c
                                    On a.Partssalescategoryid = c.Id where 1=1 ");
                    var dbParameters = new List<DbParameter>();
                    if(dealerId.HasValue) {
                        sql.Append(@" and a.dealerId={0}dealerId");
                        dbParameters.Add(db.CreateDbParameter("dealerId", dealerId.Value));
                    }
                    if(billId.HasValue) {
                        sql.Append(" and a.id = {0}billId");
                        dbParameters.Add(db.CreateDbParameter("billId", billId.Value));
                    } else {
                        if(branchId.HasValue) {
                            sql.Append(@" and a.branchId={0}branchId");
                            dbParameters.Add(db.CreateDbParameter("branchId", branchId.Value));
                        }
                        if(!string.IsNullOrEmpty(dealerCode)) {
                            sql.Append(@" and Upper(b.Code) =Upper({0}dealerCode) ");
                            dbParameters.Add(db.CreateDbParameter("dealerCode", "%" + dealerCode + "%"));
                        }
                        if(!string.IsNullOrEmpty(dealerName)) {
                            sql.Append(@" and b.Name ={0}dealerName ");
                            dbParameters.Add(db.CreateDbParameter("dealerName", "%" + dealerName + "%"));
                        }
                        if(!string.IsNullOrEmpty(claimBillCode)) {
                            sql.Append(@" and Upper(a.claimBillCode) =Upper({0}claimBillCode) ");
                            dbParameters.Add(db.CreateDbParameter("claimBillCode", "%" + claimBillCode + "%"));
                        }
                        if(!string.IsNullOrEmpty(usedPartsBarCode)) {
                            sql.Append(@" and Upper(a.usedPartsBarCode) =Upper({0}usedPartsBarCode) ");
                            dbParameters.Add(db.CreateDbParameter("usedPartsBarCode", "%" + usedPartsBarCode + "%"));
                        }
                        if(!string.IsNullOrEmpty(usedPartsCode)) {
                            sql.Append(@" and Upper(a.usedPartsCode) =Upper({0}usedPartsCode) ");
                            dbParameters.Add(db.CreateDbParameter("usedPartsCode", "%" + usedPartsCode + "%"));
                        }
                        if(usedPartsReturnPolicy.HasValue) {
                            sql.Append(@" and a.usedPartsReturnPolicy={0}usedPartsReturnPolicy");
                            dbParameters.Add(db.CreateDbParameter("usedPartsReturnPolicy", usedPartsReturnPolicy.Value));
                        }
                        if(partsSalesCategoryId.HasValue) {
                            sql.Append(@" and a.partsSalesCategoryId={0}partsSalesCategoryId");
                            dbParameters.Add(db.CreateDbParameter("partsSalesCategoryId", partsSalesCategoryId.Value));
                        }
                        if(createTimeBegin.HasValue) {
                            sql.Append(@" and a.createTime >=To_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(@" and a.createTime <=To_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                    }
                    var sqlStr = sql.ToString();
                    if(sqlStr.IndexOf(" where 1=1  and ") > 0)
                        sqlStr = sqlStr.Replace(" where 1=1  and ", " where ");
                    else
                        sqlStr = sqlStr.Replace(" where 1=1 ", "");
                    var command = db.CreateDbCommand(string.Format(sqlStr, db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    ErrorStrings.Export_Title_Partshistorystock_BranchName, ErrorStrings.Export_Title_Dealer_Code, ErrorStrings.Export_Title_Dealer_Name, "索赔单编号", ErrorStrings.Export_Title_Partssalescategory_Name, "旧件条码", ErrorStrings.Export_Title_PartsBranch_PartsReturnPolicy, "旧件编号", "旧件名称", "已发运", ErrorStrings.Export_Title_InternalAllocationBill_UnitPrice, "是否祸首件", "旧件供应商编号", "旧件供应商名称", "祸首件编号", "祸首件名称", "祸首件供应商编号", "祸首件供应商名称", "索赔单生成时间", ErrorStrings.Export_Title_AccountPeriod_ModifierName, ErrorStrings.Export_Title_AccountPeriod_ModifyTime
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }

                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }

        }
    }
}