﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        /// <summary>
        /// 导出旧件运距信息
        /// </summary>
        /// <param name="companyCode">客户企业编号</param>
        /// <param name="companyName">客户企业名称</param>
        /// <param name="storageCompanyCode">仓储企业编号</param>
        /// <param name="storageCompanyName">仓储企业名称</param>
        /// <param name="warehouseCode">仓库编号</param>
        /// <param name="warehouseName">仓库名称</param>
        /// <param name="createTimeBegin">创建起始时间</param>
        /// <param name="createTimeEnd">创建结束时间</param>
        /// <param name="status">状态</param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public bool ExportUsedPartsDistanceInfor(string companyCode, string companyName, string storageCompanyCode, string storageCompanyName, string warehouseCode, string warehouseName, DateTime? createTimeBegin, DateTime? createTimeEnd, int? status, out string fileName) {
            fileName = GetExportFilePath("旧件运距信息_" + ".xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated) {
                    throw new Exception(ErrorStrings.File_Validation_User);
                }
                var db = DbHelper.GetDbHelp(ConnectionString);
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.Append(@"select    b.code as StorageCompanyCode,
                                           b.name as StorageCompanyName,
                                           d.name as WarehouseName,
                                           c.code as CompanyCode,
                                           c.name as CompanyName,
                                           e.DetailAddress,
                                           a.ShippingDistance,
                                           cast(case a.Status
                                             when 1 then
                                              '有效'
                                             when 99 then
                                              '作废'
                                           end as varchar2(20)) as Status,
                                           a.CreatorName,
                                           a.CreateTime,
                                           a.ModifierName,
                                           a.ModifyTime,
                                           a.AbandonerName,
                                           a.AbandonTime
                                      from UsedPartsDistanceInfor a
                                     left join company b
                                        on a.storagecompanyid = b.id
                                     left join company c
                                        on a.companyid = c.id
                                     left join warehouse d
                                        on a.warehouseid = d.id
                                     left join companyAddress e
                                        on a.companyaddressid = e.id where 1=1 ");
                    var dbParameters = new List<DbParameter>();
                    if(!String.IsNullOrEmpty(companyCode)) {
                        sql.Append(@" and c.code like {0}companyCode ");
                        dbParameters.Add(db.CreateDbParameter("companyCode", "%" + companyCode + "%"));
                    }
                    if(!String.IsNullOrEmpty(companyName)) {
                        sql.Append(@" and c.name like {0}companyName ");
                        dbParameters.Add(db.CreateDbParameter("companyName", "%" + companyName + "%"));
                    }

                    if(!String.IsNullOrEmpty(storageCompanyCode)) {
                        sql.Append(@" and b.code like {0}storagecompanyCode ");
                        dbParameters.Add(db.CreateDbParameter("storageCompanyCode", "%" + storageCompanyCode + "%"));
                    }
                    if(!String.IsNullOrEmpty(storageCompanyName)) {
                        sql.Append(@" and b.name like {0}storageCompanyName ");
                        dbParameters.Add(db.CreateDbParameter("storageCompanyName", "%" + storageCompanyName + "%"));
                    }
                    if(!String.IsNullOrEmpty(warehouseCode)) {
                        sql.Append(@" and d.code like {0}warehouseCode ");
                        dbParameters.Add(db.CreateDbParameter("warehouseCode", "%" + warehouseCode + "%"));
                    }
                    if(!String.IsNullOrEmpty(warehouseName)) {
                        sql.Append(@" and d.name like {0}warehouseName ");
                        dbParameters.Add(db.CreateDbParameter("warehouseName", "%" + warehouseName + "%"));
                    }
                    if(createTimeBegin.HasValue) {
                        sql.Append(@" and a.createTime >=to_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                        var tempValue = createTimeBegin.Value;
                        var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                        dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                    }
                    if(createTimeEnd.HasValue) {
                        sql.Append(@" and a.createTime <=to_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                        var tempValue = createTimeEnd.Value;
                        var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                        dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                    }
                    if(status.HasValue) {
                        sql.Append(@" and a.status={0}status ");
                        dbParameters.Add(db.CreateDbParameter("status", status.Value));
                    }
                    var sqlStr = sql.ToString();
                    if(sqlStr.IndexOf(" where 1=1  and ") > 0)
                        sqlStr = sqlStr.Replace(" where 1=1  and ", " where ");
                    else
                        sqlStr = sqlStr.Replace(" where 1=1 ", "");
                    var command = db.CreateDbCommand(string.Format(sqlStr, db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    ErrorStrings.Export_Title_PartsInboundCheckBill_StorageCompanyCode,ErrorStrings.Export_Title_Partshistorystock_StorageCompanyName,ErrorStrings.Export_Title_Company_WarehouseName,ErrorStrings.Export_Title_Credenceapplication_CompanyCode,ErrorStrings.Export_Title_Credenceapplication_CompanyName,ErrorStrings.Export_Title_PartsPurchaseOrder_ReceiveAddress,"运距",ErrorStrings.Export_Title_AccountPeriod_Status,ErrorStrings.Export_Title_AccountPeriod_CreatorName,ErrorStrings.Export_Title_AccountPeriod_CreateTime,ErrorStrings.Export_Title_AccountPeriod_ModifierName,ErrorStrings.Export_Title_AccountPeriod_ModifyTime,ErrorStrings.Export_Title_PartsBranch_AbandonerName,ErrorStrings.Export_Title_PartsBranch_AbandonerTime
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }

                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }
        /// <summary>
        /// 导入旧件运距信息
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="excelImportNum"></param>
        /// <param name="errorData"></param>
        /// <param name="errorDataFileName"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public bool ImportUsedPartsDistanceInfor(string fileName, out int excelImportNum, out List<UsedPartsDistanceInforExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<UsedPartsDistanceInforExtend>();
            var allList = new List<UsedPartsDistanceInforExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("UsedPartsDistanceInfor", out notNullableFields, out fieldLenght);

                List<string> notNullableFieldsCompany;
                Dictionary<string, int> fieldLenghtCompany;
                db.GetTableSchema("Company", out notNullableFieldsCompany, out fieldLenghtCompany);

                List<string> notNullableFieldsUsedPartsWarehouse;
                Dictionary<string, int> fieldLenghtUsedPartsWarehouse;
                db.GetTableSchema("UsedPartsWarehouse", out notNullableFieldsUsedPartsWarehouse, out fieldLenghtUsedPartsWarehouse);


                List<object> excelColumns;
                List<UsedPartsDistanceInforExtend> rightList;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    #region 指定文件中的列对应的名称 例如 excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode, "PartCode");

                    excelOperator.AddColumnDataSource("旧件仓储企业名称", "UsedPartsCompanyName");
                    excelOperator.AddColumnDataSource("旧件仓库名称", "UsedPartsWarehouseName");
                    excelOperator.AddColumnDataSource("旧件运距类型", "UsedPartsDistanceType");
                    excelOperator.AddColumnDataSource("源服务站编号", "OriginCompanyCode");
                    excelOperator.AddColumnDataSource("源服务站名称", "OriginCompanyName");
                    excelOperator.AddColumnDataSource("源旧件库编号", "OriginUsedPartsWarehouseCode");
                    excelOperator.AddColumnDataSource("源旧件库名称", "OriginUsedPartsWarehouseName");
                    excelOperator.AddColumnDataSource("旧件运距", "ShippingDistance");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Remark, "Remark");

                    #endregion
                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;
                    #region 获取对应枚举

                    var keyValuePairs = new[] {
                        new KeyValuePair<string, string>("BaseDataStatus", "BaseData_Status"),
                        new KeyValuePair<string, string>("DistanceType", "UsedParts_DistanceType")
                    };
                    tempExcelOperator.ImportHelper.LoadEnumFromDb(null, null, keyValuePairs);

                    #endregion
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var tempImportObj = new UsedPartsDistanceInforExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        #region 根据导入的字段给对应的实体属性赋值 tempImportObj.PartsSalesCategoryNameStr = row["PartsSalesCategoryName"].Trim();

                        tempImportObj.UsedPartsCompanyNameStr = newRow["UsedPartsCompanyName"];
                        tempImportObj.UsedPartsWarehouseNameStr = newRow["UsedPartsWarehouseName"];
                        tempImportObj.UsedPartsDistanceTypeStr = newRow["UsedPartsDistanceType"];
                        tempImportObj.OriginCompanyCodeStr = newRow["OriginCompanyCode"];
                        tempImportObj.OriginCompanyNameStr = newRow["OriginCompanyName"];
                        tempImportObj.OriginUsedPartsWarehouseCodeStr = newRow["OriginUsedPartsWarehouseCode"];
                        tempImportObj.OriginUsedPartsWarehouseNameStr = newRow["OriginUsedPartsWarehouseName"];
                        tempImportObj.ShippingDistanceStr = newRow["ShippingDistance"];
                        tempImportObj.RemarkStr = newRow["Remark"];

                        #endregion

                        var tempErrorMessage = new List<string>();
                        #region 导入的数据基本检查


                        //旧件仓储企业名称检查
                        var fieldIndex = notNullableFieldsCompany.IndexOf("UsedPartsCompanyName".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.UsedPartsCompanyNameStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add("旧件仓储企业名称不能为空");
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.UsedPartsCompanyNameStr) > fieldLenghtCompany["UsedPartsCompanyName".ToUpper()])
                                tempErrorMessage.Add("旧件仓储企业名称过长");
                        }
                        //旧件仓库名称检查
                        fieldIndex = notNullableFieldsUsedPartsWarehouse.IndexOf("UsedPartsWarehouseName".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.UsedPartsWarehouseNameStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add("旧件仓库名称不能为空");
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.UsedPartsWarehouseNameStr) > fieldLenghtUsedPartsWarehouse["UsedPartsWarehouseName".ToUpper()])
                                tempErrorMessage.Add("旧件仓库名称过长");
                        }
                        //旧件运距类型检查
                        fieldIndex = notNullableFields.IndexOf("UsedPartsDistanceType".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.UsedPartsDistanceTypeStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add("旧件运距类型不能为空");
                        } else {
                            var tempEnumValue = tempExcelOperator.ImportHelper.GetEnumValue("UsedPartsDistanceType", tempImportObj.UsedPartsDistanceTypeStr);
                            if(!tempEnumValue.HasValue) {
                                tempErrorMessage.Add("旧件运距类型不正确");
                            } else {
                                tempImportObj.UsedPartsDistanceType = tempEnumValue.Value;
                            }
                        }

                        //源服务站编号检查
                        fieldIndex = notNullableFieldsCompany.IndexOf("OriginCompanyCode".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.OriginCompanyCodeStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add("源服务站编号不能为空");
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.OriginCompanyCodeStr) > fieldLenghtCompany["OriginCompanyCode".ToUpper()])
                                tempErrorMessage.Add("源服务站编号过长");
                        }
                        //源服务站名称检查
                        fieldIndex = notNullableFieldsCompany.IndexOf("OriginCompanyName".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.OriginCompanyNameStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add("源服务站名称不能为空");
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.OriginCompanyNameStr) > fieldLenghtCompany["OriginCompanyName".ToUpper()])
                                tempErrorMessage.Add("源服务站名称过长");
                        }

                        //源旧件库编号检查
                        fieldIndex = notNullableFieldsUsedPartsWarehouse.IndexOf("OriginUsedPartsWarehouseCode".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.OriginUsedPartsWarehouseCodeStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add("源旧件库编号不能为空");
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.OriginUsedPartsWarehouseCodeStr) > fieldLenghtUsedPartsWarehouse["OriginUsedPartsWarehouseCode".ToUpper()])
                                tempErrorMessage.Add("源旧件库编号过长");
                        }
                        //源旧件库名称检查
                        fieldIndex = notNullableFieldsUsedPartsWarehouse.IndexOf("OriginUsedPartsWarehouseName".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.OriginUsedPartsWarehouseNameStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add("源旧件库名称不能为空");
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.OriginUsedPartsWarehouseNameStr) > fieldLenghtUsedPartsWarehouse["OriginUsedPartsWarehouseName".ToUpper()])
                                tempErrorMessage.Add("源旧件库名称过长");
                        }

                        //旧件运距检查
                        fieldIndex = notNullableFields.IndexOf("ShippingDistance".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.ShippingDistanceStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add("旧件运距不能为空");
                        } else {
                            int checkValue;
                            if(int.TryParse(tempImportObj.ShippingDistanceStr, out checkValue)) {
                                if(checkValue < 0) {
                                    tempErrorMessage.Add("旧件运距必须大于0");
                                }
                            } else {
                                tempErrorMessage.Add("旧件运距必须是数字");
                            }
                        }

                        //备注检查
                        fieldIndex = notNullableFields.IndexOf("Remark".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.RemarkStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchasePricingDetail_Validation15);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.RemarkStr) > fieldLenght["Remark".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchasePricingDetail_Validation16);
                        }
                        #endregion
                        if(tempErrorMessage.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });
                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    #region 剩下的数据进行业务检查

                    //6、文件内，旧件仓储企业名称、旧件仓库名称、旧件运距类型、源服务站编号、源旧件库编号的组合必须唯一，否则提示：文件内数据重复；
                    var groups = tempRightList.GroupBy(r => new {
                        r.UsedPartsCompanyNameStr,
                        r.UsedPartsWarehouseNameStr,
                        r.UsedPartsDistanceTypeStr,
                        r.OriginCompanyCodeStr,
                        r.OriginUsedPartsWarehouseCodeStr
                    }).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        groupItem.ErrorMsg = ErrorStrings.ImpPartsSupplierRelation_Validation31;
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    //1、旧件仓储企业名称，需存在于企业内，否则提示：旧件仓储企业*****不存在；
                    var usedPartsCompanyNamesNeedCheck = tempRightList.Select(r => r.UsedPartsCompanyNameStr).Distinct().ToArray();
                    var dbCompanys = new List<CompanyExtend>();
                    Func<string[], bool> getDbCompanys = value => {
                        var dbObj = new CompanyExtend {
                            Id = Convert.ToInt32(value[0]),
                            Code = value[1],
                            Name = value[2]
                        };
                        dbCompanys.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select Id,Code,Name from Company where status=1 ", "Name", false, usedPartsCompanyNamesNeedCheck, getDbCompanys);
                    foreach(var tempRight in tempRightList) {
                        var company = dbCompanys.FirstOrDefault(v => v.Name == tempRight.UsedPartsCompanyNameStr);
                        if(company == null) {
                            tempRight.ErrorMsg = String.Format("旧件仓储企业“{0}”不存在", tempRight.UsedPartsCompanyNameStr);
                            continue;
                        }
                        tempRight.UsedPartsCompanyId = company.Id;
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    //2、旧件仓库名称，需存在于 旧件仓库 内，否则提示：旧件仓库*****不存在；
                    var usedPartsWarehouseNamesNeedCheck = tempRightList.Select(r => r.UsedPartsWarehouseNameStr).Distinct().ToArray();
                    var dbUsedPartsWarehouses = new List<UsedPartsWarehouseExtend>();
                    Func<string[], bool> getDbUsedPartsWarehouses = value => {
                        var dbObj = new UsedPartsWarehouseExtend {
                            Id = Convert.ToInt32(value[0]),
                            Code = value[1],
                            Name = value[2]
                        };
                        dbUsedPartsWarehouses.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select Id,Code,Name from UsedPartsWarehouse where status=1 ", "Name", false, usedPartsWarehouseNamesNeedCheck, getDbUsedPartsWarehouses);
                    foreach(var tempRight in tempRightList) {
                        var usedPartsWarehouse = dbUsedPartsWarehouses.FirstOrDefault(v => v.Name == tempRight.UsedPartsWarehouseNameStr);
                        if(usedPartsWarehouse == null) {
                            tempRight.ErrorMsg = String.Format("旧件仓库“{0}”不存在", tempRight.UsedPartsWarehouseNameStr);
                            continue;
                        }
                        tempRight.UsedPartsWarehouseId = usedPartsWarehouse.Id;
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    //3、源服务站编号、源服务站名称的组合需存在于 企业 内，否则提示：源服务站编号***、源服务站名称***不存在；
                    var originCompanyCodesNeedCheck = tempRightList.Select(r => r.OriginCompanyCodeStr).Distinct().ToArray();
                    db.QueryDataWithInOperator("select Id,Code,Name from Company where status=1 ", "Code", false, originCompanyCodesNeedCheck, getDbCompanys);
                    foreach(var tempRight in tempRightList) {
                        var originCompanys = dbCompanys.FirstOrDefault(v => v.Code == tempRight.OriginCompanyCodeStr && v.Name == tempRight.OriginCompanyNameStr);
                        if(originCompanys == null) {
                            tempRight.ErrorMsg = String.Format("源服务站编号“{0}”、源服务站名称“{1}”不存在", tempRight.OriginCompanyCodeStr, tempRight.OriginCompanyNameStr);
                            continue;
                        }
                        tempRight.OriginCompanyId = originCompanys.Id;
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    //4、源旧件库编号、源旧件库名称的组合需存在于 旧件仓库 内，否则提示：源旧件库编号***、源旧件库名称***不存在；
                    var originUsedPartsWarehouseNamesNeedCheck = tempRightList.Select(r => r.OriginUsedPartsWarehouseNameStr).Distinct().ToArray();
                    db.QueryDataWithInOperator("select Id,Code,Name from UsedPartsWarehouse where status=1 ", "Name", false, originUsedPartsWarehouseNamesNeedCheck, getDbUsedPartsWarehouses);
                    foreach(var tempRight in tempRightList) {
                        var originUsedPartsWarehouse = dbUsedPartsWarehouses.FirstOrDefault(v => v.Code == tempRight.OriginUsedPartsWarehouseCodeStr && v.Name == tempRight.OriginUsedPartsWarehouseNameStr);
                        if(originUsedPartsWarehouse == null) {
                            tempRight.ErrorMsg = String.Format("源服务站编号“{0}”、源服务站名称“{1}”不存在", tempRight.OriginCompanyCodeStr, tempRight.OriginCompanyNameStr);
                            continue;
                        }
                        tempRight.OriginUsedPartsWarehouseId = originUsedPartsWarehouse.Id;
                    }

                    //7、系统内，旧件仓储企业Id、旧件仓库Id、旧件运距类型、源服务站Id、源旧件库Id的组合必须唯一，否则提示：系统内数据重复；
                    var usedPartsDistanceTypesNeedCheck = tempRightList.Select(r => tempExcelOperator.ImportHelper.GetEnumValue("DistanceType", r.UsedPartsDistanceTypeStr).ToString()).Distinct().ToArray();
                    var dbUsedPartsDistanceInfors = new List<UsedPartsDistanceInforExtend>();
                    Func<string[], bool> getDbUsedPartsDistanceInfors = value => {
                        var dbObj = new UsedPartsDistanceInforExtend {
                            UsedPartsCompanyId = Convert.ToInt32(value[0]),
                            UsedPartsWarehouseId = Convert.ToInt32(value[1]),
                            UsedPartsDistanceType = Convert.ToInt32(value[2]),
                            OriginCompanyId = Convert.ToInt32(value[3]),
                            OriginUsedPartsWarehouseId = Convert.ToInt32(value[4])
                        };
                        dbUsedPartsDistanceInfors.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select UsedPartsCompanyId,UsedPartsWarehouseId,UsedPartsDistanceType,OriginCompanyId,OriginUsedPartsWarehouseId from UsedPartsDistanceInfor where status=1 ", "UsedPartsDistanceType", false, usedPartsDistanceTypesNeedCheck, getDbUsedPartsDistanceInfors);
                    foreach(var tempRight in tempRightList) {
                        var usedPartsDistanceInfor = dbUsedPartsDistanceInfors.FirstOrDefault(v => v.UsedPartsCompanyId == tempRight.UsedPartsCompanyId && v.UsedPartsWarehouseId == tempRight.UsedPartsWarehouseId && v.UsedPartsDistanceType == tempRight.UsedPartsDistanceType && v.OriginCompanyId == tempRight.OriginCompanyId && v.OriginUsedPartsWarehouseId == tempRight.OriginUsedPartsWarehouseId);
                        if(usedPartsDistanceInfor != null) {
                            tempRight.ErrorMsg = "系统内数据重复";
                        }
                    }
                    #endregion
                    //获取所有不合格数据
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    //获取所有合格数据
                    rightList = allList.Except(errorList).ToList();
                    #region 将合格数据的值填上
                    foreach(var rightItem in rightList) {
                        rightItem.Remark = rightItem.RemarkStr;
                        rightItem.Status = tempExcelOperator.ImportHelper.GetEnumValue("BaseDataStatus", ErrorStrings.Export_Validation_PartsBranch_Used) ?? 0;
                    }
                    #endregion
                }
                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempObj = list[index - 1];
                            var values = new object[] {
                                #region 设置错误信息导出的列的值
                                tempObj.UsedPartsCompanyNameStr,tempObj.UsedPartsWarehouseNameStr ,
                                tempObj.UsedPartsDistanceTypeStr,tempObj.OriginCompanyCodeStr,
                                tempObj.OriginCompanyNameStr,tempObj.OriginUsedPartsWarehouseCodeStr ,
                                tempObj.OriginUsedPartsWarehouseNameStr ,tempObj.ShippingDistanceStr,
                                tempObj.RemarkStr,tempObj.ErrorMsg
                                #endregion
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }

                //导入所有合格数据
                if(!rightList.Any())
                    return true;
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    //开启事务，新增更新配件在一个事务内
                    var ts = conn.BeginTransaction();
                    try {
                        //新增配件
                        if(rightList.Any()) {
                            #region 获取对应的插入的SQL 通过db.GetInsertSql方法
                            //获取新增数据的sql语句，Id为主键
                            var sqlInsert = db.GetInsertSql("UsedPartsDistanceInfor", "Id", new[] {
                                "UsedPartsCompanyId","UsedPartsWarehouseId","UsedPartsDistanceType","OriginCompanyId","OriginUsedPartsWarehouseId","ShippingDistance","Status","Remark","CreatorId","CreatorName","CreateTime"
                            });
                            #endregion
                            var userInfo = Utils.GetCurrentUserInfo();
                            //往数据库增加配件信息
                            foreach(var item in rightList) {
                                #region 添加Sql的参数
                                var command = db.CreateDbCommand(sqlInsert, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("UsedPartsCompanyId", item.UsedPartsCompanyId));
                                command.Parameters.Add(db.CreateDbParameter("UsedPartsWarehouseId", item.UsedPartsWarehouseId));
                                command.Parameters.Add(db.CreateDbParameter("UsedPartsDistanceType", item.UsedPartsDistanceType));
                                command.Parameters.Add(db.CreateDbParameter("OriginCompanyId", item.OriginCompanyId));
                                command.Parameters.Add(db.CreateDbParameter("OriginUsedPartsWarehouseId", item.OriginUsedPartsWarehouseId));
                                command.Parameters.Add(db.CreateDbParameter("ShippingDistance", item.ShippingDistance));
                                command.Parameters.Add(db.CreateDbParameter("Status", item.Status));
                                command.Parameters.Add(db.CreateDbParameter("Remark", item.Remark));
                                command.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                command.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                command.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                command.ExecuteNonQuery();
                                #endregion

                            }
                        }
                        //无异常提交
                        ts.Commit();
                    } catch(Exception) {
                        //报错回滚
                        ts.Rollback();
                        throw;
                    } finally {
                        if(conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
            }
        }
    }
}
