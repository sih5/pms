﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        /// <summary>
        /// 合并导出旧件发运单
        /// </summary>
        public bool ExportUsedPartsShippingOrderWithDetail(int? billId, int? branchId, int? partsSalesCategoryId, string code, int? shippingMethod, int? destinationwarehouseid, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName) {
            fileName = GetExportFilePath("旧件发运主清单_" + ".xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated) {
                    throw new Exception(ErrorStrings.File_Validation_User);
                }

                var userInfo = Utils.GetCurrentUserInfo();
                var dealerid = userInfo.EnterpriseId;

                var db = DbHelper.GetDbHelp(ConnectionString);
                using(var conn = db.CreateDbConnection()) {

                    conn.Open();
                    var sql = new StringBuilder();
                    #region SQL
                    sql.Append(@"Select a.Code,--发运单编号
                               a.Branchname,--分公司名称
                               (Select Name
                                  From Partssalescategory
                                 Where Id = a.Partssalescategoryid) As Partssalescategoryname,--品牌
                               (Select Name
                                  From Usedpartswarehouse
                                 Where Id = a.Destinationwarehouseid),--收货仓库
                               cast(Case a.Shippingmethod--发运方式
                                 When 1 Then
                                  '公路货运'
                                 When 2 Then
                                  '公路客运'
                                 When 3 Then
                                  '铁路集装箱'
                                 When 4 Then
                                  '铁路快件'
                                 When 5 Then
                                  '特快专递'
                                 When 6 Then
                                  '邮寄包裹'
                                 When 7 Then
                                  '空运'
                                 When 8 Then
                                  '客户自行运输'
                                 When 9 Then
                                  '专车发运'
                               End as varchar2(20)),
                               cast(Case a.Dispatcher--发运方
                                 When 1 Then
                                  '经销商发运'
                                 When 2 Then
                                  '厂方发运'
                                 When 3 Then
                                  '第三方'
                               End as varchar2(20)),
                               a.Warehousecontact,--仓库联系人
                               a.Warehousephone,--仓库电话
                               a.Destinationaddress,--收货地址
                               a.Transportdriver,--承运司机
                               a.Driverphone,--司机电话
                               a.Supervisor,--督办人
                               a.Supervisorphone,--督办人电话
                              (Select sum(t.Quantity*t.UnitPrice) From UsedPartsShippingDetail t Where t.UsedPartsShippingOrderId =a.Id),--总金额
                               a.Remark,--备注
                               cast(Case b.ReceptionStatus
                               When 1 Then  '在途' 
                               When 2 Then  '已接收' 
                               When 3 Then  '未接收' 
                               When 4 Then  '罚没' 
                               When 5 Then  '不合格' End as varchar2(20)),
                               a.Receptionremark,--验收备注
                               d.Claimbillcode,--索赔单编号
                               b.Usedpartsbarcode,--旧件条码
                               b.Usedpartscode,--旧件编号
                               b.Usedpartsname,--旧件名称
                               cast(Decode(b.Iffaultyparts, 0, '否', '是') as varchar2(20)) As Iffaultyparts,--是否祸首件
                               b.Quantity,--数量：入库量
                               b.Shippingremark,--发运备注
                               b.Unitprice,--单价
                               b.Faultypartscode,--祸首件编号
                               b.Faultypartsname,--祸首件名称
                                b.FaultypartssupplierCode,
                               b.Faultypartssuppliername--祸首件供应商名称
                          From Usedpartsshippingorder a
                         Inner Join Usedpartsshippingdetail b
                            On a.Id = b.Usedpartsshippingorderid
                         Inner Join Repairclaimbill d
                            On b.Claimbillid = d.Id
                                      WHERE a.dealerid=" + dealerid);
                    #endregion

                    var dbParameters = new List<DbParameter>();
                    if(billId.HasValue) {
                        sql.Append(@" and a.Id={0}billId");
                        dbParameters.Add(db.CreateDbParameter("billId", billId.Value));
                    } else {


                        if(branchId.HasValue) {
                            sql.Append(@" and a.branchId={0}branchId");
                            dbParameters.Add(db.CreateDbParameter("branchId", branchId.Value));
                        }

                        if(partsSalesCategoryId.HasValue) {
                            sql.Append(@" and a.partsSalesCategoryId={0}partsSalesCategoryId");
                            dbParameters.Add(db.CreateDbParameter("partsSalesCategoryId", partsSalesCategoryId.Value));
                        }

                        if(!string.IsNullOrEmpty(code)) {
                            sql.Append(@" and a.code ={0}code ");
                            dbParameters.Add(db.CreateDbParameter("code", code));
                        }

                        if(shippingMethod.HasValue) {
                            sql.Append(@" and a.shippingMethod ={0}shippingMethod ");
                            dbParameters.Add(db.CreateDbParameter("shippingMethod", shippingMethod.Value));
                        }

                        if(destinationwarehouseid.HasValue) {
                            sql.Append(@" and a.destinationwarehouseid ={0}destinationwarehouseid ");
                            dbParameters.Add(db.CreateDbParameter("destinationwarehouseid", destinationwarehouseid.Value));
                        }

                        if(status.HasValue) {
                            sql.Append(@" and a.status ={0}Status");
                            dbParameters.Add(db.CreateDbParameter("status", status.Value));
                        }
                        if(createTimeBegin.HasValue) {
                            sql.Append(@" and a.createTime >=to_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(@" and a.createTime <=to_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                    }
                    sql.Append(@" order by b.UsedPartsName,b.ClaimBillId");
                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    ErrorStrings.Export_Title_SupplierShippingOrder_Code,ErrorStrings.Export_Title_Branch_BranchName,ErrorStrings.Export_Title_Partssalescategory_Name,"收货仓库",ErrorStrings.Export_Title_PartsPurchaseOrder_ShippingMethod,"发运方","仓库联系人","仓库电话",ErrorStrings.Export_Title_PartsPurchaseOrder_ReceiveAddress,
                                    "承运司机","司机电话","督办人","督办人电话",ErrorStrings.Export_Title_InternalAllocationBill_TotalAmount,ErrorStrings.Export_Title_PartsBranch_Remark,"验收状态","验收备注","索赔单编号","旧件条码",
                                    "旧件编号","旧件名称","是否祸首件","数量：入库量","发运备注",ErrorStrings.Export_Title_InternalAllocationBill_UnitPrice,"祸首件编号","祸首件名称","责任单位编号","责任单位名称" 
                                 };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }

                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }

        /// <summary>
        /// 导出旧件发运单
        /// </summary>
        public bool ExportUsedPartsShippingOrder(int? billId, int? branchId, int? partsSalesCategoryId, string code, int? shippingMethod, int? destinationwarehouseid, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName) {
            fileName = GetExportFilePath("旧件发运单.xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated) {
                    throw new Exception(ErrorStrings.File_Validation_User);
                }

                var userInfo = Utils.GetCurrentUserInfo();
                var dealerid = userInfo.EnterpriseId;

                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {

                    conn.Open();
                    var sql = new StringBuilder();
                    #region SQL
                    sql.Append(@"Select a.Branchname,--分公司名称
                                   (Select Name
                                      From Partssalescategory
                                     Where Id = a.Partssalescategoryid) As Partssalescategoryname,--品牌
                                   (Select Name
                                      From Usedpartswarehouse
                                     Where Id = a.Destinationwarehouseid),--收货仓库
                                   cast(Case a.Shippingmethod--发运方式
                                     When 1 Then
                                      '公路货运'
                                     When 2 Then
                                      '公路客运'
                                     When 3 Then
                                      '铁路集装箱'
                                     When 4 Then
                                      '铁路快件'
                                     When 5 Then
                                      '特快专递'
                                     When 6 Then
                                      '邮寄包裹'
                                     When 7 Then
                                      '空运'
                                     When 8 Then
                                      '客户自行运输'
                                     When 9 Then
                                      '专车发运'
                                   End as varchar2(20)),
                                   cast(Case a.Dispatcher--发运方
                                     When 1 Then
                                      '经销商发运'
                                     When 2 Then
                                      '厂方发运'
                                     When 3 Then
                                      '第三方'
                                   End as varchar2(20)),
                                   a.Warehousecontact,--仓库联系人
                                   a.Warehousephone,--仓库电话
                                   a.Destinationaddress,--收货地址
                                   a.Transportdriver,--承运司机
                                   a.Driverphone,--司机电话
                                   a.Supervisor,--督办人
                                   a.Supervisorphone,--督办人电话
                                    (Select sum(t.Quantity*t.UnitPrice) From UsedPartsShippingDetail t Where t.UsedPartsShippingOrderId =a.Id),--总金额
                                   a.Remark,--备注
                                   a.Receptionremark,--验收备注
                                   a.Creatorname,--创建人
                                   a.Createtime--创建时间
                              From Usedpartsshippingorder a
                              WHERE a.dealerid=" + dealerid);
                    #endregion

                    var dbParameters = new List<DbParameter>();
                    if(billId.HasValue) {
                        sql.Append(@" and a.Id={0}billId");
                        dbParameters.Add(db.CreateDbParameter("billId", billId.Value));
                    } else {
                        if(branchId.HasValue) {
                            sql.Append(@" and a.branchId={0}branchId");
                            dbParameters.Add(db.CreateDbParameter("branchId", branchId.Value));
                        }

                        if(partsSalesCategoryId.HasValue) {
                            sql.Append(@" and a.partsSalesCategoryId={0}partsSalesCategoryId");
                            dbParameters.Add(db.CreateDbParameter("partsSalesCategoryId", partsSalesCategoryId.Value));
                        }

                        if(!string.IsNullOrEmpty(code)) {
                            sql.Append(@" and a.code ={0}code ");
                            dbParameters.Add(db.CreateDbParameter("code", code));
                        }

                        if(shippingMethod.HasValue) {
                            sql.Append(@" and a.shippingMethod ={0}shippingMethod ");
                            dbParameters.Add(db.CreateDbParameter("shippingMethod", shippingMethod.Value));
                        }

                        if(destinationwarehouseid.HasValue) {
                            sql.Append(@" and a.destinationwarehouseid ={0}destinationwarehouseid ");
                            dbParameters.Add(db.CreateDbParameter("destinationwarehouseid", destinationwarehouseid.Value));
                        }

                        if(status.HasValue) {
                            sql.Append(@" and a.status ={0}Status");
                            dbParameters.Add(db.CreateDbParameter("status", status.Value));
                        }
                        if(createTimeBegin.HasValue) {
                            sql.Append(@" and a.createTime >=to_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(@" and a.createTime <=to_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                    }
                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    ErrorStrings.Export_Title_Branch_BranchName,ErrorStrings.Export_Title_Partssalescategory_Name,"收货仓库",ErrorStrings.Export_Title_PartsPurchaseOrder_ShippingMethod,"发运方","仓库联系人","仓库电话",ErrorStrings.Export_Title_PartsPurchaseOrder_ReceiveAddress,
                                    "承运司机","司机电话","督办人","督办人电话",ErrorStrings.Export_Title_InternalAllocationBill_TotalAmount,ErrorStrings.Export_Title_PartsBranch_Remark,"验收备注",ErrorStrings.Export_Title_AccountPeriod_CreatorName,ErrorStrings.Export_Title_AccountPeriod_CreateTime  
                                 };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }

                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }

        /// <summary>
        /// 导出旧件发运确认单
        /// </summary>
        public bool ExportUsedPartsShippingOrderForConfirm(int? billId, int personnelId, int? branchId, int? partsSalesCategoryId, string dealerCode, string dealerName, string code, int? shippingMethod, int? destinationwarehouseid, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName) {
            fileName = GetExportFilePath("旧件发运确认单.xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated) {
                    throw new Exception(ErrorStrings.File_Validation_User);
                }
                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {

                    conn.Open();
                    var sql = new StringBuilder();
                    #region SQL
                    sql.AppendFormat(@"Select a.Dealercode,
                                   a.Dealername,
                                   a.Code,
                                   a.Branchname,
                                   (Select Name
                                      From Partssalescategory
                                     Where Id = a.Partssalescategoryid) As Partssalescategoryname,
                                   (Select Name
                                      From Usedpartswarehouse
                                     Where Id = a.Destinationwarehouseid),
                                   cast(Case a.Shippingmethod
                                     When 1 Then
                                      '公路货运'
                                     When 2 Then
                                      '公路客运'
                                     When 3 Then
                                      '铁路集装箱'
                                     When 4 Then
                                      '铁路快件'
                                     When 5 Then
                                      '特快专递'
                                     When 6 Then
                                      '邮寄包裹'
                                     When 7 Then
                                      '空运'
                                     When 8 Then
                                      '客户自行运输'
                                     When 9 Then
                                      '专车发运'
                                   End as varchar2(20)),
                                   cast(Case a.Dispatcher
                                     When 1 Then
                                      '经销商发运'
                                     When 2 Then
                                      '厂方发运'
                                     When 3 Then
                                      '第三方'
                                   End as varchar2(20)),
                                   a.Warehousecontact,
                                   a.Warehousephone,
                                   a.Destinationaddress,
                                   a.Transportdriver,
                                   a.Driverphone,
                                   a.Supervisor,
                                   a.Supervisorphone,
                         (Select sum(t.Quantity) From UsedPartsShippingDetail t
                           Where t.UsedPartsShippingOrderId =a.Id), --总数量 = sum旧件发运清单.数量）
                                 (Select sum(t.Quantity*t.UnitPrice) From UsedPartsShippingDetail t Where t.UsedPartsShippingOrderId =a.Id),
                                   a.Remark,
                                   a.Receptionremark,
                                   a.Creatorname,
                                   a.Createtime
                              From Usedpartsshippingorder a
                             Where a.destinationwarehouseid In (Select t.usedpartswarehouseid From UsedPartsWarehouseStaff t Where t.personnelid={0}) ", personnelId);
                    #endregion
                    var dbParameters = new List<DbParameter>();
                    if(billId.HasValue) {
                        sql.Append(@" and a.Id={0}billId");
                        dbParameters.Add(db.CreateDbParameter("billId", billId.Value));
                    } else {
                        if(branchId.HasValue) {
                            sql.Append(@" and a.branchId={0}branchId");
                            dbParameters.Add(db.CreateDbParameter("branchId", branchId.Value));
                        }

                        if(partsSalesCategoryId.HasValue) {
                            sql.Append(@" and a.partsSalesCategoryId={0}partsSalesCategoryId");
                            dbParameters.Add(db.CreateDbParameter("partsSalesCategoryId", partsSalesCategoryId.Value));
                        }
                        if(!string.IsNullOrEmpty(dealerCode)) {
                            sql.Append(@" and Upper(a.dealerCode) like Upper({0}dealerCode) ");
                            dbParameters.Add(db.CreateDbParameter("dealerCode", "%" + dealerCode + "%"));
                        }
                        if(!string.IsNullOrEmpty(dealerName)) {
                            sql.Append(@" and a.dealerName like{0}dealerName ");
                            dbParameters.Add(db.CreateDbParameter("dealerName", "%" + dealerName + "%"));
                        }
                        if(!string.IsNullOrEmpty(code)) {
                            sql.Append(@" and a.code =Upper({0}code) ");
                            dbParameters.Add(db.CreateDbParameter("code", code));
                        }

                        if(shippingMethod.HasValue) {
                            sql.Append(@" and a.shippingMethod ={0}shippingMethod ");
                            dbParameters.Add(db.CreateDbParameter("shippingMethod", shippingMethod.Value));
                        }

                        if(destinationwarehouseid.HasValue) {
                            sql.Append(@" and a.destinationwarehouseid ={0}destinationwarehouseid ");
                            dbParameters.Add(db.CreateDbParameter("destinationwarehouseid", destinationwarehouseid.Value));
                        }

                        if(status.HasValue) {
                            sql.Append(@" and a.status ={0}Status");
                            dbParameters.Add(db.CreateDbParameter("status", status.Value));
                        }
                        if(createTimeBegin.HasValue) {
                            sql.Append(@" and a.createTime >=to_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(@" and a.createTime <=to_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                    }
                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    ErrorStrings.Export_Title_Dealer_Code,ErrorStrings.Export_Title_Dealer_Name,ErrorStrings.Export_Title_SupplierShippingOrder_Code,ErrorStrings.Export_Title_Branch_BranchName,ErrorStrings.Export_Title_Partssalescategory_Name,"收货仓库",ErrorStrings.Export_Title_PartsPurchaseOrder_ShippingMethod,"发运方","仓库联系人","仓库电话",ErrorStrings.Export_Title_PartsPurchaseOrder_ReceiveAddress,
                                    "承运司机","司机电话","督办人","督办人电话","总数量",ErrorStrings.Export_Title_InternalAllocationBill_TotalAmount,ErrorStrings.Export_Title_PartsBranch_Remark,"验收备注",ErrorStrings.Export_Title_AccountPeriod_CreatorName,ErrorStrings.Export_Title_AccountPeriod_CreateTime 
                                 };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }

                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }

        /// <summary>
        /// 合并导出旧件发运确认单
        /// </summary>
        public bool ExportUsedPartsShippingOrderForConfirmWithDetail(int? billId, int personnelId, int? branchId, int? partsSalesCategoryId, string dealerCode, string dealerName, string code, int? shippingMethod, int? destinationwarehouseid, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName) {
            fileName = GetExportFilePath("旧件发运确认主清单_" + ".xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated) {
                    throw new Exception(ErrorStrings.File_Validation_User);
                }
                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {

                    conn.Open();
                    var sql = new StringBuilder();
                    #region SQL
                    sql.AppendFormat(@"Select a.Dealercode,
                                   a.Dealername,
                                    a.Code,
                                b.Usedpartsbarcode,
                               b.Usedpartscode,
                               b.Usedpartsname,
                                b.Quantity,
                                b.Unitprice,
                                d.Claimbillcode,
                               b.Faultypartssuppliername,
                                b.Faultypartsname,
                                b.faultypartssuppliercode,
                                b.Faultypartscode,
                               a.Branchname,
                               (Select Name
                                  From Partssalescategory
                                 Where Id = a.Partssalescategoryid) As Partssalescategoryname,
                               (Select Name
                                  From Usedpartswarehouse
                                 Where Id = a.Destinationwarehouseid),
                               cast(Case a.Shippingmethod
                                 When 1 Then
                                  '公路货运'
                                 When 2 Then
                                  '公路客运'
                                 When 3 Then
                                  '铁路集装箱'
                                 When 4 Then
                                  '铁路快件'
                                 When 5 Then
                                  '特快专递'
                                 When 6 Then
                                  '邮寄包裹'
                                 When 7 Then
                                  '空运'
                                 When 8 Then
                                  '客户自行运输'
                                 When 9 Then
                                  '专车发运'
                               End as varchar2(20)),
                               cast(Case a.Dispatcher
                                 When 1 Then
                                  '经销商发运'
                                 When 2 Then
                                  '厂方发运'
                                 When 3 Then
                                  '第三方'
                               End as varchar2(20)),
                               a.Warehousecontact,
                               a.Warehousephone,
                               a.Destinationaddress,
                               a.Transportdriver,
                               a.Driverphone,
                               a.Supervisor,
                               a.Supervisorphone,
                                (Select sum(t.Quantity*t.UnitPrice) From UsedPartsShippingDetail t Where t.UsedPartsShippingOrderId =a.Id),
                               a.Remark,
                              cast(Case    b.ReceptionStatus
                               When 1 Then  '在途' 
                               When 2 Then  '已接收' 
                               When 3 Then  '未接收' 
                               When 4 Then  '罚没' 
                               When 5 Then  '不合格' End as varchar2(20)),
                               a.Receptionremark                           
                          From Usedpartsshippingorder a
                         Inner Join Usedpartsshippingdetail b
                            On a.Id = b.Usedpartsshippingorderid
                         Inner Join Repairclaimbill d
                            On b.Claimbillid = d.Id
                                      WHERE a.destinationwarehouseid In (Select t.usedpartswarehouseid From UsedPartsWarehouseStaff t Where t.personnelid={0}) ", personnelId);
                    #endregion

                    var dbParameters = new List<DbParameter>();
                    if(billId.HasValue) {
                        sql.Append(@" and a.Id={0}billId");
                        dbParameters.Add(db.CreateDbParameter("billId", billId.Value));
                    } else {
                        if(branchId.HasValue) {
                            sql.Append(@" and a.branchId={0}branchId");
                            dbParameters.Add(db.CreateDbParameter("branchId", branchId.Value));
                        }

                        if(partsSalesCategoryId.HasValue) {
                            sql.Append(@" and a.partsSalesCategoryId={0}partsSalesCategoryId");
                            dbParameters.Add(db.CreateDbParameter("partsSalesCategoryId", partsSalesCategoryId.Value));
                        }

                        if(!string.IsNullOrEmpty(dealerCode)) {
                            sql.Append(@" and Upper(a.dealerCode) like Upper({0}dealerCode) ");
                            dbParameters.Add(db.CreateDbParameter("dealerCode", "%" + dealerCode + "%"));
                        }
                        if(!string.IsNullOrEmpty(dealerName)) {
                            sql.Append(@" and a.dealerName like {0}dealerName ");
                            dbParameters.Add(db.CreateDbParameter("dealerName", "%" + dealerName + "%"));
                        }

                        if(!string.IsNullOrEmpty(code)) {
                            sql.Append(@" and a.code ={0}code ");
                            dbParameters.Add(db.CreateDbParameter("code", code));
                        }

                        if(shippingMethod.HasValue) {
                            sql.Append(@" and a.shippingMethod ={0}shippingMethod ");
                            dbParameters.Add(db.CreateDbParameter("shippingMethod", shippingMethod.Value));
                        }

                        if(destinationwarehouseid.HasValue) {
                            sql.Append(@" and a.destinationwarehouseid ={0}destinationwarehouseid ");
                            dbParameters.Add(db.CreateDbParameter("destinationwarehouseid", destinationwarehouseid.Value));
                        }

                        if(status.HasValue) {
                            sql.Append(@" and a.status ={0}Status");
                            dbParameters.Add(db.CreateDbParameter("status", status.Value));
                        }
                        if(createTimeBegin.HasValue) {
                            sql.Append(@" and a.createTime >=to_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(@" and a.createTime <=to_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                    }
                    sql.Append(@" order by b.UsedPartsName,b.ClaimBillId ");
                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                //服务站编号 服务站名称 发运单编号 旧件条码 旧件编号 旧件名称 数量:入库量 单价 索赔单编号 祸首件供应商名称  祸首件名称 
                                //祸首件供应商编号 祸首件编号 分公司名称 品牌 收货仓库 发运方式 
                                //发运方 仓库联系人 仓库电话 收货地址 承运司机 司机电话 督办人 督办人电话 总金额 备注 验收备注
                                return new object[] {
                                    ErrorStrings.Export_Title_Dealer_Code,ErrorStrings.Export_Title_Dealer_Name,ErrorStrings.Export_Title_SupplierShippingOrder_Code,"旧件条码","旧件编号","旧件名称","数量：入库量",ErrorStrings.Export_Title_InternalAllocationBill_UnitPrice,"索赔单编号","责任单位名称" ,"祸首件名称","责任单位编号","祸首件编号",
                                    ErrorStrings.Export_Title_Branch_BranchName,ErrorStrings.Export_Title_Partssalescategory_Name,"收货仓库",ErrorStrings.Export_Title_PartsPurchaseOrder_ShippingMethod,"发运方","仓库联系人","仓库电话",ErrorStrings.Export_Title_PartsPurchaseOrder_ReceiveAddress,
                                    "承运司机","司机电话","督办人","督办人电话",ErrorStrings.Export_Title_InternalAllocationBill_TotalAmount,ErrorStrings.Export_Title_PartsBranch_Remark,"验收状态","验收备注"//,"是否祸首件","发运备注"
                                 };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }

                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }
    }
}
