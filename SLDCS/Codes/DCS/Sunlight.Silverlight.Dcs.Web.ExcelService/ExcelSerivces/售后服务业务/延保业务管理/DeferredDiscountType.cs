﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        public bool ImportDeferredDiscountType(string fileName, out int excelImportNum, out List<DeferredDiscountTypeExtend> rightData, out List<DeferredDiscountTypeExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<DeferredDiscountTypeExtend>();
            var rightList = new List<DeferredDiscountTypeExtend>();
            var allList = new List<DeferredDiscountTypeExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);
                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("DeferredDiscountType", out notNullableFields, out fieldLenght);

                List<object> excelColumns;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    //指定文件中的列对应的名称
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Partssalescategory_Name, "PartsSalesCategory");
                    excelOperator.AddColumnDataSource("延保产品编号", "ExtendedWarrantyProductCode");
                    excelOperator.AddColumnDataSource("延保产品", "ExtendedWarrantyProductName");
                    excelOperator.AddColumnDataSource("延保折扣类型", "ExtendedWarrantyAgioType");
                    excelOperator.AddColumnDataSource("延保折扣", "ExtendedWarrantyAgio");

                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    //Excel单行导入
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        //接收Excel导入数据
                        var tempImportObj = new DeferredDiscountTypeExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);

                        tempImportObj.PartsSalesCategoryStr = newRow["PartsSalesCategory"];
                        tempImportObj.ExtendedWarrantyProductCode = newRow["ExtendedWarrantyProductCode"];
                        tempImportObj.ExtendedWarrantyProductNameStr = newRow["ExtendedWarrantyProductName"];
                        tempImportObj.ExtendedWarrantyAgioTypeStr = newRow["ExtendedWarrantyAgioType"];
                        tempImportObj.ExtendedWarrantyAgioStr = newRow["ExtendedWarrantyAgio"];

                        var errorMsgs = new List<string>();
                        //旧件条码检查
                        var fieldIndex1 = notNullableFields.IndexOf("PartsSalesCategory".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.PartsSalesCategoryStr)) {
                            if(fieldIndex1 > -1)
                                errorMsgs.Add(ErrorStrings.ImpDeferredDiscountType_Validation1);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.PartsSalesCategoryStr) > fieldLenght["PartsSalesCategory".ToUpper()])
                                errorMsgs.Add(ErrorStrings.ImpDeferredDiscountType_Validation2);
                        }
                        var fieldIndex2 = notNullableFields.IndexOf("ExtendedWarrantyProductName".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.ExtendedWarrantyProductNameStr)) {
                            if(fieldIndex2 > -1)
                                errorMsgs.Add(ErrorStrings.ImpDeferredDiscountType_Validation3);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.ExtendedWarrantyProductNameStr) > fieldLenght["ExtendedWarrantyProductName".ToUpper()])
                                errorMsgs.Add(ErrorStrings.ImpDeferredDiscountType_Validation4);
                        }
                        var fieldIndex3 = notNullableFields.IndexOf("ExtendedWarrantyAgioType".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.ExtendedWarrantyAgioTypeStr)) {
                            if(fieldIndex3 > -1)
                                errorMsgs.Add(ErrorStrings.ImpDeferredDiscountType_Validation5);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.ExtendedWarrantyAgioTypeStr) > fieldLenght["ExtendedWarrantyAgioType".ToUpper()])
                                errorMsgs.Add(ErrorStrings.ImpDeferredDiscountType_Validation6);
                        }
                        var fieldIndex4 = notNullableFields.IndexOf("ExtendedWarrantyAgio".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.ExtendedWarrantyAgioStr)) {
                            if(fieldIndex4 > -1)
                                errorMsgs.Add(ErrorStrings.ImpDeferredDiscountType_Validation7);
                        } else {
                            double checkValue;
                            if(double.TryParse(tempImportObj.ExtendedWarrantyAgioStr, out checkValue)) {
                                if(checkValue <= 0 || checkValue > 0.99) {
                                    errorMsgs.Add("延保折扣必须大于0或小于等于0.99");
                                }
                            } else {
                                errorMsgs.Add("延保折扣必须是数字");
                            }
                        }

                        if(errorMsgs.Count > 0)
                            tempImportObj.ErrorMsg = string.Join("; ", errorMsgs);
                        allList.Add(tempImportObj);
                        return false;
                    });
                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    //业务检查
                    //6、文件内，旧件仓储企业名称、旧件仓库名称、旧件运距类型、源服务站编号、源旧件库编号的组合必须唯一，否则提示：文件内数据重复；
                    var groups = tempRightList.GroupBy(r => new {
                        r.PartsSalesCategoryStr,
                        r.ExtendedWarrantyProductCode,
                        r.ExtendedWarrantyProductNameStr,
                        r.ExtendedWarrantyAgioTypeStr,
                        r.ExtendedWarrantyAgioStr
                    }).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        groupItem.ErrorMsg = ErrorStrings.ImpPartsSupplierRelation_Validation31;
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    //1、查询品牌，否则提示：品牌*****不存在；
                    var partsSalesCategoryCheck = tempRightList.Select(r => r.PartsSalesCategoryStr).Distinct().ToArray();
                    var dbPartsSalesCategorys = new List<PartsSalesCategory>();
                    Func<string[], bool> getDbPartsSalesCategorys = value => {
                        var dbObj = new PartsSalesCategory {
                            Id = Convert.ToInt32(value[0]),
                            Code = value[1],
                            Name = value[2]
                        };
                        dbPartsSalesCategorys.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select Id,Code,Name from PartsSalesCategory where status=1 ", "Name", false, partsSalesCategoryCheck, getDbPartsSalesCategorys);
                    foreach(var tempRight in tempRightList) {
                        var partsSalesCategory = dbPartsSalesCategorys.FirstOrDefault(v => v.Name == tempRight.PartsSalesCategoryStr);
                        if(partsSalesCategory == null) {
                            tempRight.ErrorMsg = String.Format(ErrorStrings.ImpPartsSupplierRelation_Validation28, tempRight.PartsSalesCategoryStr);
                            continue;
                        }
                        tempRight.PartsSalesCategoryId = partsSalesCategory.Id;
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    //2、查询延保产品，否则提示：延保产品*****不存在；
                    var extendedWarrantyProductCheck = tempRightList.Select(r => r.ExtendedWarrantyProductCode).Distinct().ToArray();
                    var dbExtendedWarrantyProducts = new List<ExtendedWarrantyProduct>();
                    Func<string[], bool> getDbExtendedWarrantyProducts = value => {
                        var dbObj = new ExtendedWarrantyProduct {
                            Id = Convert.ToInt32(value[0]),
                            ExtendedWarrantyProductCode = value[1],
                            ExtendedWarrantyProductName = value[2],
                            PartsSalesCategoryId = int.Parse(value[3]),
                            PartsSalesCategory = value[4],
                        };
                        dbExtendedWarrantyProducts.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select Id,ExtendedWarrantyProductCode,ExtendedWarrantyProductName,PartsSalesCategoryId,PartsSalesCategory from ExtendedWarrantyProduct where status=1 ", "ExtendedWarrantyProductCode", false, extendedWarrantyProductCheck, getDbExtendedWarrantyProducts);
                    foreach(var tempRight in tempRightList) {
                        var extendedWarrantyProduct = dbExtendedWarrantyProducts.FirstOrDefault(v => v.ExtendedWarrantyProductName == tempRight.ExtendedWarrantyProductNameStr && v.PartsSalesCategoryId == tempRight.PartsSalesCategoryId);
                        if(extendedWarrantyProduct == null) {
                            tempRight.ErrorMsg = String.Format("延保产品编号为“{0}”的延保产品不存在", tempRight.ExtendedWarrantyProductCode);
                            continue;
                        }
                        tempRight.ExtendedWarrantyProductId = extendedWarrantyProduct.Id;
                        tempRight.ExtendedWarrantyProductIdStr = extendedWarrantyProduct.Id.ToString();
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    //查询延保折扣类型 
                    var dbDeferredDiscountTypeExtends = new List<DeferredDiscountTypeExtend>();
                    Func<string[], bool> getDbDeferredDiscountTypeExtends = value => {
                        var dbObj = new DeferredDiscountTypeExtend {
                            Id = Convert.ToInt32(value[0]),
                            PartsSalesCategory = value[1],
                            ExtendedWarrantyProductName = value[2],
                            ExtendedWarrantyAgio = value[3],
                            ExtendedWarrantyProductId= Convert.ToInt32(value[4])
                        };
                        dbDeferredDiscountTypeExtends.Add(dbObj);
                        return false;
                    };
                    var extendedWarrantyProductIdCheck = tempRightList.Select(r => r.ExtendedWarrantyProductIdStr).Distinct().ToArray();
                    db.QueryDataWithInOperator("select Id,PartsSalesCategory,ExtendedWarrantyProductName,ExtendedWarrantyAgio,ExtendedWarrantyProductId from DeferredDiscountType where status=1 ", "ExtendedWarrantyProductId", false, extendedWarrantyProductIdCheck, getDbDeferredDiscountTypeExtends);
                    foreach(var tempRight in tempRightList) {
                        var dbDeferredDiscountTypeExtend = dbDeferredDiscountTypeExtends.FirstOrDefault(v => v.ExtendedWarrantyProductId == tempRight.ExtendedWarrantyProductId && v.PartsSalesCategory == tempRight.PartsSalesCategoryStr && v.ExtendedWarrantyAgio == Math.Round(Convert.ToDouble(tempRight.ExtendedWarrantyAgioStr), 2).ToString());
                        if(dbDeferredDiscountTypeExtend != null) {
                            tempRight.ErrorMsg = "延保折扣类型已经存在";
                            continue;
                        }
                    }
                    //获取所有不合格数据
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    //获取所有合格数据
                    rightList = allList.Except(errorList).ToList();

                    //将合格数据的值填上

                }
                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempObj = list[index - 1];
                            var values = new object[] {
                                //设置错误信息导出的列的值
                                tempObj.PartsSalesCategoryStr,
                                tempObj.ExtendedWarrantyProductCode,
                                tempObj.ExtendedWarrantyProductNameStr,
                                tempObj.ExtendedWarrantyAgioTypeStr,
                                tempObj.ExtendedWarrantyAgioStr, 
                                tempObj.ErrorMsg
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }
                //导入所有合格数据
                if(!rightList.Any())
                    return true;
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    //开启事务，新增更新配件在一个事务内
                    var ts = conn.BeginTransaction();
                    try {
                        //新增配件
                        if(rightList.Any()) {
                            #region 获取对应的插入的SQL 通过db.GetInsertSql方法
                            //获取新增数据的sql语句，Id为主键
                            var sqlInsert = db.GetInsertSql("DeferredDiscountType", "Id", new[] {
                                "PartsSalesCategoryId","PartsSalesCategory","ExtendedWarrantyProductId","ExtendedWarrantyProductName","ExtendedWarrantyAgioType","ExtendedWarrantyAgio","Status","CreatorId","CreatorName","CreatorTime"
                            });
                            #endregion
                            var userInfo = Utils.GetCurrentUserInfo();
                            //往数据库增加配件信息
                            foreach(var item in rightList) {
                                #region 添加Sql的参数
                                var command = db.CreateDbCommand(sqlInsert, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("PartsSalesCategoryId", item.PartsSalesCategoryId));
                                command.Parameters.Add(db.CreateDbParameter("PartsSalesCategory", item.PartsSalesCategoryStr));
                                command.Parameters.Add(db.CreateDbParameter("ExtendedWarrantyProductId", item.ExtendedWarrantyProductId));
                                command.Parameters.Add(db.CreateDbParameter("ExtendedWarrantyProductName", item.ExtendedWarrantyProductNameStr));
                                command.Parameters.Add(db.CreateDbParameter("ExtendedWarrantyAgioType", item.ExtendedWarrantyAgioTypeStr));
                                command.Parameters.Add(db.CreateDbParameter("ExtendedWarrantyAgio", Math.Round(Convert.ToDouble(item.ExtendedWarrantyAgioStr), 2)));
                                command.Parameters.Add(db.CreateDbParameter("Status", 1));
                                command.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                command.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                command.Parameters.Add(db.CreateDbParameter("CreatorTime", DateTime.Now));
                                command.ExecuteNonQuery();
                                #endregion

                            }
                        }
                        //无异常提交
                        ts.Commit();
                    } catch(Exception) {
                        //报错回滚
                        ts.Rollback();
                        throw;
                    } finally {
                        if(conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }
    }
}
