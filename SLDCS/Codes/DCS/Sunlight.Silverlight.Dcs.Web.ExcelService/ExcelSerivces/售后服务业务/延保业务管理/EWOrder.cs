﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;


namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        public bool ExportEWOrder(int[] ids, int? branchId, int? dealerId, int? partsSalesCategoryId, int? serviceProductLineId, string extendedWarrantyOrderCode, string VIN, int? status,
        string vehicleLicensePlate, string customerName, string cellNumber, string dealerCode, string dealerName, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName) {

            fileName = GetExportFilePath("延保订单管理_" + ".xlsx");

            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated) {
                    throw new Exception(ErrorStrings.File_Validation_User);
                }
                var db = DbHelper.GetDbHelp(ConnectionString);
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.Append(@"   select 
                                       a.ExtendedWarrantyOrderCode,
                                       a.PartsSalesCategoryName,
                                       a.VIN,
                                       a.ServiceProductLine,
                                       a.DealerName,
                                       a.DealerCode,
                                       a.SalesDate,
                                       a.CarSalesDate,
                                       a.CustomerName,
                                       a.CellNumber,
                                       a.Email,
                                       (select value from keyvalueitem where NAME = 'ExtendedWarrantyIDType'and key=a.IDType) As IDType,
                                       a.IDNumer,
                                       a.province||a.city||a.region,
                                       (select RepairTarget from RepairObject where id=a.RepairObjectId) As RepairObjectId,
                                       a.SerialNumber,                            
                                       a.Mileage,
                                       a.PurchaseAmount,
                                       a.EngineModel,
                                       a.VehicleLicensePlate,
                                       a.EngineCylinder,
                                       a.TotalAmount,
                                       a.DealerTotalAmount,
                                       a.SellPersonnel,
                                        (select value from keyvalueitem where NAME = 'ExtendedWarrantyStatus'and key=a.status) As status,
                                       a.Remark,
                                       a.CreatorName,
                                       a.CreatorTime,
                                       a.ModifierName,
                                       a.ModifierTime,
                                       a.CheckerName,
                                       a.CheckerTime,
                                       a.AbandonerName,
                                       a.AbandonerTime
                                  from ExtendedWarrantyOrder a where 1=1 ");
                    var dbParameters = new List<DbParameter>();
                    if(ids != null && ids.Length > 0) {
                        sql.Append(string.Format(" and a.id in ({0})", string.Join(",", ids)));
                    } else {
                        if(branchId.HasValue) {
                            sql.Append(@" and a.branchId = {0}branchId ");
                            dbParameters.Add(db.CreateDbParameter("branchId", branchId.Value));
                        }
                        if(dealerId.HasValue) {
                            sql.Append(@" and a.dealerId = {0}dealerId ");
                            dbParameters.Add(db.CreateDbParameter("dealerId", dealerId.Value));
                        }
                        if(partsSalesCategoryId.HasValue) {
                            sql.Append(@" and a.partsSalesCategoryId = {0}partsSalesCategoryId ");
                            dbParameters.Add(db.CreateDbParameter("partsSalesCategoryId", partsSalesCategoryId.Value));
                        }
                        if(serviceProductLineId.HasValue) {
                            sql.Append(@" and a.serviceProductLineId = {0}serviceProductLineId ");
                            dbParameters.Add(db.CreateDbParameter("serviceProductLineId", serviceProductLineId.Value));
                        }
                        if(!string.IsNullOrEmpty(extendedWarrantyOrderCode)) {
                            sql.Append(@" and a.extendedWarrantyOrderCode like {0}extendedWarrantyOrderCode ");
                            dbParameters.Add(db.CreateDbParameter("extendedWarrantyOrderCode", "%" + extendedWarrantyOrderCode + "%"));
                        }
                        if(!string.IsNullOrEmpty(VIN)) {
                            sql.Append(@" and a.VIN like {0}VIN ");
                            dbParameters.Add(db.CreateDbParameter("VIN", "%" + VIN + "%"));
                        }
                        if(status.HasValue) {
                            sql.Append(@" and a.status ={0}status");
                            dbParameters.Add(db.CreateDbParameter("status", status.Value));
                        }
                        if(!string.IsNullOrEmpty(vehicleLicensePlate)) {
                            sql.Append(@" and a.vehicleLicensePlate like {0}vehicleLicensePlate ");
                            dbParameters.Add(db.CreateDbParameter("vehicleLicensePlate", "%" + vehicleLicensePlate + "%"));
                        }
                        if(!string.IsNullOrEmpty(customerName)) {
                            sql.Append(@" and a.customerName like {0}customerName ");
                            dbParameters.Add(db.CreateDbParameter("customerName", "%" + customerName + "%"));
                        }
                        if(!string.IsNullOrEmpty(cellNumber)) {
                            sql.Append(@" and a.cellNumber like {0}cellNumber ");
                            dbParameters.Add(db.CreateDbParameter("cellNumber", "%" + cellNumber + "%"));
                        }
                        if(!string.IsNullOrEmpty(dealerCode)) {
                            sql.Append(@" and a.dealerCode like {0}dealerCode ");
                            dbParameters.Add(db.CreateDbParameter("dealerCode", "%" + dealerCode + "%"));
                        }
                        if(!string.IsNullOrEmpty(dealerName)) {
                            sql.Append(@" and a.dealerName like {0}dealerName ");
                            dbParameters.Add(db.CreateDbParameter("dealerName", "%" + dealerName + "%"));
                        }
                        if(createTimeBegin.HasValue) {
                            sql.Append(@" and a.CreatorTime >=to_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(@" and a.CreatorTime <=to_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                    }
                    var sqlStr = sql.ToString();
                    if(sqlStr.IndexOf(" where 1=1  and ") > 0)
                        sqlStr = sqlStr.Replace(" where 1=1  and ", " where ");
                    else
                        sqlStr = sqlStr.Replace(" where 1=1 ", "");
                    var command = db.CreateDbCommand(string.Format(sqlStr, db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    "延保单号", ErrorStrings.Export_Title_Partssalescategory_Name, "VIN", "服务产品线", ErrorStrings.Export_Title_Dealer_Name, ErrorStrings.Export_Title_Dealer_Code,"延保销售日期","车辆销售日期",ErrorStrings.Export_Title_DealerPartsRetailOrder_Customer,ErrorStrings.Export_Title_PersonSalesCenterLink_CellNumber,"邮箱地址","证件类型","证件号码","通讯地址","延保对象","出厂编号",
                                    "行驶里程","购车金额","发动机型号","车牌号","排量",ErrorStrings.Export_Title_PartsSalesOrder_TotalAmount,"服务商回款总金额","销售人员",ErrorStrings.Export_Title_AccountPeriod_Status,ErrorStrings.Export_Title_PartsBranch_Remark,ErrorStrings.Export_Title_AccountPeriod_CreatorName,ErrorStrings.Export_Title_AccountPeriod_CreateTime,ErrorStrings.Export_Title_AccountPeriod_ModifierName,ErrorStrings.Export_Title_AccountPeriod_ModifyTime,ErrorStrings.Export_Title_PackingPropertyApp_ApproverName,ErrorStrings.Export_Title_PackingPropertyApp_ApproveTime,ErrorStrings.Export_Title_PartsBranch_AbandonerName,ErrorStrings.Export_Title_PartsBranch_AbandonerTime,
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }

                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {

                return false;
            }
        }

        public bool ExportMergeEWOrder(int[] ids, int? branchId, int? dealerId, int? partsSalesCategoryId, int? serviceProductLineId, string extendedWarrantyOrderCode, string VIN, int? status,
            string vehicleLicensePlate, string customerName, string cellNumber, string dealerCode, string dealerName, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName) {
            fileName = GetExportFilePath("延保订单管理_" + ".xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated) {
                    throw new Exception(ErrorStrings.File_Validation_User);
                }
                var db = DbHelper.GetDbHelp(ConnectionString);
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.Append(@"   select    a.ExtendedWarrantyOrderCode,
                                       a.PartsSalesCategoryName,
                                       a.VIN,
                                       a.ServiceProductLine,
                                       a.DealerName,
                                       a.DealerCode,
                                       a.SalesDate,
                                       a.CarSalesDate,
                                       a.CustomerName,
                                       a.CellNumber,
                                       a.Email,
     (select value from keyvalueitem where NAME = 'ExtendedWarrantyIDType'and key=a.IDType) As IDType,
                                       a.IDNumer,
                                       a.province||a.city||a.region,
                                       (select RepairTarget from RepairObject where id=a.RepairObjectId) As RepairObjectId,
                                       a.SerialNumber,  
                                       a.Mileage,
                                       a.PurchaseAmount,
                                       a.EngineModel,
                                       a.VehicleLicensePlate,
                                       a.EngineCylinder,
                                       a.TotalAmount,
                                       a.DealerTotalAmount,
                                       a.SellPersonnel,
                                       (select value from keyvalueitem where NAME = 'ExtendedWarrantyStatus'and key=a.status) As status,
                                       a.Remark,
                                       a.CreatorName,
                                       a.CreatorTime,
                                       a.ModifierName,
                                       a.ModifierTime,
                                       a.CheckerName,
                                       a.CheckerTime,
                                       a.AbandonerName,
                                       a.AbandonerTime,
                                       c.extendedwarrantyproductname,
                                       b.CustomerRetailPrice,
                                       b.SalesPrice,
                                       d.ExtendedWarrantyAgioType As AgioType,
                                       b.DiscountRate,
                                       b.DiscountedPrice,
                                       b.TotalAmount,
                                       b.ValidFrom,
                                       b.EndDate,
                                       b.StartMileage ,
                                       b.EndMileage,
                                       b.ExtensionCoverage
                                  from ExtendedWarrantyOrder a 
                                    left join ExtendedWarrantyOrderList b on a.id =b.extendedwarrantyorderid
                                    left join ExtendedWarrantyProduct c on b.extendedwarrantyproductid=c.id
                                    left join DeferredDiscountType d on b.agiotype = d.Id and d.Status = 1 where 1=1
");
                    var dbParameters = new List<DbParameter>();
                    if(ids != null && ids.Length > 0) {
                        sql.Append(string.Format(" and a.id in ({0})", string.Join(",", ids)));
                    } else {
                        if(branchId.HasValue) {
                            sql.Append(@" and a.branchId = {0}branchId ");
                            dbParameters.Add(db.CreateDbParameter("branchId", branchId.Value));
                        }

                        if(dealerId.HasValue) {
                            sql.Append(@" and a.dealerId = {0}dealerId ");
                            dbParameters.Add(db.CreateDbParameter("dealerId", dealerId.Value));
                        }
                        if(partsSalesCategoryId.HasValue) {
                            sql.Append(@" and a.partsSalesCategoryId like {0}partsSalesCategoryId ");
                            dbParameters.Add(db.CreateDbParameter("partsSalesCategoryId", partsSalesCategoryId.Value));
                        }
                        if(serviceProductLineId.HasValue) {
                            sql.Append(@" and a.serviceProductLineId = {0}serviceProductLineId ");
                            dbParameters.Add(db.CreateDbParameter("serviceProductLineId", serviceProductLineId.Value));
                        }
                        if(!string.IsNullOrEmpty(extendedWarrantyOrderCode)) {
                            sql.Append(@" and a.extendedWarrantyOrderCode like {0}extendedWarrantyOrderCode ");
                            dbParameters.Add(db.CreateDbParameter("extendedWarrantyOrderCode", "%" + extendedWarrantyOrderCode + "%"));
                        }
                        if(!string.IsNullOrEmpty(VIN)) {
                            sql.Append(@" and a.VIN like {0}VIN ");
                            dbParameters.Add(db.CreateDbParameter("VIN", "%" + VIN + "%"));
                        }
                        if(status.HasValue) {
                            sql.Append(@" and a.status ={0}status");
                            dbParameters.Add(db.CreateDbParameter("status", status.Value));
                        }
                        if(!string.IsNullOrEmpty(vehicleLicensePlate)) {
                            sql.Append(@" and a.vehicleLicensePlate like {0}vehicleLicensePlate ");
                            dbParameters.Add(db.CreateDbParameter("vehicleLicensePlate", "%" + vehicleLicensePlate + "%"));
                        }
                        if(!string.IsNullOrEmpty(customerName)) {
                            sql.Append(@" and a.customerName like {0}customerName ");
                            dbParameters.Add(db.CreateDbParameter("customerName", "%" + customerName + "%"));
                        }
                        if(!string.IsNullOrEmpty(cellNumber)) {
                            sql.Append(@" and a.cellNumber like {0}cellNumber ");
                            dbParameters.Add(db.CreateDbParameter("cellNumber", "%" + cellNumber + "%"));
                        }
                        if(!string.IsNullOrEmpty(dealerCode)) {
                            sql.Append(@" and a.dealerCode like {0}dealerCode ");
                            dbParameters.Add(db.CreateDbParameter("dealerCode", "%" + dealerCode + "%"));
                        }
                        if(!string.IsNullOrEmpty(dealerName)) {
                            sql.Append(@" and a.dealerName like {0}dealerName ");
                            dbParameters.Add(db.CreateDbParameter("dealerName", "%" + dealerName + "%"));
                        }
                        if(createTimeBegin.HasValue) {
                            sql.Append(@" and a.CreatorTime >=to_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(@" and a.CreatorTime <=to_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                    }
                    var sqlStr = sql.ToString();
                    if(sqlStr.IndexOf(" where 1=1  and ") > 0)
                        sqlStr = sqlStr.Replace(" where 1=1  and ", " where ");
                    else
                        sqlStr = sqlStr.Replace(" where 1=1 ", "");
                    var command = db.CreateDbCommand(string.Format(sqlStr, db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    "延保单号",
                                    ErrorStrings.Export_Title_Partssalescategory_Name, 
                                    "VIN", 
                                    "服务产品线", 
                                    ErrorStrings.Export_Title_Dealer_Name, 
                                    ErrorStrings.Export_Title_Dealer_Code,
                                    "延保销售日期",
                                    "车辆销售日期",
                                    ErrorStrings.Export_Title_DealerPartsRetailOrder_Customer,
                                    ErrorStrings.Export_Title_PersonSalesCenterLink_CellNumber,
                                    "邮箱地址",
                                    "证件类型",
                                    "证件号码",
                                    "通讯地址",
                                    "延保对象",
                                    "出厂编号",
                                    "行驶里程",
                                    "购车金额",
                                    "发动机型号",
                                    "车牌号",
                                    "排量",
                                    ErrorStrings.Export_Title_PartsSalesOrder_TotalAmount,
                                    "服务商回款总金额",
                                    "销售人员",
                                    ErrorStrings.Export_Title_AccountPeriod_Status,
                                    ErrorStrings.Export_Title_PartsBranch_Remark,
                                    ErrorStrings.Export_Title_AccountPeriod_CreatorName,
                                    ErrorStrings.Export_Title_AccountPeriod_CreateTime,
                                    ErrorStrings.Export_Title_AccountPeriod_ModifierName,
                                    ErrorStrings.Export_Title_AccountPeriod_ModifyTime,
                                    ErrorStrings.Export_Title_PackingPropertyApp_ApproverName,
                                    ErrorStrings.Export_Title_PackingPropertyApp_ApproveTime,
                                    ErrorStrings.Export_Title_PartsBranch_AbandonerName,
                                    ErrorStrings.Export_Title_PartsBranch_AbandonerTime,
                                    "延保产品",
                                    "客户建议售价",
                                    ErrorStrings.Export_Title_DealerPartsStockQueryView_SalesPrice,
                                    "折扣类型",
                                    "折扣",
                                    "折扣价",
                                    "延保总价",
                                    "延保起始日期",
                                    "延保终止日期",
                                    "延保起始里程",
                                    "延保终止里程",
                                    "延保范围"
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }

                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {

                return false;
            }
        }
    }
}
