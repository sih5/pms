﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        /// <summary>
        /// 导入旧件处理单清单
        /// </summary>
        public bool ImportUsedPartsDisposalDetail(int usedPartsWarehouseId, string fileName, out int excelImportNum, out List<UsedPartsDisposalDetailExtend> rightData, out List<UsedPartsDisposalDetailExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<UsedPartsDisposalDetailExtend>();
            var rightList = new List<UsedPartsDisposalDetailExtend>();
            var allList = new List<UsedPartsDisposalDetailExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);
                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("UsedPartsDisposalDetail", out notNullableFields, out fieldLenght);

                List<object> excelColumns;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    //指定文件中的列对应的名称
                    //excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_InternalAllocationBill_SerialNumber, "SerialNumber");
                    excelOperator.AddColumnDataSource("旧件条码", "UsedPartsBarCode");
                    excelOperator.AddColumnDataSource("旧件配件编号", "UsedPartsCode");
                    excelOperator.AddColumnDataSource("旧件配件名称", "UsedPartsName");
                    //excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsPurchasePlanDetail_PlanAmount, "PlannedAmount");
                    //excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsPurReturnOrder_Price, "Price");
                    excelOperator.AddColumnDataSource("索赔单编号", "ClaimBillCode");
                    excelOperator.AddColumnDataSource("索赔单类型", "ClaimBillType");
                    excelOperator.AddColumnDataSource("是否祸首件", "IfFaultyParts");
                    excelOperator.AddColumnDataSource("祸首件编号", "FaultyPartsCode");
                    excelOperator.AddColumnDataSource("祸首件供应商编号", "FaultyPartsSupplierCode");
                    excelOperator.AddColumnDataSource("祸首件供应商名称", "FaultyPartsSupplierName");
                    excelOperator.AddColumnDataSource("责任单位编号", "ResponsibleUnitCode");
                    excelOperator.AddColumnDataSource("旧件供应商编号", "UsedPartsSupplierCode");
                    excelOperator.AddColumnDataSource("旧件供应商名称", "UsedPartsSupplierName");

                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;
                    //加载字典项
                    var keyValuePairs = new[] {
                                new KeyValuePair<string, string>("ClaimBillType", "ClaimBill_Type"),               
                                };
                    tempExcelOperator.ImportHelper.LoadEnumFromDb(null, null, keyValuePairs);

                    //Excel单行导入
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        //接收Excel导入数据
                        var tempImportObj = new UsedPartsDisposalDetailExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        //tempImportObj.SerialNumberStr = newRow["SerialNumber"];
                        tempImportObj.UsedPartsBarCodeStr = newRow["UsedPartsBarCode"];
                        tempImportObj.UsedPartsCodeStr = newRow["UsedPartsCode"];
                        tempImportObj.UsedPartsNameStr = newRow["UsedPartsName"];
                        //tempImportObj.PlannedAmountStr = newRow["PlannedAmount"];
                        //tempImportObj.PriceStr = newRow["Price"];
                        tempImportObj.ClaimBillCodeStr = newRow["ClaimBillCode"];
                        tempImportObj.ClaimBillTypeStr = newRow["ClaimBillType"];
                        tempImportObj.IfFaultyPartsStr = newRow["IfFaultyParts"];
                        tempImportObj.FaultyPartsCodeStr = newRow["FaultyPartsCode"];
                        tempImportObj.FaultyPartsSupplierCodeStr = newRow["FaultyPartsSupplierCode"];
                        tempImportObj.FaultyPartsSupplierNameStr = newRow["FaultyPartsSupplierName"];
                        tempImportObj.ResponsibleUnitCodeStr = newRow["ResponsibleUnitCode"];
                        tempImportObj.UsedPartsSupplierCodeStr = newRow["UsedPartsSupplierCode"];
                        tempImportObj.UsedPartsSupplierNameStr = newRow["UsedPartsSupplierName"];

                        var errorMsgs = new List<string>();
                        //旧件条码检查
                        var fieldIndex = notNullableFields.IndexOf("UsedPartsBarCode".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.UsedPartsBarCodeStr)) {
                            if(fieldIndex > -1)
                                errorMsgs.Add(ErrorStrings.ImpUsedPartsReturnDetail_Validation1);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.UsedPartsBarCodeStr) > fieldLenght["UsedPartsBarCode".ToUpper()])
                                errorMsgs.Add(ErrorStrings.ImpUsedPartsReturnDetail_Validation2);
                        }
                        //旧件配件编号
                        fieldIndex = notNullableFields.IndexOf("UsedPartsCode".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.UsedPartsCodeStr)) {
                            if(fieldIndex > -1)
                                errorMsgs.Add("旧件配件编号不能为空");
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.UsedPartsCodeStr) > fieldLenght["UsedPartsCode".ToUpper()])
                                errorMsgs.Add("旧件配件编号过长");
                        }
                        //旧件配件名称
                        fieldIndex = notNullableFields.IndexOf("UsedPartsName".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.UsedPartsNameStr)) {
                            if(fieldIndex > -1)
                                errorMsgs.Add("旧件配件名称不能为空");
                        } else
                            if(Encoding.Default.GetByteCount(tempImportObj.UsedPartsNameStr) > fieldLenght["UsedPartsName".ToUpper()])
                                errorMsgs.Add("旧件配件名称过长");
                        //索赔单编号
                        fieldIndex = notNullableFields.IndexOf("ClaimBillCode".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.ClaimBillCodeStr)) {
                            if(fieldIndex > -1)
                                errorMsgs.Add("索赔单编号不能为空");
                        } else
                            if(Encoding.Default.GetByteCount(tempImportObj.ClaimBillCodeStr) > fieldLenght["ClaimBillCode".ToUpper()])
                                errorMsgs.Add("索赔单编号过长");
                        ////索赔单类型
                        fieldIndex = notNullableFields.IndexOf("ClaimBillType".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.ClaimBillTypeStr)) {
                            if(fieldIndex > -1)
                                errorMsgs.Add("索赔单类型不能为空");
                        } else {
                            var tempEnumValue = tempExcelOperator.ImportHelper.GetEnumValue("ClaimBillType", tempImportObj.ClaimBillTypeStr);
                            if(!tempEnumValue.HasValue) {
                                errorMsgs.Add("索赔单类型不正确");
                            } else {
                                tempImportObj.ClaimBillType = tempEnumValue.Value;
                            }
                        }
                        //是否祸首件IfFaultyParts

                        //祸首件编号
                        fieldIndex = notNullableFields.IndexOf("FaultyPartsCode".ToUpper());
                        if (string.IsNullOrEmpty(tempImportObj.FaultyPartsCodeStr)) {
                            if (fieldIndex > -1)
                                errorMsgs.Add("祸首件编号不能为空");
                        }
                        else
                            if (Encoding.Default.GetByteCount(tempImportObj.FaultyPartsCodeStr) > fieldLenght["FaultyPartsCode".ToUpper()])
                                errorMsgs.Add("祸首件编号过长");


                        //祸首件供应商编号
                        fieldIndex = notNullableFields.IndexOf("FaultyPartsSupplierCode".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.FaultyPartsSupplierCodeStr)) {
                            if(fieldIndex > -1)
                                errorMsgs.Add("祸首件供应商编号不能为空");
                        } else
                            if(Encoding.Default.GetByteCount(tempImportObj.FaultyPartsSupplierCodeStr) > fieldLenght["FaultyPartsSupplierCode".ToUpper()])
                                errorMsgs.Add("祸首件供应商编号过长");
                        //祸首件供应商名称
                        fieldIndex = notNullableFields.IndexOf("FaultyPartsSupplierName".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.FaultyPartsSupplierNameStr)) {
                            if(fieldIndex > -1)
                                errorMsgs.Add("祸首件供应商名称不能为空");
                        } else
                            if(Encoding.Default.GetByteCount(tempImportObj.FaultyPartsSupplierNameStr) > fieldLenght["FaultyPartsSupplierName".ToUpper()])
                                errorMsgs.Add("祸首件供应商名称过长");
                        //责任单位编号
                        fieldIndex = notNullableFields.IndexOf("ResponsibleUnitCode".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.ResponsibleUnitCodeStr)) {
                            if(fieldIndex > -1)
                                errorMsgs.Add("责任单位编号不能为空");
                        } else
                            if(Encoding.Default.GetByteCount(tempImportObj.ResponsibleUnitCodeStr) > fieldLenght["ResponsibleUnitCode".ToUpper()])
                                errorMsgs.Add("责任单位编号过长");
                        //旧件供应商编号
                        fieldIndex = notNullableFields.IndexOf("UsedPartsSupplierCode".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.UsedPartsSupplierCodeStr)) {
                            if(fieldIndex > -1)
                                errorMsgs.Add("旧件供应商编号不能为空");
                        } else
                            if(Encoding.Default.GetByteCount(tempImportObj.UsedPartsSupplierCodeStr) > fieldLenght["UsedPartsSupplierCode".ToUpper()])
                                errorMsgs.Add("旧件供应商编号过长");
                        //旧件供应商名称
                        fieldIndex = notNullableFields.IndexOf("UsedPartsSupplierName".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.UsedPartsSupplierNameStr)) {
                            if(fieldIndex > -1)
                                errorMsgs.Add("旧件供应商名称不能为空");
                        } else
                            if(Encoding.Default.GetByteCount(tempImportObj.UsedPartsSupplierNameStr) > fieldLenght["UsedPartsSupplierName".ToUpper()])
                                errorMsgs.Add("旧件供应商名称过长");

                        if(errorMsgs.Count > 0)
                            tempImportObj.ErrorMsg = string.Join("; ", errorMsgs);
                        allList.Add(tempImportObj);
                        return false;
                    });
                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    //剩下的数据进行业务检查
                    var groups = tempRightList.GroupBy(r => new {
                        r.UsedPartsBarCodeStr
                    }).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        groupItem.ErrorMsg = ErrorStrings.ImpPartsSupplierRelation_Validation31;
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    //查询旧件库存，旧件条码 in（excel.旧件条码）
                    var usedPartsBarCodeNeedCheck = tempRightList.Select(r => r.UsedPartsBarCodeStr).Distinct().ToArray();
                    var dbUsedPartsStocks = new List<UsedPartsStockExtend>();
                    Func<string[], bool> getDbUsedPartsStocks = values => {
                        dbUsedPartsStocks.Add(new UsedPartsStockExtend {
                            UsedPartsBarCode = values[0],
                            LockedQuantity = Convert.ToInt32(values[1]),
                            SettlementPrice = Convert.ToDecimal(values[2]),
                            BranchId = Convert.ToInt32(values[3]),
                            FaultyPartsId = Convert.ToInt32(values[4]),
                            FaultyPartsName= values[5],
                            FaultyPartsSupplierId = Convert.ToInt32(values[6]),
                            ResponsibleUnitId = Convert.ToInt32(values[7]),
                            ResponsibleUnitName = values[8],
                            UsedPartsSerialNumber = values[9],
                            UsedPartsId = Convert.ToInt32(values[10]),
                            UsedPartsSupplierId=Convert.ToInt32(values[11]),
                            ClaimBillId = Convert.ToInt32(values[12])
                        });
                        return false;
                    };


                    var sql = string.Format(" select UsedPartsBarCode,LockedQuantity,SettlementPrice,BranchId,FaultyPartsId,FaultyPartsName,FaultyPartsSupplierId,ResponsibleUnitId,        ResponsibleUnitName,UsedPartsSerialNumber,UsedPartsId,UsedPartsSupplierId,ClaimBillId from UsedPartsStock where UsedPartsWarehouseId = {0} ", usedPartsWarehouseId);
                    db.QueryDataWithInOperator(sql, "UsedPartsBarCode", false, usedPartsBarCodeNeedCheck, getDbUsedPartsStocks);
                    foreach(var tempRight in tempRightList) {
                        var usedPartsStock = dbUsedPartsStocks.FirstOrDefault(v => v.UsedPartsBarCode == tempRight.UsedPartsBarCodeStr);
                        if (usedPartsStock != null) {
                            if (usedPartsStock.LockedQuantity == 1) {
                                tempRight.ErrorMsg = "该旧件已经被锁定";
                                continue;
                            }
                            tempRight.PriceStr = usedPartsStock.SettlementPrice.ToString();
                            tempRight.BranchId = usedPartsStock.BranchId;
                            tempRight.FaultyPartsId = usedPartsStock.FaultyPartsId;
                            tempRight.FaultyPartsName = usedPartsStock.FaultyPartsName;
                            tempRight.FaultyPartsSupplierId = usedPartsStock.FaultyPartsSupplierId;
                            tempRight.ResponsibleUnitId = usedPartsStock.ResponsibleUnitId;
                            tempRight.ResponsibleUnitName = usedPartsStock.ResponsibleUnitName;
                            tempRight.UsedPartsSerialNumber = usedPartsStock.UsedPartsSerialNumber;
                            tempRight.UsedPartsId = usedPartsStock.UsedPartsId;
                            tempRight.UsedPartsSupplierId = usedPartsStock.UsedPartsSupplierId??0;
                            tempRight.ClaimBillId = usedPartsStock.ClaimBillId??0;
                        }
                        else {
                            tempRight.ErrorMsg = "没有查到该旧件条码";
                        }
                    }

                    //配件编号存在配件信息中
                    //var partCodesNeedCheck = tempRightList.Select(r => r.UsedPartsCodeStr.ToUpper()).Distinct().ToArray();
                    //var dbSpareParts = new List<SparePartExtend>();
                    //Func<string[], bool> getDbSparePartCodes = value =>
                    //{
                    //    var dbObj = new SparePartExtend
                    //    {
                    //        Id = Convert.ToInt32(value[0]),
                    //        Code = value[1],
                    //        Name = value[2]
                    //    };
                    //    dbSpareParts.Add(dbObj);
                    //    return false;
                    //};
                    //db.QueryDataWithInOperator("select Id,Trim(Code) as Code,Trim(Name) as Name from sparePart where status=1 ", "Code", true, partCodesNeedCheck, getDbSparePartCodes);
                    //foreach (var tempRight in tempRightList) {
                    //    var sparePart = dbSpareParts.FirstOrDefault(v => v.Code == tempRight.UsedPartsCodeStr);
                    //    if (sparePart == null) {
                    //        tempRight.ErrorMsg = String.Format("配件图号“{0}”", tempRight.UsedPartsCodeStr);
                    //        continue;
                    //    }
                    //    tempRight.UsedPartsId = sparePart.Id;
                    //}
                    //tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    ////供应商验证
                    //var partsSupplierCodes = tempRightList.Select(v => v.UsedPartsSupplierCodeStr).Distinct().ToArray();
                    //var partsSuppliers = new List<PartsSupplierExtend>();
                    //Func<string[], bool> dealPartsSupplier = values =>
                    //{
                    //    partsSuppliers.Add(new PartsSupplierExtend
                    //    {
                    //        Id = Convert.ToInt32(values[0]),
                    //        Code = values[1],
                    //        Name = values[2]
                    //    });
                    //    return false;
                    //};
                    //db.QueryDataWithInOperator(string.Format("select Id,SupplierCode as Code, Name from Company where status={0}", (int)DcsMasterDataStatus.有效), "SupplierCode", false, partsSupplierCodes, dealPartsSupplier);

                    //foreach (var tempRight in tempRightList) {
                    //    var partsSupplier = partsSuppliers.FirstOrDefault(v => v.Code == tempRight.UsedPartsSupplierCodeStr);
                    //    if (partsSupplier == null) {
                    //        tempRight.ErrorMsg = ErrorStrings.Export_Validation_PartsPurchaseOrder_Validation8;
                    //        continue;
                    //    }
                    //    tempRight.UsedPartsSupplierId = partsSupplier.Id;
                    //}

                    //tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    ////校验索赔单
                    //var ClaimBillCodesNeedCheck = tempRightList.Select(r => r.ClaimBillCodeStr.ToUpper()).Distinct().ToArray();
                    //var dbClaimBillCode = new List<SparePartExtend>();
                    //Func<string[], bool> getDbClaimBillCodes = value =>
                    //{
                    //    var dbObj = new SparePartExtend
                    //    {
                    //        Id = Convert.ToInt32(value[0]),
                    //        Code = value[1]
                    //    };
                    //    dbClaimBillCode.Add(dbObj);
                    //    return false;
                    //};
                    //db.QueryDataWithInOperator("select Id,Trim(ClaimBillCode) as Code from RepairClaimBill where 1=1", "ClaimBillCode", true, ClaimBillCodesNeedCheck, getDbClaimBillCodes);
                    //foreach (var tempRight in tempRightList) {
                    //    var claimBillCode = dbClaimBillCode.FirstOrDefault(v => v.Code == tempRight.ClaimBillCodeStr);
                    //    if (claimBillCode == null) {
                    //        tempRight.ErrorMsg = String.Format("索赔单“{0}”不存在", tempRight.ClaimBillCodeStr);
                    //        continue;
                    //    }
                    //    tempRight.ClaimBillId = claimBillCode.Id;
                    //}
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();


                    //获取所有不合格数据
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    //获取所有合格数据
                    rightList = allList.Except(errorList).ToList();
                    //将合格数据的值填上
                    foreach(var rightItem in rightList) {
                        rightItem.Id = default(int);
                        rightItem.UsedPartsDisposalBillId = default(int);
                        rightItem.UsedPartsCode = rightItem.UsedPartsCodeStr;
                        rightItem.UsedPartsName = rightItem.UsedPartsNameStr;
                        rightItem.UsedPartsBarCode = rightItem.UsedPartsBarCodeStr;
                        rightItem.Price = Convert.ToDecimal(rightItem.PriceStr);
                        rightItem.PlannedAmount = 1;
                        rightItem.ConfirmedAmount = 0;
                        rightItem.OutboundAmount = 0;
                        rightItem.UsedPartsSupplierCode = rightItem.UsedPartsSupplierCodeStr;
                        rightItem.UsedPartsSupplierName = rightItem.UsedPartsSupplierNameStr;
                        rightItem.ClaimBillCode = rightItem.ClaimBillCodeStr;
                        rightItem.IfFaultyParts = rightItem.IfFaultyPartsStr == ErrorStrings.Export_Title_PartsBranch_Yes ? true : false;   // Convert.ToBoolean(rightItem.IfFaultyPartsStr);
                        rightItem.FaultyPartsCode = rightItem.FaultyPartsCodeStr;
                        rightItem.FaultyPartsSupplierCode = rightItem.FaultyPartsSupplierCodeStr;
                        rightItem.FaultyPartsSupplierName = rightItem.FaultyPartsSupplierNameStr;
                        rightItem.ResponsibleUnitCode = rightItem.ResponsibleUnitCodeStr;
                    }
                }
                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempObj = list[index - 1];
                            var values = new object[] {
                                //设置错误信息导出的列的值
                                tempObj.UsedPartsBarCodeStr,tempObj.UsedPartsCodeStr,tempObj.UsedPartsNameStr,
                                //tempObj.PlannedAmountStr,tempObj.PriceStr,
                                tempObj.ClaimBillCodeStr,tempObj.ClaimBillTypeStr,tempObj.IfFaultyPartsStr,tempObj.FaultyPartsCodeStr,tempObj.FaultyPartsSupplierCodeStr,tempObj.FaultyPartsSupplierNameStr,tempObj.ResponsibleUnitCodeStr,tempObj.UsedPartsSupplierCodeStr,tempObj.UsedPartsSupplierNameStr, tempObj.ErrorMsg
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }

        /// <summary>
        /// 导出旧件处理清单
        /// </summary>
        public bool ExportUsedPartsDisposalDetail(int? billId, int personnelId, string usedPartsWarehouseCode, string usedPartsWarehouseName, int? status, int? outboundStatus, string relatedCompanyName, int? usedPartsDisposalMethod, string code, DateTime? createTimeBegin, DateTime? createTimeEnd, DateTime? approveTimeBegin, DateTime? approveTimeEnd, out string fileName) {
            fileName = GetExportFilePath("旧件处理清单.xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {

                    conn.Open();
                    var sql = new StringBuilder();
                    #region SQL
                    sql.AppendFormat(@"Select a.Code, /*旧件处理单编号*/
                                   a.Usedpartswarehousename, /*旧件仓库名称*/
                                   cast(Decode(a.Usedpartsdisposalmethod, 1, '拍卖', 2, '销毁') as varchar2(20)), /*旧件处理类型*/
                                   b.Usedpartsbarcode, /*旧件配件编号*/
                                   b.Usedpartsname, /*旧件配件名称*/
                                   b.Price, /*价格*/
                                   b.Plannedamount, /*计划量*/
                                   b.Confirmedamount, /*确认量*/
                                   b.Outboundamount, /*出库量*/
                                   b.Usedpartssuppliercode, /*旧件供应商编号*/
                                   b.Usedpartssuppliername, /*旧件供应商名称*/
                                   cast(Case b.Claimbilltype /*索赔单类型*/
                                     When 1 Then
                                      '维修索赔'
                                     When 2 Then
                                      '保养索赔'
                                     When 3 Then
                                      '服务活动索赔'
                                     When 4 Then
                                      '配件索赔'
                                   End as varchar2(20)),
                                   b.Claimbillcode, /*索赔单编号*/
                                   cast (Decode(b.Iffaultyparts, 1, '是', 0, '否') as varchar2(20)), /*是否祸首件*/
                                   b.Faultypartssuppliercode, /*祸首件供应商编号*/
                                   b.Faultypartssuppliername, /*祸首件供应商名称*/
                                   b.Responsibleunitcode, /*责任单位编号*/
                                   b.Faultypartscode, /*祸首件编号*/
                                   b.Faultypartsname /*祸首件名称*/
                              From Usedpartsdisposalbill a
                              Left Join Usedpartsdisposaldetail b
                                On a.Id = b.Usedpartsdisposalbillid
                             Where (Exists
                                    (Select 1
                                       From Usedpartswarehousestaff t
                                      Where (t.Personnelid = {0})
                                        And (t.Usedpartswarehouseid = a.Usedpartswarehouseid))) ", personnelId);
                    #endregion
                    var tableName = sql.ToString().GetRegexNames();
                    var dbParameters = new List<DbParameter>();
                    if(billId.HasValue) {
                        sql.Append(" and a.id = {0}billId");
                        dbParameters.Add(db.CreateDbParameter("billId", billId.Value));
                    } else {
                        if(!string.IsNullOrEmpty(usedPartsWarehouseCode)) {
                            sql.Append(@" and Upper(a.usedPartsWarehouseCode) =Upper({0}usedPartsWarehouseCode) ");
                            dbParameters.Add(db.CreateDbParameter("usedPartsWarehouseCode", "%" + usedPartsWarehouseCode + "%"));
                        }
                        if(!string.IsNullOrEmpty(usedPartsWarehouseName)) {
                            sql.Append(@" and a.usedPartsWarehouseName ={0}usedPartsWarehouseName ");
                            dbParameters.Add(db.CreateDbParameter("usedPartsWarehouseName", "%" + usedPartsWarehouseName + "%"));
                        }
                        if(status.HasValue) {
                            sql.Append(@" and a.status ={0}Status");
                            dbParameters.Add(db.CreateDbParameter("status", status.Value));
                        }
                        if(outboundStatus.HasValue) {
                            sql.Append(@" and a.outboundStatus ={0}outboundStatus");
                            dbParameters.Add(db.CreateDbParameter("outboundStatus", outboundStatus.Value));
                        }
                        if(!string.IsNullOrEmpty(relatedCompanyName)) {
                            sql.Append(@" and a.relatedCompanyName ={0}relatedCompanyName ");
                            dbParameters.Add(db.CreateDbParameter("relatedCompanyName", "%" + relatedCompanyName + "%"));
                        }
                        if(usedPartsDisposalMethod.HasValue) {
                            sql.Append(@" and a.usedPartsDisposalMethod ={0}usedPartsDisposalMethod");
                            dbParameters.Add(db.CreateDbParameter("usedPartsDisposalMethod", usedPartsDisposalMethod.Value));
                        }
                        if(!string.IsNullOrEmpty(code)) {
                            sql.Append(@" and Upper(a.code) =Upper({0}code) ");
                            dbParameters.Add(db.CreateDbParameter("code", "%" + code + "%"));
                        }
                        if(createTimeBegin.HasValue) {
                            sql.Append(@" and a.createTime >=To_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(@" and a.createTime <=To_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                        if(approveTimeBegin.HasValue) {
                            sql.Append(@" and a.approveTime >=To_date({0}approveTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = approveTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("approveTimeBegin", tempTime.ToString("G")));
                        }
                        if(approveTimeEnd.HasValue) {
                            sql.Append(@" and a.approveTime <=To_date({0}approveTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = approveTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("approveTimeEnd", tempTime.ToString("G")));
                        }
                    }
                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return tableName;
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }

                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }

        }
    }
}
