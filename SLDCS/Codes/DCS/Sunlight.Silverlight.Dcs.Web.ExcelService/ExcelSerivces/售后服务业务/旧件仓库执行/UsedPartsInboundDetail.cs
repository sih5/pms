﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Globalization;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        public bool ExportUsedPartsInboundDetail(int[] usedPartsInboundOrderIds, string code, int? inboundType, string usedPartsWarehouseCode, string usedPartsWarehouseName, DateTime? inboundTimeStart, DateTime? inboundTimeEnd, string sourceCode, string relatedCompanyCode, string relatedCompanyName, out string fileName) {
            fileName = GetExportFilePath("旧件入库单清单_" + ".xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated) {
                    throw new Exception(ErrorStrings.File_Validation_User);
                }
                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.Append(@"select upi.Code,
                                        upi.UsedPartsWarehouseName,
                                       cast( decode(upi.InboundType,
                                                1,
                                              '服务站返件入库',
                                              2,
                                              '调拨入库',
                                              3,
                                              '借用归还',
                                              4,
                                              '加工入库'
                                                ) as varchar2(20)) as InboundType,
                                        upi.RelatedCompanyCode,
                                        upi.RelatedCompanyName,
                                        upid.UsedPartsCode,
                                        upid.UsedPartsName,
                                        upid.UsedPartsBarCode,
                                        upid.UsedPartsWarehouseAreaCode,
                                        upid.Quantity,
                                        upid.SettlementPrice,
                                        upid.CostPrice,
                                        upid.UsedPartsEncourageAmount,
                                        cast(decode(upid.ReceptionStatus,1,'在途',2,'已接收',3,'未接收',4,'罚没',5,'不合格') as varchar2(20)),
                                        upid.ReceptionRemark,
                                        upid.Remark,
                                        upid.UsedPartsSupplierCode,
                                        upid.UsedPartsSupplierName,
                                        cast(decode(upid.ClaimBillType,
                                                1,'维修索赔',
                                                2,'保养索赔',
                                                3,'服务活动索赔',
                                                4,'配件索赔'
                                                ) as varchar2(20)) as ClaimBillType,
                                        upid.ClaimBillCode,
                                        cast(decode(upid.IfFaultyParts,
                                                0,'否',
                                                1,'是'
                                                ) as varchar2(20)) as IfFaultyParts,
                                        upid.FaultyPartsSupplierCode,
                                        upid.FaultyPartsSupplierName,
                                        upid.ResponsibleUnitCode,
                                        upid.ResponsibleUnitName,
                                        upid.FaultyPartsCode,
                                        upid.FaultyPartsName                                        
                                    from UsedPartsInboundDetail upid
                                    inner join UsedPartsInboundOrder upi
                                    on  upid.UsedPartsInboundOrderId=upi.Id where 1=1 ");
                    var dbParameters = new List<DbParameter>();

                    sql.Append(@" and (( EXISTS (SELECT 
                                                1 AS C1
                                                FROM UsedPartsWarehouseStaff upws
                                                WHERE (upws.PersonnelId = {0}personnelId) AND (upws.UsedPartsWarehouseId = upi.UsedPartsWarehouseId)
                                            )))");
                    dbParameters.Add(db.CreateDbParameter("personnelId", Utils.GetCurrentUserInfo().Id));

                    if(usedPartsInboundOrderIds != null && usedPartsInboundOrderIds.Length > 0) {
                        sql.Append(" and upi.id in (");
                        for(var i = 0; i < usedPartsInboundOrderIds.Length; i++) {
                            if(usedPartsInboundOrderIds.Length == i + 1) {
                                sql.Append("{0}" + usedPartsInboundOrderIds[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter(usedPartsInboundOrderIds[i].ToString(CultureInfo.InvariantCulture), usedPartsInboundOrderIds[i]));
                            } else {
                                sql.Append("{0}" + usedPartsInboundOrderIds[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter(usedPartsInboundOrderIds[i].ToString(CultureInfo.InvariantCulture), usedPartsInboundOrderIds[i]));
                            }
                        }
                        sql.Append(")");
                    } else {
                        if(!string.IsNullOrEmpty(code)) {
                            sql.Append(@" and upi.code like {0}code ");
                            dbParameters.Add(db.CreateDbParameter("code", "%" + code + "%"));
                        }
                        if(inboundType.HasValue) {
                            sql.Append(@" and upi.inboundType={0}inboundType");
                            dbParameters.Add(db.CreateDbParameter("inboundType", inboundType.Value));
                        }
                        if(!string.IsNullOrEmpty(usedPartsWarehouseCode)) {
                            sql.Append(@" and upi.usedPartsWarehouseCode like {0}usedPartsWarehouseCode ");
                            dbParameters.Add(db.CreateDbParameter("usedPartsWarehouseCode", "%" + usedPartsWarehouseCode + "%"));
                        }
                        if(!string.IsNullOrEmpty(usedPartsWarehouseName)) {
                            sql.Append(@" and upi.usedPartsWarehouseName like {0}usedPartsWarehouseName ");
                            dbParameters.Add(db.CreateDbParameter("usedPartsWarehouseName", "%" + usedPartsWarehouseName + "%"));
                        }
                        if(inboundTimeStart.HasValue) {
                            sql.Append(" and upi.createTime>=to_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = inboundTimeStart.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(inboundTimeEnd.HasValue) {
                            sql.Append(" and upi.createTime<=to_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = inboundTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                        if(!string.IsNullOrEmpty(sourceCode)) {
                            sql.Append(@" and upi.sourceCode like {0}sourceCode ");
                            dbParameters.Add(db.CreateDbParameter("sourceCode", "%" + sourceCode + "%"));
                        }
                        if(!string.IsNullOrEmpty(relatedCompanyCode)) {
                            sql.Append(@" and upi.relatedCompanyCode like {0}relatedCompanyCode ");
                            dbParameters.Add(db.CreateDbParameter("relatedCompanyCode", "%" + relatedCompanyCode + "%"));
                        }
                        if(!string.IsNullOrEmpty(relatedCompanyName)) {
                            sql.Append(@" and upi.relatedCompanyName like {0}relatedCompanyName ");
                            dbParameters.Add(db.CreateDbParameter("relatedCompanyName", "%" + relatedCompanyName + "%"));
                        }
                    }
                    var sqlStr = sql.ToString();
                    if(sqlStr.IndexOf(" where 1=1  and ") > 0)
                        sqlStr = sqlStr.Replace(" where 1=1  and ", " where ");
                    else
                        sqlStr = sqlStr.Replace(" where 1=1 ", "");
                    var command = db.CreateDbCommand(string.Format(sqlStr, db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0)
                                return new object[] {
                                    "旧件入库单编号",ErrorStrings.Export_Title_PartsSalesReturnBill_WarehouseName,ErrorStrings.Export_Title_PackingTask_InboundType,ErrorStrings.Export_Title_BoxUpTask_CounterpartCompanyCode,
                                    ErrorStrings.Export_Title_BoxUpTask_CounterpartCompanyName,"旧件配件编号","旧件配件名称","旧件条码","旧件库位编号",
                                    ErrorStrings.Export_Title_WarehouseArea_Quantity,"结算价格","成本价格","旧件激励金额","验收状态","验收备注",ErrorStrings.Export_Title_PartsBranch_Remark,"旧件供应商编号","旧件供应商名称","索赔单类型",
                                    "索赔单编号","是否祸首件","祸首件供应商编号","祸首件供应商名称","责任单位编号","责任单位名称",
                                    "祸首件编号","祸首件名称"
                                };
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch
                (Exception) {
                return false;
            }
        }
    }
}