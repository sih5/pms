﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Globalization;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        public bool ExportUsedPartsOutboundDetail(int[] usedPartsOutboundOrderIds, string code, int? outboundType, string usedPartsWarehouseCode, string usedPartsWarehouseName, DateTime? outboundTimeStart, DateTime? outboundTimeEnd, string sourceCode, string relatedCompanyCode, string relatedCompanyName, string usedPartsBarCode, string claimBillCode, bool? ifBillable, bool? ifAlreadySettled, out string fileName) {
            fileName = GetExportFilePath("旧件出库单清单_" + ".xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated) {
                    throw new Exception(ErrorStrings.File_Validation_User);
                }
                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.Append(@"select upo.Code,
                                        upo.UsedPartsWarehouseName,
                                        cast(decode(upo.OutboundType,
                                                1,'调拨出库',
                                                2,'旧件处理出库',
                                                3,'清退供应商出库',
                                                4,'借用出库',
                                                5,'加工领料出库'
                                                ) as varchar2(20)) as OutboundType,
                                        upo.SourceCode,
                                        upo.RelatedCompanyCode,
                                        upo.RelatedCompanyName,
                                        upod.UsedPartsCode,
                                        upod.UsedPartsName,
                                        upod.UsedPartsBarCode,
                                        upod.UsedPartsWarehouseAreaCode,
                                        upod.Quantity,
                                        upod.SettlementPrice,
                                        upod.CostPrice,
                                        upod.UsedPartsSupplierCode,
                                        upod.UsedPartsSupplierName,
                                        cast(decode(upod.ClaimBillType,
                                                1,'维修索赔',
                                                2,'保养索赔',
                                                3,'服务活动索赔',
                                                4,'配件索赔'
                                                ) as varchar2(20)) as ClaimBillType,
                                        upod.ClaimBillCode,
                                        cast(decode(upod.IfFaultyParts,
                                                0,'否',
                                                1,'是'
                                                ) as varchar2(20)) as IfFaultyParts,
                                        upod.FaultyPartsSupplierCode,
                                        upod.FaultyPartsSupplierName,
                                        upod.ResponsibleUnitCode,
                                        upod.FaultyPartsCode,
                                        upod.FaultyPartsName,
                                        upod.Remark
                                    from UsedPartsOutboundDetail upod
                                    inner join UsedPartsOutboundOrder upo
                                    on  upod.UsedPartsOutboundOrderId=upo.Id where 1=1 ");
                    var dbParameters = new List<DbParameter>();

                    sql.Append(@" and (( EXISTS (SELECT 
                                                1 AS C1
                                                FROM UsedPartsWarehouseStaff upws
                                                WHERE (upws.PersonnelId = {0}personnelId) AND (upws.UsedPartsWarehouseId = upo.UsedPartsWarehouseId)
                                            )))");
                    dbParameters.Add(db.CreateDbParameter("personnelId", Utils.GetCurrentUserInfo().Id));
                    if(usedPartsOutboundOrderIds != null && usedPartsOutboundOrderIds.Length > 0) {
                        sql.Append(" and upo.id in (");
                        for(var i = 0; i < usedPartsOutboundOrderIds.Length; i++) {
                            if(usedPartsOutboundOrderIds.Length == i + 1) {
                                sql.Append("{0}" + usedPartsOutboundOrderIds[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter(usedPartsOutboundOrderIds[i].ToString(CultureInfo.InvariantCulture), usedPartsOutboundOrderIds[i]));
                            } else {
                                sql.Append("{0}" + usedPartsOutboundOrderIds[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter(usedPartsOutboundOrderIds[i].ToString(CultureInfo.InvariantCulture), usedPartsOutboundOrderIds[i]));
                            }
                        }
                        sql.Append(")");
                    } else {
                        if(!string.IsNullOrEmpty(code)) {
                            sql.Append(@" and upo.code like {0}code ");
                            dbParameters.Add(db.CreateDbParameter("code", "%" + code + "%"));
                        }
                        if(outboundType.HasValue) {
                            sql.Append(@" and upo.outboundType={0}outboundType");
                            dbParameters.Add(db.CreateDbParameter("outboundType", outboundType.Value));
                        }
                        if(!string.IsNullOrEmpty(usedPartsWarehouseCode)) {
                            sql.Append(@" and upo.usedPartsWarehouseCode like {0}usedPartsWarehouseCode ");
                            dbParameters.Add(db.CreateDbParameter("usedPartsWarehouseCode", "%" + usedPartsWarehouseCode + "%"));
                        }
                        if(!string.IsNullOrEmpty(usedPartsWarehouseName)) {
                            sql.Append(@" and upo.usedPartsWarehouseName like {0}usedPartsWarehouseName ");
                            dbParameters.Add(db.CreateDbParameter("usedPartsWarehouseName", "%" + usedPartsWarehouseName + "%"));
                        }
                        if(outboundTimeStart.HasValue) {
                            sql.Append(" and upo.createTime>=to_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = outboundTimeStart.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(outboundTimeEnd.HasValue) {
                            sql.Append(" and upo.createTime<=to_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = outboundTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                        if(!string.IsNullOrEmpty(sourceCode)) {
                            sql.Append(@" and upo.sourceCode like {0}sourceCode ");
                            dbParameters.Add(db.CreateDbParameter("sourceCode", "%" + sourceCode + "%"));
                        }
                        if(!string.IsNullOrEmpty(relatedCompanyCode)) {
                            sql.Append(@" and upo.relatedCompanyCode like {0}relatedCompanyCode ");
                            dbParameters.Add(db.CreateDbParameter("relatedCompanyCode", "%" + relatedCompanyCode + "%"));
                        }
                        if(!string.IsNullOrEmpty(relatedCompanyName)) {
                            sql.Append(@" and upo.relatedCompanyName like {0}relatedCompanyName ");
                            dbParameters.Add(db.CreateDbParameter("relatedCompanyName", "%" + relatedCompanyName + "%"));
                        }
                        if(!string.IsNullOrEmpty(usedPartsBarCode)) {
                            sql.Append(@" and upo.usedPartsBarCode like {0}usedPartsBarCode ");
                            dbParameters.Add(db.CreateDbParameter("usedPartsBarCode", "%" + usedPartsBarCode + "%"));
                        }
                        if(!string.IsNullOrEmpty(claimBillCode)) {
                            sql.Append(@" and upod.ClaimBillCode like {0}claimBillCode ");
                            dbParameters.Add(db.CreateDbParameter("claimBillCode", "%" + claimBillCode + "%"));
                        }
                        if(ifBillable.HasValue) {
                            sql.Append(@" and upo.ifBillable={0}ifBillable");
                            dbParameters.Add(db.CreateDbParameter("ifBillable", ifBillable.Value ? 1 : 0));
                        }
                        if(ifAlreadySettled.HasValue) {
                            sql.Append(@" and upo.ifAlreadySettled={0}ifAlreadySettled");
                            dbParameters.Add(db.CreateDbParameter("ifAlreadySettled", ifAlreadySettled.Value ? 1 : 0));
                        }
                    }
                    var sqlStr = sql.ToString();
                    if(sqlStr.IndexOf(" where 1=1  and ") > 0)
                        sqlStr = sqlStr.Replace(" where 1=1  and ", " where ");
                    else
                        sqlStr = sqlStr.Replace(" where 1=1 ", "");
                    var command = db.CreateDbCommand(string.Format(sqlStr, db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0)
                                return new object[] {
                                    "旧件出库单编号","出库仓库名称",ErrorStrings.Export_Title_PartsOutboundBill_OutBoundType,ErrorStrings.Export_Title_PartsOuterPurchaseChange_SourceCode,ErrorStrings.Export_Title_BoxUpTask_CounterpartCompanyCode,
                                    ErrorStrings.Export_Title_BoxUpTask_CounterpartCompanyName,"旧件配件编号","旧件配件名称","旧件条码","旧件库位编号",
                                    ErrorStrings.Export_Title_WarehouseArea_Quantity,"结算价格","成本价格","旧件供应商编号","旧件供应商名称","索赔单类型",
                                    "索赔单编号","是否祸首件","祸首件供应商编号","祸首件供应商名称","责任单位编号",
                                    "祸首件编号","祸首件名称",ErrorStrings.Export_Title_PartsBranch_Remark
                                };
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch
                (Exception) {
                return false;
            }
        }
    }
}