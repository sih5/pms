﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Globalization;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        public bool ExportUsedPartsOutboundOrder(int[] ids, string code, int? outboundType, string usedPartsWarehouseCode, string usedPartsWarehouseName, DateTime? outboundTimeStart, DateTime? outboundTimeEnd, string sourceCode, string relatedCompanyCode, string relatedCompanyName, string usedPartsBarCode, string claimBillCode, bool? ifBillable, bool? ifAlreadySettled, out string fileName) {
            fileName = GetExportFilePath("旧件出库单_" + ".xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated) {
                    throw new Exception(ErrorStrings.File_Validation_User);
                }
                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.Append(@"select distinct
                                        upo.Code,
                                        upo.UsedPartsWarehouseCode,
                                        upo.UsedPartsWarehouseName,
                                        cast(decode(upo.OutboundType,
                                                1,'调拨出库',
                                                2,'旧件处理出库',
                                                3,'清退供应商出库',
                                                4,'借用出库',
                                                5,'加工领料出库'
                                                ) as varchar2(20)) as OutboundType,
                                        upo.SourceCode,
                                        upo.RelatedCompanyCode,
                                        upo.RelatedCompanyName,
                                        upo.TotalAmount,
                                        upo.TotalQuantity,
                                        upo.Operator,
                                        upo.Remark,
                                        cast(decode(upo.IfBillable,
                                                0,'否',
                                                1,'是'
                                                ) as varchar2(20)) as IfBillable,
                                        cast(decode(upo.IfAlreadySettled,
                                                0,'否',
                                                1,'是'
                                                ) as varchar2(20)) as IfAlreadySettled,
                                        upo.CreatorName,
                                        upo.CreateTime
                                    from UsedPartsOutboundOrder upo");
                    var dbParameters = new List<DbParameter>();

                    sql.Append(@" where (( EXISTS (SELECT 
                                                1 AS C1
                                                FROM UsedPartsWarehouseStaff upws
                                                WHERE (upws.PersonnelId = {0}personnelId) AND (upws.UsedPartsWarehouseId = upo.UsedPartsWarehouseId)
                                            )))");
                    dbParameters.Add(db.CreateDbParameter("personnelId", Utils.GetCurrentUserInfo().Id));

                    if(ids != null && ids.Length > 0) {
                        sql.Append(" and upo.id in (");
                        for(var i = 0; i < ids.Length; i++) {
                            if(ids.Length == i + 1) {
                                sql.Append("{0}" + ids[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter(ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            } else {
                                sql.Append("{0}" + ids[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter(ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                        }
                        sql.Append(")");
                    } else {
                        if(!string.IsNullOrEmpty(code)) {
                            sql.Append(@" and code like {0}code ");
                            dbParameters.Add(db.CreateDbParameter("code", "%" + code + "%"));
                        }
                        if(outboundType.HasValue) {
                            sql.Append(@" and outboundType={0}outboundType");
                            dbParameters.Add(db.CreateDbParameter("outboundType", outboundType.Value));
                        }
                        if(!string.IsNullOrEmpty(usedPartsWarehouseCode)) {
                            sql.Append(@" and usedPartsWarehouseCode like {0}usedPartsWarehouseCode ");
                            dbParameters.Add(db.CreateDbParameter("usedPartsWarehouseCode", "%" + usedPartsWarehouseCode + "%"));
                        }
                        if(!string.IsNullOrEmpty(usedPartsWarehouseName)) {
                            sql.Append(@" and usedPartsWarehouseName like {0}usedPartsWarehouseName ");
                            dbParameters.Add(db.CreateDbParameter("usedPartsWarehouseName", "%" + usedPartsWarehouseName + "%"));
                        }
                        if(outboundTimeStart.HasValue) {
                            sql.Append(" and createTime>=to_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = outboundTimeStart.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(outboundTimeEnd.HasValue) {
                            sql.Append(" and createTime<=to_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = outboundTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                        if(!string.IsNullOrEmpty(sourceCode)) {
                            sql.Append(@" and sourceCode like {0}sourceCode ");
                            dbParameters.Add(db.CreateDbParameter("sourceCode", "%" + sourceCode + "%"));
                        }
                        if(!string.IsNullOrEmpty(relatedCompanyCode)) {
                            sql.Append(@" and relatedCompanyCode like {0}relatedCompanyCode ");
                            dbParameters.Add(db.CreateDbParameter("relatedCompanyCode", "%" + relatedCompanyCode + "%"));
                        }
                        if(!string.IsNullOrEmpty(relatedCompanyName)) {
                            sql.Append(@" and relatedCompanyName like {0}relatedCompanyName ");
                            dbParameters.Add(db.CreateDbParameter("relatedCompanyName", "%" + relatedCompanyName + "%"));
                        }
                        if(!string.IsNullOrEmpty(usedPartsBarCode)) {
                            sql.Append(@" and usedPartsBarCode like {0}usedPartsBarCode ");
                            dbParameters.Add(db.CreateDbParameter("usedPartsBarCode", "%" + usedPartsBarCode + "%"));
                        }
                        if(!string.IsNullOrEmpty(claimBillCode)) {
                            sql.Append(@" and upod.ClaimBillCode like {0}claimBillCode ");
                            dbParameters.Add(db.CreateDbParameter("claimBillCode", "%" + claimBillCode + "%"));
                        }
                        if(ifBillable.HasValue) {
                            sql.Append(@" and ifBillable={0}ifBillable");
                            dbParameters.Add(db.CreateDbParameter("ifBillable", ifBillable.Value ? 1 : 0));
                        }
                        if(ifAlreadySettled.HasValue) {
                            sql.Append(@" and ifAlreadySettled={0}ifAlreadySettled");
                            dbParameters.Add(db.CreateDbParameter("ifAlreadySettled", ifAlreadySettled.Value ? 1 : 0));
                        }
                    }
                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0)
                                return new object[] {
                                    "旧件出库单编号","出库仓库编号","出库仓库名称",ErrorStrings.Export_Title_PartsOutboundBill_OutBoundType,ErrorStrings.Export_Title_PartsOuterPurchaseChange_SourceCode,
                                    ErrorStrings.Export_Title_BoxUpTask_CounterpartCompanyCode,ErrorStrings.Export_Title_BoxUpTask_CounterpartCompanyName,ErrorStrings.Export_Title_InternalAllocationBill_TotalAmount,"旧件总数量",ErrorStrings.Export_Title_Customertransferbill_Operator,ErrorStrings.Export_Title_PartsBranch_Remark,
                                    "是否可结算","是否已结算",ErrorStrings.Export_Title_AccountPeriod_CreatorName,ErrorStrings.Export_Title_AccountPeriod_CreateTime
                                };
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch
                (Exception) {
                return false;
            }
        }
    }
}
