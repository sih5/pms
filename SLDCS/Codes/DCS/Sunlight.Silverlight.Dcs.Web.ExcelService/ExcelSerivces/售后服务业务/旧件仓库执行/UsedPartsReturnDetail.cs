﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        /// <summary>
        /// 导出旧件清退单清单
        /// </summary>
        public bool ExportUsedPartsReturnDetail(int[] usedPartsReturnOrderIds, out string fileName) {
            fileName = GetExportFilePath("旧件清退单清单_" + ".xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated) {
                    throw new Exception(ErrorStrings.File_Validation_User);
                }
                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.Append(@"Select (Select Code From UsedPartsReturnOrder Where Usedpartsreturndetail.Usedpartsreturnorderid=Id),
                                            Serialnumber,
                                           Usedpartscode,
                                           Usedpartsname,
                                           Usedpartsbarcode,
                                           Usedpartssuppliercode,
                                           Usedpartssuppliername,
                                           cast(Case Claimbilltype
                                             When 1 Then
                                              '维修索赔'
                                             When 2 Then
                                              '保养索赔'
                                             When 3 Then
                                              '服务活动索赔'
                                             When 4 Then
                                              '配件索赔'
                                           End as varchar2(20)),
                                           Claimbillcode,
                                           Unitprice,
                                           Plannedamount,
                                           Confirmedamount,
                                           Outboundamount,
                                           cast(Decode(Iffaultyparts, 1, '是', 0, '否') as varchar2(20)),
                                           Faultypartscode,
                                           Faultypartssuppliercode,
                                           Faultypartssuppliername,
                                           Responsibleunitcode,
                                           Responsibleunitname
                                      From Usedpartsreturndetail");
                    var dbParameters = new List<DbParameter>();

                    if(usedPartsReturnOrderIds.Length > 0) {
                        sql.Append(@" where UsedPartsReturnDetail.UsedPartsReturnOrderId in (");
                        for(var i = 0; i < usedPartsReturnOrderIds.Length; i++) {
                            var tempPartsPurchaseOrderIdWithNum = "partsPurchaseOrderId" + i;
                            if(i == usedPartsReturnOrderIds.Length - 1) {
                                var tempParam = "{0}" + tempPartsPurchaseOrderIdWithNum;
                                sql.Append(tempParam);
                            } else {
                                var tempParam = "{0}" + tempPartsPurchaseOrderIdWithNum + ",";
                                sql.Append(tempParam);
                            }
                            dbParameters.Add(db.CreateDbParameter(tempPartsPurchaseOrderIdWithNum, usedPartsReturnOrderIds[i]));
                        }
                        sql.Append(@")");
                    }
                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0)
                                return new object[] {
                                    "清退单编号",ErrorStrings.Export_Title_InternalAllocationBill_SerialNumber,"旧件配件编号","旧件配件名称","旧件条码","旧件供应商编号","旧件供应商名称","索赔单类型","索赔单编号",
                                    ErrorStrings.Export_Title_InternalAllocationBill_UnitPrice,ErrorStrings.Export_Title_PartsPurchasePlanDetail_PlanAmount,ErrorStrings.Export_Title_PartsPurchaseOrder_ConfirmedAmount,ErrorStrings.Export_Title_Partsoutboundplan_OutboundFulfillment,"是否祸首件","祸首件编号","祸首件供应商编号","祸首件供应商名称","责任单位编号","责任单位名称"
                                };
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch
                (Exception) {
                return false;
            }
        }

        public bool ImportUsedPartsReturnDetail(int usedWarehouseId, string fileName, out int excelImportNum, out List<UsedPartsReturnDetailExtend> rightData, out List<UsedPartsReturnDetailExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<UsedPartsReturnDetailExtend>();
            var rightList = new List<UsedPartsReturnDetailExtend>();
            var allList = new List<UsedPartsReturnDetailExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);
                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("UsedPartsReturnDetail", out notNullableFields, out fieldLenght);

                List<object> excelColumns;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    //指定文件中的列对应的名称
                    excelOperator.AddColumnDataSource("旧件条码", "UsedPartsBarCode");

                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    //Excel单行导入
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        //接收Excel导入数据
                        var tempImportObj = new UsedPartsReturnDetailExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);

                        tempImportObj.UsedPartsBarCodeStr = newRow["UsedPartsBarCode"];

                        var errorMsgs = new List<string>();
                        //旧件条码检查
                        var fieldIndex = notNullableFields.IndexOf("UsedPartsBarCode".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.UsedPartsBarCodeStr)) {
                            if(fieldIndex > -1)
                                errorMsgs.Add(ErrorStrings.ImpUsedPartsReturnDetail_Validation1);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.UsedPartsBarCodeStr) > fieldLenght["UsedPartsBarCode".ToUpper()])
                                errorMsgs.Add(ErrorStrings.ImpUsedPartsReturnDetail_Validation2);
                        }

                        if(errorMsgs.Count > 0)
                            tempImportObj.ErrorMsg = string.Join("; ", errorMsgs);
                        allList.Add(tempImportObj);
                        return false;
                    });
                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    //剩下的数据进行业务检查
                    var usedPartsStockCheck = tempRightList.Select(r => r.UsedPartsBarCodeStr).Distinct().ToArray();
                    var dbUsedPartsStocks = new List<UsedPartsStockExtend>();
                    Func<string[], bool> getDbUsedPartsStocks = values => {
                        var usedpartsstock = new UsedPartsStockExtend();
                        usedpartsstock.BranchId = values[0] == "" ? 0 : Convert.ToInt32(values[0]);
                        usedpartsstock.UsedPartsBarCode = values[1];
                        usedpartsstock.UsedPartsId = Convert.ToInt32(values[2]);
                        usedpartsstock.UsedPartsCode = values[3];
                        usedpartsstock.UsedPartsName = values[4];
                        usedpartsstock.SettlementPrice = values[5] == "" ? 0 : Convert.ToDecimal(values[5]);
                        usedpartsstock.ClaimBillId = values[6] == "" ? 0 : Convert.ToInt32(values[6]);
                        usedpartsstock.ClaimBillCode = values[7];
                        usedpartsstock.ClaimBillType = values[8] == "" ? 0 : Convert.ToInt32(values[8]);
                        usedpartsstock.IfFaultyParts = values[9] != "0";
                        usedpartsstock.FaultyPartsCode = values[10];
                        usedpartsstock.FaultyPartsSupplierId = values[11] == "" ? 0 : Convert.ToInt32(values[11]);
                        usedpartsstock.FaultyPartsSupplierCode = values[12];
                        usedpartsstock.FaultyPartsSupplierName = values[13];
                        usedpartsstock.ResponsibleUnitId = values[14] == "" ? 0 : Convert.ToInt32(values[14]);
                        usedpartsstock.ResponsibleUnitCode = values[15];
                        usedpartsstock.ResponsibleUnitName = values[16];
                        usedpartsstock.UsedPartsSupplierId = values[17] == "" ? 0 : Convert.ToInt32(values[17]);
                        usedpartsstock.UsedPartsSupplierCode = values[18];
                        usedpartsstock.UsedPartsSupplierName = values[19];
                        usedpartsstock.LockedQuantity = Convert.ToInt32(values[20]);
                        dbUsedPartsStocks.Add(usedpartsstock);
                        return false;
                    };
                    var sql = string.Format("select BranchId,UsedPartsBarCode,UsedPartsId,UsedPartsCode,UsedPartsName,SettlementPrice,ClaimBillId,ClaimBillCode,ClaimBillType,IfFaultyParts,FaultyPartsCode,FaultyPartsSupplierId,FaultyPartsSupplierCode,FaultyPartsSupplierName,ResponsibleUnitId,ResponsibleUnitCode,ResponsibleUnitName,UsedPartsSupplierId,UsedPartsSupplierCode,UsedPartsSupplierName,LockedQuantity from UsedPartsStock where UsedPartsWarehouseId={0}", usedWarehouseId);
                    db.QueryDataWithInOperator(sql, "UsedPartsBarCode", false, usedPartsStockCheck, getDbUsedPartsStocks);
                    foreach(var item in tempRightList) {
                        var temp = dbUsedPartsStocks.FirstOrDefault(r => r.UsedPartsBarCode == item.UsedPartsBarCodeStr);
                        if(temp == null)
                            item.ErrorMsg = "仓库中不存在此条码的旧件";
                        else {
                            if(temp.LockedQuantity != 0) {
                                item.ErrorMsg = "此旧件已被锁定，无法清退";
                            } else {
                                item.BranchId = temp.BranchId ?? 0;
                                item.UsedPartsBarCode = temp.UsedPartsBarCode;
                                item.UsedPartsId = temp.UsedPartsId;
                                item.UsedPartsCode = temp.UsedPartsCode;
                                item.UsedPartsName = temp.UsedPartsName;
                                item.UnitPrice = temp.SettlementPrice ?? 0;
                                item.ClaimBillId = temp.ClaimBillId ?? 0;
                                item.ClaimBillCode = temp.ClaimBillCode;
                                item.ClaimBillType = temp.ClaimBillType ?? 0;
                                item.IfFaultyParts = temp.IfFaultyParts;
                                item.FaultyPartsCode = temp.FaultyPartsCode;
                                item.FaultyPartsSupplierId = temp.FaultyPartsSupplierId;
                                item.FaultyPartsSupplierCode = temp.FaultyPartsSupplierCode;
                                item.FaultyPartsSupplierName = temp.FaultyPartsSupplierName;
                                item.ResponsibleUnitId = temp.ResponsibleUnitId;
                                item.ResponsibleUnitCode = temp.ResponsibleUnitCode;
                                item.ResponsibleUnitName = temp.ResponsibleUnitName;
                                item.UsedPartsSupplierId = temp.UsedPartsSupplierId;
                                item.UsedPartsSupplierCode = temp.UsedPartsSupplierCode;
                                item.UsedPartsSupplierName = temp.UsedPartsSupplierName;
                            }
                        }
                    }

                    //获取所有不合格数据
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    //获取所有合格数据
                    rightList = allList.Except(errorList).ToList();
                    var serialNumber = 1;
                    //将合格数据的值填上
                    foreach(var rightItem in rightList) {
                        rightItem.SerialNumber = serialNumber++;
                        rightItem.PlannedAmount = 1;
                        rightItem.ConfirmedAmount = 0;
                        rightItem.OutboundAmount = 0;
                    }
                }
                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempObj = list[index - 1];
                            var values = new object[] {
                                //设置错误信息导出的列的值
                                tempObj.UsedPartsBarCodeStr, tempObj.ErrorMsg
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }

                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }
    }
}
