﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        /// <summary>
        /// 导出旧件清退单
        /// </summary>
        public bool ExportUsedPartsReturnOrder(int[] usedPartsReturnOrderIds, int personnelId, string outboundWarehouseCode, string outboundWarehouseName, string returnOfficeCode, string returnOfficeName, string code, int? returnType, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName) {
            fileName = GetExportFilePath("旧件清退单_" + ".xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated) {
                    throw new Exception(ErrorStrings.File_Validation_User);
                }
                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.AppendFormat(@"Select a.Code, /*清退单编号*/
                                       a.Outboundwarehousecode, /*出库仓库编号*/
                                       a.Outboundwarehousename, /*出库仓库名称*/
                                       a.Returnofficecode, /*清退单位编号*/
                                       a.Returnofficename, /*清退单位名称*/
                                       cast(Case a.Returntype /*清退类型*/
                                         When 1 Then
                                          '清退责任单位'
                                         When 2 Then
                                          '清退配件供应商'
                                         When 3 Then
                                          '内部部门'
                                       End as varchar2(20)),
                                       cast(Decode(a.Status, 1, '新建', 2, '已批准', '作废') as varchar2(20)), /*状态*/
                                       a.Totalamount, /*总金额*/
                                       a.Totalquantity, /*总数量*/
                                       a.Remark, /*备注*/
                                       a.Operator, /*经办人*/
                                       a.Creatorname, /*创建人*/
                                       a.Createtime, /*创建时间*/
                                       a.Modifiername, /*修改人*/
                                       a.Modifytime, /*修改时间*/
                                       a.Approvername, /*审批人*/
                                       a.Approvetime, /*审批时间*/
                                       a.Abandonername, /*作废人*/
                                       a.Abandontime /*作废时间*/
                                  From Usedpartsreturnorder a Where Exists
                             (Select 1 From Usedpartswarehousestaff t
                                     Where (t.Personnelid = {0})
                                       And (t.Usedpartswarehouseid = a.Outboundwarehouseid)) ", personnelId);
                    var tableNames = sql.ToString().GetRegexNames();
                    var dbParameters = new List<DbParameter>();

                    if(usedPartsReturnOrderIds != null && usedPartsReturnOrderIds.Length > 0) {
                        sql.Append(@" and a.id in (");
                        for(var i = 0; i < usedPartsReturnOrderIds.Length; i++) {
                            var tempPartsPurchaseOrderIdWithNum = "partsPurchaseOrderId" + i;
                            if(i == usedPartsReturnOrderIds.Length - 1) {
                                var tempParam = "{0}" + tempPartsPurchaseOrderIdWithNum;
                                sql.Append(tempParam);
                            } else {
                                var tempParam = "{0}" + tempPartsPurchaseOrderIdWithNum + ",";
                                sql.Append(tempParam);
                            }
                            dbParameters.Add(db.CreateDbParameter(tempPartsPurchaseOrderIdWithNum, usedPartsReturnOrderIds[i]));
                        }
                        sql.Append(@")");
                    } else {
                        if(!string.IsNullOrEmpty(outboundWarehouseCode)) {
                            sql.Append(@" and Upper(a.outboundWarehouseCode) =Upper({0}outboundWarehouseCode) ");
                            dbParameters.Add(db.CreateDbParameter("outboundWarehouseCode", "%" + outboundWarehouseCode + "%"));
                        }
                        if(!string.IsNullOrEmpty(outboundWarehouseName)) {
                            sql.Append(@" and a.outboundWarehouseName ={0}outboundWarehouseName ");
                            dbParameters.Add(db.CreateDbParameter("outboundWarehouseName", "%" + outboundWarehouseName + "%"));
                        }
                        if(!string.IsNullOrEmpty(returnOfficeCode)) {
                            sql.Append(@" and Upper(a.returnOfficeCode) =Upper({0}returnOfficeCode) ");
                            dbParameters.Add(db.CreateDbParameter("returnOfficeCode", "%" + returnOfficeCode + "%"));
                        }
                        if(!string.IsNullOrEmpty(returnOfficeName)) {
                            sql.Append(@" and a.returnOfficeName ={0}returnOfficeName ");
                            dbParameters.Add(db.CreateDbParameter("returnOfficeName", "%" + returnOfficeName + "%"));
                        }
                        if(!string.IsNullOrEmpty(code)) {
                            sql.Append(@" and Upper(a.code) =Upper({0}code) ");
                            dbParameters.Add(db.CreateDbParameter("code", "%" + code + "%"));
                        }
                        if(returnType.HasValue) {
                            sql.Append(@" and a.returnType ={0}returnType");
                            dbParameters.Add(db.CreateDbParameter("returnType", returnType.Value));
                        }
                        if(status.HasValue) {
                            sql.Append(@" and a.status ={0}Status");
                            dbParameters.Add(db.CreateDbParameter("status", status.Value));
                        }
                        if(createTimeBegin.HasValue) {
                            sql.Append(@" and a.createTime >=To_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(@" and a.createTime <=To_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                    }
                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0)
                                return tableNames;
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch
                (Exception) {
                return false;
            }
        }
    }
}
