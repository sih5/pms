﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        public bool ExportUsedPartsTransferDetail(int[] ids, int personnelId, string code, int? status, string originWarehouseCode, string destinationWarehouseCode, string originWarehouseName, string destinationWarehouseName, int? outboundStatus, int? inboundStatus, DateTime? createTimeBegin, DateTime? createTimeEnd, DateTime? approveTimeBegin, DateTime? approveTimeEnd, out string fileName)
        {
            fileName = GetExportFilePath("旧件调拨单清单_" + ".xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated) {
                    throw new Exception(ErrorStrings.File_Validation_User);
                }
                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.AppendFormat(@"select     a.Code,
                                            a.OriginWarehouseName,
                                            a.DestinationWarehouseName,
                                            cast(case a.Status
                                            when 1 then '新建'
                                            when 2 then '生效' 
                                            when 99 then '作废' end as varchar2(20)) as Status,
                                            cast(case a.OutboundStatus
                                            when 1 then '待出库'
                                            when 2 then '部分出库'
                                            when 3 then '出库完成'
                                            when 4 then '终止出库' end as varchar2(20)) as OutboundStatus,
                                            cast(case a.InboundStatus
                                            when 1 then '待入库'
                                            when 2 then '部分入库'
                                            when 3 then '入库完成'
                                            when 4 then '终止入库' end as varchar2(20)) as InboundStatus,
                                            a.Remark,
                                            a.CreatorName,
                                            a.CreateTime,
                                            a.ApproverName,
                                            a.ApproveTime,
                                            a.ModifierName,
                                            a.ModifyTime,
                                            a.AbandonerName,
                                            a.AbandonTime,
                                            b.UsedPartsBarCode,
                                            b.UsedPartsCode,
                                            b.UsedPartsName,
                                            b.Price,
                                            b.PlannedAmount,
                                            b.ConfirmedAmount,
                                            b.UsedPartsBatchNumber,
                                            b.UsedPartsSerialNumber,
                                            b.ClaimBillCode,
                                            b.ClaimBillType,
                                            cast(case b.IfFaultyParts
                                            when 1 then '是'
                                            when 0 then '否' end as varchar2(20)) as IfFaultyParts,
                                            rel.BusinessCode as UsedPartsBrandCode,
                                            b.UsedPartsSupplierName,
                                            b.FaultyPartsSupplierName,
                                            rel2.BusinessCode as FPSupplierBusinessCode,
                                            b.ResponsibleUnitName
                                    from UsedPartsTransferOrder a
                                    left join UsedPartsTransferDetail b
                                    on a.id = b.UsedPartsTransferOrderId
                                    left join usedpartswarehouse uw on uw.id = a.originwarehouseid
                                    left join BranchSupplierRelation rel on rel.supplierid =
                                                                              b.usedpartssupplierid
                                                                          and rel.branchid = b.branchid
                                                                          and rel.partssalescategoryid =
                                                                              uw.partssalescategoryid
                                                                          and rel.status = 1
                                    left join BranchSupplierRelation rel2 on rel2.supplierid =
                                                                              b.FaultyPartsSupplierId
                                                                          and rel2.branchid = b.branchid
                                                                          and rel2.partssalescategoryid =
                                                                              uw.partssalescategoryid
                                                                          and rel2.status = 1
                                    where  ((Exists
                                        (Select 1
                                            From Usedpartswarehousestaff T1
                                            Where (T1.Personnelid = {0})
                                            And (T1.Usedpartswarehouseid = a.Originwarehouseid)))
                                    Or (Exists
                                        (Select 1
                                            From Usedpartswarehousestaff T2
                                            Where (T2.Personnelid = {0})
                                            And (T2.Usedpartswarehouseid = a.Destinationwarehouseid)))) ", personnelId);
                    var dbParameters = new List<DbParameter>();
                    if (ids.Length>0)
                    {
                        sql.Append(@" and a.id in (");
                        for (var i = 0; i < ids.Length; i++)
                        {
                            var tempCodeWithNum = "id" + i;
                            if (i == ids.Length - 1)
                            {
                                var tempParam = "{0}" + tempCodeWithNum;
                                sql.Append(tempParam);
                            }
                            else
                            {
                                var tempParam = "{0}" + tempCodeWithNum + ",";
                                sql.Append(tempParam);
                            }
                            dbParameters.Add(db.CreateDbParameter(tempCodeWithNum, ids[i]));
                        }
                        sql.Append(@")");
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(code))
                        {
                            sql.Append(" and Upper(a.code) like Upper({0}code) ");
                            dbParameters.Add(db.CreateDbParameter("code", "%" + code + "%"));
                        }
                        if (status.HasValue)
                        {
                            sql.Append(" and a.status={0}status");
                            dbParameters.Add(db.CreateDbParameter("status", status.Value));
                        }
                        if (!string.IsNullOrEmpty(originWarehouseCode))
                        {
                            sql.Append(" and Upper(a.originWarehouseCode) like Upper({0}originWarehouseCode) ");
                            dbParameters.Add(db.CreateDbParameter("originWarehouseCode", "%" + originWarehouseCode + "%"));
                        }
                        if (!string.IsNullOrEmpty(destinationWarehouseCode))
                        {
                            sql.Append(" and Upper(a.destinationWarehouseCode) like Upper({0}destinationWarehouseCode) ");
                            dbParameters.Add(db.CreateDbParameter("destinationWarehouseCode", "%" + destinationWarehouseCode + "%"));
                        }
                        if (!string.IsNullOrEmpty(originWarehouseName))
                        {
                            sql.Append(" and a.originWarehouseName like {0}originWarehouseName ");
                            dbParameters.Add(db.CreateDbParameter("originWarehouseName", "%" + originWarehouseName + "%"));
                        }
                        if (!string.IsNullOrEmpty(destinationWarehouseName))
                        {
                            sql.Append(" and a.destinationWarehouseName like {0}destinationWarehouseName ");
                            dbParameters.Add(db.CreateDbParameter("destinationWarehouseName", "%" + destinationWarehouseName + "%"));
                        }
                        if (outboundStatus.HasValue)
                        {
                            sql.Append(" and a.outboundStatus={0}outboundStatus");
                            dbParameters.Add(db.CreateDbParameter("outboundStatus", outboundStatus.Value));
                        }
                        if (inboundStatus.HasValue)
                        {
                            sql.Append(" and a.inboundStatus={0}inboundStatus");
                            dbParameters.Add(db.CreateDbParameter("inboundStatus", inboundStatus.Value));
                        }
                        if (createTimeBegin.HasValue)
                        {
                            sql.Append(" and a.createTime>=to_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if (createTimeEnd.HasValue)
                        {
                            sql.Append(" and a.createTime<=to_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                        if (approveTimeBegin.HasValue)
                        {
                            sql.Append(" and a.approveTime>=to_date({0}approveTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = approveTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("approveTimeBegin", tempTime.ToString("G")));
                        }
                        if (approveTimeEnd.HasValue)
                        {
                            sql.Append(" and a.approveTime<=to_date({0}approveTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = approveTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("approveTimeEnd", tempTime.ToString("G")));
                        }
                    }
                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    "旧件调拨单编号","源旧件仓库名称","目的旧件仓库名称","旧件调拨单状态",ErrorStrings.Export_Title_PartsPurReturnOrder_OutStatus,ErrorStrings.Export_Title_PartsPurchaseOrder_InBoundStatus,ErrorStrings.Export_Title_PartsBranch_Remark,ErrorStrings.Export_Title_AccountPeriod_CreatorName,ErrorStrings.Export_Title_AccountPeriod_CreateTime,ErrorStrings.Export_Title_PackingPropertyApp_ApproverName,ErrorStrings.Export_Title_PackingPropertyApp_ApproveTime,ErrorStrings.Export_Title_AccountPeriod_ModifierName,ErrorStrings.Export_Title_AccountPeriod_ModifyTime,
                                    ErrorStrings.Export_Title_PartsBranch_AbandonerName,ErrorStrings.Export_Title_PartsBranch_AbandonerTime,"旧件条码","旧件配件编号","旧件配件名称",ErrorStrings.Export_Title_PartsPurReturnOrder_Price,ErrorStrings.Export_Title_PartsPurchasePlanDetail_PlanAmount,ErrorStrings.Export_Title_PartsPurchaseOrder_ConfirmedAmount,"旧件批次号","旧件序列号","索赔单编号","索赔单类型","是否祸首件","旧件供应商业务编号","旧件供应商名称","祸首件供应商名称","祸首件供应商业务编码","责任单位名称"
                                 };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }

                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }


        }
    }
}
