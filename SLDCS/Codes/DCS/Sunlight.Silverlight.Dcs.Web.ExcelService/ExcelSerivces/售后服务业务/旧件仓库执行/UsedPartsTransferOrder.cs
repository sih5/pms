﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        /// <summary>
        /// 导出旧件调拨单
        /// </summary>
        public bool ExportUsedPartsTransferOrder(int? billId, int personnelId, string code, int? status, string originWarehouseCode, string destinationWarehouseCode, string originWarehouseName, string destinationWarehouseName, int? outboundStatus, int? inboundStatus, DateTime? createTimeBegin, DateTime? createTimeEnd, DateTime? approveTimeBegin, DateTime? approveTimeEnd, out string fileName) {
            fileName = GetExportFilePath("旧件调拨单.xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated) {
                    throw new Exception(ErrorStrings.File_Validation_User);
                }
                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.AppendFormat(@"Select a.Code, /*旧件调拨单编号*/
                                        a.Originwarehousename, /*源旧件仓库名称*/
                                        a.Destinationwarehousename, /*目的旧件仓库名称*/
                                        a.Totalamount, /*总金额*/
                                        a.Totalquantity, /*总数量*/
                                        cast(Decode(a.Status, 1, '新建', 2, '生效', 99, '作废') as varchar2(20)), /*旧件调拨单状态*/
                                        cast(Case a.Outboundstatus /*出库状态*/
                                            When 1 Then
                                            '待出库'
                                            When 2 Then
                                            '部分出库'
                                            When 3 Then
                                            '出库完成'
                                            When 4 Then
                                            '终止出库'
                                        End as varchar2(20)),
                                        cast(Case a.Inboundstatus /*入库状态*/
                                            When 1 Then
                                            '待入库'
                                            When 2 Then
                                            '部分入库'
                                            When 3 Then
                                            '入库完成'
                                            When 4 Then
                                            '终止入库'
                                        End as varchar2(20)),
                                        a.Remark, /*备注*/
                                        a.Createtime, /*创建时间*/
                                        a.Approvername, /*审批人*/
                                        a.Approvetime, /*审批 时间*/
                                        a.Modifiername, /*修改人*/
                                        a.Modifytime, /*修改时间*/
                                        a.Abandonername, /*作废人*/
                                        a.Abandontime /*作废时间*/
                                    From Usedpartstransferorder a
                                    Where ((Exists
                                        (Select 1
                                            From Usedpartswarehousestaff T1
                                            Where (T1.Personnelid = {0})
                                            And (T1.Usedpartswarehouseid = a.Originwarehouseid)))
                                    Or (Exists
                                        (Select 1
                                            From Usedpartswarehousestaff T2
                                            Where (T2.Personnelid = {0})
                                            And (T2.Usedpartswarehouseid = a.Destinationwarehouseid)))) ", personnelId);
                    var tableNames = sql.ToString().GetRegexNames();
                    var dbParameters = new List<DbParameter>();
                    if(billId.HasValue) {
                        sql.Append(" and a.Id={0}billId");
                        dbParameters.Add(db.CreateDbParameter("billId", billId.Value));
                    } else {
                        if(!string.IsNullOrEmpty(code)) {
                            sql.Append(" and Upper(a.code) like Upper({0}code) ");
                            dbParameters.Add(db.CreateDbParameter("code", "%" + code + "%"));
                        }
                        if(status.HasValue) {
                            sql.Append(" and a.status={0}status");
                            dbParameters.Add(db.CreateDbParameter("status", status.Value));
                        }
                        if(!string.IsNullOrEmpty(originWarehouseCode)) {
                            sql.Append(" and Upper(a.originWarehouseCode) like Upper({0}originWarehouseCode) ");
                            dbParameters.Add(db.CreateDbParameter("originWarehouseCode", "%" + originWarehouseCode + "%"));
                        }
                        if(!string.IsNullOrEmpty(destinationWarehouseCode)) {
                            sql.Append(" and Upper(a.destinationWarehouseCode) like Upper({0}destinationWarehouseCode) ");
                            dbParameters.Add(db.CreateDbParameter("destinationWarehouseCode", "%" + destinationWarehouseCode + "%"));
                        }
                        if(!string.IsNullOrEmpty(originWarehouseName)) {
                            sql.Append(" and a.originWarehouseName like {0}originWarehouseName ");
                            dbParameters.Add(db.CreateDbParameter("originWarehouseName", "%" + originWarehouseName + "%"));
                        }
                        if(!string.IsNullOrEmpty(destinationWarehouseName)) {
                            sql.Append(" and a.destinationWarehouseName like {0}destinationWarehouseName ");
                            dbParameters.Add(db.CreateDbParameter("destinationWarehouseName", "%" + destinationWarehouseName + "%"));
                        }
                        if(outboundStatus.HasValue) {
                            sql.Append(" and a.outboundStatus={0}outboundStatus");
                            dbParameters.Add(db.CreateDbParameter("outboundStatus", outboundStatus.Value));
                        }
                        if(inboundStatus.HasValue) {
                            sql.Append(" and a.inboundStatus={0}inboundStatus");
                            dbParameters.Add(db.CreateDbParameter("inboundStatus", inboundStatus.Value));
                        }
                        if(createTimeBegin.HasValue) {
                            sql.Append(" and a.createTime>=to_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(" and a.createTime<=to_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                        if(approveTimeBegin.HasValue) {
                            sql.Append(" and a.approveTime>=to_date({0}approveTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = approveTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("approveTimeBegin", tempTime.ToString("G")));
                        }
                        if(approveTimeEnd.HasValue) {
                            sql.Append(" and a.approveTime<=to_date({0}approveTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = approveTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("approveTimeEnd", tempTime.ToString("G")));
                        }
                    }
                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return tableNames;
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }

                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }


        }
    }
}
