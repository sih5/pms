﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        /// <summary>
        /// 导出旧件库存
        /// </summary>
        public bool ExportUsedPartsStock(int? usedPartsWarehouseId, string usedPartsBarCode, string claimBillCode, int? claimBillType, string code, string usedPartsCode, string usedPartsName, string usedPartsSupplierCode, string usedPartsSupplierName, string faultyPartsSupplierCode, string faultyPartsSupplierName, string responsibleUnitCode, string responsibleUnitName, DateTime? createTimeBegin, DateTime? createTimeEnd, bool? ifFaultyParts, out string fileName) {
            fileName = GetExportFilePath("旧件库存_" + ".xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated) {
                    throw new Exception(ErrorStrings.File_Validation_User);
                }

                var userinfo = Utils.GetCurrentUserInfo();

                var db = DbHelper.GetDbHelp(ConnectionString);
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.AppendFormat(@"select upw.Code,
                               upw.Name,
                               upwa.Code,
                               ups.UsedPartsCode,
                               ups.UsedPartsName,
                               ups.UsedPartsBarCode,
                               ups.StorageQuantity,
                               ups.LockedQuantity,
                               ups.SettlementPrice,
                               cast(Case Ups.Claimbilltype
                                 When 1 Then
                                  '维修索赔'
                                 When 2 Then
                                  '保养索赔'
                                 When 3 Then
                                  '服务活动索赔'
                                 When 4 Then
                                  '配件索赔'
                               End as varchar2(20)),
                               ups.ClaimBillCode,
                               cast(Decode(Ups.Iffaultyparts, 1, '是', 0, '否') as varchar2(20)),
                               ups.FaultyPartsCode,
                               ups.UsedPartsSupplierCode,
                               ups.UsedPartsSupplierName,
                               ups.FaultyPartsSupplierId,
                               ups.FaultyPartsSupplierCode,
                               ups.FaultyPartsSupplierName,
                               ups.ResponsibleUnitCode,
                               ups.ResponsibleUnitName,
                               ups.CreateTime,
                               ups.CreatorName
                        from UsedPartsStock ups
                        inner join UsedPartsWarehouse upw
                        on upw.Id = ups.UsedPartsWarehouseId
                        inner join UsedPartsWarehouseArea upwa 
                        on upwa.Id = ups.UsedPartsWarehouseAreaId where ups.BranchId = {0} ", userinfo.EnterpriseId);
                    var dbParameters = new List<DbParameter>();
                    if(usedPartsWarehouseId.HasValue) {
                        sql.Append(@" and ups.usedPartsWarehouseId = {0}usedPartsWarehouseId ");
                        dbParameters.Add(db.CreateDbParameter("usedPartsWarehouseId", usedPartsWarehouseId.Value));
                    }
                    if(!String.IsNullOrEmpty(usedPartsBarCode)) {
                        sql.Append(@" and ups.code like {0}usedPartsBarCode ");
                        dbParameters.Add(db.CreateDbParameter("usedPartsBarCode", "%" + usedPartsBarCode + "%"));
                    }
                    if(!String.IsNullOrEmpty(claimBillCode)) {
                        sql.Append(@" and ups.claimBillCode like {0}claimBillCode ");
                        dbParameters.Add(db.CreateDbParameter("claimBillCode", "%" + claimBillCode + "%"));
                    }
                    if(claimBillType.HasValue) {
                        sql.Append(@" and ups.claimBillType = {0}claimBillType ");
                        dbParameters.Add(db.CreateDbParameter("claimBillType", "%" + claimBillType + "%"));
                    }
                    if(!String.IsNullOrEmpty(code)) {
                        sql.Append(@" and upwa.code like {0}code ");
                        dbParameters.Add(db.CreateDbParameter("code", "%" + code + "%"));
                    }
                    if(!String.IsNullOrEmpty(usedPartsCode)) {
                        sql.Append(@" and ups.usedPartsCode like {0}usedPartsCode ");
                        dbParameters.Add(db.CreateDbParameter("usedPartsCode", "%" + usedPartsCode + "%"));
                    }
                    if(!String.IsNullOrEmpty(usedPartsName)) {
                        sql.Append(@" and ups.usedPartsName like {0}usedPartsName ");
                        dbParameters.Add(db.CreateDbParameter("usedPartsName", "%" + usedPartsName + "%"));
                    }
                    if(!String.IsNullOrEmpty(usedPartsSupplierCode)) {
                        sql.Append(@" and ups.usedPartsSupplierCode like {0}usedPartsSupplierCode ");
                        dbParameters.Add(db.CreateDbParameter("usedPartsSupplierCode", "%" + usedPartsSupplierCode + "%"));
                    }
                    if(!String.IsNullOrEmpty(usedPartsSupplierName)) {
                        sql.Append(@" and ups.usedPartsSupplierName like {0}usedPartsSupplierName ");
                        dbParameters.Add(db.CreateDbParameter("usedPartsSupplierName", "%" + usedPartsSupplierName + "%"));
                    }
                    if(!String.IsNullOrEmpty(faultyPartsSupplierCode)) {
                        sql.Append(@" and ups.faultyPartsSupplierCode like {0}faultyPartsSupplierCode ");
                        dbParameters.Add(db.CreateDbParameter("faultyPartsSupplierCode", "%" + faultyPartsSupplierCode + "%"));
                    }
                    if(!String.IsNullOrEmpty(faultyPartsSupplierName)) {
                        sql.Append(@" and ups.faultyPartsSupplierName like {0}faultyPartsSupplierName ");
                        dbParameters.Add(db.CreateDbParameter("faultyPartsSupplierName", "%" + faultyPartsSupplierName + "%"));
                    }
                    if(!String.IsNullOrEmpty(responsibleUnitCode)) {
                        sql.Append(@" and ups.responsibleUnitCode like {0}responsibleUnitCode ");
                        dbParameters.Add(db.CreateDbParameter("responsibleUnitCode", "%" + responsibleUnitCode + "%"));
                    }
                    if(!String.IsNullOrEmpty(responsibleUnitName)) {
                        sql.Append(@" and ups.responsibleUnitName like {0}responsibleUnitName ");
                        dbParameters.Add(db.CreateDbParameter("responsibleUnitName", "%" + responsibleUnitName + "%"));
                    }
                    if(createTimeBegin.HasValue) {
                        sql.Append(@" and ups.createTime >=to_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                        var tempValue = createTimeBegin.Value;
                        var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                        dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                    }
                    if(createTimeEnd.HasValue) {
                        sql.Append(@" and ups.createTime <=to_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                        var tempValue = createTimeEnd.Value;
                        var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                        dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                    }
                    if(ifFaultyParts.HasValue) {
                        sql.Append(@" and ups.ifFaultyParts = {0}ifFaultyParts ");
                        dbParameters.Add(db.CreateDbParameter("ifFaultyParts", ifFaultyParts.Value));
                    }
                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    "旧件仓库编号","旧件仓库名称",ErrorStrings.Export_Title_WarehouseArea_WarehouseAreaCode,"旧件配件编号","旧件配件名称","旧件条码","库存数量","锁定数量",ErrorStrings.Export_Title_PartsPurReturnOrder_Price,"索赔单类型","索赔单编号","是否祸首件","祸首件编号","旧件供应商编号","旧件供应商名称","祸首件供应商Id","祸首件供应商编号","祸首件供应商名称","责任单位编号","责任单位名称",ErrorStrings.Export_Title_PartsInboundCheckBill_InBoundTime,"入库人"
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }
    }
}
