﻿

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        /// <summary>
        /// 导入客户服务评价明细
        /// </summary>
        public bool ImportCustomerServiceEvaluate(string fileName, out int excelImportNum, int branchId, out List<CustomerServiceEvaluateExtend> rightData, out List<CustomerServiceEvaluateExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<CustomerServiceEvaluateExtend>();
            var rightList = new List<CustomerServiceEvaluateExtend>();
            var allList = new List<CustomerServiceEvaluateExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("CustomerServiceEvaluate", out notNullableFields, out fieldLenght);

                List<object> excelColumns;

                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    #region 指定文件中的列对应的名称 例如 excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode, "PartCode");
                    excelOperator.AddColumnDataSource("索赔单编号", "RepairClaimBillCode");
                    excelOperator.AddColumnDataSource("评价来源", "EvaluateSource");
                    excelOperator.AddColumnDataSource("评价分值", "EvaluateGrade");
                    excelOperator.AddColumnDataSource("不满意要素", "DisContentFactor");
                    excelOperator.AddColumnDataSource("评价时间", "EvaluateTime");
                    #endregion

                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;

                    #region 获取对应枚举
                    var keyValuePairs = new[] {
                            new KeyValuePair<string, string>("DisContentFactor", "DisContentFactor")
                        };
                    tempExcelOperator.ImportHelper.LoadEnumFromDb(null, null, keyValuePairs);

                    #endregion

                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var tempImportObj = new CustomerServiceEvaluateExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        #region 根据导入的字段给对应的实体属性赋值 tempImportObj.PartsSalesCategoryNameStr = row["PartsSalesCategoryName"].Trim();
                        tempImportObj.RepairClaimBillCode = newRow["RepairClaimBillCode"];
                        tempImportObj.EvaluateSource = newRow["EvaluateSource"];
                        tempImportObj.EvaluateGradeStr = newRow["EvaluateGrade"];
                        tempImportObj.DisContentFactorStr = newRow["DisContentFactor"];
                        tempImportObj.EvaluateTimeStr = newRow["EvaluateTime"];
                        #endregion

                        var tempErrorMessage = new List<string>();

                        #region 导入的数据基本检查
                        var fieldIndex = notNullableFields.IndexOf("RepairClaimBillCode".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.RepairClaimBillCode)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add("索赔单编号不能为空");
                        }

                        fieldIndex = notNullableFields.IndexOf("EvaluateSource".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.EvaluateSource)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add("评价来源不能为空");
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.EvaluateSource) > fieldLenght["EvaluateSource".ToUpper()])
                                tempErrorMessage.Add("评价来源过长");
                        }

                        fieldIndex = notNullableFields.IndexOf("EvaluateGrade".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.EvaluateGradeStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add("评价分值不能为空");
                        } else {
                            int checkValue;
                            if(int.TryParse(tempImportObj.EvaluateGradeStr, out checkValue)) {
                                if(checkValue < 1 || checkValue > 10)
                                    tempErrorMessage.Add("评价分值范围是1-10");
                                else
                                    tempImportObj.EvaluateGrade = checkValue;
                            } else {
                                tempErrorMessage.Add("评价分值必须是整数");
                            }
                        }
                        if(!string.IsNullOrEmpty(tempImportObj.DisContentFactorStr)) {
                            var tempEnumValue = tempExcelOperator.ImportHelper.GetEnumValue("DisContentFactor", tempImportObj.DisContentFactorStr);
                            if(!tempEnumValue.HasValue)
                                tempErrorMessage.Add("不满意要素不正确");
                            else
                                tempImportObj.DisContentFactor = tempEnumValue.Value;
                        }

                        if(!string.IsNullOrEmpty(tempImportObj.EvaluateTimeStr)) {
                            DateTime checkValue;
                            if(DateTime.TryParse(tempImportObj.EvaluateTimeStr, out checkValue)) {
                                tempImportObj.EvaluateTime = checkValue;
                            } else {
                                tempErrorMessage.Add("评价时间填写错误");
                            }
                        }
                        #endregion

                        if(tempErrorMessage.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });
                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    #region 剩下的数据进行业务检查

                    //6、查询客户服务评价（客户服务评价.维修保养索赔单编号=导入数据.索赔单号）如果存在返回数据，提示：客户服务评价已存在

                    var groups = tempRightList.GroupBy(r => new {
                        r.RepairClaimBillCode
                    }).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        groupItem.ErrorMsg = "文件内数索赔单编号重复";
                    }
                    var repairClaimBillCodeNeedCheck = tempRightList.Select(r => r.RepairClaimBillCode).Distinct().ToArray();
                    var dbRepairClaimBills = new List<RepairClaimBillExtend>();
                    Func<string[], bool> getDbRepairClaimBills = value => {
                        var dbObj = new RepairClaimBillExtend {
                            Id = Convert.ToInt32(value[0]),
                            ClaimBillCode = value[1],
                        };
                        dbRepairClaimBills.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select Id,ClaimBillCode from RepairClaimBill ", "ClaimBillCode", false, repairClaimBillCodeNeedCheck, getDbRepairClaimBills);
                    foreach(var tempRight in tempRightList) {
                        var repairClaimBill = dbRepairClaimBills.FirstOrDefault(v => v.ClaimBillCode == tempRight.RepairClaimBillCode);
                        if(repairClaimBill == null) {
                            tempRight.ErrorMsg = string.Format("索赔单:{0}不存在", tempRight.RepairClaimBillCode);
                        } else {
                            tempRight.RepairClaimBillId = repairClaimBill.Id;
                        }
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    repairClaimBillCodeNeedCheck = tempRightList.Select(r => r.RepairClaimBillCode).Distinct().ToArray();
                    var dbCustomerServiceEvaluates = new List<CustomerServiceEvaluateExtend>();
                    Func<string[], bool> getDbCustomerServiceEvaluates = value => {
                        var dbObj = new CustomerServiceEvaluateExtend {
                            RepairClaimBillCode = value[0],
                        };
                        dbCustomerServiceEvaluates.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select RepairClaimBillCode from CustomerServiceEvaluate ", "RepairClaimBillCode", false, repairClaimBillCodeNeedCheck, getDbCustomerServiceEvaluates);
                    foreach(var tempRight in tempRightList) {
                        var repairClaimBill = dbCustomerServiceEvaluates.FirstOrDefault(v => v.RepairClaimBillCode == tempRight.RepairClaimBillCode);
                        if(repairClaimBill != null) {
                            tempRight.ErrorMsg = "客户服务评价已存在";
                        }
                    }

                    #endregion
                    //获取所有不合格数据
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    //获取所有合格数据
                    rightList = allList.Except(errorList).ToList();
                    //获取星级系数
                    #region 将合格数据的值填上
                    #endregion
                }
                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempObj = list[index - 1];
                            var values = new object[] {
                                #region 设置错误信息导出的列的值
                                tempObj.RepairClaimBillCode,
                                tempObj.EvaluateSource,
                                tempObj.EvaluateGradeStr,
                                tempObj.DisContentFactorStr,
                                tempObj.EvaluateTimeStr,
                                tempObj.ErrorMsg
                                #endregion
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }

                //导入所有合格数据
                if(!rightList.Any())
                    return true;
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    //开启事务，新增更新配件在一个事务内
                    var ts = conn.BeginTransaction();
                    try {
                        //新增配件
                        if(rightList.Any()) {
                            #region 获取对应的插入的SQL 通过db.GetInsertSql方法
                            //获取新增数据的sql语句，Id为主键
                            var sqlInsert = db.GetInsertSql("CustomerServiceEvaluate", "Id", new[] {
                                    "RepairClaimBillId", "RepairClaimBillCode", "EvaluateSource", "EvaluateGrade", "DisContentFactor", "EvaluateTime","CreatorId","CreatorName","CreateTime"
                                });
                            #endregion

                            //往数据库增加配件信息
                            var userinfo = Utils.GetCurrentUserInfo();
                            foreach(var item in rightList) {
                                #region 添加Sql的参数
                                var command = db.CreateDbCommand(sqlInsert, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("RepairClaimBillId", item.RepairClaimBillId));
                                command.Parameters.Add(db.CreateDbParameter("RepairClaimBillCode", item.RepairClaimBillCode));
                                command.Parameters.Add(db.CreateDbParameter("EvaluateSource", item.EvaluateSource));
                                command.Parameters.Add(db.CreateDbParameter("EvaluateGrade", item.EvaluateGrade));
                                command.Parameters.Add(db.CreateDbParameter("DisContentFactor", item.DisContentFactor));
                                command.Parameters.Add(db.CreateDbParameter("EvaluateTime", item.EvaluateTime));
                                command.Parameters.Add(db.CreateDbParameter("CreatorId", userinfo.Id));
                                command.Parameters.Add(db.CreateDbParameter("CreatorName", userinfo.Name));
                                command.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                command.ExecuteNonQuery();
                                #endregion
                            }
                        }
                        //无异常提交
                        ts.Commit();
                    } catch(Exception) {
                        //报错回滚
                        ts.Rollback();
                        throw;
                    } finally {
                        if(conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }

    }
}
