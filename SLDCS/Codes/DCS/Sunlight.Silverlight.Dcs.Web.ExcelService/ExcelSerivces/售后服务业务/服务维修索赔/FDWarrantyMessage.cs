﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Entities;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        public bool ImportFdWarrantyMessage(string fileName, out int excelImportNum, out List<FdWarrantyMessageExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<FdWarrantyMessageExtend>();
            var rightList = new List<FdWarrantyMessageExtend>();
            var allList = new List<FdWarrantyMessageExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("FdWarrantyMessage", out notNullableFields, out fieldLenght);

                List<object> excelColumns;

                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    #region 指定文件中的列对应的名称 例如 excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode, "PartCode");

                    excelOperator.AddColumnDataSource("更新时间", "gengxinshijian");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_BranchSupplierRelation_Code, "fengongsibianhao");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Branch_BranchName, "fengongsimingcheng");
                    excelOperator.AddColumnDataSource("索赔单编号", "suopeidanbianhao");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_BottomStock_Status, "danjuzhuangtai");
                    excelOperator.AddColumnDataSource("维修类型", "weixiuleixing");
                    excelOperator.AddColumnDataSource("VIN", "vin");
                    excelOperator.AddColumnDataSource("出厂编号", "chuchangbianhao");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Partssalescategory_Name, "pinpai");
                    excelOperator.AddColumnDataSource("整车产品线", "zhengchechanpinxian");
                    excelOperator.AddColumnDataSource("服务产品线", "fuwuchanpinxian");
                    excelOperator.AddColumnDataSource("产品线类型", "chanpinxianleixing");
                    excelOperator.AddColumnDataSource("出厂日期", "chuchangriqi");
                    excelOperator.AddColumnDataSource("购买日期", "goumairiqi");
                    excelOperator.AddColumnDataSource("行驶里程", "xingshilicheng");
                    excelOperator.AddColumnDataSource("首次故障里程", "shouciguzhanglicheng");
                    excelOperator.AddColumnDataSource("工况类别", "gongkuangleibie");
                    excelOperator.AddColumnDataSource("驱动形式", "qudongxingshi");
                    excelOperator.AddColumnDataSource("车型", "chexing");
                    excelOperator.AddColumnDataSource("公告号", "gonggaohao");
                    excelOperator.AddColumnDataSource("发动机型号", "fadongjixinghao");
                    excelOperator.AddColumnDataSource("发动机编号", "fadongjibianhao");
                    excelOperator.AddColumnDataSource("市场部", "shichangbu");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_Code, "fuwuzhanbianhao");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_BranchSupplierRelation_BusinessCode, "yewubianhao");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_Name, "fuwuzhanmingcheng");
                    excelOperator.AddColumnDataSource("车辆联系人", "chelianglianxiren");
                    excelOperator.AddColumnDataSource("产品编号", "chanpinbianhao");
                    excelOperator.AddColumnDataSource("报修时间", "baoxiushijian");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealerserviceinfo_CreateTime, "chuangjianriqi");
                    excelOperator.AddColumnDataSource("故障代码", "guzhangdaima");
                    excelOperator.AddColumnDataSource("故障现象描述", "guzhangxianxiangmiaoshu");
                    excelOperator.AddColumnDataSource("故障模式", "guzhangmoshi");
                    excelOperator.AddColumnDataSource("祸首件图号", "huoshoujiantuhao");
                    excelOperator.AddColumnDataSource("祸首件名称", "huoshoujianmingcheng");
                    excelOperator.AddColumnDataSource("祸首件生产厂家", "huoshoujianshengchanchangjia");
                    excelOperator.AddColumnDataSource("责任单位编码", "zerendanweibianma");
                    excelOperator.AddColumnDataSource("责任单位名称", "zerendanweimingcheng");
                    excelOperator.AddColumnDataSource("祸首件所属总成编号", "huoshoujiansuoshuzongcheng");
                    excelOperator.AddColumnDataSource("配件所属总成", "peijiansuoshuzongcheng");
                    excelOperator.AddColumnDataSource("供应商确认状态", "gongyingshangquerenzhuangtai");
                    excelOperator.AddColumnDataSource("分公司审核时间", "fengongsishenheshijian");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsPurReturnOrder_ApprovertComment, "zhongshenyijian");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsShiftOrder_ApproverTime, "zhongshenshijian");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsShiftOrder_ApproverName, "zhongshenren");
                    excelOperator.AddColumnDataSource("供应商确认意见", "gongyingshangquerenyijian");
                    excelOperator.AddColumnDataSource("是否向责任供应商索赔", "iszerengongyingshangsuopei");
                    excelOperator.AddColumnDataSource("维修索赔申请单号", "weixiusuopeishenqingdanhao");
                    excelOperator.AddColumnDataSource("维修索赔申请类型", "weixiusuopeishenqingleixing");
                    excelOperator.AddColumnDataSource("外出索赔单编号", "waichusuopeidanbianhao");
                    excelOperator.AddColumnDataSource("外出索赔单状态", "waichusuopeidanzhuangtai");
                    excelOperator.AddColumnDataSource("外出索赔申请单号", "waichusuopeishenqingdanhao");
                    excelOperator.AddColumnDataSource("外出人数", "waichurenshu");
                    excelOperator.AddColumnDataSource("外出天数", "waichutianshu");
                    excelOperator.AddColumnDataSource("外出里程", "waichulicheng");
                    excelOperator.AddColumnDataSource("外出车费单价", "waichuchefeidanjia");
                    excelOperator.AddColumnDataSource("外出路途补助单价", "waichulutubuzhudanjia");
                    excelOperator.AddColumnDataSource("外出类型", "waichuleixing");
                    excelOperator.AddColumnDataSource("外出原因", "waichuyuanyin");
                    excelOperator.AddColumnDataSource("外出审核意见", "waichushenheyijian");
                    excelOperator.AddColumnDataSource("维修索赔单其他费用说明", "suopeidanqitafeiyongshuoming");
                    excelOperator.AddColumnDataSource("外出其他费用说明", "waichuqitafeiyongshuoming");
                    excelOperator.AddColumnDataSource("故障备注", "guzhangbeizhu");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsInboundCheckBill_SettlementStatus, "jiesuanzhuangtai");
                    excelOperator.AddColumnDataSource("结算时间", "jiesuanshijian");
                    excelOperator.AddColumnDataSource("材料费", "cailiaofei");
                    excelOperator.AddColumnDataSource("工时费", "gongshifei");
                    excelOperator.AddColumnDataSource("外出服务费", "waichufuwufei");
                    excelOperator.AddColumnDataSource("配件管理费", "peijianguanlifei");
                    excelOperator.AddColumnDataSource("故障件清退运费", "guzhangjianqingtuiyunfei");
                    excelOperator.AddColumnDataSource("其他费用", "qitafeiyong");
                    excelOperator.AddColumnDataSource("费用合计", "feiyongheji");
                    excelOperator.AddColumnDataSource("工时定额", "gongshidinge");
                    excelOperator.AddColumnDataSource("工时单价", "gongshidanjia");
                    excelOperator.AddColumnDataSource("服务活动编号", "fuwuhuodongbianhao");
                    excelOperator.AddColumnDataSource("服务活动名称", "fuwuhuodongmingcheng");
                    excelOperator.AddColumnDataSource("服务活动类型", "fuwuhuodongleixing");
                    excelOperator.AddColumnDataSource("服务活动内容", "fuwuhuodongneirong");
                    excelOperator.AddColumnDataSource("客户类型", "kehuleixing");
                    excelOperator.AddColumnDataSource("费用分类", "feiyongfenlei");
                    excelOperator.AddColumnDataSource("文件编号", "wenjianbianhao");
                    excelOperator.AddColumnDataSource("报告简述", "baogaojianshu");
                    excelOperator.AddColumnDataSource("市场部意见", "shichangbuyijian");
                    excelOperator.AddColumnDataSource("批准人", "pizhunren");
                    excelOperator.AddColumnDataSource("涉及金额", "shejijine");
                    excelOperator.AddColumnDataSource("质量部意见", "zhiliangbuyijian");
                    excelOperator.AddColumnDataSource("快报代号", "kuaibaodaihao");
                    excelOperator.AddColumnDataSource("涉及车辆", "shejicheliang");
                    excelOperator.AddColumnDataSource("问题描述", "wentimiaoshu");
                    excelOperator.AddColumnDataSource("整改维修方案", "zhenggaiweixiufangan");
                    excelOperator.AddColumnDataSource("索赔单位", "suopeidanwei");
                    excelOperator.AddColumnDataSource("是否需要向事业部索赔", "isshiyebusuopei");
                    excelOperator.AddColumnDataSource("总成件名称", "zongchengjianmingcheng");
                    excelOperator.AddColumnDataSource("市场AB质量信息快报编号", "shichangABkuaibaobianhao");
                    excelOperator.AddColumnDataSource("维修索赔单编号", "weixiusuopeidanbianhao");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_AccountPeriod_Status, "zhuangtai");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Agency_LinkManMobile, "lianxidianhua");
                    excelOperator.AddColumnDataSource("扣款金额", "koukuanjine");
                    excelOperator.AddColumnDataSource("补款金额", "bukuanjine");
                    excelOperator.AddColumnDataSource("索赔单位事业部编码", "suopeidanweishiyebubianma");
                    excelOperator.AddColumnDataSource("索赔单位事业部名称", "suopeidanweishiyebumingcheng");
                    excelOperator.AddColumnDataSource("外出公里数", "waichugonglishu");
                    excelOperator.AddColumnDataSource("高速公路拖车费", "gaosugonglutuochefei");
                    excelOperator.AddColumnDataSource("外地救援地", "waidijiuyuandi");
                    excelOperator.AddColumnDataSource("车牌号", "chepaihao");
                    excelOperator.AddColumnDataSource("申请单类型", "shendanleixing");
                    excelOperator.AddColumnDataSource("索赔供应商编号", "suopeigongyingshangbianhao");
                    excelOperator.AddColumnDataSource("索赔供应商名称", "suopeigongyingshangmingcheng");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_AccountPeriod_CreateTime, "chuangjianshijian");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_DealerPartsRetailOrder_Address, "kehudizhi");
                    excelOperator.AddColumnDataSource("维修属性", "weixiushuxing");
                    excelOperator.AddColumnDataSource("外出索赔备注信息", "waichusuopeibeizhuxinxi");
                    excelOperator.AddColumnDataSource("服务站注册证号", "fuwuzhanzhucezhenghao");
                    excelOperator.AddColumnDataSource("服务站注册名称", "fuwuzhanzhucemingcheng");
                    excelOperator.AddColumnDataSource("服务站邮政编码", "fuwuzhanyouzhengbianma");
                    excelOperator.AddColumnDataSource("服务站固定电话", "fuwuzhangudingdianhua");
                    excelOperator.AddColumnDataSource("服务站传真", "fuwuzhanchuanzhen");
                    excelOperator.AddColumnDataSource("服务站企业类型", "fuwuzhanqiyeleixing");
                    excelOperator.AddColumnDataSource("服务站省编码", "fuwuzhanshengbianma");
                    excelOperator.AddColumnDataSource("服务站省名称", "fuwuzhanshengmingcheng");
                    excelOperator.AddColumnDataSource("服务站市编码", "fuwuzhanshibianma");
                    excelOperator.AddColumnDataSource("服务站市名称", "fuwuzhanshimingcheng");
                    excelOperator.AddColumnDataSource("服务站区编码", "fuwuzhanqubianma");
                    excelOperator.AddColumnDataSource("服务站区名称", "fuwuzhanqumingcheng");
                    excelOperator.AddColumnDataSource("服务站地址", "fuwuzhandizhi");
                    excelOperator.AddColumnDataSource("服务站联系人", "fuwuzhanlianxiren");
                    excelOperator.AddColumnDataSource("服务站电子邮箱", "fuwuzhandianziyouxiang");
                    excelOperator.AddColumnDataSource("服务站服务经理", "fuwuzhanjingli");
                    excelOperator.AddColumnDataSource("服务站成立日期", "fuwuzhanchengliriqi");
                    excelOperator.AddColumnDataSource("服务站状态", "fuwuzhanzhuangtai");
                    excelOperator.AddColumnDataSource("二级服务站编号", "erjifuwuzhanbianhao");
                    excelOperator.AddColumnDataSource("二级服务站名称", "erjifuwuzhanmingcheng");
                    excelOperator.AddColumnDataSource("二级服务站固定电话", "erjifuwuzhangudingdianhua");
                    excelOperator.AddColumnDataSource("二级服务站企业类型", "erjifuwuzhanqiyeleixing");
                    excelOperator.AddColumnDataSource("二级服务站地址", "erjifuwuzhandizhi");
                    excelOperator.AddColumnDataSource("二级服务站电子邮箱", "erjifuwuzhandianziyouxiang");
                    excelOperator.AddColumnDataSource("二级服务站服务经理", "erjifuwuzhanjingli");
                    excelOperator.AddColumnDataSource("二级服务站状态", "erjifuwuzhanzhuangtai");
                    #endregion

                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;

                    #region 获取对应枚举
                    var keyValuePairs = new[] {
                            new KeyValuePair<string, string>("Status","BaseData_Status")
                        };
                    tempExcelOperator.ImportHelper.LoadEnumFromDb(null, null, keyValuePairs);

                    #endregion

                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var tempImportObj = new FdWarrantyMessageExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        #region

                        tempImportObj.GengxinshijianErrorStr = newRow["gengxinshijian"];
                        tempImportObj.Fengongsibianhao = newRow["fengongsibianhao"];
                        tempImportObj.Fengongsimingcheng = newRow["fengongsimingcheng"];
                        tempImportObj.Suopeidanbianhao = newRow["suopeidanbianhao"];
                        tempImportObj.Danjuzhuangtai = newRow["danjuzhuangtai"];
                        tempImportObj.Weixiuleixing = newRow["weixiuleixing"];
                        tempImportObj.Vin = newRow["vin"];
                        tempImportObj.Chuchangbianhao = newRow["chuchangbianhao"];
                        tempImportObj.Pinpai = newRow["pinpai"];
                        tempImportObj.Zhengchechanpinxian = newRow["zhengchechanpinxian"];
                        tempImportObj.Fuwuchanpinxian = newRow["fuwuchanpinxian"];
                        tempImportObj.Chanpinxianleixing = newRow["chanpinxianleixing"];
                        tempImportObj.ChuchangriqiErrorStr = newRow["chuchangriqi"];
                        tempImportObj.GoumairiqiErrorStr = newRow["goumairiqi"];
                        tempImportObj.XingshilichengErrorStr = newRow["xingshilicheng"];
                        tempImportObj.ShouciguzhanglichengErrorStr = newRow["shouciguzhanglicheng"];
                        tempImportObj.Gongkuangleibie = newRow["gongkuangleibie"];
                        tempImportObj.Qudongxingshi = newRow["qudongxingshi"];
                        tempImportObj.Chexing = newRow["chexing"];
                        tempImportObj.Gonggaohao = newRow["gonggaohao"];
                        tempImportObj.Fadongjixinghao = newRow["fadongjixinghao"];
                        tempImportObj.Fadongjibianhao = newRow["fadongjibianhao"];
                        tempImportObj.Shichangbu = newRow["shichangbu"];
                        tempImportObj.Fuwuzhanbianhao = newRow["fuwuzhanbianhao"];
                        tempImportObj.Yewubianhao = newRow["yewubianhao"];
                        tempImportObj.Fuwuzhanmingcheng = newRow["fuwuzhanmingcheng"];
                        tempImportObj.Chelianglianxiren = newRow["chelianglianxiren"];
                        tempImportObj.Chanpinbianhao = newRow["chanpinbianhao"];
                        tempImportObj.BaoxiushijianErrorStr = newRow["baoxiushijian"];
                        tempImportObj.ChuangjianriqiErrorStr = newRow["chuangjianriqi"];
                        tempImportObj.Guzhangdaima = newRow["guzhangdaima"];
                        tempImportObj.Guzhangxianxiangmiaoshu = newRow["guzhangxianxiangmiaoshu"];
                        tempImportObj.Guzhangmoshi = newRow["guzhangmoshi"];
                        tempImportObj.Huoshoujiantuhao = newRow["huoshoujiantuhao"];
                        tempImportObj.Huoshoujianmingcheng = newRow["huoshoujianmingcheng"];
                        tempImportObj.Huoshoujianshengchanchangjia = newRow["huoshoujianshengchanchangjia"];
                        tempImportObj.Zerendanweibianma = newRow["zerendanweibianma"];
                        tempImportObj.Zerendanweimingcheng = newRow["zerendanweimingcheng"];
                        tempImportObj.Huoshoujiansuoshuzongcheng = newRow["huoshoujiansuoshuzongcheng"];
                        tempImportObj.Peijiansuoshuzongcheng = newRow["peijiansuoshuzongcheng"];
                        tempImportObj.Gongyingshangquerenzhuangtai = newRow["gongyingshangquerenzhuangtai"];
                        tempImportObj.FengongsishenheshijianErrorStr = newRow["fengongsishenheshijian"];
                        tempImportObj.Zhongshenyijian = newRow["zhongshenyijian"];
                        tempImportObj.ZhongshenshijianErrorStr = newRow["zhongshenshijian"];
                        tempImportObj.Zhongshenren = newRow["zhongshenren"];
                        tempImportObj.Gongyingshangquerenyijian = newRow["gongyingshangquerenyijian"];
                        tempImportObj.Iszerengongyingshangsuopei = newRow["iszerengongyingshangsuopei"];
                        tempImportObj.Weixiusuopeishenqingdanhao = newRow["weixiusuopeishenqingdanhao"];
                        tempImportObj.Weixiusuopeishenqingleixing = newRow["weixiusuopeishenqingleixing"];
                        tempImportObj.Waichusuopeidanbianhao = newRow["waichusuopeidanbianhao"];
                        tempImportObj.Waichusuopeidanzhuangtai = newRow["waichusuopeidanzhuangtai"];
                        tempImportObj.Waichusuopeishenqingdanhao = newRow["waichusuopeishenqingdanhao"];
                        tempImportObj.WaichurenshuErrorStr = newRow["waichurenshu"];
                        tempImportObj.WaichutianshuErrorStr = newRow["waichutianshu"];
                        tempImportObj.WaichulichengErrorStr = newRow["waichulicheng"];
                        tempImportObj.WaichuchefeidanjiaErrorStr = newRow["waichuchefeidanjia"];
                        tempImportObj.WaichulutubuzhudanjiaErrorStr = newRow["waichulutubuzhudanjia"];
                        tempImportObj.Waichuleixing = newRow["waichuleixing"];
                        tempImportObj.Waichuyuanyin = newRow["waichuyuanyin"];
                        tempImportObj.Waichushenheyijian = newRow["waichushenheyijian"];
                        tempImportObj.Suopeidanqitafeiyongshuoming = newRow["suopeidanqitafeiyongshuoming"];
                        tempImportObj.Waichuqitafeiyongshuoming = newRow["waichuqitafeiyongshuoming"];
                        tempImportObj.Guzhangbeizhu = newRow["guzhangbeizhu"];
                        tempImportObj.Jiesuanzhuangtai = newRow["jiesuanzhuangtai"];
                        tempImportObj.JiesuanshijianErrorStr = newRow["jiesuanshijian"];
                        tempImportObj.CailiaofeiErrorStr = newRow["cailiaofei"];
                        tempImportObj.GongshifeiErrorStr = newRow["gongshifei"];
                        tempImportObj.WaichufuwufeiErrorStr = newRow["waichufuwufei"];
                        tempImportObj.PeijianguanlifeiErrorStr = newRow["peijianguanlifei"];
                        tempImportObj.GuzhangjianqingtuiyunfeiErrorStr = newRow["guzhangjianqingtuiyunfei"];
                        tempImportObj.QitafeiyongErrorStr = newRow["qitafeiyong"];
                        tempImportObj.FeiyonghejiErrorStr = newRow["feiyongheji"];
                        tempImportObj.GongshidingeErrorStr = newRow["gongshidinge"];
                        tempImportObj.GongshidanjiaErrorStr = newRow["gongshidanjia"];
                        tempImportObj.Fuwuhuodongbianhao = newRow["fuwuhuodongbianhao"];
                        tempImportObj.Fuwuhuodongmingcheng = newRow["fuwuhuodongmingcheng"];
                        tempImportObj.Fuwuhuodongleixing = newRow["fuwuhuodongleixing"];
                        tempImportObj.Fuwuhuodongneirong = newRow["fuwuhuodongneirong"];
                        tempImportObj.Kehuleixing = newRow["kehuleixing"];
                        tempImportObj.Feiyongfenlei = newRow["feiyongfenlei"];
                        tempImportObj.Wenjianbianhao = newRow["wenjianbianhao"];
                        tempImportObj.Baogaojianshu = newRow["baogaojianshu"];
                        tempImportObj.Shichangbuyijian = newRow["shichangbuyijian"];
                        tempImportObj.Pizhunren = newRow["pizhunren"];
                        tempImportObj.ShejijineErrorStr = newRow["shejijine"];
                        tempImportObj.Zhiliangbuyijian = newRow["zhiliangbuyijian"];
                        tempImportObj.Kuaibaodaihao = newRow["kuaibaodaihao"];
                        tempImportObj.Shejicheliang = newRow["shejicheliang"];
                        tempImportObj.Wentimiaoshu = newRow["wentimiaoshu"];
                        tempImportObj.Zhenggaiweixiufangan = newRow["zhenggaiweixiufangan"];
                        tempImportObj.Suopeidanwei = newRow["suopeidanwei"];
                        tempImportObj.Isshiyebusuopei = newRow["isshiyebusuopei"];
                        tempImportObj.Zongchengjianmingcheng = newRow["zongchengjianmingcheng"];
                        tempImportObj.ShichangABkuaibaobianhao = newRow["shichangABkuaibaobianhao"];
                        tempImportObj.Weixiusuopeidanbianhao = newRow["weixiusuopeidanbianhao"];
                        tempImportObj.Zhuangtai = newRow["zhuangtai"];
                        tempImportObj.Lianxidianhua = newRow["lianxidianhua"];
                        tempImportObj.KoukuanjineErrorStr = newRow["koukuanjine"];
                        tempImportObj.BukuanjineErrorStr = newRow["bukuanjine"];
                        tempImportObj.Suopeidanweishiyebubianma = newRow["suopeidanweishiyebubianma"];
                        tempImportObj.Suopeidanweishiyebumingcheng = newRow["suopeidanweishiyebumingcheng"];
                        tempImportObj.WaichugonglishuErrorStr = newRow["waichugonglishu"];
                        tempImportObj.GaosugonglutuochefeiErrorStr = newRow["gaosugonglutuochefei"];
                        tempImportObj.Waidijiuyuandi = newRow["waidijiuyuandi"];
                        tempImportObj.Chepaihao = newRow["chepaihao"];
                        tempImportObj.Shendanleixing = newRow["shendanleixing"];
                        tempImportObj.Suopeigongyingshangbianhao = newRow["suopeigongyingshangbianhao"];
                        tempImportObj.Suopeigongyingshangmingcheng = newRow["suopeigongyingshangmingcheng"];
                        tempImportObj.ChuangjianshijianErrorStr = newRow["chuangjianshijian"];
                        tempImportObj.Kehudizhi = newRow["kehudizhi"];
                        tempImportObj.Weixiushuxing = newRow["weixiushuxing"];
                        tempImportObj.Waichusuopeibeizhuxinxi = newRow["waichusuopeibeizhuxinxi"];
                        tempImportObj.Fuwuzhanzhucezhenghao = newRow["fuwuzhanzhucezhenghao"];
                        tempImportObj.Fuwuzhanzhucemingcheng = newRow["fuwuzhanzhucemingcheng"];
                        tempImportObj.Fuwuzhanyouzhengbianma = newRow["fuwuzhanyouzhengbianma"];
                        tempImportObj.Fuwuzhangudingdianhua = newRow["fuwuzhangudingdianhua"];
                        tempImportObj.Fuwuzhanchuanzhen = newRow["fuwuzhanchuanzhen"];
                        tempImportObj.Fuwuzhanqiyeleixing = newRow["fuwuzhanqiyeleixing"];
                        tempImportObj.Fuwuzhanshengbianma = newRow["fuwuzhanshengbianma"];
                        tempImportObj.Fuwuzhanshengmingcheng = newRow["fuwuzhanshengmingcheng"];
                        tempImportObj.Fuwuzhanshibianma = newRow["fuwuzhanshibianma"];
                        tempImportObj.Fuwuzhanshimingcheng = newRow["fuwuzhanshimingcheng"];
                        tempImportObj.Fuwuzhanqubianma = newRow["fuwuzhanqubianma"];
                        tempImportObj.Fuwuzhanqumingcheng = newRow["fuwuzhanqumingcheng"];
                        tempImportObj.Fuwuzhandizhi = newRow["fuwuzhandizhi"];
                        tempImportObj.Fuwuzhanlianxiren = newRow["fuwuzhanlianxiren"];
                        tempImportObj.Fuwuzhandianziyouxiang = newRow["fuwuzhandianziyouxiang"];
                        tempImportObj.Fuwuzhanjingli = newRow["fuwuzhanjingli"];
                        tempImportObj.FuwuzhanchengliriqiErrorStr = newRow["fuwuzhanchengliriqi"];
                        tempImportObj.Fuwuzhanzhuangtai = newRow["fuwuzhanzhuangtai"];
                        tempImportObj.Erjifuwuzhanbianhao = newRow["erjifuwuzhanbianhao"];
                        tempImportObj.Erjifuwuzhanmingcheng = newRow["erjifuwuzhanmingcheng"];
                        tempImportObj.Erjifuwuzhangudingdianhua = newRow["erjifuwuzhangudingdianhua"];
                        tempImportObj.Erjifuwuzhanqiyeleixing = newRow["erjifuwuzhanqiyeleixing"];
                        tempImportObj.Erjifuwuzhandizhi = newRow["erjifuwuzhandizhi"];
                        tempImportObj.Erjifuwuzhandianziyouxiang = newRow["erjifuwuzhandianziyouxiang"];
                        tempImportObj.Erjifuwuzhanjingli = newRow["erjifuwuzhanjingli"];
                        tempImportObj.Erjifuwuzhanzhuangtai = newRow["erjifuwuzhanzhuangtai"];
                        #endregion

                        var tempErrorMessage = new List<string>();

                        #region 导入的数据基本检查

                        var fieldIndex = notNullableFields.IndexOf("SUOPEIDANBIANHAO");
                        if(string.IsNullOrEmpty(tempImportObj.Suopeidanbianhao)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add("索赔单编号不能为空");
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.Suopeidanbianhao) > fieldLenght["SUOPEIDANBIANHAO"])
                                tempErrorMessage.Add("索赔单编号过长");
                        }
                        fieldIndex = notNullableFields.IndexOf("VIN");
                        if(string.IsNullOrEmpty(tempImportObj.Vin)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add("VIN不能为空");
                        } else if(!string.IsNullOrEmpty(tempImportObj.Vin) && Encoding.Default.GetByteCount(tempImportObj.Vin) > fieldLenght["VIN"]) {
                            tempErrorMessage.Add("VIN过长");
                        }
                        DateTime outDateTime;
                        int outInt;
                        decimal outDecimal;
                        //时间转换
                        if(!string.IsNullOrEmpty(tempImportObj.GengxinshijianErrorStr) && !DateTime.TryParse(tempImportObj.GengxinshijianErrorStr, out outDateTime)) {
                            tempErrorMessage.Add("更新时间不能为空");
                        } else {
                            tempImportObj.Gengxinshijian = Convert.ToDateTime(tempImportObj.GengxinshijianErrorStr);
                        }

                        if(!string.IsNullOrEmpty(tempImportObj.ChuchangriqiErrorStr) && !DateTime.TryParse(tempImportObj.ChuchangriqiErrorStr, out outDateTime)) {
                            tempErrorMessage.Add("出厂日期不能为空");
                        } else {
                            tempImportObj.Chuchangriqi = Convert.ToDateTime(tempImportObj.ChuchangriqiErrorStr);
                        }

                        if(!string.IsNullOrEmpty(tempImportObj.GoumairiqiErrorStr) && !DateTime.TryParse(tempImportObj.GoumairiqiErrorStr, out outDateTime)) {
                            tempErrorMessage.Add("购买日期不能为空");
                        } else {
                            tempImportObj.Goumairiqi = Convert.ToDateTime(tempImportObj.GoumairiqiErrorStr);
                        }

                        if(!string.IsNullOrEmpty(tempImportObj.BaoxiushijianErrorStr) && !DateTime.TryParse(tempImportObj.BaoxiushijianErrorStr, out outDateTime)) {
                            tempErrorMessage.Add("更新时间不能为空");
                        } else {
                            tempImportObj.Baoxiushijian = Convert.ToDateTime(tempImportObj.BaoxiushijianErrorStr);
                        }

                        if(!string.IsNullOrEmpty(tempImportObj.ChuangjianriqiErrorStr) && !DateTime.TryParse(tempImportObj.ChuangjianriqiErrorStr, out outDateTime)) {
                            tempErrorMessage.Add("创建日期不能为空");
                        } else {
                            tempImportObj.Chuangjianriqi = Convert.ToDateTime(tempImportObj.ChuangjianriqiErrorStr);
                        }

                        if(!string.IsNullOrEmpty(tempImportObj.FengongsishenheshijianErrorStr) && !DateTime.TryParse(tempImportObj.FengongsishenheshijianErrorStr, out outDateTime)) {
                            tempErrorMessage.Add("分公司审核时间不能为空");
                        } else {
                            tempImportObj.Fengongsishenheshijian = Convert.ToDateTime(tempImportObj.FengongsishenheshijianErrorStr);
                        }

                        if(!string.IsNullOrEmpty(tempImportObj.ZhongshenshijianErrorStr) && !DateTime.TryParse(tempImportObj.ZhongshenshijianErrorStr, out outDateTime)) {
                            tempErrorMessage.Add("终审时间不能为空");
                        } else {
                            tempImportObj.Zhongshenshijian = Convert.ToDateTime(tempImportObj.ZhongshenshijianErrorStr);
                        }

                        if(!string.IsNullOrEmpty(tempImportObj.JiesuanshijianErrorStr) && !DateTime.TryParse(tempImportObj.JiesuanshijianErrorStr, out outDateTime)) {
                            tempErrorMessage.Add("结算时间不能为空");
                        } else {
                            tempImportObj.Jiesuanshijian = Convert.ToDateTime(tempImportObj.JiesuanshijianErrorStr);
                        }

                        if(!string.IsNullOrEmpty(tempImportObj.ChuangjianshijianErrorStr) && !DateTime.TryParse(tempImportObj.ChuangjianshijianErrorStr, out outDateTime)) {
                            tempErrorMessage.Add("创建时间不能为空");
                        } else {
                            tempImportObj.Chuangjianshijian = Convert.ToDateTime(tempImportObj.ChuangjianshijianErrorStr);
                        }

                        if(!string.IsNullOrEmpty(tempImportObj.FuwuzhanchengliriqiErrorStr) && !DateTime.TryParse(tempImportObj.FuwuzhanchengliriqiErrorStr, out outDateTime)) {
                            tempErrorMessage.Add("服务站成立日期不能为空");
                        } else {
                            tempImportObj.Fuwuzhanchengliriqi = Convert.ToDateTime(tempImportObj.FuwuzhanchengliriqiErrorStr);
                        }

                        if(!string.IsNullOrEmpty(tempImportObj.CreateTimeErrorStr) && !DateTime.TryParse(tempImportObj.CreateTimeErrorStr, out outDateTime)) {
                            tempErrorMessage.Add("系统创建时间不能为空");
                        } else {
                            tempImportObj.CreateTime = Convert.ToDateTime(tempImportObj.CreateTimeErrorStr);
                        }

                        if(!string.IsNullOrEmpty(tempImportObj.ModifyTimeErrorStr) && !DateTime.TryParse(tempImportObj.ModifyTimeErrorStr, out outDateTime)) {
                            tempErrorMessage.Add("系统修改时间不能为空");
                        } else {
                            tempImportObj.ModifyTime = Convert.ToDateTime(tempImportObj.ModifyTimeErrorStr);
                        }


                        if(!string.IsNullOrEmpty(tempImportObj.XingshilichengErrorStr) && !int.TryParse(tempImportObj.XingshilichengErrorStr, out outInt)) {
                            tempErrorMessage.Add("行驶里程类型错误");
                        } else {
                            tempImportObj.Xingshilicheng = Convert.ToInt32(tempImportObj.XingshilichengErrorStr);
                        }

                        if(!string.IsNullOrEmpty(tempImportObj.ShouciguzhanglichengErrorStr) && !int.TryParse(tempImportObj.ShouciguzhanglichengErrorStr, out outInt)) {
                            tempErrorMessage.Add("首次故障里程类型错误");
                        } else {
                            tempImportObj.Shouciguzhanglicheng = Convert.ToInt32(tempImportObj.ShouciguzhanglichengErrorStr);
                        }

                        if(!string.IsNullOrEmpty(tempImportObj.WaichurenshuErrorStr) && !int.TryParse(tempImportObj.WaichurenshuErrorStr, out outInt)) {
                            tempErrorMessage.Add("外出人数类型错误");
                        } else {
                            tempImportObj.Waichurenshu = Convert.ToInt32(tempImportObj.WaichurenshuErrorStr);
                        }

                        if(!string.IsNullOrEmpty(tempImportObj.WaichutianshuErrorStr) && !int.TryParse(tempImportObj.WaichutianshuErrorStr, out outInt)) {
                            tempErrorMessage.Add("外出天数类型错误");
                        } else {
                            tempImportObj.Waichutianshu = Convert.ToInt32(tempImportObj.WaichutianshuErrorStr);
                        }

                        if(!string.IsNullOrEmpty(tempImportObj.WaichulichengErrorStr) && !int.TryParse(tempImportObj.WaichulichengErrorStr, out outInt)) {
                            tempErrorMessage.Add("外出里程类型错误");
                        } else {
                            tempImportObj.Waichulicheng = Convert.ToInt32(tempImportObj.WaichulichengErrorStr);
                        }

                        if(!string.IsNullOrEmpty(tempImportObj.WaichuchefeidanjiaErrorStr) && !decimal.TryParse(tempImportObj.WaichuchefeidanjiaErrorStr, out outDecimal)) {
                            tempErrorMessage.Add("外出车费单价类型错误");
                        } else {
                            tempImportObj.Waichuchefeidanjia = Convert.ToDecimal(tempImportObj.WaichuchefeidanjiaErrorStr);
                        }

                        if(!string.IsNullOrEmpty(tempImportObj.WaichulutubuzhudanjiaErrorStr) && !decimal.TryParse(tempImportObj.WaichulutubuzhudanjiaErrorStr, out outDecimal)) {
                            tempErrorMessage.Add("外出路途补助单价类型错误");
                        } else {
                            tempImportObj.Waichulutubuzhudanjia = Convert.ToDecimal(tempImportObj.WaichulutubuzhudanjiaErrorStr);
                        }

                        if(!string.IsNullOrEmpty(tempImportObj.CailiaofeiErrorStr) && !decimal.TryParse(tempImportObj.CailiaofeiErrorStr, out outDecimal)) {
                            tempErrorMessage.Add("材料费类型错误");
                        } else {
                            tempImportObj.Cailiaofei = Convert.ToDecimal(tempImportObj.CailiaofeiErrorStr);
                        }

                        if(!string.IsNullOrEmpty(tempImportObj.GongshifeiErrorStr) && !decimal.TryParse(tempImportObj.GongshifeiErrorStr, out outDecimal)) {
                            tempErrorMessage.Add("工时费类型错误");
                        } else {
                            tempImportObj.Gongshifei = Convert.ToDecimal(tempImportObj.GongshifeiErrorStr);
                        }

                        if(!string.IsNullOrEmpty(tempImportObj.WaichufuwufeiErrorStr) && !decimal.TryParse(tempImportObj.WaichufuwufeiErrorStr, out outDecimal)) {
                            tempErrorMessage.Add("外出服务费类型错误");
                        } else {
                            tempImportObj.Waichufuwufei = Convert.ToDecimal(tempImportObj.WaichufuwufeiErrorStr);
                        }

                        if(!string.IsNullOrEmpty(tempImportObj.PeijianguanlifeiErrorStr) && !decimal.TryParse(tempImportObj.PeijianguanlifeiErrorStr, out outDecimal)) {
                            tempErrorMessage.Add("配件管理费类型错误");
                        } else {
                            tempImportObj.Peijianguanlifei = Convert.ToDecimal(tempImportObj.PeijianguanlifeiErrorStr);
                        }

                        if(!string.IsNullOrEmpty(tempImportObj.GuzhangjianqingtuiyunfeiErrorStr) && !decimal.TryParse(tempImportObj.GuzhangjianqingtuiyunfeiErrorStr, out outDecimal)) {
                            tempErrorMessage.Add("故障件清退运费类型错误");
                        } else {
                            tempImportObj.Guzhangjianqingtuiyunfei = Convert.ToDecimal(tempImportObj.GuzhangjianqingtuiyunfeiErrorStr);
                        }

                        if(!string.IsNullOrEmpty(tempImportObj.QitafeiyongErrorStr) && !decimal.TryParse(tempImportObj.QitafeiyongErrorStr, out outDecimal)) {
                            tempErrorMessage.Add("其他费用类型错误");
                        } else {
                            tempImportObj.Qitafeiyong = Convert.ToDecimal(tempImportObj.QitafeiyongErrorStr);
                        }

                        if(!string.IsNullOrEmpty(tempImportObj.FeiyonghejiErrorStr) && !decimal.TryParse(tempImportObj.FeiyonghejiErrorStr, out outDecimal)) {
                            tempErrorMessage.Add("费用合计类型错误");
                        } else {
                            tempImportObj.Feiyongheji = Convert.ToDecimal(tempImportObj.FeiyonghejiErrorStr);
                        }

                        if(!string.IsNullOrEmpty(tempImportObj.GongshidingeErrorStr) && !decimal.TryParse(tempImportObj.GongshidingeErrorStr, out outDecimal)) {
                            tempErrorMessage.Add("工时定额类型错误");
                        } else {
                            tempImportObj.Gongshidinge = Convert.ToDecimal(tempImportObj.GongshidingeErrorStr);
                        }

                        if(!string.IsNullOrEmpty(tempImportObj.GongshidanjiaErrorStr) && !decimal.TryParse(tempImportObj.GongshidanjiaErrorStr, out outDecimal)) {
                            tempErrorMessage.Add("工时单价类型错误");
                        } else {
                            tempImportObj.Gongshidanjia = Convert.ToDecimal(tempImportObj.GongshidanjiaErrorStr);
                        }

                        if(!string.IsNullOrEmpty(tempImportObj.ShejijineErrorStr) && !decimal.TryParse(tempImportObj.ShejijineErrorStr, out outDecimal)) {
                            tempErrorMessage.Add("涉及金额类型错误");
                        } else {
                            tempImportObj.Shejijine = Convert.ToDecimal(tempImportObj.ShejijineErrorStr);
                        }

                        if(!string.IsNullOrEmpty(tempImportObj.KoukuanjineErrorStr) && !decimal.TryParse(tempImportObj.KoukuanjineErrorStr, out outDecimal)) {
                            tempErrorMessage.Add("扣款金额类型错误");
                        } else {
                            tempImportObj.Koukuanjine = Convert.ToDecimal(tempImportObj.KoukuanjineErrorStr);
                        }

                        if(!string.IsNullOrEmpty(tempImportObj.BukuanjineErrorStr) && !decimal.TryParse(tempImportObj.BukuanjineErrorStr, out outDecimal)) {
                            tempErrorMessage.Add("补款金额类型错误");
                        } else {
                            tempImportObj.Bukuanjine = Convert.ToDecimal(tempImportObj.BukuanjineErrorStr);
                        }

                        if(!string.IsNullOrEmpty(tempImportObj.WaichugonglishuErrorStr) && !decimal.TryParse(tempImportObj.WaichugonglishuErrorStr, out outDecimal)) {
                            tempErrorMessage.Add("外出公里数类型错误");
                        } else {
                            tempImportObj.Waichugonglishu = Convert.ToInt32(tempImportObj.WaichugonglishuErrorStr);
                        }

                        if(!string.IsNullOrEmpty(tempImportObj.GaosugonglutuochefeiErrorStr) && !decimal.TryParse(tempImportObj.GaosugonglutuochefeiErrorStr, out outDecimal)) {
                            tempErrorMessage.Add("高速公路拖车费类型错误");
                        } else {
                            tempImportObj.Gaosugonglutuochefei = Convert.ToDecimal(tempImportObj.GaosugonglutuochefeiErrorStr);
                        }
                        #endregion

                        if(tempErrorMessage.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        } else {
                            var tempStatus = tempExcelOperator.ImportHelper.GetEnumValue("Status", ErrorStrings.Export_Validation_PartsBranch_Used);
                            if(tempStatus.HasValue) {
                                tempImportObj.Status = tempStatus.Value;
                            }
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });
                    //正确的数据
                    var tempRightList = allList.Where(r => string.IsNullOrEmpty(r.ErrorMsg)).ToList();

                }
                //获取所有不合格数据
                errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                //获取所有合格数据
                rightList = allList.Except(errorList).ToList();
                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempErrorObj = list[index - 1];
                            var values = new object[] {
                                tempErrorObj.GengxinshijianErrorStr ,
                                tempErrorObj.Fengongsibianhao ,
                                tempErrorObj.Fengongsimingcheng ,
                                tempErrorObj.Suopeidanbianhao ,
                                tempErrorObj.Danjuzhuangtai ,
                                tempErrorObj.Weixiuleixing ,
                                tempErrorObj.Vin ,
                                tempErrorObj.Chuchangbianhao ,
                                tempErrorObj.Pinpai ,
                                tempErrorObj.Zhengchechanpinxian ,
                                tempErrorObj.Fuwuchanpinxian ,
                                tempErrorObj.Chanpinxianleixing ,
                                tempErrorObj.ChuchangriqiErrorStr ,
                                tempErrorObj.GoumairiqiErrorStr ,
                                tempErrorObj.XingshilichengErrorStr ,
                                tempErrorObj.ShouciguzhanglichengErrorStr ,
                                tempErrorObj.Gongkuangleibie ,
                                tempErrorObj.Qudongxingshi ,
                                tempErrorObj.Chexing ,
                                tempErrorObj.Gonggaohao ,
                                tempErrorObj.Fadongjixinghao ,
                                tempErrorObj.Fadongjibianhao ,
                                tempErrorObj.Shichangbu ,
                                tempErrorObj.Fuwuzhanbianhao ,
                                tempErrorObj.Yewubianhao ,
                                tempErrorObj.Fuwuzhanmingcheng ,
                                tempErrorObj.Chelianglianxiren ,
                                tempErrorObj.Chanpinbianhao ,
                                tempErrorObj.BaoxiushijianErrorStr ,
                                tempErrorObj.ChuangjianriqiErrorStr ,
                                tempErrorObj.Guzhangdaima ,
                                tempErrorObj.Guzhangxianxiangmiaoshu ,
                                tempErrorObj.Guzhangmoshi ,
                                tempErrorObj.Huoshoujiantuhao ,
                                tempErrorObj.Huoshoujianmingcheng ,
                                tempErrorObj.Huoshoujianshengchanchangjia ,
                                tempErrorObj.Zerendanweibianma ,
                                tempErrorObj.Zerendanweimingcheng ,
                                tempErrorObj.Huoshoujiansuoshuzongcheng ,
                                tempErrorObj.Peijiansuoshuzongcheng ,
                                tempErrorObj.Gongyingshangquerenzhuangtai ,
                                tempErrorObj.FengongsishenheshijianErrorStr ,
                                tempErrorObj.Zhongshenyijian ,
                                tempErrorObj.ZhongshenshijianErrorStr ,
                                tempErrorObj.Zhongshenren ,
                                tempErrorObj.Gongyingshangquerenyijian ,
                                tempErrorObj.Iszerengongyingshangsuopei ,
                                tempErrorObj.Weixiusuopeishenqingdanhao ,
                                tempErrorObj.Weixiusuopeishenqingleixing ,
                                tempErrorObj.Waichusuopeidanbianhao ,
                                tempErrorObj.Waichusuopeidanzhuangtai ,
                                tempErrorObj.Waichusuopeishenqingdanhao ,
                                tempErrorObj.WaichurenshuErrorStr ,
                                tempErrorObj.WaichutianshuErrorStr ,
                                tempErrorObj.WaichulichengErrorStr ,
                                tempErrorObj.WaichuchefeidanjiaErrorStr ,
                                tempErrorObj.WaichulutubuzhudanjiaErrorStr ,
                                tempErrorObj.Waichuleixing ,
                                tempErrorObj.Waichuyuanyin ,
                                tempErrorObj.Waichushenheyijian ,
                                tempErrorObj.Suopeidanqitafeiyongshuoming ,
                                tempErrorObj.Waichuqitafeiyongshuoming ,
                                tempErrorObj.Guzhangbeizhu ,
                                tempErrorObj.Jiesuanzhuangtai ,
                                tempErrorObj.JiesuanshijianErrorStr ,
                                tempErrorObj.CailiaofeiErrorStr ,
                                tempErrorObj.GongshifeiErrorStr ,
                                tempErrorObj.WaichufuwufeiErrorStr ,
                                tempErrorObj.PeijianguanlifeiErrorStr ,
                                tempErrorObj.GuzhangjianqingtuiyunfeiErrorStr ,
                                tempErrorObj.QitafeiyongErrorStr ,
                                tempErrorObj.FeiyonghejiErrorStr ,
                                tempErrorObj.GongshidingeErrorStr ,
                                tempErrorObj.GongshidanjiaErrorStr ,
                                tempErrorObj.Fuwuhuodongbianhao ,
                                tempErrorObj.Fuwuhuodongmingcheng ,
                                tempErrorObj.Fuwuhuodongleixing ,
                                tempErrorObj.Fuwuhuodongneirong ,
                                tempErrorObj.Kehuleixing ,
                                tempErrorObj.Feiyongfenlei ,
                                tempErrorObj.Wenjianbianhao ,
                                tempErrorObj.Baogaojianshu ,
                                tempErrorObj.Shichangbuyijian ,
                                tempErrorObj.Pizhunren ,
                                tempErrorObj.ShejijineErrorStr ,
                                tempErrorObj.Zhiliangbuyijian ,
                                tempErrorObj.Kuaibaodaihao ,
                                tempErrorObj.Shejicheliang ,
                                tempErrorObj.Wentimiaoshu ,
                                tempErrorObj.Zhenggaiweixiufangan ,
                                tempErrorObj.Suopeidanwei ,
                                tempErrorObj.Isshiyebusuopei ,
                                tempErrorObj.Zongchengjianmingcheng ,
                                tempErrorObj.ShichangABkuaibaobianhao ,
                                tempErrorObj.Weixiusuopeidanbianhao ,
                                tempErrorObj.Zhuangtai ,
                                tempErrorObj.Lianxidianhua ,
                                tempErrorObj.KoukuanjineErrorStr ,
                                tempErrorObj.BukuanjineErrorStr ,
                                tempErrorObj.Suopeidanweishiyebubianma ,
                                tempErrorObj.Suopeidanweishiyebumingcheng ,
                                tempErrorObj.WaichugonglishuErrorStr ,
                                tempErrorObj.GaosugonglutuochefeiErrorStr ,
                                tempErrorObj.Waidijiuyuandi ,
                                tempErrorObj.Chepaihao ,
                                tempErrorObj.Shendanleixing ,
                                tempErrorObj.Suopeigongyingshangbianhao ,
                                tempErrorObj.Suopeigongyingshangmingcheng ,
                                tempErrorObj.ChuangjianshijianErrorStr ,
                                tempErrorObj.Kehudizhi ,
                                tempErrorObj.Weixiushuxing ,
                                tempErrorObj.Waichusuopeibeizhuxinxi ,
                                tempErrorObj.Fuwuzhanzhucezhenghao ,
                                tempErrorObj.Fuwuzhanzhucemingcheng ,
                                tempErrorObj.Fuwuzhanyouzhengbianma ,
                                tempErrorObj.Fuwuzhangudingdianhua ,
                                tempErrorObj.Fuwuzhanchuanzhen ,
                                tempErrorObj.Fuwuzhanqiyeleixing ,
                                tempErrorObj.Fuwuzhanshengbianma ,
                                tempErrorObj.Fuwuzhanshengmingcheng ,
                                tempErrorObj.Fuwuzhanshibianma ,
                                tempErrorObj.Fuwuzhanshimingcheng ,
                                tempErrorObj.Fuwuzhanqubianma ,
                                tempErrorObj.Fuwuzhanqumingcheng ,
                                tempErrorObj.Fuwuzhandizhi ,
                                tempErrorObj.Fuwuzhanlianxiren ,
                                tempErrorObj.Fuwuzhandianziyouxiang ,
                                tempErrorObj.Fuwuzhanjingli ,
                                tempErrorObj.FuwuzhanchengliriqiErrorStr ,
                                tempErrorObj.Fuwuzhanzhuangtai ,
                                tempErrorObj.Erjifuwuzhanbianhao ,
                                tempErrorObj.Erjifuwuzhanmingcheng ,
                                tempErrorObj.Erjifuwuzhangudingdianhua ,
                                tempErrorObj.Erjifuwuzhanqiyeleixing ,
                                tempErrorObj.Erjifuwuzhandizhi ,
                                tempErrorObj.Erjifuwuzhandianziyouxiang ,
                                tempErrorObj.Erjifuwuzhanjingli ,
                                tempErrorObj.Erjifuwuzhanzhuangtai,
                                tempErrorObj.ErrorMsg
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }

                //导入所有合格数据
                if(!rightList.Any())
                    return true;
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    //开启事务，新增更新配件在一个事务内
                    var ts = conn.BeginTransaction();
                    try {
                        //新增配件
                        if(rightList.Any()) {
                            //获取新增数据的sql语句，Id为主键
                            var sqlInsert = db.GetInsertSql("FdWarrantyMessage", "Id", new[] {
                                "Gengxinshijian",
                                "Fengongsibianhao",
                                "Fengongsimingcheng",
                                "Suopeidanbianhao",
                                "Danjuzhuangtai",
                                "Weixiuleixing",
                                "Vin",
                                "Chuchangbianhao",
                                "Pinpai",
                                "Zhengchechanpinxian",
                                "Fuwuchanpinxian",
                                "Chanpinxianleixing",
                                "Chuchangriqi",
                                "Goumairiqi",
                                "Xingshilicheng",
                                "Shouciguzhanglicheng",
                                "Gongkuangleibie",
                                "Qudongxingshi",
                                "Chexing",
                                "Gonggaohao",
                                "Fadongjixinghao",
                                "Fadongjibianhao",
                                "Shichangbu",
                                "Fuwuzhanbianhao",
                                "Yewubianhao",
                                "Fuwuzhanmingcheng",
                                "Chelianglianxiren",
                                "Chanpinbianhao",
                                "Baoxiushijian",
                                "Chuangjianriqi",
                                "Guzhangdaima",
                                "Guzhangxianxiangmiaoshu",
                                "Guzhangmoshi",
                                "Huoshoujiantuhao",
                                "Huoshoujianmingcheng",
                                "Huoshoujianshengchanchangjia",
                                "Zerendanweibianma",
                                "Zerendanweimingcheng",
                                "Huoshoujiansuoshuzongcheng",
                                "Peijiansuoshuzongcheng",
                                "Gongyingshangquerenzhuangtai",
                                "Fengongsishenheshijian",
                                "Zhongshenyijian",
                                "Zhongshenshijian",
                                "Zhongshenren",
                                "Gongyingshangquerenyijian",
                                "Iszerengongyingshangsuopei",
                                "Weixiusuopeishenqingdanhao",
                                "Weixiusuopeishenqingleixing",
                                "Waichusuopeidanbianhao",
                                "Waichusuopeidanzhuangtai",
                                "Waichusuopeishenqingdanhao",
                                "Waichurenshu",
                                "Waichutianshu",
                                "Waichulicheng",
                                "Waichuchefeidanjia",
                                "Waichulutubuzhudanjia",
                                "Waichuleixing",
                                "Waichuyuanyin",
                                "Waichushenheyijian",
                                "Suopeidanqitafeiyongshuoming",
                                "Waichuqitafeiyongshuoming",
                                "Guzhangbeizhu",
                                "Jiesuanzhuangtai",
                                "Jiesuanshijian",
                                "Cailiaofei",
                                "Gongshifei",
                                "Waichufuwufei",
                                "Peijianguanlifei",
                                "Guzhangjianqingtuiyunfei",
                                "Qitafeiyong",
                                "Feiyongheji",
                                "Gongshidinge",
                                "Gongshidanjia",
                                "Fuwuhuodongbianhao",
                                "Fuwuhuodongmingcheng",
                                "Fuwuhuodongleixing",
                                "Fuwuhuodongneirong",
                                "Kehuleixing",
                                "Feiyongfenlei",
                                "Wenjianbianhao",
                                "Baogaojianshu",
                                "Shichangbuyijian",
                                "Pizhunren",
                                "Shejijine",
                                "Zhiliangbuyijian",
                                "Kuaibaodaihao",
                                "Shejicheliang",
                                "Wentimiaoshu",
                                "Zhenggaiweixiufangan",
                                "Suopeidanwei",
                                "Isshiyebusuopei",
                                "Zongchengjianmingcheng",
                                "ShichangABkuaibaobianhao",
                                "Weixiusuopeidanbianhao",
                                "Zhuangtai",
                                "Lianxidianhua",
                                "Koukuanjine",
                                "Bukuanjine",
                                "Suopeidanweishiyebubianma",
                                "Suopeidanweishiyebumingcheng",
                                "Waichugonglishu",
                                "Gaosugonglutuochefei",
                                "Waidijiuyuandi",
                                "Chepaihao",
                                "Shendanleixing",
                                "Suopeigongyingshangbianhao",
                                "Suopeigongyingshangmingcheng",
                                "Chuangjianshijian",
                                "Kehudizhi",
                                "Weixiushuxing",
                                "Waichusuopeibeizhuxinxi",
                                "Fuwuzhanzhucezhenghao",
                                "Fuwuzhanzhucemingcheng",
                                "Fuwuzhanyouzhengbianma",
                                "Fuwuzhangudingdianhua",
                                "Fuwuzhanchuanzhen",
                                "Fuwuzhanqiyeleixing",
                                "Fuwuzhanshengbianma",
                                "Fuwuzhanshengmingcheng",
                                "Fuwuzhanshibianma",
                                "Fuwuzhanshimingcheng",
                                "Fuwuzhanqubianma",
                                "Fuwuzhanqumingcheng",
                                "Fuwuzhandizhi",
                                "Fuwuzhanlianxiren",
                                "Fuwuzhandianziyouxiang",
                                "Fuwuzhanjingli",
                                "Fuwuzhanchengliriqi",
                                "Fuwuzhanzhuangtai",
                                "Erjifuwuzhanbianhao",
                                "Erjifuwuzhanmingcheng",
                                "Erjifuwuzhangudingdianhua",
                                "Erjifuwuzhanqiyeleixing",
                                "Erjifuwuzhandizhi",
                                "Erjifuwuzhandianziyouxiang",
                                "Erjifuwuzhanjingli",
                                "Erjifuwuzhanzhuangtai",
                                "Status",
                                "CreatorId",
                                "CreatorName",
                                "CreateTime"
                            });
                            var userInfo = Utils.GetCurrentUserInfo();
                            //往数据库增加祸首件所属总成
                            foreach(var item in rightList) {
                                var command = db.CreateDbCommand(sqlInsert, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("Gengxinshijian", item.Gengxinshijian));
                                command.Parameters.Add(db.CreateDbParameter("Fengongsibianhao", item.Fengongsibianhao));
                                command.Parameters.Add(db.CreateDbParameter("Fengongsimingcheng", item.Fengongsimingcheng));
                                command.Parameters.Add(db.CreateDbParameter("Suopeidanbianhao", item.Suopeidanbianhao));
                                command.Parameters.Add(db.CreateDbParameter("Danjuzhuangtai", item.Danjuzhuangtai));
                                command.Parameters.Add(db.CreateDbParameter("Weixiuleixing", item.Weixiuleixing));
                                command.Parameters.Add(db.CreateDbParameter("Vin", item.Vin));
                                command.Parameters.Add(db.CreateDbParameter("Chuchangbianhao", item.Chuchangbianhao));
                                command.Parameters.Add(db.CreateDbParameter("Pinpai", item.Pinpai));
                                command.Parameters.Add(db.CreateDbParameter("Zhengchechanpinxian", item.Zhengchechanpinxian));
                                command.Parameters.Add(db.CreateDbParameter("Fuwuchanpinxian", item.Fuwuchanpinxian));
                                command.Parameters.Add(db.CreateDbParameter("Chanpinxianleixing", item.Chanpinxianleixing));
                                command.Parameters.Add(db.CreateDbParameter("Chuchangriqi", item.Chuchangriqi));
                                command.Parameters.Add(db.CreateDbParameter("Goumairiqi", item.Goumairiqi));
                                command.Parameters.Add(db.CreateDbParameter("Xingshilicheng", item.Xingshilicheng));
                                command.Parameters.Add(db.CreateDbParameter("Shouciguzhanglicheng", item.Shouciguzhanglicheng));
                                command.Parameters.Add(db.CreateDbParameter("Gongkuangleibie", item.Gongkuangleibie));
                                command.Parameters.Add(db.CreateDbParameter("Qudongxingshi", item.Qudongxingshi));
                                command.Parameters.Add(db.CreateDbParameter("Chexing", item.Chexing));
                                command.Parameters.Add(db.CreateDbParameter("Gonggaohao", item.Gonggaohao));
                                command.Parameters.Add(db.CreateDbParameter("Fadongjixinghao", item.Fadongjixinghao));
                                command.Parameters.Add(db.CreateDbParameter("Fadongjibianhao", item.Fadongjibianhao));
                                command.Parameters.Add(db.CreateDbParameter("Shichangbu", item.Shichangbu));
                                command.Parameters.Add(db.CreateDbParameter("Fuwuzhanbianhao", item.Fuwuzhanbianhao));
                                command.Parameters.Add(db.CreateDbParameter("Yewubianhao", item.Yewubianhao));
                                command.Parameters.Add(db.CreateDbParameter("Fuwuzhanmingcheng", item.Fuwuzhanmingcheng));
                                command.Parameters.Add(db.CreateDbParameter("Chelianglianxiren", item.Chelianglianxiren));
                                command.Parameters.Add(db.CreateDbParameter("Chanpinbianhao", item.Chanpinbianhao));
                                command.Parameters.Add(db.CreateDbParameter("Baoxiushijian", item.Baoxiushijian));
                                command.Parameters.Add(db.CreateDbParameter("Chuangjianriqi", item.Chuangjianriqi));
                                command.Parameters.Add(db.CreateDbParameter("Guzhangdaima", item.Guzhangdaima));
                                command.Parameters.Add(db.CreateDbParameter("Guzhangxianxiangmiaoshu", item.Guzhangxianxiangmiaoshu));
                                command.Parameters.Add(db.CreateDbParameter("Guzhangmoshi", item.Guzhangmoshi));
                                command.Parameters.Add(db.CreateDbParameter("Huoshoujiantuhao", item.Huoshoujiantuhao));
                                command.Parameters.Add(db.CreateDbParameter("Huoshoujianmingcheng", item.Huoshoujianmingcheng));
                                command.Parameters.Add(db.CreateDbParameter("Huoshoujianshengchanchangjia", item.Huoshoujianshengchanchangjia));
                                command.Parameters.Add(db.CreateDbParameter("Zerendanweibianma", item.Zerendanweibianma));
                                command.Parameters.Add(db.CreateDbParameter("Zerendanweimingcheng", item.Zerendanweimingcheng));
                                command.Parameters.Add(db.CreateDbParameter("Huoshoujiansuoshuzongcheng", item.Huoshoujiansuoshuzongcheng));
                                command.Parameters.Add(db.CreateDbParameter("Peijiansuoshuzongcheng", item.Peijiansuoshuzongcheng));
                                command.Parameters.Add(db.CreateDbParameter("Gongyingshangquerenzhuangtai", item.Gongyingshangquerenzhuangtai));
                                command.Parameters.Add(db.CreateDbParameter("Fengongsishenheshijian", item.Fengongsishenheshijian));
                                command.Parameters.Add(db.CreateDbParameter("Zhongshenyijian", item.Zhongshenyijian));
                                command.Parameters.Add(db.CreateDbParameter("Zhongshenshijian", item.Zhongshenshijian));
                                command.Parameters.Add(db.CreateDbParameter("Zhongshenren", item.Zhongshenren));
                                command.Parameters.Add(db.CreateDbParameter("Gongyingshangquerenyijian", item.Gongyingshangquerenyijian));
                                command.Parameters.Add(db.CreateDbParameter("Iszerengongyingshangsuopei", item.Iszerengongyingshangsuopei));
                                command.Parameters.Add(db.CreateDbParameter("Weixiusuopeishenqingdanhao", item.Weixiusuopeishenqingdanhao));
                                command.Parameters.Add(db.CreateDbParameter("Weixiusuopeishenqingleixing", item.Weixiusuopeishenqingleixing));
                                command.Parameters.Add(db.CreateDbParameter("Waichusuopeidanbianhao", item.Waichusuopeidanbianhao));
                                command.Parameters.Add(db.CreateDbParameter("Waichusuopeidanzhuangtai", item.Waichusuopeidanzhuangtai));
                                command.Parameters.Add(db.CreateDbParameter("Waichusuopeishenqingdanhao", item.Waichusuopeishenqingdanhao));
                                command.Parameters.Add(db.CreateDbParameter("Waichurenshu", item.Waichurenshu));
                                command.Parameters.Add(db.CreateDbParameter("Waichutianshu", item.Waichutianshu));
                                command.Parameters.Add(db.CreateDbParameter("Waichulicheng", item.Waichulicheng));
                                command.Parameters.Add(db.CreateDbParameter("Waichuchefeidanjia", item.Waichuchefeidanjia));
                                command.Parameters.Add(db.CreateDbParameter("Waichulutubuzhudanjia", item.Waichulutubuzhudanjia));
                                command.Parameters.Add(db.CreateDbParameter("Waichuleixing", item.Waichuleixing));
                                command.Parameters.Add(db.CreateDbParameter("Waichuyuanyin", item.Waichuyuanyin));
                                command.Parameters.Add(db.CreateDbParameter("Waichushenheyijian", item.Waichushenheyijian));
                                command.Parameters.Add(db.CreateDbParameter("Suopeidanqitafeiyongshuoming", item.Suopeidanqitafeiyongshuoming));
                                command.Parameters.Add(db.CreateDbParameter("Waichuqitafeiyongshuoming", item.Waichuqitafeiyongshuoming));
                                command.Parameters.Add(db.CreateDbParameter("Guzhangbeizhu", item.Guzhangbeizhu));
                                command.Parameters.Add(db.CreateDbParameter("Jiesuanzhuangtai", item.Jiesuanzhuangtai));
                                command.Parameters.Add(db.CreateDbParameter("Jiesuanshijian", item.Jiesuanshijian));
                                command.Parameters.Add(db.CreateDbParameter("Cailiaofei", item.Cailiaofei));
                                command.Parameters.Add(db.CreateDbParameter("Gongshifei", item.Gongshifei));
                                command.Parameters.Add(db.CreateDbParameter("Waichufuwufei", item.Waichufuwufei));
                                command.Parameters.Add(db.CreateDbParameter("Peijianguanlifei", item.Peijianguanlifei));
                                command.Parameters.Add(db.CreateDbParameter("Guzhangjianqingtuiyunfei", item.Guzhangjianqingtuiyunfei));
                                command.Parameters.Add(db.CreateDbParameter("Qitafeiyong", item.Qitafeiyong));
                                command.Parameters.Add(db.CreateDbParameter("Feiyongheji", item.Feiyongheji));
                                command.Parameters.Add(db.CreateDbParameter("Gongshidinge", item.Gongshidinge));
                                command.Parameters.Add(db.CreateDbParameter("Gongshidanjia", item.Gongshidanjia));
                                command.Parameters.Add(db.CreateDbParameter("Fuwuhuodongbianhao", item.Fuwuhuodongbianhao));
                                command.Parameters.Add(db.CreateDbParameter("Fuwuhuodongmingcheng", item.Fuwuhuodongmingcheng));
                                command.Parameters.Add(db.CreateDbParameter("Fuwuhuodongleixing", item.Fuwuhuodongleixing));
                                command.Parameters.Add(db.CreateDbParameter("Fuwuhuodongneirong", item.Fuwuhuodongneirong));
                                command.Parameters.Add(db.CreateDbParameter("Kehuleixing", item.Kehuleixing));
                                command.Parameters.Add(db.CreateDbParameter("Feiyongfenlei", item.Feiyongfenlei));
                                command.Parameters.Add(db.CreateDbParameter("Wenjianbianhao", item.Wenjianbianhao));
                                command.Parameters.Add(db.CreateDbParameter("Baogaojianshu", item.Baogaojianshu));
                                command.Parameters.Add(db.CreateDbParameter("Shichangbuyijian", item.Shichangbuyijian));
                                command.Parameters.Add(db.CreateDbParameter("Pizhunren", item.Pizhunren));
                                command.Parameters.Add(db.CreateDbParameter("Shejijine", item.Shejijine));
                                command.Parameters.Add(db.CreateDbParameter("Zhiliangbuyijian", item.Zhiliangbuyijian));
                                command.Parameters.Add(db.CreateDbParameter("Kuaibaodaihao", item.Kuaibaodaihao));
                                command.Parameters.Add(db.CreateDbParameter("Shejicheliang", item.Shejicheliang));
                                command.Parameters.Add(db.CreateDbParameter("Wentimiaoshu", item.Wentimiaoshu));
                                command.Parameters.Add(db.CreateDbParameter("Zhenggaiweixiufangan", item.Zhenggaiweixiufangan));
                                command.Parameters.Add(db.CreateDbParameter("Suopeidanwei", item.Suopeidanwei));
                                command.Parameters.Add(db.CreateDbParameter("Isshiyebusuopei", item.Isshiyebusuopei));
                                command.Parameters.Add(db.CreateDbParameter("Zongchengjianmingcheng", item.Zongchengjianmingcheng));
                                command.Parameters.Add(db.CreateDbParameter("ShichangABkuaibaobianhao", item.ShichangABkuaibaobianhao));
                                command.Parameters.Add(db.CreateDbParameter("Weixiusuopeidanbianhao", item.Weixiusuopeidanbianhao));
                                command.Parameters.Add(db.CreateDbParameter("Zhuangtai", item.Zhuangtai));
                                command.Parameters.Add(db.CreateDbParameter("Lianxidianhua", item.Lianxidianhua));
                                command.Parameters.Add(db.CreateDbParameter("Koukuanjine", item.Koukuanjine));
                                command.Parameters.Add(db.CreateDbParameter("Bukuanjine", item.Bukuanjine));
                                command.Parameters.Add(db.CreateDbParameter("Suopeidanweishiyebubianma", item.Suopeidanweishiyebubianma));
                                command.Parameters.Add(db.CreateDbParameter("Suopeidanweishiyebumingcheng", item.Suopeidanweishiyebumingcheng));
                                command.Parameters.Add(db.CreateDbParameter("Waichugonglishu", item.Waichugonglishu));
                                command.Parameters.Add(db.CreateDbParameter("Gaosugonglutuochefei", item.Gaosugonglutuochefei));
                                command.Parameters.Add(db.CreateDbParameter("Waidijiuyuandi", item.Waidijiuyuandi));
                                command.Parameters.Add(db.CreateDbParameter("Chepaihao", item.Chepaihao));
                                command.Parameters.Add(db.CreateDbParameter("Shendanleixing", item.Shendanleixing));
                                command.Parameters.Add(db.CreateDbParameter("Suopeigongyingshangbianhao", item.Suopeigongyingshangbianhao));
                                command.Parameters.Add(db.CreateDbParameter("Suopeigongyingshangmingcheng", item.Suopeigongyingshangmingcheng));
                                command.Parameters.Add(db.CreateDbParameter("Chuangjianshijian", item.Chuangjianshijian));
                                command.Parameters.Add(db.CreateDbParameter("Kehudizhi", item.Kehudizhi));
                                command.Parameters.Add(db.CreateDbParameter("Weixiushuxing", item.Weixiushuxing));
                                command.Parameters.Add(db.CreateDbParameter("Waichusuopeibeizhuxinxi", item.Waichusuopeibeizhuxinxi));
                                command.Parameters.Add(db.CreateDbParameter("Fuwuzhanzhucezhenghao", item.Fuwuzhanzhucezhenghao));
                                command.Parameters.Add(db.CreateDbParameter("Fuwuzhanzhucemingcheng", item.Fuwuzhanzhucemingcheng));
                                command.Parameters.Add(db.CreateDbParameter("Fuwuzhanyouzhengbianma", item.Fuwuzhanyouzhengbianma));
                                command.Parameters.Add(db.CreateDbParameter("Fuwuzhangudingdianhua", item.Fuwuzhangudingdianhua));
                                command.Parameters.Add(db.CreateDbParameter("Fuwuzhanchuanzhen", item.Fuwuzhanchuanzhen));
                                command.Parameters.Add(db.CreateDbParameter("Fuwuzhanqiyeleixing", item.Fuwuzhanqiyeleixing));
                                command.Parameters.Add(db.CreateDbParameter("Fuwuzhanshengbianma", item.Fuwuzhanshengbianma));
                                command.Parameters.Add(db.CreateDbParameter("Fuwuzhanshengmingcheng", item.Fuwuzhanshengmingcheng));
                                command.Parameters.Add(db.CreateDbParameter("Fuwuzhanshibianma", item.Fuwuzhanshibianma));
                                command.Parameters.Add(db.CreateDbParameter("Fuwuzhanshimingcheng", item.Fuwuzhanshimingcheng));
                                command.Parameters.Add(db.CreateDbParameter("Fuwuzhanqubianma", item.Fuwuzhanqubianma));
                                command.Parameters.Add(db.CreateDbParameter("Fuwuzhanqumingcheng", item.Fuwuzhanqumingcheng));
                                command.Parameters.Add(db.CreateDbParameter("Fuwuzhandizhi", item.Fuwuzhandizhi));
                                command.Parameters.Add(db.CreateDbParameter("Fuwuzhanlianxiren", item.Fuwuzhanlianxiren));
                                command.Parameters.Add(db.CreateDbParameter("Fuwuzhandianziyouxiang", item.Fuwuzhandianziyouxiang));
                                command.Parameters.Add(db.CreateDbParameter("Fuwuzhanjingli", item.Fuwuzhanjingli));
                                command.Parameters.Add(db.CreateDbParameter("Fuwuzhanchengliriqi", item.Fuwuzhanchengliriqi));
                                command.Parameters.Add(db.CreateDbParameter("Fuwuzhanzhuangtai", item.Fuwuzhanzhuangtai));
                                command.Parameters.Add(db.CreateDbParameter("Erjifuwuzhanbianhao", item.Erjifuwuzhanbianhao));
                                command.Parameters.Add(db.CreateDbParameter("Erjifuwuzhanmingcheng", item.Erjifuwuzhanmingcheng));
                                command.Parameters.Add(db.CreateDbParameter("Erjifuwuzhangudingdianhua", item.Erjifuwuzhangudingdianhua));
                                command.Parameters.Add(db.CreateDbParameter("Erjifuwuzhanqiyeleixing", item.Erjifuwuzhanqiyeleixing));
                                command.Parameters.Add(db.CreateDbParameter("Erjifuwuzhandizhi", item.Erjifuwuzhandizhi));
                                command.Parameters.Add(db.CreateDbParameter("Erjifuwuzhandianziyouxiang", item.Erjifuwuzhandianziyouxiang));
                                command.Parameters.Add(db.CreateDbParameter("Erjifuwuzhanjingli", item.Erjifuwuzhanjingli));
                                command.Parameters.Add(db.CreateDbParameter("Erjifuwuzhanzhuangtai", item.Erjifuwuzhanzhuangtai));

                                command.Parameters.Add(db.CreateDbParameter("Status", (int)DcsBaseDataStatus.有效));
                                command.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                command.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                command.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                command.ExecuteNonQuery();
                            }

                        }
                        //无异常提交
                        ts.Commit();
                    } catch(Exception ex) {
                        //报错回滚
                        ts.Rollback();
                        throw new Exception(ex.Message);
                    } finally {
                        if(conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
            }
        }
    }
}
