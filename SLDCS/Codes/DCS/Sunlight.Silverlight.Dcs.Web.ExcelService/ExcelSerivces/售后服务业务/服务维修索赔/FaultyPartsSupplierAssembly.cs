﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using Microsoft.VisualBasic;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        /// <summary>
        /// 导入祸首件所属总成
        /// </summary>
        public bool ImportFaultyPartsSupplierAssembly(string fileName, out int excelImportNum, out List<FaultyPartsSupplierAssemblyExtend> rightData, out List<FaultyPartsSupplierAssemblyExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<FaultyPartsSupplierAssemblyExtend>();
            var rightList = new List<FaultyPartsSupplierAssemblyExtend>();
            var allList = new List<FaultyPartsSupplierAssemblyExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("FaultyPartsSupplierAssembly", out notNullableFields, out fieldLenght);

                List<object> excelColumns;

                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    #region 指定文件中的列对应的名称 例如 excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode, "PartCode");

                    excelOperator.AddColumnDataSource("祸首件所属总成编号", "Code");
                    excelOperator.AddColumnDataSource("祸首件所属总成名称", "Name");
                    #endregion

                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;

                    #region 获取对应枚举
                    var keyValuePairs = new[] {
                            new KeyValuePair<string, string>("Status","BaseData_Status")
                        };
                    tempExcelOperator.ImportHelper.LoadEnumFromDb(null, null, keyValuePairs);

                    #endregion

                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var tempImportObj = new FaultyPartsSupplierAssemblyExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        #region
                        tempImportObj.CodeStr = newRow["Code"];
                        tempImportObj.Code = newRow["Code"];
                        tempImportObj.NameStr = newRow["Name"];
                        tempImportObj.Name = newRow["Name"];
                        #endregion

                        var tempErrorMessage = new List<string>();

                        #region 导入的数据基本检查

                        var fieldIndex = notNullableFields.IndexOf("CODE");
                        if(string.IsNullOrEmpty(tempImportObj.CodeStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add("祸首件所属总成编号不能为空");
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.CodeStr) > fieldLenght["CODE"])
                                tempErrorMessage.Add("祸首件所属总成编号过长");
                        }
                        fieldIndex = notNullableFields.IndexOf("NAME");
                        if(!string.IsNullOrEmpty(tempImportObj.NameStr)) {
                            if(Encoding.Default.GetByteCount(tempImportObj.NameStr) > fieldLenght["NAME"])
                                tempErrorMessage.Add("祸首件所属总成名称过长");
                        }
                        #endregion

                        if(tempErrorMessage.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        } else {
                            var tempStatus = tempExcelOperator.ImportHelper.GetEnumValue("Status", ErrorStrings.Export_Validation_PartsBranch_Used);
                            if(tempStatus.HasValue) {
                                tempImportObj.Status = tempStatus.Value;
                            }
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });
                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    #region 剩下的数据进行业务检查
                    //校验导入的数据 编号重复
                    var groups = allList.GroupBy(r =>
                        r.Code
                    ).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        if(groupItem.ErrorMsg == null) {
                            groupItem.ErrorMsg = "祸首件所属总成编号" + groupItem.Code + ErrorStrings.Export_Validation_PartsBranch_Validation5;
                        } else {
                            groupItem.ErrorMsg = groupItem.ErrorMsg + ";祸首件所属总成编号" + groupItem.Code + ErrorStrings.Export_Validation_PartsBranch_Validation5;
                        }
                    }
                    var faultyPartsSupplierAssemblysNeedCheck = allList.ToList();
                    var codesNeedCheck = faultyPartsSupplierAssemblysNeedCheck.Select(r => r.Code.ToUpper()).Distinct().ToArray();
                    var dbCodes = new List<string>();
                    Func<string[], bool> getDbCodes = vales => {
                        dbCodes.Add(vales[0]);
                        return false;
                    };
                    db.QueryDataWithInOperator("select Code from FaultyPartsSupplierAssembly where status!=99 ", "Code", true, codesNeedCheck, getDbCodes);
                    if(codesNeedCheck.Length > 0) {
                        var faultyPartsSupplierAssemblysExistsCode = faultyPartsSupplierAssemblysNeedCheck.Where(r => dbCodes.Any(v => v.ToUpper() == r.Code)).ToArray();
                        foreach(var item in faultyPartsSupplierAssemblysExistsCode) {
                            if(item.ErrorMsg == null) {
                                item.ErrorMsg = "系统中祸首件所属总成编号" + item.Code + ErrorStrings.Export_Validation_PartsBranch_Validation7;
                            } else {
                                item.ErrorMsg = item.ErrorMsg + ";系统中祸首件所属总成编号" + item.Code + ErrorStrings.Export_Validation_PartsBranch_Validation7;
                            }
                        }
                    }
                    #endregion
                }
                //获取所有不合格数据
                errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                //获取所有合格数据
                rightList = allList.Except(errorList).ToList();
                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempSparePart = list[index - 1];
                            var values = new object[] {
                                tempSparePart.CodeStr, tempSparePart.NameStr,
                                tempSparePart.ErrorMsg
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }

                //导入所有合格数据
                if(!rightList.Any())
                    return true;
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    //开启事务，新增更新配件在一个事务内
                    var ts = conn.BeginTransaction();
                    try {
                        //新增配件
                        if(rightList.Any()) {
                            //获取新增数据的sql语句，Id为主键
                            var sqlInsert = db.GetInsertSql("FaultyPartsSupplierAssembly", "Id", new[] {
                                "Code","Name","Status","CreatorId","CreatorName","CreateTime"
                            });
                            var userInfo = Utils.GetCurrentUserInfo();
                            //往数据库增加祸首件所属总成
                            foreach(var item in rightList) {
                                var command = db.CreateDbCommand(sqlInsert, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("Code", Strings.StrConv(item.Code, VbStrConv.Narrow).Replace("【", "[")));
                                command.Parameters.Add(db.CreateDbParameter("Name", item.Name));
                                command.Parameters.Add(db.CreateDbParameter("Status", item.Status));
                                command.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                command.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                command.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                command.ExecuteNonQuery();
                            }

                        }
                        //无异常提交
                        ts.Commit();
                    } catch(Exception ex) {
                        //报错回滚
                        ts.Rollback();
                        throw new Exception(ex.Message);
                    } finally {
                        if(conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }

        /// <summary>
        /// 导出祸首件所属总成
        /// </summary>
        public bool ExportFaultyPartsSupplierAssembly(int[] ids, string code, string name, int? status, out string fileName) {
            fileName = GetExportFilePath("导出祸首件所属总成.xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.Append(@"select a.code,
                                        a.name,
                                        b.value as status,
                                        a.creatorname,
                                        a.createtime,
                                        a.modifiername,
                                        a.modifytime,
                                        a.abandonername,
                                        a.abandontime
                                   from FaultyPartsSupplierAssembly a
                                   left join keyvalueitem b
                                     on a.status = b.key
                                    and b.name = 'BaseData_Status' where 1=1 ");
                    var dbParameters = new List<DbParameter>();
                    if(ids != null && ids.Length > 0) {
                        sql.Append(" and a.id in (");
                        for(var i = 0; i < ids.Length; i++) {
                            if(ids.Length == i + 1) {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            } else {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                        }
                        sql.Append(")");
                    } else {
                        if(!string.IsNullOrEmpty(code)) {
                            sql.Append(" and LOWER(a.Code) like {0}code ");
                            dbParameters.Add(db.CreateDbParameter("code", "%" + code.ToLower() + "%"));
                        }
                        if(!string.IsNullOrEmpty(name)) {
                            sql.Append(" and LOWER(a.Name) like {0}name ");
                            dbParameters.Add(db.CreateDbParameter("name", "%" + name.ToLower() + "%"));
                        }
                        if(status.HasValue) {
                            sql.Append(" and a.status={0}status");
                            dbParameters.Add(db.CreateDbParameter("status", status.Value));
                        }
                    }
                    var sqlStr = sql.ToString();
                    if(sqlStr.IndexOf(" where 1=1  and ") > 0)
                        sqlStr = sqlStr.Replace(" where 1=1  and ", " where ");
                    else
                        sqlStr = sqlStr.Replace(" where 1=1 ", "");
                    var command = db.CreateDbCommand(string.Format(sqlStr, db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    "祸首件所属总成编号", 
                                    "祸首件所属总成名称", 
                                    ErrorStrings.Export_Title_AccountPeriod_Status, 
                                    ErrorStrings.Export_Title_AccountPeriod_CreatorName, 
                                    ErrorStrings.Export_Title_AccountPeriod_CreateTime, 
                                    ErrorStrings.Export_Title_AccountPeriod_ModifierName,
                                    ErrorStrings.Export_Title_AccountPeriod_ModifyTime,
                                    ErrorStrings.Export_Title_PartsBranch_AbandonerName, 
                                    ErrorStrings.Export_Title_PartsBranch_AbandonerTime
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();

                }
                return true;
            } catch(Exception) {
                return false;
            }
        }
    }
}
