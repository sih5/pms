﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        /// <summary>
        /// 批量导入扣补款单
        /// </summary>
        public bool ImportOutofWarrantyPayment(string fileName, out int excelImportNum, int branchId, out List<OutofWarrantyPaymentExtend> rightData, out List<OutofWarrantyPaymentExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<OutofWarrantyPaymentExtend>();
            var rightList = new List<OutofWarrantyPaymentExtend>();
            var allList = new List<OutofWarrantyPaymentExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("OutofWarrantyPayment", out notNullableFields, out fieldLenght);

                List<string> notNullableFieldsServiceProductLineView;
                Dictionary<string, int> fieldLenghtServiceProductLineView;
                db.GetTableSchema("ServiceProductLineView", out notNullableFieldsServiceProductLineView, out fieldLenghtServiceProductLineView);

                List<string> notNullableFieldsPartsSalesCategory;
                Dictionary<string, int> fieldLenghtPartsSalesCategory;
                db.GetTableSchema("PartsSalesCategory", out notNullableFieldsPartsSalesCategory, out fieldLenghtPartsSalesCategory);

                List<object> excelColumns;

                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    #region 指定文件中的列对应的名称 例如 excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode, "PartCode");

                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Partssalescategory_Name, "PartsSalesCategoryName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_Code, "DealerCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_Name, "DealerName");
                    excelOperator.AddColumnDataSource("服务产品线", "ServiceProductLineName");
                    excelOperator.AddColumnDataSource("扣补款方向", "DebitOrReplenish");
                    excelOperator.AddColumnDataSource("扣补款类型", "TransactionCategory");
                    excelOperator.AddColumnDataSource("扣补款金额", "TransactionAmount");
                    excelOperator.AddColumnDataSource("扣补款原因", "TransactionReason");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsOuterPurchaseChange_SourceCategoryr, "SourceType");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsOuterPurchaseChange_SourceCode, "SourceCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Remark, "Remark");
                    #endregion

                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;

                    #region 获取对应枚举
                    var keyValuePairs = new[] {
                            new KeyValuePair<string, string>("DebitOrReplenish", "ExpenseAdjustmentBill_DebitOrReplenish"),
                            new KeyValuePair<string, string>("TransactionCategory","OutofWarrantyPayment_TransactionCategory"),
                            new KeyValuePair<string, string>("Status","ExpenseAdjustmentBill_Status"),
                            new KeyValuePair<string, string>("SourceType","OutofWarrantyPayment_SourceType")
                        };
                    tempExcelOperator.ImportHelper.LoadEnumFromDb(null, null, keyValuePairs);

                    #endregion

                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var tempImportObj = new OutofWarrantyPaymentExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        #region 根据导入的字段给对应的实体属性赋值 tempImportObj.PartsSalesCategoryNameStr = row["PartsSalesCategoryName"].Trim();
                        tempImportObj.PartsSalesCategoryNameStr = newRow["PartsSalesCategoryName"];
                        tempImportObj.DealerCodeStr = newRow["DealerCode"];
                        tempImportObj.DealerNameStr = newRow["DealerName"];
                        tempImportObj.ServiceProductLineNameStr = newRow["ServiceProductLineName"];
                        tempImportObj.DebitOrReplenishStr = newRow["DebitOrReplenish"];
                        tempImportObj.TransactionCategoryStr = newRow["TransactionCategory"];
                        tempImportObj.TransactionAmountStr = newRow["TransactionAmount"];
                        tempImportObj.TransactionReasonStr = newRow["TransactionReason"];
                        tempImportObj.SourceTypeStr = newRow["SourceType"];
                        tempImportObj.SourceCodeStr = newRow["SourceCode"];
                        tempImportObj.RemarkStr = newRow["Remark"];
                        tempImportObj.Remark = newRow["Remark"];
                        #endregion

                        var tempErrorMessage = new List<string>();

                        #region 导入的数据基本检查

                        //配件销售类型名称检查
                        var fieldIndex = notNullableFieldsPartsSalesCategory.IndexOf("Name".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.PartsSalesCategoryNameStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add("配件销售类型名称不能为空");
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.PartsSalesCategoryNameStr) > fieldLenghtPartsSalesCategory["Name".ToUpper()])
                                tempErrorMessage.Add("配件销售类型名称过长");
                        }

                        //服务站编号检查
                        fieldIndex = notNullableFields.IndexOf("DealerCode".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.DealerCodeStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add("服务站编号不能为空");
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.DealerCodeStr) > fieldLenght["DealerCode".ToUpper()])
                                tempErrorMessage.Add("服务站编号过长");
                        }

                        //服务产品线名称检查
                        fieldIndex = notNullableFields.IndexOf("ServiceProductLineId".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.ServiceProductLineNameStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add("服务产品线名称不能为空");
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.ServiceProductLineNameStr) > fieldLenghtServiceProductLineView["ProductLineName".ToUpper()])
                                tempErrorMessage.Add("服务产品线名称过长");
                        }

                        //扣补款方向类型
                        fieldIndex = notNullableFields.IndexOf("DebitOrReplenish".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.DebitOrReplenishStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add("扣补款方向不能为空");
                        } else {
                            var tempEnumValue = tempExcelOperator.ImportHelper.GetEnumValue("DebitOrReplenish", tempImportObj.DebitOrReplenishStr);
                            if(!tempEnumValue.HasValue)
                                tempErrorMessage.Add("扣补款方向不正确");
                            else
                                tempImportObj.DebitOrReplenish = tempEnumValue.Value;
                        }


                        //扣补款类型检查
                        fieldIndex = notNullableFields.IndexOf("TransactionCategory".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.DebitOrReplenishStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add("扣补款类型不能为空");
                        } else {
                            var tempEnumValue = tempExcelOperator.ImportHelper.GetEnumValue("TransactionCategory", tempImportObj.TransactionCategoryStr);
                            if(!tempEnumValue.HasValue)
                                tempErrorMessage.Add("扣补款类型不正确");
                            else
                                tempImportObj.TransactionCategory = tempEnumValue.Value;
                        }

                        //扣补款金额检查
                        fieldIndex = notNullableFields.IndexOf("TransactionAmount".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.TransactionAmountStr)) {
                            tempErrorMessage.Add("扣补款金额不能为空");
                        } else {
                            decimal checkValue;
                            if(decimal.TryParse(tempImportObj.TransactionAmountStr, out checkValue)) {
                                checkValue = Math.Round(checkValue, 2, MidpointRounding.AwayFromZero);
                                if(checkValue <= 0)
                                    tempErrorMessage.Add("扣补款金额必须大于0");
                                else
                                    tempImportObj.TransactionAmount = checkValue;
                            } else {
                                tempErrorMessage.Add("扣补款金额必须是数字");
                            }
                        }

                        //扣补款原因检查 
                        fieldIndex = notNullableFields.IndexOf("TransactionReason".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.TransactionReasonStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add("扣补款原因不能为空");
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.TransactionReasonStr) > fieldLenght["TransactionReason".ToUpper()])
                                tempErrorMessage.Add("扣补款原因过长");
                        }

                        //源单据编号检查 
                        if(string.IsNullOrEmpty(tempImportObj.SourceCodeStr) || string.IsNullOrEmpty(tempImportObj.SourceTypeStr)) {
                            tempErrorMessage.Add("源单据编号，源单据类型请同时提供");
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.SourceCodeStr) > 50) {
                                tempErrorMessage.Add("源单据编号过长");
                            }
                            //源单据类型检查 
                            var tempEnumValue = tempExcelOperator.ImportHelper.GetEnumValue("SourceType", tempImportObj.SourceTypeStr);
                            if(!tempEnumValue.HasValue)
                                tempErrorMessage.Add("源单据类型不正确");
                            else
                                tempImportObj.SourceType = tempEnumValue.Value;
                        }
                        //备注检查
                        fieldIndex = notNullableFields.IndexOf("Remark".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.RemarkStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchasePricingDetail_Validation15);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.RemarkStr) > fieldLenght["Remark".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchasePricingDetail_Validation16);
                        }
                        #endregion

                        if(tempErrorMessage.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });
                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    #region 剩下的数据进行业务检查

                    //品牌
                    var partsSalesCategoryNamesNeedCheck = tempRightList.Select(r => r.PartsSalesCategoryNameStr).Distinct().ToArray();
                    var dbPartsSalesCategorys = new List<PartsSalesCategoryExtend>();
                    Func<string[], bool> getDbPartsSalesCategorys = value => {
                        var dbObj = new PartsSalesCategoryExtend {
                            Id = Convert.ToInt32(value[0]),
                            Code = value[1],
                            Name = value[2],
                            BranchId = Convert.ToInt32(value[3])
                        };
                        dbPartsSalesCategorys.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select Id,Code,Name,BranchId from PartsSalesCategory where status=1 ", "Name", false, partsSalesCategoryNamesNeedCheck, getDbPartsSalesCategorys);
                    //产品线
                    var serviceProductLineNameNeedCheck = tempRightList.Select(r => r.ServiceProductLineNameStr).Distinct().ToArray();
                    var dbServiceProductLines = new List<ServiceProductLineViewExtend>();
                    Func<string[], bool> getDbServiceProductLines = value => {
                        var dbObj = new ServiceProductLineViewExtend {
                            ServiceProductLineId = Convert.ToInt32(value[0]),
                            ProductLineType = Convert.ToInt32(value[1]),
                            ProductLineName = value[2],
                            PartsSalesCategoryId = Convert.ToInt32(value[3])
                        };
                        dbServiceProductLines.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("Select ProductLineId,ProductLineType,ProductLineName,PartsSalesCategoryId From ServiceProductLineView", "ProductLineName", true, serviceProductLineNameNeedCheck, getDbServiceProductLines);
                    //服务站
                    var dealerCodesNeedCheck = tempRightList.Select(r => r.DealerCodeStr).Distinct().ToArray();
                    var dbDealers = new List<DealerExtend>();
                    Func<string[], bool> getDbDealers = value => {
                        var dbObj = new DealerExtend {
                            Id = Convert.ToInt32(value[0]),
                            Code = value[1],
                            Name = value[2]
                        };
                        dbDealers.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select Id,Code,Name from Dealer where status in (1,2) ", "Code", false, dealerCodesNeedCheck, getDbDealers);

                    var dbDealerServiceInfos = new List<DealerServiceInfoExtend>();
                    Func<string[], bool> getDbDealerServiceInfos = value => {
                        var dbObj = new DealerServiceInfoExtend {
                            DealerId = Convert.ToInt32(value[0]),
                            BranchId = Convert.ToInt32(value[1]),
                            PartsSalesCategoryId = Convert.ToInt32(value[2]),
                            GradeCoefficientId = Convert.ToInt32(value[3])
                        };
                        dbDealerServiceInfos.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select DealerId,BranchId,PartsSalesCategoryId,GradeCoefficientId from DealerServiceInfo where status in (1,2) ", "BranchId", false, new[] { branchId.ToString(CultureInfo.InvariantCulture) }, getDbDealerServiceInfos);
                    var integralClaimBillCodesNeedCheck1 = tempRightList.Where(r => r.SourceType == (int)DcsOutofWarrantyPaymentSourceType.会员积分费).Select(r => r.SourceCodeStr).Distinct().ToArray();
                    var integralClaimBillCodesNeedCheck2 = tempRightList.Where(r => r.SourceType == (int)DcsOutofWarrantyPaymentSourceType.红包费).Select(r => r.SourceCodeStr).Distinct().ToArray();
                    var dbPartsClaimOrders = new Dictionary<string, int>();
                    Func<string[], bool> getDbPartsClaimOrders = value => {
                        dbPartsClaimOrders.Add(value[0], Convert.ToInt32(value[1]));
                        return false;
                    };
                    db.QueryDataWithInOperator("select IntegralClaimBillCode,id from IntegralClaimBill where IntegralClaimBillType=4", "IntegralClaimBillCode", false, integralClaimBillCodesNeedCheck1, getDbPartsClaimOrders);
                    db.QueryDataWithInOperator("select IntegralClaimBillCode,id from IntegralClaimBill where IntegralClaimBillType=5", "IntegralClaimBillCode", false, integralClaimBillCodesNeedCheck2, getDbPartsClaimOrders);


                    foreach(var tempRight in tempRightList) {
                        var partsSalesCategory = dbPartsSalesCategorys.FirstOrDefault(v => v.Name == tempRight.PartsSalesCategoryNameStr && v.BranchId == branchId);
                        if(partsSalesCategory == null) {
                            tempRight.ErrorMsg = ErrorStrings.ImpVehicleDLRStartSecurityDeposit_Validation8;
                            continue;
                        }
                        if(!string.IsNullOrWhiteSpace(tempRight.ServiceProductLineNameStr)) {
                            var serviceProductLine = dbServiceProductLines.FirstOrDefault(r => r.ProductLineName == tempRight.ServiceProductLineNameStr);
                            if(serviceProductLine == null) {
                                tempRight.ErrorMsg = "服务产品线不存在";
                                continue;
                            }
                            if(serviceProductLine.PartsSalesCategoryId != partsSalesCategory.Id) {
                                tempRight.ErrorMsg = "导入产品线与导入的所属品牌不符";
                                continue;
                            }
                            tempRight.ServiceProductLineId = serviceProductLine.ServiceProductLineId;
                            tempRight.ServiceProductLineType = serviceProductLine.ProductLineType;
                        }
                        var dealer = dbDealers.FirstOrDefault(v => v.Code == tempRight.DealerCodeStr);
                        if(dealer == null) {
                            tempRight.ErrorMsg = "服务站不存在";
                            continue;
                        }
                        var dealerServiceInfo = dbDealerServiceInfos.FirstOrDefault(r => r.DealerId == dealer.Id && r.BranchId == branchId && r.PartsSalesCategoryId == partsSalesCategory.Id);
                        if(dealerServiceInfo == null) {
                            tempRight.ErrorMsg = "服务站不存在";
                            continue;
                        }
                        var integralClaimBill = dbPartsClaimOrders.FirstOrDefault(v => v.Key == tempRight.SourceCodeStr);
                        if(integralClaimBill.Key == null) {
                            tempRight.ErrorMsg = "源单据类型或源单据编号不正确";
                            continue;
                        }
                        tempRight.SourceId = integralClaimBill.Value;
                        tempRight.GradeCoefficientId = dealerServiceInfo.GradeCoefficientId;
                        tempRight.PartsSalesCategoryId = dealerServiceInfo.PartsSalesCategoryId;
                        tempRight.DealerId = dealerServiceInfo.DealerId;
                        tempRight.DealerCode = dealer.Code;
                        tempRight.DealerName = dealer.Name;
                        tempRight.BranchId = branchId;
                        tempRight.TransactionReason = tempRight.TransactionReasonStr;
                        tempRight.Status = tempExcelOperator.ImportHelper.GetEnumValue("Status", "新增") ?? 0;
                        tempRight.SettlementStatus = (int)DcsClaimBillSettlementStatus.待结算;
                        tempRight.Status = tempExcelOperator.ImportHelper.GetEnumValue("Status", "新增") ?? 0;
                        tempRight.SourceCode = tempRight.SourceCodeStr;
                        tempRight.ServiceProductLineName = tempRight.ServiceProductLineNameStr;

                    }
                    #endregion

                    //获取所有不合格数据
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    //获取所有合格数据
                    rightList = allList.Except(errorList).ToList();

                    //获取星级系数
                    #region 将合格数据的值填上
                    //foreach(var rightItem in rightList) {
                    //    rightItem.BranchId = branchId;
                    //    rightItem.DebitOrReplenish = tempExcelOperator.ImportHelper.GetEnumValue("DebitOrReplenish", rightItem.DebitOrReplenishStr) ?? 0;
                    //    rightItem.DealerCode = rightItem.DealerCodeStr;
                    //    rightItem.DealerName = rightItem.DealerNameStr;
                    //    rightItem.TransactionCategory = tempExcelOperator.ImportHelper.GetEnumValue("TransactionCategory", rightItem.TransactionCategoryStr) ?? 0;
                    //    rightItem.TransactionAmount = Convert.ToDecimal(rightItem.TransactionAmountStr);
                    //    rightItem.TransactionReason = rightItem.TransactionReasonStr;
                    //    rightItem.Status = tempExcelOperator.ImportHelper.GetEnumValue("Status", "新增") ?? 0;
                    //    rightItem.SettlementStatus = (int)DcsClaimBillSettlementStatus.待结算;
                    //    rightItem.Status = tempExcelOperator.ImportHelper.GetEnumValue("Status", "新增") ?? 0;
                    //    rightItem.SourceCode = rightItem.SourceCodeStr;
                    //    rightItem.SourceType = tempExcelOperator.ImportHelper.GetEnumValue("SourceType", rightItem.SourceTypeStr);
                    //    rightItem.ServiceProductLineName = rightItem.ServiceProductLineNameStr;
                    //}
                    #endregion
                }
                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempObj = list[index - 1];
                            var values = new object[] {
                                #region 设置错误信息导出的列的值
                                tempObj.PartsSalesCategoryNameStr, tempObj.DealerCodeStr, 
                                tempObj.DealerNameStr,tempObj.ServiceProductLineNameStr,
                                tempObj.DebitOrReplenishStr, tempObj.TransactionCategoryStr,
                                tempObj.TransactionAmountStr, tempObj.TransactionReasonStr,
                                tempObj.SourceTypeStr,tempObj.SourceCodeStr,
                                tempObj.RemarkStr, tempObj.ErrorMsg
                                #endregion
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }

                //导入所有合格数据
                //if(!rightList.Any())
                //    return true;
                //using(var conn = db.CreateDbConnection()) {
                //    conn.Open();
                //    //开启事务，新增更新配件在一个事务内
                //    var ts = conn.BeginTransaction();
                //    try {
                //        //新增配件
                //        if(rightList.Any()) {
                //            #region 获取对应的插入的SQL 通过db.GetInsertSql方法
                //            //获取新增数据的sql语句，Id为主键
                //            var sqlInsert = db.GetInsertSql("OutofWarrantyPayment", "Id", new[] {
                //                    "PartsSalesCategoryId", "DealerName", "DebitOrReplenish", "TransactionCategory", "TransactionAmount", "TransactionReason","Status","Remark"
                //                });
                //            #endregion

                //            //往数据库增加配件信息
                //            foreach(var item in rightList) {
                //                #region 添加Sql的参数
                //                var command = db.CreateDbCommand(sqlInsert, conn, ts);
                //                command.Parameters.Add(db.CreateDbParameter("PartsSalesCategoryId", item.PartsSalesCategoryId));
                //                command.Parameters.Add(db.CreateDbParameter("DealerName", item.DealerName));
                //                command.Parameters.Add(db.CreateDbParameter("DebitOrReplenish", item.DebitOrReplenish));
                //                command.Parameters.Add(db.CreateDbParameter("TransactionCategory", item.TransactionCategory));
                //                command.Parameters.Add(db.CreateDbParameter("TransactionAmount", item.TransactionAmount));
                //                command.Parameters.Add(db.CreateDbParameter("TransactionReason", item.TransactionReason));
                //                command.Parameters.Add(db.CreateDbParameter("Status", item.Status));
                //                command.Parameters.Add(db.CreateDbParameter("Remark", item.Remark));
                //                command.ExecuteNonQuery();
                //                #endregion
                //            }
                //        }
                //        //无异常提交
                //        ts.Commit();
                //    } catch(Exception) {
                //        //报错回滚
                //        ts.Rollback();
                //        throw;
                //    }finally {
                //      if(conn.State == System.Data.ConnectionState.Open) {
                //          conn.Close();
                //      }
                //    }
                //}
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }

        /// <summary>
        /// 导出扣补款单
        /// </summary>
        public bool ExportOutofWarrantyPayment(int[] ids, int? branchId, int? dealerId, int? panelBranchId, int? status, int? settlementStatus, int? transactionCategory, int? sourceType, string code, string sourceCode, string dealerCode, string dealerName, int? responsibleUnitId, int? partsSalesCategoryId, DateTime? createTimeBegin, DateTime? createTimeEnd, string companyCustomerCode, int? marketingDepartmentId, out string fileName) {
            fileName = GetExportFilePath("保外扣补款单.xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    #region SQL
                    sql.Append(@"Select a.Code,
                                               k2.value as status,
                                               k3.value as Settlementstatus,
                                               k.value as Transactioncategory,
                                               b.Name,
                                               d.Name,
                                               e.Name,
                                               a.Dealercode,
                                               T1.Businesscode,
                                               a.Dealername,
                                               T2.Name as MarketName,
                                               a.Remark,
                                               g.Grade,
                                               cast(decode(SourceType,1,'会员积分费',2,'红包费') as varchar2(20)), --源单据类型
                                               SourceCode, --源单据编号
                                               cast(decode(DebitOrReplenish,1,'扣款',2,'补款') as varchar2(20)), --扣补款方向
                                               TransactionAmount, --扣补款金额 
                                               a.Transactionreason,
                                               a.Dealercontactperson,
                                               a.Dealerphonenumber,
                                               a.CreatorName,
                                               a.CreateTime,
                                               a.abandonername,
                                               a.approvername                                               
                                          From OutofWarrantyPayment a
                                          left join GradeCoefficient g on a.GradeCoefficientId=g.id
                                          left join keyvalueitem k
                                          on a.transactioncategory=k.key and k.name='OutofWarrantyPayment_TransactionCategory'
                                          left join keyvalueitem k1
                                          on a.sourcetype=k1.key and k1.name='OutofWarrantyPayment_SourceType'
                                          left join keyvalueitem k2
                                          on a.status=k2.key and k2.name='ExpenseAdjustmentBill_Status'
                                          left join keyvalueitem k3
                                          on a.Settlementstatus=k3.key and k3.name='ClaimBill_SettlementStatus' 
                                          left join keyvalueitem k4
                                          on a.Debitorreplenish=k4.key and k4.name='ExpenseAdjustmentBill_DebitOrReplenish' 
                                          join company cc
                                            on a.dealerid=cc.id
                                          Left Join Branch b
                                            On a.Branchid = b.Id
                                          Left Join Responsibleunit c
                                            On a.Responsibleunitid = c.Id
                                          Left Join Partssalescategory d
                                            On a.Partssalescategoryid = d.Id
                                          Left Join Serviceproductline e
                                            On a.Serviceproductlineid = e.Id
                                          Left Join DealerServiceInfo T1
                                            On  T1.status=1 and a.branchid=T1.branchid and a.dealerid=T1.dealerid and a.partssalescategoryid=T1.PartsSalesCategoryId
                                          left join Marketingdepartment T2
                                            On T1.MarketingDepartmentId=T2.Id where 1=1 ");
                    #endregion
                    var dbParameters = new List<DbParameter>();
                    if(ids != null && ids.Length > 0) {
                        var idValues = new List<string>();
                        for(int i = 0; i < ids.Length; i++) {
                            dbParameters.Add(db.CreateDbParameter("billId" + i, ids[i]));
                            idValues.Add("{0}billId" + i);
                        }
                        var idsSql = string.Join(",", idValues);
                        sql.Append(string.Format(" and a.id in ({0})", idsSql));
                    } else {
                        if(branchId.HasValue) {
                            sql.Append(" and a.branchId ={0}branchId ");
                            dbParameters.Add(db.CreateDbParameter("branchId", branchId.Value));
                        }
                        if(dealerId.HasValue) {
                            sql.Append(" and a.dealerId ={0}dealerId ");
                            dbParameters.Add(db.CreateDbParameter("dealerId", dealerId.Value));
                        }
                        if(panelBranchId.HasValue) {
                            sql.Append(" and a.branchId = {0}panelBranchId ");
                            dbParameters.Add(db.CreateDbParameter("panelBranchId", panelBranchId.Value));
                        }
                        if(status.HasValue) {
                            sql.Append(" and a.status = {0}status ");
                            dbParameters.Add(db.CreateDbParameter("status", status.Value));
                        }
                        if(settlementStatus.HasValue) {
                            sql.Append(" and a.settlementStatus = {0}settlementStatus ");
                            dbParameters.Add(db.CreateDbParameter("settlementStatus", settlementStatus.Value));
                        }
                        if(transactionCategory.HasValue) {
                            sql.Append(" and a.transactionCategory = {0}transactionCategory ");
                            dbParameters.Add(db.CreateDbParameter("transactionCategory", transactionCategory.Value));
                        }
                        if(sourceType.HasValue) {
                            sql.Append(" and a.sourceType = {0}sourceType ");
                            dbParameters.Add(db.CreateDbParameter("sourceType", sourceType.Value));
                        }
                        if(!string.IsNullOrEmpty(code)) {
                            sql.Append(" and a.code like Upper({0}code) ");
                            dbParameters.Add(db.CreateDbParameter("code", "%" + code + "%"));
                        }
                        if(!string.IsNullOrEmpty(sourceCode)) {
                            sql.Append(" and a.sourceCode like Upper({0}sourceCode) ");
                            dbParameters.Add(db.CreateDbParameter("sourceCode", "%" + sourceCode + "%"));
                        }
                        if(!string.IsNullOrEmpty(dealerCode)) {
                            sql.Append(" and a.dealerCode like Upper({0}dealerCode) ");
                            dbParameters.Add(db.CreateDbParameter("dealerCode", "%" + dealerCode + "%"));
                        }

                        if(!string.IsNullOrEmpty(dealerName)) {
                            sql.Append(" and a.dealerName like {0}dealerName ");
                            dbParameters.Add(db.CreateDbParameter("dealerName", "%" + dealerName + "%"));
                        }
                        if(!string.IsNullOrEmpty(companyCustomerCode)) {
                            sql.Append(" and T1.Businesscode like {0}companyCustomerCode ");
                            dbParameters.Add(db.CreateDbParameter("companyCustomerCode", "%" + companyCustomerCode + "%"));
                        }
                        if(marketingDepartmentId.HasValue) {
                            sql.Append(" and T2.Id ={0}marketingDepartmentId ");
                            dbParameters.Add(db.CreateDbParameter("marketingDepartmentId", marketingDepartmentId.Value));
                        }
                        if(responsibleUnitId.HasValue) {
                            sql.Append(" and a.responsibleUnitId ={0}responsibleUnitId ");
                            dbParameters.Add(db.CreateDbParameter("responsibleUnitId", responsibleUnitId.Value));
                        }

                        if(partsSalesCategoryId.HasValue) {
                            sql.Append(" and a.partsSalesCategoryId ={0}partsSalesCategoryId ");
                            dbParameters.Add(db.CreateDbParameter("partsSalesCategoryId", partsSalesCategoryId.Value));
                        }
                        if(createTimeBegin.HasValue) {
                            sql.Append(@" and a.createTime >=To_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(@" and a.createTime <=To_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                    }
                    var sqlStr = sql.ToString();
                    if(sqlStr.IndexOf(" where 1=1  and ") > 0)
                        sqlStr = sqlStr.Replace(" where 1=1  and ", " where ");
                    else
                        sqlStr = sqlStr.Replace(" where 1=1 ", "");
                    var command = db.CreateDbCommand(string.Format(sqlStr, db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    "扣补款单编号", ErrorStrings.Export_Title_AccountPeriod_Status, ErrorStrings.Export_Title_PartsInboundCheckBill_SettlementStatus, "扣补款类型",ErrorStrings.Export_Title_PartsBranch_BranchName, ErrorStrings.Export_Title_Partssalescategory_Name,"服务产品线",ErrorStrings.Export_Title_Dealer_Code,"服务站业务编码", ErrorStrings.Export_Title_Dealer_Name,ErrorStrings.Export_Title_PartsBranch_Remark,"星级","市场部名称", ErrorStrings.Export_Title_PartsOuterPurchaseChange_SourceCategoryr, ErrorStrings.Export_Title_PartsOuterPurchaseChange_SourceCode, "扣补款方向", "扣补款金额", "扣补款原因", "服务站联系人", "服务站联系电话",ErrorStrings.Export_Title_AccountPeriod_CreatorName,ErrorStrings.Export_Title_AccountPeriod_CreateTime,ErrorStrings.Export_Title_PartsBranch_AbandonerName,ErrorStrings.Export_Title_PackingPropertyApp_ApproverName
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }

        /// <summary>
        /// 分公司导出扣补款单
        /// </summary>
        public bool ExportOutofWarrantyPaymentForBranch(int[] ids, int? userId, int? branchId, int? dealerId, int? panelBranchId, int? status, int? settlementStatus, int? transactionCategory, int? sourceType, string code, string sourceCode, string dealerCode, string dealerName, int? responsibleUnitId, int? partsSalesCategoryId, DateTime? createTimeBegin, DateTime? createTimeEnd, string companyCustomerCode, int? marketingDepartmentId, out string fileName) {
            fileName = GetExportFilePath("保外扣补款单.xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    #region SQL
                    sql.AppendFormat(@"Select a.Code,
                                               k2.value as status,
                                               k3.value as Settlementstatus,
                                               k.value as Transactioncategory,
                                               b.Name,
                                               d.Name,
                                               e.Name,
                                               a.Dealercode,
                                               T1.Businesscode,
                                               a.Dealername,
                                               T2.Name as MarketName,
                                              a.Remark,
                                               g.Grade,
                                               k1.value as Sourcetype,
                                               a.Sourcecode,
                                               k4.value as Debitorreplenish,
                                               Decode(a.Debitorreplenish, 1, -a.Transactionamount, 2, a.Transactionamount),
                                               a.Transactionreason,
                                               a.Dealercontactperson,
                                               a.Dealerphonenumber,
                                               a.CreatorName,
                                               a.CreateTime,
                                               a.abandonername,
                                               a.approvername
                                          From OutofWarrantyPayment a
                                          left join GradeCoefficient g on a.GradeCoefficientId=g.id
                                          left join keyvalueitem k
                                          on a.transactioncategory=k.key and k.name='OutofWarrantyPayment_TransactionCategory'
                                          left join keyvalueitem k1
                                          on a.sourcetype=k1.key and k1.name='OutofWarrantyPayment_SourceType'
                                          left join keyvalueitem k2
                                          on a.status=k2.key and k2.name='ExpenseAdjustmentBill_Status'
                                          left join keyvalueitem k3
                                          on a.Settlementstatus=k3.key and k3.name='ClaimBill_SettlementStatus' 
                                          left join keyvalueitem k4
                                          on a.Debitorreplenish=k4.key and k4.name='ExpenseAdjustmentBill_DebitOrReplenish' 
                                          join company cc
                                          on a.dealerid=cc.id
                                          Left Join Branch b
                                            On a.Branchid = b.Id
                                          Left Join Responsibleunit c
                                            On a.Responsibleunitid = c.Id
                                          Left Join Partssalescategory d
                                            On a.Partssalescategoryid = d.Id
                                          Left Join Serviceproductline e
                                            On a.Serviceproductlineid = e.Id
                                          Left Join DealerServiceInfo T1
                                            On T1.status=1 and a.branchid=T1.branchid and a.dealerid=T1.dealerid and a.partssalescategoryid=T1.PartsSalesCategoryId
                                          left join Marketingdepartment T2
                                            On T1.MarketingDepartmentId=T2.Id
                                          INNER JOIN PersonSalesCenterLink f ON a.PartsSalesCategoryId = f.PartsSalesCategoryId
                                                                where   f.PersonId ={0}", userId);
                    #endregion
                    var dbParameters = new List<DbParameter>();
                    if(ids != null && ids.Length > 0) {
                        var idValues = new List<string>();
                        for(int i = 0; i < ids.Length; i++) {
                            dbParameters.Add(db.CreateDbParameter("billId" + i, ids[i]));
                            idValues.Add("{0}billId" + i);
                        }
                        var idsSql = string.Join(",", idValues);
                        sql.Append(string.Format("and a.id in ({0})", idsSql));
                    } else {
                        if(branchId.HasValue) {
                            sql.Append("and a.branchId ={0}branchId ");
                            dbParameters.Add(db.CreateDbParameter("branchId", branchId.Value));
                        }
                        if(dealerId.HasValue) {
                            sql.Append("and a.dealerId ={0}dealerId ");
                            dbParameters.Add(db.CreateDbParameter("dealerId", dealerId.Value));
                        }
                        if(panelBranchId.HasValue) {
                            sql.Append("and a.branchId = {0}panelBranchId ");
                            dbParameters.Add(db.CreateDbParameter("panelBranchId", panelBranchId.Value));
                        }
                        if(status.HasValue) {
                            sql.Append("and a.status = {0}status ");
                            dbParameters.Add(db.CreateDbParameter("status", status.Value));
                        }
                        if(settlementStatus.HasValue) {
                            sql.Append("and a.settlementStatus = {0}settlementStatus ");
                            dbParameters.Add(db.CreateDbParameter("settlementStatus", settlementStatus.Value));
                        }
                        if(transactionCategory.HasValue) {
                            sql.Append("and a.transactionCategory = {0}transactionCategory ");
                            dbParameters.Add(db.CreateDbParameter("transactionCategory", transactionCategory.Value));
                        }
                        if(!string.IsNullOrEmpty(companyCustomerCode)) {
                            sql.Append("and T1.Businesscode like {0}companyCustomerCode ");
                            dbParameters.Add(db.CreateDbParameter("companyCustomerCode", "%" + companyCustomerCode + "%"));
                        }
                        if(marketingDepartmentId.HasValue) {
                            sql.Append("and T2.Id ={0}marketingDepartmentId ");
                            dbParameters.Add(db.CreateDbParameter("marketingDepartmentId", marketingDepartmentId.Value));
                        }
                        if(sourceType.HasValue) {
                            sql.Append("and a.sourceType = {0}sourceType ");
                            dbParameters.Add(db.CreateDbParameter("sourceType", sourceType.Value));
                        }
                        if(!string.IsNullOrEmpty(code)) {
                            sql.Append("and a.code like Upper({0}code) ");
                            dbParameters.Add(db.CreateDbParameter("code", "%" + code + "%"));
                        }
                        if(!string.IsNullOrEmpty(sourceCode)) {
                            sql.Append("and a.sourceCode like Upper({0}sourceCode) ");
                            dbParameters.Add(db.CreateDbParameter("sourceCode", "%" + sourceCode + "%"));
                        }
                        if(!string.IsNullOrEmpty(dealerCode)) {
                            sql.Append("and a.dealerCode like Upper({0}dealerCode) ");
                            dbParameters.Add(db.CreateDbParameter("dealerCode", "%" + dealerCode + "%"));
                        }
                        if(!string.IsNullOrEmpty(dealerName)) {
                            sql.Append("and a.dealerName like {0}dealerName ");
                            dbParameters.Add(db.CreateDbParameter("dealerName", "%" + dealerName + "%"));
                        }
                        if(responsibleUnitId.HasValue) {
                            sql.Append("and a.responsibleUnitId ={0}responsibleUnitId ");
                            dbParameters.Add(db.CreateDbParameter("responsibleUnitId", responsibleUnitId.Value));
                        }
                        if(partsSalesCategoryId.HasValue) {
                            sql.Append("and a.partsSalesCategoryId ={0}partsSalesCategoryId ");
                            dbParameters.Add(db.CreateDbParameter("partsSalesCategoryId", partsSalesCategoryId.Value));
                        }
                        if(createTimeBegin.HasValue) {
                            sql.Append(@" and a.createTime >=To_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(@" and a.createTime <=To_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                    }
                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    "扣补款单编号", ErrorStrings.Export_Title_AccountPeriod_Status, ErrorStrings.Export_Title_PartsInboundCheckBill_SettlementStatus, "扣补款类型",ErrorStrings.Export_Title_PartsBranch_BranchName, ErrorStrings.Export_Title_Partssalescategory_Name,"服务产品线",ErrorStrings.Export_Title_Dealer_Code,"服务站业务编码", ErrorStrings.Export_Title_Dealer_Name,"市场部名称",ErrorStrings.Export_Title_PartsBranch_Remark,"星级", ErrorStrings.Export_Title_PartsOuterPurchaseChange_SourceCategoryr, ErrorStrings.Export_Title_PartsOuterPurchaseChange_SourceCode, "扣补款方向", "扣补款金额", "扣补款原因", "服务站联系人", "服务站联系电话",ErrorStrings.Export_Title_AccountPeriod_CreatorName,ErrorStrings.Export_Title_AccountPeriod_CreateTime,ErrorStrings.Export_Title_PartsBranch_AbandonerName,ErrorStrings.Export_Title_PackingPropertyApp_ApproverName
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }

    }
}

