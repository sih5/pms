﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        /// <summary>
        /// 导出售前检查单
        /// </summary>
        public bool ExportPreSaleCheckOrder(int? branchId, string code, int? partsSalesCategoryId, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, int? settleStatus, string dealerCode, string dealerName, out string fileName) {
            fileName = GetExportFilePath("售前检查单_" + ".xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated) {
                    throw new Exception(ErrorStrings.File_Validation_User);
                }
                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {

                    conn.Open();
                    var sql = new StringBuilder();
                    sql.Append(@"select    a.Code,
                                               b.Name  as BranchName,
                                               c.name  as PartsSalesCategoryName, 
                                               a.DealerCode,
                                               a.DealerName,
                                               cast(case a.Status
                                               when 1 then '新建'
                                               when 2	then '提交'
                                               when 3	then '已审核'
                                               when 99	then '作废'
                                               end  as varchar2(20)) as status,
                                               cast(decode(a.settleStatus,
                                                    1,'不结算',
                                                    2,'待结算',
                                                    3,'已结算'
                                               ) as varchar2(20))  as  settleStatus,
                                               f.PreSaleTime,
                                               a.VIN,
                                               a.InternalCode,
                                               a.Revision,
                                               a.Series,
                                               a.ProductCategory,
                                               a.Drive,
                                               f.VehicleLicensePlate,
                                               a.DriverTel,
                                               a.DriverName,
                                               e.name  as ResponsibleUnitName,
                                               d.name  as ServiceProductLineName,
                                               a.Cost,
                                               a.IsSaleService,
                                               a.SaleDealerName,
                                               a.CreateTime,
                                               a.CreatorName,
                                               a.ApproveTime,
                                               a.ApproverName,
                                               a.AbandonTime,
                                               a.AbandonerName,
                                               a.ModifyTime,
                                               a.ModifierName
                                      from PreSaleCheckOrder a
                                     left join branch b
                                        on a.branchid = b.id
                                     left join partssalescategory c
                                        on a.partssalescategoryid = c.id
                                     left join serviceproductline d
                                        on a.serviceproductlineid=d.id
                                     left join Responsibleunit e
                                        on a.responsibleunitid=e.id
                                    left join VehicleInformation f
                                        on f.id=a.vehicleid where 1=1 ");
                    var dbParameters = new List<DbParameter>();
                    if(branchId.HasValue) {
                        sql.Append(@" and a.branchId={0}branchId");
                        dbParameters.Add(db.CreateDbParameter("branchId", branchId.Value));
                    }

                    if(!string.IsNullOrEmpty(code)) {
                        sql.Append(@" and a.code ={0}code ");
                        dbParameters.Add(db.CreateDbParameter("code", code));
                    }
                    if(partsSalesCategoryId.HasValue) {
                        sql.Append(@" and a.partsSalesCategoryId={0}partsSalesCategoryId");
                        dbParameters.Add(db.CreateDbParameter("partsSalesCategoryId", partsSalesCategoryId.Value));
                    }
                    if(status.HasValue) {
                        sql.Append(@" and a.status ={0}Status");
                        dbParameters.Add(db.CreateDbParameter("status", status.Value));
                    }
                    if(settleStatus.HasValue) {
                        sql.Append(@" and a.settleStatus ={0}settleStatus");
                        dbParameters.Add(db.CreateDbParameter("settleStatus", settleStatus.Value));
                    }
                    if(createTimeBegin.HasValue) {
                        sql.Append(@" and a.createTime >=to_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                        var tempValue = createTimeBegin.Value;
                        var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                        dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                    }
                    if(createTimeEnd.HasValue) {
                        sql.Append(@" and a.createTime <=to_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                        var tempValue = createTimeEnd.Value;
                        var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                        dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                    }
                    if(!string.IsNullOrEmpty(dealerCode)) {
                        sql.Append(@" and a.dealerCode ={0}dealerCode ");
                        dbParameters.Add(db.CreateDbParameter("dealerCode", dealerCode));
                    }
                    if(!string.IsNullOrEmpty(dealerName)) {
                        sql.Append(@" and a.dealerName ={0}dealerName ");
                        dbParameters.Add(db.CreateDbParameter("dealerName", dealerName));
                    }
                    var sqlStr = sql.ToString();
                    if(sqlStr.IndexOf(" where 1=1  and ") > 0)
                        sqlStr = sqlStr.Replace(" where 1=1  and ", " where ");
                    else
                        sqlStr = sqlStr.Replace(" where 1=1 ", "");
                    var command = db.CreateDbCommand(string.Format(sqlStr, db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    "售前检查单编号",ErrorStrings.Export_Title_PartsBranch_BranchName,ErrorStrings.Export_Title_Partssalescategory_Name,ErrorStrings.Export_Title_Dealer_Code,ErrorStrings.Export_Title_Dealer_Name,ErrorStrings.Export_Title_AccountPeriod_Status,ErrorStrings.Export_Title_PartsInboundCheckBill_SettlementStatus,"售前检查次数","VIN码","内部编号","版次","系列","产品类别","驱动",
                                    "车牌号","驾送司机电话","驾加送司机姓名","事业部","服务产品线","费用","是否销服一体","经销商名称",ErrorStrings.Export_Title_AccountPeriod_CreateTime,ErrorStrings.Export_Title_AccountPeriod_CreatorName,ErrorStrings.Export_Title_Dealerpartsinventorybill_ApproverTime,ErrorStrings.Export_Title_Dealerpartsinventorybill_ApproverName,ErrorStrings.Export_Title_PartsBranch_AbandonerTime,ErrorStrings.Export_Title_PartsBranch_AbandonerName,ErrorStrings.Export_Title_AccountPeriod_ModifyTime,ErrorStrings.Export_Title_AccountPeriod_ModifierName
                                 };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }

                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }

        /// <summary>
        /// 批量导出售前检查单
        /// </summary>
        public bool ExportPreSaleCheckOrders(int? branchId, string code, int? status, int? partsSalesCategoryId, string vin, DateTime? createTimeBegin, DateTime? createTimeEnd, int? settleStatus, int[] preSaleCheckOrderIds, string dealerCode, string dealerName, out string fileName) {
            fileName = GetExportFilePath("售前检查单.xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated) {
                    throw new Exception(ErrorStrings.File_Validation_User);
                }
                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {

                    conn.Open();
                    var sql = new StringBuilder();
                    sql.Append(string.Format(@"select a.Code,--售前检查单编号
                                c.Name  as BranchName,--营销分公司
                                d.name  as PartsSalesCategoryName,--品牌
                                i.RegionName,
                                k.Name,
                                a.DealerCode,
                                a.DealerName,--服务站
                                cast(case a.Status--状态
                                when 1 then '新建'
                                when 2  then '提交'
                                when 3  then '已审核'
                                when 99  then '作废'
                                end as varchar2(20))  as status,
                                cast(decode(a.settleStatus,
                                    1,'不结算',
                                    2,'待结算',
                                    3,'已结算'
                                ) as varchar2(20)) as  settleStatus,
                                g.PreSaleTime,
                                a.VIN,--VIN码
                                b.FaultDescription,
                                b.RepairMethod,
                                b.ClaimUnit,
                                g.VehicleLicensePlate,--车牌号
                                a.InternalCode,
                                a.Revision,
                                a.Series,
                                a.ProductCategory,
                                a.Drive,
                                g.SerialNumber,
                                g.OutOfFactoryDate,
                                g.SalesDate,
                                a.CreateTime,
                                (case when g.presaletime is null
                                      then 0
                                      else g.presaletime
                                 end)+ct.a1,
                                a.DriverTel,--驾送司机电话
                                a.DriverName,--驾加送司机姓名
                                e.name  as ResponsibleUnitName,--事业部
                                f.name as ServiceProductLineName,--服务产品线
                                a.Cost,--费用
                                b.Category,--类别
                                b.CheckItem,--检查项目
                                b.FaultPattern,--故障项目    
                                a.IsSaleService,
                                a.SaleDealerName,                                                  
                                a.CreateTime,
                                a.CreatorName,
                                a.ApproveTime,
                                a.ApproverName,
                                a.ApprovertComment,
                                a.RejectTime,
                                a.RejectName,
                                a.RejectComment,
                                a.AbandonTime,
                                a.AbandonerName,
                                a.ModifyTime,
                                a.ModifierName
                                from PreSaleCheckOrder a
                                left join PreSalesCheckDetail b
                                on a.Id=b.PreSalesCheckId
                                left join branch c
                                on a.branchid=c.id
                                left join partssalescategory d
                                on a.partssalescategoryid=d.id
                                left join responsibleunit e
                                on a.responsibleunitid=e.id
                                left join serviceproductline f
                                on a.serviceproductlineid=f.id
                                left join VehicleInformation g
                                on g.id=a.vehicleid
                                left join RegionPersonnelRelation h
                                on h.id={0}
                                left join SalesRegion i
                                on i.id=h.SalesRegionId
                                left join MarketDptPersonnelRelation j
                                on j.id={0}
                                left join MarketingDepartment k
                                on k.id=j.MarketDepartmentId
                                left join (select extent2.vehicleid, count(1) as a1
                               from presalecheckorder extent2
                              where (extent2.status in (2, 1))
                              group by extent2.vehicleid) ct on  g.id = ct.vehicleid where 1=1 ", Utils.GetCurrentUserInfo().Id));
                    var dbParameters = new List<DbParameter>();

                    if(preSaleCheckOrderIds != null) {
                        sql.Append(@" and a.Id in ( ");
                        foreach(var preSaleCheckOrderId in preSaleCheckOrderIds) {
                            sql.Append(@"{0}preSaleCheckOrderId" + preSaleCheckOrderId + ", ");
                            dbParameters.Add(db.CreateDbParameter("preSaleCheckOrderId" + preSaleCheckOrderId, preSaleCheckOrderId));
                        }
                        sql.Append(@" -1) ");
                    } else {
                        if(branchId.HasValue) {
                            sql.Append(@" and a.branchId={0}branchId");
                            dbParameters.Add(db.CreateDbParameter("branchId", branchId.Value));
                        }
                        if(!string.IsNullOrEmpty(code)) {
                            sql.Append(@" and a.code ={0}code ");
                            dbParameters.Add(db.CreateDbParameter("code", code));
                        }
                        if(status.HasValue) {
                            sql.Append(@" and a.status ={0}Status");
                            dbParameters.Add(db.CreateDbParameter("status", status.Value));
                        }
                        if(settleStatus.HasValue) {
                            sql.Append(@" and a.settleStatus ={0}settleStatus");
                            dbParameters.Add(db.CreateDbParameter("settleStatus", settleStatus.Value));
                        }
                        if(partsSalesCategoryId.HasValue) {
                            sql.Append(@" and a.partsSalesCategoryId={0}partsSalesCategoryId");
                            dbParameters.Add(db.CreateDbParameter("partsSalesCategoryId", partsSalesCategoryId.Value));
                        }
                        if(!string.IsNullOrEmpty(vin)) {
                            sql.Append(@" and a.vin ={0}vin ");
                            dbParameters.Add(db.CreateDbParameter("vin", vin));
                        }
                        if(createTimeBegin.HasValue) {
                            sql.Append(@" and a.createTime >=to_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(@" and a.createTime <=to_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                        if(!string.IsNullOrEmpty(dealerCode)) {
                            sql.Append(@" and a.dealerCode ={0}dealerCode ");
                            dbParameters.Add(db.CreateDbParameter("dealerCode", dealerCode));
                        }
                        if(!string.IsNullOrEmpty(dealerName)) {
                            sql.Append(@" and a.dealerName ={0}dealerName ");
                            dbParameters.Add(db.CreateDbParameter("dealerName", dealerName));
                        }
                    }
                    sql.Append(@" order by a.code");
                    var sqlStr = sql.ToString();
                    if(sqlStr.IndexOf(" where 1=1  and ") > 0)
                        sqlStr = sqlStr.Replace(" where 1=1  and ", " where ");
                    else
                        sqlStr = sqlStr.Replace(" where 1=1 ", "");
                    var command = db.CreateDbCommand(string.Format(sqlStr, db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    "售前检查单编号", ErrorStrings.Export_Title_PartsBranch_BranchName, ErrorStrings.Export_Title_Partssalescategory_Name,"大区","市场部",ErrorStrings.Export_Title_Dealer_Code, ErrorStrings.Export_Title_Dealer_Name, ErrorStrings.Export_Title_AccountPeriod_Status,ErrorStrings.Export_Title_PartsInboundCheckBill_SettlementStatus,"售前检查次数", "VIN码","故障描述","维修方式","索赔单位",
                                    "车牌号","内部编号","版次","系列","产品类别","驱动","出厂编号","生产日期","发车日期","检查时间","售前检查次数", "驾送司机电话", "驾加送司机姓名",
                                    "事业部", "服务产品线", "费用", "类别", "检查项目", "故障项目","是否销服一体","经销商名称",ErrorStrings.Export_Title_AccountPeriod_CreateTime,ErrorStrings.Export_Title_AccountPeriod_CreatorName,ErrorStrings.Export_Title_Dealerpartsinventorybill_ApproverTime,ErrorStrings.Export_Title_Dealerpartsinventorybill_ApproverName,ErrorStrings.Export_Title_PartsOuterPurchaseChange_ApproveAmont,ErrorStrings.Export_Title_PartsOuterPurchaseChange_RejectTime,ErrorStrings.Export_Title_PartsOuterPurchaseChange_RejecterName,"驳回意见",ErrorStrings.Export_Title_PartsBranch_AbandonerTime,ErrorStrings.Export_Title_PartsBranch_AbandonerName,ErrorStrings.Export_Title_AccountPeriod_ModifyTime,ErrorStrings.Export_Title_AccountPeriod_ModifierName
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }
    }
}
