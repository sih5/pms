﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        /// <summary>
        /// 导入售前检查项目
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="excelImportNum"></param>
        /// <param name="branchId" >营销分公司Id</param>
        /// <param name="partsSalesCategoryId">配件销售类型Id</param>
        /// <param name="rightData"></param>
        /// <param name="errorData"></param>
        /// <param name="errorDataFileName"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public bool ImportPreSaleItem(string fileName, out int excelImportNum, int branchId, int partsSalesCategoryId, out List<PreSaleItemExtend> rightData, out List<PreSaleItemExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<PreSaleItemExtend>();
            var allList = new List<PreSaleItemExtend>();
            var rightList = new List<PreSaleItemExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("PreSaleItem", out notNullableFields, out fieldLenght);
                //获取服务产品线表结构
                List<string> notNullableServiceProductLineViewFields;
                Dictionary<string, int> fieldServiceProductLineViewLenght;
                db.GetTableSchema("ServiceProductLineView", out notNullableServiceProductLineViewFields, out fieldServiceProductLineViewLenght);

                List<object> excelColumns;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    #region 指定文件中的列对应的名称 例如 ：excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode, "PartCode");
                    excelOperator.AddColumnDataSource("服务产品线", "ProductLineName");
                    excelOperator.AddColumnDataSource("类别", "Category");
                    excelOperator.AddColumnDataSource("检查项目", "CheckItem");
                    excelOperator.AddColumnDataSource("故障模式", "FaultPattern");

                    #endregion
                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;
                    #region 获取对应枚举 例如：
                    var keyValuePairs = new[] { new KeyValuePair<string, string>("BaseDataStatus", "BaseData_Status") };
                    tempExcelOperator.ImportHelper.LoadEnumFromDb(null,null, keyValuePairs);

                    #endregion
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var tempImportObj = new PreSaleItemExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        #region 根据导入的字段给对应的实体属性赋值
                        tempImportObj.ServiceProductLineNameStr = newRow["ProductLineName"];
                        tempImportObj.CategoryStr = newRow["Category"];
                        tempImportObj.CheckItemStr = newRow["CheckItem"];
                        tempImportObj.FaultPatternStr = newRow["FaultPattern"];

                        #endregion

                        var tempErrorMessage = new List<string>();
                        #region 导入的数据基本检查
                        //服务产品线校验
                        var fieldIndex = notNullableServiceProductLineViewFields.IndexOf("ProductLineName".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.ServiceProductLineNameStr)) {
                            if(fieldIndex > -1) {
                                tempErrorMessage.Add("服务产品线不能为空");
                            }
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.ServiceProductLineNameStr) > fieldServiceProductLineViewLenght["ProductLineName".ToUpper()])
                                tempErrorMessage.Add("服务产品线过长");
                        }
                        //检查项目校验
                        fieldIndex = notNullableFields.IndexOf("Category".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.CategoryStr)) {
                            if(fieldIndex > -1) {
                                tempErrorMessage.Add("类别不能为空");
                            }
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.CategoryStr) > fieldLenght["Category".ToUpper()])
                                tempErrorMessage.Add("类别过长");
                        }
                        //检查项目校验
                        fieldIndex = notNullableFields.IndexOf("CheckItem".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.CheckItemStr)) {
                            if(fieldIndex > -1) {
                                tempErrorMessage.Add("检查项目不能为空");
                            }
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.CheckItemStr) > fieldLenght["CheckItem".ToUpper()])
                                tempErrorMessage.Add("检查项目过长");
                        }
                        //故障现象校验
                        fieldIndex = notNullableFields.IndexOf("FaultPattern".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.FaultPatternStr)) {
                            if(fieldIndex > -1) {
                                tempErrorMessage.Add("故障模式不能为空");
                            }
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.FaultPatternStr) > fieldLenght["FaultPattern".ToUpper()])
                                tempErrorMessage.Add("故障模式过长");
                        }

                        #endregion
                        if(tempErrorMessage.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });
                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    #region 剩下的数据进行业务检查
                    //1.查询售前检查项目（售前检查项目.营销分公司id=参数 营销分公司id，售前检查项目.配件销售类型Id=参数 配件销售类型Id,
                    //售前检查项目.类别=导入数据.类别，售前检查项目.检查项目=导入数据.检查项目，售前检查项目.故障现象=导入数据.故障现象）
                    //如果存在返回数据，校验不通过：售前检查项目已存在，无法新增。
                    var productLineNameNeedCheck = tempRightList.Select(r => r.ServiceProductLineNameStr.ToString(CultureInfo.InvariantCulture)).Distinct().ToArray();
                    var dbServiceProductLineViews = new List<ServiceProductLineViewExtend>();
                    Func<string[], bool> getDbServiceProductLineViews = values => {
                        dbServiceProductLineViews.Add(new ServiceProductLineViewExtend {
                            ServiceProductLineId = Convert.ToInt32(values[0]),
                            ProductLineName = values[1],
                            PartsSalesCategoryId = int.Parse(values[2])
                        });
                        return false;
                    };
                    db.QueryDataWithInOperator("select ProductLineId,ProductLineName,PartsSalesCategoryId from ServiceProductLineView", "ProductLineName", false, productLineNameNeedCheck, getDbServiceProductLineViews);
                    foreach(var tempRight in tempRightList) {
                        var productLineName = dbServiceProductLineViews.FirstOrDefault(v => v.ProductLineName == tempRight.ServiceProductLineNameStr && v.PartsSalesCategoryId == partsSalesCategoryId);
                        if(productLineName == null) {
                            tempRight.ErrorMsg = "服务产品线不存在";
                            continue;
                        }
                        tempRight.ServiceProductLineId = productLineName.ServiceProductLineId;
                    }

                    var categorysNeedCheck = tempRightList.Select(r => r.CategoryStr.ToString(CultureInfo.InvariantCulture)).Distinct().ToArray();
                    var dbPreSaleItems = new List<PreSaleItemExtend>();
                    Func<string[], bool> getDbPreSaleItems = value => {
                        var dbObj = new PreSaleItemExtend {
                            BranchId = Convert.ToInt32(value[0]),
                            PartsSalesCategoryId = Convert.ToInt32(value[1]),
                            Category = value[2],
                            CheckItem = value[3],
                            FaultPattern = value[4]
                        };
                        dbPreSaleItems.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select BranchId,PartsSalesCategoryId,Category,CheckItem,FaultPattern from PreSaleItem where status=1 ", "Category", false, categorysNeedCheck, getDbPreSaleItems);
                    foreach(var tempRight in tempRightList) {
                        var preSaleItem = dbPreSaleItems.FirstOrDefault(v => v.BranchId == branchId && v.PartsSalesCategoryId == partsSalesCategoryId && v.ServiceProductLineId == tempRight.ServiceProductLineId && v.Category == tempRight.Category && v.CheckItem == tempRight.CheckItem && v.FaultPattern == tempRight.FaultPattern);
                        if(preSaleItem != null) {
                            tempRight.ErrorMsg = "售前检查项目已存在，无法新增";
                        }
                    }

                    #endregion
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    //获取所有合格数据
                    rightList = allList.Except(errorList).ToList();
                    foreach(var rightItem in rightList) {
                        rightItem.Category = rightItem.CategoryStr;
                        rightItem.CheckItem = rightItem.CheckItemStr;
                        rightItem.FaultPattern = rightItem.FaultPatternStr;
                        rightItem.Status = tempExcelOperator.ImportHelper.GetEnumValue("BaseDataStatus", ErrorStrings.Export_Validation_PartsBranch_Used) ?? 0;
                    }
                }
                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempObj = list[index - 1];
                            var values = new object[] {
                                #region 设置错误信息导出的列的值
                                tempObj.CategoryStr,tempObj.CheckItemStr,
                                tempObj.FaultPatternStr,tempObj.ErrorMsg
                                #endregion                     
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }

                //导入所有合格数据
                if(!rightList.Any())
                    return true;
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    //开启事务，新增更新配件在一个事务内
                    var ts = conn.BeginTransaction();
                    try {
                        //新增配件
                        if(rightList.Any()) {
                            #region 获取对应的插入的SQL 通过db.GetInsertSql方法
                            //获取新增数据的sql语句，Id为主键
                            var sqlInsert = db.GetInsertSql("PreSaleItem", "Id", new[] {
                                "BranchId","PartsSalesCategoryId","ServiceProductLineId","Category","CheckItem","FaultPattern","Status"
                            });
                            #endregion
                            //往数据库增加配件信息
                            foreach(var item in rightList) {
                                #region 添加Sql的参数
                                var command = db.CreateDbCommand(sqlInsert, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("BranchId", branchId));
                                command.Parameters.Add(db.CreateDbParameter("PartsSalesCategoryId", partsSalesCategoryId));
                                command.Parameters.Add(db.CreateDbParameter("ServiceProductLineId", item.ServiceProductLineId));
                                command.Parameters.Add(db.CreateDbParameter("Category", item.Category));
                                command.Parameters.Add(db.CreateDbParameter("CheckItem", item.CheckItem));
                                command.Parameters.Add(db.CreateDbParameter("FaultPattern", item.FaultPattern));
                                command.Parameters.Add(db.CreateDbParameter("Status", item.Status));
                                command.ExecuteNonQuery();
                                #endregion
                            }
                        }
                        //无异常提交
                        ts.Commit();
                    } catch(Exception) {
                        //报错回滚
                        ts.Rollback();
                        throw;
                    } finally {
                        if(conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }
    }
}
