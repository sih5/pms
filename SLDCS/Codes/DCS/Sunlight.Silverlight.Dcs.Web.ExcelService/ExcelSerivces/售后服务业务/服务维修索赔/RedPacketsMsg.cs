﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        public bool 导出红包信息(int[] ids, string couponNo, string title, string tel, int? brandId, DateTime? createTimeBegin, DateTime? createTimeEnd, int? type, string lssuingUnit, string vin, out string fileName) {
            fileName = GetExportFilePath("红包信息.xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);
                using(var conn = db.CreateDbConnection()) {
                    var sql = new StringBuilder();
                    sql.AppendFormat(@"Select CouponNo,
                                               Title,
                                               Tel,
                                               VIN,
                                               BrandName,
                                               k1.value as LssuingUnit,
                                               cast(Case RedPacketsType
                                                      When 1 Then
                                                       '普通红包'
                                                      When 2 Then
                                                       '保养红包'
                                                      When 3 Then
                                                       '终免工时'
                                                    End as varchar2(50)) as RedPacketsType,
                                               cast(Case RedPacketsRange
                                                      When 1 Then
                                                       '828活动'
                                                      When 2 Then
                                                       '非828活动'
                                                    End as varchar2(50)) as RedPacketsRange,
                                               UsedPrice,
                                               StartTime,
                                               EndTime,
                                               LimitPrice,
                                               cast(Case State
                                                      When 0 Then
                                                       '未使用'
                                                      When 1 Then
                                                       '已使用'
                                                    End as varchar2(50)),
                                               RepairCode,
                                               CreatorName,
                                               CreateTime
                                          From RedPacketsMsg a
                                          left join Keyvalueitem k1
                                            on a.LssuingUnit = k1.key
                                           and k1.name = 'LssuingUnit'
                                         where 1 = 1 ");
                    var dbParameters = new List<DbParameter>();
                    if(ids != null && ids.Length > 0) {
                        sql.Append(" and a.Id in (");
                        for(var i = 0; i < ids.Length; i++) {
                            if(ids.Length == i + 1) {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            } else {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                        }
                        sql.Append(")");
                    } else {
                        if(!string.IsNullOrEmpty(couponNo)) {
                            sql.Append(" and Lower(CouponNo) like {0}couponNo ");
                            dbParameters.Add(db.CreateDbParameter("couponNo", "%" + couponNo.ToLower() + "%"));
                        }
                        if(!string.IsNullOrEmpty(title)) {
                            sql.Append(" and Lower(Title) like {0}title ");
                            dbParameters.Add(db.CreateDbParameter("title", "%" + title.ToLower() + "%"));
                        }
                        if(!string.IsNullOrEmpty(vin)) {
                            sql.Append(" and Lower(VIN) like {0}vin ");
                            dbParameters.Add(db.CreateDbParameter("vin", "%" + vin.ToLower() + "%"));
                        }
                        if(!string.IsNullOrEmpty(lssuingUnit)) {
                            sql.Append(" and LssuingUnit={0}lssuingUnit");
                            dbParameters.Add(db.CreateDbParameter("lssuingUnit", lssuingUnit));
                        }
                        if(!string.IsNullOrEmpty(tel)) {
                            sql.Append(" and Lower(Tel) like {0}tel ");
                            dbParameters.Add(db.CreateDbParameter("tel", "%" + tel.ToLower() + "%"));
                        }
                        if(type.HasValue) {
                            sql.Append(" and RedPacketsType={0}RedPacketsType");
                            dbParameters.Add(db.CreateDbParameter("RedPacketsType", type));
                        }
                        if(brandId.HasValue) {
                            sql.Append(" and BrandId={0}brandId");
                            dbParameters.Add(db.CreateDbParameter("brandId", brandId));
                        }
                        if(createTimeBegin.HasValue) {
                            sql.Append(@" and CreateTime >=to_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(@" and CreateTime <=to_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                    }
                    var sqlStr = sql.ToString();
                    if(sqlStr.IndexOf(" where 1=1  and ") > 0)
                        sqlStr = sqlStr.Replace(" where 1=1  and ", " where ");
                    else
                        sqlStr = sqlStr.Replace(" where 1=1 ", "");
                    var command = db.CreateDbCommand(string.Format(sqlStr, db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    conn.Open();
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    "红包编号","红包名称",ErrorStrings.Export_Title_PersonSalesCenterLink_CellNumber,"VIN",ErrorStrings.Export_Title_Partssalescategory_Name,"发放单位","红包类型","红包范围","红包金额",ErrorStrings.Export_Title_Credenceapplication_ValidationDate,ErrorStrings.Export_Title_Credenceapplication_ExpireDate,"使用金额上限"
                                     ,"使用状态","维修单编号",ErrorStrings.Export_Title_AccountPeriod_CreatorName,ErrorStrings.Export_Title_AccountPeriod_CreateTime
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }
    }
}
