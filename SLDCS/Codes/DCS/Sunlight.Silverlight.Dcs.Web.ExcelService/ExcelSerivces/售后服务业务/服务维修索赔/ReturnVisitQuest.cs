﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Globalization;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        //回访问卷 导出
        public bool ExportReturnVisitQuests(int[] billIds, string orderId, string dealerCode, int? partsSalesCategoryId, string dealerName, int? type, int? marketDepartmentId, DateTime? revisitDaysBegin, DateTime? revisitDaysEnd, out string fileName) {
            fileName = GetExportFilePath("回访问卷_" + ".xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {
                    var sql = new StringBuilder();
                    #region SQL
                    sql.AppendFormat(@" select 
                                        a.OrderId,/*单据编号*/
                                        cast(decode(a.Type,1,'维修单',2,'工单','') as varchar2(20)) as Type,/*单据类型*/
                                        c.Name,/*品牌*/
                                        b.Name,/*市场部*/
                                        a.DealerCode,/*服务站编号*/
                                        a.DealerName,/*服务站名称*/
                                        decode(a.IsConnection,1,'是',0,'否','') as IsConnection,/*接通标识*/
                                        a.FailReason,/*接通失败原因*/
                                        decode(a.IsTrue,1,'是',0,'否','') as IsTrue,/*是否真实*/
                                        a.Description,/*不真实说明*/
                                        a.Score,/*满意度*/
                                        a.SurverName, /*问卷名称*/
                                        a.RevisitDays/*回访时间*/
                                        FROM ReturnVisitQuest a
                                        left join MarketingDepartment b on b.id=a.MarketDepartmentId
                                        left join PartsSalesCategory c on a.PartsSalesCategoryId=c.id where 1=1 ");
                    #endregion
                    var dbParameters = new List<DbParameter>();
                    #region 条件过滤
                    if(billIds != null && billIds.Length > 0) {
                        sql.Append(" and a.id in (");
                        for(var i = 0; i < billIds.Length; i++) {
                            if(billIds.Length == i + 1) {
                                sql.Append("{0}id" + billIds[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter("id" + billIds[i].ToString(CultureInfo.InvariantCulture), billIds[i]));
                            } else {
                                sql.Append("{0}id" + billIds[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter("id" + billIds[i].ToString(CultureInfo.InvariantCulture), billIds[i]));
                            }
                        }
                        sql.Append(")");
                    } else {
                        if(!string.IsNullOrEmpty(orderId)) {
                            sql.Append(" and Upper(a.orderId) like Upper({0}orderId) ");
                            dbParameters.Add(db.CreateDbParameter("orderId", "%" + orderId + "%"));
                        }
                        if(!string.IsNullOrEmpty(dealerCode)) {
                            sql.Append(" and Upper(a.dealerCode) like Upper({0}dealerCode) ");
                            dbParameters.Add(db.CreateDbParameter("dealerCode", "%" + dealerCode + "%"));
                        }
                        if(!string.IsNullOrEmpty(dealerName)) {
                            sql.Append(" and a.dealerName like {0}dealerName ");
                            dbParameters.Add(db.CreateDbParameter("dealerName", "%" + dealerName + "%"));
                        }
                        if(partsSalesCategoryId.HasValue) {
                            sql.Append(" and a.partsSalesCategoryId={0}partsSalesCategoryId");
                            dbParameters.Add(db.CreateDbParameter("partsSalesCategoryId", partsSalesCategoryId.Value));
                        }
                        if(type.HasValue) {
                            sql.Append(" and a.type={0}type");
                            dbParameters.Add(db.CreateDbParameter("type", type.Value));
                        }
                        if(marketDepartmentId.HasValue) {
                            sql.Append(" and a.marketDepartmentId={0}marketDepartmentId");
                            dbParameters.Add(db.CreateDbParameter("marketDepartmentId", marketDepartmentId.Value));
                        }
                        if(revisitDaysBegin.HasValue) {
                            sql.Append(" and a.RevisitDays>=to_date({0}revisitDaysBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = revisitDaysBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("revisitDaysBegin", tempTime.ToString("G")));
                        }
                        if(revisitDaysEnd.HasValue) {
                            sql.Append(" and a.RevisitDays<=to_date({0}revisitDaysEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = revisitDaysEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("revisitDaysEnd", tempTime.ToString("G")));
                        }
                    }
                    #endregion
                    sql.Append(" order by a.id");
                    var sqlStr = sql.ToString();
                    if(sqlStr.IndexOf(" where 1=1  and ") > 0)
                        sqlStr = sqlStr.Replace(" where 1=1  and ", " where ");
                    else
                        sqlStr = sqlStr.Replace(" where 1=1 ", "");
                    var command = db.CreateDbCommand(string.Format(sqlStr, db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    conn.Open();
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new[] { ErrorStrings.Export_Title_PackingPropertyApp_Code, "单据类型", ErrorStrings.Export_Title_Partssalescategory_Name, "市场部", ErrorStrings.Export_Title_Dealer_Code, ErrorStrings.Export_Title_Dealer_Name, "接通标识", "接通失败原因", "是否真实", "不真实说明", "满意度", "问卷名称", "回访时间" };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }

        //回访问卷 合并导出
        public bool ExportReturnVisitQuestWithDetails(int[] billIds, string orderId, string dealerCode, int? partsSalesCategoryId, string dealerName, int? type, int? marketDepartmentId, DateTime? revisitDaysBegin, DateTime? revisitDaysEnd, out string fileName) {
            fileName = GetExportFilePath("配件销售结算主清单_" + ".xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {
                    var sql = new StringBuilder();
                    #region SQL
                    sql.AppendFormat(@" select 
                                        a.OrderId,/*单据编号*/
                                        cast(decode(a.Type,1,'维修单',2,'工单','') as varchar2(20)) as Type,/*单据类型*/
                                        c.Name,/*品牌*/
                                        b.Name,/*市场部*/
                                        a.DealerCode,/*服务站编号*/
                                        a.DealerName,/*服务站名称*/
                                        decode(a.IsConnection,1,'是',0,'否','') as IsConnection,/*接通标识*/
                                        a.FailReason,/*接通失败原因*/
                                        decode(a.IsTrue,1,'是',0,'否','') as IsTrue,/*是否真实*/
                                        a.Description,/*不真实说明*/
                                        a.Score,/*满意度*/
                                        a.SurverName, /*问卷名称*/
                                        a.RevisitDays,/*回访时间*/
                                        d.QUEST,
                                        d.Choice,
                                        d.ChoiceScore,
                                        d.TextResponse
                                        FROM ReturnVisitQuest a
                                        left join ReturnVisitQuestDetail d on d.ReturnVisitQuestId = a.Id
                                        left join MarketingDepartment b on b.id=a.MarketDepartmentId
                                        left join PartsSalesCategory c on a.PartsSalesCategoryId=c.id where 1=1 ");
                    #endregion
                    var dbParameters = new List<DbParameter>();
                    #region 条件过滤
                    if(billIds != null && billIds.Length > 0) {
                        sql.Append(" and a.id in (");
                        for(var i = 0; i < billIds.Length; i++) {
                            if(billIds.Length == i + 1) {
                                sql.Append("{0}id" + billIds[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter("id" + billIds[i].ToString(CultureInfo.InvariantCulture), billIds[i]));
                            } else {
                                sql.Append("{0}id" + billIds[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter("id" + billIds[i].ToString(CultureInfo.InvariantCulture), billIds[i]));
                            }
                        }
                        sql.Append(")");
                    } else {
                        if(!string.IsNullOrEmpty(orderId)) {
                            sql.Append(" and Upper(a.orderId) like Upper({0}orderId) ");
                            dbParameters.Add(db.CreateDbParameter("orderId", "%" + orderId + "%"));
                        }
                        if(!string.IsNullOrEmpty(dealerCode)) {
                            sql.Append(" and Upper(a.dealerCode) like Upper({0}dealerCode) ");
                            dbParameters.Add(db.CreateDbParameter("dealerCode", "%" + dealerCode + "%"));
                        }
                        if(!string.IsNullOrEmpty(dealerName)) {
                            sql.Append(" and a.dealerName like {0}dealerName ");
                            dbParameters.Add(db.CreateDbParameter("dealerName", "%" + dealerName + "%"));
                        }
                        if(partsSalesCategoryId.HasValue) {
                            sql.Append(" and a.partsSalesCategoryId={0}partsSalesCategoryId");
                            dbParameters.Add(db.CreateDbParameter("partsSalesCategoryId", partsSalesCategoryId.Value));
                        }
                        if(type.HasValue) {
                            sql.Append(" and a.type={0}type");
                            dbParameters.Add(db.CreateDbParameter("type", type.Value));
                        }
                        if(marketDepartmentId.HasValue) {
                            sql.Append(" and a.marketDepartmentId={0}marketDepartmentId");
                            dbParameters.Add(db.CreateDbParameter("marketDepartmentId", marketDepartmentId.Value));
                        }
                        if(revisitDaysBegin.HasValue) {
                            sql.Append(" and a.RevisitDays>=to_date({0}revisitDaysBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = revisitDaysBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("revisitDaysBegin", tempTime.ToString("G")));
                        }
                        if(revisitDaysEnd.HasValue) {
                            sql.Append(" and a.RevisitDays<=to_date({0}revisitDaysEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = revisitDaysEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("revisitDaysEnd", tempTime.ToString("G")));
                        }
                    }
                    #endregion
                    sql.Append(" order by a.id");
                    var sqlStr = sql.ToString();
                    if(sqlStr.IndexOf(" where 1=1  and ") > 0)
                        sqlStr = sqlStr.Replace(" where 1=1  and ", " where ");
                    else
                        sqlStr = sqlStr.Replace(" where 1=1 ", "");
                    var command = db.CreateDbCommand(string.Format(sqlStr, db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    conn.Open();
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new[] { ErrorStrings.Export_Title_PackingPropertyApp_Code, "单据类型", ErrorStrings.Export_Title_Partssalescategory_Name, "市场部", ErrorStrings.Export_Title_Dealer_Code, ErrorStrings.Export_Title_Dealer_Name, "接通标识", "接通失败原因", "是否真实", "不真实说明", "满意度", "问卷名称", "回访时间", "问题", "选择答案", "答复得分", "文本答复" };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }

    }
}
