﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        /// <summary>
        /// 批量导入供应商扣补款单
        /// </summary>
        public bool ImportSupplierExpenseAdjustBill(string fileName, out int excelImportNum, int branchId, out List<SupplierExpenseAdjustBillExtend> rightData, out List<SupplierExpenseAdjustBillExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<SupplierExpenseAdjustBillExtend>();
            var rightList = new List<SupplierExpenseAdjustBillExtend>();
            var allList = new List<SupplierExpenseAdjustBillExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("SupplierExpenseAdjustBill", out notNullableFields, out fieldLenght);

                List<string> notNullableFieldsPartsSupplier;
                Dictionary<string, int> fieldLenghtPartsSupplier;
                db.GetTableSchema("PartsSupplier", out notNullableFieldsPartsSupplier, out fieldLenghtPartsSupplier);


                List<string> notNullableFieldsResponsibleUnit;
                Dictionary<string, int> fieldLenghtResponsibleUnit;
                db.GetTableSchema("ResponsibleUnit", out notNullableFieldsResponsibleUnit, out fieldLenghtResponsibleUnit);

                List<string> notNullableFieldsServiceProductLineView;
                Dictionary<string, int> fieldLenghtServiceProductLineView;
                db.GetTableSchema("ServiceProductLineView", out notNullableFieldsServiceProductLineView, out fieldLenghtServiceProductLineView);

                List<string> notNullableFieldsPartsSalesCategory;
                Dictionary<string, int> fieldLenghtPartsSalesCategory;
                db.GetTableSchema("PartsSalesCategory", out notNullableFieldsPartsSalesCategory, out fieldLenghtPartsSalesCategory);

                List<object> excelColumns;

                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    #region 指定文件中的列对应的名称 例如 excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode, "PartCode");

                    excelOperator.AddColumnDataSource("扣补款类型", "TransactionCategory");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Partshistorystock_BranchName, "BranchName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Partssalescategory_Name, "PartsSalesCategoryName");
                    excelOperator.AddColumnDataSource("产品线", "ServiceProductLineName");
                    excelOperator.AddColumnDataSource("产品线类型", "ProductLineType");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_FactoryPurchacePrice_SupplierName, "SupplierName");
                    excelOperator.AddColumnDataSource("供应商联系人", "SupplierContactPerson");
                    excelOperator.AddColumnDataSource("供应商联系电话", "SupplierPhoneNumber");
                    excelOperator.AddColumnDataSource("供应商联系地址", "SupplierAddress");
                    excelOperator.AddColumnDataSource("扣补款方向", "DebitOrReplenish");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsOuterPurchaseChange_SourceCategoryr, "SourceType");
                    excelOperator.AddColumnDataSource("源单据", "SourceCode");
                    excelOperator.AddColumnDataSource("扣补款金额", "TransactionAmount");
                    excelOperator.AddColumnDataSource("向责任单位索赔", "IfClaimToResponsible");
                    excelOperator.AddColumnDataSource("扣补款原因", "TransactionReason");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Remark, "Memo");
                    #endregion

                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;

                    #region 获取对应枚举
                    var keyValuePairs = new[] {
                            new KeyValuePair<string, string>("DebitOrReplenish", "ExpenseAdjustmentBill_DebitOrReplenish"),
                            new KeyValuePair<string, string>("TransactionCategory","ExpenseAdjustmentBill_TransactionCategory"),
                            new KeyValuePair<string, string>("Status","ExpenseAdjustmentBill_Status"),
                            new KeyValuePair<string, string>("SourceType","ExpenseAdjustmentBill_SourceType"),
                            new KeyValuePair<string, string>("ProductLineType","ServiceProductLineView_ProductLineType")
                        };
                    tempExcelOperator.ImportHelper.LoadEnumFromDb(null,null, keyValuePairs);

                    #endregion

                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var tempImportObj = new SupplierExpenseAdjustBillExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        #region
                        tempImportObj.TransactionCategoryStr = newRow["TransactionCategory"];
                        tempImportObj.BranchNameStr = newRow["BranchName"];
                        tempImportObj.PartsSalesCategoryNameStr = newRow["PartsSalesCategoryName"];
                        tempImportObj.ServiceProductLineNameStr = newRow["ServiceProductLineName"];
                        tempImportObj.ProductLineTypeStr = newRow["ProductLineType"];
                        tempImportObj.SupplierNameStr = newRow["SupplierName"];
                        tempImportObj.SupplierContactPersonStr = newRow["SupplierContactPerson"];
                        tempImportObj.SupplierPhoneNumberStr = newRow["SupplierPhoneNumber"];
                        tempImportObj.SupplierAddressStr = newRow["SupplierAddress"];
                        tempImportObj.DebitOrReplenishStr = newRow["DebitOrReplenish"];
                        tempImportObj.SourceTypeStr = newRow["SourceType"];
                        tempImportObj.SourceCodeStr = newRow["SourceCode"];
                        tempImportObj.TransactionAmountStr = newRow["TransactionAmount"];
                        tempImportObj.IfClaimToResponsibleStr = newRow["IfClaimToResponsible"];
                        tempImportObj.TransactionReasonStr = newRow["TransactionReason"];
                        tempImportObj.MemoStr = newRow["Memo"];
                        #endregion

                        var tempErrorMessage = new List<string>();

                        #region 导入的数据基本检查
                        var fieldIndex = notNullableFields.IndexOf("TRANSACTIONCATEGORY");
                        if(string.IsNullOrEmpty(tempImportObj.TransactionCategoryStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add("扣补款类型不能为空");
                        } else {
                            var tempEnumValue = tempExcelOperator.ImportHelper.GetEnumValue("TransactionCategory", tempImportObj.TransactionCategoryStr);
                            if(!tempEnumValue.HasValue) {
                                tempErrorMessage.Add("扣补款类型不正确");
                            } else {
                                tempImportObj.TransactionCategory = tempEnumValue.Value;
                            }
                        }

                        fieldIndex = notNullableFields.IndexOf("BRANCHNAME");
                        if(string.IsNullOrEmpty(tempImportObj.BranchNameStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Branch_BranchNameIsNull);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.BranchNameStr) > fieldLenght["BRANCHNAME"])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Branch_BranchNameIsLong);
                        }

                        fieldIndex = notNullableFields.IndexOf("PARTSSALESCATEGORYID");
                        if(string.IsNullOrEmpty(tempImportObj.PartsSalesCategoryNameStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_PartsSalescategoryNameIsNull);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.PartsSalesCategoryNameStr) > fieldLenghtPartsSalesCategory["NAME"])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_PartsSalescategoryNameIsLong);
                        }

                        fieldIndex = notNullableFields.IndexOf("SERVICEPRODUCTLINEID");
                        if(string.IsNullOrEmpty(tempImportObj.ServiceProductLineNameStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add("产品线类型名称不能为空");
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.ServiceProductLineNameStr) > fieldLenghtServiceProductLineView["PRODUCTLINENAME"])
                                tempErrorMessage.Add("产品线类型名称过长");
                        }

                        fieldIndex = notNullableFields.IndexOf("PRODUCTLINETYPE");
                        if(string.IsNullOrEmpty(tempImportObj.ProductLineTypeStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add("产品线类型不能为空");
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.ProductLineTypeStr) > fieldLenght["PRODUCTLINETYPE"])
                                tempErrorMessage.Add("产品线类型过长");
                        }

                        fieldIndex = notNullableFieldsPartsSupplier.IndexOf("NAME");
                        if(string.IsNullOrEmpty(tempImportObj.SupplierNameStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_BranchSupplierRelation_SupplierNameIsNull);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.SupplierNameStr) > fieldLenghtPartsSupplier["NAME"])
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchaseOrderDetail_Validation16);
                        }

                        fieldIndex = notNullableFields.IndexOf("SUPPLIERCONTACTPERSON");
                        if(string.IsNullOrEmpty(tempImportObj.SupplierContactPersonStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add("供应商联系人不能为空");
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.SupplierContactPersonStr) > fieldLenght["SUPPLIERCONTACTPERSON"])
                                tempErrorMessage.Add("供应商联系人过长");
                        }

                        fieldIndex = notNullableFields.IndexOf("SUPPLIERPHONENUMBER");
                        if(string.IsNullOrEmpty(tempImportObj.SupplierPhoneNumberStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add("供应商联系电话不能为空");
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.SupplierPhoneNumberStr) > fieldLenght["SUPPLIERPHONENUMBER"])
                                tempErrorMessage.Add("供应商联系电话过长");
                        }

                        fieldIndex = notNullableFields.IndexOf("SUPPLIERADDRESS");
                        if(string.IsNullOrEmpty(tempImportObj.SupplierAddressStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add("供应商联系地址不能为空");
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.SupplierAddressStr) > fieldLenght["SUPPLIERADDRESS"])
                                tempErrorMessage.Add("供应商联系地址过长");
                        }

                        fieldIndex = notNullableFields.IndexOf("DEBITORREPLENISH");
                        if(string.IsNullOrEmpty(tempImportObj.DebitOrReplenishStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add("扣补款方向不能为空");
                        } else {

                            var tempEnumValue = tempExcelOperator.ImportHelper.GetEnumValue("DebitOrReplenish", tempImportObj.DebitOrReplenishStr);
                            if(!tempEnumValue.HasValue)
                                tempErrorMessage.Add("扣补款方向不正确");
                            else
                                tempImportObj.DebitOrReplenish = tempEnumValue.Value;
                        }

                        fieldIndex = notNullableFields.IndexOf("SOURCETYPE");
                        if(string.IsNullOrEmpty(tempImportObj.SourceTypeStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add("源单据类型不能为空");
                        } else {
                            var tempEnumValue = tempExcelOperator.ImportHelper.GetEnumValue("SourceType", tempImportObj.SourceTypeStr);
                            if(!tempEnumValue.HasValue)
                                tempErrorMessage.Add("源单据类型不正确");
                            else
                                tempImportObj.SourceType = tempEnumValue.Value;
                        }

                        fieldIndex = notNullableFields.IndexOf("SOURCECODE");
                        if(string.IsNullOrEmpty(tempImportObj.SourceCodeStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add("源单据编号不能为空");
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.SourceCodeStr) > fieldLenght["SOURCECODE"])
                                tempErrorMessage.Add("源单据编号过长");
                        }

                        fieldIndex = notNullableFields.IndexOf("TRANSACTIONAMOUNT");
                        if(string.IsNullOrEmpty(tempImportObj.TransactionAmountStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add("扣补款金额不能为空");
                        } else {
                            decimal checkValue;
                            if(decimal.TryParse(tempImportObj.TransactionAmountStr, out checkValue)) {
                                if(checkValue <= 0)
                                    tempErrorMessage.Add("扣补款金额必须大于0");
                                else
                                    tempImportObj.TransactionAmount = checkValue;
                            } else {
                                tempErrorMessage.Add("扣补款金额必须是数字");
                            }
                        }

                        fieldIndex = notNullableFields.IndexOf("IFCLAIMTORESPONSIBLE");
                        if(string.IsNullOrEmpty(tempImportObj.IfClaimToResponsibleStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add("向责任单位索赔不能为空");
                        } else {
                            if(tempImportObj.IfClaimToResponsibleStr == ErrorStrings.Export_Title_PartsBranch_Yes) {
                                tempImportObj.IfClaimToResponsible = true;
                            } else if(tempImportObj.IfClaimToResponsibleStr == ErrorStrings.Export_Title_PartsBranch_No) {
                                tempImportObj.IfClaimToResponsible = false;
                            } else {
                                tempErrorMessage.Add("向责任单位索赔不正确");
                            }
                        }

                        fieldIndex = notNullableFields.IndexOf("TRANSACTIONREASON");
                        if(string.IsNullOrEmpty(tempImportObj.TransactionReasonStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add("扣补款原因不能为空");
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.TransactionReasonStr) > fieldLenght["TRANSACTIONREASON"])
                                tempErrorMessage.Add("扣补款原因过长");
                        }

                        //备注检查
                        fieldIndex = notNullableFields.IndexOf("MEMO");
                        if(string.IsNullOrEmpty(tempImportObj.MemoStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchasePricingDetail_Validation15);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.MemoStr) > fieldLenght["MEMO"])
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchasePricingDetail_Validation16);
                        }
                        #endregion

                        if(tempErrorMessage.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });
                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    #region 剩下的数据进行业务检查

                    //品牌
                    var partsSalesCategoryNamesNeedCheck = tempRightList.Select(r => r.PartsSalesCategoryNameStr).Distinct().ToArray();
                    var dbPartsSalesCategorys = new List<PartsSalesCategoryExtend>();
                    Func<string[], bool> getDbPartsSalesCategorys = value => {
                        var dbObj = new PartsSalesCategoryExtend {
                            Id = Convert.ToInt32(value[0]),
                            Code = value[1],
                            Name = value[2],
                            BranchId = Convert.ToInt32(value[3]),
                            BranchCode = value[4]
                        };
                        dbPartsSalesCategorys.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select Id,Code,Name,BranchId,BranchCode from PartsSalesCategory where status=1 ", "Name", false, partsSalesCategoryNamesNeedCheck, getDbPartsSalesCategorys);
                    //产品线
                    var serviceProductLineNameNeedCheck = tempRightList.Select(r => r.ServiceProductLineNameStr).Distinct().ToArray();
                    var dbServiceProductLines = new List<ServiceProductLineViewExtend>();
                    Func<string[], bool> getDbServiceProductLines = value => {
                        var dbObj = new ServiceProductLineViewExtend {
                            ServiceProductLineId = Convert.ToInt32(value[0]),
                            ProductLineType = Convert.ToInt32(value[1]),
                            ProductLineName = value[2],
                            PartsSalesCategoryId = Convert.ToInt32(value[3])
                        };
                        dbServiceProductLines.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("Select ProductLineId,ProductLineType,ProductLineName,PartsSalesCategoryId From ServiceProductLineView", "ProductLineName", true, serviceProductLineNameNeedCheck, getDbServiceProductLines);
                    //供应商
                    var supplierNamesNeedCheck = tempRightList.Select(r => r.SupplierNameStr).Distinct().ToArray();
                    var dbPartsSuppliers = new List<PartsSupplierExtend>();
                    Func<string[], bool> getDbPartsSuppliers = value => {
                        var dbObj = new PartsSupplierExtend {
                            Id = Convert.ToInt32(value[0]),
                            Code = value[1],
                            Name = value[2]
                        };
                        dbPartsSuppliers.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select Id,Code,Name from PartsSupplier where status=1 ", "Name", false, supplierNamesNeedCheck, getDbPartsSuppliers);

                    //7、源单据编号有效性校验：
                    //源单据类型=维修索赔/保养索赔/服务活动索赔，查询维修保养索赔单供应商索赔清单，索赔单编号 是否等于 源单据编号，且存在供应商索赔清单.供应商Id=步骤6供应商Id，如果单据不存在，则提示：源单据类型或源单据编号不正确
                    //源单据类型=外出服务索赔，查询外出服务索赔单，外出索赔单编号 是否等于 源单据编号且索赔供应商Id=步骤6索赔供应商Id，如果不等于，则提示：源单据类型或源单据编号不正确
                    //校验成功新增供应商扣补款单
                    var repairClaimBillCodesNeedCheck = tempRightList.Where(r => r.SourceType == (int)DcsExpenseAdjustmentBillSourceType.维修索赔 || r.SourceType == (int)DcsExpenseAdjustmentBillSourceType.保养索赔 || r.SourceType == (int)DcsExpenseAdjustmentBillSourceType.服务活动索赔).Select(r => r.SourceCodeStr).Distinct().ToArray();
                    var serviceTripClaimBillCodesNeedCheck = tempRightList.Where(r => r.SourceType == (int)DcsExpenseAdjustmentBillSourceType.外出服务索赔).Select(r => r.SourceCodeStr).Distinct().ToArray();

                    var dbRepairClaimBills = new List<RepairClaimBillExtend>();
                    Func<string[], bool> getDbRepairClaimBills = value => {
                        var dbObj = new RepairClaimBillExtend {
                            Id = int.Parse(value[0]),
                            ClaimBillCode = value[1],
                            SupplierId = int.Parse(value[2])
                        };
                        dbRepairClaimBills.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select a.Id,ClaimBillCode,SupplierId from RepairClaimBill a join RepairClaimSupplierDetail b on  a.id=b.RepairClaimId", "ClaimBillCode", false, repairClaimBillCodesNeedCheck, getDbRepairClaimBills);

                    var dbServiceTripClaimBills = new List<ServiceTripClaimBillExtend>();
                    Func<string[], bool> getDbServiceTripClaimBills = value => {
                        var dbObj = new ServiceTripClaimBillExtend {
                            Id = int.Parse(value[0]),
                            Code = value[1],
                            ClaimSupplierId = int.Parse(value[2])
                        };
                        dbServiceTripClaimBills.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select Id,Code,ClaimSupplierId from ServiceTripClaimBill", "Code", false, serviceTripClaimBillCodesNeedCheck, getDbServiceTripClaimBills);

                    foreach(var tempRight in tempRightList) {
                        var partsSalesCategory = dbPartsSalesCategorys.FirstOrDefault(v => v.Name == tempRight.PartsSalesCategoryNameStr && v.BranchId == branchId);
                        if(partsSalesCategory == null) {
                            tempRight.ErrorMsg = ErrorStrings.ImpVehicleDLRStartSecurityDeposit_Validation8;
                            continue;
                        }
                        if(!string.IsNullOrWhiteSpace(tempRight.ServiceProductLineNameStr)) {
                            var serviceProductLine = dbServiceProductLines.FirstOrDefault(r => r.ProductLineName == tempRight.ServiceProductLineNameStr);
                            if(serviceProductLine == null) {
                                tempRight.ErrorMsg = "服务产品线不存在";
                                continue;
                            }
                            if(serviceProductLine.PartsSalesCategoryId != partsSalesCategory.Id) {
                                tempRight.ErrorMsg = "导入产品线与导入的所属品牌不符";
                                continue;
                            }
                            tempRight.ServiceProductLineId = serviceProductLine.ServiceProductLineId;
                            tempRight.ProductLineType = serviceProductLine.ProductLineType;
                        }
                        var partsSupplier = dbPartsSuppliers.FirstOrDefault(v => v.Name == tempRight.SupplierNameStr);
                        if(partsSupplier == null) {
                            tempRight.ErrorMsg = ErrorStrings.Export_Validation_PartsPurchaseOrder_Validation8;
                            continue;
                        }

                        switch(tempRight.SourceTypeStr) {
                            case "维修索赔":
                            case "保养索赔":
                            case "服务活动索赔":
                                var repairClaimBill = dbRepairClaimBills.FirstOrDefault(v => v.ClaimBillCode == tempRight.SourceCodeStr && v.SupplierId == partsSupplier.Id);
                                if(repairClaimBill == null) {
                                    tempRight.ErrorMsg = "源单据类型或源单据编号不正确";
                                    continue;
                                } else {
                                    tempRight.SourceId = repairClaimBill.Id;
                                }
                                break;
                            case "外出服务索赔":
                                var serviceTripClaimBill = dbServiceTripClaimBills.FirstOrDefault(v => v.Code == tempRight.SourceCodeStr && v.ClaimSupplierId == partsSupplier.Id);
                                if(serviceTripClaimBill == null) {
                                    tempRight.ErrorMsg = "源单据类型或源单据编号不正确";
                                    continue;
                                } else {
                                    tempRight.SourceId = serviceTripClaimBill.Id;
                                }
                                break;
                        }

                        tempRight.PartsSalesCategoryId = partsSalesCategory.Id;
                        tempRight.SupplierId = partsSupplier.Id;
                        tempRight.SupplierCode = partsSupplier.Code;
                        tempRight.BranchId = branchId;
                        tempRight.BranchCode = partsSalesCategory.BranchCode;

                        tempRight.BranchName = tempRight.BranchNameStr;
                        tempRight.SupplierName = tempRight.SupplierNameStr;
                        tempRight.SupplierContactPerson = tempRight.SupplierContactPersonStr;
                        tempRight.SupplierPhoneNumber = tempRight.SupplierPhoneNumberStr;
                        tempRight.SupplierAddress = tempRight.SupplierAddressStr;
                        tempRight.SourceCode = tempRight.SourceCodeStr;
                        tempRight.TransactionReason = tempRight.TransactionReasonStr;
                        tempRight.Memo = tempRight.MemoStr;
                    }
                    #endregion

                    //获取所有不合格数据
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    //获取所有合格数据
                    rightList = allList.Except(errorList).ToList();

                    #region 将合格数据的值填上
                    #endregion
                }
                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempObj = list[index - 1];
                            var values = new object[] {
                                #region 设置错误信息导出的列的值
                                tempObj.TransactionCategoryStr, tempObj.BranchNameStr, 
                                tempObj.PartsSalesCategoryNameStr,tempObj.ServiceProductLineNameStr,
                                tempObj.ProductLineTypeStr, tempObj.SupplierNameStr,
                                tempObj.SupplierContactPersonStr,tempObj.SupplierPhoneNumber, 
                                tempObj.SupplierAddressStr,tempObj.DebitOrReplenishStr,tempObj.SourceTypeStr,
                                tempObj.SourceCodeStr,tempObj.TransactionAmountStr,
                                tempObj.IfClaimToResponsibleStr, tempObj.TransactionReasonStr,
                                tempObj.MemoStr,tempObj.ErrorMsg
                                #endregion
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }

                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }
    }
}
