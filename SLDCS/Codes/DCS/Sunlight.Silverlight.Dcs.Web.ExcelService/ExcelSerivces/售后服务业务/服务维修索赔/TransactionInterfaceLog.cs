﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        public bool 导出保外会员交易接口日志(int[] ids, string repairCode, string memberPhone, string memberName, int? syncStatus, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName) {
            fileName = GetExportFilePath("保外会员交易接口日志.xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);
                using(var conn = db.CreateDbConnection()) {
                    var sql = new StringBuilder();
                    sql.AppendFormat(@"Select t.MemberPhone,t.MemberName,t.VIN,t.MinServiceCode,r.Code,cast(Case t.SyncStatus
                                               When 1 Then
                                               '未处理'
                                               When 2 Then
                                               '处理成功'
                                                When 3 Then
                                               '处理失败'
                                           End as varchar2(20)),t.SyncTime  from TransactionInterfaceLog t left join RepairOrder r on t.RepairOrderID=r.Id where 1=1 ");
                    var dbParameters = new List<DbParameter>();
                    if(ids != null && ids.Length > 0) {
                        sql.Append(" and t.Id in (");
                        for(var i = 0; i < ids.Length; i++) {
                            if(ids.Length == i + 1) {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            } else {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                        }
                        sql.Append(")");
                    } else {
                        if(!string.IsNullOrEmpty(repairCode)) {
                            sql.Append(" and Lower(r.Code) like {0}repairCode ");
                            dbParameters.Add(db.CreateDbParameter("repairCode", "%" + repairCode.ToLower() + "%"));
                        }
                        if(!string.IsNullOrEmpty(memberPhone)) {
                            sql.Append(" and Lower(t.MemberPhone) like {0}memberPhone ");
                            dbParameters.Add(db.CreateDbParameter("memberPhone", "%" + memberPhone.ToLower() + "%"));
                        }
                        if(!string.IsNullOrEmpty(memberName)) {
                            sql.Append(" and Lower(t.MemberName) like {0}memberName ");
                            dbParameters.Add(db.CreateDbParameter("memberName", "%" + memberName.ToLower() + "%"));
                        }
                        if(syncStatus.HasValue) {
                            sql.Append(" and t.SyncStatus={0}syncStatus");
                            dbParameters.Add(db.CreateDbParameter("syncStatus", syncStatus));
                        }
                        if(createTimeBegin.HasValue) {
                            sql.Append(@" and t.SyncTime >=to_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(@" and t.SyncTime <=to_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                    }
                    sql.Append(" order by t.Id desc");
                    var sqlStr = sql.ToString();
                    if(sqlStr.IndexOf(" where 1=1  and ") > 0)
                        sqlStr = sqlStr.Replace(" where 1=1  and ", " where ");
                    else
                        sqlStr = sqlStr.Replace(" where 1=1 ", "");
                    var command = db.CreateDbCommand(string.Format(sqlStr, db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    conn.Open();
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    ErrorStrings.Export_Title_PersonSalesCenterLink_CellNumber,ErrorStrings.Export_Title_DealerPartsRetailOrder_Customer,"VIN",ErrorStrings.Export_Title_Dealer_Code,"维修单号",ErrorStrings.Export_Title_AccountPeriod_Status,ErrorStrings.Export_Title_Dealerserviceinfo_CreateTime
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }
    }
}
