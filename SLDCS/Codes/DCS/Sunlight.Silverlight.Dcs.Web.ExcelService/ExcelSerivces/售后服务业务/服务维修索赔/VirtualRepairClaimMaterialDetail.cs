﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        public bool ExportPartsClaimOrderDealerInformation(int[] ids, string usedPartsCode, string usedPartsName, string claimBillCode, string repairContractCode, int? status, string serialNumber, string dealerCode, string dealerName, string claimSupplierCode, string claimSupplierName, DateTime? createTimeBegin, DateTime? createTimeEnd,int repairClaimBranchId, out string fileName) {
            fileName = GetExportFilePath("导出配件索赔供应商信息" + ".xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);
                using(var conn = db.CreateDbConnection()) {
                    var sql = new StringBuilder();
                    sql.Append(@" select rm.UsedPartsCode,rm.UsedPartsName,rm.Quantity,r.ClaimBillCode,r.RepairContractCode, cast(Case r.Status
                                            When 1 Then
                                            '新增'
                                            When 2 Then
                                            '提交'
                                            When 3 Then
                                            '初审通过'
                                            When 4 Then
                                            '生效'
                                            When 6 Then
                                            '审核不通过'
                                            When 99 Then
                                            '作废'
                                        End as varchar2(20)),v.SerialNumber,
                                  v.ProductCategoryName,r.DealerCode,r.DealerName,m.Name,r.ClaimSupplierCode,r.ClaimSupplierName,rm.UnitPrice,r.CreateTime 
                                  from RepairClaimMaterialDetail rm 
                                  INNER JOIN RepairClaimItemDetail ri ON rm.RepairClaimItemDetailId = ri.Id 
                                  INNER JOIN RepairClaimBill r ON ri.RepairClaimBillId = r.Id 
 	                              INNER JOIN VehicleInformation v ON r.VehicleId = v.Id 
                                  INNER JOIN Product p ON v.ProductId = p.Id 
                                  INNER JOIN MarketingDepartment m ON m.Id=r.MarketingDepartmentId
                                  WHERE m.BranchId = " + repairClaimBranchId);
                    var dbParameters = new List<DbParameter>();
                    if(ids != null && ids.Length > 0) {
                        sql.Append(" and rm.Id in (");
                        for(var i = 0; i < ids.Length; i++) {
                            if(ids.Length == i + 1) {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            } else {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                        }
                        sql.Append(")");
                    } else {
                        if(!string.IsNullOrEmpty(usedPartsCode)) {
                            sql.Append(" and rm.UsedPartsCode like {0}usedPartsCode ");
                            dbParameters.Add(db.CreateDbParameter("usedPartsCode", "%" + usedPartsCode + "%"));
                        }
                        if(!string.IsNullOrEmpty(usedPartsName)) {
                            sql.Append(" and rm.UsedPartsName like {0}usedPartsName ");
                            dbParameters.Add(db.CreateDbParameter("usedPartsName", "%" + usedPartsName + "%"));
                        }
                        if(!string.IsNullOrEmpty(claimBillCode)) {
                            sql.Append(" and r.ClaimBillCode like {0}claimBillCode ");
                            dbParameters.Add(db.CreateDbParameter("claimBillCode", "%" + claimBillCode + "%"));
                        }
                        if(!string.IsNullOrEmpty(repairContractCode)) {
                            sql.Append(" and r.RepairContractCode like {0}repairContractCode ");
                            dbParameters.Add(db.CreateDbParameter("repairContractCode", "%" + repairContractCode + "%"));
                        }
                        if(status.HasValue) {
                            sql.Append(" and r.Status like {0}status ");
                            dbParameters.Add(db.CreateDbParameter("status", status));
                        }
                        if(!string.IsNullOrEmpty(serialNumber)) {
                            sql.Append(" and v.SerialNumber like {0}serialNumber ");
                            dbParameters.Add(db.CreateDbParameter("serialNumber", "%" + serialNumber + "%"));
                        }
                        if(!string.IsNullOrEmpty(dealerCode)) {
                            sql.Append(" and r.DealerCode like {0}dealerCode ");
                            dbParameters.Add(db.CreateDbParameter("dealerCode", "%" + dealerCode + "%"));
                        }
                        if(!string.IsNullOrEmpty(dealerName)) {
                            sql.Append(" and r.DealerName like {0}dealerName ");
                            dbParameters.Add(db.CreateDbParameter("dealerName", "%" + dealerName + "%"));
                        }
                        if(!string.IsNullOrEmpty(claimSupplierCode)) {
                            sql.Append(" and r.ClaimSupplierCode like {0}claimSupplierCode ");
                            dbParameters.Add(db.CreateDbParameter("claimSupplierCode", "%" + claimSupplierCode + "%"));
                        }
                        if(!string.IsNullOrEmpty(claimSupplierName)) {
                            sql.Append(" and r.ClaimSupplierName like {0}claimSupplierName ");
                            dbParameters.Add(db.CreateDbParameter("claimSupplierName", "%" + claimSupplierName + "%"));
                        }
                        if(createTimeBegin.HasValue) {
                            sql.Append(@" and r.CreateTime >=to_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(@" and r.CreateTime <=to_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                    }
                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    conn.Open();
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    "旧件图号","旧件名称",ErrorStrings.Export_Title_WarehouseArea_Quantity,"索赔单编号","维修单编号",ErrorStrings.Export_Title_AccountPeriod_Status,"出厂编号","车型",ErrorStrings.Export_Title_Dealer_Code,ErrorStrings.Export_Title_Dealer_Name,"市场部","旧件供应商编号","旧件供应商名称","配件销售价格","索赔单创建时间"
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }
    }
}
