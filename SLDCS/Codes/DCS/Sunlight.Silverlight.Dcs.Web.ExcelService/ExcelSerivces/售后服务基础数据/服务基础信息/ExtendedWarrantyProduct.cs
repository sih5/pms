﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;



namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        public bool ExportExtendedWarrantyProduct(int[] ids, int? partsSalesCategoryId, string extendedWarrantyProductName, int? status, int? serviceProductLineId,
            DateTime? creatorTimeBegin, DateTime? creatorTimeEnd, out string fileName) {

            fileName = GetExportFilePath("延保产品维护_" + ".xlsx");

            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated) {
                    throw new Exception(ErrorStrings.File_Validation_User);
                }
                var db = DbHelper.GetDbHelp(ConnectionString);
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.Append(@"   select 
                                       a.PartsSalesCategory,
                                       a.ServiceProductLine,
                                       a.ExtendedWarrantyProductCode,
                                       a.ExtendedWarrantyProductName,
                                       (select value from keyvalueitem where NAME = 'BaseData_Status'and key=a.status) As status,
                                       a.ExtendedWarrantyTerm,
                                       a.ExtendedWarrantyMileage,

                                       a.ExtendedWarrantyTermRat,
                                       a.ExtendedWarrantyMileageRat,
                                       a.CustomerRetailPrice,
                                       a.TradePrice,
                                       a.PayStartTimeCondition,
                                       
                                       a.PayEndTimeCondition,
                                       a.PayStartTimeMileage,
                                       a.PayEndTimeMileage,
                                       (select value from keyvalueitem where NAME = 'AutomaticAuditPeriod'and key=a.AutomaticAuditPeriod) As automaticAuditPeriod,
                                       a.ExtensionCoverage,
                                       
                                       a.OutExtensionCoverage,
                                       a.CreatorName,
                                       a.CreatorTime,
                                       a.ModifierName,
                                       a.ModifierTime,
                                       a.AbandonerName,

                                       a.AbandonerTime

                                  from ExtendedWarrantyProduct a where 1=1 ");
                    var dbParameters = new List<DbParameter>();
                    if(ids != null && ids.Length > 0) {
                        sql.Append(string.Format(" and a.id in ({0})", string.Join(",", ids)));
                    } else {
                        if(partsSalesCategoryId.HasValue) {
                            sql.Append(@" and a.partsSalesCategoryId = {0}partsSalesCategoryId ");
                            dbParameters.Add(db.CreateDbParameter("partsSalesCategoryId", partsSalesCategoryId));
                        }
                        if(!string.IsNullOrEmpty(extendedWarrantyProductName)) {
                            sql.Append(@" and a.extendedWarrantyProductName like {0}extendedWarrantyProductName ");
                            dbParameters.Add(db.CreateDbParameter("extendedWarrantyProductName", "%" + extendedWarrantyProductName + "%"));
                        }
                        if(status.HasValue) {
                            sql.Append(@" and a.status = {0}status ");
                            dbParameters.Add(db.CreateDbParameter("status", status));
                        }
                        if(serviceProductLineId.HasValue) {
                            sql.Append(@" and a.serviceProductLineId = {0}serviceProductLineId ");
                            dbParameters.Add(db.CreateDbParameter("serviceProductLineId", serviceProductLineId));
                        }
                        if(creatorTimeBegin.HasValue) {
                            sql.Append(@" and a.creatorTime >=to_date({0}creatorTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = creatorTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("creatorTimeBegin", tempTime.ToString("G")));
                        }
                        if(creatorTimeEnd.HasValue) {
                            sql.Append(@" and a.creatorTime <=to_date({0}creatorTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = creatorTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("creatorTimeEnd", tempTime.ToString("G")));
                        }
                    }

                    var sqlStr = sql.ToString();
                    if(sqlStr.IndexOf(" where 1=1  and ") > 0)
                        sqlStr = sqlStr.Replace(" where 1=1  and ", " where ");
                    else
                        sqlStr = sqlStr.Replace(" where 1=1 ", "");
                    var command = db.CreateDbCommand(string.Format(sqlStr, db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    ErrorStrings.Export_Title_Partssalescategory_Name, 
                                    "服务产品线", 
                                    "延保产品编号", 
                                    "延保产品", 
                                    ErrorStrings.Export_Title_AccountPeriod_Status,
                                    "延保期限", 
                                    "延保里程", 

                                    "延保期限（比例）", 
                                    "延保里程（比例）", 
                                    "客户建议售价", 
                                    ErrorStrings.Export_Title_DealerPartsStockQueryView_SalesPrice, 
                                    "客户购买延保条件（在保时间）（下限）", 

                                    "客户购买延保条件（在保时间）（上限）", 
                                    "客户购买延保条件（在保里程）（下限）", 
                                    "客户购买延保条件（在保里程）（上限）", 
                                    "系统自动审核期限", 
                                    "延保范围", 

                                    "非保修范围", 
                                    ErrorStrings.Export_Title_AccountPeriod_CreatorName, 
                                    ErrorStrings.Export_Title_AccountPeriod_CreateTime, 
                                    ErrorStrings.Export_Title_AccountPeriod_ModifierName, 
                                    ErrorStrings.Export_Title_AccountPeriod_ModifyTime, 

                                    ErrorStrings.Export_Title_PartsBranch_AbandonerName, 
                                    ErrorStrings.Export_Title_PartsBranch_AbandonerTime, 
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }

                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {

                return false;
            }
        }

        public bool ImportExtendedWarrantyProduct(string fileName, out int excelImportNum, out List<ExtendedWarrantyProductExtend> rightData, out List<ExtendedWarrantyProductExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<ExtendedWarrantyProductExtend>();
            var rightList = new List<ExtendedWarrantyProductExtend>();
            var allList = new List<ExtendedWarrantyProductExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);
                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("DeferredDiscountType", out notNullableFields, out fieldLenght);

                List<object> excelColumns;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    //指定文件中的列对应的名称
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Partssalescategory_Name, "PartsSalesCategory");
                    excelOperator.AddColumnDataSource("服务产品线", "ServiceProductLine");
                    excelOperator.AddColumnDataSource("延保产品", "ExtendedWarrantyProductName");
                    excelOperator.AddColumnDataSource("延保期限", "ExtendedWarrantyTermStr");
                    excelOperator.AddColumnDataSource("延保里程", "ExtendedWarrantyMileageStr");

                    excelOperator.AddColumnDataSource("延保期限（比例）", "ExtendedWarrantyTermRatStr");
                    excelOperator.AddColumnDataSource("延保里程（比例）", "ExtendedWarrantyMileageRatStr");
                    excelOperator.AddColumnDataSource("客户建议售价", "CustomerRetailPriceStr");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_DealerPartsStockQueryView_SalesPrice, "TradePriceStr");
                    excelOperator.AddColumnDataSource("客户购买延保条件（在保时间）（下限）", "PayStartTimeConditionStr");

                    excelOperator.AddColumnDataSource("客户购买延保条件（在保时间）（上限）", "PayEndTimeConditionStr");
                    excelOperator.AddColumnDataSource("客户购买延保条件（在保里程）（下限）", "PayStartTimeMileageStr");
                    excelOperator.AddColumnDataSource("客户购买延保条件（在保里程）（上限）", "PayEndTimeMileageStr");
                    excelOperator.AddColumnDataSource("系统自动审核期限", "AutomaticAuditPeriodStr");
                    excelOperator.AddColumnDataSource("延保范围", "ExtensionCoverage");

                    excelOperator.AddColumnDataSource("非保修范围", "OutExtensionCoverage");

                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    #region 获取对应枚举
                    var tempExcelOperator = excelOperator;
                    var keyValuePairs = new[] {
                            new KeyValuePair<string, string>("DcsAutomaticAuditPeriod","AutomaticAuditPeriod")
                        };
                    tempExcelOperator.ImportHelper.LoadEnumFromDb(null, null, keyValuePairs);

                    #endregion
                    //Excel单行导入
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        //接收Excel导入数据
                        var tempImportObj = new ExtendedWarrantyProductExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);

                        tempImportObj.PartsSalesCategory = newRow["PartsSalesCategory"];
                        tempImportObj.ServiceProductLine = newRow["ServiceProductLine"];
                        tempImportObj.ExtendedWarrantyProductName = newRow["ExtendedWarrantyProductName"];
                        tempImportObj.ExtendedWarrantyTermStr = newRow["ExtendedWarrantyTermStr"];
                        tempImportObj.ExtendedWarrantyMileageStr = newRow["ExtendedWarrantyMileageStr"];

                        tempImportObj.ExtendedWarrantyTermRatStr = newRow["ExtendedWarrantyTermRatStr"];
                        tempImportObj.ExtendedWarrantyMileageRatStr = newRow["ExtendedWarrantyMileageRatStr"];
                        tempImportObj.CustomerRetailPriceStr = newRow["CustomerRetailPriceStr"];
                        tempImportObj.TradePriceStr = newRow["TradePriceStr"];
                        tempImportObj.PayStartTimeConditionStr = newRow["PayStartTimeConditionStr"];

                        tempImportObj.PayEndTimeConditionStr = newRow["PayEndTimeConditionStr"];
                        tempImportObj.PayStartTimeMileageStr = newRow["PayStartTimeMileageStr"];
                        tempImportObj.PayEndTimeMileageStr = newRow["PayEndTimeMileageStr"];
                        tempImportObj.AutomaticAuditPeriodStr = newRow["AutomaticAuditPeriodStr"];
                        tempImportObj.ExtensionCoverage = newRow["ExtensionCoverage"];

                        tempImportObj.OutExtensionCoverage = newRow["OutExtensionCoverage"];

                        var errorMsgs = new List<string>();
                        if(errorMsgs.Count > 0)
                            tempImportObj.ErrorMsg = string.Join("; ", errorMsgs);
                        allList.Add(tempImportObj);
                        return false;
                    });
                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    if(tempRightList.Count > 1000)
                        throw new Exception("导入时，最大数量限制1000条");
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    // 可输入逻辑校验
                    foreach(var tempRight in tempRightList) {
                        if(string.IsNullOrEmpty(tempRight.ExtendedWarrantyProductName)) {
                            tempRight.ErrorMsg = "延保产品不能为空";
                            continue;
                        }
                        if(string.IsNullOrEmpty(tempRight.PartsSalesCategory)) {
                            tempRight.ErrorMsg = ErrorStrings.Export_Validation_MarketingDepartment_PartsSalesCategoryIsNull;
                            continue;
                        }
                        if(string.IsNullOrEmpty(tempRight.ServiceProductLine)) {
                            tempRight.ErrorMsg = "服务产品线不能为空";
                            continue;
                        }
                        if(string.IsNullOrEmpty(tempRight.TradePriceStr)) {
                            tempRight.ErrorMsg = "批发价不能为空";
                            continue;
                        }
                        if(string.IsNullOrEmpty(tempRight.CustomerRetailPriceStr)) {
                            tempRight.ErrorMsg = "客户建议售价不能为空";
                            continue;
                        }
                        if(!string.IsNullOrEmpty(tempRight.ExtendedWarrantyTermRatStr)) {
                            try {
                                if(Convert.ToInt32(tempRight.ExtendedWarrantyTermRatStr) < 0) {
                                    tempRight.ErrorMsg = "延保期限（比例）不能为负数";
                                    continue;
                                }
                            } catch {
                                tempRight.ErrorMsg = "延保期限（比例）格式错误";
                                continue;
                            }
                        }

                        if(!string.IsNullOrEmpty(tempRight.ExtendedWarrantyMileageRatStr)) {
                            try {
                                if(Convert.ToInt32(tempRight.ExtendedWarrantyMileageRatStr) < 0) {
                                    tempRight.ErrorMsg = "延保里程（比例）不能为负数";
                                    continue;
                                }
                            } catch {
                                tempRight.ErrorMsg = "延保里程（比例）格式错误";
                                continue;
                            }
                        }

                        if(!string.IsNullOrEmpty(tempRight.TradePriceStr)) {
                            try {
                                if(Convert.ToDecimal(tempRight.TradePriceStr) < 0) {
                                    tempRight.ErrorMsg = "批发价不能为负数";
                                    continue;
                                }
                            } catch {
                                tempRight.ErrorMsg = "批发价格式错误";
                                continue;
                            }
                        }

                        if(!string.IsNullOrEmpty(tempRight.CustomerRetailPriceStr)) {
                            try {
                                if(Convert.ToDecimal(tempRight.CustomerRetailPriceStr) < 0) {
                                    tempRight.ErrorMsg = "客户建议售价不能为负数";
                                    continue;
                                }
                            } catch {
                                tempRight.ErrorMsg = "客户建议售价格式错误";
                                continue;
                            }
                        }

                        if(!string.IsNullOrEmpty(tempRight.PayStartTimeConditionStr)) {
                            try {
                                if(Convert.ToInt32(tempRight.PayStartTimeConditionStr) < 0) {
                                    tempRight.ErrorMsg = "在保时间下限不能为负数";
                                    continue;
                                }
                            } catch {
                                tempRight.ErrorMsg = "在保时间下限格式错误";
                                continue;
                            }
                        }

                        if(!string.IsNullOrEmpty(tempRight.PayEndTimeConditionStr)) {
                            try {
                                if(Convert.ToInt32(tempRight.PayEndTimeConditionStr) < 0) {
                                    tempRight.ErrorMsg = "在保时间上限不能为负数";
                                    continue;
                                }
                            } catch {
                                tempRight.ErrorMsg = "在保时间上限格式错误";
                                continue;
                            }
                        }

                        if(!string.IsNullOrEmpty(tempRight.ExtendedWarrantyTermRatStr)) {
                            try {
                                if(Convert.ToInt32(tempRight.ExtendedWarrantyTermRatStr) < 0) {
                                    tempRight.ErrorMsg = "延保期限（比例）不能为负数";
                                    continue;
                                }
                            } catch {
                                tempRight.ErrorMsg = "延保期限（比例）格式错误";
                                continue;
                            }
                        }

                        if(!string.IsNullOrEmpty(tempRight.ExtendedWarrantyMileageRatStr)) {
                            try {
                                if(Convert.ToInt32(tempRight.ExtendedWarrantyMileageRatStr) < 0) {
                                    tempRight.ErrorMsg = "延保里程（比例）不能为负数";
                                    continue;
                                }
                            } catch {
                                tempRight.ErrorMsg = "延保里程（比例）格式错误";
                                continue;
                            }
                        }

                        if(!string.IsNullOrEmpty(tempRight.PayStartTimeMileageStr)) {
                            try {
                                if(Convert.ToInt32(tempRight.PayStartTimeMileageStr) < 0) {
                                    tempRight.ErrorMsg = "在保里程下限不能为负数";
                                    continue;
                                }
                            } catch {
                                tempRight.ErrorMsg = "在保里程下限格式错误";
                                continue;
                            }
                        }

                        if(!string.IsNullOrEmpty(tempRight.PayEndTimeMileageStr)) {
                            try {
                                if(Convert.ToInt32(tempRight.PayEndTimeMileageStr) < 0) {
                                    tempRight.ErrorMsg = "在保里程上限不能为负数";
                                    continue;
                                }
                            } catch {
                                tempRight.ErrorMsg = "在保里程上限格式错误";
                                continue;
                            }
                        }

                        if(Convert.ToDecimal(tempRight.TradePriceStr) <= 0) {
                            tempRight.ErrorMsg = "批发价必须大于0";
                            continue;
                        }
                        if(Convert.ToDecimal(tempRight.CustomerRetailPriceStr) <= 0) {
                            tempRight.ErrorMsg = "客户建议售价必须大于0";
                            continue;
                        }

                        //if(string.IsNullOrEmpty(tempRight.PayEndTimeMileageStr) || string.IsNullOrEmpty(tempRight.PayStartTimeMileageStr)) {
                        //    tempRight.ErrorMsg = "客户购买延保条件（在保里程）不能为空";
                        //    continue;
                        //}
                        if(string.IsNullOrEmpty(tempRight.PayStartTimeConditionStr) || string.IsNullOrEmpty(tempRight.PayStartTimeConditionStr)) {
                            tempRight.ErrorMsg = "客户购买延保条件（在保时间）不能为空";
                            continue;
                        }
                        if(!string.IsNullOrEmpty(tempRight.ExtendedWarrantyTermStr) && !string.IsNullOrEmpty(tempRight.ExtendedWarrantyTermRatStr)) {
                            tempRight.ErrorMsg = "延保期限 和 延保期限（比例）只能填写一个";
                            continue;
                        }
                        if(string.IsNullOrEmpty(tempRight.ExtendedWarrantyTermStr) && string.IsNullOrEmpty(tempRight.ExtendedWarrantyTermRatStr)) {
                            tempRight.ErrorMsg = "延保期限 和 延保期限（比例）至少填一个";
                            continue;
                        }
                        if(!string.IsNullOrEmpty(tempRight.ExtendedWarrantyTermStr) && !string.IsNullOrEmpty(tempRight.ExtendedWarrantyMileageRatStr)) {
                            tempRight.ErrorMsg = "填写了延保期限，只允许填写延保里程";
                            continue;
                        }

                        if(!string.IsNullOrEmpty(tempRight.ExtendedWarrantyTermRatStr) && !string.IsNullOrEmpty(tempRight.ExtendedWarrantyMileageStr)) {
                            tempRight.ErrorMsg = "填写了延保期限比例，只允许填写延保里程比例";
                            continue;
                        }

                        if(!string.IsNullOrEmpty(tempRight.AutomaticAuditPeriodStr)) {
                            var automaticAuditPeriod = tempExcelOperator.ImportHelper.GetEnumValue("DcsAutomaticAuditPeriod", tempRight.AutomaticAuditPeriodStr);
                            if(automaticAuditPeriod.HasValue) {
                                tempRight.AutomaticAuditPeriod = automaticAuditPeriod.Value;
                            } else {
                                tempRight.ErrorMsg = string.Format("不存在系统自动审核期限“{0}”", tempRight.AutomaticAuditPeriodStr);
                                continue;
                            }
                        }
                    }

                    //1、查询品牌，否则提示：品牌*****不存在；
                    var partsSalesCategoryCheck = tempRightList.Select(r => r.PartsSalesCategory).Distinct().ToArray();
                    var dbPartsSalesCategorys = new List<PartsSalesCategory>();
                    Func<string[], bool> getDbPartsSalesCategorys = value => {
                        var dbObj = new PartsSalesCategory {
                            Id = Convert.ToInt32(value[0]),
                            Code = value[1],
                            Name = value[2],

                        };
                        dbPartsSalesCategorys.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select Id,Code,Name from PartsSalesCategory where status=1 ", "Name", false, partsSalesCategoryCheck, getDbPartsSalesCategorys);
                    foreach(var tempRight in tempRightList) {
                        var partsSalesCategory = dbPartsSalesCategorys.FirstOrDefault(v => v.Name == tempRight.PartsSalesCategory);
                        if(partsSalesCategory == null) {
                            tempRight.ErrorMsg = String.Format(ErrorStrings.ImpPartsSupplierRelation_Validation28, tempRight.PartsSalesCategory);
                            continue;
                        }
                        tempRight.PartsSalesCategoryId = partsSalesCategory.Id;
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();


                    //1、查询服务产品线，否则提示：服务产品线*****不存在；
                    var serviceProductLineCheck = tempRightList.Select(r => r.ServiceProductLine).Distinct().ToArray();
                    var dbServiceProductLines = new List<ServiceProductLineViewExtend>();
                    Func<string[], bool> getDbServiceProductLines = value => {
                        var dbObj = new ServiceProductLineViewExtend {
                            Id = Convert.ToInt32(value[0]),
                            Name = value[2],
                            PartsSalesCategoryId = Convert.ToInt32(value[3])
                        };
                        dbServiceProductLines.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select Id,Code, Name, PartsSalesCategoryId from ServiceProductLine where status=1", "Name", false, serviceProductLineCheck, getDbServiceProductLines);
                    foreach(var tempRight in tempRightList) {
                        var serviceProductLine = dbServiceProductLines.FirstOrDefault(v => v.Name == tempRight.ServiceProductLine && v.PartsSalesCategoryId == tempRight.PartsSalesCategoryId);
                        if(serviceProductLine == null) {
                            tempRight.ErrorMsg = String.Format("品牌“{0}”中不存在服务产品线“{1}”", tempRight.PartsSalesCategory, tempRight.ServiceProductLine);
                            continue;
                        }
                        tempRight.ServiceProductLineId = serviceProductLine.Id;
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();


                    //1、查询服务产品，否则提示：服务产品*****已存在；
                    var extendedWarrantyProductCheck = tempRightList.Select(r => r.ExtendedWarrantyProductName).Distinct().ToArray();
                    var dbExtendedWarrantyProducts = new List<ExtendedWarrantyProductExtend>();
                    Func<string[], bool> getDbExtendedWarrantyProducts = value => {
                        var dbObj = new ExtendedWarrantyProductExtend {
                            ExtendedWarrantyProductName = value[2],
                            PartsSalesCategory = value[3],
                            ServiceProductLine = value[4],
                            ExtendedWarrantyMileageStr = value[5],
                            ExtendedWarrantyTermStr = value[6],
                            ExtendedWarrantyMileageRatStr = value[7],
                            ExtendedWarrantyTermRatStr = value[8],
                        };
                        dbExtendedWarrantyProducts.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator(@"select Id,ExtendedWarrantyProductCode,ExtendedWarrantyProductName,PartsSalesCategory,ServiceProductLine,ExtendedWarrantyMileage,
                    ExtendedWarrantyTerm, ExtendedWarrantyMileageRat, ExtendedWarrantyTermRat  from ExtendedWarrantyProduct where status !=99 ", "ExtendedWarrantyProductName",
                        false, extendedWarrantyProductCheck, getDbExtendedWarrantyProducts);
                    foreach(var tempRight in tempRightList) {
                        var serviceProductLinelike = dbExtendedWarrantyProducts.FirstOrDefault(v => v.PartsSalesCategory == tempRight.PartsSalesCategory
                                                                                               && v.ServiceProductLine == tempRight.ServiceProductLine
                                                                                               && ((v.ExtendedWarrantyMileageStr == tempRight.ExtendedWarrantyMileageStr
                                                                                               && v.ExtendedWarrantyTermStr == tempRight.ExtendedWarrantyTermStr)
                                                                                               || (v.ExtendedWarrantyMileageRatStr == tempRight.ExtendedWarrantyMileageRatStr
                                                                                               && v.ExtendedWarrantyTermRatStr == tempRight.ExtendedWarrantyTermRatStr)));
                        if(serviceProductLinelike != null) {
                            tempRight.ErrorMsg = "同一品牌，同一产品线，不能有延保里程和延保期限都相同的有效数据";
                            continue;
                        }
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    //获取所有不合格数据
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    //获取所有合格数据
                    rightList = allList.Except(errorList).ToList();
                }
                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempObj = list[index - 1];
                            var values = new object[] {
                                //设置错误信息导出的列的值
                                tempObj.PartsSalesCategory,
                                tempObj.ServiceProductLine,
                                tempObj.ExtendedWarrantyProductName,
                                tempObj.ExtendedWarrantyTermStr,
                                tempObj.ExtendedWarrantyMileageStr,
                                
                                tempObj.ExtendedWarrantyTermRatStr,
                                tempObj.ExtendedWarrantyMileageRatStr,
                                tempObj.CustomerRetailPriceStr,
                                tempObj.TradePriceStr,
                                tempObj.PayStartTimeConditionStr,
                                
                                tempObj.PayEndTimeConditionStr,
                                tempObj.PayStartTimeMileageStr,
                                tempObj.PayEndTimeMileageStr,
                                tempObj.AutomaticAuditPeriodStr,
                                tempObj.ExtensionCoverage,
                                
                                tempObj.OutExtensionCoverage,
                                tempObj.ErrorMsg
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }
                //导入所有合格数据
                if(!rightList.Any())
                    return true;
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    //开启事务，新增更新配件在一个事务内
                    var ts = conn.BeginTransaction();
                    try {
                        //新增配件
                        if(rightList.Any()) {
                            #region 获取对应的插入的SQL 通过db.GetInsertSql方法
                            //获取新增数据的sql语句，Id为主键
                            var sqlInsert = db.GetInsertSql("ExtendedWarrantyProduct", "Id", new[] {
                                "PartsSalesCategory",
                                "ServiceProductLine",
                                "ExtendedWarrantyProductName",
                                "ExtendedWarrantyTerm",
                                "ExtendedWarrantyMileage",

                                "ExtendedWarrantyTermRat",
                                "ExtendedWarrantyMileageRat",
                                "CustomerRetailPrice",
                                "TradePrice",
                                "PayStartTimeCondition",

                                "PayEndTimeCondition",
                                "PayStartTimeMileage",
                                "PayEndTimeMileage",
                                "AutomaticAuditPeriod",
                                "ExtensionCoverage",

                                "OutExtensionCoverage",
                                "Status",
                                "CreatorId",
                                "CreatorName",
                                "CreatorTime",
                                "ExtendedWarrantyProductCode",
                                "PartsSalesCategoryId",
                                "ServiceProductLineId"
                            });
                            #endregion
                            var userInfo = Utils.GetCurrentUserInfo();
                            //往数据库增加配件信息
                            foreach(var item in rightList) {
                                var prefixAndBrand = new StringBuilder("YBCP");
                                switch(item.PartsSalesCategory) {
                                    case "奥铃":
                                        prefixAndBrand.Append("AL");
                                        break;
                                    case "拓陆者":
                                        prefixAndBrand.Append("TL");
                                        break;
                                    case "欧马可":
                                        prefixAndBrand.Append("OK");
                                        break;
                                    case "商务汽车":
                                        prefixAndBrand.Append("SW");
                                        break;
                                    case "伽途":
                                        prefixAndBrand.Append("JT");
                                        break;
                                    case "欧曼":
                                        prefixAndBrand.Append("OM");
                                        break;
                                    case "时代":
                                        prefixAndBrand.Append("SD");
                                        break;
                                    case "瑞沃":
                                        prefixAndBrand.Append("RW");
                                        break;
                                }
                                var ExtendedWarrantyProductCode = CodeGenerator.Generate(db, "ExtendedWarrantyProduct").Replace("YBCP", prefixAndBrand.ToString());
                                #region 添加Sql的参数
                                var command = db.CreateDbCommand(sqlInsert, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("PartsSalesCategory", item.PartsSalesCategory));
                                command.Parameters.Add(db.CreateDbParameter("ServiceProductLine", item.ServiceProductLine));
                                command.Parameters.Add(db.CreateDbParameter("ExtendedWarrantyProductName", item.ExtendedWarrantyProductName));
                                command.Parameters.Add(db.CreateDbParameter("ExtendedWarrantyTerm", item.ExtendedWarrantyTermStr));
                                command.Parameters.Add(db.CreateDbParameter("ExtendedWarrantyMileage", item.ExtendedWarrantyMileageStr));

                                command.Parameters.Add(db.CreateDbParameter("ExtendedWarrantyTermRat", item.ExtendedWarrantyTermRatStr));
                                command.Parameters.Add(db.CreateDbParameter("ExtendedWarrantyMileageRat", item.ExtendedWarrantyMileageRatStr));
                                command.Parameters.Add(db.CreateDbParameter("CustomerRetailPrice", item.CustomerRetailPriceStr));
                                command.Parameters.Add(db.CreateDbParameter("TradePrice", item.TradePriceStr));
                                command.Parameters.Add(db.CreateDbParameter("PayStartTimeCondition", item.PayStartTimeConditionStr));

                                command.Parameters.Add(db.CreateDbParameter("PayEndTimeCondition", item.PayEndTimeConditionStr));
                                command.Parameters.Add(db.CreateDbParameter("PayStartTimeMileage", item.PayStartTimeMileageStr));
                                command.Parameters.Add(db.CreateDbParameter("PayEndTimeMileage", item.PayEndTimeMileageStr));
                                command.Parameters.Add(db.CreateDbParameter("AutomaticAuditPeriod", item.AutomaticAuditPeriod));
                                command.Parameters.Add(db.CreateDbParameter("ExtensionCoverage", item.ExtensionCoverage));

                                command.Parameters.Add(db.CreateDbParameter("OutExtensionCoverage", item.OutExtensionCoverage));
                                command.Parameters.Add(db.CreateDbParameter("Status", 1));
                                command.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                command.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                command.Parameters.Add(db.CreateDbParameter("CreatorTime", DateTime.Now));

                                command.Parameters.Add(db.CreateDbParameter("ExtendedWarrantyProductCode", ExtendedWarrantyProductCode));
                                command.Parameters.Add(db.CreateDbParameter("PartsSalesCategoryId", item.PartsSalesCategoryId));
                                command.Parameters.Add(db.CreateDbParameter("ServiceProductLineId", item.ServiceProductLineId));
                                command.ExecuteNonQuery();
                                #endregion

                            }
                        }
                        //无异常提交
                        ts.Commit();
                    } catch(Exception) {
                        //报错回滚
                        ts.Rollback();
                        throw;
                    } finally {
                        if(conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }
    }
}
