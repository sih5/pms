﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {

        /// <summary>
        /// 装车明细
        /// </summary>
        public bool ImportLoadingDetail(string fileName, out int excelImportNum, out List<LoadingDetailExtend> rightData, out List<LoadingDetailExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<LoadingDetailExtend>();
            var allList = new List<LoadingDetailExtend>();
            var rightList = new List<LoadingDetailExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("ExpenseAdjustmentBill", out notNullableFields, out fieldLenght);

                List<string> notNullableFieldsServiceProductLineView;
                Dictionary<string, int> fieldLenghtServiceProductLineView;
                db.GetTableSchema("ServiceProductLineView", out notNullableFieldsServiceProductLineView, out fieldLenghtServiceProductLineView);

                List<string> notNullableFieldsPartsSalesCategory;
                Dictionary<string, int> fieldLenghtPartsSalesCategory;
                db.GetTableSchema("PartsSalesCategory", out notNullableFieldsPartsSalesCategory, out fieldLenghtPartsSalesCategory);

                var serialNumber = new List<string>();
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    #region 指定文件中的列对应的名称 例如 excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode, "PartCode");
                    var excelColumn = this.GetExcleColumn();
                    foreach(var value in excelColumn) {
                        excelOperator.AddColumnDataSource(value.Key, value.Value);
                    }
                    #endregion

                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        #region 根据导入的字段给对应的实体属性赋值 tempImportObj.PartsSalesCategoryNameStr = row["PartsSalesCategoryName"].Trim();
                        serialNumber.Add(newRow["1"]);
                        foreach(var value in excelColumn.Where(r => r.Value != "1")) {
                            var data = newRow[value.Value];
                            if(!string.IsNullOrEmpty(data)) {
                                var tempImportObj = new LoadingDetailExtend();
                                tempImportObj.SerialNumber = newRow["1"];
                                tempImportObj.FaultyPartsAssemblyName = value.Key;
                                tempImportObj.ResponsibleUnitCode = data;
                                allList.Add(tempImportObj);
                            }
                        }
                        #endregion
                        return false;
                    });

                    #region 剩下的数据进行业务检查
                    //2、文件内，出厂编号唯一，否则提示：ErrorStrings.ImpPartsSupplierRelation_Validation31;
                    if(serialNumber.GroupBy(r => r).Any(r => r.Count() > 1)) {
                        var numbers = serialNumber.GroupBy(r => r).Where(r => r.Count() > 1).Select(r => r.Key).ToArray();
                        foreach(var number in numbers) {
                            //获取重复数据的出厂编号,在allList 标志出来并且加入错误数据列
                            foreach(var i in allList.Where(r => r.SerialNumber == number)) {
                                i.ErrorMsg = ErrorStrings.ImpPartsSupplierRelation_Validation31;
                                //错误数据集合增加数据
                            }
                            var errorLoadingDetail = new LoadingDetailExtend();
                            errorLoadingDetail.SerialNumber = number;
                            errorLoadingDetail.ErrorMsg = ErrorStrings.ImpPartsSupplierRelation_Validation31;
                            errorList.Add(errorLoadingDetail);
                        }
                    }
                    //1、出厂编号存在车辆信息(车辆信息.出厂编号=出厂编号)中，有且只有一条数据。否则提示："出厂编号为***的车辆不存在"；
                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    var serialNumberCheck = tempRightList.Select(r => r.SerialNumber).Distinct().ToArray();
                    var dbVehicleInformation = new List<VehicleInformationExtend>();
                    Func<string[], bool> getDbVehicleInformations = value => {
                        var vehicleInformation = new VehicleInformationExtend {
                            Id = Convert.ToInt32(value[0]),
                            SerialNumber = value[1],
                            VIN = value[2],
                        };
                        dbVehicleInformation.Add(vehicleInformation);
                        return false;
                    };
                    db.QueryDataWithInOperator("select Id,SerialNumber,VIN  from VehicleInformation", "SerialNumber", false, serialNumberCheck, getDbVehicleInformations);
                    foreach(var rightItem in tempRightList) {
                        if(dbVehicleInformation.Where(r => r.SerialNumber == rightItem.SerialNumber).GroupBy(r => r.SerialNumber).Any(r => r.Count() == 1)) {
                            //存在1条正确的出厂编号
                            var vehicleInformation = dbVehicleInformation.SingleOrDefault(r => r.SerialNumber == rightItem.SerialNumber);
                            rightItem.VehicleId = vehicleInformation.Id;
                            rightItem.VIN = vehicleInformation.VIN;
                        } else {
                            //存在多条或者不存在出厂编号
                            rightItem.ErrorMsg = "出厂编号为:" + rightItem.SerialNumber + "的车辆不存在";
                            if(!errorList.Any(r => r.SerialNumber == rightItem.SerialNumber)) {
                                //车辆信息不存在只需要一条错误提示信息就Ok啦
                                var errorLoadingDetail = new LoadingDetailExtend();
                                errorLoadingDetail.SerialNumber = rightItem.SerialNumber;
                                errorLoadingDetail.ErrorMsg = "出厂编号为:" + rightItem.SerialNumber + "车辆不存在或者存在多条";
                                errorList.Add(errorLoadingDetail);
                            }
                        }
                    }

                    //4、查询装车明细(状态=有效)，如果导入文件内出厂编号+祸首件所属总成+厂家代码能在查询结果中匹配到数据，提示："数据库已存在"
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    serialNumberCheck = tempRightList.Select(r => r.SerialNumber).Distinct().ToArray();
                    var dbLoadingDetail = new List<LoadingDetailExtend>();
                    Func<string[], bool> getDbLoadingDetails = value => {
                        var loadingDetail = new LoadingDetailExtend {

                            SerialNumber = value[0],
                            FaultyPartsAssemblyName = value[1],
                            ResponsibleUnitCode = value[2],
                        };
                        dbLoadingDetail.Add(loadingDetail);
                        return false;
                    };
                    db.QueryDataWithInOperator("select SerialNumber,FaultyPartsAssemblyName,ResponsibleUnitCode  from LoadingDetail", "SerialNumber", false, serialNumberCheck, getDbLoadingDetails);
                    foreach(var rightItem in tempRightList) {
                        if(dbLoadingDetail.Any(r => r.SerialNumber == rightItem.SerialNumber && r.FaultyPartsAssemblyName == rightItem.FaultyPartsAssemblyName && r.ResponsibleUnitCode == rightItem.ResponsibleUnitCode)) {
                            //存在数据则提示数据重复
                            rightItem.ErrorMsg = "数据库已存在";
                            var errorLoadingDetail = new LoadingDetailExtend();
                            errorLoadingDetail.SerialNumber = rightItem.SerialNumber;
                            errorLoadingDetail.ResponsibleUnitCode = rightItem.ResponsibleUnitCode;
                            errorLoadingDetail.FaultyPartsAssemblyName = rightItem.FaultyPartsAssemblyName;
                            errorLoadingDetail.ErrorMsg = "数据库已存在";
                            errorList.Add(errorLoadingDetail);
                        }
                    }
                    #endregion
                    //获取所有合格数据
                    rightList = allList.Where(r => r.ErrorMsg == null).ToList();
                }
                //导出所有不合格数据
                if(errorList.Any()) {
                    //excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return new object[] { "出厂编号", "祸首件所属总成", "厂家代码", "错误信息" };
                            var tempObj = list[index - 1];
                            var values = new object[] {
                                #region 设置错误信息导出的列的值
                             tempObj.SerialNumber,
                             tempObj.FaultyPartsAssemblyName,
                             tempObj.ResponsibleUnitCode,
                             tempObj.ErrorMsg
                                #endregion
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }

                //导入所有合格数据
                if(!rightList.Any())
                    return true;
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    //开启事务，新增更新配件在一个事务内
                    var ts = conn.BeginTransaction();
                    try {
                        //新增配件
                        if(rightList.Any()) {
                            var userInfo = Utils.GetCurrentUserInfo();
                            #region 获取对应的插入的SQL 通过db.GetInsertSql方法
                            //获取新增数据的sql语句，Id为主键
                            var sqlInsert = db.GetInsertSql("LoadingDetail", "Id", new[] {
                                    "BranchId", "VehicleId", "VIN", "SerialNumber", "FaultyPartsAssemblyName", "ResponsibleUnitCode","Status","CreatorId","CreatorName","CreateTime"
                                });
                            #endregion
                            //往数据库增加配件信息
                            foreach(var item in rightList) {
                                #region 添加Sql的参数
                                var command = db.CreateDbCommand(sqlInsert, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("BranchId", userInfo.EnterpriseId));
                                command.Parameters.Add(db.CreateDbParameter("VehicleId", item.VehicleId));
                                command.Parameters.Add(db.CreateDbParameter("VIN", item.VIN));
                                command.Parameters.Add(db.CreateDbParameter("SerialNumber", item.SerialNumber));
                                command.Parameters.Add(db.CreateDbParameter("FaultyPartsAssemblyName", item.FaultyPartsAssemblyName));
                                command.Parameters.Add(db.CreateDbParameter("ResponsibleUnitCode", item.ResponsibleUnitCode));
                                command.Parameters.Add(db.CreateDbParameter("Status", (int)DcsBaseDataStatus.有效));
                                command.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                command.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                command.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                command.ExecuteNonQuery();
                                #endregion
                            }
                        }
                        //无异常提交
                        ts.Commit();
                    } catch(Exception) {
                        //报错回滚
                        ts.Rollback();
                        throw;
                    } finally {
                        if(conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }


        private Dictionary<string, string> GetExcleColumn() {
            var excelColumn = new Dictionary<string, string>();
            excelColumn.Add("出厂编号", "1");
            excelColumn.Add("车架", "2");
            excelColumn.Add("副车架", "3");
            excelColumn.Add("后板簧", "4");
            excelColumn.Add("后桥", "5");
            excelColumn.Add("前板簧", "6");
            excelColumn.Add("前桥", "7");
            excelColumn.Add("底盘线束", "8");
            excelColumn.Add("启动继电器", "9");
            excelColumn.Add("分配阀", "10");
            excelColumn.Add("快放阀", "11");
            excelColumn.Add("四回路保护阀", "12");
            excelColumn.Add("双线式气压报警开关", "13");
            excelColumn.Add("前减振器", "14");
            excelColumn.Add("传动轴", "15");
            excelColumn.Add("燃油箱", "16");
            excelColumn.Add("散热器", "17");
            excelColumn.Add("消音器", "18");
            excelColumn.Add("排气制动阀", "19");
            excelColumn.Add("选档软轴", "20");
            excelColumn.Add("轮辋", "21");
            excelColumn.Add("轮胎", "22");
            excelColumn.Add("转向器", "23");
            excelColumn.Add("转向传动轴", "24");
            excelColumn.Add("油缸", "25");
            excelColumn.Add("空滤器", "26");
            excelColumn.Add("蓄电池", "27");
            excelColumn.Add("组合仪表", "28");
            excelColumn.Add("前照灯", "29");
            excelColumn.Add("方向盘", "30");
            excelColumn.Add("放大架", "31");
            excelColumn.Add("货箱", "32");
            excelColumn.Add("发动机", "33");
            excelColumn.Add("变速器", "34");
            excelColumn.Add("离合分泵", "35");
            excelColumn.Add("齿轮泵", "36");
            excelColumn.Add("刮水器", "37");
            excelColumn.Add("车身线束", "38");
            excelColumn.Add("油门拉线", "39");
            excelColumn.Add("玻璃升降器", "40");
            excelColumn.Add("暖风机", "41");
            excelColumn.Add("制动灯开关", "42");
            excelColumn.Add("制动阀", "43");
            excelColumn.Add("离合总泵", "44");
            excelColumn.Add("手控制阀", "45");
            excelColumn.Add("前变速操纵机构", "46");
            excelColumn.Add("主座椅", "47");
            excelColumn.Add("副座椅", "48");
            excelColumn.Add("液压油箱", "49");
            excelColumn.Add("车载MP3", "50");
            excelColumn.Add("危险报警开关", "51");
            excelColumn.Add("干燥器", "52");
            excelColumn.Add("三通电磁阀", "53");
            excelColumn.Add("平衡轴", "54");
            excelColumn.Add("电磁气阀", "55");
            excelColumn.Add("举升控制器", "56");
            excelColumn.Add("继动阀", "57");
            excelColumn.Add("仪表板", "58");
            excelColumn.Add("保险杠(塑料)", "59");
            excelColumn.Add("前风挡玻璃", "60");
            excelColumn.Add("高位进气管", "61");
            excelColumn.Add("备注1", "62");
            excelColumn.Add("备注2", "63");
            excelColumn.Add("备注3", "64");
            excelColumn.Add("备注4", "65");
            excelColumn.Add("备注5", "66");
            excelColumn.Add("备注6", "67");
            excelColumn.Add("备注7", "68");
            excelColumn.Add("备注8", "69");
            excelColumn.Add("备注9", "70");
            excelColumn.Add("备注10", "71");
            return excelColumn;
        }
    }
}