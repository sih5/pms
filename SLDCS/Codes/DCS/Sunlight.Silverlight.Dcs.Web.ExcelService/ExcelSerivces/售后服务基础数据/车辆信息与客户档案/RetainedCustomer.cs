﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using System.Linq;
using System.Text.RegularExpressions;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        public bool ExportRetainedCustomer(int[] ids, string customerCustomerCode, string customerName, string customerCellPhoneNumber, string customerIdDocumentNumber, int? customerCustomerType, int? status, int? customerOccupationType, int? customerBusinessType, bool? customerIfVIP, int? vIPType, DateTime? createDateStart, DateTime? createDateEnd, DateTime? modifyDateStart, DateTime? modifyDateEnd, out string fileName) {
            fileName = GetExportFilePath("导出客户信息_" + ".xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated) {
                    throw new Exception(ErrorStrings.File_Validation_User);
                }
                var db = DbHelper.GetDbHelp(ConnectionString);
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.Append(@"select 
                                customer.customercode,
                                customer.Name,
                                (select value from keyvalueitem where NAME = 'Sex_Type'and key=customer.Gender) As Gender,
                                customer.cellPhoneNumber ,
                                customer.HomePhoneNumber ,
                                (select value from keyvalueitem where NAME = 'CustomerIdentity'and key=customer.CustomerIdentity) As CustomerIdentity,
                                (select value from keyvalueitem where NAME = 'CRMCustomerNature'and key=customer.CustomerNature) As CustomerNature,                           
                                (select value from keyvalueitem where NAME = 'Customer_IdDocumentType'and key=customer.IdDocumentType) As IdDocumentType,
                                customer.IdDocumentNumber ,
                                customer.ProvinceName  ,
                                customer.cityname  ,
                                customer.countyname  ,
                                (select value from keyvalueitem where NAME = 'RetainedCustomer_Status'and key=retainedCustomer.Status) As Status,
                                (select value from keyvalueitem where NAME = 'Customer_CustomerType'and key=customer.customerType) As customerType,
                                (select value from keyvalueitem where NAME = 'Customer_OccupationType'and key=customer.OccupationType) As OccupationType,
                                (select value from keyvalueitem where NAME = 'Customer_BusinessType'and key=customer.BusinessType) As BusinessType,
                                (select value from keyvalueitem where NAME = 'IsOrNot'and key=retainedCustomer.IfVIP) As IfVIP,
                                (select value from keyvalueitem where NAME = 'MemberType'and key=retainedCustomer.MemberType) As viptype,                    
                                customer.Address,
                                retainedCustomer.CreatorName,
                                retainedCustomer.createTime ,
                                retainedCustomer.ModifierName ,
                                retainedCustomer.ModifyTime
                                FROM  RetainedCustomer retainedCustomer  LEFT OUTER JOIN Customer customer ON retainedCustomer.CustomerId = customer.Id where 1=1 ");
                    var dbParameters = new List<DbParameter>();
                    if(ids != null) {
                        sql.Append(@" and retainedCustomer.ID in (");
                        foreach(var id in ids) {
                            sql.Append(id + ",");
                        }
                        sql.Remove(sql.Length - 1, 1);
                        sql.Append(@")");
                    } else {
                        if(!string.IsNullOrEmpty(customerCustomerCode)) {
                            sql.Append(" and Upper(customer.CustomerCode) like Upper({0}customerCustomerCode) ");
                            dbParameters.Add(db.CreateDbParameter("customerCustomerCode", "%" + customerCustomerCode + "%"));
                        }
                        if(!string.IsNullOrEmpty(customerName)) {
                            sql.Append(" and Upper(customer.Name) like Upper({0}customerName) ");
                            dbParameters.Add(db.CreateDbParameter("customerName", "%" + customerName + "%"));
                        }
                        if(!string.IsNullOrEmpty(customerCellPhoneNumber)) {
                            sql.Append(" and Upper(customer.CellPhoneNumber) like Upper({0}customerCellPhoneNumber) ");
                            dbParameters.Add(db.CreateDbParameter("customerCellPhoneNumber", "%" + customerCellPhoneNumber + "%"));
                        }
                        if(!string.IsNullOrEmpty(customerIdDocumentNumber)) {
                            sql.Append(" and Upper(customer.IdDocumentNumber) like Upper({0}customerIdDocumentNumber) ");
                            dbParameters.Add(db.CreateDbParameter("customerIdDocumentNumber", "%" + customerIdDocumentNumber + "%"));
                        }
                        if(customerCustomerType.HasValue) {
                            sql.Append(" and customer.CustomerType={0}customerCustomerType");
                            dbParameters.Add(db.CreateDbParameter("customerCustomerType", customerCustomerType.Value));
                        }
                        if(status.HasValue) {
                            sql.Append(" and retainedCustomer.status={0}status");
                            dbParameters.Add(db.CreateDbParameter("status", status.Value));
                        }
                        if(customerOccupationType.HasValue) {
                            sql.Append(" and customer.OccupationType={0}customerOccupationType");
                            dbParameters.Add(db.CreateDbParameter("customerOccupationType", customerOccupationType.Value));
                        }
                        if(customerOccupationType.HasValue) {
                            sql.Append(" and customer.BusinessType={0}customerBusinessType");
                            dbParameters.Add(db.CreateDbParameter("customerBusinessType", customerBusinessType.Value));
                        }
                        if(customerOccupationType.HasValue) {
                            sql.Append(" and customer.IfVIP={0}customerIfVIP");
                            dbParameters.Add(db.CreateDbParameter("customerIfVIP", customerIfVIP.Value));
                        }
                        if(customerOccupationType.HasValue) {
                            sql.Append(" and retainedCustomer.MemberType={0}vIPType");
                            dbParameters.Add(db.CreateDbParameter("vIPType", vIPType.Value));
                        }
                        if(createDateStart.HasValue) {
                            sql.Append(" and retainedCustomer.CreateTime>=to_date({0}createDateStart,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createDateStart.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createDateStart", tempTime.ToString("G")));
                        }
                        if(createDateEnd.HasValue) {
                            sql.Append(" and retainedCustomer.CreateTime<=to_date({0}createDateEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createDateEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createDateEnd", tempTime.ToString("G")));
                        }
                        if(modifyDateStart.HasValue) {
                            sql.Append(" and retainedCustomer.ModifyTime>=to_date({0}modifyDateStart,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = modifyDateStart.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("modifyDateStart", tempTime.ToString("G")));
                        }
                        if(modifyDateEnd.HasValue) {
                            sql.Append(" and retainedCustomer.ModifyTime<=to_date({0}modifyDateEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = modifyDateEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("modifyDateEnd", tempTime.ToString("G")));
                        }
                    }
                    var sqlStr = sql.ToString();
                    if(sqlStr.IndexOf(" where 1=1  and ") > 0)
                        sqlStr = sqlStr.Replace(" where 1=1  and ", " where ");
                    else
                        sqlStr = sqlStr.Replace(" where 1=1 ", "");
                    var command = db.CreateDbCommand(string.Format(sqlStr, db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    "客户ID",
                                    ErrorStrings.Export_Title_Credenceapplication_CustomerName,
                                    "性别",
                                    ErrorStrings.Export_Title_PersonSalesCenterLink_CellNumber,
                                    ErrorStrings.Export_Title_Dealer_ContactPhone,
                                    "证件类型",
                                    "证件号码",
                                    ErrorStrings.Export_Title_Agency_Province,
                                    ErrorStrings.Export_Title_Agency_City,
                                    "县区",
                                    ErrorStrings.Export_Title_AccountPeriod_Status,
                                    ErrorStrings.Export_Validation_Company_Type,
                                    "职业类型",
                                    "行业类型",
                                    "是否VIP",
                                    "VIP类型",
                                    //"是否需要回访",
                                    //"是否接受短信",
                                    //"是否接受资料",
                                    "详细地址",
                                    ErrorStrings.Export_Title_AccountPeriod_CreatorName,
                                    ErrorStrings.Export_Title_AccountPeriod_CreateTime,
                                    ErrorStrings.Export_Title_AccountPeriod_ModifierName,
                                    ErrorStrings.Export_Title_AccountPeriod_ModifyTime
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }

        public bool 批量导入客户信息(string fileName, out int excelImportNum, out List<RetainedCustomerExtend> rightData, out List<RetainedCustomerExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<RetainedCustomerExtend>();
            var rightList = new List<RetainedCustomerExtend>();
            var allList = new List<RetainedCustomerExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("RetainedCustomer", out notNullableFields, out fieldLenght);
                List<object> excelColumns;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    excelOperator.AddColumnDataSource("客户ID", "CustomerCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Credenceapplication_CustomerName, "CustomerName");
                    excelOperator.AddColumnDataSource("性别", "Gender");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Validation_Company_Type, "CustomerType");
                    excelOperator.AddColumnDataSource("出生日期", "Birthdate");
                    excelOperator.AddColumnDataSource("年龄", "Age");
                    excelOperator.AddColumnDataSource("证件类型", "IdDocumentType");
                    excelOperator.AddColumnDataSource("证件号码", "IdDocumentNumber");
                    excelOperator.AddColumnDataSource("驾龄", "DrivingSeniority");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PersonSalesCenterLink_CellNumber, "CellPhoneNumber");
                    excelOperator.AddColumnDataSource("家庭电话", "HomePhoneNumber");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_ContactPhone, "PhoneNumber");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Agency_Email, "Email");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_MarketingDepartment_PostCode, "PostCode");
                    excelOperator.AddColumnDataSource("地址类型", "AddressType");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealerserviceinfo_Area, "RegionName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Agency_Province, "ProvinceName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Agency_City, "CityName");
                    excelOperator.AddColumnDataSource("区县", "CountyName");
                    excelOperator.AddColumnDataSource("详细地址", "Address");
                    excelOperator.AddColumnDataSource("单位名称", "CompanyName");
                    excelOperator.AddColumnDataSource("单位电话", "OfficePhoneNumber");
                    excelOperator.AddColumnDataSource("行业类型", "BusinessType");
                    excelOperator.AddColumnDataSource("职业类型", "OccupationType");
                    excelOperator.AddColumnDataSource("职务", "JobPosition");
                    excelOperator.AddColumnDataSource("文化程度", "EducationLevel");
                    excelOperator.AddColumnDataSource("月收入", "Income");
                    excelOperator.AddColumnDataSource("婚否", "IfMarried");
                    excelOperator.AddColumnDataSource("结婚纪念日", "WeddingDay");
                    excelOperator.AddColumnDataSource("地理位置", "Location");
                    excelOperator.AddColumnDataSource("家庭成员构成", "FamilyComposition");
                    excelOperator.AddColumnDataSource("是否为会员", "IfVIP");
                    excelOperator.AddColumnDataSource("会员级别", "MemberType");
                    excelOperator.AddColumnDataSource("VIP卡号", "CardNumber");
                    excelOperator.AddColumnDataSource("使用者性别", "UserGender");
                    excelOperator.AddColumnDataSource("特殊纪念日", "SpecialDay");
                    excelOperator.AddColumnDataSource("特殊纪念内容", "SpecialDayDetails");
                    excelOperator.AddColumnDataSource("爱好", "Hobby");
                    excelOperator.AddColumnDataSource("推荐人", "Referral");
                    excelOperator.AddColumnDataSource("上牌人", "PlateOwner");



                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;

                    //单次最大导入条数不能大于300！
                    if(excelOperator.LastRowNum > 300) {
                        throw new Exception("单次最大导入条数不能大于300！");
                    }

                    #region 获取对应枚举
                    var keyValuePairs = new[] {
                        new KeyValuePair<string, string>("Gender", "Sex_Type"), 
                        new KeyValuePair<string, string>("CustomerType", "Customer_CustomerType"),
                        new KeyValuePair<string, string>("IdDocumentType", "Customer_IdDocumentType"), 
                        new KeyValuePair<string, string>("AddressType", "CustomerAddresType"), 
                        new KeyValuePair<string, string>("BusinessType", "Customer_BusinessType"),
                        new KeyValuePair<string, string>("OccupationType", "Customer_OccupationType"),
                        new KeyValuePair<string, string>("EducationLevel", "Customer_EducationLevel"),
                        new KeyValuePair<string, string>("Income", "Customer_Income"),
                        new KeyValuePair<string, string>("Location", "RetainedCustomer_Location"),
                        new KeyValuePair<string, string>("MemberType", "MemberType"),
                        new KeyValuePair<string, string>("UserGender", "Sex_Type"),
                        new KeyValuePair<string, string>("IfMarried", "IsOrNot"),
                        new KeyValuePair<string, string>("IfVIP", "IsOrNot"),
                        new KeyValuePair<string, string>("JobPosition", "JobPosition"),
                        new KeyValuePair<string, string>("FamilyComposition", "FamilyComposition"),
                    };
                    tempExcelOperator.ImportHelper.LoadEnumFromDb(null, null, keyValuePairs);
                    #endregion

                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var tempImportObj = new RetainedCustomerExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);

                        #region
                        tempImportObj.CustomerCode = newRow["CustomerCode"];
                        tempImportObj.CustomerName = newRow["CustomerName"];
                        tempImportObj.GenderStr = newRow["Gender"];
                        tempImportObj.CustomerTypeStr = newRow["CustomerType"];
                        tempImportObj.BirthdateStr = newRow["Birthdate"];
                        tempImportObj.AgeStr = newRow["Age"];
                        tempImportObj.IdDocumentTypeStr = newRow["IdDocumentType"];
                        tempImportObj.IdDocumentNumber = newRow["IdDocumentNumber"];
                        tempImportObj.DrivingSeniorityStr = newRow["DrivingSeniority"];
                        tempImportObj.CellPhoneNumber = newRow["CellPhoneNumber"];
                        tempImportObj.HomePhoneNumber = newRow["HomePhoneNumber"];
                        tempImportObj.PhoneNumber = newRow["PhoneNumber"];
                        tempImportObj.Email = newRow["Email"];
                        tempImportObj.PostCode = newRow["PostCode"];
                        tempImportObj.AddressTypeStr = newRow["AddressType"];
                        tempImportObj.RegionName = newRow["RegionName"];
                        tempImportObj.ProvinceName = newRow["ProvinceName"];
                        tempImportObj.CityName = newRow["CityName"];
                        tempImportObj.CountyName = newRow["CountyName"];
                        tempImportObj.Address = newRow["Address"];
                        tempImportObj.CompanyName = newRow["CompanyName"];
                        tempImportObj.OfficePhoneNumber = newRow["OfficePhoneNumber"];
                        tempImportObj.BusinessTypeStr = newRow["BusinessType"];
                        tempImportObj.OccupationTypeStr = newRow["OccupationType"];
                        tempImportObj.JobPositionStr = newRow["JobPosition"];
                        tempImportObj.EducationLevelStr = newRow["EducationLevel"];
                        tempImportObj.IncomeStr = newRow["Income"];
                        tempImportObj.IfMarriedStr = newRow["IfMarried"];
                        tempImportObj.WeddingDayStr = newRow["WeddingDay"];
                        tempImportObj.LocationStr = newRow["Location"];
                        tempImportObj.FamilyCompositionStr = newRow["FamilyComposition"];
                        tempImportObj.IfVIPStr = newRow["IfVIP"];
                        tempImportObj.MemberTypeStr = newRow["MemberType"];
                        tempImportObj.CardNumber = newRow["CardNumber"];
                        tempImportObj.UserGenderStr = newRow["UserGender"];
                        tempImportObj.SpecialDayStr = newRow["SpecialDay"];
                        tempImportObj.SpecialDayDetails = newRow["SpecialDayDetails"];
                        tempImportObj.Hobby = newRow["Hobby"];
                        tempImportObj.Referral = newRow["Referral"];
                        tempImportObj.PlateOwner = newRow["PlateOwner"];


                        #endregion

                        var tempErrorMessage = new List<string>();

                        //#region 导入的内容基本检查
                        //导入模板必填
                        if(string.IsNullOrEmpty(tempImportObj.CustomerTypeStr)) {
                            tempErrorMessage.Add("客户属性不能为空");
                        }
                        if(string.IsNullOrEmpty(tempImportObj.IdDocumentTypeStr)) {
                            tempErrorMessage.Add("证件类型不能为空");
                        }
                        if(string.IsNullOrEmpty(tempImportObj.AddressTypeStr)) {
                            tempErrorMessage.Add("地址类型不能为空");
                        }
                        if(string.IsNullOrEmpty(tempImportObj.CustomerName)) {
                            tempErrorMessage.Add("客户姓名不能为空");
                        }

                        if(string.IsNullOrEmpty(tempImportObj.CityName) || string.IsNullOrEmpty(tempImportObj.ProvinceName) || string.IsNullOrEmpty(tempImportObj.CountyName)) {
                            tempErrorMessage.Add("省市区信息必填");
                        }

                        if(!string.IsNullOrEmpty(row["Gender"])) {
                            var gender = tempExcelOperator.ImportHelper.GetEnumValue("Gender", row["Gender"]);
                            if(!gender.HasValue) {
                                tempErrorMessage.Add("性别类型不正确");
                            } else {
                                tempImportObj.Gender = gender.Value;
                            }
                        }
                        if(!string.IsNullOrEmpty(row["CustomerType"])) {
                            var customerType = tempExcelOperator.ImportHelper.GetEnumValue("CustomerType", row["CustomerType"]);
                            if(!customerType.HasValue) {
                                tempErrorMessage.Add("客户属性不正确");
                            } else {
                                tempImportObj.CustomerType = customerType.Value;
                            }
                        }
                        if(!string.IsNullOrEmpty(row["IdDocumentType"])) {
                            var idDocumentType = tempExcelOperator.ImportHelper.GetEnumValue("IdDocumentType", row["IdDocumentType"]);
                            if(!idDocumentType.HasValue || !System.Enum.IsDefined(typeof(DcsCustomerIdDocumentType), idDocumentType)) {
                                tempErrorMessage.Add("证件类型不正确");
                            } else {
                                tempImportObj.IdDocumentType = idDocumentType.Value;
                                if(tempImportObj.IdDocumentType == (int)DcsCustomerIdDocumentType.组织机构代码证 || tempImportObj.IdDocumentType == (int)DcsCustomerIdDocumentType.统一社会信用代码) {
                                    tempErrorMessage.Add("证件类型不是个人客户");
                                }
                                var ErrStr = checkIdDocumentNumber((int)tempImportObj.IdDocumentType, tempImportObj.IdDocumentNumber);
                                if(!string.IsNullOrEmpty(ErrStr))
                                    tempErrorMessage.Add(ErrStr);
                            }
                        }
                        if(!string.IsNullOrEmpty(row["AddressType"])) {
                            var addressType = tempExcelOperator.ImportHelper.GetEnumValue("AddressType", row["AddressType"]);
                            if(!addressType.HasValue) {
                                tempErrorMessage.Add("地址类型不正确");
                            } else {
                                tempImportObj.AddressType = addressType.Value;
                            }
                        }
                        if(!string.IsNullOrEmpty(row["BusinessType"])) {
                            var businessType = tempExcelOperator.ImportHelper.GetEnumValue("BusinessType", row["BusinessType"]);
                            if(!businessType.HasValue) {
                                tempErrorMessage.Add("行业类型不正确");
                            } else {
                                tempImportObj.BusinessType = businessType.Value;
                            }
                        }
                        if(!string.IsNullOrEmpty(row["OccupationType"])) {
                            var occupationType = tempExcelOperator.ImportHelper.GetEnumValue("OccupationType", row["OccupationType"]);
                            if(!occupationType.HasValue) {
                                tempErrorMessage.Add("职业类型不正确");
                            } else {
                                tempImportObj.OccupationType = occupationType.Value;
                            }
                        }
                        if(!string.IsNullOrEmpty(row["EducationLevel"])) {
                            var educationLevel = tempExcelOperator.ImportHelper.GetEnumValue("EducationLevel", row["EducationLevel"]);
                            if(!educationLevel.HasValue) {
                                tempErrorMessage.Add("文化程度不正确");
                            } else {
                                tempImportObj.EducationLevel = educationLevel.Value;
                            }
                        }
                        if(!string.IsNullOrEmpty(row["Income"])) {
                            var income = tempExcelOperator.ImportHelper.GetEnumValue("Income", row["Income"]);
                            if(!income.HasValue) {
                                tempErrorMessage.Add("月收入不正确");
                            } else {
                                tempImportObj.Income = income.Value;
                            }
                        }
                        if(!string.IsNullOrEmpty(row["IfMarried"])) {
                            var ifMarried = tempExcelOperator.ImportHelper.GetEnumValue("IfMarried", row["IfMarried"]);
                            if(!ifMarried.HasValue) {
                                tempErrorMessage.Add("婚否请输入是或否");
                            } else {
                                tempImportObj.IfMarried = Convert.ToBoolean(ifMarried.Value);
                            }
                        }
                        if(!string.IsNullOrEmpty(row["Location"])) {
                            var location = tempExcelOperator.ImportHelper.GetEnumValue("Location", row["Location"]);
                            if(!location.HasValue) {
                                tempErrorMessage.Add("地理位置不正确");
                            } else {
                                tempImportObj.Location = location.Value;
                            }
                        }
                        if(!string.IsNullOrEmpty(row["IfVIP"])) {
                            var ifVIP = tempExcelOperator.ImportHelper.GetEnumValue("IfVIP", row["IfVIP"]);
                            if(!ifVIP.HasValue) {
                                tempErrorMessage.Add("是否为会员请输入是或否");
                            } else {
                                tempImportObj.IfVIP = Convert.ToBoolean(ifVIP.Value);
                            }
                        }
                        if(!string.IsNullOrEmpty(row["MemberType"])) {
                            var memberType = tempExcelOperator.ImportHelper.GetEnumValue("MemberType", row["MemberType"]);
                            if(!memberType.HasValue) {
                                tempErrorMessage.Add("会员类型不正确");
                            } else {
                                tempImportObj.MemberType = memberType.Value;
                            }
                        }
                        if(!string.IsNullOrEmpty(row["UserGender"])) {
                            var userGender = tempExcelOperator.ImportHelper.GetEnumValue("UserGender", row["UserGender"]);
                            if(!userGender.HasValue) {
                                tempErrorMessage.Add("使用者性别不正确");
                            } else {
                                tempImportObj.UserGender = userGender.Value;
                            }
                        }
                        if(!string.IsNullOrEmpty(row["FamilyComposition"])) {
                            var familyComposition = tempExcelOperator.ImportHelper.GetEnumValue("FamilyComposition", row["FamilyComposition"]);
                            if(!familyComposition.HasValue) {
                                tempErrorMessage.Add("家庭成员构成不正确");
                            } else {
                                tempImportObj.FamilyComposition = familyComposition.Value;
                            }
                        }
                        if(!string.IsNullOrEmpty(row["JobPosition"])) {
                            var jobPosition = tempExcelOperator.ImportHelper.GetEnumValue("JobPosition", row["JobPosition"]);
                            if(!jobPosition.HasValue) {
                                tempErrorMessage.Add("家庭成员构成不正确");
                            } else {
                                tempImportObj.JobPosition = jobPosition.Value;
                            }
                        }
                        if(tempErrorMessage.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });
                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    var groups = tempRightList.GroupBy(r => new {
                        r.CustomerCode,
                    }).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        groupItem.ErrorMsg = ErrorStrings.ImpPartsSupplierRelation_Validation31;
                    }
                    var sparePartsNeedCheck = allList.ToList();
                    var partCodesNeedCheck = sparePartsNeedCheck.Select(r => r.CustomerCode).Distinct().ToArray();
                    var dbSparePartCodes = new List<string>();
                    Func<string[], bool> getDbSparePartCodes = vales => {
                        dbSparePartCodes.Add(vales[0]);
                        return false;
                    };
                    //根据SQL查询数据库，将查出来的结果根据定义的委托中的方法（这里是getDbSparePartCodes）放到具体的集合中（这里是dbSparePartCodes）
                    db.QueryDataWithInOperator("select CustomerCode from Customer where status!=99 ", "CustomerCode", true, partCodesNeedCheck, getDbSparePartCodes);
                    if(partCodesNeedCheck.Length > 0) {
                        var sparePartsExistsCode = sparePartsNeedCheck.Where(r => !dbSparePartCodes.Any(v => v == r.CustomerCode)).ToArray();
                        foreach(var item in sparePartsExistsCode) {
                            item.ErrorMsg = "系统中基础客户ID" + item.CustomerCode + ErrorStrings.Export_Validation_Sparepart_NotExist;
                        }
                    }

                    var dbTiledRegion = new List<TiledRegion>();
                    var dbTiledRegionCheck = new List<string>();
                    dbTiledRegionCheck.Add("1");
                    Func<string[], bool> getTiledRegion = vales => {
                        dbTiledRegion.Add(new TiledRegion {
                            Id = int.Parse(vales[0]),
                            RegionName = vales[1],
                            ProvinceName = vales[2],
                            CityName = vales[3],
                            CountyName = vales[4],

                        });
                        return false;
                    };
                    //根据SQL查询数据库，
                    db.QueryDataWithInOperator("select Id,RegionName,ProvinceName,CityName,CountyName from TiledRegion  ", "1", true, dbTiledRegionCheck.ToArray(), getTiledRegion);
                    var tiledRegionNeedCheck = sparePartsNeedCheck.Where(r => string.IsNullOrEmpty(r.ErrorMsg)).ToArray();
                    foreach(var item in tiledRegionNeedCheck) {
                        var provinceNames = dbTiledRegion.Where(r => r.ProvinceName == item.ProvinceName);
                        if(!provinceNames.Any())
                            provinceNames = dbTiledRegion.Where(r => r.ProvinceName.Contains(item.ProvinceName));
                        if(!provinceNames.Any()) {
                            item.ErrorMsg = "系统中不存在名称为：" + item.ProvinceName + "的省份";
                        } else {
                            item.ProvinceName = provinceNames.FirstOrDefault().ProvinceName;
                            var cityNames = provinceNames.Where(r => r.CityName == item.CityName);
                            if(!cityNames.Any())
                                cityNames = provinceNames.Where(r => r.CityName.Contains(item.CityName));
                            if(!cityNames.Any()) {
                                item.ErrorMsg = "系统中不存在名称为：" + item.CityName + "的城市";
                            } else {
                                item.CityName = cityNames.FirstOrDefault().CityName;
                                var countyNames = cityNames.Where(r => r.CountyName == item.CountyName);
                                if(!countyNames.Any())
                                    countyNames = cityNames.Where(r => r.CountyName.Contains(item.CountyName));
                                if(!countyNames.Any()) {
                                    item.ErrorMsg = "系统中不存在名称为：" + item.CountyName + "的区县";
                                } else {
                                    item.CountyName = countyNames.FirstOrDefault().CountyName;
                                    item.RegionName = countyNames.FirstOrDefault().RegionName;
                                }
                            }
                        }
                    }
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    rightList = allList.Except(errorList).ToList();


                }
                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempObj = list[index - 1];
                            var values = new object[] {
                                #region 字段
                                tempObj.CustomerCode, tempObj.CustomerName, tempObj.GenderStr, tempObj.CustomerTypeStr, tempObj.BirthdateStr, tempObj.AgeStr, tempObj.IdDocumentTypeStr, tempObj.IdDocumentNumber, tempObj.DrivingSeniorityStr, tempObj.CellPhoneNumber, tempObj.HomePhoneNumber, tempObj.PhoneNumber, tempObj.Email, tempObj.PostCode, tempObj.AddressTypeStr, tempObj.RegionName, tempObj.ProvinceName, tempObj.CityName, tempObj.CountyName, tempObj.Address, tempObj.CompanyName, tempObj.OfficePhoneNumber, tempObj.BusinessTypeStr, tempObj.OccupationTypeStr, tempObj.JobPositionStr, tempObj.EducationLevelStr, tempObj.IncomeStr, tempObj.IfMarriedStr, tempObj.WeddingDayStr,
                                tempObj.LocationStr, tempObj.FamilyComposition,
                                tempObj.IfVIPStr, tempObj.MemberTypeStr, tempObj.CardNumber, tempObj.UserGenderStr, tempObj.SpecialDayStr, tempObj.SpecialDayDetails, tempObj.Hobby, tempObj.Referral, tempObj.PlateOwner, tempObj.ErrorMsg
                                #endregion
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }



                //导入所有合格数据
                if(!rightList.Any())
                    return true;
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    //开启事务
                    var ts = conn.BeginTransaction();
                    try {
                        if(rightList.Any()) {

                            var customerCodeCheck = rightList.Select(r => r.CustomerCode).Distinct().ToArray();
                            var dbCustomer = new List<RetainedCustomerExtend>();
                            Func<string[], bool> getDbcustomers = values => {
                                dbCustomer.Add(new RetainedCustomerExtend {
                                    Id = int.Parse(values[0]),
                                    CustomerCode = values[1],

                                });
                                return false;
                            };
                            db.QueryDataWithInOperator("select Id,CustomerCode from Customer", "CustomerCode", true, customerCodeCheck, getDbcustomers);
                            var userInfo = Utils.GetCurrentUserInfo();
                            foreach(var item in rightList) {


                                List<string> fields = new List<string>();
                                fields.Add("ModifyTime");
                                fields.Add("ModifierId");
                                fields.Add("ModifierName");
                                string sqlUpdateCustomer = null;
                                var commandCustomer = db.CreateDbCommand(sqlUpdateCustomer, conn, ts);
                                if(!string.IsNullOrEmpty(item.CustomerName)) {
                                    fields.Add("Name");
                                    commandCustomer.Parameters.Add(db.CreateDbParameter("Name", item.CustomerName));
                                }
                                if(!string.IsNullOrEmpty(item.GenderStr)) {
                                    fields.Add("Gender");
                                    commandCustomer.Parameters.Add(db.CreateDbParameter("Gender", item.Gender));
                                }
                                if(!string.IsNullOrEmpty(item.CustomerTypeStr)) {
                                    fields.Add("CustomerType");
                                    commandCustomer.Parameters.Add(db.CreateDbParameter("CustomerType", item.CustomerType));
                                }
                                if(!string.IsNullOrEmpty(item.AgeStr)) {
                                    fields.Add("Age");
                                    commandCustomer.Parameters.Add(db.CreateDbParameter("Age", item.AgeStr));
                                }
                                if(!string.IsNullOrEmpty(item.RegionName)) {
                                    fields.Add("RegionName");
                                    commandCustomer.Parameters.Add(db.CreateDbParameter("RegionName", item.RegionName));
                                }
                                if(!string.IsNullOrEmpty(item.ProvinceName)) {
                                    fields.Add("ProvinceName");
                                    commandCustomer.Parameters.Add(db.CreateDbParameter("ProvinceName", item.ProvinceName));
                                }
                                if(!string.IsNullOrEmpty(item.CityName)) {
                                    fields.Add("CityName");
                                    commandCustomer.Parameters.Add(db.CreateDbParameter("CityName", item.CityName));
                                }
                                if(!string.IsNullOrEmpty(item.CountyName)) {
                                    fields.Add("CountyName");
                                    commandCustomer.Parameters.Add(db.CreateDbParameter("CountyName", item.CountyName));
                                }
                                if(!string.IsNullOrEmpty(item.Address)) {
                                    fields.Add("Address");
                                    commandCustomer.Parameters.Add(db.CreateDbParameter("Address", item.Address));
                                }
                                if(!string.IsNullOrEmpty(item.PostCode)) {
                                    fields.Add("PostCode");
                                    commandCustomer.Parameters.Add(db.CreateDbParameter("PostCode", item.PostCode));
                                }
                                if(!string.IsNullOrEmpty(item.CellPhoneNumber)) {
                                    fields.Add("CellPhoneNumber");
                                    commandCustomer.Parameters.Add(db.CreateDbParameter("CellPhoneNumber", item.CellPhoneNumber));
                                }
                                if(!string.IsNullOrEmpty(item.HomePhoneNumber)) {
                                    fields.Add("HomePhoneNumber");
                                    commandCustomer.Parameters.Add(db.CreateDbParameter("HomePhoneNumber", item.HomePhoneNumber));
                                }
                                if(!string.IsNullOrEmpty(item.OfficePhoneNumber)) {
                                    fields.Add("OfficePhoneNumber");
                                    commandCustomer.Parameters.Add(db.CreateDbParameter("OfficePhoneNumber", item.OfficePhoneNumber));
                                }
                                if(!string.IsNullOrEmpty(item.IdDocumentTypeStr)) {
                                    fields.Add("IdDocumentType");
                                    commandCustomer.Parameters.Add(db.CreateDbParameter("IdDocumentType", item.IdDocumentType));
                                }
                                if(!string.IsNullOrEmpty(item.IdDocumentNumber)) {
                                    fields.Add("IdDocumentNumber");
                                    commandCustomer.Parameters.Add(db.CreateDbParameter("IdDocumentNumber", item.IdDocumentNumber));
                                }

                                if(!string.IsNullOrEmpty(item.BirthdateStr)) {
                                    fields.Add("Birthdate");
                                    commandCustomer.Parameters.Add(db.CreateDbParameter("Birthdate", Convert.ToDateTime(DateTime.Parse(item.BirthdateStr).ToString("yyyy/MM/dd hh:mm:ss")
)));
                                }
                                if(!string.IsNullOrEmpty(item.Email)) {
                                    fields.Add("Email");
                                    commandCustomer.Parameters.Add(db.CreateDbParameter("Email", item.Email));
                                }
                                if(!string.IsNullOrEmpty(item.CompanyName)) {
                                    fields.Add("CompanyName");
                                    commandCustomer.Parameters.Add(db.CreateDbParameter("CompanyName", item.CompanyName));
                                }
                                if(!string.IsNullOrEmpty(item.BusinessTypeStr)) {
                                    fields.Add("BusinessType");
                                    commandCustomer.Parameters.Add(db.CreateDbParameter("BusinessType", item.BusinessType));
                                }
                                if(!string.IsNullOrEmpty(item.OccupationTypeStr)) {
                                    fields.Add("OccupationType");
                                    commandCustomer.Parameters.Add(db.CreateDbParameter("OccupationType", item.OccupationType));
                                }
                                if(!string.IsNullOrEmpty(item.JobPositionStr)) {
                                    fields.Add("JobPosition");
                                    commandCustomer.Parameters.Add(db.CreateDbParameter("JobPosition", item.JobPosition));
                                }
                                if(!string.IsNullOrEmpty(item.EducationLevelStr)) {
                                    fields.Add("EducationLevel");
                                    commandCustomer.Parameters.Add(db.CreateDbParameter("EducationLevel", item.EducationLevel));
                                }
                                if(!string.IsNullOrEmpty(item.DrivingSeniorityStr)) {
                                    fields.Add("DrivingSeniority");
                                    commandCustomer.Parameters.Add(db.CreateDbParameter("DrivingSeniority", item.DrivingSeniorityStr));
                                }
                                if(!string.IsNullOrEmpty(item.IncomeStr)) {
                                    fields.Add("Income");
                                    commandCustomer.Parameters.Add(db.CreateDbParameter("Income", item.Income));
                                }
                                if(!string.IsNullOrEmpty(item.AddressTypeStr)) {
                                    fields.Add("AddressType");
                                    commandCustomer.Parameters.Add(db.CreateDbParameter("AddressType", item.AddressType));
                                }
                                if(!string.IsNullOrEmpty(item.PhoneNumber)) {
                                    fields.Add("PhoneNumber");
                                    commandCustomer.Parameters.Add(db.CreateDbParameter("PhoneNumber", item.PhoneNumber));
                                }

                                var customers = dbCustomer.FirstOrDefault(v => v.CustomerCode == item.CustomerCode);
                                sqlUpdateCustomer = db.GetUpdateSql("Customer", fields.ToArray(), new[] { "Id" });
                                commandCustomer.Parameters.Add(db.CreateDbParameter("Id", customers.Id));
                                commandCustomer.Parameters.Add(db.CreateDbParameter("ModifyTime", DateTime.Now));
                                commandCustomer.Parameters.Add(db.CreateDbParameter("ModifierId", userInfo.Id));
                                commandCustomer.Parameters.Add(db.CreateDbParameter("ModifierName", userInfo.Name));
                                commandCustomer.CommandText = sqlUpdateCustomer;
                                commandCustomer.ExecuteNonQuery();

                                string sqlUpdateRetainedCustomer = null;
                                List<string> fieldRetainedCustomers = new List<string>();
                                fieldRetainedCustomers.Add("ModifyTime");
                                fieldRetainedCustomers.Add("ModifierId");
                                fieldRetainedCustomers.Add("ModifierName");
                                var commandRetainedCustomer = db.CreateDbCommand(sqlUpdateRetainedCustomer, conn, ts);
                                //if(!string.IsNullOrEmpty(item.IfVisitBackNeededStr)) {
                                //    fieldRetainedCustomers.Add("IfVisitBackNeeded");
                                //    commandRetainedCustomer.Parameters.Add(db.CreateDbParameter("IfVisitBackNeeded", item.IfVisitBackNeeded));
                                //}
                                //if(!string.IsNullOrEmpty(item.IfTextAcceptedStr)) {
                                //    fieldRetainedCustomers.Add("IfTextAccepted");
                                //    commandRetainedCustomer.Parameters.Add(db.CreateDbParameter("IfTextAccepted", item.IfTextAccepted));
                                //}
                                //if(!string.IsNullOrEmpty(item.IfAdAcceptedStr)) {
                                //    fieldRetainedCustomers.Add("IfAdAccepted");
                                //    commandRetainedCustomer.Parameters.Add(db.CreateDbParameter("IfAdAccepted", item.IfAdAccepted));
                                //}
                                if(!string.IsNullOrEmpty(item.CardNumber)) {
                                    fieldRetainedCustomers.Add("CardNumber");
                                    commandRetainedCustomer.Parameters.Add(db.CreateDbParameter("CardNumber", item.CardNumber));
                                }
                                if(!string.IsNullOrEmpty(item.IfVIPStr)) {
                                    fieldRetainedCustomers.Add("IfVIP");
                                    commandRetainedCustomer.Parameters.Add(db.CreateDbParameter("IfVIP", item.IfVIP));
                                }
                                if(!string.IsNullOrEmpty(item.MemberTypeStr)) {
                                    fieldRetainedCustomers.Add("MemberType");
                                    commandRetainedCustomer.Parameters.Add(db.CreateDbParameter("MemberType", item.MemberType));
                                }
                                if(!string.IsNullOrEmpty(item.PlateOwner)) {
                                    fieldRetainedCustomers.Add("PlateOwner");
                                    commandRetainedCustomer.Parameters.Add(db.CreateDbParameter("PlateOwner", item.PlateOwner));
                                }
                                if(!string.IsNullOrEmpty(item.IfMarriedStr)) {
                                    fieldRetainedCustomers.Add("IfMarried");
                                    commandRetainedCustomer.Parameters.Add(db.CreateDbParameter("IfMarried", item.IfMarried));
                                }
                                if(!string.IsNullOrEmpty(item.WeddingDayStr)) {
                                    fieldRetainedCustomers.Add("WeddingDay");
                                    commandRetainedCustomer.Parameters.Add(db.CreateDbParameter("WeddingDay", Convert.ToDateTime(DateTime.Parse(item.WeddingDayStr).ToString("yyyy/MM/dd hh:mm:ss")
)));
                                }
                                //if(!string.IsNullOrEmpty(item.AnnualIncomeStr)) {
                                //    fieldRetainedCustomers.Add("AnnualIncome");
                                //    commandRetainedCustomer.Parameters.Add(db.CreateDbParameter("AnnualIncome", item.AnnualIncome));
                                //}
                                if(!string.IsNullOrEmpty(item.LocationStr)) {
                                    fieldRetainedCustomers.Add("Location");
                                    commandRetainedCustomer.Parameters.Add(db.CreateDbParameter("Location", item.Location));
                                }
                                if(!string.IsNullOrEmpty(item.FamilyCompositionStr)) {
                                    fieldRetainedCustomers.Add("FamilyComposition");
                                    commandRetainedCustomer.Parameters.Add(db.CreateDbParameter("FamilyComposition", item.FamilyComposition));
                                }
                                if(!string.IsNullOrEmpty(item.Referral)) {
                                    fieldRetainedCustomers.Add("Referral");
                                    commandRetainedCustomer.Parameters.Add(db.CreateDbParameter("Referral", item.Referral));
                                }
                                if(!string.IsNullOrEmpty(item.Hobby)) {
                                    fieldRetainedCustomers.Add("Hobby");
                                    commandRetainedCustomer.Parameters.Add(db.CreateDbParameter("Hobby", item.Hobby));
                                }
                                if(!string.IsNullOrEmpty(item.SpecialDayStr)) {
                                    fieldRetainedCustomers.Add("SpecialDay");
                                    commandRetainedCustomer.Parameters.Add(db.CreateDbParameter("SpecialDay", Convert.ToDateTime(DateTime.Parse(item.SpecialDayStr).ToString("yyyy/MM/dd hh:mm:ss")
)));
                                }
                                if(!string.IsNullOrEmpty(item.SpecialDayDetails)) {
                                    fieldRetainedCustomers.Add("SpecialDayDetails");
                                    commandRetainedCustomer.Parameters.Add(db.CreateDbParameter("SpecialDayDetails", item.SpecialDayDetails));
                                }
                                if(!string.IsNullOrEmpty(item.UserGenderStr)) {
                                    fieldRetainedCustomers.Add("UserGender");
                                    commandRetainedCustomer.Parameters.Add(db.CreateDbParameter("UserGender", item.UserGender));
                                }
                                sqlUpdateRetainedCustomer = db.GetUpdateSql("RetainedCustomer", fieldRetainedCustomers.ToArray(), new[] { "CustomerId" });
                                commandRetainedCustomer.Parameters.Add(db.CreateDbParameter("CustomerId", customers.Id));
                                commandRetainedCustomer.Parameters.Add(db.CreateDbParameter("ModifyTime", DateTime.Now));
                                commandRetainedCustomer.Parameters.Add(db.CreateDbParameter("ModifierId", userInfo.Id));
                                commandRetainedCustomer.Parameters.Add(db.CreateDbParameter("ModifierName", userInfo.Name));
                                commandRetainedCustomer.CommandText = sqlUpdateRetainedCustomer;
                                commandRetainedCustomer.ExecuteNonQuery();

                            }



                        }
                        //无异常提交
                        ts.Commit();
                    } catch(Exception) {
                        //报错回滚
                        ts.Rollback();
                        throw;
                    } finally {
                        if(conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }

        public bool 批量导入组织客户信息(string fileName, out int excelImportNum, out List<RetainedCustomerExtend> rightData, out List<RetainedCustomerExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<RetainedCustomerExtend>();
            var rightList = new List<RetainedCustomerExtend>();
            var allList = new List<RetainedCustomerExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("RetainedCustomer", out notNullableFields, out fieldLenght);
                List<object> excelColumns;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    excelOperator.AddColumnDataSource("客户ID", "CustomerCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Credenceapplication_CustomerName, "CustomerName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Validation_Company_Type, "CustomerType");
                    excelOperator.AddColumnDataSource("证件类型", "IdDocumentType");
                    excelOperator.AddColumnDataSource("证件号码", "IdDocumentNumber");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PersonSalesCenterLink_CellNumber, "CellPhoneNumber");
                    excelOperator.AddColumnDataSource("家庭电话", "HomePhoneNumber");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_ContactPhone, "PhoneNumber");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Agency_Email, "Email");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_MarketingDepartment_PostCode, "PostCode");
                    excelOperator.AddColumnDataSource("地址类型", "AddressType");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealerserviceinfo_Area, "RegionName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Agency_Province, "ProvinceName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Agency_City, "CityName");
                    excelOperator.AddColumnDataSource("区县", "CountyName");
                    excelOperator.AddColumnDataSource("详细地址", "Address");
                    excelOperator.AddColumnDataSource("单位名称", "CompanyName");
                    excelOperator.AddColumnDataSource("单位电话", "OfficePhoneNumber");
                    excelOperator.AddColumnDataSource("行业类型", "BusinessType");
                    excelOperator.AddColumnDataSource("地理位置", "Location");
                    excelOperator.AddColumnDataSource("是否为会员", "IfVIP");
                    excelOperator.AddColumnDataSource("会员级别", "MemberType");
                    excelOperator.AddColumnDataSource("VIP卡号", "CardNumber");
                    excelOperator.AddColumnDataSource("使用者性别", "UserGender");

                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;

                    //单次最大导入条数不能大于300！
                    if(excelOperator.LastRowNum > 300) {
                        throw new Exception("单次最大导入条数不能大于300！");
                    }

                    #region 获取对应枚举
                    var keyValuePairs = new[] {
                        new KeyValuePair<string, string>("CustomerType", "Customer_CustomerType"),
                        new KeyValuePair<string, string>("IdDocumentType", "Customer_IdDocumentType"),
                        new KeyValuePair<string, string>("AddressType", "CustomerAddresType"),
                        new KeyValuePair<string, string>("BusinessType", "Customer_BusinessType"),
                        new KeyValuePair<string, string>("Location", "RetainedCustomer_Location"),
                        new KeyValuePair<string, string>("MemberType", "MemberType"),
                        new KeyValuePair<string, string>("IfVIP", "IsOrNot"),
                        new KeyValuePair<string, string>("UserGender", "Sex_Type"),
                    };
                    tempExcelOperator.ImportHelper.LoadEnumFromDb(null, null, keyValuePairs);
                    #endregion
                    Regex regexgz = new Regex(@"^[0-9A-HJ-NP-RT-UW-Y]{2}[0-9]{6}[0-9A-HJ-NP-RT-UW-Y]{10}$");
                    Regex regexName = new Regex(@"[\u4E00-\u9FFF]{5,}");
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var tempImportObj = new RetainedCustomerExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);

                        #region
                        tempImportObj.CustomerCode = newRow["CustomerCode"];
                        tempImportObj.CustomerName = newRow["CustomerName"];
                        tempImportObj.CustomerTypeStr = newRow["CustomerType"];
                        tempImportObj.IdDocumentTypeStr = newRow["IdDocumentType"];
                        tempImportObj.IdDocumentNumber = newRow["IdDocumentNumber"];
                        tempImportObj.CellPhoneNumber = newRow["CellPhoneNumber"];
                        tempImportObj.HomePhoneNumber = newRow["HomePhoneNumber"];
                        tempImportObj.PhoneNumber = newRow["PhoneNumber"];
                        tempImportObj.Email = newRow["Email"];
                        tempImportObj.PostCode = newRow["PostCode"];
                        tempImportObj.AddressTypeStr = newRow["AddressType"];
                        tempImportObj.RegionName = newRow["RegionName"];
                        tempImportObj.ProvinceName = newRow["ProvinceName"];
                        tempImportObj.CityName = newRow["CityName"];
                        tempImportObj.CountyName = newRow["CountyName"];
                        tempImportObj.Address = newRow["Address"];
                        tempImportObj.CompanyName = newRow["CompanyName"];
                        tempImportObj.OfficePhoneNumber = newRow["OfficePhoneNumber"];
                        tempImportObj.BusinessTypeStr = newRow["BusinessType"];
                        tempImportObj.LocationStr = newRow["Location"];
                        tempImportObj.IfVIPStr = newRow["IfVIP"];
                        tempImportObj.MemberTypeStr = newRow["MemberType"];
                        tempImportObj.CardNumber = newRow["CardNumber"];
                        tempImportObj.UserGenderStr = newRow["UserGender"];

                        #endregion

                        var tempErrorMessage = new List<string>();

                        //#region 导入的内容基本检查
                        //导入模板必填
                        if(string.IsNullOrEmpty(tempImportObj.CustomerTypeStr)) {
                            tempErrorMessage.Add("客户属性不能为空");
                        }
                        if(string.IsNullOrEmpty(tempImportObj.IdDocumentTypeStr)) {
                            tempErrorMessage.Add("证件类型不能为空");
                        }
                        if(string.IsNullOrEmpty(tempImportObj.AddressTypeStr)) {
                            tempErrorMessage.Add("地址类型不能为空");
                        }
                        if(string.IsNullOrEmpty(tempImportObj.CustomerName)) {
                            tempErrorMessage.Add("客户姓名不能为空");
                        }

                        if(string.IsNullOrEmpty(tempImportObj.CityName) || string.IsNullOrEmpty(tempImportObj.ProvinceName) || string.IsNullOrEmpty(tempImportObj.CountyName)) {
                            tempErrorMessage.Add("省市区信息必填");
                        }

                        if(!string.IsNullOrEmpty(row["CustomerType"])) {
                            var customerType = tempExcelOperator.ImportHelper.GetEnumValue("CustomerType", row["CustomerType"]);
                            if(!customerType.HasValue) {
                                tempErrorMessage.Add("客户属性不正确");
                            } else {
                                tempImportObj.CustomerType = customerType.Value;
                            }
                        }

                        if(regexName.IsMatch(tempImportObj.CustomerName) == false) {
                            tempErrorMessage.Add("客户名称必须大于4个汉字");
                        }
                        if(!string.IsNullOrEmpty(row["IdDocumentType"])) {
                            var idDocumentType = tempExcelOperator.ImportHelper.GetEnumValue("IdDocumentType", row["IdDocumentType"]);
                            if(!idDocumentType.HasValue || !System.Enum.IsDefined(typeof(DcsCustomerIdDocumentType), idDocumentType)) {
                                tempErrorMessage.Add("证件类型不正确");
                            } else {
                                tempImportObj.IdDocumentType = idDocumentType.Value;
                                if(tempImportObj.IdDocumentType == (int)DcsCustomerIdDocumentType.军官证 || tempImportObj.IdDocumentType == (int)DcsCustomerIdDocumentType.居民身份证 || tempImportObj.IdDocumentType == (int)DcsCustomerIdDocumentType.护照) {
                                    tempErrorMessage.Add("证件类型不是组织客户");
                                }
                                var ErrStr = checkIdDocumentNumber((int)tempImportObj.IdDocumentType, tempImportObj.IdDocumentNumber);
                                if(!string.IsNullOrEmpty(ErrStr))
                                    tempErrorMessage.Add(ErrStr);
                            }
                        }
                        if(!string.IsNullOrEmpty(row["AddressType"])) {
                            var addressType = tempExcelOperator.ImportHelper.GetEnumValue("AddressType", row["AddressType"]);
                            if(!addressType.HasValue) {
                                tempErrorMessage.Add("地址类型不正确");
                            } else {
                                tempImportObj.AddressType = addressType.Value;
                            }
                        }
                        if(!string.IsNullOrEmpty(row["BusinessType"])) {
                            var businessType = tempExcelOperator.ImportHelper.GetEnumValue("BusinessType", row["BusinessType"]);
                            if(!businessType.HasValue) {
                                tempErrorMessage.Add("行业类型不正确");
                            } else {
                                tempImportObj.BusinessType = businessType.Value;
                            }
                        }
                        if(!string.IsNullOrEmpty(row["Location"])) {
                            var location = tempExcelOperator.ImportHelper.GetEnumValue("Location", row["Location"]);
                            if(!location.HasValue) {
                                tempErrorMessage.Add("地理位置不正确");
                            } else {
                                tempImportObj.Location = location.Value;
                            }
                        }
                        if(!string.IsNullOrEmpty(row["IfVIP"])) {
                            var ifVIP = tempExcelOperator.ImportHelper.GetEnumValue("IfVIP", row["IfVIP"]);
                            if(!ifVIP.HasValue) {
                                tempErrorMessage.Add("是否为会员请输入是或否");
                            } else {
                                tempImportObj.IfVIP = Convert.ToBoolean(ifVIP.Value);
                            }
                        }
                        if(!string.IsNullOrEmpty(row["MemberType"])) {
                            var memberType = tempExcelOperator.ImportHelper.GetEnumValue("MemberType", row["MemberType"]);
                            if(!memberType.HasValue) {
                                tempErrorMessage.Add("会员类型不正确");
                            } else {
                                tempImportObj.MemberType = memberType.Value;
                            }
                        }
                        if(!string.IsNullOrEmpty(row["UserGender"])) {
                            var userGender = tempExcelOperator.ImportHelper.GetEnumValue("UserGender", row["UserGender"]);
                            if(!userGender.HasValue) {
                                tempErrorMessage.Add("使用者性别不正确");
                            } else {
                                tempImportObj.UserGender = userGender.Value;
                            }
                        }
                        if(tempErrorMessage.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);

                        return false;
                    });
                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    var groups = tempRightList.GroupBy(r => new {
                        r.CustomerCode,
                    }).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        groupItem.ErrorMsg = ErrorStrings.ImpPartsSupplierRelation_Validation31;
                    }
                    var sparePartsNeedCheck = allList.ToList();
                    var partCodesNeedCheck = sparePartsNeedCheck.Select(r => r.CustomerCode).Distinct().ToArray();
                    var dbSparePartCodes = new List<string>();
                    Func<string[], bool> getDbSparePartCodes = vales => {
                        dbSparePartCodes.Add(vales[0]);
                        return false;
                    };
                    //根据SQL查询数据库，将查出来的结果根据定义的委托中的方法（这里是getDbSparePartCodes）放到具体的集合中（这里是dbSparePartCodes）
                    db.QueryDataWithInOperator("select CustomerCode from Customer where status!=99 ", "CustomerCode", true, partCodesNeedCheck, getDbSparePartCodes);
                    if(partCodesNeedCheck.Length > 0) {
                        var sparePartsExistsCode = sparePartsNeedCheck.Where(r => !dbSparePartCodes.Any(v => v == r.CustomerCode)).ToArray();
                        foreach(var item in sparePartsExistsCode) {
                            item.ErrorMsg = "系统中基础客户ID" + item.CustomerCode + ErrorStrings.Export_Validation_Sparepart_NotExist;
                        }
                    }

                    var dbTiledRegion = new List<TiledRegion>();
                    var dbTiledRegionCheck = new List<string>();
                    dbTiledRegionCheck.Add("1");
                    Func<string[], bool> getTiledRegion = vales => {
                        dbTiledRegion.Add(new TiledRegion {
                            Id = int.Parse(vales[0]),
                            RegionName = vales[1],
                            ProvinceName = vales[2],
                            CityName = vales[3],
                            CountyName = vales[4],

                        });
                        return false;
                    };
                    //根据SQL查询数据库，
                    db.QueryDataWithInOperator("select Id,RegionName,ProvinceName,CityName,CountyName from TiledRegion  ", "1", true, dbTiledRegionCheck.ToArray(), getTiledRegion);
                    var tiledRegionNeedCheck = sparePartsNeedCheck.Where(r => string.IsNullOrEmpty(r.ErrorMsg)).ToArray();
                    foreach(var item in tiledRegionNeedCheck) {
                        var provinceNames = dbTiledRegion.Where(r => r.ProvinceName == item.ProvinceName);
                        if(!provinceNames.Any())
                            provinceNames = dbTiledRegion.Where(r => r.ProvinceName.Contains(item.ProvinceName));
                        if(!provinceNames.Any()) {
                            item.ErrorMsg = "系统中不存在名称为：" + item.ProvinceName + "的省份";
                        } else {
                            item.ProvinceName = provinceNames.FirstOrDefault().ProvinceName;
                            var cityNames = provinceNames.Where(r => r.CityName == item.CityName);
                            if(!cityNames.Any())
                                cityNames = provinceNames.Where(r => r.CityName.Contains(item.CityName));
                            if(!cityNames.Any()) {
                                item.ErrorMsg = "系统中不存在名称为：" + item.CityName + "的城市";
                            } else {
                                item.CityName = cityNames.FirstOrDefault().CityName;
                                var countyNames = cityNames.Where(r => r.CountyName == item.CountyName);
                                if(!countyNames.Any())
                                    countyNames = cityNames.Where(r => r.CountyName.Contains(item.CountyName));
                                if(!countyNames.Any()) {
                                    item.ErrorMsg = "系统中不存在名称为：" + item.CountyName + "的区县";
                                } else {
                                    item.CountyName = countyNames.FirstOrDefault().CountyName;
                                    item.RegionName = countyNames.FirstOrDefault().RegionName;
                                }
                            }
                        }
                    }
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    rightList = allList.Except(errorList).ToList();


                }
                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempObj = list[index - 1];
                            var values = new object[] {
                                #region 导出字段
                                tempObj.CustomerCode, tempObj.CustomerName, tempObj.CustomerTypeStr, tempObj.IdDocumentTypeStr, tempObj.IdDocumentNumber, tempObj.CellPhoneNumber, tempObj.HomePhoneNumber, tempObj.PhoneNumber, tempObj.Email, tempObj.PostCode, tempObj.AddressTypeStr, tempObj.RegionName, tempObj.ProvinceName, tempObj.CityName, tempObj.CountyName, tempObj.Address, tempObj.CompanyName, tempObj.OfficePhoneNumber, tempObj.BusinessTypeStr, tempObj.LocationStr, tempObj.IfVIPStr, tempObj.MemberTypeStr, tempObj.CardNumber, tempObj.UserGenderStr, tempObj.ErrorMsg
                                #endregion
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }


                //导入所有合格数据
                if(!rightList.Any())
                    return true;
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    //开启事务
                    var ts = conn.BeginTransaction();
                    try {
                        if(rightList.Any()) {
                            var customerCodeCheck = rightList.Select(r => r.CustomerCode).Distinct().ToArray();
                            var dbCustomer = new List<RetainedCustomerExtend>();
                            Func<string[], bool> getDbcustomers = values => {
                                dbCustomer.Add(new RetainedCustomerExtend {
                                    Id = int.Parse(values[0]),
                                    CustomerCode = values[1],
                                });
                                return false;
                            };
                            db.QueryDataWithInOperator("select Id,CustomerCode from Customer", "CustomerCode", true, customerCodeCheck, getDbcustomers);
                            var userInfo = Utils.GetCurrentUserInfo();
                            foreach(var item in rightList) {
                                List<string> fields = new List<string>();
                                fields.Add("ModifyTime");
                                fields.Add("ModifierId");
                                fields.Add("ModifierName");
                                string sqlUpdateCustomer = null;
                                var commandCustomer = db.CreateDbCommand(sqlUpdateCustomer, conn, ts);
                                if(!string.IsNullOrEmpty(item.CustomerName)) {
                                    fields.Add("Name");
                                    commandCustomer.Parameters.Add(db.CreateDbParameter("Name", item.CustomerName));
                                }

                                if(!string.IsNullOrEmpty(item.CustomerTypeStr)) {
                                    fields.Add("CustomerType");
                                    commandCustomer.Parameters.Add(db.CreateDbParameter("CustomerType", item.CustomerType));
                                }
                                if(!string.IsNullOrEmpty(item.RegionName)) {
                                    fields.Add("RegionName");
                                    commandCustomer.Parameters.Add(db.CreateDbParameter("RegionName", item.RegionName));
                                }
                                if(!string.IsNullOrEmpty(item.ProvinceName)) {
                                    fields.Add("ProvinceName");
                                    commandCustomer.Parameters.Add(db.CreateDbParameter("ProvinceName", item.ProvinceName));
                                }
                                if(!string.IsNullOrEmpty(item.CityName)) {
                                    fields.Add("CityName");
                                    commandCustomer.Parameters.Add(db.CreateDbParameter("CityName", item.CityName));
                                }
                                if(!string.IsNullOrEmpty(item.CountyName)) {
                                    fields.Add("CountyName");
                                    commandCustomer.Parameters.Add(db.CreateDbParameter("CountyName", item.CountyName));
                                }
                                if(!string.IsNullOrEmpty(item.Address)) {
                                    fields.Add("Address");
                                    commandCustomer.Parameters.Add(db.CreateDbParameter("Address", item.Address));
                                }
                                if(!string.IsNullOrEmpty(item.PostCode)) {
                                    fields.Add("PostCode");
                                    commandCustomer.Parameters.Add(db.CreateDbParameter("PostCode", item.PostCode));
                                }
                                if(!string.IsNullOrEmpty(item.CellPhoneNumber)) {
                                    fields.Add("CellPhoneNumber");
                                    commandCustomer.Parameters.Add(db.CreateDbParameter("CellPhoneNumber", item.CellPhoneNumber));
                                }
                                if(!string.IsNullOrEmpty(item.HomePhoneNumber)) {
                                    fields.Add("HomePhoneNumber");
                                    commandCustomer.Parameters.Add(db.CreateDbParameter("HomePhoneNumber", item.HomePhoneNumber));
                                }
                                if(!string.IsNullOrEmpty(item.OfficePhoneNumber)) {
                                    fields.Add("OfficePhoneNumber");
                                    commandCustomer.Parameters.Add(db.CreateDbParameter("OfficePhoneNumber", item.OfficePhoneNumber));
                                }
                                if(!string.IsNullOrEmpty(item.IdDocumentTypeStr)) {
                                    fields.Add("IdDocumentType");
                                    commandCustomer.Parameters.Add(db.CreateDbParameter("IdDocumentType", item.IdDocumentType));
                                }
                                if(!string.IsNullOrEmpty(item.IdDocumentNumber)) {
                                    fields.Add("IdDocumentNumber");
                                    commandCustomer.Parameters.Add(db.CreateDbParameter("IdDocumentNumber", item.IdDocumentNumber));
                                }
                                if(!string.IsNullOrEmpty(item.Email)) {
                                    fields.Add("Email");
                                    commandCustomer.Parameters.Add(db.CreateDbParameter("Email", item.Email));
                                }
                                if(!string.IsNullOrEmpty(item.CompanyName)) {
                                    fields.Add("CompanyName");
                                    commandCustomer.Parameters.Add(db.CreateDbParameter("CompanyName", item.CompanyName));
                                }
                                if(!string.IsNullOrEmpty(item.BusinessTypeStr)) {
                                    fields.Add("BusinessType");
                                    commandCustomer.Parameters.Add(db.CreateDbParameter("BusinessType", item.BusinessType));
                                }

                                if(!string.IsNullOrEmpty(item.AddressTypeStr)) {
                                    fields.Add("AddressType");
                                    commandCustomer.Parameters.Add(db.CreateDbParameter("AddressType", item.AddressType));
                                }
                                if(!string.IsNullOrEmpty(item.PhoneNumber)) {
                                    fields.Add("PhoneNumber");
                                    commandCustomer.Parameters.Add(db.CreateDbParameter("PhoneNumber", item.PhoneNumber));
                                }

                                var customers = dbCustomer.FirstOrDefault(v => v.CustomerCode == item.CustomerCode);
                                sqlUpdateCustomer = db.GetUpdateSql("Customer", fields.ToArray(), new[] { "Id" });
                                commandCustomer.Parameters.Add(db.CreateDbParameter("Id", customers.Id));
                                commandCustomer.Parameters.Add(db.CreateDbParameter("ModifyTime", DateTime.Now));
                                commandCustomer.Parameters.Add(db.CreateDbParameter("ModifierId", userInfo.Id));
                                commandCustomer.Parameters.Add(db.CreateDbParameter("ModifierName", userInfo.Name));
                                commandCustomer.CommandText = sqlUpdateCustomer;
                                commandCustomer.ExecuteNonQuery();

                                string sqlUpdateRetainedCustomer = null;
                                List<string> fieldRetainedCustomers = new List<string>();
                                fieldRetainedCustomers.Add("ModifyTime");
                                fieldRetainedCustomers.Add("ModifierId");
                                fieldRetainedCustomers.Add("ModifierName");
                                var commandRetainedCustomer = db.CreateDbCommand(sqlUpdateRetainedCustomer, conn, ts);

                                if(!string.IsNullOrEmpty(item.CardNumber)) {
                                    fieldRetainedCustomers.Add("CardNumber");
                                    commandRetainedCustomer.Parameters.Add(db.CreateDbParameter("CardNumber", item.CardNumber));
                                }
                                if(!string.IsNullOrEmpty(item.IfVIPStr)) {
                                    fieldRetainedCustomers.Add("IfVIP");
                                    commandRetainedCustomer.Parameters.Add(db.CreateDbParameter("IfVIP", item.IfVIP));
                                }
                                if(!string.IsNullOrEmpty(item.MemberTypeStr)) {
                                    fieldRetainedCustomers.Add("MemberType");
                                    commandRetainedCustomer.Parameters.Add(db.CreateDbParameter("MemberType", item.MemberType));
                                }
                                if(!string.IsNullOrEmpty(item.LocationStr)) {
                                    fieldRetainedCustomers.Add("Location");
                                    commandRetainedCustomer.Parameters.Add(db.CreateDbParameter("Location", item.Location));
                                }
                                if(!string.IsNullOrEmpty(item.UserGenderStr)) {
                                    fieldRetainedCustomers.Add("UserGender");
                                    commandRetainedCustomer.Parameters.Add(db.CreateDbParameter("UserGender", item.UserGender));
                                }
                                sqlUpdateRetainedCustomer = db.GetUpdateSql("RetainedCustomer", fieldRetainedCustomers.ToArray(), new[] { "CustomerId" });
                                commandRetainedCustomer.Parameters.Add(db.CreateDbParameter("CustomerId", customers.Id));
                                commandRetainedCustomer.Parameters.Add(db.CreateDbParameter("ModifyTime", DateTime.Now));
                                commandRetainedCustomer.Parameters.Add(db.CreateDbParameter("ModifierId", userInfo.Id));
                                commandRetainedCustomer.Parameters.Add(db.CreateDbParameter("ModifierName", userInfo.Name));
                                commandRetainedCustomer.CommandText = sqlUpdateRetainedCustomer;
                                commandRetainedCustomer.ExecuteNonQuery();

                            }
                        }
                        //无异常提交
                        ts.Commit();
                    } catch(Exception) {
                        //报错回滚
                        ts.Rollback();
                        throw;
                    } finally {
                        if(conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }


        private string checkIdDocumentNumber(int idDocumentType, string idDocumentNumber) {
            if(idDocumentType == (int)DcsCustomerIdDocumentType.居民身份证) {
                if(idDocumentNumber.Length == 18) {
                    char[] chaStr = idDocumentNumber.ToCharArray();
                    List<int> list = new List<int> {
                        7,
                        9,
                        10,
                        5,
                        8,
                        4,
                        2,
                        1,
                        6,
                        3,
                        7,
                        9,
                        10,
                        5,
                        8,
                        4,
                        2
                    }; //身份证对应系数                           
                    string Num = "10X98765432";
                    int sum = 0;
                    for(int i = 0; i < list.Count; i++) {
                        sum += list[i] * Convert.ToInt32(chaStr[i].ToString());
                    }
                    int a = sum % 11;
                    if(Num.Substring(a, 1) != idDocumentNumber.ToUpper().Substring(17, 1)) {
                        return "请输入合法的居民身份证号码";
                    }
                } else if(idDocumentNumber.Length != 15) {
                    return "请输入正确的居民身份证号码.15或18位";
                }
            }
            if(idDocumentType == (int)DcsCustomerIdDocumentType.组织机构代码证) { //应该是证件类型=组织机构代码证情况下.目前没字典项 暂时用毕业证类型代替
                switch(idDocumentNumber.Length) {
                    case 10:
                        string strNum = "^[A-Za-z0-9]+$";
                        Regex regexNum = new Regex(strNum);
                        string idDocumentNumbers = idDocumentNumber.Substring(0, 8);
                        string idDocumentNumberTens = idDocumentNumber.Substring(9, 1);
                        if(regexNum.IsMatch(idDocumentNumbers) == false) {
                            return "请输入正确的组织机构代码证号.前8位为数字或字母";
                        }
                        if(idDocumentNumber.Substring(8, 1) != ("-")) {
                            return "请输入正确的组织机构代码证号.第9位为连接符“-”";
                        }
                        if(regexNum.IsMatch(idDocumentNumberTens) == false) {
                            return "请输入正确的组织机构代码证号.第十位为数字或字母";
                        }
                        break;
                    default:
                        return "请输入正确的组织机构代码证号.10位";
                }
            }
            if(idDocumentType == (int)DcsCustomerIdDocumentType.护照) {
                switch(idDocumentNumber.Length) {
                    case 8:
                        string zm = "[a-zA-Z]";
                        Regex regexZm = new Regex(zm);
                        string idDocumentNumberZm = idDocumentNumber.Substring(0, 1);
                        if(regexZm.IsMatch(idDocumentNumberZm) == false) {
                            return "请输入正确的护照号码.第一位为字母";
                        }
                        string strsSz = @"^\d+$";
                        Regex regexSz = new Regex(strsSz);
                        string idDocumentNumberSz = idDocumentNumber.Substring(1, 7);
                        if(regexSz.IsMatch(idDocumentNumberSz) == false) {
                            return "请输入正确的护照号码.后7位为数字";
                        }
                        break;
                    case 9:
                        string zm9 = "[a-zA-Z]";
                        Regex regexZm9 = new Regex(zm9);
                        string idDocumentNumberZm9 = idDocumentNumber.Substring(0, 1);
                        if(regexZm9.IsMatch(idDocumentNumberZm9) == false) {
                            return "请输入正确的护照号码.第一位为字母";
                        }
                        string strsSz9 = @"^\d+$";
                        Regex regexSz9 = new Regex(strsSz9);
                        string idDocumentNumberSz9 = idDocumentNumber.Substring(1, 8);
                        if(regexSz9.IsMatch(idDocumentNumberSz9) == false) {
                            return "请输入正确的护照号码.后8位为数字";
                        }
                        break;
                    default:
                        return "请输入正确的护照号码.8或9位";
                }
            }
            if(idDocumentType == (int)DcsCustomerIdDocumentType.统一社会信用代码) {
                idDocumentNumber = idDocumentNumber.ToUpper();
                if(idDocumentNumber.Length != 18) {
                    return "请输入正确的统一社会信用代码,18位";
                } else {
                    Regex regexgz = new Regex(@"^[0-9A-HJ-NP-RT-UW-Y]{2}[0-9]{6}[0-9A-HJ-NP-RT-UW-Y]{10}$");
                    if(regexgz.IsMatch(idDocumentNumber) == false) {
                        return "请输入正确的统一社会信用代码.数字或大写英文字母组合(不使用I、O、Z、S、V),3-8位只能是数字";
                    }
                }
            }
            if(idDocumentType == (int)DcsCustomerIdDocumentType.军官证) {
                if(idDocumentNumber.Length != 12) {
                    return "请输入正确的军官证号码,12位";
                } else {
                    Regex regexgz = new Regex(@"南字第(\d{8})号|北字第(\d{8})号|沈字第(\d{8})号|兰字第(\d{8})号|成字第(\d{8})号|济字第(\d{8})号|广字第(\d{8})号|海字第(\d{8})号|空字第(\d{8})号|参字第(\d{8})号|政字第(\d{8})号|后字第(\d{8})号|装字第(\d{8})号");
                    if(regexgz.IsMatch(idDocumentNumber) == false) {
                        return "请输入正确的军官证号码：第一个为汉字（“南”、“北”、“沈”、“兰”、“成”、“济”、“广”、“海”、“空”、“参”、“政”、“后”、“装”，其中之一）+“字第”+8位数字（0~9） +“号”";
                    }
                }
            }
            return null;
        }
    }
}
