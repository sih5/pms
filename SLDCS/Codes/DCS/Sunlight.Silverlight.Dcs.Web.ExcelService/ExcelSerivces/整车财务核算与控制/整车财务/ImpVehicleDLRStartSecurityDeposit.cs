﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        public bool ImportVehicleDLRStartSecurityDeposit(string fileName, out int excelImportNum, out List<ImpVehicleDLRStartSecurityDeposit> errorData, out string errorDataFileName, out string errorMessage) {
            //初始化返回数据
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            List<ImpVehicleDLRStartSecurityDeposit> errorVehicleDLRStartSecurityDeposits = null;
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception("身份认证失效，请登录系统后再次尝试");

                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("ImpVehicleDLRStartSecurityDeposit", out notNullableFields, out fieldLenght);

                errorVehicleDLRStartSecurityDeposits = new List<ImpVehicleDLRStartSecurityDeposit>();
                var vehicleDLRStartSecurityDeposits = new List<ImpVehicleDLRStartSecurityDeposit>();

                List<object> excelColumns;
                //Excel数据导入
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    //指定Excel列名与字段名关系
                    excelOperator.AddColumnDataSource("经销商编号", "DealerCode");
                    excelOperator.AddColumnDataSource("记账日期", "TimeOfRecord", typeof(DateTime));
                    excelOperator.AddColumnDataSource("借方", "Debtor");
                    excelOperator.AddColumnDataSource("贷方", "Lender");
                    excelOperator.AddColumnDataSource("本期余额借/贷", "CurrentBalance");
                    excelOperator.AddColumnDataSource("摘要", "Remark");

                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();

                    //Excel单行导入
                    List<ImpVehicleDLRStartSecurityDeposit> deposits = errorVehicleDLRStartSecurityDeposits;
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        //接收Excel导入数据
                        var vehicleDLRStartSecurityDeposit = new ImpVehicleDLRStartSecurityDeposit {
                            DealerCode = row["DealerCode"],
                            Remark = row["Remark"],
                        };
                        var errorMsgs = new List<string>();
                        //记账日期检查
                        if(!string.IsNullOrEmpty(row["TimeOfRecord"])) {
                            DateTime timeOfRecord;
                            if(DateTime.TryParse(row["TimeOfRecord"], out timeOfRecord))
                                vehicleDLRStartSecurityDeposit.TimeOfRecord = timeOfRecord;
                            else {
                                vehicleDLRStartSecurityDeposit.TimeOfRecordStr = row["TimeOfRecord"] != null ? row["TimeOfRecord"].Trim() : "";
                                errorMsgs.Add(ErrorStrings.ImpVehicleDLRStartSecurityDeposit_Validation1);
                            }
                        }
                        //借方检查
                        if(!string.IsNullOrEmpty(row["Debtor"])) {
                            decimal debtor;
                            if(decimal.TryParse(row["Debtor"], out debtor))
                                vehicleDLRStartSecurityDeposit.Debtor = debtor;
                            else {
                                vehicleDLRStartSecurityDeposit.DebtorStr = row["Debtor"] != null ? row["Debtor"].Trim() : "";
                                errorMsgs.Add(ErrorStrings.ImpVehicleDLRStartSecurityDeposit_Validation2);
                            }
                        }
                        //贷方检查
                        if(!string.IsNullOrEmpty(row["Lender"])) {
                            decimal lender;
                            if(decimal.TryParse(row["Lender"], out lender))
                                vehicleDLRStartSecurityDeposit.Lender = lender;
                            else {
                                vehicleDLRStartSecurityDeposit.DebtorStr = row["Lender"] != null ? row["Lender"].Trim() : "";
                                errorMsgs.Add(ErrorStrings.ImpVehicleDLRStartSecurityDeposit_Validation3);
                            }
                        }
                        //本期余额借/贷检查
                        if(!string.IsNullOrEmpty(row["CurrentBalance"])) {
                            decimal currentBalance;
                            if(decimal.TryParse(row["CurrentBalance"], out currentBalance))
                                vehicleDLRStartSecurityDeposit.CurrentBalance = currentBalance;
                            else {
                                vehicleDLRStartSecurityDeposit.CurrentBalanceStr = row["CurrentBalance"] != null ? row["CurrentBalance"].Trim() : "";
                                errorMsgs.Add(ErrorStrings.ImpVehicleDLRStartSecurityDeposit_Validation4);
                            }
                        }
                        if(errorMsgs.Count > 0) {
                            vehicleDLRStartSecurityDeposit.ErrorMsg = string.Join("; ", errorMsgs);
                            deposits.Add(vehicleDLRStartSecurityDeposit);
                        } else
                            vehicleDLRStartSecurityDeposits.Add(vehicleDLRStartSecurityDeposit);
                        return false;
                    });
                    var dealerCodes = vehicleDLRStartSecurityDeposits.Select(v => v.DealerCode.ToUpper()).Distinct().ToArray();
                    var dealers = new List<Dealer>();
                    Func<string[], bool> dealProduct = values => {
                        dealers.Add(new Dealer {
                            Id = int.Parse(values[0]),
                            Code = values[1],
                            Name = values[2]
                        });
                        //是否需要停止查询
                        return false;
                    };
                    db.QueryDataWithInOperator("select Id, Code,Name from Dealer", "Code", true, dealerCodes, dealProduct);
                    for(var i = vehicleDLRStartSecurityDeposits.Count - 1; i >= 0; i--) {
                        var dealer = dealers.FirstOrDefault(v => String.Compare(v.Code, vehicleDLRStartSecurityDeposits[i].DealerCode, StringComparison.OrdinalIgnoreCase) == 0);
                        if(dealer == null) {
                            vehicleDLRStartSecurityDeposits[i].ErrorMsg = string.Format(ErrorStrings.ImpVehicleDLRStartSecurityDeposit_Validation5, vehicleDLRStartSecurityDeposits[i].DealerCode);
                            errorVehicleDLRStartSecurityDeposits.Add(vehicleDLRStartSecurityDeposits[i]);
                        } else {
                            vehicleDLRStartSecurityDeposits[i].DealerId = dealer.Id;
                            vehicleDLRStartSecurityDeposits[i].DealerName = dealer.Name;
                        }
                    }
                }
                var rightvehicleDLRStartSecurityDeposits = vehicleDLRStartSecurityDeposits.Except(errorVehicleDLRStartSecurityDeposits).ToArray();


                if(errorVehicleDLRStartSecurityDeposits.Any()) {
                    excelColumns.Add("校验信息");
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        List<ImpVehicleDLRStartSecurityDeposit> deposits = errorVehicleDLRStartSecurityDeposits;
                        excelExport.ExportByRow(index => {
                            if(index == deposits.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var deposit = deposits[index - 1];
                            var values = new object[] {
                                deposit.DealerCode, deposit.TimeOfRecord, deposit.Debtor, deposit.Lender, deposit.CurrentBalance, deposit.Remark, deposit.ErrorMsg
                            };
                            return values;
                        });
                    }
                    errorVehicleDLRStartSecurityDeposits = null;
                }

                //导入正常结束，不存在需要写入数据库的合法数据
                if(!rightvehicleDLRStartSecurityDeposits.Any())
                    return true;

                //写数据
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    //开启事务，新增更新配件在一个事务内
                    var ts = conn.BeginTransaction();
                    try {
                        //新增配件
                        if(rightvehicleDLRStartSecurityDeposits.Any()) {
                            //获取新增数据的sql语句，Id为主键
                            var sqlInsert = db.GetInsertSql("VehicleDLRStartSecurityDeposit", "Id", new[] {
                                "Code", "DealerId", "DealerName", "DealerCode", "Status", "Remark", "Debtor", "Lender", "CurrentBalance", "TimeOfRecord", "CreatorId", "CreatorName", "CreateTime"
                            });
                            var userInfo = Utils.GetCurrentUserInfo();
                            foreach(var vehicleDLRStartSecurityDeposit in rightvehicleDLRStartSecurityDeposits) {
                                var command = db.CreateDbCommand(sqlInsert, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("Code", CodeGenerator.Generate(db, "VehicleDLRStartSecurityDeposit", vehicleDLRStartSecurityDeposit.DealerCode)));
                                command.Parameters.Add(db.CreateDbParameter("DealerId", vehicleDLRStartSecurityDeposit.DealerId));
                                command.Parameters.Add(db.CreateDbParameter("DealerName", vehicleDLRStartSecurityDeposit.DealerName));
                                command.Parameters.Add(db.CreateDbParameter("DealerCode", vehicleDLRStartSecurityDeposit.DealerCode));
                                command.Parameters.Add(db.CreateDbParameter("Status", (int)DcsBaseDataStatus.有效));
                                command.Parameters.Add(db.CreateDbParameter("Remark", vehicleDLRStartSecurityDeposit.Remark));
                                command.Parameters.Add(db.CreateDbParameter("Debtor", vehicleDLRStartSecurityDeposit.Debtor));
                                command.Parameters.Add(db.CreateDbParameter("Lender", vehicleDLRStartSecurityDeposit.Lender));
                                command.Parameters.Add(db.CreateDbParameter("CurrentBalance", vehicleDLRStartSecurityDeposit.CurrentBalance));
                                command.Parameters.Add(db.CreateDbParameter("TimeOfRecord", vehicleDLRStartSecurityDeposit.TimeOfRecord));
                                command.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                command.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                command.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                command.ExecuteNonQuery();
                            }
                        }
                        //无异常提交
                        ts.Commit();
                    } catch(Exception) {
                        //报错回滚
                        ts.Rollback();
                        throw;
                    } finally {
                        if(conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorVehicleDLRStartSecurityDeposits = null;
                return false;
            } finally {
                errorData = errorVehicleDLRStartSecurityDeposits;
            }
        }
    }
}
