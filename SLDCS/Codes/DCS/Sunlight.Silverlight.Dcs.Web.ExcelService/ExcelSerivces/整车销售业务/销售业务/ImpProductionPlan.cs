﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        public bool ImportProductionPlan(string fileName, out int excelImportNum, out List<ImpProductionPlan> errorData, out string errorDataFileName, out string errorMessage) {
            //初始化返回数据
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            List<ImpProductionPlan> errorProductionPlans = null;

            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception("身份认证失效，请登录系统后再次尝试");

                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("ProductionPlan", out notNullableFields, out fieldLenght);

                errorProductionPlans = new List<ImpProductionPlan>();
                var productionPlans = new List<ImpProductionPlan>();
                List<object> excelColumns;

                //Excel数据导入
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    //指定Excel列名与字段名关系
                    excelOperator.AddColumnDataSource("订单年", "YearOfOrder");
                    excelOperator.AddColumnDataSource("订单月", "MonthOfOrder");
                    excelOperator.AddColumnDataSource("车型编号", "ProductCategoryCode");
                    excelOperator.AddColumnDataSource("产品编号", "ProductCode");
                    excelOperator.AddColumnDataSource("SON", "ProductionPlanSON");
                    excelOperator.AddColumnDataSource("工厂确认状态", "Status");
                    excelOperator.AddColumnDataSource("下线日期", "RolloutDate", typeof(DateTime));

                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;
                    //加载导入相关的字典项
                    tempExcelOperator.ImportHelper.LoadEnumFromDb(null, new KeyValuePair<string, string>("Status", "ProductionPlan_Status"));
                    //Excel单行导入
                    List<ImpProductionPlan> plans = errorProductionPlans;
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        //接收Excel导入数据
                        var productionPlan = new ImpProductionPlan {
                            ProductCode = row["ProductCode"],
                            ProductCategoryCode = row["ProductCategoryCode"],
                            ProductionPlanSON = row["ProductionPlanSON"],
                        };
                        var errorMsgs = new List<string>();
                        //产品编号检查
                        var fieldIndex = notNullableFields.IndexOf("PRODUCTCODE");
                        if(string.IsNullOrEmpty(productionPlan.ProductCode)) {
                            if(fieldIndex > -1)
                                errorMsgs.Add(ErrorStrings.ImpProductionPlan_Validation1);
                        } else {
                            if(Encoding.Default.GetByteCount(productionPlan.ProductCode) > fieldLenght["PRODUCTCODE"])
                                errorMsgs.Add(ErrorStrings.ImpProductionPlan_Validation2);
                        }
                        //车型编号检查
                        fieldIndex = notNullableFields.IndexOf("PRODUCTCATEGORYCODE");
                        if(string.IsNullOrEmpty(productionPlan.ProductCategoryCode)) {
                            if(fieldIndex > -1)
                                errorMsgs.Add(ErrorStrings.ImpProductionPlan_Validation12);
                        } else {
                            if(Encoding.Default.GetByteCount(productionPlan.ProductCategoryCode) > fieldLenght["PRODUCTCATEGORYCODE"])
                                errorMsgs.Add(ErrorStrings.ImpProductionPlan_Validation13);
                        }
                        //订单年检查
                        fieldIndex = notNullableFields.IndexOf("YEAROFORDER");
                        if(string.IsNullOrEmpty(row["YearOfOrder"])) {
                            if(fieldIndex > -1)
                                errorMsgs.Add(ErrorStrings.ImpProductionPlan_Validation3);
                        } else {
                            productionPlan.YearOfOrderStr = row["YearOfOrder"];
                            int yearOfOrder;
                            if(int.TryParse(row["YearOfOrder"], out yearOfOrder))
                                productionPlan.YearOfOrder = yearOfOrder;
                            else
                                errorMsgs.Add(ErrorStrings.ImpProductionPlan_Validation4);
                        }
                        //订单月检查
                        fieldIndex = notNullableFields.IndexOf("MONTHOFORDER");
                        if(string.IsNullOrEmpty(row["MonthOfOrder"])) {
                            if(fieldIndex > -1)
                                errorMsgs.Add(ErrorStrings.ImpProductionPlan_Validation5);
                        } else {
                            productionPlan.MonthOfOrderStr = row["MonthOfOrder"];
                            int monthOfOrder;
                            if(int.TryParse(row["MonthOfOrder"], out monthOfOrder))
                                productionPlan.MonthOfOrder = monthOfOrder;
                            else
                                errorMsgs.Add(ErrorStrings.ImpProductionPlan_Validation6);
                        }
                        //工厂确认状态检查&字典项检查
                        fieldIndex = notNullableFields.IndexOf("STATUS");
                        if(string.IsNullOrEmpty(row["Status"])) {
                            if(fieldIndex > -1)
                                errorMsgs.Add(ErrorStrings.ImpProductionPlan_Validation7);
                        } else {
                            productionPlan.StatusStr = row["Status"];
                            var status = tempExcelOperator.ImportHelper.GetEnumValue("Status", row["Status"]);
                            if(!status.HasValue)
                                errorMsgs.Add(ErrorStrings.ImpProductionPlan_Validation8);
                            else
                                productionPlan.Status = status.Value;
                        }
                        //下线日期检查
                        if(!string.IsNullOrEmpty(row["RolloutDate"])) {
                            productionPlan.RolloutDateStr = row["RolloutDate"];
                            DateTime rolloutDate;
                            if(DateTime.TryParse(row["RolloutDate"], out rolloutDate))
                                productionPlan.RolloutDate = rolloutDate;
                            else
                                errorMsgs.Add(ErrorStrings.ImpProductionPlan_Validation9);
                        }
                        if(errorMsgs.Count > 0) {
                            productionPlan.ErrorMsg = string.Join("; ", errorMsgs);
                            plans.Add(productionPlan);
                        } else
                            productionPlans.Add(productionPlan);
                        return false;
                    });
                    var productCodes = productionPlans.Select(v => v.ProductCode.ToUpper()).Distinct().ToArray();
                    var products = new List<Product>();
                    Func<string[], bool> dealProduct = values => {
                        products.Add(new Product {
                            Id = int.Parse(values[0]),
                            Code = values[1],
                            Name = values[2]
                        });
                        return false;
                    };
                    db.QueryDataWithInOperator("select Id, Code, Name from Product", "Code", true, productCodes, dealProduct);
                    var vehicleModelAffiProducts = new List<VehicleModelAffiProduct>();
                    var productIds = products.Select(r => r.Id.ToString(CultureInfo.InvariantCulture)).Distinct().ToArray();
                    Func<string[], bool> dealVehicleModelAffiProduct = values => {
                        vehicleModelAffiProducts.Add(new VehicleModelAffiProduct {
                            ProductId = int.Parse(values[0]),
                            ProductCategoryCode = values[1],
                            ProductCategoryName = values[2]
                        });
                        return false;
                    };
                    db.QueryDataWithInOperator("select ProductId, ProductCategoryCode, ProductCategoryName from VehicleModelAffiProduct", "ProductId", true, productIds, dealVehicleModelAffiProduct);
                    for(var i = productionPlans.Count - 1; i >= 0; i--) {
                        var product = products.FirstOrDefault(v => String.Compare(v.Code, productionPlans[i].ProductCode, StringComparison.OrdinalIgnoreCase) == 0);
                        if(product == null) {
                            productionPlans[i].ErrorMsg = string.Format(ErrorStrings.ImpProductionPlan_Validation10, productionPlans[i].ProductCode);
                            errorProductionPlans.Add(productionPlans[i]);
                        } else {
                            productionPlans[i].ProductId = product.Id;
                            productionPlans[i].ProductName = product.Name;
                            var vehicleModelAffiProduct = vehicleModelAffiProducts.FirstOrDefault(v => v.ProductId == product.Id);
                            if(vehicleModelAffiProduct == null) {
                                productionPlans[i].ErrorMsg = string.Format(ErrorStrings.ImpProductionPlan_Validation11, productionPlans[i].ProductCode);
                                errorProductionPlans.Add(productionPlans[i]);
                            } else {
                                productionPlans[i].ProductCategoryCode = vehicleModelAffiProduct.ProductCategoryCode;
                                productionPlans[i].ProductCategoryName = vehicleModelAffiProduct.ProductCategoryName;
                            }
                        }
                    }
                }
                var rightProductionPlans = productionPlans.Except(errorProductionPlans).ToArray();
                if(errorProductionPlans.Any()) {
                    excelColumns.Add("校验信息");
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        List<ImpProductionPlan> plans = errorProductionPlans;
                        excelExport.ExportByRow(index => {
                            if(index == plans.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var productionPlan = plans[index - 1];
                            var values = new object[]{
                                        productionPlan.YearOfOrderStr, productionPlan.MonthOfOrderStr,
                                        productionPlan.ProductCategoryCode, productionPlan.ProductCode,
                                        productionPlan.ProductionPlanSON, productionPlan.StatusStr,
                                        productionPlan.RolloutDateStr, productionPlan.ErrorMsg
                                    };
                            return values;
                        });
                    }
                    errorProductionPlans = null;
                }

                //导入正常结束，不存在需要写入数据库的合法数据
                if(!rightProductionPlans.Any())
                    return true;

                //写数据
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    //开启事务，新增更新配件在一个事务内
                    var ts = conn.BeginTransaction();
                    try {
                        //新增配件
                        if(rightProductionPlans.Any()) {
                            //获取新增数据的sql语句，Id为主键
                            var sqlInsert = db.GetInsertSql("ProductionPlan", "Id", new[] {
                                "ProductId", "ProductCode", "ProductName", "ProductCategoryCode", "ProductCategoryName", "YearOfOrder", "MonthOfOrder", "ProductionPlanSON", "RolloutDate", "Status", "CreatorId","CreatorName","CreateTime"
                            });
                            var userInfo = Utils.GetCurrentUserInfo();
                            foreach(var productionPlan in rightProductionPlans) {
                                var command = db.CreateDbCommand(sqlInsert, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("ProductId", productionPlan.ProductId));
                                command.Parameters.Add(db.CreateDbParameter("ProductCode", productionPlan.ProductCode));
                                command.Parameters.Add(db.CreateDbParameter("ProductName", productionPlan.ProductName));
                                command.Parameters.Add(db.CreateDbParameter("ProductCategoryCode", productionPlan.ProductCategoryCode));
                                command.Parameters.Add(db.CreateDbParameter("ProductCategoryName", productionPlan.ProductCategoryName));
                                command.Parameters.Add(db.CreateDbParameter("YearOfOrder", productionPlan.YearOfOrder));
                                command.Parameters.Add(db.CreateDbParameter("MonthOfOrder", productionPlan.MonthOfOrder));
                                command.Parameters.Add(db.CreateDbParameter("ProductionPlanSON", productionPlan.ProductionPlanSON));
                                command.Parameters.Add(db.CreateDbParameter("RolloutDate", productionPlan.RolloutDate));
                                command.Parameters.Add(db.CreateDbParameter("Status", productionPlan.Status));
                                command.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                command.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                command.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                command.ExecuteNonQuery();
                            }
                        }
                        //无异常提交
                        ts.Commit();
                    } catch(Exception) {
                        //报错回滚
                        ts.Rollback();
                        throw;
                    } finally {
                        if(conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorProductionPlans = null;
                return false;
            } finally {
                errorData = errorProductionPlans;
            }
        }
    }
}