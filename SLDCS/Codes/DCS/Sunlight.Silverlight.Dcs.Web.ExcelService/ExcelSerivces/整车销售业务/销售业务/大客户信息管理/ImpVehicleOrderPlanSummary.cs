﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        public bool ImportVehicleOrderPlanSummaryAdd(string fileName, out string errorDataFileName, out string errorMessage) {
            //初始化返回数据
            errorMessage = "";
            errorDataFileName = "";

            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception("身份认证失效，请登录系统后再次尝试");

                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("VehicleOrderPlanSummary", out notNullableFields, out fieldLenght);

                var errorImportVehicleOrderPlanSummaries = new List<ImportVehicleOrderPlanSummary>();
                var importVehicleOrderPlanSummaries = new List<ImportVehicleOrderPlanSummary>();
                List<object> excelColumns;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    //指定Excel列名与字段名关系
                    excelOperator.AddColumnDataSource("订单年", "YearOfOrder");
                    excelOperator.AddColumnDataSource("订单月", "MonthOfOrder");
                    excelOperator.AddColumnDataSource("产品编号", "ProductCode");
                    excelOperator.AddColumnDataSource("数量", "Amount");

                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();

                    //Excel单行导入
                    List<ImportVehicleOrderPlanSummary> summaries = errorImportVehicleOrderPlanSummaries;
                    excelOperator.LoadExcelRow(row => {
                        //接收Excel导入数据
                        var vehicleOrderPlanSummary = new ImportVehicleOrderPlanSummary {
                            ProductCode = row["ProductCode"]
                        };
                        var errorMsgs = new List<string>();
                        //产品编号检查
                        var fieldIndex = notNullableFields.IndexOf("PRODUCTCODE");
                        if(string.IsNullOrEmpty(vehicleOrderPlanSummary.ProductCode)) {
                            if(fieldIndex > -1)
                                errorMsgs.Add(ErrorStrings.ImportVehicleOrderPlanSummary_Validation1);
                        } else {
                            if(Encoding.Default.GetByteCount(vehicleOrderPlanSummary.ProductCode) > fieldLenght["PRODUCTCODE"])
                                errorMsgs.Add(ErrorStrings.ImportVehicleOrderPlanSummary_Validation2);
                        }
                        //订单年检查
                        fieldIndex = notNullableFields.IndexOf("YEAROFORDER");
                        if(string.IsNullOrEmpty(row["YearOfOrder"])) {
                            if(fieldIndex > -1)
                                errorMsgs.Add(ErrorStrings.ImportVehicleOrderPlanSummary_Validation3);
                        } else {
                            vehicleOrderPlanSummary.YearOfOrderStr = row["YearOfOrder"];
                            int yearOfOrder;
                            if(int.TryParse(row["YearOfOrder"], out yearOfOrder))
                                vehicleOrderPlanSummary.YearOfOrder = yearOfOrder;
                            else
                                errorMsgs.Add(ErrorStrings.ImportVehicleOrderPlanSummary_Validation4);
                        }
                        //订单月检查
                        fieldIndex = notNullableFields.IndexOf("MONTHOFORDER");
                        if(string.IsNullOrEmpty(row["MonthOfOrder"])) {
                            if(fieldIndex > -1)
                                errorMsgs.Add(ErrorStrings.ImportVehicleOrderPlanSummary_Validation5);
                        } else {
                            vehicleOrderPlanSummary.MonthOfOrderStr = row["MonthOfOrder"];
                            int monthOfOrder;
                            if(int.TryParse(row["MonthOfOrder"], out monthOfOrder))
                                vehicleOrderPlanSummary.MonthOfOrder = monthOfOrder;
                            else
                                errorMsgs.Add(ErrorStrings.ImportVehicleOrderPlanSummary_Validation6);
                        }
                        //数量
                        if(string.IsNullOrEmpty(row["Amount"])) {
                            errorMsgs.Add(ErrorStrings.ImportVehicleOrderPlanSummary_Validation7);
                        } else {
                            int amount;
                            if(int.TryParse(row["Amount"], out amount))
                                vehicleOrderPlanSummary.Amount = amount;
                            else
                                errorMsgs.Add(ErrorStrings.ImportVehicleOrderPlanSummary_Validation8);
                        }
                        if(errorMsgs.Count > 0) {
                            vehicleOrderPlanSummary.ErrorMsg = string.Join("; ", errorMsgs);
                            summaries.Add(vehicleOrderPlanSummary);
                        } else
                            importVehicleOrderPlanSummaries.Add(vehicleOrderPlanSummary);
                        return false;
                    });

                    //产品编号是否重复。
                    foreach(var importVehicleOrderPlanSummary in importVehicleOrderPlanSummaries.GroupBy(r => r.ProductCode.ToUpper()).Where(vChangeRec => vChangeRec.Count() > 1).SelectMany(vChangeRec => vChangeRec)) {
                        importVehicleOrderPlanSummary.ErrorMsg = string.Join("; ", string.Format(ErrorStrings.ImportVehicleOrderPlanSummary_Validation9, importVehicleOrderPlanSummary.ProductCode));
                        importVehicleOrderPlanSummaries.Remove(importVehicleOrderPlanSummary);
                        errorImportVehicleOrderPlanSummaries.Add(importVehicleOrderPlanSummary);
                    }
                    var products = new List<Product>();
                    Func<string[], bool> dealProduct = values => {
                        products.Add(new Product {
                            Id = int.Parse(values[0]),
                            Code = values[1],
                            Name = values[2]
                        });
                        return false;
                    };
                    var vehicleModelAffiProducts = new List<VehicleModelAffiProduct>();
                    Func<string[], bool> dealvehicleModelAffiProduct = values => {
                        vehicleModelAffiProducts.Add(new VehicleModelAffiProduct {
                            ProductCode = values[0],
                            ProductCategoryCode = values[1],
                            ProductCategoryName = values[2]
                        });
                        return false;
                    };
                    var vehicleSeries = new List<VehicleSery>();
                    Func<string[], bool> dealVehicleSery = values => {
                        vehicleSeries.Add(new VehicleSery {
                            Id = int.Parse(values[0]),
                            VehicleSeriesCode = values[1],
                            RegionCode = values[2],
                            ProductCode = values[3]
                        });
                        return false;
                    };
                    //获取产品编号
                    var productCodes = importVehicleOrderPlanSummaries.Select(v => v.ProductCode.ToUpper()).Distinct().ToArray();
                    db.QueryDataWithInOperator("select Id, Code, Name from Product", "Code", true, productCodes, dealProduct);
                    db.QueryDataWithInOperator("select ProductCode,ProductCategoryCode, ProductCategoryName from VehicleModelAffiProduct", "ProductCode ", true, productCodes, dealvehicleModelAffiProduct);
                    db.QueryDataWithInOperator("select Id, VehicleSeriesCode, RegionCode, ProductCode from VehicleSeries", "ProductCode", true, productCodes, dealVehicleSery);
                    for(var i = importVehicleOrderPlanSummaries.Count - 1; i >= 0; i--) {
                        var product = products.FirstOrDefault(v => String.Compare(v.Code, importVehicleOrderPlanSummaries[i].ProductCode, StringComparison.OrdinalIgnoreCase) == 0);
                        if(product == null) {
                            importVehicleOrderPlanSummaries[i].ErrorMsg = string.Format(ErrorStrings.ImportVehicleOrderPlanSummary_Validation10, importVehicleOrderPlanSummaries[i].ProductCode);
                            errorImportVehicleOrderPlanSummaries.Add(importVehicleOrderPlanSummaries[i]);
                            continue;
                        }
                        var vehicleModelAffiProduct = vehicleModelAffiProducts.FirstOrDefault(v => String.Compare(v.ProductCode, importVehicleOrderPlanSummaries[i].ProductCode, StringComparison.OrdinalIgnoreCase) == 0);
                        if(vehicleModelAffiProduct == null) {
                            importVehicleOrderPlanSummaries[i].ErrorMsg = string.Format(ErrorStrings.ImportVehicleOrderPlanSummary_Validation11, importVehicleOrderPlanSummaries[i].ProductCode);
                            errorImportVehicleOrderPlanSummaries.Add(importVehicleOrderPlanSummaries[i]);
                            continue;
                        }
                        var vehicleSery = vehicleSeries.FirstOrDefault(v => String.Compare(v.ProductCode, importVehicleOrderPlanSummaries[i].ProductCode, StringComparison.OrdinalIgnoreCase) == 0);
                        if(vehicleSery == null) {
                            importVehicleOrderPlanSummaries[i].ErrorMsg = string.Format(ErrorStrings.ImportVehicleOrderPlanSummary_Validation12, importVehicleOrderPlanSummaries[i].ProductCode);
                            errorImportVehicleOrderPlanSummaries.Add(importVehicleOrderPlanSummaries[i]);
                            continue;
                        }

                        importVehicleOrderPlanSummaries[i].BranchId = GlobalVar.DEFAULT_VEHICLEORDERPLANSUMMARY_BRANCHID;
                        importVehicleOrderPlanSummaries[i].BranchCode = GlobalVar.DEFAULT_VEHICLEORDERPLANSUMMARY_BRANCHCODE;
                        importVehicleOrderPlanSummaries[i].BranchName = GlobalVar.DEFAULT_VEHICLEORDERPLANSUMMARY_BRANCHNAME;
                        importVehicleOrderPlanSummaries[i].ProductId = product.Id;
                        importVehicleOrderPlanSummaries[i].ProductCode = product.Code;
                        importVehicleOrderPlanSummaries[i].ProductName = product.Name;
                        importVehicleOrderPlanSummaries[i].ProductCategoryCode = vehicleModelAffiProduct.ProductCategoryCode;
                        importVehicleOrderPlanSummaries[i].ProductCategoryName = vehicleModelAffiProduct.ProductCategoryName;
                        importVehicleOrderPlanSummaries[i].OrderType = (int)DcsVehicleOrderPlanSummaryOrderType.订单;
                        importVehicleOrderPlanSummaries[i].RegionCode = vehicleSery.RegionCode;
                        importVehicleOrderPlanSummaries[i].VehicleSeriesId = vehicleSery.Id;
                        importVehicleOrderPlanSummaries[i].VehicleSeriesCode = vehicleSery.VehicleSeriesCode;
                        importVehicleOrderPlanSummaries[i].StartTime = DateTime.Now;
                        importVehicleOrderPlanSummaries[i].EndTime = DateTime.Now;
                        importVehicleOrderPlanSummaries[i].UploadType = (int)DcsVehicleOrderPlanSummaryUploadType.新增;
                        importVehicleOrderPlanSummaries[i].VehicleOrderUploadStatus = (int)DcsVehicleOrderPlanSummaryVehicleOrderUploadStatus.未上传;
                    }
                }
                var rightImportVehicleOrderPlanSummaries = importVehicleOrderPlanSummaries.Except(errorImportVehicleOrderPlanSummaries).ToArray();
                if(errorImportVehicleOrderPlanSummaries.Any()) {
                    excelColumns.Add("校验信息");
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        List<ImportVehicleOrderPlanSummary> summaries = errorImportVehicleOrderPlanSummaries;
                        excelExport.ExportByRow(index => {
                            if(index == summaries.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var vehicleOrderPlanSummary = summaries[index - 1];
                            var values = new object[]{
                                        vehicleOrderPlanSummary.YearOfOrderStr,vehicleOrderPlanSummary.MonthOfOrderStr,vehicleOrderPlanSummary.ProductCode,vehicleOrderPlanSummary.Amount,vehicleOrderPlanSummary.ErrorMsg
                                    };
                            return values;
                        });
                    }
                }

                //导入正常结束，不存在需要写入数据库的合法数据
                if(!rightImportVehicleOrderPlanSummaries.Any())
                    return true;

                //写数据
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    //开启事务，新增更新配件在一个事务内
                    var ts = conn.BeginTransaction();
                    try {
                        //新增配件
                        if(rightImportVehicleOrderPlanSummaries.Any()) {
                            //获取新增数据的sql语句，Id为主键
                            var sqlInsert = db.GetInsertSql("VehicleOrderPlanSummary", "Id", new[] {
                                "BranchId", "BranchCode", "BranchName", "ProductId", "ProductCode", "ProductName", "ProductCategoryCode", "ProductCategoryName", "YearOfOrder", "MonthOfOrder", "OrderType","CreatorId","CreatorName","CreateTime"
                            });
                            var userInfo = Utils.GetCurrentUserInfo();
                            foreach(var vehicleOrderPlanSummary in rightImportVehicleOrderPlanSummaries) {
                                for(var i = 0; i < vehicleOrderPlanSummary.Amount; i++) {
                                    var command = db.CreateDbCommand(sqlInsert, conn, ts);
                                    command.Parameters.Add(db.CreateDbParameter("BranchId", vehicleOrderPlanSummary.BranchId));
                                    command.Parameters.Add(db.CreateDbParameter("BranchCode", vehicleOrderPlanSummary.BranchCode));
                                    command.Parameters.Add(db.CreateDbParameter("BranchName", vehicleOrderPlanSummary.BranchName));
                                    command.Parameters.Add(db.CreateDbParameter("ProductId", vehicleOrderPlanSummary.ProductId));
                                    command.Parameters.Add(db.CreateDbParameter("ProductCode", vehicleOrderPlanSummary.ProductCode));
                                    command.Parameters.Add(db.CreateDbParameter("ProductName", vehicleOrderPlanSummary.ProductName));
                                    command.Parameters.Add(db.CreateDbParameter("ProductCategoryCode", vehicleOrderPlanSummary.ProductCategoryCode));
                                    command.Parameters.Add(db.CreateDbParameter("ProductCategoryName", vehicleOrderPlanSummary.ProductCategoryName));
                                    command.Parameters.Add(db.CreateDbParameter("YearOfOrder", vehicleOrderPlanSummary.YearOfOrder));
                                    command.Parameters.Add(db.CreateDbParameter("MonthOfOrder", vehicleOrderPlanSummary.MonthOfOrder));
                                    command.Parameters.Add(db.CreateDbParameter("OrderType", vehicleOrderPlanSummary.OrderType));
                                    command.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                    command.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                    command.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                    command.ExecuteNonQuery();
                                }
                            }
                        }
                        //无异常提交
                        ts.Commit();
                    } catch(Exception) {
                        //报错回滚
                        ts.Rollback();
                        throw;
                    } finally {
                        if(conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                return false;
            }
        }

        public bool ImportVehicleOrderPlanSummaryDel(string fileName, out string errorDataFileName, out string errorMessage) {
            //初始化返回数据
            errorMessage = "";
            errorDataFileName = "";

            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception("身份认证失效，请登录系统后再次尝试");

                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("VehicleOrderPlanSummary", out notNullableFields, out fieldLenght);

                var errorImportVehicleOrderPlanSummaries = new List<ImportVehicleOrderPlanSummary>();
                var importVehicleOrderPlanSummaries = new List<ImportVehicleOrderPlanSummary>();
                List<object> excelColumns;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    //指定Excel列名与字段名关系
                    excelOperator.AddColumnDataSource("订单年", "YearOfOrder");
                    excelOperator.AddColumnDataSource("订单月", "MonthOfOrder");
                    excelOperator.AddColumnDataSource("产品编号", "ProductCode");
                    excelOperator.AddColumnDataSource("JUMPSON", "ProductionPlanSON");

                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();

                    //Excel单行导入
                    List<ImportVehicleOrderPlanSummary> summaries = errorImportVehicleOrderPlanSummaries;
                    excelOperator.LoadExcelRow(row => {
                        //接收Excel导入数据
                        var vehicleOrderPlanSummary = new ImportVehicleOrderPlanSummary {
                            ProductCode = row["ProductCode"],
                            ProductionPlanSON = row["ProductionPlanSON"]
                        };
                        var errorMsgs = new List<string>();
                        //产品编号检查
                        var fieldIndex = notNullableFields.IndexOf("PRODUCTCODE");
                        if(string.IsNullOrEmpty(vehicleOrderPlanSummary.ProductCode)) {
                            if(fieldIndex > -1)
                                errorMsgs.Add(ErrorStrings.ImportVehicleOrderPlanSummary_Validation1);
                        } else {
                            if(Encoding.Default.GetByteCount(vehicleOrderPlanSummary.ProductCode) > fieldLenght["PRODUCTCODE"])
                                errorMsgs.Add(ErrorStrings.ImportVehicleOrderPlanSummary_Validation2);
                        }
                        //生产SON检查
                        fieldIndex = notNullableFields.IndexOf("PRODUCTIONPLANSON");
                        if(string.IsNullOrEmpty(vehicleOrderPlanSummary.ProductionPlanSON)) {
                            if(fieldIndex > -1)
                                errorMsgs.Add(ErrorStrings.ImportVehicleOrderPlanSummary_Validation13);
                        } else {
                            if(Encoding.Default.GetByteCount(vehicleOrderPlanSummary.ProductionPlanSON) > fieldLenght["PRODUCTIONPLANSON"])
                                errorMsgs.Add(ErrorStrings.ImportVehicleOrderPlanSummary_Validation14);
                        }
                        //订单年检查
                        fieldIndex = notNullableFields.IndexOf("YEAROFORDER");
                        if(string.IsNullOrEmpty(row["YearOfOrder"])) {
                            if(fieldIndex > -1)
                                errorMsgs.Add(ErrorStrings.ImportVehicleOrderPlanSummary_Validation3);
                        } else {
                            vehicleOrderPlanSummary.YearOfOrderStr = row["YearOfOrder"];
                            int yearOfOrder;
                            if(int.TryParse(row["YearOfOrder"], out yearOfOrder))
                                vehicleOrderPlanSummary.YearOfOrder = yearOfOrder;
                            else
                                errorMsgs.Add(ErrorStrings.ImportVehicleOrderPlanSummary_Validation4);
                        }
                        //订单月检查
                        fieldIndex = notNullableFields.IndexOf("MONTHOFORDER");
                        if(string.IsNullOrEmpty(row["MonthOfOrder"])) {
                            if(fieldIndex > -1)
                                errorMsgs.Add(ErrorStrings.ImportVehicleOrderPlanSummary_Validation5);
                        } else {
                            vehicleOrderPlanSummary.MonthOfOrderStr = row["MonthOfOrder"];
                            int monthOfOrder;
                            if(int.TryParse(row["MonthOfOrder"], out monthOfOrder))
                                vehicleOrderPlanSummary.MonthOfOrder = monthOfOrder;
                            else
                                errorMsgs.Add(ErrorStrings.ImportVehicleOrderPlanSummary_Validation6);
                        }

                        if(errorMsgs.Count > 0) {
                            vehicleOrderPlanSummary.ErrorMsg = string.Join("; ", errorMsgs);
                            summaries.Add(vehicleOrderPlanSummary);
                        } else
                            importVehicleOrderPlanSummaries.Add(vehicleOrderPlanSummary);
                        return false;
                    });

                    var products = new List<Product>();
                    Func<string[], bool> dealProduct = values => {
                        products.Add(new Product {
                            Id = int.Parse(values[0]),
                            Code = values[1],
                            Name = values[2]
                        });
                        return false;
                    };
                    var vehicleModelAffiProducts = new List<VehicleModelAffiProduct>();
                    Func<string[], bool> dealvehicleModelAffiProduct = values => {
                        vehicleModelAffiProducts.Add(new VehicleModelAffiProduct {
                            ProductCode = values[0],
                            ProductCategoryCode = values[1],
                            ProductCategoryName = values[2]
                        });
                        return false;
                    };
                    var vehicleSeries = new List<VehicleSery>();
                    Func<string[], bool> dealVehicleSery = values => {
                        vehicleSeries.Add(new VehicleSery {
                            Id = int.Parse(values[0]),
                            VehicleSeriesCode = values[1],
                            RegionCode = values[2],
                            ProductCode = values[3]
                        });
                        return false;
                    };
                    var productionPlans = new List<ProductionPlan>();
                    Func<string[], bool> dealProductionPlan = values => {
                        productionPlans.Add(new ProductionPlan {
                            ProductCode = values[0],
                            ProductionPlanSON = values[1]
                        });
                        return false;
                    };
                    //获取产品编号
                    var productCodes = importVehicleOrderPlanSummaries.Select(v => v.ProductCode.ToUpper()).Distinct().ToArray();
                    var productionPlanSoNs = importVehicleOrderPlanSummaries.Select(v => v.ProductionPlanSON.ToUpper()).Distinct().ToArray();
                    db.QueryDataWithInOperator("select Id, Code, Name from Product", "Code", true, productCodes, dealProduct);
                    db.QueryDataWithInOperator("select ProductCode,ProductCategoryCode, ProductCategoryName from VehicleModelAffiProduct", "ProductCode ", true, productCodes, dealvehicleModelAffiProduct);
                    db.QueryDataWithInOperator("select Id, VehicleSeriesCode, RegionCode, ProductCode from VehicleSeries", "ProductCode", true, productCodes, dealVehicleSery);
                    db.QueryDataWithInOperator(string.Format("select Id, ProductCode, ProductionPlanSON  from ProductionPlan where Status = {0}", (int)DcsProductionPlanStatus.接受), "ProductCode", true, productionPlanSoNs, dealProductionPlan);
                    for(var i = importVehicleOrderPlanSummaries.Count - 1; i >= 0; i--) {
                        var product = products.FirstOrDefault(v => String.Compare(v.Code, importVehicleOrderPlanSummaries[i].ProductCode, StringComparison.OrdinalIgnoreCase) == 0);
                        if(product == null) {
                            importVehicleOrderPlanSummaries[i].ErrorMsg = string.Format(ErrorStrings.ImportVehicleOrderPlanSummary_Validation10, importVehicleOrderPlanSummaries[i].ProductCode);
                            errorImportVehicleOrderPlanSummaries.Add(importVehicleOrderPlanSummaries[i]);
                            continue;
                        }
                        var vehicleModelAffiProduct = vehicleModelAffiProducts.FirstOrDefault(v => String.Compare(v.ProductCode, importVehicleOrderPlanSummaries[i].ProductCode, StringComparison.OrdinalIgnoreCase) == 0);
                        if(vehicleModelAffiProduct == null) {
                            importVehicleOrderPlanSummaries[i].ErrorMsg = string.Format(ErrorStrings.ImportVehicleOrderPlanSummary_Validation11, importVehicleOrderPlanSummaries[i].ProductCode);
                            errorImportVehicleOrderPlanSummaries.Add(importVehicleOrderPlanSummaries[i]);
                            continue;
                        }
                        var vehicleSery = vehicleSeries.FirstOrDefault(v => String.Compare(v.ProductCode, importVehicleOrderPlanSummaries[i].ProductCode, StringComparison.OrdinalIgnoreCase) == 0);
                        if(vehicleSery == null) {
                            importVehicleOrderPlanSummaries[i].ErrorMsg = string.Format(ErrorStrings.ImportVehicleOrderPlanSummary_Validation12, importVehicleOrderPlanSummaries[i].ProductCode);
                            errorImportVehicleOrderPlanSummaries.Add(importVehicleOrderPlanSummaries[i]);
                            continue;
                        }
                        var productionPlan = productionPlans.FirstOrDefault(r => r.ProductionPlanSON == importVehicleOrderPlanSummaries[i].ProductionPlanSON);
                        if(productionPlan == null) {
                            importVehicleOrderPlanSummaries[i].ErrorMsg = string.Format(ErrorStrings.ImportVehicleOrderPlanSummary_Validation15, importVehicleOrderPlanSummaries[i].ProductCode);
                            errorImportVehicleOrderPlanSummaries.Add(importVehicleOrderPlanSummaries[i]);
                            continue;
                        }

                        importVehicleOrderPlanSummaries[i].BranchId = GlobalVar.DEFAULT_VEHICLEORDERPLANSUMMARY_BRANCHID;
                        importVehicleOrderPlanSummaries[i].BranchCode = GlobalVar.DEFAULT_VEHICLEORDERPLANSUMMARY_BRANCHCODE;
                        importVehicleOrderPlanSummaries[i].BranchName = GlobalVar.DEFAULT_VEHICLEORDERPLANSUMMARY_BRANCHNAME;
                        importVehicleOrderPlanSummaries[i].ProductId = product.Id;
                        importVehicleOrderPlanSummaries[i].ProductCode = product.Code;
                        importVehicleOrderPlanSummaries[i].ProductName = product.Name;
                        importVehicleOrderPlanSummaries[i].ProductCategoryCode = vehicleModelAffiProduct.ProductCategoryCode;
                        importVehicleOrderPlanSummaries[i].ProductCategoryName = vehicleModelAffiProduct.ProductCategoryName;
                        importVehicleOrderPlanSummaries[i].OrderType = (int)DcsVehicleOrderPlanSummaryOrderType.订单;
                        importVehicleOrderPlanSummaries[i].RegionCode = vehicleSery.RegionCode;
                        importVehicleOrderPlanSummaries[i].VehicleSeriesId = vehicleSery.Id;
                        importVehicleOrderPlanSummaries[i].VehicleSeriesCode = vehicleSery.VehicleSeriesCode;
                        importVehicleOrderPlanSummaries[i].StartTime = DateTime.Now;
                        importVehicleOrderPlanSummaries[i].EndTime = DateTime.Now;
                        importVehicleOrderPlanSummaries[i].UploadType = (int)DcsVehicleOrderPlanSummaryUploadType.删除;
                        importVehicleOrderPlanSummaries[i].VehicleOrderUploadStatus = (int)DcsVehicleOrderPlanSummaryVehicleOrderUploadStatus.未上传;

                    }
                }
                var rightImportVehicleOrderPlanSummaries = importVehicleOrderPlanSummaries.Except(errorImportVehicleOrderPlanSummaries).ToArray();
                if(errorImportVehicleOrderPlanSummaries.Any()) {
                    excelColumns.Add("校验信息");
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        List<ImportVehicleOrderPlanSummary> summaries = errorImportVehicleOrderPlanSummaries;
                        excelExport.ExportByRow(index => {
                            if(index == summaries.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var vehicleOrderPlanSummary = summaries[index - 1];
                            var values = new object[]{
                                        vehicleOrderPlanSummary.YearOfOrderStr,vehicleOrderPlanSummary.MonthOfOrderStr,vehicleOrderPlanSummary.ProductCode,vehicleOrderPlanSummary.ProductionPlanSON,vehicleOrderPlanSummary.ErrorMsg
                            };
                            return values;
                        });
                    }
                }

                //导入正常结束，不存在需要写入数据库的合法数据
                if(!rightImportVehicleOrderPlanSummaries.Any())
                    return true;

                //写数据
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    //开启事务，新增更新配件在一个事务内
                    var ts = conn.BeginTransaction();
                    try {
                        //新增整车订货计划汇总单
                        if(rightImportVehicleOrderPlanSummaries.Any()) {
                            //获取新增数据的sql语句，Id为主键
                            var sqlInsert = db.GetInsertSql("VehicleOrderPlanSummary", "Id", new[] {
                                "BranchId", "BranchCode", "BranchName", "ProductId", "ProductCode", "ProductName", "ProductCategoryCode", "ProductCategoryName", "YearOfOrder", "MonthOfOrder", "OrderType","ProductionPlanSON","CreatorId","CreatorName","CreateTime"
                            });
                            var userInfo = Utils.GetCurrentUserInfo();
                            foreach(var vehicleOrderPlanSummary in rightImportVehicleOrderPlanSummaries) {
                                var command = db.CreateDbCommand(sqlInsert, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("BranchId", vehicleOrderPlanSummary.BranchId));
                                command.Parameters.Add(db.CreateDbParameter("BranchCode", vehicleOrderPlanSummary.BranchCode));
                                command.Parameters.Add(db.CreateDbParameter("BranchName", vehicleOrderPlanSummary.BranchName));
                                command.Parameters.Add(db.CreateDbParameter("ProductId", vehicleOrderPlanSummary.ProductId));
                                command.Parameters.Add(db.CreateDbParameter("ProductCode", vehicleOrderPlanSummary.ProductCode));
                                command.Parameters.Add(db.CreateDbParameter("ProductName", vehicleOrderPlanSummary.ProductName));
                                command.Parameters.Add(db.CreateDbParameter("ProductCategoryCode", vehicleOrderPlanSummary.ProductCategoryCode));
                                command.Parameters.Add(db.CreateDbParameter("ProductCategoryName", vehicleOrderPlanSummary.ProductCategoryName));
                                command.Parameters.Add(db.CreateDbParameter("YearOfOrder", vehicleOrderPlanSummary.YearOfOrder));
                                command.Parameters.Add(db.CreateDbParameter("MonthOfOrder", vehicleOrderPlanSummary.MonthOfOrder));
                                command.Parameters.Add(db.CreateDbParameter("OrderType", vehicleOrderPlanSummary.OrderType));
                                command.Parameters.Add(db.CreateDbParameter("ProductionPlanSON", vehicleOrderPlanSummary.ProductionPlanSON));
                                command.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                command.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                command.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                command.ExecuteNonQuery();
                            }
                        }
                        //无异常提交
                        ts.Commit();
                    } catch(Exception) {
                        //报错回滚
                        ts.Rollback();
                        throw;
                    } finally {
                        if(conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                }
                return true;

            } catch(Exception ex) {
                errorMessage = ex.Message;
                return false;
            }
        }
    }
}
