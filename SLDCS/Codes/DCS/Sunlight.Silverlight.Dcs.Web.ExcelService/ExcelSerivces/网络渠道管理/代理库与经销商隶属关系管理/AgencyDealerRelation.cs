﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        /// <summary>
        /// 导出代理库与经销商隶属关系
        /// </summary>
        /// <param name="agencyCode">代理库编号</param>
        /// <param name="agencyName">代理库名称</param>
        /// <param name="dealerCode">服务站编号</param>
        /// <param name="dealerName">服务站名称</param>
        /// <param name="status">状态</param>
        /// <param name="createTimeBegin">创建开始时间</param>
        /// <param name="createTimeEnd">创建结束时间</param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public bool ExportAgencyDealerRelation(string agencyCode, string agencyName, string dealerCode, string dealerName, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName) {
            fileName = GetExportFilePath("中心库与服务站隶属关系_" + ".xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated) {
                    throw new Exception(ErrorStrings.Export_Validation_Dealer_Validation52);
                }
                var db = DbHelper.GetDbHelp(ConnectionString);
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();

                    sql.Append(@"select b.Name as BranchName,
                                        a.AgencyCode,
                                        a.AgencyName,
                                        c.Name as PartsSalesCategoryName,
                                        a.DealerCode, 
                                        a.DealerName, 
                                        a.Remark,
                                        cast(case a.Status
                                        when 1 then '有效'
                                        when 99 then '作废'
                                        end as varchar2(100)) as  Status,
                                        a.CreatorName, 
                                        a.CreateTime, 
                                        a.ModifierName, 
                                        a.ModifyTime,
                                        a.AbandonerName,
                                        a.AbandonTime
                                  from AgencyDealerRelation a
                                  inner join Branch b
                                  on a.BranchId=b.id
                                  inner join PartsSalesCategory c
                                  on a.PartsSalesOrderTypeId=c.id
                                  where 1 = 1");
                    var dbParameters = new List<DbParameter>();
                    if(!string.IsNullOrEmpty(agencyCode)) {
                        sql.Append(@" and a.agencyCode like {0}agencyCode ");
                        dbParameters.Add(db.CreateDbParameter("agencyCode", '%' + agencyCode + '%'));
                    }
                    if(!string.IsNullOrEmpty(agencyName)) {
                        sql.Append(@" and a.agencyName like {0}agencyName ");
                        dbParameters.Add(db.CreateDbParameter("agencyName", '%' + agencyName + '%'));
                    }
                    if(!string.IsNullOrEmpty(dealerCode)) {
                        sql.Append(@" and a.dealerCode like {0}dealerCode ");
                        dbParameters.Add(db.CreateDbParameter("dealerCode", '%' + dealerCode + '%'));
                    }
                    if(!string.IsNullOrEmpty(dealerName)) {
                        sql.Append(@" and a.dealerName like {0}dealerName ");
                        dbParameters.Add(db.CreateDbParameter("dealerName", '%' + dealerName + '%'));
                    }
                    if(status.HasValue) {
                        sql.Append(@" and a.status ={0}status");
                        dbParameters.Add(db.CreateDbParameter("status", status.Value));
                    }
                    if(createTimeBegin.HasValue) {
                        sql.Append(@" and a.createTime >=To_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                        var tempValue = createTimeBegin.Value;
                        var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                        dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                    }
                    if(createTimeEnd.HasValue) {
                        sql.Append(" and a.createTime<=To_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                        var tempValue = createTimeEnd.Value;
                        var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                        dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                    }

                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    ErrorStrings.Export_Title_Branch_BranchName, ErrorStrings.Export_Title_Agency_Code, ErrorStrings.Export_Title_Agency_Name, ErrorStrings.Export_Title_Partssalescategory_Name, ErrorStrings.Export_Title_Dealer_Code, ErrorStrings.Export_Title_Dealer_Name, ErrorStrings.Export_Title_PartsBranch_Remark, ErrorStrings.Export_Title_AccountPeriod_Status, ErrorStrings.Export_Title_AccountPeriod_CreatorName, ErrorStrings.Export_Title_AccountPeriod_CreateTime, ErrorStrings.Export_Title_AccountPeriod_ModifierName, ErrorStrings.Export_Title_AccountPeriod_ModifyTime, ErrorStrings.Export_Title_PartsBranch_AbandonerName, ErrorStrings.Export_Title_PartsBranch_AbandonerTime
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }

                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }

        /// <summary>
        /// 导入代理库与经销商隶属关系
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="excelImportNum"></param>
        /// <param name="errorData"></param>
        /// <param name="errorDataFileName"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public bool ImportAgencyDealerRelation(string fileName, out int excelImportNum, out List<AgencyDealerRelationExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<AgencyDealerRelationExtend>();
            var allList = new List<AgencyDealerRelationExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("AgencyDealerRelation", out notNullableFields, out fieldLenght);

                List<string> notNullableFieldsBranch;
                Dictionary<string, int> fieldLenghtBranch;
                db.GetTableSchema("Branch", out notNullableFieldsBranch, out fieldLenghtBranch);

                List<string> notNullableFieldsPartsSalesCategory;
                Dictionary<string, int> fieldLenghtPartsSalesCategory;
                db.GetTableSchema("PartsSalesCategory", out notNullableFieldsPartsSalesCategory, out fieldLenghtPartsSalesCategory);

                List<object> excelColumns;
                List<AgencyDealerRelationExtend> rightList;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    #region 指定文件中的列对应的名称 例如
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Branch_BranchName, "BranchName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Agency_Code, "AgencyCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Agency_Name, "AgencyName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Partssalescategory_Name, "PartsSalesOrderTypeName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_Code, "DealerCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_Name, "DealerName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Remark, "Remark");
                    #endregion
                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;
                    #region 获取对应枚举
                    var keyValuePairs = new[] { new KeyValuePair<string, string>("BaseDataStatus", "BaseData_Status") };
                    tempExcelOperator.ImportHelper.LoadEnumFromDb(null,null, keyValuePairs);
                    #endregion
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var tempImportObj = new AgencyDealerRelationExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        #region 根据导入的字段给对应的实体属性赋值
                        tempImportObj.BranchNameStr = newRow["BranchName"];
                        tempImportObj.AgencyCodeStr = newRow["AgencyCode"];
                        tempImportObj.AgencyNameStr = newRow["AgencyName"];
                        tempImportObj.PartsSalesOrderTypeNameStr = newRow["PartsSalesOrderTypeName"];
                        tempImportObj.DealerCodeStr = newRow["DealerCode"];
                        tempImportObj.DealerNameStr = newRow["DealerName"];
                        tempImportObj.RemarkStr = newRow["Remark"];
                        #endregion

                        var tempErrorMessage = new List<string>();
                        #region 导入的数据基本检查
                        //分公司名称检查
                        var fieldIndex = notNullableFieldsBranch.IndexOf("Name".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.BranchNameStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Branch_BranchNameIsNull);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.BranchNameStr) > fieldLenghtBranch["Name".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Branch_BranchNameIsLong);
                        }
                        //代理库编号检查
                        fieldIndex = notNullableFields.IndexOf("AgencyCode".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.AgencyCodeStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add("代理库编号不能为空");
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.AgencyCodeStr) > fieldLenght["AgencyCode".ToUpper()])
                                tempErrorMessage.Add("代理库编号过长");
                        }
                        //代理库名称检查
                        fieldIndex = notNullableFields.IndexOf("AgencyName".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.AgencyNameStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add("代理库名称不能为空");
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.AgencyNameStr) > fieldLenght["AgencyName".ToUpper()])
                                tempErrorMessage.Add("代理库名称过长");
                        }
                        //品牌检查
                        fieldIndex = notNullableFieldsPartsSalesCategory.IndexOf("Name".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.PartsSalesOrderTypeNameStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_MarketingDepartment_PartsSalesCategoryIsNull);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.PartsSalesOrderTypeNameStr) > fieldLenghtPartsSalesCategory["Name".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_MarketingDepartment_PartsSalesCategoryIsLong);
                        }
                        //服务站编号检查
                        fieldIndex = notNullableFields.IndexOf("DealerCode".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.DealerCodeStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add("服务站编号不能为空");
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.DealerCodeStr) > fieldLenght["DealerCode".ToUpper()])
                                tempErrorMessage.Add("服务站编号过长");
                        }
                        //服务站名称检查
                        fieldIndex = notNullableFields.IndexOf("DealerName".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.DealerNameStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add("服务站名称不能为空");
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.DealerNameStr) > fieldLenght["DealerName".ToUpper()])
                                tempErrorMessage.Add("服务站名称过长");
                        }
                        //备注检查
                        fieldIndex = notNullableFields.IndexOf("Remark".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.RemarkStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchasePricingDetail_Validation15);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.RemarkStr) > fieldLenght["Remark".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchasePricingDetail_Validation16);
                        }
                        #endregion
                        if(tempErrorMessage.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });
                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    #region 剩下的数据进行业务检查
                    //6、文件中唯一性校验：分公司名称、代理库编号、代理库名称、品牌、服务站编号、服务站名称组合唯一存在于文件中，否则提示：数据重复
                    var groups = tempRightList.GroupBy(r => new {
                        r.BranchNameStr,
                        r.AgencyCodeStr,
                        r.AgencyNameStr,
                        r.PartsSalesOrderTypeNameStr,
                        r.DealerCodeStr,
                        r.DealerNameStr
                    }).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        groupItem.ErrorMsg = ErrorStrings.ImpPartsSupplierRelation_Validation31;
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    //1、合法性校验：分公司名称存在于营销分公司，否则提示：分公司不存在
                    var branchNamesNeedCheck = tempRightList.Select(r => r.BranchNameStr).Distinct().ToArray();
                    var dbCompanys = new List<BranchExtend>();
                    Func<string[], bool> getDbCompanys = value => {
                        var dbObj = new BranchExtend {
                            Id = Convert.ToInt32(value[0]),
                            Name = value[1]
                        };
                        dbCompanys.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select Id,Name from Branch where status=1 ", "Name", false, branchNamesNeedCheck, getDbCompanys);
                    foreach(var tempRight in tempRightList) {
                        var repairItem = dbCompanys.FirstOrDefault(v => v.Name == tempRight.BranchNameStr);
                        if(repairItem == null) {
                            tempRight.ErrorMsg = "分公司不存在";
                            continue;
                        }
                        tempRight.BranchId = repairItem.Id;
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    //2、合法性校验：代理库编号、代理库名称组合存在于代理库中，否则提示：代理库不存在
                    var agencyCodeStrsNeedCheck = tempRightList.Select(r => r.AgencyCodeStr).Distinct().ToArray();
                    var dbAgencys = new List<AgencyExtend>();
                    Func<string[], bool> getDbAgencys = value => {
                        var dbObj = new AgencyExtend {
                            Id = Convert.ToInt32(value[0]),
                            Code = value[1],
                            Name = value[2]
                        };
                        dbAgencys.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select Id,Code,Name from Agency where status=1 ", "Code", false, agencyCodeStrsNeedCheck, getDbAgencys);
                    foreach(var tempRight in tempRightList) {
                        var repairItem = dbAgencys.FirstOrDefault(v => v.Name == tempRight.AgencyNameStr && v.Code == tempRight.AgencyCodeStr);
                        if(repairItem == null) {
                            tempRight.ErrorMsg = "代理库不存在";
                            continue;
                        }
                        tempRight.AgencyId = repairItem.Id;
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    //3、合法性校验：品牌存在于配件销售类型，否则提示：品牌不存在
                    var partsSalesOrderTypeNameStrsNeedCheck = tempRightList.Select(r => r.PartsSalesOrderTypeNameStr).Distinct().ToArray();
                    var dbPartsSalesCategorys = new List<PartsSalesCategoryExtend>();
                    Func<string[], bool> getDbPartsSalesCategorys = value => {
                        var dbObj = new PartsSalesCategoryExtend {
                            Id = Convert.ToInt32(value[0]),
                            Name = value[1]
                        };
                        dbPartsSalesCategorys.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select Id,Name from PartsSalesCategory where status=1 ", "Name", false, partsSalesOrderTypeNameStrsNeedCheck, getDbPartsSalesCategorys);
                    foreach(var tempRight in tempRightList) {
                        var repairItem = dbPartsSalesCategorys.FirstOrDefault(v => v.Name == tempRight.PartsSalesOrderTypeNameStr);
                        if(repairItem == null) {
                            tempRight.ErrorMsg = ErrorStrings.ImpVehicleDLRStartSecurityDeposit_Validation8;
                            continue;
                        }
                        tempRight.PartsSalesOrderTypeId = repairItem.Id;
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    //4、合法性校验：经销商编号、经销商名称组合存在于经销商中，否则提示：服务站不存在
                    var dealerCodeStrsNeedCheck = tempRightList.Select(r => r.DealerCodeStr).Distinct().ToArray();
                    var dbDealers = new List<DealerExtend>();
                    Func<string[], bool> getDbDealers = value => {
                        var dbObj = new DealerExtend {
                            Id = Convert.ToInt32(value[0]),
                            Code = value[1],
                            Name = value[2]
                        };
                        dbDealers.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select Id,Code,Name from Dealer where status=1 ", "Code", false, dealerCodeStrsNeedCheck, getDbDealers);
                    foreach(var tempRight in tempRightList) {
                        var repairItem = dbDealers.FirstOrDefault(v => v.Name == tempRight.DealerNameStr && v.Code == tempRight.DealerCodeStr);
                        if(repairItem == null) {
                            tempRight.ErrorMsg = "服务站不存在";
                            continue;
                        }
                        tempRight.DealerId = repairItem.Id;
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();


                    //5、唯一性校验：营销分公司Id、代理库Id、配件销售类型Id、经销商Id组合唯一，否则提示：数据已存在
                    var branchIdsNeedCheck = tempRightList.Select(r => r.BranchId.ToString(CultureInfo.InvariantCulture)).Distinct().ToArray();
                    var dbAgencyDealerRelations = new List<AgencyDealerRelationExtend>();
                    Func<string[], bool> getDbAgencyDealerRelations = value => {
                        var dbObj = new AgencyDealerRelationExtend {
                            BranchId = Convert.ToInt32(value[0]),
                            AgencyId = Convert.ToInt32(value[1]),
                            PartsSalesOrderTypeId = Convert.ToInt32(value[2]),
                            DealerId = Convert.ToInt32(value[3])
                        };
                        dbAgencyDealerRelations.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select BranchId,AgencyId,PartsSalesOrderTypeId,DealerId from AgencyDealerRelation where status=1 ", "BranchId", false, branchIdsNeedCheck, getDbAgencyDealerRelations);
                    foreach(var tempRight in tempRightList) {
                        var usedPartsDistanceInfor = dbAgencyDealerRelations.FirstOrDefault(v => v.BranchId == tempRight.BranchId && v.AgencyId == tempRight.AgencyId && v.PartsSalesOrderTypeId == tempRight.PartsSalesOrderTypeId && v.DealerId == tempRight.DealerId);
                        if(usedPartsDistanceInfor != null) {
                            tempRight.ErrorMsg = "代理库与经销商隶属关系已经存在";
                        }
                    }
                    #endregion
                    //获取所有不合格数据
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    //获取所有合格数据
                    rightList = allList.Except(errorList).ToList();
                    #region 将合格数据的值填上
                    var userInfo = Utils.GetCurrentUserInfo();
                    foreach(var rightItem in rightList) {
                        rightItem.AgencyCode = rightItem.AgencyCodeStr;
                        rightItem.AgencyName = rightItem.AgencyNameStr;
                        rightItem.DealerCode = rightItem.DealerCodeStr;
                        rightItem.DealerName = rightItem.DealerNameStr;
                        rightItem.Remark = rightItem.RemarkStr;
                        rightItem.Status = tempExcelOperator.ImportHelper.GetEnumValue("BaseDataStatus", ErrorStrings.Export_Validation_PartsBranch_Used) ?? 0;
                        rightItem.CreatorId = userInfo.Id;
                        rightItem.CreatorName = userInfo.Name;
                        rightItem.CreateTime = DateTime.Now;
                    }
                    #endregion
                }
                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempObj = list[index - 1];
                            var values = new object[] {
                                #region 设置错误信息导出的列的值
                                tempObj.BranchNameStr,tempObj.AgencyCodeStr,
                                tempObj.AgencyNameStr,tempObj.PartsSalesOrderTypeNameStr,
                                tempObj.DealerCodeStr,tempObj.DealerNameStr,
                                tempObj.RemarkStr,tempObj.ErrorMsg
                                #endregion                     
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }
                //导入所有合格数据
                if(!rightList.Any())
                    return true;
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    //开启事务，新增更新配件在一个事务内
                    var ts = conn.BeginTransaction();
                    try {
                        //新增配件
                        if(rightList.Any()) {
                            #region 获取对应的插入的SQL 通过db.GetInsertSql方法
                            //获取新增数据的sql语句，Id为主键
                            var sqlInsert = db.GetInsertSql("AgencyDealerRelation", "Id", new[] {
                                "BranchId", "AgencyId", "AgencyCode", "AgencyName", "PartsSalesOrderTypeId","DealerId","DealerCode","DealerName","Remark","Status", "CreatorId", "CreatorName", "CreateTime"
                            });
                            #endregion
                            //往数据库增加市场部信息
                            foreach(var item in rightList) {
                                #region 添加Sql的参数
                                var command = db.CreateDbCommand(sqlInsert, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("BranchId", item.BranchId));
                                command.Parameters.Add(db.CreateDbParameter("AgencyId", item.AgencyId));
                                command.Parameters.Add(db.CreateDbParameter("AgencyCode", item.AgencyCode));
                                command.Parameters.Add(db.CreateDbParameter("AgencyName", item.AgencyName));
                                command.Parameters.Add(db.CreateDbParameter("PartsSalesOrderTypeId", item.PartsSalesOrderTypeId));
                                command.Parameters.Add(db.CreateDbParameter("DealerId", item.DealerId));
                                command.Parameters.Add(db.CreateDbParameter("DealerCode", item.DealerCode));
                                command.Parameters.Add(db.CreateDbParameter("DealerName", item.DealerName));
                                command.Parameters.Add(db.CreateDbParameter("Remark", item.Remark));
                                command.Parameters.Add(db.CreateDbParameter("Status", item.Status));
                                command.Parameters.Add(db.CreateDbParameter("CreatorId", item.CreatorId));
                                command.Parameters.Add(db.CreateDbParameter("CreatorName", item.CreatorName));
                                command.Parameters.Add(db.CreateDbParameter("CreateTime", item.CreateTime));
                                command.ExecuteNonQuery();
                                #endregion
                            }
                        }
                        //无异常提交
                        ts.Commit();
                    } catch(Exception) {
                        //报错回滚
                        ts.Rollback();
                        throw;
                    } finally {
                        if(conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
            }
        }
    }
}
