﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        //导入认证类型
        public bool ImportAuthenticationType(string fileName, out int excelImportNum, out List<AuthenticationTypeExtend> rightData, out List<AuthenticationTypeExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<AuthenticationTypeExtend>();
            var rightList = new List<AuthenticationTypeExtend>();
            var allList = new List<AuthenticationTypeExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("AuthenticationType", out notNullableFields, out fieldLenght);

                List<object> excelColumns;
                var userInfo = Utils.GetCurrentUserInfo();

                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    #region 指定文件中的列对应的名称 例如 excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode, "PartCode");
                    excelOperator.AddColumnDataSource("培训认证类型编码", "AuthenticationTypeCode");
                    excelOperator.AddColumnDataSource("培训认证类型名称", "AuthenticationTypeName");
                    excelOperator.AddColumnDataSource("有效期", "ValDate");
                    #endregion

                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;

                    #region 获取对应枚举
                    var keyValuePairs = new[] {
                            new KeyValuePair<string, string>("ValiDate","ValDate")
                        };
                    tempExcelOperator.ImportHelper.LoadEnumFromDb(null, null, keyValuePairs);
                    #endregion

                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var tempImportObj = new AuthenticationTypeExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        #region 根据导入的字段给对应的实体属性赋值
                        tempImportObj.AuthenticationTypeCode = newRow["AuthenticationTypeCode"];
                        tempImportObj.AuthenticationTypeName = newRow["AuthenticationTypeName"];
                        tempImportObj.ValDateStr = (string.IsNullOrEmpty(newRow["ValDate"]) ? "" : "_") + newRow["ValDate"];
                        #endregion

                        var tempErrorMessage = new List<string>();

                        #region 导入的数据基本检查
                        //var fieldIndex = notNullableFields.IndexOf("Code".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.AuthenticationTypeCode)) {
                            //if(fieldIndex > -1)
                            tempErrorMessage.Add("认证类型编码不能为空");
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.AuthenticationTypeCode) > fieldLenght["AuthenticationTypeCode".ToUpper()])
                                tempErrorMessage.Add("认证类型编码过长");
                        }

                        if(string.IsNullOrEmpty(tempImportObj.AuthenticationTypeName)) {
                            tempErrorMessage.Add("认证类型名称不能为空");
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.AuthenticationTypeName) > fieldLenght["AuthenticationTypeName".ToUpper()])
                                tempErrorMessage.Add("认证类型名称过长");
                        }
                        if(string.IsNullOrEmpty(tempImportObj.ValDateStr)) {
                            //if(fieldIndex > -1)
                            tempErrorMessage.Add("有效期不能为空");
                        } else {
                            DcsValiDate dcsValiDate;
                            if(Enum.TryParse<DcsValiDate>(tempImportObj.ValDateStr, out dcsValiDate) && Enum.IsDefined(typeof(DcsValiDate), dcsValiDate)) {
                                tempImportObj.ValDate = (int)dcsValiDate;
                            } else {
                                tempErrorMessage.Add("有效期填写有误！");
                            }
                        }
                        #endregion

                        if(tempErrorMessage.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });
                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    #region 剩下的数据进行业务检查
                    //4、校验导入文件中二级站编号 服务站编号组合是否重复， 有重复则提示：文件内数据重复
                    var groups = tempRightList.GroupBy(r => new {
                        r.AuthenticationTypeCode
                    }).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        groupItem.ErrorMsg = ErrorStrings.ImpPartsSupplierRelation_Validation31;
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    var codesNeedCheck = tempRightList.Select(r => r.AuthenticationTypeCode.ToString()).Distinct().ToArray();
                    var dbAuthenticationTypeExtends = new List<AuthenticationTypeExtend>();
                    Func<string[], bool> getDbAuthenticationTypeExtends = value => {
                        var dbObj = new AuthenticationTypeExtend {
                            AuthenticationTypeCode = value[0]
                        };
                        dbAuthenticationTypeExtends.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select authenticationtypecode from authenticationtype where status=1 and branchid=" + userInfo.EnterpriseId, "AuthenticationTypeCode", false, codesNeedCheck, getDbAuthenticationTypeExtends);
                    foreach(AuthenticationTypeExtend tmpItem in dbAuthenticationTypeExtends) {
                        var tmpError= tempRightList.FirstOrDefault(r => r.AuthenticationTypeCode == tmpItem.AuthenticationTypeCode);
                        tmpError.ErrorMsg = "分公司的【" + tmpError.AuthenticationTypeCode + "】认证类型编码已存在，不能重复新增。";
                    }
                    #endregion

                    //获取所有不合格数据
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    //获取所有合格数据
                    rightList = allList.Except(errorList).ToList();

                    //#region 将合格数据的值填上
                    //foreach(var rightItem in rightList) {
                    //    rightItem.Code = rightItem.CodeStr;
                    //    rightItem.Name = rightItem.NameStr;
                    //    rightItem.DealerCode = rightItem.DealerCodeStr;
                    //    rightItem.DealerName = rightItem.DealerNameStr;
                    //    rightItem.Manager = rightItem.ManagerStr;
                    //    rightItem.DealerManager = rightItem.DealerManagerStr;
                    //    rightItem.ManagerPhoneNumber = rightItem.ManagerPhoneNumberStr;
                    //    rightItem.ManagerMail = rightItem.ManagerMailStr;
                    //    rightItem.ManagerMobile = rightItem.ManagerMobileStr;
                    //    rightItem.Address = rightItem.AddressStr;
                    //    rightItem.Remark = rightItem.RemarkStr;
                    //    rightItem.Status = (int)DcsBaseDataStatus.有效;
                    //}
                    //#endregion
                }
                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempObj = list[index - 1];
                            var values = new object[] {
                                #region 设置错误信息导出的列的值
                                tempObj.AuthenticationTypeCode, tempObj.AuthenticationTypeName,
                                tempObj.ValDateStr,
                                tempObj.ErrorMsg
                                #endregion
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }

                //导入所有合格数据
                if(!rightList.Any())
                    return true;
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    //开启事务，新增更新配件在一个事务内
                    var ts = conn.BeginTransaction();
                    try {
                        //新增配件
                        if(rightList.Any()) {
                            #region 获取对应的插入的SQL 通过db.GetInsertSql方法
                            //获取新增数据的sql语句，Id为主键
                            var sqlInsert = db.GetInsertSql("AuthenticationType", "Id", new[] {
                                    "BranchId", "AuthenticationTypeCode", "AuthenticationTypeName","ValDate","Status","CreatorName",
                                    "CreatorId","CreateTime"
                                });
                            #endregion
                            //往数据库增加配件信息
                            foreach(var item in rightList) {
                                #region 添加Sql的参数
                                var command = db.CreateDbCommand(sqlInsert, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("BranchId", userInfo.EnterpriseId));
                                command.Parameters.Add(db.CreateDbParameter("AuthenticationTypeCode", item.AuthenticationTypeCode));
                                command.Parameters.Add(db.CreateDbParameter("AuthenticationTypeName", item.AuthenticationTypeName));
                                command.Parameters.Add(db.CreateDbParameter("ValDate", item.ValDate));
                                command.Parameters.Add(db.CreateDbParameter("Status", (int)DcsBaseDataStatus.有效));
                                command.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                command.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                command.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                command.ExecuteNonQuery();
                                #endregion
                            }
                        }
                        //无异常提交
                        ts.Commit();
                    } catch(Exception) {
                        //报错回滚
                        ts.Rollback();
                        throw;
                    } finally {
                        if(conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }
    }
}
