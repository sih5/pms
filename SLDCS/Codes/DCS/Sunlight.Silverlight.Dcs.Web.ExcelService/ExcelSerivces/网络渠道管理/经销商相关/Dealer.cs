﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        public bool 批量导入修改经销商基本信息(int branchId, string fileName, out int excelImportNum, out List<DealerExtend> rightData, out List<DealerExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<DealerExtend>();
            var rightList = new List<DealerExtend>();
            var allList = new List<DealerExtend>();
            var dbRegions = new List<RegionExtend>();
            var dbDealers = new List<DealerExtend>();
            var dbPartsCompanyInvoiceInfoes = new List<DealerExtend>();
            var dbDealerServiceInfos = new List<DealerExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);
                List<object> excelColumns;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_DealerCode, "DealerCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_DealerName, "DealerName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Agency_Abbreviation, "ShortName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_Manager, "Manager");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_CustomerCode, "CustomerCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_AccountPay_SupplierCode, "SupplierCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Agency_FoundDate, "FoundDate");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Agency_Province, "ProvinceName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Agency_City, "CityName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_MarketingDepartment_CountyName, "CountyName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_CityLevel, "CityLevel");

                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Agency_LinkMan, "ContactPerson");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_ContactPhone, "ContactPhone");
                    excelOperator.AddColumnDataSource("E-MAIL", "ContactMail");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Agency_LinkManPhone, "ContactMobile");

                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Agency_Fax, "Fax");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_ContactPostCode, "ContactPostCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_BusinessAddress, "BusinessAddress");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_BusinessAddressLongitude, "BusinessAddressLongitude");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_BusinessAddressLatitude, "BusinessAddressLatitude");

                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Agency_RegistrationNo, "RegisterCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Agency_RegistrationName, "RegisterName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Agency_RegisterDate, "RegisterDate");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Agency_RegisterCapital, "RegisterCapital");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_FixedAsset, "FixedAsset");

                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Agency_LegalRepresentative, "LegalRepresentative");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_LegalRepresentTel, "LegalRepresentTel");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Agency_Nature, "CorporateNature");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Agency_BusinessScope, "BusinessScope");

                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Agency_RegisteredAddress, "RegisteredAddress");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Agency_IdDocumentType, "IdDocumentType");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Agency_IdDocumentNumber, "IdDocumentNumber");

                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_RepairQualification, "RepairQualification");

                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_OwnerCompany, "OwnerCompany");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_BuildTime, "BuildTime");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_TrafficRestrictionsdescribe, "TrafficRestrictionsdescribe");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_MainBusinessAreas, "MainBusinessAreas");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_AndBusinessAreas, "AndBusinessAreas");
                    //excelOperator.AddColumnDataSource("红岩授权品牌", "BrandScope");
                    //excelOperator.AddColumnDataSource("非红岩授权品牌", "CompetitiveBrandScope");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_DangerousRepairQualification, "DangerousRepairQualification");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_HasBranch, "HasBranch");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_VehicleTravelRoute, "VehicleTravelRoute");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_VehicleUseSpeciality, "VehicleUseSpeciality");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_VehicleDockingStation, "VehicleDockingStation");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_EmployeeNumber, "EmployeeNumber");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_RepairingArea, "RepairingArea");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_ReceptionRoomArea, "ReceptionRoomArea");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_ParkingArea, "ParkingArea");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_PartWarehouseArea, "PartWarehouseArea");

                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_TaxRegisteredNumber, "TaxRegisteredNumber");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_TaxpayerQualification, "TaxpayerQualification");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_InvoiceAmountQuota, "InvoiceAmountQuota");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_InvoiceTitle, "InvoiceTitle");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsInboundCheckBill_BillingType, "InvoiceType");

                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_BankName, "BankName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_InvoiceTax, "InvoiceTax");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_BankAccount, "BankAccount");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_Linkman, "Linkman");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_ContactNumber, "ContactNumber");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_AccountFax, "AccountFax");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_TaxRegisteredAddress, "TaxRegisteredAddress");
                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;

                    //单次最大导入条数不能大于300
                    if(excelOperator.LastRowNum > 300) {
                        throw new Exception(ErrorStrings.Export_Validation_Dealer_Validation1);
                    }
                    #region 获取对应枚举
                    var keyValuePairs = new[] {
                            new KeyValuePair<string, string>("RepairQualification", "Repair_Qualification_Grade"),
                            new KeyValuePair<string, string>("CityLevel","Company_CityLevel"),
                            new KeyValuePair<string, string>("CorporateNature","Corporate_Nature"),
                            new KeyValuePair<string, string>("IdDocumentType","Customer_IdDocumentType"),
                            new KeyValuePair<string, string>("TaxpayerQualification","PartsSupplier_TaxpayerKind"),
                            new KeyValuePair<string, string>("InvoiceType","Invoice_Type"),
                            new KeyValuePair<string, string>("VehicleTravelRoute","DealerServiceExt_VehicleTravelRoute"),
                            new KeyValuePair<string, string>("VehicleUseSpeciality","DealerServiceExt_VehicleUseSpeciality"),
                            new KeyValuePair<string, string>("VehicleDockingStation","DealerServiceExt_VehicleDockingStations")
                        };
                    tempExcelOperator.ImportHelper.LoadEnumFromDb(null, null, keyValuePairs);
                    #endregion
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var tempImportObj = new DealerExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        #region
                        tempImportObj.Code = newRow["DealerCode"];
                        tempImportObj.Name = newRow["DealerName"];

                        tempImportObj.ShortName = newRow["ShortName"];
                        tempImportObj.Manager = newRow["Manager"];
                        tempImportObj.CustomerCode = newRow["CustomerCode"];
                        tempImportObj.SupplierCode = newRow["SupplierCode"];
                        tempImportObj.FoundDateStr = newRow["FoundDate"];
                        tempImportObj.ProvinceName = newRow["ProvinceName"];
                        tempImportObj.CityName = newRow["CityName"];
                        tempImportObj.CountyName = newRow["CountyName"];
                        tempImportObj.CityLevelStr = newRow["CityLevel"];

                        tempImportObj.ContactPerson = newRow["ContactPerson"];
                        tempImportObj.ContactMobile = newRow["ContactMobile"];
                        tempImportObj.ContactPhone = newRow["ContactPhone"];

                        tempImportObj.ContactMail = newRow["ContactMail"];
                        tempImportObj.Fax = newRow["Fax"];
                        tempImportObj.ContactPostCode = newRow["ContactPostCode"];
                        tempImportObj.BusinessAddress = newRow["BusinessAddress"];

                        tempImportObj.RegisterCode = newRow["RegisterCode"];
                        tempImportObj.RegisterName = newRow["RegisterName"];
                        tempImportObj.RegisterDateStr = newRow["RegisterDate"];
                        tempImportObj.FixedAsset = newRow["FixedAsset"];

                        tempImportObj.LegalRepresentative = newRow["LegalRepresentative"];
                        tempImportObj.RegisterCapital = newRow["RegisterCapital"];
                        tempImportObj.LegalRepresentTel = newRow["LegalRepresentTel"];
                        tempImportObj.CorporateNatureStr = newRow["CorporateNature"];
                        tempImportObj.BusinessScope = newRow["BusinessScope"];
                        tempImportObj.BusinessAddressLatitudeStr = newRow["BusinessAddressLatitude"];
                        tempImportObj.BusinessAddressLongitudeStr = newRow["BusinessAddressLongitude"];
                        tempImportObj.RegisteredAddress = newRow["RegisteredAddress"];
                        tempImportObj.IdDocumentTypeStr = newRow["IdDocumentType"];
                        tempImportObj.IdDocumentNumber = newRow["IdDocumentNumber"];

                        tempImportObj.RepairQualificationStr = newRow["RepairQualification"];

                        tempImportObj.OwnerCompany = newRow["OwnerCompany"];
                        tempImportObj.BuildTimeStr = newRow["BuildTime"];
                        tempImportObj.TrafficRestrictionsdescribe = newRow["TrafficRestrictionsdescribe"];
                        tempImportObj.MainBusinessAreas = newRow["MainBusinessAreas"];
                        tempImportObj.AndBusinessAreas = newRow["AndBusinessAreas"];
                        //tempImportObj.BrandScope = newRow["BrandScope"];
                        //tempImportObj.CompetitiveBrandScope = newRow["CompetitiveBrandScope"];
                        tempImportObj.DangerousRepairQualificationStr = newRow["DangerousRepairQualification"];
                        tempImportObj.HasBranchStr = newRow["HasBranch"];
                        tempImportObj.VehicleTravelRouteStr = newRow["VehicleTravelRoute"];
                        tempImportObj.VehicleUseSpecialityStr = newRow["VehicleUseSpeciality"];
                        tempImportObj.VehicleDockingStationStr = newRow["VehicleDockingStation"];
                        tempImportObj.EmployeeNumberStr = newRow["EmployeeNumber"];
                        tempImportObj.RepairingArea = newRow["RepairingArea"];
                        tempImportObj.ReceptionRoomArea = newRow["ReceptionRoomArea"];
                        tempImportObj.ParkingArea = newRow["ParkingArea"];
                        tempImportObj.PartWarehouseArea = newRow["PartWarehouseArea"];

                        tempImportObj.TaxRegisteredNumber = newRow["TaxRegisteredNumber"];

                        tempImportObj.TaxpayerQualificationStr = newRow["TaxpayerQualification"];
                        tempImportObj.InvoiceAmountQuota = newRow["InvoiceAmountQuota"];
                        tempImportObj.InvoiceTitle = newRow["InvoiceTitle"];
                        tempImportObj.InvoiceTypeStr = newRow["InvoiceType"];

                        tempImportObj.BankName = newRow["BankName"];
                        tempImportObj.InvoiceTax = newRow["InvoiceTax"];

                        tempImportObj.BankAccount = newRow["BankAccount"];
                        tempImportObj.Linkman = newRow["Linkman"];
                        tempImportObj.ContactNumber = newRow["ContactNumber"];
                        tempImportObj.AccountFax = newRow["AccountFax"];
                        tempImportObj.TaxRegisteredAddress = newRow["TaxRegisteredAddress"];
                        #endregion
                        var tempErrorMessage = new List<string>();
                        #region 导入的内容基本检查
                        //导入模板必填
                        if(string.IsNullOrEmpty(tempImportObj.Code)) {
                            tempErrorMessage.Add(ErrorStrings.Export_Validation_Dealer_Validation2);
                        }
                        if(!string.IsNullOrEmpty(tempImportObj.RepairQualificationStr)) {
                            //try {
                            //    tempImportObj.RepairQualification = (int)Enum.Parse(typeof(DcsRepairQualificationGrade), tempImportObj.RepairQualificationStr);
                            //} catch(Exception) {
                            //    tempErrorMessage.Add(ErrorStrings.Export_Validation_Dealer_Validation3);
                            //}
                            DcsRepairQualificationGrade repairQualificationGrade;
                            if(Enum.TryParse<DcsRepairQualificationGrade>(tempImportObj.RepairQualificationStr, out repairQualificationGrade) && Enum.IsDefined(typeof(DcsRepairQualificationGrade), repairQualificationGrade)) {
                                tempImportObj.RepairQualification = (int)repairQualificationGrade;
                            } else {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Dealer_Validation3);
                            }
                        }


                        //城市级别
                        if(!string.IsNullOrEmpty(tempImportObj.CityLevelStr)) {
                            DcsCompanyCityLevel cityLevel;
                            if(Enum.TryParse<DcsCompanyCityLevel>(tempImportObj.CityLevelStr, out cityLevel) && Enum.IsDefined(typeof(DcsCompanyCityLevel), cityLevel)) {
                                tempImportObj.CityLevel = (int)cityLevel;
                            } else {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Dealer_Validation4);
                            }
                        }

                        //企业性质
                        if(!string.IsNullOrEmpty(tempImportObj.CorporateNatureStr)) {
                            DcsCorporateNature corporateNature;
                            if(Enum.TryParse<DcsCorporateNature>(tempImportObj.CorporateNatureStr, out corporateNature) && Enum.IsDefined(typeof(DcsCorporateNature), corporateNature)) {
                                tempImportObj.CorporateNature = (int)corporateNature;
                            } else {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Dealer_Validation5);
                            }
                        }
                        //身份证类型
                        if(!string.IsNullOrEmpty(tempImportObj.IdDocumentTypeStr)) {
                            DcsCustomerIdDocumentType idDocumentType;
                            if(Enum.TryParse<DcsCustomerIdDocumentType>(tempImportObj.IdDocumentTypeStr, out idDocumentType) && Enum.IsDefined(typeof(DcsCustomerIdDocumentType), idDocumentType)) {
                                tempImportObj.IdDocumentType = (int)idDocumentType;
                            } else {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Dealer_Validation6);
                            }
                        }

                        //纳税人性质
                        if(!string.IsNullOrEmpty(tempImportObj.TaxpayerQualificationStr)) {
                            DcsPartsSupplierTaxpayerKind taxpayerKind;
                            if(Enum.TryParse<DcsPartsSupplierTaxpayerKind>(tempImportObj.TaxpayerQualificationStr, out taxpayerKind) && Enum.IsDefined(typeof(DcsPartsSupplierTaxpayerKind), taxpayerKind)) {
                                tempImportObj.TaxpayerQualification = (int)taxpayerKind;
                            } else {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Dealer_Validation7);
                            }
                        }
                        //开票类型
                        if(!string.IsNullOrEmpty(tempImportObj.InvoiceTypeStr)) {
                            DcsInvoiceType dcsInvoiceType;
                            if(Enum.TryParse<DcsInvoiceType>(tempImportObj.InvoiceTypeStr, out dcsInvoiceType) && Enum.IsDefined(typeof(DcsInvoiceType), dcsInvoiceType)) {
                                tempImportObj.InvoiceType = (int)dcsInvoiceType;
                            } else {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Dealer_Validation8);
                            }
                        }



                        //车辆行驶路线
                        if(!string.IsNullOrEmpty(tempImportObj.VehicleTravelRouteStr)) {
                            DcsDealerServiceExtVehicleTravelRoute travelRoute;
                            if(Enum.TryParse<DcsDealerServiceExtVehicleTravelRoute>(tempImportObj.VehicleTravelRouteStr, out travelRoute) && Enum.IsDefined(typeof(DcsDealerServiceExtVehicleTravelRoute), travelRoute)) {
                                tempImportObj.VehicleTravelRoute = (int)travelRoute;
                            } else {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Dealer_Validation9);
                            }
                        }

                        //车辆使用特性
                        if(!string.IsNullOrEmpty(tempImportObj.VehicleUseSpecialityStr)) {
                            DcsDealerServiceExtVehicleUseSpeciality useSpeciality;
                            if(Enum.TryParse<DcsDealerServiceExtVehicleUseSpeciality>(tempImportObj.VehicleUseSpecialityStr, out useSpeciality) && Enum.IsDefined(typeof(DcsDealerServiceExtVehicleUseSpeciality), useSpeciality)) {
                                tempImportObj.VehicleUseSpeciality = (int)useSpeciality;
                            } else {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Dealer_Validation10);
                            }
                        }

                        //车辆停靠点
                        if(!string.IsNullOrEmpty(tempImportObj.VehicleDockingStationStr)) {
                            DcsDealerServiceExtVehicleDockingStations dockingStation;
                            if(Enum.TryParse<DcsDealerServiceExtVehicleDockingStations>(tempImportObj.VehicleDockingStationStr, out dockingStation) && Enum.IsDefined(typeof(DcsDealerServiceExtVehicleDockingStations), dockingStation)) {
                                tempImportObj.VehicleDockingStation = (int)dockingStation;
                            } else {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Dealer_Validation11);
                            }
                        }

                        //有无二级服务站
                        if(!string.IsNullOrEmpty(tempImportObj.HasBranchStr)) {
                            DcsIsOrNot dcsIsOrNot;
                            if(Enum.TryParse<DcsIsOrNot>(tempImportObj.HasBranchStr, out dcsIsOrNot) && Enum.IsDefined(typeof(DcsIsOrNot), dcsIsOrNot)) {
                                tempImportObj.HasBranch = (int)dcsIsOrNot;
                            } else {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Dealer_Validation12);
                            }
                            //try {
                            //    tempImportObj.HasBranch = (int)Enum.Parse(typeof(DcsIsOrNot),tempImportObj.HasBranchStr);
                            //}catch(Exception){
                            //    tempErrorMessage.Add("请填写“是”或“否”");
                            //}
                        }
                        //危险品运输车辆维修许可证
                        if(!string.IsNullOrEmpty(tempImportObj.DangerousRepairQualificationStr)) {
                            DcsIsOrNot dcsIsOrNotDan;
                            if(Enum.TryParse<DcsIsOrNot>(tempImportObj.DangerousRepairQualificationStr, out dcsIsOrNotDan) && Enum.IsDefined(typeof(DcsIsOrNot), dcsIsOrNotDan)) {
                                tempImportObj.DangerousRepairQualification = (int)dcsIsOrNotDan;
                            } else {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Dealer_Validation13);
                            }
                        }

                        if(!string.IsNullOrEmpty(tempImportObj.RegisterCapital)) {
                            int checkValue;
                            if(int.TryParse(tempImportObj.RegisterCapital, out checkValue)) {
                                if(checkValue <= 0) {
                                    tempErrorMessage.Add(ErrorStrings.Export_Validation_Dealer_Validation14);
                                }
                                tempImportObj.RegisterCapital = tempImportObj.RegisterCapital;
                            } else {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Dealer_Validation15);
                            }
                        }

                        //字段长度校验
                        if(!string.IsNullOrEmpty(tempImportObj.BusinessAddress)) {
                            if(Encoding.Default.GetByteCount(tempImportObj.BusinessAddress) > 200) {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Dealer_Validation16);
                            }
                        }
                        if(!string.IsNullOrEmpty(tempImportObj.BusinessScope)) {
                            if(Encoding.Default.GetByteCount(tempImportObj.BusinessScope) > 200) {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Dealer_Validation17);
                            }
                        }
                        if(!string.IsNullOrEmpty(tempImportObj.RegisteredAddress)) {
                            if(Encoding.Default.GetByteCount(tempImportObj.RegisteredAddress) > 200) {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Dealer_Validation18);
                            }
                        }
                        if(!string.IsNullOrEmpty(tempImportObj.MainBusinessAreas)) {
                            if(Encoding.Default.GetByteCount(tempImportObj.MainBusinessAreas) > 200) {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Dealer_Validation19);
                            }
                        }
                        if(!string.IsNullOrEmpty(tempImportObj.AndBusinessAreas)) {
                            if(Encoding.Default.GetByteCount(tempImportObj.AndBusinessAreas) > 200) {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Dealer_Validation20);
                            }
                        }

                        if(!string.IsNullOrEmpty(tempImportObj.RegisterName)) {
                            if(Encoding.Default.GetByteCount(tempImportObj.RegisterName) > 100) {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Dealer_Validation21);
                            }
                        }
                        if(!string.IsNullOrEmpty(tempImportObj.TrafficRestrictionsdescribe)) {
                            if(Encoding.Default.GetByteCount(tempImportObj.TrafficRestrictionsdescribe) > 100) {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Dealer_Validation22);
                            }
                        }

                        //if(!string.IsNullOrEmpty(tempImportObj.BrandScope)) {
                        //    if(Encoding.Default.GetByteCount(tempImportObj.BrandScope) > 50) {
                        //        tempErrorMessage.Add("红岩授权品牌长度过长");
                        //    }
                        //}
                        //if(!string.IsNullOrEmpty(tempImportObj.CompetitiveBrandScope)) {
                        //    if(Encoding.Default.GetByteCount(tempImportObj.CompetitiveBrandScope) > 50) {
                        //        tempErrorMessage.Add("非红岩授权品牌长度过长");
                        //    }
                        //}


                        if(!string.IsNullOrEmpty(tempImportObj.FoundDateStr)) {
                            DateTime dt;
                            if(!DateTime.TryParse(tempImportObj.FoundDateStr, out dt))
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Dealer_Validation23);
                        }

                        if(!string.IsNullOrEmpty(tempImportObj.RegisterDateStr)) {
                            DateTime dt;
                            if(!DateTime.TryParse(tempImportObj.RegisterDateStr, out dt))
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Dealer_Validation24);
                        }

                        if(!string.IsNullOrEmpty(tempImportObj.EmployeeNumberStr)) {
                            int checkValue;
                            if(!int.TryParse(tempImportObj.EmployeeNumberStr, out checkValue)) {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Dealer_Validation25);
                            }
                        }

                        if(!string.IsNullOrEmpty(tempImportObj.FixedAsset)) {
                            if(Regex.IsMatch(tempImportObj.FixedAsset.ToString(), @"^(?:(?:0(?=(?:0?\.)|$))|[1-9])\d*(?:\.(?:(?:0(?=[1-9]))|[1-9])\d?)?$") == false)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Dealer_Validation26);
                        }

                        //if(!string.IsNullOrEmpty(tempImportObj.CorporateNature)) {
                        //   if(Regex.IsMatch(tempImportObj.CorporateNature.ToString(), "^[0-9]*[1-9][0-9]*$") == false)
                        //       tempErrorMessage.Add("企业性质应为数字");
                        //}

                        if(!string.IsNullOrEmpty(tempImportObj.BuildTimeStr)) {
                            DateTime dt;
                            if(!DateTime.TryParse(tempImportObj.BuildTimeStr, out dt))
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Dealer_Validation27);
                        }

                        if(!string.IsNullOrEmpty(tempImportObj.RepairingArea)) {
                            if(Regex.IsMatch(tempImportObj.RepairingArea.ToString(), @"^(?:(?:0(?=(?:0?\.)|$))|[1-9])\d*(?:\.(?:(?:0(?=[1-9]))|[1-9])\d?)?$") == false)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Dealer_Validation28);
                        }

                        if(!string.IsNullOrEmpty(tempImportObj.ReceptionRoomArea)) {
                            if(Regex.IsMatch(tempImportObj.ReceptionRoomArea.ToString(), @"^(?:(?:0(?=(?:0?\.)|$))|[1-9])\d*(?:\.(?:(?:0(?=[1-9]))|[1-9])\d?)?$") == false)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Dealer_Validation29);
                        }

                        if(!string.IsNullOrEmpty(tempImportObj.ParkingArea)) {
                            if(Regex.IsMatch(tempImportObj.ParkingArea.ToString(), @"^(?:(?:0(?=(?:0?\.)|$))|[1-9])\d*(?:\.(?:(?:0(?=[1-9]))|[1-9])\d?)?$") == false)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Dealer_Validation30);
                        }

                        if(!string.IsNullOrEmpty(tempImportObj.PartWarehouseArea)) {
                            if(Regex.IsMatch(tempImportObj.PartWarehouseArea.ToString(), @"^(?:(?:0(?=(?:0?\.)|$))|[1-9])\d*(?:\.(?:(?:0(?=[1-9]))|[1-9])\d?)?$") == false)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Dealer_Validation31);
                        }

                        if(!string.IsNullOrEmpty(tempImportObj.InvoiceAmountQuota)) {
                            if(Regex.IsMatch(tempImportObj.InvoiceAmountQuota.ToString(), @"^(?:(?:0(?=(?:0?\.)|$))|[1-9])\d*(?:\.(?:(?:0(?=[1-9]))|[1-9])\d?)?$") == false)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Dealer_Validation32);
                        }

                        if(!string.IsNullOrEmpty(tempImportObj.InvoiceTax)) {
                            if(Regex.IsMatch(tempImportObj.InvoiceTax.ToString(), @"^(?:(?:0(?=(?:0?\.)|$))|[1-9])\d*(?:\.(?:(?:0(?=[1-9]))|[1-9])\d?)?$") == false)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Dealer_Validation33);
                        }

                        #endregion
                        if(tempErrorMessage.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });
                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    var groups = tempRightList.GroupBy(r => new {
                        r.Code
                    }).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        groupItem.ErrorMsg = ErrorStrings.ImpPartsSupplierRelation_Validation31;
                    }
                    //1、查询企业基本信息,校验经销商基本信息.经销商编号,名称存在，否则提示：经销商编号，经销商名称不存在。
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();
                    var dealerCodeNeedCheck = tempRightList.Select(r => r.Code).Distinct().ToArray();

                    Func<string[], bool> getDbDealers = value => {
                        var dbObj = new DealerExtend {
                            Id = Convert.ToInt32(value[0]),
                            Code = value[1],
                            Name = value[2]
                        };
                        dbDealers.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select id,code,name from company where status=1 ", "Code", true, dealerCodeNeedCheck, getDbDealers);
                    foreach(var tempObj in tempRightList) {
                        var tempobjcheck = dbDealers.FirstOrDefault(r => r.Code == tempObj.Code);
                        if(tempobjcheck == null) {
                            tempObj.ErrorMsg = tempObj.Code + ErrorStrings.Export_Validation_PartsSpecialTreatyPrice_Validation1;
                            continue;
                        } else {
                            tempObj.Code = tempobjcheck.Code;
                            tempObj.Id = tempobjcheck.Id;
                        }
                    }
                    //2、校验是否存在企业开票信息 否则提示：企业开票信息不存在。
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();
                    if(tempRightList.Any(r => !string.IsNullOrEmpty(r.TaxRegisteredNumber) || !string.IsNullOrEmpty(r.BankName) || !string.IsNullOrEmpty(r.BankAccount) || !string.IsNullOrEmpty(r.ContactNumber) || !string.IsNullOrEmpty(r.TaxRegisteredAddress))) {
                        var codeNeedCheck = tempRightList.Where(r => !string.IsNullOrEmpty(r.TaxRegisteredNumber) || !string.IsNullOrEmpty(r.BankName) || !string.IsNullOrEmpty(r.BankAccount) || !string.IsNullOrEmpty(r.ContactNumber) || !string.IsNullOrEmpty(r.TaxRegisteredAddress)).Select(r => r.Code).Distinct().ToArray();
                        //var dbPartsCompanyInvoiceInfoes = new List<DealerExtend>();
                        Func<string[], bool> getDbPartsCompanyInvoiceInfoes = value => {
                            var dbObj = new DealerExtend {
                                InvoiceCompanyId = Convert.ToInt32(value[0]),
                                Id = Convert.ToInt32(value[1]),
                                Code = value[2],
                                CompanyInvoiceInfoId = Convert.ToInt32(value[3])
                            };
                            dbPartsCompanyInvoiceInfoes.Add(dbObj);
                            return false;
                        };
                        db.QueryDataWithInOperator(string.Format("select InvoiceCompanyId,CompanyId,CompanyCode,id from CompanyInvoiceInfo where status=1 and InvoiceCompanyId={0} ", branchId), "CompanyCode", true, codeNeedCheck, getDbPartsCompanyInvoiceInfoes);
                        foreach(var tempObj in tempRightList.Where(r => !string.IsNullOrEmpty(r.TaxRegisteredNumber) || !string.IsNullOrEmpty(r.BankName) || !string.IsNullOrEmpty(r.BankAccount) || !string.IsNullOrEmpty(r.ContactNumber) || !string.IsNullOrEmpty(r.TaxRegisteredAddress))) {
                            var tempobjcheck = dbPartsCompanyInvoiceInfoes.FirstOrDefault(r => r.Code == tempObj.Code);
                            if(tempobjcheck == null)
                                continue;
                            tempObj.CompanyInvoiceInfoId = tempobjcheck.CompanyInvoiceInfoId;
                        }
                    }
                    //经销商服务扩展信息不存在（有效的）  提示 企业分品牌信息不存在
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();
                    // if(tempRightList.Any(r => !string.IsNullOrEmpty(r.RepairQualificationStr))) {
                    var dealerIdNeedCheck = tempRightList.Select(r => r.Id.ToString(CultureInfo.InvariantCulture)).Distinct().ToArray();
                    //var dbDealerServiceInfos = new List<DealerExtend>();
                    Func<string[], bool> getDbDealerServiceInfos = value => {
                        var dbObj = new DealerExtend {
                            Id = Convert.ToInt32(value[0]),
                        };
                        dbDealerServiceInfos.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select Id from DealerServiceExt where status=1  ", "Id", true, dealerIdNeedCheck, getDbDealerServiceInfos);
                    foreach(var tempObj in tempRightList) {
                        var tempobjcheck = dbDealerServiceInfos.FirstOrDefault(r => r.Id == tempObj.Id);
                        if(tempobjcheck == null)
                            continue;
                        tempObj.Id = tempobjcheck.Id;
                    }
                    //}


                    //-------------------------------
                    //省市县匹配校验
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();

                    //var dbRegions = new List<RegionExtend>();
                    Func<string[], bool> getDbRegionNames = value => {
                        var dbObj = new RegionExtend {
                            Id = Convert.ToInt32(value[0]),
                            ProvinceName = value[1],
                            CityName = value[2],
                            CountyName = value[3]
                        };
                        dbRegions.Add(dbObj);
                        return false;
                    };

                    var reg = new List<string>();
                    reg.Add("1");
                    db.QueryDataWithInOperator("select Id,ProvinceName,CityName,CountyName from tiledregion ", "1", false, reg.ToArray(), getDbRegionNames);

                    foreach(var tempObj in tempRightList) {
                        var regionid = dbRegions.Where(r => r.ProvinceName == tempObj.ProvinceName && r.CityName == tempObj.CityName && r.CountyName == tempObj.CountyName).FirstOrDefault();
                        if(regionid != null) {
                            tempObj.RegionId = regionid.Id;   //command.Parameters.Add(db.CreateDbParameter("ARegionId", regionid.Id));
                        } else {
                            tempObj.ErrorMsg = ErrorStrings.Export_Validation_Dealer_Validation34;
                            //continue;
                        }

                    }
                    ////---------------------------


                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    rightList = allList.Except(errorList).ToList();
                }

                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempObj = list[index - 1];
                            var values = new object[] {
                                tempObj.Code, 
                                tempObj.Name,
                                tempObj.ShortName,
                                tempObj.Manager,
                                tempObj.CustomerCode,
                                tempObj.SupplierCode,
                                tempObj.FoundDate,
                                tempObj.ProvinceName,
                                tempObj.CityName,
                                tempObj.CountyName,
                                tempObj.CityLevelStr,
                                tempObj.ContactPerson,
                                tempObj.ContactMobile,
                                tempObj.ContactPhone,

                                tempObj.ContactMail,
                                tempObj.Fax,
                                tempObj.ContactPostCode,
                                tempObj.BusinessAddress,

                                tempObj.RegisterCode ,
                                tempObj.RegisterName,
                                tempObj.RegisterDateStr,
                                tempObj.FixedAsset,

                                tempObj.LegalRepresentative ,
                                tempObj.RegisterCapital,
                                tempObj.LegalRepresentTel,
                                tempObj.CorporateNature,
                                tempObj.BusinessScope,
                                tempObj.BusinessAddressLongitude,
                                tempObj.BusinessAddressLatitude,
                                tempObj.RegisteredAddress,
                                tempObj.IdDocumentTypeStr,
                                tempObj.IdDocumentNumber,

                                tempObj.RepairQualificationStr,

                                tempObj.OwnerCompany,
                                tempObj.BuildTimeStr,
                                tempObj.TrafficRestrictionsdescribe,
                                tempObj.MainBusinessAreas,
                                tempObj.AndBusinessAreas,
                                //tempObj.BrandScope,
                          

                                //tempObj.CompetitiveBrandScope,
                                tempObj.DangerousRepairQualificationStr,
                                tempObj.HasBranchStr,
                                tempObj.VehicleTravelRouteStr,
                                tempObj.VehicleUseSpecialityStr,
                                tempObj.VehicleDockingStationStr,
                                tempObj.EmployeeNumberStr,
                                tempObj.RepairingArea,
                                tempObj.ReceptionRoomArea,
                                tempObj.ParkingArea,
                                tempObj.PartWarehouseArea,

                                tempObj.TaxRegisteredNumber, 
                                tempObj.TaxpayerQualificationStr,
                                tempObj.InvoiceAmountQuota,
                                tempObj.InvoiceTitle,
                                tempObj.InvoiceTypeStr,

                                tempObj.BankName,
                                tempObj.InvoiceTax,

                                tempObj.BankAccount,
                                tempObj.Linkman,

                                tempObj.ContactNumber,
                                tempObj.AccountFax,
                                tempObj.TaxRegisteredAddress,
                                tempObj.ErrorMsg
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }
                //导入所有合格数据
                if(!rightList.Any())
                    return true;
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    //开启事务，新增更新配件分品牌信息在一个事务内
                    var ts = conn.BeginTransaction();
                    try {
                        if(rightList.Any()) {
                            var userInfo = Utils.GetCurrentUserInfo();
                            //单个更新字段
                            //服务站名称


                            foreach(var item in rightList) {
                                #region  Dealer
                                List<string> fieldDealers = new List<string>();
                                string sqlUpdateDealers = null;
                                fieldDealers.Add("ModifyTime");
                                fieldDealers.Add("ModifierId");
                                fieldDealers.Add("ModifierName");
                                var commandDealers = db.CreateDbCommand(sqlUpdateDealers, conn, ts);

                                if(!string.IsNullOrEmpty(item.Name)) {
                                    fieldDealers.Add("Name");
                                    commandDealers.Parameters.Add(db.CreateDbParameter("Name", item.Name));
                                }
                                if(!string.IsNullOrEmpty(item.ShortName)) {
                                    fieldDealers.Add("ShortName");
                                    commandDealers.Parameters.Add(db.CreateDbParameter("ShortName", item.ShortName));
                                }
                                if(!string.IsNullOrEmpty(item.Manager)) {
                                    fieldDealers.Add("Manager");
                                    commandDealers.Parameters.Add(db.CreateDbParameter("Manager", item.Manager));
                                }
                                sqlUpdateDealers = db.GetUpdateSql("Dealer", fieldDealers.ToArray(), new[] { "Id" });
                                commandDealers.Parameters.Add(db.CreateDbParameter("Id", item.Id));
                                commandDealers.Parameters.Add(db.CreateDbParameter("ModifyTime", DateTime.Now));
                                commandDealers.Parameters.Add(db.CreateDbParameter("ModifierId", userInfo.Id));
                                commandDealers.Parameters.Add(db.CreateDbParameter("ModifierName", userInfo.Name));
                                commandDealers.CommandText = sqlUpdateDealers;
                                commandDealers.ExecuteNonQuery();
                                #endregion

                                #region //Company
                                List<string> fieldCompanys = new List<string>();
                                string sqlUpdateCompanys = null;
                                fieldCompanys.Add("ModifyTime");
                                fieldCompanys.Add("ModifierId");
                                fieldCompanys.Add("ModifierName");
                                var commandCompanyes = db.CreateDbCommand(sqlUpdateCompanys, conn, ts);

                                if(!string.IsNullOrEmpty(item.Name)) {
                                    fieldCompanys.Add("Name");
                                    commandCompanyes.Parameters.Add(db.CreateDbParameter("Name", item.Name));
                                }
                                if(!string.IsNullOrEmpty(item.ShortName)) {
                                    fieldCompanys.Add("ShortName");
                                    commandCompanyes.Parameters.Add(db.CreateDbParameter("ShortName", item.ShortName));
                                }
                                if(!string.IsNullOrEmpty(item.CustomerCode)) {
                                    fieldCompanys.Add("CustomerCode");
                                    commandCompanyes.Parameters.Add(db.CreateDbParameter("CustomerCode", item.CustomerCode));
                                }
                                if(!string.IsNullOrEmpty(item.SupplierCode)) {
                                    fieldCompanys.Add("SupplierCode");
                                    commandCompanyes.Parameters.Add(db.CreateDbParameter("SupplierCode", item.SupplierCode));
                                }
                                if(!string.IsNullOrEmpty(item.FoundDateStr)) {
                                    fieldCompanys.Add("FoundDate");
                                    commandCompanyes.Parameters.Add(db.CreateDbParameter("FoundDate", Convert.ToDateTime(item.FoundDateStr)));
                                }
                                if(!string.IsNullOrEmpty(item.ProvinceName)) {
                                    fieldCompanys.Add("ProvinceName");
                                    commandCompanyes.Parameters.Add(db.CreateDbParameter("ProvinceName", item.ProvinceName));
                                }
                                if(!string.IsNullOrEmpty(item.CityName)) {
                                    fieldCompanys.Add("CityName");
                                    commandCompanyes.Parameters.Add(db.CreateDbParameter("CityName", item.CityName));
                                }
                                if(!string.IsNullOrEmpty(item.CountyName)) {
                                    fieldCompanys.Add("CountyName");
                                    commandCompanyes.Parameters.Add(db.CreateDbParameter("CountyName", item.CountyName));
                                }
                                if(!string.IsNullOrEmpty(item.CityLevelStr)) {
                                    fieldCompanys.Add("CityLevel");
                                    commandCompanyes.Parameters.Add(db.CreateDbParameter("CityLevel", item.CityLevel));
                                }
                                if(!string.IsNullOrEmpty(item.ContactPerson)) {
                                    fieldCompanys.Add("ContactPerson");
                                    commandCompanyes.Parameters.Add(db.CreateDbParameter("ContactPerson", item.ContactPerson));
                                }
                                if(!string.IsNullOrEmpty(item.ContactMobile)) {
                                    fieldCompanys.Add("ContactMobile");
                                    commandCompanyes.Parameters.Add(db.CreateDbParameter("ContactMobile", item.ContactMobile));
                                }
                                if(!string.IsNullOrEmpty(item.ContactPhone)) {
                                    fieldCompanys.Add("ContactPhone");
                                    commandCompanyes.Parameters.Add(db.CreateDbParameter("ContactPhone", item.ContactPhone));
                                }
                                if(!string.IsNullOrEmpty(item.ContactMail)) {
                                    fieldCompanys.Add("ContactMail");
                                    commandCompanyes.Parameters.Add(db.CreateDbParameter("ContactMail", item.ContactMail));
                                }
                                if(!string.IsNullOrEmpty(item.Fax)) {
                                    fieldCompanys.Add("Fax");
                                    commandCompanyes.Parameters.Add(db.CreateDbParameter("Fax", item.Fax));
                                }
                                if(!string.IsNullOrEmpty(item.ContactPostCode)) {
                                    fieldCompanys.Add("ContactPostCode");
                                    commandCompanyes.Parameters.Add(db.CreateDbParameter("ContactPostCode", item.ContactPostCode));
                                }
                                if(!string.IsNullOrEmpty(item.BusinessAddress)) {
                                    fieldCompanys.Add("BusinessAddress");
                                    commandCompanyes.Parameters.Add(db.CreateDbParameter("BusinessAddress", item.BusinessAddress));
                                }
                                if(!string.IsNullOrEmpty(item.RegisterCode)) {
                                    fieldCompanys.Add("RegisterCode");
                                    commandCompanyes.Parameters.Add(db.CreateDbParameter("RegisterCode", item.RegisterCode));
                                }
                                if(!string.IsNullOrEmpty(item.RegisterName)) {
                                    fieldCompanys.Add("RegisterName");
                                    commandCompanyes.Parameters.Add(db.CreateDbParameter("RegisterName", item.RegisterName));
                                }
                                if(!string.IsNullOrEmpty(item.RegisterDateStr)) {
                                    fieldCompanys.Add("RegisterDate");
                                    commandCompanyes.Parameters.Add(db.CreateDbParameter("RegisterDate", Convert.ToDateTime(item.RegisterDateStr)));
                                }
                                if(!string.IsNullOrEmpty(item.FixedAsset)) {
                                    fieldCompanys.Add("FixedAsset");
                                    commandCompanyes.Parameters.Add(db.CreateDbParameter("FixedAsset", item.FixedAsset));
                                }
                                if(!string.IsNullOrEmpty(item.LegalRepresentative)) {
                                    fieldCompanys.Add("LegalRepresentative");
                                    commandCompanyes.Parameters.Add(db.CreateDbParameter("LegalRepresentative", item.LegalRepresentative));
                                }
                                if(!string.IsNullOrEmpty(item.RegisterCapital)) {
                                    fieldCompanys.Add("RegisterCapital");
                                    commandCompanyes.Parameters.Add(db.CreateDbParameter("RegisterCapital", item.RegisterCapital));
                                }
                                if(!string.IsNullOrEmpty(item.LegalRepresentTel)) {
                                    fieldCompanys.Add("LegalRepresentTel");
                                    commandCompanyes.Parameters.Add(db.CreateDbParameter("LegalRepresentTel", item.LegalRepresentTel));
                                }
                                if(!string.IsNullOrEmpty(item.CorporateNatureStr)) {
                                    fieldCompanys.Add("CorporateNature");
                                    commandCompanyes.Parameters.Add(db.CreateDbParameter("CorporateNature", item.CorporateNature));
                                }
                                if(!string.IsNullOrEmpty(item.BusinessScope)) {
                                    fieldCompanys.Add("BusinessScope");
                                    commandCompanyes.Parameters.Add(db.CreateDbParameter("BusinessScope", item.BusinessScope));
                                }
                                if(!string.IsNullOrEmpty(item.BusinessAddressLongitudeStr)) {
                                    fieldCompanys.Add("BusinessAddressLongitude");
                                    commandCompanyes.Parameters.Add(db.CreateDbParameter("BusinessAddressLongitude", item.BusinessAddressLongitudeStr));
                                }
                                if(!string.IsNullOrEmpty(item.BusinessAddressLatitudeStr)) {
                                    fieldCompanys.Add("BusinessAddressLatitude");
                                    commandCompanyes.Parameters.Add(db.CreateDbParameter("BusinessAddressLatitude", item.BusinessAddressLatitudeStr));
                                }
                                if(!string.IsNullOrEmpty(item.RegisteredAddress)) {
                                    fieldCompanys.Add("RegisteredAddress");
                                    commandCompanyes.Parameters.Add(db.CreateDbParameter("RegisteredAddress", item.RegisteredAddress));
                                }
                                if(!string.IsNullOrEmpty(item.IdDocumentTypeStr)) {
                                    fieldCompanys.Add("IdDocumentType");
                                    commandCompanyes.Parameters.Add(db.CreateDbParameter("IdDocumentType", item.IdDocumentType));
                                }
                                if(!string.IsNullOrEmpty(item.IdDocumentNumber)) {
                                    fieldCompanys.Add("IdDocumentNumber");
                                    commandCompanyes.Parameters.Add(db.CreateDbParameter("IdDocumentNumber", item.IdDocumentNumber));
                                }

                                sqlUpdateCompanys = db.GetUpdateSql("Company", fieldCompanys.ToArray(), new[] { "Id" });
                                commandCompanyes.Parameters.Add(db.CreateDbParameter("Id", item.Id));
                                commandCompanyes.Parameters.Add(db.CreateDbParameter("ModifyTime", DateTime.Now));
                                commandCompanyes.Parameters.Add(db.CreateDbParameter("ModifierId", userInfo.Id));
                                commandCompanyes.Parameters.Add(db.CreateDbParameter("ModifierName", userInfo.Name));
                                commandCompanyes.CommandText = sqlUpdateCompanys;
                                commandCompanyes.ExecuteNonQuery();
                                #endregion

                                #region //sqlInsertDealerServiceInfo
                                var sqlInsertDealerServiceInfo = db.GetInsertSql("DealerServiceExt", "Id", new[] { 
                              "RepairQualification","OwnerCompany","BuildTime","TrafficRestrictionsdescribe",
                              "MainBusinessAreas","AndBusinessAreas",
                              "DangerousRepairQualification","HasBranch","VehicleTravelRoute","VehicleUseSpeciality",
                              "VehicleDockingStation","EmployeeNumber","RepairingArea","ReceptionRoomArea","ParkingArea",
                              "PartWarehouseArea","Status"
                                 });
                                var tempDealerServiceExt = dbDealerServiceInfos.FirstOrDefault(r => r.Id == item.Id);
                                var temdbDealers = dbDealers.FirstOrDefault(r => r.Code == item.Code);
                                if(tempDealerServiceExt == null) {
                                    var commandDealerServiceInfos = db.CreateDbCommand(sqlInsertDealerServiceInfo, conn, ts);
                                    commandDealerServiceInfos.Parameters.Add(db.CreateDbParameter("Id", temdbDealers.Id));
                                    commandDealerServiceInfos.Parameters.Add(db.CreateDbParameter("RepairQualification", item.RepairQualification));
                                    commandDealerServiceInfos.Parameters.Add(db.CreateDbParameter("OwnerCompany", item.OwnerCompany));
                                    commandDealerServiceInfos.Parameters.Add(db.CreateDbParameter("BuildTime", item.BuildTimeStr == null ? null : DateTime.Parse(item.BuildTimeStr).ToString("yyyy/MM/dd hh:mm:ss")));
                                    commandDealerServiceInfos.Parameters.Add(db.CreateDbParameter("TrafficRestrictionsdescribe", item.TrafficRestrictionsdescribe));
                                    commandDealerServiceInfos.Parameters.Add(db.CreateDbParameter("MainBusinessAreas", item.MainBusinessAreas));
                                    commandDealerServiceInfos.Parameters.Add(db.CreateDbParameter("AndBusinessAreas", item.AndBusinessAreas));
                                    //commandDealerServiceInfos.Parameters.Add(db.CreateDbParameter("BrandScope", item.BrandScope));
                                    //commandDealerServiceInfos.Parameters.Add(db.CreateDbParameter("CompetitiveBrandScope", item.CompetitiveBrandScope));
                                    commandDealerServiceInfos.Parameters.Add(db.CreateDbParameter("DangerousRepairQualification", item.DangerousRepairQualification));
                                    commandDealerServiceInfos.Parameters.Add(db.CreateDbParameter("HasBranch", item.HasBranch));
                                    commandDealerServiceInfos.Parameters.Add(db.CreateDbParameter("VehicleTravelRoute", item.VehicleTravelRoute));
                                    commandDealerServiceInfos.Parameters.Add(db.CreateDbParameter("VehicleUseSpeciality", item.VehicleUseSpeciality));
                                    commandDealerServiceInfos.Parameters.Add(db.CreateDbParameter("VehicleDockingStation", item.VehicleDockingStation));
                                    commandDealerServiceInfos.Parameters.Add(db.CreateDbParameter("EmployeeNumber", item.EmployeeNumberStr));
                                    commandDealerServiceInfos.Parameters.Add(db.CreateDbParameter("RepairingArea", item.RepairingArea));
                                    commandDealerServiceInfos.Parameters.Add(db.CreateDbParameter("ReceptionRoomArea", item.ReceptionRoomArea));
                                    commandDealerServiceInfos.Parameters.Add(db.CreateDbParameter("ParkingArea", item.ParkingArea));
                                    commandDealerServiceInfos.Parameters.Add(db.CreateDbParameter("PartWarehouseArea", item.PartWarehouseArea));
                                    commandDealerServiceInfos.Parameters.Add(db.CreateDbParameter("Status", (int)DcsBaseDataStatus.有效));
                                    commandDealerServiceInfos.ExecuteNonQuery();
                                } else {
                                    List<string> fieldDealerServiceExts = new List<string>();
                                    string sqlUpdatefieldDealerServiceExts = null;
                                    fieldDealerServiceExts.Add("ModifyTime");
                                    fieldDealerServiceExts.Add("ModifierId");
                                    fieldDealerServiceExts.Add("ModifierName");
                                    var commandDealerServiceExts = db.CreateDbCommand(sqlUpdatefieldDealerServiceExts, conn, ts);

                                    if(!string.IsNullOrEmpty(item.RepairQualificationStr)) {
                                        fieldDealerServiceExts.Add("RepairQualification");
                                        commandDealerServiceExts.Parameters.Add(db.CreateDbParameter("RepairQualification", item.RepairQualification));
                                    }
                                    if(!string.IsNullOrEmpty(item.OwnerCompany)) {
                                        fieldDealerServiceExts.Add("OwnerCompany");
                                        commandDealerServiceExts.Parameters.Add(db.CreateDbParameter("OwnerCompany", item.OwnerCompany));
                                    }
                                    if(!string.IsNullOrEmpty(item.BuildTimeStr)) {
                                        fieldDealerServiceExts.Add("BuildTime");
                                        commandDealerServiceExts.Parameters.Add(db.CreateDbParameter("BuildTime", Convert.ToDateTime(item.BuildTimeStr)));
                                    }
                                    if(!string.IsNullOrEmpty(item.TrafficRestrictionsdescribe)) {
                                        fieldDealerServiceExts.Add("TrafficRestrictionsdescribe");
                                        commandDealerServiceExts.Parameters.Add(db.CreateDbParameter("TrafficRestrictionsdescribe", item.TrafficRestrictionsdescribe));
                                    }
                                    if(!string.IsNullOrEmpty(item.MainBusinessAreas)) {
                                        fieldDealerServiceExts.Add("MainBusinessAreas");
                                        commandDealerServiceExts.Parameters.Add(db.CreateDbParameter("MainBusinessAreas", item.MainBusinessAreas));
                                    }
                                    if(!string.IsNullOrEmpty(item.AndBusinessAreas)) {
                                        fieldDealerServiceExts.Add("AndBusinessAreas");
                                        commandDealerServiceExts.Parameters.Add(db.CreateDbParameter("AndBusinessAreas", item.AndBusinessAreas));
                                    }
                                    //if(!string.IsNullOrEmpty(item.BrandScope)) {
                                    //    fieldDealerServiceExts.Add("BrandScope");
                                    //    commandDealerServiceExts.Parameters.Add(db.CreateDbParameter("BrandScope", item.BrandScope));
                                    //}
                                    //if(!string.IsNullOrEmpty(item.CompetitiveBrandScope)) {
                                    //    fieldDealerServiceExts.Add("CompetitiveBrandScope");
                                    //    commandDealerServiceExts.Parameters.Add(db.CreateDbParameter("CompetitiveBrandScope", item.CompetitiveBrandScope));
                                    //}
                                    if(!string.IsNullOrEmpty(item.DangerousRepairQualificationStr)) {
                                        fieldDealerServiceExts.Add("DangerousRepairQualification");
                                        commandDealerServiceExts.Parameters.Add(db.CreateDbParameter("DangerousRepairQualification", item.DangerousRepairQualification));
                                    }
                                    if(!string.IsNullOrEmpty(item.HasBranchStr)) {
                                        fieldDealerServiceExts.Add("HasBranch");
                                        commandDealerServiceExts.Parameters.Add(db.CreateDbParameter("HasBranch", item.HasBranch));
                                    }
                                    if(!string.IsNullOrEmpty(item.VehicleTravelRouteStr)) {
                                        fieldDealerServiceExts.Add("VehicleTravelRoute");
                                        commandDealerServiceExts.Parameters.Add(db.CreateDbParameter("VehicleTravelRoute", item.VehicleTravelRoute));
                                    }
                                    if(!string.IsNullOrEmpty(item.VehicleUseSpecialityStr)) {
                                        fieldDealerServiceExts.Add("VehicleUseSpeciality");
                                        commandDealerServiceExts.Parameters.Add(db.CreateDbParameter("VehicleUseSpeciality", item.VehicleUseSpeciality));
                                    }
                                    //if(!string.IsNullOrEmpty(item.VehicleUseSpecialityStr)) {
                                    //fieldDealerServiceExts.Add("VehicleUseSpeciality");
                                    //commandDealerServiceExts.Parameters.Add(db.CreateDbParameter("VehicleUseSpeciality", item.VehicleUseSpeciality));
                                    //}
                                    if(!string.IsNullOrEmpty(item.VehicleDockingStationStr)) {
                                        fieldDealerServiceExts.Add("VehicleDockingStation");
                                        commandDealerServiceExts.Parameters.Add(db.CreateDbParameter("VehicleDockingStation", item.VehicleDockingStation));
                                    }
                                    if(!string.IsNullOrEmpty(item.EmployeeNumberStr)) {
                                        fieldDealerServiceExts.Add("EmployeeNumber");
                                        commandDealerServiceExts.Parameters.Add(db.CreateDbParameter("EmployeeNumber", Convert.ToInt32(item.EmployeeNumberStr)));
                                    }
                                    if(!string.IsNullOrEmpty(item.RepairingArea)) {
                                        fieldDealerServiceExts.Add("RepairingArea");
                                        commandDealerServiceExts.Parameters.Add(db.CreateDbParameter("RepairingArea", item.RepairingArea));
                                    }
                                    if(!string.IsNullOrEmpty(item.ReceptionRoomArea)) {
                                        fieldDealerServiceExts.Add("ReceptionRoomArea");
                                        commandDealerServiceExts.Parameters.Add(db.CreateDbParameter("ReceptionRoomArea", item.ReceptionRoomArea));
                                    }
                                    if(!string.IsNullOrEmpty(item.ParkingArea)) {
                                        fieldDealerServiceExts.Add("ParkingArea");
                                        commandDealerServiceExts.Parameters.Add(db.CreateDbParameter("ParkingArea", item.ParkingArea));
                                    }
                                    if(!string.IsNullOrEmpty(item.PartWarehouseArea)) {
                                        fieldDealerServiceExts.Add("PartWarehouseArea");
                                        commandDealerServiceExts.Parameters.Add(db.CreateDbParameter("PartWarehouseArea", item.PartWarehouseArea));
                                    }
                                    sqlUpdatefieldDealerServiceExts = db.GetUpdateSql("DealerServiceExt", fieldDealerServiceExts.ToArray(), new[] { "Id" });
                                    commandDealerServiceExts.Parameters.Add(db.CreateDbParameter("Id", item.Id));
                                    commandDealerServiceExts.Parameters.Add(db.CreateDbParameter("ModifyTime", DateTime.Now));
                                    commandDealerServiceExts.Parameters.Add(db.CreateDbParameter("ModifierId", userInfo.Id));
                                    commandDealerServiceExts.Parameters.Add(db.CreateDbParameter("ModifierName", userInfo.Name));
                                    commandDealerServiceExts.CommandText = sqlUpdatefieldDealerServiceExts;
                                    commandDealerServiceExts.ExecuteNonQuery();
                                }
                                #endregion

                                #region //sqlInsertCompanyInvoiceInfo
                                //TaxRegisteredNumber BankName BankAccount ContactNumber TaxRegisteredAddress
                                //防止新增全空的企业开票信息
                                if(!string.IsNullOrEmpty(item.TaxRegisteredNumber)
                                    || !string.IsNullOrEmpty(item.BankName)
                                    || !string.IsNullOrEmpty(item.BankAccount)
                                    || !string.IsNullOrEmpty(item.ContactNumber)
                                    || !string.IsNullOrEmpty(item.TaxRegisteredAddress)) {

                                    var sqlInsertCompanyInvoiceInfo = db.GetInsertSql("CompanyInvoiceInfo", "Id", new[] { 
                               "TaxRegisteredNumber","TaxpayerQualification","InvoiceAmountQuota","InvoiceTitle",
                               "InvoiceType","BankName","InvoiceTax","BankAccount","Linkman","ContactNumber",
                               "Fax","TaxRegisteredAddress","InvoiceCompanyId","CompanyId","CompanyCode","CompanyName","IsVehicleSalesInvoice",
                               "IsPartsSalesInvoice","Status","CreateTime","CreatorId","CreatorName"
                             });
                                    var tempobjcheckCompanyInvoiceInfo = dbPartsCompanyInvoiceInfoes.FirstOrDefault(r => r.Code == item.Code);
                                    var temdbCompanyInvoiceInfos = dbDealers.FirstOrDefault(r => r.Code == item.Code);
                                    if(tempobjcheckCompanyInvoiceInfo == null) {
                                        var commandCompanyInvoiceInfoes = db.CreateDbCommand(sqlInsertCompanyInvoiceInfo, conn, ts);
                                        commandCompanyInvoiceInfoes.Parameters.Add(db.CreateDbParameter("TaxRegisteredNumber", item.TaxRegisteredNumber));
                                        commandCompanyInvoiceInfoes.Parameters.Add(db.CreateDbParameter("TaxpayerQualification", item.TaxpayerQualification));
                                        commandCompanyInvoiceInfoes.Parameters.Add(db.CreateDbParameter("InvoiceAmountQuota", item.InvoiceAmountQuota));
                                        commandCompanyInvoiceInfoes.Parameters.Add(db.CreateDbParameter("InvoiceTitle", item.InvoiceTitle));
                                        commandCompanyInvoiceInfoes.Parameters.Add(db.CreateDbParameter("InvoiceType", item.InvoiceType));
                                        commandCompanyInvoiceInfoes.Parameters.Add(db.CreateDbParameter("BankName", item.BankName));
                                        commandCompanyInvoiceInfoes.Parameters.Add(db.CreateDbParameter("InvoiceTax", item.InvoiceTax));
                                        commandCompanyInvoiceInfoes.Parameters.Add(db.CreateDbParameter("BankAccount", item.BankAccount));
                                        commandCompanyInvoiceInfoes.Parameters.Add(db.CreateDbParameter("Linkman", item.Linkman));
                                        commandCompanyInvoiceInfoes.Parameters.Add(db.CreateDbParameter("ContactNumber", item.ContactNumber));
                                        commandCompanyInvoiceInfoes.Parameters.Add(db.CreateDbParameter("Fax", item.Fax));
                                        commandCompanyInvoiceInfoes.Parameters.Add(db.CreateDbParameter("TaxRegisteredAddress", item.TaxRegisteredAddress));
                                        commandCompanyInvoiceInfoes.Parameters.Add(db.CreateDbParameter("InvoiceCompanyId", userInfo.EnterpriseId));
                                        commandCompanyInvoiceInfoes.Parameters.Add(db.CreateDbParameter("CompanyId", temdbCompanyInvoiceInfos.Id));
                                        commandCompanyInvoiceInfoes.Parameters.Add(db.CreateDbParameter("CompanyCode", temdbCompanyInvoiceInfos.Code));
                                        commandCompanyInvoiceInfoes.Parameters.Add(db.CreateDbParameter("CompanyName", temdbCompanyInvoiceInfos.Name));
                                        commandCompanyInvoiceInfoes.Parameters.Add(db.CreateDbParameter("IsVehicleSalesInvoice", false));
                                        commandCompanyInvoiceInfoes.Parameters.Add(db.CreateDbParameter("IsPartsSalesInvoice", true));
                                        commandCompanyInvoiceInfoes.Parameters.Add(db.CreateDbParameter("Status", 1));
                                        commandCompanyInvoiceInfoes.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                        commandCompanyInvoiceInfoes.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                        commandCompanyInvoiceInfoes.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                        commandCompanyInvoiceInfoes.ExecuteNonQuery();
                                    } else {
                                        List<string> fieldCompanyInvoiceInfos = new List<string>();
                                        string sqlUpdateCompanyInvoiceInfos = null;
                                        fieldCompanyInvoiceInfos.Add("ModifyTime");
                                        fieldCompanyInvoiceInfos.Add("ModifierId");
                                        fieldCompanyInvoiceInfos.Add("ModifierName");
                                        var commandCompanyInvoiceInfos = db.CreateDbCommand(sqlUpdateCompanyInvoiceInfos, conn, ts);

                                        if(!string.IsNullOrEmpty(item.TaxRegisteredNumber)) {
                                            fieldCompanyInvoiceInfos.Add("TaxRegisteredNumber");
                                            commandCompanyInvoiceInfos.Parameters.Add(db.CreateDbParameter("TaxRegisteredNumber", item.TaxRegisteredNumber));
                                        }
                                        if(!string.IsNullOrEmpty(item.TaxpayerQualificationStr)) {
                                            fieldCompanyInvoiceInfos.Add("TaxpayerQualification");
                                            commandCompanyInvoiceInfos.Parameters.Add(db.CreateDbParameter("TaxpayerQualification", item.TaxpayerQualification));
                                        }
                                        if(!string.IsNullOrEmpty(item.InvoiceAmountQuota)) {
                                            fieldCompanyInvoiceInfos.Add("InvoiceAmountQuota");
                                            commandCompanyInvoiceInfos.Parameters.Add(db.CreateDbParameter("InvoiceAmountQuota", item.InvoiceAmountQuota));
                                        }
                                        if(!string.IsNullOrEmpty(item.InvoiceTitle)) {
                                            fieldCompanyInvoiceInfos.Add("InvoiceTitle");
                                            commandCompanyInvoiceInfos.Parameters.Add(db.CreateDbParameter("InvoiceTitle", item.InvoiceTitle));
                                        }
                                        if(!string.IsNullOrEmpty(item.InvoiceTypeStr)) {
                                            fieldCompanyInvoiceInfos.Add("InvoiceType");
                                            commandCompanyInvoiceInfos.Parameters.Add(db.CreateDbParameter("InvoiceType", item.InvoiceType));
                                        }
                                        if(!string.IsNullOrEmpty(item.BankName)) {
                                            fieldCompanyInvoiceInfos.Add("BankName");
                                            commandCompanyInvoiceInfos.Parameters.Add(db.CreateDbParameter("BankName", item.BankName));
                                        }
                                        if(!string.IsNullOrEmpty(item.InvoiceTax)) {
                                            fieldCompanyInvoiceInfos.Add("InvoiceTax");
                                            commandCompanyInvoiceInfos.Parameters.Add(db.CreateDbParameter("InvoiceTax", item.InvoiceTax));
                                        }
                                        if(!string.IsNullOrEmpty(item.BankAccount)) {
                                            fieldCompanyInvoiceInfos.Add("BankAccount");
                                            commandCompanyInvoiceInfos.Parameters.Add(db.CreateDbParameter("BankAccount", item.BankAccount));
                                        }
                                        if(!string.IsNullOrEmpty(item.Linkman)) {
                                            fieldCompanyInvoiceInfos.Add("Linkman");
                                            commandCompanyInvoiceInfos.Parameters.Add(db.CreateDbParameter("Linkman", item.Linkman));
                                        }
                                        if(!string.IsNullOrEmpty(item.ContactNumber)) {
                                            fieldCompanyInvoiceInfos.Add("ContactNumber");
                                            commandCompanyInvoiceInfos.Parameters.Add(db.CreateDbParameter("ContactNumber", item.ContactNumber));
                                        }
                                        if(!string.IsNullOrEmpty(item.AccountFax)) {
                                            fieldCompanyInvoiceInfos.Add("Fax");
                                            commandCompanyInvoiceInfos.Parameters.Add(db.CreateDbParameter("Fax", item.AccountFax));
                                        }
                                        if(!string.IsNullOrEmpty(item.TaxRegisteredAddress)) {
                                            fieldCompanyInvoiceInfos.Add("TaxRegisteredAddress");
                                            commandCompanyInvoiceInfos.Parameters.Add(db.CreateDbParameter("TaxRegisteredAddress", item.TaxRegisteredAddress));
                                        }
                                        sqlUpdateCompanyInvoiceInfos = db.GetUpdateSql("CompanyInvoiceInfo", fieldCompanyInvoiceInfos.ToArray(), new[] { "Id" });
                                        commandCompanyInvoiceInfos.Parameters.Add(db.CreateDbParameter("Id", tempobjcheckCompanyInvoiceInfo.CompanyInvoiceInfoId));
                                        commandCompanyInvoiceInfos.Parameters.Add(db.CreateDbParameter("ModifyTime", DateTime.Now));
                                        commandCompanyInvoiceInfos.Parameters.Add(db.CreateDbParameter("ModifierId", userInfo.Id));
                                        commandCompanyInvoiceInfos.Parameters.Add(db.CreateDbParameter("ModifierName", userInfo.Name));
                                        commandCompanyInvoiceInfos.CommandText = sqlUpdateCompanyInvoiceInfos;
                                        commandCompanyInvoiceInfos.ExecuteNonQuery();
                                    }
                                }
                                #endregion


                                #region //新增履历
                                var sqlInsertDealerHistory = db.GetInsertSql("DealerHistory", "Id", new[] { 
                            "Code","RecordId","AName","AShortName","AManager","ARepairQualification","AHasBranch","ADangerousRepairQualification","ABrandScope","ACompetitiveBrandScope","AParkingArea",
                            "ARepairingArea","AEmployeeNumber","APartWarehouseArea","AGeographicPosition","AOwnerCompany","AMainBusinessAreas","AAndBusinessAreas","ABuildTime",
                            "ATrafficRestrictionsdescribe","AManagerPhoneNumber","AManagerMobile","AManagerMail","AReceptionRoomArea","AIsVehicleSalesInvoice","AIsPartsSalesInvoice",
                            "ATaxRegisteredNumber",
                            "AInvoiceTitle","AInvoiceType","ATaxpayerQualification","AInvoiceAmountQuota","ATaxRegisteredAddress","ATaxRegisteredPhone",
                            "ABankName","ABankAccount","AInvoiceTax","ALinkman","AContactNumber","AFax",
                            "ACustomerCode","ASupplierCode","AFoundDate","ARegionId","AProvinceName","ACityName","ACountyName","ACityLevel",
                            "AContactPerson","AContactMobile","AContactPhone","AContactMail","ACompanyFax","AContactPostCode","ABusinessAddress",
                            "ARegisterCode","ARegisterName","ARegisterCapital","AFixedAsset","ARegisterDate","ACorporateNature","ALegalRepresentative",
                            "ALegalRepresentTel","AIdDocumentType","AIdDocumentNumber","ARegisteredAddress","ABusinessScope","ABusinessAddressLongitude","ABusinessAddressLatitude",
                            "AStatus","FilerName","FileTime","FilerId","CreatorName","CreateTime","CreatorId"
                            });
                                var commandDealerHistory = db.CreateDbCommand(sqlInsertDealerHistory, conn, ts);
                                commandDealerHistory.Parameters.Add(db.CreateDbParameter("Code", item.Code));
                                commandDealerHistory.Parameters.Add(db.CreateDbParameter("RecordId", item.Id));
                                commandDealerHistory.Parameters.Add(db.CreateDbParameter("AName", item.Name));
                                commandDealerHistory.Parameters.Add(db.CreateDbParameter("AShortName", item.ShortName));
                                commandDealerHistory.Parameters.Add(db.CreateDbParameter("AManager", item.Manager));
                                commandDealerHistory.Parameters.Add(db.CreateDbParameter("ARepairQualification", item.RepairQualification));
                                commandDealerHistory.Parameters.Add(db.CreateDbParameter("AHasBranch", item.HasBranch));
                                commandDealerHistory.Parameters.Add(db.CreateDbParameter("ADangerousRepairQualification", item.DangerousRepairQualification));
                                commandDealerHistory.Parameters.Add(db.CreateDbParameter("ABrandScope", item.BrandScope));
                                commandDealerHistory.Parameters.Add(db.CreateDbParameter("ACompetitiveBrandScope", item.CompetitiveBrandScope));
                                commandDealerHistory.Parameters.Add(db.CreateDbParameter("AParkingArea", item.ParkingArea));
                                commandDealerHistory.Parameters.Add(db.CreateDbParameter("ARepairingArea", item.RepairingArea));
                                commandDealerHistory.Parameters.Add(db.CreateDbParameter("AEmployeeNumber", Convert.ToInt32(item.EmployeeNumberStr)));
                                commandDealerHistory.Parameters.Add(db.CreateDbParameter("APartWarehouseArea", item.PartWarehouseArea));
                                commandDealerHistory.Parameters.Add(db.CreateDbParameter("AGeographicPosition", item.VehicleDockingStation));
                                commandDealerHistory.Parameters.Add(db.CreateDbParameter("AOwnerCompany", item.OwnerCompany));
                                commandDealerHistory.Parameters.Add(db.CreateDbParameter("AMainBusinessAreas", item.MainBusinessAreas));
                                commandDealerHistory.Parameters.Add(db.CreateDbParameter("AAndBusinessAreas", item.AndBusinessAreas));
                                commandDealerHistory.Parameters.Add(db.CreateDbParameter("ABuildTime", Convert.ToDateTime(item.BuildTimeStr)));
                                commandDealerHistory.Parameters.Add(db.CreateDbParameter("ATrafficRestrictionsdescribe", item.TrafficRestrictionsdescribe));
                                commandDealerHistory.Parameters.Add(db.CreateDbParameter("AManagerPhoneNumber", item.ContactPhone));
                                commandDealerHistory.Parameters.Add(db.CreateDbParameter("AManagerMobile", item.ContactMobile));
                                commandDealerHistory.Parameters.Add(db.CreateDbParameter("AManagerMail", item.ContactMail));
                                commandDealerHistory.Parameters.Add(db.CreateDbParameter("AReceptionRoomArea", item.ReceptionRoomArea));
                                commandDealerHistory.Parameters.Add(db.CreateDbParameter("AIsVehicleSalesInvoice", '0'));
                                commandDealerHistory.Parameters.Add(db.CreateDbParameter("AIsPartsSalesInvoice", '1'));
                                commandDealerHistory.Parameters.Add(db.CreateDbParameter("ATaxRegisteredNumber", item.TaxRegisteredNumber));
                                commandDealerHistory.Parameters.Add(db.CreateDbParameter("AInvoiceTitle", item.InvoiceTitle));
                                commandDealerHistory.Parameters.Add(db.CreateDbParameter("AInvoiceType", item.InvoiceType));
                                commandDealerHistory.Parameters.Add(db.CreateDbParameter("ATaxpayerQualification", item.TaxpayerQualification));
                                commandDealerHistory.Parameters.Add(db.CreateDbParameter("AInvoiceAmountQuota", item.InvoiceAmountQuota));
                                commandDealerHistory.Parameters.Add(db.CreateDbParameter("ATaxRegisteredAddress", item.TaxRegisteredAddress));
                                commandDealerHistory.Parameters.Add(db.CreateDbParameter("ATaxRegisteredPhone", item.ContactPhone));
                                commandDealerHistory.Parameters.Add(db.CreateDbParameter("ABankName", item.BankName));
                                commandDealerHistory.Parameters.Add(db.CreateDbParameter("ABankAccount", item.BankAccount));
                                commandDealerHistory.Parameters.Add(db.CreateDbParameter("AInvoiceTax", item.InvoiceTax));
                                commandDealerHistory.Parameters.Add(db.CreateDbParameter("ALinkman", item.Linkman));
                                commandDealerHistory.Parameters.Add(db.CreateDbParameter("AContactNumber", item.ContactNumber));
                                commandDealerHistory.Parameters.Add(db.CreateDbParameter("AFax", item.AccountFax));
                                commandDealerHistory.Parameters.Add(db.CreateDbParameter("ACustomerCode", item.CustomerCode));
                                commandDealerHistory.Parameters.Add(db.CreateDbParameter("ASupplierCode", item.SupplierCode));
                                commandDealerHistory.Parameters.Add(db.CreateDbParameter("AFoundDate", Convert.ToDateTime(item.FoundDateStr)));
                                commandDealerHistory.Parameters.Add(db.CreateDbParameter("ARegionId", item.RegionId));
                                commandDealerHistory.Parameters.Add(db.CreateDbParameter("AProvinceName", item.ProvinceName));
                                commandDealerHistory.Parameters.Add(db.CreateDbParameter("ACityName", item.CityName));
                                commandDealerHistory.Parameters.Add(db.CreateDbParameter("ACountyName", item.CountyName));
                                commandDealerHistory.Parameters.Add(db.CreateDbParameter("ACityLevel", item.CityLevel));
                                commandDealerHistory.Parameters.Add(db.CreateDbParameter("AContactPerson", item.ContactPerson));
                                commandDealerHistory.Parameters.Add(db.CreateDbParameter("AContactMobile", item.ContactMobile));
                                commandDealerHistory.Parameters.Add(db.CreateDbParameter("AContactPhone", item.ContactPhone));
                                commandDealerHistory.Parameters.Add(db.CreateDbParameter("AContactMail", item.ContactMail));
                                commandDealerHistory.Parameters.Add(db.CreateDbParameter("ACompanyFax", item.Fax));
                                commandDealerHistory.Parameters.Add(db.CreateDbParameter("AContactPostCode", item.ContactPostCode));
                                commandDealerHistory.Parameters.Add(db.CreateDbParameter("ABusinessAddress", item.BusinessAddress));
                                commandDealerHistory.Parameters.Add(db.CreateDbParameter("ARegisterCode", item.RegisterCode));
                                commandDealerHistory.Parameters.Add(db.CreateDbParameter("ARegisterName", item.RegisterName));
                                commandDealerHistory.Parameters.Add(db.CreateDbParameter("ARegisterCapital", item.RegisterCapital));
                                commandDealerHistory.Parameters.Add(db.CreateDbParameter("AFixedAsset", item.FixedAsset));
                                commandDealerHistory.Parameters.Add(db.CreateDbParameter("ARegisterDate", Convert.ToDateTime(item.RegisterDateStr)));
                                commandDealerHistory.Parameters.Add(db.CreateDbParameter("ACorporateNature", item.CorporateNature));
                                commandDealerHistory.Parameters.Add(db.CreateDbParameter("ALegalRepresentative", item.LegalRepresentative));
                                commandDealerHistory.Parameters.Add(db.CreateDbParameter("ALegalRepresentTel", item.LegalRepresentTel));
                                commandDealerHistory.Parameters.Add(db.CreateDbParameter("AIdDocumentType", item.IdDocumentType));
                                commandDealerHistory.Parameters.Add(db.CreateDbParameter("AIdDocumentNumber", item.IdDocumentNumber));
                                commandDealerHistory.Parameters.Add(db.CreateDbParameter("ARegisteredAddress", item.RegisteredAddress));
                                commandDealerHistory.Parameters.Add(db.CreateDbParameter("ABusinessScope", item.BusinessScope));
                                commandDealerHistory.Parameters.Add(db.CreateDbParameter("ABusinessAddressLongitude", item.BusinessAddressLongitude));
                                commandDealerHistory.Parameters.Add(db.CreateDbParameter("ABusinessAddressLatitude", item.BusinessAddressLatitude));
                                commandDealerHistory.Parameters.Add(db.CreateDbParameter("AStatus", (int)DcsBaseDataStatus.有效));
                                commandDealerHistory.Parameters.Add(db.CreateDbParameter("FilerName", userInfo.Name));
                                commandDealerHistory.Parameters.Add(db.CreateDbParameter("FileTime", DateTime.Now));
                                commandDealerHistory.Parameters.Add(db.CreateDbParameter("FilerId", userInfo.Id));
                                commandDealerHistory.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                commandDealerHistory.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                commandDealerHistory.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                commandDealerHistory.ExecuteNonQuery();
                                #endregion
                            }

                        }
                        //无异常提交
                        ts.Commit();
                    } catch(Exception) {
                        //报错回滚
                        ts.Rollback();
                        throw;
                    } finally {
                        if(conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }


        public bool 批量导入经销商基本信息(int branchId, string fileName, out int excelImportNum, out List<DealerExtend> rightData, out List<DealerExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<DealerExtend>();
            var rightList = new List<DealerExtend>();
            var allList = new List<DealerExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                //List<string> notNullableFilelds;
                //Dictionary<string, int> fieldLenght;
                //db.GetTableSchema("",out notNullableFields,out fieldLenght);

                List<object> excelColumns;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_DealerCode, "DealerCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_DealerName, "DealerName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Agency_Abbreviation, "ShortName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_Manager, "Manager");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_CustomerCode, "CustomerCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_AccountPay_SupplierCode, "SupplierCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Agency_FoundDate, "FoundDate");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Agency_Province, "ProvinceName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Agency_City, "CityName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_MarketingDepartment_CountyName, "CountyName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_CityLevel, "CityLevel");

                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Agency_LinkMan, "ContactPerson");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_ContactPhone, "ContactPhone");
                    excelOperator.AddColumnDataSource("E-MAIL", "ContactMail");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Agency_LinkManPhone, "ContactMobile");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Agency_Fax, "Fax");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_ContactPostCode, "ContactPostCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_BusinessAddress, "BusinessAddress");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_BusinessAddressLongitude, "BusinessAddressLongitude");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_BusinessAddressLatitude, "BusinessAddressLatitude");

                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Agency_RegistrationNo, "RegisterCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Agency_RegistrationName, "RegisterName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Agency_RegisterDate, "RegisterDate");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Agency_RegisterCapital, "RegisterCapital");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_FixedAsset, "FixedAsset");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Agency_LegalRepresentative, "LegalRepresentative");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_LegalRepresentTel, "LegalRepresentTel");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Agency_Nature, "CorporateNature");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Agency_BusinessScope, "BusinessScope");

                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Agency_RegisteredAddress, "RegisteredAddress");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Agency_IdDocumentType, "IdDocumentType");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Agency_IdDocumentNumber, "IdDocumentNumber");

                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_RepairQualification, "RepairQualification");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_OwnerCompany, "OwnerCompany");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_BuildTime, "BuildTime");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_TrafficRestrictionsdescribe, "TrafficRestrictionsdescribe");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_MainBusinessAreas, "MainBusinessAreas");

                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_AndBusinessAreas, "AndBusinessAreas");
                    //excelOperator.AddColumnDataSource("红岩授权品牌", "BrandScope");
                    //excelOperator.AddColumnDataSource("非红岩授权品牌", "CompetitiveBrandScope");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_DangerousRepairQualification, "DangerousRepairQualification");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_HasBranch, "HasBranch");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_VehicleTravelRoute, "VehicleTravelRoute");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_VehicleUseSpeciality, "VehicleUseSpeciality");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_VehicleDockingStation, "VehicleDockingStation");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_EmployeeNumber, "EmployeeNumber");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_RepairingArea, "RepairingArea");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_ReceptionRoomArea, "ReceptionRoomArea");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_ParkingArea, "ParkingArea");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_PartWarehouseArea, "PartWarehouseArea");

                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_TaxRegisteredNumber, "TaxRegisteredNumber");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_TaxpayerQualification, "TaxpayerQualification");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_InvoiceAmountQuota, "InvoiceAmountQuota");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_InvoiceTitle, "InvoiceTitle");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsInboundCheckBill_BillingType, "InvoiceType");

                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_BankName, "BankName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_InvoiceTax, "InvoiceTax");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_BankAccount, "BankAccount");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_Linkman, "Linkman");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_ContactNumber, "ContactNumber");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_AccountFax, "AccountFax");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_TaxRegisteredAddress, "TaxRegisteredAddress");

                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;

                    //单次最大导入条数不能大于300
                    if(excelOperator.LastRowNum > 300) {
                        throw new Exception(ErrorStrings.Export_Validation_Dealer_Validation1);
                    }

                    #region 获取对应枚举
                    var keyValuePairs = new[] {
                            new KeyValuePair<string, string>("RepairQualification", "Repair_Qualification_Grade"),
                            new KeyValuePair<string, string>("CorporateNature","Corporate_Nature"),
                            new KeyValuePair<string, string>("IdDocumentType","Customer_IdDocumentType"),
                            new KeyValuePair<string, string>("VehicleTravelRoute","DealerServiceExt_VehicleTravelRoute"),
                            new KeyValuePair<string, string>("VehicleUseSpeciality","DealerServiceExt_VehicleUseSpeciality"),
                            new KeyValuePair<string, string>("VehicleDockingStation","DealerServiceExt_VehicleDockingStations"),
                            new KeyValuePair<string, string>("CityLevel","Company_CityLevel"),
                            new KeyValuePair<string, string>("TaxpayerQualification","PartsSupplier_TaxpayerKind"),
                            new KeyValuePair<string, string>("InvoiceType","Invoice_Type")
                        };
                    tempExcelOperator.ImportHelper.LoadEnumFromDb(null, null, keyValuePairs);
                    #endregion

                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var tempImportObj = new DealerExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        #region
                        tempImportObj.Code = newRow["DealerCode"];
                        tempImportObj.Name = newRow["DealerName"];

                        tempImportObj.ShortName = newRow["ShortName"];
                        tempImportObj.Manager = newRow["Manager"];
                        tempImportObj.CustomerCode = newRow["CustomerCode"];
                        tempImportObj.SupplierCode = newRow["SupplierCode"];
                        tempImportObj.FoundDateStr = newRow["FoundDate"];
                        tempImportObj.ProvinceName = newRow["ProvinceName"];
                        tempImportObj.CityName = newRow["CityName"];
                        tempImportObj.CountyName = newRow["CountyName"];
                        tempImportObj.CityLevelStr = newRow["CityLevel"];

                        tempImportObj.ContactPerson = newRow["ContactPerson"];
                        tempImportObj.ContactMobile = newRow["ContactMobile"];
                        tempImportObj.ContactPhone = newRow["ContactPhone"];

                        tempImportObj.ContactMail = newRow["ContactMail"];
                        tempImportObj.Fax = newRow["Fax"];
                        tempImportObj.ContactPostCode = newRow["ContactPostCode"];
                        tempImportObj.BusinessAddress = newRow["BusinessAddress"];

                        tempImportObj.RegisterCode = newRow["RegisterCode"];
                        tempImportObj.RegisterName = newRow["RegisterName"];
                        tempImportObj.RegisterDateStr = newRow["RegisterDate"];
                        tempImportObj.FixedAsset = newRow["FixedAsset"];

                        tempImportObj.LegalRepresentative = newRow["LegalRepresentative"];
                        tempImportObj.RegisterCapital = newRow["RegisterCapital"];
                        tempImportObj.LegalRepresentTel = newRow["LegalRepresentTel"];
                        tempImportObj.CorporateNatureStr = newRow["CorporateNature"];
                        tempImportObj.BusinessScope = newRow["BusinessScope"];
                        tempImportObj.RegisteredAddress = newRow["RegisteredAddress"];
                        tempImportObj.IdDocumentTypeStr = newRow["IdDocumentType"];
                        tempImportObj.IdDocumentNumber = newRow["IdDocumentNumber"];

                        tempImportObj.RepairQualificationStr = newRow["RepairQualification"];

                        tempImportObj.OwnerCompany = newRow["OwnerCompany"];
                        tempImportObj.BuildTimeStr = newRow["BuildTime"];
                        tempImportObj.TrafficRestrictionsdescribe = newRow["TrafficRestrictionsdescribe"];
                        tempImportObj.MainBusinessAreas = newRow["MainBusinessAreas"];
                        tempImportObj.AndBusinessAreas = newRow["AndBusinessAreas"];
                        //tempImportObj.BrandScope = newRow["BrandScope"];
                        //tempImportObj.CompetitiveBrandScope = newRow["CompetitiveBrandScope"];
                        tempImportObj.DangerousRepairQualificationStr = newRow["DangerousRepairQualification"];
                        tempImportObj.HasBranchStr = newRow["HasBranch"];
                        tempImportObj.VehicleTravelRouteStr = newRow["VehicleTravelRoute"];
                        tempImportObj.VehicleUseSpecialityStr = newRow["VehicleUseSpeciality"];
                        tempImportObj.VehicleDockingStationStr = newRow["VehicleDockingStation"];
                        tempImportObj.EmployeeNumberStr = newRow["EmployeeNumber"];
                        tempImportObj.RepairingArea = newRow["RepairingArea"];
                        tempImportObj.ReceptionRoomArea = newRow["ReceptionRoomArea"];
                        tempImportObj.ParkingArea = newRow["ParkingArea"];
                        tempImportObj.PartWarehouseArea = newRow["PartWarehouseArea"];

                        tempImportObj.TaxRegisteredNumber = newRow["TaxRegisteredNumber"];

                        tempImportObj.TaxpayerQualificationStr = newRow["TaxpayerQualification"];
                        tempImportObj.InvoiceAmountQuota = newRow["InvoiceAmountQuota"];
                        tempImportObj.InvoiceTitle = newRow["InvoiceTitle"];
                        tempImportObj.InvoiceTypeStr = newRow["InvoiceType"];

                        tempImportObj.BankName = newRow["BankName"];
                        tempImportObj.InvoiceTax = newRow["InvoiceTax"];

                        tempImportObj.BankAccount = newRow["BankAccount"];
                        tempImportObj.Linkman = newRow["Linkman"];
                        tempImportObj.ContactNumber = newRow["ContactNumber"];
                        tempImportObj.AccountFax = newRow["AccountFax"];
                        tempImportObj.TaxRegisteredAddress = newRow["TaxRegisteredAddress"];

                        #endregion

                        var tempErrorMessage = new List<string>();

                        #region 导入的内容基本检查
                        //企业编号、企业名称、简称、MDM客户编码、省\市\县、固定电话、税务登记号、票名称、
                        //开票类型、银行名称、银行账号、财务联系电话、税务登记地址
                        //导入模板必填
                        if(string.IsNullOrEmpty(tempImportObj.Code)) {
                            tempErrorMessage.Add(ErrorStrings.Export_Validation_Dealer_Validation2);
                        }
                        if(string.IsNullOrEmpty(tempImportObj.Name)) {
                            tempErrorMessage.Add(ErrorStrings.Export_Validation_Dealer_Validation35);
                        }
                        if(string.IsNullOrEmpty(tempImportObj.ShortName)) {
                            tempErrorMessage.Add(ErrorStrings.Export_Validation_Dealer_Validation36);
                        }
                        if(string.IsNullOrEmpty(tempImportObj.CustomerCode)) {
                            tempErrorMessage.Add(ErrorStrings.Export_Validation_Dealer_Validation37);
                        }
                        if(string.IsNullOrEmpty(tempImportObj.ProvinceName)) {
                            tempErrorMessage.Add(ErrorStrings.Export_Validation_Dealer_Validation38);
                        }
                        if(string.IsNullOrEmpty(tempImportObj.CityName)) {
                            tempErrorMessage.Add(ErrorStrings.Export_Validation_Dealer_Validation39);
                        }
                        if(string.IsNullOrEmpty(tempImportObj.CountyName)) {
                            tempErrorMessage.Add(ErrorStrings.Export_Validation_Dealer_Validation40);
                        }
                        if(string.IsNullOrEmpty(tempImportObj.ContactPhone)) {
                            tempErrorMessage.Add(ErrorStrings.Export_Validation_Dealer_Validation41);
                        }
                        if(string.IsNullOrEmpty(tempImportObj.TaxRegisteredNumber)) {
                            tempErrorMessage.Add(ErrorStrings.Export_Validation_Dealer_Validation42);
                        }
                        if(string.IsNullOrEmpty(tempImportObj.InvoiceTitle)) {
                            tempErrorMessage.Add(ErrorStrings.Export_Validation_Dealer_Validation43);
                        }
                        if(string.IsNullOrEmpty(tempImportObj.InvoiceTypeStr)) {
                            tempErrorMessage.Add(ErrorStrings.Export_Validation_Dealer_Validation44);
                        }
                        if(string.IsNullOrEmpty(tempImportObj.BankName)) {
                            tempErrorMessage.Add(ErrorStrings.Export_Validation_Dealer_Validation45);
                        }
                        if(string.IsNullOrEmpty(tempImportObj.BankAccount)) {
                            tempErrorMessage.Add(ErrorStrings.Export_Validation_Dealer_Validation46);
                        }
                        if(string.IsNullOrEmpty(tempImportObj.ContactNumber)) {
                            tempErrorMessage.Add(ErrorStrings.Export_Validation_Dealer_Validation47);
                        }
                        if(string.IsNullOrEmpty(tempImportObj.TaxRegisteredAddress)) {
                            tempErrorMessage.Add(ErrorStrings.Export_Validation_Dealer_Validation48);
                        }

                        if(!string.IsNullOrEmpty(tempImportObj.FoundDateStr)) {
                            DateTime dt;
                            if(!DateTime.TryParse(tempImportObj.FoundDateStr, out dt))
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Dealer_Validation23);
                        }

                        if(!string.IsNullOrEmpty(tempImportObj.RegisterDateStr)) {
                            DateTime dt;
                            if(!DateTime.TryParse(tempImportObj.RegisterDateStr, out dt))
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Dealer_Validation24);
                        }

                        if(!string.IsNullOrEmpty(tempImportObj.FixedAsset)) {
                            if(Regex.IsMatch(tempImportObj.FixedAsset.ToString(), @"^(?:(?:0(?=(?:0?\.)|$))|[1-9])\d*(?:\.(?:(?:0(?=[1-9]))|[1-9])\d?)?$") == false)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Dealer_Validation26);
                        }

                        //if(!string.IsNullOrEmpty(tempImportObj.CorporateNature)) {
                        //    if(Regex.IsMatch(tempImportObj.CorporateNature.ToString(), "^[0-9]*[1-9][0-9]*$") == false)
                        //        tempErrorMessage.Add("企业性质应为数字");
                        //}

                        if(!string.IsNullOrEmpty(tempImportObj.BuildTimeStr)) {
                            DateTime dt;
                            if(!DateTime.TryParse(tempImportObj.BuildTimeStr, out dt))
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Dealer_Validation27);
                        }
                        if(!string.IsNullOrEmpty(tempImportObj.EmployeeNumberStr)) {
                            int checkValue;
                            if(!int.TryParse(tempImportObj.EmployeeNumberStr, out checkValue)) {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Dealer_Validation25);
                            }
                        }
                        if(!string.IsNullOrEmpty(tempImportObj.RepairingArea)) {
                            if(Regex.IsMatch(tempImportObj.RepairingArea.ToString(), @"^(?:(?:0(?=(?:0?\.)|$))|[1-9])\d*(?:\.(?:(?:0(?=[1-9]))|[1-9])\d?)?$") == false)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Dealer_Validation28);
                        }

                        if(!string.IsNullOrEmpty(tempImportObj.ReceptionRoomArea)) {
                            if(Regex.IsMatch(tempImportObj.ReceptionRoomArea.ToString(), @"^(?:(?:0(?=(?:0?\.)|$))|[1-9])\d*(?:\.(?:(?:0(?=[1-9]))|[1-9])\d?)?$") == false)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Dealer_Validation29);
                        }

                        if(!string.IsNullOrEmpty(tempImportObj.ParkingArea)) {
                            if(Regex.IsMatch(tempImportObj.ParkingArea.ToString(), @"^(?:(?:0(?=(?:0?\.)|$))|[1-9])\d*(?:\.(?:(?:0(?=[1-9]))|[1-9])\d?)?$") == false)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Dealer_Validation30);
                        }

                        if(!string.IsNullOrEmpty(tempImportObj.PartWarehouseArea)) {
                            if(Regex.IsMatch(tempImportObj.PartWarehouseArea.ToString(), @"^(?:(?:0(?=(?:0?\.)|$))|[1-9])\d*(?:\.(?:(?:0(?=[1-9]))|[1-9])\d?)?$") == false)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Dealer_Validation31);
                        }

                        if(!string.IsNullOrEmpty(tempImportObj.InvoiceAmountQuota)) {
                            if(Regex.IsMatch(tempImportObj.InvoiceAmountQuota.ToString(), @"^(?:(?:0(?=(?:0?\.)|$))|[1-9])\d*(?:\.(?:(?:0(?=[1-9]))|[1-9])\d?)?$") == false)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Dealer_Validation32);
                        }

                        if(!string.IsNullOrEmpty(tempImportObj.InvoiceTax)) {
                            if(Regex.IsMatch(tempImportObj.InvoiceTax.ToString(), @"^(?:(?:0(?=(?:0?\.)|$))|[1-9])\d*(?:\.(?:(?:0(?=[1-9]))|[1-9])\d?)?$") == false)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Dealer_Validation33);
                        }

                        //维修资质等级
                        if(!string.IsNullOrEmpty(tempImportObj.RepairQualificationStr)) {
                            //try {
                            //    tempImportObj.RepairQualification = (int)Enum.Parse(typeof(DcsRepairQualificationGrade), tempImportObj.RepairQualificationStr);
                            //} catch(Exception) {
                            //    tempErrorMessage.Add(ErrorStrings.Export_Validation_Dealer_Validation3);
                            //}
                            DcsRepairQualificationGrade repairQualificationGrade;
                            if(Enum.TryParse<DcsRepairQualificationGrade>(tempImportObj.RepairQualificationStr, out repairQualificationGrade) && Enum.IsDefined(typeof(DcsRepairQualificationGrade), repairQualificationGrade)) {
                                tempImportObj.RepairQualification = (int)repairQualificationGrade;
                            } else {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Dealer_Validation49);
                            }
                        }

                        //企业性质
                        if(!string.IsNullOrEmpty(tempImportObj.CorporateNatureStr)) {
                            DcsCorporateNature corporateNature;
                            if(Enum.TryParse<DcsCorporateNature>(tempImportObj.CorporateNatureStr, out corporateNature) && Enum.IsDefined(typeof(DcsCorporateNature), corporateNature)) {
                                tempImportObj.CorporateNature = (int)corporateNature;
                            } else {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Dealer_Validation5);
                            }
                        }

                        //身份证类型
                        if(!string.IsNullOrEmpty(tempImportObj.IdDocumentTypeStr)) {
                            //try {
                            //    tempImportObj.IdDocumentType = (int)Enum.Parse(typeof(DcsCustomerIdDocumentType), tempImportObj.IdDocumentTypeStr);
                            //} catch(Exception) {
                            //    tempErrorMessage.Add(ErrorStrings.Export_Validation_Dealer_Validation6);
                            //}
                            DcsCustomerIdDocumentType idDocumentType;
                            if(Enum.TryParse<DcsCustomerIdDocumentType>(tempImportObj.IdDocumentTypeStr, out idDocumentType) && Enum.IsDefined(typeof(DcsCustomerIdDocumentType), idDocumentType)) {
                                tempImportObj.IdDocumentType = (int)idDocumentType;
                            } else {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Dealer_Validation6);
                            }
                        }

                        //车辆行驶路线
                        if(!string.IsNullOrEmpty(tempImportObj.VehicleTravelRouteStr)) {
                            DcsDealerServiceExtVehicleTravelRoute travelRoute;
                            if(Enum.TryParse<DcsDealerServiceExtVehicleTravelRoute>(tempImportObj.VehicleTravelRouteStr, out travelRoute) && Enum.IsDefined(typeof(DcsDealerServiceExtVehicleTravelRoute), travelRoute)) {
                                tempImportObj.VehicleTravelRoute = (int)travelRoute;
                            } else {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Dealer_Validation9);
                            }
                        }

                        //车辆使用特性
                        if(!string.IsNullOrEmpty(tempImportObj.VehicleUseSpecialityStr)) {
                            DcsDealerServiceExtVehicleUseSpeciality useSpeciality;
                            if(Enum.TryParse<DcsDealerServiceExtVehicleUseSpeciality>(tempImportObj.VehicleUseSpecialityStr, out useSpeciality) && Enum.IsDefined(typeof(DcsDealerServiceExtVehicleUseSpeciality), useSpeciality)) {
                                tempImportObj.VehicleUseSpeciality = (int)useSpeciality;
                            } else {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Dealer_Validation10);
                            }
                        }

                        //车辆停靠点
                        if(!string.IsNullOrEmpty(tempImportObj.VehicleDockingStationStr)) {
                            DcsDealerServiceExtVehicleDockingStations dockingStation;
                            if(Enum.TryParse<DcsDealerServiceExtVehicleDockingStations>(tempImportObj.VehicleDockingStationStr, out dockingStation) && Enum.IsDefined(typeof(DcsDealerServiceExtVehicleDockingStations), dockingStation)) {
                                tempImportObj.VehicleDockingStation = (int)dockingStation;
                            } else {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Dealer_Validation11);
                            }
                        }

                        //城市级别
                        if(!string.IsNullOrEmpty(tempImportObj.CityLevelStr)) {
                            DcsCompanyCityLevel cityLevel;
                            if(Enum.TryParse<DcsCompanyCityLevel>(tempImportObj.CityLevelStr, out cityLevel) && Enum.IsDefined(typeof(DcsCompanyCityLevel), cityLevel)) {
                                tempImportObj.CityLevel = (int)cityLevel;
                            } else {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Dealer_Validation4);
                            }
                        }

                        //纳税人性质
                        if(!string.IsNullOrEmpty(tempImportObj.TaxpayerQualificationStr)) {
                            DcsPartsSupplierTaxpayerKind taxpayerKind;
                            if(Enum.TryParse<DcsPartsSupplierTaxpayerKind>(tempImportObj.TaxpayerQualificationStr, out taxpayerKind) && Enum.IsDefined(typeof(DcsPartsSupplierTaxpayerKind), taxpayerKind)) {
                                tempImportObj.TaxpayerQualification = (int)taxpayerKind;
                            } else {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Dealer_Validation7);
                            }
                        }
                        //开票类型
                        if(!string.IsNullOrEmpty(tempImportObj.InvoiceTypeStr)) {
                            DcsInvoiceType dcsInvoiceType;
                            if(Enum.TryParse<DcsInvoiceType>(tempImportObj.InvoiceTypeStr, out dcsInvoiceType) && Enum.IsDefined(typeof(DcsInvoiceType), dcsInvoiceType)) {
                                tempImportObj.InvoiceType = (int)dcsInvoiceType;
                            } else {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Dealer_Validation8);
                            }
                        }
                        //有无二级服务站
                        if(!string.IsNullOrEmpty(tempImportObj.HasBranchStr)) {
                            DcsIsOrNot dcsIsOrNot;
                            if(Enum.TryParse<DcsIsOrNot>(tempImportObj.HasBranchStr, out dcsIsOrNot) && Enum.IsDefined(typeof(DcsIsOrNot), dcsIsOrNot)) {
                                tempImportObj.HasBranch = (int)dcsIsOrNot;
                            } else {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Dealer_Validation12);
                            }
                        }
                        //危险品运输车辆维修许可证
                        if(!string.IsNullOrEmpty(tempImportObj.DangerousRepairQualificationStr)) {
                            DcsIsOrNot dcsIsOrNotDan;
                            if(Enum.TryParse<DcsIsOrNot>(tempImportObj.DangerousRepairQualificationStr, out dcsIsOrNotDan) && Enum.IsDefined(typeof(DcsIsOrNot), dcsIsOrNotDan)) {
                                tempImportObj.DangerousRepairQualification = (int)dcsIsOrNotDan;
                            } else {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Dealer_Validation13);
                            }
                        }

                        //字段长度校验
                        if(!string.IsNullOrEmpty(tempImportObj.Code)) {
                            if(Encoding.Default.GetByteCount(tempImportObj.Code) > 11) {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Dealer_Validation50);
                            }
                        }
                        if(!string.IsNullOrEmpty(tempImportObj.BusinessAddress)) {
                            if(Encoding.Default.GetByteCount(tempImportObj.BusinessAddress) > 200) {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Dealer_Validation16);
                            }
                        }
                        if(!string.IsNullOrEmpty(tempImportObj.BusinessScope)) {
                            if(Encoding.Default.GetByteCount(tempImportObj.BusinessScope) > 200) {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Dealer_Validation17);
                            }
                        }
                        if(!string.IsNullOrEmpty(tempImportObj.RegisteredAddress)) {
                            if(Encoding.Default.GetByteCount(tempImportObj.RegisteredAddress) > 200) {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Dealer_Validation18);
                            }
                        }
                        if(!string.IsNullOrEmpty(tempImportObj.MainBusinessAreas)) {
                            if(Encoding.Default.GetByteCount(tempImportObj.MainBusinessAreas) > 200) {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Dealer_Validation19);
                            }
                        }
                        if(!string.IsNullOrEmpty(tempImportObj.AndBusinessAreas)) {
                            if(Encoding.Default.GetByteCount(tempImportObj.AndBusinessAreas) > 200) {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Dealer_Validation20);
                            }
                        }

                        if(!string.IsNullOrEmpty(tempImportObj.RegisterName)) {
                            if(Encoding.Default.GetByteCount(tempImportObj.RegisterName) > 100) {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Dealer_Validation21);
                            }
                        }
                        if(!string.IsNullOrEmpty(tempImportObj.TrafficRestrictionsdescribe)) {
                            if(Encoding.Default.GetByteCount(tempImportObj.TrafficRestrictionsdescribe) > 100) {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Dealer_Validation22);
                            }
                        }

                        //if(!string.IsNullOrEmpty(tempImportObj.BrandScope)) {
                        //    if(Encoding.Default.GetByteCount(tempImportObj.BrandScope) > 50) {
                        //        tempErrorMessage.Add("红岩授权品牌长度过长");
                        //    }
                        //}
                        //if(!string.IsNullOrEmpty(tempImportObj.CompetitiveBrandScope)) {
                        //    if(Encoding.Default.GetByteCount(tempImportObj.CompetitiveBrandScope) > 50) {
                        //        tempErrorMessage.Add("非红岩授权品牌长度过长");
                        //    }
                        //}

                        #endregion

                        if(tempErrorMessage.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;

                    });

                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    var groups = tempRightList.GroupBy(r => new {
                        r.Code
                    }).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        groupItem.ErrorMsg = ErrorStrings.ImpPartsSupplierRelation_Validation31;
                    }

                    //根据服务站编号与现有的企业信息匹配，匹配上的报错：企业信息已经存在
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();
                    var dealerCodeNeedCheck = tempRightList.Select(r => r.Code).Distinct().ToArray();
                    var dbDealers = new List<DealerExtend>();
                    Func<string[], bool> getDbDealers = value => {
                        var dbObj = new DealerExtend {
                            Id = Convert.ToInt32(value[0]),
                            Code = value[1],
                            Name = value[2]
                        };
                        dbDealers.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select Id,Code,Name from company where status = 1", "Code", true, dealerCodeNeedCheck, getDbDealers);
                    foreach(var tempObj in tempRightList) {
                        var tempobjcheck = dbDealers.FirstOrDefault(r => r.Code == tempObj.Code);
                        if(tempobjcheck != null) {
                            tempObj.ErrorMsg = tempObj.Code + ErrorStrings.Export_Validation_Dealer_Validation51;
                        }
                    }

                    //省市县匹配校验
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();

                    var dbRegions = new List<RegionExtend>();
                    Func<string[], bool> getDbRegionNames = value => {
                        var dbObj = new RegionExtend {
                            Id = Convert.ToInt32(value[0]),
                            ProvinceName = value[1],
                            CityName = value[2],
                            CountyName = value[3]
                        };
                        dbRegions.Add(dbObj);
                        return false;
                    };

                    var reg = new List<string>();
                    reg.Add("1");
                    db.QueryDataWithInOperator("select Id,ProvinceName,CityName,CountyName from tiledregion ", "1", false, reg.ToArray(), getDbRegionNames);


                    foreach(var tempObj in tempRightList) {
                        var regionid = dbRegions.Where(r => r.ProvinceName == tempObj.ProvinceName && r.CityName == tempObj.CityName && r.CountyName == tempObj.CountyName).FirstOrDefault();
                        if(regionid != null) {
                            tempObj.RegionId = regionid.Id; //command.Parameters.Add(db.CreateDbParameter("RegionId", regionid.Id));
                        } else {
                            tempObj.ErrorMsg = ErrorStrings.Export_Validation_Dealer_Validation34;
                            //continue;
                        }
                    }

                    //----------------------------------


                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    rightList = allList.Except(errorList).ToList();

                    //导出所有不合格数据
                    if(errorList.Any()) {
                        excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                        errorDataFileName = GetErrorFilePath(fileName);
                        using(var excelExport = new ExcelExport(errorDataFileName)) {
                            var list = errorList;
                            excelExport.ExportByRow(index => {
                                if(index == list.Count + 1)
                                    return null;
                                if(index == 0)
                                    return excelColumns.ToArray();
                                var tempObj = list[index - 1];
                                var values = new object[] {
                                tempObj.Code, 
                                tempObj.Name,
                                tempObj.ShortName,
                                tempObj.Manager,
                                tempObj.CustomerCode,
                                tempObj.SupplierCode,
                                tempObj.FoundDate,
                                tempObj.ProvinceName,
                                tempObj.CityName,
                                tempObj.CountyName,
                                tempObj.CityLevelStr,
                                tempObj.ContactPerson,
                               
                                tempObj.ContactPhone,

                                tempObj.ContactMail,
                                 tempObj.ContactMobile,
                                tempObj.Fax,
                                tempObj.ContactPostCode,
                                tempObj.BusinessAddress,
                                 tempObj.BusinessAddressLongitude,
                                tempObj.BusinessAddressLatitude,

                                tempObj.RegisterCode ,
                                tempObj.RegisterName,
                                tempObj.RegisterDateStr,
                                 tempObj.RegisterCapital,
                                tempObj.FixedAsset,

                                tempObj.LegalRepresentative ,
                               
                                tempObj.LegalRepresentTel,
                                tempObj.CorporateNature,
                                tempObj.BusinessScope,
                               


                                tempObj.RegisteredAddress,
                                tempObj.IdDocumentTypeStr,
                                tempObj.IdDocumentNumber,

                                tempObj.RepairQualificationStr,

                                tempObj.OwnerCompany,
                                tempObj.BuildTimeStr,
                                tempObj.TrafficRestrictionsdescribe,
                                tempObj.MainBusinessAreas,
                                tempObj.AndBusinessAreas,
                                //tempObj.BrandScope,
                                //tempObj.CompetitiveBrandScope,
                                tempObj.DangerousRepairQualificationStr,
                                tempObj.HasBranchStr,
                                tempObj.VehicleTravelRouteStr,
                                tempObj.VehicleUseSpecialityStr,
                                tempObj.VehicleDockingStationStr,
                                tempObj.EmployeeNumberStr,
                                tempObj.RepairingArea,
                                tempObj.ReceptionRoomArea,
                                tempObj.ParkingArea,
                                tempObj.PartWarehouseArea,

                                tempObj.TaxRegisteredNumber, 
                                tempObj.TaxpayerQualificationStr,
                                tempObj.InvoiceAmountQuota,
                                tempObj.InvoiceTitle,
                                tempObj.InvoiceTypeStr,

                                tempObj.BankName,
                                tempObj.InvoiceTax,

                                tempObj.BankAccount,
                                tempObj.Linkman,

                                tempObj.ContactNumber,
                                tempObj.AccountFax,
                                tempObj.TaxRegisteredAddress,
                                tempObj.ErrorMsg
                            };
                                return values;
                            });
                        }
                        errorList = null;
                    }

                    //导入所有合格数据
                    if(!rightList.Any())
                        return true;
                    using(var conn = db.CreateDbConnection()) {
                        conn.Open();
                        //开启事务
                        var ts = conn.BeginTransaction();
                        try {
                            if(rightList.Any()) {
                                var userInfo = Utils.GetCurrentUserInfo();

                                //获取新增数据的sql语句
                                var sqlComanyInsert = db.GetInsertSql("Company", "Id", new[]{
                            "Code","Type","Name","ShortName","RegionId","CustomerCode","SupplierCode","FoundDate","ProvinceName","CityName","CountyName","CityLevel",
                            "ContactPerson","ContactPhone","ContactMail","ContactMobile","Fax","ContactPostCode","BusinessAddress",
                            "RegisterCode","RegisterName","RegisterDate","RegisterCapital","FixedAsset","LegalRepresentative","LegalRepresentTel","CorporateNature",
                            "BusinessScope","BusinessAddressLongitude","BusinessAddressLatitude","RegisteredAddress","IdDocumentType","IdDocumentNumber","Status","CreateTime","CreatorId","CreatorName"
                            });

                                var sqlDealerServiceExtInsert = db.GetInsertSql("DealerServiceExt", "Id", new[] {
                            "RepairQualification","OwnerCompany","BuildTime","TrafficRestrictionsdescribe","MainBusinessAreas","AndBusinessAreas",
                            "DangerousRepairQualification","HasBranch","VehicleTravelRoute","VehicleUseSpeciality",
                            "VehicleDockingStation","EmployeeNumber","RepairingArea","ReceptionRoomArea","ParkingArea","PartWarehouseArea",
                            "Status","CreateTime","CreatorId","CreatorName"
                            });

                                var sqlDealerInsert = db.GetInsertSql("Dealer", "Id", new[] {
                            "Code","Name","IsVirtualDealer","ShortName","Manager","Status","CreateTime","CreatorId","CreatorName"
                            });

                                var sqlCompanyInvoiceInfoInsert = db.GetInsertSql("CompanyInvoiceInfo", "Id", new[] { 
                            "InvoiceCompanyId","CompanyId","CompanyCode","CompanyName","IsVehicleSalesInvoice","IsPartsSalesInvoice","TaxRegisteredNumber","TaxpayerQualification","InvoiceAmountQuota","InvoiceTitle","InvoiceType","BankName","InvoiceTax","BankAccount","Linkman","ContactNumber","Fax",
                            "TaxRegisteredAddress","Status","CreateTime","CreatorId","CreatorName"
                            });

                                //新增履历
                                var sqlInsertDealerHistory = db.GetInsertSql("DealerHistory", "Id", new[] { 
                            "Code","RecordId","AName","AShortName","AManager","ARepairQualification","AHasBranch","ADangerousRepairQualification","ABrandScope","ACompetitiveBrandScope","AParkingArea",
                            "ARepairingArea","AEmployeeNumber","APartWarehouseArea","AGeographicPosition","AOwnerCompany","AMainBusinessAreas","AAndBusinessAreas","ABuildTime",
                            "ATrafficRestrictionsdescribe","AManagerPhoneNumber","AManagerMobile","AManagerMail","AReceptionRoomArea","AIsVehicleSalesInvoice","AIsPartsSalesInvoice",
                            "ATaxRegisteredNumber",
                            "AInvoiceTitle","AInvoiceType","ATaxpayerQualification","AInvoiceAmountQuota","ATaxRegisteredAddress","ATaxRegisteredPhone",
                            "ABankName","ABankAccount","AInvoiceTax","ALinkman","AContactNumber","AFax",
                            "ACustomerCode","ASupplierCode","AFoundDate","ARegionId","AProvinceName","ACityName","ACountyName","ACityLevel",
                            "AContactPerson","AContactMobile","AContactPhone","AContactMail","ACompanyFax","AContactPostCode","ABusinessAddress",
                            "ARegisterCode","ARegisterName","ARegisterCapital","AFixedAsset","ARegisterDate","ACorporateNature","ALegalRepresentative",
                            "ALegalRepresentTel","AIdDocumentType","AIdDocumentNumber","ARegisteredAddress","ABusinessScope","ABusinessAddressLongitude","ABusinessAddressLatitude",
                            "AStatus","FilerName","FileTime","FilerId","CreatorName","CreateTime","CreatorId"
                            });

                                //增加企业信息
                                foreach(var item in rightList) {
                                    var command = db.CreateDbCommand(sqlComanyInsert, conn, ts);
                                    command.Parameters.Add(db.CreateDbParameter("Code", item.Code));
                                    command.Parameters.Add(db.CreateDbParameter("Type", (int)DcsCompanyType.服务站));
                                    command.Parameters.Add(db.CreateDbParameter("Name", item.Name));
                                    command.Parameters.Add(db.CreateDbParameter("ShortName", item.ShortName));

                                    command.Parameters.Add(db.CreateDbParameter("RegionId", item.RegionId));

                                    command.Parameters.Add(db.CreateDbParameter("CustomerCode", item.CustomerCode));
                                    command.Parameters.Add(db.CreateDbParameter("SupplierCode", item.SupplierCode));
                                    command.Parameters.Add(db.CreateDbParameter("FoundDate", Convert.ToDateTime(item.FoundDateStr)));
                                    command.Parameters.Add(db.CreateDbParameter("ProvinceName", item.ProvinceName));
                                    command.Parameters.Add(db.CreateDbParameter("CityName", item.CityName));
                                    command.Parameters.Add(db.CreateDbParameter("CountyName", item.CountyName));
                                    command.Parameters.Add(db.CreateDbParameter("CityLevel", item.CityLevel));
                                    command.Parameters.Add(db.CreateDbParameter("ContactPerson", item.ContactPerson));
                                    command.Parameters.Add(db.CreateDbParameter("ContactPhone", item.ContactPhone));
                                    command.Parameters.Add(db.CreateDbParameter("ContactMail", item.ContactMail));
                                    command.Parameters.Add(db.CreateDbParameter("ContactMobile", item.ContactMobile));
                                    command.Parameters.Add(db.CreateDbParameter("Fax", item.Fax));
                                    command.Parameters.Add(db.CreateDbParameter("ContactPostCode", item.ContactPostCode));
                                    command.Parameters.Add(db.CreateDbParameter("BusinessAddress", item.BusinessAddress));
                                    command.Parameters.Add(db.CreateDbParameter("RegisterCode", item.RegisterCode));
                                    command.Parameters.Add(db.CreateDbParameter("RegisterName", item.RegisterName));
                                    command.Parameters.Add(db.CreateDbParameter("RegisterDate", Convert.ToDateTime(item.RegisterDateStr)));
                                    command.Parameters.Add(db.CreateDbParameter("RegisterCapital", item.RegisterCapital));
                                    command.Parameters.Add(db.CreateDbParameter("FixedAsset", item.FixedAsset));
                                    command.Parameters.Add(db.CreateDbParameter("LegalRepresentative", item.LegalRepresentative));
                                    command.Parameters.Add(db.CreateDbParameter("LegalRepresentTel", item.LegalRepresentTel));
                                    command.Parameters.Add(db.CreateDbParameter("CorporateNature", item.CorporateNature));
                                    command.Parameters.Add(db.CreateDbParameter("BusinessScope", item.BusinessScope));
                                    command.Parameters.Add(db.CreateDbParameter("BusinessAddressLongitude", item.BusinessAddressLongitude));
                                    command.Parameters.Add(db.CreateDbParameter("BusinessAddressLatitude", item.BusinessAddressLatitude));
                                    command.Parameters.Add(db.CreateDbParameter("RegisteredAddress", item.RegisteredAddress));
                                    command.Parameters.Add(db.CreateDbParameter("IdDocumentType", item.IdDocumentType));
                                    command.Parameters.Add(db.CreateDbParameter("IdDocumentNumber", item.IdDocumentNumber));
                                    command.Parameters.Add(db.CreateDbParameter("Status", (int)DcsMasterDataStatus.有效));
                                    command.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                    command.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                    command.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                    var tempId = db.ExecuteInsert(command, "Id");

                                    //增加经销商服务扩展信息
                                    var commandDealerServiceExt = db.CreateDbCommand(sqlDealerServiceExtInsert, conn, ts);
                                    commandDealerServiceExt.Parameters.Add(db.CreateDbParameter("Id", tempId));
                                    commandDealerServiceExt.Parameters.Add(db.CreateDbParameter("RepairQualification", item.RepairQualification));
                                    commandDealerServiceExt.Parameters.Add(db.CreateDbParameter("OwnerCompany", item.OwnerCompany));
                                    commandDealerServiceExt.Parameters.Add(db.CreateDbParameter("BuildTime", Convert.ToDateTime(item.BuildTimeStr)));
                                    commandDealerServiceExt.Parameters.Add(db.CreateDbParameter("TrafficRestrictionsdescribe", item.TrafficRestrictionsdescribe));
                                    commandDealerServiceExt.Parameters.Add(db.CreateDbParameter("MainBusinessAreas", item.MainBusinessAreas));
                                    commandDealerServiceExt.Parameters.Add(db.CreateDbParameter("AndBusinessAreas", item.AndBusinessAreas));
                                    //commandDealerServiceExt.Parameters.Add(db.CreateDbParameter("BrandScope", item.BrandScope));
                                    //commandDealerServiceExt.Parameters.Add(db.CreateDbParameter("CompetitiveBrandScope", item.CompetitiveBrandScope));
                                    commandDealerServiceExt.Parameters.Add(db.CreateDbParameter("DangerousRepairQualification", item.DangerousRepairQualification));
                                    commandDealerServiceExt.Parameters.Add(db.CreateDbParameter("HasBranch", item.HasBranch));
                                    commandDealerServiceExt.Parameters.Add(db.CreateDbParameter("VehicleTravelRoute", item.VehicleTravelRoute));
                                    commandDealerServiceExt.Parameters.Add(db.CreateDbParameter("VehicleUseSpeciality", item.VehicleUseSpeciality));
                                    commandDealerServiceExt.Parameters.Add(db.CreateDbParameter("VehicleDockingStation", item.VehicleDockingStation));
                                    commandDealerServiceExt.Parameters.Add(db.CreateDbParameter("EmployeeNumber", Convert.ToInt32(item.EmployeeNumberStr)));
                                    commandDealerServiceExt.Parameters.Add(db.CreateDbParameter("RepairingArea", item.RepairingArea));
                                    commandDealerServiceExt.Parameters.Add(db.CreateDbParameter("ReceptionRoomArea", item.ReceptionRoomArea));
                                    commandDealerServiceExt.Parameters.Add(db.CreateDbParameter("ParkingArea", item.ParkingArea));
                                    commandDealerServiceExt.Parameters.Add(db.CreateDbParameter("PartWarehouseArea", item.PartWarehouseArea));
                                    commandDealerServiceExt.Parameters.Add(db.CreateDbParameter("Status", (int)DcsBaseDataStatus.有效));
                                    commandDealerServiceExt.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                    commandDealerServiceExt.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                    commandDealerServiceExt.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                    commandDealerServiceExt.ExecuteNonQuery();

                                    //增加经销商基本信息
                                    var commandDealer = db.CreateDbCommand(sqlDealerInsert, conn, ts);
                                    commandDealer.Parameters.Add(db.CreateDbParameter("Id", tempId));
                                    commandDealer.Parameters.Add(db.CreateDbParameter("Code", item.Code));
                                    commandDealer.Parameters.Add(db.CreateDbParameter("Name", item.Name));
                                    commandDealer.Parameters.Add(db.CreateDbParameter("IsVirtualDealer", '0'));
                                    commandDealer.Parameters.Add(db.CreateDbParameter("ShortName", item.ShortName));
                                    commandDealer.Parameters.Add(db.CreateDbParameter("Manager", item.ContactPerson));
                                    commandDealer.Parameters.Add(db.CreateDbParameter("Status", (int)DcsBaseDataStatus.有效));
                                    commandDealer.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                    commandDealer.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                    commandDealer.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                    commandDealer.ExecuteNonQuery();

                                    //增加企业开票信息
                                    var commandCompanyInvoiceInfo = db.CreateDbCommand(sqlCompanyInvoiceInfoInsert, conn, ts);
                                    commandCompanyInvoiceInfo.Parameters.Add(db.CreateDbParameter("InvoiceCompanyId", userInfo.EnterpriseId));
                                    commandCompanyInvoiceInfo.Parameters.Add(db.CreateDbParameter("CompanyId", tempId));
                                    commandCompanyInvoiceInfo.Parameters.Add(db.CreateDbParameter("CompanyCode", item.Code));
                                    commandCompanyInvoiceInfo.Parameters.Add(db.CreateDbParameter("CompanyName", item.Name));
                                    commandCompanyInvoiceInfo.Parameters.Add(db.CreateDbParameter("IsVehicleSalesInvoice", '0'));
                                    commandCompanyInvoiceInfo.Parameters.Add(db.CreateDbParameter("IsPartsSalesInvoice", '1'));

                                    commandCompanyInvoiceInfo.Parameters.Add(db.CreateDbParameter("TaxRegisteredNumber", item.TaxRegisteredNumber));
                                    commandCompanyInvoiceInfo.Parameters.Add(db.CreateDbParameter("TaxpayerQualification", item.TaxpayerQualification));
                                    commandCompanyInvoiceInfo.Parameters.Add(db.CreateDbParameter("InvoiceAmountQuota", item.InvoiceAmountQuota));
                                    commandCompanyInvoiceInfo.Parameters.Add(db.CreateDbParameter("InvoiceTitle", item.InvoiceTitle));
                                    commandCompanyInvoiceInfo.Parameters.Add(db.CreateDbParameter("InvoiceType", item.InvoiceType));
                                    commandCompanyInvoiceInfo.Parameters.Add(db.CreateDbParameter("BankName", item.BankName));
                                    commandCompanyInvoiceInfo.Parameters.Add(db.CreateDbParameter("InvoiceTax", item.InvoiceTax));
                                    commandCompanyInvoiceInfo.Parameters.Add(db.CreateDbParameter("BankAccount", item.BankAccount));
                                    commandCompanyInvoiceInfo.Parameters.Add(db.CreateDbParameter("Linkman", item.Linkman));
                                    commandCompanyInvoiceInfo.Parameters.Add(db.CreateDbParameter("ContactNumber", item.ContactNumber));
                                    commandCompanyInvoiceInfo.Parameters.Add(db.CreateDbParameter("Fax", item.AccountFax));
                                    commandCompanyInvoiceInfo.Parameters.Add(db.CreateDbParameter("TaxRegisteredAddress", item.TaxRegisteredAddress));
                                    commandCompanyInvoiceInfo.Parameters.Add(db.CreateDbParameter("Status", (int)DcsBaseDataStatus.有效));
                                    commandCompanyInvoiceInfo.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                    commandCompanyInvoiceInfo.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                    commandCompanyInvoiceInfo.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                    commandCompanyInvoiceInfo.ExecuteNonQuery();

                                    //新增履历
                                    var commandDealerHistory = db.CreateDbCommand(sqlInsertDealerHistory, conn, ts);

                                    commandDealerHistory.Parameters.Add(db.CreateDbParameter("Code", item.Code));
                                    commandDealerHistory.Parameters.Add(db.CreateDbParameter("RecordId", tempId));
                                    commandDealerHistory.Parameters.Add(db.CreateDbParameter("AName", item.Name));
                                    commandDealerHistory.Parameters.Add(db.CreateDbParameter("AShortName", item.ShortName));
                                    commandDealerHistory.Parameters.Add(db.CreateDbParameter("AManager", item.Manager));
                                    commandDealerHistory.Parameters.Add(db.CreateDbParameter("ARepairQualification", item.RepairQualification));
                                    commandDealerHistory.Parameters.Add(db.CreateDbParameter("AHasBranch", item.HasBranch));
                                    commandDealerHistory.Parameters.Add(db.CreateDbParameter("ADangerousRepairQualification", item.DangerousRepairQualification));
                                    commandDealerHistory.Parameters.Add(db.CreateDbParameter("ABrandScope", item.BrandScope));
                                    commandDealerHistory.Parameters.Add(db.CreateDbParameter("ACompetitiveBrandScope", item.CompetitiveBrandScope));
                                    commandDealerHistory.Parameters.Add(db.CreateDbParameter("AParkingArea", item.ParkingArea));
                                    commandDealerHistory.Parameters.Add(db.CreateDbParameter("ARepairingArea", item.RepairingArea));
                                    commandDealerHistory.Parameters.Add(db.CreateDbParameter("AEmployeeNumber", Convert.ToInt32(item.EmployeeNumberStr)));
                                    commandDealerHistory.Parameters.Add(db.CreateDbParameter("APartWarehouseArea", item.PartWarehouseArea));
                                    commandDealerHistory.Parameters.Add(db.CreateDbParameter("AGeographicPosition", item.VehicleDockingStation));
                                    commandDealerHistory.Parameters.Add(db.CreateDbParameter("AOwnerCompany", item.OwnerCompany));
                                    commandDealerHistory.Parameters.Add(db.CreateDbParameter("AMainBusinessAreas", item.MainBusinessAreas));
                                    commandDealerHistory.Parameters.Add(db.CreateDbParameter("AAndBusinessAreas", item.AndBusinessAreas));
                                    commandDealerHistory.Parameters.Add(db.CreateDbParameter("ABuildTime", Convert.ToDateTime(item.BuildTimeStr)));
                                    commandDealerHistory.Parameters.Add(db.CreateDbParameter("ATrafficRestrictionsdescribe", item.TrafficRestrictionsdescribe));
                                    commandDealerHistory.Parameters.Add(db.CreateDbParameter("AManagerPhoneNumber", item.ContactPhone));
                                    commandDealerHistory.Parameters.Add(db.CreateDbParameter("AManagerMobile", item.ContactMobile));
                                    commandDealerHistory.Parameters.Add(db.CreateDbParameter("AManagerMail", item.ContactMail));
                                    commandDealerHistory.Parameters.Add(db.CreateDbParameter("AReceptionRoomArea", item.ReceptionRoomArea));
                                    commandDealerHistory.Parameters.Add(db.CreateDbParameter("AIsVehicleSalesInvoice", '0'));
                                    commandDealerHistory.Parameters.Add(db.CreateDbParameter("AIsPartsSalesInvoice", '1'));
                                    commandDealerHistory.Parameters.Add(db.CreateDbParameter("ATaxRegisteredNumber", item.TaxRegisteredNumber));
                                    commandDealerHistory.Parameters.Add(db.CreateDbParameter("AInvoiceTitle", item.InvoiceTitle));
                                    commandDealerHistory.Parameters.Add(db.CreateDbParameter("AInvoiceType", item.InvoiceType));
                                    commandDealerHistory.Parameters.Add(db.CreateDbParameter("ATaxpayerQualification", item.TaxpayerQualification));
                                    commandDealerHistory.Parameters.Add(db.CreateDbParameter("AInvoiceAmountQuota", item.InvoiceAmountQuota));
                                    commandDealerHistory.Parameters.Add(db.CreateDbParameter("ATaxRegisteredAddress", item.TaxRegisteredAddress));
                                    commandDealerHistory.Parameters.Add(db.CreateDbParameter("ATaxRegisteredPhone", item.ContactPhone));
                                    commandDealerHistory.Parameters.Add(db.CreateDbParameter("ABankName", item.BankName));
                                    commandDealerHistory.Parameters.Add(db.CreateDbParameter("ABankAccount", item.BankAccount));
                                    commandDealerHistory.Parameters.Add(db.CreateDbParameter("AInvoiceTax", item.InvoiceTax));
                                    commandDealerHistory.Parameters.Add(db.CreateDbParameter("ALinkman", item.Linkman));
                                    commandDealerHistory.Parameters.Add(db.CreateDbParameter("AContactNumber", item.ContactNumber));
                                    commandDealerHistory.Parameters.Add(db.CreateDbParameter("AFax", item.AccountFax));
                                    commandDealerHistory.Parameters.Add(db.CreateDbParameter("ACustomerCode", item.CustomerCode));
                                    commandDealerHistory.Parameters.Add(db.CreateDbParameter("ASupplierCode", item.SupplierCode));
                                    commandDealerHistory.Parameters.Add(db.CreateDbParameter("AFoundDate", Convert.ToDateTime(item.FoundDateStr)));
                                    commandDealerHistory.Parameters.Add(db.CreateDbParameter("ARegionId", item.RegionId));
                                    commandDealerHistory.Parameters.Add(db.CreateDbParameter("AProvinceName", item.ProvinceName));
                                    commandDealerHistory.Parameters.Add(db.CreateDbParameter("ACityName", item.CityName));
                                    commandDealerHistory.Parameters.Add(db.CreateDbParameter("ACountyName", item.CountyName));
                                    commandDealerHistory.Parameters.Add(db.CreateDbParameter("ACityLevel", item.CityLevel));

                                    commandDealerHistory.Parameters.Add(db.CreateDbParameter("AContactPerson", item.ContactPerson));
                                    commandDealerHistory.Parameters.Add(db.CreateDbParameter("AContactMobile", item.ContactMobile));
                                    commandDealerHistory.Parameters.Add(db.CreateDbParameter("AContactPhone", item.ContactPhone));
                                    commandDealerHistory.Parameters.Add(db.CreateDbParameter("AContactMail", item.ContactMail));
                                    commandDealerHistory.Parameters.Add(db.CreateDbParameter("ACompanyFax", item.Fax));
                                    commandDealerHistory.Parameters.Add(db.CreateDbParameter("AContactPostCode", item.ContactPostCode));
                                    commandDealerHistory.Parameters.Add(db.CreateDbParameter("ABusinessAddress", item.BusinessAddress));
                                    commandDealerHistory.Parameters.Add(db.CreateDbParameter("ARegisterCode", item.RegisterCode));
                                    commandDealerHistory.Parameters.Add(db.CreateDbParameter("ARegisterName", item.RegisterName));
                                    commandDealerHistory.Parameters.Add(db.CreateDbParameter("ARegisterCapital", item.RegisterCapital));
                                    commandDealerHistory.Parameters.Add(db.CreateDbParameter("AFixedAsset", item.FixedAsset));
                                    commandDealerHistory.Parameters.Add(db.CreateDbParameter("ARegisterDate", Convert.ToDateTime(item.RegisterDateStr)));
                                    commandDealerHistory.Parameters.Add(db.CreateDbParameter("ACorporateNature", item.CorporateNature));
                                    commandDealerHistory.Parameters.Add(db.CreateDbParameter("ALegalRepresentative", item.LegalRepresentative));
                                    commandDealerHistory.Parameters.Add(db.CreateDbParameter("ALegalRepresentTel", item.LegalRepresentTel));
                                    commandDealerHistory.Parameters.Add(db.CreateDbParameter("AIdDocumentType", item.IdDocumentType));
                                    commandDealerHistory.Parameters.Add(db.CreateDbParameter("AIdDocumentNumber", item.IdDocumentNumber));
                                    commandDealerHistory.Parameters.Add(db.CreateDbParameter("ARegisteredAddress", item.RegisteredAddress));
                                    commandDealerHistory.Parameters.Add(db.CreateDbParameter("ABusinessScope", item.BusinessScope));
                                    commandDealerHistory.Parameters.Add(db.CreateDbParameter("ABusinessAddressLongitude", item.BusinessAddressLongitude));
                                    commandDealerHistory.Parameters.Add(db.CreateDbParameter("ABusinessAddressLatitude", item.BusinessAddressLatitude));

                                    commandDealerHistory.Parameters.Add(db.CreateDbParameter("AStatus", (int)DcsBaseDataStatus.有效));
                                    commandDealerHistory.Parameters.Add(db.CreateDbParameter("FilerName", userInfo.Name));
                                    commandDealerHistory.Parameters.Add(db.CreateDbParameter("FileTime", DateTime.Now));
                                    commandDealerHistory.Parameters.Add(db.CreateDbParameter("FilerId", userInfo.Id));
                                    commandDealerHistory.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                    commandDealerHistory.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                    commandDealerHistory.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));

                                    commandDealerHistory.ExecuteNonQuery();

                                }
                            }
                            //无异常提交
                            ts.Commit();
                        } catch(Exception ex) {
                            //报错回滚
                            ts.Rollback();
                            throw new Exception(ex.Message);
                        } finally {
                            if(conn.State == System.Data.ConnectionState.Open) {
                                conn.Close();
                            }
                        }

                    }
                    return true;
                }

            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }

        public bool 更新站间距离(string EnterpriseCode, out string errorMessage) {
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    //开启事务
                    var ts = conn.BeginTransaction();
                    try {
                        var sqlUpdate = @"
                        UPDATE COMPANY A
                        SET(A.MINSERVICECODE,A.MINSERVICENAME,A.DISTANCE) =
                        (SELECT  C.CODE,C.NAME,C.DISTANCE
                        FROM
                        (SELECT  D.ID,B.ID AS ID2,B.CODE,B.NAME,
                        (ROUND(2 *ASIN(SQRT(POWER(SIN((D.BUSINESSADDRESSLATITUDE - B.BUSINESSADDRESSLATITUDE)* 0.0085), 2) 
                        + COS(D.BUSINESSADDRESSLATITUDE* 0.017) * COS(B.BUSINESSADDRESSLATITUDE* 0.017) 
                        * POWER(SIN((D.BUSINESSADDRESSLONGITUDE - B.BUSINESSADDRESSLONGITUDE)* 0.0085), 2)))* 63781370) / 10000) DISTANCE
                        FROM COMPANY B
                        INNER JOIN COMPANY D
                        ON B.ID <> D.ID 
                        AND (B.TYPE = 2 OR B.TYPE = 7)  AND B.STATUS =1
                        AND (D.TYPE = 2 OR D.TYPE = 7)  AND D.STATUS =1
                        and B.BUSINESSADDRESSLATITUDE is not null
                        and B.BUSINESSADDRESSLONGITUDE is not null
                       and d.BUSINESSADDRESSLATITUDE is not null
                       and d.BUSINESSADDRESSLONGITUDE is not null
                        ORDER BY DISTANCE ASC) C
                        WHERE ROWNUM =1
                        AND C.ID = A.ID 
                        )
                        WHERE (A.TYPE = 2 OR A.TYPE = 7)  AND A.STATUS =1
                     
                        ";

                        var commandDealer = db.CreateDbCommand(sqlUpdate, conn, ts);
                        //commandDealer.Parameters.Add(db.CreateDbParameter("EnterpriseCode", EnterpriseCode));

                        commandDealer.ExecuteNonQuery();

                        ts.Commit();

                    } catch(Exception ex) {
                        //报错回滚
                        ts.Rollback();
                        throw new Exception(ex.Message);
                    } finally {
                        if(conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                    errorMessage = null;
                    return true;
                }

            } catch(Exception ex) {
                errorMessage = ex.Message;
                return false;
            } finally {

            }
        }

        public bool 更新站间距离分品牌(string EnterpriseCode, out string errorMessage) {
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    //开启事务
                    var ts = conn.BeginTransaction();
                    try {
                        var sqlUpdate = @"UPDATE DealerServiceInfo A
                                         SET(A.MINSERVICECODE,A.MINSERVICENAME,A.DISTANCE) =
                                         (SELECT  C.CODE,C.NAME,C.DISTANCE
                                         FROM
                                         (SELECT DISTINCT dsd.ID ,D.ID AS ID1,B.ID AS ID2,B.CODE,B.NAME,
                                         (ROUND(2 *ASIN(SQRT(POWER(SIN((D.BUSINESSADDRESSLATITUDE - B.BUSINESSADDRESSLATITUDE)* 0.0085), 2) 
                                         + COS(D.BUSINESSADDRESSLATITUDE* 0.017) * COS(B.BUSINESSADDRESSLATITUDE* 0.017) 
                                         * POWER(SIN((D.BUSINESSADDRESSLONGITUDE - B.BUSINESSADDRESSLONGITUDE)* 0.0085), 2)))* 63781370) / 10000) DISTANCE
                                         FROM COMPANY B
                                         INNER JOIN COMPANY D
                                         ON B.ID <> D.ID 
                                         AND (B.TYPE = 2 OR B.TYPE = 7)  AND B.STATUS =1
                                         AND (D.TYPE = 2 OR D.TYPE = 7)  AND D.STATUS =1
                                         and B.BUSINESSADDRESSLATITUDE is not null
                                         and B.BUSINESSADDRESSLONGITUDE is not null
                                         and d.BUSINESSADDRESSLATITUDE is not null
                                         and d.BUSINESSADDRESSLONGITUDE is not null
                                         INNER JOIN Dealer db
                                         on db.CODE = B.CODE
                                         INNER JOIN Dealer dd
                                         on dd.CODE = D.CODE
                                         INNER JOIN DealerServiceInfo dsb
                                         on dsb.DealerId = db.ID
                                         INNER JOIN DealerServiceInfo dsd
                                         on dsd.DealerId = dd.ID
                                         WHERE dsb.PartsSalesCategoryId = dsd.PartsSalesCategoryId 
                                         ORDER BY DISTANCE ASC) C
                                         WHERE ROWNUM =1
                                         AND C.ID = A.ID 
                                         )
                                         WHERE (A.STATUS <> 99) 
                        ";

                        var commandDealer = db.CreateDbCommand(sqlUpdate, conn, ts);
                        //commandDealer.Parameters.Add(db.CreateDbParameter("EnterpriseCode", EnterpriseCode));

                        commandDealer.ExecuteNonQuery();

                        ts.Commit();

                    } catch(Exception ex) {
                        //报错回滚
                        ts.Rollback();
                        throw new Exception(ex.Message);
                    } finally {
                        if(conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                    errorMessage = null;
                    return true;
                }

            } catch(Exception ex) {
                errorMessage = ex.Message;
                return false;
            } finally {

            }
        }

        public bool ExportDealer(int? id, string code, string name, int? status, DateTime? createTimeStart, DateTime? createTimeEnd, out string fileName) {
            fileName = GetExportFilePath("服务站/经销商信息_" + ".xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated) {
                    throw new Exception(ErrorStrings.Export_Validation_Dealer_Validation52);
                }
                var db = DbHelper.GetDbHelp(ConnectionString);
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();

                    sql.Append(@"select a.code,
b.name,
a.shortname,
a.manager,
b.customercode,
b.suppliercode,
b.founddate,
b.provincename,
b.cityname,
b.countyname,
 cast(case b.citylevel 
                                        When 1 Then 
                                        '省级'
                                        When 2 Then 
                                        '地级' 
                                        when 3 Then
                                        '县级' 
                                        End as varchar2(10)),
b.contactperson,
b.contactphone,
b.contactmail,
b.contactmobile,
b.BusinessLinkName,
b.BusinessContactMethod,
b.fax,
b.contactpostcode,
b.businessaddress,
b.businessaddresslongitude,
b.businessaddresslatitude,
b.distance,
b.minservicecode,
b.minservicename,
b.registercode,
b.registername,
b.registerdate,
b.registercapital,
b.fixedasset,
b.legalrepresentative,
b.legalrepresenttel,
b.corporatenature,
b.businessscope,
b.registeredaddress,
 cast(case b.iddocumenttype
                                        When 65011001 Then 
                                        '居民身份证'
                                        When 65011002 Then 
                                        '组织机构代码证' 
                                        when 65011003 Then
                                        '护照' 
                                        when 65011004 then
                                         '军官证'
                                         when 65011005 then
                                         '其它'
                                        End as varchar2(10)),
b.iddocumentnumber,
 cast(case c.repairqualification 
                                        When 1 Then 
                                        '一类'
                                        When 2 Then 
                                        '二类' 
                                        when 3 Then
                                        '三类'
                                        when 4 then
                                          '无' 
                                        End as varchar2(10)),
c.ownercompany,
c.buildtime,
c.trafficrestrictionsdescribe,
c.mainbusinessareas,
c.andbusinessareas,

 cast(case c.dangerousrepairqualification 
                                        When 1 Then 
                                        '有'
                                        When 0 Then 
                                        '无' 
                                        End as varchar2(10)),
 cast(case c.hasbranch 
                                        When 1 Then 
                                        '有'
                                        When 0 Then 
                                        '无' 
                                        End as varchar2(10)),
 cast(case c.vehicletravelroute 
                                        When 1 Then 
                                        '高速公路出入口'
                                        When 2 Then 
                                        '国家公路' 
                                        when 3 Then
                                        '省级公路'
                                        when 4 then
                                          '县级公路' 
                                        When 5 Then 
                                        '乡村道路' 
                                        when 6 Then
                                        '非临近限行区域'
                                        when 7 then
                                          '无'
                                        End as varchar2(10)),
c.vehicleusespeciality,
c.vehicledockingstation,
c.employeenumber,
c.repairingarea,
c.receptionroomarea,
c.parkingarea,
c.partwarehousearea,
 cast(case b.CompDocumentType
                                        When 1 Then 
                                        '营业执照'
                                        When 2 Then 
                                        '税务登记证' 
                                        when 3 Then
                                        '居民身份证'
                                        when 4 then
                                        '事业单位法人证书' 
                                        When 5 Then 
                                        '统一社会信用代码证书' 
                                        when 6 Then
                                        '部队代码'
                                        when 7 then
                                        '护照'
                                        when 8 then
                                        '组织机构代码证'
                                        when 9 then
                                        '军官证'
                                        when 99 then
                                        '其他'
                                        End as varchar2(20)),
d.taxregisterednumber,
cast(case d.taxpayerqualification
                                        When 1 Then 
                                        '一般纳税人'
                                        When 2 Then 
                                        '小规模纳税人' 
                                        End as varchar2(10)),
d.invoiceamountquota,
d.invoicetitle,
d.invoicetype,--
 cast(case b.citylevel 
                                        When 0 Then 
                                        '为空'
                                        When 1 Then 
                                        '增值税发票' 
                                        when 2 Then
                                        '普通发票' 
                                        End as varchar2(10)),
d.bankname,
d.invoicetax,
d.bankaccount,
d.linkman,
d.contactnumber,
d.fax,
d.taxregisteredaddress,
a.status,
a.creatorname,
a.createtime,
a.modifiername,
a.modifytime
 from Dealer a
 left join Company b on a.id=b.id 
 left join DealerServiceExt c on c.id=a.id
 left join CompanyInvoiceInfo d on d.companyid=b.id and d.InvoiceCompanyId=" + Utils.GetCurrentUserInfo().EnterpriseId
                    + @" where 1=1 ");
                    var dbParameters = new List<DbParameter>();
                    if(id.HasValue) {
                        sql.Append(@" and a.id ={0}id");
                        dbParameters.Add(db.CreateDbParameter("id", id.Value));
                    }
                    if(!string.IsNullOrEmpty(code)) {
                        sql.Append(@" and a.code like {0}code ");
                        dbParameters.Add(db.CreateDbParameter("code", '%' + code + '%'));
                    }
                    if(!string.IsNullOrEmpty(name)) {
                        sql.Append(@" and b.name like {0}name ");
                        dbParameters.Add(db.CreateDbParameter("name", '%' + name + '%'));
                    }
                    if(status.HasValue) {
                        sql.Append(@" and a.status ={0}status");
                        dbParameters.Add(db.CreateDbParameter("status", status.Value));
                    }
                    if(createTimeStart.HasValue) {
                        sql.Append(@" and a.createTime >=To_date({0}createTimeStart,'yyyy-mm-dd hh24:mi:ss')");
                        var tempValue = createTimeStart.Value;
                        var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                        dbParameters.Add(db.CreateDbParameter("createTimeStart", tempTime.ToString("G")));
                    }
                    if(createTimeEnd.HasValue) {
                        sql.Append(" and a.createTime<=To_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                        var tempValue = createTimeEnd.Value;
                        var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                        dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                    }
                    var sqlStr = sql.ToString();
                    if(sqlStr.IndexOf(" where 1=1  and ") > 0)
                        sqlStr = sqlStr.Replace(" where 1=1  and ", " where ");
                    else
                        sqlStr = sqlStr.Replace(" where 1=1 ", "");
                    var command = db.CreateDbCommand(string.Format(sqlStr, db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                                     ErrorStrings.Export_Title_Dealer_DealerCode,
                 ErrorStrings.Export_Title_Dealer_DealerName,
                 ErrorStrings.Export_Title_Agency_Abbreviation,
                 ErrorStrings.Export_Title_Dealer_Manager,
                 ErrorStrings.Export_Title_Dealer_CustomerCode,
                 ErrorStrings.Export_Title_AccountPay_SupplierCode,
                 ErrorStrings.Export_Title_Agency_FoundDate,
                 ErrorStrings.Export_Title_Agency_Province,
                 ErrorStrings.Export_Title_Agency_City,
                 ErrorStrings.Export_Title_Agency_Country,
                 ErrorStrings.Export_Title_Dealer_CityLevel,
                 ErrorStrings.Export_Title_Agency_LinkMan,
                 ErrorStrings.Export_Title_Dealer_ContactPhone,
                 "E-MAIL",
                 ErrorStrings.Export_Title_Agency_LinkManPhone,
                 ErrorStrings.Export_Validation_Dealer_Validation53,
                 ErrorStrings.Export_Validation_Dealer_Validation54,
                 ErrorStrings.Export_Title_Agency_Fax,
                 ErrorStrings.Export_Title_Dealer_ContactPostCode,
                 ErrorStrings.Export_Title_Dealer_BusinessAddress,
                 ErrorStrings.Export_Title_Dealer_BusinessAddressLongitude,
                 ErrorStrings.Export_Title_Dealer_BusinessAddressLatitude,
                 ErrorStrings.Export_Validation_Dealer_Validation55,
                 ErrorStrings.Export_Validation_Dealer_Validation56,
                 ErrorStrings.Export_Validation_Dealer_Validation57,
                 ErrorStrings.Export_Title_Agency_RegistrationNo,
                 ErrorStrings.Export_Title_Agency_RegistrationName,
                 ErrorStrings.Export_Title_Agency_RegisterDate,
                 ErrorStrings.Export_Title_Agency_RegisterCapital,
                 ErrorStrings.Export_Title_Dealer_FixedAsset,
                 ErrorStrings.Export_Title_Agency_LegalRepresentative,
                 ErrorStrings.Export_Title_Dealer_LegalRepresentTel,
                 ErrorStrings.Export_Title_Agency_Nature,
                 ErrorStrings.Export_Title_Agency_BusinessScope,
                 ErrorStrings.Export_Title_Agency_RegisteredAddress,
                 ErrorStrings.Export_Title_Agency_IdDocumentType,
                 ErrorStrings.Export_Title_Agency_IdDocumentNumber,
                 ErrorStrings.Export_Title_Dealer_RepairQualification,
                 ErrorStrings.Export_Title_Dealer_OwnerCompany,
                 ErrorStrings.Export_Title_Dealer_BuildTime,
                 ErrorStrings.Export_Title_Dealer_TrafficRestrictionsdescribe,
                 ErrorStrings.Export_Title_Dealer_MainBusinessAreas,
                 ErrorStrings.Export_Title_Dealer_AndBusinessAreas,               
                 ErrorStrings.Export_Title_Dealer_DangerousRepairQualification,
                 ErrorStrings.Export_Title_Dealer_HasBranch,
                 ErrorStrings.Export_Title_Dealer_VehicleTravelRoute,
                 ErrorStrings.Export_Validation_Dealer_Validation58,
                 ErrorStrings.Export_Title_Dealer_VehicleDockingStation,
                 ErrorStrings.Export_Title_Dealer_EmployeeNumber,
                 ErrorStrings.Export_Title_Dealer_RepairingArea,
                 ErrorStrings.Export_Title_Dealer_ReceptionRoomArea,
                 ErrorStrings.Export_Title_Dealer_ParkingArea,
                 ErrorStrings.Export_Title_Dealer_PartWarehouseArea,
                 ErrorStrings.Export_Validation_Dealer_Validation59,
                 ErrorStrings.Export_Title_Dealer_TaxRegisteredNumber,
                 ErrorStrings.Export_Title_Dealer_TaxpayerQualification,
                 ErrorStrings.Export_Title_Dealer_InvoiceAmountQuota,
                 ErrorStrings.Export_Title_Dealer_InvoiceTitle,
                 ErrorStrings.Export_Title_PartsInboundCheckBill_BillingType,
                 ErrorStrings.Export_Title_Dealer_BankName,
                 ErrorStrings.Export_Title_Dealer_InvoiceTax,
                 ErrorStrings.Export_Title_Dealer_BankAccount,
                 ErrorStrings.Export_Title_Dealer_Linkman,
                 ErrorStrings.Export_Title_Dealer_ContactNumber,
                 ErrorStrings.Export_Title_Dealer_AccountFax,
                 ErrorStrings.Export_Title_Dealer_TaxRegisteredAddress,
                 ErrorStrings.Export_Title_Dealer_BankName,
                 ErrorStrings.Export_Title_AccountPeriod_Status,
                 ErrorStrings.Export_Title_AccountPeriod_CreatorName,
                 ErrorStrings.Export_Title_AccountPeriod_CreateTime,
                 ErrorStrings.Export_Title_AccountPeriod_ModifierName,
                 ErrorStrings.Export_Title_AccountPeriod_ModifyTime,
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }

                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }
    }
}
