﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        //导入经销商视频手机清单
        public bool ImportDealerMobileNumberList(string fileName, out List<DealerMobileNumberListExtend> rightData, out List<DealerMobileNumberListExtend> errorData, out string errorDataFileName, out string errorMessage) {
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<DealerMobileNumberListExtend>();
            var rightList = new List<DealerMobileNumberListExtend>();
            var allList = new List<DealerMobileNumberListExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("DealerMobileNumberList", out notNullableFields, out fieldLenght);
                List<string> notNullableFieldsDealer;
                Dictionary<string, int> fieldLenghtDealer;
                db.GetTableSchema("Dealer", out notNullableFieldsDealer, out fieldLenghtDealer);

                List<object> excelColumns;

                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    #region 指定文件中的列对应的名称 例如 excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode, "PartCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_Code, "DealerCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_Name, "DealerName");
                    excelOperator.AddColumnDataSource("视频手机号", "VideoMobileNumber");
                    #endregion

                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    //var tempExcelOperator = excelOperator;

                    #region 获取对应枚举
                    //var keyValuePairs = new[] {
                    //        new KeyValuePair<string, string>("DebitOrReplenish", "SubDealer_DebitOrReplenish")
                    //    };
                    //tempExcelOperator.ImportHelper.LoadEnumFromDb(null,null, keyValuePairs);
                    #endregion

                    excelOperator.LoadExcelRow(row => {
                        var tempImportObj = new DealerMobileNumberListExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        #region 根据导入的字段给对应的实体属性赋值
                        tempImportObj.DealerCode = newRow["DealerCode"];
                        tempImportObj.DealerName = newRow["DealerName"];
                        tempImportObj.VideoMobileNumber = newRow["VideoMobileNumber"];
                        #endregion

                        var tempErrorMessage = new List<string>();

                        #region 导入的数据基本检查
                        var fieldIndex = notNullableFieldsDealer.IndexOf("Code".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.DealerCode)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add("服务站编号不能为空");
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.DealerCode) > fieldLenghtDealer["Code".ToUpper()])
                                tempErrorMessage.Add("服务站编号过长");
                        }

                        fieldIndex = notNullableFields.IndexOf("VideoMobileNumber".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.VideoMobileNumber)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add("视频手机号不能为空");
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.VideoMobileNumber) > fieldLenght["VideoMobileNumber".ToUpper()])
                                tempErrorMessage.Add("视频手机号过长");
                        }
                        #endregion

                        if(tempErrorMessage.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });
                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    #region 剩下的数据进行业务检查
                    //校验导入文件是否重复
                    var groups = tempRightList.GroupBy(r => new {
                        r.DealerCode,
                        r.VideoMobileNumber
                    }).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        groupItem.ErrorMsg = ErrorStrings.ImpPartsSupplierRelation_Validation31;
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    //经销商信息
                    var dealerCodesNeedCheck = tempRightList.Select(r => r.DealerCode.ToUpper()).Distinct().ToArray();
                    var dbDealers = new List<DealerExtend>();
                    Func<string[], bool> getDbDealerNames = value => {
                        var dbObj = new DealerExtend {
                            Id = Convert.ToInt32(value[0]),
                            Code = value[1],
                            Name = value[2]
                        };
                        dbDealers.Add(dbObj);
                        return false;
                    };
                    var sql = string.Format("select Id,Code,Name from Dealer where status={0} ", (int)DcsBaseDataStatus.有效);
                    db.QueryDataWithInOperator(sql, "Code", true, dealerCodesNeedCheck, getDbDealerNames);
                    foreach(var tempRight in tempRightList) {
                        var dealer = dbDealers.FirstOrDefault(v => v.Code.ToUpper() == tempRight.DealerCode.ToUpper());
                        if(dealer == null) {
                            tempRight.ErrorMsg = String.Format("视频手机所属服务站编号“{0}”不存在", tempRight.DealerCode);
                            continue;
                        }
                        tempRight.DealerId = dealer.Id;
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    //服务站编号，视频手机号组合唯一存在于经销商视频手机清单中
                    if(tempRightList.Any()) {
                        var listsNeedCheck = tempRightList.Select(r => new {
                            r.DealerId,
                            r.VideoMobileNumber
                        }).Distinct().ToList();
                        var dbDealerMobileNumberLists = new List<DealerMobileNumberListExtend>();
                        Func<string[], bool> getDbDealerMobileNumberLists = value => {
                            dbDealerMobileNumberLists.Add(new DealerMobileNumberListExtend {
                                DealerId = Convert.ToInt32(value[0]),
                                VideoMobileNumber = value[1]
                            });
                            return false;
                        };
                        string sql1 = string.Empty; ;
                        listsNeedCheck.ForEach(r => sql1 = string.Format(sql1 + " or (Dealerid = {0} And Videomobilenumber = {1})", r.DealerId, r.VideoMobileNumber));
                        sql1 = sql1.IndexOf(" or ") == 0 ? sql1.Substring(" or ".Length) : sql1;
                        sql1 = @"Select Dealerid, Videomobilenumber From Dealermobilenumberlist where " + sql1;
                        db.QueryDataWithInOperator(sql1, "1", false, new[] { "1" }, getDbDealerMobileNumberLists);
                        foreach(var tempRight in from tempRight in tempRightList
                                                 let dealer = dbDealerMobileNumberLists.FirstOrDefault(v => v.DealerId == tempRight.DealerId && v.VideoMobileNumber.ToUpper() == tempRight.VideoMobileNumber.ToUpper())
                                                 where dealer != null
                                                 select tempRight) {
                            tempRight.ErrorMsg = String.Format("服务站编号:{0}视频手机号:{1}已存在", tempRight.DealerCode, tempRight.VideoMobileNumber);
                        }
                    }
                    #endregion

                    //获取所有不合格数据
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    //获取所有合格数据
                    rightList = allList.Except(errorList).ToList();

                    #region 将合格数据的值填上
                    //foreach(var rightItem in rightList) {
                    //}
                    #endregion
                }
                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempObj = list[index - 1];
                            var values = new object[] {
                                #region 设置错误信息导出的列的值
                                tempObj.DealerCode,tempObj.DealerName,tempObj.VideoMobileNumber,tempObj.ErrorMsg
                                #endregion
                            };
                            return values;
                        });
                    }
                }

                //导入所有合格数据
                if(!rightList.Any())
                    return true;
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    //开启事务，新增更新配件在一个事务内
                    var ts = conn.BeginTransaction();
                    try {
                        if(rightList.Any()) {
                            #region 获取对应的插入的SQL 通过db.GetInsertSql方法
                            //获取新增数据的sql语句，Id为主键
                            var sqlInsert = db.GetInsertSql("DealerMobileNumberList", "Id", new[] { "DealerId", "VideoMobileNumber" });
                            var sqlInsertHistory = db.GetInsertSql("DealerMobileNumberHistory", "Id", new[] {
                                "DealerId", "VideoMobileNumber", "CreateTime", "CreatorId", "CreatorName"
                            });
                            var userInfo = Utils.GetCurrentUserInfo();
                            #endregion
                            foreach(var item in rightList) {
                                #region 添加Sql的参数
                                var command = db.CreateDbCommand(sqlInsert, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("DealerId", item.DealerId));
                                command.Parameters.Add(db.CreateDbParameter("VideoMobileNumber", item.VideoMobileNumber));
                                db.ExecuteInsert(command, "Id");
                                //往数据库增加履历
                                var commandHistory = db.CreateDbCommand(sqlInsertHistory, conn, ts);
                                commandHistory.Parameters.Add(db.CreateDbParameter("DealerId", item.DealerId));
                                commandHistory.Parameters.Add(db.CreateDbParameter("VideoMobileNumber", item.VideoMobileNumber));
                                commandHistory.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                commandHistory.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                commandHistory.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                commandHistory.ExecuteNonQuery();
                                #endregion
                            }
                        }
                        //无异常提交
                        ts.Commit();
                    } catch(Exception) {
                        //报错回滚
                        ts.Rollback();
                        throw;
                    } finally {
                        if(conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }
    }
}
