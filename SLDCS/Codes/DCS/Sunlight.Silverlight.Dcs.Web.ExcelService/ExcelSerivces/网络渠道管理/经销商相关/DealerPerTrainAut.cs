﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        //服务站人员培训认证
        public bool ExportDealerPerTrainAut(int[] ids, int? partsSalesCategoryId, int? marketingDepartmentId, string dealerCode, string dealerName, DateTime? trainingTimeBegin, DateTime? trainingTimeEnd, DateTime? authenticationTimeBegin, DateTime? authenticationTimeEnd, int? positionId, int? status, string idCard, int? authenticationType, int? trainingType, string certificateId, string trainingName, out string fileName) {
            fileName = GetExportFilePath("服务站人员培训认证_" + ".xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated) {
                    throw new Exception(ErrorStrings.File_Validation_User);
                }
                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {

                    conn.Open();
                    var sql = new StringBuilder();
                    sql.Append(@"select 
                                    PartsSalesCategory.Name,
                                    MarketingDepartment.Name,
                                    DealerPerTrainAut.DealerCode,
                                    DealerPerTrainAut.DealerName,
                                    DealerPerTrainAut.Name,
                                    DealerPerTrainAut.IdCard,
                                    AuthenticationType.AuthenticationTypeName,
                                    DealerPerTrainAut.AuthenticationTime,
                                    (select value from keyvalueitem where name='TrainingTypeName' and key = DealerPerTrainAut.Trainingtype),
                                    DealerPerTrainAut.TrainingTime,
                                    DealerPerTrainAut.TrainingName,
                                    DealerPerTrainAut.CertificateId,
                                    add_months(DealerPerTrainAut.AuthenticationTime,24),
                                    DealerKeyPosition.PositionName,
                                    Cast (decode(DealerPerTrainAut.Status,1,'有效',99,'作废')as varchar2(20)) as Status,
                                    DealerPerTrainAut.CreatorName,
                                    DealerPerTrainAut.CreateTime,
                                    DealerPerTrainAut.ModifierName,
                                    DealerPerTrainAut.ModifyTime,
                                    DealerPerTrainAut.CancelName,
                                    DealerPerTrainAut.CancelTime
                                    from DealerPerTrainAut 
                                    LEFT JOIN MarketingDepartment ON DealerPerTrainAut.MarketingDepartmentId = MarketingDepartment.Id
                                    LEFT JOIN AuthenticationType ON DealerPerTrainAut.AuthenticationType = AuthenticationType.Id
                                    LEFT JOIN PartsSalesCategory ON DealerPerTrainAut.PartsSalesCategoryId = PartsSalesCategory.Id
                                    LEFT JOIN DealerKeyPosition ON DealerPerTrainAut.PositionId = DealerKeyPosition.Id where 1=1 ");
                    var dbParameters = new List<DbParameter>();
                    #region 条件过滤
                    if(ids != null && ids.Length > 0) {
                        sql.Append(" and DealerPerTrainAut.id in (");
                        for(var i = 0; i < ids.Length; i++) {
                            if(ids.Length == i + 1) {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            } else {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                        }
                        sql.Append(")");
                    } else {
                        if(partsSalesCategoryId.HasValue) {
                            sql.Append(@" and DealerPerTrainAut.partsSalesCategoryId = {0}partsSalesCategoryId ");
                            dbParameters.Add(db.CreateDbParameter("partsSalesCategoryId", partsSalesCategoryId.Value));
                        }
                        if(marketingDepartmentId.HasValue) {
                            sql.Append(@" and DealerPerTrainAut.marketingDepartmentId = {0}marketingDepartmentId ");
                            dbParameters.Add(db.CreateDbParameter("marketingDepartmentId", marketingDepartmentId.Value));
                        }
                        if(!String.IsNullOrEmpty(dealerCode)) {
                            sql.Append(@" and DealerPerTrainAut.dealerCode like {0}dealerCode ");
                            dbParameters.Add(db.CreateDbParameter("dealerCode", "%" + dealerCode + "%"));
                        }
                        if(!String.IsNullOrEmpty(dealerName)) {
                            sql.Append(@" and DealerPerTrainAut.dealerName like {0}dealerName ");
                            dbParameters.Add(db.CreateDbParameter("dealerName", "%" + dealerName + "%"));
                        }
                        if(trainingTimeBegin.HasValue) {
                            sql.Append(@" and DealerPerTrainAut.trainingTime>=to_date({0}trainingTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = trainingTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("trainingTimeBegin", tempTime.ToString("G")));
                        }
                        if(trainingTimeEnd.HasValue) {
                            sql.Append(@" and DealerPerTrainAut.trainingTime <=to_date({0}trainingTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = trainingTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("trainingTimeEnd", tempTime.ToString("G")));
                        }
                        if(authenticationTimeBegin.HasValue) {
                            sql.Append(@" and DealerPerTrainAut.authenticationTime>=to_date({0}authenticationTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = authenticationTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("authenticationTimeBegin", tempTime.ToString("G")));
                        }
                        if(authenticationTimeEnd.HasValue) {
                            sql.Append(@" and DealerPerTrainAut.authenticationTime <=to_date({0}authenticationTimeEnd,'yyyy-mm-dd hh024:mi:ss')");
                            var tempValue = authenticationTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("authenticationTimeEnd", tempTime.ToString("G")));
                        }
                        if(positionId.HasValue) {
                            sql.Append(@" and DealerPerTrainAut.positionId = {0}positionId ");
                            dbParameters.Add(db.CreateDbParameter("positionId", positionId.Value));
                        }
                        if(status.HasValue) {
                            sql.Append(@" and DealerPerTrainAut.status = {0}status ");
                            dbParameters.Add(db.CreateDbParameter("status", status.Value));
                        }
                        if(!string.IsNullOrEmpty(idCard)) {
                            sql.Append(@" and DealerPerTrainAut.IdCard like {0}idCard ");
                            dbParameters.Add(db.CreateDbParameter("idCard", "%" + idCard + "%"));
                        }
                        if(authenticationType.HasValue) {
                            sql.Append(@" and DealerPerTrainAut.AuthenticationType = {0}authenticationType ");
                            dbParameters.Add(db.CreateDbParameter("authenticationType", authenticationType.Value));
                        }
                        if(trainingType.HasValue) {
                            sql.Append(@" and DealerPerTrainAut.TrainingType = {0}trainingType ");
                            dbParameters.Add(db.CreateDbParameter("trainingType", trainingType.Value));
                        }
                        if(!string.IsNullOrEmpty(certificateId)) {
                            sql.Append(@" and DealerPerTrainAut.CertificateId like {0}certificateId ");
                            dbParameters.Add(db.CreateDbParameter("certificateId", "%" + certificateId + "%"));
                        }
                        if(!string.IsNullOrEmpty(trainingName)) {
                            sql.Append(@" and DealerPerTrainAut.TrainingName like {0}trainingName ");
                            dbParameters.Add(db.CreateDbParameter("trainingName", "%" + trainingName + "%"));
                        }
                    }
                    sql.Append(" ORDER BY DealerPerTrainAut.Id ASC");
                    #endregion
                    var sqlStr = sql.ToString();
                    if(sqlStr.IndexOf(" where 1=1  and ") > 0)
                        sqlStr = sqlStr.Replace(" where 1=1  and ", " where ");
                    else
                        sqlStr = sqlStr.Replace(" where 1=1 ", "");
                    var command = db.CreateDbCommand(string.Format(sqlStr, db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                  ErrorStrings.Export_Title_Partssalescategory_Name,"市场部",ErrorStrings.Export_Title_Dealer_Code,ErrorStrings.Export_Title_Dealer_Name,"姓名","身份证号码","认证类型","认证时间","培训类型","培训时间","培训课程","证书ID","证书有效期","岗位",ErrorStrings.Export_Title_AccountPeriod_Status,ErrorStrings.Export_Title_AccountPeriod_CreatorName,ErrorStrings.Export_Title_AccountPeriod_CreateTime,ErrorStrings.Export_Title_AccountPeriod_ModifierName,ErrorStrings.Export_Title_AccountPeriod_ModifyTime,ErrorStrings.Export_Title_PartsBranch_AbandonerName,ErrorStrings.Export_Title_PartsBranch_AbandonerTime
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }

                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }

        public bool ImportDealerPerTrainAut(string fileName, out int excelImportNum, out List<DealerPerTrainAutExtend> rightData, out List<DealerPerTrainAutExtend> errorData, out string errorDataFileName, out string errorMessage) {
            var userinfo = Utils.GetCurrentUserInfo();
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<DealerPerTrainAutExtend>();
            var rightList = new List<DealerPerTrainAutExtend>();
            var allList = new List<DealerPerTrainAutExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("DealerPerTrainAut", out notNullableFields, out fieldLenght);
                List<object> excelColumns;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Partssalescategory_Name, "PartsSalesCategoryName");
                    excelOperator.AddColumnDataSource("市场部", "MarketingDepartmentName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_Code, "DealerCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_Name, "DealerName");
                    excelOperator.AddColumnDataSource("岗位", "PositionName");
                    excelOperator.AddColumnDataSource("姓名", "Name");
                    excelOperator.AddColumnDataSource("认证类型", "AuthenticationTypeName");
                    excelOperator.AddColumnDataSource("认证时间", "AuthenticationTime");
                    excelOperator.AddColumnDataSource("培训类型", "TrainingTypeName");
                    excelOperator.AddColumnDataSource("培训时间", "TrainingTime");
                    excelOperator.AddColumnDataSource("证书ID", "CertificateId");
                    excelOperator.AddColumnDataSource("培训课程", "TrainingName");
                    excelOperator.AddColumnDataSource("身份证号码", "IdCard");

                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var tempImportObj = new DealerPerTrainAutExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        #region
                        tempImportObj.PartsSalesCategoryName = newRow["PartsSalesCategoryName"];
                        tempImportObj.MarketingDepartmentName = newRow["MarketingDepartmentName"];
                        tempImportObj.DealerCode = newRow["DealerCode"];
                        tempImportObj.DealerName = newRow["DealerName"];
                        tempImportObj.PositionName = newRow["PositionName"];
                        tempImportObj.Name = newRow["Name"];
                        tempImportObj.AuthenticationTypeName = newRow["AuthenticationTypeName"];
                        tempImportObj.AuthenticationTimeStr = newRow["AuthenticationTime"];
                        tempImportObj.TrainingTypeName = newRow["TrainingTypeName"];
                        tempImportObj.TrainingTimeStr = newRow["TrainingTime"];
                        tempImportObj.CertificateId = newRow["CertificateId"];
                        tempImportObj.TrainingName = newRow["TrainingName"];
                        tempImportObj.IdCard = newRow["IdCard"];
                        #endregion
                        var tempErrorMessage = new List<string>();
                        #region 导入的内容基本检查

                        //品牌检查
                        if(string.IsNullOrEmpty(tempImportObj.PartsSalesCategoryName)) {
                            tempErrorMessage.Add(ErrorStrings.Export_Validation_MarketingDepartment_PartsSalesCategoryIsNull);
                        }

                        //if(string.IsNullOrEmpty(tempImportObj.MarketingDepartmentName)) {
                        //    tempErrorMessage.Add("市场部不能为空");
                        //}

                        if(string.IsNullOrEmpty(tempImportObj.PositionName)) {
                            tempErrorMessage.Add("岗位不能为空");
                        }

                        var fieldIndex = notNullableFields.IndexOf("Name".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.Name)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add("姓名不能为空");
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.Name) > fieldLenght["Name".ToUpper()])
                                tempErrorMessage.Add("姓名过长");
                        }

                        fieldIndex = notNullableFields.IndexOf("DealerCode".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.DealerCode)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add("服务站编号不能为空");
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.DealerCode) > fieldLenght["DealerCode".ToUpper()])
                                tempErrorMessage.Add("服务站编号过长");
                        }

                        fieldIndex = notNullableFields.IndexOf("DealerName".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.DealerName)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add("服务站名称不能为空");
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.DealerName) > fieldLenght["DealerName".ToUpper()])
                                tempErrorMessage.Add("服务站名称过长");
                        }

                        fieldIndex = notNullableFields.IndexOf("AuthenticationTime".ToUpper());
                        if(newRow["AuthenticationTime"] == null) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add("认证时间不能为空");
                        } else {
                            DateTime authenticationTime;
                            if(DateTime.TryParse(newRow["AuthenticationTime"], out authenticationTime)) {
                                tempImportObj.AuthenticationTime = authenticationTime;
                            } else {
                                tempErrorMessage.Add("认证时间格式无法解析");
                            }
                        }

                        if(string.IsNullOrEmpty(tempImportObj.TrainingTypeName)) {
                            tempErrorMessage.Add("培训类型名称不能为空");
                        } else {
                            DcsTrainingTypeName dcsTrainingTypeName;
                            if(Enum.TryParse<DcsTrainingTypeName>(tempImportObj.TrainingTypeName, out dcsTrainingTypeName) && Enum.IsDefined(typeof(DcsTrainingTypeName), dcsTrainingTypeName)) {
                                tempImportObj.TrainingType = (int)dcsTrainingTypeName;
                            } else {
                                tempErrorMessage.Add("培训类型名称填写有误！");
                            }
                        }

                        fieldIndex = notNullableFields.IndexOf("TrainingTime".ToUpper());
                        if(newRow["TrainingTime"] == null) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add("培训时间不能为空");
                        } else {
                            DateTime trainingTime;
                            if(DateTime.TryParse(newRow["TrainingTime"], out trainingTime)) {
                                tempImportObj.TrainingTime = trainingTime;
                            } else {
                                tempErrorMessage.Add("培训时间格式无法解析");
                            }
                        }
                        if(string.IsNullOrEmpty(newRow["CertificateId"])) {
                            tempErrorMessage.Add("证书ID不能为空");
                        }
                        if(string.IsNullOrEmpty(newRow["TrainingName"])) {
                            tempErrorMessage.Add("培训课程不能为空");
                        }
                        if(!string.IsNullOrEmpty(newRow["IdCard"])) {
                            string idDocumentNumber = newRow["IdCard"].Trim();
                            if(idDocumentNumber.Length == 18) {
                                char[] chaStr = idDocumentNumber.ToCharArray();
                                List<int> list = new List<int> {
                                7,
                                9,
                                10,
                                5,
                                8,
                                4,
                                2,
                                1,
                                6,
                                3,
                                7,
                                9,
                                10,
                                5,
                                8,
                                4,
                                2
                            }; //身份证对应系数                           
                                string Num = "10X98765432";
                                int sum = 0;
                                for(int i = 0; i < list.Count; i++) {
                                    sum += list[i] * Convert.ToInt32(chaStr[i].ToString());
                                }
                                int a = sum % 11;
                                if(Num.Substring(a, 1) != idDocumentNumber.ToUpper().Substring(17, 1)) {
                                    tempErrorMessage.Add("请输入合法的居民身份证号码");
                                }
                            } else if(idDocumentNumber.Length != 15) {
                                tempErrorMessage.Add("请输入正确的居民身份证号码,15或18位");
                            }
                        }
                        #endregion
                        if(tempErrorMessage.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });
                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    #region 导入内容业务检查
                    var groups = tempRightList.GroupBy(r => new {
                        r.PartsSalesCategoryName,
                        r.MarketingDepartmentName,
                        r.DealerCode,
                        r.DealerName,
                        r.PositionName,
                        r.Name,
                        r.AuthenticationTypeName,
                        r.AuthenticationTimeStr,
                        r.TrainingTypeName,
                        r.TrainingTimeStr
                    }).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        groupItem.ErrorMsg = ErrorStrings.ImpPartsSupplierRelation_Validation31;
                    }
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();

                    //校验品牌是否存在
                    var partsSalesCategoryNeedCheck = tempRightList.Select(r => r.PartsSalesCategoryName).Distinct().ToArray();
                    var dbPartsSalesCategories = new List<DealerPerTrainAutExtend>();
                    Func<string[], bool> getDbPartsSalesCategories = value => {
                        var dbObj = new DealerPerTrainAutExtend {
                            PartsSalesCategoryId = Convert.ToInt32(value[0]),
                            PartsSalesCategoryName = value[1]
                        };
                        dbPartsSalesCategories.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select id,name from PartsSalesCategory where status=1 ", "Name", true, partsSalesCategoryNeedCheck, getDbPartsSalesCategories);
                    foreach(var tempObj in tempRightList) {
                        var tempobjcheck = dbPartsSalesCategories.FirstOrDefault(r => r.PartsSalesCategoryName == tempObj.PartsSalesCategoryName);
                        if(tempobjcheck == null) {
                            tempObj.ErrorMsg = "营销分公司下品牌" + tempObj.PartsSalesCategoryName + ErrorStrings.Export_Validation_Sparepart_NotExist;
                        } else {
                            tempObj.PartsSalesCategoryId = tempobjcheck.PartsSalesCategoryId;
                        }

                    }

                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();
                    //经销商编号、经销商名称组合存在于经销商中，否则提示：服务站不存在
                    var dealerCodeStrsNeedCheck = tempRightList.Select(r => r.DealerCode).Distinct().ToArray();
                    var dbDealers = new List<DealerPerTrainAutExtend>();
                    Func<string[], bool> getDbDealers = value => {
                        var dbObj = new DealerPerTrainAutExtend {
                            DealerId = Convert.ToInt32(value[0]),
                            DealerCode = value[1],
                            DealerName = value[2]
                        };
                        dbDealers.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select Id,Code,Name from Dealer where status=1 ", "Code", false, dealerCodeStrsNeedCheck, getDbDealers);
                    foreach(var tempRight in tempRightList) {
                        var dealer = dbDealers.FirstOrDefault(v => v.DealerName == tempRight.DealerName && v.DealerCode == tempRight.DealerCode);
                        if(dealer == null) {
                            tempRight.ErrorMsg = "服务站不存在";
                        } else {
                            tempRight.DealerId = dealer.DealerId;
                        }
                    }

                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();
                    //校验市场部是否存在
                    var MarketingDepartmentNeedCheck = tempRightList.Select(r => r.DealerId.ToString()).Distinct().ToArray();
                    var dbMarketingDepartments = new List<DealerPerTrainAutExtend>();
                    Func<string[], bool> getDbMarketingDepartmentns = value => {
                        var dbObj = new DealerPerTrainAutExtend {
                            MarketingDepartmentId = Convert.ToInt32(value[0]),
                            DealerId = Convert.ToInt32(value[1]),
                            MPartsSalesCategoryId = Convert.ToInt32(value[2])
                        };
                        dbMarketingDepartments.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select MarketingDepartmentId,DealerId,partssalescategoryid from DealerServiceInfo where status=1 ", "DealerId", false, MarketingDepartmentNeedCheck, getDbMarketingDepartmentns);
                    foreach(var tempObj in tempRightList) {
                        var marketingDepartment = dbMarketingDepartments.FirstOrDefault(v => v.DealerId == tempObj.DealerId && v.MPartsSalesCategoryId == tempObj.PartsSalesCategoryId);
                        if(marketingDepartment == null) {
                            tempObj.ErrorMsg = "该品牌下经销商分公司管理信息市场部不存在";
                        } else {
                            tempObj.MarketingDepartmentId = marketingDepartment.MarketingDepartmentId;
                        }
                    }

                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();
                    //岗位校验
                    var DealerKeyPositionNeedCheck = tempRightList.Select(r => r.PositionName).Distinct().ToArray();
                    var dbPositions = new List<DealerPerTrainAutExtend>();
                    Func<string[], bool> getDbPosition = value => {
                        var dbObj = new DealerPerTrainAutExtend {
                            PositionId = Convert.ToInt32(value[0]),
                            PositionName = value[1]
                        };
                        dbPositions.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select Id,PositionName from DealerKeyPosition where status=1 and BranchId=" + userinfo.EnterpriseId, "PositionName", false, DealerKeyPositionNeedCheck, getDbPosition);
                    foreach(var tempRight in tempRightList) {
                        var dealerKeyPosition = dbPositions.FirstOrDefault(v => v.PositionName == tempRight.PositionName);
                        if(dealerKeyPosition == null) {
                            tempRight.ErrorMsg = "关键岗位不存在";
                        } else {
                            tempRight.PositionId = dealerKeyPosition.PositionId;
                        }
                    }

                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();
                    //人员校验
                    var DealerKeyEmployeeNeedCheck = tempRightList.Select(r => r.Name).Distinct().ToArray();
                    var dbDealerKeyEmployee = new List<DealerPerTrainAutExtend>();
                    Func<string[], bool> getDbDealerKeyEmployee = value => {
                        var dbObj = new DealerPerTrainAutExtend {
                            PPartsSalesCategoryId = Convert.ToInt32(value[0]),
                            PDealerId = Convert.ToInt32(value[1]),
                            Name = value[2]
                        };
                        dbDealerKeyEmployee.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select PartsSalesCategoryId,DealerId,Name from DealerKeyEmployee where status=1 ", "Name", false, DealerKeyEmployeeNeedCheck, getDbDealerKeyEmployee);
                    foreach(var tempRight in tempRightList) {
                        var dealerKeyEmployee = dbDealerKeyEmployee.FirstOrDefault(v => v.Name == tempRight.Name && v.PPartsSalesCategoryId == tempRight.PartsSalesCategoryId && v.PDealerId == tempRight.DealerId);
                        if(dealerKeyEmployee == null) {
                            tempRight.ErrorMsg = "关键岗位人员不存在";
                        }
                    }

                    //tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();
                    ////培训类型
                    //var TrainingTypeNeedCheck = tempRightList.Select(r => r.TrainingTypeName).Distinct().ToArray();
                    //var dbTrainingType = new List<DealerPerTrainAutExtend>();
                    //Func<string[], bool> getDbTrainingType = value => {
                    //    var dbObj = new DealerPerTrainAutExtend {
                    //        TrainingType = Convert.ToInt32(value[0]),
                    //        TrainingTypeName = value[1]
                    //    };
                    //    dbTrainingType.Add(dbObj);
                    //    return false;
                    //};
                    //db.QueryDataWithInOperator("select Id,TrainingTypeName from TrainingType where BranchId=" + userinfo.EnterpriseId, "TrainingTypeName", false, TrainingTypeNeedCheck, getDbTrainingType);
                    //foreach(var tempRight in tempRightList) {
                    //    var trainingType = dbTrainingType.FirstOrDefault(v => v.TrainingTypeName == tempRight.TrainingTypeName);
                    //    if(trainingType == null) {
                    //        if(!string.IsNullOrEmpty(tempRight.TrainingTypeName)) {
                    //            tempRight.ErrorMsg = "培训类型不存在";
                    //        }
                    //    } else {
                    //        tempRight.TrainingType = trainingType.TrainingType;
                    //    }
                    //}

                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();
                    //认证类型
                    var AuthenticationTypeNeedCheck = tempRightList.Select(r => r.AuthenticationTypeName).Distinct().ToArray();
                    var dbAuthenticationType = new List<DealerPerTrainAutExtend>();
                    Func<string[], bool> getDbAuthenticationType = value => {
                        var dbObj = new DealerPerTrainAutExtend {
                            AuthenticationType = Convert.ToInt32(value[0]),
                            AuthenticationTypeName = value[1]
                        };
                        dbAuthenticationType.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select Id,AuthenticationTypeName from AuthenticationType where status=1 and BranchId=" + userinfo.EnterpriseId, "AuthenticationTypeName", false, AuthenticationTypeNeedCheck, getDbAuthenticationType);
                    foreach(var tempRight in tempRightList) {
                        var authenticationType = dbAuthenticationType.FirstOrDefault(v => v.AuthenticationTypeName == tempRight.AuthenticationTypeName);
                        if(authenticationType == null) {
                            if(!string.IsNullOrEmpty(tempRight.AuthenticationTypeName)) {
                                tempRight.ErrorMsg = "认证类型不存在";
                            }
                        } else {
                            tempRight.AuthenticationType = authenticationType.AuthenticationType;
                        }
                    }

                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();
                    //培训课程
                    var TrainingNameNeedCheck = tempRightList.Select(r => r.TrainingName).Distinct().ToArray();
                    var dbTrainingName = new List<DealerPerTrainAutExtend>();
                    Func<string[], bool> getDbTrainingName = value => {
                        var dbObj = new DealerPerTrainAutExtend {
                            TrainingType = Convert.ToInt32(value[0]),
                            TrainingName = value[1]
                        };
                        dbTrainingName.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select TrainingTypeName,TrainingName from TrainingType where status=1 and BranchId =" + userinfo.EnterpriseId, "TrainingName", true, TrainingNameNeedCheck, getDbTrainingName);
                    foreach(var tempRight in tempRightList) {
                        if(!dbTrainingName.Any(v => v.TrainingName == tempRight.TrainingName && v.TrainingType == tempRight.TrainingType)) {
                            tempRight.ErrorMsg = "不存在此培训类型对应的培训课程";
                        }
                    }

                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();

                    #endregion
                    //获取所有不合格数据
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    //获取所有合格数据
                    rightList = allList.Except(errorList).ToList();
                }

                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempObj = list[index - 1];
                            var values = new object[] {
                                tempObj.PartsSalesCategoryName,
                                tempObj.MarketingDepartmentName,
                                tempObj.DealerCode,
                                tempObj.DealerName,
                                tempObj.PositionName,
                                tempObj.Name,
                                tempObj.AuthenticationTypeName,
                                tempObj.AuthenticationTimeStr,
                                tempObj.TrainingTypeName,
                                tempObj.TrainingTimeStr,
                                tempObj.CertificateId,
                                tempObj.TrainingName,
                                tempObj.IdCard,
                                tempObj.ErrorMsg
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }

                //导入所有合格数据
                if(!rightList.Any())
                    return true;
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    //开启事务，新增更新配件在一个事务内
                    var ts = conn.BeginTransaction();
                    try {
                        //新增配件
                        if(rightList.Any()) {
                            //获取新增数据的sql语句，Id为主键
                            var sqlInsert = db.GetInsertSql("DealerPerTrainAut", "Id", new[] {"BranchId","PartsSalesCategoryId","MarketingDepartmentId","DealerId","DealerCode","DealerName","Name","PositionId","TrainingType","TrainingTime","AuthenticationType","AuthenticationTime","CertificateId","TrainingName","IdCard","Status","CreateTime","CreatorId","CreatorName"
                            });

                            var userInfo = Utils.GetCurrentUserInfo();
                            //往数据库增加配件信息
                            foreach(var item in rightList) {
                                var command = db.CreateDbCommand(sqlInsert, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("BranchId", userinfo.EnterpriseId));
                                command.Parameters.Add(db.CreateDbParameter("PartsSalesCategoryId", item.PartsSalesCategoryId));
                                command.Parameters.Add(db.CreateDbParameter("MarketingDepartmentId", item.MarketingDepartmentId));
                                command.Parameters.Add(db.CreateDbParameter("DealerId", item.DealerId));
                                command.Parameters.Add(db.CreateDbParameter("DealerCode", item.DealerCode));
                                command.Parameters.Add(db.CreateDbParameter("DealerName", item.DealerName));
                                command.Parameters.Add(db.CreateDbParameter("Name", item.Name));
                                command.Parameters.Add(db.CreateDbParameter("PositionId", item.PositionId));
                                command.Parameters.Add(db.CreateDbParameter("TrainingType", item.TrainingType));
                                if(item.TrainingTime.HasValue) {
                                    command.Parameters.Add(db.CreateDbParameter("TrainingTime", item.TrainingTime));
                                } else {
                                    command.Parameters.Add(db.CreateDbParameter("TrainingTime", DBNull.Value));
                                }
                                if(item.AuthenticationTime.HasValue) {
                                    command.Parameters.Add(db.CreateDbParameter("AuthenticationTime", item.AuthenticationTime.Value));
                                } else {
                                    command.Parameters.Add(db.CreateDbParameter("AuthenticationTime", DBNull.Value));
                                }
                                if(item.AuthenticationType.HasValue) {
                                    command.Parameters.Add(db.CreateDbParameter("AuthenticationType", item.AuthenticationType.Value));
                                } else {
                                    command.Parameters.Add(db.CreateDbParameter("AuthenticationType", DBNull.Value));
                                }
                                command.Parameters.Add(db.CreateDbParameter("CertificateId", item.CertificateId));
                                command.Parameters.Add(db.CreateDbParameter("TrainingName", item.TrainingName));
                                if(!string.IsNullOrEmpty(item.IdCard)) {
                                    command.Parameters.Add(db.CreateDbParameter("IdCard", item.IdCard));
                                } else {
                                    command.Parameters.Add(db.CreateDbParameter("IdCard", DBNull.Value));
                                }

                                command.Parameters.Add(db.CreateDbParameter("Status", (int)DcsBaseDataStatus.有效));
                                command.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                command.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                command.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                var tempId = db.ExecuteInsert(command, "Id");
                            }
                        }
                        //无异常提交
                        ts.Commit();
                    } catch(Exception) {
                        //报错回滚
                        ts.Rollback();
                        throw;
                    } finally {
                        if(conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }

    }
}
