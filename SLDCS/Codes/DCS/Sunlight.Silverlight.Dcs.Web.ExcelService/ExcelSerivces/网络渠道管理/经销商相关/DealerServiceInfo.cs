﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        //导出经销商分品牌管理信息
        public bool ExportDealerServiceInfo(int? dealerServiceInfoId, int branchId, string dealerCode, string dealerName, string businessCode, string businessName, int? partsSalesCategoryId, string marketingDepartmentName, int? usedPartsWarehouseId, DateTime? createTimeBegin, DateTime? createTimeEnd, int? status, int companyType, out string fileName) {
            fileName = GetExportFilePath("服务站专卖店分品牌信息管理_" + ".xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated) {
                    throw new Exception(ErrorStrings.File_Validation_User);
                }
                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.Append(this.GetSqlByType(companyType, branchId));
                    var dbParameters = new List<DbParameter>();
                    if(dealerServiceInfoId.HasValue) {
                        sql.Append(@" and a.Id = {0}dealerServiceInfoId ");
                        dbParameters.Add(db.CreateDbParameter("dealerServiceInfoId", dealerServiceInfoId.Value));
                    } else {
                        if(!string.IsNullOrEmpty(dealerCode)) {
                            sql.Append(@" and Upper(b.Code) like Upper({0}dealerCode)");
                            dbParameters.Add(db.CreateDbParameter("dealerCode", "%" + dealerCode + "%"));
                        }
                        if(!string.IsNullOrEmpty(dealerName)) {
                            sql.Append(@" and b.Name like {0}dealerName");
                            dbParameters.Add(db.CreateDbParameter("dealerName", "%" + dealerName + "%"));
                        }
                        if(!string.IsNullOrEmpty(businessCode)) {
                            sql.Append(@" and Upper(a.businessCode) like Upper({0}businessCode)");
                            dbParameters.Add(db.CreateDbParameter("businessCode", "%" + businessCode + "%"));
                        }
                        if(!string.IsNullOrEmpty(businessName)) {
                            sql.Append(@" and a.businessName like {0}businessName");
                            dbParameters.Add(db.CreateDbParameter("businessName", "%" + businessName + "%"));
                        }
                        if(partsSalesCategoryId.HasValue) {
                            sql.Append(@" and a.partsSalesCategoryId = {0}partsSalesCategoryId ");
                            dbParameters.Add(db.CreateDbParameter("partsSalesCategoryId", partsSalesCategoryId.Value));
                        }
                        if(!string.IsNullOrEmpty(marketingDepartmentName)) {
                            sql.Append(@" and f.Name like {0}marketingDepartmentName");
                            dbParameters.Add(db.CreateDbParameter("marketingDepartmentName", "%" + marketingDepartmentName + "%"));
                        }
                        if(usedPartsWarehouseId.HasValue) {
                            sql.Append(@" and a.usedPartsWarehouseId = {0}usedPartsWarehouseId ");
                            dbParameters.Add(db.CreateDbParameter("usedPartsWarehouseId", usedPartsWarehouseId.Value));
                        }
                        if(status.HasValue) {
                            sql.Append(@" and a.status = {0}status ");
                            dbParameters.Add(db.CreateDbParameter("status", status.Value));
                        }
                        if(createTimeBegin.HasValue) {
                            sql.Append(" and a.createTime>=to_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(" and a.createTime<=to_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                    }

                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var execlExport = new ExcelExport(fileName)) {
                        execlExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    ErrorStrings.Export_Title_Dealerserviceinfo_Name, 
                                    ErrorStrings.Export_Title_Partssalescategory_Name, 
                                    ErrorStrings.Export_Title_Dealer_DealerCode,
                                    ErrorStrings.Export_Title_Dealer_DealerName, 
                                    ErrorStrings.Export_Title_BranchSupplierRelation_BusinessCode, 
                                    ErrorStrings.Export_Title_BranchSupplierRelation_BusinessName, 
                                    ErrorStrings.Export_Title_Agency_Abbreviation,
                                    ErrorStrings.Export_Title_Branch_BranchName,
                                    ErrorStrings.Export_Title_Dealerserviceinfo_BranchName, 
                                    ErrorStrings.Export_Title_Agency_MarketingDepartmentName, 
                                    ErrorStrings.Export_Title_Dealerserviceinfo_DealerType,
                                    ErrorStrings.Export_Title_Dealerserviceinfo_Area,
                                    ErrorStrings.Export_Title_Dealerserviceinfo_Regiontype, 
                                    ErrorStrings.Export_Title_Dealerserviceinfo_ServicePermission,
                                    ErrorStrings.Export_Title_Dealerserviceinfo_AccreditTime,
                                    ErrorStrings.Export_Title_Dealerserviceinfo_HotLine,
                                    ErrorStrings.Export_Title_Dealerserviceinfo_HourPhone24,
                                    ErrorStrings.Export_Title_Dealer_ContactPhone, 
                                    ErrorStrings.Export_Title_Agency_Fax,
                                    ErrorStrings.Export_Title_Dealerserviceinfo_WarehouseName,
                                    ErrorStrings.Export_Title_Dealerserviceinfo_SaleandServicesiteLayout,
                                    ErrorStrings.Export_Title_Dealerserviceinfo_PartReserveAmount, 
                                    ErrorStrings.Export_Title_Dealerserviceinfo_OldName, 
                                    ErrorStrings.Export_Title_Dealerserviceinfo_MaterialCostInvoiceType,
                                    ErrorStrings.Export_Title_Dealerserviceinfo_InvoiceTypeInvoiceRatio,
                                    ErrorStrings.Export_Title_Dealerserviceinfo_LaborCostCostInvoiceType,
                                    ErrorStrings.Export_Title_Dealerserviceinfo_LaborCostInvoiceRatio,


                                      ErrorStrings.Export_Title_AccountPeriod_Status,
                                    ErrorStrings.Export_Title_AccountPeriod_CreatorName, 
                                    ErrorStrings.Export_Title_Dealerserviceinfo_CreateTime, 
                                    ErrorStrings.Export_Title_AccountPeriod_ModifierName, 
                                    ErrorStrings.Export_Title_Dealerserviceinfo_ModifyTime                                                             
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }

        private string GetSqlByType(int companyType, int branchId) {
            if(companyType == (int)DcsCompanyType.分公司) {
                var userInfo = Utils.GetCurrentUserInfo();

                return string.Format(@"Select 
                                        cc.Name,
                                        d.Name,
                                        b.Code,
                                        b.Name,
                                        a.BusinessCode,
                                        a.BusinessName,
                                        b.Shortname, 
                                        c.name,
                                        a.Businessdivision,
                                        f.Name,
                                        cast( Case a.Servicestationtype
                                          When 1 Then
                                           '模拟站'
                                          When 2 Then
                                           '星级服务中心'
                                          When 3 Then
                                           '服务站'
                                          When 4 Then
                                           '快修店'
                                          When 5 Then
                                           '服务工程师'
                                          When 6 Then
                                           '标杆服务站'
                                          When 7 Then
                                           '自保站'
                                          When 8 Then
                                           '大客户自保站'
                                        End as varchar2(10)),
                                        a.Area,
                                        a.Regiontype,
                                        a.ServicePermission,
                                        a.AccreditTime,
                                        a.HotLine,
                                        a.HourPhone24,
                                        a.Fix,
                                        a.Fax,
                                        j.Name,
                                        cast(case a.SaleandServicesiteLayout When 1 Then '前后' When 2 Then '左右' When 3 Then '分离' End as varchar2(10)),
                                        a.PartReserveAmount,
                                        e.Name,
                                        cast(case a.MaterialCostInvoiceType When 1 Then '增值税发票' When 2 Then '普通发票' End as varchar2(10)),
                                        a.InvoiceTypeInvoiceRatio,
                                        cast(case a.LaborCostCostInvoiceType When 1 Then '增值税发票' When 2 Then '普通发票' End as varchar2(10)),
                                        a.LaborCostInvoiceRatio,
                                       -- cc.Name,
                                       cast(Decode(a.Status, 1, '有效', 2, '停用',99, '作废')as varchar2(10)),
                                        a.Creatorname,
                                        a.Createtime,
                                        a.Modifiername,
                                       a.Modifytime
                                  From Dealerserviceinfo a
                                  Inner  Join PersonSalesCenterLink ps
                                    On  a.PartsSalesCategoryId = ps.PartsSalesCategoryId
                                  Left Outer Join Dealer b
                                    On a.Dealerid = b.Id
                                  Left Outer Join Branch c
                                    On a.Branchid = c.Id
                                  Left Outer Join Partssalescategory d
                                    On a.Partssalescategoryid = d.Id
                                  Left Outer Join Usedpartswarehouse e
                                    On a.Usedpartswarehouseid = e.Id
                                  Left Outer Join Marketingdepartment f
                                    On a.Marketingdepartmentid = f.Id
                                  Left Outer Join Warehouse j
                                    On a.Warehouseid = j.Id
                                  Left Outer Join ChannelCapability cc
                                    On a.ChannelCapabilityId = cc.Id
                                 Where a.Branchid = {0} and ps.PersonId ={1} ", branchId, userInfo.Id);
            } else {
                // Decode(a.Status, 1, '有效', 2, '停用',99, '作废'),
                return string.Format(@"Select 
                                        cc.Name,
                                        d.Name,
                                        b.Code,
                                        b.Name,
                                        a.BusinessCode,
                                        a.BusinessName,
                                        b.Shortname,
                                       -- cast(Decode(a.Status, 1, '有效', 2, '停用',99, '作废')as varchar2(10)),                                 
                                        a.Businessdivision,
                                        f.Name,
                                        cast(Case a.Servicestationtype
                                          When 1 Then
                                           '模拟站'
                                          When 2 Then
                                           '星级服务中心'
                                          When 3 Then
                                           '服务站'
                                          When 4 Then
                                           '快修店'
                                          When 5 Then
                                           '服务工程师'
                                          When 6 Then
                                           '标杆服务站'
                                          When 7 Then
                                           '自保站'
                                          When 8 Then
                                           '大客户自保站'
                                        End as varchar2(10)),
                                        a.Area,
                                        a.Regiontype,
                                        a.ServicePermission,
                                        a.AccreditTime,
                                        a.HotLine,
                                         a.HourPhone24,
                                        a.Fix,
                                        a.Fax,
                                        j.Name,
                                        cast(case a.SaleandServicesiteLayout When 1 Then '前后' When 2 Then '左右' When 3 Then '分离' End as varchar2(10)),
                                        a.PartReserveAmount,
                                        e.Name,
                                        cast(case a.MaterialCostInvoiceType When 1 Then '增值税发票' When 2 Then '普通发票' End as varchar2(10)),
                                        a.InvoiceTypeInvoiceRatio,
                                        cast(case a.LaborCostCostInvoiceType When 1 Then '增值税发票' When 2 Then '普通发票' End as varchar2(10)),
                                        a.LaborCostInvoiceRatio,
                                       cast(Decode(a.Status, 1, '有效', 2, '停用',99, '作废')as varchar2(10)), 
                                       -- a.Remark,
                                      --  cc.Name,
                                        a.Creatorname,
                                        a.Createtime,
                                       a.Modifiername,
                                        a.Modifytime
                                  From Dealerserviceinfo a
                                  Inner  Join PersonSalesCenterLink ps
                                    On  a.PartsSalesCategoryId = ps.PartsSalesCategoryId
                                  Left Outer Join Dealer b
                                    On a.Dealerid = b.Id
                                  Left Outer Join Branch c
                                    On a.Branchid = c.Id
                                  Left Outer Join Partssalescategory d
                                    On a.Partssalescategoryid = d.Id
                                  Left Outer Join Usedpartswarehouse e
                                    On a.Usedpartswarehouseid = e.Id
                                  Left Outer Join Marketingdepartment f
                                    On a.Marketingdepartmentid = f.Id
                                  Left Outer Join Warehouse j
                                    On a.Warehouseid = j.Id
                                  Left Outer Join ChannelCapability cc
                                    On a.ChannelCapabilityId = cc.Id
                                 Where a.Branchid = {0} ", branchId);
            }
        }

        public bool ImportDealerServiceInfo(string fileName, out int excelImportNum, out List<DealerBusinessPermitExtend> rightData, out List<DealerBusinessPermitExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<DealerBusinessPermitExtend>();
            var rightList = new List<DealerBusinessPermitExtend>();
            var allList = new List<DealerBusinessPermitExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("DealerBusinessPermit", out notNullableFields, out fieldLenght);
                List<object> excelColumns;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    //服务站编号、服务站名称、品牌、服务产品线编号、服务产品线名称、产品线类型、工时单价等级,其中服务站编号、品牌、服务产品线编号、产品线类型、工时单价等级导入模板必填
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_Code, "DealerCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_Name, "DealerName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Partssalescategory_Name, "PartsSalesCategoryName");
                    excelOperator.AddColumnDataSource("服务产品线编号", "ProductLineCode");
                    excelOperator.AddColumnDataSource("服务产品线名称", "ProductLineName");
                    excelOperator.AddColumnDataSource("产品线类型", "ProductLineType");
                    excelOperator.AddColumnDataSource("发动机维修权限", "EngineRepairPower");
                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;

                    //单次最大导入条数不能大于300！
                    if(excelOperator.LastRowNum > 300) {
                        throw new Exception("单次最大导入条数不能大于300！");
                    }

                    #region 获取对应枚举
                    var keyValuePairs = new[] {
                        new KeyValuePair<string, string>("ProductLineType", "ServiceProductLineView_ProductLineType"),
                        new KeyValuePair<string,string>("EngineRepairPower","EngineRepairPower")
                    };
                    tempExcelOperator.ImportHelper.LoadEnumFromDb(null, null, keyValuePairs);
                    #endregion

                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var tempImportObj = new DealerBusinessPermitExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);

                        #region
                        tempImportObj.DealerCode = newRow["DealerCode"];
                        tempImportObj.DealerName = newRow["DealerName"];
                        tempImportObj.PartsSalesCategoryName = newRow["PartsSalesCategoryName"];
                        tempImportObj.ProductLineCode = newRow["ProductLineCode"];
                        tempImportObj.ProductLineName = newRow["ProductLineName"];
                        tempImportObj.ProductLineTypeStr = newRow["ProductLineType"];
                        tempImportObj.EngineRepairPowerStr = newRow["EngineRepairPower"];
                        #endregion

                        var tempErrorMessage = new List<string>();

                        #region 导入的内容基本检查
                        //服务站编号、品牌、服务产品线编号、产品线类型、工时单价等级导入模板必填
                        //品牌检查
                        if(string.IsNullOrEmpty(tempImportObj.DealerCode)) {
                            tempErrorMessage.Add("服务站编号不能为空");
                        }

                        if(string.IsNullOrEmpty(tempImportObj.PartsSalesCategoryName)) {
                            tempErrorMessage.Add(ErrorStrings.Export_Validation_MarketingDepartment_PartsSalesCategoryIsNull);
                        }

                        if(string.IsNullOrEmpty(tempImportObj.ProductLineCode)) {
                            tempErrorMessage.Add("服务产品线编号不能为空");
                        }
                        if(string.IsNullOrEmpty(tempImportObj.ProductLineTypeStr)) {
                            tempErrorMessage.Add("产品线类型不能为空");
                        } else {
                            try {
                                tempImportObj.ProductLineType = (int)Enum.Parse(typeof(DcsServiceProductLineViewProductLineType), tempImportObj.ProductLineTypeStr);
                            } catch(Exception) {
                                tempErrorMessage.Add("产品线类型不正确");
                            }
                        }

                        #endregion

                        if(tempErrorMessage.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });

                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    var groups = tempRightList.GroupBy(r => new {
                        r.DealerCode,
                        r.PartsSalesCategoryName,
                        r.ProductLineCode,
                        r.ProductLineType
                    }).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        groupItem.ErrorMsg = ErrorStrings.ImpPartsSupplierRelation_Validation31;
                    }
                    //4、查询配件销售类型（依据经销商分公司管理信息.经销商Id=经销商服务经营权限.经销商Id,配件销售类型id=经销商分公司管理信息.配件销售类型Id）校验品牌存在配件销售类型 否则提示：品牌不存在。
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();
                    var partsSalesCategoryNeedCheck = tempRightList.Select(r => r.PartsSalesCategoryName).Distinct().ToArray();
                    var dbPartsSalesCategories = new List<DealerBusinessPermitExtend>();
                    Func<string[], bool> getDbPartsSalesCategories = value => {
                        var dbObj = new DealerBusinessPermitExtend {
                            PartsSalesCategoryId = Convert.ToInt32(value[0]),
                            PartsSalesCategoryName = value[1]
                        };
                        dbPartsSalesCategories.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select id,name from PartsSalesCategory where status=1 ", "Name", true, partsSalesCategoryNeedCheck, getDbPartsSalesCategories);
                    foreach(var tempObj in tempRightList) {
                        var tempobjcheck = dbPartsSalesCategories.FirstOrDefault(r => r.PartsSalesCategoryName == tempObj.PartsSalesCategoryName);
                        if(tempobjcheck == null) {
                            tempObj.ErrorMsg = ErrorStrings.Export_Title_Partssalescategory_Name + tempObj.PartsSalesCategoryName + ErrorStrings.Export_Validation_Sparepart_NotExist;
                        } else {
                            tempObj.PartsSalesCategoryId = tempobjcheck.PartsSalesCategoryId;
                        }
                    }
                    //1、查询经销商基本信息,(依据 经销商服务经营权限.经销商Id=经销商基本信息.Id)，校验经销商基本信息.经销商编号,名称存在，否则提示：经销商编号，经销商名称不存在。
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();
                    var dealerCodeNeedCheck = tempRightList.Select(r => r.DealerCode).Distinct().ToArray();
                    var dbDealers = new List<DealerBusinessPermitExtend>();
                    Func<string[], bool> getDbDealers = value => {
                        var dbObj = new DealerBusinessPermitExtend {
                            DealerId = Convert.ToInt32(value[0]),
                            DealerCode = value[1],
                            DealerName = value[2]
                        };
                        dbDealers.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperatorByLower("select id,code,name from dealer where status=1 ", "Code", true, dealerCodeNeedCheck, getDbDealers);
                    foreach(var tempObj in tempRightList) {
                        var tempobjcheck = dbDealers.FirstOrDefault(r => r.DealerCode == tempObj.DealerCode);
                        if(tempobjcheck == null) {
                            tempObj.ErrorMsg = tempObj.DealerCode + ErrorStrings.Export_Validation_PartsSpecialTreatyPrice_Validation1;
                        } else {
                            tempObj.DealerId = tempobjcheck.DealerId;
                        }
                    }
                    //2、查询服务产品线视图（依据经销商服务经营权限.服务产品线Id=服务产品线（视图）.Id）服务产品线编号、服务产品线名称组合存在于服务产品线（视图），否则提示：服务产品线编号对应的服务产品线名称不存在。
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();
                    var productLineCodeNeedCheck = tempRightList.Select(r => r.ProductLineCode).Distinct().ToArray();
                    var dbProductLines = new List<DealerBusinessPermitExtend>();
                    Func<string[], bool> getDbProductLines = value => {
                        var dbObj = new DealerBusinessPermitExtend {
                            ProductLineId = Convert.ToInt32(value[0]),
                            ProductLineCode = value[1],
                            ProductLineName = value[2],
                            ProductLineType = Convert.ToInt32(value[3]),
                            BranchId = Convert.ToInt32(value[4]),
                            PartsSalesCategoryId = Convert.ToInt32(value[5]),
                        };
                        dbProductLines.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select ProductLineid,ProductLinecode,ProductLinename,ProductLineType,branchid,PartsSalesCategoryId from ServiceProductLineView", "ProductLinecode", true, productLineCodeNeedCheck, getDbProductLines);
                    foreach(var tempObj in tempRightList) {
                        var tempobjcheck = dbProductLines.FirstOrDefault(r => r.ProductLineCode == tempObj.ProductLineCode && r.ProductLineType == tempObj.ProductLineType);
                        if(tempobjcheck == null) {
                            tempObj.ErrorMsg = "产品线信息不存在";
                        } else {
                            tempObj.ProductLineId = tempobjcheck.ProductLineId;
                            tempObj.BranchId = tempobjcheck.BranchId;

                            //判断如果是发动机产品线时  增加 发动机维修权限的判断
                            if(tempObj.ProductLineType == (int)DcsServiceProductLineViewProductLineType.发动机产品线 && (tempObj.ProductLineCode == "OM-FDJ-ISGFGL" || tempObj.ProductLineCode == "OM-FDJ-ISGGL")) {
                                if(string.IsNullOrEmpty(tempObj.EngineRepairPowerStr)) {
                                    tempObj.ErrorMsg = "经营权限的产品线类型为发动机产品线并且产品线编号为 【OM-FDJ-ISGFGL】 或者 【OM-FDJ-ISGGL】 时，发动机维修权限不能为空";
                                } else {
                                    var tempEnumValue = tempExcelOperator.ImportHelper.GetEnumValue("EngineRepairPower", tempObj.EngineRepairPowerStr);
                                    if(!tempEnumValue.HasValue) {
                                        tempObj.ErrorMsg = "发动机维修权限不正确";
                                    } else {
                                        tempObj.EngineRepairPower = tempEnumValue.Value;
                                    }
                                }
                            }
                        }
                    }
                    //企业分品牌信息不存在（有效的）  提示 企业分品牌信息不存在
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();
                    //var dealerIdNeedCheck = tempRightList.Select(r => r.DealerId.ToString()).Distinct().ToArray();
                    //var dbDealerServiceInfos = new List<DealerServiceInfoExtend>();
                    //Func<string[], bool> getDbDealerServiceInfos = value => {
                    //    var dbObj = new DealerServiceInfoExtend {
                    //        Id = Convert.ToInt32(value[0]),
                    //        DealerId = Convert.ToInt32(value[1]),
                    //        PartsSalesCategoryId = Convert.ToInt32(value[2]),
                    //    };
                    //    dbDealerServiceInfos.Add(dbObj);
                    //    return false;
                    //};
                    //db.QueryDataWithInOperator("select Id,DealerId,PartsSalesCategoryId from DealerServiceInfo where status=1  ", "DealerId", true, dealerIdNeedCheck, getDbDealerServiceInfos);
                    //foreach(var tempObj in tempRightList) {
                    //    var tempobjcheck = dbDealerServiceInfos.FirstOrDefault(r => r.DealerId == tempObj.DealerId && r.PartsSalesCategoryId == tempObj.PartsSalesCategoryId);
                    //    if(tempobjcheck == null) {
                    //        tempObj.ErrorMsg = "企业分品牌信息不存在";
                    //    }
                    //}

                    //品牌下是否存在服务产品线
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();
                    var artsSalesCategoryNeedCheck = tempRightList.Select(r => r.PartsSalesCategoryId.ToString()).Distinct().ToArray();
                    var dbPartsSalesCategorys = new List<DealerBusinessPermitExtend>();
                    Func<string[], bool> getDbPartsSalesCategorys = value => {
                        var dbObj = new DealerBusinessPermitExtend {
                            ProductLineId = Convert.ToInt32(value[0]),
                            PartsSalesCategoryId = Convert.ToInt32(value[1]),
                        };
                        dbPartsSalesCategorys.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select ProductLineId,PartsSalesCategoryId from ServiceProductLineView ", "PartsSalesCategoryId", true, artsSalesCategoryNeedCheck, getDbPartsSalesCategorys);
                    foreach(var tempObj in tempRightList) {
                        if(!dbPartsSalesCategorys.Any(r => r.ProductLineId == tempObj.ProductLineId && r.PartsSalesCategoryId == tempObj.PartsSalesCategoryId)) {
                            tempObj.ErrorMsg = ErrorStrings.Export_Title_Partssalescategory_Name + tempObj.PartsSalesCategoryName + "不存在产品线" + tempObj.ProductLineCode;
                        }
                    }


                    //3、查询工时单价等级 （依据经销商服务经营权限.工时单价等级Id=工时单价等级.Id）校验工时单价等级名称存在，否则提示：工时单价等级名称不存在。
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();
                    var dbLaborHourUnitPriceGrades = new List<DealerBusinessPermitExtend>();
                    Func<string[], bool> getDbLaborHourUnitPriceGrades = value => {
                        var dbObj = new DealerBusinessPermitExtend {
                            LaborHourUnitPriceGradeId = Convert.ToInt32(value[0]),
                            BranchId = Convert.ToInt32(value[1]),
                        };
                        dbLaborHourUnitPriceGrades.Add(dbObj);
                        return false;
                    };

                    var tempRightListss = tempRightList.GroupBy(p => new {
                        p.DealerId,
                        p.ProductLineId
                    }).Where(p => p.Count() > 1).SelectMany(p => p);
                    foreach(var dbtempRightList in tempRightListss) {
                        dbtempRightList.ErrorMsg = "维修权限不可重复";
                        //errorImportSpareParts.Add(dbtempRightList);
                        tempRightList.Remove(dbtempRightList);
                    }


                    ////5、服务站编号+品牌+服务产品线编号+产品线类型
                    //tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();
                    //var dealerBusinessPermitNeedCheck = tempRightList.Select(r => r.DealerId.ToString()).Distinct().ToArray();
                    //var dbDealerBusinessPermits = new List<DealerBusinessPermitExtend>();
                    //Func<string[], bool> getDbDealerBusinessPermits = value => {
                    //    var dbObj = new DealerBusinessPermitExtend {
                    //        ProductLineId = Convert.ToInt32(value[0]),
                    //        DealerId = Convert.ToInt32(value[1])
                    //    };
                    //    dbDealerBusinessPermits.Add(dbObj);
                    //    return false;
                    //};
                    //db.QueryDataWithInOperator("select ServiceProductLineId,DealerId from DealerBusinessPermit ", "DealerId", true, dealerBusinessPermitNeedCheck, getDbDealerBusinessPermits);
                    //foreach(var tempObj in tempRightList) {
                    //    if(dbDealerBusinessPermits.Any(r => r.DealerId == tempObj.DealerId && r.ProductLineId == tempObj.ProductLineId)) {
                    //        tempObj.ErrorMsg = "维修权限不可重复";
                    //    }
                    //}
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    rightList = allList.Except(errorList).ToList();
                }

                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempObj = list[index - 1];
                            var values = new object[] {
                                tempObj.DealerCode, tempObj.DealerName, tempObj.PartsSalesCategoryName, tempObj.ProductLineCode, tempObj.ProductLineName, tempObj.ProductLineTypeStr, tempObj.EngineRepairPowerStr, tempObj.ErrorMsg
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }

                //导入所有合格数据
                if(!rightList.Any())
                    return true;
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    //开启事务，新增更新配件在一个事务内
                    List<string> deleteIds = new List<string>();
                    var ts = conn.BeginTransaction();
                    try {
                        if(rightList.Any()) {

                            //string sqlDelete = "Delete from DealerBusinessPermit where DealerId =:DealerId  And  ServiceProductLineId =:ServiceProductLineId";
                            string sqlDelete = "Delete from DealerBusinessPermit where DealerId =:DealerId and exists(select 1 from serviceproductlineview a where a.ProductLineId=dealerbusinesspermit.serviceproductlineid and a.Partssalescategoryid=:PartsSalesCategoryId)";
                            foreach(var item in rightList) {
                                var command = db.CreateDbCommand(sqlDelete, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("DealerId", item.DealerId));
                                command.Parameters.Add(db.CreateDbParameter("PartsSalesCategoryId", item.PartsSalesCategoryId));
                                command.ExecuteScalar();
                            }

                            var sqlInsert = db.GetInsertSql("DealerBusinessPermit", "Id", new[] {
                                "DealerId", "ServiceProductLineId", "ProductLineType", "BranchId", "ServiceFeeGradeId", "EngineRepairPower","CreateTime", "CreatorId", "CreatorName"
                            });

                            //记录变更履历
                            string sqlInsertDealerBusinessPermitHistory = db.GetInsertSql("DealerBusinessPermitHistory", "Id", new[]{
                                "DealerBusinessPermitId","DealerId","ServiceProductLineId","BranchId","CreateTime","CreatorId","CreatorName","ProductLineType","EngineRepairPower","Status"
                            });

                            var userInfo = Utils.GetCurrentUserInfo();
                            foreach(var item in rightList) {
                                var command = db.CreateDbCommand(sqlInsert, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("DealerId", item.DealerId));
                                command.Parameters.Add(db.CreateDbParameter("BranchId", item.BranchId));
                                command.Parameters.Add(db.CreateDbParameter("ServiceProductLineId", item.ProductLineId));
                                command.Parameters.Add(db.CreateDbParameter("ProductLineType", item.ProductLineType));
                                command.Parameters.Add(db.CreateDbParameter("ServiceFeeGradeId", item.LaborHourUnitPriceGradeId));
                                command.Parameters.Add(db.CreateDbParameter("EngineRepairPower", item.EngineRepairPower));
                                command.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                command.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                command.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                var tempId = db.ExecuteInsert(command, "Id");

                                command = db.CreateDbCommand(sqlInsertDealerBusinessPermitHistory, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("DealerBusinessPermitId", tempId));
                                command.Parameters.Add(db.CreateDbParameter("BranchId", item.BranchId));
                                command.Parameters.Add(db.CreateDbParameter("DealerId", item.DealerId));
                                command.Parameters.Add(db.CreateDbParameter("ServiceProductLineId", item.ProductLineId));
                                command.Parameters.Add(db.CreateDbParameter("ProductLineType", item.ProductLineType));
                                command.Parameters.Add(db.CreateDbParameter("EngineRepairPower", item.EngineRepairPower));
                                command.Parameters.Add(db.CreateDbParameter("Status", (int)DcsDealerBusinessPermitHistoryStatus.新增));
                                command.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                command.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                command.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                db.ExecuteInsert(command, "Id");
                            }

                            //"ModifyTime", "ModifierId", "ModifierName
                            string sqlDeleteDealerServiceInfo = "update DealerServiceInfo set ModifyTime=:ModifyTime,ModifierId=:ModifierId,ModifierName=:ModifierName where DealerId =:DealerId and Partssalescategoryid=:PartsSalesCategoryId ";
                            foreach(var item in rightList) {
                                var command = db.CreateDbCommand(sqlDeleteDealerServiceInfo, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("ModifyTime", DateTime.Now));
                                command.Parameters.Add(db.CreateDbParameter("ModifierId", userInfo.Id));
                                command.Parameters.Add(db.CreateDbParameter("ModifierName", userInfo.Name));
                                command.Parameters.Add(db.CreateDbParameter("DealerId", item.DealerId));
                                command.Parameters.Add(db.CreateDbParameter("PartsSalesCategoryId", item.PartsSalesCategoryId));
                                command.ExecuteNonQuery();
                            }

                        }
                        //无异常提交
                        ts.Commit();
                    } catch(Exception) {
                        //报错回滚
                        ts.Rollback();
                        throw;
                    } finally {
                        if(conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }

        public bool 批量导入修改经销商分公司管理信息(string fileName, out int excelImportNum, out List<DealerServiceInfoExtend> rightData, out List<DealerServiceInfoExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<DealerServiceInfoExtend>();
            var rightList = new List<DealerServiceInfoExtend>();
            var allList = new List<DealerServiceInfoExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("DealerServiceInfo", out notNullableFields, out fieldLenght);
                List<object> excelColumns;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    #region
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_DealerCode, "DealerCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_DealerName, "DealerName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Partssalescategory_Name, "PartsSalesCategoryName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealerserviceinfo_Name, "ChannelCapability");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_BranchSupplierRelation_BusinessCode, "BusinessCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_BranchSupplierRelation_BusinessName, "BusinessName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Agency_Abbreviation, "ShortName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealerserviceinfo_BranchName, "Branch");
                    excelOperator.AddColumnDataSource("市场部", "MarketingDepartment");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealerserviceinfo_DealerType, "ServiceStationType");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealerserviceinfo_Area, "Area");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealerserviceinfo_Regiontype, "RegionType");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealerserviceinfo_ServicePermission, "ServicePermission");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealerserviceinfo_AccreditTime, "AccreditTime");
                    excelOperator.AddColumnDataSource("是否值守", "IsOnDuty");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealerserviceinfo_HotLine, "HotLine");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_ContactPhone, "Fix");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Agency_Fax, "Fax");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealerserviceinfo_WarehouseName, "Warehouse");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealerserviceinfo_SaleandServicesiteLayout, "SaleandServicesiteLayout");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealerserviceinfo_PartReserveAmount, "PartReserveAmount");
                    excelOperator.AddColumnDataSource("服务站维修权限", "RepairAuthorityGrade");
                    excelOperator.AddColumnDataSource("配件管理费率", "PartsManagingFeeGrade");
                    excelOperator.AddColumnDataSource("外出服务费等级", "OutFeeGrade");
                    excelOperator.AddColumnDataSource("星级", "Grade");
                    excelOperator.AddColumnDataSource("结算星级", "GradeCoefficient");
                    excelOperator.AddColumnDataSource("外出服务半径", "OutServiceradii");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealerserviceinfo_HourPhone24, "HourPhone24");
                    excelOperator.AddColumnDataSource("主干网络类型", "TrunkNetworkType");
                    excelOperator.AddColumnDataSource("是否中心站", "CenterStack");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealerserviceinfo_OldName, "UsedPartsWarehouse");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealerserviceinfo_MaterialCostInvoiceType, "MaterialCostInvoiceType");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealerserviceinfo_LaborCostCostInvoiceType, "LaborCostCostInvoiceType");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealerserviceinfo_LaborCostInvoiceRatio, "LaborCostInvoiceRatio");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealerserviceinfo_InvoiceTypeInvoiceRatio, "InvoiceTypeInvoiceRatio");
                    excelOperator.AddColumnDataSource("对外状态", "ExternalState");
                    #endregion

                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;

                    //单次最大导入条数不能大于300！
                    if(excelOperator.LastRowNum > 300) {
                        throw new Exception("单次最大导入条数不能大于300！");
                    }

                    #region 获取对应枚举
                    var keyValuePairs = new[] {
                        new KeyValuePair<string, string>("ServiceStationType", "DealerServiceInfo_ServiceStationType"),
                        new KeyValuePair<string, string>("SaleandServicesiteLayout", "SaleServiceLayout"), 
                        new KeyValuePair<string, string>("RepairAuthorityGrade", "ServicePermission"), 
                        new KeyValuePair<string, string>("MaterialCostInvoiceType", "Invoice_Type"), 
                        new KeyValuePair<string, string>("TrunkNetworkType", "DlrSerInfo_TrunkNetworkType"),  
                         new KeyValuePair<string, string>("CenterStack", "IsOrNot"), 
                          new KeyValuePair<string, string>("Grade", "Grade"), 
                    };
                    tempExcelOperator.ImportHelper.LoadEnumFromDb(null, null, keyValuePairs);
                    #endregion

                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var tempImportObj = new DealerServiceInfoExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);

                        #region
                        tempImportObj.DealerIdStr = newRow["DealerCode"];
                        tempImportObj.DealerName = newRow["DealerName"];
                        tempImportObj.PartsSalesCategoryIdStr = newRow["PartsSalesCategoryName"];
                        tempImportObj.ChannelCapabilityIdStr = newRow["ChannelCapability"]; //网络模式
                        tempImportObj.BusinessCode = newRow["BusinessCode"]; //业务编号
                        tempImportObj.BusinessName = newRow["BusinessName"]; //业务名称
                        tempImportObj.ShortName = newRow["ShortName"]; //简称
                        tempImportObj.BranchIdStr = newRow["Branch"]; //营销公司
                        tempImportObj.MarketingDepartmentIdStr = newRow["MarketingDepartment"]; //市场部
                        tempImportObj.ServiceStationTypeStr = newRow["ServiceStationType"]; //服务站类型
                        tempImportObj.AreaStr = newRow["Area"]; //区域
                        tempImportObj.RegionTypeStr = newRow["RegionType"]; //地区类别
                        tempImportObj.ServicePermission = newRow["ServicePermission"]; //维修权限
                        tempImportObj.AccreditTimeStr = newRow["AccreditTime"]; //授权时间
                        tempImportObj.IsOnDutyStr = newRow["IsOnDuty"]; //是否值守
                        tempImportObj.HotLine = newRow["HotLine"]; //24小时热线
                        tempImportObj.Fix = newRow["Fix"]; //固定电话
                        tempImportObj.Fax = newRow["Fax"]; //传真
                        tempImportObj.WarehouseIdStr = newRow["Warehouse"]; //订货默认仓库
                        tempImportObj.SaleandServicesiteLayoutStr = newRow["SaleandServicesiteLayout"]; //销售与服务场地布局关系
                        tempImportObj.PartReserveAmountStr = newRow["PartReserveAmount"]; //配件储备金额
                        tempImportObj.RepairAuthorityGradeStr = newRow["RepairAuthorityGrade"]; //服务站维修权限
                        tempImportObj.PartsManagingFeeGradeIdStr = newRow["PartsManagingFeeGrade"]; //配件管理费率
                        tempImportObj.OutFeeGradeIdStr = newRow["OutFeeGrade"]; //外出服务费等级
                        tempImportObj.GradeStr = newRow["Grade"]; //星级
                        tempImportObj.GradeCoefficientIdStr = newRow["GradeCoefficient"]; //结算星级
                        tempImportObj.OutServiceradiiStr = newRow["OutServiceradii"]; //外出服务半径
                        tempImportObj.HourPhone24Str = newRow["HourPhone24"]; //24小时手机号
                        tempImportObj.TrunkNetworkTypeStr = newRow["TrunkNetworkType"]; //主干网络类型
                        tempImportObj.CenterStackStr = newRow["CenterStack"]; //是否中心站

                        tempImportObj.UsedPartsWarehouseIdStr = newRow["UsedPartsWarehouse"]; //旧件仓库
                        tempImportObj.MaterialCostInvoiceTypeStr = newRow["MaterialCostInvoiceType"]; //材料费开票类型
                        tempImportObj.LaborCostCostInvoiceTypeStr = newRow["LaborCostCostInvoiceType"]; //工时费开票类型
                        tempImportObj.LaborCostInvoiceRatioStr = newRow["LaborCostInvoiceRatio"]; //工时费开票系数
                        tempImportObj.InvoiceTypeInvoiceRatioStr = newRow["InvoiceTypeInvoiceRatio"]; //材料费开票系数
                        tempImportObj.ExternalStateStr = newRow["ExternalState"];

                        #endregion

                        var tempErrorMessage = new List<string>();

                        #region 导入的内容基本检查
                        //导入模板必填
                        if(string.IsNullOrEmpty(tempImportObj.DealerIdStr)) {
                            tempErrorMessage.Add("服务站编号不能为空");
                        }
                        if(string.IsNullOrEmpty(tempImportObj.PartsSalesCategoryIdStr)) {
                            tempErrorMessage.Add(ErrorStrings.Export_Validation_MarketingDepartment_PartsSalesCategoryIsNull);
                        }
                        if(!string.IsNullOrEmpty(tempImportObj.TrunkNetworkTypeStr)) {
                            DcsDlrSerInfoTrunkNetworkType trunkNetworkType;
                            if(Enum.TryParse<DcsDlrSerInfoTrunkNetworkType>(tempImportObj.TrunkNetworkTypeStr, out trunkNetworkType) && Enum.IsDefined(typeof(DcsDlrSerInfoTrunkNetworkType), trunkNetworkType)) {
                                tempImportObj.TrunkNetworkType = (int)trunkNetworkType;
                            } else {
                                tempErrorMessage.Add("主干网络不存在！");
                            }
                        }
                        if(!string.IsNullOrEmpty(tempImportObj.CenterStackStr)) {
                            DcsIsOrNot centerStack;
                            if(Enum.TryParse<DcsIsOrNot>(tempImportObj.CenterStackStr, out centerStack) && Enum.IsDefined(typeof(DcsIsOrNot), centerStack)) {
                                tempImportObj.CenterStack = Convert.ToBoolean(centerStack);
                            } else {
                                tempErrorMessage.Add("是否中心站不存在！");
                            }
                        }
                        //if(!string.IsNullOrEmpty(tempImportObj.HourPhone24Str)) {

                        //    if(tempImportObj.HourPhone24Str.Length > 11) {

                        //        tempErrorMessage.Add("请输入有效的24小时手机号.号码位数为11位");

                        //    }

                        //}

                        if(!string.IsNullOrEmpty(tempImportObj.RepairAuthorityGradeStr)) {
                            DcsServicePermission dcsServicePermission;
                            if(Enum.TryParse<DcsServicePermission>(tempImportObj.RepairAuthorityGradeStr, out dcsServicePermission) && Enum.IsDefined(typeof(DcsServicePermission), dcsServicePermission)) {
                                tempImportObj.RepairAuthorityGrade = (int)dcsServicePermission;
                            }
                        }
                        if(!string.IsNullOrEmpty(tempImportObj.ServiceStationTypeStr)) {
                            DcsDealerServiceInfoServiceStationType dcsDealerServiceInfoServiceStationType;
                            if(Enum.TryParse<DcsDealerServiceInfoServiceStationType>(tempImportObj.ServiceStationTypeStr, out dcsDealerServiceInfoServiceStationType) && Enum.IsDefined(typeof(DcsDealerServiceInfoServiceStationType), dcsDealerServiceInfoServiceStationType)) {
                                tempImportObj.ServiceStationType = (int)dcsDealerServiceInfoServiceStationType;
                            } else {
                                tempErrorMessage.Add("服务站类型不存在！");
                            }
                        }
                        if (!string.IsNullOrEmpty(tempImportObj.SaleandServicesiteLayoutStr)) {
                            DcsSaleServiceLayout dcsSaleServiceLayout;
                            if(Enum.TryParse<DcsSaleServiceLayout>(tempImportObj.SaleandServicesiteLayoutStr, out dcsSaleServiceLayout) && Enum.IsDefined(typeof(DcsSaleServiceLayout), dcsSaleServiceLayout)) {
                                tempImportObj.SaleandServicesiteLayout = (int)dcsSaleServiceLayout;
                            } else {
                                tempErrorMessage.Add("销售与服务场地布局关系不存在！");
                            }
                        }
                        if(!string.IsNullOrEmpty(tempImportObj.MaterialCostInvoiceTypeStr)) {
                            DcsInvoiceType dcsInvoiceType;
                            if(Enum.TryParse<DcsInvoiceType>(tempImportObj.MaterialCostInvoiceTypeStr, out dcsInvoiceType) && Enum.IsDefined(typeof(DcsInvoiceType), dcsInvoiceType)) {
                                tempImportObj.MaterialCostInvoiceType = (int)dcsInvoiceType;
                            }
                        }
                        if(!string.IsNullOrEmpty(tempImportObj.LaborCostCostInvoiceTypeStr)) {
                            DcsInvoiceType dcsInvoiceType;
                            if(Enum.TryParse<DcsInvoiceType>(tempImportObj.LaborCostCostInvoiceTypeStr, out dcsInvoiceType) && Enum.IsDefined(typeof(DcsInvoiceType), dcsInvoiceType)) {
                                tempImportObj.LaborCostCostInvoiceType = (int)dcsInvoiceType;
                            }
                        }
                        if(!string.IsNullOrEmpty(tempImportObj.IsOnDutyStr)) {
                            try {
                                tempImportObj.IsOnDuty = ((int)Enum.Parse(typeof(DcsIsOrNot), tempImportObj.IsOnDutyStr) == 1) ? true : false;
                            } catch(Exception) {
                               // tempErrorMessage.Add("是否职守类型不存在！");
                            }
                        }

                        if(!string.IsNullOrEmpty(tempImportObj.ExternalStateStr)) {
                            DcsExternalState dcsExternalState;
                            if(Enum.TryParse<DcsExternalState>(tempImportObj.ExternalStateStr, out dcsExternalState) && Enum.IsDefined(typeof(DcsExternalState), dcsExternalState)) {
                                tempImportObj.ExternalState = (int)dcsExternalState;
                            } else {
                                tempErrorMessage.Add("对外状态应填写为有效或者停用！");
                            }
                        }

                        if(!string.IsNullOrEmpty(tempImportObj.OutServiceradiiStr)) {
                            int checkValue;
                            if(int.TryParse(tempImportObj.OutServiceradiiStr, out checkValue)) {
                                if(checkValue <= 0) {
                                    tempErrorMessage.Add("外出服务半径必须大于0");
                                }
                                tempImportObj.OutServiceradii = Convert.ToInt32(tempImportObj.OutServiceradiiStr);
                            } else {
                                tempErrorMessage.Add("外出服务半径必须是数字");
                            }
                        }

                        if(!string.IsNullOrEmpty(tempImportObj.PartReserveAmountStr)) {
                            int checkValue;
                            if(int.TryParse(tempImportObj.PartReserveAmountStr, out checkValue)) {
                                if(checkValue <= 0) {
                                    tempErrorMessage.Add("配件储备金额必须大于0");
                                }
                                tempImportObj.PartReserveAmount = Convert.ToInt32(tempImportObj.PartReserveAmountStr);
                            } else {
                                tempErrorMessage.Add("配件储备金额必须是数字");
                            }
                        }
                        if(!string.IsNullOrEmpty(tempImportObj.LaborCostInvoiceRatioStr)) {
                            decimal checkValue;
                            if(decimal.TryParse(tempImportObj.LaborCostInvoiceRatioStr, out checkValue)) {
                                if(checkValue <= 0) {
                                    tempErrorMessage.Add("工时费开票系数必须大于0");
                                } else {
                                    tempImportObj.LaborCostInvoiceRatio = checkValue;
                                }
                            } else {
                                tempErrorMessage.Add("工时费开票系数必须是数字");
                            }
                        }
                        if(!string.IsNullOrEmpty(tempImportObj.InvoiceTypeInvoiceRatioStr)) {
                            decimal checkValue;
                            if(decimal.TryParse(tempImportObj.InvoiceTypeInvoiceRatioStr, out checkValue)) {
                                if(checkValue <= 0) {
                                    tempErrorMessage.Add("材料费开票系数必须大于0");
                                } else {
                                    tempImportObj.InvoiceTypeInvoiceRatio = checkValue;
                                }
                            } else {
                                tempErrorMessage.Add("材料费开票系数必须是数字");
                            }
                        }
                        if(!string.IsNullOrEmpty(tempImportObj.AccreditTimeStr)) {
                            DateTime checkValue;
                            if(DateTime.TryParse(tempImportObj.AccreditTimeStr, out checkValue)) {
                                tempImportObj.AccreditTime = Convert.ToDateTime(tempImportObj.AccreditTimeStr);
                            } else {
                                tempErrorMessage.Add("授权时间必须是数字");
                            }
                        }
                        #endregion

                        if(tempErrorMessage.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });
                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    var groups = tempRightList.GroupBy(r => new {
                        r.DealerIdStr,
                        r.PartsSalesCategoryIdStr
                    }).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        groupItem.ErrorMsg = ErrorStrings.ImpPartsSupplierRelation_Validation31;
                    }

                    #region 验证

                    //1、查询经销商基本信息,(依据 经销商服务经营权限.经销商Id=经销商基本信息.Id)，校验经销商基本信息.经销商编号,名称存在，否则提示：经销商编号，经销商名称不存在。
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();
                    var dealerCodeNeedCheck = tempRightList.Select(r => r.DealerIdStr).Distinct().ToArray();
                    var dbDealers = new List<DealerServiceInfoExtend>();
                    Func<string[], bool> getDbDealers = value => {
                        var dbObj = new DealerServiceInfoExtend {
                            DealerId = Convert.ToInt32(value[0]),
                            DealerIdStr = value[1],
                            DealerName = value[2]
                        };
                        dbDealers.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperatorByLower("select id,code,name from dealer where status=1 ", "Code", true, dealerCodeNeedCheck, getDbDealers);
                    foreach(var tempObj in tempRightList) {
                        var tempobjcheck = dbDealers.FirstOrDefault(r => r.DealerIdStr == tempObj.DealerIdStr);
                        if(tempobjcheck == null) {
                            tempObj.ErrorMsg = tempObj.DealerIdStr + ErrorStrings.Export_Validation_PartsSpecialTreatyPrice_Validation1;
                        } else {
                            tempObj.DealerId = tempobjcheck.DealerId;
                        }
                    }
                    //2、校验品牌存在配件销售类型 否则提示：品牌不存在。
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();
                    var partsSalesCategoryNeedCheck = tempRightList.Select(r => r.PartsSalesCategoryIdStr).Distinct().ToArray();
                    var dbPartsSalesCategories = new List<DealerServiceInfoExtend>();
                    Func<string[], bool> getDbPartsSalesCategories = value => {
                        var dbObj = new DealerServiceInfoExtend {
                            PartsSalesCategoryId = Convert.ToInt32(value[0]),
                            PartsSalesCategoryIdStr = value[1],
                            BranchId = Convert.ToInt32(value[2]),
                        };
                        dbPartsSalesCategories.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select id,name,branchid from PartsSalesCategory where status=1 ", "Name", true, partsSalesCategoryNeedCheck, getDbPartsSalesCategories);
                    foreach(var tempObj in tempRightList) {
                        var tempobjcheck = dbPartsSalesCategories.FirstOrDefault(r => r.PartsSalesCategoryIdStr == tempObj.PartsSalesCategoryIdStr);
                        if(tempobjcheck == null) {
                            tempObj.ErrorMsg = ErrorStrings.Export_Title_Partssalescategory_Name + tempObj.PartsSalesCategoryIdStr + ErrorStrings.Export_Validation_Sparepart_NotExist;
                        } else {
                            tempObj.PartsSalesCategoryId = tempobjcheck.PartsSalesCategoryId;
                            tempObj.BranchId = tempobjcheck.BranchId;
                        }
                    }
                    //3.企业分品牌信息不存在（有效的）  提示 企业分品牌信息不存在
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();
                    var dealerIdNeedCheck = tempRightList.Select(r => r.DealerId.ToString()).Distinct().ToArray();
                    var dbDealerServiceInfos = new List<DealerServiceInfoExtend>();
                    Func<string[], bool> getDbDealerServiceInfos = value => {
                        var dbObj = new DealerServiceInfoExtend {
                            Id = Convert.ToInt32(value[0]),
                            DealerId = Convert.ToInt32(value[1]),
                            PartsSalesCategoryId = Convert.ToInt32(value[2]),
                        };
                        dbDealerServiceInfos.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select Id,DealerId,PartsSalesCategoryId from DealerServiceInfo where status=1  ", "DealerId", true, dealerIdNeedCheck, getDbDealerServiceInfos);
                    foreach(var tempObj in tempRightList) {
                        var tempobjcheck = dbDealerServiceInfos.FirstOrDefault(r => r.DealerId == tempObj.DealerId && r.PartsSalesCategoryId == tempObj.PartsSalesCategoryId);
                        if(tempobjcheck == null) {
                            tempObj.ErrorMsg = "企业分品牌信息不存在";
                        } else {
                            tempObj.Id = tempobjcheck.Id;
                        }
                    }
                    //4、查询市场部，否则提示：对应的市场部不存在
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();
                    var marketingDepartmentNeedCheck = tempRightList.Select(r => r.MarketingDepartmentIdStr).Distinct().ToArray();
                    var dbMarketingDepartments = new List<DealerServiceInfoExtend>();
                    Func<string[], bool> getDbMarketingDepartments = value => {
                        var dbObj = new DealerServiceInfoExtend {
                            MarketingDepartmentId = Convert.ToInt32(value[0]),
                            MarketingDepartmentIdStr = value[1],
                            PartsSalesCategoryId = Convert.ToInt32(value[2]),
                        };
                        dbMarketingDepartments.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select id,name,PartsSalesCategoryId from MarketingDepartment where Status=1 ", "Name", true, marketingDepartmentNeedCheck, getDbMarketingDepartments);
                    foreach(var tempObj in tempRightList.Where(r => !string.IsNullOrEmpty(r.MarketingDepartmentIdStr))) {
                        var tempobjcheck = dbMarketingDepartments.FirstOrDefault(r => r.MarketingDepartmentIdStr == tempObj.MarketingDepartmentIdStr && r.PartsSalesCategoryId == tempObj.PartsSalesCategoryId);
                        if(tempobjcheck == null) {
                            tempObj.ErrorMsg = "营销分公司分销中心信息不存在";
                        } else {
                            tempObj.MarketingDepartmentId = tempobjcheck.MarketingDepartmentId;
                        }
                    }
                    //5、查询外出服务等级，否则提示：对应外出服务等级不存在。
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();
                    var serviceTripPriceGradeNeedCheck = tempRightList.Select(r => r.OutFeeGradeIdStr).Distinct().ToArray();
                    var dbServiceTripPriceGrades = new List<DealerServiceInfoExtend>();
                    Func<string[], bool> getDbServiceTripPriceGrades = value => {
                        var dbObj = new DealerServiceInfoExtend {
                            OutFeeGradeId = Convert.ToInt32(value[0]),
                            OutFeeGradeIdStr = value[1],
                            PartsSalesCategoryId = Convert.ToInt32(value[2])
                        };
                        dbServiceTripPriceGrades.Add(dbObj);
                        return false;
                    };
                    foreach(var tempObj in tempRightList.Where(r => !string.IsNullOrEmpty(r.OutFeeGradeIdStr))) {
                        var tempobjcheck = dbServiceTripPriceGrades.FirstOrDefault(r => r.OutFeeGradeIdStr == tempObj.OutFeeGradeIdStr && r.PartsSalesCategoryId == tempObj.PartsSalesCategoryId);
                        if(tempobjcheck == null) {
                            tempObj.ErrorMsg = "外出服务费等级不存在";
                        } else {
                            tempObj.OutFeeGradeId = tempobjcheck.OutFeeGradeId;
                        }
                    }
                    //6、查询星级系数，否则提示：对应的星级不存在
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();
                    var gradeCoefficientNeedCheck = tempRightList.Select(r => r.GradeCoefficientIdStr).Distinct().ToArray();
                    var dbGradeCoefficients = new List<DealerServiceInfoExtend>();
                    Func<string[], bool> getDbGradeCoefficients = value => {
                        var dbObj = new DealerServiceInfoExtend {
                            GradeCoefficientId = Convert.ToInt32(value[0]),
                            GradeCoefficientIdStr = value[1],
                            PartsSalesCategoryId = Convert.ToInt32(value[2]),
                        };
                        dbGradeCoefficients.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select id,Grade,PartsSalesCategoryId from GradeCoefficient where Status=1 ", "Grade", true, gradeCoefficientNeedCheck, getDbGradeCoefficients);
                    foreach(var tempObj in tempRightList.Where(r => !string.IsNullOrEmpty(r.GradeCoefficientIdStr))) {
                        var tempobjcheck = dbGradeCoefficients.FirstOrDefault(r => r.GradeCoefficientIdStr == tempObj.GradeCoefficientIdStr && r.PartsSalesCategoryId == tempObj.PartsSalesCategoryId);
                        if(tempobjcheck == null) {
                            tempObj.ErrorMsg = "该结算星级不存在";
                        } else {
                            tempObj.GradeCoefficientId = tempobjcheck.GradeCoefficientId;
                        }
                    }
                    //9、查询品牌星级，否则提示：对应的星级不存在
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();
                    var gradeNeedCheck = tempRightList.Select(r => r.GradeStr).Distinct().ToArray();
                    var dbGrades = new List<DealerServiceInfoExtend>();
                    Func<string[], bool> getDbGrades = value => {
                        var dbObj = new DealerServiceInfoExtend {

                            Grade = value[0].ToString(),
                            PartsSalesCategoryId = Convert.ToInt32(value[1]),
                        };
                        dbGrades.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select Grade,BrandId from BrandGradeMessage where Status=1 ", "Grade", true, gradeNeedCheck, getDbGrades);
                    foreach(var tempObj in tempRightList.Where(r => !string.IsNullOrEmpty(r.GradeStr))) {
                        var tempobjcheck = dbGrades.FirstOrDefault(r => r.Grade == tempObj.GradeStr && r.PartsSalesCategoryId == tempObj.PartsSalesCategoryId);
                        if(tempobjcheck == null) {
                            tempObj.ErrorMsg = "该星级不存在";
                        } else {
                            tempObj.Grade = tempobjcheck.Grade;
                        }
                    }
                    //7、查询旧件仓库，否则提示：对应的旧件仓库不存在
                    //tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();
                    //var usedPartsWarehouseNeedCheck = tempRightList.Select(r => r.UsedPartsWarehouseIdStr).Distinct().ToArray();
                    //var dbUsedPartsWarehouses = new List<DealerServiceInfoExtend>();
                    //Func<string[], bool> getDbUsedPartsWarehouses = value => {
                    //    var dbObj = new DealerServiceInfoExtend {
                    //        UsedPartsWarehouseId = Convert.ToInt32(value[0]),
                    //        UsedPartsWarehouseIdStr = value[1],
                    //        PartsSalesCategoryId = Convert.ToInt32(value[2]),
                    //    };
                    //    dbUsedPartsWarehouses.Add(dbObj);
                    //    return false;
                    //};
                    //db.QueryDataWithInOperator("select id,Name,PartsSalesCategoryId from UsedPartsWarehouse where status=1 ", "Name", true, usedPartsWarehouseNeedCheck, getDbUsedPartsWarehouses);
                    //foreach(var tempObj in tempRightList.Where(r => !string.IsNullOrEmpty(r.UsedPartsWarehouseIdStr))) {
                    //    var tempobjcheck = dbUsedPartsWarehouses.FirstOrDefault(r => r.UsedPartsWarehouseIdStr == tempObj.UsedPartsWarehouseIdStr && r.PartsSalesCategoryId == tempObj.PartsSalesCategoryId);
                    //    if(tempobjcheck == null) {
                    //        tempObj.ErrorMsg = "对应的旧件仓库不存在";
                    //    } else {
                    //        tempObj.UsedPartsWarehouseId = tempobjcheck.UsedPartsWarehouseId;
                    //    }
                    //}
                    //8、查询网络模式，否则提示：对应的网络模式不存在
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();
                    var ChannelCapabilityCheck = tempRightList.Select(r => r.ChannelCapabilityIdStr).Distinct().ToArray();
                    var dbChannelCapability = new List<DealerServiceInfoExtend>();
                    Func<string[], bool> getDbChannelCapabilitys = value => {
                        var dbObj = new DealerServiceInfoExtend {
                            ChannelCapabilityId = Convert.ToInt32(value[0]),
                            ChannelCapabilityIdStr = value[1]
                        };
                        dbChannelCapability.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select id,Name from ChannelCapability where status=1 ", "Name", true, ChannelCapabilityCheck, getDbChannelCapabilitys);
                    foreach(var tempObj in tempRightList.Where(r => !string.IsNullOrEmpty(r.ChannelCapabilityIdStr))) {
                        var tempobjcheck = dbChannelCapability.FirstOrDefault(r => r.ChannelCapabilityIdStr == tempObj.ChannelCapabilityIdStr);
                        if(tempobjcheck == null) {
                            tempObj.ErrorMsg = "该网络模式不存在";
                        } else {
                            tempObj.ChannelCapabilityId = tempobjcheck.ChannelCapabilityId;
                        }
                    }
                    //9、订货默认仓库，否则提示：对应的订货默认仓库不存在
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();
                    var WarehouseCheck = tempRightList.Select(r => r.PartsSalesCategoryId.ToString()).Distinct().ToArray();
                    var dbWarehouse = new List<DealerServiceInfoExtend>();
                    Func<string[], bool> getDbWarehouses = value => {
                        var dbObj = new DealerServiceInfoExtend {
                            WarehouseId = Convert.ToInt32(value[0]),
                            WarehouseIdStr = value[1],
                            PartsSalesCategoryId = Convert.ToInt32(value[2])
                        };
                        dbWarehouse.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select w.id,w.Name,s.PartsSalesCategoryId from SalesUnit s left join SalesUnitAffiWarehouse su on s.Id = su.SalesUnitId left join Warehouse w on w.Id = su.WarehouseId  where w.status=1 and s.status=1", "s.PartsSalesCategoryId", true, WarehouseCheck, getDbWarehouses);
                    foreach(var tempObj in tempRightList.Where(r => !string.IsNullOrEmpty(r.WarehouseIdStr))) {
                        var tempobjcheck = dbWarehouse.FirstOrDefault(r => r.WarehouseIdStr == tempObj.WarehouseIdStr && r.PartsSalesCategoryId == tempObj.PartsSalesCategoryId);
                        if(tempobjcheck == null) {
                            tempObj.ErrorMsg = "该订货默认仓库不存在";
                        } else {
                            tempObj.WarehouseId = tempobjcheck.WarehouseId;
                        }
                    }
                    //10、配件管理费率等级，否则提示：对应的配件管理费率等级不存在
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();
                    var PartsManagementCheck = tempRightList.Select(r => r.PartsManagingFeeGradeIdStr).Distinct().ToArray();
                    var dbPartsManagement = new List<DealerServiceInfoExtend>();
                    Func<string[], bool> getDbPartsManagement = value => {
                        var dbObj = new DealerServiceInfoExtend {
                            PartsManagingFeeGradeId = Convert.ToInt32(value[0]),
                            PartsManagingFeeGradeIdStr = value[1],
                            BranchId = Convert.ToInt32(value[2]),
                            PartsSalesCategoryId = Convert.ToInt32(value[3]),
                        };
                        dbPartsManagement.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select id,Name,BranchId,PartsSalesCategoryId from PartsManagementCostGrade where status=1 ", "Name", true, PartsManagementCheck, getDbPartsManagement);
                    foreach(var tempObj in tempRightList.Where(r => !string.IsNullOrEmpty(r.PartsManagingFeeGradeIdStr))) {
                        var tempobjcheck = dbPartsManagement.FirstOrDefault(r => r.PartsManagingFeeGradeIdStr == tempObj.PartsManagingFeeGradeIdStr && r.BranchId == tempObj.BranchId && r.PartsSalesCategoryId == tempObj.PartsSalesCategoryId);
                        if(tempobjcheck == null) {
                            tempObj.ErrorMsg = "该配件管理费率等级不存在";
                        } else {
                            tempObj.PartsManagingFeeGradeId = tempobjcheck.PartsManagingFeeGradeId;
                        }
                    }
                    //11、服务站分级验证，提示：对应的服务站分级不存在
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();
                    var GradeName = tempRightList.Select(r => r.PartsSalesCategoryId.ToString()).Distinct().ToArray();
                    var dbGradeName = new List<DealerServiceInfoExtend>();
                    Func<string[], bool> getGradeName = value => {
                        var dbObj = new DealerServiceInfoExtend
                        {
                            PartsSalesCategoryId = Convert.ToInt32(value[0]),
                        };
                        dbGradeName.Add(dbObj);
                        return false;
                    };
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    rightList = allList.Except(errorList).ToList();

                    #endregion
                }
                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempObj = list[index - 1];
                            var values = new object[] {
                                #region 字段
                                tempObj.DealerIdStr, tempObj.DealerName,tempObj.PartsSalesCategoryIdStr,
                                tempObj.ChannelCapabilityIdStr, tempObj.BusinessCode, tempObj.BusinessName, tempObj.ShortName,
                                tempObj.BranchIdStr, tempObj.MarketingDepartmentIdStr, tempObj.ServiceStationTypeStr, tempObj.AreaStr,
                                tempObj.RegionTypeStr, tempObj.ServicePermission, tempObj.AccreditTimeStr, tempObj.IsOnDutyStr, tempObj.HotLine, tempObj.Fix, 
                                tempObj.Fax, tempObj.WarehouseIdStr, tempObj.SaleandServicesiteLayoutStr, tempObj.PartReserveAmountStr, tempObj.RepairAuthorityGradeStr,
                                tempObj.PartsManagingFeeGradeIdStr, tempObj.OutFeeGradeIdStr,tempObj.GradeStr, tempObj.GradeCoefficientIdStr, tempObj.OutServiceradiiStr, 
                                tempObj.HourPhone24Str,tempObj.TrunkNetworkTypeStr,tempObj.CenterStackStr,tempObj.UsedPartsWarehouseIdStr, tempObj.MaterialCostInvoiceTypeStr, 
                                tempObj.LaborCostCostInvoiceTypeStr, tempObj.LaborCostInvoiceRatioStr, tempObj.InvoiceTypeInvoiceRatioStr,
                                tempObj.ExternalStateStr,
                                tempObj.ErrorMsg
                                #endregion
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }
                //导入所有合格数据
                if(!rightList.Any())
                    return true;
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    //开启事务，新增更新配件分品牌信息在一个事务内
                    var ts = conn.BeginTransaction();
                    try {
                        if(rightList.Any()) {
                            var userInfo = Utils.GetCurrentUserInfo();
                            foreach(var item in rightList) {
                                #region DealerServiceInfo
                                List<string> fieldDealerServiceInfos = new List<string>();
                                string sqlUpdateDealerServiceInfos = null;
                                fieldDealerServiceInfos.Add("ModifyTime");
                                fieldDealerServiceInfos.Add("ModifierId");
                                fieldDealerServiceInfos.Add("ModifierName");
                                var commandDealerServiceInfos = db.CreateDbCommand(sqlUpdateDealerServiceInfos, conn, ts);
                                if(!string.IsNullOrEmpty(item.MarketingDepartmentIdStr)) {
                                    fieldDealerServiceInfos.Add("MarketingDepartmentId");
                                    commandDealerServiceInfos.Parameters.Add(db.CreateDbParameter("MarketingDepartmentId", item.MarketingDepartmentId));
                                }
                                if(!string.IsNullOrEmpty(item.ServicePermission)) {
                                    fieldDealerServiceInfos.Add("ServicePermission");
                                    commandDealerServiceInfos.Parameters.Add(db.CreateDbParameter("ServicePermission", item.ServicePermission));
                                }
                                if(!string.IsNullOrEmpty(item.HotLine)) {
                                    fieldDealerServiceInfos.Add("HotLine");
                                    commandDealerServiceInfos.Parameters.Add(db.CreateDbParameter("HotLine", item.HotLine));
                                }
                                if (!string.IsNullOrEmpty(item.Fix)) {
                                    fieldDealerServiceInfos.Add("Fix");
                                    commandDealerServiceInfos.Parameters.Add(db.CreateDbParameter("Fix", item.Fix));
                                }
                                if(!string.IsNullOrEmpty(item.RepairAuthorityGradeStr)) {
                                    fieldDealerServiceInfos.Add("RepairAuthorityGrade");
                                    commandDealerServiceInfos.Parameters.Add(db.CreateDbParameter("RepairAuthorityGrade", item.RepairAuthorityGrade));
                                }
                                if(!string.IsNullOrEmpty(item.OutServiceradiiStr)) {
                                    fieldDealerServiceInfos.Add("OutServiceradii");
                                    commandDealerServiceInfos.Parameters.Add(db.CreateDbParameter("OutServiceradii", item.OutServiceradii));
                                }
                                if(!string.IsNullOrEmpty(item.HourPhone24Str)) {
                                    fieldDealerServiceInfos.Add("HourPhone24");
                                    commandDealerServiceInfos.Parameters.Add(db.CreateDbParameter("HourPhone24", item.HourPhone24Str));
                                }
                                if(!string.IsNullOrEmpty(item.TrunkNetworkTypeStr)) {
                                    fieldDealerServiceInfos.Add("TrunkNetworkType");
                                    commandDealerServiceInfos.Parameters.Add(db.CreateDbParameter("TrunkNetworkType", item.TrunkNetworkType));
                                }
                                if(!string.IsNullOrEmpty(item.CenterStackStr)) {
                                    fieldDealerServiceInfos.Add("CenterStack");
                                    commandDealerServiceInfos.Parameters.Add(db.CreateDbParameter("CenterStack", item.CenterStack));
                                }
                                if(!string.IsNullOrEmpty(item.BusinessCode)) {
                                    fieldDealerServiceInfos.Add("BusinessCode");
                                    commandDealerServiceInfos.Parameters.Add(db.CreateDbParameter("BusinessCode", item.BusinessCode));
                                }
                                if(!string.IsNullOrEmpty(item.BusinessName)) {
                                    fieldDealerServiceInfos.Add("BusinessName");
                                    commandDealerServiceInfos.Parameters.Add(db.CreateDbParameter("BusinessName", item.BusinessName));
                                }
                                if(!string.IsNullOrEmpty(item.BranchIdStr)) {
                                    fieldDealerServiceInfos.Add("BusinessDivision");
                                    commandDealerServiceInfos.Parameters.Add(db.CreateDbParameter("BusinessDivision", item.BranchIdStr));
                                }
                                if(!string.IsNullOrEmpty(item.ServiceStationTypeStr)) {
                                    fieldDealerServiceInfos.Add("ServiceStationType");
                                    commandDealerServiceInfos.Parameters.Add(db.CreateDbParameter("ServiceStationType", item.ServiceStationType));
                                }
                                if(!string.IsNullOrEmpty(item.AreaStr)) {
                                    fieldDealerServiceInfos.Add("Area");
                                    commandDealerServiceInfos.Parameters.Add(db.CreateDbParameter("Area", item.AreaStr));
                                }
                                if(!string.IsNullOrEmpty(item.RegionTypeStr)) {
                                    fieldDealerServiceInfos.Add("RegionType");
                                    commandDealerServiceInfos.Parameters.Add(db.CreateDbParameter("RegionType", item.RegionTypeStr));
                                }
                                if(!string.IsNullOrEmpty(item.AccreditTimeStr)) {
                                    fieldDealerServiceInfos.Add("AccreditTime");
                                    commandDealerServiceInfos.Parameters.Add(db.CreateDbParameter("AccreditTime", item.AccreditTime));
                                }
                                if(!string.IsNullOrEmpty(item.IsOnDutyStr)) {
                                    fieldDealerServiceInfos.Add("IsOnDuty");
                                    commandDealerServiceInfos.Parameters.Add(db.CreateDbParameter("IsOnDuty", item.IsOnDuty ? 1 : 0));
                                }
                                if(!string.IsNullOrEmpty(item.Fax)) {
                                    fieldDealerServiceInfos.Add("Fax");
                                    commandDealerServiceInfos.Parameters.Add(db.CreateDbParameter("Fax", item.Fax));
                                }
                                if(!string.IsNullOrEmpty(item.SaleandServicesiteLayoutStr)) {
                                    fieldDealerServiceInfos.Add("SaleandServicesiteLayout");
                                    commandDealerServiceInfos.Parameters.Add(db.CreateDbParameter("SaleandServicesiteLayout", item.SaleandServicesiteLayout));
                                }
                                if(!string.IsNullOrEmpty(item.PartReserveAmountStr)) {
                                    fieldDealerServiceInfos.Add("PartReserveAmount");
                                    commandDealerServiceInfos.Parameters.Add(db.CreateDbParameter("PartReserveAmount", item.PartReserveAmount));
                                }
                                if(!string.IsNullOrEmpty(item.MaterialCostInvoiceTypeStr)) {
                                    fieldDealerServiceInfos.Add("MaterialCostInvoiceType");
                                    commandDealerServiceInfos.Parameters.Add(db.CreateDbParameter("MaterialCostInvoiceType", item.MaterialCostInvoiceType));
                                }
                                if(!string.IsNullOrEmpty(item.LaborCostCostInvoiceTypeStr)) {
                                    fieldDealerServiceInfos.Add("LaborCostCostInvoiceType");
                                    commandDealerServiceInfos.Parameters.Add(db.CreateDbParameter("LaborCostCostInvoiceType", item.LaborCostCostInvoiceType));
                                }
                                if(!string.IsNullOrEmpty(item.LaborCostInvoiceRatioStr)) {
                                    fieldDealerServiceInfos.Add("LaborCostInvoiceRatio");
                                    commandDealerServiceInfos.Parameters.Add(db.CreateDbParameter("LaborCostInvoiceRatio", item.LaborCostInvoiceRatio));
                                }
                                if(!string.IsNullOrEmpty(item.InvoiceTypeInvoiceRatioStr)) {
                                    fieldDealerServiceInfos.Add("InvoiceTypeInvoiceRatio");
                                    commandDealerServiceInfos.Parameters.Add(db.CreateDbParameter("InvoiceTypeInvoiceRatio", item.InvoiceTypeInvoiceRatio));
                                }
                                if(!string.IsNullOrEmpty(item.OutFeeGradeIdStr)) {
                                    fieldDealerServiceInfos.Add("OutFeeGradeId");
                                    commandDealerServiceInfos.Parameters.Add(db.CreateDbParameter("OutFeeGradeId", item.OutFeeGradeId));
                                }
                                if(!string.IsNullOrEmpty(item.GradeStr)) {
                                    fieldDealerServiceInfos.Add("Grade");
                                    commandDealerServiceInfos.Parameters.Add(db.CreateDbParameter("Grade", item.GradeStr));
                                }
                                if(!string.IsNullOrEmpty(item.GradeCoefficientIdStr)) {
                                    fieldDealerServiceInfos.Add("GradeCoefficientId");
                                    commandDealerServiceInfos.Parameters.Add(db.CreateDbParameter("GradeCoefficientId", item.GradeCoefficientId));
                                }
                                if(!string.IsNullOrEmpty(item.UsedPartsWarehouseIdStr)) {
                                    fieldDealerServiceInfos.Add("UsedPartsWarehouseId");
                                    commandDealerServiceInfos.Parameters.Add(db.CreateDbParameter("UsedPartsWarehouseId", item.UsedPartsWarehouseId));
                                }
                                if(!string.IsNullOrEmpty(item.PartsManagingFeeGradeIdStr)) {
                                    fieldDealerServiceInfos.Add("PartsManagingFeeGradeId");
                                    commandDealerServiceInfos.Parameters.Add(db.CreateDbParameter("PartsManagingFeeGradeId", item.PartsManagingFeeGradeId));
                                }
                                if(!string.IsNullOrEmpty(item.ChannelCapabilityIdStr)) {
                                    fieldDealerServiceInfos.Add("ChannelCapabilityId");
                                    commandDealerServiceInfos.Parameters.Add(db.CreateDbParameter("ChannelCapabilityId", item.ChannelCapabilityId));
                                }
                                if(!string.IsNullOrEmpty(item.WarehouseIdStr)) {
                                    fieldDealerServiceInfos.Add("WarehouseId");
                                    commandDealerServiceInfos.Parameters.Add(db.CreateDbParameter("WarehouseId", item.WarehouseId));
                                }
                                if(!string.IsNullOrEmpty(item.ExternalStateStr)) {
                                    fieldDealerServiceInfos.Add("ExternalState");
                                    commandDealerServiceInfos.Parameters.Add(db.CreateDbParameter("ExternalState", item.ExternalState));
                                }
                                sqlUpdateDealerServiceInfos = db.GetUpdateSql("DealerServiceInfo", fieldDealerServiceInfos.ToArray(), new[] { "Id" });
                                commandDealerServiceInfos.Parameters.Add(db.CreateDbParameter("Id", item.Id));
                                commandDealerServiceInfos.Parameters.Add(db.CreateDbParameter("ModifyTime", DateTime.Now));
                                commandDealerServiceInfos.Parameters.Add(db.CreateDbParameter("ModifierId", userInfo.Id));
                                commandDealerServiceInfos.Parameters.Add(db.CreateDbParameter("ModifierName", userInfo.Name));
                                commandDealerServiceInfos.CommandText = sqlUpdateDealerServiceInfos;
                                commandDealerServiceInfos.ExecuteNonQuery();

                                #endregion

                                #region
                                List<string> fieldDealers = new List<string>();
                                string sqlUpdateDealers = null;
                                fieldDealers.Add("ModifyTime");
                                fieldDealers.Add("ModifierId");
                                fieldDealers.Add("ModifierName");
                                var commandDealers = db.CreateDbCommand(sqlUpdateDealers, conn, ts);


                                if(!string.IsNullOrEmpty(item.ShortName)) {
                                    fieldDealers.Add("ShortName");
                                    commandDealers.Parameters.Add(db.CreateDbParameter("ShortName", item.ShortName));
                                }

                                sqlUpdateDealers = db.GetUpdateSql("Dealer", fieldDealers.ToArray(), new[] { "Id" });
                                commandDealers.Parameters.Add(db.CreateDbParameter("Id", item.DealerId));
                                commandDealers.Parameters.Add(db.CreateDbParameter("ModifyTime", DateTime.Now));
                                commandDealers.Parameters.Add(db.CreateDbParameter("ModifierId", userInfo.Id));
                                commandDealers.Parameters.Add(db.CreateDbParameter("ModifierName", userInfo.Name));
                                commandDealers.CommandText = sqlUpdateDealers;
                                commandDealers.ExecuteNonQuery();
                                #endregion

                                #region 新增经销商分公司管理信息履历
                                var sqlUpdate = db.GetInsertSql("DealerServiceInfoHistory", "Id", new[] {
                                "RecordId", "PartsSalesCategoryId", "BranchId", "DealerId", "FileTime", "FilerId", "FilerName", "CreateTime", "CreatorId", "CreatorName", "ABusinessCode", "ABusinessName", "AMarketingDepartmentId",
                                "AUsedPartsWarehouseId", "AWarehouseId", "AChannelCapabilityId", "APartsManagingFeeGradeId","AGrade", "AGradeCoefficientId", "AOutFeeGradeId", "AOutServiceradii","A24HourPhone","ATrunkNetworkType","ACenterStack", 
                                "ABusinessDivision",
                                "AAccreditTime", "AServicePermission", "AServiceStationType", "AArea", "ARegionType", "AMaterialCostInvoiceType", "ALaborCostCostInvoiceType", "ALaborCostInvoiceRatio", "AInvoiceTypeInvoiceRatio",
                                "AIsOnDuty", "ARepairAuthorityGrade", "AHotLine", "AFix", "AFax", "APartReserveAmount", "ASaleandServicesiteLayout", "AStatus"
                            });

                                var command = db.CreateDbCommand(sqlUpdate, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("RecordId", item.Id)); //主记录Id
                                command.Parameters.Add(db.CreateDbParameter("PartsSalesCategoryId", item.PartsSalesCategoryId)); //配件销售类型Id
                                command.Parameters.Add(db.CreateDbParameter("BranchId", item.BranchId)); //营销分公司Id
                                command.Parameters.Add(db.CreateDbParameter("DealerId", item.DealerId)); //经销商Id
                                command.Parameters.Add(db.CreateDbParameter("FileTime", DateTime.Now)); //归档时间
                                command.Parameters.Add(db.CreateDbParameter("FilerId", userInfo.Id)); //归档人Id
                                command.Parameters.Add(db.CreateDbParameter("FilerName", userInfo.Name)); //归档人
                                command.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now)); //创建时间
                                command.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id)); //创建人Id
                                command.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name)); //创建人
                                command.Parameters.Add(db.CreateDbParameter("ABusinessCode", item.BusinessCode)); //变更后业务编号
                                command.Parameters.Add(db.CreateDbParameter("ABusinessName", item.BusinessName)); //变更后业务名称
                                command.Parameters.Add(db.CreateDbParameter("AMarketingDepartmentId", item.MarketingDepartmentId)); //变更后市场部Id
                                command.Parameters.Add(db.CreateDbParameter("AUsedPartsWarehouseId", item.UsedPartsWarehouseId)); //变更后旧件仓库Id
                                command.Parameters.Add(db.CreateDbParameter("AWarehouseId", item.WarehouseId)); //变更后仓库Id
                                command.Parameters.Add(db.CreateDbParameter("AChannelCapabilityId", item.ChannelCapabilityId)); //变更后网络业务能力Id
                                command.Parameters.Add(db.CreateDbParameter("APartsManagingFeeGradeId", item.PartsManagingFeeGradeId)); //变更后配件管理费率等级Id
                                command.Parameters.Add(db.CreateDbParameter("AGrade", item.Grade));
                                command.Parameters.Add(db.CreateDbParameter("AGradeCoefficientId", item.GradeCoefficientId)); //变更后星级系数Id
                                command.Parameters.Add(db.CreateDbParameter("AOutFeeGradeId", item.OutFeeGradeId)); //变更后外出服务费等级Id
                                command.Parameters.Add(db.CreateDbParameter("AOutServiceradii", item.OutServiceradii)); //变更后外出服务半径

                                command.Parameters.Add(db.CreateDbParameter("A24HourPhone", item.HourPhone24Str)); //24小时手机号
                                command.Parameters.Add(db.CreateDbParameter("ATrunkNetworkType", item.TrunkNetworkType)); //主干网络类型
                                command.Parameters.Add(db.CreateDbParameter("ACenterStack", item.CenterStack)); //是否中心站

                                command.Parameters.Add(db.CreateDbParameter("ABusinessDivision", item.BusinessDivision)); //变更后事业本部
                                command.Parameters.Add(db.CreateDbParameter("AAccreditTime", item.AccreditTime)); //变更后授权时间
                                command.Parameters.Add(db.CreateDbParameter("AServicePermission", item.ServicePermission)); //变更后维修权限
                                command.Parameters.Add(db.CreateDbParameter("AServiceStationType", item.ServiceStationType)); //变更后服务站类别
                                command.Parameters.Add(db.CreateDbParameter("AArea", item.Area)); //变更后区域
                                command.Parameters.Add(db.CreateDbParameter("ARegionType", item.RegionTypeStr)); //变更后地区类别
                                command.Parameters.Add(db.CreateDbParameter("AMaterialCostInvoiceType", item.MaterialCostInvoiceType)); //变更后材料费开票类型
                                command.Parameters.Add(db.CreateDbParameter("ALaborCostCostInvoiceType", item.LaborCostCostInvoiceType)); //变更后工时费开票类型
                                command.Parameters.Add(db.CreateDbParameter("ALaborCostInvoiceRatio", item.LaborCostInvoiceRatio)); //变更后工时费开票系数
                                command.Parameters.Add(db.CreateDbParameter("AInvoiceTypeInvoiceRatio", item.InvoiceTypeInvoiceRatio)); //变更后材料费开票系数
                                command.Parameters.Add(db.CreateDbParameter("AIsOnDuty", item.IsOnDuty)); //变更后是否职守
                                command.Parameters.Add(db.CreateDbParameter("ARepairAuthorityGrade", item.RepairAuthorityGrade)); //变更后维修资质等级
                                command.Parameters.Add(db.CreateDbParameter("AHotLine", item.HotLine)); //变更后24小时热线
                                command.Parameters.Add(db.CreateDbParameter("AFix", item.Fix)); //变更后固定电话
                                command.Parameters.Add(db.CreateDbParameter("AFax", item.Fax)); //变更后传真
                                command.Parameters.Add(db.CreateDbParameter("APartReserveAmount", item.PartReserveAmount)); //变更后配件储备金额
                                command.Parameters.Add(db.CreateDbParameter("ASaleandServicesiteLayout", item.SaleandServicesiteLayout)); //变更后销售与服务场地布局关系
                                command.Parameters.Add(db.CreateDbParameter("AStatus", 1)); //变更后状态
                                command.ExecuteNonQuery();
                            }
                                #endregion
                        }
                        //无异常提交
                        ts.Commit();
                    } catch(Exception) {
                        //报错回滚
                        ts.Rollback();
                        throw;
                    } finally {
                        if(conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }

        public bool 批量导入经销商分公司管理信息(string fileName, out int excelImportNum, out List<DealerServiceInfoExtend> rightData, out List<DealerServiceInfoExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<DealerServiceInfoExtend>();
            var rightList = new List<DealerServiceInfoExtend>();
            var allList = new List<DealerServiceInfoExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("DealerServiceInfo", out notNullableFields, out fieldLenght);
                List<object> excelColumns;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    #region
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealerserviceinfo_Name, "ChannelCapability");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Partssalescategory_Name, "PartsSalesCategoryName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_DealerCode, "DealerCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_DealerName, "DealerName");
                    //excelOperator.AddColumnDataSource("市场部", "MarketingDepartmentName");
                    //excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealerserviceinfo_ServicePermission, "ServicePermission");
                    //excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealerserviceinfo_HotLine, "HotLine");
                    //excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_ContactPhone, "Fix");
                    //excelOperator.AddColumnDataSource("服务站维修权限", "RepairAuthorityGrade");
                    //excelOperator.AddColumnDataSource("外出服务费等级", "OutFeeGrade");
                    //excelOperator.AddColumnDataSource("星级", "GradeCoefficient");
                    //excelOperator.AddColumnDataSource("外出服务半径", "OutServiceradii");
                    //excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealerserviceinfo_OldName, "UsedPartsWarehouse");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_BranchSupplierRelation_BusinessCode, "BusinessCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_BranchSupplierRelation_BusinessName, "BusinessName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Agency_Abbreviation, "ShortName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealerserviceinfo_BranchName, "Branch");
                    excelOperator.AddColumnDataSource("市场部", "MarketingDepartment");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealerserviceinfo_DealerType, "ServiceStationType");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealerserviceinfo_Area, "Area");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealerserviceinfo_Regiontype, "RegionType");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealerserviceinfo_ServicePermission, "ServicePermission");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealerserviceinfo_AccreditTime, "AccreditTime");
                    excelOperator.AddColumnDataSource("是否值守", "IsOnDuty");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealerserviceinfo_HotLine, "HotLine");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_ContactPhone, "Fix");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Agency_Fax, "Fax");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealerserviceinfo_WarehouseName, "Warehouse");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealerserviceinfo_SaleandServicesiteLayout, "SaleandServicesiteLayout");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealerserviceinfo_PartReserveAmount, "PartReserveAmount");
                    excelOperator.AddColumnDataSource("服务站维修权限", "RepairAuthorityGrade");
                    excelOperator.AddColumnDataSource("配件管理费率", "PartsManagingFeeGrade");
                    excelOperator.AddColumnDataSource("外出服务费等级", "OutFeeGrade");
                    excelOperator.AddColumnDataSource("星级", "Grade");
                    excelOperator.AddColumnDataSource("结算星级", "GradeCoefficient");
                    excelOperator.AddColumnDataSource("外出服务半径", "OutServiceradii");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealerserviceinfo_HourPhone24, "HourPhone24");
                    excelOperator.AddColumnDataSource("主干网络类型", "TrunkNetworkType");
                    excelOperator.AddColumnDataSource("是否中心站", "CenterStack");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealerserviceinfo_OldName, "UsedPartsWarehouse");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealerserviceinfo_MaterialCostInvoiceType, "MaterialCostInvoiceType");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealerserviceinfo_LaborCostCostInvoiceType, "LaborCostCostInvoiceType");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealerserviceinfo_LaborCostInvoiceRatio, "LaborCostInvoiceRatio");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealerserviceinfo_InvoiceTypeInvoiceRatio, "InvoiceTypeInvoiceRatio");
                    #endregion

                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;

                    //单次最大导入条数不能大于300！
                    if(excelOperator.LastRowNum > 300) {
                        throw new Exception("单次最大导入条数不能大于300！");
                    }

                    #region 获取对应枚举
                    var keyValuePairs = new[] {
                        new KeyValuePair<string, string>("ServiceStationType", "DealerServiceInfo_ServiceStationType"), 
                        new KeyValuePair<string, string>("SaleandServicesiteLayout", "SaleServiceLayout"),
                        new KeyValuePair<string, string>("RepairAuthorityGrade", "ServicePermission"),
                        new KeyValuePair<string, string>("MaterialCostInvoiceType", "Invoice_Type"), 
                        new KeyValuePair<string, string>("LaborCostCostInvoiceType", "Invoice_Type"),
                        new KeyValuePair<string, string>("TrunkNetworkType", "DlrSerInfo_TrunkNetworkType"),  
                        new KeyValuePair<string, string>("CenterStack", "IsOrNot"),     
                    };
                    tempExcelOperator.ImportHelper.LoadEnumFromDb(null, null, keyValuePairs);
                    #endregion

                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var tempImportObj = new DealerServiceInfoExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);

                        #region
                        tempImportObj.DealerIdStr = newRow["DealerCode"];
                        tempImportObj.DealerName = newRow["DealerName"];
                        tempImportObj.PartsSalesCategoryIdStr = newRow["PartsSalesCategoryName"];
                        tempImportObj.ChannelCapabilityIdStr = newRow["ChannelCapability"]; //网络模式
                        tempImportObj.BusinessCode = newRow["BusinessCode"]; //业务编号
                        tempImportObj.BusinessName = newRow["BusinessName"]; //业务名称
                        tempImportObj.ShortName = newRow["ShortName"]; //简称
                        tempImportObj.BranchIdStr = newRow["Branch"]; //营销公司
                        tempImportObj.MarketingDepartmentIdStr = newRow["MarketingDepartment"]; //市场部
                        tempImportObj.ServiceStationTypeStr = newRow["ServiceStationType"]; //服务站类型
                        tempImportObj.AreaStr = newRow["Area"]; //区域
                        tempImportObj.RegionTypeStr = newRow["RegionType"]; //地区类别
                        tempImportObj.ServicePermission = newRow["ServicePermission"]; //维修权限
                        tempImportObj.AccreditTimeStr = newRow["AccreditTime"]; //授权时间
                        tempImportObj.IsOnDutyStr = newRow["IsOnDuty"]; //是否值守
                        tempImportObj.HotLine = newRow["HotLine"]; //24小时热线
                        tempImportObj.Fix = newRow["Fix"]; //固定电话
                        tempImportObj.Fax = newRow["Fax"]; //传真
                        tempImportObj.WarehouseIdStr = newRow["Warehouse"]; //订货默认仓库
                        tempImportObj.SaleandServicesiteLayoutStr = newRow["SaleandServicesiteLayout"]; //销售与服务场地布局关系
                        tempImportObj.PartReserveAmountStr = newRow["PartReserveAmount"]; //配件储备金额
                        tempImportObj.RepairAuthorityGradeStr = newRow["RepairAuthorityGrade"]; //服务站维修权限
                        tempImportObj.PartsManagingFeeGradeIdStr = newRow["PartsManagingFeeGrade"]; //配件管理费率
                        tempImportObj.OutFeeGradeIdStr = newRow["OutFeeGrade"]; //外出服务费等级
                        tempImportObj.GradeStr = newRow["Grade"];
                        tempImportObj.GradeCoefficientIdStr = newRow["GradeCoefficient"]; //结算星级
                        tempImportObj.OutServiceradiiStr = newRow["OutServiceradii"]; //外出服务半径

                        tempImportObj.HourPhone24Str = newRow["HourPhone24"]; //24小时手机号
                        tempImportObj.TrunkNetworkTypeStr = newRow["TrunkNetworkType"]; //主干网络类型
                        tempImportObj.CenterStackStr = newRow["CenterStack"]; //是否中心站

                        tempImportObj.UsedPartsWarehouseIdStr = newRow["UsedPartsWarehouse"]; //旧件仓库
                        tempImportObj.MaterialCostInvoiceTypeStr = newRow["MaterialCostInvoiceType"]; //材料费开票类型
                        tempImportObj.LaborCostCostInvoiceTypeStr = newRow["LaborCostCostInvoiceType"]; //工时费开票类型
                        tempImportObj.LaborCostInvoiceRatioStr = newRow["LaborCostInvoiceRatio"]; //工时费开票系数
                        tempImportObj.InvoiceTypeInvoiceRatioStr = newRow["InvoiceTypeInvoiceRatio"]; //材料费开票系数

                        #endregion

                        var tempErrorMessage = new List<string>();

                        #region 导入的内容基本检查
                        //导入模板必填
                        if(string.IsNullOrEmpty(tempImportObj.DealerIdStr)) {
                            tempErrorMessage.Add("服务站编号不能为空");
                        }
                        if(string.IsNullOrEmpty(tempImportObj.PartsSalesCategoryIdStr)) {
                            tempErrorMessage.Add(ErrorStrings.Export_Validation_MarketingDepartment_PartsSalesCategoryIsNull);
                        }

                        #region 模式验证
                        if(string.IsNullOrEmpty(tempImportObj.BusinessCode)) {
                            tempErrorMessage.Add(ErrorStrings.Export_Validation_BranchSupplierRelation_BusinessCodeIsNull);
                        }
                        if(string.IsNullOrEmpty(tempImportObj.BusinessName)) {
                            tempErrorMessage.Add(ErrorStrings.Export_Validation_BranchSupplierRelation_BusinessNameIsNull);
                        }
                        if(string.IsNullOrEmpty(tempImportObj.ShortName)) {
                            tempErrorMessage.Add(ErrorStrings.Export_Validation_Dealer_Validation36);
                        }
                        if(string.IsNullOrEmpty(tempImportObj.BranchIdStr)) {
                            tempErrorMessage.Add("营销公司不能为空");
                        }
                        if(string.IsNullOrEmpty(tempImportObj.MarketingDepartmentIdStr)) {
                            tempErrorMessage.Add("市场部不能为空");
                        }
                        if(string.IsNullOrEmpty(tempImportObj.ServiceStationTypeStr)) {
                            tempErrorMessage.Add("服务站类型不能为空");
                        }
                        if(string.IsNullOrEmpty(tempImportObj.IsOnDutyStr)) {
                            tempErrorMessage.Add("是否值守不能为空");
                        }
                        if(string.IsNullOrEmpty(tempImportObj.RepairAuthorityGradeStr)) {
                            tempErrorMessage.Add("服务站维修权限不能为空");
                        }
                        if(string.IsNullOrEmpty(tempImportObj.GradeCoefficientIdStr)) {
                            tempErrorMessage.Add("结算星级不能为空");
                        }
                        if(string.IsNullOrEmpty(tempImportObj.GradeStr)) {
                            tempErrorMessage.Add("星级不能为空");
                        }

                        //bool ChannelCapabilityflag = false;
                        //if(!string.IsNullOrEmpty(tempImportObj.ChannelCapabilityIdStr)) {
                        //    try {
                        //        tempImportObj.ChannelCapabilityId = (int)Enum.Parse(typeof(DcsChannelGrade), tempImportObj.ChannelCapabilityIdStr);
                        //        ChannelCapabilityflag = true;
                        //    } catch(Exception) {
                        //        tempErrorMessage.Add("该网络模式不存在！");
                        //    }
                        //}

                        //服务站
                        if("服务站".Equals(tempImportObj.ChannelCapabilityIdStr)) {

                            if(string.IsNullOrEmpty(tempImportObj.ServicePermission)) {
                                tempErrorMessage.Add("维修权限不能为空");
                            }
                            if(string.IsNullOrEmpty(tempImportObj.AccreditTimeStr)) {
                                tempErrorMessage.Add("授权时间不能为空");
                            }
                            if(string.IsNullOrEmpty(tempImportObj.HotLine)) {
                                tempErrorMessage.Add("24小时热线不能为空");
                            }
                            if(string.IsNullOrEmpty(tempImportObj.PartsManagingFeeGradeIdStr)) {
                                tempErrorMessage.Add("配件管理费率不能为空");
                            }
                            if(string.IsNullOrEmpty(tempImportObj.OutFeeGradeIdStr)) {
                                tempErrorMessage.Add("外出服务费等级不能为空");
                            }
                            if(string.IsNullOrEmpty(tempImportObj.UsedPartsWarehouseIdStr)) {
                                tempErrorMessage.Add("旧件仓库不能为空");
                            }
                        }
                            //4S店 
                        else if("4S店".Equals(tempImportObj.ChannelCapabilityIdStr)) {
                            if(string.IsNullOrEmpty(tempImportObj.ServicePermission)) {
                                tempErrorMessage.Add("维修权限不能为空");
                            }
                            if(string.IsNullOrEmpty(tempImportObj.AccreditTimeStr)) {
                                tempErrorMessage.Add("授权时间不能为空");
                            }
                            if(string.IsNullOrEmpty(tempImportObj.HotLine)) {
                                tempErrorMessage.Add("24小时热线不能为空");
                            }
                            if(string.IsNullOrEmpty(tempImportObj.PartsManagingFeeGradeIdStr)) {
                                tempErrorMessage.Add("配件管理费率不能为空");
                            }
                            if(string.IsNullOrEmpty(tempImportObj.OutFeeGradeIdStr)) {
                                tempErrorMessage.Add("外出服务费等级不能为空");
                            }
                            if(string.IsNullOrEmpty(tempImportObj.UsedPartsWarehouseIdStr)) {
                                tempErrorMessage.Add("旧件仓库不能为空");
                            }
                            if(string.IsNullOrEmpty(tempImportObj.SaleandServicesiteLayoutStr)) {
                                tempErrorMessage.Add("销售与服务场地布局关系不能为空");
                            }
                        }
                            //专卖店
                        else if("专卖店".Equals(tempImportObj.ChannelCapabilityIdStr)) {
                        }
                        #endregion
                        if(!string.IsNullOrEmpty(tempImportObj.TrunkNetworkTypeStr)) {
                            DcsDlrSerInfoTrunkNetworkType trunkNetworkType;
                            if(Enum.TryParse<DcsDlrSerInfoTrunkNetworkType>(tempImportObj.TrunkNetworkTypeStr, out trunkNetworkType) && Enum.IsDefined(typeof(DcsDlrSerInfoTrunkNetworkType), trunkNetworkType)) {
                                tempImportObj.TrunkNetworkType = (int)trunkNetworkType;
                            } else {
                                tempErrorMessage.Add("主干网络不存在！");
                            }
                        }
                        if(!string.IsNullOrEmpty(tempImportObj.CenterStackStr)) {
                            DcsIsOrNot centerStack;
                            if(Enum.TryParse<DcsIsOrNot>(tempImportObj.CenterStackStr, out centerStack) && Enum.IsDefined(typeof(DcsIsOrNot), centerStack)) {
                                tempImportObj.CenterStack = Convert.ToBoolean(centerStack);
                            } else {
                                tempErrorMessage.Add("是否中心站不存在！");
                            }
                        }
                        //if(!string.IsNullOrEmpty(tempImportObj.HourPhone24Str)) {

                        //    if(tempImportObj.HourPhone24Str.Length > 11) {

                        //        tempErrorMessage.Add("请输入有效的24小时手机号.号码位数为11位");

                        //    }

                        //}
                        if(!string.IsNullOrEmpty(tempImportObj.RepairAuthorityGradeStr)) {
                            DcsServicePermission dcsServicePermission;
                            if(Enum.TryParse<DcsServicePermission>(tempImportObj.RepairAuthorityGradeStr, out dcsServicePermission) && Enum.IsDefined(typeof(DcsServicePermission), dcsServicePermission)) {
                                tempImportObj.RepairAuthorityGrade = (int)dcsServicePermission;
                            }
                        }
                        if(!string.IsNullOrEmpty(tempImportObj.ServiceStationTypeStr)) {
                            DcsDealerServiceInfoServiceStationType dcsDealerServiceInfoServiceStationType;
                            if(Enum.TryParse<DcsDealerServiceInfoServiceStationType>(tempImportObj.ServiceStationTypeStr, out dcsDealerServiceInfoServiceStationType) && Enum.IsDefined(typeof(DcsDealerServiceInfoServiceStationType), dcsDealerServiceInfoServiceStationType)) {
                                tempImportObj.ServiceStationType = (int)dcsDealerServiceInfoServiceStationType;
                            } else {
                                tempErrorMessage.Add("服务站类型不存在！");
                            }
                        }
                        if(!string.IsNullOrEmpty(tempImportObj.SaleandServicesiteLayoutStr)) {
                            DcsSaleServiceLayout dcsSaleServiceLayout;
                            if(Enum.TryParse<DcsSaleServiceLayout>(tempImportObj.SaleandServicesiteLayoutStr, out dcsSaleServiceLayout) && Enum.IsDefined(typeof(DcsSaleServiceLayout), dcsSaleServiceLayout)) {
                                tempImportObj.SaleandServicesiteLayout = (int)dcsSaleServiceLayout;
                            } else {
                                tempErrorMessage.Add("销售与服务场地布局关系不存在！");
                            }
                        }
                        if(!string.IsNullOrEmpty(tempImportObj.MaterialCostInvoiceTypeStr)) {
                            DcsInvoiceType dcsInvoiceType;
                            if(Enum.TryParse<DcsInvoiceType>(tempImportObj.MaterialCostInvoiceTypeStr, out dcsInvoiceType) && Enum.IsDefined(typeof(DcsInvoiceType), dcsInvoiceType)) {
                                tempImportObj.MaterialCostInvoiceType = (int)dcsInvoiceType;
                            } else {
                                tempErrorMessage.Add("材料开票类型不存在！");
                            }
                        }
                        if(!string.IsNullOrEmpty(tempImportObj.LaborCostCostInvoiceTypeStr)) {
                            DcsInvoiceType dcsInvoiceType;
                            if(Enum.TryParse<DcsInvoiceType>(tempImportObj.LaborCostCostInvoiceTypeStr, out dcsInvoiceType) && Enum.IsDefined(typeof(DcsInvoiceType), dcsInvoiceType)) {
                                tempImportObj.LaborCostCostInvoiceType = (int)dcsInvoiceType;
                            }
                        }
                        if(!string.IsNullOrEmpty(tempImportObj.IsOnDutyStr)) {
                            try {
                                tempImportObj.IsOnDuty = ((int)Enum.Parse(typeof(DcsIsOrNot), tempImportObj.IsOnDutyStr) == 1) ? true : false;
                            } catch(Exception) {
                                tempErrorMessage.Add("是否职守类型不存在！");
                            }
                        }

                        if(!string.IsNullOrEmpty(tempImportObj.OutServiceradiiStr)) {
                            int checkValue;
                            if(int.TryParse(tempImportObj.OutServiceradiiStr, out checkValue)) {
                                if(checkValue <= 0) {
                                    tempErrorMessage.Add("外出服务半径必须大于0");
                                }
                                tempImportObj.OutServiceradii = Convert.ToInt32(tempImportObj.OutServiceradiiStr);
                            } else {
                                tempErrorMessage.Add("外出服务半径必须是数字");
                            }
                        }
                        if(!string.IsNullOrEmpty(tempImportObj.PartReserveAmountStr)) {
                            int checkValue;
                            if(int.TryParse(tempImportObj.PartReserveAmountStr, out checkValue)) {
                                if(checkValue <= 0) {
                                    tempErrorMessage.Add("配件储备金额必须大于0");
                                }
                                tempImportObj.PartReserveAmount = Convert.ToInt32(tempImportObj.PartReserveAmountStr);
                            } else {
                                tempErrorMessage.Add("配件储备金额必须是数字");
                            }
                        }
                        if(!string.IsNullOrEmpty(tempImportObj.LaborCostInvoiceRatioStr)) {
                            decimal checkValue;
                            if(decimal.TryParse(tempImportObj.LaborCostInvoiceRatioStr, out checkValue)) {
                                if(checkValue <= 0) {
                                    tempErrorMessage.Add("工时费开票系数必须大于0");
                                } else {
                                    tempImportObj.LaborCostInvoiceRatio = checkValue;
                                }
                            } else {
                                tempErrorMessage.Add("工时费开票系数必须是数字");
                            }
                        }
                        if(!string.IsNullOrEmpty(tempImportObj.InvoiceTypeInvoiceRatioStr)) {
                            decimal checkValue;
                            if(decimal.TryParse(tempImportObj.InvoiceTypeInvoiceRatioStr, out checkValue)) {
                                if(checkValue <= 0) {
                                    tempErrorMessage.Add("材料费开票系数必须大于0");
                                } else {
                                    tempImportObj.InvoiceTypeInvoiceRatio = checkValue;
                                }
                            } else {
                                tempErrorMessage.Add("材料费开票系数必须是数字");
                            }
                        }
                        if(!string.IsNullOrEmpty(tempImportObj.AccreditTimeStr)) {
                            DateTime checkValue;
                            if(DateTime.TryParse(tempImportObj.AccreditTimeStr, out checkValue)) {
                                tempImportObj.AccreditTime = Convert.ToDateTime(tempImportObj.AccreditTimeStr);
                            } else {
                                tempErrorMessage.Add("授权时间必须是数字");
                            }
                        }
                        #endregion

                        if(tempErrorMessage.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });
                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    var groups = tempRightList.GroupBy(r => new {
                        r.DealerIdStr,
                        r.PartsSalesCategoryIdStr
                    }).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        groupItem.ErrorMsg = ErrorStrings.ImpPartsSupplierRelation_Validation31;
                    }

                    #region 验证

                    //1、查询经销商基本信息,(依据 经销商服务经营权限.经销商Id=经销商基本信息.Id)，校验经销商基本信息.经销商编号,名称存在，否则提示：经销商编号，经销商名称不存在。
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();
                    var dealerCodeNeedCheck = tempRightList.Select(r => r.DealerIdStr).Distinct().ToArray();
                    var dbDealers = new List<DealerServiceInfoExtend>();
                    Func<string[], bool> getDbDealers = value => {
                        var dbObj = new DealerServiceInfoExtend {
                            DealerId = Convert.ToInt32(value[0]),
                            DealerIdStr = value[1],
                            DealerName = value[2]
                        };
                        dbDealers.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperatorByLower("select id,code,name from dealer where status=1 ", "Code", true, dealerCodeNeedCheck, getDbDealers);
                    foreach(var tempObj in tempRightList) {
                        var tempobjcheck = dbDealers.FirstOrDefault(r => r.DealerIdStr == tempObj.DealerIdStr);
                        if(tempobjcheck == null) {
                            tempObj.ErrorMsg = tempObj.DealerIdStr + "此经销商编号，名称不存在";
                        } else {
                            tempObj.DealerId = tempobjcheck.DealerId;
                        }
                    }
                    //2、校验品牌存在配件销售类型 否则提示：品牌不存在。
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();
                    var partsSalesCategoryNeedCheck = tempRightList.Select(r => r.PartsSalesCategoryIdStr).Distinct().ToArray();
                    var dbPartsSalesCategories = new List<DealerServiceInfoExtend>();
                    Func<string[], bool> getDbPartsSalesCategories = value => {
                        var dbObj = new DealerServiceInfoExtend {
                            PartsSalesCategoryId = Convert.ToInt32(value[0]),
                            PartsSalesCategoryIdStr = value[1],
                            BranchId = Convert.ToInt32(value[2]),
                        };
                        dbPartsSalesCategories.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select id,name,branchid from PartsSalesCategory where status=1 ", "Name", true, partsSalesCategoryNeedCheck, getDbPartsSalesCategories);
                    foreach(var tempObj in tempRightList) {
                        var tempobjcheck = dbPartsSalesCategories.FirstOrDefault(r => r.PartsSalesCategoryIdStr == tempObj.PartsSalesCategoryIdStr);
                        if(tempobjcheck == null) {
                            tempObj.ErrorMsg = ErrorStrings.Export_Title_Partssalescategory_Name + tempObj.PartsSalesCategoryIdStr + ErrorStrings.Export_Validation_Sparepart_NotExist;
                        } else {
                            tempObj.PartsSalesCategoryId = tempobjcheck.PartsSalesCategoryId;
                            tempObj.BranchId = tempobjcheck.BranchId;
                        }
                    }
                    //3.企业分品牌信息存在（有效的）  提示 企业分品牌信息已存在
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();
                    var dealerIdNeedCheck = tempRightList.Select(r => r.DealerId.ToString()).Distinct().ToArray();
                    var dbDealerServiceInfos = new List<DealerServiceInfoExtend>();
                    Func<string[], bool> getDbDealerServiceInfos = value => {
                        var dbObj = new DealerServiceInfoExtend {
                            Id = Convert.ToInt32(value[0]),
                            DealerId = Convert.ToInt32(value[1]),
                            PartsSalesCategoryId = Convert.ToInt32(value[2]),
                        };
                        dbDealerServiceInfos.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select Id,DealerId,PartsSalesCategoryId from DealerServiceInfo where status in (1,2)  ", "DealerId", true, dealerIdNeedCheck, getDbDealerServiceInfos);
                    foreach(var tempObj in tempRightList) {
                        var tempobjcheck = dbDealerServiceInfos.FirstOrDefault(r => r.DealerId == tempObj.DealerId && r.PartsSalesCategoryId == tempObj.PartsSalesCategoryId);
                        if(tempobjcheck != null) {
                            tempObj.ErrorMsg = "同一个品牌已存在“有效”或“停用”";
                        } else {
                            //tempObj.Id = tempobjcheck.Id;
                        }
                    }
                    //4、查询市场部，否则提示：对应的市场部不存在
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();
                    var marketingDepartmentNeedCheck = tempRightList.Select(r => r.MarketingDepartmentIdStr).Distinct().ToArray();
                    var dbMarketingDepartments = new List<DealerServiceInfoExtend>();
                    Func<string[], bool> getDbMarketingDepartments = value => {
                        var dbObj = new DealerServiceInfoExtend {
                            MarketingDepartmentId = Convert.ToInt32(value[0]),
                            MarketingDepartmentIdStr = value[1],
                            PartsSalesCategoryId = Convert.ToInt32(value[2]),
                        };
                        dbMarketingDepartments.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select id,name,PartsSalesCategoryId from MarketingDepartment where Status=1 ", "Name", true, marketingDepartmentNeedCheck, getDbMarketingDepartments);
                    foreach(var tempObj in tempRightList.Where(r => !string.IsNullOrEmpty(r.MarketingDepartmentIdStr))) {
                        var tempobjcheck = dbMarketingDepartments.FirstOrDefault(r => r.MarketingDepartmentIdStr == tempObj.MarketingDepartmentIdStr && r.PartsSalesCategoryId == tempObj.PartsSalesCategoryId);
                        if(tempobjcheck == null) {
                            tempObj.ErrorMsg = "营销分公司分销中心信息不存在";
                        } else {
                            tempObj.MarketingDepartmentId = tempobjcheck.MarketingDepartmentId;
                        }
                    }
                    //5.查询网络模式，否则提示：对应的网络模式不存在
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();
                    var ChannelCapabilityCheck = tempRightList.Select(r => r.ChannelCapabilityIdStr).Distinct().ToArray();
                    var dbChannelCapability = new List<DealerServiceInfoExtend>();
                    Func<string[], bool> getDbChannelCapabilitys = value => {
                        var dbObj = new DealerServiceInfoExtend {
                            ChannelCapabilityId = Convert.ToInt32(value[0]),
                            ChannelCapabilityIdStr = value[1]
                        };
                        dbChannelCapability.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select id,Name from ChannelCapability where status=1 ", "Name", true, ChannelCapabilityCheck, getDbChannelCapabilitys);
                    foreach(var tempObj in tempRightList.Where(r => !string.IsNullOrEmpty(r.ChannelCapabilityIdStr))) {
                        var tempobjcheck = dbChannelCapability.FirstOrDefault(r => r.ChannelCapabilityIdStr == tempObj.ChannelCapabilityIdStr);
                        if(tempobjcheck == null) {
                            tempObj.ErrorMsg = "该网络模式不存在";
                        } else {
                            tempObj.ChannelCapabilityId = tempobjcheck.ChannelCapabilityId;
                        }
                    }
                    //6、订货默认仓库，否则提示：对应的订货默认仓库不存在
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();
                    var WarehouseCheck = tempRightList.Select(r => r.PartsSalesCategoryId.ToString()).Distinct().ToArray();
                    var dbWarehouse = new List<DealerServiceInfoExtend>();
                    Func<string[], bool> getDbWarehouses = value => {
                        var dbObj = new DealerServiceInfoExtend {
                            WarehouseId = Convert.ToInt32(value[0]),
                            WarehouseIdStr = value[1],
                            PartsSalesCategoryId = Convert.ToInt32(value[2])
                        };
                        dbWarehouse.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select w.id,w.Name,s.PartsSalesCategoryId from SalesUnit s left join SalesUnitAffiWarehouse su on s.Id = su.SalesUnitId left join Warehouse w on w.Id = su.WarehouseId  where w.status=1 and s.status=1", "s.PartsSalesCategoryId", true, WarehouseCheck, getDbWarehouses);
                    foreach(var tempObj in tempRightList.Where(r => !string.IsNullOrEmpty(r.WarehouseIdStr))) {
                        var tempobjcheck = dbWarehouse.FirstOrDefault(r => r.WarehouseIdStr == tempObj.WarehouseIdStr && r.PartsSalesCategoryId == tempObj.PartsSalesCategoryId);
                        if(tempobjcheck == null) {
                            tempObj.ErrorMsg = "该订货默认仓库不存在";
                        } else {
                            tempObj.WarehouseId = tempobjcheck.WarehouseId;
                        }
                    }
                    //7、查询旧件仓库，否则提示：对应的旧件仓库不存在
                    //tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();
                    //var usedPartsWarehouseNeedCheck = tempRightList.Select(r => r.UsedPartsWarehouseIdStr).Distinct().ToArray();
                    //var dbUsedPartsWarehouses = new List<DealerServiceInfoExtend>();
                    //Func<string[], bool> getDbUsedPartsWarehouses = value => {
                    //    var dbObj = new DealerServiceInfoExtend {
                    //        UsedPartsWarehouseId = Convert.ToInt32(value[0]),
                    //        UsedPartsWarehouseIdStr = value[1],
                    //        PartsSalesCategoryId = Convert.ToInt32(value[2]),
                    //    };
                    //    dbUsedPartsWarehouses.Add(dbObj);
                    //    return false;
                    //};
                    //db.QueryDataWithInOperator("select id,Name,PartsSalesCategoryId from UsedPartsWarehouse where status=1 ", "Name", true, usedPartsWarehouseNeedCheck, getDbUsedPartsWarehouses);
                    //foreach(var tempObj in tempRightList.Where(r => !string.IsNullOrEmpty(r.UsedPartsWarehouseIdStr))) {
                    //    var tempobjcheck = dbUsedPartsWarehouses.FirstOrDefault(r => r.UsedPartsWarehouseIdStr == tempObj.UsedPartsWarehouseIdStr && r.PartsSalesCategoryId == tempObj.PartsSalesCategoryId);
                    //    if(tempobjcheck == null) {
                    //        tempObj.ErrorMsg = "对应的旧件仓库不存在";
                    //    } else {
                    //        tempObj.UsedPartsWarehouseId = tempobjcheck.UsedPartsWarehouseId;
                    //    }
                    //}
                    //8、配件管理费率等级，否则提示：对应的配件管理费率等级不存在
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();
                    var PartsManagementCheck = tempRightList.Select(r => r.PartsManagingFeeGradeIdStr).Distinct().ToArray();
                    var dbPartsManagement = new List<DealerServiceInfoExtend>();
                    Func<string[], bool> getDbPartsManagement = value => {
                        var dbObj = new DealerServiceInfoExtend {
                            PartsManagingFeeGradeId = Convert.ToInt32(value[0]),
                            PartsManagingFeeGradeIdStr = value[1],
                            BranchId = Convert.ToInt32(value[2]),
                            PartsSalesCategoryId = Convert.ToInt32(value[3]),
                        };
                        dbPartsManagement.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select id,Name,BranchId,PartsSalesCategoryId from PartsManagementCostGrade where status=1 ", "Name", true, PartsManagementCheck, getDbPartsManagement);
                    foreach(var tempObj in tempRightList.Where(r => !string.IsNullOrEmpty(r.PartsManagingFeeGradeIdStr))) {
                        var tempobjcheck = dbPartsManagement.FirstOrDefault(r => r.PartsManagingFeeGradeIdStr == tempObj.PartsManagingFeeGradeIdStr && r.BranchId == tempObj.BranchId && r.PartsSalesCategoryId == tempObj.PartsSalesCategoryId);
                        if(tempobjcheck == null) {
                            tempObj.ErrorMsg = "该配件管理费率等级不存在";
                        } else {
                            tempObj.PartsManagingFeeGradeId = tempobjcheck.PartsManagingFeeGradeId;
                        }
                    }
                    //9、查询星级系数，否则提示：对应的结算星级不存在
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();
                    var gradeCoefficientNeedCheck = tempRightList.Select(r => r.GradeCoefficientIdStr).Distinct().ToArray();
                    var dbGradeCoefficients = new List<DealerServiceInfoExtend>();
                    Func<string[], bool> getDbGradeCoefficients = value => {
                        var dbObj = new DealerServiceInfoExtend {
                            GradeCoefficientId = Convert.ToInt32(value[0]),
                            GradeCoefficientIdStr = value[1],
                            PartsSalesCategoryId = Convert.ToInt32(value[2]),
                        };
                        dbGradeCoefficients.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select id,Grade,PartsSalesCategoryId from GradeCoefficient where Status=1 ", "Grade", true, gradeCoefficientNeedCheck, getDbGradeCoefficients);
                    foreach(var tempObj in tempRightList.Where(r => !string.IsNullOrEmpty(r.GradeCoefficientIdStr))) {
                        var tempobjcheck = dbGradeCoefficients.FirstOrDefault(r => r.GradeCoefficientIdStr == tempObj.GradeCoefficientIdStr && r.PartsSalesCategoryId == tempObj.PartsSalesCategoryId);
                        if(tempobjcheck == null) {
                            tempObj.ErrorMsg = "该结算星级不存在";
                        } else {
                            tempObj.GradeCoefficientId = tempobjcheck.GradeCoefficientId;
                        }
                    }
                    //9、查询品牌星级，否则提示：对应的星级不存在
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();
                    var gradeNeedCheck = tempRightList.Select(r => r.GradeStr).Distinct().ToArray();
                    var dbGrades = new List<DealerServiceInfoExtend>();
                    Func<string[], bool> getDbGrades = value => {
                        var dbObj = new DealerServiceInfoExtend {

                            Grade = value[0].ToString(),
                            PartsSalesCategoryId = Convert.ToInt32(value[1]),
                        };
                        dbGrades.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select Grade,BrandId from BrandGradeMessage where Status=1 ", "Grade", true, gradeNeedCheck, getDbGrades);
                    foreach(var tempObj in tempRightList.Where(r => !string.IsNullOrEmpty(r.GradeStr))) {
                        var tempobjcheck = dbGrades.FirstOrDefault(r => r.Grade == tempObj.GradeStr && r.PartsSalesCategoryId == tempObj.PartsSalesCategoryId);
                        if(tempobjcheck == null) {
                            tempObj.ErrorMsg = "该星级不存在";
                        } else {
                            tempObj.Grade = tempobjcheck.Grade;
                        }
                    }
                    //10、查询外出服务等级，否则提示：对应外出服务等级不存在。
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();
                    var serviceTripPriceGradeNeedCheck = tempRightList.Select(r => r.OutFeeGradeIdStr).Distinct().ToArray();
                    var dbServiceTripPriceGrades = new List<DealerServiceInfoExtend>();
                    Func<string[], bool> getDbServiceTripPriceGrades = value => {
                        var dbObj = new DealerServiceInfoExtend {
                            OutFeeGradeId = Convert.ToInt32(value[0]),
                            OutFeeGradeIdStr = value[1],
                            PartsSalesCategoryId = Convert.ToInt32(value[2])
                        };
                        dbServiceTripPriceGrades.Add(dbObj);
                        return false;
                    };
                    foreach(var tempObj in tempRightList.Where(r => !string.IsNullOrEmpty(r.OutFeeGradeIdStr))) {
                        var tempobjcheck = dbServiceTripPriceGrades.FirstOrDefault(r => r.OutFeeGradeIdStr == tempObj.OutFeeGradeIdStr && r.PartsSalesCategoryId == tempObj.PartsSalesCategoryId);
                        if(tempobjcheck == null) {
                            tempObj.ErrorMsg = "外出服务费等级不存在";
                        } else {
                            tempObj.OutFeeGradeId = tempobjcheck.OutFeeGradeId;
                        }
                    }
                    //11、服务站分级验证，提示：对应的服务站分级不存在
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();
                    var GradeName = tempRightList.Select(r => r.PartsSalesCategoryId.ToString()).Distinct().ToArray();
                    var dbGradeName = new List<DealerServiceInfoExtend>();
                    Func<string[], bool> getGradeName = value => {
                        var dbObj = new DealerServiceInfoExtend
                        {
                            PartsSalesCategoryId = Convert.ToInt32(value[0]),
                        };
                        dbGradeName.Add(dbObj);
                        return false;
                    };
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    rightList = allList.Except(errorList).ToList();

                    #endregion
                }
                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempObj = list[index - 1];
                            var values = new object[] {
                                #region 字段
                                tempObj.ChannelCapabilityIdStr, 
                                tempObj.PartsSalesCategoryIdStr,
                                tempObj.DealerIdStr, 
                                tempObj.DealerName,
                                tempObj.BusinessCode,
                                tempObj.BusinessName, 
                                tempObj.ShortName, 
                                tempObj.BranchIdStr, 
                                tempObj.MarketingDepartmentIdStr,
                                tempObj.ServiceStationTypeStr, 
                                tempObj.AreaStr,
                                tempObj.RegionTypeStr, 
                                tempObj.ServicePermission,
                                tempObj.AccreditTimeStr,
                                tempObj.IsOnDuty,
                                tempObj.HotLine,
                                tempObj.Fix,
                                tempObj.Fax,
                                tempObj.WarehouseIdStr,
                                tempObj.SaleandServicesiteLayoutStr, 
                                tempObj.PartReserveAmountStr,
                                tempObj.RepairAuthorityGradeStr,
                                tempObj.PartsManagingFeeGradeIdStr,
                                tempObj.OutFeeGradeIdStr, 
                                tempObj.GradeStr,
                                tempObj.GradeCoefficientIdStr,
                                tempObj.OutServiceradiiStr,
                                tempObj.HourPhone24Str,
                                tempObj.TrunkNetworkTypeStr,
                                tempObj.CenterStackStr, 
                                tempObj.UsedPartsWarehouseIdStr,
                                tempObj.MaterialCostInvoiceTypeStr,
                                tempObj.LaborCostCostInvoiceTypeStr, 
                                tempObj.LaborCostInvoiceRatioStr,
                                tempObj.InvoiceTypeInvoiceRatioStr, 
                                tempObj.ErrorMsg
                                #endregion
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }
                //导入所有合格数据
                if(!rightList.Any())
                    return true;
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    //开启事务，新增更新配件分品牌信息在一个事务内
                    var ts = conn.BeginTransaction();
                    try {
                        if(rightList.Any()) {
                            var userInfo = Utils.GetCurrentUserInfo();

                            #region 新增经销商分公司管理信息
                            var sqlInsertDealerServiceInfo = db.GetInsertSql("DealerServiceInfo", "Id", new[] {
                               "BranchId",
                               "BusinessCode",
                               "BusinessName",
                               "PartsSalesCategoryId",
                               "DealerId",
                               "MarketingDepartmentId",
                               "UsedPartsWarehouseId",
                               "WarehouseId",
                               "ChannelCapabilityId",
                               "PartsManagingFeeGradeId",
                               "GradeCoefficientId",
                               "Grade",
                               "OutServiceradii",
                               "HourPhone24",
                               "TrunkNetworkType",
                               "CenterStack",
                               "BusinessDivision",
                               "AccreditTime",
                               "ServicePermission",
                               "ServiceStationType",
                               "Area",
                               "RegionType",
                               "MaterialCostInvoiceType",
                               "LaborCostCostInvoiceType",
                               "LaborCostInvoiceRatio",
                               "InvoiceTypeInvoiceRatio",
                               "IsOnDuty",
                               "OutFeeGradeId",
                               "RepairAuthorityGrade",
                               "HotLine",
                               "Fix",
                               "Fax",
                               "Status",
                               "PartReserveAmount",
                               "SaleandServicesiteLayout",
                               "CreateTime",
                               "CreatorId",
                               "CreatorName",
                               "ExternalState"
                            });
                            foreach(var item in rightList) {
                                var command = db.CreateDbCommand(sqlInsertDealerServiceInfo, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("BranchId", userInfo.EnterpriseId));//营销分公司Id
                                command.Parameters.Add(db.CreateDbParameter("BusinessCode", item.BusinessCode));//业务编号
                                command.Parameters.Add(db.CreateDbParameter("BusinessName", item.BusinessName));//业务名称
                                command.Parameters.Add(db.CreateDbParameter("PartsSalesCategoryId", item.PartsSalesCategoryId));//配件销售类型Id
                                command.Parameters.Add(db.CreateDbParameter("DealerId", item.DealerId));//经销商Id
                                command.Parameters.Add(db.CreateDbParameter("MarketingDepartmentId", item.MarketingDepartmentId));//市场部Id
                                command.Parameters.Add(db.CreateDbParameter("UsedPartsWarehouseId", item.UsedPartsWarehouseId));//旧件仓库Id
                                command.Parameters.Add(db.CreateDbParameter("WarehouseId", item.WarehouseId));//仓库Id
                                command.Parameters.Add(db.CreateDbParameter("ChannelCapabilityId", item.ChannelCapabilityId));//网络业务能力Id
                                command.Parameters.Add(db.CreateDbParameter("PartsManagingFeeGradeId", item.PartsManagingFeeGradeId));//配件管理费率等级Id
                                command.Parameters.Add(db.CreateDbParameter("GradeCoefficientId", item.GradeCoefficientId));//星级系数Id
                                command.Parameters.Add(db.CreateDbParameter("Grade", item.GradeStr));//星级系数Id
                                command.Parameters.Add(db.CreateDbParameter("OutServiceradii", item.OutServiceradii));//外出服务半径
                                command.Parameters.Add(db.CreateDbParameter("HourPhone24", item.HourPhone24Str));//24小时手机号
                                command.Parameters.Add(db.CreateDbParameter("TrunkNetworkType", item.TrunkNetworkType));//主干网络类型
                                command.Parameters.Add(db.CreateDbParameter("CenterStack", item.CenterStack));//是否中心站
                                command.Parameters.Add(db.CreateDbParameter("BusinessDivision", item.BranchIdStr));//事业本部
                                command.Parameters.Add(db.CreateDbParameter("AccreditTime", item.AccreditTime));//授权时间
                                command.Parameters.Add(db.CreateDbParameter("ServicePermission", item.ServicePermission));//维修权限
                                command.Parameters.Add(db.CreateDbParameter("ServiceStationType", item.ServiceStationType));//服务站类别
                                command.Parameters.Add(db.CreateDbParameter("Area", item.AreaStr));//区域
                                command.Parameters.Add(db.CreateDbParameter("RegionType", item.RegionTypeStr));//地区类别
                                command.Parameters.Add(db.CreateDbParameter("MaterialCostInvoiceType", item.MaterialCostInvoiceType));//材料费开票类型
                                command.Parameters.Add(db.CreateDbParameter("LaborCostCostInvoiceType", item.LaborCostCostInvoiceType));//工时费开票类型
                                command.Parameters.Add(db.CreateDbParameter("LaborCostInvoiceRatio", item.LaborCostInvoiceRatio));//工时费开票系数
                                command.Parameters.Add(db.CreateDbParameter("InvoiceTypeInvoiceRatio", item.InvoiceTypeInvoiceRatio));//材料费开票系数
                                command.Parameters.Add(db.CreateDbParameter("IsOnDuty", item.IsOnDuty));//是否职守
                                command.Parameters.Add(db.CreateDbParameter("OutFeeGradeId", item.OutFeeGradeId));//外出服务费等级Id
                                command.Parameters.Add(db.CreateDbParameter("RepairAuthorityGrade", item.RepairAuthorityGrade));//服务站维修权限
                                command.Parameters.Add(db.CreateDbParameter("HotLine", item.HotLine));//24小时热线
                                command.Parameters.Add(db.CreateDbParameter("Fix", item.Fix));//固定电话
                                command.Parameters.Add(db.CreateDbParameter("Fax", item.Fax));//传真
                                command.Parameters.Add(db.CreateDbParameter("Status", "1"));//状态
                                command.Parameters.Add(db.CreateDbParameter("PartReserveAmount", item.PartReserveAmount));//配件储备金额
                                command.Parameters.Add(db.CreateDbParameter("SaleandServicesiteLayout", item.SaleandServicesiteLayout));//销售与服务场地布局关系
                                command.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now)); //创建时间
                                command.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id)); //创建人Id
                                command.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name)); //创建人
                                command.Parameters.Add(db.CreateDbParameter("ExternalState", 2)); //对外状态 字典项-（1.有效  2.停用）
                                command.ExecuteNonQuery();
                            }
                            #endregion

                            #region 新增经销商分公司管理信息履历
                            var sqlInsertDealerServiceInfoHistory = db.GetInsertSql("DealerServiceInfoHistory", "Id", new[] {
                                "RecordId", "PartsSalesCategoryId", "BranchId", "DealerId", "FileTime", "FilerId", "FilerName", "CreateTime", "CreatorId", "CreatorName", "ABusinessCode", "ABusinessName", "AMarketingDepartmentId", "AUsedPartsWarehouseId", "AWarehouseId", "AChannelCapabilityId", "APartsManagingFeeGradeId", "AGradeCoefficientId","AGrade", "AOutFeeGradeId", "AOutServiceradii","HourPhone24","ATrunkNetworkType","ACenterStack", "ABusinessDivision", "AAccreditTime", "AServicePermission", "AServiceStationType", "AArea", "ARegionType", "AMaterialCostInvoiceType", "ALaborCostCostInvoiceType", "ALaborCostInvoiceRatio", "AInvoiceTypeInvoiceRatio", "AIsOnDuty", "ARepairAuthorityGrade", "AHotLine", "AFix", "AFax", "APartReserveAmount", "ASaleandServicesiteLayout", "AStatus"
                            });
                            foreach(var item in rightList) {
                                var command = db.CreateDbCommand(sqlInsertDealerServiceInfoHistory, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("RecordId", item.Id)); //主记录Id
                                command.Parameters.Add(db.CreateDbParameter("PartsSalesCategoryId", item.PartsSalesCategoryId)); //配件销售类型Id
                                command.Parameters.Add(db.CreateDbParameter("BranchId", item.BranchId)); //营销分公司Id
                                command.Parameters.Add(db.CreateDbParameter("DealerId", item.DealerId)); //经销商Id

                                command.Parameters.Add(db.CreateDbParameter("FileTime", DateTime.Now)); //归档时间
                                command.Parameters.Add(db.CreateDbParameter("FilerId", userInfo.Id)); //归档人Id
                                command.Parameters.Add(db.CreateDbParameter("FilerName", userInfo.Name)); //归档人
                                command.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now)); //创建时间
                                command.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id)); //创建人Id
                                command.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name)); //创建人

                                command.Parameters.Add(db.CreateDbParameter("ABusinessCode", item.BusinessCode)); //变更后业务编号
                                command.Parameters.Add(db.CreateDbParameter("ABusinessName", item.BusinessName)); //变更后业务名称
                                command.Parameters.Add(db.CreateDbParameter("AMarketingDepartmentId", item.MarketingDepartmentId)); //变更后市场部Id
                                command.Parameters.Add(db.CreateDbParameter("AUsedPartsWarehouseId", item.UsedPartsWarehouseId)); //变更后旧件仓库Id
                                command.Parameters.Add(db.CreateDbParameter("AWarehouseId", item.WarehouseId)); //变更后仓库Id
                                command.Parameters.Add(db.CreateDbParameter("AChannelCapabilityId", item.ChannelCapabilityId)); //变更后网络业务能力Id
                                command.Parameters.Add(db.CreateDbParameter("APartsManagingFeeGradeId", item.PartsManagingFeeGradeId)); //变更后配件管理费率等级Id
                                command.Parameters.Add(db.CreateDbParameter("AGradeCoefficientId", item.GradeCoefficientId)); //变更后星级系数Id
                                command.Parameters.Add(db.CreateDbParameter("AGrade", item.GradeStr)); //变更后星级系数Id

                                command.Parameters.Add(db.CreateDbParameter("AOutFeeGradeId", item.OutFeeGradeId)); //变更后外出服务费等级Id
                                command.Parameters.Add(db.CreateDbParameter("AOutServiceradii", item.OutServiceradii)); //变更后外出服务半径

                                command.Parameters.Add(db.CreateDbParameter("HourPhone24", item.HourPhone24Str)); //24小时手机号
                                command.Parameters.Add(db.CreateDbParameter("ATrunkNetworkType", item.TrunkNetworkType)); //主干网络类型
                                command.Parameters.Add(db.CreateDbParameter("ACenterStack", item.CenterStack)); //是否中心站


                                command.Parameters.Add(db.CreateDbParameter("ABusinessDivision", item.BusinessDivision)); //变更后事业本部
                                command.Parameters.Add(db.CreateDbParameter("AAccreditTime", item.AccreditTime)); //变更后授权时间
                                command.Parameters.Add(db.CreateDbParameter("AServicePermission", item.ServicePermission)); //变更后维修权限
                                command.Parameters.Add(db.CreateDbParameter("AServiceStationType", item.ServiceStationType)); //变更后服务站类别
                                command.Parameters.Add(db.CreateDbParameter("AArea", item.AreaStr)); //变更后区域
                                command.Parameters.Add(db.CreateDbParameter("ARegionType", item.RegionTypeStr)); //变更后地区类别
                                command.Parameters.Add(db.CreateDbParameter("AMaterialCostInvoiceType", item.MaterialCostInvoiceType)); //变更后材料费开票类型
                                command.Parameters.Add(db.CreateDbParameter("ALaborCostCostInvoiceType", item.LaborCostCostInvoiceType)); //变更后工时费开票类型
                                command.Parameters.Add(db.CreateDbParameter("ALaborCostInvoiceRatio", item.LaborCostInvoiceRatio)); //变更后工时费开票系数
                                command.Parameters.Add(db.CreateDbParameter("AInvoiceTypeInvoiceRatio", item.InvoiceTypeInvoiceRatio)); //变更后材料费开票系数
                                command.Parameters.Add(db.CreateDbParameter("AIsOnDuty", item.IsOnDuty)); //变更后是否职守
                                command.Parameters.Add(db.CreateDbParameter("ARepairAuthorityGrade", item.RepairAuthorityGrade)); //变更后服务站维修权限
                                command.Parameters.Add(db.CreateDbParameter("AHotLine", item.HotLine)); //变更后24小时热线
                                command.Parameters.Add(db.CreateDbParameter("AFix", item.Fix)); //变更后固定电话
                                command.Parameters.Add(db.CreateDbParameter("AFax", item.Fax)); //变更后传真
                                command.Parameters.Add(db.CreateDbParameter("APartReserveAmount", item.PartReserveAmount)); //变更后配件储备金额
                                command.Parameters.Add(db.CreateDbParameter("ASaleandServicesiteLayout", item.SaleandServicesiteLayout)); //变更后销售与服务场地布局关系
                                command.Parameters.Add(db.CreateDbParameter("AStatus", "1")); //变更后状态
                                command.ExecuteNonQuery();
                            }
                            #endregion

                        }
                        //无异常提交
                        ts.Commit();
                    } catch(Exception) {
                        //报错回滚
                        ts.Rollback();
                        throw;
                    } finally {
                        if(conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }

        public bool ImportEditDealerServiceInfo(string fileName, out int excelImportNum, out List<DealerBusinessPermitExtend> rightData, out List<DealerBusinessPermitExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<DealerBusinessPermitExtend>();
            var rightList = new List<DealerBusinessPermitExtend>();
            var allList = new List<DealerBusinessPermitExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("DealerBusinessPermit", out notNullableFields, out fieldLenght);
                List<object> excelColumns;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    //服务站编号、服务站名称、品牌、服务产品线编号、服务产品线名称、产品线类型、工时单价等级,其中服务站编号、品牌、服务产品线编号、产品线类型、工时单价等级导入模板必填
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_Code, "DealerCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_Name, "DealerName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Partssalescategory_Name, "PartsSalesCategoryName");
                    excelOperator.AddColumnDataSource("服务产品线编号", "ProductLineCode");
                    excelOperator.AddColumnDataSource("服务产品线名称", "ProductLineName");
                    excelOperator.AddColumnDataSource("产品线类型", "ProductLineType");
                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;

                    #region 获取对应枚举
                    var keyValuePairs = new[] {
                            new KeyValuePair<string, string>("ProductLineType", "ServiceProductLineView_ProductLineType")
                        };
                    tempExcelOperator.ImportHelper.LoadEnumFromDb(null, null, keyValuePairs);
                    #endregion
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var tempImportObj = new DealerBusinessPermitExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        #region
                        tempImportObj.DealerCode = newRow["DealerCode"];
                        tempImportObj.DealerName = newRow["DealerName"];
                        tempImportObj.PartsSalesCategoryName = newRow["PartsSalesCategoryName"];
                        tempImportObj.ProductLineCode = newRow["ProductLineCode"];
                        tempImportObj.ProductLineName = newRow["ProductLineName"];
                        tempImportObj.ProductLineTypeStr = newRow["ProductLineType"];
                        #endregion
                        var tempErrorMessage = new List<string>();
                        #region 导入的内容基本检查
                        //服务站编号、品牌、服务产品线编号、产品线类型、工时单价等级导入模板必填
                        //品牌检查
                        if(string.IsNullOrEmpty(tempImportObj.DealerCode)) {
                            tempErrorMessage.Add("服务站编号不能为空");
                        }

                        if(string.IsNullOrEmpty(tempImportObj.PartsSalesCategoryName)) {
                            tempErrorMessage.Add(ErrorStrings.Export_Validation_MarketingDepartment_PartsSalesCategoryIsNull);
                        }

                        if(string.IsNullOrEmpty(tempImportObj.ProductLineCode)) {
                            tempErrorMessage.Add("服务产品线编号不能为空");
                        }
                        if(string.IsNullOrEmpty(tempImportObj.ProductLineTypeStr)) {
                            tempErrorMessage.Add("产品线类型不能为空");
                        } else {
                            try {
                                tempImportObj.ProductLineType = (int)Enum.Parse(typeof(DcsServiceProductLineViewProductLineType), tempImportObj.ProductLineTypeStr);
                            } catch(Exception) {
                                tempErrorMessage.Add("产品线类型不正确");
                            }
                        }

                        #endregion
                        if(tempErrorMessage.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });

                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    var groups = tempRightList.GroupBy(r => new {
                        r.DealerCode,
                        r.PartsSalesCategoryName,
                        r.ProductLineCode,
                        r.ProductLineType
                    }).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        groupItem.ErrorMsg = ErrorStrings.ImpPartsSupplierRelation_Validation31;
                    }
                    //4、查询配件销售类型（依据经销商分公司管理信息.经销商Id=经销商服务经营权限.经销商Id,配件销售类型id=经销商分公司管理信息.配件销售类型Id）校验品牌存在配件销售类型 否则提示：品牌不存在。
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();
                    var partsSalesCategoryNeedCheck = tempRightList.Select(r => r.PartsSalesCategoryName).Distinct().ToArray();
                    var dbPartsSalesCategories = new List<DealerBusinessPermitExtend>();
                    Func<string[], bool> getDbPartsSalesCategories = value => {
                        var dbObj = new DealerBusinessPermitExtend {
                            PartsSalesCategoryId = Convert.ToInt32(value[0]),
                            PartsSalesCategoryName = value[1]
                        };
                        dbPartsSalesCategories.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select id,name from PartsSalesCategory where status=1 ", "Name", true, partsSalesCategoryNeedCheck, getDbPartsSalesCategories);
                    foreach(var tempObj in tempRightList) {
                        var tempobjcheck = dbPartsSalesCategories.FirstOrDefault(r => r.PartsSalesCategoryName == tempObj.PartsSalesCategoryName);
                        if(tempobjcheck == null) {
                            tempObj.ErrorMsg = ErrorStrings.Export_Title_Partssalescategory_Name + tempObj.PartsSalesCategoryName + ErrorStrings.Export_Validation_Sparepart_NotExist;
                        } else {
                            tempObj.PartsSalesCategoryId = tempobjcheck.PartsSalesCategoryId;
                        }
                    }
                    //1、查询经销商基本信息,(依据 经销商服务经营权限.经销商Id=经销商基本信息.Id)，校验经销商基本信息.经销商编号,名称存在，否则提示：经销商编号，经销商名称不存在。
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();
                    var dealerCodeNeedCheck = tempRightList.Select(r => r.DealerCode).Distinct().ToArray();
                    var dbDealers = new List<DealerBusinessPermitExtend>();
                    Func<string[], bool> getDbDealers = value => {
                        var dbObj = new DealerBusinessPermitExtend {
                            DealerId = Convert.ToInt32(value[0]),
                            DealerCode = value[1],
                            DealerName = value[2]
                        };
                        dbDealers.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperatorByLower("select id,code,name from dealer where status=1 ", "Code", true, dealerCodeNeedCheck, getDbDealers);
                    foreach(var tempObj in tempRightList) {
                        var tempobjcheck = dbDealers.FirstOrDefault(r => r.DealerCode == tempObj.DealerCode);
                        if(tempobjcheck == null) {
                            tempObj.ErrorMsg = tempObj.DealerCode + ErrorStrings.Export_Validation_PartsSpecialTreatyPrice_Validation1;
                        } else {
                            tempObj.DealerId = tempobjcheck.DealerId;
                        }
                    }

                    //企业分品牌信息不存在（有效的）  提示 企业分品牌信息不存在
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();
                    var dealerIdNeedCheck = tempRightList.Select(r => r.DealerId.ToString()).Distinct().ToArray();
                    var dbDealerServiceInfos = new List<DealerServiceInfoExtend>();
                    Func<string[], bool> getDbDealerServiceInfos = value => {
                        var dbObj = new DealerServiceInfoExtend {
                            Id = Convert.ToInt32(value[0]),
                            DealerId = Convert.ToInt32(value[1]),
                            PartsSalesCategoryId = Convert.ToInt32(value[2]),
                        };
                        dbDealerServiceInfos.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select Id,DealerId,PartsSalesCategoryId from DealerServiceInfo where status=1  ", "DealerId", true, dealerIdNeedCheck, getDbDealerServiceInfos);
                    foreach(var tempObj in tempRightList) {
                        var tempobjcheck = dbDealerServiceInfos.FirstOrDefault(r => r.DealerId == tempObj.DealerId && r.PartsSalesCategoryId == tempObj.PartsSalesCategoryId);
                        if(tempobjcheck == null) {
                            tempObj.ErrorMsg = "企业分品牌信息不存在";
                        }
                    }
                    //2、查询服务产品线视图（依据经销商服务经营权限.服务产品线Id=服务产品线（视图）.Id）服务产品线编号、服务产品线名称组合存在于服务产品线（视图），否则提示：服务产品线编号对应的服务产品线名称不存在。
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();
                    var productLineCodeNeedCheck = tempRightList.Select(r => r.ProductLineCode).Distinct().ToArray();
                    var dbProductLines = new List<DealerBusinessPermitExtend>();
                    Func<string[], bool> getDbProductLines = value => {
                        var dbObj = new DealerBusinessPermitExtend {
                            ProductLineId = Convert.ToInt32(value[0]),
                            ProductLineCode = value[1],
                            ProductLineName = value[2],
                            ProductLineType = Convert.ToInt32(value[3]),
                            BranchId = Convert.ToInt32(value[4]),
                            PartsSalesCategoryId = Convert.ToInt32(value[5]),
                        };
                        dbProductLines.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select ProductLineid,ProductLinecode,ProductLinename,ProductLineType,branchid,PartsSalesCategoryId from ServiceProductLineView", "ProductLinecode", true, productLineCodeNeedCheck, getDbProductLines);
                    foreach(var tempObj in tempRightList) {
                        var tempobjcheck = dbProductLines.FirstOrDefault(r => r.ProductLineCode == tempObj.ProductLineCode && r.ProductLineType == tempObj.ProductLineType);
                        if(tempobjcheck == null) {
                            tempObj.ErrorMsg = "产品线信息不存在";
                        } else {
                            tempObj.ProductLineId = tempobjcheck.ProductLineId;
                            tempObj.BranchId = tempobjcheck.BranchId;
                        }
                    }

                    //品牌下是否存在服务产品线
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();
                    var artsSalesCategoryNeedCheck = tempRightList.Select(r => r.PartsSalesCategoryId.ToString()).Distinct().ToArray();
                    var dbPartsSalesCategorys = new List<DealerBusinessPermitExtend>();
                    Func<string[], bool> getDbPartsSalesCategorys = value => {
                        var dbObj = new DealerBusinessPermitExtend {
                            ProductLineId = Convert.ToInt32(value[0]),
                            PartsSalesCategoryId = Convert.ToInt32(value[1]),
                        };
                        dbPartsSalesCategorys.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select ProductLineId,PartsSalesCategoryId from ServiceProductLineView ", "PartsSalesCategoryId", true, artsSalesCategoryNeedCheck, getDbPartsSalesCategorys);
                    foreach(var tempObj in tempRightList) {
                        if(!dbPartsSalesCategorys.Any(r => r.ProductLineId == tempObj.ProductLineId && r.PartsSalesCategoryId == tempObj.PartsSalesCategoryId)) {
                            tempObj.ErrorMsg = ErrorStrings.Export_Title_Partssalescategory_Name + tempObj.PartsSalesCategoryName + "不存在产品线" + tempObj.ProductLineCode;
                        }
                    }


                    //3、查询工时单价等级 （依据经销商服务经营权限.工时单价等级Id=工时单价等级.Id）校验工时单价等级名称存在，否则提示：工时单价等级名称不存在。
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();
                    var dbLaborHourUnitPriceGrades = new List<DealerBusinessPermitExtend>();
                    Func<string[], bool> getDbLaborHourUnitPriceGrades = value => {
                        var dbObj = new DealerBusinessPermitExtend {
                            LaborHourUnitPriceGradeId = Convert.ToInt32(value[0]),
                            BranchId = Convert.ToInt32(value[1]),
                        };
                        dbLaborHourUnitPriceGrades.Add(dbObj);
                        return false;
                    };
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    //根据导入的经销商编号和品牌找到对应的这条数
                    rightList = allList.Except(errorList).ToList();
                }

                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempObj = list[index - 1];
                            var values = new object[] {
                                tempObj.DealerCode,
                                tempObj.DealerName,
                                tempObj.PartsSalesCategoryName,
                                tempObj.ProductLineCode,
                                tempObj.ProductLineName,
                                tempObj.ProductLineTypeStr,
                                tempObj.ErrorMsg
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }

                //导入所有合格数据
                if(!rightList.Any())
                    return true;
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    //开启事务，新增更新配件在一个事务内
                    var ts = conn.BeginTransaction();
                    try {
                        if(rightList.Any()) {
                            //根据dealerid删除数据
                            //var sqlDetal
                            var sqlDelete = "delete from  DealerBusinessPermit r  where  ServiceProductLineId in(select ProductLineid from ServiceProductLineView where PartsSalesCategoryId=:PartsSalesCategoryId and BranchId=:BranchId) and  DealerId=:DealerId";
                            foreach(var item in rightList) {
                                var command = db.CreateDbCommand(sqlDelete, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("DealerId", item.DealerId));
                                command.Parameters.Add(db.CreateDbParameter("BranchId", item.BranchId));
                                command.Parameters.Add(db.CreateDbParameter("PartsSalesCategoryId", item.PartsSalesCategoryId));
                                command.ExecuteNonQuery();
                            }

                            var sqlInsert = db.GetInsertSql("DealerBusinessPermit", "Id", new[] {"DealerId","ServiceProductLineId","ProductLineType","BranchId","ServiceFeeGradeId","CreateTime","CreatorId","CreatorName"
                            });
                            var userInfo = Utils.GetCurrentUserInfo();
                            foreach(var item in rightList) {
                                var command = db.CreateDbCommand(sqlInsert, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("DealerId", item.DealerId));
                                command.Parameters.Add(db.CreateDbParameter("BranchId", item.BranchId));
                                command.Parameters.Add(db.CreateDbParameter("ServiceProductLineId", item.ProductLineId));
                                command.Parameters.Add(db.CreateDbParameter("ProductLineType", item.ProductLineType));
                                command.Parameters.Add(db.CreateDbParameter("ServiceFeeGradeId", item.LaborHourUnitPriceGradeId));
                                command.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                command.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                command.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                var tempId = db.ExecuteInsert(command, "Id");
                            }
                        }
                        //无异常提交
                        ts.Commit();
                    } catch(Exception) {
                        //报错回滚
                        ts.Rollback();
                        throw;
                    } finally {
                        if(conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }

        }

    }
}
