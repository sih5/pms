﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        //导入二级站
        public bool ImportSubDealer(string fileName, out int excelImportNum, int branchId, out List<SubDealerExtend> rightData, out List<SubDealerExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<SubDealerExtend>();
            var rightList = new List<SubDealerExtend>();
            var allList = new List<SubDealerExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("SubDealer", out notNullableFields, out fieldLenght);
                List<string> notNullableFieldsDealer;
                Dictionary<string, int> fieldLenghtDealer;
                db.GetTableSchema("Dealer", out notNullableFieldsDealer, out fieldLenghtDealer);

                List<object> excelColumns;

                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    #region 指定文件中的列对应的名称 例如 excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode, "PartCode");
                    excelOperator.AddColumnDataSource("二级站编号", "Code");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_DealerPartsRetailOrder_SubDealerName, "Name");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_Code, "DealerCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_Name, "DealerName");
                    excelOperator.AddColumnDataSource("二级站类型", "SubDealerType");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_Manager, "Manager");
                    excelOperator.AddColumnDataSource("服务站站长", "DealerManager");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_ContactPhone, "ManagerPhoneNumber");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Agency_Email, "ManagerMail");
                    excelOperator.AddColumnDataSource("移动电话", "ManagerMobile");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Warehouse_Address, "Address");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Remark, "Remark");
                    #endregion

                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;

                    #region 获取对应枚举
                    var keyValuePairs = new[] {
                            new KeyValuePair<string, string>("DebitOrReplenish", "SubDealer_DebitOrReplenish")
                        };
                    tempExcelOperator.ImportHelper.LoadEnumFromDb(null,null, keyValuePairs);
                    #endregion

                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var tempImportObj = new SubDealerExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        #region 根据导入的字段给对应的实体属性赋值
                        tempImportObj.CodeStr = newRow["Code"];
                        tempImportObj.NameStr = newRow["Name"];
                        tempImportObj.DealerCodeStr = newRow["DealerCode"];
                        tempImportObj.DealerNameStr = newRow["DealerName"];
                        tempImportObj.SubDealerTypeStr = newRow["SubDealerType"];
                        tempImportObj.ManagerStr = newRow["Manager"];
                        tempImportObj.DealerManagerStr = newRow["DealerManager"];
                        tempImportObj.ManagerPhoneNumberStr = newRow["ManagerPhoneNumber"];
                        tempImportObj.ManagerMailStr = newRow["ManagerMail"];
                        tempImportObj.ManagerMobileStr = newRow["ManagerMobile"];
                        tempImportObj.AddressStr = newRow["Address"];
                        tempImportObj.RemarkStr = newRow["Remark"];
                        #endregion

                        var tempErrorMessage = new List<string>();

                        #region 导入的数据基本检查
                        var fieldIndex = notNullableFields.IndexOf("Code".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.CodeStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add("二级站编号不能为空");
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.CodeStr) > fieldLenght["Code".ToUpper()])
                                tempErrorMessage.Add("二级站编号过长");
                        }

                        fieldIndex = notNullableFields.IndexOf("Name".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.NameStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add("二级站名称不能为空");
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.NameStr) > fieldLenght["Name".ToUpper()])
                                tempErrorMessage.Add("二级站名称过长");
                        }

                        fieldIndex = notNullableFields.IndexOf("DealerId".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.DealerCodeStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add("服务站编号不能为空");
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.DealerCodeStr) > fieldLenghtDealer["Code".ToUpper()])
                                tempErrorMessage.Add("服务站编号过长");
                        }

                        fieldIndex = notNullableFields.IndexOf("DealerId".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.DealerNameStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add("服务站名称不能为空");
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.DealerNameStr) > fieldLenghtDealer["Name".ToUpper()])
                                tempErrorMessage.Add("服务站名称过长");
                        }

                        fieldIndex = notNullableFields.IndexOf("SubDealerType".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.SubDealerTypeStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add("二级站类型不能为空");
                        } else {
                            try {
                                tempImportObj.SubDealerType = (int)Enum.Parse(typeof(DcsSubDealerType), tempImportObj.SubDealerTypeStr);
                            } catch(Exception) {
                                tempErrorMessage.Add("二级站类型不正确");
                            }
                        }

                        fieldIndex = notNullableFields.IndexOf("Manager".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.ManagerStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add("服务经理不能为空");
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.ManagerStr) > fieldLenght["Manager".ToUpper()])
                                tempErrorMessage.Add("服务经理过长");
                        }

                        fieldIndex = notNullableFields.IndexOf("DealerManager".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.DealerManagerStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add("服务站站长不能为空");
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.DealerManagerStr) > fieldLenght["DealerManager".ToUpper()])
                                tempErrorMessage.Add("服务站站长过长");
                        }

                        fieldIndex = notNullableFields.IndexOf("ManagerPhoneNumber".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.ManagerPhoneNumberStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Dealer_Validation41);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.ManagerPhoneNumberStr) > fieldLenght["ManagerPhoneNumber".ToUpper()])
                                tempErrorMessage.Add("固定电话过长");
                        }

                        fieldIndex = notNullableFields.IndexOf("Address".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.AddressStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add("地址不能为空");
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.AddressStr) > fieldLenght["Address".ToUpper()])
                                tempErrorMessage.Add("地址过长");
                        }

                        fieldIndex = notNullableFields.IndexOf("Remark".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.RemarkStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchasePricingDetail_Validation15);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.RemarkStr) > fieldLenght["Remark".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchasePricingDetail_Validation16);
                        }
                        #endregion

                        if(tempErrorMessage.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });
                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    #region 剩下的数据进行业务检查
                    //4、校验导入文件中二级站编号 服务站编号组合是否重复， 有重复则提示：文件内数据重复
                    var groups = tempRightList.GroupBy(r => new {
                        r.CodeStr,
                        r.DealerNameStr
                    }).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        groupItem.ErrorMsg = ErrorStrings.ImpPartsSupplierRelation_Validation31;
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    //1、校验服务站名称 是否存在于经销商信息中 否则 提示：二级站***所属一级站 不存在
                    //存在则保存一级站Id
                    var dealerNamesNeedCheck = tempRightList.Select(r => r.DealerNameStr).Distinct().ToArray();
                    var dbDealers = new List<DealerExtend>();
                    Func<string[], bool> getDbDealerNames = value => {
                        var dbObj = new DealerExtend {
                            Id = Convert.ToInt32(value[0]),
                            Name = value[1]
                        };
                        dbDealers.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select Id,Name from Dealer where status=1 ", "Name", false, dealerNamesNeedCheck, getDbDealerNames);
                    foreach(var tempRight in tempRightList) {
                        var dealer = dbDealers.FirstOrDefault(v => v.Name == tempRight.DealerNameStr);
                        if(dealer == null) {
                            tempRight.ErrorMsg = String.Format("服务站名称“{0}”不存在", tempRight.DealerNameStr);
                            continue;
                        }
                        tempRight.DealerId = dealer.Id;
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    //2、二级站编号、一级站Id组合存在于营销分公司，是则提示：导入的二级站***已存在于所属一级站***中
                    var codesNeedCheck = tempRightList.Select(r => r.CodeStr).Distinct().ToArray();
                    var dbBranchs = new List<SubDealerExtend>();
                    Func<string[], bool> getDbSubDealerCodes = value => {
                        var dbObj = new SubDealerExtend {
                            Code = value[0],
                            DealerId = Convert.ToInt32(value[1])
                        };
                        dbBranchs.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select Code,DealerId from SubDealer where status=1 ", "Code", false, codesNeedCheck, getDbSubDealerCodes);
                    foreach(var tempRight in from tempRight in tempRightList
                                             let marketDptPersonnelRelation = dbBranchs.FirstOrDefault(v => v.Code == tempRight.CodeStr && v.DealerId == tempRight.DealerId)
                                             where marketDptPersonnelRelation != null
                                             select tempRight) {
                        tempRight.ErrorMsg = ErrorStrings.Export_Validation_MarketingDepartment_Validation4;
                    }
                    #endregion

                    //获取所有不合格数据
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    //获取所有合格数据
                    rightList = allList.Except(errorList).ToList();

                    #region 将合格数据的值填上
                    foreach(var rightItem in rightList) {
                        rightItem.Code = rightItem.CodeStr;
                        rightItem.Name = rightItem.NameStr;
                        rightItem.DealerCode = rightItem.DealerCodeStr;
                        rightItem.DealerName = rightItem.DealerNameStr;
                        rightItem.Manager = rightItem.ManagerStr;
                        rightItem.DealerManager = rightItem.DealerManagerStr;
                        rightItem.ManagerPhoneNumber = rightItem.ManagerPhoneNumberStr;
                        rightItem.ManagerMail = rightItem.ManagerMailStr;
                        rightItem.ManagerMobile = rightItem.ManagerMobileStr;
                        rightItem.Address = rightItem.AddressStr;
                        rightItem.Remark = rightItem.RemarkStr;
                        rightItem.Status = (int)DcsBaseDataStatus.有效;
                    }
                    #endregion
                }
                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempObj = list[index - 1];
                            var values = new object[] {
                                #region 设置错误信息导出的列的值
                                tempObj.CodeStr, tempObj.NameStr, 
                                tempObj.DealerCodeStr,tempObj.DealerNameStr,
                                tempObj.SubDealerTypeStr,tempObj.ManagerStr,
                                tempObj.DealerManagerStr,tempObj.ManagerPhoneNumberStr,
                                tempObj.ManagerMailStr,tempObj.ManagerMobileStr,
                                tempObj.AddressStr,tempObj.RemarkStr,
                                tempObj.ErrorMsg
                                #endregion
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }

                //导入所有合格数据
                if(!rightList.Any())
                    return true;
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    //开启事务，新增更新配件在一个事务内
                    var ts = conn.BeginTransaction();
                    try {
                        //新增配件
                        if(rightList.Any()) {
                            #region 获取对应的插入的SQL 通过db.GetInsertSql方法
                            //获取新增数据的sql语句，Id为主键
                            var sqlInsert = db.GetInsertSql("SubDealer", "Id", new[] {
                                    "Code", "Name", "DealerId","SubDealerType", "Manager","DealerManager","ManagerPhoneNumber",
                                    "ManagerMail","ManagerMobile","Address","Status","Remark"
                                });
                            #endregion
                            //往数据库增加配件信息
                            foreach(var item in rightList) {
                                #region 添加Sql的参数
                                var command = db.CreateDbCommand(sqlInsert, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("Code", item.Code));
                                command.Parameters.Add(db.CreateDbParameter("Name", item.Name));
                                command.Parameters.Add(db.CreateDbParameter("DealerId", item.DealerId));
                                command.Parameters.Add(db.CreateDbParameter("SubDealerType", item.SubDealerType));
                                command.Parameters.Add(db.CreateDbParameter("Manager", item.Manager));
                                command.Parameters.Add(db.CreateDbParameter("DealerManager", item.DealerManager));
                                command.Parameters.Add(db.CreateDbParameter("ManagerPhoneNumber", item.ManagerPhoneNumber));
                                command.Parameters.Add(db.CreateDbParameter("ManagerMail", item.ManagerMail));
                                command.Parameters.Add(db.CreateDbParameter("ManagerMobile", item.ManagerMobile));
                                command.Parameters.Add(db.CreateDbParameter("Address", item.Address));
                                command.Parameters.Add(db.CreateDbParameter("Status", item.Status));
                                command.Parameters.Add(db.CreateDbParameter("Remark", item.Remark));
                                command.ExecuteNonQuery();
                                #endregion
                            }
                        }
                        //无异常提交
                        ts.Commit();
                    } catch(Exception) {
                        //报错回滚
                        ts.Rollback();
                        throw;
                    } finally {
                        if(conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }
    }
}
