﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService
{
    public partial class ExcelService
    {
        public bool ExportAccountPeriod(int[] ids, string year, string month, int? status, DateTime? bCreateTime, DateTime? eCreateTime, out string fileName)
        {
            //导出账期设置.xlsx
            fileName = GetExportFilePath(ErrorStrings.File_Name_ExportAccount);
            try
            {
                if (!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);
                using (var conn = db.CreateDbConnection())
                {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.Append(@"select a.year,a.month,(select value from keyvalueitem where name='AccountPeriodStatus' and key=a.Status),a.creatorname,a.createtime,a.modifiername,a.modifytime from AccountPeriod a  where 1=1 ");
                    var dbParameters = new List<DbParameter>();
                    if (ids != null && ids.Length > 0)
                    {
                        sql.Append(" and a.id in (");
                        for (var i = 0; i < ids.Length; i++)
                        {
                            if (ids.Length == i + 1)
                            {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                            else
                            {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                        }
                        sql.Append(")");
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(year))
                        {
                            sql.Append("and LOWER(a.year) like {0}year ");
                            dbParameters.Add(db.CreateDbParameter("year", "%" + year.ToLower() + "%"));
                        }
                        if (!string.IsNullOrEmpty(month))
                        {
                            sql.Append(" and LOWER(a.month) like {0}month");
                            dbParameters.Add(db.CreateDbParameter("month", "%" + month.ToLower() + "%"));
                        }
                       
                        if (status.HasValue)
                        {
                            sql.Append(@" and a.Status = {0}status ");
                            dbParameters.Add(db.CreateDbParameter("status", status.Value));
                        }

                        if (bCreateTime.HasValue)
                        {
                            sql.Append(@" and a.createTime >=To_date({0}bCreateTime,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = bCreateTime.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("bCreateTime", tempTime.ToString("G")));
                        }
                        if (eCreateTime.HasValue)
                        {
                            sql.Append(@" and a.createTime <=To_date({0}eCreateTime,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = eCreateTime.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("eCreateTime", tempTime.ToString("G")));
                        }
                       
                    }
                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using (var excelExport = new ExcelExport(fileName))
                    {
                        excelExport.ExportByRow(index =>
                        {
                            if (index == 0)
                            {
                                return new object[] {

                                    ErrorStrings.ExportFile_Title_Account_Year, ErrorStrings.ExportFile_Title_Account_Month, ErrorStrings.Export_Title_AccountPeriod_Status, ErrorStrings.Export_Title_AccountPeriod_CreatorName, ErrorStrings.Export_Title_AccountPeriod_CreateTime, ErrorStrings.Export_Title_AccountPeriod_ModifierName, ErrorStrings.Export_Title_AccountPeriod_ModifyTime
                                };
                            }
                            if (reader.Read())
                            {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if (num != reader.FieldCount)
                                {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
