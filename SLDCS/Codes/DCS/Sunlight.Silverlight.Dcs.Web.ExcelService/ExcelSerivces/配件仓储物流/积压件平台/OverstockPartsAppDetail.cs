﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        public bool ImportOverstockPartsAppDetail(int branchId, int partsSalesCategoryId, int companyType, string fileName, out int excelImportNum, out List<OverstockPartsAppDetailExtend> rightData, out List<OverstockPartsAppDetailExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<OverstockPartsAppDetailExtend>();
            var rightList = new List<OverstockPartsAppDetailExtend>();
            var allList = new List<OverstockPartsAppDetailExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);
                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("UsedPartsReturnDetail", out notNullableFields, out fieldLenght);

                List<object> excelColumns;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    //指定文件中的列对应的名称
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Code, "SparePartCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName, "SparePartName");

                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    //Excel单行导入
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        //接收Excel导入数据
                        var tempImportObj = new OverstockPartsAppDetailExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);

                        tempImportObj.SparePartCodeStr = newRow["SparePartCode"];
                        tempImportObj.SparePartNameStr = newRow["SparePartName"];

                        var errorMsgs = new List<string>();
                        //配件图号检查
                        var fieldIndex = notNullableFields.IndexOf("SparePartCode".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.SparePartCodeStr)) {
                            if(fieldIndex > -1)
                                errorMsgs.Add(ErrorStrings.OverstockPartsAppDetail__Validation1);
                        }
                        if(errorMsgs.Count > 0)
                            tempImportObj.ErrorMsg = string.Join("; ", errorMsgs);
                        allList.Add(tempImportObj);
                        return false;
                    });
                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    //剩下的数据进行业务检查
                    var sparePartCodeCheck = tempRightList.Select(r => r.SparePartCodeStr).Distinct().ToArray();
                    var dbSparePartCodes = new List<OverstockPartsAppDetailExtend>();
                    Func<string[], bool> getdbSparePartCodes = values => {
                        decimal partsSalesPrice = -1;
                        if(!string.IsNullOrEmpty(values[6]))
                            partsSalesPrice = Convert.ToDecimal(values[6]);
                        var overstockPartsApp = new OverstockPartsAppDetailExtend();
                        overstockPartsApp.Id = Convert.ToInt32(values[1]);
                        overstockPartsApp.SparePartCode = values[2];
                        overstockPartsApp.SparePartName = values[3];
                        overstockPartsApp.PartsSalesPrice = partsSalesPrice;
                        dbSparePartCodes.Add(overstockPartsApp);
                        return false;
                    };

                    if(companyType == (int)DcsCompanyType.代理库 || companyType == (int)DcsCompanyType.分公司) {
                        var sql = string.Format("select distinct d.BranchId, d.PartId, b.Code as SparePartCode,b.Name as SparePartName,"
                              + " p.PartsSalesCategoryId, p.PartsSalesCategoryName,p.SalesPrice "
                              + "from PartsStock  d left join SparePart b on d.PartId=b.Id  "
                              + "left join PartsSalesPrice p on d.PartId=p.SparePartId where  d.BranchId={0}  and p.PartsSalesCategoryId={1}",
                              branchId, partsSalesCategoryId);
                        db.QueryDataWithInOperator(sql, "b.Code", false, sparePartCodeCheck, getdbSparePartCodes);
                    } else {
                        var sql = string.Format("select distinct d.BranchId, d.SparePartId, d.SparePartCode,b.Name as SparePartName, d.SalesCategoryId, d.SalesCategoryName,p.SalesPrice "
                        + "from DealerPartsStock  d left join SparePart b on d.SparePartId=b.Id  left join PartsSalesPrice p on d.SparePartId=p.SparePartId where  d.BranchId={0} and  d.SalesCategoryId={1}",
                        branchId, partsSalesCategoryId);
                        db.QueryDataWithInOperator(sql, "d.SparePartCode", false, sparePartCodeCheck, getdbSparePartCodes);
                    }
                    foreach(var item in tempRightList) {
                        var temp = dbSparePartCodes.FirstOrDefault(r => r.SparePartCode == item.SparePartCodeStr);
                        if(temp == null)
                            item.ErrorMsg = "库存信息不存在";
                        else if(temp.PartsSalesPrice == -1) {
                            item.ErrorMsg = "配件对应的销售价不能为空";
                        } else {
                            item.Id = temp.Id;
                            item.SparePartCode = temp.SparePartCode;
                            item.SparePartName = temp.SparePartName;
                            item.PartsSalesPrice = temp.PartsSalesPrice;
                        }
                    }

                    //获取所有不合格数据
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    //获取所有合格数据
                    rightList = allList.Except(errorList).ToList();
                }
                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempObj = list[index - 1];
                            var values = new object[] {
                                 //设置错误信息导出的列的值
                                tempObj.SparePartCodeStr, tempObj.SparePartNameStr, tempObj.ErrorMsg
                             };
                            return values;
                        });
                    }
                    errorList = null;
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }
    }

}
