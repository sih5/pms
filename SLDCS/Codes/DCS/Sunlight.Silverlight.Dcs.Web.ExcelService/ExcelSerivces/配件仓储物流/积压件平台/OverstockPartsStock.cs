﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        public bool 导出积压件信息(int[] ids, int EnterpriseId, string SparePartCode, string SparePartName, string StorageCompanyCode, string StorageCompanyName, string ProvinceName, string CityName, string CountyName, int? PartsSalesCategoryId, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName) {
            fileName = GetExportFilePath("积压件信息_" + ".xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.Append(@"SELECT 
		Project7.Code2,
		Project7.Name2,
		CASE WHEN Project7.Type = 2 THEN Project7.C1 ELSE ((Project7.C2 - (CASE WHEN Project7.C3 IS NULL THEN 0 ELSE Project7.C3 END)) - (CASE WHEN Project7.C4 IS NULL THEN 0 ELSE Project7.C4 END)) - (CASE WHEN Project7.C5 IS NULL THEN 0 ELSE Project7.C5 END) END AS C1,
		Project7.Code,
		Project7.Name,
		Project7.KYTYPE,
		Project7.PartsSalesCategoryName,
		Project7.dealprice,
		Project7.SalePrice,
		CASE WHEN Project7.Type = 2 THEN Project7.dealprice * Project7.C1 ELSE Project7.dealprice * (TO_NUMBER(((Project7.C2 - (CASE WHEN Project7.C3 IS NULL THEN 0 ELSE Project7.C3 END)) - (CASE WHEN Project7.C4 IS NULL THEN 0 ELSE Project7.C4 END)) - (CASE WHEN Project7.C5 IS NULL THEN 0 ELSE Project7.C5 END))) END AS C2,
		Project7.ProvinceName,
		Project7.CityName,
		Project7.CountyName,
		Project7.ContactPerson,
		Project7.ContactPhone,
		Project7.CreateTime
		FROM ( SELECT 
			Project6.Id,
			Project6.StorageCompanyId,
			Project6.BranchId,
			Project6.PartsSalesCategoryId,
			Project6.PartsSalesCategoryName,
			Project6.SparePartId,
			Project6.dealprice,
			Project6.ContactPerson,
			Project6.ContactPhone,
			Project6.CreateTime,
			Project6.SalePrice,
			Project6.Type,
            Project6.KYTYPE,
			Project6.Code,
			Project6.Name,
			Project6.ProvinceName,
			Project6.CityName,
			Project6.CountyName,
			Project6.Code1,
			Project6.Name1,
			Project6.Code2,
			Project6.Name2,
			Project6.C1,
			Project6.C2,
			Project6.C3,
			Project6.C4,
			(SELECT Sum(Extent9.DisabledStock) AS A1
FROM (SELECT * FROM WmsCongelationStockView) Extent9
WHERE ((Extent9.BranchId = Project6.BranchId) AND (Extent9.SparePartId = Project6.SparePartId)) AND (Extent9.PartsSalesCategoryId = Project6.PartsSalesCategoryId)) AS C5
			FROM ( SELECT 
				Project5.Id,
				Project5.StorageCompanyId,
				Project5.BranchId,
				Project5.PartsSalesCategoryId,
				Project5.PartsSalesCategoryName,
				Project5.SparePartId,
				Project5.dealprice,
				Project5.ContactPerson,
				Project5.ContactPhone,
				Project5.CreateTime,
				Project5.SalePrice,
				Project5.Type,
                Project5.KYTYPE,
				Project5.Code,
				Project5.Name,
				Project5.ProvinceName,
				Project5.CityName,
				Project5.CountyName,
				Project5.Code1,
				Project5.Name1,
				Project5.Code2,
				Project5.Name2,
				Project5.C1,
				Project5.C2,
				Project5.C3,
				(SELECT Sum(Extent8.CongelationStockQty) AS A1
FROM (SELECT * FROM WmsCongelationStockView) Extent8
WHERE ((Extent8.BranchId = Project5.BranchId) AND (Extent8.SparePartId = Project5.SparePartId)) AND (Extent8.PartsSalesCategoryId = Project5.PartsSalesCategoryId)) AS C4
				FROM ( SELECT 
					Project4.Id,
					Project4.StorageCompanyId,
					Project4.BranchId,
					Project4.PartsSalesCategoryId,
					Project4.PartsSalesCategoryName,
					Project4.SparePartId,
					Project4.dealprice,
					Project4.ContactPerson,
					Project4.ContactPhone,
					Project4.CreateTime,
					Project4.SalePrice,
					Project4.Type,
                    Project4.KYTYPE,
					Project4.Code,
					Project4.Name,
					Project4.ProvinceName,
					Project4.CityName,
					Project4.CountyName,
					Project4.Code1,
					Project4.Name1,
					Project4.Code2,
					Project4.Name2,
					Project4.C1,
					Project4.C2,
					(SELECT Sum(Extent7.LockedQuantity) AS A1
FROM PartsLockedStock Extent7
WHERE ((Extent7.StorageCompanyId = Project4.StorageCompanyId) AND (Extent7.BranchId = Project4.BranchId)) AND (Extent7.PartId = Project4.SparePartId)) AS C3
					FROM ( SELECT 
						Project3.Id,
						Project3.StorageCompanyId,
						Project3.BranchId,
						Project3.PartsSalesCategoryId,
						Project3.PartsSalesCategoryName,
						Project3.SparePartId,
						Project3.dealprice,
						Project3.ContactPerson,
						Project3.ContactPhone,
						Project3.CreateTime,
						Project3.SalePrice,
						Project3.Type,
						Project3.KYTYPE,
						Project3.Code,
						Project3.Name,
						Project3.ProvinceName,
						Project3.CityName,
						Project3.CountyName,
						Project3.Code1,
						Project3.Name1,
						Project3.Code2,
						Project3.Name2,
						Project3.C1,
						(SELECT Sum(Extent6.Quantity) AS A1
FROM PartsStock Extent6
WHERE ((Extent6.StorageCompanyId = Project3.StorageCompanyId) AND (Extent6.BranchId = Project3.BranchId)) AND (Extent6.PartId = Project3.SparePartId)) AS C2
						FROM ( SELECT 
							Project2.Id,
							Project2.StorageCompanyId,
							Project2.BranchId,
							Project2.PartsSalesCategoryId,
							Project2.PartsSalesCategoryName,
							Project2.SparePartId,
							Project2.dealprice,
							Project2.ContactPerson,
							Project2.ContactPhone,
							Project2.CreateTime,
							Project2.SalePrice,
							Project2.Type,
							Project2.KYTYPE,
							Project2.Code,
							Project2.Name,
							Project2.ProvinceName,
							Project2.CityName,
							Project2.CountyName,
							Project2.Code1,
							Project2.Name1,
							Project2.Code2,
							Project2.Name2,
							Project2.C1
							FROM ( SELECT 
								Project1.Id,
								Project1.StorageCompanyId,
								Project1.BranchId,
								Project1.PartsSalesCategoryId,
								Project1.PartsSalesCategoryName,
								Project1.SparePartId,
								Project1.dealprice,
								Project1.ContactPerson,
								Project1.ContactPhone,
								Project1.CreateTime,
								Project1.SalePrice,
								Project1.Id1,
								Project1.Type,
								Project1.KYTYPE,
								Project1.Code,
								Project1.Name,
								Project1.ProvinceName,
								Project1.CityName,
								Project1.CountyName,
								Project1.Status,
								Project1.Id2,
								Project1.Code1,
								Project1.Name1,
								Project1.Status1,
								Project1.Id3,
								Project1.Code2,
								Project1.Name2,
								Project1.Status2,
								(SELECT Sum(Extent5.Quantity) AS A1
FROM DealerPartsStock Extent5
WHERE ((Extent5.DealerId = Project1.StorageCompanyId) AND (Extent5.BranchId = Project1.BranchId)) AND (Extent5.SparePartId = Project1.SparePartId)) AS C1
								FROM ( SELECT 
									Extent1.Id,
									Extent1.StorageCompanyId,
									Extent1.BranchId,
									Extent1.PartsSalesCategoryId,
									Extent1.PartsSalesCategoryName,
									Extent1.SparePartId,
									Extent1.dealprice,
									Extent1.ContactPerson,
									Extent1.ContactPhone,
									Extent1.CreateTime,
									Extent1.SalePrice,
									Extent2.Id AS Id1,
                                    EXTENT2.TYPE,
									Extent5.value as KYTYPE,
									Extent2.Code,
									Extent2.Name,
									Extent2.ProvinceName,
									Extent2.CityName,
									Extent2.CountyName,
									Extent2.Status,
									Extent3.Id AS Id2,
									Extent3.Code AS Code1,
									Extent3.Name AS Name1,
									Extent3.Status AS Status1,
									Extent4.Id AS Id3,
									Extent4.Code AS Code2,
									Extent4.Name AS Name2,
									Extent4.Status AS Status2
									FROM OVERSTOCKPARTSINFORMATION Extent1
									inner JOIN Company Extent2
                                    on Extent1.StorageCompanyId = Extent2.Id
									inner JOIN Branch Extent3
                                    on Extent1.branchid = Extent3.Id
									inner JOIN SparePart Extent4
                                    on Extent1.SparePartId = Extent4.Id
                                    INNER JOIN  KEYVALUEITEM EXTENT5 
                                    ON EXTENT5.KEY =  EXTENT2.TYPE AND EXTENT5.NAME ='Company_Type'
									WHERE  (Extent1.BranchId = " + EnterpriseId + @")
								)  Project1
							)  Project2
							WHERE (((((Project2.Id2 = Project2.BranchId) AND (Project2.Status1 = 1)) AND (Project2.Id1 = Project2.StorageCompanyId)) AND (Project2.Status = 1)) AND (Project2.Id3 = Project2.SparePartId)) AND (Project2.Status2 = 1)
						)  Project3
					)  Project4
				)  Project5
			)  Project6
		)  Project7 where 1=1 ");
                    var dbParameters = new List<DbParameter>();
                    if(ids != null && ids.Length > 0) {
                        sql.Append(" and Project7.Id in (");
                        for(var i = 0; i < ids.Length; i++) {
                            if(ids.Length == i + 1) {
                                sql.Append(ids[i].ToString());
                            } else {
                                sql.Append(ids[i].ToString() + ",");
                            }
                        }
                        sql.Append(") ");
                    } else {


                        if(!String.IsNullOrEmpty(SparePartCode)) {
                            sql.Append(@" and Project7.Code2 like {0}Code2 ");
                            dbParameters.Add(db.CreateDbParameter("Code2", "%" + SparePartCode + "%"));
                        }
                        if(!String.IsNullOrEmpty(SparePartName)) {
                            sql.Append(@" and Project7.Name2 like {0}Name2 ");
                            dbParameters.Add(db.CreateDbParameter("Name2", "%" + SparePartName + "%"));
                        }
                        if(!String.IsNullOrEmpty(StorageCompanyCode)) {
                            sql.Append(@" and Project7.Code like {0}Code ");
                            dbParameters.Add(db.CreateDbParameter("Code", "%" + StorageCompanyCode + "%"));
                        }
                        if(!String.IsNullOrEmpty(StorageCompanyName)) {
                            sql.Append(@" and Project7.Name like {0}Name ");
                            dbParameters.Add(db.CreateDbParameter("Name", "%" + StorageCompanyName + "%"));
                        }
                        if(PartsSalesCategoryId.HasValue) {
                            sql.Append(@" and Project7.PartsSalesCategoryId={0}PartsSalesCategoryId ");
                            dbParameters.Add(db.CreateDbParameter("PartsSalesCategoryId", PartsSalesCategoryId.Value));
                        }
                        if(!String.IsNullOrEmpty(ProvinceName)) {
                            sql.Append(@" and Project7.ProvinceName like {0}ProvinceName ");
                            dbParameters.Add(db.CreateDbParameter("ProvinceName", "%" + ProvinceName + "%"));
                        }
                        if(!String.IsNullOrEmpty(CityName)) {
                            sql.Append(@" and Project7.CityName like {0}CityName ");
                            dbParameters.Add(db.CreateDbParameter("CityName", "%" + CityName + "%"));
                        }
                        if(!String.IsNullOrEmpty(CountyName)) {
                            sql.Append(@" and Project7.CountyName like {0}CountyName ");
                            dbParameters.Add(db.CreateDbParameter("CountyName", "%" + CountyName + "%"));
                        }
                        if(createTimeBegin.HasValue) {
                            sql.Append(@" and Project7.createTime >=To_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(@" and Project7.createTime <=To_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                    }
                    var sqlStr = sql.ToString();
                    if(sqlStr.IndexOf(" where 1=1  and ") > 0)
                        sqlStr = sqlStr.Replace(" where 1=1  and ", " where ");
                    else
                        sqlStr = sqlStr.Replace(" where 1=1 ", "");

                    var command = db.CreateDbCommand(string.Format(sqlStr, db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                  ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode,ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName,"可用库存",ErrorStrings.Export_Title_Company_Code,ErrorStrings.Export_Title_Company_Name,ErrorStrings.Export_Title_Company_Type,ErrorStrings.Export_Title_Partssalescategory_Name,"处理价格",ErrorStrings.Export_Title_DealerPartsStockQueryView_SalesPrice,"处理价金额",ErrorStrings.Export_Title_Agency_Province,ErrorStrings.Export_Title_Agency_City,"区县",ErrorStrings.Export_Title_Agency_LinkMan,"联系人电话",ErrorStrings.Export_Title_AccountPeriod_CreateTime
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }

                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }

        public bool 导出积压件信息查询(int[] ids, int EnterpriseId, string SparePartCode, string SparePartName, string StorageCompanyCode, string StorageCompanyName, string ProvinceName, string CityName, string CountyName, int? PartsSalesCategoryId, int? BranchId, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName) {
            fileName = GetExportFilePath("积压件信息查询_" + ".xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.Append(@"SELECT 
		Project7.Code2,
		Project7.Name2,
		CASE WHEN Project7.Type = 2 THEN Project7.C1 ELSE ((Project7.C2 - (CASE WHEN Project7.C3 IS NULL THEN 0 ELSE Project7.C3 END)) - (CASE WHEN Project7.C4 IS NULL THEN 0 ELSE Project7.C4 END)) - (CASE WHEN Project7.C5 IS NULL THEN 0 ELSE Project7.C5 END) END AS C1,
		Project7.Code,
		Project7.Name,
		Project7.KYTYPE,
		Project7.PartsSalesCategoryName,
		Project7.dealprice,
		Project7.SalePrice,
		CASE WHEN Project7.Type = 2 THEN Project7.dealprice * Project7.C1 ELSE Project7.dealprice * (TO_NUMBER(((Project7.C2 - (CASE WHEN Project7.C3 IS NULL THEN 0 ELSE Project7.C3 END)) - (CASE WHEN Project7.C4 IS NULL THEN 0 ELSE Project7.C4 END)) - (CASE WHEN Project7.C5 IS NULL THEN 0 ELSE Project7.C5 END))) END AS C2,
		Project7.ProvinceName,
		Project7.CityName,
		Project7.CountyName,
		Project7.ContactPerson,
		Project7.ContactPhone,
		Project7.CreateTime
		FROM ( SELECT 
			Project6.Id,
			Project6.StorageCompanyId,
			Project6.BranchId,
			Project6.PartsSalesCategoryId,
			Project6.PartsSalesCategoryName,
			Project6.SparePartId,
			Project6.dealprice,
			Project6.ContactPerson,
			Project6.ContactPhone,
			Project6.CreateTime,
			Project6.SalePrice,
			Project6.Type,
            Project6.KYTYPE,
			Project6.Code,
			Project6.Name,
			Project6.ProvinceName,
			Project6.CityName,
			Project6.CountyName,
			Project6.Code1,
			Project6.Name1,
			Project6.Code2,
			Project6.Name2,
			Project6.C1,
			Project6.C2,
			Project6.C3,
			Project6.C4,
			(SELECT Sum(Extent9.DisabledStock) AS A1
FROM (SELECT * FROM WmsCongelationStockView) Extent9
WHERE ((Extent9.BranchId = Project6.BranchId) AND (Extent9.SparePartId = Project6.SparePartId)) AND (Extent9.PartsSalesCategoryId = Project6.PartsSalesCategoryId)) AS C5
			FROM ( SELECT 
				Project5.Id,
				Project5.StorageCompanyId,
				Project5.BranchId,
				Project5.PartsSalesCategoryId,
				Project5.PartsSalesCategoryName,
				Project5.SparePartId,
				Project5.dealprice,
				Project5.ContactPerson,
				Project5.ContactPhone,
				Project5.CreateTime,
				Project5.SalePrice,
				Project5.Type,
                Project5.KYTYPE,
				Project5.Code,
				Project5.Name,
				Project5.ProvinceName,
				Project5.CityName,
				Project5.CountyName,
				Project5.Code1,
				Project5.Name1,
				Project5.Code2,
				Project5.Name2,
				Project5.C1,
				Project5.C2,
				Project5.C3,
				(SELECT Sum(Extent8.CongelationStockQty) AS A1
FROM (SELECT * FROM WmsCongelationStockView) Extent8
WHERE ((Extent8.BranchId = Project5.BranchId) AND (Extent8.SparePartId = Project5.SparePartId)) AND (Extent8.PartsSalesCategoryId = Project5.PartsSalesCategoryId)) AS C4
				FROM ( SELECT 
					Project4.Id,
					Project4.StorageCompanyId,
					Project4.BranchId,
					Project4.PartsSalesCategoryId,
					Project4.PartsSalesCategoryName,
					Project4.SparePartId,
					Project4.dealprice,
					Project4.ContactPerson,
					Project4.ContactPhone,
					Project4.CreateTime,
					Project4.SalePrice,
					Project4.Type,
                    Project4.KYTYPE,
					Project4.Code,
					Project4.Name,
					Project4.ProvinceName,
					Project4.CityName,
					Project4.CountyName,
					Project4.Code1,
					Project4.Name1,
					Project4.Code2,
					Project4.Name2,
					Project4.C1,
					Project4.C2,
					(SELECT Sum(Extent7.LockedQuantity) AS A1
FROM PartsLockedStock Extent7
WHERE ((Extent7.StorageCompanyId = Project4.StorageCompanyId) AND (Extent7.BranchId = Project4.BranchId)) AND (Extent7.PartId = Project4.SparePartId)) AS C3
					FROM ( SELECT 
						Project3.Id,
						Project3.StorageCompanyId,
						Project3.BranchId,
						Project3.PartsSalesCategoryId,
						Project3.PartsSalesCategoryName,
						Project3.SparePartId,
						Project3.dealprice,
						Project3.ContactPerson,
						Project3.ContactPhone,
						Project3.CreateTime,
						Project3.SalePrice,
						Project3.Type,
						Project3.KYTYPE,
						Project3.Code,
						Project3.Name,
						Project3.ProvinceName,
						Project3.CityName,
						Project3.CountyName,
						Project3.Code1,
						Project3.Name1,
						Project3.Code2,
						Project3.Name2,
						Project3.C1,
						(SELECT Sum(Extent6.Quantity) AS A1
FROM PartsStock Extent6
WHERE ((Extent6.StorageCompanyId = Project3.StorageCompanyId) AND (Extent6.BranchId = Project3.BranchId)) AND (Extent6.PartId = Project3.SparePartId)) AS C2
						FROM ( SELECT 
							Project2.Id,
							Project2.StorageCompanyId,
							Project2.BranchId,
							Project2.PartsSalesCategoryId,
							Project2.PartsSalesCategoryName,
							Project2.SparePartId,
							Project2.dealprice,
							Project2.ContactPerson,
							Project2.ContactPhone,
							Project2.CreateTime,
							Project2.SalePrice,
							Project2.Type,
							Project2.KYTYPE,
							Project2.Code,
							Project2.Name,
							Project2.ProvinceName,
							Project2.CityName,
							Project2.CountyName,
							Project2.Code1,
							Project2.Name1,
							Project2.Code2,
							Project2.Name2,
							Project2.C1
							FROM ( SELECT 
								Project1.Id,
								Project1.StorageCompanyId,
								Project1.BranchId,
								Project1.PartsSalesCategoryId,
								Project1.PartsSalesCategoryName,
								Project1.SparePartId,
								Project1.dealprice,
								Project1.ContactPerson,
								Project1.ContactPhone,
								Project1.CreateTime,
								Project1.SalePrice,
								Project1.Id1,
								Project1.Type,
								Project1.KYTYPE,
								Project1.Code,
								Project1.Name,
								Project1.ProvinceName,
								Project1.CityName,
								Project1.CountyName,
								Project1.Status,
								Project1.Id2,
								Project1.Code1,
								Project1.Name1,
								Project1.Status1,
								Project1.Id3,
								Project1.Code2,
								Project1.Name2,
								Project1.Status2,
								(SELECT Sum(Extent5.Quantity) AS A1
FROM DealerPartsStock Extent5
WHERE ((Extent5.DealerId = Project1.StorageCompanyId) AND (Extent5.BranchId = Project1.BranchId)) AND (Extent5.SparePartId = Project1.SparePartId)) AS C1
								FROM ( SELECT 
									Extent1.Id,
									Extent1.StorageCompanyId,
									Extent1.BranchId,
									Extent1.PartsSalesCategoryId,
									Extent1.PartsSalesCategoryName,
									Extent1.SparePartId,
									Extent1.dealprice,
									Extent1.ContactPerson,
									Extent1.ContactPhone,
									Extent1.CreateTime,
									Extent1.SalePrice,
									Extent2.Id AS Id1,
                                    EXTENT2.TYPE,
									Extent5.value as KYTYPE,
									Extent2.Code,
									Extent2.Name,
									Extent2.ProvinceName,
									Extent2.CityName,
									Extent2.CountyName,
									Extent2.Status,
									Extent3.Id AS Id2,
									Extent3.Code AS Code1,
									Extent3.Name AS Name1,
									Extent3.Status AS Status1,
									Extent4.Id AS Id3,
									Extent4.Code AS Code2,
									Extent4.Name AS Name2,
									Extent4.Status AS Status2
									FROM OVERSTOCKPARTSINFORMATION Extent1
									inner JOIN Company Extent2
                                    on Extent1.StorageCompanyId = Extent2.Id
									inner JOIN Branch Extent3
                                    on Extent1.branchid = Extent3.Id
									inner JOIN SparePart Extent4
                                    on Extent1.SparePartId = Extent4.Id
                                    INNER JOIN  KEYVALUEITEM EXTENT5 
                                    ON EXTENT5.KEY =  EXTENT2.TYPE AND EXTENT5.NAME ='Company_Type'
								)  Project1
							)  Project2
							WHERE (((((Project2.Id2 = Project2.BranchId) AND (Project2.Status1 = 1)) AND (Project2.Id1 = Project2.StorageCompanyId)) AND (Project2.Status = 1)) AND (Project2.Id3 = Project2.SparePartId)) AND (Project2.Status2 = 1)
						)  Project3
					)  Project4
				)  Project5
			)  Project6
		)  Project7 where 1=1 ");
                    var dbParameters = new List<DbParameter>();
                    if(ids != null && ids.Length > 0) {
                        sql.Append(" and Project7.Id in (");
                        for(var i = 0; i < ids.Length; i++) {
                            if(ids.Length == i + 1) {
                                sql.Append(ids[i].ToString());
                            } else {
                                sql.Append(ids[i].ToString() + ",");
                            }
                        }
                        sql.Append(") ");
                    } else {


                        if(!String.IsNullOrEmpty(SparePartCode)) {
                            sql.Append(@" and Project7.Code2 like {0}Code2 ");
                            dbParameters.Add(db.CreateDbParameter("Code2", "%" + SparePartCode + "%"));
                        }
                        if(!String.IsNullOrEmpty(SparePartName)) {
                            sql.Append(@" and Project7.Name2 like {0}Name2 ");
                            dbParameters.Add(db.CreateDbParameter("Name2", "%" + SparePartName + "%"));
                        }
                        if(!String.IsNullOrEmpty(StorageCompanyCode)) {
                            sql.Append(@" and Project7.Code like {0}Code ");
                            dbParameters.Add(db.CreateDbParameter("Code", "%" + StorageCompanyCode + "%"));
                        }
                        if(!String.IsNullOrEmpty(StorageCompanyName)) {
                            sql.Append(@" and Project7.Name like {0}Name ");
                            dbParameters.Add(db.CreateDbParameter("Name", "%" + StorageCompanyName + "%"));
                        }
                        if(PartsSalesCategoryId.HasValue) {
                            sql.Append(@" and Project7.PartsSalesCategoryId={0}PartsSalesCategoryId ");
                            dbParameters.Add(db.CreateDbParameter("PartsSalesCategoryId", PartsSalesCategoryId.Value));
                        }
                        if(BranchId.HasValue) {
                            sql.Append(@" and Project7.BranchId={0}BranchId ");
                            dbParameters.Add(db.CreateDbParameter("BranchId", BranchId.Value));
                        }
                        if(!String.IsNullOrEmpty(ProvinceName)) {
                            sql.Append(@" and Project7.ProvinceName like {0}ProvinceName ");
                            dbParameters.Add(db.CreateDbParameter("ProvinceName", "%" + ProvinceName + "%"));
                        }
                        if(!String.IsNullOrEmpty(CityName)) {
                            sql.Append(@" and Project7.CityName like {0}CityName ");
                            dbParameters.Add(db.CreateDbParameter("CityName", "%" + CityName + "%"));
                        }
                        if(!String.IsNullOrEmpty(CountyName)) {
                            sql.Append(@" and Project7.CountyName like {0}CountyName ");
                            dbParameters.Add(db.CreateDbParameter("CountyName", "%" + CountyName + "%"));
                        }
                        if(createTimeBegin.HasValue) {
                            sql.Append(@" and Project7.createTime >=To_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(@" and Project7.createTime <=To_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                    }
                    var sqlStr = sql.ToString();
                    if(sqlStr.IndexOf(" where 1=1  and ") > 0)
                        sqlStr = sqlStr.Replace(" where 1=1  and ", " where ");
                    else
                        sqlStr = sqlStr.Replace(" where 1=1 ", "");

                    var command = db.CreateDbCommand(string.Format(sqlStr, db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                  ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode,ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName,"可用库存",ErrorStrings.Export_Title_Company_Code,ErrorStrings.Export_Title_Company_Name,ErrorStrings.Export_Title_Company_Type,ErrorStrings.Export_Title_Partssalescategory_Name,"处理价格",ErrorStrings.Export_Title_DealerPartsStockQueryView_SalesPrice,"处理价金额",ErrorStrings.Export_Title_Agency_Province,ErrorStrings.Export_Title_Agency_City,"区县",ErrorStrings.Export_Title_Agency_LinkMan,"联系人电话",ErrorStrings.Export_Title_AccountPeriod_CreateTime
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }

                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }
    }
}
