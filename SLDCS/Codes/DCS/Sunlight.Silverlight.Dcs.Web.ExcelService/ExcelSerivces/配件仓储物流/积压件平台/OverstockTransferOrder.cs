﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Globalization;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        
        public bool ExportOverstockTransferOrder(int[] ids,string code, int? status, string transferInCorpName, string transferInWarehouseCode, string transferOutCorpName, string transferOutWarehouseName, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName)
        {
            fileName = GetExportFilePath("积压件调拨单.xlsx");

            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);
                var userInfo = Utils.GetCurrentUserInfo();
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.Append(string.Format(@"select (select value from keyvalueitem where name = 'OverstockPartsApp_Status' and key= a.status) as status,a.code,(select value from keyvalueitem where name = 'Company_Type' and key= a.CustomerType) as CustomerType,a.TransferInCorpCode,a.TransferInCorpName,a.TransferInWarehouseCode,a.TransferInWarehouseName,
a.TransferOutCorpCode,a.TransferOutCorpName,a.TransferOutWarehouseCode,a.TransferOutWarehouseName,a.TotalAmount,a.ReceivingAddress,a.ShippingMethod,a.PromisedDeliveryTime,
a.CreatorName,a.CreateTime,a.ModifierName,a.ModifyTime,a.ApproverName,a.ApproveTime,a.AbandonerName,a.AbandonTime,
b.sparepartcode,b.sparepartname,b.transferqty,b.enoughqty,b.RetailPrice,b.DiscountRate,b.DiscountedPrice from OverstockTransferOrder a inner join OverstockTransferOrderDetail b on a.id = b.OverstockPartsTransferOrderId
where (a.TransferInCorpId = {0} or (a.TransferOutCorpId = {1} and a.status in (2,3))) 
", userInfo.EnterpriseId, userInfo.EnterpriseId));

                    var dbParameters = new List<DbParameter>();
                    if (ids != null && ids.Length > 0) {
                        sql.Append(" and a.id in (");
                        for (var i = 0; i < ids.Length; i++) {
                            if (ids.Length == i + 1) {
                                sql.Append("{0}" + ids[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter(ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            } else {
                                sql.Append("{0}" + ids[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter(ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                        }
                        sql.Append(")");
                    } else {
                        if (!string.IsNullOrEmpty(code)) {
                            sql.Append("and a.Code like {0}code ");
                            dbParameters.Add(db.CreateDbParameter("code", "%" + code + "%"));
                        }
                        if (status.HasValue) {
                            sql.Append("and a.status = {0}status ");
                            dbParameters.Add(db.CreateDbParameter("status", status.Value));
                        }
                        if (!string.IsNullOrEmpty(transferInCorpName)) {
                            sql.Append("and a.transferInCorpName like {0}transferInCorpName ");
                            dbParameters.Add(db.CreateDbParameter("transferInCorpName", "%" + transferInCorpName + "%"));
                        }
                        if (!string.IsNullOrEmpty(transferInWarehouseCode)) {
                            sql.Append("and a.transferInWarehouseCode like {0}transferInWarehouseCode ");
                            dbParameters.Add(db.CreateDbParameter("transferInWarehouseCode", "%" + transferInWarehouseCode + "%"));
                        }
                        if (!string.IsNullOrEmpty(transferOutCorpName)) {
                            sql.Append("and a.transferOutCorpName like {0}transferOutCorpName ");
                            dbParameters.Add(db.CreateDbParameter("transferOutCorpName", "%" + transferOutCorpName + "%"));
                        }
                        if (!string.IsNullOrEmpty(transferOutWarehouseName)) {
                            sql.Append("and a.transferOutWarehouseName like {0}transferOutWarehouseName ");
                            dbParameters.Add(db.CreateDbParameter("transferOutWarehouseName", "%" + transferOutWarehouseName + "%"));
                        }
                        if (createTimeBegin.HasValue) {
                            sql.Append(@" and a.createTime >=To_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if (createTimeEnd.HasValue) {
                            sql.Append(@" and a.createTime <=To_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                    }

                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using (var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if (index == 0) {
                                return new object[] {
                                   ErrorStrings.Export_Title_AccountPeriod_Status,ErrorStrings.Export_OverstockPartsApp_Code,ErrorStrings.Export_Validation_Company_Type,ErrorStrings.Export_Title_OverstockPartsAdjustBill_OriginalStorageCompanyCode,ErrorStrings.Export_Title_OverstockPartsAdjustBill_OriginalStorageCompanyName,ErrorStrings.Export_Title_OverstockPartsAdjustBill_OriginalWarehouseCode,
                                  ErrorStrings.Export_Title_OverstockPartsAdjustBill_OriginalWarehouseName,ErrorStrings.Export_Title_OverstockPartsAdjustBill_DestStorageCompanyCode,ErrorStrings.Export_Title_OverstockPartsAdjustBill_DestStorageCompanyName,ErrorStrings.Export_Title_OverstockPartsAdjustBill_DestStorageWarehouseCode,ErrorStrings.Export_Title_OverstockPartsAdjustBill_DestStorageWarehouseName,
                                  ErrorStrings.Export_Title_InternalAllocationBill_TotalAmount,ErrorStrings.Export_Title_PartsPurchaseOrder_ReceiveAddress,ErrorStrings.Export_Title_PartsPurchaseOrder_ShippingMethod,ErrorStrings.Export_Title_PartsPurchaseOrder_RequestedDeliveryTime,ErrorStrings.Export_Title_AccountPeriod_CreatorName,ErrorStrings.Export_Title_AccountPeriod_CreateTime,
                                  ErrorStrings.Export_Title_AccountPeriod_ModifierName,ErrorStrings.Export_Title_AccountPeriod_ModifyTime,ErrorStrings.Export_Title_PackingPropertyApp_ApproverName,ErrorStrings.Export_Title_PackingPropertyApp_ApproveTime,ErrorStrings.Export_Title_PartsBranch_AbandonerName,ErrorStrings.Export_Title_PartsBranch_AbandonerTime,
                                   ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode,ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName,ErrorStrings.Export_Title_OverstockPartsAdjustDetail_Quantity,ErrorStrings.Export_Title_PartsSalesOrder_CurrentFulfilledQuantity,ErrorStrings.Export_Title_PartSstock_RetailGuidePrice,ErrorStrings.Export_Title_OverstockPartsAdjustDetail_DiscountRate,ErrorStrings.Export_Title_OverstockPartsAdjustDetail_DiscountPrice,
                                };
                            }
                            if (reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if (num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();

                }
                return true;
            } catch(Exception) {
                return false;
            }
        }

    }
}
