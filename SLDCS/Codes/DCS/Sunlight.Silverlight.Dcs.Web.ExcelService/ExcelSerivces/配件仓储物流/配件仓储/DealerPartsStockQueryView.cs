﻿
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {

        public bool ExportDealerPartsStockQueryView(int[] partsSalesCategoryIds, string[] dealerCodes, string[] partCodes, int branchId, int? partsSalesCategoryId, string dealerCode, string partCode, bool? isStoreMoreThanZero, out string fileName,out string errmesage) {
            fileName = GetExportFilePath("配件库存明细_" + ".xlsx");
            try {
                errmesage = string.Empty;
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.Append(@"select 
                                    PartsSalesCategoryName,--品牌 
                                    DealerCode,--服务站编码 
                                    DealerName,--服务站名称  
                                    CustomerType,--客户属性  
                                    ProvinceName,--省份 
                                    PartCode,--配件图号 
                                    PartName,--配件名称 
                                    Quantity,--库存 
                                    SalesPrice,--批发价 
                                    SalesPriceAmount,--批发价金额 
                                    StockMaximum--库存高限    
                                    From DealerPartsStockQueryView a  where a.BranchId=" + branchId);
                    var dbParameters = new List<DbParameter>();
                    if(partsSalesCategoryIds != null && partsSalesCategoryIds.Length > 0) {
                        sql.Append(" and a.PartsSalesCategoryId in (");
                        for(var i = 0; i < partsSalesCategoryIds.Length; i++) {
                            if(partsSalesCategoryIds.Length == i + 1) {
                                sql.Append(partsSalesCategoryIds[i].ToString());
                            } else {
                                sql.Append(partsSalesCategoryIds[i].ToString() + ",");
                            }
                        }
                        sql.Append(") and a.DealerCode in (");
                        for(var i = 0; i < dealerCodes.Length; i++) {
                            if(dealerCodes.Length == i + 1) {
                                sql.Append("'" + dealerCodes[i].ToString() + "'");
                            } else {
                                sql.Append("'" + dealerCodes[i].ToString() + "',");
                            }
                        }

                        sql.Append(") and a.PartCode in (");
                        for(var i = 0; i < partCodes.Length; i++) {
                            if(partCodes.Length == i + 1) {
                                sql.Append("'" + partCodes[i] + "'");
                            } else {
                                sql.Append("'" + partCodes[i] + "',");
                            }
                        }
                        sql.Append(") ");
                    } else {

                        if(partsSalesCategoryId.HasValue) {
                            sql.Append(@" and a.partsSalesCategoryId={0}partsSalesCategoryId ");
                            dbParameters.Add(db.CreateDbParameter("partsSalesCategoryId", partsSalesCategoryId.Value));
                        }

                        if(!String.IsNullOrEmpty(dealerCode)) {
                            sql.Append(@" and a.dealerCode like {0}dealerCode ");
                            dbParameters.Add(db.CreateDbParameter("dealerCode", "%" + dealerCode + "%"));
                        }
                        if(!String.IsNullOrEmpty(partCode)) {
                            sql.Append(@" and a.Partcode like {0}partCode ");
                            dbParameters.Add(db.CreateDbParameter("partCode", "%" + partCode + "%"));
                        }
                        if(isStoreMoreThanZero.HasValue) {
                            sql.Append(@" and a.isStoreMoreThanZero={0}isStoreMoreThanZero ");
                            dbParameters.Add(db.CreateDbParameter("isStoreMoreThanZero", isStoreMoreThanZero.Value ? 1 : 0));
                        }
                    }
                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                  ErrorStrings.Export_Title_Partssalescategory_Name,ErrorStrings.Export_Title_DealerPartsStockQueryView_DealerCode,ErrorStrings.Export_Title_Dealer_Name,ErrorStrings.Export_Validation_Company_Type,ErrorStrings.Export_Validation_Company_ProvinceName,ErrorStrings.Export_Title_PartsBranch_Code,ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName,
                                  ErrorStrings.Export_Title_DealerPartsStockQueryView_Quantity,ErrorStrings.Export_Title_DealerPartsStockQueryView_SalesPrice,ErrorStrings.Export_Title_DealerPartsStockQueryView_SalesPriceAmount,ErrorStrings.Export_Title_PartsBranch_StockMaximum
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }

                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception ex) {
                errmesage = ex.Message;
                return false;
            }
        }

    }
}
