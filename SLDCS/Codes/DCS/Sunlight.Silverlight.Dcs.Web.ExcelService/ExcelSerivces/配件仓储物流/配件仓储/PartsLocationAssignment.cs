﻿
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {

        public bool ExportPartsLocationAssignment(int id, int areaKind, out string fileName) {
            fileName = GetExportFilePath("配件库位分配信息_" + ".xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.Append(@"select d.code as WarehouseCode,
                               d.name as WarehouseName,
                               e.code as TopAreaCode,
                               (select value
                                  from keyvalueitem
                                 where name = 'Area_Category'
                                   and key = f.category) as areaCategory,
                               a.code as WarehouseAreaCode,
                               c.code as SparepartCode,
                               c.name as SparepartName
                                  from warehousearea a
                                 inner join warehouse d
                                    on d.id = a.warehouseid
                                  left join partsstock b
                                    on a.id = b.warehouseareaid
                                  left join sparepart c
                                    on c.id = b.partid
                                 inner join warehousearea e
                                    on e.id = a.toplevelwarehouseareaid
                                  left join WarehouseAreaCategory f
                                    on f.id = e.areacategoryid
                                 where a.status <> 99
                                   and a.areakind = 3
                                ") ;

                    if (areaKind == (int)DcsAreaKind.仓库)
                    {
                        sql.Append(" and  a.warehouseid = " + id);
                    }
                    else { 
                        sql.Append(" and  a.id = " + id);
                    }
                    sql.Append("  order by a.id");
                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                  ErrorStrings.Export_Title_Company_WarehouseCode,ErrorStrings.Export_Title_Company_WarehouseName,ErrorStrings.Export_Title_WarehouseArea_CodeNew,ErrorStrings.Export_Title_WarehouseArea_Category,ErrorStrings.Export_Title_WarehouseArea_WarehouseAreaCode,ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode,ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }

                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }

    }
}
