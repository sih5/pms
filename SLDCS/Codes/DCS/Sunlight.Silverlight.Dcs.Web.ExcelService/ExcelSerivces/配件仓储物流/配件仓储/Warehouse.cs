﻿
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {

        public bool ExportWarehouseQueryView(int[] warehouseIds, int StorageCompanyId, int branchId, string Code, string Name, int? StorageCenter, int? Status, out string fileName) {
            fileName = GetExportFilePath("仓库信息_" + ".xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.Append(@"SELECT 
                                 EXTENT1.CODE,
                                 EXTENT1.NAME,
						         EXTENT2.NAME AS NAME1,
                                 EXTENT3.VALUE AS STORAGECENTER,
						         CASE EXTENT1.ISCENTRALIZEDPURCHASE WHEN 1 THEN '是'  WHEN 0 THEN '否' ELSE '' END AS ISCENTRALIZEDPURCHASE,
                                 CASE EXTENT1.IsAutoApp WHEN 1 THEN '是'  WHEN 0 THEN '否' ELSE '' END AS IsAutoApp,
                                 EXTENT4.VALUE AS TYPE,
                                 EXTENT1.ADDRESS,
                                 EXTENT1.PHONENUMBER,
                                 EXTENT1.CONTACT,
                                 EXTENT1.FAX,
                                 EXTENT1.EMAIL,
                                 EXTENT1.REMARK,
                                 CASE EXTENT1.WMSINTERFACE WHEN 1 THEN '是'  WHEN 0 THEN '否' ELSE '' END AS WMSINTERFACE,
                                 CASE EXTENT1.PWMSINTERFACE WHEN 1 THEN '是'  WHEN 0 THEN '否' ELSE '' END AS PWMSINTERFACE,
                                 EXTENT5.VALUE AS STATUS,
                                 EXTENT1.CREATORNAME,
                                 EXTENT1.CREATETIME,
                                 EXTENT1.MODIFIERNAME,
                                 EXTENT1.MODIFYTIME,
                                 CASE EXTENT1.IfSync WHEN 1 THEN '是'  WHEN 0 THEN '否' ELSE '' END AS IfSync,
                                CASE EXTENT1.IsSales WHEN 1 THEN '是'  WHEN 0 THEN '否' ELSE '' END AS IsSales,
                                CASE EXTENT1.IsPurchase WHEN 1 THEN '是'  WHEN 0 THEN '否' ELSE '' END AS IsPurchase
                                 FROM  WAREHOUSE EXTENT1
                                 LEFT JOIN BRANCH EXTENT2 
								 ON EXTENT1.BRANCHID = EXTENT2.ID 
								 LEFT JOIN KEYVALUEITEM EXTENT3
								 ON EXTENT3.KEY = EXTENT1.STORAGECENTER AND EXTENT3.NAME ='Storage_Center'
								 LEFT JOIN KEYVALUEITEM EXTENT4
								 ON EXTENT4.KEY = EXTENT1.TYPE AND EXTENT4.NAME ='Warehouse_Type'
								 LEFT JOIN KEYVALUEITEM EXTENT5
								 ON EXTENT5.KEY = EXTENT1.STATUS AND EXTENT5.NAME ='BaseData_Status'
								 WHERE EXTENT1.BRANCHID=" + branchId + " AND EXTENT1.STORAGECOMPANYID=" + StorageCompanyId);
                    var dbParameters = new List<DbParameter>();
                    if(warehouseIds != null && warehouseIds.Length > 0) {
                        sql.Append(" and Extent1.Id in (");
                        for(var i = 0; i < warehouseIds.Length; i++) {
                            if(warehouseIds.Length == i + 1) {
                                sql.Append(warehouseIds[i].ToString());
                            } else {
                                sql.Append(warehouseIds[i].ToString() + ",");
                            }
                        }
                        sql.Append(") ");
                    } else {


                        if(!String.IsNullOrEmpty(Code)) {
                            sql.Append(@" and Extent1.Code like {0}Code ");
                            dbParameters.Add(db.CreateDbParameter("Code", "%" + Code + "%"));
                        }
                        if(!String.IsNullOrEmpty(Name)) {
                            sql.Append(@" and Extent1.Name like {0}Name ");
                            dbParameters.Add(db.CreateDbParameter("Name", "%" + Name + "%"));
                        }
                        if(StorageCenter.HasValue) {
                            sql.Append(@" and Extent1.StorageCenter={0}StorageCenter ");
                            dbParameters.Add(db.CreateDbParameter("StorageCenter", StorageCenter.Value));
                        }
                        if(Status.HasValue) {
                            sql.Append(@" and Extent1.Status={0}Status ");
                            dbParameters.Add(db.CreateDbParameter("Status", Status.Value));
                        }
                    }
                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                  ErrorStrings.Export_Title_Company_WarehouseCode,ErrorStrings.Export_Title_Company_WarehouseName,ErrorStrings.Export_Title_Branch_BranchName,ErrorStrings.Export_Title_Warehouse_StorageCompany,ErrorStrings.Export_Title_Warehouse_IsQualityWarehouse,"是否自动审单",ErrorStrings.Export_Title_Warehouse_Type,ErrorStrings.Export_Title_Warehouse_Address,ErrorStrings.Export_Title_Agency_LinkManMobile,ErrorStrings.Export_Title_Agency_LinkMan,ErrorStrings.Export_Title_Agency_Fax,
                                  ErrorStrings.Export_Title_Warehouse_Email,ErrorStrings.Export_Title_PartsBranch_Remark,ErrorStrings.Export_Title_Warehouse_WMSInterface,ErrorStrings.Export_Title_Warehouse_PWMSInterface,ErrorStrings.Export_Title_AccountPeriod_Status,ErrorStrings.Export_Title_AccountPeriod_CreatorName,ErrorStrings.Export_Title_AccountPeriod_CreateTime,ErrorStrings.Export_Title_AccountPeriod_ModifierName,ErrorStrings.Export_Title_AccountPeriod_ModifyTime,ErrorStrings.Export_Title_Warehouse_IfSync,"是否参与智能订货(销售部分)","是否参与智能订货(采购部分)"
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }

                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }

    }
}
