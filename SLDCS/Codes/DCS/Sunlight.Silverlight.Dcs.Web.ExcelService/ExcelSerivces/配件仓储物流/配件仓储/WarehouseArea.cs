﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        //导入库区
        public bool ImportWarehouseArea(int parentId, string fileName, out int excelImportNum, out List<WarehouseAreaExtend> rightData, out List<WarehouseAreaExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorDataFileName = "";
            errorMessage = "";
            var errorList = new List<WarehouseAreaExtend>();
            var allList = new List<WarehouseAreaExtend>();
            var rightList = new List<WarehouseAreaExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);
                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("WarehouseArea", out notNullableFields, out fieldLenght);


                List<string> notNullableFieldsWarehouseAreaCategory;
                Dictionary<string, int> fieldLenghtWarehouseAreaCategory;
                db.GetTableSchema("WarehouseAreaCategory", out notNullableFieldsWarehouseAreaCategory, out fieldLenghtWarehouseAreaCategory);


                List<object> excelColumns;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    //指定文件中的列对应的名称
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_WarehouseArea_CodeNew, "Code");//必填
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_WarehouseAreaCategory_Category, "Category");//必填
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Remark, "Remark");

                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;
                    //获取对应枚举
                    var keyValuePairs = new[] {
                        new KeyValuePair<string, string>("BaseDataStatus", "BaseData_Status"),
                        new KeyValuePair<string, string>("AreaKind","Area_Kind"),
                        new KeyValuePair<string, string>("AreaCategory","Area_Category")
                    };
                    tempExcelOperator.ImportHelper.LoadEnumFromDb(null,null, keyValuePairs);

                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var tempImportObj = new WarehouseAreaExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        //根据导入的字段给对应的实体属性赋值 
                        tempImportObj.CodeStr = newRow["Code"];
                        tempImportObj.CategoryStr = newRow["Category"];
                        tempImportObj.RemarkStr = newRow["Remark"];

                        var tempErrorMessage = new List<string>();
                        //导入的数据基本检查
                        //库区编号检查
                        var fieldIndex = notNullableFields.IndexOf("Code".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.CodeStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_WarehouseAreaCategory_CodeIsNull);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.CodeStr) > fieldLenght["Code".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_WarehouseAreaCategory_CodeIsLong);
                        }
                        //库区用途检查
                        fieldIndex = notNullableFieldsWarehouseAreaCategory.IndexOf("Category".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.CategoryStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_WarehouseAreaCategory_CategoryIsNull);
                        } else {
                            var tempEnumValue = tempExcelOperator.ImportHelper.GetEnumValue("AreaCategory", tempImportObj.CategoryStr);
                            if (!tempEnumValue.HasValue)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_WarehouseAreaCategory_CategoryValueError);
                            else
                            {
                                tempImportObj.AreaCategoryId = tempEnumValue;
                            }
                                
                        }

                        if(tempErrorMessage.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });
                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    //剩下的数据进行业务检查
                    //文件内数据重复
                    var groups = tempRightList.GroupBy(r => new {
                        r.CodeStr
                    }).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        groupItem.ErrorMsg = ErrorStrings.Export_Validation_WarehouseAreaCategory_Validation1;
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    //库区编号是否存在或存在冲突
                    var codesCheck = tempRightList.Select(r => r.CodeStr).Distinct().ToArray();
                    var dbCodes = new List<WarehouseAreaExtend>();
                    Func<string[], bool> getDbCodes = values => {
                        dbCodes.Add(new WarehouseAreaExtend
                        {
                            Code = values[0],
                            ParentId = int.Parse(values[1]),
                            Status = int.Parse(values[2]),
                            WarehouseId=int.Parse(values[3]),
                        });
                        return false;
                    };
    //                var dbwarehouseArea = ObjectContext.WarehouseAreas.Where(r => r.WarehouseId == warehouseArea.WarehouseId && r.Code.ToLower() == warehouseArea.Code.ToLower()).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
    //                if(dbwarehouseArea != null && dbwarehouseArea.Status == (int)DcsBaseDataStatus.有效)
    //                    throw new ValidationException(string.Format(ErrorStrings.WarehouseArea_Validation1, warehouseArea.Code));


                    db.QueryDataWithInOperator("select Code,parentid,status,WarehouseId from WarehouseArea ", "Code", true, codesCheck, getDbCodes);
                    foreach(var tempRight in tempRightList) {
                        //var code = dbCodes.FirstOrDefault(v => v.Code == tempRight.CodeStr);
                        foreach (var code in dbCodes)
                        {
                            if(code.Code == tempRight.CodeStr && code.ParentId == parentId && code.WarehouseId == tempRight.WarehouseId)
                            {
                                if (code.Status == 1)
                                {
                                    tempRight.ErrorMsg = ErrorStrings.Export_Validation_WarehouseAreaCategory_Validation2;
                                }
                                else
                                {
                                    tempRight.StatusStr = "1";
                                    tempRight.Status = 1;
                                }
                            }
                        }
                    }

                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    //查询上级库区库位
                    //var parentIdCheck = tempRightList.Select(r => r.ParentIdStr).Distinct().ToArray();
                    var dbParentId = new List<WarehouseAreaExtend>();
                    Func<string[], bool> getDbParentId = values => {
                        var p = new WarehouseAreaExtend {
                            Id = Convert.ToInt32(values[0]),
                            WarehouseId = Convert.ToInt32(values[1]),
                            AreaKind = Convert.ToInt32(values[2]),
                            TopLevelWarehouseAreaId = values[3] == "" ? -1 : Convert.ToInt32(values[3])
                        };
                        dbParentId.Add(p);
                        return false;
                    };
                    var sql = string.Format("select Id,WarehouseId, AreaKind,TopLevelWarehouseAreaId from WarehouseArea Where status=1 and id = {0} ", parentId);
                    db.QueryDataWithInOperator(sql, "1", true, new[] { "1" }, getDbParentId);
                    foreach(var tempRight in tempRightList) {
                        var wid = dbParentId.FirstOrDefault(v => v.AreaKind == tempExcelOperator.ImportHelper.GetEnumValue("AreaKind", ErrorStrings.Export_Title_WarehouseAreaCategory_AreaKind));
                        if(wid != null) {
                            tempRight.WarehouseId = wid.WarehouseId;
                            //tempRight.ParentIdStr = pid.Id.ToString();
                            tempRight.TopLevelWarehouseAreaId = null;
                        }
                        var pId = dbParentId.FirstOrDefault(v => v.AreaKind == tempExcelOperator.ImportHelper.GetEnumValue("AreaKind", ErrorStrings.Export_Title_WarehouseAreaCategory_AreaKindCode));
                        if(pId != null) {
                            tempRight.ErrorMsg = ErrorStrings.Export_Validation_WarehouseAreaCategory_Validation3;
                            continue;
                        }
                        var pid = dbParentId.FirstOrDefault(v => v.AreaKind == tempExcelOperator.ImportHelper.GetEnumValue("AreaKind", ErrorStrings.Export_Title_WarehouseAreaCategory_Area));
                        if(pid != null) {
                            tempRight.WarehouseId = pid.WarehouseId;
                            //tempRight.ParentIdStr = pid.Id.ToString();
                            tempRight.TopLevelWarehouseAreaId = pid.TopLevelWarehouseAreaId == -1 ? pid.Id : pid.TopLevelWarehouseAreaId;
                        }

                    }

                    //获取所有不合格数据
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    //获取所有合格数据
                    rightList = allList.Except(errorList).ToList();
                    //将合格数据的值填上
                    foreach(var tempRight in rightList) {
                        tempRight.Code = tempRight.CodeStr;
                        tempRight.AreaKind = tempExcelOperator.ImportHelper.GetEnumValue("AreaKind", ErrorStrings.Export_Title_WarehouseAreaCategory_Area) ?? 0;
                        tempRight.Remark = tempRight.RemarkStr;
                        tempRight.ParentId = parentId;
                        //tempRight.Status = tempExcelOperator.ImportHelper.GetEnumValue("BaseDataStatus", ErrorStrings.Export_Validation_PartsBranch_Used) ?? 0;
                    }
                }
                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempObj = list[index - 1];
                            //设置错误信息导出的列的值
                            var values = new object[] {
                                tempObj.CodeStr, tempObj.CategoryStr, tempObj.RemarkStr, tempObj.ErrorMsg
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }
                //导入所有合格数据
                if(!rightList.Any())
                    return true;
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    //开启事务，新增更新配件在一个事务内
                    var ts = conn.BeginTransaction();
                    try {
                        //新增库区库位
                        if(rightList.Any()) {
                            //填充正确的库区用途
                            List<int> dbCategory1 = new List<int>();
                            Func<string[], bool> getDbCategroy1 = values =>
                            {
                                dbCategory1.Add(int.Parse(values[0]));
                                return false;
                            };
                            db.QueryDataWithInOperator("select Id,Category from warehouseareacategory ", "Category", false, new[] { "1" }, getDbCategroy1);
                            List<int> dbCategory2 = new List<int>();
                            Func<string[], bool> getDbCategroy2 = values =>
                            {
                                dbCategory2.Add(int.Parse(values[0]));
                                return false;
                            };
                            db.QueryDataWithInOperator("select Id,Category from warehouseareacategory ", "Category", false, new[] { "2" }, getDbCategroy2);
                            List<int> dbCategory3 = new List<int>();
                            Func<string[], bool> getDbCategroy3 = values =>
                            {
                                dbCategory3.Add(int.Parse(values[0]));
                                return false;
                            };
                            db.QueryDataWithInOperator("select Id,Category from warehouseareacategory ", "Category", false, new[] { "3" }, getDbCategroy3);
                            var userInfo = Utils.GetCurrentUserInfo();
                            //往数据库增加库区库位信息
                            foreach(var item in rightList) {
                                if (item.AreaCategoryId == 1)
                                {
                                    item.AreaCategoryId = dbCategory1.FirstOrDefault();
                                }
                                else if (item.AreaCategoryId == 2)
                                {
                                    item.AreaCategoryId = dbCategory2.FirstOrDefault();
                                }
                                else
                                {
                                    item.AreaCategoryId = dbCategory3.FirstOrDefault();
                                }
                                if (item.Status == 1)
                                {
                                    //获取对应的更新sql，将已作废的库位变为生效，并将最新信息更新至原有库位中记录
                                    var sqlUpdate = db.GetUpdateSql("WarehouseArea", new string[] { "Status", "AreaCategoryId", "Remark", "CreatorId", "CreatorName", "CreateTime" }, new string[] { "Code", "ParentId" });
                                    //添加sql参数
                                    var command = db.CreateDbCommand(sqlUpdate, conn, ts);
                                    command.Parameters.Add(db.CreateDbParameter("ParentId", item.ParentId));
                                    command.Parameters.Add(db.CreateDbParameter("Code", item.Code));
                                    command.Parameters.Add(db.CreateDbParameter("AreaCategoryId", item.AreaCategoryId));
                                    command.Parameters.Add(db.CreateDbParameter("Status", item.Status));
                                    command.Parameters.Add(db.CreateDbParameter("Remark", item.Remark));
                                    command.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                    command.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                    command.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                    command.ExecuteNonQuery();

                                    //删除原有库管员清单
                                    var sqlDelete = "Delete from WarehouseAreaManager where WarehouseAreaId = (select id from warehouseArea where parentid = :parentid and code = :code)";
                                    //添加sql参数
                                    var delCommand = db.CreateDbCommand(sqlDelete, conn, ts);
                                    delCommand.Parameters.Add(db.CreateDbParameter("ParentId", item.ParentId));
                                    delCommand.Parameters.Add(db.CreateDbParameter("Code", item.Code));
                                    delCommand.ExecuteNonQuery();
                                }
                                else
                                {
                                    item.Status = 1;
                                    //获取对应的插入的SQL 通过db.GetInsertSql方法
                                    //获取新增数据的sql语句，Id为主键
                                    var sqlInsert = db.GetInsertSql("WarehouseArea", "Id", new[] {
                                    "WarehouseId","ParentId","Code","TopLevelWarehouseAreaId","AreaCategoryId","Status","Remark","AreaKind","CreatorId","CreatorName","CreateTime"
                                    });
                                    //添加Sql的参数
                                    var command = db.CreateDbCommand(sqlInsert, conn, ts);
                                    command.Parameters.Add(db.CreateDbParameter("WarehouseId", item.WarehouseId));
                                    command.Parameters.Add(db.CreateDbParameter("ParentId", item.ParentId));
                                    command.Parameters.Add(db.CreateDbParameter("Code", item.Code));
                                    command.Parameters.Add(db.CreateDbParameter("TopLevelWarehouseAreaId", item.TopLevelWarehouseAreaId));
                                    command.Parameters.Add(db.CreateDbParameter("AreaCategoryId", item.AreaCategoryId));
                                    command.Parameters.Add(db.CreateDbParameter("Status", item.Status));
                                    command.Parameters.Add(db.CreateDbParameter("Remark", item.Remark));
                                    command.Parameters.Add(db.CreateDbParameter("AreaKind", item.AreaKind));
                                    command.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                    command.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                    command.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                    command.ExecuteNonQuery();
                                }
                            }
                        }
                        //无异常提交
                        ts.Commit();
                    } catch(Exception) {
                        //报错回滚
                        ts.Rollback();
                        throw;
                    } finally {
                        if(conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }
        //导入库位
        public bool ImportWarehouseRegion(int parentId, string fileName, out int excelImportNum, out List<WarehouseAreaExtend> rightData, out List<WarehouseAreaExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorDataFileName = "";
            errorMessage = "";
            var errorList = new List<WarehouseAreaExtend>();
            var allList = new List<WarehouseAreaExtend>();
            var rightList = new List<WarehouseAreaExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);
                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("WarehouseArea", out notNullableFields, out fieldLenght);

                List<object> excelColumns;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    //指定文件中的列对应的名称
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_WarehouseArea_WarehouseAreaCode, "Code");//必填
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Remark, "Remark");

                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;
                    //获取对应枚举
                    var keyValuePairs = new[] {
                        new KeyValuePair<string, string>("BaseDataStatus", "BaseData_Status"),
                        new KeyValuePair<string, string>("AreaKind","Area_Kind")
                    };
                    tempExcelOperator.ImportHelper.LoadEnumFromDb(null,null, keyValuePairs);

                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var tempImportObj = new WarehouseAreaExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        //根据导入的字段给对应的实体属性赋值 
                        tempImportObj.CodeStr = newRow["Code"];
                        tempImportObj.RemarkStr = newRow["Remark"];

                        var tempErrorMessage = new List<string>();
                        //导入的数据基本检查
                        //库区编号检查
                        var fieldIndex = notNullableFields.IndexOf("Code".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.CodeStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_WarehouseArea_WarehouseAreaCodeIsNull);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.CodeStr) > fieldLenght["Code".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_WarehouseArea_WarehouseAreaCodeIsLong);
                        }

                        if(tempErrorMessage.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });
                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    //剩下的数据进行业务检查
                    //文件内数据重复
                    var groups = tempRightList.GroupBy(r => new {
                        r.CodeStr
                    }).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        groupItem.ErrorMsg = ErrorStrings.Export_Validation_WarehouseArea_Validation1;
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    //库位编号是否存在或存在冲突
                    var codesCheck = tempRightList.Select(r => r.CodeStr.ToUpper()).Distinct().ToArray();
                    var dbCodes = new List<WarehouseAreaExtend>();
                    Func<string[], bool> getDbCodes = values => {
                        dbCodes.Add(new WarehouseAreaExtend {
                            Code = values[0],
                            WarehouseId = Convert.ToInt32(values[1]),
                            ParentId = int.Parse(values[2]),
                            Status = int.Parse(values[3])
                        });
                        return false;
                    };
                    db.QueryDataWithInOperator("select Code,warehouseId,parentid,status from WarehouseArea  ", "Code", true, codesCheck, getDbCodes);

                    //查询上级库区库位
                    //var parentIdCheck = tempRightList.Select(r => r.ParentIdStr).Distinct().ToArray();
                    var dbParentId = new List<WarehouseAreaExtend>();
                    Func<string[], bool> getDbParentId = values => {
                        dbParentId.Add(new WarehouseAreaExtend {
                            Id = Convert.ToInt32(values[0]),
                            WarehouseId = Convert.ToInt32(values[1]),
                            AreaKind = Convert.ToInt32(values[2]),
                            AreaCategoryId = Convert.ToInt32(values[3]),
                            TopLevelWarehouseAreaId = values[4] == "" ? -1 : Convert.ToInt32(values[4])
                        });
                        return false;
                    };
                    var sql = string.Format("select Id,WarehouseId, AreaKind,AreaCategoryId,TopLevelWarehouseAreaId from WarehouseArea Where status=1 and id = {0}", parentId);
                    db.QueryDataWithInOperator(sql, "1", true, new[] { "1" }, getDbParentId);

                    foreach(var tempRight in tempRightList) {
                        var pId = dbParentId.FirstOrDefault(v => v.AreaKind != tempExcelOperator.ImportHelper.GetEnumValue("AreaKind", ErrorStrings.Export_Title_WarehouseAreaCategory_Area));
                        if(pId != null) {
                            tempRight.ErrorMsg = ErrorStrings.Export_Validation_WarehouseArea_Validation2;
                            continue;
                        }
                        foreach (var code in dbCodes)
                        {
                            if (code.Code == tempRight.CodeStr && code.ParentId == parentId)
                            {
                                if (code.Status == 1)
                                {
                                    tempRight.ErrorMsg = ErrorStrings.Export_Validation_WarehouseArea_Validation3;
                                }
                                else
                                {
                                    tempRight.StatusStr = "1";
                                    tempRight.Status = 1;
                                }
                            }
                        }
                        var pid = dbParentId.FirstOrDefault(v => v.AreaKind == tempExcelOperator.ImportHelper.GetEnumValue("AreaKind", ErrorStrings.Export_Title_WarehouseAreaCategory_Area));
                        if (pid == null)
                            continue;
                        tempRight.WarehouseId = pid.WarehouseId;
                        tempRight.AreaCategoryId = pid.AreaCategoryId;
                        //tempRight.ParentId = pid.Id;
                        tempRight.TopLevelWarehouseAreaId = pid.TopLevelWarehouseAreaId == -1 ? pid.Id : pid.TopLevelWarehouseAreaId;
                    }

                    //获取所有不合格数据
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    //获取所有合格数据
                    rightList = allList.Except(errorList).ToList();
                    //将合格数据的值填上
                    foreach(var tempRight in tempRightList) {
                        tempRight.Code = tempRight.CodeStr;
                        tempRight.AreaKind = tempExcelOperator.ImportHelper.GetEnumValue("AreaKind", ErrorStrings.Export_Title_WarehouseAreaCategory_AreaKindCode) ?? 0;
                        tempRight.Remark = tempRight.RemarkStr;
                        tempRight.ParentId = parentId;
                        //tempRight.Status = tempExcelOperator.ImportHelper.GetEnumValue("BaseDataStatus", ErrorStrings.Export_Validation_PartsBranch_Used) ?? 0;
                    }
                }
                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempObj = list[index - 1];
                            //设置错误信息导出的列的值
                            var values = new object[] {
                                tempObj.CodeStr, tempObj.RemarkStr, tempObj.ErrorMsg
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }
                //导入所有合格数据
                if(!rightList.Any())
                    return true;
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    //开启事务，新增更新配件在一个事务内
                    var ts = conn.BeginTransaction();
                    try {
                        //新增库区库位
                        if(rightList.Any()) {
                            
                            var userInfo = Utils.GetCurrentUserInfo();
                            //往数据库增加库区库位信息
                            foreach (var item in rightList)
                            {
                                if (item.Status == 1)
                                {
                                    //获取对应的更新sql，将已作废的库位变为生效，并将最新信息更新至原有库位中记录
                                    var sqlUpdate = db.GetUpdateSql("WarehouseArea", new string[] { "TopLevelWarehouseAreaId", "Status", "AreaKind", "AreaCategoryId", "Remark", "CreatorId", "CreatorName", "CreateTime" }, new string[] { "Code", "ParentId" });
                                    //添加sql参数
                                    var command = db.CreateDbCommand(sqlUpdate, conn, ts);
                                    command.Parameters.Add(db.CreateDbParameter("ParentId", item.ParentId));
                                    command.Parameters.Add(db.CreateDbParameter("Code", item.Code));
                                    command.Parameters.Add(db.CreateDbParameter("TopLevelWarehouseAreaId", item.TopLevelWarehouseAreaId));
                                    command.Parameters.Add(db.CreateDbParameter("AreaCategoryId", item.AreaCategoryId));
                                    command.Parameters.Add(db.CreateDbParameter("Status", item.Status));
                                    command.Parameters.Add(db.CreateDbParameter("Remark", item.Remark));
                                    command.Parameters.Add(db.CreateDbParameter("AreaKind", item.AreaKind));
                                    command.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                    command.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                    command.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                    command.ExecuteNonQuery();
                                }
                                else
                                {
                                    item.Status = 1;
                                    //获取对应的插入的SQL 通过db.GetInsertSql方法
                                    //获取新增数据的sql语句，Id为主键
                                    var sqlInsert = db.GetInsertSql("WarehouseArea", "Id", new[] {
                                    "WarehouseId","ParentId","Code","TopLevelWarehouseAreaId","AreaCategoryId","Status","Remark","AreaKind","CreatorId","CreatorName","CreateTime"
                                     });
                                    //添加Sql的参数
                                    var command = db.CreateDbCommand(sqlInsert, conn, ts);
                                    command.Parameters.Add(db.CreateDbParameter("WarehouseId", item.WarehouseId));
                                    command.Parameters.Add(db.CreateDbParameter("ParentId", item.ParentId));
                                    command.Parameters.Add(db.CreateDbParameter("Code", item.Code));
                                    command.Parameters.Add(db.CreateDbParameter("TopLevelWarehouseAreaId", item.TopLevelWarehouseAreaId));
                                    command.Parameters.Add(db.CreateDbParameter("AreaCategoryId", item.AreaCategoryId));
                                    command.Parameters.Add(db.CreateDbParameter("Status", item.Status));
                                    command.Parameters.Add(db.CreateDbParameter("Remark", item.Remark));
                                    command.Parameters.Add(db.CreateDbParameter("AreaKind", item.AreaKind));
                                    command.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                    command.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                    command.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                    command.ExecuteNonQuery();
                                }
                            }
                        }
                        //无异常提交
                        ts.Commit();
                    } catch(Exception) {
                        //报错回滚
                        ts.Rollback();
                        throw;
                    } finally {
                        if(conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }
    }
}
