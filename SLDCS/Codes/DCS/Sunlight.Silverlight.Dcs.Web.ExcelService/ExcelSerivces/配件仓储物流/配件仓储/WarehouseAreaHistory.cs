﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService{
    public partial class ExcelService {

        public bool ExportWarehouseAreaHistory(int[] ids, int? warehouseId, int? warehouseAreaCategoryId, string partCodes, string partName, string WarehouseAreaCode, out string fileName) {
            fileName = GetExportFilePath("配件库位变更履历_" + ".xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.Append(@"select h.name,
                                 (select value from keyvalueitem where NAME = 'Area_Category'and key=c.category) As category,
                                        a.code,
                                         a1.code,
                                         s.code,
                                         s.name,
                                         w.quantity,
                                         w.creatorname,
                                         w.createtime 
                                         from WarehouseAreaHistory w
                                        left join SparePart s on w.partid=s.id
                                        left join Warehouse h on w.warehouseid=h.id
                                        left join warehousearea a on w.warehouseareaid=a.id
                                        left join warehousearea a1 on w.destwarehouseareaid=a1.id
                                        left join WarehouseAreaCategory c on w.warehouseareacategoryid=c.id where 1=1 ");
                    var dbParameters = new List<DbParameter>();
                    if(ids != null && ids.Length > 0) {
                        sql.Append(" and w.Id in (");
                        for(var i = 0; i < ids.Length; i++) {
                            if(ids.Length == i + 1) {
                                sql.Append(ids[i].ToString());
                            } else {
                                sql.Append(ids[i].ToString() + ",");
                            }
                        }
                        sql.Append(") ");
                    } else {

                        if(warehouseId.HasValue) {
                            sql.Append(@" and w.warehouseId={0}warehouseId ");
                            dbParameters.Add(db.CreateDbParameter("warehouseId", warehouseId.Value));
                        }
                        if(warehouseAreaCategoryId.HasValue) {
                            sql.Append(@" and w.warehouseAreaCategoryId={0}warehouseAreaCategoryId ");
                            dbParameters.Add(db.CreateDbParameter("warehouseAreaCategoryId", warehouseAreaCategoryId.Value));
                        }

                        if(!String.IsNullOrEmpty(partCodes)) {
                            sql.Append(@" and s.code like {0}partCodes ");
                            dbParameters.Add(db.CreateDbParameter("partCodes", "%" + partCodes + "%"));
                        }
                        if(!String.IsNullOrEmpty(partName)) {
                            sql.Append(@" and s.name like {0}partName ");
                            dbParameters.Add(db.CreateDbParameter("partName", "%" + partName + "%"));
                        }
                        if(!String.IsNullOrEmpty(WarehouseAreaCode)) {
                            sql.Append(@" and a.code like {0}WarehouseAreaCode ");
                            dbParameters.Add(db.CreateDbParameter("WarehouseAreaCode", "%" + WarehouseAreaCode + "%"));
                        }

                    }
                    var sqlStr = sql.ToString();
                    if(sqlStr.IndexOf(" where 1=1  and ") > 0)
                        sqlStr = sqlStr.Replace(" where 1=1  and ", " where ");
                    else
                        sqlStr = sqlStr.Replace(" where 1=1 ", "");
                    var command = db.CreateDbCommand(string.Format(sqlStr, db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                  ErrorStrings.Export_Title_Company_WarehouseName,ErrorStrings.Export_Title_WarehouseAreaCategory_Category,ErrorStrings.Export_Title_WarehouseArea_OldWarehouseAreaCode,ErrorStrings.Export_Title_WarehouseArea_TargetWarehouseAreaCode,ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode,ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName,ErrorStrings.Export_Title_WarehouseAreaCategory_Quantity,ErrorStrings.Export_Title_AccountPeriod_CreatorName,ErrorStrings.Export_Title_AccountPeriod_CreateTime
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }

                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }

    }
}
