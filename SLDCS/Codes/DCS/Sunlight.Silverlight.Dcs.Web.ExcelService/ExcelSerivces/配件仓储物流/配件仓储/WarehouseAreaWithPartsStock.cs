﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        /// <summary>
        /// 批量导出配件库存明细
        /// </summary>
        /// <param name="warehouseId">仓库Id（非必填）</param>
        /// <param name="areaCategory">库区用途（非必填）</param>
        /// <param name="sparePartCode">配件编号（非必填）</param>
        /// <param name="sparePartName">配件名称 （非必填）</param>
        /// <param name="warehouseAreaCode">库位编号（非必填）</param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public bool WarehouseAreaWithPartsStock(int? warehouseId, int? areaCategory, string sparePartCode, string sparePartName, string warehouseAreaCode, out string fileName) {
            fileName = GetExportFilePath("配件库存明细_" + ".xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.Append(@"select     a.WarehouseName,
                                            (select value from keyvalueitem where NAME = 'Area_Category'and key=a.AreaCategory) As AreaCategory,
                                            a.WarehouseAreaCode,
                                            a.SparePartCode,
                                            a.SparePartName,
                                            a.Quantity
                                            from WarehouseAreaWithPartsStock a
                                            where 1 = 1");
                    var dbParameters = new List<DbParameter>();
                    if(warehouseId.HasValue) {
                        sql.Append(@" and a.warehouseId={0}warehouseId ");
                        dbParameters.Add(db.CreateDbParameter("warehouseId", warehouseId.Value));
                    }
                    if(areaCategory.HasValue) {
                        sql.Append(@" and a.areaCategory={0}areaCategory ");
                        dbParameters.Add(db.CreateDbParameter("areaCategory", areaCategory.Value));
                    }
                    if(!String.IsNullOrEmpty(sparePartCode)) {
                        sql.Append(@" and a.sparePartCode like {0}sparePartCode ");
                        dbParameters.Add(db.CreateDbParameter("sparePartCode", "%" + sparePartCode + "%"));
                    }
                    if(!String.IsNullOrEmpty(sparePartName)) {
                        sql.Append(@" and a.sparePartName like {0}sparePartName ");
                        dbParameters.Add(db.CreateDbParameter("sparePartName", "%" + sparePartName + "%"));
                    }
                    if(!String.IsNullOrEmpty(warehouseAreaCode)) {
                        sql.Append(@" and a.warehouseAreaCode like {0}warehouseAreaCode ");
                        dbParameters.Add(db.CreateDbParameter("warehouseAreaCode", "%" + warehouseAreaCode + "%"));
                    }
                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                   ErrorStrings.Export_Title_Company_WarehouseName, ErrorStrings.Export_Title_WarehouseAreaCategory_Category, ErrorStrings.Export_Title_WarehouseArea_WarehouseAreaCode, ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode, ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName, ErrorStrings.Export_Title_WarehouseArea_Quantity
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }

                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }

        public bool ScheduleWarehouseAreaWithPartsStock(int? warehouseId, int? areaCategory, string sparePartCode, string sparePartName, string warehouseAreaCode, string jobName, out string fileName) {
            fileName = GetExportFilePath("配件库存明细_" + ".xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.Append(@"select     a.WarehouseName,
                                            (select value from keyvalueitem where NAME = 'Area_Category'and key=a.AreaCategory) As AreaCategory,
                                            a.WarehouseAreaCode,
                                            a.SparePartCode,
                                            a.SparePartName,
                                            a.Quantity
                                            from WarehouseAreaWithPartsStock a
                                            where 1 = 1");
                    var dbParameters = new List<DbParameter>();
                    if(warehouseId.HasValue) {
                        sql.Append(@" and a.warehouseId={0}warehouseId ");
                        dbParameters.Add(db.CreateDbParameter("warehouseId", warehouseId.Value));
                    }
                    if(areaCategory.HasValue) {
                        sql.Append(@" and a.areaCategory={0}areaCategory ");
                        dbParameters.Add(db.CreateDbParameter("areaCategory", areaCategory.Value));
                    }
                    if(!String.IsNullOrEmpty(sparePartCode)) {
                        sql.Append(@" and a.sparePartCode like {0}sparePartCode ");
                        dbParameters.Add(db.CreateDbParameter("sparePartCode", "%" + sparePartCode + "%"));
                    }
                    if(!String.IsNullOrEmpty(sparePartName)) {
                        sql.Append(@" and a.sparePartName like {0}sparePartName ");
                        dbParameters.Add(db.CreateDbParameter("sparePartName", "%" + sparePartName + "%"));
                    }
                    if(!String.IsNullOrEmpty(warehouseAreaCode)) {
                        sql.Append(@" and a.warehouseAreaCode like {0}warehouseAreaCode ");
                        dbParameters.Add(db.CreateDbParameter("warehouseAreaCode", "%" + warehouseAreaCode + "%"));
                    }
                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                   ErrorStrings.Export_Title_Company_WarehouseName, ErrorStrings.Export_Title_WarehouseAreaCategory_Category, ErrorStrings.Export_Title_WarehouseArea_WarehouseAreaCode, ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode, ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName, ErrorStrings.Export_Title_WarehouseArea_Quantity
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }

                            return null;
                        });
                    }
                    reader.Close();
                }
                UpdateScheduleExportState(jobName, fileName);
                return true;
            } catch(Exception) {
                return false;
            }
        }
    }
}
