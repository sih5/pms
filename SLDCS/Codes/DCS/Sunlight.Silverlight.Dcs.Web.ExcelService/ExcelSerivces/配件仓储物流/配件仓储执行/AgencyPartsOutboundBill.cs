﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Globalization;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        /// <summary>
        /// 合并导出配件出库主清单代理库
        /// </summary>
        public bool ExportAgencyPartsOutboundBillWithDetail(int[] ids, int? userId, int? partsSalesCategoryId, int storageCompanyId, string partsOutBoundBillCode, int? warehouseId, string counterpartcompanycode, string counterpartcompanyname, int? outBoundType, int? settlementStatus, string partsSalesOrderTypeName, string partsOutboundPlanCode, string outboundPackPlanCode, string contractCode, string provinceName, DateTime? createTimeBegin, DateTime? createTimeEnd, string ERPSourceOrderCode, out string fileName) {
            fileName = GetExportFilePath("配件出库单主清单-代理库.xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);
                var userInfo = Utils.GetCurrentUserInfo();
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.Append(string.Format(@"select a.code,
                                        a.warehousecode,
                                        a.warehousename,
                                        (select value from keyvalueitem where NAME = 'Parts_OutboundType'and key=a.outboundtype) As outboundtype,
                                        (Select Sum(t.SettlementPrice * t.OutboundAmount) From APartsOutboundBillDetail t Where t.PartsOutboundBillId = a.Id),
                                        a.originalrequirementbillcode,
                                        c.code as partsoutboundplancode,
                                        d.name as partssalescategoryname,
                                        a.ERPSourceOrderCode,
                                        a.branchname,
                                        a.counterpartcompanycode,
                                        a.counterpartcompanyname,
                                        (select value from keyvalueitem where NAME = 'Company_Type'and key=comp.type) As counterpartcompanyType, --对方单位类型
                                        a.ReceivingWarehouseCode,
                                        a.ReceivingWarehouseName,
                                        comp.ProvinceName as provinceName,   --省份
                                        a.InterfaceRecordId,--WMS装载单号
                                        (select value from keyvalueitem where NAME = 'Parts_SettlementStatus'and key=a.settlementstatus) As settlementstatus,
                                        (select value from keyvalueitem where NAME = 'ERPInvoiceInformation_Type'and key=ps.InvoiceType) As InvoiceType,
                                        ps.ERPSourceOrderCode,
                                        a.remark,
                                        a.creatorname,
                                        a.createtime,
                                        a.modifiername,
                                        a.modifytime, 
                                        (select value from keyvalueitem where NAME = 'PartsShipping_Method'and key=ps.ShippingMethod) As ShippingMethod,
                                        b.sparepartcode,
                                        b.sparepartname,
                                        b.warehouseareacode,
                                        b.outboundamount,
                                        b.settlementprice,
                                        s.MeasureUnit,
                                        s.OverseasPartsFigure
                                    from agencypartsoutboundbill a
                                    LEFT OUTER join APartsOutboundBillDetail b
                                    on a.id = b.partsoutboundbillid
                                    LEFT OUTER join partsoutboundplan c
                                    on a.partsoutboundplanid=c.id
                                    left outer join PartsSalesOrder ps
                                    on c.OriginalRequirementBillType=1 and c.OriginalRequirementBillId=ps.id
                                    LEFT OUTER join partssalescategory d on
                                    a.partssalescategoryid=d.id
                                    left outer join company comp on a.receivingcompanyid = comp.id 
                                    left outer join sparepart s on b.sparepartid = s.id 
                                    where a.StorageCompanyId={0} ", userInfo.EnterpriseId));
                    var dbParameters = new List<DbParameter>();
                    if(ids != null && ids.Length > 0) {
                        sql.Append(" and a.id in (");
                        for(var i = 0;i < ids.Length;i++) {
                            if(ids.Length == i + 1) {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            } else {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                        }
                        sql.Append(")");
                    } else {
                        if(userId.HasValue) {
                            sql.Append(@"and  ( EXISTS (SELECT 
                                                  1 AS C1
                                                  FROM WarehouseOperator Extent4
                                                  WHERE Extent4.OperatorId = {0}userId AND Extent4.WarehouseId = a.WarehouseId)
                                                  or Exists(Select 1 From warehouse t Where t.id=a.warehouseid And Type=99)) ");
                            dbParameters.Add(db.CreateDbParameter("userId", userInfo.Id));
                        }
                        if(partsSalesCategoryId.HasValue) {
                            sql.Append(@" and a.partsSalesCategoryId = {0}partsSalesCategoryId ");
                            dbParameters.Add(db.CreateDbParameter("partsSalesCategoryId", partsSalesCategoryId.Value));
                        }
                        if(!string.IsNullOrEmpty(partsOutBoundBillCode)) {
                            sql.Append("and LOWER(a.Code) like {0}partsOutBoundBillCode ");
                            dbParameters.Add(db.CreateDbParameter("partsOutBoundBillCode", "%" + partsOutBoundBillCode.ToLower() + "%"));
                        }

                        if(warehouseId.HasValue) {
                            sql.Append("and a.warehouseId = {0}warehouseId ");
                            dbParameters.Add(db.CreateDbParameter("warehouseId", warehouseId.Value));
                        }
                        if(!string.IsNullOrEmpty(counterpartcompanycode)) {
                            sql.Append("and LOWER(a.counterpartcompanycode) like {0}counterpartcompanycode ");
                            dbParameters.Add(db.CreateDbParameter("counterpartcompanycode", "%" + counterpartcompanycode.ToLower() + "%"));
                        }
                        if(!string.IsNullOrEmpty(counterpartcompanyname)) {
                            sql.Append("and LOWER(a.counterpartcompanyname) like {0}counterpartcompanyname ");
                            dbParameters.Add(db.CreateDbParameter("counterpartcompanyname", "%" + counterpartcompanyname.ToLower() + "%"));
                        }
                        if(!string.IsNullOrEmpty(ERPSourceOrderCode)) {
                            sql.Append("and LOWER(a.ERPSourceOrderCode) like {0}ERPSourceOrderCode ");
                            dbParameters.Add(db.CreateDbParameter("ERPSourceOrderCode", "%" + ERPSourceOrderCode.ToLower() + "%"));
                        }
                        if(outBoundType.HasValue) {
                            sql.Append("and a.outBoundType ={0}outBoundType ");
                            dbParameters.Add(db.CreateDbParameter("outBoundType", outBoundType.Value));
                        }
                        if(!string.IsNullOrEmpty(partsSalesOrderTypeName)) {
                            sql.Append("and LOWER(a.partsSalesOrderTypeName) like {0}partsSalesOrderTypeName ");
                            dbParameters.Add(db.CreateDbParameter("partsSalesOrderTypeName", "%" + partsSalesOrderTypeName.ToLower() + "%"));
                        }
                        if(!string.IsNullOrEmpty(outboundPackPlanCode)) {
                            sql.Append("and LOWER(a.outboundPackPlanCode) like {0}outboundPackPlanCode ");
                            dbParameters.Add(db.CreateDbParameter("outboundPackPlanCode", "%" + outboundPackPlanCode.ToLower() + "%"));
                        }
                        if(!string.IsNullOrEmpty(partsOutboundPlanCode)) {
                            sql.Append("and LOWER(c.code) like {0}partsoutboundplancode ");
                            dbParameters.Add(db.CreateDbParameter("partsoutboundplancode", "%" + partsOutboundPlanCode.ToLower() + "%"));
                        }
                        if(!string.IsNullOrEmpty(provinceName)) {
                            sql.Append("and LOWER(comp.ProvinceName) like {0}provinceName ");
                            dbParameters.Add(db.CreateDbParameter("provinceName", "%" + provinceName.ToLower() + "%"));
                        }

                        if(!string.IsNullOrEmpty(contractCode)) {
                            sql.Append("and LOWER(a.contractCode) like {0}contractCode ");
                            dbParameters.Add(db.CreateDbParameter("contractCode", "%" + contractCode.ToLower() + "%"));
                        }
                        if(settlementStatus.HasValue) {
                            sql.Append("and a.settlementStatus ={0}settlementStatus ");
                            dbParameters.Add(db.CreateDbParameter("settlementStatus", settlementStatus.Value));
                        }

                        if(createTimeBegin.HasValue) {
                            sql.Append(@" and a.createTime >=To_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }


                        if(createTimeEnd.HasValue) {
                            sql.Append(@" and a.createTime <=To_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                    }
                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    ErrorStrings.Export_Title_PartsOutboundBill_Code, ErrorStrings.Export_Title_Company_WarehouseCode, ErrorStrings.Export_Title_Company_WarehouseName, ErrorStrings.Export_Title_PartsOutboundBill_OutBoundType, ErrorStrings.Export_Title_InternalAllocationBill_TotalAmount, ErrorStrings.Export_Title_PartsInboundCheckBill_OriginalRequirementBillCode,ErrorStrings.Export_Title_PartsOutboundBill_PartsOutboundPlanCode,ErrorStrings.Export_Title_Partssalescategory_Name, ErrorStrings.Export_Title_Partsoutboundplan_ERPOrderCode,ErrorStrings.Export_Title_PartsBranch_BranchName, ErrorStrings.Export_Title_BoxUpTask_CounterpartCompanyCode, ErrorStrings.Export_Title_BoxUpTask_CounterpartCompanyName,ErrorStrings.Export_Title_PartsOutboundBill_CounterpartCompanyType, ErrorStrings.Export_Title_PartsOutboundBill_ReceivingWarehouseCode, ErrorStrings.Export_Title_SupplierShippingOrder_ReveiveWarehouseName,ErrorStrings.Export_Validation_Company_ProvinceName,ErrorStrings.Export_Title_AgencyPartsShippingOrder_WMSCode, ErrorStrings.Export_Title_PartsInboundCheckBill_SettlementStatus,ErrorStrings.Export_Title_PartsInboundCheckBill_BillingType,ErrorStrings.Export_Title_Partsoutboundplan_ERPOrderCode, ErrorStrings.Export_Title_PartsBranch_Remark, ErrorStrings.Export_Title_AccountPeriod_CreatorName, ErrorStrings.Export_Title_AccountPeriod_CreateTime, ErrorStrings.Export_Title_AccountPeriod_ModifierName, ErrorStrings.Export_Title_AccountPeriod_ModifyTime,ErrorStrings.Export_Title_PartsPurchaseOrder_ShippingMethod,
                                    ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode,ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName,ErrorStrings.Export_Title_WarehouseArea_WarehouseAreaCode,ErrorStrings.Export_Title_PartsOutboundBill_OutboundAmount,ErrorStrings.Export_Title_PartsPurchaseRtnSettleBill_SettlementPrice,ErrorStrings.Export_Title_PackingPropertyApp_MeasureUnit,ErrorStrings.Export_Title_Sparepart_OverseasPartsCode,ErrorStrings.Export_Title_PartsPurchaseOrder_DetailRemark
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }

        /// <summary>
        /// 导出配件出库主单代理库
        /// </summary>
        public bool ExportAgencyPartsOutboundBill(int[] ids, int? userId, int? partsSalesCategoryId, int storageCompanyId, string partsOutBoundBillCode, int? warehouseId, string counterpartcompanycode, string counterpartcompanyname, int? outBoundType, int? settlementStatus, string partsSalesOrderTypeName, string partsOutBoundPlanCode, string outboundPackPlanCode, string contractCode, string provinceName, DateTime? createTimeBegin, DateTime? createTimeEnd, string ERPSourceOrderCode, out string fileName) {
            fileName = GetExportFilePath("配件出库单主单-代理库.xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);
                var userInfo = Utils.GetCurrentUserInfo();
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.Append(string.Format(@"select a.code,
                                        a.warehousecode,
                                        a.warehousename,
                                        (select value from keyvalueitem where NAME = 'Parts_OutboundType'and key=a.outboundtype) As outboundtype,
                                        GroupBy1.A1,
                                        a.originalrequirementbillcode,
                                        a.partssalesordertypename,
                                        c.code as partsoutboundplancode,
                                        d.name as partssalescategoryname,
                                        a.ERPSourceOrderCode,
                                        a.branchname,
                                        a.counterpartcompanycode,
                                        a.counterpartcompanyname,
                                        (select value from keyvalueitem where NAME = 'Company_Type'and key=comp.type) As counterpartcompanyType, --对方单位类型
                                        a.ReceivingWarehouseCode,
                                        a.ReceivingWarehouseName,
                                        comp.ProvinceName as provinceName,    --省份
                                        a.InterfaceRecordId,
                                        (select value from keyvalueitem where NAME = 'Parts_SettlementStatus'and key=a.settlementstatus) As settlementstatus,
                                        (select value from keyvalueitem where NAME = 'ERPInvoiceInformation_Type'and key=pso.InvoiceType) As InvoiceType,
                                        a.remark,
                                        a.orderapprovecomment,
                                        a.creatorname,
                                        a.createtime,
                                        a.modifiername,
                                        a.ModifyTime
                                    from AgencyPartsOutboundBill a
                                    INNER JOIN (SELECT Extent2.PartsOutboundBillId AS K1, Sum(Extent2.OutboundAmount * Extent2.SettlementPrice) AS A1
                                    FROM APartsOutboundBillDetail Extent2
                                    GROUP BY Extent2.PartsOutboundBillId ) GroupBy1 ON a.Id = GroupBy1.K1
                                    inner join Agencypartsoutboundplan c
                                    on a.partsoutboundplanid=c.id
                                    inner join partssalescategory d on
                                    a.partssalescategoryid=d.id
                                    left outer join partssalesorder pso on a.OriginalRequirementBillId = pso.id and a.Originalrequirementbilltype=1
                                    left outer join company comp on a.receivingcompanyid = comp.id
                                    where a.StorageCompanyId={0} ", userInfo.EnterpriseId));
                    var dbParameters = new List<DbParameter>();
                    if(ids != null && ids.Length > 0) {
                        sql.Append(" and a.id in (");
                        for(var i = 0;i < ids.Length;i++) {
                            if(ids.Length == i + 1) {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            } else {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                        }
                        sql.Append(")");
                    } else {
                        if(userId.HasValue) {
                            sql.Append(@"and  ( EXISTS (SELECT 
                                                  1 AS C1
                                                  FROM WarehouseOperator Extent4
                                                  WHERE Extent4.OperatorId = {0}userId AND Extent4.WarehouseId = a.WarehouseId)
                                            or Exists(Select 1 From warehouse t Where t.id=a.warehouseid And Type=99)) ");
                            dbParameters.Add(db.CreateDbParameter("userId", userInfo.Id));
                        }
                        if(partsSalesCategoryId.HasValue) {
                            sql.Append(@" and a.partsSalesCategoryId = {0}partsSalesCategoryId ");
                            dbParameters.Add(db.CreateDbParameter("partsSalesCategoryId", partsSalesCategoryId.Value));
                        }
                        if(!string.IsNullOrEmpty(partsOutBoundBillCode)) {
                            sql.Append("and LOWER(a.Code) like {0}partsOutBoundBillCode ");
                            dbParameters.Add(db.CreateDbParameter("partsOutBoundBillCode", "%" + partsOutBoundBillCode.ToLower() + "%"));
                        }
                        if(warehouseId.HasValue) {
                            sql.Append("and a.warehouseId = {0}warehouseId ");
                            dbParameters.Add(db.CreateDbParameter("warehouseId", warehouseId.Value));
                        }
                        if(!string.IsNullOrEmpty(counterpartcompanycode)) {
                            sql.Append("and LOWER(a.counterpartcompanycode) like {0}counterpartcompanycode ");
                            dbParameters.Add(db.CreateDbParameter("counterpartcompanycode", "%" + counterpartcompanycode.ToLower() + "%"));
                        }

                        if(!string.IsNullOrEmpty(counterpartcompanyname)) {
                            sql.Append("and LOWER(a.counterpartcompanyname) like {0}counterpartcompanyname ");
                            dbParameters.Add(db.CreateDbParameter("counterpartcompanyname", "%" + counterpartcompanyname.ToLower() + "%"));
                        }

                        if(outBoundType.HasValue) {
                            sql.Append("and a.outBoundType ={0}outBoundType ");
                            dbParameters.Add(db.CreateDbParameter("outBoundType", outBoundType.Value));
                        }
                        if(!string.IsNullOrEmpty(partsSalesOrderTypeName)) {
                            sql.Append("and LOWER(a.partsSalesOrderTypeName) like {0}partsSalesOrderTypeName ");
                            dbParameters.Add(db.CreateDbParameter("partsSalesOrderTypeName", "%" + partsSalesOrderTypeName.ToLower() + "%"));
                        }
                        if(!string.IsNullOrEmpty(ERPSourceOrderCode)) {
                            sql.Append("and LOWER(a.ERPSourceOrderCode) like {0}ERPSourceOrderCode ");
                            dbParameters.Add(db.CreateDbParameter("ERPSourceOrderCode", "%" + ERPSourceOrderCode.ToLower() + "%"));
                        }
                        if(!string.IsNullOrEmpty(partsOutBoundPlanCode)) {
                            sql.Append("and LOWER(c.Code) like {0}partsoutboundplancode ");
                            dbParameters.Add(db.CreateDbParameter("partsoutboundplancode", "%" + partsOutBoundPlanCode.ToLower() + "%"));
                        }

                        if(!string.IsNullOrEmpty(outboundPackPlanCode)) {
                            sql.Append("and LOWER(a.outboundPackPlanCode) like {0}outboundPackPlanCode ");
                            dbParameters.Add(db.CreateDbParameter("outboundPackPlanCode", "%" + outboundPackPlanCode.ToLower() + "%"));
                        }
                        if(!string.IsNullOrEmpty(provinceName)) {
                            sql.Append("and LOWER(comp.ProvinceName) like {0}provinceName ");
                            dbParameters.Add(db.CreateDbParameter("provinceName", "%" + provinceName.ToLower() + "%"));
                        }

                        if(!string.IsNullOrEmpty(contractCode)) {
                            sql.Append("and LOWER(a.contractCode) like {0}contractCode ");
                            dbParameters.Add(db.CreateDbParameter("contractCode", "%" + contractCode.ToLower() + "%"));
                        }

                        if(settlementStatus.HasValue) {
                            sql.Append("and a.settlementStatus ={0}settlementStatus ");
                            dbParameters.Add(db.CreateDbParameter("settlementStatus", settlementStatus.Value));
                        }

                        if(createTimeBegin.HasValue) {
                            sql.Append(@" and a.createTime >=To_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }


                        if(createTimeEnd.HasValue) {
                            sql.Append(@" and a.createTime <=To_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                    }
                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    ErrorStrings.Export_Title_PartsOutboundBill_Code, ErrorStrings.Export_Title_Company_WarehouseCode, ErrorStrings.Export_Title_Company_WarehouseName, ErrorStrings.Export_Title_PartsOutboundBill_OutBoundType, ErrorStrings.Export_Title_InternalAllocationBill_TotalAmount,ErrorStrings.Export_Title_PartsInboundCheckBill_OriginalRequirementBillCode,ErrorStrings.Export_Title_PartsOutboundBill_SalesOrderType,ErrorStrings.Export_Title_PartsOutboundBill_PartsOutboundPlanCode,ErrorStrings.Export_Title_Partssalescategory_Name,ErrorStrings.Export_Title_Partsoutboundplan_ERPOrderCode, ErrorStrings.Export_Title_PartsBranch_BranchName, ErrorStrings.Export_Title_BoxUpTask_CounterpartCompanyCode, ErrorStrings.Export_Title_BoxUpTask_CounterpartCompanyName,ErrorStrings.Export_Title_PartsOutboundBill_CounterpartCompanyType, ErrorStrings.Export_Title_PartsOutboundBill_ReceivingWarehouseCode, ErrorStrings.Export_Title_SupplierShippingOrder_ReveiveWarehouseName,ErrorStrings.Export_Validation_Company_ProvinceName,"装载单编号", ErrorStrings.Export_Title_PartsInboundCheckBill_SettlementStatus,ErrorStrings.Export_Title_PartsInboundCheckBill_BillingType, ErrorStrings.Export_Title_PartsBranch_Remark,ErrorStrings.Export_Title_PartsOutboundBill_OrderApproveComment, ErrorStrings.Export_Title_AccountPeriod_CreatorName, ErrorStrings.Export_Title_AccountPeriod_CreateTime, ErrorStrings.Export_Title_AccountPeriod_ModifierName, ErrorStrings.Export_Title_AccountPeriod_ModifyTime
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }


    }
}
