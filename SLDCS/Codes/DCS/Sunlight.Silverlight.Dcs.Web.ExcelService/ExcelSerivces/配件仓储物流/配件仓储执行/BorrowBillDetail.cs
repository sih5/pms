﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService
{
    public partial class ExcelService
    {
        public bool ImportBorrowBillDetail(string fileName, int excelImportNum, int? warehouseId, out List<BorrowBillDetailExtend> rightData, out List<BorrowBillDetailExtend> errorData, out string errorDataFileName, out string errorMessage)
        {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<BorrowBillDetailExtend>();
            var allList = new List<BorrowBillDetailExtend>();
            List<BorrowBillDetailExtend> rightList = null;
            try
            {
                if (!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("BorrowBillDetail", out notNullableFields, out fieldLenght);
                List<object> excelColumns;
                using (var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString))
                {
                    #region 指定文件中的列对应的名称 例如
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode, "SparePartCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName, "SparePartName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_BorrowBillDetail_BorrowQty, "BorrowQty");
                    #endregion
                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    excelImportNum = excelOperator.LoadExcelRow(row =>
                    {
                        var tempImportObj = new BorrowBillDetailExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        #region 根据导入的字段给对应的实体属性赋值
                        tempImportObj.SparePartCodeStr = newRow["SparePartCode"];
                        tempImportObj.SparePartNameStr = newRow["SparePartName"];
                        tempImportObj.BorrowQtyStr = newRow["BorrowQty"];
                        #endregion
                        var tempErrorMessage = new List<string>();
                        #region 导入的数据基本检查
                        if (string.IsNullOrEmpty(tempImportObj.SparePartCodeStr))
                        {
                            tempErrorMessage.Add(ErrorStrings.ImpPartsPurchaseOrderDetail_Validation1);
                        }
                        else
                        {
                            if (Encoding.Default.GetByteCount(tempImportObj.SparePartCodeStr) > fieldLenght["SparePartCode".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchaseOrderDetail_Validation2);
                        }

                        if (string.IsNullOrEmpty(tempImportObj.BorrowQtyStr))
                        {
                            tempImportObj.BorrowQty = 0;
                        }
                        else
                        {
                            //借用数量检查
                            int checkValue;
                            if (int.TryParse(tempImportObj.BorrowQtyStr, out checkValue))
                            {
                                if (checkValue <= 0)
                                {
                                    tempImportObj.BorrowQty = 0;
                                }
                                else
                                {
                                    tempImportObj.BorrowQty = checkValue;
                                }

                            }
                            else
                            {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_BorrowBillDetail_BorrowQtyInteger);
                            }
                        }
                        #endregion
                        if (tempErrorMessage.Count > 0)
                        {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });
                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    #region 剩下的数据进行业务检查
                    //文件内数据重复
                    var groups = tempRightList.GroupBy(r => new
                    {
                        r.SparePartCodeStr
                    }).Where(r => r.Count() > 1);
                    foreach (var groupItem in groups.SelectMany(r => r))
                    {
                        groupItem.ErrorMsg = ErrorStrings.Export_Validation_PartsPurchaseOrder_Validation2;
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    //1、配件有效性校验（配件信息.配件编号=导入数据.配件图号），
                    //如果校验不通过，记录校验错误信息：配件不存在。
                    var sparePartCodesNeedCheck = tempRightList.Select(r => r.SparePartCodeStr).Distinct().ToArray();
                    var dbSpareParts = new List<SparePartExtend>();
                    Func<string[], bool> getDbSpareParts = value =>
                    {
                        var dbObj = new SparePartExtend
                        {
                            Id = Convert.ToInt32(value[0]),
                            Code = value[1],
                            Name = value[2],
                            MeasureUnit = value[3],
                            ReferenceCode = value[4]
                        };
                        dbSpareParts.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator(string.Format("Select Id,Code,Name,MeasureUnit,ReferenceCode From SparePart Where Status = {0}", (int)DcsMasterDataStatus.有效), "Code", true, sparePartCodesNeedCheck, getDbSpareParts);
                    var errorSpareParts = tempRightList.Where(r => dbSpareParts.All(v => v.Code != r.SparePartCodeStr)).ToList();
                    foreach (var errorItem in errorSpareParts)
                    {
                        errorItem.ErrorMsg = ErrorStrings.Export_Validation_InternalAcquisitionDetail_Validation1;
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    foreach (var rightItem in tempRightList)
                    {
                        var tempSparePart = dbSpareParts.Single(r => r.Code == rightItem.SparePartCodeStr);
                        rightItem.SparePartId = tempSparePart.Id;
                        rightItem.SparePartCode = tempSparePart.Code;
                        rightItem.SparePartName = tempSparePart.Name;
                        rightItem.MeasureUnit = tempSparePart.MeasureUnit;//单位
                        rightItem.SihCode = tempSparePart.ReferenceCode;//红岩号
                    }

                    var sparePartIds = dbSpareParts.Select(r => r.Id.ToString(CultureInfo.InvariantCulture)).Distinct().ToArray();
                    if (sparePartIds.Any())
                    {
                        var dbPartsStocks = new List<PartsStockExtend>();
                        Func<string[], bool> getDbPartsStocks = value =>
                        {
                            var dbObj = new PartsStockExtend
                            {
                                PartCode = value[0],
                                Quantity = Convert.ToInt32(value[1]),
                                Code = value[2],
                                WarehouseAreaId = Convert.ToInt32(value[3]),
                                WarehouseId = Convert.ToInt32(value[4]),
                            };
                            dbPartsStocks.Add(dbObj);
                            return false;
                        };
                        var sql1 = string.Format(@"Select  d.code,a.Quantity,c.Code,c.Id,a.warehouseid,a.Partid
                                                    From Partsstock a
                                                    Inner Join Warehouseareacategory b
                                                    On b.Id = a.Warehouseareacategoryid
                                                    Inner Join Warehousearea c
                                                    On a.Warehouseareaid = c.Id
                                                    inner join SparePart d on a.partid=d.id
                                                    Where a.Warehouseid = {0}
                                                    And b.Category = 1  ", warehouseId);
                        db.QueryDataWithInOperator(sql1, "a.Partid", false, sparePartIds, getDbPartsStocks);
                        foreach (var tempRight in tempRightList)
                        {
                            var partsStock = dbPartsStocks.FirstOrDefault(r => r.PartCode == tempRight.SparePartCodeStr && r.WarehouseId == warehouseId);
                            if (partsStock == null)
                            {
                                tempRight.ErrorMsg = ErrorStrings.Export_Validation_BorrowBillDetail_Validation1;
                                continue;
                            }
                            tempRight.WarehouseAreaId = partsStock.WarehouseAreaId;
                            tempRight.WarehouseAreaCode = partsStock.Code;
                        }
                    }

                    
                    //获取所有不合格数据
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    //获取所有合格数据
                    rightList = allList.Except(errorList).ToList();
                    #endregion
                }
                //导出所有不合格数据
                if (errorList.Any())
                {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using (var excelExport = new ExcelExport(errorDataFileName))
                    {
                        var list = errorList;
                        excelExport.ExportByRow(index =>
                        {
                            if (index == list.Count + 1)
                                return null;
                            if (index == 0)
                                return excelColumns.ToArray();
                            var tempObj = list[index - 1];
                            var values = new object[] {
                                #region 设置错误信息导出的列的值
                                tempObj.SparePartCodeStr, tempObj.SparePartNameStr,
                                tempObj.BorrowQtyStr,
                                tempObj.ErrorMsg
                                #endregion
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }
                return true;
            }
           catch (Exception ex)
            {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            }
            finally
            {
                errorData = errorList;
                rightData = rightList;
            }
        }
    }
}
