﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Globalization;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        /// <summary>
        /// 合并导出装箱任务单
        /// </summary>
        public bool ExportBoxUpTask(int[] ids,string code,int? warehouseId,int? partsSalesCategoryId,int? status,string sourceCode,string partsOutboundPlanCode,
            string counterpartCompanyCode, string counterpartCompanyName, DateTime? createTimeBegin, DateTime? createTimeEnd, DateTime? modifyTimeBegin, DateTime? modifyTimeEnd,string sparePartCode,string sparePartName, out string fileName)
        {
            fileName = GetExportFilePath("配件装箱任务单.xlsx");

            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);
                var userInfo = Utils.GetCurrentUserInfo();
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.Append(string.Format(@"select bt.Code,bt.OrderTypeName,psc.name as PartsSalesCategoryName,bt.WarehouseCode,bt.WarehouseName,bt.CounterpartCompanyCode,bt.CounterpartCompanyName,
                                                (select value from keyvalueitem where NAME = 'BoxUpTaskStatus'and key=bt.Status) As Status,DECODE(bt.isexistshippingorder,1,'是',0,'否') as IsExistShippingOrder,
                                                bt.CreatorName,bt.CreateTime,bt.ModifierName,bt.ModifyTime,bt.BoxUpFinishTime,bt.Remark,
                                                btd.PartsOutboundPlanCode,btd.PickingTaskCode,btd.SourceCode,btd.SparePartCode,btd.SparePartName,btd.SihCode,btd.PlanQty,nvl(btd.BoxUpQty,0) as BoxUpQty,btd.ContainerNumber
                                                from boxuptask bt left join boxuptaskdetail btd on bt.id=btd.boxuptaskid left join PartsSalesCategory psc on psc.id=bt.partssalescategoryid left join warehouseoperator wo on wo.warehouseid=bt.warehouseid where 1=1 "));

                    var dbParameters = new List<DbParameter>();
                    sql.Append("and wo.operatorid = {0}operatorid ");
                    dbParameters.Add(db.CreateDbParameter("operatorid", Utils.GetCurrentUserInfo().Id));
                    if (ids != null && ids.Length > 0)
                    {
                        sql.Append(" and bt.id in (");
                        for (var i = 0; i < ids.Length; i++)
                        {
                            if (ids.Length == i + 1)
                            {
                                sql.Append("{0}" + ids[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter(ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                            else
                            {
                                sql.Append("{0}" + ids[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter(ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                        }
                        sql.Append(")");
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(code))
                        {
                            sql.Append("and bt.Code like {0}code ");
                            dbParameters.Add(db.CreateDbParameter("code", "%" + code + "%"));
                        }
                        if (warehouseId.HasValue)
                        {
                            sql.Append("and bt.warehouseId like {0}warehouseId ");
                            dbParameters.Add(db.CreateDbParameter("warehouseId", "%" + warehouseId + "%"));
                        }
                        if (partsSalesCategoryId.HasValue)
                        {
                            sql.Append("and bt.partsSalesCategoryId = {0}partsSalesCategoryId ");
                            dbParameters.Add(db.CreateDbParameter("partsSalesCategoryId", partsSalesCategoryId.Value));
                        }
                         if (status.HasValue)
                        {
                            sql.Append("and bt.status = {0}status ");
                            dbParameters.Add(db.CreateDbParameter("status", status.Value));
                        }
                        if (!string.IsNullOrEmpty(sourceCode))
                        {
                            sql.Append("and btd.sourceCode like {0}sourceCode ");
                            dbParameters.Add(db.CreateDbParameter("sourceCode", "%" + sourceCode + "%"));
                        }
                        if (!string.IsNullOrEmpty(partsOutboundPlanCode))
                        {
                            sql.Append("and btd.partsOutboundPlanCode like {0}partsOutboundPlanCode ");
                            dbParameters.Add(db.CreateDbParameter("partsOutboundPlanCode", "%" + partsOutboundPlanCode + "%"));
                        }
                        if (!string.IsNullOrEmpty(sparePartCode))
                        {
                            sql.Append("and btd.sparePartCode like {0}sparePartCode ");
                            dbParameters.Add(db.CreateDbParameter("sparePartCode", "%" + sparePartCode + "%"));
                        }
                        if (!string.IsNullOrEmpty(sparePartName))
                        {
                            sql.Append("and btd.sparePartName like {0}sparePartName ");
                            dbParameters.Add(db.CreateDbParameter("sparePartName", "%" + sparePartName + "%"));
                        }
                        if (!string.IsNullOrEmpty(counterpartCompanyCode))
                        {
                            sql.Append("and bt.counterpartCompanyCode like {0}counterpartCompanyCode ");
                            dbParameters.Add(db.CreateDbParameter("counterpartCompanyCode", "%" + counterpartCompanyCode + "%"));
                        }
                        if (!string.IsNullOrEmpty(counterpartCompanyName))
                        {
                            sql.Append("and bt.counterpartCompanyName like {0}counterpartCompanyName ");
                            dbParameters.Add(db.CreateDbParameter("counterpartCompanyName", "%" + counterpartCompanyName + "%"));
                        }

                        if (createTimeBegin.HasValue)
                        {
                            sql.Append(@" and bt.createTime >=To_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if (createTimeEnd.HasValue)
                        {
                            sql.Append(@" and bt.createTime <=To_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                        if (modifyTimeBegin.HasValue)
                        {
                            sql.Append(@" and bt.BoxUpFinishTime >=To_date({0}modifyTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = modifyTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("modifyTimeBegin", tempTime.ToString("G")));
                        }
                        if (modifyTimeEnd.HasValue)
                        {
                            sql.Append(@" and bt.BoxUpFinishTime <=To_date({0}modifyTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = modifyTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("modifyTimeEnd", tempTime.ToString("G")));
                        }
                    }

                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                   ErrorStrings.Export_Title_BoxUpTask_Code,ErrorStrings.Export_Title_PartsPurchaseOrder_OrderType, ErrorStrings.Export_Title_Partssalescategory_Name, ErrorStrings.Export_Title_Company_WarehouseCode,ErrorStrings.Export_Title_Company_WarehouseName, ErrorStrings.Export_Title_BoxUpTask_CounterpartCompanyCode,ErrorStrings.Export_Title_BoxUpTask_CounterpartCompanyName,ErrorStrings.Export_Title_AccountPeriod_Status, ErrorStrings.Export_Title_BoxUpTask_IsExistShippingOrder, ErrorStrings.Export_Title_AccountPeriod_CreatorName, ErrorStrings.Export_Title_AccountPeriod_CreateTime, ErrorStrings.Export_Title_AccountPeriod_ModifierName, ErrorStrings.Export_Title_AccountPeriod_ModifyTime,
                                   ErrorStrings.Export_Title_BoxUpTask_BoxUpFinishTime,ErrorStrings.Export_Title_PartsBranch_Remark, ErrorStrings.Export_Title_BoxUpTask_PartsOutboundPlanCode,ErrorStrings.Export_Title_BoxUpTask_PickingTaskCode,ErrorStrings.Export_Title_PartsOuterPurchaseChange_SourceCode,ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode,ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName,ErrorStrings.Export_Title_Accessinfo_Tmp_ReferenceCode,ErrorStrings.Export_Title_PartsPurchasePlanDetail_PlanAmount,ErrorStrings.Export_Title_BoxUpTask_BoxUpQty, ErrorStrings.Export_Title_BoxUpTask_ContainerNumber};
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();

                }
                return true;
            } catch(Exception) {
                return false;
            }
        }

    }
}
