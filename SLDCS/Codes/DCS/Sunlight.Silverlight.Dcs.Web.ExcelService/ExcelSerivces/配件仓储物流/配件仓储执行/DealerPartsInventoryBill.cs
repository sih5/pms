﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Globalization;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        /// <summary>
        /// 合并导出经销商配件盘点主清单   zs
        /// </summary>
        public bool ExportDealerPartsInventoryBill(int[] ids, int? branchId, int? storageCompanyId, string code, string storageCompanyCode, string storageCompanyName, int? salesCategoryId, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName) {
            fileName = GetExportFilePath("服务站配件盘点单_" + ".xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {
                    var sql = new StringBuilder();
                    sql.Append(@"Select a.Code,
                                        c.name,
                                        k.value as status,
                                        d.name,
                                        a.Storagecompanycode,
                                        a.Storagecompanyname,
                                        a.cancelreason,
                                        a.creatorname,
                                        a.createtime,
                                        a.approvername,
                                        a.approvetime,
                                        sum(b.dealerprice * b.currentstorage) as beforeAmount,
                                       sum(b.dealerprice * b.StorageAfterInventory) as afterAmount,
                                       sum(b.dealerprice * b.storagedifference) as differenceAmount,
                                        b.Sparepartcode,
                                        b.Sparepartname,
                                        b.dealerprice,
                                        --(Select c.salesprice From partssalesprice c Where c.sparepartid=b.sparepartid And c.partssalescategoryid=a.salescategoryid And c.status=1 and PriceType=1),
                                        b.Currentstorage,
                                        b.Storageafterinventory,
                                        b.Storagedifference,
                                        b.Memo
                                From Dealerpartsinventorybill a
                                left join keyvalueitem k
                                on a.status=k.key and k.name='DealerPartsInventoryBill_Status'
                                left join branch c
                                on a.branchid=c.id
                                left join partssalescategory d
                                on a.salescategoryid=d.id
                                Inner Join Dealerpartsinventorydetail b
                                On b.Dealerpartsinventoryid = a.Id where 1=1 ");
                    if(branchId.HasValue) {
                        sql.AppendFormat(" and a.branchId={0} ", branchId);
                    }
                    if(storageCompanyId.HasValue) {
                        sql.AppendFormat(" and a.StorageCompanyId={0} ", storageCompanyId);
                    }
                    var dbParameters = new List<DbParameter>();
                    #region 条件过滤
                    if(ids != null && ids.Length > 0) {
                        sql.Append(" and a.id in (");
                        for(var i = 0; i < ids.Length; i++) {
                            if(ids.Length == i + 1) {
                                sql.Append("{0}" + ids[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter(ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            } else {
                                sql.Append("{0}" + ids[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter(ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                        }
                        sql.Append(")");
                    } else {
                        if(!string.IsNullOrEmpty(code)) {
                            sql.Append(" and a.code like {0}code ");
                            dbParameters.Add(db.CreateDbParameter("code", "%" + code + "%"));
                        }
                        if(!string.IsNullOrEmpty(storageCompanyCode)) {
                            sql.Append(" and a.storageCompanyCode like {0}storageCompanyCode ");
                            dbParameters.Add(db.CreateDbParameter("storageCompanyCode", "%" + storageCompanyCode + "%"));
                        }
                        if(!string.IsNullOrEmpty(storageCompanyName)) {
                            sql.Append(" and a.storageCompanyName like {0}storageCompanyName ");
                            dbParameters.Add(db.CreateDbParameter("storageCompanyName", "%" + storageCompanyName + "%"));
                        }
                        if(salesCategoryId.HasValue) {
                            sql.Append(" and a.salesCategoryId={0}salesCategoryId");
                            dbParameters.Add(db.CreateDbParameter("salesCategoryId", salesCategoryId.Value));
                        }
                        if(status.HasValue) {
                            sql.Append(" and a.status={0}status");
                            dbParameters.Add(db.CreateDbParameter("status", status.Value));
                        }
                        if(createTimeBegin.HasValue) {
                            sql.Append(@" and a.createTime >=To_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(@" and a.createTime <=To_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                    }
                    sql.Append(" group by a.Code,c.name,k.value ,d.name,a.Storagecompanycode,a.Storagecompanyname,a.cancelreason,a.creatorname,a.createtime,a.approvername,a.approvetime, b.Sparepartcode,b.Sparepartname,b.Currentstorage,b.Storageafterinventory,b.Storagedifference, b.Memo, b.dealerprice");
                    #endregion
                    var sqlStr = sql.ToString();
                    if(sqlStr.IndexOf(" where 1=1  and ") > 0)
                        sqlStr = sqlStr.Replace(" where 1=1  and ", " where ");
                    else
                        sqlStr = sqlStr.Replace(" where 1=1 ", "");

                    var command = db.CreateDbCommand(string.Format(sqlStr, db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    conn.Open();
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                ErrorStrings.Export_Title_Dealerpartsinventorybill_Code,ErrorStrings.Export_Title_Branch_BranchName,ErrorStrings.Export_Title_AccountPeriod_Status,ErrorStrings.Export_Title_Partssalescategory_Name,ErrorStrings.Export_Title_Dealer_Name,ErrorStrings.Export_Title_Dealer_Code,ErrorStrings.Export_Title_PartsOuterPurchaseChange_AbandonComment,ErrorStrings.Export_Title_AccountPeriod_CreatorName,ErrorStrings.Export_Title_AccountPeriod_CreateTime,
                                ErrorStrings.Export_Title_Dealerpartsinventorybill_ApproverName,ErrorStrings.Export_Title_Dealerpartsinventorybill_ApproverTime,ErrorStrings.Export_Title_Dealerpartsinventorybill_BeforeAmount,ErrorStrings.Export_Title_Dealerpartsinventorybill_AfterAmount,ErrorStrings.Export_Title_Dealerpartsinventorybill_DifferenceAmount,
                                ErrorStrings.Export_Title_PartsBranch_Code,ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName,ErrorStrings.Export_Title_Dealerpartsinventorybill_Dealerprice,
                                ErrorStrings.Export_Title_Dealerpartsinventorybill_Currentstorage,ErrorStrings.Export_Title_Dealerpartsinventorybill_Storageafterinventory,ErrorStrings.Export_Title_Dealerpartsinventorybill_Storagedifference,ErrorStrings.Export_Title_PartsBranch_Remark
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }
        public bool 导出经销商配件盘点单主单清单(int[] ids, int? branchId, string code, string storageCompanyCode, string storageCompanyName, int? salesCategoryId, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd,int? amountDifferenceRange,string agencyName,out string fileName) {
            fileName = GetExportFilePath("服务站配件盘点单_" + ".xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);
                    var userInfo = Utils.GetCurrentUserInfo();
                using(var conn = db.CreateDbConnection()) {
                    var finalSql = "";
                    var sql = new StringBuilder();
                    sql.Append(string.Format(@"Select a.Code,
                                        c.name,
                                        k.value as status,
                                        d.name as name1,
                                        a.AgencyName,
                                        a.Storagecompanyname,
                                        a.Storagecompanycode,
                                        a.cancelreason,
                                        a.creatorname,
                                        a.createtime,a.InitialApproverName,a.InitialApproveTime,
                                        a.approvername,
                                        a.approvetime,a.CheckerName,a.CheckTime,a.UpperCheckerName,a.UpperCheckTime,
                                        a.seniorname,a.seniortime,
                                        sum(b.dealerprice * b.currentstorage) as beforeAmount,
                                        sum(b.dealerprice * b.StorageAfterInventory) as afterAmount,
                                        sum(b.dealerprice * b.storagedifference) as differenceAmount,
                                        b.Sparepartcode,
                                        b.Sparepartname,
                                        --(Select c.salesprice From partssalesprice c Where c.sparepartid=b.sparepartid And c.partssalescategoryid=a.salescategoryid And c.status=1 and PriceType=1),
                                        b.dealerprice,
                                        b.Currentstorage,
                                        b.Storageafterinventory,
                                        b.Storagedifference,
                                        b.Memo 
                                From Dealerpartsinventorybill a
                                left join keyvalueitem k
                                on a.status=k.key and k.name='DealerPartsInventoryBill_Status'
                                left join branch c
                                on a.branchid=c.id
                                left join partssalescategory d
                                on a.salescategoryid=d.id
                                Inner Join Dealerpartsinventorydetail b
                                On b.Dealerpartsinventoryid = a.Id where 1=1  and (exists (
 select 1 from  DealerServiceInfo e
		INNER JOIN MarketDptPersonnelRelation f ON e.MarketingDepartmentId = f.MarketDepartmentId and f.personnelid = {0} where f.status<>99 and a.StorageCompanyId = e.DealerId AND a.SalesCategoryId = e.PartsSalesCategoryId
 ) or 
  not exists (
    select id from MarketDptPersonnelRelation where personnelid = {1} and status <> 99
   )
 )
 ", userInfo.Id,userInfo.Id ));
                    if(branchId.HasValue) {
                        sql.AppendFormat(" and a.branchId={0} ", branchId);
                    }
                    var dbParameters = new List<DbParameter>();
                    #region 条件过滤
                    if(ids != null && ids.Length > 0) {
                        sql.Append(" and a.id in (");
                        for(var i = 0; i < ids.Length; i++) {
                            if(ids.Length == i + 1) {
                                sql.Append("{0}" + ids[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter(ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            } else {
                                sql.Append("{0}" + ids[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter(ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                        }
                        sql.Append(")");
                        finalSql = sql.ToString();
                    } else {
                       
                        if(!string.IsNullOrEmpty(code)) {
                            sql.Append(" and a.code like {0}code ");
                            dbParameters.Add(db.CreateDbParameter("code", "%" + code + "%"));
                        }
                        if(!string.IsNullOrEmpty(agencyName)) {
                            sql.Append(" and a.AgencyName like {0}AgencyName ");
                            dbParameters.Add(db.CreateDbParameter("AgencyName", agencyName + code + "%"));
                        }
                        if(!string.IsNullOrEmpty(storageCompanyCode)) {
                            sql.Append(" and a.storageCompanyCode like {0}storageCompanyCode ");
                            dbParameters.Add(db.CreateDbParameter("storageCompanyCode", "%" + storageCompanyCode + "%"));
                        }
                        if(!string.IsNullOrEmpty(storageCompanyName)) {
                            sql.Append(" and a.storageCompanyName like {0}storageCompanyName ");
                            dbParameters.Add(db.CreateDbParameter("storageCompanyName", "%" + storageCompanyName + "%"));
                        }
                        if(salesCategoryId.HasValue) {
                            sql.Append(" and a.salesCategoryId={0}salesCategoryId");
                            dbParameters.Add(db.CreateDbParameter("salesCategoryId", salesCategoryId.Value));
                        }
                        if(status.HasValue) {
                            sql.Append(" and a.status={0}status");
                            dbParameters.Add(db.CreateDbParameter("status", status.Value));
                        }
                        if(createTimeBegin.HasValue) {
                            sql.Append(@" and a.createTime >=To_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(@" and a.createTime <=To_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
						
                    }
                    sql.Append("  group by  a.Code,c.name,k.value,d.name,a.AgencyName,a.Storagecompanyname,a.Storagecompanycode,a.cancelreason,a.creatorname,a.createtime,a.InitialApproverName,a.InitialApproveTime,a.approvername, a.approvetime, a.CheckerName, a.CheckTime, a.UpperCheckerName,a.UpperCheckTime, a.seniorname, a.seniortime,b.Sparepartcode, b.Sparepartname, b.dealerprice,b.Currentstorage,b.Storageafterinventory,b.Storagedifference,b.Memo");
                    if(amountDifferenceRange.HasValue) {
                        finalSql = "select * from (" + sql.ToString() + " )ff where exists(select 1 from MultiLevelApproveConfig mu where mu.id=" + amountDifferenceRange.Value + " and  ff.differenceAmount>=mu.MinApproveFee and ff.differenceAmount<mu.MaxApproveFee)";
                    } else {
                        finalSql = sql.ToString();
                    }
                    
                    #endregion
                    var sqlStr = finalSql.ToString();
                    if(sqlStr.IndexOf(" where 1=1  and ") > 0)
                        sqlStr = sqlStr.Replace(" where 1=1  and ", " where ");
                    else
                        sqlStr = sqlStr.Replace(" where 1=1 ", "");

                    var command = db.CreateDbCommand(string.Format(sqlStr, db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    conn.Open();
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                ErrorStrings.Export_Title_Dealerpartsinventorybill_Code,ErrorStrings.Export_Title_Branch_BranchName,ErrorStrings.Export_Title_AccountPeriod_Status,ErrorStrings.Export_Title_Partssalescategory_Name,"中心库名称",ErrorStrings.Export_Title_Dealer_Name,ErrorStrings.Export_Title_Dealer_Code,ErrorStrings.Export_Title_PartsOuterPurchaseChange_AbandonComment,ErrorStrings.Export_Title_AccountPeriod_CreatorName,ErrorStrings.Export_Title_AccountPeriod_CreateTime,
                                ErrorStrings.Export_Title_PartsPurReturnOrder_InitialApproverName,ErrorStrings.Export_Title_PartsPurReturnOrder_InitialApproveTime,
                                ErrorStrings.Export_Title_Dealerpartsinventorybill_ApproverName,ErrorStrings.Export_Title_Dealerpartsinventorybill_ApproverTime,
                                ErrorStrings.Export_Title_PackingPropertyApp_ApproverName,ErrorStrings.Export_Title_PackingPropertyApp_ApproveTime,
                                ErrorStrings.Export_Title_PackingPropertyApp_UpperCheckerName,ErrorStrings.Export_Title_PackingPropertyApp_UpperCheckTime,
                                ErrorStrings.Export_Title_PackingPropertyApp_SeniorName,ErrorStrings.Export_Title_PackingPropertyApp_SeniorTime,
                                ErrorStrings.Export_Title_Dealerpartsinventorybill_BeforeAmount,ErrorStrings.Export_Title_Dealerpartsinventorybill_AfterAmount,ErrorStrings.Export_Title_Dealerpartsinventorybill_DifferenceAmount,
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }

                return true;
            } catch(Exception) {
                return false;
            }
        }
        public bool 导出服务站配件盘点提报(int[] ids, int? branchId, int? storageCompanyId, string code, string storageCompanyCode, string storageCompanyName, int? salesCategoryId, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName) {
            fileName = GetExportFilePath("服务站配件盘点提报单_" + ".xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {
                    var sql = new StringBuilder();
                    sql.Append(@"Select a.Code,
                                        c.name,
                                        k.value as status,
                                        d.name,
                                        a.Storagecompanyname,
                                        a.Storagecompanycode,
                                        a.cancelreason,
                                        a.creatorname,
                                        a.createtime,
                                        a.approvername,
                                        a.approvetime,
                                        sum(b.dealerprice * b.currentstorage) as beforeAmount,
                                        sum(b.dealerprice * b.StorageAfterInventory) as afterAmount,
                                        sum(b.dealerprice * b.storagedifference) as differenceAmount
                                From Dealerpartsinventorybill a
                                Inner Join Dealerpartsinventorydetail b On b.Dealerpartsinventoryid = a.Id
                                left join keyvalueitem k
                                on a.status=k.key and k.name='DealerPartsInventoryBill_Status'
                                left join branch c
                                on a.branchid=c.id
                                left join partssalescategory d
                                on a.salescategoryid=d.id where 1=1 ");
                    if(branchId.HasValue) {
                        sql.AppendFormat(" and a.branchId={0} ", branchId);
                    }
                    if(storageCompanyId.HasValue) {
                        sql.AppendFormat(" and a.StorageCompanyId={0} ", storageCompanyId);
                    }
                    var dbParameters = new List<DbParameter>();
                    #region 条件过滤
                    if(ids != null && ids.Length > 0) {
                        sql.Append(" and a.id in (");
                        for(var i = 0; i < ids.Length; i++) {
                            if(ids.Length == i + 1) {
                                sql.Append("{0}" + ids[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter(ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            } else {
                                sql.Append("{0}" + ids[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter(ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                        }
                        sql.Append(")");
                    } else {
                        if(!string.IsNullOrEmpty(code)) {
                            sql.Append(" and a.code like {0}code ");
                            dbParameters.Add(db.CreateDbParameter("code", "%" + code + "%"));
                        }
                        if(!string.IsNullOrEmpty(storageCompanyCode)) {
                            sql.Append(" and a.storageCompanyCode like {0}storageCompanyCode ");
                            dbParameters.Add(db.CreateDbParameter("storageCompanyCode", "%" + storageCompanyCode + "%"));
                        }
                        if(!string.IsNullOrEmpty(storageCompanyName)) {
                            sql.Append(" and a.storageCompanyName like {0}storageCompanyName ");
                            dbParameters.Add(db.CreateDbParameter("storageCompanyName", "%" + storageCompanyName + "%"));
                        }
                        if(salesCategoryId.HasValue) {
                            sql.Append(" and a.salesCategoryId={0}salesCategoryId");
                            dbParameters.Add(db.CreateDbParameter("salesCategoryId", salesCategoryId.Value));
                        }
                        if(status.HasValue) {
                            sql.Append(" and a.status={0}status");
                            dbParameters.Add(db.CreateDbParameter("status", status.Value));
                        }
                        if(createTimeBegin.HasValue) {
                            sql.Append(@" and a.createTime >=To_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(@" and a.createTime <=To_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                    }
                    sql.Append(" group by a.Code,c.name,k.value,d.name, a.Storagecompanyname, a.Storagecompanycode,a.cancelreason,a.creatorname,a.createtime,a.approvername,a.approvetime");
                    #endregion

                    var sqlStr = sql.ToString();
                    if(sqlStr.IndexOf(" where 1=1  and ") > 0)
                        sqlStr = sqlStr.Replace(" where 1=1  and ", " where ");
                    else
                        sqlStr = sqlStr.Replace(" where 1=1 ", "");

                    var command = db.CreateDbCommand(string.Format(sqlStr, db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    conn.Open();
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                ErrorStrings.Export_Title_Dealerpartsinventorybill_Code,ErrorStrings.Export_Title_Branch_BranchName,ErrorStrings.Export_Title_AccountPeriod_Status,ErrorStrings.Export_Title_Partssalescategory_Name,ErrorStrings.Export_Title_Dealer_Name,ErrorStrings.Export_Title_Dealer_Code,ErrorStrings.Export_Title_PartsOuterPurchaseChange_AbandonComment,ErrorStrings.Export_Title_AccountPeriod_CreatorName,ErrorStrings.Export_Title_AccountPeriod_CreateTime,ErrorStrings.Export_Title_Dealerpartsinventorybill_ApproverName,ErrorStrings.Export_Title_Dealerpartsinventorybill_ApproverTime,ErrorStrings.Export_Title_Dealerpartsinventorybill_BeforeAmount,ErrorStrings.Export_Title_Dealerpartsinventorybill_AfterAmount,ErrorStrings.Export_Title_Dealerpartsinventorybill_DifferenceAmount
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }

                return true;
            } catch(Exception) {
                return false;
            }
        }
    }
}
