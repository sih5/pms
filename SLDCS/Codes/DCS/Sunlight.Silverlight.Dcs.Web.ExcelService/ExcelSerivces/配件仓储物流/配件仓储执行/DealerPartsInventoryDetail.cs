﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        public bool ImportDealerPartsInventoryBillForReport(int dealerId, int salesCategoryId, string fileName, out int excelImportNum, out List<DealerPartsInventoryDetail> rightData, out List<DealerPartsInventoryDetail> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorDealerPartsInventoryDetails = new List<DealerPartsInventoryDetail>();
            var rightDealerPartsInventoryDetails = new List<DealerPartsInventoryDetail>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);
                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("DealerPartsInventoryDetail", out notNullableFields, out fieldLenght);
                var dealerPartsInventoryDetails = new List<DealerPartsInventoryDetail>();
                List<object> excelColumns;
                //Excel数据导入
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    //指定Excel列名与字段名关系
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Code, "SparePartCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName, "SparePartName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealerpartsinventorybill_Storageafterinventory, "StorageAfterInventory");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Remark, "Memo");

                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();

                    //Excel单行导入
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        //接收Excel导入数据
                        var tempImportObj = new DealerPartsInventoryDetail();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        tempImportObj.SparePartCodeStr = newRow["SparePartCode"];
                        tempImportObj.SparePartNameStr = newRow["SparePartName"];
                        tempImportObj.StorageAfterInventoryStr = newRow["StorageAfterInventory"];
                        tempImportObj.Memo = newRow["Memo"];

                        var errorMsgs = new List<string>();
                        //配件图号检查
                        var fieldIndex = notNullableFields.IndexOf("SparePartCode".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.SparePartCodeStr)) {
                            if(fieldIndex > -1)
                                errorMsgs.Add(ErrorStrings.ImpPartsPurchasePricingDetail_Validation1);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.SparePartCodeStr) > fieldLenght["SparePartCode".ToUpper()])
                                errorMsgs.Add(ErrorStrings.ImpPartsPurchasePricingDetail_Validation2);
                        }
                        //盘点后库存
                        //fieldIndex = notNullableFields.IndexOf("StorageAfterInventory".ToUpper());
                        if(string.IsNullOrWhiteSpace(tempImportObj.StorageAfterInventoryStr)) {
                           // if(fieldIndex > -1)
                                errorMsgs.Add(ErrorStrings.Export_Validation_Dealerpartsinventorybill_StorageAfterInventoryNull);
                        } else {
                            int storageAfterInventory;
                            if(int.TryParse(tempImportObj.StorageAfterInventoryStr, out storageAfterInventory)) {
                                tempImportObj.StorageAfterInventory = storageAfterInventory;
                            } else {
                                errorMsgs.Add(ErrorStrings.Export_Validation_Dealerpartsinventorybill_StorageAfterInventoryInteger);
                            }
                        }
                        if(errorMsgs.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", errorMsgs);
                        }
                        dealerPartsInventoryDetails.Add(tempImportObj);
                        return false;
                    });

                    var tempRightList = dealerPartsInventoryDetails.Where(r => r.ErrorMsg == null).ToList();
                    var groups = tempRightList.GroupBy(r => new {
                        r.SparePartCodeStr
                    }).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        groupItem.ErrorMsg = ErrorStrings.Export_Validation_Dealerpartsinventorybill_SparepartCodeReport;
                    }

                    #region 校验编号正确 并回填配件Id，配件名称
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();
                    var sparePartCodes = tempRightList.Where(r => r.ErrorMsg == null).Select(r => r.SparePartCodeStr).Distinct().ToArray();
                    var spareParts = new List<DealerPartsInventoryDetail>();
                    Func<string[], bool> dealSparePart = values => {
                        spareParts.Add(new DealerPartsInventoryDetail {
                            SparePartId = int.Parse(values[0]),
                            SparePartCode = values[1],
                            SparePartName = values[2]
                        });
                        return false;
                    };
                    var sql = string.Format("select Id, Code,Name from SparePart Where Status = {0} ", (int)DcsMasterDataStatus.有效);
                    var partsSalesCategoryName = new List<string>();
                    Func<string[], bool> partsSalesCategoryNames = values => {
                        partsSalesCategoryName.Add(values[0]);
                        return false;
                    };
                    var sql3 = string.Format("select Name from PartsSalesCategory Where Status = {0} and Id={1}", (int)DcsMasterDataStatus.有效, salesCategoryId);
                    db.QueryDataWithInOperator(sql3, "Id", false, new[] { salesCategoryId.ToString() }, partsSalesCategoryNames);
                    db.QueryDataWithInOperator(sql, "Code", true, sparePartCodes, dealSparePart);
                    var partsBranches = new List<PartsBranch>();
                    Func<string[], bool> partsBranch = values => {
                        partsBranches.Add(new PartsBranch {
                            PartId = int.Parse(values[0]),
                            PartCode = values[1],
                            PartName = values[2],
                            PartsSalesCategoryId = int.Parse(values[3]),
                            PartsSalesCategoryName = values[4]
                        });
                        return false;
                    };
                    var partIds = spareParts.Select(r => r.SparePartId.ToString()).ToArray();
                    var sql2 = string.Format("select PartId, PartCode,PartName,PartsSalesCategoryId,PartsSalesCategoryName from PartsBranch Where Status = {0}", (int)DcsMasterDataStatus.有效);
                    db.QueryDataWithInOperator(sql2, "PartId", false, partIds, partsBranch);
                    foreach(var item in tempRightList) {
                        var temp = spareParts.FirstOrDefault(r => r.SparePartCode == item.SparePartCodeStr);
                        if(temp == null) {
                            item.ErrorMsg = ErrorStrings.Export_Validation_Dealerpartsinventorybill_Validarion1;
                            continue;
                        }
                        var partsbranch = partsBranches.FirstOrDefault(r => r.PartId == temp.SparePartId && r.PartsSalesCategoryId == salesCategoryId);
                        if(partsbranch == null) {
                            item.ErrorMsg = string.Format(ErrorStrings.Export_Validation_Dealerpartsinventorybill_Validarion2, partsSalesCategoryName.First());
                            continue;
                        }
                        item.SparePartId = temp.SparePartId;
                        item.SparePartCode = temp.SparePartCode;
                        item.SparePartName = temp.SparePartName;
                    }

                    var partsSalesPrices = new List<PartsSalesPrice>();
                    Func<string[], bool> partsSalesPrice = values => {
                        partsSalesPrices.Add(new PartsSalesPrice {
                            SparePartId = int.Parse(values[0]),
                            SparePartCode = values[1],
                            BranchId = int.Parse(values[2]),
                            SalesPrice = decimal.Parse(values[3]),
                            PartsSalesCategoryId = int.Parse(values[4])
                        });
                        return false;
                    };
                    var sql4 = string.Format("select SparePartId,SparePartCode,BranchId,SalesPrice,PartsSalesCategoryId from PartsSalesPrice  Where Status = {0}", (int)DcsMasterDataStatus.有效);
                    db.QueryDataWithInOperator(sql4, "SparePartId", false, partIds, partsSalesPrice);
                    foreach(var item in tempRightList) {
                        var temp = spareParts.FirstOrDefault(r => r.SparePartCode == item.SparePartCodeStr);
                        if (temp == null)
                            continue;
                        var partsSalesPrice1 = partsSalesPrices.FirstOrDefault(r => r.SparePartId == temp.SparePartId && r.PartsSalesCategoryId == salesCategoryId);
                        if(partsSalesPrice1 == null) {
                            item.ErrorMsg = string.Format(ErrorStrings.Export_Validation_Dealerpartsinventorybill_Validarion3, partsSalesCategoryName.First());
                            continue;
                        }
                        item.DealerPrice = partsSalesPrice1.SalesPrice;
                    }

                    #endregion
                    #region 查询该品牌的经销商配件库存获取当前库存
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();
                    var dbDealerPartsStocks = tempRightList.Where(r => r.ErrorMsg == null).Select(r => r.SparePartId.ToString(CultureInfo.InvariantCulture)).Distinct().ToArray();
                    var dealerPartsStocks = new List<DealerPartsStockExtend>();
                    Func<string[], bool> getDealerPartsStock = values => {
                        dealerPartsStocks.Add(new DealerPartsStockExtend {
                            Id = int.Parse(values[0]),
                            SparePartId = int.Parse(values[1]),
                            Quantity = int.Parse(values[2])
                        });
                        return false;
                    };
                    var sql1 = string.Format("Select Id,SparePartId,Quantity From DealerPartsStock Where dealerId={0} and SalesCategoryId={1} ", dealerId, salesCategoryId);
                    db.QueryDataWithInOperator(sql1, "SparePartId", false, dbDealerPartsStocks, getDealerPartsStock);
                    foreach(var item in tempRightList) {
                        var temp = dealerPartsStocks.FirstOrDefault(r => r.SparePartId == item.SparePartId);
                        item.CurrentStorage = temp == null ? 0 : temp.Quantity;
                    }
                    #endregion
                }

                errorDealerPartsInventoryDetails = dealerPartsInventoryDetails.Where(r => r.ErrorMsg != null).ToList();
                //获取所有合格数据
                rightDealerPartsInventoryDetails = dealerPartsInventoryDetails.Except(errorDealerPartsInventoryDetails).ToList();
                //foreach(var rightItem in rightDealerPartsInventoryDetails) {
                //}

                //导出所有不合格数据
                if(errorDealerPartsInventoryDetails.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorDealerPartsInventoryDetails;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempObj = list[index - 1];
                            var values = new object[] {
                                #region 设置错误信息导出的列的值
                                tempObj.SparePartCodeStr, tempObj.SparePartNameStr, tempObj.StorageAfterInventoryStr, tempObj.MemoStr, tempObj.ErrorMsg
                                #endregion
                            };
                            return values;
                        });
                    }
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorDealerPartsInventoryDetails = null;
                return false;
            } finally {
                errorData = errorDealerPartsInventoryDetails;
                rightData = rightDealerPartsInventoryDetails;
            }
        }
    }
}
