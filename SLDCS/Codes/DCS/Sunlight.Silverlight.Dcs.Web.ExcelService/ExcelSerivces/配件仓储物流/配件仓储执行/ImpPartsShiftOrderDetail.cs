﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        /// <summary>
        /// 导入配件移库单清单
        /// </summary>
        public bool ImpPartsShiftOrderDetail(string fileName, int warehouseId, int OriginalWarehouseAreaCategory,int DestWarehouseAreaCategory, out int excelImportNum, out List<ImpPartsShiftOrderDetail> rightData, out List<ImpPartsShiftOrderDetail> errorData, out string errorDataFileName, out string errorMessage) {
            //初始化返回数据
            excelImportNum = 0;
            errorDataFileName = "";
            errorMessage = "";
            var errorList = new List<ImpPartsShiftOrderDetail>();
            var rightList = new List<ImpPartsShiftOrderDetail>();
            var allList = new List<ImpPartsShiftOrderDetail>();

            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var userInfo = Utils.GetCurrentUserInfo();
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("PartsShiftOrderDetail", out notNullableFields, out fieldLenght);

                List<object> excelColumns;

                //Excel数据导入
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    //指定Excel列名与字段名关系
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode, "SparePartCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsShiftOrder_OriginalWarehouseAreaCode, "OriginalWarehouseAreaCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsShiftOrder_DestWarehouseAreaCode, "DestWarehouseAreaCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsShiftOrder_Quantity, "ShiftQuantity");

                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();

                    //Excel单行导入
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        //接收Excel导入数据
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        var tempImportObj = new ImpPartsShiftOrderDetail {
                            SparePartCode = newRow["SparePartCode"],
                            OriginalWarehouseAreaCode = newRow["OriginalWarehouseAreaCode"],
                            DestWarehouseAreaCode = newRow["DestWarehouseAreaCode"],
                            ShiftQuantityStr = newRow["ShiftQuantity"],
                        };
                        var tempErrorMessage = new List<string>();
                        //配件编号检查
                        var fieldIndex = notNullableFields.IndexOf("SparePartCode".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.SparePartCode)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchasePricingDetail_Validation1);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.SparePartCode) > fieldLenght["SparePartCode".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchasePricingDetail_Validation2);
                        }
                        //原库位编号检查
                        fieldIndex = notNullableFields.IndexOf("OriginalWarehouseAreaCode".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.OriginalWarehouseAreaCode)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.ImpPartsShiftOrderDetail_Validation1);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.OriginalWarehouseAreaCode) > fieldLenght["OriginalWarehouseAreaCode".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.ImpPartsShiftOrderDetail_Validation2);
                        }
                        //目标库位编号检查
                        fieldIndex = notNullableFields.IndexOf("DestWarehouseAreaCode".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.DestWarehouseAreaCode)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.ImpPartsShiftOrderDetail_Validation3);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.DestWarehouseAreaCode) > fieldLenght["DestWarehouseAreaCode".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.ImpPartsShiftOrderDetail_Validation4);
                        }
                        
                        //闭口数量检查
                        fieldIndex = notNullableFields.IndexOf("Quantity".ToUpper());
                        if(newRow["ShiftQuantity"] == null) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.ImpPartsShiftOrderDetail_Validation5);
                        } else {
                            int shiftQuantity;
                            if(int.TryParse(newRow["ShiftQuantity"], out shiftQuantity)) {
                                if(shiftQuantity > 0)
                                    tempImportObj.ShiftQuantity = shiftQuantity;
                                else
                                    tempImportObj.ShiftQuantity = 0;
                            } else {
                                tempErrorMessage.Add(ErrorStrings.ImpPartsShiftOrderDetail_Validation6);
                            }
                        }

                        if(tempErrorMessage.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });

                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    //检查配件是否重复
                    var partCodeGroups = tempRightList.GroupBy(r => new { r.SparePartCode, r.OriginalWarehouseAreaCode });
                    foreach (var partCodeGroup in partCodeGroups) {
                        if (partCodeGroup.Count() > 1) {
                            foreach (var partCode in partCodeGroup) {
                                partCode.ErrorMsg = ErrorStrings.ImpPartsShiftOrderDetail_Validation7;
                            }
                        }
                    }

                    //获取配件信息表数据
                    var spareParts = new List<SparePart>();
                    Func<string[], bool> dealSparePart = values => {
                        spareParts.Add(new SparePart {
                            Id = int.Parse(values[0]),
                            Code = values[1],
                            Name = values[2],
                            MeasureUnit = values[3]
                        });
                        return false;
                    };

                    //获取库位信息
                    var warehouseAreas = new List<WarehouseExtend>();
                    Func<string[], bool> dealWarehouseArea = values => {
                        warehouseAreas.Add(new WarehouseExtend {
                            Id = int.Parse(values[0]),
                            Code = values[1],
                        });
                        return false;
                    };

                    //获取库位信息
                    var destWarehouseAreas = new List<WarehouseExtend>();
                    Func<string[], bool> dealDestWarehouseArea = values => {
                        destWarehouseAreas.Add(new WarehouseExtend {
                            Id = int.Parse(values[0]),
                            Code = values[1],
                        });
                        return false;

                    };
                    var partCodes = tempRightList.Select(v => v.SparePartCode.ToUpper()).Distinct().ToArray();
                    db.QueryDataWithInOperator(string.Format("select Id, Trim(Code), Trim(Name), MeasureUnit from SparePart where status={0}", (int)DcsMasterDataStatus.有效), "Code", true, partCodes, dealSparePart);

                    var originalWarehouseAreaCodes = tempRightList.Select(r => r.OriginalWarehouseAreaCode).ToArray();
                    db.QueryDataWithInOperator(string.Format("select a.id,a.code from warehousearea a inner join warehouseareacategory b on a.areacategoryid = b.id where a.status =1 and a.areakind=3 and b.category = {0} and a.warehouseid = {1}", OriginalWarehouseAreaCategory,warehouseId), "a.Code", true, originalWarehouseAreaCodes, dealWarehouseArea);

                    var destWarehouseAreaCodes = tempRightList.Select(r => r.DestWarehouseAreaCode).ToArray();
                    db.QueryDataWithInOperator(string.Format("select a.id,a.code from warehousearea a inner join warehouseareacategory b on a.areacategoryid = b.id where a.status =1 and a.areakind=3 and b.category = {0} and a.warehouseid = {1}", DestWarehouseAreaCategory,warehouseId), "a.Code", true, destWarehouseAreaCodes, dealDestWarehouseArea);

                    #region 循环
                    for(var i = tempRightList.Count - 1; i >= 0; i--) {
                        //配件有效性校验
                        var sparePart = spareParts.FirstOrDefault(v => String.Compare(v.Code, tempRightList[i].SparePartCode, StringComparison.OrdinalIgnoreCase) == 0);
                        if(sparePart == null) {
                            tempRightList[i].ErrorMsg = string.Format(ErrorStrings.ImpPartsPurchasePricingDetail_Validation17, tempRightList[i].SparePartCode);
                            continue;
                        }
                        tempRightList[i].SparePartId = sparePart.Id;
                        tempRightList[i].SparePartName = sparePart.Name;
                        //库位有效性校验
                        var oriWarehouseAreaCode = warehouseAreas.FirstOrDefault(r => r.Code.Equals(tempRightList[i].OriginalWarehouseAreaCode.ToString()));
                        if (oriWarehouseAreaCode == null) { 
                            tempRightList[i].ErrorMsg = string.Format(ErrorStrings.ImpPartsShiftOrderDetail_Validation8);
                            continue;
                        }
                        tempRightList[i].OriginalWarehouseAreaId = oriWarehouseAreaCode.Id;
                        var destWarehouseAreaCode = destWarehouseAreas.FirstOrDefault(r => r.Code.Equals(tempRightList[i].DestWarehouseAreaCode.ToString()));
                        if (destWarehouseAreaCode == null) { 
                            tempRightList[i].ErrorMsg = string.Format(ErrorStrings.ImpPartsShiftOrderDetail_Validation9);
                            continue;
                        }
                        tempRightList[i].DestWarehouseAreaId = destWarehouseAreaCode.Id;

                    }

                    #endregion
                }

                errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                rightList = allList.Except(errorList).ToList();
                if(rightList.Any()) {
                    string sql = @"SELECT 
top.Id,
top.StorageCompanyId,
top.StorageCompanyType,
top.BranchId,
top.Id1 as partid,
top.Code3 AS sparepartcode,
top.WarehouseAreaId,
top.Code AS WarehouseAreaCode,
top.Quantity,
top.C1 as LockedQuantity
FROM ( SELECT 
  Project2.Id,
  Project2.WarehouseId,
  Project2.StorageCompanyId,
  Project2.StorageCompanyType,
  Project2.BranchId,
  Project2.WarehouseAreaId,
  Project2.Quantity,
  Project2.Code,
  Project2.Code1,
  Project2.Code2,
  Project2.Name,
  Project2.Id1,
  Project2.Code3,
  Project2.Name1,
  Project2.MeasureUnit,
  Project2.Category,
  Project2.Id2,
  Project2.Name2,
  Project2.C1,
  Project2.C2,
  Project2.C3,
  Project2.C4,
  Project2.C5,
  Project2.PriceType
  FROM ( SELECT 
    Filter3.Id3 AS Id,
    Filter3.WarehouseId1 AS WarehouseId,
    Filter3.StorageCompanyId1 AS StorageCompanyId,
    Filter3.StorageCompanyType1 AS StorageCompanyType,
    Filter3.BranchId1 AS BranchId,
    Filter3.WarehouseAreaId,
    Filter3.Quantity,
    Filter3.Code4 AS Code,
    Filter3.Code5 AS Code1,
    Filter3.Code6 AS Code2,
    Filter3.Name3 AS Name,
    Filter3.Id4 AS Id1,
    Filter3.Code7 AS Code3,
    Filter3.Name4 AS Name1,
    Filter3.MeasureUnit,
    Filter3.Category,
    Filter3.Id5 AS Id2,
    Filter3.Name5 AS Name2,
    CASE WHEN Filter3.LockedQty IS NULL THEN 0 ELSE Filter3.LockedQty END AS C1,
    CASE WHEN Extent10.Quantity IS NULL THEN Filter3.Quantity ELSE Extent10.Quantity END AS C2,
    CASE WHEN Extent10.BatchNumber IS NULL THEN '' ELSE Extent10.BatchNumber END AS C3,
    Extent10.InboundTime AS C4,
    CASE WHEN Extent11.Id IS NULL THEN 0 ELSE Extent11.SalesPrice END AS C5,
    Extent11.PriceType
    FROM    (SELECT Filter2.Id3, Filter2.WarehouseId1, Filter2.StorageCompanyId1, Filter2.StorageCompanyType1, Filter2.BranchId1, Filter2.WarehouseAreaId, Filter2.WarehouseAreaCategoryId, Filter2.PartId, Filter2.Quantity, Filter2.Remark1, Filter2.CreatorId1, Filter2.CreatorName1, Filter2.CreateTime1, Filter2.ModifierId1, Filter2.ModifierName1, Filter2.ModifyTime1, Filter2.RowVersion1, Filter2.LockedQty, Filter2.OutingQty, Filter2.Id6, Filter2.SalesUnitId, Filter2.WarehouseId2, Filter2.Id7, Filter2.Code8, Filter2.Name6, Filter2.BusinessCode, Filter2.BusinessName, Filter2.BranchId2, Filter2.OwnerCompanyId, Filter2.AccountGroupId, Filter2.PartsSalesCategoryId, Filter2.Status1, Filter2.Remark2, Filter2.RowVersion2, Filter2.SubmitCompanyId, Filter2.Id8, Filter2.WarehouseId3, Filter2.ParentId1, Filter2.Code4, Filter2.TopLevelWarehouseAreaId1, Filter2.AreaCategoryId1, Filter2.Status2, Filter2.Remark3, Filter2.AreaKind1, Filter2.CreatorId2, Filter2.CreatorName2, Filter2.CreateTime2, Filter2.ModifierId2, Filter2.ModifierName2, Filter2.ModifyTime2, Filter2.RowVersion3, Filter2.Id9, Filter2.WarehouseId4, Filter2.ParentId2, Filter2.Code5, Filter2.TopLevelWarehouseAreaId2, Filter2.AreaCategoryId2, Filter2.Status3, Filter2.Remark4, Filter2.AreaKind2, Filter2.CreatorId3, Filter2.CreatorName3, Filter2.CreateTime3, Filter2.ModifierId3, Filter2.ModifierName3, Filter2.ModifyTime3, Filter2.RowVersion4, Filter2.Id10, Filter2.Code6, Filter2.Name3, Filter2.Type, Filter2.Status4, Filter2.Address, Filter2.RegionId, Filter2.PhoneNumber, Filter2.Contact, Filter2.Fax, Filter2.Email, Filter2.StorageStrategy, Filter2.Remark5, Filter2.Picture, Filter2.BranchId3, Filter2.StorageCompanyId2, Filter2.StorageCompanyType2, Filter2.StorageCenter, Filter2.WmsInterface, Filter2.CreatorId4, Filter2.CreatorName4, Filter2.CreateTime4, Filter2.ModifierId4, Filter2.ModifierName4, Filter2.ModifyTime4, Filter2.RowVersion5, Filter2.IsCentralizedPurchase, Filter2.IsQualityWarehouse, Filter2.IfSync, Filter2.IsEcommerceWarehouse, Filter2.PwmsInterface, 
      Extent7.Id AS Id4,
      Extent7.Code AS Code7,
      Extent7.Name AS Name4,
      Extent7.LastSubstitute,
      Extent7.NextSubstitute,
      Extent7.ShelfLife,
      Extent7.EnglishName,
      Extent7.PinyinCode,
      Extent7.ReferenceCode,
      Extent7.ReferenceName,
      Extent7.CADCode,
      Extent7.CADName,
      Extent7.PartType,
      Extent7.Specification,
      Extent7.Feature,
      Extent7.Status AS Status5,
      Extent7.Length,
      Extent7.Width,
      Extent7.Height,
      Extent7.Volume,
      Extent7.Weight,
      Extent7.Material,
      Extent7.PackingAmount,
      Extent7.PackingSpecification,
      Extent7.PartsOutPackingCode,
      Extent7.PartsInPackingCode,
      Extent7.MeasureUnit,
      Extent7.CreatorId AS CreatorId5,
      Extent7.CreatorName AS CreatorName5,
      Extent7.CreateTime AS CreateTime5,
      Extent7.ModifierId AS ModifierId5,
      Extent7.ModifierName AS ModifierName5,
      Extent7.ModifyTime AS ModifyTime5,
      Extent7.AbandonerId AS AbandonerId1,
      Extent7.AbandonerName AS AbandonerName1,
      Extent7.AbandonTime AS AbandonTime1,
      Extent7.RowVersion AS RowVersion6,
      Extent7.MInPackingAmount,
      Extent7.GroupABCCategory,
      Extent7.IMSCompressionNumber,
      Extent7.IMSManufacturerNumber,
      Extent7.ProductBrand,
      Extent7.SubstandardName,
      Extent7.TotalNumber,
      Extent7.Factury,
      Extent7.IsOriginal,
      Extent7.CategoryCode,
      Extent7.OverseasPartsFigure,
      Extent7.CategoryName,
      Extent7.ExchangeIdentification,
      Extent7.GoldenTaxClassifyId,
      Extent7.GoldenTaxClassifyCode,
      Extent7.GoldenTaxClassifyName,
      Extent7.ExpandLanguageNAME1,
      Extent7.ExpandLanguageCode1,
      Extent7.ExpandLanguageCode13,
      Extent7.ExpandLanguageNAME13,
      Extent7.ExpandLanguageCode12,
      Extent7.ExpandLanguageNAME12,
      Extent7.ExpandLanguageCode11,
      Extent7.ExpandLanguageNAME11,
      Extent7.ExpandLanguageCode10,
      Extent7.ExpandLanguageNAME10,
      Extent7.ExpandLanguageCode9,
      Extent7.ExpandLanguageNAME9,
      Extent7.ExpandLanguageCode8,
      Extent7.ExpandLanguageNAME8,
      Extent7.ExpandLanguageCode7,
      Extent7.ExpandLanguageNAME7,
      Extent7.ExpandLanguageCode6,
      Extent7.ExpandLanguageNAME6,
      Extent7.ExpandLanguageCode5,
      Extent7.ExpandLanguageNAME5,
      Extent7.ExpandLanguageCode4,
      Extent7.ExpandLanguageNAME4,
      Extent7.ExpandLanguageCode3,
      Extent7.ExpandLanguageNAME3,
      Extent7.ExpandLanguageCode2,
      Extent7.ExpandLanguageNAME2,
      Extent7.StandardCode,
      Extent7.StandardName,
      Extent7.IsNotWarrantyTransfer,
      Extent7.OMSparePartMark,
      Extent7.DeclareElement,
      Extent7.Path,
      Extent8.Id AS Id11,
      Extent8.Category,
      Extent9.Id AS Id5,
      Extent9.Code AS Code9,
      Extent9.Name AS Name5,
      Extent9.BranchId AS BranchId4,
      Extent9.BranchCode,
      Extent9.BranchName,
      Extent9.IsForClaim,
      Extent9.IsNotWarranty,
      Extent9.Status AS Status6,
      Extent9.Remark AS Remark6,
      Extent9.CreatorId AS CreatorId6,
      Extent9.CreatorName AS CreatorName6,
      Extent9.CreateTime AS CreateTime6,
      Extent9.ModifierId AS ModifierId6,
      Extent9.ModifierName AS ModifierName6,
      Extent9.ModifyTime AS ModifyTime6,
      Extent9.AbandonerId AS AbandonerId2,
      Extent9.AbandonerName AS AbandonerName2,
      Extent9.AbandonTime AS AbandonTime2,
      Extent9.RowVersion AS RowVersion7,
      Extent9.IsOverseas
      FROM     (SELECT Filter1.Id3, Filter1.WarehouseId1, Filter1.StorageCompanyId1, Filter1.StorageCompanyType1, Filter1.BranchId1, Filter1.WarehouseAreaId, Filter1.WarehouseAreaCategoryId, Filter1.PartId, Filter1.Quantity, Filter1.Remark1, Filter1.CreatorId1, Filter1.CreatorName1, Filter1.CreateTime1, Filter1.ModifierId1, Filter1.ModifierName1, Filter1.ModifyTime1, Filter1.RowVersion1, Filter1.LockedQty, Filter1.OutingQty, Filter1.Id6, Filter1.SalesUnitId, Filter1.WarehouseId2, Filter1.Id7, Filter1.Code8, Filter1.Name6, Filter1.BusinessCode, Filter1.BusinessName, Filter1.BranchId2, Filter1.OwnerCompanyId, Filter1.AccountGroupId, Filter1.PartsSalesCategoryId, Filter1.Status1, Filter1.Remark2, Filter1.RowVersion2, Filter1.SubmitCompanyId, Filter1.Id8, Filter1.WarehouseId3, Filter1.ParentId1, Filter1.Code4, Filter1.TopLevelWarehouseAreaId1, Filter1.AreaCategoryId1, Filter1.Status2, Filter1.Remark3, Filter1.AreaKind1, Filter1.CreatorId2, Filter1.CreatorName2, Filter1.CreateTime2, Filter1.ModifierId2, Filter1.ModifierName2, Filter1.ModifyTime2, Filter1.RowVersion3, 
        Extent5.Id AS Id9,
        Extent5.WarehouseId AS WarehouseId4,
        Extent5.ParentId AS ParentId2,
        Extent5.Code AS Code5,
        Extent5.TopLevelWarehouseAreaId AS TopLevelWarehouseAreaId2,
        Extent5.AreaCategoryId AS AreaCategoryId2,
        Extent5.Status AS Status3,
        Extent5.Remark AS Remark4,
        Extent5.AreaKind AS AreaKind2,
        Extent5.CreatorId AS CreatorId3,
        Extent5.CreatorName AS CreatorName3,
        Extent5.CreateTime AS CreateTime3,
        Extent5.ModifierId AS ModifierId3,
        Extent5.ModifierName AS ModifierName3,
        Extent5.ModifyTime AS ModifyTime3,
        Extent5.RowVersion AS RowVersion4,
        Extent6.Id AS Id10,
        Extent6.Code AS Code6,
        Extent6.Name AS Name3,
        Extent6.Type,
        Extent6.Status AS Status4,
        Extent6.Address,
        Extent6.RegionId,
        Extent6.PhoneNumber,
        Extent6.Contact,
        Extent6.Fax,
        Extent6.Email,
        Extent6.StorageStrategy,
        Extent6.Remark AS Remark5,
        Extent6.Picture,
        Extent6.BranchId AS BranchId3,
        Extent6.StorageCompanyId AS StorageCompanyId2,
        Extent6.StorageCompanyType AS StorageCompanyType2,
        Extent6.StorageCenter,
        Extent6.WmsInterface,
        Extent6.CreatorId AS CreatorId4,
        Extent6.CreatorName AS CreatorName4,
        Extent6.CreateTime AS CreateTime4,
        Extent6.ModifierId AS ModifierId4,
        Extent6.ModifierName AS ModifierName4,
        Extent6.ModifyTime AS ModifyTime4,
        Extent6.RowVersion AS RowVersion5,
        Extent6.IsCentralizedPurchase,
        Extent6.IsQualityWarehouse,
        Extent6.IfSync,
        Extent6.IsEcommerceWarehouse,
        Extent6.PwmsInterface
        FROM    (SELECT 
          Extent1.Id AS Id3,
          Extent1.WarehouseId AS WarehouseId1,
          Extent1.StorageCompanyId AS StorageCompanyId1,
          Extent1.StorageCompanyType AS StorageCompanyType1,
          Extent1.BranchId AS BranchId1,
          Extent1.WarehouseAreaId,
          Extent1.WarehouseAreaCategoryId,
          Extent1.PartId,
          Extent1.Quantity,
          Extent1.Remark AS Remark1,
          Extent1.CreatorId AS CreatorId1,
          Extent1.CreatorName AS CreatorName1,
          Extent1.CreateTime AS CreateTime1,
          Extent1.ModifierId AS ModifierId1,
          Extent1.ModifierName AS ModifierName1,
          Extent1.ModifyTime AS ModifyTime1,
          Extent1.RowVersion AS RowVersion1,
          Extent1.LockedQty,
          Extent1.OutingQty,
          Extent2.Id AS Id6,
          Extent2.SalesUnitId,
          Extent2.WarehouseId AS WarehouseId2,
          Extent3.Id AS Id7,
          Extent3.Code AS Code8,
          Extent3.Name AS Name6,
          Extent3.BusinessCode,
          Extent3.BusinessName,
          Extent3.BranchId AS BranchId2,
          Extent3.OwnerCompanyId,
          Extent3.AccountGroupId,
          Extent3.PartsSalesCategoryId,
          Extent3.Status AS Status1,
          Extent3.Remark AS Remark2,
          Extent3.RowVersion AS RowVersion2,
          Extent3.SubmitCompanyId,
          Extent4.Id AS Id8,
          Extent4.WarehouseId AS WarehouseId3,
          Extent4.ParentId AS ParentId1,
          Extent4.Code AS Code4,
          Extent4.TopLevelWarehouseAreaId AS TopLevelWarehouseAreaId1,
          Extent4.AreaCategoryId AS AreaCategoryId1,
          Extent4.Status AS Status2,
          Extent4.Remark AS Remark3,
          Extent4.AreaKind AS AreaKind1,
          Extent4.CreatorId AS CreatorId2,
          Extent4.CreatorName AS CreatorName2,
          Extent4.CreateTime AS CreateTime2,
          Extent4.ModifierId AS ModifierId2,
          Extent4.ModifierName AS ModifierName2,
          Extent4.ModifyTime AS ModifyTime2,
          Extent4.RowVersion AS RowVersion3
          FROM    PartsStock Extent1
          INNER JOIN SalesUnitAffiWarehouse Extent2 ON Extent1.WarehouseId = Extent2.WarehouseId
          INNER JOIN SalesUnit Extent3 ON Extent2.SalesUnitId = Extent3.Id
          INNER JOIN WarehouseArea Extent4 ON Extent1.WarehouseAreaId = Extent4.Id
          WHERE (((Extent1.WarehouseId = {0}) AND (Extent3.Status = 1)) AND (Extent4.AreaKind = 3)) AND (Extent4.Status = 1) ) Filter1
        INNER JOIN WarehouseArea Extent5 ON Filter1.TopLevelWarehouseAreaId1 = Extent5.Id
        INNER JOIN Warehouse Extent6 ON Filter1.WarehouseId1 = Extent6.Id
        WHERE (Extent5.Status = 1) AND (Extent6.Status = 1) ) Filter2
      INNER JOIN SparePart Extent7 ON Filter2.PartId = Extent7.Id
      INNER JOIN WarehouseAreaCategory Extent8 ON Filter2.WarehouseAreaCategoryId = Extent8.Id
      INNER JOIN PartsSalesCategory Extent9 ON Filter2.PartsSalesCategoryId = Extent9.Id
      WHERE ((Extent7.Status = 1) ) AND (Extent9.Status = 1) ) Filter3
    LEFT OUTER JOIN PartsStockBatchDetail Extent10 ON Filter3.Id3 = Extent10.PartsStockId
    LEFT OUTER JOIN PartsSalesPrice Extent11 ON (((Filter3.PartsSalesCategoryId = Extent11.PartsSalesCategoryId) AND (Filter3.Id4 = Extent11.SparePartId)) AND (Extent11.Status = 1))
    WHERE ( EXISTS (SELECT 
      1 AS C1
      FROM WarehouseAreaManager Extent12
      WHERE (Extent12.ManagerId = {4}) AND (Extent12.WarehouseAreaId = Filter3.TopLevelWarehouseAreaId1)
    )) AND (Filter3.Category = {1})
  )  Project2
  ORDER BY Project2.Id ASC
)  top where {2} and {3}";
                    var partIds = rightList.Select(r => r.SparePartId).ToArray().Distinct();
                    var originalWarehouseAreaCodes = rightList.Select(r => r.OriginalWarehouseAreaCode).ToArray();
                    using (var conn = db.CreateDbConnection()) {
                        conn.Open();
                        string orcalepartIds = getOracleSQLIn(partIds.ToList(), "top.id1");
                        string orcaleOriginalWarehouseAreaCodes = getOracleSQLIn(originalWarehouseAreaCodes.ToList(), "top.code");
                        //查询原库位可用库存
                        var sql2 = string.Format(sql,warehouseId,OriginalWarehouseAreaCategory, orcalepartIds, orcaleOriginalWarehouseAreaCodes,userInfo.Id);

                        var details = new List<PartsStock>();
                        var cmd2 = db.CreateDbCommand(sql2, conn, null);
                        var odr2 = cmd2.ExecuteReader();

                        try {
                            if(odr2.HasRows) {
                                while(odr2.Read()) {
                                    var item = new PartsStock();
                                    item.PartId = Convert.ToInt32(odr2["partid"]);
                                    item.WarehouseAreaId = Convert.ToInt32(odr2["WarehouseAreaId"]);
                                    item.WarehouseAreaCode = Convert.ToString(odr2["WarehouseAreaCode"]);
                                    item.Quantity = Convert.ToInt32(odr2["Quantity"]);
                                    item.LockedQuantity = Convert.ToInt32(odr2["LockedQuantity"]);
                                    details.Add(item);
                                }
                            }
                        } finally {
                            if(conn.State == System.Data.ConnectionState.Open) {
                                conn.Close();
                            }
                        }

                        foreach(var rightItem in rightList) {
                            var detail = details.FirstOrDefault(r => r.PartId == rightItem.SparePartId && r.WarehouseAreaCode == rightItem.OriginalWarehouseAreaCode);
                            if (detail == null) {
                                rightItem.ErrorMsg = ErrorStrings.ImpPartsShiftOrderDetail_Validation10;
                            } else {
                                if (detail.Quantity - detail.LockedQuantity < rightItem.ShiftQuantity) {
                                    rightItem.ErrorMsg = string.Format(ErrorStrings.ImpPartsShiftOrderDetail_Validation11, detail.Quantity - detail.LockedQuantity);
                                } else {
                                    rightItem.OriginalWarehouseAreaQuantity = detail.Quantity - detail.LockedQuantity;
                                    rightItem.OriginalWarehouseAreaId = detail.WarehouseAreaId;
                                }
                            }
                        }
                    }
                }
                var err2 = rightList.Where(r => !string.IsNullOrEmpty(r.ErrorMsg));
                errorList = errorList.Union(err2).ToList();
                rightList = allList.Except(errorList).ToList();
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var details = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == details.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var partsShiftOrderDetail = details[index - 1];
                            var values = new object[]{
                                        partsShiftOrderDetail.SparePartCode,partsShiftOrderDetail.OriginalWarehouseAreaCode,
                                        partsShiftOrderDetail.DestWarehouseAreaCode,partsShiftOrderDetail.ShiftQuantity,partsShiftOrderDetail.ErrorMsg
                                    };
                            return values;
                        });
                    }
                    errorList = null;
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {

                errorData = errorList;
                rightData = rightList;
            }
        }

    }
}
