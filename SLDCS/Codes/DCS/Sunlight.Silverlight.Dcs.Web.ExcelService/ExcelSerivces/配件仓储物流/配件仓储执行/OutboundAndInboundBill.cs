﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        /// <summary>
        /// 导出历史出入库记录
        /// </summary>
        public bool ExportOutboundAndInboundBill(int sparePartId, int warehouseId, DateTime? createTimeBegin, DateTime? createTimeEnd, string warehouseAreaCode, out string fileName) {
            fileName = GetExportFilePath("历史出入库记录_" + ".xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {
                    var sql = new StringBuilder();
                    sql.AppendFormat(@"Select c.WarehouseCode,
                                            c.WarehouseName,
                                            c.ReceivingWarehouseName,
                                            c.BranchName,
                                            c.BillCode,
                                            c.BillBusinessType,
                                            c.CounterpartCompanyCode,
                                            c.CounterpartCompanyName,
                                            c.SparePartCode,
                                            c.SparePartName,
                                            c.WarehouseAreaCode,
                                            c.Quantity,
                                            c.CreateTime,
                                            c.WholesalePrice
                                        From ((Select a.Warehousecode As WarehouseCode,
                                           a.Warehousename As WarehouseName,
                                           a.ReceivingWarehouseName,
                                           a.Branchname As BranchName,
                                           a.Code As BillCode,
                                           (select value from keyvalueitem where NAME = 'Parts_OutboundType'and key=a.Outboundtype) As BillBusinessType,
                                           a.Counterpartcompanycode As CounterpartCompanyCode,
                                           a.Counterpartcompanyname As CounterpartCompanyName,
                                           b.WarehouseAreaCode As WarehouseAreaCode,
                                           b.Sparepartcode As SparePartCode,
                                           b.Sparepartname As SparePartName,
                                           b.outboundamount As Quantity,
                                           a.Createtime As CreateTime,
                                           b.Settlementprice As WholesalePrice
                                      From Partsoutboundbill a
                                      Left Join Partsoutboundbilldetail b
                                        On a.Id = b.Partsoutboundbillid
                                    where a.Warehouseid = {0} And b.Sparepartid = {1}) ", warehouseId, sparePartId);
                    sql.AppendFormat(@"Union (Select a.Warehousecode As WarehouseCode,
                                           a.Warehousename As WarehouseName,
                                           null as ReceivingWarehouseName,
                                           a.Branchname As BranchName,
                                           a.Code As BillCode,
                                           (select value from keyvalueitem where NAME = 'Parts_InboundType'and key=a.Inboundtype) As BillBusinessType,
                                           a.Counterpartcompanycode As CounterpartCompanyCode,
                                           a.Counterpartcompanyname As CounterpartCompanyName,
                                           w.Code As WarehouseAreaCode,
                                           b.Sparepartcode As SparePartCode,
                                           b.Sparepartname As SparePartName,
                                           b.InspectedQuantity As Quantity,
                                           a.Createtime As CreateTime,
                                           b.Settlementprice As WholesalePrice
                                      From PartsInboundCheckBill a
                                      Left Join PartsInboundCheckBillDetail b
                                        On a.Id = b.PartsInboundCheckBillId
                                      Left Join PartsStock p 
                                        on a.Warehouseid=p.WarehouseId and b.sparePartId =p.PartId
                                      inner Join WarehouseArea w 
                                        on w.Id=p.Warehouseareaid  and w.Status=1
                                      inner Join WarehouseAreaCategory wc 
                                        on wc.id=w.AreaCategoryId and wc.Category=1
                                    where a.Warehouseid = {0} And b.Sparepartid = {1})) c where 1=1 ", warehouseId, sparePartId);
                    var dbParameters = new List<DbParameter>();
                    if(!string.IsNullOrEmpty(warehouseAreaCode)) {
                        sql.Append(@" and c.WarehouseAreaCode like '%" + warehouseAreaCode + "%'");
                    }
                    if(createTimeBegin.HasValue) {
                        sql.Append(@" and c.CreateTime >=to_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                        var tempValue = createTimeBegin.Value;
                        var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                        dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                    }
                    if(createTimeEnd.HasValue) {
                        sql.Append(@" and c.CreateTime <=to_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                        var tempValue = createTimeEnd.Value;
                        var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                        dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                    }
                    var sqlStr = sql.ToString();
                    if(sqlStr.IndexOf(" where 1=1  and ") > 0)
                        sqlStr = sqlStr.Replace(" where 1=1  and ", " where ");
                    else
                        sqlStr = sqlStr.Replace(" where 1=1 ", "");

                    var command = db.CreateDbCommand(string.Format(sqlStr, db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    conn.Open();
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                        ErrorStrings.Export_Title_Company_WarehouseCode, ErrorStrings.Export_Title_Company_WarehouseName,ErrorStrings.Export_Title_SupplierShippingOrder_ReveiveWarehouseName, ErrorStrings.Export_Title_PartsBranch_BranchName, ErrorStrings.Export_Title_OutAndInBound_Code, ErrorStrings.Export_Title_OutAndInBound_Type, ErrorStrings.Export_Title_BoxUpTask_CounterpartCompanyCode, ErrorStrings.Export_Title_BoxUpTask_CounterpartCompanyName, ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode, ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName,ErrorStrings.Export_Title_OutAndInBound_WareArea, ErrorStrings.Export_Title_WarehouseArea_Quantity, ErrorStrings.Export_Title_AccountPeriod_CreateTime, ErrorStrings.Export_Title_PartsPurchaseRtnSettleBill_SettlementPrice
                                    };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }
    }
}
