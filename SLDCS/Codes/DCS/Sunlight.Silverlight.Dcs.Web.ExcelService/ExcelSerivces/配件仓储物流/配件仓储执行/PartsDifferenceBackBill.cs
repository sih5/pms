﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        /// <summary>
        /// 合并导出配件差异回退单主清单
        /// </summary>
        public bool ExportPartsDifferenceBackBillWithDetail(int[] ids, string code, int? status, int? type, string taskCode, string packingCode, string inPlanCode, DateTime? createTimeBegin, DateTime? createTimeEnd, DateTime? bSubmitTime, DateTime? eSubmitTime, out string fileName) {
            fileName = GetExportFilePath("配件差异回退主清单_" + ".xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.Append(string.Format(@"select pb.code,/*差异单号*/
                                               (select value from keyvalueitem kv where kv.key =pb.status and kv.name='PartsDifferenceBackBillStatus')as status,/*状态*/
                                                (select value from keyvalueitem kv where kv.key =pb.type and kv.name='PartsDifferenceBackBillType')as type,/*回退类型*/
                                               pb.sourcecode,/*源单据编号*/
                                                pb.PartsSupplierCode,/*供应商编号*/
                                                pb.PartsSupplierName,/*供应商名称*/
                                               pb.partsinboundcheckbillcode,/*入库单号*/
                                               pb.submittername,/*提交人*/
                                               pb.submittime,/*提交时间*/
                                               pb.initialapprovername,/*初审人*/
                                               pb.initialapprovetime,/*初审时间*/
                                               pb.checkername,/*审核人*/
                                               pb.checktime,/*审核时间*/
                                               pb.approvername,/*审批人*/
                                               pb.approvetime,/*审批时间*/
                                               pb.rejectername,/*驳回人*/
                                               pb.rejecttime,/*驳回时间*/
                                               pb.abandonername,/*作废人*/
                                               pb.abandontime,/*作废时间*/
                                               pb.creatorname,/*创建人*/
                                               pb.createtime,/*创建时间*/
                                               pb.modifiername,/*修改人*/
                                               pb.modifytime,/*修改时间*/
                                               pbd.sparepartcode,/*配件编号*/
                                               pbd.sparepartname,/*配件名称*/
                                               pbd.diffquantity,/*差异量*/
                                                (select value from keyvalueitem kv where kv.key = pbd.errortype and kv.name='PartsDifferenceBackBillDetailErrorType')as ErrorType,/*错误类型*/
                                               pbd.taskcode,/*任务单号*/
                                             (select value from keyvalueitem kv where kv.key = pbd.TraceProperty and kv.name='TraceProperty')as TraceProperty,/*追溯属性*/
                                              pbd.TraceCode /*追溯码*/
                                          from PartsDifferenceBackBill pb
                                          join PartsDifferenceBackBillDtl pbd
                                            on pb.id = pbd.partsdifferencebackbillid
                                        where 1=1"));
                    var tableNames = sql.ToString().GetRegexNames();
                    var dbParameters = new List<DbParameter>();
                    if(ids != null && ids.Length > 0) {
                        sql.Append(@" and pb.id in (");
                        for(var i = 0; i < ids.Length; i++) {
                            var tempPartsPurchaseOrderIdWithNum = "partsDifferenceBackBillId" + i;
                            if(i == ids.Length - 1) {
                                var tempParam = "{0}" + tempPartsPurchaseOrderIdWithNum;
                                sql.Append(tempParam);
                            } else {
                                var tempParam = "{0}" + tempPartsPurchaseOrderIdWithNum + ",";
                                sql.Append(tempParam);
                            }
                            dbParameters.Add(db.CreateDbParameter(tempPartsPurchaseOrderIdWithNum, ids[i]));
                        }
                        sql.Append(@")");
                    } else {
                        if(!String.IsNullOrEmpty(code)) {
                            sql.Append(@" and pb.Code like {0}code ");
                            dbParameters.Add(db.CreateDbParameter("code", "%" + code + "%"));
                        }
                        if(status.HasValue) {
                            sql.Append(@" and pb.status = {0}status ");
                            dbParameters.Add(db.CreateDbParameter("status", status.Value));
                        }
                        if(type.HasValue) {
                            sql.Append(@" and pb.type = {0}type ");
                            dbParameters.Add(db.CreateDbParameter("type", type.Value));
                        }
                        if(type.HasValue) {
                            sql.Append(@" and a.type = {0}type ");
                            dbParameters.Add(db.CreateDbParameter("type", type.Value));
                        }
                        if(!string.IsNullOrEmpty(taskCode)) {
                            sql.Append(@" and  exists(select 1 from PartsDifferenceBackBillDtl pst where pst.TaskCode={0}taskCode and pst.PartsDifferenceBackBillId=pb.Id) ");
                            dbParameters.Add(db.CreateDbParameter("taskCode", taskCode));
                        }
                        //if(!string.IsNullOrEmpty(packingCode)) {
                        //    sql.Append(@" and  exists(select 1 from PackingTask pst where pst.code={0}packingCode and pst.SourceCode=pb.sourcecode) ");
                        //    dbParameters.Add(db.CreateDbParameter("packingCode", packingCode));
                        //}
                        //if(!string.IsNullOrEmpty(inPlanCode)) {
                        //    sql.Append(@" and  exists(select 1 from PartsInboundCheckBill pst where pst.code={0}inPlanCode and pst.OriginalRequirementBillCode=pb.sourcecode) ");
                        //    dbParameters.Add(db.CreateDbParameter("inPlanCode", inPlanCode));
                        //}
                        if(createTimeBegin.HasValue) {
                            sql.Append(@" and pb.createTime >=to_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(@" and pb.createTime <=to_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                        if(bSubmitTime.HasValue) {
                            sql.Append(@" and pb.SubmitTime >=to_date({0}bSubmitTime,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = bSubmitTime.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("bSubmitTime", tempTime.ToString("G")));
                        }
                        if(eSubmitTime.HasValue) {
                            sql.Append(@" and a.SubmitTime <=to_date({0}eSubmitTime,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = eSubmitTime.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("eSubmitTime", tempTime.ToString("G")));
                        }

                    }

                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();

                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0)
                                return tableNames;
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }
    }
}