﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Globalization;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        /// <summary>
        /// 合并导出配件入库检验主清单
        /// </summary>
        public bool ExportPartsInboundCheckBillWithDetail(int[] ids, string originalRequirementBillCode, int? userId, int? storageCompanyId, string partsInboundCheckBillCode, int? warehouseId, int? counterpartCompanyId, string counterpartcompanyname, int? inboundType, int? settlementStatus, DateTime? createTimeBegin, DateTime? createTimeEnd, string sparePartCode, string sparePartName, string partsInboundPlanCode, string batchNumber, bool? hasDifference, out string fileName) {
            fileName = GetExportFilePath("配件入库检验主清单.xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);
                var userInfo = Utils.GetCurrentUserInfo();
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.Append(@"Select a.Code,
                                a.Warehousename,            
                                (select value from keyvalueitem where NAME = 'Parts_InboundType'and key=a.Inboundtype) As Inboundtype,
                                a.Counterpartcompanycode,
                                a.Counterpartcompanyname,
                                (Select Sum(t.Settlementprice * t.Inspectedquantity)
                                    From Partsinboundcheckbilldetail t
                                    Where t.Partsinboundcheckbillid = a.Id),
                                p.Name,
                                a.Originalrequirementbillcode,
                                c.Code As Partsinboundplanidcode,   
                                d.Name As Partssalescategoryname,
                                a.Branchname,
                                a.Warehousecode,             
                                po.PlanSource,      
                                (select value from keyvalueitem where NAME = 'Parts_SettlementStatus'and key=a.Settlementstatus) As Settlementstatus,
                                (select value from keyvalueitem where NAME = 'PartsSalesReturn_ReturnType'and key=f.ReturnType) As ReturnType, 
                                a.ERPSourceOrderCode,
                                a.Remark,
                                a.Creatorname,
                                a.Createtime,
                                a.Modifiername,
                                a.Modifytime,
                                (select value from keyvalueitem where NAME = 'PartsShipping_Method'and key=sso.ShippingMethod) As ShippingMethod,
                                b.Sparepartcode,
                                b.Sparepartname,
                                b.Warehouseareacode,
                                ( Select Min(Extent2.Code) As A1
                           From Partsstock Extent1
                          Inner Join Warehousearea Extent2
                             On Extent1.Warehouseareaid = Extent2.Id
                          Inner Join Warehouseareacategory Extent3
                             On Extent2.Areacategoryid = Extent3.Id
                          Where (((Extent1.Warehouseid = a.Warehouseid)and Extent1.partid=b.Sparepartid) And
                                (Extent2.Areakind = 3))
                            And (Extent3.Category = 1)),
                               b.Inspectedquantity,
                               b.Settlementprice,
                               s.Measureunit,
                               b.Remark As Remarkdetails,
                               b.SpareOrderRemark,
                               b.batchNumber,
                               case nvl(a.IsDifference, 0)
                                    when 0  then cast('否' as varchar2(100)) else cast('是' as varchar2(100)) end as HasDifference 
                          From Partsinboundcheckbill a
                           left join PartsPurchaseOrder po
                             on a.OriginalRequirementBillCode=po.Code
                           left join PartsPurchaseOrderType p
                              on po.PartsPurchaseOrderTypeId=p.Id
                          Left Outer Join Partsinboundcheckbilldetail b
                            On a.Id = b.Partsinboundcheckbillid
                          Left Outer Join Partsinboundplan c
                            On a.Partsinboundplanid = c.Id
                          left outer join SupplierShippingOrder sso
                            on c.sourceid=sso.id and c.SourceCode=sso.Code
                          Left Outer Join Partssalescategory d
                            On a.Partssalescategoryid = d.Id
                          Left Outer Join PartsInboundPlan e
                             On a.PartsInboundPlanId = e.Id
                          Left Outer Join PartsSalesReturnBill f
                             On e.SourceId = f.Id   
                          Left Outer Join sparepart s
                             On b.Sparepartid = s.Id where 1=1 ");
                    var dbParameters = new List<DbParameter>();
                    if(ids != null && ids.Length > 0) {
                        sql.Append(" and a.id in (");
                        for(var i = 0; i < ids.Length; i++) {
                            if(ids.Length == i + 1) {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            } else {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                        }
                        sql.Append(")");
                    } else {
                        if(storageCompanyId.HasValue) {
                            sql.Append(" and a.storageCompanyId = {0}storageCompanyId ");
                            dbParameters.Add(db.CreateDbParameter("storageCompanyId", userInfo.EnterpriseId));
                        }
                        if(userId.HasValue) {
                            sql.Append(@" and (Exists (Select 1 As C1
                  From Warehouseoperator Extent9
                 Where Extent9.Operatorid = {0}userId
                   And Extent9.Warehouseid = a.Warehouseid)) ");
                            dbParameters.Add(db.CreateDbParameter("userId", userInfo.Id));
                        }
                        if (hasDifference == true) {
                            sql.Append(" and exists(select 1 from PackingTask pt inner join PartsDifferenceBackBill pdb on pt.code = pdb.sourcecode and pdb.type=2 and pdb.status<>99 where pt.PartsInboundCheckBillCode = a.code) ");
                        }
                        if (hasDifference == false) {
                            sql.Append(" and not exists(select 1 from PackingTask pt inner join PartsDifferenceBackBill pdb on pt.code = pdb.sourcecode and pdb.type=2 and pdb.status<>99 where pt.PartsInboundCheckBillCode = a.code) ");
                        }
                        if(!string.IsNullOrEmpty(partsInboundCheckBillCode)) {
                            sql.Append(" and LOWER(a.Code) like {0}partsInboundCheckBillCode ");
                            dbParameters.Add(db.CreateDbParameter("partsInboundCheckBillCode", "%" + partsInboundCheckBillCode.ToLower() + "%"));
                        }
                        if(!string.IsNullOrEmpty(originalRequirementBillCode)) {
                            sql.Append(" and LOWER(a.originalRequirementBillCode) like {0}originalRequirementBillCode ");
                            dbParameters.Add(db.CreateDbParameter("originalRequirementBillCode", "%" + originalRequirementBillCode.ToLower() + "%"));
                        }
                        if(warehouseId.HasValue) {
                            sql.Append(" and a.warehouseId = {0}warehouseId ");
                            dbParameters.Add(db.CreateDbParameter("warehouseId", warehouseId.Value));
                        }
                        if(counterpartCompanyId.HasValue) {
                            sql.Append(" and a.counterpartCompanyId = {0}counterpartCompanyId ");
                            dbParameters.Add(db.CreateDbParameter("counterpartCompanyId", counterpartCompanyId.Value));
                        }

                        if(!string.IsNullOrEmpty(counterpartcompanyname)) {
                            sql.Append(" and LOWER(a.counterpartcompanyname) like {0}counterpartcompanyname ");
                            dbParameters.Add(db.CreateDbParameter("counterpartcompanyname", "%" + counterpartcompanyname.ToLower() + "%"));
                        }
                        if(!string.IsNullOrEmpty(partsInboundPlanCode)) {
                            sql.Append(" and LOWER(c.code) like {0}partsInboundPlanCode ");
                            dbParameters.Add(db.CreateDbParameter("partsInboundPlanCode", "%" + partsInboundPlanCode.ToLower() + "%"));
                        }

                        if(inboundType.HasValue) {
                            sql.Append(" and a.inboundType ={0}inboundType ");
                            dbParameters.Add(db.CreateDbParameter("inboundType", inboundType.Value));
                        }


                        if(settlementStatus.HasValue) {
                            sql.Append(" and a.settlementStatus ={0}settlementStatus ");
                            dbParameters.Add(db.CreateDbParameter("settlementStatus", settlementStatus.Value));
                        }

                        if(createTimeBegin.HasValue) {
                            sql.Append(@" and a.createTime >=To_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }


                        if(createTimeEnd.HasValue) {
                            sql.Append(@" and a.createTime <=To_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }


                        if(!string.IsNullOrEmpty(sparePartName)) {
                            sql.Append(" and LOWER(b.sparePartName) like {0}sparePartName ");
                            dbParameters.Add(db.CreateDbParameter("sparePartName", "%" + sparePartName.ToLower() + "%"));
                        } if(!string.IsNullOrEmpty(sparePartCode)) {
                            sql.Append(" and LOWER(b.sparePartCode) like {0}sparePartCode ");
                            dbParameters.Add(db.CreateDbParameter("sparePartCode", "%" + sparePartCode.ToLower() + "%"));
                        }
                        if(!string.IsNullOrEmpty(batchNumber)) {
                            sql.Append(" and LOWER(b.batchNumber) like {0}batchNumber ");
                            dbParameters.Add(db.CreateDbParameter("batchNumber", "%" + batchNumber.ToLower() + "%"));
                        }
                    }
                    var sqlStr = sql.ToString();
                    if(sqlStr.IndexOf(" where 1=1  and ") > 0)
                        sqlStr = sqlStr.Replace(" where 1=1  and ", " where ");
                    else
                        sqlStr = sqlStr.Replace(" where 1=1 ", "");
                    var command = db.CreateDbCommand(string.Format(sqlStr, db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                   ErrorStrings.Export_Title_PartsInboundCheckBill_Code, ErrorStrings.Export_Title_Company_WarehouseName, ErrorStrings.Export_Title_PartsInboundCheckBill_InBoundType,ErrorStrings.Export_Title_BoxUpTask_CounterpartCompanyCode, ErrorStrings.Export_Title_BoxUpTask_CounterpartCompanyName,ErrorStrings.Export_Title_InternalAllocationBill_TotalAmount
                                   ,ErrorStrings.Export_Title_PartsInboundCheckBill_PurchaseType, ErrorStrings.Export_Title_PartsInboundCheckBill_OriginalRequirementBillCode, ErrorStrings.Export_Title_PartsInboundCheckBill_PartsInboundPlanCode, ErrorStrings.Export_Title_Partssalescategory_Name, ErrorStrings.Export_Title_PartsBranch_BranchName, ErrorStrings.Export_Title_Company_WarehouseCode, ErrorStrings.Export_Title_PartsPurchaseOrder_PlanSource,
                                   ErrorStrings.Export_Title_PartsInboundCheckBill_SettlementStatus,ErrorStrings.Export_Title_PartsInboundCheckBill_ReturnType,ErrorStrings.Export_Title_PartsInboundCheckBill_DeliveryCode, ErrorStrings.Export_Title_PartsBranch_Remark, ErrorStrings.Export_Title_AccountPeriod_CreatorName, ErrorStrings.Export_Title_AccountPeriod_CreateTime, ErrorStrings.Export_Title_AccountPeriod_ModifierName, ErrorStrings.Export_Title_AccountPeriod_ModifyTime, ErrorStrings.Export_Title_PartsPurchaseOrder_ShippingMethod,ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode, ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName, ErrorStrings.Export_Title_WarehouseArea_WarehouseAreaCode,ErrorStrings.Export_Title_PartsInboundCheckBill_TargetLocation, ErrorStrings.Export_Title_PartsInboundCheckBill_InspectedQuantity, ErrorStrings.Export_Title_PartsPurchaseRtnSettleBill_SettlementPrice, ErrorStrings.Export_Title_PackingPropertyApp_MeasureUnit, ErrorStrings.Export_Title_PartsPurchaseOrder_DetailRemark,ErrorStrings.Export_Title_SupplierShippingOrder_DetailRemark,"批次号",
                                   ErrorStrings.Export_Title_PackingTask_HasDifference
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();

                }
                return true;
            } catch(Exception) {
                return false;
            }
        }

        /// <summary>
        /// 合并导出配件入库检验主清单（无价格）
        /// </summary>
        public bool ExportPartsInboundCheckBillWithDetailNoPrice(int[] ids, string originalRequirementBillCode, int? userId, int? partsSalesCategoryId, int? storageCompanyId, string partsInboundCheckBillCode, int? warehouseId, int? counterpartCompanyId, string counterpartcompanycode, string counterpartcompanyname, string eRPOrderCode, string partsSalesOrderCode, int? partsSalesOrderInvoiceType, int? inboundType, int? settlementStatus, DateTime? createTimeBegin, DateTime? createTimeEnd, int? partsPurchaseOrderTypeId, string sAPPurchasePlanCode, out string fileName) {
            fileName = GetExportFilePath("配件入库检验主清单.xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);
                var userInfo = Utils.GetCurrentUserInfo();
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.Append(@"Select a.Code,a.WarehouseCode,
       a.Warehousename,
      (select value from keyvalueitem where NAME = 'Parts_InboundType'and key=a.Inboundtype) As Inboundtype,
        a.OriginalRequirementBillCode,
        c.Code,
        d.Name,
        a.BranchName,
        po.PlanSource,
       a.Counterpartcompanycode,
       a.Counterpartcompanyname,
       p.Name,
       a.Objid,
       a.GPMSPurOrderCode,
       (select value from keyvalueitem where NAME = 'Parts_SettlementStatus'and key=a.Settlementstatus) As Settlementstatus,
       (select value from keyvalueitem where NAME = 'PartsSalesReturn_ReturnType'and key=f.ReturnType) As ReturnType, 
       a.Remark,
       a.ERPSourceOrderCode,
       a.SAPPurchasePlanCode,
       g.Code,
       (select value from keyvalueitem where NAME = 'ERPInvoiceInformation_Type'and key=g.InvoiceType) As InvoiceType,
       a.Creatorname,
       a.Createtime,
       a.Modifiername,
       a.Modifytime,
       (select value from keyvalueitem where NAME = 'PartsShipping_Method'and key=sso.ShippingMethod) As ShippingMethod,
       b.Sparepartcode,
       b.Sparepartname,
       b.Warehouseareacode,
       ( Select Min(Extent2.Code) As A1
   From Partsstock Extent1
  Inner Join Warehousearea Extent2
     On Extent1.Warehouseareaid = Extent2.Id
  Inner Join Warehouseareacategory Extent3
     On Extent2.Areacategoryid = Extent3.Id
  Where (((Extent1.Warehouseid = a.Warehouseid)and Extent1.partid=b.Sparepartid) And
        (Extent2.Areakind = 3))
    And (Extent3.Category = 1)),
       b.Inspectedquantity,
       (Select t.Measureunit From Sparepart t Where t.Id = b.Sparepartid),
       b.Remark As Remarkdetails,
       b.SpareOrderRemark
  From Partsinboundcheckbill a
   left join PartsPurchaseOrder po
     on a.OriginalRequirementBillCode=po.Code
   left join PartsPurchaseOrderType p
      on po.PartsPurchaseOrderTypeId=p.Id
  Left Outer Join Partsinboundcheckbilldetail b
    On a.Id = b.Partsinboundcheckbillid
  Left Outer Join Partsinboundplan c
    On a.Partsinboundplanid = c.Id
  left outer join SupplierShippingOrder sso
    on c.sourceid=sso.id and c.SourceCode=sso.Code
  Left Outer Join Partssalescategory d
    On a.Partssalescategoryid = d.Id
  Left Outer Join PartsInboundPlan e
     On a.PartsInboundPlanId = e.Id
  Left Outer Join PartsSalesReturnBill f
     On e.SourceId = f.Id   
   Left Outer Join PartsSalesOrder g
     On f.PartsSalesOrderId = g.Id where 1=1 ");
                    var dbParameters = new List<DbParameter>();
                    if(ids != null && ids.Length > 0) {
                        sql.Append(" and a.id in (");
                        for(var i = 0; i < ids.Length; i++) {
                            if(ids.Length == i + 1) {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            } else {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                        }
                        sql.Append(")");
                    } else {
                        if(partsPurchaseOrderTypeId.HasValue) {
                            //采购订单类型
                            sql.Append(@" and  po.PartsPurchaseOrderTypeId={0}partsPurchaseOrderTypeId ");
                            dbParameters.Add(db.CreateDbParameter("partsPurchaseOrderTypeId", partsPurchaseOrderTypeId.Value));
                        }
                        if(storageCompanyId.HasValue) {
                            sql.Append(" and a.storageCompanyId = {0}storageCompanyId ");
                            dbParameters.Add(db.CreateDbParameter("storageCompanyId", userInfo.EnterpriseId));
                        }
                        if(userId.HasValue) {
                            sql.Append(@" and (Exists (Select 1 As C1
                  From Warehouseoperator Extent9
                 Where Extent9.Operatorid = {0}userId
                   And Extent9.Warehouseid = a.Warehouseid)) ");
                            dbParameters.Add(db.CreateDbParameter("userId", userInfo.Id));
                        }
                        if(partsSalesCategoryId.HasValue) {
                            sql.Append(@" and a.partsSalesCategoryId = {0}partsSalesCategoryId ");
                            dbParameters.Add(db.CreateDbParameter("partsSalesCategoryId", partsSalesCategoryId.Value));
                        }
                        if(!string.IsNullOrEmpty(partsInboundCheckBillCode)) {
                            sql.Append(" and LOWER(a.Code) like {0}partsInboundCheckBillCode ");
                            dbParameters.Add(db.CreateDbParameter("partsInboundCheckBillCode", "%" + partsInboundCheckBillCode.ToLower() + "%"));
                        }
                        if(!string.IsNullOrEmpty(originalRequirementBillCode)) {
                            sql.Append(" and LOWER(a.originalRequirementBillCode) like {0}originalRequirementBillCode ");
                            dbParameters.Add(db.CreateDbParameter("originalRequirementBillCode", "%" + originalRequirementBillCode.ToLower() + "%"));
                        }


                        if(warehouseId.HasValue) {
                            sql.Append(" and a.warehouseId = {0}warehouseId ");
                            dbParameters.Add(db.CreateDbParameter("warehouseId", warehouseId.Value));
                        }
                        if(!string.IsNullOrEmpty(counterpartcompanycode)) {
                            sql.Append(" and LOWER(a.counterpartcompanycode) like {0}counterpartcompanycode ");
                            dbParameters.Add(db.CreateDbParameter("counterpartcompanycode", "%" + counterpartcompanycode.ToLower() + "%"));
                        }
                        if(counterpartCompanyId.HasValue) {
                            sql.Append(" and a.counterpartCompanyId = {0}counterpartCompanyId ");
                            dbParameters.Add(db.CreateDbParameter("counterpartCompanyId", counterpartCompanyId.Value));
                        }

                        if(!string.IsNullOrEmpty(counterpartcompanyname)) {
                            sql.Append("and LOWER(a.counterpartcompanyname) like {0}counterpartcompanyname ");
                            dbParameters.Add(db.CreateDbParameter("counterpartcompanyname", "%" + counterpartcompanyname.ToLower() + "%"));
                        }
                        if(!string.IsNullOrEmpty(eRPOrderCode)) {
                            sql.Append(" and LOWER(a.ERPSourceOrderCode) like {0}eRPOrderCode ");
                            dbParameters.Add(db.CreateDbParameter("eRPOrderCode", "%" + eRPOrderCode.ToLower() + "%"));
                        }

                        if(!string.IsNullOrEmpty(sAPPurchasePlanCode)) {
                            sql.Append(" and LOWER(a.SAPPurchasePlanCode) like {0}sAPPurchasePlanCode ");
                            dbParameters.Add(db.CreateDbParameter("sAPPurchasePlanCode", "%" + sAPPurchasePlanCode.ToLower() + "%"));
                        }
                        if(!string.IsNullOrEmpty(partsSalesOrderCode)) {
                            sql.Append(" and LOWER(g.code) like {0}code ");
                            dbParameters.Add(db.CreateDbParameter("code", "%" + partsSalesOrderCode.ToLower() + "%"));
                        }
                        if(partsSalesOrderInvoiceType.HasValue) {
                            sql.Append(" and g.InvoiceType ={0}InvoiceType ");
                            dbParameters.Add(db.CreateDbParameter("InvoiceType", partsSalesOrderInvoiceType.Value));
                        }
                        if(inboundType.HasValue) {
                            sql.Append("and a.inboundType ={0}inboundType ");
                            dbParameters.Add(db.CreateDbParameter("inboundType", inboundType.Value));
                        }


                        if(settlementStatus.HasValue) {
                            sql.Append(" and a.settlementStatus ={0}settlementStatus ");
                            dbParameters.Add(db.CreateDbParameter("settlementStatus", settlementStatus.Value));
                        }

                        if(createTimeBegin.HasValue) {
                            sql.Append(@" and a.createTime >=To_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }


                        if(createTimeEnd.HasValue) {
                            sql.Append(@" and a.createTime <=To_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                    }
                    var sqlStr = sql.ToString();
                    if(sqlStr.IndexOf(" where 1=1  and ") > 0)
                        sqlStr = sqlStr.Replace(" where 1=1  and ", " where ");
                    else
                        sqlStr = sqlStr.Replace(" where 1=1 ", "");

                    var command = db.CreateDbCommand(string.Format(sqlStr, db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    ErrorStrings.Export_Title_PartsInboundCheckBill_Code,ErrorStrings.Export_Title_Company_WarehouseCode,ErrorStrings.Export_Title_Company_WarehouseName, ErrorStrings.Export_Title_PartsInboundCheckBill_InBoundType,ErrorStrings.Export_Title_PartsInboundCheckBill_OriginalRequirementBillCode,ErrorStrings.Export_Title_PartsInboundCheckBill_PartsInboundPlanCode,ErrorStrings.Export_Title_Partssalescategory_Name,ErrorStrings.Export_Title_PartsBranch_BranchName,ErrorStrings.Export_Title_PartsPurchaseOrder_PlanSource,ErrorStrings.Export_Title_BoxUpTask_CounterpartCompanyCode, ErrorStrings.Export_Title_BoxUpTask_CounterpartCompanyName,ErrorStrings.Export_Title_PartsInboundCheckBill_PurchaseType,ErrorStrings.Export_Title_PartsInboundCheckBill_Objid,ErrorStrings.Export_Title_PartsPurchaseOrder_GPMSPurOrderCode, ErrorStrings.Export_Title_PartsInboundCheckBill_SettlementStatus, ErrorStrings.Export_Title_PartsInboundCheckBill_ReturnType, ErrorStrings.Export_Title_PartsBranch_Remark,ErrorStrings.Export_Title_SupplierShippingOrder_ERPSourceOrderCode,ErrorStrings.Export_Title_PartsInboundCheckBill_SAPPurchasePlanCode, ErrorStrings.Export_Title_SupplierShippingOrder_SalesOrderCode,ErrorStrings.Export_Title_PartsInboundCheckBill_BillingType, ErrorStrings.Export_Title_AccountPeriod_CreatorName, ErrorStrings.Export_Title_AccountPeriod_CreateTime, ErrorStrings.Export_Title_AccountPeriod_ModifierName, ErrorStrings.Export_Title_AccountPeriod_ModifyTime, ErrorStrings.Export_Title_PartsPurchaseOrder_ShippingMethod,ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode, ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName, ErrorStrings.Export_Title_WarehouseArea_WarehouseAreaCode,ErrorStrings.Export_Title_PartsInboundCheckBill_TargetLocation, ErrorStrings.Export_Title_PartsInboundCheckBill_InspectedQuantity, ErrorStrings.Export_Title_PackingPropertyApp_MeasureUnit, ErrorStrings.Export_Title_PartsPurchaseOrder_DetailRemark,ErrorStrings.Export_Title_SupplierShippingOrder_DetailRemark
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();

                }
                return true;
            } catch(Exception) {
                return false;
            }
        }

        /// <summary>
        /// 导出配件入库检验单（计划价）
        /// </summary>
        /// <returns></returns>
        public bool ExportPartsInboundCheckBillForPlannedPrice(int[] ids, int? storageCompanyId, string partsInboundCheckBillCode, int? partsSalesCategoryId, int? warehouseId, int? inboundType, string counterpartCompanyCode, string counterpartCompanyName, int? settlementStatus, DateTime? createTimeBegin, DateTime? createTimeEnd, string eRPSourceOrderCode, string sAPPurchasePlanCode, out string fileName) {
            fileName = GetExportFilePath("导出配件入库检验单(计划价).xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);
                var userInfo = Utils.GetCurrentUserInfo();
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.Append(@"select a.Code,
                                        a.WarehouseCode,
                                        a.WarehouseName,
                                        b.value As Inboundtype,
                                        (select sum(c.InspectedQuantity * c.costprice)
                                           from Partsinboundcheckbilldetail c
                                          where c.partsinboundcheckbillid = a.id) as totalAmout,
                                         a.OriginalRequirementBillCode,
                                         d.code as InboundPlanCode,
                                         e.name as PartsSalesCategoryName,
                                         a.BranchName,
                                         a.CounterpartCompanyCode,
                                         a.CounterpartCompanyName,
                                         a.GPMSPurOrderCode,
                                         a.ERPSourceOrderCode,
                                         a.SAPPurchasePlanCode
                                    From PartsInboundCheckBill a
                                    LEFT JOIN KeyValueItem b
                                      on a.Inboundtype = b.key
                                     and b.name = 'Parts_InboundType'
                                    LEFT JOIN PartsInboundPlan d
                                      on d.id = a.partsinboundplanid
                                    Left Join PartsSalesCategory e
                                      on e.id = a.partssalescategoryid and e.Status = 1 where 1=1 ");
                    var dbParameters = new List<DbParameter>();
                    if(ids != null && ids.Length > 0) {
                        sql.Append(" and a.id in (");
                        for(var i = 0; i < ids.Length; i++) {
                            if(ids.Length == i + 1) {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            } else {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                        }
                        sql.Append(")");
                    } else {
                        if(storageCompanyId.HasValue) {
                            sql.Append(" and a.StorageCompanyId = {0}storageCompanyId ");
                            dbParameters.Add(db.CreateDbParameter("storageCompanyId", userInfo.EnterpriseId));
                        }
                        if(!string.IsNullOrEmpty(partsInboundCheckBillCode)) {
                            sql.Append(" and LOWER(a.Code) like {0}partsInboundCheckBillCode ");
                            dbParameters.Add(db.CreateDbParameter("partsInboundCheckBillCode", "%" + partsInboundCheckBillCode.ToLower() + "%"));
                        }
                        if(!string.IsNullOrEmpty(eRPSourceOrderCode)) {
                            sql.Append(" and LOWER(a.ERPSourceOrderCode) like {0}eRPSourceOrderCode ");
                            dbParameters.Add(db.CreateDbParameter("eRPSourceOrderCode", "%" + eRPSourceOrderCode.ToLower() + "%"));
                        }

                        if(!string.IsNullOrEmpty(sAPPurchasePlanCode)) {
                            sql.Append(" and LOWER(a.SAPPurchasePlanCode) like {0}sAPPurchasePlanCode ");
                            dbParameters.Add(db.CreateDbParameter("sAPPurchasePlanCode", "%" + sAPPurchasePlanCode.ToLower() + "%"));
                        }
                        if(partsSalesCategoryId.HasValue) {
                            sql.Append(@" and a.partsSalesCategoryId = {0}partsSalesCategoryId ");
                            dbParameters.Add(db.CreateDbParameter("partsSalesCategoryId", partsSalesCategoryId.Value));
                        }
                        if(warehouseId.HasValue) {
                            sql.Append("and a.warehouseId = {0}warehouseId ");
                            dbParameters.Add(db.CreateDbParameter("warehouseId", warehouseId.Value));
                        }
                        if(inboundType.HasValue) {
                            sql.Append(" and a.inboundType ={0}inboundType ");
                            dbParameters.Add(db.CreateDbParameter("inboundType", inboundType.Value));
                        }
                        if(!string.IsNullOrEmpty(counterpartCompanyCode)) {
                            sql.Append(" and LOWER(a.counterpartCompanyCode) like {0}counterpartCompanyCode ");
                            dbParameters.Add(db.CreateDbParameter("counterpartCompanyCode", "%" + counterpartCompanyCode.ToLower() + "%"));
                        }
                        if(!string.IsNullOrEmpty(counterpartCompanyName)) {
                            sql.Append(" and LOWER(a.counterpartCompanyName) like {0}counterpartCompanyName ");
                            dbParameters.Add(db.CreateDbParameter("counterpartCompanyName", "%" + counterpartCompanyName.ToLower() + "%"));
                        }
                        if(settlementStatus.HasValue) {
                            sql.Append(" and a.settlementStatus ={0}settlementStatus ");
                            dbParameters.Add(db.CreateDbParameter("settlementStatus", settlementStatus.Value));
                        }
                        if(createTimeBegin.HasValue) {
                            sql.Append(@" and a.createTime >=To_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }


                        if(createTimeEnd.HasValue) {
                            sql.Append(@" and a.createTime <=To_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                    }
                    var sqlStr = sql.ToString();
                    if(sqlStr.IndexOf(" where 1=1  and ") > 0)
                        sqlStr = sqlStr.Replace(" where 1=1  and ", " where ");
                    else
                        sqlStr = sqlStr.Replace(" where 1=1 ", "");

                    var command = db.CreateDbCommand(string.Format(sqlStr, db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    ErrorStrings.Export_Title_PartsInboundCheckBill_Code, 
                                    ErrorStrings.Export_Title_Company_WarehouseCode, 
                                    ErrorStrings.Export_Title_Company_WarehouseName, 
                                    ErrorStrings.Export_Title_PackingTask_InboundType, 
                                    ErrorStrings.Export_Title_PartsInboundCheckBill_PlanPriceAmount, 
                                    ErrorStrings.Export_Title_PartsInboundCheckBill_OriginalRequirementBillCode,
                                    ErrorStrings.Export_Title_PartsInboundCheckBill_PartsInboundPlanCode,
                                    ErrorStrings.Export_Title_Partssalescategory_Name, 
                                    ErrorStrings.Export_Title_PartsBranch_BranchName, 
                                    ErrorStrings.Export_Title_BoxUpTask_CounterpartCompanyCode, 
                                    ErrorStrings.Export_Title_BoxUpTask_CounterpartCompanyName, 
                                    ErrorStrings.Export_Title_PartsPurchaseOrder_GPMSPurOrderCode,
                                    ErrorStrings.Export_Title_SupplierShippingOrder_ERPSourceOrderCode,
                                    ErrorStrings.Export_Title_PartsInboundCheckBill_SAPPurchasePlanCode
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();

                }
                return true;
            } catch(Exception) {
                return false;
            }
        }

        /// <summary>
        /// 合并导出配件入库检验单（计划价）
        /// </summary>
        /// <returns></returns>
        public bool MergeExportPartsInboundCheckBillWithDetailForPlannedPrice(int[] ids, int? storageCompanyId, string partsInboundCheckBillCode, int? partsSalesCategoryId, int? warehouseId, int? inboundType, string counterpartCompanyCode, string counterpartCompanyName, int? settlementStatus, DateTime? createTimeBegin, DateTime? createTimeEnd, string eRPSourceOrderCode, string sAPPurchasePlanCode, out string fileName) {
            fileName = GetExportFilePath("合并导出配件入库检验单(计划价).xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);
                var userInfo = Utils.GetCurrentUserInfo();
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.Append(@"select a.Code,
                                        a.WarehouseCode,
                                        a.WarehouseName,
                                        b.value As Inboundtype,
                                        (select sum(c.InspectedQuantity * c.costprice)
                                           from Partsinboundcheckbilldetail c
                                          where c.partsinboundcheckbillid = a.id) as totalAmout,
                                         a.OriginalRequirementBillCode,
                                         d.code as InboundPlanCode,
                                         e.name as PartsSalesCategoryName,
                                         a.BranchName,
                                         a.CounterpartCompanyCode,
                                         a.CounterpartCompanyName,
                                         a.Objid,
                                         a.GPMSPurOrderCode,
                                         f.Value as SettlementStatus,
                                         a.Remark,
                                         a.ERPSourceOrderCode,
                                         a.SAPPurchasePlanCode,
                                         a.CreatorName,
                                         a.CreateTime,
                                         a.ModifierName,
                                         a.ModifyTime,
                                         g.SparePartCode,
                                         g.SparePartName,
                                         g.WarehouseAreaCode,
                                         g.InspectedQuantity,
                                         g.CostPrice,
                                         g.Remark as DetailRemark
                                    From PartsInboundCheckBill a
                                    LEFT JOIN KeyValueItem b
                                      on a.Inboundtype = b.key
                                     and b.name = 'Parts_InboundType'
                                    LEFT JOIN PartsInboundPlan d
                                      on d.id = a.partsinboundplanid
                                    Left Join PartsSalesCategory e
                                      on e.id = a.partssalescategoryid and e.Status = 1
                                    LEFT JOIN KeyValueItem f
                                      on a.SettlementStatus = f.key
                                     and f.name = 'Parts_SettlementStatus'
                                    LEFT JOIN PartsInboundCheckBillDetail g 
                                      on a.id = g.PartsInboundCheckBillId where 1=1 ");
                    var dbParameters = new List<DbParameter>();
                    if(ids != null && ids.Length > 0) {
                        sql.Append(" and a.id in (");
                        for(var i = 0; i < ids.Length; i++) {
                            if(ids.Length == i + 1) {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            } else {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                        }
                        sql.Append(") order by a.code,g.sparepartcode desc");
                    } else {
                        if(storageCompanyId.HasValue) {
                            sql.Append(" and a.StorageCompanyId = {0}storageCompanyId ");
                            dbParameters.Add(db.CreateDbParameter("storageCompanyId", userInfo.EnterpriseId));
                        }
                        if(!string.IsNullOrEmpty(partsInboundCheckBillCode)) {
                            sql.Append(" and LOWER(a.Code) like {0}partsInboundCheckBillCode ");
                            dbParameters.Add(db.CreateDbParameter("partsInboundCheckBillCode", "%" + partsInboundCheckBillCode.ToLower() + "%"));
                        }
                        if(!string.IsNullOrEmpty(eRPSourceOrderCode)) {
                            sql.Append(" and LOWER(a.ERPSourceOrderCode) like {0}eRPSourceOrderCode ");
                            dbParameters.Add(db.CreateDbParameter("eRPSourceOrderCode", "%" + eRPSourceOrderCode.ToLower() + "%"));
                        }

                        if(!string.IsNullOrEmpty(sAPPurchasePlanCode)) {
                            sql.Append(" and LOWER(a.SAPPurchasePlanCode) like {0}sAPPurchasePlanCode ");
                            dbParameters.Add(db.CreateDbParameter("sAPPurchasePlanCode", "%" + sAPPurchasePlanCode.ToLower() + "%"));
                        }
                        if(partsSalesCategoryId.HasValue) {
                            sql.Append(@" and a.partsSalesCategoryId = {0}partsSalesCategoryId ");
                            dbParameters.Add(db.CreateDbParameter("partsSalesCategoryId", partsSalesCategoryId.Value));
                        }
                        if(warehouseId.HasValue) {
                            sql.Append(" and a.warehouseId = {0}warehouseId ");
                            dbParameters.Add(db.CreateDbParameter("warehouseId", warehouseId.Value));
                        }
                        if(inboundType.HasValue) {
                            sql.Append(" and a.inboundType ={0}inboundType ");
                            dbParameters.Add(db.CreateDbParameter("inboundType", inboundType.Value));
                        }
                        if(!string.IsNullOrEmpty(counterpartCompanyCode)) {
                            sql.Append(" and LOWER(a.counterpartCompanyCode) like {0}counterpartCompanyCode ");
                            dbParameters.Add(db.CreateDbParameter("counterpartCompanyCode", "%" + counterpartCompanyCode.ToLower() + "%"));
                        }
                        if(!string.IsNullOrEmpty(counterpartCompanyName)) {
                            sql.Append(" and LOWER(a.counterpartCompanyName) like {0}counterpartCompanyName ");
                            dbParameters.Add(db.CreateDbParameter("counterpartCompanyName", "%" + counterpartCompanyName.ToLower() + "%"));
                        }
                        if(settlementStatus.HasValue) {
                            sql.Append(" and a.settlementStatus ={0}settlementStatus ");
                            dbParameters.Add(db.CreateDbParameter("settlementStatus", settlementStatus.Value));
                        }
                        if(createTimeBegin.HasValue) {
                            sql.Append(@" and a.createTime >=To_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }


                        if(createTimeEnd.HasValue) {
                            sql.Append(@" and a.createTime <=To_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                        sql.Append(" order by a.code,g.sparepartcode desc");
                    }
                    var sqlStr = sql.ToString();
                    if(sqlStr.IndexOf(" where 1=1  and ") > 0)
                        sqlStr = sqlStr.Replace(" where 1=1  and ", " where ");
                    else
                        sqlStr = sqlStr.Replace(" where 1=1 ", "");

                    var command = db.CreateDbCommand(string.Format(sqlStr, db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    ErrorStrings.Export_Title_PartsInboundCheckBill_Code, 
                                    ErrorStrings.Export_Title_Company_WarehouseCode, 
                                    ErrorStrings.Export_Title_Company_WarehouseName, 
                                    ErrorStrings.Export_Title_PackingTask_InboundType, 
                                    ErrorStrings.Export_Title_PartsInboundCheckBill_PlanPriceAmount, 
                                    ErrorStrings.Export_Title_PartsInboundCheckBill_OriginalRequirementBillCode,
                                    ErrorStrings.Export_Title_PartsInboundCheckBill_PartsInboundPlanCode,
                                    ErrorStrings.Export_Title_Partssalescategory_Name, 
                                    ErrorStrings.Export_Title_PartsBranch_BranchName, 
                                    ErrorStrings.Export_Title_BoxUpTask_CounterpartCompanyCode, 
                                    ErrorStrings.Export_Title_BoxUpTask_CounterpartCompanyName, 
                                    ErrorStrings.Export_Title_PartsInboundCheckBill_WMSObjid,
                                    ErrorStrings.Export_Title_PartsPurchaseOrder_GPMSPurOrderCode,
                                    ErrorStrings.Export_Title_PartsInboundCheckBill_SettlementStatus,
                                    ErrorStrings.Export_Title_PartsBranch_Remark,
                                    ErrorStrings.Export_Title_SupplierShippingOrder_ERPSourceOrderCode,
                                    ErrorStrings.Export_Title_PartsInboundCheckBill_SAPPurchasePlanCode,
                                    ErrorStrings.Export_Title_AccountPeriod_CreatorName,
                                    ErrorStrings.Export_Title_AccountPeriod_CreateTime,
                                    ErrorStrings.Export_Title_AccountPeriod_ModifierName,
                                    ErrorStrings.Export_Title_AccountPeriod_ModifyTime,
                                    ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode,
                                    ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName,
                                    ErrorStrings.Export_Title_WarehouseArea_WarehouseAreaCode,
                                    ErrorStrings.Export_Title_PartsInboundCheckBill_InspectedQuantity,
                                    ErrorStrings.Export_Title_PartsPlannedPrice_PlannedPrice,
                                    ErrorStrings.Export_Title_PartsBranch_Remark
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();

                }
                return true;
            } catch(Exception) {
                return false;
            }
        }


        /// <summary>
        /// 导出工程车配件入库检验单查询
        /// </summary>
        /// <returns></returns>
        public bool 导出工程车配件入库检验单查询(int[] ids, string code, string partsInboundPlanCode, string name, string warehouseName, int? inboundType, int? settlementStatus, string ERPSourceOrderCode, string partsSalesOrderCode, string counterpartCompanyCode, string counterpartCompanyName, DateTime? createTimeBegin, DateTime? createTimeEnd, int? invoiceType, string originalRequirementBillCode, out string fileName) {
            fileName = GetExportFilePath("工程车配件入库检验单.xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.Append(@"select pic.code,
                                        pic.warehousecode,
                                        pic.WarehouseName,
                                        d.Value as InboundType,
                                        (SELECT SUM(picbd.SettlementPrice*picbd.InspectedQuantity) FROM gcdcs.PartsInboundCheckBillDetail picbd WHERE picbd.PartsInboundCheckBillId=pic.id) AS zongjine,
                                        pic.OriginalRequirementBillCode,
                                        pip.code as PartsInboundPlanCode,
                                        psc.name,
                                        pic.BranchName,
                                        pic.CounterpartCompanyCode,
                                        pic.Counterpartcompanyname,
                                        pic.Objid,
                                        pic.GPMSPurOrderCode,
                                        e.Value as SettlementStatus,
                                        pic.remark,
                                        pso.ERPSourceOrderCode,
                                        pso.code as PartsSalesOrderCode,
                                        f.Value as InvoiceInformation_Type,
                                        pic.Creatorname,
                                        pic.createtime,
                                        pic.ModifierName,
                                        pic.modifytime
                                        from gcdcs.PartsInboundCheckBill pic 
                                        inner join gcdcs.PartsInboundPlan pip on pip.id=pic.PartsInboundPlanId
                                        inner join gcdcs.PartsSalesCategory psc on psc.id=pic.PartsSalesCategoryId
                                        inner join gcdcs.PartsSalesOrder pso on pso.id=pic.OriginalRequirementBillId and pic.Originalrequirementbilltype=1
                                    LEFT JOIN KeyValueItem d
                                      on pic.InboundType = d.key
                                     and d.name = 'Parts_InboundType'
                                    LEFT JOIN KeyValueItem e
                                      on pic.SettlementStatus = e.key
                                     and e.name = 'Parts_SettlementStatus'
                                    LEFT JOIN KeyValueItem f
                                      on pso.InvoiceType = f.key
                                     and f.name = 'ERPInvoiceInformation_Type' where 1=1 ");
                    var dbParameters = new List<DbParameter>();
                    if(ids != null && ids.Length > 0) {
                        sql.Append(" and pic.id in (");
                        for(var i = 0; i < ids.Length; i++) {
                            if(ids.Length == i + 1) {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            } else {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                        }
                        sql.Append(") order by pic.code desc");
                    } else {
                        if(!string.IsNullOrEmpty(code)) {
                            sql.Append(" and pic.code = {0}code ");
                            dbParameters.Add(db.CreateDbParameter("code", code));
                        }
                        if(!string.IsNullOrEmpty(partsInboundPlanCode)) {
                            sql.Append(" and pip.code = {0}partsInboundPlanCode ");
                            dbParameters.Add(db.CreateDbParameter("partsInboundPlanCode", partsInboundPlanCode));
                        }
                        if(!string.IsNullOrEmpty(name)) {
                            sql.Append(@" and psc.name = {0}name ");
                            dbParameters.Add(db.CreateDbParameter("name", name));
                        }
                        if(!string.IsNullOrEmpty(warehouseName)) {
                            sql.Append(" and pic.WarehouseName = {0}warehouseName ");
                            dbParameters.Add(db.CreateDbParameter("warehouseName", warehouseName));
                        }
                        if(inboundType.HasValue) {
                            sql.Append(" and pic.InboundType ={0}inboundType ");
                            dbParameters.Add(db.CreateDbParameter("inboundType", inboundType.Value));
                        }
                        if(settlementStatus.HasValue) {
                            sql.Append(" and pic.SettlementStatus = {0}settlementStatus ");
                            dbParameters.Add(db.CreateDbParameter("settlementStatus", settlementStatus.Value));
                        }
                        if(!string.IsNullOrEmpty(ERPSourceOrderCode)) {
                            sql.Append(" and pso.ERPSourceOrderCode = {0}ERPSourceOrderCode ");
                            dbParameters.Add(db.CreateDbParameter("ERPSourceOrderCode", ERPSourceOrderCode));
                        }
                        if(settlementStatus.HasValue) {
                            sql.Append(" and pic.settlementStatus ={0}settlementStatus ");
                            dbParameters.Add(db.CreateDbParameter("settlementStatus", settlementStatus.Value));
                        }
                        if(!string.IsNullOrEmpty(partsSalesOrderCode)) {
                            sql.Append(" and pso.code = {0}partsSalesOrderCode ");
                            dbParameters.Add(db.CreateDbParameter("partsSalesOrderCode", partsSalesOrderCode));
                        }
                        if(!string.IsNullOrEmpty(counterpartCompanyCode)) {
                            sql.Append(" and pic.counterpartCompanyCode = {0}counterpartCompanyCode ");
                            dbParameters.Add(db.CreateDbParameter("counterpartCompanyCode", counterpartCompanyCode));
                        }
                        if(!string.IsNullOrEmpty(counterpartCompanyName)) {
                            sql.Append(" and pic.counterpartCompanyName = {0}counterpartCompanyName ");
                            dbParameters.Add(db.CreateDbParameter("counterpartCompanyName", counterpartCompanyName));
                        }


                        if(createTimeBegin.HasValue) {
                            sql.Append(@" and pic.createTime >=To_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }


                        if(createTimeEnd.HasValue) {
                            sql.Append(@" and pic.createTime <=To_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }

                        if(invoiceType.HasValue) {
                            sql.Append(" and pso.InvoiceType ={0}invoiceType ");
                            dbParameters.Add(db.CreateDbParameter("invoiceType", invoiceType.Value));
                        }
                        if(!string.IsNullOrEmpty(originalRequirementBillCode)) {
                            sql.Append(" and pic.OriginalRequirementBillCode = {0}originalRequirementBillCode ");
                            dbParameters.Add(db.CreateDbParameter("originalRequirementBillCode", originalRequirementBillCode));
                        }
                        sql.Append(" order by pic.code desc");
                    }
                    var sqlStr = sql.ToString();
                    if(sqlStr.IndexOf(" where 1=1  and ") > 0)
                        sqlStr = sqlStr.Replace(" where 1=1  and ", " where ");
                    else
                        sqlStr = sqlStr.Replace(" where 1=1 ", "");
                    var command = db.CreateDbCommand(string.Format(sqlStr, db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    ErrorStrings.Export_Title_PartsInboundCheckBill_Code, 
                                    ErrorStrings.Export_Title_Company_WarehouseCode, 
                                    ErrorStrings.Export_Title_Company_WarehouseName, 
                                    ErrorStrings.Export_Title_PackingTask_InboundType, 
                                    ErrorStrings.Export_Title_PartsInboundCheckBill_PlanPriceAmount, 
                                    ErrorStrings.Export_Title_PartsInboundCheckBill_OriginalRequirementBillCode,
                                    ErrorStrings.Export_Title_PartsInboundCheckBill_PartsInboundPlanCode,
                                    ErrorStrings.Export_Title_Partssalescategory_Name, 
                                    ErrorStrings.Export_Title_PartsBranch_BranchName, 
                                    ErrorStrings.Export_Title_BoxUpTask_CounterpartCompanyCode, 
                                    ErrorStrings.Export_Title_BoxUpTask_CounterpartCompanyName, 
                                    ErrorStrings.Export_Title_PartsInboundCheckBill_WMSObjid,
                                    ErrorStrings.Export_Title_PartsPurchaseOrder_GPMSPurOrderCode,
                                    ErrorStrings.Export_Title_PartsInboundCheckBill_SettlementStatus,
                                    ErrorStrings.Export_Title_PartsBranch_Remark,
                                    ErrorStrings.Export_Title_SupplierShippingOrder_ERPSourceOrderCode,
                                    ErrorStrings.Export_Title_SupplierShippingOrder_SalesOrderCode,
                                    ErrorStrings.Export_Title_PartsInboundCheckBill_BillingType,
                                    ErrorStrings.Export_Title_AccountPeriod_CreatorName,
                                    ErrorStrings.Export_Title_AccountPeriod_CreateTime,
                                    ErrorStrings.Export_Title_AccountPeriod_ModifierName,
                                    ErrorStrings.Export_Title_AccountPeriod_ModifyTime
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();

                }
                return true;
            } catch(Exception) {
                return false;
            }
        }

        public bool 导出历史入库记录查询(string[] codes, int? branchId, string partsInboundOrderCode, int? inboundType, int? partsSalesCategoryId, int? partsPurchaseOrderTypeId, string originalRequirementBillCode, int? warehouseId, int? settlementstatus, string partCode, string partName, string companyName, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName) {
            fileName = GetExportFilePath("导出历史入库记录查询_" + ".xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);
                using(var conn = db.CreateDbConnection()) {
                    var sql = new StringBuilder();
                    sql.Append(@"select 
                                     c.Code as WarehouseCode, --仓库编号
                                     c.Name as WarehouseName, --仓库名称
                                     psc.name as PartsSalesCategoryName,
                                     a.code as PartsInboundOrderCode, --入库单编号
                                     keyvalue.value as InboundType,--入库类型
                                     a.OriginalRequirementBillCode as OriginalRequirementBillCode,--源单据编号
                                     ppot.name as PartsPurchaseOrderTypeName,--采购订单类型
                                     a.CounterpartCompanyCode as CompanyCode,--对方单位编号
                                     a.CounterpartCompanyName as CompanyName,--对方单位名称
                                     a.CreateTime as Createtime,--创建时间
                                     /*case when a.settlementstatus = 1 then 1 when a.settlementstatus = 2 then 2 when jsd.status = 1 then 3 when jsd.status = 2 then 4 
                                          when jsd.status = 3 then 5 when jsd.status = 5 then 6 when jsd.status = 6 then 7 end as settlementstatus, --结算状态*/
                                     case when a.settlementstatus = 1 then '不结算' when a.settlementstatus = 2 then '待结算' when jsd.status = 1 then '新建'
                                          when jsd.status = 2 then '已审批' when jsd.status = 3 then '发票登记' when jsd.status = 5 then '发票驳回' 
                                          when jsd.status = 6 then '发票已审核' end as settlementstatus,--结算状态
                                     f.code as PartCode,--配件编号
                                     f.name as PartName,--配件名称
                                     sum(b.InspectedQuantity) as InQty,--入库数量
                                     b.CostPrice,
                                     sum(b.inspectedquantity * b.CostPrice) as TotalJH,
                                     b.SettlementPrice as Price,
                                     sum(b.InspectedQuantity * b.Settlementprice) as Total,
                                     jsd.code as JsdNum
                                     from PartsInboundCheckBill a
                                     left join (
                                            select ppsb.status,ppsr.sourcecode,ppsb.code from PartsPurchaseSettleRef ppsr 
                                                   left join PartsPurchaseSettleBill ppsb on ppsr.PartsPurchaseSettleBillId = ppsb.id
                                                   where ppsb.status in (1,2,3,5,6)
                                       ) jsd on jsd.sourcecode = a.code
                                      inner join partsinboundcheckbilldetail b on a.id = b.partsinboundcheckbillid
                                      inner join warehouse c on c.id = a.warehouseid
                                      inner join partsinboundplan d on d.id = a.partsinboundplanid
                                      inner join sparepart f on f.id = b.sparepartid
                                      inner join keyvalueitem keyvalue on keyvalue.name='Parts_InboundType' and keyvalue.key = a.inboundtype
                                       left join PartsPurchaseOrder e on e.code =d.OriginalRequirementBillCode
                                       left join PartsPurchaseOrderType ppot on e.PartsPurchaseOrderTypeId = ppot.id
                                       left join partssalesorder g on g.code = d.OriginalRequirementBillCode
                                       left join PartsSalesCategory psc on a.PartsSalesCategoryId = psc.id
                                      where  a.storagecompanyid in (2,2203,8481, 11181,12261)  
                                      ");
                    if(branchId.HasValue) {
                        sql.AppendFormat(" and a.branchId={0} ", branchId);
                    }
                    var dbParameters = new List<DbParameter>();
                    #region 条件过滤
                    if(codes != null && codes.Length > 0) {
                        sql.Append(" and a.code in (");
                        for(var i = 0; i < codes.Length; i++) {
                            if(codes.Length == i + 1) {
                                sql.Append("{0}" + codes[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter(codes[i].ToString(CultureInfo.InvariantCulture), codes[i]));
                            } else {
                                sql.Append("{0}" + codes[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter(codes[i].ToString(CultureInfo.InvariantCulture), codes[i]));
                            }
                        }
                        sql.Append(")");
                    } else {
                        if(inboundType.HasValue) {
                            sql.Append(" and a.InboundType={0}InboundType");
                            dbParameters.Add(db.CreateDbParameter("InboundType", inboundType.Value));
                        }
                        if(partsSalesCategoryId.HasValue) {
                            sql.Append(" and a.PartsSalesCategoryId={0}PartsSalesCategoryId");
                            dbParameters.Add(db.CreateDbParameter("PartsSalesCategoryId", partsSalesCategoryId.Value));
                        }
                        if(partsPurchaseOrderTypeId.HasValue) {
                            sql.Append(" and e.partsPurchaseOrderTypeId={0}partsPurchaseOrderTypeId");
                            dbParameters.Add(db.CreateDbParameter("partsPurchaseOrderTypeId", partsPurchaseOrderTypeId.Value));
                        }
                        if(settlementstatus.HasValue) {
                            sql.Append(" and a.settlementstatus={0}settlementstatus");
                            dbParameters.Add(db.CreateDbParameter("settlementstatus", settlementstatus.Value));
                        }
                        if(warehouseId.HasValue) {
                            sql.Append(" and c.Id={0}warehouseId");
                            dbParameters.Add(db.CreateDbParameter("warehouseId", warehouseId.Value));
                        }
                        if(!string.IsNullOrEmpty(partsInboundOrderCode)) {
                            sql.Append(" and a.code like {0}code ");
                            dbParameters.Add(db.CreateDbParameter("code", "%" + partsInboundOrderCode + "%"));
                        }
                        if(!string.IsNullOrEmpty(originalRequirementBillCode)) {
                            sql.Append(" and a.OriginalRequirementBillCode like {0}OriginalRequirementBillCode ");
                            dbParameters.Add(db.CreateDbParameter("OriginalRequirementBillCode", "%" + originalRequirementBillCode + "%"));
                        }
                        if(!string.IsNullOrEmpty(partCode)) {
                            sql.Append(" and f.Code={0}PartCode");
                            dbParameters.Add(db.CreateDbParameter("PartCode", partCode));
                        }
                        if(!string.IsNullOrEmpty(partName)) {
                            sql.Append(" and f.Name={0}PartName");
                            dbParameters.Add(db.CreateDbParameter("PartName", partName));
                        }
                        if(!string.IsNullOrEmpty(companyName)) {
                            sql.Append(" and a.CounterpartCompanyName={0}CounterpartCompanyName");
                            dbParameters.Add(db.CreateDbParameter("CounterpartCompanyName", companyName));
                        }
                        if(createTimeBegin.HasValue) {
                            sql.Append(@" and a.createTime >=To_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(@" and a.createTime <=To_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                    }
                    #endregion

                    sql.Append(@"group by c.code, c.name,psc.name, a.code,keyvalue.value,a.OriginalRequirementBillCode,ppot.name, a.counterpartcompanycode, a.counterpartcompanyname, a.createtime,a.settlementstatus,jsd.status,f.code, f.name,
                                 b.CostPrice,b.settlementprice,jsd.code ");

                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    conn.Open();
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {ErrorStrings.Export_Title_PartsInboundCheckBill_WarehouseCode,
                                                     ErrorStrings.Export_Title_Company_WarehouseName,
                                                     ErrorStrings.Export_Title_Partssalescategory_Name,
                                                     ErrorStrings.Export_Title_PartsInboundCheckBill_PartsInboundCode,
                                                     ErrorStrings.Export_Title_PartsInboundCheckBill_PartsInboundCodeType,
                                                     ErrorStrings.Export_Title_PartsOuterPurchaseChange_SourceCode,
                                                     ErrorStrings.Export_Title_PartsInboundCheckBill_PurchaseType,
                                                     ErrorStrings.Export_Title_PartsInboundCheckBill_CompanyCode,
                                                     ErrorStrings.Export_Title_PartsInboundCheckBill_CompanyName,
                                                     ErrorStrings.Export_Title_PartsInboundCheckBill_InBoundTime,
                                                     ErrorStrings.Export_Title_PartsInboundCheckBill_SettlementStatus,
                                                     ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode,
                                                     ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName,
                                                     ErrorStrings.Export_Title_PartsInboundCheckBill_InBoundQty,
                                                     ErrorStrings.Export_Title_FactoryPurchacePrice_PurchasingPrice,
                                                     ErrorStrings.Export_Title_PartsInboundCheckBill_PurChaseAmount,
                                                     ErrorStrings.Export_Title_PartsPlannedPrice_PlannedPrice,
                                                     ErrorStrings.Export_Title_PartsInboundCheckBill_PlanAmount,
                                                     ErrorStrings.Export_Title_PartsInboundCheckBill_SettlementNo};
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }

        public bool 导出代理库入库记录查询(string[] codes, int? branchId, int? partsSalesCategoryId, int? warehouseId, int? inboundType, string storageCompanyCode, string storageCompanyName, string partCode, string partName, DateTime? startCreateTime, DateTime? endCreateTimeout, out string fileName) {
            fileName = GetExportFilePath("代理库出库记录查询_" + ".xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);
                using(var conn = db.CreateDbConnection()) {
                    var sql = new StringBuilder();
                    sql.Append(@" select  
                                       kv1.value as InboundType,    
                                       a.StorageCompanyCode as StorageCompanyCode, --仓储企业编号
                                       a.storagecompanyname as StorageCompanyName,--仓储企业名称
                                       c.provincename as ProvinceName,
                                       a.warehousecode as  WarehouseCode, --仓库编号
                                       a.warehousename as WarehouseName,--仓库名称
                                       psc.name as PartsSalesCategoryName,
                                       a.code as PartsInboundPlanCode,--入库单编号
                                       a.CreateTime as PartsInboundPlanTime,--入库单时间
                                       a.OriginalRequirementBillCode as  OriginalRequirementBillCode, --原始需求单据编号
                                       kv2.value as  OriginalRequirementBillType,--原始需求单据类型
                                       b.warehouseareacode as WarehouseAreaCode,--库位
                                       b.SparePartCode as PartCode,--配件编号
                                       b.sparepartname as PartName,--配件名称
                                       b.InspectedQuantity as InspectedQuantity, --入库数量
                                       b.SettlementPrice as SettlementPrice, --代理库批发价
                                       (b.InspectedQuantity*b.SettlementPrice) as Price, --入库金额
                                       kv3.value as SettlementStatus,
                                       a.BranchName as BranchName,--分公司名称
                                       a.CounterpartCompanyCode  as CounterPartCompanyCode, --对方单位编号
                                       a.CounterpartCompanyName  as CounterPartCompanyName  --对方单位名称
                                  from partsinboundcheckbill a
                                  left join partssalesorder pso on pso.code = a.originalrequirementbillcode
                                  left join partssalescategory psc on a.partssalescategoryid = psc.id
                                  left join company c on a.StorageCompanyCode = c.code
                                  left join keyvalueitem kv1 on kv1.name='Parts_InboundType' and kv1.key = a.InboundType
                                  left join keyvalueitem kv2 on kv2.name='OriginalRequirementBill_Type' and kv2.key = pso.OriginalRequirementBillType
                                  left join keyvalueitem kv3 on kv3.name='Parts_SettlementStatus' and kv3.key =  a.SettlementStatus
                                 inner join partsinboundcheckbilldetail b on a.id = b.partsinboundcheckbillid
                                 where a.StorageCompanytype in (3,7)
                                      ");
                    if(branchId.HasValue) {
                        sql.AppendFormat(" and a.branchId={0} ", branchId);
                    }
                    var dbParameters = new List<DbParameter>();
                    #region 条件过滤
                    if(codes != null && codes.Length > 0) {
                        sql.Append(" and a.code in (");
                        for(var i = 0; i < codes.Length; i++) {
                            if(codes.Length == i + 1) {
                                sql.Append("{0}" + codes[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter(codes[i].ToString(CultureInfo.InvariantCulture), codes[i]));
                            } else {
                                sql.Append("{0}" + codes[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter(codes[i].ToString(CultureInfo.InvariantCulture), codes[i]));
                            }
                        }
                        sql.Append(")");
                    } else {
                        if(partsSalesCategoryId.HasValue) {
                            sql.Append(" and a.partsSalesCategoryId={0}partsSalesCategoryId");
                            dbParameters.Add(db.CreateDbParameter("partsSalesCategoryId", partsSalesCategoryId.Value));
                        }
                        if(inboundType.HasValue) {
                            sql.Append(" and a.inboundType={0}inboundType");
                            dbParameters.Add(db.CreateDbParameter("inboundType", inboundType.Value));
                        }
                        if(warehouseId.HasValue) {
                            sql.Append(" and a.warehouseId={0}warehouseId");
                            dbParameters.Add(db.CreateDbParameter("warehouseId", warehouseId.Value));
                        }
                        if(!string.IsNullOrEmpty(storageCompanyCode)) {
                            sql.Append(" and a.StorageCompanyCode like {0}StorageCompanyCode ");
                            dbParameters.Add(db.CreateDbParameter("StorageCompanyCode", "%" + storageCompanyCode + "%"));
                        }
                        if(!string.IsNullOrEmpty(storageCompanyName)) {
                            sql.Append(" and a.StorageCompanyName like {0}StorageCompanyName ");
                            dbParameters.Add(db.CreateDbParameter("StorageCompanyName", "%" + storageCompanyName + "%"));
                        }
                        if(!string.IsNullOrEmpty(partCode)) {
                            sql.Append(" and b.SparePartCode={0}SparePartCode");
                            dbParameters.Add(db.CreateDbParameter("SparePartCode", partCode));
                        }
                        if(!string.IsNullOrEmpty(partName)) {
                            sql.Append(" and b.sparepartname={0}sparepartname");
                            dbParameters.Add(db.CreateDbParameter("sparepartname", partName));
                        }
                        if(startCreateTime.HasValue) {
                            sql.Append(@" and a.createTime >=To_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = startCreateTime.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(endCreateTimeout.HasValue) {
                            sql.Append(@" and a.createTime <=To_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = endCreateTimeout.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                    }
                    #endregion

                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    conn.Open();
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {ErrorStrings.Export_Title_PackingTask_InboundType,
                                                     ErrorStrings.Export_Title_PartsInboundCheckBill_StorageCompanyCode,
                                                     ErrorStrings.Export_Title_Partshistorystock_StorageCompanyName,
                                                     ErrorStrings.Export_Validation_Company_ProvinceName,
                                                     ErrorStrings.Export_Title_Company_WarehouseCode,
                                                     ErrorStrings.Export_Title_Company_WarehouseName,
                                                     ErrorStrings.Export_Title_Partssalescategory_Name,
                                                     ErrorStrings.Export_Title_PartsInboundCheckBill_PartsInboundCode,
                                                     ErrorStrings.Export_Title_PartsInboundCheckBill_InBoundTime,
                                                     ErrorStrings.Export_Title_PartsInboundCheckBill_OriginalRequirementBillCode,
                                                     ErrorStrings.Export_Title_PartsInboundCheckBill_OriginalRequirementBillType,
                                                     ErrorStrings.Export_Title_WarehouseAreaCategory_AreaKindCode,
                                                     ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode,
                                                     ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName,
                                                     ErrorStrings.Export_Title_PartsInboundCheckBill_InBoundQty,
                                                     ErrorStrings.Export_Title_PartsInboundCheckBill_BatchAmount,
                                                     ErrorStrings.Export_Title_PartsInboundCheckBill_InBoundAmount,
                                                     ErrorStrings.Export_Title_PartsInboundCheckBill_SettlementStatus,
                                                     ErrorStrings.Export_Title_Branch_BranchName,
                                                     ErrorStrings.Export_Title_BoxUpTask_CounterpartCompanyCode,
                                                     ErrorStrings.Export_Title_BoxUpTask_CounterpartCompanyName};
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }

        /// <summary>
        /// 合并导出配件入库检验主清单
        /// </summary>
        public bool ExportPartsInboundCheckBillWithDetailForWarehouse(int[] ids, string originalRequirementBillCode, int? userId, int? storageCompanyId, string partsInboundCheckBillCode, int? warehouseId, int? counterpartCompanyId, string counterpartcompanyname, int? inboundType, int? settlementStatus, DateTime? createTimeBegin, DateTime? createTimeEnd, string sparePartCode, string sparePartName, string partsInboundPlanCode, string batchNumber,bool? hasDifference, out string fileName) {
            fileName = GetExportFilePath("配件入库检验主清单.xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);
                var userInfo = Utils.GetCurrentUserInfo();
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.Append(@"Select a.Code,
                                a.Warehousename,            
                                (select value from keyvalueitem where NAME = 'Parts_InboundType'and key=a.Inboundtype) As Inboundtype,
                                a.Counterpartcompanycode,
                                a.Counterpartcompanyname,
  (Select Sum(m.RetailGuidePrice * t.Inspectedquantity)  From Partsinboundcheckbilldetail t  join PartsRetailGuidePrice m    on t.sparepartid = m.sparepartid  and m.status = 1 
                                          Where t.Partsinboundcheckbillid = a.id and m.branchid = a.branchid and m.PartsSalesCategoryId = a.partssalescategoryid), 
                                p.Name,
                                a.Originalrequirementbillcode,
                                c.Code As Partsinboundplanidcode,   
                                d.Name As Partssalescategoryname,
                                a.Branchname,
                                a.Warehousecode,             
                                po.PlanSource,      
                                (select value from keyvalueitem where NAME = 'Parts_SettlementStatus'and key=a.Settlementstatus) As Settlementstatus,
                                (select value from keyvalueitem where NAME = 'PartsSalesReturn_ReturnType'and key=f.ReturnType) As ReturnType, 
                                a.Remark,
                                a.Creatorname,
                                a.Createtime,
                                a.Modifiername,
                                a.Modifytime,
                                (select value from keyvalueitem where NAME = 'PartsShipping_Method'and key=sso.ShippingMethod) As ShippingMethod,
                                b.Sparepartcode,
                                b.Sparepartname,
                                b.Warehouseareacode,
                                ( Select Min(Extent2.Code) As A1
                           From Partsstock Extent1
                          Inner Join Warehousearea Extent2
                             On Extent1.Warehouseareaid = Extent2.Id
                          Inner Join Warehouseareacategory Extent3
                             On Extent2.Areacategoryid = Extent3.Id
                          Where (((Extent1.Warehouseid = a.Warehouseid)and Extent1.partid=b.Sparepartid) And
                                (Extent2.Areakind = 3))
                            And (Extent3.Category = 1)),
                               b.Inspectedquantity,
                               pt.RetailGuidePrice,
                               s.Measureunit,
                               b.Remark As Remarkdetails,
                               b.SpareOrderRemark,
                               b.batchNumber,
                               case nvl(a.IsDifference, 0)
                                    when 0 then cast('否' as varchar2(100)) else cast('是' as varchar2(100)) end as HasDifference  
                          From Partsinboundcheckbill a
                           left join PartsPurchaseOrder po
                             on a.OriginalRequirementBillCode=po.Code
                           left join PartsPurchaseOrderType p
                              on po.PartsPurchaseOrderTypeId=p.Id
                          Left Outer Join Partsinboundcheckbilldetail b
                            On a.Id = b.Partsinboundcheckbillid
                          Left Outer Join Partsinboundplan c
                            On a.Partsinboundplanid = c.Id
                          left outer join SupplierShippingOrder sso
                            on c.sourceid=sso.id and c.SourceCode=sso.Code
                          Left Outer Join Partssalescategory d
                            On a.Partssalescategoryid = d.Id
                          Left Outer Join PartsInboundPlan e
                             On a.PartsInboundPlanId = e.Id
                          Left Outer Join PartsSalesReturnBill f
                             On e.SourceId = f.Id   
                          Left Outer Join sparepart s
                             On b.Sparepartid = s.Id 
                         left  join PartsRetailGuidePrice pt on a.BranchId=pt.BranchId and a.PartsSalesCategoryId=pt.PartsSalesCategoryId and b.SparePartId=pt.SparePartId and pt.Status=1 
                          where 1=1 ");
                    var dbParameters = new List<DbParameter>();
                    if(ids != null && ids.Length > 0) {
                        sql.Append(" and a.id in (");
                        for(var i = 0; i < ids.Length; i++) {
                            if(ids.Length == i + 1) {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            } else {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                        }
                        sql.Append(")");
                    } else {
                        if(storageCompanyId.HasValue) {
                            sql.Append(" and a.storageCompanyId = {0}storageCompanyId ");
                            dbParameters.Add(db.CreateDbParameter("storageCompanyId", userInfo.EnterpriseId));
                        }
                        if(userId.HasValue) {
                            sql.Append(@" and (Exists (Select 1 As C1
                  From Warehouseoperator Extent9
                 Where Extent9.Operatorid = {0}userId
                   And Extent9.Warehouseid = a.Warehouseid)) ");
                            dbParameters.Add(db.CreateDbParameter("userId", userInfo.Id));
                        }
                        if (hasDifference == true) { 
                            sql.Append(" and exists(select 1 from PackingTask pt inner join PartsDifferenceBackBill pdb on pt.code = pdb.sourcecode and pdb.type=2 and pdb.status<>99 where pt.PartsInboundCheckBillCode = a.code) "); 
                        } 
                        if (hasDifference == false) { 
                            sql.Append(" and not exists(select 1 from PackingTask pt inner join PartsDifferenceBackBill pdb on pt.code = pdb.sourcecode and pdb.type=2 and pdb.status<>99 where pt.PartsInboundCheckBillCode = a.code) "); 
                        }
                        if(!string.IsNullOrEmpty(partsInboundCheckBillCode)) {
                            sql.Append(" and LOWER(a.Code) like {0}partsInboundCheckBillCode ");
                            dbParameters.Add(db.CreateDbParameter("partsInboundCheckBillCode", "%" + partsInboundCheckBillCode.ToLower() + "%"));
                        }
                        if(!string.IsNullOrEmpty(originalRequirementBillCode)) {
                            sql.Append(" and LOWER(a.originalRequirementBillCode) like {0}originalRequirementBillCode ");
                            dbParameters.Add(db.CreateDbParameter("originalRequirementBillCode", "%" + originalRequirementBillCode.ToLower() + "%"));
                        }


                        if(warehouseId.HasValue) {
                            sql.Append(" and a.warehouseId = {0}warehouseId ");
                            dbParameters.Add(db.CreateDbParameter("warehouseId", warehouseId.Value));
                        }
                        if(!string.IsNullOrEmpty(partsInboundPlanCode)) {
                            sql.Append(" and LOWER(c.code) like {0}partsInboundPlanCode ");
                            dbParameters.Add(db.CreateDbParameter("partsInboundPlanCode", "%" + partsInboundPlanCode.ToLower() + "%"));
                        }
                        if(counterpartCompanyId.HasValue) {
                            sql.Append(" and a.counterpartCompanyId = {0}counterpartCompanyId ");
                            dbParameters.Add(db.CreateDbParameter("counterpartCompanyId", counterpartCompanyId.Value));
                        }

                        if(!string.IsNullOrEmpty(counterpartcompanyname)) {
                            sql.Append(" and LOWER(a.counterpartcompanyname) like {0}counterpartcompanyname ");
                            dbParameters.Add(db.CreateDbParameter("counterpartcompanyname", "%" + counterpartcompanyname.ToLower() + "%"));
                        }
                        if(inboundType.HasValue) {
                            sql.Append(" and a.inboundType ={0}inboundType ");
                            dbParameters.Add(db.CreateDbParameter("inboundType", inboundType.Value));
                        }


                        if(settlementStatus.HasValue) {
                            sql.Append(" and a.settlementStatus ={0}settlementStatus ");
                            dbParameters.Add(db.CreateDbParameter("settlementStatus", settlementStatus.Value));
                        }

                        if(createTimeBegin.HasValue) {
                            sql.Append(@" and a.createTime >=To_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }


                        if(createTimeEnd.HasValue) {
                            sql.Append(@" and a.createTime <=To_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }


                        if(!string.IsNullOrEmpty(sparePartName)) {
                            sql.Append(" and LOWER(b.sparePartName) like {0}sparePartName ");
                            dbParameters.Add(db.CreateDbParameter("sparePartName", "%" + sparePartName.ToLower() + "%"));
                        } if(!string.IsNullOrEmpty(sparePartCode)) {
                            sql.Append(" and LOWER(b.sparePartCode) like {0}sparePartCode ");
                            dbParameters.Add(db.CreateDbParameter("sparePartCode", "%" + sparePartCode.ToLower() + "%"));
                        }
                        if(!string.IsNullOrEmpty(batchNumber)) {
                            sql.Append(" and LOWER(b.batchNumber) like {0}batchNumber ");
                            dbParameters.Add(db.CreateDbParameter("batchNumber", "%" + batchNumber.ToLower() + "%"));
                        }
                    }
                    var sqlStr = sql.ToString();
                    if(sqlStr.IndexOf(" where 1=1  and ") > 0)
                        sqlStr = sqlStr.Replace(" where 1=1  and ", " where ");
                    else
                        sqlStr = sqlStr.Replace(" where 1=1 ", "");
                    var command = db.CreateDbCommand(string.Format(sqlStr, db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                   ErrorStrings.Export_Title_PartsInboundCheckBill_Code, ErrorStrings.Export_Title_Company_WarehouseName, ErrorStrings.Export_Title_PartsInboundCheckBill_InBoundType,ErrorStrings.Export_Title_BoxUpTask_CounterpartCompanyCode, ErrorStrings.Export_Title_BoxUpTask_CounterpartCompanyName,ErrorStrings.Export_Title_InternalAllocationBill_TotalAmount,ErrorStrings.Export_Title_PartsInboundCheckBill_PurchaseType, ErrorStrings.Export_Title_PartsInboundCheckBill_OriginalRequirementBillCode, ErrorStrings.Export_Title_PartsInboundCheckBill_PartsInboundPlanCode, ErrorStrings.Export_Title_Partssalescategory_Name, ErrorStrings.Export_Title_PartsBranch_BranchName, ErrorStrings.Export_Title_Company_WarehouseCode, ErrorStrings.Export_Title_PartsPurchaseOrder_PlanSource,ErrorStrings.Export_Title_PartsInboundCheckBill_SettlementStatus,ErrorStrings.Export_Title_PartsInboundCheckBill_ReturnType, ErrorStrings.Export_Title_PartsBranch_Remark, ErrorStrings.Export_Title_AccountPeriod_CreatorName, ErrorStrings.Export_Title_AccountPeriod_CreateTime, ErrorStrings.Export_Title_AccountPeriod_ModifierName, ErrorStrings.Export_Title_AccountPeriod_ModifyTime, ErrorStrings.Export_Title_PartsPurchaseOrder_ShippingMethod,ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode, ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName, ErrorStrings.Export_Title_WarehouseArea_WarehouseAreaCode,ErrorStrings.Export_Title_PartsInboundCheckBill_TargetLocation, ErrorStrings.Export_Title_PartsInboundCheckBill_InspectedQuantity, ErrorStrings.Export_Title_PartsPurchaseRtnSettleBill_SettlementPrice, ErrorStrings.Export_Title_PackingPropertyApp_MeasureUnit, ErrorStrings.Export_Title_PartsPurchaseOrder_DetailRemark,ErrorStrings.Export_Title_SupplierShippingOrder_DetailRemark,"批次号"
                                    ,ErrorStrings.Export_Title_PackingTask_HasDifference
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();

                }
                return true;
            } catch(Exception) {
                return false;
            }
        }

        /// <summary>
        /// 供应商合并导出配件入库检验主清单
        /// </summary>
        public bool ExportPartsInboundCheckBillWithDetailForSupplier(int[] ids, string originalRequirementBillCode, int? userId, int? partsSalesCategoryId, int? storageCompanyId, string partsInboundCheckBillCode, int? warehouseId, int? counterpartCompanyId, string counterpartcompanyname, string eRPOrderCode, string partsSalesOrderCode, int? partsSalesOrderInvoiceType, int? inboundType, int? settlementStatus, DateTime? createTimeBegin, DateTime? createTimeEnd, string sAPPurchasePlanCode, string cpPartsPurchaseOrderCode, string cpPartsInboundCheckCode, string sparePartCode, string sparePartName, out string fileName) {
            fileName = GetExportFilePath("配件入库检验主清单.xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);
                var userInfo = Utils.GetCurrentUserInfo();
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.Append(@"Select a.Code,
                                a.Warehousename,            
                                (select value from keyvalueitem where NAME = 'Parts_InboundType'and key=a.Inboundtype) As Inboundtype,
                                a.Counterpartcompanycode,
                                a.Counterpartcompanyname,
                                (Select Sum(t.Settlementprice * t.Inspectedquantity)
                                    From Partsinboundcheckbilldetail t
                                    Where t.Partsinboundcheckbillid = a.Id),
                                p.Name,
                                a.Originalrequirementbillcode,
                                c.Code As Partsinboundplanidcode,   
                                d.Name As Partssalescategoryname,
                                a.Branchname,
                                a.Warehousecode,             
                                po.PlanSource,      
                                (select value from keyvalueitem where NAME = 'Parts_SettlementStatus'and key=a.Settlementstatus) As Settlementstatus,
                                (select value from keyvalueitem where NAME = 'PartsSalesReturn_ReturnType'and key=f.ReturnType) As ReturnType, 
                                a.Remark,
                                a.Creatorname,
                                a.Createtime,
                                a.Modifiername,
                                a.Modifytime,
                                (select value from keyvalueitem where NAME = 'PartsShipping_Method'and key=sso.ShippingMethod) As ShippingMethod,
                                h.SupplierPartCode,
                                b.Sparepartname,
                                b.Warehouseareacode,
                                ( Select Min(Extent2.Code) As A1
                           From Partsstock Extent1
                          Inner Join Warehousearea Extent2
                             On Extent1.Warehouseareaid = Extent2.Id
                          Inner Join Warehouseareacategory Extent3
                             On Extent2.Areacategoryid = Extent3.Id
                          Where (((Extent1.Warehouseid = a.Warehouseid)and Extent1.partid=b.Sparepartid) And
                                (Extent2.Areakind = 3))
                            And (Extent3.Category = 1)),
                               b.Inspectedquantity,
                               b.Settlementprice,
                               s.Measureunit,
                               b.Remark As Remarkdetails,
                               b.SpareOrderRemark
                          From Partsinboundcheckbill a
                           left join PartsPurchaseOrder po
                             on a.OriginalRequirementBillCode=po.Code
                           left join PartsPurchaseOrderType p
                              on po.PartsPurchaseOrderTypeId=p.Id
                          Left Outer Join Partsinboundcheckbilldetail b
                            On a.Id = b.Partsinboundcheckbillid
                          Left Outer Join Partsinboundplan c
                            On a.Partsinboundplanid = c.Id
                          left outer join SupplierShippingOrder sso
                            on c.sourceid=sso.id and c.SourceCode=sso.Code
                          Left Outer Join Partssalescategory d
                            On a.Partssalescategoryid = d.Id
                          Left Outer Join PartsInboundPlan e
                             On a.PartsInboundPlanId = e.Id
                          Left Outer Join PartsSalesReturnBill f
                             On e.OriginalRequirementBillId = f.Id   
                          Left Outer Join sparepart s
                             On b.Sparepartid = s.Id 
                           Left Outer Join PartsSalesReturnBillDetail j
                             On f.Id = j.PartsSalesReturnBillId
                           Left Outer Join PartsSalesOrder g 
                             On j.PartsSalesOrderId = g.Id Left Outer Join PartsSupplierRelation h
                             On h.partid = b.sparepartid and h.supplierid = a.counterpartcompanyid  where 1=1 ");
                    var dbParameters = new List<DbParameter>();
                    if(ids != null && ids.Length > 0) {
                        sql.Append(" and a.id in (");
                        for(var i = 0; i < ids.Length; i++) {
                            if(ids.Length == i + 1) {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            } else {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                        }
                        sql.Append(")");
                    } else {
                        //if(partsPurchaseOrderTypeId.HasValue) {
                        //    //采购订单类型
                        //    sql.Append(@" and  po.PartsPurchaseOrderTypeId={0}partsPurchaseOrderTypeId ");
                        //    dbParameters.Add(db.CreateDbParameter("partsPurchaseOrderTypeId", partsPurchaseOrderTypeId.Value));
                        //}
                        if(storageCompanyId.HasValue) {
                            sql.Append(" and a.storageCompanyId = {0}storageCompanyId ");
                            dbParameters.Add(db.CreateDbParameter("storageCompanyId", userInfo.EnterpriseId));
                        }
                        if(userId.HasValue) {
                            sql.Append(@" and (Exists (Select 1 As C1
                  From Warehouseoperator Extent9
                 Where Extent9.Operatorid = {0}userId
                   And Extent9.Warehouseid = a.Warehouseid)) ");
                            dbParameters.Add(db.CreateDbParameter("userId", userInfo.Id));
                        }
                        if(partsSalesCategoryId.HasValue) {
                            sql.Append(@" and a.partsSalesCategoryId = {0}partsSalesCategoryId ");
                            dbParameters.Add(db.CreateDbParameter("partsSalesCategoryId", partsSalesCategoryId.Value));
                        }
                        if(!string.IsNullOrEmpty(partsInboundCheckBillCode)) {
                            sql.Append(" and LOWER(a.Code) like {0}partsInboundCheckBillCode ");
                            dbParameters.Add(db.CreateDbParameter("partsInboundCheckBillCode", "%" + partsInboundCheckBillCode.ToLower() + "%"));
                        }
                        if(!string.IsNullOrEmpty(originalRequirementBillCode)) {
                            sql.Append(" and LOWER(a.originalRequirementBillCode) like {0}originalRequirementBillCode ");
                            dbParameters.Add(db.CreateDbParameter("originalRequirementBillCode", "%" + originalRequirementBillCode.ToLower() + "%"));
                        }


                        if(warehouseId.HasValue) {
                            sql.Append(" and a.warehouseId = {0}warehouseId ");
                            dbParameters.Add(db.CreateDbParameter("warehouseId", warehouseId.Value));
                        }
                        //if(!string.IsNullOrEmpty(counterpartcompanycode)) {
                        //    sql.Append(" and LOWER(a.counterpartcompanycode) like {0}counterpartcompanycode ");
                        //    dbParameters.Add(db.CreateDbParameter("counterpartcompanycode", "%" + counterpartcompanycode.ToLower() + "%"));
                        //}
                        if(counterpartCompanyId.HasValue) {
                            sql.Append(" and a.counterpartCompanyId = {0}counterpartCompanyId ");
                            dbParameters.Add(db.CreateDbParameter("counterpartCompanyId", counterpartCompanyId.Value));
                        }

                        if(!string.IsNullOrEmpty(counterpartcompanyname)) {
                            sql.Append(" and LOWER(a.counterpartcompanyname) like {0}counterpartcompanyname ");
                            dbParameters.Add(db.CreateDbParameter("counterpartcompanyname", "%" + counterpartcompanyname.ToLower() + "%"));
                        }
                        if(!string.IsNullOrEmpty(eRPOrderCode)) {
                            sql.Append(" and LOWER(a.ERPSourceOrderCode) like {0}eRPOrderCode ");
                            dbParameters.Add(db.CreateDbParameter("eRPOrderCode", "%" + eRPOrderCode.ToLower() + "%"));
                        }

                        if(!string.IsNullOrEmpty(sAPPurchasePlanCode)) {
                            sql.Append(" and LOWER(a.SAPPurchasePlanCode) like {0}sAPPurchasePlanCode ");
                            dbParameters.Add(db.CreateDbParameter("sAPPurchasePlanCode", "%" + sAPPurchasePlanCode.ToLower() + "%"));
                        }
                        if(!string.IsNullOrEmpty(partsSalesOrderCode)) {
                            sql.Append(" and LOWER(g.code) like {0}code ");
                            dbParameters.Add(db.CreateDbParameter("code", "%" + partsSalesOrderCode.ToLower() + "%"));
                        }
                        if(partsSalesOrderInvoiceType.HasValue) {
                            sql.Append(" and g.InvoiceType ={0}InvoiceType ");
                            dbParameters.Add(db.CreateDbParameter("InvoiceType", partsSalesOrderInvoiceType.Value));
                        }
                        if(inboundType.HasValue) {
                            sql.Append(" and a.inboundType ={0}inboundType ");
                            dbParameters.Add(db.CreateDbParameter("inboundType", inboundType.Value));
                        }


                        if(settlementStatus.HasValue) {
                            sql.Append(" and a.settlementStatus ={0}settlementStatus ");
                            dbParameters.Add(db.CreateDbParameter("settlementStatus", settlementStatus.Value));
                        }

                        if(createTimeBegin.HasValue) {
                            sql.Append(@" and a.createTime >=To_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }


                        if(createTimeEnd.HasValue) {
                            sql.Append(@" and a.createTime <=To_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }

                        if(!string.IsNullOrEmpty(cpPartsPurchaseOrderCode)) {
                            sql.Append(" and LOWER(a.CPPartsPurchaseOrderCode) like {0}cpPartsPurchaseOrderCode ");
                            dbParameters.Add(db.CreateDbParameter("cpPartsPurchaseOrderCode", "%" + cpPartsPurchaseOrderCode.ToLower() + "%"));
                        }

                        if(!string.IsNullOrEmpty(cpPartsInboundCheckCode)) {
                            sql.Append(" and LOWER(a.CPPartsInboundCheckCode) like {0}cpPartsInboundCheckCode ");
                            dbParameters.Add(db.CreateDbParameter("cpPartsInboundCheckCode", "%" + cpPartsInboundCheckCode.ToLower() + "%"));
                        }
                        if(!string.IsNullOrEmpty(sparePartName)) {
                            sql.Append(" and LOWER(b.sparePartName) like {0}sparePartName ");
                            dbParameters.Add(db.CreateDbParameter("sparePartName", "%" + sparePartName.ToLower() + "%"));
                        } if(!string.IsNullOrEmpty(sparePartCode)) {
                            sql.Append(" and LOWER(b.sparePartCode) like {0}sparePartCode ");
                            dbParameters.Add(db.CreateDbParameter("sparePartCode", "%" + sparePartCode.ToLower() + "%"));
                        }
                    }
                    var sqlStr = sql.ToString();
                    if(sqlStr.IndexOf(" where 1=1  and ") > 0)
                        sqlStr = sqlStr.Replace(" where 1=1  and ", " where ");
                    else
                        sqlStr = sqlStr.Replace(" where 1=1 ", "");
                    var command = db.CreateDbCommand(string.Format(sqlStr, db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                   ErrorStrings.Export_Title_PartsInboundCheckBill_Code, ErrorStrings.Export_Title_Company_WarehouseName, ErrorStrings.Export_Title_PartsInboundCheckBill_InBoundType,ErrorStrings.Export_Title_BoxUpTask_CounterpartCompanyCode, ErrorStrings.Export_Title_BoxUpTask_CounterpartCompanyName,ErrorStrings.Export_Title_InternalAllocationBill_TotalAmount,ErrorStrings.Export_Title_PartsInboundCheckBill_PurchaseType, ErrorStrings.Export_Title_PartsInboundCheckBill_OriginalRequirementBillCode, ErrorStrings.Export_Title_PartsInboundCheckBill_PartsInboundPlanCode, ErrorStrings.Export_Title_Partssalescategory_Name, ErrorStrings.Export_Title_PartsBranch_BranchName, ErrorStrings.Export_Title_Company_WarehouseCode, ErrorStrings.Export_Title_PartsPurchaseOrder_PlanSource,ErrorStrings.Export_Title_PartsInboundCheckBill_SettlementStatus,ErrorStrings.Export_Title_PartsInboundCheckBill_ReturnType, ErrorStrings.Export_Title_PartsBranch_Remark, ErrorStrings.Export_Title_AccountPeriod_CreatorName, ErrorStrings.Export_Title_AccountPeriod_CreateTime, ErrorStrings.Export_Title_AccountPeriod_ModifierName, ErrorStrings.Export_Title_AccountPeriod_ModifyTime, ErrorStrings.Export_Title_PartsPurchaseOrder_ShippingMethod,ErrorStrings.Export_Title_SupplierShippingOrder_SupplierpartCode, ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName, ErrorStrings.Export_Title_WarehouseArea_WarehouseAreaCode,ErrorStrings.Export_Title_PartsInboundCheckBill_TargetLocation, ErrorStrings.Export_Title_PartsInboundCheckBill_InspectedQuantity, ErrorStrings.Export_Title_PartsPurchaseRtnSettleBill_SettlementPrice, ErrorStrings.Export_Title_PackingPropertyApp_MeasureUnit, ErrorStrings.Export_Title_PartsPurchaseOrder_DetailRemark,ErrorStrings.Export_Title_SupplierShippingOrder_DetailRemark
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();

                }
                return true;
            } catch(Exception) {
                return false;
            }
        }

    }
}
