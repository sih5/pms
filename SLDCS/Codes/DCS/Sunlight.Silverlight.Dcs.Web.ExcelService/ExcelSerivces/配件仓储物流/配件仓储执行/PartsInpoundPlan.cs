﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Globalization;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        /// <summary>
        /// 合并导出配件入库计划主清单
        /// </summary>
        public bool ExportPartsInboundPlanWithDetail(int[] ids, int? partsSalesCategoryId, int userId, int? status, int storageCompanyId, string partsInboundPlanCode, string sourceCode, int? warehouseId, int? inboundType, DateTime? createTimeBegin, DateTime? createTimeEnd, DateTime? planDeliveryTimeBegin, DateTime? planDeliveryTimeEnd, string counterpartCompanyCode, string counterpartCompanyName, string sparePartCode, string sparePartName, string originalRequirementBillCode, out string fileName) {
            fileName = GetExportFilePath("配件入库计划主清单.xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);
                var userInfo = Utils.GetCurrentUserInfo();
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.AppendFormat(@"Select a.Code As Partsinboundplancode,
       a.Warehousecode,
       a.Warehousename,
       (select value from keyvalueitem where NAME = 'Parts_InboundType'and key=a.Inboundtype) As Inboundtype,
       (Select Sum(t.Price * t.Plannedamount)
          From Partsinboundplandetail t
         Where t.Partsinboundplanid = a.Id),
       a.Counterpartcompanycode,
       a.Counterpartcompanyname,
       a.ERPSourceOrderCode,
       a.Createtime,
       (Select Min(t.Code)
          From Warehousearea t
         Inner Join Partsstock v
            On t.Id = v.Warehouseareaid
         Inner Join Warehouseareacategory x
            On x.Id = t.Areacategoryid
         Where b.Sparepartid = v.Partid
           And a.Warehouseid = v.Warehouseid
           And t.Areakind = 3
           And x.Category = 1),
       psr.SupplierPartCode,
       b.Sparepartcode,
       b.Sparepartname,
       b.Plannedamount,
       b.Inspectedquantity,
       b.Price,
       (Select t.Measureunit From Sparepart t Where t.Id = b.Sparepartid),
       b.Remark
  From Partsinboundplan a
  Left Join Partsinboundplandetail b
    On a.Id = b.Partsinboundplanid
  Left Join Partssalescategory c
    On a.Partssalescategoryid = c.Id
left join PartsSupplierRelation psr on a.CounterpartCompanyId=psr.SupplierId and b.SparePartId=psr.PartId 
 Where a.Storagecompanyid = {0}
   And ((( EXISTS (SELECT 
          1 AS C1
          FROM WarehouseArea Extent3
          WHERE (((Extent3.Status = 1) AND ( EXISTS (SELECT 
            1 AS C1
            FROM WarehouseAreaManager Extent4
            WHERE (Extent4.ManagerId = {1}) AND (Extent4.WarehouseAreaId = Extent3.TopLevelWarehouseAreaId)
          ))) AND ( EXISTS (SELECT 
            1 AS C1
            FROM WarehouseAreaCategory Extent5
            WHERE (Extent5.Category = 3) AND (Extent5.Id = Extent3.AreaCategoryId)
          ))) AND (Extent3.WarehouseId = a.WarehouseId)
        )) AND (a.Status IN (1,4))) AND ( NOT (( EXISTS (SELECT 
          1 AS C1
          FROM Warehouse Extent6
          WHERE (Extent6.Id = a.WarehouseId) AND (Extent6.WmsInterface = 1)
        )) AND ((a.InboundType IN (1,2,4)) OR ((a.InboundType = 3) AND ( EXISTS (SELECT 
          1 AS C1
          FROM   PartsTransferOrder Extent7
          INNER JOIN Warehouse Extent8 ON Extent7.OriginalWarehouseId = Extent8.Id
          INNER JOIN Warehouse Extent9 ON Extent7.DestWarehouseId = Extent9.Id
          WHERE (Extent8.StorageCenter <> Extent9.StorageCenter) AND (Extent7.Id = a.OriginalRequirementBillId)
        ))))))) ", userInfo.EnterpriseId, userInfo.Id);
                    var dbParameters = new List<DbParameter>();
                    if(ids != null && ids.Length > 0) {
                        sql.Append(" and a.id in (");
                        for(var i = 0; i < ids.Length; i++) {
                            if(ids.Length == i + 1) {
                                sql.Append(ids[i].ToString(CultureInfo.InvariantCulture));
                            } else {
                                sql.Append(ids[i].ToString(CultureInfo.InvariantCulture) + ",");
                            }
                        }
                        sql.Append(")");
                    } else {
                        if(partsSalesCategoryId.HasValue) {
                            sql.Append(@" and a.partsSalesCategoryId = {0}partsSalesCategoryId ");
                            dbParameters.Add(db.CreateDbParameter("partsSalesCategoryId", partsSalesCategoryId.Value));
                        }
                        if(status.HasValue) {
                            sql.Append(@" and a.status = {0}status ");
                            dbParameters.Add(db.CreateDbParameter("status", status.Value));
                        }
                        if(!String.IsNullOrEmpty(partsInboundPlanCode)) {
                            sql.Append(@" and LOWER(a.Code) like {0}partsInboundPlanCode ");
                            dbParameters.Add(db.CreateDbParameter("partsInboundPlanCode", "%" + partsInboundPlanCode.ToLower() + "%"));
                        }
                        if(!String.IsNullOrEmpty(sourceCode)) {
                            sql.Append(@" and LOWER(a.sourceCode) like {0}sourceCode ");
                            dbParameters.Add(db.CreateDbParameter("sourceCode", "%" + sourceCode.ToLower() + "%"));
                        }
                        if(warehouseId.HasValue) {
                            sql.Append(@" and a.warehouseId = {0}warehouseId ");
                            dbParameters.Add(db.CreateDbParameter("warehouseId", warehouseId.Value));
                        }
                        if(inboundType.HasValue) {
                            sql.Append(@" and a.inboundType = {0}inboundType ");
                            dbParameters.Add(db.CreateDbParameter("inboundType", inboundType.Value));
                        }
                        if(createTimeBegin.HasValue) {
                            sql.Append(@" and a.createTime >=to_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(@" and a.createTime <=to_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                        if(planDeliveryTimeBegin.HasValue) {
                            sql.Append(@" and a.planDeliveryTime >=to_date({0}planDeliveryTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = planDeliveryTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("planDeliveryTimeBegin", tempTime.ToString("G")));
                        }
                        if(planDeliveryTimeEnd.HasValue) {
                            sql.Append(@" and a.planDeliveryTime <=to_date({0}planDeliveryTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = planDeliveryTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("planDeliveryTimeEnd", tempTime.ToString("G")));
                        }
                        if(!String.IsNullOrEmpty(counterpartCompanyCode)) {
                            sql.Append(@" and LOWER(a.counterpartCompanyCode) like {0}counterpartCompanyCode ");
                            dbParameters.Add(db.CreateDbParameter("counterpartCompanyCode", "%" + counterpartCompanyCode.ToLower() + "%"));
                        }
                        if(!String.IsNullOrEmpty(counterpartCompanyName)) {
                            sql.Append(@" and LOWER(a.counterpartCompanyName) like {0}counterpartCompanyName ");
                            dbParameters.Add(db.CreateDbParameter("counterpartCompanyName", "%" + counterpartCompanyName.ToLower() + "%"));
                        }
                        if(!String.IsNullOrEmpty(sparePartCode)) {
                            sql.Append(@" and b.sparepartCode like {0}sparepartCode ");
                            dbParameters.Add(db.CreateDbParameter("sparepartCode", "%" + sparePartCode + "%"));
                        }
                        if(!String.IsNullOrEmpty(sparePartName)) {
                            sql.Append(@" and b.sparepartName like {0}sparepartName ");
                            dbParameters.Add(db.CreateDbParameter("sparepartName", "%" + sparePartName + "%"));
                        }
                        if(!String.IsNullOrEmpty(originalRequirementBillCode)) {
                            sql.Append(@" and a.originalRequirementBillCode like {0}originalRequirementBillCode ");
                            dbParameters.Add(db.CreateDbParameter("originalRequirementBillCode", "%" + originalRequirementBillCode + "%"));
                        }
                        //需要手动复制该导出函数的所有参数名（除了filename）
                        //var param = new {
                        //    partsInboundPlanCode,
                        //    sourceCode,
                        //    warehouseId,
                        //    inboundType,
                        //    createTimeBegin,
                        //    createTimeEnd,
                        //    counterpartCompanyCode,
                        //    counterpartCompanyName
                        //};

                        //foreach(var parameterInfo in param.GetType().GetProperties()) {
                        //    //无法使用switch语句(typeof非静态字符串) 所以if加上了continue来实现switch功能
                        //    if(parameterInfo.Name == typeof(string).FullName) {
                        //        if(!string.IsNullOrEmpty((string)parameterInfo.GetValue(param, null))) {
                        //            sql.Append(" and " + parameterInfo.Name + " like {0}" + parameterInfo.Name);
                        //            dbParameters.Add(db.CreateDbParameter(parameterInfo.Name, "%" + (string)parameterInfo.GetValue(param, null) + "%"));
                        //        }
                        //        continue;
                        //    }
                        //    if(parameterInfo.Name == typeof(int?).FullName) {
                        //        if(((int?)parameterInfo.GetValue(param, null)).HasValue) {
                        //            sql.Append(" and " + parameterInfo.Name + "={0}" + parameterInfo.Name);
                        //            dbParameters.Add(db.CreateDbParameter(parameterInfo.Name, ((int?)parameterInfo.GetValue(param, null)).Value));
                        //        }
                        //        continue;
                        //    }
                        //    if(parameterInfo.Name == typeof(DateTime?).FullName) {
                        //        string op = parameterInfo.Name.Contains("Begin") ? ">=" : (parameterInfo.Name.Contains("End") ? "<=" : "=");
                        //        if(((DateTime?)parameterInfo.GetValue(param, null)).HasValue) {
                        //            sql.Append(" and createTime" + op + "to_date({0}" + parameterInfo.Name + ",'yyyy-mm-dd hh24:mi:ss')");
                        //            var tempValue = ((DateTime?)parameterInfo.GetValue(param, null)).Value;
                        //            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                        //            dbParameters.Add(db.CreateDbParameter(parameterInfo.Name, tempTime.ToString("G")));
                        //        }
                        //    }
                        //}
                    }

                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    ErrorStrings.Export_Title_PartsInboundCheckBill_PartsInboundPlanCode, ErrorStrings.Export_Title_Company_WarehouseCode, ErrorStrings.Export_Title_Company_WarehouseName, ErrorStrings.Export_Title_PackingTask_InboundType,ErrorStrings.Export_Title_InternalAllocationBill_TotalAmount, ErrorStrings.Export_Title_BoxUpTask_CounterpartCompanyCode, ErrorStrings.Export_Title_BoxUpTask_CounterpartCompanyName,ErrorStrings.Export_Title_PartsInboundCheckBill_DeliveryCode,ErrorStrings.Export_Title_AccountPeriod_CreateTime,ErrorStrings.Export_Title_PartsInboundCheckBill_SparePartsLocation,
                                    ErrorStrings.Export_Title_SupplierShippingOrder_SupplierpartCode,ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode, ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName, ErrorStrings.Export_Title_PartsPurchasePlanDetail_PlanAmount, ErrorStrings.Export_Title_PartsInboundCheckBill_InspectedQuantity, ErrorStrings.Export_Title_PartsPurReturnOrder_Price,ErrorStrings.Export_Title_PackingPropertyApp_MeasureUnit,ErrorStrings.Export_Title_PartsPurchaseOrder_DetailRemark
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }

        /// <summary>
        /// 导出配件入库计划单ForQuery
        /// </summary>
        public bool ExportPartsInboundPlansForQuery(int[] ids, int? partsSalesCategoryId, int? status, int? userId, int storageCompanyId, string partsInboundPlanCode, string sourceCode, int? warehouseId, int? inboundType, DateTime? createTimeBegin, DateTime? createTimeEnd, string counterpartCompanyCode, string counterpartCompanyName, string eRPOrderCode, string partsSalesOrderCode, int? partsPurchaseOrderTypeId, string SAPPurchasePlanCode, out string fileName) {
            fileName = GetExportFilePath("配件入库计划单.xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.AppendFormat(@"select code,
       Warehousename,
       (select value
          from keyvalueitem
         where NAME = 'Parts_InboundType'
           and key = a.Inboundtype) As Inboundtype,
       Counterpartcompanycode,
       Counterpartcompanyname,
       PlannedAmount,
       Name,
       OriginalRequirementBillCode,
       Partssalescategoryname,
       Warehousecode,
       PlanSource,
       (select value
          from keyvalueitem
         where NAME = 'PartsInboundPlan_Status'
           and key = a.Status) As Status,
       Sourcecode,
       (select value
          from keyvalueitem
         where NAME = 'PartsSalesReturn_ReturnType'
           and key = a.ReturnType) As ReturnType,
       Remark,
       ERPSourceOrderCode,
       PartssalesorderCode,
       PlanDeliveryTime,
       CreatorName,
       Createtime,
       ModifierName,
       ModifyTime,
       Arrivaldate,
       SAPPurchasePlanCode
  from (select pip.Id,
               pip.WarehouseId,
               po.PartsPurchaseOrderTypeId,
               pip.partsSalesCategoryId,
               pip.Code,
               pip.Warehousename,
               pip.Inboundtype,
               pip.Counterpartcompanycode,
               pip.Counterpartcompanyname,
               (Select Sum(t.Price * t.PlannedAmount)
                  From Partsinboundplandetail t
                 Where t.partsinboundplanid = pip.id) as PlannedAmount,
               p.Name,
               pip.OriginalRequirementBillCode,
               psc.name As Partssalescategoryname,
               pip.Warehousecode,
               po.PlanSource,
               pip.Status,
               pip.Sourcecode,
               psrb.ReturnType,
               pip.ERPSourceOrderCode,
               pip.PlanDeliveryTime,
               pip.CreatorName,
               pip.Createtime,
               pip.ModifierName,
               pip.Remark,
               pip.ModifyTime,
               pip.Arrivaldate,
               pip.SAPPurchasePlanCode,
               pip.StorageCompanyId,
               pso.code as PartssalesorderCode
          from PartsInboundPlan pip
          left join PartsSalesReturnBill psrb
            on pip.OriginalRequirementBillCode = psrb.code
          left join Partssalesorder pso
            on psrb. partssalesorderid = pso.id
          left join PartsTransferOrder po
            on po.Code = pip.OriginalRequirementBillCode
          left join partssalescategory psc
            on pip.partssalescategoryid = psc.id
          left join PartsPurchaseOrder po
            on pip.OriginalRequirementBillCode = po.Code
           and pip.Originalrequirementbillid = po.id
          left join PartsPurchaseOrderType p
            on po.PartsPurchaseOrderTypeId = p.Id) a
 where StorageCompanyId = {0} ", storageCompanyId);
                    var dbParameters = new List<DbParameter>();
                    if(ids != null && ids.Length > 0) {
                        sql.Append(@" and id in (");
                        for(var i = 0; i < ids.Length; i++) {
                            var tempPartsInpoundPlanWithNum = "partsInpoundPlan" + i;
                            if(i == ids.Length - 1) {
                                var tempParam = "{0}" + tempPartsInpoundPlanWithNum;
                                sql.Append(tempParam);
                            } else {
                                var tempParam = "{0}" + tempPartsInpoundPlanWithNum + ",";
                                sql.Append(tempParam);
                            }
                            dbParameters.Add(db.CreateDbParameter(tempPartsInpoundPlanWithNum, ids[i]));
                        }
                        sql.Append(@")");
                    } else {
                        if(userId.HasValue) {
                            sql.Append(@"  and ( EXISTS (SELECT 
                                                                  1 AS C1
                                                                  FROM WarehouseOperator Extent3
                                                                  WHERE Extent3.OperatorId = {0}userId AND Extent3.WarehouseId = WarehouseId)) ");
                            dbParameters.Add(db.CreateDbParameter("userId", userId.Value));
                        }
                        if(partsPurchaseOrderTypeId.HasValue) {
                            //采购订单类型
                            sql.Append(@" and  PartsPurchaseOrderTypeId={0}partsPurchaseOrderTypeId ");
                            dbParameters.Add(db.CreateDbParameter("partsPurchaseOrderTypeId", partsPurchaseOrderTypeId.Value));
                        }
                        if(partsSalesCategoryId.HasValue) {
                            sql.Append(@" and partsSalesCategoryId = {0}partsSalesCategoryId ");
                            dbParameters.Add(db.CreateDbParameter("partsSalesCategoryId", partsSalesCategoryId.Value));
                        }
                        if(status.HasValue) {
                            sql.Append(@" and status = {0}status ");
                            dbParameters.Add(db.CreateDbParameter("status", status.Value));
                        }
                        if(!String.IsNullOrEmpty(partsInboundPlanCode)) {
                            sql.Append(@" and LOWER(Code) like {0}partsInboundPlanCode ");
                            dbParameters.Add(db.CreateDbParameter("partsInboundPlanCode", "%" + partsInboundPlanCode.ToLower() + "%"));
                        }
                        if(!String.IsNullOrEmpty(sourceCode)) {
                            sql.Append(@" and LOWER(sourceCode) like {0}sourceCode ");
                            dbParameters.Add(db.CreateDbParameter("sourceCode", "%" + sourceCode.ToLower() + "%"));
                        }
                        if(warehouseId.HasValue) {
                            sql.Append(@" and warehouseId = {0}warehouseId ");
                            dbParameters.Add(db.CreateDbParameter("warehouseId", warehouseId.Value));
                        }
                        if(inboundType.HasValue) {
                            sql.Append(@" and inboundType = {0}inboundType ");
                            dbParameters.Add(db.CreateDbParameter("inboundType", inboundType.Value));
                        }
                        if(createTimeBegin.HasValue) {
                            sql.Append(@" and createTime >=to_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(@" and createTime <=to_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                        if(!String.IsNullOrEmpty(counterpartCompanyCode)) {
                            sql.Append(@" and LOWER(counterpartCompanyCode) like {0}counterpartCompanyCode ");
                            dbParameters.Add(db.CreateDbParameter("counterpartCompanyCode", "%" + counterpartCompanyCode.ToLower() + "%"));
                        }
                        if(!String.IsNullOrEmpty(counterpartCompanyName)) {
                            sql.Append(@" and LOWER(counterpartCompanyName) like {0}counterpartCompanyName ");
                            dbParameters.Add(db.CreateDbParameter("counterpartCompanyName", "%" + counterpartCompanyName.ToLower() + "%"));
                        }
                        if(!string.IsNullOrEmpty(eRPOrderCode)) {
                            sql.Append("and LOWER(Erpsourceordercode) like {0}Erpsourceordercode ");
                            dbParameters.Add(db.CreateDbParameter("Erpsourceordercode", "%" + eRPOrderCode.ToLower() + "%"));
                        }
                        if(!string.IsNullOrEmpty(partsSalesOrderCode)) {
                            sql.Append("and LOWER(PartssalesorderCode) like {0}code ");
                            dbParameters.Add(db.CreateDbParameter("code", "%" + partsSalesOrderCode.ToLower() + "%"));
                        }
                    }

                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    ErrorStrings.Export_Title_PartsInboundCheckBill_PartsInboundPlanCode, ErrorStrings.Export_Title_Company_WarehouseName, ErrorStrings.Export_Title_PackingTask_InboundType, ErrorStrings.Export_Title_BoxUpTask_CounterpartCompanyCode, ErrorStrings.Export_Title_BoxUpTask_CounterpartCompanyName, ErrorStrings.Export_Title_InternalAllocationBill_TotalAmount, ErrorStrings.Export_Title_PartsInboundCheckBill_PurchaseType, ErrorStrings.Export_Title_PartsInboundPlan_OriginalRequirementBillCode, ErrorStrings.Export_Title_Partssalescategory_Name, ErrorStrings.Export_Title_Company_WarehouseCode, ErrorStrings.Export_Title_PartsPurchaseOrder_PlanSource, ErrorStrings.Export_Title_AccountPeriod_Status, ErrorStrings.Export_Title_PartsOuterPurchaseChange_SourceCode, ErrorStrings.Export_Title_PartsInboundCheckBill_ReturnType, ErrorStrings.Export_Title_PartsBranch_Remark, ErrorStrings.Export_Title_SupplierShippingOrder_ERPSourceOrderCode, ErrorStrings.Export_Title_SupplierShippingOrder_SalesOrderCode, ErrorStrings.Export_Title_PartsInboundPlan_PlanDeliveryTime,ErrorStrings.Export_Title_AccountPeriod_CreatorName, ErrorStrings.Export_Title_AccountPeriod_CreateTime, ErrorStrings.Export_Title_AccountPeriod_ModifierName, ErrorStrings.Export_Title_AccountPeriod_ModifyTime, ErrorStrings.Export_Title_SupplierShippingOrder_ArriveDate, ErrorStrings.Export_Title_PartsInboundCheckBill_SAPPurchasePlanCode
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }

        /// <summary>
        /// 合并导出配件入库计划主清单ForQuery
        /// </summary>
        public bool ExportPartsInboundPlanWithDetailForQuery(int[] ids, int? partsSalesCategoryId, int? status, int? userId, int storageCompanyId, string partsInboundPlanCode, string sourceCode, int? warehouseId, int? inboundType, DateTime? createTimeBegin, DateTime? createTimeEnd, string counterpartCompanyCode, string counterpartCompanyName, int? partsPurchaseOrderTypeId, string SAPPurchasePlanCode, string sparepartCode, string sparepartName, string originalRequirementBillCode, DateTime? planDeliveryTimeBegin, DateTime? planDeliveryTimeEnd,bool? hasDifference, out string fileName) {
            fileName = GetExportFilePath("配件入库计划主清单.xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);
                var userInfo = Utils.GetCurrentUserInfo(); 
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.AppendFormat(@"select Code,
                                                   Warehousename,
                                                   (select value
                                                      from keyvalueitem
                                                     where NAME = 'Parts_InboundType'
                                                       and key = a.Inboundtype) As Inboundtype,
                                                   Counterpartcompanycode,
                                                   Counterpartcompanyname,
                                                   Amount,
                                                   Name,
                                                   OriginalRequirementBillCode,
                                                   Partssalescategoryname,
                                                   Warehousecode,
                                                   (select value
                                                      from keyvalueitem
                                                     where NAME = 'PartsInboundPlan_Status'
                                                       and key = a.Status) As Status,
                                                   Sourcecode,
                                                   (select value
                                                      from keyvalueitem
                                                     where NAME = 'PartsSalesReturn_ReturnType'
                                                       and key = a.ReturnType) As ReturnType,
                                                   Remark,
                                                   ERPSourceOrderCode,
                                                   RequestedDeliveryTime,
                                                   PlanDeliveryTime,
                                                   PlanSource,
                                                   Createtime,
                                                   Arrivaldate,
                                                   sscode,
                                                   tcode,
                                                   Sparepartcode,
                                                   Sparepartname,
                                                   Plannedamount,
                                                   Inspectedquantity,
                                                   Price,
                                                   MeasureUnit,
                                                   dRemark,
                                                   SpareOrderRemark,
                                                   HasDifference 
                                              from (Select a.Code,
                                                           a.id,a.StorageCompanyId,
                                                           a.Warehousename,
                                                           a.Inboundtype,
                                                           a.Counterpartcompanycode,
                                                            po.PartsPurchaseOrderTypeId,
                                                            a.partsSalesCategoryId,
                                                             a.warehouseId,
                                                           a.Counterpartcompanyname,
                                                           (Select Sum(t.Price * t.PlannedAmount)
                                                              From Partsinboundplandetail t
                                                             Where t.partsinboundplanid = a.id) Amount,
                                                           p.Name,
                                                           a.OriginalRequirementBillCode,
                                                           c.Name As Partssalescategoryname,
                                                           a.Warehousecode,
                                                           a. Status,
                                                           a.Sourcecode,
                                                           f.ReturnType,
                                                           a.Remark,
                                                           a.ERPSourceOrderCode,
                                                           a.RequestedDeliveryTime,
                                                           a.PlanDeliveryTime,
                                                           po.PlanSource,
                                                           a.Createtime,
                                                           a.Arrivaldate,
                                                           (Select min(ss.code)
                                                              From Warehousearea t
                                                             Inner Join warehousearea Ss
                                                                On t.parentid = ss.id
                                                             Inner Join Partsstock v
                                                                On t.Id = v.Warehouseareaid
                                                             Inner Join Warehouseareacategory x
                                                                On x.Id = t.Areacategoryid
                                                             Where b.Sparepartid = v.Partid
                                                               And a.Warehouseid = v.Warehouseid
                                                               And t.Areakind = 3
                                                               And x.Category = 1) as sscode,
                                                           (Select Min(t.Code)
                                                              From Warehousearea t
                                                             Inner Join Partsstock v
                                                                On t.Id = v.Warehouseareaid
                                                             Inner Join Warehouseareacategory x
                                                                On x.Id = t.Areacategoryid
                                                             Where b.Sparepartid = v.Partid
                                                               And a.Warehouseid = v.Warehouseid
                                                               And t.Areakind = 3
                                                               And x.Category = 1) as tcode,
                                                           b.Sparepartcode,
                                                           b.Sparepartname,
                                                           b.Plannedamount,
                                                           b.Inspectedquantity,
                                                           b.Price,
                                                           (Select t.MeasureUnit
                                                              From SparePart t
                                                             Where t.id = b.sparepartid) as MeasureUnit,
                                                           b.POCode,
                                                           b.Remark as dremark,
                                                           b.SpareOrderRemark,
                                                           a.SAPPurchasePlanCode,
                                                           case (select count(1) from PartsDifferenceBackBill where rownum=1 and status<>99 and SourceCode=a.OriginalRequirementBillCode) when 1 then cast('是' as varchar2(100)) else cast('否' as varchar2(100)) end as HasDifference 
                                                      From Partsinboundplan a
                                                      Left Outer Join PartsSalesReturnBill f
                                                        On a.OriginalRequirementBillCode = f.Code                                                      
                                                      left join PartsPurchaseOrder po
                                                        on a.OriginalRequirementBillCode = po.Code
                                                        left join PartsTransferOrder pt on a.OriginalRequirementBillCode = pt.Code
                                                      left join PartsPurchaseOrderType p
                                                        on po.PartsPurchaseOrderTypeId = p.Id
                                                      left Join Partsinboundplandetail b
                                                        On a.Id = b.Partsinboundplanid
                                                      left Join Partssalescategory c
                                                        On a.Partssalescategoryid = c.Id)a
                                            where StorageCompanyId = {0} ", storageCompanyId);
                    var dbParameters = new List<DbParameter>();
                    if(ids != null && ids.Length > 0) {
                        sql.Append(@" and id in (");
                        for(var i = 0; i < ids.Length; i++) {
                            var tempPartsInpoundPlanWithNum = "partsInpoundPlan" + i;
                            if(i == ids.Length - 1) {
                                var tempParam = "{0}" + tempPartsInpoundPlanWithNum;
                                sql.Append(tempParam);
                            } else {
                                var tempParam = "{0}" + tempPartsInpoundPlanWithNum + ",";
                                sql.Append(tempParam);
                            }
                            dbParameters.Add(db.CreateDbParameter(tempPartsInpoundPlanWithNum, ids[i]));
                        }
                        sql.Append(@")");
                    } else {
                        if(userId.HasValue) {
                            sql.Append(@"  and ( EXISTS (SELECT 
          1 AS C1
          FROM WarehouseOperator Extent3
          WHERE Extent3.OperatorId = {0}userId AND Extent3.WarehouseId = a.WarehouseId)) ");
                            dbParameters.Add(db.CreateDbParameter("userId", userId.Value));
                        }
                        if(partsPurchaseOrderTypeId.HasValue) {
                            //采购订单类型
                            sql.Append(@" and  PartsPurchaseOrderTypeId={0}partsPurchaseOrderTypeId ");
                            dbParameters.Add(db.CreateDbParameter("partsPurchaseOrderTypeId", partsPurchaseOrderTypeId.Value));
                        }
                        if(partsSalesCategoryId.HasValue) {
                            sql.Append(@" and partsSalesCategoryId = {0}partsSalesCategoryId ");
                            dbParameters.Add(db.CreateDbParameter("partsSalesCategoryId", partsSalesCategoryId.Value));
                        }
                        if(status.HasValue) {
                            sql.Append(@" and status = {0}status ");
                            dbParameters.Add(db.CreateDbParameter("status", status.Value));
                        }
                        if(!String.IsNullOrEmpty(partsInboundPlanCode)) {
                            sql.Append(@" and LOWER(Code) like {0}partsInboundPlanCode ");
                            dbParameters.Add(db.CreateDbParameter("partsInboundPlanCode", "%" + partsInboundPlanCode.ToLower() + "%"));
                        }
                        if(hasDifference == true) {
                            sql.Append(@" and exists (select 1 from PartsDifferenceBackBill where status <>99 and SourceCode=a.Code) ");
                        }
                        if(hasDifference == false) {
                            sql.Append(@" and not exists (select 1 from PartsDifferenceBackBill where status <>99 and SourceCode=a.Code) ");
                        }
                        if(!String.IsNullOrEmpty(sourceCode)) {
                            sql.Append(@" and LOWER(sourceCode) like {0}sourceCode ");
                            dbParameters.Add(db.CreateDbParameter("sourceCode", "%" + sourceCode.ToLower() + "%"));
                        }
                        if(warehouseId.HasValue) {
                            sql.Append(@" and warehouseId = {0}warehouseId ");
                            dbParameters.Add(db.CreateDbParameter("warehouseId", warehouseId.Value));
                        }
                        if(inboundType.HasValue) {
                            sql.Append(@" and inboundType = {0}inboundType ");
                            dbParameters.Add(db.CreateDbParameter("inboundType", inboundType.Value));
                        }
                        if(createTimeBegin.HasValue) {
                            sql.Append(@" and createTime >=to_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(@" and createTime <=to_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                        if(!String.IsNullOrEmpty(counterpartCompanyCode)) {
                            sql.Append(@" and LOWER(counterpartCompanyCode) like {0}counterpartCompanyCode ");
                            dbParameters.Add(db.CreateDbParameter("counterpartCompanyCode", "%" + counterpartCompanyCode.ToLower() + "%"));
                        }
                        if(!String.IsNullOrEmpty(counterpartCompanyName)) {
                            sql.Append(@" and LOWER(counterpartCompanyName) like {0}counterpartCompanyName ");
                            dbParameters.Add(db.CreateDbParameter("counterpartCompanyName", "%" + counterpartCompanyName.ToLower() + "%"));
                        }
                        if(!String.IsNullOrEmpty(SAPPurchasePlanCode)) {
                            sql.Append(@" and LOWER(SAPPurchasePlanCode) like {0}SAPPurchasePlanCode ");
                            dbParameters.Add(db.CreateDbParameter("SAPPurchasePlanCode", "%" + SAPPurchasePlanCode.ToLower() + "%"));
                        }
                        if(!String.IsNullOrEmpty(sparepartCode)) {
                            sql.Append(@" and sparepartCode like {0}sparepartCode ");
                            dbParameters.Add(db.CreateDbParameter("sparepartCode", "%" + sparepartCode + "%"));
                        }
                        if(!String.IsNullOrEmpty(sparepartName)) {
                            sql.Append(@" and sparepartName like {0}sparepartName ");
                            dbParameters.Add(db.CreateDbParameter("sparepartName", "%" + sparepartName + "%"));
                        }
                        if(!String.IsNullOrEmpty(originalRequirementBillCode)) {
                            sql.Append(@" and originalRequirementBillCode like {0}originalRequirementBillCode ");
                            dbParameters.Add(db.CreateDbParameter("originalRequirementBillCode", "%" + originalRequirementBillCode + "%"));
                        }
                        if(planDeliveryTimeBegin.HasValue) {
                            sql.Append(@" and PlanDeliveryTime >=to_date({0}planDeliveryTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = planDeliveryTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("planDeliveryTimeBegin", tempTime.ToString("G")));
                        }
                        if(planDeliveryTimeEnd.HasValue) {
                            sql.Append(@" and PlanDeliveryTime <=to_date({0}planDeliveryTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = planDeliveryTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("planDeliveryTimeEnd", tempTime.ToString("G")));
                        }
                    }
                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    ErrorStrings.Export_Title_PartsInboundCheckBill_PartsInboundPlanCode, ErrorStrings.Export_Title_Company_WarehouseName, ErrorStrings.Export_Title_PackingTask_InboundType, ErrorStrings.Export_Title_BoxUpTask_CounterpartCompanyCode, ErrorStrings.Export_Title_BoxUpTask_CounterpartCompanyName, ErrorStrings.Export_Title_InternalAllocationBill_TotalAmount, ErrorStrings.Export_Title_PartsInboundCheckBill_PurchaseType, ErrorStrings.Export_Title_PartsInboundPlan_OriginalRequirementBillCode, ErrorStrings.Export_Title_Partssalescategory_Name, ErrorStrings.Export_Title_Company_WarehouseCode, ErrorStrings.Export_Title_AccountPeriod_Status, ErrorStrings.Export_Title_PartsOuterPurchaseChange_SourceCode, ErrorStrings.Export_Title_PartsInboundCheckBill_ReturnType, ErrorStrings.Export_Title_PartsBranch_Remark, ErrorStrings.Export_Title_SupplierShippingOrder_ERPSourceOrderCode, ErrorStrings.Export_Title_PartsPurchaseOrder_RequestedDeliveryTime,ErrorStrings.Export_Title_PartsInboundPlan_PlanDeliveryTime, ErrorStrings.Export_Title_PartsPurchaseOrder_PlanSource, ErrorStrings.Export_Title_AccountPeriod_CreateTime, ErrorStrings.Export_Title_SupplierShippingOrder_ArriveDate, ErrorStrings.Export_Title_WarehouseAreaCategory_Area, ErrorStrings.Export_Title_PartsInboundCheckBill_SparePartsLocation, ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode, ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName, ErrorStrings.Export_Title_PartsPurchasePlanDetail_PlanAmount, ErrorStrings.Export_Title_PartsInboundCheckBill_InspectedQuantity, ErrorStrings.Export_Title_PartsPurReturnOrder_Price, ErrorStrings.Export_Title_PackingPropertyApp_MeasureUnit, ErrorStrings.Export_Title_PartsPurchaseOrder_DetailRemark, ErrorStrings.Export_Title_SupplierShippingOrder_DetailRemark,ErrorStrings.Export_Title_PackingTask_HasDifference
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }

        /// <summary>
        /// 合并导出配件入库计划主清单ForQuery(无价格)
        /// </summary>
        public bool ExportPartsInboundPlanWithDetailForQueryNoPrice(int[] ids, int? partsSalesCategoryId, int? status, int? userId, int storageCompanyId, string partsInboundPlanCode, string sourceCode, int? warehouseId, int? inboundType, DateTime? createTimeBegin, DateTime? createTimeEnd, string counterpartCompanyCode, string counterpartCompanyName, string eRPOrderCode, string partsSalesOrderCode, int? partsPurchaseOrderTypeId, out string fileName) {
            fileName = GetExportFilePath("配件入库计划主清单.xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.AppendFormat(@"Select a.Code As Partsinboundplancode,
                                               a.Warehousecode,
                                               a.Warehousename,
                                               (select value from keyvalueitem where NAME = 'Parts_InboundType'and key=a.Inboundtype) As Inboundtype,
                                               c.Name As Partssalescategoryname,
                                               a.Branchname,
                                               a.Counterpartcompanycode,
                                               a.Counterpartcompanyname,
                                                p.Name,
                                               (select value from keyvalueitem where NAME = 'PartsInboundPlan_Status'and key=a.Status) As Status,
                                               a.OriginalRequirementBillCode,
                                               a.Sourcecode,
                                               (select value from keyvalueitem where NAME = 'PartsSalesReturn_ReturnType'and key=f.ReturnType) As ReturnType, 
                                               a.Remark,  
                                               g.ERPOrderCode,
                                               g.Code,                                   
                                               a.RequestedDeliveryTime,
                                                po.PlanSource,
                                               --a.Creatorname,
                                               a.Createtime,
                                               --a.Modifiername,
                                               --a.Modifytime,
                                               a.Arrivaldate,
                                               (Select min(ss.code)
                                                  From Warehousearea t
                                                 Inner Join warehousearea Ss
                                                  On t.parentid=ss.id
                                                 Inner Join Partsstock v
                                                    On t.Id = v.Warehouseareaid
                                                 Inner Join Warehouseareacategory x
                                                    On x.Id = t.Areacategoryid
                                                 Where b.Sparepartid = v.Partid
                                                   And a.Warehouseid = v.Warehouseid
                                                   And t.Areakind = 3
                                                   And x.Category = 1),
                                               (Select Min(t.Code)
                                                  From Warehousearea t
                                                 Inner Join Partsstock v
                                                    On t.Id = v.Warehouseareaid
                                                 Inner Join Warehouseareacategory x
                                                    On x.Id = t.Areacategoryid
                                                 Where b.Sparepartid = v.Partid
                                                   And a.Warehouseid = v.Warehouseid
                                                   And t.Areakind = 3
                                                   And x.Category = 1),
                                               b.Sparepartcode,
                                               b.Sparepartname,
                                               b.Plannedamount,
                                               b.Inspectedquantity,
                                               (Select t.MeasureUnit From SparePart t Where t.id=b.sparepartid),
                                               b.POCode,
                                               b.Remark,
                                               b.SpareOrderRemark
                                          From Partsinboundplan a
                                          Left Outer Join PartsSalesReturnBill f
                                            On a.SourceId = f.Id   
                                          Left Outer Join PartsSalesOrder g
                                            On f.PartsSalesOrderId = g.Id  
                                         left join PartsPurchaseOrder po
                                            on a.OriginalRequirementBillCode=po.Code
                                         left join PartsPurchaseOrderType p
                                            on po.PartsPurchaseOrderTypeId=p.Id
                                         left Join Partsinboundplandetail b
                                            On a.Id = b.Partsinboundplanid
                                         left Join Partssalescategory c
                                            On a.Partssalescategoryid = c.Id
                                            where a.StorageCompanyId = {0} ", storageCompanyId);
                    var dbParameters = new List<DbParameter>();
                    if(ids != null && ids.Length > 0) {
                        sql.Append(@" and a.id in (");
                        for(var i = 0; i < ids.Length; i++) {
                            var tempPartsInpoundPlanWithNum = "partsInpoundPlan" + i;
                            if(i == ids.Length - 1) {
                                var tempParam = "{0}" + tempPartsInpoundPlanWithNum;
                                sql.Append(tempParam);
                            } else {
                                var tempParam = "{0}" + tempPartsInpoundPlanWithNum + ",";
                                sql.Append(tempParam);
                            }
                            dbParameters.Add(db.CreateDbParameter(tempPartsInpoundPlanWithNum, ids[i]));
                        }
                        sql.Append(@")");
                    } else {
                        if(userId.HasValue) {
                            sql.Append(@"  and ( EXISTS (SELECT 
          1 AS C1
          FROM WarehouseOperator Extent3
          WHERE Extent3.OperatorId = {0}userId AND Extent3.WarehouseId = a.WarehouseId)) ");
                            dbParameters.Add(db.CreateDbParameter("userId", userId.Value));
                        }
                        if(partsPurchaseOrderTypeId.HasValue) {
                            //采购订单类型
                            sql.Append(@" and  po.PartsPurchaseOrderTypeId={0}partsPurchaseOrderTypeId ");
                            dbParameters.Add(db.CreateDbParameter("partsPurchaseOrderTypeId", partsPurchaseOrderTypeId.Value));
                        }
                        if(partsSalesCategoryId.HasValue) {
                            sql.Append(@" and a.partsSalesCategoryId = {0}partsSalesCategoryId ");
                            dbParameters.Add(db.CreateDbParameter("partsSalesCategoryId", partsSalesCategoryId.Value));
                        }
                        if(status.HasValue) {
                            sql.Append(@" and a.status = {0}status ");
                            dbParameters.Add(db.CreateDbParameter("status", status.Value));
                        }
                        if(!String.IsNullOrEmpty(partsInboundPlanCode)) {
                            sql.Append(@" and LOWER(a.Code) like {0}partsInboundPlanCode ");
                            dbParameters.Add(db.CreateDbParameter("partsInboundPlanCode", "%" + partsInboundPlanCode.ToLower() + "%"));
                        }
                        if(!String.IsNullOrEmpty(sourceCode)) {
                            sql.Append(@" and LOWER(a.sourceCode) like {0}sourceCode ");
                            dbParameters.Add(db.CreateDbParameter("sourceCode", "%" + sourceCode.ToLower() + "%"));
                        }
                        if(warehouseId.HasValue) {
                            sql.Append(@" and a.warehouseId = {0}warehouseId ");
                            dbParameters.Add(db.CreateDbParameter("warehouseId", warehouseId.Value));
                        }
                        if(inboundType.HasValue) {
                            sql.Append(@" and a.inboundType = {0}inboundType ");
                            dbParameters.Add(db.CreateDbParameter("inboundType", inboundType.Value));
                        }
                        if(createTimeBegin.HasValue) {
                            sql.Append(@" and a.createTime >=to_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(@" and a.createTime <=to_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                        if(!String.IsNullOrEmpty(counterpartCompanyCode)) {
                            sql.Append(@" and LOWER(a.counterpartCompanyCode) like {0}counterpartCompanyCode ");
                            dbParameters.Add(db.CreateDbParameter("counterpartCompanyCode", "%" + counterpartCompanyCode.ToLower() + "%"));
                        }
                        if(!String.IsNullOrEmpty(counterpartCompanyName)) {
                            sql.Append(@" and LOWER(a.counterpartCompanyName) like {0}counterpartCompanyName ");
                            dbParameters.Add(db.CreateDbParameter("counterpartCompanyName", "%" + counterpartCompanyName.ToLower() + "%"));
                        }
                        if(!string.IsNullOrEmpty(eRPOrderCode)) {
                            sql.Append("and LOWER(g.eRPOrderCode) like {0}eRPOrderCode ");
                            dbParameters.Add(db.CreateDbParameter("eRPOrderCode", "%" + eRPOrderCode.ToLower() + "%"));
                        }
                        if(!string.IsNullOrEmpty(partsSalesOrderCode)) {
                            sql.Append("and LOWER(g.code) like {0}code ");
                            dbParameters.Add(db.CreateDbParameter("code", "%" + partsSalesOrderCode.ToLower() + "%"));
                        }
                    }

                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    ErrorStrings.Export_Title_PartsInboundCheckBill_PartsInboundPlanCode,ErrorStrings.Export_Title_Company_WarehouseCode,ErrorStrings.Export_Title_Company_WarehouseName, ErrorStrings.Export_Title_PackingTask_InboundType,ErrorStrings.Export_Title_Partssalescategory_Name,ErrorStrings.Export_Title_PartsBranch_BranchName,ErrorStrings.Export_Title_BoxUpTask_CounterpartCompanyCode, ErrorStrings.Export_Title_BoxUpTask_CounterpartCompanyName,ErrorStrings.Export_Title_PartsInboundCheckBill_PurchaseType,ErrorStrings.Export_Title_AccountPeriod_Status,ErrorStrings.Export_Title_PartsInboundPlan_OriginalRequirementBillCode,ErrorStrings.Export_Title_PartsOuterPurchaseChange_SourceCode,ErrorStrings.Export_Title_PartsInboundCheckBill_ReturnType,ErrorStrings.Export_Title_PartsBranch_Remark,ErrorStrings.Export_Title_SupplierShippingOrder_ERPSourceOrderCode,ErrorStrings.Export_Title_SupplierShippingOrder_SalesOrderCode,ErrorStrings.Export_Title_PartsPurchaseOrder_RequestedDeliveryTime,ErrorStrings.Export_Title_PartsPurchaseOrder_PlanSource,ErrorStrings.Export_Title_AccountPeriod_CreateTime,ErrorStrings.Export_Title_SupplierShippingOrder_ArriveDate,ErrorStrings.Export_Title_WarehouseAreaCategory_Area,ErrorStrings.Export_Title_PartsInboundCheckBill_SparePartsLocation,
                                    ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode, ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName, ErrorStrings.Export_Title_PartsPurchasePlanDetail_PlanAmount, ErrorStrings.Export_Title_PartsInboundCheckBill_InspectedQuantity,ErrorStrings.Export_Title_PackingPropertyApp_MeasureUnit,ErrorStrings.Export_Title_PartsInboundPlan_POCode,ErrorStrings.Export_Title_PartsPurchaseOrder_DetailRemark,ErrorStrings.Export_Title_SupplierShippingOrder_DetailRemark
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }


        /// <summary>
        /// 合并导出配件入库计划主清单ForQuery
        /// </summary>
        public bool ExportPartsInboundPlanWithDetailForWarehouse(int[] ids, int? partsSalesCategoryId, int? status, int? userId, int storageCompanyId, string partsInboundPlanCode, string sourceCode, int? warehouseId, int? inboundType, DateTime? createTimeBegin, DateTime? createTimeEnd, string counterpartCompanyCode, string counterpartCompanyName, string eRPOrderCode, string partsSalesOrderCode, int? partsPurchaseOrderTypeId, string SAPPurchasePlanCode, string sparepartCode, string sparepartName, string originalRequirementBillCode, DateTime? planDeliveryTimeBegin, DateTime? planDeliveryTimeEnd,bool? hasDifference, out string fileName) {
            fileName = GetExportFilePath("配件入库计划主清单.xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.AppendFormat(@"select Code,
       Warehousename,
       (select value
          from keyvalueitem
         where NAME = 'Parts_InboundType'
           and key = a.Inboundtype) As Inboundtype,
       Counterpartcompanycode,
       Counterpartcompanyname,
       Amount,
       Name,
       OriginalRequirementBillCode,
       Partssalescategoryname,
       Warehousecode,
       (select value
          from keyvalueitem
         where NAME = 'PartsInboundPlan_Status'
           and key = a.Status) As Status,
       Sourcecode,
       (select value
          from keyvalueitem
         where NAME = 'PartsSalesReturn_ReturnType'
           and key = a.ReturnType) As ReturnType,
       Remark,
       ERPSourceOrderCode,
       RequestedDeliveryTime,
       PlanDeliveryTime,
       PlanSource,
       Createtime,
       Arrivaldate,
       sscode,
       tcode,
       Sparepartcode,
       Sparepartname,
       Plannedamount,
       Inspectedquantity,
       Price,
       MeasureUnit,
       dRemark,
       SpareOrderRemark,
       HasDifference 
  from (Select a.Code,
               a.id,a.StorageCompanyId,
               a.Warehousename,
               a.Inboundtype,
               a.Counterpartcompanycode,
                po.PartsPurchaseOrderTypeId,
                a.partsSalesCategoryId,
                 a.warehouseId,
               a.Counterpartcompanyname,
               (Select Sum(m.RetailGuidePrice * t.PlannedAmount)  From Partsinboundplandetail t  join PartsRetailGuidePrice m    on t.sparepartid = m.sparepartid  and m.status = 1
                 Where t.PartsInboundPlanId = a.id and m.branchid = a.branchid and m.PartsSalesCategoryId = a.partssalescategoryid) as  Amount,
               p.Name,
               a.OriginalRequirementBillCode,
               c.Name As Partssalescategoryname,
               a.Warehousecode,
               a. Status,
               a.Sourcecode,
               f.ReturnType,
               a.Remark,
               a.ERPSourceOrderCode,
               a.RequestedDeliveryTime,
               a.PlanDeliveryTime,
               po.PlanSource,
               a.Createtime,
               a.Arrivaldate,
               (Select min(ss.code)
                  From Warehousearea t
                 Inner Join warehousearea Ss
                    On t.parentid = ss.id
                 Inner Join Partsstock v
                    On t.Id = v.Warehouseareaid
                 Inner Join Warehouseareacategory x
                    On x.Id = t.Areacategoryid
                 Where b.Sparepartid = v.Partid
                   And a.Warehouseid = v.Warehouseid
                   And t.Areakind = 3
                   And x.Category = 1) as sscode,
               (Select Min(t.Code)
                  From Warehousearea t
                 Inner Join Partsstock v
                    On t.Id = v.Warehouseareaid
                 Inner Join Warehouseareacategory x
                    On x.Id = t.Areacategoryid
                 Where b.Sparepartid = v.Partid
                   And a.Warehouseid = v.Warehouseid
                   And t.Areakind = 3
                   And x.Category = 1) as tcode,
               b.Sparepartcode,
               b.Sparepartname,
               b.Plannedamount,
               b.Inspectedquantity,
                m.RetailGuidePrice as Price,
               (Select t.MeasureUnit
                  From SparePart t
                 Where t.id = b.sparepartid) as MeasureUnit,
               b.POCode,
               b.Remark as dremark,
               b.SpareOrderRemark,
               a.SAPPurchasePlanCode, case (select count(1) from PartsDifferenceBackBill where rownum=1 and status<>99 and SourceCode=a.OriginalRequirementBillCode) when 1 then cast('是' as varchar2(100)) else cast('否' as varchar2(100)) end as HasDifference  
          From Partsinboundplan a
          Left Outer Join PartsSalesReturnBill f
            On a.OriginalRequirementBillCode = f.Code
          left join PartsPurchaseOrder po
            on a.OriginalRequirementBillCode = po.Code
            left join PartsTransferOrder pt on a.OriginalRequirementBillCode = pt.Code
          left join PartsPurchaseOrderType p
            on po.PartsPurchaseOrderTypeId = p.Id
          left Join Partsinboundplandetail b
            On a.Id = b.Partsinboundplanid
          left join PartsRetailGuidePrice m  on a.BranchId=m.BranchId and a.PartsSalesCategoryId=m.PartsSalesCategoryId and b.SparePartId=m.SparePartId and m.Status=1
          left Join Partssalescategory c
            On a.Partssalescategoryid = c.Id)a
                                            where StorageCompanyId = {0} ", storageCompanyId);
                    var dbParameters = new List<DbParameter>();
                    if(ids != null && ids.Length > 0) {
                        sql.Append(@" and id in (");
                        for(var i = 0; i < ids.Length; i++) {
                            var tempPartsInpoundPlanWithNum = "partsInpoundPlan" + i;
                            if(i == ids.Length - 1) {
                                var tempParam = "{0}" + tempPartsInpoundPlanWithNum;
                                sql.Append(tempParam);
                            } else {
                                var tempParam = "{0}" + tempPartsInpoundPlanWithNum + ",";
                                sql.Append(tempParam);
                            }
                            dbParameters.Add(db.CreateDbParameter(tempPartsInpoundPlanWithNum, ids[i]));
                        }
                        sql.Append(@")");
                    } else {
                        if(userId.HasValue) {
                            sql.Append(@"  and ( EXISTS (SELECT 
          1 AS C1
          FROM WarehouseOperator Extent3
          WHERE Extent3.OperatorId = {0}userId AND Extent3.WarehouseId = a.WarehouseId)) ");
                            dbParameters.Add(db.CreateDbParameter("userId", userId.Value));
                        }
                        if(hasDifference == true) { 
                            sql.Append(@" and exists (select 1 from PartsDifferenceBackBill where status <>99 and SourceCode=a.Code) "); 
                        } 
                        if(hasDifference == false) { 
                            sql.Append(@" and not exists (select 1 from PartsDifferenceBackBill where status <>99 and SourceCode=a.Code) "); 
                        } 
                        if(partsPurchaseOrderTypeId.HasValue) {
                            //采购订单类型
                            sql.Append(@" and  PartsPurchaseOrderTypeId={0}partsPurchaseOrderTypeId ");
                            dbParameters.Add(db.CreateDbParameter("partsPurchaseOrderTypeId", partsPurchaseOrderTypeId.Value));
                        }
                        if(partsSalesCategoryId.HasValue) {
                            sql.Append(@" and partsSalesCategoryId = {0}partsSalesCategoryId ");
                            dbParameters.Add(db.CreateDbParameter("partsSalesCategoryId", partsSalesCategoryId.Value));
                        }
                        if(status.HasValue) {
                            sql.Append(@" and status = {0}status ");
                            dbParameters.Add(db.CreateDbParameter("status", status.Value));
                        }
                        if(!String.IsNullOrEmpty(partsInboundPlanCode)) {
                            sql.Append(@" and LOWER(Code) like {0}partsInboundPlanCode ");
                            dbParameters.Add(db.CreateDbParameter("partsInboundPlanCode", "%" + partsInboundPlanCode.ToLower() + "%"));
                        }
                        if(!String.IsNullOrEmpty(sourceCode)) {
                            sql.Append(@" and LOWER(sourceCode) like {0}sourceCode ");
                            dbParameters.Add(db.CreateDbParameter("sourceCode", "%" + sourceCode.ToLower() + "%"));
                        }
                        if(warehouseId.HasValue) {
                            sql.Append(@" and warehouseId = {0}warehouseId ");
                            dbParameters.Add(db.CreateDbParameter("warehouseId", warehouseId.Value));
                        }
                        if(inboundType.HasValue) {
                            sql.Append(@" and inboundType = {0}inboundType ");
                            dbParameters.Add(db.CreateDbParameter("inboundType", inboundType.Value));
                        }
                        if(createTimeBegin.HasValue) {
                            sql.Append(@" and createTime >=to_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(@" and createTime <=to_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                        if(!String.IsNullOrEmpty(counterpartCompanyCode)) {
                            sql.Append(@" and LOWER(counterpartCompanyCode) like {0}counterpartCompanyCode ");
                            dbParameters.Add(db.CreateDbParameter("counterpartCompanyCode", "%" + counterpartCompanyCode.ToLower() + "%"));
                        }
                        if(!String.IsNullOrEmpty(counterpartCompanyName)) {
                            sql.Append(@" and LOWER(counterpartCompanyName) like {0}counterpartCompanyName ");
                            dbParameters.Add(db.CreateDbParameter("counterpartCompanyName", "%" + counterpartCompanyName.ToLower() + "%"));
                        }
                        if(!string.IsNullOrEmpty(eRPOrderCode)) {
                            sql.Append("and LOWER(eRPOrderCode) like {0}eRPOrderCode ");
                            dbParameters.Add(db.CreateDbParameter("eRPOrderCode", "%" + eRPOrderCode.ToLower() + "%"));
                        }
                        if(!string.IsNullOrEmpty(partsSalesOrderCode)) {
                            sql.Append("and LOWER(PartsSalesOrderCode) like {0}code ");
                            dbParameters.Add(db.CreateDbParameter("code", "%" + partsSalesOrderCode.ToLower() + "%"));
                        }
                        if(!String.IsNullOrEmpty(SAPPurchasePlanCode)) {
                            sql.Append(@" and LOWER(SAPPurchasePlanCode) like {0}SAPPurchasePlanCode ");
                            dbParameters.Add(db.CreateDbParameter("SAPPurchasePlanCode", "%" + SAPPurchasePlanCode.ToLower() + "%"));
                        }
                        if(!String.IsNullOrEmpty(sparepartCode)) {
                            sql.Append(@" and sparepartCode like {0}sparepartCode ");
                            dbParameters.Add(db.CreateDbParameter("sparepartCode", "%" + sparepartCode + "%"));
                        }
                        if(!String.IsNullOrEmpty(sparepartName)) {
                            sql.Append(@" and sparepartName like {0}sparepartName ");
                            dbParameters.Add(db.CreateDbParameter("sparepartName", "%" + sparepartName + "%"));
                        }
                        if(!String.IsNullOrEmpty(originalRequirementBillCode)) {
                            sql.Append(@" and originalRequirementBillCode like {0}originalRequirementBillCode ");
                            dbParameters.Add(db.CreateDbParameter("originalRequirementBillCode", "%" + originalRequirementBillCode + "%"));
                        }
                        if(planDeliveryTimeBegin.HasValue) {
                            sql.Append(@" and PlanDeliveryTime >=to_date({0}planDeliveryTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = planDeliveryTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("planDeliveryTimeBegin", tempTime.ToString("G")));
                        }
                        if(planDeliveryTimeEnd.HasValue) {
                            sql.Append(@" and PlanDeliveryTime <=to_date({0}planDeliveryTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = planDeliveryTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("planDeliveryTimeEnd", tempTime.ToString("G")));
                        }
                    }
                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    ErrorStrings.Export_Title_PartsInboundCheckBill_PartsInboundPlanCode, ErrorStrings.Export_Title_Company_WarehouseName, ErrorStrings.Export_Title_PackingTask_InboundType, ErrorStrings.Export_Title_BoxUpTask_CounterpartCompanyCode, ErrorStrings.Export_Title_BoxUpTask_CounterpartCompanyName, ErrorStrings.Export_Title_InternalAllocationBill_TotalAmount, ErrorStrings.Export_Title_PartsInboundCheckBill_PurchaseType, ErrorStrings.Export_Title_PartsInboundPlan_OriginalRequirementBillCode, ErrorStrings.Export_Title_Partssalescategory_Name, ErrorStrings.Export_Title_Company_WarehouseCode, ErrorStrings.Export_Title_AccountPeriod_Status, ErrorStrings.Export_Title_PartsOuterPurchaseChange_SourceCode, ErrorStrings.Export_Title_PartsInboundCheckBill_ReturnType, ErrorStrings.Export_Title_PartsBranch_Remark, ErrorStrings.Export_Title_SupplierShippingOrder_ERPSourceOrderCode, ErrorStrings.Export_Title_PartsPurchaseOrder_RequestedDeliveryTime,ErrorStrings.Export_Title_PartsInboundPlan_PlanDeliveryTime, ErrorStrings.Export_Title_PartsPurchaseOrder_PlanSource, ErrorStrings.Export_Title_AccountPeriod_CreateTime, ErrorStrings.Export_Title_SupplierShippingOrder_ArriveDate, ErrorStrings.Export_Title_WarehouseAreaCategory_Area, ErrorStrings.Export_Title_PartsInboundCheckBill_SparePartsLocation, ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode, ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName, ErrorStrings.Export_Title_PartsPurchasePlanDetail_PlanAmount, ErrorStrings.Export_Title_PartsInboundCheckBill_InspectedQuantity, ErrorStrings.Export_Title_PartsPurReturnOrder_Price, ErrorStrings.Export_Title_PackingPropertyApp_MeasureUnit, ErrorStrings.Export_Title_PartsPurchaseOrder_DetailRemark, ErrorStrings.Export_Title_SupplierShippingOrder_DetailRemark,ErrorStrings.Export_Title_PackingTask_HasDifference
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }
    }
}