﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        /// <summary>
        /// 合并导出配件盘点主清单
        /// </summary>
        public bool ExportPartsInventoryBillWithDetail(int? id, int storageCompanyId, string partsInventoryBillCode, int? warehouseId, int? warehouseAreaCategory, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, int[] storageCompanyType, out string fileName) {
            fileName = GetExportFilePath("配件盘点主清单_.xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var userId = Utils.GetCurrentUserInfo().Id;
                var db = DbHelper.GetDbHelp(ConnectionString);
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    #region SQL语句
                    sql.AppendFormat(@"Select a.Code,
                                               a.Warehousename,
                                               (select value from keyvalueitem where NAME = 'Area_Category'and key=a.Warehouseareacategory) As Warehouseareacategory,
                                               (select value from keyvalueitem where NAME = 'PartsInventoryBill_Status'and key=a.Status) As Status,
                                               a.Initiatorname,
                                               a.Inventoryreason,
                                               (Select Sum(nvl(t.CostPrice, 0) * t.CurrentStorage)
                                                  From PartsInventoryDetail t
                                                 Where t.PartsInventoryBillId = a.Id), --盘点前成本总额
                                               (Select Sum(nvl(t.CostPrice, 0) * t.StorageAfterInventory)
                                                  From PartsInventoryDetail t
                                                 Where t.PartsInventoryBillId = a.Id), --盘点后成本总额
                                              (Select sum (case when t.ifcover =1 then t.StorageDifference*nvl(t.CostPrice, 0) else 0 end )
                                              From PartsInventoryDetail t
                                              Where t.PartsInventoryBillId = a.Id),--成本差异金额
                                               a.Remark,
                                               a.Creatorname,
                                               a.Createtime,a.InitialApproverName,a.InitialApproveTime,a.CheckerName,a.CheckTime,a.ApproverName,a.ApproveTime,a.InventoryRecordOperatorName,a.InventoryRecordTime,
                                               b.Sparepartcode,
                                               b.Sparepartname,
                                               b.Warehouseareacode,
                                               b.Currentstorage,
											   decode(b.CostPrice,null,null,b.StorageAfterInventory),
											   decode(b.CostPrice,null,null,b.StorageDifference),
                                               b.CostPrice,
                                              (select value from keyvalueitem where NAME = 'IsOrNot'and key=b.Ifcover) As Ifcover,
                                               b.Remark
                                          From Partsinventorybill a
                                          INNER JOIN Warehouse Extent2 ON a.WarehouseId = Extent2.Id
                                  INNER JOIN SalesUnitAffiWarehouse Extent3 ON Extent2.Id = Extent3.WarehouseId
                                  INNER JOIN SalesUnit Extent4 ON Extent3.SalesUnitId = Extent4.Id
                                  INNER JOIN PersonSalesCenterLink Extent5 ON Extent4.PartsSalesCategoryId = Extent5.PartsSalesCategoryId
                                          Inner Join Partsinventorydetail b
                                            On b.Partsinventorybillid = a.Id
                                    where Extent2.Status = 1 and Extent4.Status = 1 and Extent5.Status = 1 and a.StorageCompanyId = {0} and  Extent5.PersonId={1}", storageCompanyId, userId);
                    #endregion
                    var dbParameters = new List<DbParameter>();
                    if(id.HasValue) {
                        sql.Append(" and a.id = {0}id");
                        dbParameters.Add(db.CreateDbParameter("id", id.Value));
                    } else {
                        if(!string.IsNullOrEmpty(partsInventoryBillCode)) {
                            sql.Append("and a.Code like {0}partsInventoryBillCode ");
                            dbParameters.Add(db.CreateDbParameter("partsInventoryBillCode", "%" + partsInventoryBillCode + "%"));
                        }
                        if(warehouseId.HasValue) {
                            sql.Append("and a.warehouseId = {0}warehouseId ");
                            dbParameters.Add(db.CreateDbParameter("warehouseId", warehouseId.Value));
                        }
                        if(warehouseAreaCategory.HasValue) {
                            sql.Append("and c.Category ={0}warehouseAreaCategory ");
                            dbParameters.Add(db.CreateDbParameter("warehouseAreaCategory", warehouseAreaCategory.Value));
                        }
                        if(status.HasValue) {
                            sql.Append("and a.status ={0}status ");
                            dbParameters.Add(db.CreateDbParameter("status", status.Value));
                        }
                        if(createTimeBegin.HasValue) {
                            sql.Append(@" and a.createTime >=To_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(@" and a.createTime <=To_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                        if(storageCompanyType != null && storageCompanyType.Any()) {
                            sql.Append(" and a.storageCompanyType in (");
                            for(var i = 0; i < storageCompanyType.Length; i++) {
                                sql.AppendFormat(storageCompanyType.Length == i + 1 ? "{0})" : "{0},", storageCompanyType[i]);
                            }
                        }
                    }
                    sql.Append("  order by a.code asc, b.WarehouseAreaCode asc,b.SparePartCode asc");
                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    ErrorStrings.Export_Title_Dealerpartsinventorybill_Code, ErrorStrings.Export_Title_Company_WarehouseName, ErrorStrings.Export_Title_WarehouseArea_Category, ErrorStrings.Export_Title_AccountPeriod_Status, ErrorStrings.Export_Title_PartsInventoryBill_InitiatorName, ErrorStrings.Export_Title_PartsInventoryBill_InventoryReason,ErrorStrings.Export_Title_PartsInventoryBill_BeforeCostPriceAmount,ErrorStrings.Export_Title_PartsInventoryBill_AfterCostPriceAmount, ErrorStrings.Export_Title_PartsInventoryBill_DifferenceCostPriceAmount,ErrorStrings.Export_Title_PartsBranch_Remark, ErrorStrings.Export_Title_AccountPeriod_CreatorName, ErrorStrings.Export_Title_AccountPeriod_CreateTime,"初审人","初审时间", ErrorStrings.Export_Title_Dealerpartsinventorybill_ApproverName,ErrorStrings.Export_Title_Dealerpartsinventorybill_ApproverTime,ErrorStrings.Export_Title_PackingPropertyApp_ApproverName,ErrorStrings.Export_Title_PackingPropertyApp_ApproveTime,ErrorStrings.Export_Title_PartsInventoryBill_InventoryRecordOperatorName,ErrorStrings.Export_Title_PartsInventoryBill_InventoryRecordOperatorTime,
                                    ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode, ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName, ErrorStrings.Export_Title_WarehouseArea_WarehouseAreaCode, ErrorStrings.Export_Title_Dealerpartsinventorybill_Currentstorage, ErrorStrings.Export_Title_Dealerpartsinventorybill_Storageafterinventory,ErrorStrings.Export_Title_Dealerpartsinventorybill_Storagedifference,"成本价",ErrorStrings.Export_Title_PartsInventoryBill_Ifcover, ErrorStrings.Export_Title_PartsPurchaseOrder_DetailRemark
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();

                }
                return true;
            } catch(Exception) {
                return false;
            }
        }

        /// <summary>
        /// 合并导出代理库配件盘点主清单(代理库用)
        /// </summary>
        public bool ExportAgentPartsInventoryBillWithDetail(int[] ids, int[] storageCompanyType, int? storageCompanyId, string storageCompanyCode, string storageCompanyName, int? warehouseId, string warehouseName, string code, int? warehouseAreaCategory, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName) {
            fileName = GetExportFilePath("中心库导出代理库配件盘点主清单_.xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    #region SQL语句
                    sql.Append(@"Select a.Code,
                                                     a.StorageCompanyCode,
                                                     a.StorageCompanyName,
                                                     a.warehousename,
                                                     (select value
                                                        from dcs.keyvalueitem
                                                       where caption = '库区用途'
                                                         and a.warehouseareacategory = keyvalueitem.key) as warehouseareacategory,
                                                     c.Value as status,
                                                     a.InitiatorName,
                                                     a.InventoryReason,
                                                     a.RejectComment,
                                                     (select Sum(r.CostPrice * r.CurrentStorage)
                                                        from Partsinventorydetail r
                                                       where r.Partsinventorybillid = a.id
                                                       group by r.Partsinventorybillid) as SumCostBeforeInventory,
                                                     (select Sum(r.CostPrice * r.StorageAfterInventory)
                                                        from Partsinventorydetail r
                                                       where r.Partsinventorybillid = a.id
                                                       group by r.Partsinventorybillid) as SumCostAfterInventory,
                                                     (select Sum(r.CostPrice * (r.StorageAfterInventory - r.CurrentStorage))
                                                        from Partsinventorydetail r
                                                       where r.Partsinventorybillid = a.id
                                                       group by r.Partsinventorybillid) as SumCostDifference,
                                                     a.remark,
                                                     a.creatorname,
                                                     a.createtime,
                                                     b.Sparepartcode,
                                                     b.Sparepartname,
                                                     b.Warehouseareacode,
                                                     b.Currentstorage,
                                                     b.Storageafterinventory,
                                                     b.Storagedifference,
                                                     b.CostPrice,
                                                     b.Remark 
                                                From PartsInventoryBill a
                                               Inner Join Partsinventorydetail b
                                                    On b.Partsinventorybillid = a.Id
                                               Inner Join KeyValueItem C
                                                    On c.Key = a.status
                                                    And c.Name = 'PartsInventoryBill_Status' where 1=1 ");
                    #endregion
                    var dbParameters = new List<DbParameter>();
                    if(ids != null && ids.Length > 0) {
                        sql.Append(" and a.id in (");
                        for(var i = 0; i < ids.Length; i++) {
                            if(ids.Length == i + 1) {
                                sql.Append("{0}" + ids[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter(ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            } else {
                                sql.Append("{0}" + ids[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter(ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                        }
                        sql.Append(")");
                    } else {

                        if(storageCompanyId.HasValue) {
                            sql.AppendFormat(" and a.storageCompanyId={0} ", storageCompanyId);
                        }

                        if(storageCompanyType != null && storageCompanyType.Any()) {
                            sql.Append(" and a.storageCompanyType in (");
                            for(var i = 0; i < storageCompanyType.Length; i++) {
                                sql.AppendFormat(storageCompanyType.Length == i + 1 ? "{0})" : "{0},", storageCompanyType[i]);
                            }
                        }
                        if(!string.IsNullOrEmpty(code)) {
                            sql.Append(" and a.code like {0}code ");
                            dbParameters.Add(db.CreateDbParameter("code", "%" + code + "%"));
                        }
                        if(!string.IsNullOrEmpty(storageCompanyCode)) {
                            sql.Append(" and a.storageCompanyCode like {0}storageCompanyCode ");
                            dbParameters.Add(db.CreateDbParameter("storageCompanyCode", "%" + storageCompanyCode + "%"));
                        }
                        if(!string.IsNullOrEmpty(storageCompanyName)) {
                            sql.Append(" and a.storageCompanyName like {0}storageCompanyName ");
                            dbParameters.Add(db.CreateDbParameter("storageCompanyName", "%" + storageCompanyName + "%"));
                        }
                        if(warehouseId.HasValue) {
                            sql.Append(" and a.warehouseId ={0}warehouseId ");
                            dbParameters.Add(db.CreateDbParameter("warehouseId", warehouseId.Value));
                        }
                        if(!string.IsNullOrEmpty(warehouseName)) {
                            sql.Append(" and a.warehouseName like {0}warehouseName ");
                            dbParameters.Add(db.CreateDbParameter("warehouseName", "%" + warehouseName + "%"));
                        }
                        if(warehouseAreaCategory.HasValue) {
                            sql.Append(" and c.WarehouseAreaCategory ={0}warehouseAreaCategory ");
                            dbParameters.Add(db.CreateDbParameter("warehouseAreaCategory", warehouseAreaCategory.Value));
                        }
                        if(status.HasValue) {
                            sql.Append(" and a.status ={0}status ");
                            dbParameters.Add(db.CreateDbParameter("status", status.Value));
                        }
                        if(createTimeBegin.HasValue) {
                            sql.Append(@" and a.createTime >=To_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(@" and a.createTime <=To_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                    }
                    var sqlStr = sql.ToString();
                    if(sqlStr.IndexOf(" where 1=1  and ") > 0)
                        sqlStr = sqlStr.Replace(" where 1=1  and ", " where ");
                    else
                        sqlStr = sqlStr.Replace(" where 1=1 ", "");
                    var command = db.CreateDbCommand(string.Format(sqlStr, db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                            ErrorStrings.Export_Title_Dealerpartsinventorybill_Code, ErrorStrings.Export_Title_Agency_CodeNew, ErrorStrings.Export_Title_Agency_NameNew,ErrorStrings.Export_Title_Company_WarehouseName,ErrorStrings.Export_Title_WarehouseArea_Category,ErrorStrings.Export_Title_AccountPeriod_Status,ErrorStrings.Export_Title_PartsInventoryBill_InitiatorName,ErrorStrings.Export_Title_PartsInventoryBill_InventoryReason,ErrorStrings.Export_Title_PartsOuterPurchaseChange_RejectComment,ErrorStrings.Export_Title_PartsInventoryBill_BeforeCostPriceAmount,ErrorStrings.Export_Title_PartsInventoryBill_AfterCostPriceAmount,ErrorStrings.Export_Title_PartsInventoryBill_DifferenceCostPriceAmount,ErrorStrings.Export_Title_PartsBranch_Remark,ErrorStrings.Export_Title_AccountPeriod_CreatorName,ErrorStrings.Export_Title_AccountPeriod_CreateTime,
                                            ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode, ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName, ErrorStrings.Export_Title_WarehouseArea_WarehouseAreaCode, ErrorStrings.Export_Title_Dealerpartsinventorybill_Currentstorage, ErrorStrings.Export_Title_Dealerpartsinventorybill_Storageafterinventory,ErrorStrings.Export_Title_Dealerpartsinventorybill_Storagedifference,ErrorStrings.Export_Title_PartsBranch_CostPrice, ErrorStrings.Export_Title_PartsPurchaseOrder_DetailRemark
                                        };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();

                }
                return true;
            } catch(Exception) {
                return false;
            }
        }

        /// <summary>
        /// 合并导出代理库配件盘点主清单(分公司用)
        /// </summary>
        public bool ExportAgentPartsInventoryBillWithDetailForBranch(int[] ids, int[] storageCompanyType, int? storageCompanyId, string storageCompanyCode, string storageCompanyName, int? warehouseId, string warehouseName, string code, int? warehouseAreaCategory, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, int? branchId, int? amountDifference, out string fileName){
            fileName = GetExportFilePath("分公司导出代理库配件盘点主清单_.xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var userId = Utils.GetCurrentUserInfo().Id;
                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var finalSql = "";
                    var sql = new StringBuilder();
                    #region SQL语句
                    sql.AppendFormat(@"Select distinct a.Code,
                                                     a.StorageCompanyCode,
                                                     a.StorageCompanyName,
                                                     a.warehousename,
                                                     (select value
                                                        from dcs.keyvalueitem
                                                       where caption = '库区用途'
                                                         and a.warehouseareacategory = keyvalueitem.key) as warehouseareacategory,
                                                     --a.status,
                                                     (select value from keyvalueitem where NAME = 'PartsInventoryBill_Status'and key=a.status) As Status,
                                                     
                                                     a.InventoryReason,
                                                     a.RejectComment,
                                                     (select Sum(r.CostPrice * r.CurrentStorage)
                                                        from Partsinventorydetail r
                                                       where r.Partsinventorybillid = a.id
                                                       group by r.Partsinventorybillid) as SumCostBeforeInventory,
                                                     (select Sum(r.CostPrice * r.StorageAfterInventory)
                                                        from Partsinventorydetail r
                                                       where r.Partsinventorybillid = a.id
                                                       group by r.Partsinventorybillid) as SumCostAfterInventory,
                                                     (select Sum(r.CostPrice * (r.StorageAfterInventory - r.CurrentStorage))
                                                        from Partsinventorydetail r
                                                       where r.Partsinventorybillid = a.id
                                                       group by r.Partsinventorybillid) as SumCostDifference,
                                                     a.remark,
                                                     a.creatorname,
                                                     a.createtime,a.InitialApproverName,a.InitialApproveTime,a.CheckerName,a.CheckTime,a.ApproverName,a.ApproveTime,
                                                     b.Sparepartcode,
                                                     b.Sparepartname,
                                                     b.Warehouseareacode,
                                                     b.Currentstorage,
                                                     b.Storageafterinventory,
                                                     b.Storagedifference,
                                                     b.Remark as Remark1,a.InitiatorName,a.RejectTime,RejectComment as RejectComment1
                                                From PartsInventoryBill a
                                                INNER JOIN Warehouse Extent2 ON a.WarehouseId = Extent2.Id
                                                INNER JOIN SalesUnitAffiWarehouse Extent3 ON Extent2.Id = Extent3.WarehouseId
                                                INNER JOIN SalesUnit Extent4 ON Extent3.SalesUnitId = Extent4.Id
                                                INNER JOIN PersonSalesCenterLink Extent5 ON Extent4.PartsSalesCategoryId = Extent5.PartsSalesCategoryId
                                                Inner Join Partsinventorydetail b
                                                    On b.Partsinventorybillid = a.Id
                                                     where Extent2.Status = 1 and Extent4.Status = 1 and Extent5.Status = 1 and  Extent5.PersonId={0}", userId);
                    #endregion
                    var dbParameters = new List<DbParameter>();
                    if(ids != null && ids.Length > 0) {
                        sql.Append(" and a.id in (");
                        for(var i = 0; i < ids.Length; i++) {
                            if(ids.Length == i + 1) {
                                sql.Append("{0}" + ids[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter(ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            } else {
                                sql.Append("{0}" + ids[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter(ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                        }
                        sql.Append(")");
						finalSql = sql.ToString();
                    } else {
                        if(storageCompanyType != null && storageCompanyType.Any()) {
                            sql.Append(" and a.storageCompanyType in (");
                            for(var i = 0; i < storageCompanyType.Length; i++) {
                                sql.AppendFormat(storageCompanyType.Length == i + 1 ? "{0})" : "{0},", storageCompanyType[i]);
                            }
                        }
                        if(!string.IsNullOrEmpty(code)) {
                            sql.Append("and a.code like {0}code ");
                            dbParameters.Add(db.CreateDbParameter("code", "%" + code + "%"));
                        }
                        if(!string.IsNullOrEmpty(storageCompanyCode)) {
                            sql.Append("and a.storageCompanyCode like {0}storageCompanyCode ");
                            dbParameters.Add(db.CreateDbParameter("storageCompanyCode", "%" + storageCompanyCode + "%"));
                        }
                        if(!string.IsNullOrEmpty(storageCompanyName)) {
                            sql.Append("and a.storageCompanyName like {0}storageCompanyName ");
                            dbParameters.Add(db.CreateDbParameter("storageCompanyName", "%" + storageCompanyName + "%"));
                        }
                        if(warehouseId.HasValue) {
                            sql.Append("and a.warehouseId ={0}warehouseId ");
                            dbParameters.Add(db.CreateDbParameter("warehouseId", warehouseId.Value));
                        }
                        if(!string.IsNullOrEmpty(warehouseName)) {
                            sql.Append("and a.warehouseName like {0}warehouseName ");
                            dbParameters.Add(db.CreateDbParameter("warehouseName", "%" + warehouseName + "%"));
                        }
                        if(warehouseAreaCategory.HasValue) {
                            sql.Append("and c.WarehouseAreaCategory ={0}warehouseAreaCategory ");
                            dbParameters.Add(db.CreateDbParameter("warehouseAreaCategory", warehouseAreaCategory.Value));
                        }
                        if(status.HasValue) {
                            sql.Append("and a.status ={0}status ");
                            dbParameters.Add(db.CreateDbParameter("status", status.Value));
                        }
                        if(createTimeBegin.HasValue) {
                            sql.Append(@" and a.createTime >=To_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(@" and a.createTime <=To_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                        if(branchId.HasValue) {
                            sql.Append("and a.branchId ={0}branchId ");
                            dbParameters.Add(db.CreateDbParameter("branchId", branchId.Value));
                        }
                        if(storageCompanyId.HasValue) {
                            sql.AppendFormat(" and a.storageCompanyId<>{0} ", storageCompanyId);
                        }
						if(amountDifference.HasValue){
                            finalSql = "select * from (" + sql.ToString() + " )ff where exists(select 1 from MultiLevelApproveConfig mu where mu.id=" + amountDifference.Value + " and  ff.SumCostDifference>=mu.MinApproveFee and ff.SumCostDifference<mu.MaxApproveFee)";
                        } else {
                            finalSql = sql.ToString();
                        }
                    }
                    var command = db.CreateDbCommand(string.Format(finalSql, db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                            ErrorStrings.Export_Title_Dealerpartsinventorybill_Code, ErrorStrings.Export_Title_Agency_Code, ErrorStrings.Export_Title_PartsInventoryBill_StorageCompanyName,ErrorStrings.Export_Title_Company_WarehouseName,ErrorStrings.Export_Title_WarehouseArea_Category,ErrorStrings.Export_Title_AccountPeriod_Status,ErrorStrings.Export_Title_PartsInventoryBill_InventoryReason,ErrorStrings.Export_Title_PartsOuterPurchaseChange_RejectComment,ErrorStrings.Export_Title_PartsInventoryBill_BeforeCostPriceAmount,ErrorStrings.Export_Title_PartsInventoryBill_AfterCostPriceAmount,ErrorStrings.Export_Title_PartsInventoryBill_DifferenceCostPriceAmount,ErrorStrings.Export_Title_PartsBranch_Remark,ErrorStrings.Export_Title_AccountPeriod_CreatorName,ErrorStrings.Export_Title_AccountPeriod_CreateTime,"初审人","初审时间","审核人","审核时间",ErrorStrings.Export_Title_PackingPropertyApp_ApproverName,ErrorStrings.Export_Title_PackingPropertyApp_ApproveTime,
                                            ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode, ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName, ErrorStrings.Export_Title_WarehouseArea_WarehouseAreaCode, ErrorStrings.Export_Title_Dealerpartsinventorybill_Currentstorage, ErrorStrings.Export_Title_Dealerpartsinventorybill_Storageafterinventory,ErrorStrings.Export_Title_Dealerpartsinventorybill_Storagedifference, ErrorStrings.Export_Title_PartsPurchaseOrder_DetailRemark,ErrorStrings.Export_Title_PartsInventoryBill_InitiatorName,"驳回时间","审批意见"
                                        };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();

                }
                return true;
            } catch(Exception) {
                return false;
            }
        }

        /// <summary>
        /// 导出配件盘点单
        /// </summary>
        public bool ExportPartsInventoryBill(int? id, int storageCompanyId, string partsInventoryBillCode, int? warehouseId, int? warehouseAreaCategory, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, int[] storageCompanyType, out string fileName)
        {
            fileName = GetExportFilePath("配件盘点单_.xlsx");
            try
            {
                if (!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var userId = Utils.GetCurrentUserInfo().Id;
                var db = DbHelper.GetDbHelp(ConnectionString);
                using (var conn = db.CreateDbConnection())
                {
                    conn.Open();
                    var sql = new StringBuilder();
                    #region SQL语句
                    sql.AppendFormat(@"Select a.Code,
                                               a.Warehousename,
                                               (select value
                                                  from keyvalueitem
                                                 where NAME = 'Area_Category'
                                                   and key = a.Warehouseareacategory) As Warehouseareacategory,
                                               (select value
                                                  from keyvalueitem
                                                 where NAME = 'PartsInventoryBill_Status'
                                                   and key = a.Status) As Status,
                                               a.Initiatorname,
                                               a.Inventoryreason,
                                               (select sum(nvl(detail.Costprice, 0) * nvl(detail.Currentstorage, 0))
                                                  from PartsInventoryDetail detail
                                                 where detail.partsinventorybillid = a.id
                                                 group by detail.partsinventorybillid) as SumCostBeforeInventory,
                                               (select sum(nvl(detail.Costprice, 0) *
                                                           nvl(detail.StorageAfterInventory, 0))
                                                  from PartsInventoryDetail detail
                                                 where detail.partsinventorybillid = a.id
                                                 group by detail.partsinventorybillid) as SumCostAfterInventory,
                                               (select sum(nvl(detail.Costprice, 0) * nvl(detail.StorageAfterInventory, 0)) -
                                                       sum(nvl(detail.Costprice, 0) *
                                                           nvl(detail.Currentstorage, 0))
                                                  from PartsInventoryDetail detail
                                                 where detail.partsinventorybillid = a.id
                                                 group by detail.partsinventorybillid) as SumCostDifference,
                                               a.Remark,
                                               a.Creatorname,
                                               a.Createtime
                                          From Partsinventorybill a
                                         INNER JOIN Warehouse Extent2
                                            ON a.WarehouseId = Extent2.Id
                                         INNER JOIN SalesUnitAffiWarehouse Extent3
                                            ON Extent2.Id = Extent3.WarehouseId
                                         INNER JOIN SalesUnit Extent4
                                            ON Extent3.SalesUnitId = Extent4.Id
                                         INNER JOIN PersonSalesCenterLink Extent5
                                            ON Extent4.PartsSalesCategoryId = Extent5.PartsSalesCategoryId
                                         where Extent2.Status = 1
                                           and Extent4.Status = 1
                                           and Extent5.Status = 1
                                        and a.StorageCompanyId = {0}
                                        and Extent5.PersonId = {1}
                                        ", storageCompanyId, userId);
                    #endregion
                    var dbParameters = new List<DbParameter>();
                    if (id.HasValue)
                    {
                        sql.Append(" and a.id = {0}id");
                        dbParameters.Add(db.CreateDbParameter("id", id.Value));
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(partsInventoryBillCode))
                        {
                            sql.Append("and a.Code like {0}partsInventoryBillCode ");
                            dbParameters.Add(db.CreateDbParameter("partsInventoryBillCode", "%" + partsInventoryBillCode + "%"));
                        }
                        if (warehouseId.HasValue)
                        {
                            sql.Append("and a.warehouseId = {0}warehouseId ");
                            dbParameters.Add(db.CreateDbParameter("warehouseId", warehouseId.Value));
                        }
                        if (warehouseAreaCategory.HasValue)
                        {
                            sql.Append("and c.Category ={0}warehouseAreaCategory ");
                            dbParameters.Add(db.CreateDbParameter("warehouseAreaCategory", warehouseAreaCategory.Value));
                        }
                        if (status.HasValue)
                        {
                            sql.Append("and a.status ={0}status ");
                            dbParameters.Add(db.CreateDbParameter("status", status.Value));
                        }
                        if (createTimeBegin.HasValue)
                        {
                            sql.Append(@" and a.createTime >=To_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if (createTimeEnd.HasValue)
                        {
                            sql.Append(@" and a.createTime <=To_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                        if(storageCompanyType != null && storageCompanyType.Any()) {
                            sql.Append(" and a.storageCompanyType in (");
                            for(var i = 0; i < storageCompanyType.Length; i++) {
                                sql.AppendFormat(storageCompanyType.Length == i + 1 ? "{0})" : "{0},", storageCompanyType[i]);
                            }
                        }
                    }
                    sql.Append(@" order by a.id desc");
                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using (var excelExport = new ExcelExport(fileName))
                    {
                        excelExport.ExportByRow(index => {
                            if (index == 0)
                            {
                                return new object[] {
                                    ErrorStrings.Export_Title_Dealerpartsinventorybill_Code, ErrorStrings.Export_Title_Company_WarehouseName, ErrorStrings.Export_Title_WarehouseArea_Category, ErrorStrings.Export_Title_AccountPeriod_Status, ErrorStrings.Export_Title_PartsInventoryBill_InitiatorName, ErrorStrings.Export_Title_PartsInventoryBill_InventoryReason,ErrorStrings.Export_Title_PartsInventoryBill_BeforeCostPriceAmount,ErrorStrings.Export_Title_PartsInventoryBill_AfterCostPriceAmount,ErrorStrings.Export_Title_PartsInventoryBill_DifferenceCostPriceAmount,ErrorStrings.Export_Title_PartsBranch_Remark,ErrorStrings.Export_Title_AccountPeriod_CreatorName,ErrorStrings.Export_Title_AccountPeriod_CreateTime
                                };
                            }
                            if (reader.Read())
                            {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if (num != reader.FieldCount)
                                {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();

                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }








        /// <summary>
        /// 导入配件盘点单清单
        /// </summary>
        public bool ImportPartsInventoryDetail(string fileName, out int excelImportNum, int warehouseId, int warehouseAreaCategory, int storageCompanyId, out List<PartsInventoryDetailExtend> rightData, out List<PartsInventoryDetailExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<PartsInventoryDetailExtend>();
            var rightList = new List<PartsInventoryDetailExtend>();
            var allList = new List<PartsInventoryDetailExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("PartsInventoryDetail", out notNullableFields, out fieldLenght);
                List<object> excelColumns;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    #region 指定文件中的列对应的名称 例如
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode, "SparePartCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName, "SparePartName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_WarehouseArea_WarehouseAreaCode, "WarehouseAreaCode");
                    #endregion
                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();

                    #region 获取对应枚举 var keyValuePairs = new[] { new KeyValuePair<string, string>("PartABC", "ABCStrategy_Category")}; tempExcelOperator.ImportHelper.LoadEnumFromDb(null,null, keyValuePairs);

                    #endregion
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var tempImportObj = new PartsInventoryDetailExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        #region 根据导入的字段给对应的实体属性赋值
                        tempImportObj.SparePartCodeStr = newRow["SparePartCode"];
                        tempImportObj.SparePartNameStr = newRow["SparePartName"];
                        tempImportObj.WarehouseAreaCodeStr = newRow["WarehouseAreaCode"];
                        #endregion

                        var tempErrorMessage = new List<string>();
                        #region 导入的数据基本检查
                        //配件编号检查
                        var fieldIndex = notNullableFields.IndexOf("SparePartCode".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.SparePartCodeStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchaseOrderDetail_Validation1);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.SparePartCodeStr) > fieldLenght["SparePartCode".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchaseOrderDetail_Validation2);
                        }
                        //配件名称检查
                        fieldIndex = notNullableFields.IndexOf("SparePartName".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.SparePartNameStr)) {
                            //if(fieldIndex > -1)
                            //    tempErrorMessage.Add(ErrorStrings.Export_Validation_Sparepart_Name);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.SparePartNameStr) > fieldLenght["SparePartName".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Sparepart_NameIsLong);
                        }
                        //库位编号检查
                        fieldIndex = notNullableFields.IndexOf("WarehouseAreaCode".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.WarehouseAreaCodeStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_WarehouseArea_WarehouseAreaCodeIsNull);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.WarehouseAreaCodeStr) > fieldLenght["WarehouseAreaCode".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_WarehouseArea_WarehouseAreaCodeIsLong);
                        }
                        #endregion
                        if(tempErrorMessage.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });
                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    #region 剩下的数据进行业务检查
                    //配件编号重复
                    var sparePartCodesGroup = tempRightList.GroupBy(r => new { r.SparePartCodeStr, r.WarehouseAreaCodeStr });
                    foreach(var group in sparePartCodesGroup) {
                        if(group.Count() > 1) {
                            foreach(var item in group) {
                                item.ErrorMsg = "配件编号重复";
                            }
                        }
                    }

                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    //配件有效性
                    var sparePartCodesNeedCheck = tempRightList.Select(r => r.SparePartCodeStr.ToUpper()).Distinct().ToArray();
                    var dbSparePartCodes = new List<SparePartExtend>();
                    Func<string[], bool> getDbSparePartCodes = value => {
                        var dbObj = new SparePartExtend {
                            Id = Convert.ToInt32(value[0]),
                            Code = value[1],
                            Name = value[2]
                        };
                        dbSparePartCodes.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select id,Code,Name from sparePart where status=1 ", "Code", true, sparePartCodesNeedCheck, getDbSparePartCodes);
                    foreach(var tempRight in tempRightList) {
                        var sparePart = dbSparePartCodes.FirstOrDefault(v => v.Code.ToUpper() == tempRight.SparePartCodeStr.ToUpper());
                        if(sparePart == null) {
                            tempRight.ErrorMsg = ErrorStrings.Export_Validation_PartsBranch_Validation15;
                            continue;
                        }
                        tempRight.SparePartId = sparePart.Id;
                        tempRight.SparePartName = sparePart.Name;
                    }


                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    //配件品牌有效性
                    var PartsSalesCategoryNeedCheck = tempRightList.Select(r => r.SparePartId.ToString()).Distinct().ToArray();
                    var dbPartsSalesCategory = new List<SparePartExtend>();
                    Func<string[], bool> getDbPartsSalesCategory = value => {
                        var dbObj = new SparePartExtend {
                            Id = Convert.ToInt32(value[0]),
                        };
                        dbPartsSalesCategory.Add(dbObj);
                        return false;
                    };
                    string PartsSalesCategorysql = "select partid from PartsBranch where PartsBranch.Status=1 and PartsBranch.Partssalescategoryid =(select PartsSalesCategoryId from salesunit where id =(select salesunitid from SalesUnitAffiWarehouse where warehouseid=" + warehouseId + ")) ";
                    db.QueryDataWithInOperator(PartsSalesCategorysql, "partid", true, PartsSalesCategoryNeedCheck, getDbPartsSalesCategory);
                    foreach(var tempRight in tempRightList) {
                        var sparePart = dbPartsSalesCategory.FirstOrDefault(v => v.Id == tempRight.SparePartId);
                        if(sparePart == null) {
                            tempRight.ErrorMsg = "配件营销信息不存在";
                        }
                    }

                    //库位编号有效性
                    //                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    //                    var warehouseAreaCodesNeedCheck = tempRightList.Select(r => r.WarehouseAreaCodeStr.ToUpper()).Distinct().ToArray();
                    //                    var dbWarehouseAreaCodes = new List<WarehouseAreaExtend>();
                    //                    Func<string[], bool> getDbWarehouseAreaCodes = value => {
                    //                        var dbObj = new WarehouseAreaExtend();
                    //                        dbObj.Id = Convert.ToInt32(value[0]);
                    //                        dbObj.Code = value[1];
                    //                        dbWarehouseAreaCodes.Add(dbObj);
                    //                        return false;
                    //                    };
                    //                    var sql = string.Format(@"Select a.Id, a.Code
                    //                                              From Warehousearea a
                    //                                             Inner Join Warehouseareacategory b
                    //                                                On b.Id = a.Areacategoryid
                    //                                             Where Status = 1
                    //                                               And Areakind = 3
                    //                                               And Warehouseid = {0}
                    //                                               And b.Category = {1} ", warehouseId, warehouseAreaCategory);
                    //                    db.QueryDataWithInOperator(sql, "a.Code", true, warehouseAreaCodesNeedCheck, getDbWarehouseAreaCodes);
                    //校验库存
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    var sparePartIds = dbSparePartCodes.Select(r => r.Id.ToString(CultureInfo.InvariantCulture)).Distinct().ToArray();
                    if(sparePartIds.Any()) {
                        var dbPartsStocks = new List<PartsStockExtend>();
                        Func<string[], bool> getDbPartsStocks = value => {
                            var dbObj = new PartsStockExtend {
                                PartId = Convert.ToInt32(value[0]),
                                Quantity = Convert.ToInt32(value[1]),
                                Code = value[2],
                                WarehouseAreaId = Convert.ToInt32(value[3])
                            };
                            dbPartsStocks.Add(dbObj);
                            return false;
                        };
                        var sql1 = string.Format(@"Select a.Partid,a.Quantity,c.Code,c.Id
                                              From Partsstock a
                                             Inner Join Warehouseareacategory b
                                                On b.Id = a.Warehouseareacategoryid
                                             Inner Join Warehousearea c
                                                On a.Warehouseareaid = c.Id
                                             Where a.Warehouseid = {0}
                                               And a.Storagecompanyid = {1}
                                               And b.Category = {2}  and c.status=1 ", warehouseId, storageCompanyId, warehouseAreaCategory);
                        db.QueryDataWithInOperator(sql1, "a.Partid", false, sparePartIds, getDbPartsStocks);
                        foreach(var tempRight in tempRightList) {
                            var partsStock = dbPartsStocks.FirstOrDefault(r => r.PartId == tempRight.SparePartId && r.Code == tempRight.WarehouseAreaCodeStr);
                            if(partsStock == null) {
                                tempRight.ErrorMsg = ErrorStrings.Export_Validation_BorrowBillDetail_Validation1;
                                continue;
                            }
                            tempRight.WarehouseAreaId = partsStock.WarehouseAreaId;
                            tempRight.CurrentStorage = partsStock.Quantity;
                        }
                    }

                    #endregion
                    //获取所有不合格数据
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    //获取所有合格数据
                    rightList = allList.Except(errorList).ToList();

                    #region 将合格数据的值填上
                    foreach(var rightItem in rightList) {
                        rightItem.SparePartCode = rightItem.SparePartCodeStr.ToUpper();
                        rightItem.WarehouseAreaCode = rightItem.WarehouseAreaCodeStr;

                    }
                    #endregion

                }
                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempObj = list[index - 1];
                            var values = new object[] {
                        #region 设置错误信息导出的列的值

                        tempObj.SparePartCodeStr,tempObj.SparePartNameStr,tempObj.WarehouseAreaCodeStr,tempObj.ErrorMsg
                                
                        #endregion                     
                    };
                            return values;
                        });
                    }
                    errorList = null;
                }

                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }

        /// <summary>
        /// 结果录入导出配件盘点单清单
        /// </summary>
        public bool ExportPartsInventoryDetailByBillId(int partsInventoryBillId, out string fileName) {
            fileName = GetExportFilePath("配件盘点单清单_" + Guid.NewGuid() + ".xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.Append(@"Select b.Sparepartcode,
                                        b.Sparepartname,
                                        b.Storagedifference,
                                        b.Warehouseareacode,
                                        b.Currentstorage,
                                        b.Storageafterinventory,
                                        b.Remark
                                    From Partsinventorydetail b
                                    Inner Join Partsinventorybill a
                                    On a.Id = b.Partsinventorybillid");
                    var dbParameters = new List<DbParameter>();
                    sql.Append(" where a.id = {0}partsInventoryBillId");
                    dbParameters.Add(db.CreateDbParameter("partsInventoryBillId", partsInventoryBillId));
                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode,ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName,ErrorStrings.Export_Title_Dealerpartsinventorybill_Storagedifference,ErrorStrings.Export_Title_WarehouseArea_WarehouseAreaCode,ErrorStrings.Export_Title_Dealerpartsinventorybill_Currentstorage,ErrorStrings.Export_Title_Dealerpartsinventorybill_Storageafterinventory,ErrorStrings.Export_Title_PartsBranch_Remark
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();

                }
                return true;
            } catch(Exception) {
                return false;
            }
        }

        /// <summary>
        /// 结果录入导入配件盘点单清单
        /// </summary>
        public bool ImportPartsInventoryDetailByBillId(int partsInventoryBillId, string fileName, out int excelImportNum, int warehouseId, int warehouseAreaCategory, int storageCompanyId, out List<PartsInventoryDetailExtend> rightData, out List<PartsInventoryDetailExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<PartsInventoryDetailExtend>();
            var rightList = new List<PartsInventoryDetailExtend>();
            var allList = new List<PartsInventoryDetailExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("PartsInventoryDetail", out notNullableFields, out fieldLenght);
                List<object> excelColumns;
                bool check;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    #region 指定文件中的列对应的名称 例如
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode, "SparePartCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName, "SparePartName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealerpartsinventorybill_Storagedifference, "StorageDifference");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_WarehouseArea_WarehouseAreaCode, "WarehouseAreaCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealerpartsinventorybill_Currentstorage, "CurrentStorage");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealerpartsinventorybill_Storageafterinventory, "StorageAfterInventory");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Remark, "Remark");
                    #endregion
                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();

                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var tempImportObj = new PartsInventoryDetailExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        #region 根据导入的字段给对应的实体属性赋值
                        tempImportObj.SparePartCodeStr = newRow["SparePartCode"];
                        tempImportObj.SparePartNameStr = newRow["SparePartName"];
                        tempImportObj.StorageDifferenceStr = newRow["StorageDifference"];
                        tempImportObj.WarehouseAreaCodeStr = newRow["WarehouseAreaCode"];
                        tempImportObj.CurrentStorageStr = newRow["CurrentStorage"];
                        tempImportObj.StorageAfterInventoryStr = newRow["StorageAfterInventory"];
                        tempImportObj.RemarkStr = newRow["Remark"];
                        #endregion

                        var tempErrorMessage = new List<string>();
                        #region 导入的数据基本检查
                        //配件编号检查
                        var fieldIndex = notNullableFields.IndexOf("SparePartCode".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.SparePartCodeStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchaseOrderDetail_Validation1);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.SparePartCodeStr) > fieldLenght["SparePartCode".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchaseOrderDetail_Validation2);
                        }
                        //配件名称检查
                        fieldIndex = notNullableFields.IndexOf("SparePartName".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.SparePartNameStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Sparepart_Name);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.SparePartNameStr) > fieldLenght["SparePartName".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Sparepart_NameIsLong);
                        }
                        //库位编号检查
                        fieldIndex = notNullableFields.IndexOf("WarehouseAreaCode".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.WarehouseAreaCodeStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_WarehouseArea_WarehouseAreaCodeIsNull);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.WarehouseAreaCodeStr) > fieldLenght["WarehouseAreaCode".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_WarehouseArea_WarehouseAreaCodeIsLong);
                        }
                        //盘点后库存检查
                        fieldIndex = notNullableFields.IndexOf("StorageAfterInventory".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.StorageAfterInventoryStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add("盘点后库存不能为空");
                        } else {
                            int checkValue;
                            if(int.TryParse(tempImportObj.StorageAfterInventoryStr, out checkValue)) {
                                if(checkValue < 0)
                                    tempErrorMessage.Add("盘点后库存不能小于0");
                                else
                                    tempImportObj.StorageAfterInventory = checkValue;
                            } else {
                                tempErrorMessage.Add("盘点后库存必须是数字");
                            }
                        }
                        //备注检查
                        fieldIndex = notNullableFields.IndexOf("Remark".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.RemarkStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_WarehouseArea_WarehouseAreaCodeIsNull);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.RemarkStr) > fieldLenght["Remark".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_WarehouseArea_WarehouseAreaCodeIsLong);
                        }
                        #endregion
                        if(tempErrorMessage.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });

                    #region 剩下的数据进行业务检查
                    //配件编号重复
                    var sparePartCodesGroup = allList.GroupBy(r => new {
                        r.SparePartCodeStr,
                        r.SparePartNameStr,
                        r.WarehouseAreaCodeStr,
                    }).ToList();
                    foreach(var group in sparePartCodesGroup) {
                        if(group.Count() > 1) {
                            foreach(var item in group) {
                                item.ErrorMsg = string.Join("; ", "数据重复");
                            }
                        }
                    }
                    //清单完整性检查
                    var dbPartsInventoryDetailExtends = new List<PartsInventoryDetailExtend>();
                    Func<string[], bool> getDbPartsInventoryDetailExtends = value => {
                        var dbObj = new PartsInventoryDetailExtend {
                            SparePartCode = value[0],
                            SparePartName = value[1],
                            WarehouseAreaCode = value[2],
                            CurrentStorage = Convert.ToInt32(value[3]),
                            SparePartId = Convert.ToInt32(value[4]),
                            WarehouseAreaId = Convert.ToInt32(value[5])
                        };
                        dbPartsInventoryDetailExtends.Add(dbObj);
                        return false;
                    };
                    var sql = string.Format(@"Select b.sparepartcode,
                                b.sparepartname,
                                b.warehouseareacode,
                                b.CurrentStorage,
                                b.SparePartId,
                                b.WarehouseAreaId
                                From PartsInventoryBill a
                                Inner Join PartsInventoryDetail b
                                On b.partsinventorybillid=a.id where a.id={0}", partsInventoryBillId);
                    db.QueryDataWithInOperator(sql, "1", false, new[] { "1" }, getDbPartsInventoryDetailExtends);
                    if(dbPartsInventoryDetailExtends.Count() > allList.Count()) {
                        throw new Exception("该盘点单缺失部分清单，请检查后再导入");
                    }
                    foreach(var temp in allList) {
                        var partsInventoryDetail = dbPartsInventoryDetailExtends.FirstOrDefault(v => v.SparePartCode == temp.SparePartCodeStr&& v.WarehouseAreaCode == temp.WarehouseAreaCodeStr);
                        if(partsInventoryDetail == null) {
                            temp.ErrorMsg = string.Join("; ", string.Format("该条数据无效,请检验后再导入"));
                            continue;
                        }
                        //if(partsInventoryDetail.CurrentStorage < temp.StorageAfterInventory) {
                        //    temp.ErrorMsg = string.Join("; ", string.Format("盘点后库存不能大于当前库存"));
                        //    continue;
                        //}
                        temp.SparePartId = partsInventoryDetail.SparePartId;
                        temp.WarehouseAreaId = partsInventoryDetail.WarehouseAreaId;
                        temp.CurrentStorage = partsInventoryDetail.CurrentStorage;
                        temp.StorageDifference = temp.StorageAfterInventory - partsInventoryDetail.CurrentStorage;
                    }
                    //var exist = false;
                    //foreach(var dbPartsInventoryDetailExtend in dbPartsInventoryDetailExtends) {
                    //    exist = allList.All(r => dbPartsInventoryDetailExtend.SparePartCode == r.SparePartCodeStr && dbPartsInventoryDetailExtend.SparePartName == r.SparePartNameStr && dbPartsInventoryDetailExtend.WarehouseAreaCode == r.WarehouseAreaCodeStr);
                    //}

                    #endregion
                    //获取所有不合格数据
                    check = allList.All(r => r.ErrorMsg == null);
                    //获取所有合格数据
                    if(check) {
                        rightList = allList.ToList();
                        foreach(var rightItem in allList) {
                            rightItem.SparePartCode = rightItem.SparePartCodeStr;
                            rightItem.SparePartName = rightItem.SparePartNameStr;
                            rightItem.WarehouseAreaCode = rightItem.WarehouseAreaCodeStr;
                            rightItem.Remark = rightItem.RemarkStr;
                        }
                    } else {
                        errorList = allList.ToList();
                        //if(!exist) {
                        //    var newList = new PartsInventoryDetailExtend() {
                        //        SparePartCode = "缺失",
                        //        SparePartName = "缺失",
                        //        WarehouseAreaCode = "缺失",
                        //        ErrorMsg = "该盘点单缺失部分清单，请检查后再导入"
                        //    };
                        //    errorList.Add(newList);
                        //}
                    }

                }
                //导出所有不合格数据
                if(!check) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempObj = list[index - 1];
                            var values = new object[] {
                        #region 设置错误信息导出的列的值

                        tempObj.SparePartCodeStr,tempObj.SparePartNameStr,tempObj.StorageDifferenceStr,
                        tempObj.WarehouseAreaCodeStr,tempObj.CurrentStorageStr,tempObj.StorageAfterInventoryStr,
                        tempObj.RemarkStr,tempObj.ErrorMsg
                                
                        #endregion                     
                    };
                            return values;
                        });
                    }
                    errorList = null;
                }

                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }
    }
}
