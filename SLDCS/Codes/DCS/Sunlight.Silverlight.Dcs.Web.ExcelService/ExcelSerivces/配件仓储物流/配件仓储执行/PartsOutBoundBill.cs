﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Globalization;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        /// <summary>
        /// 合并导出配件出库主清单
        /// </summary>
        //public bool ExportPartsOutboundBillWithDetail(int[] ids, int? userId, int? partsSalesCategoryId, int storageCompanyId, string partsOutBoundBillCode, int? warehouseId, string counterpartcompanycode, string counterpartcompanyname, int? outBoundType, int? settlementStatus, string partsSalesOrderTypeName, string outboundPackPlanCode, string contractCode, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName) {
        public bool ExportPartsOutboundBillWithDetail(int[] ids, string eRPSourceOrderCode, int? userId, int? partsSalesCategoryId, int storageCompanyId, string partsOutBoundBillCode, int? warehouseId, string counterpartcompanycode, string counterpartcompanyname, int? outBoundType, int? settlementStatus, string partsSalesOrderTypeName, string partsOutboundPlanCode, string outboundPackPlanCode, string contractCode, string provinceName, DateTime? createTimeBegin, DateTime? createTimeEnd, string SAPPurchasePlanCode, string zpNumber, string cpPartsPurchaseOrderCode, string cpPartsInboundCheckCode,string sparePartCode,string sparePartName, out string fileName) {
            fileName = GetExportFilePath("配件出库单主清单.xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);
                var userInfo = Utils.GetCurrentUserInfo();
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.Append(string.Format(@"select a.code,
                                        a.warehousecode,
                                        a.warehousename,
                                        (select value from keyvalueitem where NAME = 'Parts_OutboundType'and key=a.outboundtype) As outboundtype,
                                        (Select Sum(t.SettlementPrice*t.OutboundAmount) From Partsoutboundbilldetail t Where t.partsoutboundbillid=a.id),
                                        a.originalrequirementbillcode,
                                        c.code as partsoutboundplancode,
                                        d.name as partssalescategoryname,
                                        a.branchname,
                                        a.counterpartcompanycode,
                                        a.counterpartcompanyname,
                                        (select value from keyvalueitem where NAME = 'Company_Type'and key=comp.type) As counterpartcompanyType, --对方单位类型
                                        a.ReceivingWarehouseCode,
                                        a.ReceivingWarehouseName,
                                        decode(d.Name,'随车行',ps.Province,comp.ProvinceName) as provinceName,    --省份
                                        (select value from keyvalueitem where NAME = 'Parts_SettlementStatus'and key=a.settlementstatus) As settlementstatus,
                                        a.remark,
                                        a.creatorname,
                                        a.createtime,
                                        a.modifiername,
                                        a.modifytime, 
                                        (select value from keyvalueitem where NAME = 'PartsShipping_Method'and key=ps.ShippingMethod) As ShippingMethod,
                                        b.sparepartcode,
                                        b.sparepartname,
                                        b.warehouseareacode,
                                        b.outboundamount,
                                        b.settlementprice,
                                        s.MeasureUnit,
                                        s.OverseasPartsFigure,
                                        b.remark as remarkdetails,
                                        c.ZPNUMBER
                                    from partsoutboundbill a
                                    LEFT OUTER join partsoutboundbilldetail b
                                    on a.id = b.partsoutboundbillid
                                    LEFT OUTER join partsoutboundplan c
                                    on a.partsoutboundplanid=c.id
                                    left outer join PartsSalesOrder ps
                                    on c.OriginalRequirementBillType=1 and c.OriginalRequirementBillId=ps.id
                                    LEFT OUTER join partssalescategory d on
                                    a.partssalescategoryid=d.id
                                    left outer join company comp on a.receivingcompanyid = comp.id 
                                    left outer join sparepart s on b.sparepartid = s.id 
                                    where a.StorageCompanyId={0} ", userInfo.EnterpriseId));
                    var dbParameters = new List<DbParameter>();
                    if(ids != null && ids.Length > 0) {
                        sql.Append(" and a.id in (");
                        for(var i = 0; i < ids.Length; i++) {
                            if(ids.Length == i + 1) {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            } else {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                        }
                        sql.Append(")");
                    } else {
                        if(userId.HasValue) {
                            sql.Append(@"and  ( EXISTS (SELECT 
                                                  1 AS C1
                                                  FROM WarehouseOperator Extent4
                                                  WHERE Extent4.OperatorId = {0}userId AND Extent4.WarehouseId = a.WarehouseId)
                                                  or Exists(Select 1 From warehouse t Where t.id=a.warehouseid And Type=99)) ");
                            dbParameters.Add(db.CreateDbParameter("userId", userInfo.Id));
                        }
                        if(partsSalesCategoryId.HasValue) {
                            sql.Append(@" and a.partsSalesCategoryId = {0}partsSalesCategoryId ");
                            dbParameters.Add(db.CreateDbParameter("partsSalesCategoryId", partsSalesCategoryId.Value));
                        }
                        if(!string.IsNullOrEmpty(partsOutBoundBillCode)) {
                            sql.Append("and LOWER(a.Code) like {0}partsOutBoundBillCode ");
                            dbParameters.Add(db.CreateDbParameter("partsOutBoundBillCode", "%" + partsOutBoundBillCode.ToLower() + "%"));
                        }

                        if(warehouseId.HasValue) {
                            sql.Append("and a.warehouseId = {0}warehouseId ");
                            dbParameters.Add(db.CreateDbParameter("warehouseId", warehouseId.Value));
                        }
                        if(!string.IsNullOrEmpty(counterpartcompanycode)) {
                            sql.Append("and LOWER(a.counterpartcompanycode) like {0}counterpartcompanycode ");
                            dbParameters.Add(db.CreateDbParameter("counterpartcompanycode", "%" + counterpartcompanycode.ToLower() + "%"));
                        }
                        if(!string.IsNullOrEmpty(eRPSourceOrderCode)) {
                            sql.Append("and LOWER(a.eRPSourceOrderCode) like {0}eRPSourceOrderCode ");
                            dbParameters.Add(db.CreateDbParameter("eRPSourceOrderCode", "%" + eRPSourceOrderCode.ToLower() + "%"));
                        }
                        if(!string.IsNullOrEmpty(counterpartcompanyname)) {
                            sql.Append("and LOWER(a.counterpartcompanyname) like {0}counterpartcompanyname ");
                            dbParameters.Add(db.CreateDbParameter("counterpartcompanyname", "%" + counterpartcompanyname.ToLower() + "%"));
                        }

                        if(outBoundType.HasValue) {
                            sql.Append("and a.outBoundType ={0}outBoundType ");
                            dbParameters.Add(db.CreateDbParameter("outBoundType", outBoundType.Value));
                        }
                        if(!string.IsNullOrEmpty(partsSalesOrderTypeName)) {
                            sql.Append("and LOWER(a.partsSalesOrderTypeName) like {0}partsSalesOrderTypeName ");
                            dbParameters.Add(db.CreateDbParameter("partsSalesOrderTypeName", "%" + partsSalesOrderTypeName.ToLower() + "%"));
                        }
                        if(!string.IsNullOrEmpty(outboundPackPlanCode)) {
                            sql.Append("and LOWER(a.outboundPackPlanCode) like {0}outboundPackPlanCode ");
                            dbParameters.Add(db.CreateDbParameter("outboundPackPlanCode", "%" + outboundPackPlanCode.ToLower() + "%"));
                        }
                        if(!string.IsNullOrEmpty(SAPPurchasePlanCode)) {
                            sql.Append("and LOWER(a.SAPPurchasePlanCode) like {0}SAPPurchasePlanCode ");
                            dbParameters.Add(db.CreateDbParameter("SAPPurchasePlanCode", "%" + SAPPurchasePlanCode.ToLower() + "%"));
                        }
                        //zpNumber改为originalRequirementBillCode
                        if(!string.IsNullOrEmpty(zpNumber)) {
                            sql.Append("and LOWER(a.OriginalRequirementBillCode) like {0}OriginalRequirementBillCode ");
                            dbParameters.Add(db.CreateDbParameter("OriginalRequirementBillCode", "%" + zpNumber.ToLower() + "%"));
                        }

                        if(!string.IsNullOrEmpty(partsOutboundPlanCode)) {
                            sql.Append("and LOWER(c.code) like {0}partsoutboundplancode ");
                            dbParameters.Add(db.CreateDbParameter("partsoutboundplancode", "%" + partsOutboundPlanCode.ToLower() + "%"));
                        }
                        if(!string.IsNullOrEmpty(provinceName)) {
                            sql.Append("and LOWER(comp.ProvinceName) like {0}provinceName ");
                            dbParameters.Add(db.CreateDbParameter("provinceName", "%" + provinceName.ToLower() + "%"));
                        }

                        if(!string.IsNullOrEmpty(contractCode)) {
                            sql.Append("and LOWER(a.contractCode) like {0}contractCode ");
                            dbParameters.Add(db.CreateDbParameter("contractCode", "%" + contractCode.ToLower() + "%"));
                        }
                        if(settlementStatus.HasValue) {
                            sql.Append("and a.settlementStatus ={0}settlementStatus ");
                            dbParameters.Add(db.CreateDbParameter("settlementStatus", settlementStatus.Value));
                        }

                        if(createTimeBegin.HasValue) {
                            sql.Append(@" and a.createTime >=To_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }


                        if(createTimeEnd.HasValue) {
                            sql.Append(@" and a.createTime <=To_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                        if (!string.IsNullOrEmpty(cpPartsPurchaseOrderCode))
                        {
                            sql.Append(" and LOWER(a.CPPartsPurchaseOrderCode) like {0}cpPartsPurchaseOrderCode ");
                            dbParameters.Add(db.CreateDbParameter("cpPartsPurchaseOrderCode", "%" + cpPartsPurchaseOrderCode.ToLower() + "%"));
                        }
                        if (!string.IsNullOrEmpty(cpPartsInboundCheckCode))
                        {
                            sql.Append(" and LOWER(a.CPPartsInboundCheckCode) like {0}cpPartsInboundCheckCode ");
                            dbParameters.Add(db.CreateDbParameter("cpPartsInboundCheckCode", "%" + cpPartsInboundCheckCode.ToLower() + "%"));
                        }
                        if (!string.IsNullOrEmpty(sparePartCode))
                        {
                            sql.Append("and LOWER(b.sparePartCode) like {0}sparePartCode ");
                            dbParameters.Add(db.CreateDbParameter("sparePartCode", "%" + sparePartCode.ToLower() + "%"));
                        }
                        if (!string.IsNullOrEmpty(sparePartName))
                        {
                            sql.Append("and LOWER(b.sparePartName) like {0}sparePartName ");
                            dbParameters.Add(db.CreateDbParameter("sparePartName", "%" + sparePartName.ToLower() + "%"));
                        }
                    }
                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    ErrorStrings.Export_Title_PartsOutboundBill_Code, ErrorStrings.Export_Title_Company_WarehouseCode, ErrorStrings.Export_Title_Company_WarehouseName, ErrorStrings.Export_Title_PartsOutboundBill_OutBoundType,ErrorStrings.Export_Title_InternalAllocationBill_TotalAmount, ErrorStrings.Export_Title_PartsInboundCheckBill_OriginalRequirementBillCode,ErrorStrings.Export_Title_PartsOutboundBill_PartsOutboundPlanCode,ErrorStrings.Export_Title_Partssalescategory_Name, ErrorStrings.Export_Title_PartsBranch_BranchName, ErrorStrings.Export_Title_BoxUpTask_CounterpartCompanyCode, ErrorStrings.Export_Title_BoxUpTask_CounterpartCompanyName,ErrorStrings.Export_Title_PartsOutboundBill_CounterpartCompanyType, ErrorStrings.Export_Title_PartsOutboundBill_ReceivingWarehouseCode, ErrorStrings.Export_Title_SupplierShippingOrder_ReveiveWarehouseName,ErrorStrings.Export_Validation_Company_ProvinceName, ErrorStrings.Export_Title_PartsInboundCheckBill_SettlementStatus, ErrorStrings.Export_Title_PartsBranch_Remark, ErrorStrings.Export_Title_AccountPeriod_CreatorName, ErrorStrings.Export_Title_AccountPeriod_CreateTime, ErrorStrings.Export_Title_AccountPeriod_ModifierName, ErrorStrings.Export_Title_AccountPeriod_ModifyTime,ErrorStrings.Export_Title_PartsPurchaseOrder_ShippingMethod,
                                    ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode,ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName,ErrorStrings.Export_Title_WarehouseArea_WarehouseAreaCode,ErrorStrings.Export_Title_PartsOutboundBill_OutboundAmount,ErrorStrings.Export_Title_PartsPurchaseRtnSettleBill_SettlementPrice,ErrorStrings.Export_Title_PackingPropertyApp_MeasureUnit,ErrorStrings.Export_Title_PartsOutboundBill_OverseasPartsFigure,ErrorStrings.Export_Title_PartsPurchaseOrder_DetailRemark
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }
        /// <summary>
        /// 合并导出配件出库主清单
        /// </summary>
        //public bool ExportPartsOutboundBillWithDetail(int[] ids, int? userId, int? partsSalesCategoryId, int storageCompanyId, string partsOutBoundBillCode, int? warehouseId, string counterpartcompanycode, string counterpartcompanyname, int? outBoundType, int? settlementStatus, string partsSalesOrderTypeName, string outboundPackPlanCode, string contractCode, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName) {
        public bool ExportPartsOutboundBillWithDetailForWarehouse(int[] ids, string eRPSourceOrderCode, int? userId, int? partsSalesCategoryId, int storageCompanyId, string partsOutBoundBillCode, int? warehouseId, string counterpartcompanycode, string counterpartcompanyname, int? outBoundType, int? settlementStatus, string partsSalesOrderTypeName, string partsOutboundPlanCode, string outboundPackPlanCode, string contractCode, string provinceName, DateTime? createTimeBegin, DateTime? createTimeEnd, string SAPPurchasePlanCode, string zpNumber, string cpPartsPurchaseOrderCode, string cpPartsInboundCheckCode, string sparePartCode, string sparePartName, out string fileName)
        {
            fileName = GetExportFilePath("配件出库单主清单.xlsx");
            try
            {
                if (!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);
                var userInfo = Utils.GetCurrentUserInfo();
                using (var conn = db.CreateDbConnection())
                {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.Append(string.Format(@"select a.code,
                                        a.warehousecode,
                                        a.warehousename,
                                        (select value from keyvalueitem where NAME = 'Parts_OutboundType'and key=a.outboundtype) As outboundtype,
                                         (select sum(s.plannedamount * nvl(d.retailguideprice,0)) from partsoutboundplandetail s  left join PartsRetailGuidePrice d  on s.sparepartid = d.sparepartid and d.status=1
                                         where s.partsoutboundplanid = a.id group by s.partsoutboundplanid)  as TotalAmountForWarehouse,
                                        a.originalrequirementbillcode,
                                        c.code as partsoutboundplancode,
                                        d.name as partssalescategoryname,
                                        a.branchname,
                                        a.counterpartcompanycode,
                                        a.counterpartcompanyname,
                                        (select value from keyvalueitem where NAME = 'Company_Type'and key=comp.type) As counterpartcompanyType, --对方单位类型
                                        a.ReceivingWarehouseCode,
                                        a.ReceivingWarehouseName,
                                        decode(d.Name,'随车行',ps.Province,comp.ProvinceName) as provinceName,    --省份
                                        (select value from keyvalueitem where NAME = 'Parts_SettlementStatus'and key=a.settlementstatus) As settlementstatus,
                                        a.remark,
                                        a.creatorname,
                                        a.createtime,
                                        a.modifiername,
                                        a.modifytime, 
                                        (select value from keyvalueitem where NAME = 'PartsShipping_Method'and key=ps.ShippingMethod) As ShippingMethod,
                                        b.sparepartcode,
                                        b.sparepartname,
                                        b.warehouseareacode,
                                        b.outboundamount,
                                        p.RetailGuidePrice,
                                        s.MeasureUnit,
                                        s.OverseasPartsFigure,
                                        b.remark as remarkdetails,
                                        c.ZPNUMBER
                                    from partsoutboundbill a
                                    LEFT OUTER join partsoutboundbilldetail b
                                    on a.id = b.partsoutboundbillid
                                    LEFT OUTER join partsoutboundplan c
                                    on a.partsoutboundplanid=c.id
                                    left outer join PartsSalesOrder ps
                                    on c.OriginalRequirementBillType=1 and c.OriginalRequirementBillId=ps.id
                                    LEFT OUTER join partssalescategory d on
                                    a.partssalescategoryid=d.id
                                    left outer join company comp on a.receivingcompanyid = comp.id 
                                    left outer join sparepart s on b.sparepartid = s.id 
                                    join PartsRetailGuidePrice p on a.BranchId=p.BranchId and a.PartsSalesCategoryId=p.PartsSalesCategoryId and b.SparePartId=p.SparePartId and p.Status=1
                                    where a.StorageCompanyId={0} ", userInfo.EnterpriseId));
                    var dbParameters = new List<DbParameter>();
                    if (ids != null && ids.Length > 0)
                    {
                        sql.Append(" and a.id in (");
                        for (var i = 0; i < ids.Length; i++)
                        {
                            if (ids.Length == i + 1)
                            {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                            else
                            {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                        }
                        sql.Append(")");
                    }
                    else
                    {
                        if (userId.HasValue)
                        {
                            sql.Append(@"and  ( EXISTS (SELECT 
                                                  1 AS C1
                                                  FROM WarehouseOperator Extent4
                                                  WHERE Extent4.OperatorId = {0}userId AND Extent4.WarehouseId = a.WarehouseId)
                                                  or Exists(Select 1 From warehouse t Where t.id=a.warehouseid And Type=99)) ");
                            dbParameters.Add(db.CreateDbParameter("userId", userInfo.Id));
                        }
                        if (partsSalesCategoryId.HasValue)
                        {
                            sql.Append(@" and a.partsSalesCategoryId = {0}partsSalesCategoryId ");
                            dbParameters.Add(db.CreateDbParameter("partsSalesCategoryId", partsSalesCategoryId.Value));
                        }
                        if (!string.IsNullOrEmpty(partsOutBoundBillCode))
                        {
                            sql.Append("and LOWER(a.Code) like {0}partsOutBoundBillCode ");
                            dbParameters.Add(db.CreateDbParameter("partsOutBoundBillCode", "%" + partsOutBoundBillCode.ToLower() + "%"));
                        }

                        if (warehouseId.HasValue)
                        {
                            sql.Append("and a.warehouseId = {0}warehouseId ");
                            dbParameters.Add(db.CreateDbParameter("warehouseId", warehouseId.Value));
                        }
                        if (!string.IsNullOrEmpty(counterpartcompanycode))
                        {
                            sql.Append("and LOWER(a.counterpartcompanycode) like {0}counterpartcompanycode ");
                            dbParameters.Add(db.CreateDbParameter("counterpartcompanycode", "%" + counterpartcompanycode.ToLower() + "%"));
                        }
                        if (!string.IsNullOrEmpty(eRPSourceOrderCode))
                        {
                            sql.Append("and LOWER(a.eRPSourceOrderCode) like {0}eRPSourceOrderCode ");
                            dbParameters.Add(db.CreateDbParameter("eRPSourceOrderCode", "%" + eRPSourceOrderCode.ToLower() + "%"));
                        }
                        if (!string.IsNullOrEmpty(counterpartcompanyname))
                        {
                            sql.Append("and LOWER(a.counterpartcompanyname) like {0}counterpartcompanyname ");
                            dbParameters.Add(db.CreateDbParameter("counterpartcompanyname", "%" + counterpartcompanyname.ToLower() + "%"));
                        }

                        if (outBoundType.HasValue)
                        {
                            sql.Append("and a.outBoundType ={0}outBoundType ");
                            dbParameters.Add(db.CreateDbParameter("outBoundType", outBoundType.Value));
                        }
                        if (!string.IsNullOrEmpty(partsSalesOrderTypeName))
                        {
                            sql.Append("and LOWER(a.partsSalesOrderTypeName) like {0}partsSalesOrderTypeName ");
                            dbParameters.Add(db.CreateDbParameter("partsSalesOrderTypeName", "%" + partsSalesOrderTypeName.ToLower() + "%"));
                        }
                        if (!string.IsNullOrEmpty(outboundPackPlanCode))
                        {
                            sql.Append("and LOWER(a.outboundPackPlanCode) like {0}outboundPackPlanCode ");
                            dbParameters.Add(db.CreateDbParameter("outboundPackPlanCode", "%" + outboundPackPlanCode.ToLower() + "%"));
                        }
                        if (!string.IsNullOrEmpty(SAPPurchasePlanCode))
                        {
                            sql.Append("and LOWER(a.SAPPurchasePlanCode) like {0}SAPPurchasePlanCode ");
                            dbParameters.Add(db.CreateDbParameter("SAPPurchasePlanCode", "%" + SAPPurchasePlanCode.ToLower() + "%"));
                        }
                        //zpNumber改为originalRequirementBillCode
                        if (!string.IsNullOrEmpty(zpNumber))
                        {
                            sql.Append("and LOWER(a.OriginalRequirementBillCode) like {0}OriginalRequirementBillCode ");
                            dbParameters.Add(db.CreateDbParameter("OriginalRequirementBillCode", "%" + zpNumber.ToLower() + "%"));
                        }

                        if (!string.IsNullOrEmpty(partsOutboundPlanCode))
                        {
                            sql.Append("and LOWER(c.code) like {0}partsoutboundplancode ");
                            dbParameters.Add(db.CreateDbParameter("partsoutboundplancode", "%" + partsOutboundPlanCode.ToLower() + "%"));
                        }
                        if (!string.IsNullOrEmpty(provinceName))
                        {
                            sql.Append("and LOWER(comp.ProvinceName) like {0}provinceName ");
                            dbParameters.Add(db.CreateDbParameter("provinceName", "%" + provinceName.ToLower() + "%"));
                        }

                        if (!string.IsNullOrEmpty(contractCode))
                        {
                            sql.Append("and LOWER(a.contractCode) like {0}contractCode ");
                            dbParameters.Add(db.CreateDbParameter("contractCode", "%" + contractCode.ToLower() + "%"));
                        }
                        if (settlementStatus.HasValue)
                        {
                            sql.Append("and a.settlementStatus ={0}settlementStatus ");
                            dbParameters.Add(db.CreateDbParameter("settlementStatus", settlementStatus.Value));
                        }

                        if (createTimeBegin.HasValue)
                        {
                            sql.Append(@" and a.createTime >=To_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }


                        if (createTimeEnd.HasValue)
                        {
                            sql.Append(@" and a.createTime <=To_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                        if (!string.IsNullOrEmpty(cpPartsPurchaseOrderCode))
                        {
                            sql.Append(" and LOWER(a.CPPartsPurchaseOrderCode) like {0}cpPartsPurchaseOrderCode ");
                            dbParameters.Add(db.CreateDbParameter("cpPartsPurchaseOrderCode", "%" + cpPartsPurchaseOrderCode.ToLower() + "%"));
                        }
                        if (!string.IsNullOrEmpty(cpPartsInboundCheckCode))
                        {
                            sql.Append(" and LOWER(a.CPPartsInboundCheckCode) like {0}cpPartsInboundCheckCode ");
                            dbParameters.Add(db.CreateDbParameter("cpPartsInboundCheckCode", "%" + cpPartsInboundCheckCode.ToLower() + "%"));
                        }
                        if (!string.IsNullOrEmpty(sparePartCode))
                        {
                            sql.Append("and LOWER(b.sparePartCode) like {0}sparePartCode ");
                            dbParameters.Add(db.CreateDbParameter("sparePartCode", "%" + sparePartCode.ToLower() + "%"));
                        }
                        if (!string.IsNullOrEmpty(sparePartName))
                        {
                            sql.Append("and LOWER(b.sparePartName) like {0}sparePartName ");
                            dbParameters.Add(db.CreateDbParameter("sparePartName", "%" + sparePartName.ToLower() + "%"));
                        }
                    }
                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using (var excelExport = new ExcelExport(fileName))
                    {
                        excelExport.ExportByRow(index =>
                        {
                            if (index == 0)
                            {
                                return new object[] {
                                    ErrorStrings.Export_Title_PartsOutboundBill_Code, ErrorStrings.Export_Title_Company_WarehouseCode, ErrorStrings.Export_Title_Company_WarehouseName, ErrorStrings.Export_Title_PartsOutboundBill_OutBoundType,ErrorStrings.Export_Title_InternalAllocationBill_TotalAmount, ErrorStrings.Export_Title_PartsInboundCheckBill_OriginalRequirementBillCode,ErrorStrings.Export_Title_PartsOutboundBill_PartsOutboundPlanCode,ErrorStrings.Export_Title_Partssalescategory_Name, ErrorStrings.Export_Title_PartsBranch_BranchName, ErrorStrings.Export_Title_BoxUpTask_CounterpartCompanyCode, ErrorStrings.Export_Title_BoxUpTask_CounterpartCompanyName,ErrorStrings.Export_Title_PartsOutboundBill_CounterpartCompanyType, ErrorStrings.Export_Title_PartsOutboundBill_ReceivingWarehouseCode, ErrorStrings.Export_Title_SupplierShippingOrder_ReveiveWarehouseName,ErrorStrings.Export_Validation_Company_ProvinceName, ErrorStrings.Export_Title_PartsInboundCheckBill_SettlementStatus, ErrorStrings.Export_Title_PartsBranch_Remark, ErrorStrings.Export_Title_AccountPeriod_CreatorName, ErrorStrings.Export_Title_AccountPeriod_CreateTime, ErrorStrings.Export_Title_AccountPeriod_ModifierName, ErrorStrings.Export_Title_AccountPeriod_ModifyTime,ErrorStrings.Export_Title_PartsPurchaseOrder_ShippingMethod,
                                    ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode,ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName,ErrorStrings.Export_Title_WarehouseArea_WarehouseAreaCode,ErrorStrings.Export_Title_PartsOutboundBill_OutboundAmount,ErrorStrings.Export_Title_PartsPurchaseRtnSettleBill_SettlementPrice,ErrorStrings.Export_Title_PackingPropertyApp_MeasureUnit,ErrorStrings.Export_Title_PartsOutboundBill_OverseasPartsFigure,ErrorStrings.Export_Title_PartsPurchaseOrder_DetailRemark
                                };
                            }
                            if (reader.Read())
                            {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if (num != reader.FieldCount)
                                {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        /// <summary>
        /// 导出配件出库单清单
        /// </summary>
        public bool ExportPartsOutboundBillDetail(int partsOutboundBillId, out string fileName) {
            fileName = GetExportFilePath("配件出库单清单_" + Guid.NewGuid() + ".xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.Append(@"select b.sparepartcode,
                                        b.sparepartname,
                                        b.warehouseareacode,
                                        b.outboundamount,
                                        b.settlementprice,
                                        b.remark
                                    from partsoutboundbill a
                                    inner join partsoutboundbilldetail b
                                    on a.id = b.partsoutboundbillid");
                    var dbParameters = new List<DbParameter>();
                    sql.Append(" where a.id = {0}partsOutboundBillId");
                    dbParameters.Add(db.CreateDbParameter("partsOutboundBillId", partsOutboundBillId));
                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode,ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName,ErrorStrings.Export_Title_WarehouseArea_WarehouseAreaCode,ErrorStrings.Export_Title_PartsOutboundBill_OutboundAmount,ErrorStrings.Export_Title_PartsPurchaseRtnSettleBill_SettlementPrice,ErrorStrings.Export_Title_PartsBranch_Remark
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();

                }
                return true;
            } catch(Exception) {
                return false;
            }
        }

        /// <summary>
        /// 导出配件出库主单
        /// </summary>
        //public bool ExportPartsOutboundBill(int[] ids, int? userId, int? partsSalesCategoryId, int storageCompanyId, string partsOutBoundBillCode, int? warehouseId, string counterpartcompanycode, string counterpartcompanyname, int? outBoundType, int? settlementStatus, string partsSalesOrderTypeName, string outboundPackPlanCode, string contractCode, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName) {
        public bool ExportPartsOutboundBill(int[] ids, string eRPSourceOrderCode, int? userId, int? partsSalesCategoryId, int storageCompanyId, string partsOutBoundBillCode, int? warehouseId, string counterpartcompanycode, string counterpartcompanyname, int? outBoundType, int? settlementStatus, string partsSalesOrderTypeName, string partsOutBoundPlanCode, string outboundPackPlanCode, string contractCode, string provinceName, DateTime? createTimeBegin, DateTime? createTimeEnd, string SAPPurchasePlanCode, string zpNumber, string cpPartsPurchaseOrderCode, string cpPartsInboundCheckCode, out string fileName) {
            fileName = GetExportFilePath("配件出库单主单.xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);
                var userInfo = Utils.GetCurrentUserInfo();
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.Append(string.Format(@"select a.code,
                                        a.warehousecode,
                                        a.warehousename,
                                        (select value from keyvalueitem where NAME = 'Parts_OutboundType'and key=a.outboundtype) As outboundtype,
                                        (Select Sum(t.SettlementPrice*t.OutboundAmount) From Partsoutboundbilldetail t Where t.partsoutboundbillid=a.id),
                                        a.EcommerceMoney,
                                        a.originalrequirementbillcode,
                                        a.partssalesordertypename,
                                        c.code as partsoutboundplancode,
                                        d.name as partssalescategoryname,
                                        a.branchname,
                                        a.counterpartcompanycode,
                                        a.counterpartcompanyname,
                                        (select value from keyvalueitem where NAME = 'Company_Type'and key=comp.type) As counterpartcompanyType, --对方单位类型
                                        a.ReceivingWarehouseCode,
                                        a.ReceivingWarehouseName,
                                        decode(d.Name,'随车行',pso.Province,comp.ProvinceName) as provinceName,    --省份
                                        a.OutboundPackPlanCode,
                                        (select ContainerNumber from PartsOutboundBillDetail pd where pd.PartsOutboundBillId=a.id and rownum=1) as ContainerNumber,
                                        a.ContractCode,
                                        a.GPMSPurOrderCode,
                                        (select value from keyvalueitem where NAME = 'Parts_SettlementStatus'and key=a.settlementstatus) As settlementstatus,
                                        (select value from keyvalueitem where NAME = 'ERPInvoiceInformation_Type'and key=pso.InvoiceType) As InvoiceType,
                                        a.ERPSourceOrderCode,
                                        a.remark,
                                        a.orderapprovecomment,
                                        a.creatorname,
                                        a.createtime,
                                        a.modifiername,
                                        a.modifytime,
                                        a.SAPPurchasePlanCode,
                                        c.ZPNUMBER,
                                        a.PWMSOutboundCode,
                                        a.CPPartsPurchaseOrderCode,
                                        a.CPPartsInboundCheckCode,
                                        decode(a.PartsPurchaseSettleCode,null,'否','是') as IsSupplySettlement
                                    from partsoutboundbill a
                                    INNER JOIN (SELECT Extent2.PartsOutboundBillId AS K1, Sum(Extent2.OutboundAmount * Extent2.SettlementPrice) AS A1
                                    FROM PartsOutboundBillDetail Extent2
                                    GROUP BY Extent2.PartsOutboundBillId ) GroupBy1 ON a.Id = GroupBy1.K1
                                    inner join partsoutboundplan c
                                    on a.partsoutboundplanid=c.id
                                    inner join partssalescategory d on
                                    a.partssalescategoryid=d.id
                                    left outer join partssalesorder pso on a.OriginalRequirementBillId = pso.id and a.Originalrequirementbilltype=1
                                    left outer join company comp on a.receivingcompanyid = comp.id
                                    where a.StorageCompanyId={0} ", userInfo.EnterpriseId));
                    var dbParameters = new List<DbParameter>();
                    if(ids != null && ids.Length > 0) {
                        sql.Append(" and a.id in (");
                        for(var i = 0; i < ids.Length; i++) {
                            if(ids.Length == i + 1) {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            } else {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                        }
                        sql.Append(")");
                    } else {
                        if(userId.HasValue) {
                            sql.Append(@"and  ( EXISTS (SELECT 
                                                  1 AS C1
                                                  FROM WarehouseOperator Extent4
                                                  WHERE Extent4.OperatorId = {0}userId AND Extent4.WarehouseId = a.WarehouseId)
                                            or Exists(Select 1 From warehouse t Where t.id=a.warehouseid And Type=99)) ");
                            dbParameters.Add(db.CreateDbParameter("userId", userInfo.Id));
                        }
                        if(partsSalesCategoryId.HasValue) {
                            sql.Append(@" and a.partsSalesCategoryId = {0}partsSalesCategoryId ");
                            dbParameters.Add(db.CreateDbParameter("partsSalesCategoryId", partsSalesCategoryId.Value));
                        }
                        if(!string.IsNullOrEmpty(partsOutBoundBillCode)) {
                            sql.Append("and LOWER(a.Code) like {0}partsOutBoundBillCode ");
                            dbParameters.Add(db.CreateDbParameter("partsOutBoundBillCode", "%" + partsOutBoundBillCode.ToLower() + "%"));
                        }
                        if(!string.IsNullOrEmpty(eRPSourceOrderCode)) {
                            sql.Append("and LOWER(pso.eRPSourceOrderCode) like {0}eRPSourceOrderCode ");
                            dbParameters.Add(db.CreateDbParameter("eRPSourceOrderCode", "%" + eRPSourceOrderCode.ToLower() + "%"));
                        }


                        if(warehouseId.HasValue) {
                            sql.Append("and a.warehouseId = {0}warehouseId ");
                            dbParameters.Add(db.CreateDbParameter("warehouseId", warehouseId.Value));
                        }
                        if(!string.IsNullOrEmpty(counterpartcompanycode)) {
                            sql.Append("and LOWER(a.counterpartcompanycode) like {0}counterpartcompanycode ");
                            dbParameters.Add(db.CreateDbParameter("counterpartcompanycode", "%" + counterpartcompanycode.ToLower() + "%"));
                        }

                        if(!string.IsNullOrEmpty(counterpartcompanyname)) {
                            sql.Append("and LOWER(a.counterpartcompanyname) like {0}counterpartcompanyname ");
                            dbParameters.Add(db.CreateDbParameter("counterpartcompanyname", "%" + counterpartcompanyname.ToLower() + "%"));
                        }

                        if(outBoundType.HasValue) {
                            sql.Append("and a.outBoundType ={0}outBoundType ");
                            dbParameters.Add(db.CreateDbParameter("outBoundType", outBoundType.Value));
                        }
                        if(!string.IsNullOrEmpty(partsSalesOrderTypeName)) {
                            sql.Append("and LOWER(a.partsSalesOrderTypeName) like {0}partsSalesOrderTypeName ");
                            dbParameters.Add(db.CreateDbParameter("partsSalesOrderTypeName", "%" + partsSalesOrderTypeName.ToLower() + "%"));
                        }

                        if(!string.IsNullOrEmpty(partsOutBoundPlanCode)) {
                            sql.Append("and LOWER(c.Code) like {0}partsoutboundplancode ");
                            dbParameters.Add(db.CreateDbParameter("partsoutboundplancode", "%" + partsOutBoundPlanCode.ToLower() + "%"));
                        }

                        if(!string.IsNullOrEmpty(outboundPackPlanCode)) {
                            sql.Append("and LOWER(a.outboundPackPlanCode) like {0}outboundPackPlanCode ");
                            dbParameters.Add(db.CreateDbParameter("outboundPackPlanCode", "%" + outboundPackPlanCode.ToLower() + "%"));
                        }
                        if(!string.IsNullOrEmpty(SAPPurchasePlanCode)) {
                            sql.Append("and LOWER(a.SAPPurchasePlanCode) like {0}SAPPurchasePlanCode ");
                            dbParameters.Add(db.CreateDbParameter("SAPPurchasePlanCode", "%" + SAPPurchasePlanCode.ToLower() + "%"));
                        }
                        if(!string.IsNullOrEmpty(zpNumber)) {
                            sql.Append("and LOWER(c.ZPNUMBER) like {0}ZPNUMBER ");
                            dbParameters.Add(db.CreateDbParameter("ZPNUMBER", "%" + zpNumber.ToLower() + "%"));
                        }
                        if(!string.IsNullOrEmpty(provinceName)) {
                            sql.Append("and LOWER(comp.ProvinceName) like {0}provinceName ");
                            dbParameters.Add(db.CreateDbParameter("provinceName", "%" + provinceName.ToLower() + "%"));
                        }

                        if(!string.IsNullOrEmpty(contractCode)) {
                            sql.Append("and LOWER(a.contractCode) like {0}contractCode ");
                            dbParameters.Add(db.CreateDbParameter("contractCode", "%" + contractCode.ToLower() + "%"));
                        }

                        if(settlementStatus.HasValue) {
                            sql.Append("and a.settlementStatus ={0}settlementStatus ");
                            dbParameters.Add(db.CreateDbParameter("settlementStatus", settlementStatus.Value));
                        }

                        if(createTimeBegin.HasValue) {
                            sql.Append(@" and a.createTime >=To_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }


                        if(createTimeEnd.HasValue) {
                            sql.Append(@" and a.createTime <=To_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }

                        if (!string.IsNullOrEmpty(cpPartsPurchaseOrderCode))
                        {
                            sql.Append(" and LOWER(a.CPPartsPurchaseOrderCode) like {0}cpPartsPurchaseOrderCode ");
                            dbParameters.Add(db.CreateDbParameter("cpPartsPurchaseOrderCode", "%" + cpPartsPurchaseOrderCode.ToLower() + "%"));
                        }

                        if (!string.IsNullOrEmpty(cpPartsInboundCheckCode))
                        {
                            sql.Append(" and LOWER(a.CPPartsInboundCheckCode) like {0}cpPartsInboundCheckCode ");
                            dbParameters.Add(db.CreateDbParameter("cpPartsInboundCheckCode", "%" + cpPartsInboundCheckCode.ToLower() + "%"));
                        }
                    }
                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    ErrorStrings.Export_Title_PartsOutboundBill_Code, ErrorStrings.Export_Title_Company_WarehouseCode, ErrorStrings.Export_Title_Company_WarehouseName, ErrorStrings.Export_Title_PartsOutboundBill_OutBoundType,ErrorStrings.Export_Title_InternalAllocationBill_TotalAmount,ErrorStrings.Export_Title_PartsOutboundBill_EmAmount, ErrorStrings.Export_Title_PartsInboundCheckBill_OriginalRequirementBillCode,ErrorStrings.Export_Title_PartsOutboundBill_SalesOrderType,ErrorStrings.Export_Title_PartsOutboundBill_PartsOutboundPlanCode,ErrorStrings.Export_Title_Partssalescategory_Name, ErrorStrings.Export_Title_PartsBranch_BranchName, ErrorStrings.Export_Title_BoxUpTask_CounterpartCompanyCode, ErrorStrings.Export_Title_BoxUpTask_CounterpartCompanyName,ErrorStrings.Export_Title_PartsOutboundBill_CounterpartCompanyType, ErrorStrings.Export_Title_PartsOutboundBill_ReceivingWarehouseCode, ErrorStrings.Export_Title_SupplierShippingOrder_ReveiveWarehouseName,ErrorStrings.Export_Validation_Company_ProvinceName,
                                    ErrorStrings.Export_Title_PartsOutboundBill_OutPackCode,ErrorStrings.Export_Title_PartsOutboundBill_BoxNo,ErrorStrings.Export_Title_PartsOutboundBill_ContractCode,ErrorStrings.Export_Title_PartsPurchaseOrder_GPMSPurOrderCode, ErrorStrings.Export_Title_PartsInboundCheckBill_SettlementStatus,ErrorStrings.Export_Title_PartsInboundCheckBill_BillingType,ErrorStrings.Export_Title_SupplierShippingOrder_ERPSourceOrderCode, ErrorStrings.Export_Title_PartsBranch_Remark,ErrorStrings.Export_Title_PartsOutboundBill_OrderApproveComment, ErrorStrings.Export_Title_AccountPeriod_CreatorName, ErrorStrings.Export_Title_AccountPeriod_CreateTime, ErrorStrings.Export_Title_AccountPeriod_ModifierName, ErrorStrings.Export_Title_AccountPeriod_ModifyTime,ErrorStrings.Export_Title_PartsInboundCheckBill_SAPPurchasePlanCode,
                                    ErrorStrings.Export_Title_PartsOutboundBill_OutPackOrderCode,"PWMS出库单号","原始采购订单编号","原始入库单编号","供应商是否已结算"
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }

        /// <summary>
        /// 合并导出工程车配件出库单
        /// </summary>
        //public bool ExportPartsOutboundBillWithDetail(int[] ids, int? userId, int? partsSalesCategoryId, int storageCompanyId, string partsOutBoundBillCode, int? warehouseId, string counterpartcompanycode, string counterpartcompanyname, int? outBoundType, int? settlementStatus, string partsSalesOrderTypeName, string outboundPackPlanCode, string contractCode, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName) {
        public bool 合并导出工程车配件出库单(int[] ids, string eRPSourceOrderCode, int? userId, int? partsSalesCategoryId, int storageCompanyId, string partsOutBoundBillCode, int? warehouseId, string counterpartcompanycode, string counterpartcompanyname, int? outBoundType, int? settlementStatus, string partsSalesOrderTypeName, string partsOutboundPlanCode, string outboundPackPlanCode, string contractCode, string provinceName, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName) {
            fileName = GetExportFilePath("工程车配件出库单主清单.xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);
                var userInfo = Utils.GetCurrentUserInfo();
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.Append(string.Format(@"select a.code,
                                        a.warehousecode,
                                        a.warehousename,
                                        (select value from keyvalueitem where NAME = 'Parts_OutboundType'and key=a.outboundtype) As outboundtype,
                                        a.zongjine,
                                        a.originalrequirementbillcode,
                                        a.PartsSalesOrderTypeName,
                                        a.PartsOutboundPlanCode,
                                        a.name,
                                        a.branchname,
                                        a.counterpartcompanycode,
                                        a.counterpartcompanyname,
                                        (select value from keyvalueitem where NAME = 'Company_Type'and key=a.type) As counterpartcompanyType, --对方单位类型
                                        a.ReceivingWarehouseCode,
                                        a.ReceivingWarehouseName,
                                        a.ProvinceName as provinceName,   --省份
                                        a.InterfaceRecordId,--WMS装载单号
                                        a.OutboundPackPlanCode,
                                        a.ContractCode,
                                        a.GPMSPurOrderCode,
                                        (select value from keyvalueitem where NAME = 'Parts_SettlementStatus'and key=a.settlementstatus) As settlementstatus,
                                        (select value from keyvalueitem where NAME = 'ERPInvoiceInformation_Type'and key=a.InvoiceType) As InvoiceType,
                                        a.ERPSourceOrderCode,
                                        a.remark,
                                        a.creatorname,
                                        a.createtime,
                                        a.modifiername,
                                        a.modifytime
                                    from OutboundOrder a
                                    where 1=1"));
                    var dbParameters = new List<DbParameter>();
                    if(ids != null && ids.Length > 0) {
                        sql.Append(" and a.id in (");
                        for(var i = 0; i < ids.Length; i++) {
                            if(ids.Length == i + 1) {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            } else {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                        }
                        sql.Append(")");
                    } else {
                        if(userId.HasValue) {
                            sql.Append(@"and  ( EXISTS (SELECT 
                                                  1 AS C1
                                                  FROM WarehouseOperator Extent4
                                                  WHERE Extent4.OperatorId = {0}userId AND Extent4.WarehouseId = a.WarehouseId)
                                                  or Exists(Select 1 From warehouse t Where t.id=a.warehouseid And Type=99)) ");
                            dbParameters.Add(db.CreateDbParameter("userId", userInfo.Id));
                        }
                        if(partsSalesCategoryId.HasValue) {
                            sql.Append(@" and a.partsSalesCategoryId = {0}partsSalesCategoryId ");
                            dbParameters.Add(db.CreateDbParameter("partsSalesCategoryId", partsSalesCategoryId.Value));
                        }
                        if(!string.IsNullOrEmpty(partsOutBoundBillCode)) {
                            sql.Append("and LOWER(a.Code) like {0}partsOutBoundBillCode ");
                            dbParameters.Add(db.CreateDbParameter("partsOutBoundBillCode", "%" + partsOutBoundBillCode.ToLower() + "%"));
                        }

                        if(warehouseId.HasValue) {
                            sql.Append("and a.warehouseId = {0}warehouseId ");
                            dbParameters.Add(db.CreateDbParameter("warehouseId", warehouseId.Value));
                        }
                        if(!string.IsNullOrEmpty(counterpartcompanycode)) {
                            sql.Append("and LOWER(a.counterpartcompanycode) like {0}counterpartcompanycode ");
                            dbParameters.Add(db.CreateDbParameter("counterpartcompanycode", "%" + counterpartcompanycode.ToLower() + "%"));
                        }
                        if(!string.IsNullOrEmpty(eRPSourceOrderCode)) {
                            sql.Append("and LOWER(ps.eRPSourceOrderCode) like {0}eRPSourceOrderCode ");
                            dbParameters.Add(db.CreateDbParameter("eRPSourceOrderCode", "%" + eRPSourceOrderCode.ToLower() + "%"));
                        }
                        if(!string.IsNullOrEmpty(counterpartcompanyname)) {
                            sql.Append("and LOWER(a.counterpartcompanyname) like {0}counterpartcompanyname ");
                            dbParameters.Add(db.CreateDbParameter("counterpartcompanyname", "%" + counterpartcompanyname.ToLower() + "%"));
                        }

                        if(outBoundType.HasValue) {
                            sql.Append("and a.outBoundType ={0}outBoundType ");
                            dbParameters.Add(db.CreateDbParameter("outBoundType", outBoundType.Value));
                        }
                        if(!string.IsNullOrEmpty(partsSalesOrderTypeName)) {
                            sql.Append("and LOWER(a.partsSalesOrderTypeName) like {0}partsSalesOrderTypeName ");
                            dbParameters.Add(db.CreateDbParameter("partsSalesOrderTypeName", "%" + partsSalesOrderTypeName.ToLower() + "%"));
                        }
                        if(!string.IsNullOrEmpty(outboundPackPlanCode)) {
                            sql.Append("and LOWER(a.outboundPackPlanCode) like {0}outboundPackPlanCode ");
                            dbParameters.Add(db.CreateDbParameter("outboundPackPlanCode", "%" + outboundPackPlanCode.ToLower() + "%"));
                        }

                        if(!string.IsNullOrEmpty(partsOutboundPlanCode)) {
                            sql.Append("and LOWER(c.code) like {0}partsoutboundplancode ");
                            dbParameters.Add(db.CreateDbParameter("partsoutboundplancode", "%" + partsOutboundPlanCode.ToLower() + "%"));
                        }
                        if(!string.IsNullOrEmpty(provinceName)) {
                            sql.Append("and LOWER(comp.ProvinceName) like {0}provinceName ");
                            dbParameters.Add(db.CreateDbParameter("provinceName", "%" + provinceName.ToLower() + "%"));
                        }

                        if(!string.IsNullOrEmpty(contractCode)) {
                            sql.Append("and LOWER(a.contractCode) like {0}contractCode ");
                            dbParameters.Add(db.CreateDbParameter("contractCode", "%" + contractCode.ToLower() + "%"));
                        }
                        if(settlementStatus.HasValue) {
                            sql.Append("and a.settlementStatus ={0}settlementStatus ");
                            dbParameters.Add(db.CreateDbParameter("settlementStatus", settlementStatus.Value));
                        }

                        if(createTimeBegin.HasValue) {
                            sql.Append(@" and a.createTime >=To_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }


                        if(createTimeEnd.HasValue) {
                            sql.Append(@" and a.createTime <=To_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                    }
                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    ErrorStrings.Export_Title_PartsOutboundBill_Code, ErrorStrings.Export_Title_Company_WarehouseCode, ErrorStrings.Export_Title_Company_WarehouseName, ErrorStrings.Export_Title_PartsOutboundBill_OutBoundType,ErrorStrings.Export_Title_InternalAllocationBill_TotalAmount, ErrorStrings.Export_Title_PartsInboundCheckBill_OriginalRequirementBillCode,ErrorStrings.Export_Title_PartsOutboundBill_SalesOrderType,ErrorStrings.Export_Title_PartsOutboundBill_PartsOutboundPlanCode,ErrorStrings.Export_Title_Partssalescategory_Name, ErrorStrings.Export_Title_PartsBranch_BranchName, ErrorStrings.Export_Title_BoxUpTask_CounterpartCompanyCode, ErrorStrings.Export_Title_BoxUpTask_CounterpartCompanyName,ErrorStrings.Export_Title_PartsOutboundBill_CounterpartCompanyType, ErrorStrings.Export_Title_PartsOutboundBill_ReceivingWarehouseCode, ErrorStrings.Export_Title_SupplierShippingOrder_ReveiveWarehouseName,ErrorStrings.Export_Validation_Company_ProvinceName,ErrorStrings.Export_Title_PartsOutboundBill_OutPackCode,ErrorStrings.Export_Title_PartsOutboundBill_ContractCode,ErrorStrings.Export_Title_PartsPurchaseOrder_GPMSPurOrderCode,ErrorStrings.Export_Title_AgencyPartsShippingOrder_WMSCode, ErrorStrings.Export_Title_PartsInboundCheckBill_SettlementStatus,ErrorStrings.Export_Title_PartsInboundCheckBill_BillingType,ErrorStrings.Export_Title_Partsoutboundplan_ERPOrderCode, ErrorStrings.Export_Title_PartsBranch_Remark, ErrorStrings.Export_Title_PartsOutboundBill_OrderApproveComment,ErrorStrings.Export_Title_AccountPeriod_CreatorName, ErrorStrings.Export_Title_AccountPeriod_CreateTime, ErrorStrings.Export_Title_AccountPeriod_ModifierName, ErrorStrings.Export_Title_AccountPeriod_ModifyTime
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }

    }
}
