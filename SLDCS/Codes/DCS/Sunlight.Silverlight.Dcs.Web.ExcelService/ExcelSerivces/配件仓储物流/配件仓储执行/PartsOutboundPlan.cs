﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Globalization;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        /// <summary>
        /// 合并导出配件出库计划主清单
        /// </summary>
        public bool ExportPartsOutboundPlanWithDetail(int? id, int? userId, int storageCompanyId, string partsOutboundPlanCode, string sourceCode, int? warehouseId, int? outboundType, DateTime? createTimeBegin, DateTime? createTimeEnd, string counterpartCompanyCode, string counterpartCompanyName, string partsSalesOrderTypeName, int? partsSalesCategoryId, int? status, string ERPSourceOrderCode, out string fileName) {
            fileName = GetExportFilePath("配件出库计划主清单.xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);
                var userInfo = Utils.GetCurrentUserInfo();
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.AppendFormat(@"Select a.Code,
                                       a.Warehousecode,
                                       a.Warehousename,
                                       (select value from keyvalueitem where NAME = 'Parts_OutboundType'and key=a.Outboundtype) As Outboundtype,
                                       (Select Sum(t.Price * t.Plannedamount)
                                          From Partsoutboundplandetail t
                                         Where t.Partsoutboundplanid = a.Id),
                                       a.Receivingcompanycode,
                                       a.Receivingcompanyname,
                                       a.ERPSourceOrderCode,
                                       a.Createtime,
                                       (Select Min(t.Code)
                                          From Warehousearea t
                                         Inner Join Partsstock v
                                            On t.Id = v.Warehouseareaid
                                         Inner Join Warehouseareacategory x
                                            On x.Id = t.Areacategoryid
                                         Where b.Sparepartid = v.Partid
                                           And a.Warehouseid = v.Warehouseid
                                           And t.Areakind = 3
                                           And x.Category = 1),
                                       b.Sparepartcode,
                                       b.Sparepartname,
                                       b.Plannedamount,
                                       b.Outboundfulfillment,
                                       b.Price,
                                       (Select t.Measureunit From Sparepart t Where t.Id = b.Sparepartid),
                                       b.Remark
                                  From Partsoutboundplan a
                                  Left Join Partsoutboundplandetail b
                                    On a.Id = b.Partsoutboundplanid
                                  Left Join Partssalescategory c
                                    On a.Partssalescategoryid = c.Id
                                 Where ((a.Status IN (1,2)) AND ( EXISTS (SELECT 
                                          1 AS C1
                                          FROM WarehouseArea Extent3
                                          WHERE ((((Extent3.AreaKind = 3) AND (Extent3.Status = 1)) AND ( EXISTS (SELECT 
                                            1 AS C1
                                            FROM WarehouseAreaCategory Extent4
                                            WHERE (Extent4.Category = 1) AND (Extent4.Id = Extent3.AreaCategoryId)
                                          )))) AND (a.WarehouseId = Extent3.WarehouseId)
                                        ))) AND ( NOT (( EXISTS (SELECT 
                                          1 AS C1
                                          FROM Warehouse Extent6
                                          WHERE (Extent6.Id = a.WarehouseId) AND (Extent6.WmsInterface = 1)
                                        )) AND (((a.OutboundType IN (2,3)) OR ((a.OutboundType = 1) AND ( NOT EXISTS (SELECT 
                                          1 AS C1
                                          FROM Warehouse Extent7
                                          WHERE (Extent7.Id = a.WarehouseId) AND (Extent7.Name IN ('海外中重卡北京CDC仓库','海外轻卡北京CDC仓库','海外乘用车北京CDC仓库'))
                                        )))) OR ((a.OutboundType = 4) AND ( EXISTS (SELECT 
                                          1 AS C1
                                          FROM   PartsTransferOrder Extent8
                                          INNER JOIN Warehouse Extent9 ON Extent8.OriginalWarehouseId = Extent9.Id
                                          INNER JOIN Warehouse a0 ON Extent8.DestWarehouseId = a0.Id
                                          WHERE (Extent9.StorageCenter <> a0.StorageCenter) AND (Extent8.Id = a.OriginalRequirementBillId)
                                        )))))) and a.Storagecompanyid = {0} ", userInfo.EnterpriseId);
                    var dbParameters = new List<DbParameter>();
                    if(id.HasValue) {
                        sql.Append(" and a.id = {0}id");
                        dbParameters.Add(db.CreateDbParameter("id", id.Value));
                    } else {
                        if(userId.HasValue) {
                            sql.Append(@"and  ( EXISTS (SELECT 
      1 AS C1
      FROM WarehouseOperator wo
      WHERE wo.OperatorId = {0}userId AND wo.WarehouseId = a.WarehouseId)) ");
                            dbParameters.Add(db.CreateDbParameter("userId", userInfo.Id));
                        }
                        if(!String.IsNullOrEmpty(partsOutboundPlanCode)) {
                            sql.Append(@" and LOWER(a.Code) like {0}partsOutboundPlanCode ");
                            dbParameters.Add(db.CreateDbParameter("partsOutboundPlanCode", "%" + partsOutboundPlanCode.ToLower() + "%"));
                        }
                        if(!String.IsNullOrEmpty(sourceCode)) {
                            sql.Append(@" and LOWER(a.sourceCode) like {0}sourceCode ");
                            dbParameters.Add(db.CreateDbParameter("sourceCode", "%" + sourceCode.ToLower() + "%"));
                        }
                        if(warehouseId.HasValue) {
                            sql.Append(@" and a.warehouseId = {0}warehouseId ");
                            dbParameters.Add(db.CreateDbParameter("warehouseId", warehouseId.Value));
                        }
                        if(outboundType.HasValue) {
                            sql.Append(@" and a.outboundType = {0}outboundType ");
                            dbParameters.Add(db.CreateDbParameter("outboundType", outboundType.Value));
                        }
                        if(createTimeBegin.HasValue) {
                            sql.Append(@" and a.createTime >=to_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(@" and a.createTime <=to_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                        if(!String.IsNullOrEmpty(counterpartCompanyCode)) {
                            sql.Append(@" and LOWER(a.counterpartCompanyCode) like {0}counterpartCompanyCode ");
                            dbParameters.Add(db.CreateDbParameter("counterpartCompanyCode", "%" + counterpartCompanyCode.ToLower() + "%"));
                        }
                        if(!String.IsNullOrEmpty(ERPSourceOrderCode)) {
                            sql.Append(@" and LOWER(a.ERPSourceOrderCode) like {0}ERPSourceOrderCode ");
                            dbParameters.Add(db.CreateDbParameter("ERPSourceOrderCode", "%" + ERPSourceOrderCode.ToLower() + "%"));
                        }
                        if(!String.IsNullOrEmpty(counterpartCompanyName)) {
                            sql.Append(@" and LOWER(a.counterpartCompanyName) like {0}counterpartCompanyName ");
                            dbParameters.Add(db.CreateDbParameter("counterpartCompanyName", "%" + counterpartCompanyName.ToLower() + "%"));
                        }
                        if(!String.IsNullOrEmpty(partsSalesOrderTypeName)) {
                            sql.Append(@" and LOWER(a.partsSalesOrderTypeName) like {0}partsSalesOrderTypeName ");
                            dbParameters.Add(db.CreateDbParameter("partsSalesOrderTypeName", "%" + partsSalesOrderTypeName.ToLower() + "%"));
                        }
                        if(partsSalesCategoryId.HasValue) {
                            sql.Append(@" and c.Id ={0}partsSalesCategoryId ");
                            dbParameters.Add(db.CreateDbParameter("partsSalesCategoryId", partsSalesCategoryId.Value));
                        }
                        if(status.HasValue) {
                            sql.Append(@" and a.Status ={0}status ");
                            dbParameters.Add(db.CreateDbParameter("status", status.Value));
                        }                       
                    }

                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    ErrorStrings.Export_Title_Partsoutboundplan_Code, ErrorStrings.Export_Title_Company_WarehouseCode, ErrorStrings.Export_Title_Company_WarehouseName, ErrorStrings.Export_Title_PartsOutboundBill_OutBoundType,ErrorStrings.Export_Title_InternalAllocationBill_TotalAmount, ErrorStrings.Export_Title_Partsoutboundplan_ReceivingCompanyCode, ErrorStrings.Export_Title_PartsPurchaseOrder_ReceivingCompanyName, ErrorStrings.Export_Title_SupplierShippingOrder_ERPSourceOrderCode,ErrorStrings.Export_Title_AccountPeriod_CreateTime, ErrorStrings.Export_Title_PartsInboundCheckBill_SparePartsLocation, 
                                    ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode, ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName, ErrorStrings.Export_Title_PartsPurchasePlanDetail_PlanAmount, ErrorStrings.Export_Title_Partsoutboundplan_OutboundFulfillment, ErrorStrings.Export_Title_PartsPurReturnOrder_Price,ErrorStrings.Export_Title_PackingPropertyApp_MeasureUnit,ErrorStrings.Export_Title_PartsPurchaseOrder_DetailRemark
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }
        /// <summary>
        /// 合并导出配件出库计划主单ForQuery
        /// </summary>
        public bool ExportPartsOutboundPlanForQuery1(int[] ids, int? userId,  int storageCompanyId, string partsOutboundPlanCode, string status, string sourceCode, int? warehouseId, int? outboundType, DateTime? createTimeBegin, DateTime? createTimeEnd, string counterpartCompanyName, string partsSalesOrderTypeName, string SAPPurchasePlanCode, string ZPNUMBER, string eRPSourceOrderCode, string sparePartCode, string sparePartName, out string fileName)
        {
            fileName = GetExportFilePath("配件出库计划主单.xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);
                var userInfo = Utils.GetCurrentUserInfo();
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.AppendFormat(@"Select  (select value from keyvalueitem where NAME = 'PartsOutboundPlan_Status'and key=a.Status) As Status, 
                                               a.Code,                                               
                                               a.Warehousename,
                                               (select value from keyvalueitem where NAME = 'Parts_OutboundType'and key=a.Outboundtype) As Outboundtype,
                                               (Select Sum(t.Price * t.Plannedamount)
                                                  From Partsoutboundplandetail t
                                                 Where t.Partsoutboundplanid = a.Id),
                                             (Select count(1)
                                                  From Partsoutboundplandetail t
                                                 Where t.Partsoutboundplanid = a.Id),
                                                 a.Createtime,
                                               a.OriginalRequirementBillCode,
                                               a.PartsSalesOrderTypeName,
                                                (select m.code from partsshippingorder m  inner join partsshippingorderdetail n on n.partsshippingorderid = m.id where m.status <> 99 and n.partsoutboundplanid = a.id and rownum <= 1) as shippingcode,
                                               (select value from keyvalueitem where NAME = 'PartsShipping_Method'and key=a.ShippingMethod) As ShippingMethod,
                                               a.CounterpartCompanyName,
                                               a.OrderApproveComment,
                                                a.ReceivingAddress,
(case when (select count(1) from InternalAllocationBill ia where ia.sourcecode=a.originalrequirementbillcode)>0 then cast('是' as varchar2(100)) else cast('否' as varchar2(100)) end ) as IsInternalAllocationBill,
                                               a.Remark,
                                               a.CreatorName,
                                               a.ModifierName,
                                               a.ModifyTime, 
                                               a.StopComment,
                                               a.Stoper,   
                                               a.StopTime,
                                               a.Warehousecode,
                                               a.CounterpartCompanyCode, 
                                               a.ReceivingWarehouseCode,
                                               a.ReceivingWarehouseName,
                                               c.Name,
                                               a.BranchName, (select sum(sp.weight)
                                                  from Partsoutboundplandetail p
                                                  join sparepart sp
                                                    on p.sparepartid = sp.id
                                                where p.Partsoutboundplanid =a.id) as weight,
                                              (select sum(sp.volume)
                                                  from Partsoutboundplandetail p
                                                  join sparepart sp
                                                    on p.sparepartid = sp.id
                                                where p.Partsoutboundplanid =a.id) as volume
                                               
                                          From Partsoutboundplan a
                                         
                                          Left Join Partssalescategory c
                                            On a.Partssalescategoryid = c.Id
                                         Where a.Storagecompanyid = {0} ", userInfo.EnterpriseId);
                    var dbParameters = new List<DbParameter>();
                    if(ids != null && ids.Length > 0) {
                        sql.Append(" and a.id in (");
                        for(var i = 0; i < ids.Length; i++) {
                            if(ids.Length == i + 1) {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            } else {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                        }
                        sql.Append(")");
                    } else {
                        if(userId.HasValue) {
                            sql.Append(@"and  ( EXISTS (SELECT 
      1 AS C1
      FROM WarehouseOperator wo
      WHERE wo.OperatorId = {0}userId AND wo.WarehouseId = a.WarehouseId)     ) ");
                            dbParameters.Add(db.CreateDbParameter("userId", userInfo.Id));
                        }
                     
                        if(!String.IsNullOrEmpty(partsOutboundPlanCode)) {
                            sql.Append(@" and LOWER(a.Code) like {0}partsOutboundPlanCode ");
                            dbParameters.Add(db.CreateDbParameter("partsOutboundPlanCode", "%" + partsOutboundPlanCode.ToLower() + "%"));
                        }

                        if(!String.IsNullOrEmpty(eRPSourceOrderCode)) {
                            sql.Append(@" and LOWER(a.eRPSourceOrderCode) like {0}eRPSourceOrderCode ");
                            dbParameters.Add(db.CreateDbParameter("eRPSourceOrderCode", "%" + eRPSourceOrderCode.ToLower() + "%"));
                        } 
                        if (!String.IsNullOrEmpty(sparePartCode))
                        {
                            sql.Append(@" and  exists(select 1 from Partsoutboundplandetail b where a.Id = b.Partsoutboundplanid and LOWER(b.sparePartCode) like {0}sparePartCode) ");
                            dbParameters.Add(db.CreateDbParameter("sparePartCode", "%" + sparePartCode.ToLower() + "%"));
                        } 
                        if (!String.IsNullOrEmpty(sparePartName))
                        {
                            sql.Append(@" and  exists(select 1 from Partsoutboundplandetail b where a.Id = b.Partsoutboundplanid and LOWER(b.sparePartName) like {0}sparePartName) ");
                            dbParameters.Add(db.CreateDbParameter("sparePartName", "%" + sparePartName.ToLower() + "%"));
                        }

                        if(!string.IsNullOrEmpty(status)) {
                            sql.Append(string.Format(" and a.status in ({0})",status));
                        }
                        if(!String.IsNullOrEmpty(sourceCode)) {
                            sql.Append(@" and LOWER(a.sourceCode) like {0}sourceCode ");
                            dbParameters.Add(db.CreateDbParameter("sourceCode", "%" + sourceCode.ToLower() + "%"));
                        }
                        if(warehouseId.HasValue) {
                            sql.Append(@" and a.warehouseId = {0}warehouseId ");
                            dbParameters.Add(db.CreateDbParameter("warehouseId", warehouseId.Value));
                        }
                        if(outboundType.HasValue) {
                            sql.Append(@" and a.outboundType = {0}outboundType ");
                            dbParameters.Add(db.CreateDbParameter("outboundType", outboundType.Value));
                        }
                        if(createTimeBegin.HasValue) {
                            sql.Append(@" and a.createTime >=to_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(@" and a.createTime <=to_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                     
                        if(!String.IsNullOrEmpty(counterpartCompanyName)) {
                            sql.Append(@" and LOWER(a.counterpartCompanyName) like {0}counterpartCompanyName ");
                            dbParameters.Add(db.CreateDbParameter("counterpartCompanyName", "%" + counterpartCompanyName.ToLower() + "%"));
                        }
                        if(!String.IsNullOrEmpty(partsSalesOrderTypeName)) {
                            sql.Append(@" and LOWER(a.partsSalesOrderTypeName) like {0}partsSalesOrderTypeName ");
                            dbParameters.Add(db.CreateDbParameter("partsSalesOrderTypeName", "%" + partsSalesOrderTypeName.ToLower() + "%"));
                        }
                        if(!String.IsNullOrEmpty(SAPPurchasePlanCode)) {
                            sql.Append(@" and LOWER(a.SAPPurchasePlanCode) like {0}SAPPurchasePlanCode ");
                            dbParameters.Add(db.CreateDbParameter("SAPPurchasePlanCode", "%" + SAPPurchasePlanCode.ToLower() + "%"));
                        }
                        if(!String.IsNullOrEmpty(ZPNUMBER)) {
                            sql.Append(@" and LOWER(a.ZPNUMBER) like {0}ZPNUMBER ");
                            dbParameters.Add(db.CreateDbParameter("ZPNUMBER", "%" + ZPNUMBER.ToLower() + "%"));
                        }
                    }

                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    ErrorStrings.Export_Title_AccountPeriod_Status,ErrorStrings.Export_Title_Partsoutboundplan_Code, ErrorStrings.Export_Title_Company_WarehouseName,  ErrorStrings.Export_Title_PartsOutboundBill_OutBoundType,ErrorStrings.Export_Title_InternalAllocationBill_TotalAmount,ErrorStrings.Export_Title_Partsoutboundplan_Item, ErrorStrings.Export_Title_AccountPeriod_CreateTime,ErrorStrings.Export_Title_PartsInboundCheckBill_OriginalRequirementBillCode,ErrorStrings.Export_Title_PartsOutboundBill_SalesOrderType,ErrorStrings.Export_Title_SupplierShippingOrder_Code,ErrorStrings.Export_Title_PartsPurchaseOrder_ShippingMethod,ErrorStrings.Export_Title_BoxUpTask_CounterpartCompanyName,ErrorStrings.Export_Title_PartsOutboundBill_OrderApproveComment,ErrorStrings.Export_Title_PartsPurchaseOrder_ReceiveAddress,"是否有内部领出单",ErrorStrings.Export_Title_PartsBranch_Remark,ErrorStrings.Export_Title_AccountPeriod_CreatorName,ErrorStrings.Export_Title_AccountPeriod_ModifierName,ErrorStrings.Export_Title_AccountPeriod_ModifyTime,ErrorStrings.Export_Title_Partsoutboundplan_StopComment,ErrorStrings.Export_Title_Partsoutboundplan_Stoper,ErrorStrings.Export_Title_Partsoutboundplan_StopTime,ErrorStrings.Export_Title_Company_WarehouseCode,ErrorStrings.Export_Title_BoxUpTask_CounterpartCompanyCode,ErrorStrings.Export_Title_PartsOutboundBill_ReceivingWarehouseCode,ErrorStrings.Export_Title_SupplierShippingOrder_ReveiveWarehouseName,  ErrorStrings.Export_Title_Partssalescategory_Name,ErrorStrings.Export_Title_Partsoutboundplan_Branch, "总重量","总体积"
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }

        /// <summary>
        /// 合并导出配件出库计划主清单ForQuery
        /// </summary>
        public bool ExportPartsOutboundPlanWithDetailForQuery(int[] ids, int? userId, int? partsSalesCategoryId, int storageCompanyId, string partsOutboundPlanCode, int? status, string sourceCode, int? warehouseId, int? outboundType, DateTime? createTimeBegin, DateTime? createTimeEnd, string counterpartCompanyCode, string counterpartCompanyName, string partsSalesOrderTypeName, string ERPSourceOrderCode, out string fileName) {
            fileName = GetExportFilePath("配件出库计划主清单.xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);
                var userInfo = Utils.GetCurrentUserInfo();
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.AppendFormat(@"Select a.Code,
       a.Warehousecode,
       a.Warehousename,
       (select value from keyvalueitem where NAME = 'Parts_OutboundType'and key=a.Outboundtype) As Outboundtype,
       (Select Sum(t.Price * t.PlannedAmount)
          From APartsOutboundPlanDetail t
         Where t.Partsoutboundplanid = a.Id),
       a.Receivingcompanycode,
       a.Receivingcompanyname,
       a.ReceivingWarehouseCode,
       a.ReceivingWarehouseName,
       a.Createtime,
       a.OriginalRequirementBillCode,
       c.Name,
       a.ERPSourceOrderCode,
       a.PartsSalesOrderTypeName,
       (select value from keyvalueitem where NAME = 'PartsOutboundPlan_Status'and key=a.Status) As Status,
       a.Remark,
       a.StopComment,
       (Select Min(t.Code)
          From Warehousearea t
         Inner Join Partsstock v
            On t.Id = v.Warehouseareaid
         Inner Join Warehouseareacategory x
            On x.Id = t.Areacategoryid
         Where b.Sparepartid = v.Partid
           And a.Warehouseid = v.Warehouseid
           And t.Areakind = 3
           And x.Category = 1),
       b.Sparepartcode,
       b.Sparepartname,
       b.Plannedamount,
       b.Outboundfulfillment,
       b.Price,
       (Select t.Measureunit From Sparepart t Where t.Id = b.Sparepartid),
       b.Remark
  From AgencyPartsOutboundPlan a
  Left Join APartsOutboundPlanDetail b
    On a.Id = b.Partsoutboundplanid
  Left Join Partssalescategory c
    On a.Partssalescategoryid = c.Id
 Where a.Storagecompanyid = {0} ", userInfo.EnterpriseId);
                    var dbParameters = new List<DbParameter>();
                    if(ids != null && ids.Length > 0) {
                        sql.Append(" and a.id in (");
                        for(var i = 0; i < ids.Length; i++) {
                            if(ids.Length == i + 1) {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            } else {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                        }
                        sql.Append(")");
                    } else {
                        if(userId.HasValue) {
                            sql.Append(@"and  ( EXISTS (SELECT 
      1 AS C1
      FROM WarehouseOperator wo
      WHERE wo.OperatorId = {0}userId AND wo.WarehouseId = a.WarehouseId     ) ");
                            dbParameters.Add(db.CreateDbParameter("userId", userInfo.Id));
                        }
                        if(partsSalesCategoryId.HasValue) {
                            sql.Append(@" and a.partsSalesCategoryId = {0}partsSalesCategoryId ");
                            dbParameters.Add(db.CreateDbParameter("partsSalesCategoryId", partsSalesCategoryId.Value));
                        }
                        if(!String.IsNullOrEmpty(partsOutboundPlanCode)) {
                            sql.Append(@" and LOWER(a.Code) like {0}partsOutboundPlanCode ");
                            dbParameters.Add(db.CreateDbParameter("partsOutboundPlanCode", "%" + partsOutboundPlanCode.ToLower() + "%"));
                        }
                        if(status.HasValue) {
                            sql.Append(@" and a.status = {0}status ");
                            dbParameters.Add(db.CreateDbParameter("status", status.Value));
                        }
                        if(!String.IsNullOrEmpty(sourceCode)) {
                            sql.Append(@" and LOWER(a.sourceCode) like {0}sourceCode ");
                            dbParameters.Add(db.CreateDbParameter("sourceCode", "%" + sourceCode.ToLower() + "%"));
                        }
                        if(warehouseId.HasValue) {
                            sql.Append(@" and a.warehouseId = {0}warehouseId ");
                            dbParameters.Add(db.CreateDbParameter("warehouseId", warehouseId.Value));
                        }
                        if(outboundType.HasValue) {
                            sql.Append(@" and a.outboundType = {0}outboundType ");
                            dbParameters.Add(db.CreateDbParameter("outboundType", outboundType.Value));
                        }
                        if(createTimeBegin.HasValue) {
                            sql.Append(@" and a.createTime >=to_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(@" and a.createTime <=to_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                        if(!String.IsNullOrEmpty(counterpartCompanyCode)) {
                            sql.Append(@" and LOWER(a.counterpartCompanyCode) like {0}counterpartCompanyCode ");
                            dbParameters.Add(db.CreateDbParameter("counterpartCompanyCode", "%" + counterpartCompanyCode.ToLower() + "%"));
                        }
                        if(!String.IsNullOrEmpty(counterpartCompanyName)) {
                            sql.Append(@" and LOWER(a.counterpartCompanyName) like {0}counterpartCompanyName ");
                            dbParameters.Add(db.CreateDbParameter("counterpartCompanyName", "%" + counterpartCompanyName.ToLower() + "%"));
                        }
                        if(!String.IsNullOrEmpty(partsSalesOrderTypeName)) {
                            sql.Append(@" and LOWER(a.partsSalesOrderTypeName) like {0}partsSalesOrderTypeName ");
                            dbParameters.Add(db.CreateDbParameter("partsSalesOrderTypeName", "%" + partsSalesOrderTypeName.ToLower() + "%"));
                        }
                        if(!String.IsNullOrEmpty(ERPSourceOrderCode)) {
                            sql.Append(@" and LOWER(a.ERPSourceOrderCode) like {0}ERPSourceOrderCode ");
                            dbParameters.Add(db.CreateDbParameter("ERPSourceOrderCode", "%" + ERPSourceOrderCode.ToLower() + "%"));
                        }
                    }

                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    ErrorStrings.Export_Title_Partsoutboundplan_Code, ErrorStrings.Export_Title_Company_WarehouseCode, ErrorStrings.Export_Title_Company_WarehouseName, ErrorStrings.Export_Title_PartsOutboundBill_OutBoundType,ErrorStrings.Export_Title_InternalAllocationBill_TotalAmount, ErrorStrings.Export_Title_Partsoutboundplan_ReceivingCompanyCode, ErrorStrings.Export_Title_PartsPurchaseOrder_ReceivingCompanyName,ErrorStrings.Export_Title_PartsOutboundBill_ReceivingWarehouseCode,ErrorStrings.Export_Title_SupplierShippingOrder_ReveiveWarehouseName, ErrorStrings.Export_Title_AccountPeriod_CreateTime, ErrorStrings.Export_Title_PartsInboundCheckBill_OriginalRequirementBillCode,ErrorStrings.Export_Title_Partssalescategory_Name,ErrorStrings.Export_Title_Partsoutboundplan_ERPOrderCode,ErrorStrings.Export_Title_PartsOutboundBill_SalesOrderType,ErrorStrings.Export_Title_AccountPeriod_Status,ErrorStrings.Export_Title_PartsBranch_Remark,ErrorStrings.Export_Title_Partsoutboundplan_StopComment,ErrorStrings.Export_Title_PartsInboundCheckBill_SparePartsLocation, 
                                    ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode, ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName, ErrorStrings.Export_Title_PartsPurchasePlanDetail_PlanAmount, ErrorStrings.Export_Title_Partsoutboundplan_OutboundFulfillment, ErrorStrings.Export_Title_PartsPurReturnOrder_Price,ErrorStrings.Export_Title_PackingPropertyApp_MeasureUnit,ErrorStrings.Export_Title_PartsPurchaseOrder_DetailRemark,ErrorStrings.Export_Title_PartsInboundCheckBill_SAPPurchasePlanCode,ErrorStrings.Export_Title_PartsOutboundBill_OutPackOrderCode
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }


        /// <summary>
        /// 合并导出配件出库计划主清单ForQuery
        /// </summary>
        public bool ExportPartsOutboundPlanWithDetailForQuery1(int[] ids, int? userId,  int storageCompanyId, string partsOutboundPlanCode, string status, string sourceCode, int? warehouseId, int? outboundType, DateTime? createTimeBegin, DateTime? createTimeEnd, string counterpartCompanyName, string partsSalesOrderTypeName, string SAPPurchasePlanCode, string ZPNUMBER, string eRPSourceOrderCode, string sparePartCode, string sparePartName, out string fileName)
        {
            fileName = GetExportFilePath("配件出库计划主清单.xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);
                var userInfo = Utils.GetCurrentUserInfo();
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.AppendFormat(@"Select  (select value from keyvalueitem where NAME = 'PartsOutboundPlan_Status'and key=a.Status) As Status, 
                                               a.Code,                                               
                                               a.Warehousename,
                                               (select value from keyvalueitem where NAME = 'Parts_OutboundType'and key=a.Outboundtype) As Outboundtype,
                                               (Select Sum(t.Price * t.Plannedamount)
                                                  From Partsoutboundplandetail t
                                                 Where t.Partsoutboundplanid = a.Id),
                                             (Select count(1)
                                                  From Partsoutboundplandetail t
                                                 Where t.Partsoutboundplanid = a.Id),
                                                 a.Createtime,
                                               a.OriginalRequirementBillCode,
                                               a.PartsSalesOrderTypeName,
                                                (select m.code from partsshippingorder m  inner join partsshippingorderdetail n on n.partsshippingorderid = m.id where m.status <> 99 and n.partsoutboundplanid = a.id and n.sparepartid = b.sparepartid and rownum <= 1) as shippingcode,
                                               (select value from keyvalueitem where NAME = 'PartsShipping_Method'and key=a.ShippingMethod) As ShippingMethod,
                                               a.CounterpartCompanyName,
                                               a.OrderApproveComment,
                                                a.ReceivingAddress,
(case when (select count(1) from InternalAllocationBill ia where ia.sourcecode=a.originalrequirementbillcode)>0 then cast('是' as varchar2(100)) else cast('否' as varchar2(100)) end ) as IsInternalAllocationBill,
                                               a.Remark,
                                               a.CreatorName,
                                               a.ModifierName,
                                               a.ModifyTime, 
                                               a.StopComment,
                                               a.Stoper,   
                                               a.StopTime,
                                               a.Warehousecode,
                                               a.CounterpartCompanyCode, 
                                               a.ReceivingWarehouseCode,
                                               a.ReceivingWarehouseName,
                                               c.Name,
                                               a.BranchName,
                                               (Select Min(t.Code)
                                                  From Warehousearea t
                                                 Inner Join Partsstock v
                                                    On t.Id = v.Warehouseareaid
                                                 Inner Join Warehouseareacategory x
                                                    On x.Id = t.Areacategoryid
                                                 Where b.Sparepartid = v.Partid
                                                   And a.Warehouseid = v.Warehouseid
                                                   And t.Areakind = 3
                                                   And x.Category = 1),
                                               (select sum(sp.weight)
                                                  from Partsoutboundplandetail p
                                                  join sparepart sp
                                                    on p.sparepartid = sp.id
                                                where p.Partsoutboundplanid =a.id) as weight,
                                              (select sum(sp.volume)
                                                  from Partsoutboundplandetail p
                                                  join sparepart sp
                                                    on p.sparepartid = sp.id
                                                where p.Partsoutboundplanid =a.id) as volume,
                                               b.Sparepartcode,
                                               b.Sparepartname,
                                               b.Plannedamount,
                                               b.Outboundfulfillment,
                                               b.Price,
                                               (Select t.Measureunit From Sparepart t Where t.Id = b.Sparepartid),
                                               b.Remark
                                               
                                          From Partsoutboundplan a
                                          Left Join Partsoutboundplandetail b
                                            On a.Id = b.Partsoutboundplanid
                                          Left Join Partssalescategory c
                                            On a.Partssalescategoryid = c.Id
                                         Where a.Storagecompanyid = {0} ", userInfo.EnterpriseId);
                    var dbParameters = new List<DbParameter>();
                    if(ids != null && ids.Length > 0) {
                        sql.Append(" and a.id in (");
                        for(var i = 0; i < ids.Length; i++) {
                            if(ids.Length == i + 1) {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            } else {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                        }
                        sql.Append(")");
                    } else {
                        if(userId.HasValue) {
                            sql.Append(@"and  ( EXISTS (SELECT 
      1 AS C1
      FROM WarehouseOperator wo
      WHERE wo.OperatorId = {0}userId AND wo.WarehouseId = a.WarehouseId)) ");
                            dbParameters.Add(db.CreateDbParameter("userId", userInfo.Id));
                        }
                     
                        if(!String.IsNullOrEmpty(partsOutboundPlanCode)) {
                            sql.Append(@" and LOWER(a.Code) like {0}partsOutboundPlanCode ");
                            dbParameters.Add(db.CreateDbParameter("partsOutboundPlanCode", "%" + partsOutboundPlanCode.ToLower() + "%"));
                        }

                        if(!String.IsNullOrEmpty(eRPSourceOrderCode)) {
                            sql.Append(@" and LOWER(a.eRPSourceOrderCode) like {0}eRPSourceOrderCode ");
                            dbParameters.Add(db.CreateDbParameter("eRPSourceOrderCode", "%" + eRPSourceOrderCode.ToLower() + "%"));
                        } 
                        if (!String.IsNullOrEmpty(sparePartCode))
                        {
                            sql.Append(@" and LOWER(b.sparePartCode) like {0}sparePartCode ");
                            dbParameters.Add(db.CreateDbParameter("sparePartCode", "%" + sparePartCode.ToLower() + "%"));
                        } 
                        if (!String.IsNullOrEmpty(sparePartName))
                        {
                            sql.Append(@" and LOWER(b.sparePartName) like {0}sparePartName ");
                            dbParameters.Add(db.CreateDbParameter("sparePartName", "%" + sparePartName.ToLower() + "%"));
                        }

                        if(!string.IsNullOrEmpty(status)) {
                            sql.Append(string.Format(" and a.status in ({0})",status));
                        }
                        if(!String.IsNullOrEmpty(sourceCode)) {
                            sql.Append(@" and LOWER(a.sourceCode) like {0}sourceCode ");
                            dbParameters.Add(db.CreateDbParameter("sourceCode", "%" + sourceCode.ToLower() + "%"));
                        }
                        if(warehouseId.HasValue) {
                            sql.Append(@" and a.warehouseId = {0}warehouseId ");
                            dbParameters.Add(db.CreateDbParameter("warehouseId", warehouseId.Value));
                        }
                        if(outboundType.HasValue) {
                            sql.Append(@" and a.outboundType = {0}outboundType ");
                            dbParameters.Add(db.CreateDbParameter("outboundType", outboundType.Value));
                        }
                        if(createTimeBegin.HasValue) {
                            sql.Append(@" and a.createTime >=to_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(@" and a.createTime <=to_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                     
                        if(!String.IsNullOrEmpty(counterpartCompanyName)) {
                            sql.Append(@" and LOWER(a.counterpartCompanyName) like {0}counterpartCompanyName ");
                            dbParameters.Add(db.CreateDbParameter("counterpartCompanyName", "%" + counterpartCompanyName.ToLower() + "%"));
                        }
                        if(!String.IsNullOrEmpty(partsSalesOrderTypeName)) {
                            sql.Append(@" and LOWER(a.partsSalesOrderTypeName) like {0}partsSalesOrderTypeName ");
                            dbParameters.Add(db.CreateDbParameter("partsSalesOrderTypeName", "%" + partsSalesOrderTypeName.ToLower() + "%"));
                        }
                        if(!String.IsNullOrEmpty(SAPPurchasePlanCode)) {
                            sql.Append(@" and LOWER(a.SAPPurchasePlanCode) like {0}SAPPurchasePlanCode ");
                            dbParameters.Add(db.CreateDbParameter("SAPPurchasePlanCode", "%" + SAPPurchasePlanCode.ToLower() + "%"));
                        }
                        if(!String.IsNullOrEmpty(ZPNUMBER)) {
                            sql.Append(@" and LOWER(a.ZPNUMBER) like {0}ZPNUMBER ");
                            dbParameters.Add(db.CreateDbParameter("ZPNUMBER", "%" + ZPNUMBER.ToLower() + "%"));
                        }
                    }

                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    ErrorStrings.Export_Title_AccountPeriod_Status,ErrorStrings.Export_Title_Partsoutboundplan_Code, ErrorStrings.Export_Title_Company_WarehouseName,  ErrorStrings.Export_Title_PartsOutboundBill_OutBoundType,ErrorStrings.Export_Title_InternalAllocationBill_TotalAmount,ErrorStrings.Export_Title_Partsoutboundplan_Item, ErrorStrings.Export_Title_AccountPeriod_CreateTime,ErrorStrings.Export_Title_PartsInboundCheckBill_OriginalRequirementBillCode,ErrorStrings.Export_Title_PartsOutboundBill_SalesOrderType,ErrorStrings.Export_Title_SupplierShippingOrder_Code,ErrorStrings.Export_Title_PartsPurchaseOrder_ShippingMethod,ErrorStrings.Export_Title_BoxUpTask_CounterpartCompanyName,ErrorStrings.Export_Title_PartsOutboundBill_OrderApproveComment,ErrorStrings.Export_Title_PartsPurchaseOrder_ReceiveAddress,"是否有内部领出单",ErrorStrings.Export_Title_PartsBranch_Remark,ErrorStrings.Export_Title_AccountPeriod_CreatorName,ErrorStrings.Export_Title_AccountPeriod_ModifierName,ErrorStrings.Export_Title_AccountPeriod_ModifyTime,ErrorStrings.Export_Title_Partsoutboundplan_StopComment,ErrorStrings.Export_Title_Partsoutboundplan_Stoper,ErrorStrings.Export_Title_Partsoutboundplan_StopTime,ErrorStrings.Export_Title_Company_WarehouseCode,ErrorStrings.Export_Title_BoxUpTask_CounterpartCompanyCode,ErrorStrings.Export_Title_PartsOutboundBill_ReceivingWarehouseCode,ErrorStrings.Export_Title_SupplierShippingOrder_ReveiveWarehouseName,  ErrorStrings.Export_Title_Partssalescategory_Name,ErrorStrings.Export_Title_Partsoutboundplan_Branch,ErrorStrings.Export_Title_PartsInboundCheckBill_SparePartsLocation, 
                                    "总重量","总体积",ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode, ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName, ErrorStrings.Export_Title_PartsPurchasePlanDetail_PlanAmount, ErrorStrings.Export_Title_Partsoutboundplan_OutboundFulfillment, ErrorStrings.Export_Title_PartsPurReturnOrder_Price,ErrorStrings.Export_Title_PackingPropertyApp_MeasureUnit,ErrorStrings.Export_Title_PartsPurchaseOrder_DetailRemark
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }
        /// <summary>
        /// 合并导出配件出库计划主清单ForQuery
        /// </summary>
        public bool ExportPartsOutboundPlanForWarehouse(int[] ids, int? userId, int storageCompanyId, string partsOutboundPlanCode, string status, string sourceCode, int? warehouseId, int? outboundType, DateTime? createTimeBegin, DateTime? createTimeEnd, string counterpartCompanyName, string partsSalesOrderTypeName, string SAPPurchasePlanCode, string ZPNUMBER, string eRPSourceOrderCode, string sparePartCode, string sparePartName, out string fileName)
        {
            fileName = GetExportFilePath("配件出库计划主清单.xlsx");
            try
            {
                if (!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);
                var userInfo = Utils.GetCurrentUserInfo();
                using (var conn = db.CreateDbConnection())
                {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.AppendFormat(@"Select  (select value from keyvalueitem where NAME = 'PartsOutboundPlan_Status'and key=a.Status) As Status, 
                                               a.Code,                                               
                                               a.Warehousename,
                                               (select value from keyvalueitem where NAME = 'Parts_OutboundType'and key=a.Outboundtype) As Outboundtype,
                                                (select sum(plannedamount * price)
          from (select s.plannedamount,
                       nvl(d.retailguideprice, 0) as price,
                       partssalescategoryid,
                       Branchid,
                       partsoutboundplanid
                  from partsoutboundplandetail s
                  left join PartsRetailGuidePrice d
                    on s.sparepartid = d.sparepartid) tt
         where tt.partssalescategoryid = a.partssalescategoryid
           and tt.Branchid = a.Branchid
           and tt.partsoutboundplanid = a.id) as TotalAmountForWarehouse,
                                              (select count(1) from partsoutboundplandetail s where s.partsoutboundplanid=a.id),
                                                a.Createtime,
                                               a.OriginalRequirementBillCode,
                                               a.PartsSalesOrderTypeName,
                                                (select m.code from partsshippingorder m  inner join partsshippingorderdetail n on n.partsshippingorderid = m.id where m.status <> 99 and n.partsoutboundplanid = a.id and n.sparepartid = b.sparepartid and rownum <= 1) as shippingcode, 
                                               (select value from keyvalueitem where NAME = 'PartsShipping_Method'and key=a.ShippingMethod) As ShippingMethod,
                                               a.CounterpartCompanyName,
                                               a.OrderApproveComment,
                                                a.ReceivingAddress,
(case when (select count(1) from InternalAllocationBill ia where ia.sourcecode=a.originalrequirementbillcode)>0 then  cast('是' as varchar2(100))
         else
          cast('否' as varchar2(100)) end ) as IsInternalAllocationBill,
                                               a.Remark,
                                               a.CreatorName,
                                               a.ModifierName,
                                               a.ModifyTime, 
                                               a.StopComment,
                                               a.Stoper,   
                                               a.StopTime,
                                               a.Warehousecode,
                                               a.CounterpartCompanyCode, 
                                               a.ReceivingWarehouseCode,
                                               a.ReceivingWarehouseName,
                                               c.Name,
                                               a.BranchName,
                                               (Select Min(t.Code)
                                                  From Warehousearea t
                                                 Inner Join Partsstock v
                                                    On t.Id = v.Warehouseareaid
                                                 Inner Join Warehouseareacategory x
                                                    On x.Id = t.Areacategoryid
                                                 Where b.Sparepartid = v.Partid
                                                   And a.Warehouseid = v.Warehouseid
                                                   And t.Areakind = 3
                                                   And x.Category = 1),
                                               (select sum(sp.weight)
                                                  from Partsoutboundplandetail p
                                                  join sparepart sp
                                                    on p.sparepartid = sp.id
                                                where p.Partsoutboundplanid =a.id) as weight,
                                              (select sum(sp.volume)
                                                  from Partsoutboundplandetail p
                                                  join sparepart sp
                                                    on p.sparepartid = sp.id
                                                where p.Partsoutboundplanid =a.id) as volume,
                                               b.Sparepartcode,
                                               b.Sparepartname,
                                               b.Plannedamount,
                                               b.Outboundfulfillment,
                                               d.RetailGuidePrice,
                                               (Select t.Measureunit From Sparepart t Where t.Id = b.Sparepartid),
                                               b.Remark                           
                                          From Partsoutboundplan a
                                          Left Join Partsoutboundplandetail b
                                            On a.Id = b.Partsoutboundplanid
                                          Left Join Partssalescategory c
                                            On a.Partssalescategoryid = c.Id
                                          left  join PartsRetailGuidePrice d on a.PartsSalesCategoryId=d.PartsSalesCategoryId and a.BranchId=d.BranchId and b.SparePartId=d.SparePartId and d.Status=1
                                         Where a.Storagecompanyid = {0} ", userInfo.EnterpriseId);
                    var dbParameters = new List<DbParameter>();
                    if (ids != null && ids.Length > 0)
                    {
                        sql.Append(" and a.id in (");
                        for (var i = 0; i < ids.Length; i++)
                        {
                            if (ids.Length == i + 1)
                            {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                            else
                            {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                        }
                        sql.Append(")");
                    }
                    else
                    {
                        if (userId.HasValue)
                        {
                            sql.Append(@"and  ( EXISTS (SELECT 
      1 AS C1
      FROM WarehouseOperator wo
      WHERE wo.OperatorId = {0}userId AND wo.WarehouseId = a.WarehouseId)) ");
                            dbParameters.Add(db.CreateDbParameter("userId", userInfo.Id));
                        }

                        if (!String.IsNullOrEmpty(partsOutboundPlanCode))
                        {
                            sql.Append(@" and LOWER(a.Code) like {0}partsOutboundPlanCode ");
                            dbParameters.Add(db.CreateDbParameter("partsOutboundPlanCode", "%" + partsOutboundPlanCode.ToLower() + "%"));
                        }

                        if (!String.IsNullOrEmpty(eRPSourceOrderCode))
                        {
                            sql.Append(@" and LOWER(a.eRPSourceOrderCode) like {0}eRPSourceOrderCode ");
                            dbParameters.Add(db.CreateDbParameter("eRPSourceOrderCode", "%" + eRPSourceOrderCode.ToLower() + "%"));
                        }
                        if (!String.IsNullOrEmpty(sparePartCode))
                        {
                            sql.Append(@" and LOWER(b.sparePartCode) like {0}sparePartCode ");
                            dbParameters.Add(db.CreateDbParameter("sparePartCode", "%" + sparePartCode.ToLower() + "%"));
                        }
                        if (!String.IsNullOrEmpty(sparePartName))
                        {
                            sql.Append(@" and LOWER(b.sparePartName) like {0}sparePartName ");
                            dbParameters.Add(db.CreateDbParameter("sparePartName", "%" + sparePartName.ToLower() + "%"));
                        }

                        if (!string.IsNullOrEmpty(status))
                        {
                            sql.Append(string.Format(" and a.status in ({0})",status));
                        }
                        if (!String.IsNullOrEmpty(sourceCode))
                        {
                            sql.Append(@" and LOWER(a.sourceCode) like {0}sourceCode ");
                            dbParameters.Add(db.CreateDbParameter("sourceCode", "%" + sourceCode.ToLower() + "%"));
                        }
                        if (warehouseId.HasValue)
                        {
                            sql.Append(@" and a.warehouseId = {0}warehouseId ");
                            dbParameters.Add(db.CreateDbParameter("warehouseId", warehouseId.Value));
                        }
                        if (outboundType.HasValue)
                        {
                            sql.Append(@" and a.outboundType = {0}outboundType ");
                            dbParameters.Add(db.CreateDbParameter("outboundType", outboundType.Value));
                        }
                        if (createTimeBegin.HasValue)
                        {
                            sql.Append(@" and a.createTime >=to_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if (createTimeEnd.HasValue)
                        {
                            sql.Append(@" and a.createTime <=to_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }

                        if (!String.IsNullOrEmpty(counterpartCompanyName))
                        {
                            sql.Append(@" and LOWER(a.counterpartCompanyName) like {0}counterpartCompanyName ");
                            dbParameters.Add(db.CreateDbParameter("counterpartCompanyName", "%" + counterpartCompanyName.ToLower() + "%"));
                        }
                        if (!String.IsNullOrEmpty(partsSalesOrderTypeName))
                        {
                            sql.Append(@" and LOWER(a.partsSalesOrderTypeName) like {0}partsSalesOrderTypeName ");
                            dbParameters.Add(db.CreateDbParameter("partsSalesOrderTypeName", "%" + partsSalesOrderTypeName.ToLower() + "%"));
                        }
                        if (!String.IsNullOrEmpty(SAPPurchasePlanCode))
                        {
                            sql.Append(@" and LOWER(a.SAPPurchasePlanCode) like {0}SAPPurchasePlanCode ");
                            dbParameters.Add(db.CreateDbParameter("SAPPurchasePlanCode", "%" + SAPPurchasePlanCode.ToLower() + "%"));
                        }
                        if (!String.IsNullOrEmpty(ZPNUMBER))
                        {
                            sql.Append(@" and LOWER(a.ZPNUMBER) like {0}ZPNUMBER ");
                            dbParameters.Add(db.CreateDbParameter("ZPNUMBER", "%" + ZPNUMBER.ToLower() + "%"));
                        }
                    }

                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using (var excelExport = new ExcelExport(fileName))
                    {
                        excelExport.ExportByRow(index =>
                        {
                            if (index == 0)
                            {
                                return new object[] {
                                    ErrorStrings.Export_Title_AccountPeriod_Status,ErrorStrings.Export_Title_Partsoutboundplan_Code, ErrorStrings.Export_Title_Company_WarehouseName,  ErrorStrings.Export_Title_PartsOutboundBill_OutBoundType,ErrorStrings.Export_Title_InternalAllocationBill_TotalAmount,ErrorStrings.Export_Title_Partsoutboundplan_Item, ErrorStrings.Export_Title_AccountPeriod_CreateTime,ErrorStrings.Export_Title_PartsInboundCheckBill_OriginalRequirementBillCode,ErrorStrings.Export_Title_PartsOutboundBill_SalesOrderType,ErrorStrings.Export_Title_Partsoutboundplan_ShippingOrder,ErrorStrings.Export_Title_PartsPurchaseOrder_ShippingMethod,ErrorStrings.Export_Title_BoxUpTask_CounterpartCompanyName,ErrorStrings.Export_Title_PartsOutboundBill_OrderApproveComment,ErrorStrings.Export_Title_PartsPurchaseOrder_ReceiveAddress,"是否有内部领出单",ErrorStrings.Export_Title_PartsBranch_Remark,ErrorStrings.Export_Title_AccountPeriod_CreatorName,ErrorStrings.Export_Title_AccountPeriod_ModifierName,ErrorStrings.Export_Title_AccountPeriod_ModifyTime,ErrorStrings.Export_Title_Partsoutboundplan_StopComment,ErrorStrings.Export_Title_Partsoutboundplan_Stoper,ErrorStrings.Export_Title_Partsoutboundplan_StopTime,ErrorStrings.Export_Title_Company_WarehouseCode,ErrorStrings.Export_Title_BoxUpTask_CounterpartCompanyCode,ErrorStrings.Export_Title_PartsOutboundBill_ReceivingWarehouseCode,ErrorStrings.Export_Title_SupplierShippingOrder_ReveiveWarehouseName,  ErrorStrings.Export_Title_Partssalescategory_Name,ErrorStrings.Export_Title_Partsoutboundplan_Branch,ErrorStrings.Export_Title_PartsInboundCheckBill_SparePartsLocation, 
                                    "总重量","总体积",ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode, ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName, ErrorStrings.Export_Title_PartsPurchasePlanDetail_PlanAmount, ErrorStrings.Export_Title_Partsoutboundplan_OutboundFulfillment, ErrorStrings.Export_Title_PartsPurReturnOrder_Price,ErrorStrings.Export_Title_PackingPropertyApp_MeasureUnit,ErrorStrings.Export_Title_PartsPurchaseOrder_DetailRemark
                                };
                            }
                            if (reader.Read())
                            {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if (num != reader.FieldCount)
                                {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

    }
}