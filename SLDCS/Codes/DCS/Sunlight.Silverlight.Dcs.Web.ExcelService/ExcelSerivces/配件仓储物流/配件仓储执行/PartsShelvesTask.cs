﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Globalization;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        /// <summary>
        /// 合并导出配件上架任务单
        /// </summary>
        public bool ExportPartsShelvesTask(int[] ids, string code, string sourceBillCode, string partsInboundPlanCode, string partsInboundCheckBillCode, int? inboundType, string status, int? warehouseId, string sparePartCode, string sparePartName,bool? noWarehouseArea,string warehouseAreaCode,string warehouseArea, bool? hasDifference, DateTime? begainDate, DateTime? endDate,DateTime? shelvesFinishTimeBegin, DateTime? shelvesFinishTimeEnd, out string fileName) {
            fileName = GetExportFilePath("配件上架任务单.xlsx");

            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);
                var userInfo = Utils.GetCurrentUserInfo();
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.Append(string.Format(@"select PartsInboundCheckBillCode,WarehouseName,SparePartCode,
                                       SparePartName,PlannedAmount,
                                       ShelvesAmount,WarehouseArea,WarehouseAreaCode,ShelvesFinishTime,Remark,
                                       CreatorName,
                                       CreateTime,
                                       Modifiername,
                                       Modifytime,
									   (select value from keyvalueitem where NAME = 'ShelvesTaskStatus'and key = ppt.Status) as Status,
                                       PackingTaskCode,
                                       PartsInboundPlanCode,
                                      (select value from keyvalueitem where NAME = 'Parts_InboundType'and key = ppt.InboundType) as InboundType,
                                       SourceBillCode,SIHCodes,
                                       WarehouseCode,   
                                       BatchNumber,
                                      case HasDifference when 1 then cast('是' as varchar2(100)) else cast('否' as varchar2(100)) end as HasDifference ,
                                       topCode,
                                       warehouseArea1,
                                       qty,
                                       operatername,
                                       OperaterEndTime,
                                       equipment
                                  from (select pst.id                        as Id,
                                               pst.code                      as Code,
                                               pst.packingTaskCode           as PackingTaskCode,
                                               pst.partsInboundPlanCode      as PartsInboundPlanCode,
                                               pst.PartsInboundCheckBillCode as PartsInboundCheckBillCode,
                                               pst.WarehouseId               as WarehouseId,
                                               pst.WarehouseCode             as WarehouseCode,
                                               pst.WarehouseName             as WarehouseName,
                                               sp.Code                       as SparePartCode,
                                               sp.Name                       as SparePartName,
                                               sp.Measureunit,
                                               pst.BatchNumber               as BatchNumber,
                                               pst.PlannedAmount             as PlannedAmount,
                                               pst.ShelvesAmount             as ShelvesAmount,
                                               pst.SourceBillCode            as SourceBillCode,
                                                pst.Status  as Status,
                                               pst.CreatorName               as CreatorName,
                                              to_char(pst.CreateTime,'yyyy-MM-dd HH24:mi:ss')as CreateTime ,
                                               pst.ModifierName              as ModifierName,
                                               to_char(pst.ModifyTime,'yyyy-MM-dd HH24:mi:ss') as ModifyTime,
                                                to_char(pst.ShelvesFinishTime,'yyyy-MM-dd HH24:mi:ss') as ShelvesFinishTime,
                                               pst.Remark,
                                               decode(yy.WarehouseAreaCode,null,1,0)  as noWarehouseArea,
                                               pip.inboundtype,
                                               yy.WarehouseAreaCode          WarehouseAreaCode,
                                               yy.warehousearea              as WarehouseArea, pst.SIHCodes,
                                               (select count(1) from PartsDifferenceBackBill d inner join PartsDifferenceBackBillDtl dd on d.id =dd.partsdifferencebackbillid where d.type =1 and d.status<>99 and rownum=1 and dd.TaskId=pst.Id) as HasDifference ,
											   wat.code as topCode,
											   wa.code as warehouseArea1,
											   pp.qty,
											   pp.operatername,
											   pp.OperaterEndTime,
											   (select kv.value from keyvalueitem kv where kv.name='PartsInboundPer_EquipmentType' and kv.key=pp.equipment) as equipment
                                          from partsshelvestask pst
                                         inner join sparepart sp
                                            on sp.id = pst.sparepartId
                                         inner join partsinboundplan pip
                                            on pip.id = pst.partsinboundplanid
                                         inner join WarehouseOperator wo
                                            on wo.warehouseid = pst.warehouseid
                                           and wo.operatorid = {0}
                                          left join (select WarehouseArea.code as WarehouseAreaCode,
                                                    partsstock.warehouseid as warehouseid,
                                                    sd.code as warehousearea,
                                                    partsstock.partid as partid,
                                                     row_number() over(partition by partsstock.warehouseid, partsstock.partid order by WarehouseArea.Id) my_rank
                                               from WarehouseArea                                            
                                              inner join WarehouseAreaCategory
                                                 on WarehouseArea.AreaCategoryId = WarehouseAreaCategory.Id
                                                AND WarehouseAreaCategory.Category = 1
                                              inner join warehouseareamanager
                                                 on warehouseareamanager.warehouseareaid =
                                                    warehousearea.ParentId
                                                and warehouseareamanager.managerid = {0}
                                              inner join WarehouseArea sd
                                                 on sd.id = warehousearea.ParentId
                                               join partsstock
                                                 on partsstock.warehouseareaid = warehousearea.id) yy
                                    on yy.warehouseid = pst.warehouseid and yy.partid = sp.id and my_rank = 1  
									  left join PartsInboundPerformance pp on pst.code=pp.code and sp.id=pp.sparepartid and pp.intype=2
									   left join warehousearea wa on pp.warehouseareaid =wa.id
									   left join warehousearea wat on wa.parentid=wat.id
                                         where 1 = 1                                                                                  
                                        ) ppt 
                                 where 1 = 1
                                ", userInfo.Id));

                    var dbParameters = new List<DbParameter>();
                    if(ids != null && ids.Length > 0) {
                        sql.Append(" and id in (");
                        for(var i = 0; i < ids.Length; i++) {
                            if(ids.Length == i + 1) {
                                sql.Append("{0}" + ids[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter(ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            } else {
                                sql.Append("{0}" + ids[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter(ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                        }
                        sql.Append(")");
                    } else {

                        if(!string.IsNullOrEmpty(code)) {
                            sql.Append("and Code like {0}code ");
                            dbParameters.Add(db.CreateDbParameter("code", "%" + code + "%"));
                        }
                        if(!string.IsNullOrEmpty(sourceBillCode)) {
                            sql.Append("and SourceBillCode like {0}sourceBillCode ");
                            dbParameters.Add(db.CreateDbParameter("sourceBillCode", "%" + sourceBillCode + "%"));
                        }
                        if(!string.IsNullOrEmpty(partsInboundPlanCode)) {
                            sql.Append("and partsInboundPlanCode like {0}partsInboundPlanCode ");
                            dbParameters.Add(db.CreateDbParameter("partsInboundPlanCode", "%" + partsInboundPlanCode + "%"));
                        }
                        if(!string.IsNullOrEmpty(partsInboundCheckBillCode)) {
                            sql.Append("and partsInboundCheckBillCode like {0}partsInboundCheckBillCode ");
                            dbParameters.Add(db.CreateDbParameter("partsInboundCheckBillCode", "%" + partsInboundCheckBillCode + "%"));
                        }
                        if(!string.IsNullOrEmpty(sparePartCode)) {
                            sql.Append("and SparePartCode like {0}sparePartCode ");
                            dbParameters.Add(db.CreateDbParameter("sparePartCode", "%" + sparePartCode + "%"));
                        }
                        if(!string.IsNullOrEmpty(sparePartName)) {
                            sql.Append("and SparePartName like {0}sparePartName ");
                            dbParameters.Add(db.CreateDbParameter("sparePartName", "%" + sparePartName + "%"));
                        }

                        if(inboundType.HasValue) {
                            sql.Append("and inboundType = {0}inboundType ");
                            dbParameters.Add(db.CreateDbParameter("inboundType", inboundType.Value));
                        }
                        if(!string.IsNullOrEmpty(status)) {
                            sql.Append(string.Format("and status in ({0}) ", status));
                        }
                        if(warehouseId.HasValue) {
                            sql.Append("and warehouseId like {0}warehouseId ");
                            dbParameters.Add(db.CreateDbParameter("warehouseId", "%" + warehouseId + "%"));
                        }
                        if(begainDate.HasValue) {
                            sql.Append(@" and to_date(createTime,'yyyy-MM-dd HH24:mi:ss') >=To_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = begainDate.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(endDate.HasValue) {
                            sql.Append(@" and to_date(createTime,'yyyy-MM-dd HH24:mi:ss') <=To_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = endDate.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                        if(noWarehouseArea != null) {
                            if((bool)noWarehouseArea)
                                sql.Append("  and noWarehouseArea = 1");
                            else
                                sql.Append("  and noWarehouseArea = 0");
                        }
                        if(!string.IsNullOrEmpty(warehouseAreaCode)) {
                            sql.Append("and warehouseAreaCode like {0}warehouseAreaCode ");
                            dbParameters.Add(db.CreateDbParameter("warehouseAreaCode", "%" + warehouseAreaCode + "%"));
                        }
                        if(!string.IsNullOrEmpty(warehouseArea)) {
                            sql.Append("and warehouseArea like {0}warehouseArea ");
                            dbParameters.Add(db.CreateDbParameter("warehouseArea", "%" + warehouseArea + "%"));
                        }
                        if(shelvesFinishTimeBegin.HasValue) {
                            sql.Append(@" and to_date(shelvesFinishTime,'yyyy-MM-dd HH24:mi:ss') >=To_date({0}shelvesFinishTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = shelvesFinishTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("shelvesFinishTimeBegin", tempTime.ToString("G")));
                        }
                        if(shelvesFinishTimeEnd.HasValue) {
                            sql.Append(@" and to_date(shelvesFinishTime,'yyyy-MM-dd HH24:mi:ss') <=To_date({0}shelvesFinishTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = shelvesFinishTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("shelvesFinishTimeEnd", tempTime.ToString("G")));
                        }
                        if(hasDifference == true) {
                            sql.Append(" and exists (select 1 from PartsDifferenceBackBill where type=1 and status <>99 and SourceCode=ppt.Code)");
                        }
                        if(hasDifference == false) {
                            sql.Append(" and not exists (select 1 from PartsDifferenceBackBill where type=1 and status <>99 and SourceCode=ppt.Code)");
                        }
                    }
                    sql.Append(" order by createTime");
                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                   ErrorStrings.Export_Title_PartsShelvesTask_InBoundCode,ErrorStrings.Export_Title_Company_WarehouseName,ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode, ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName,ErrorStrings.Export_Title_PartsPurchasePlanDetail_PlanAmount, ErrorStrings.Export_Title_PartsShelvesTask_ShelvesAmount,ErrorStrings.Export_Title_WarehouseAreaCategory_Area, ErrorStrings.Export_Title_WarehouseAreaCategory_AreaKindCode,ErrorStrings.Export_Title_PartsShelvesTask_ShelvesFinishTime,ErrorStrings.Export_Title_PartsBranch_Remark, ErrorStrings.Export_Title_AccountPeriod_CreatorName,ErrorStrings.Export_Title_AccountPeriod_CreateTime, ErrorStrings.Export_Title_AccountPeriod_ModifierName,ErrorStrings.Export_Title_AccountPeriod_ModifyTime,ErrorStrings.Export_Title_AccountPeriod_Status,ErrorStrings.Export_Title_PartsShelvesTask_PackingCode, ErrorStrings.Export_Title_PartsShelvesTask_InBoundPlanCode,ErrorStrings.Export_Title_PackingTask_InboundType,ErrorStrings.Export_Title_PartsOuterPurchaseChange_SourceCode,"上架标签码",ErrorStrings.Export_Title_Company_WarehouseCode, ErrorStrings.Export_Title_PackingTask_BatchNumber,ErrorStrings.Export_Title_PackingTask_HasDifference,
								   "库区","库位","上架数量","上架人","上架时间","上架设备"};
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();

                }
                return true;
            } catch(Exception) {
                return false;
            }
        }

    }
}
