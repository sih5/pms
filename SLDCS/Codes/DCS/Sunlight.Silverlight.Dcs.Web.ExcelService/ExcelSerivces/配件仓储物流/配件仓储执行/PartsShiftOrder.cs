﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        /// <summary>
        /// 合并导出配件移库主清单
        /// </summary>
        public bool ExportPartsShiftOrderWithDetail(int? id, int storageCompanyId, string partsShiftOrderCode, int? warehouseId, int? type, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, int? shiftStatus, out string fileName) {
            fileName = GetExportFilePath("配件移库主清单_" + ".xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.Append(string.Format(@"Select a.Code,
                                                a.WarehouseName,
                                                (select value from keyvalueitem where NAME = 'PartsShiftOrder_Type'and key=a.Type) As Type,
                                                (select value from keyvalueitem where NAME = 'PartsShiftOrder_QuestionType'and key=a.QuestionType) As QuestionType,
                                                (select value from keyvalueitem where NAME = 'PartsShiftOrder_Status'and key=a.Status) As Status,
                                                (select value from keyvalueitem where NAME = 'PartsShiftOrderShiftStatus'and key=a.ShiftStatus) As ShiftStatus,
                                                a.Remark,
                                                a.CreatorName,
                                                a.CreateTime,
                                                a.modifiername,
                                                a.modifytime,
                                                a.InitialApproverName,
                                                a.InitialApproveTime,
                                                a.approvername,
                                                a.approvetime,a.SubmitterName,a.SubmitTime,a.CloserName,decode(a.status,5,a.ModifyTime,null) as cloaseTime,
                                                b.SparePartCode,
                                                b.SparePartName,
                                                b.OriginalWarehouseAreaCode,
                                                b.DestWarehouseAreaCode,
                                                b.Quantity,
                                                b.Remark,
                                                b.DownShelfQty,
                                                b.DownSIHCode,
                                                b.UpShelfQty,
                                                b.SIHCode
                                                From PartsShiftOrder a
                                                Inner Join PartsShiftOrderDetail b
                                                On b.PartsShiftOrderId=a.id
                                            where a.StorageCompanyId = {0}", storageCompanyId));
                    var dbParameters = new List<DbParameter>();
                    if(id.HasValue) {
                        sql.Append(" and a.id = {0}id");
                        dbParameters.Add(db.CreateDbParameter("id", id.Value));
                    } else {
                        if(!String.IsNullOrEmpty(partsShiftOrderCode)) {
                            sql.Append(@" and a.Code like {0}partsShiftOrderCode ");
                            dbParameters.Add(db.CreateDbParameter("partsShiftOrderCode", "%" + partsShiftOrderCode + "%"));
                        }
                        if(warehouseId.HasValue) {
                            sql.Append(@" and a.warehouseId = {0}warehouseId ");
                            dbParameters.Add(db.CreateDbParameter("warehouseId", warehouseId.Value));
                        }
                        if(status.HasValue){
                            sql.Append(@" and a.status = {0}status ");
                            dbParameters.Add(db.CreateDbParameter("status", status.Value));
                        }
                        if(type.HasValue) {
                            sql.Append(@" and a.type = {0}type ");
                            dbParameters.Add(db.CreateDbParameter("type", type.Value));
                        }
                        if(shiftStatus.HasValue) {
                            sql.Append(@" and a.shiftStatus = {0}shiftStatus ");
                            dbParameters.Add(db.CreateDbParameter("shiftStatus", shiftStatus.Value));
                        }
                        if(createTimeBegin.HasValue) {
                            sql.Append(@" and a.createTime >=to_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(@" and a.createTime <=to_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }

                    }

                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    ErrorStrings.Export_Title_PartsShiftOrder_Code, ErrorStrings.Export_Title_Company_WarehouseName, ErrorStrings.Export_Title_PartsShiftOrder_Type,ErrorStrings.Export_Title_PartsShiftOrder_QuestionType,ErrorStrings.Export_Title_AccountPeriod_Status,ErrorStrings.Export_PartsShiftOrder_Status, ErrorStrings.Export_Title_PartsBranch_Remark, ErrorStrings.Export_Title_AccountPeriod_CreatorName, ErrorStrings.Export_Title_AccountPeriod_CreateTime, ErrorStrings.Export_Title_AccountPeriod_ModifierName,ErrorStrings.Export_Title_AccountPeriod_ModifyTime,ErrorStrings.Export_Title_PartsPurReturnOrder_InitialApproverName,ErrorStrings.Export_Title_PartsPurReturnOrder_InitialApproveTime,ErrorStrings.Export_Title_PartsShiftOrder_ApproverName,ErrorStrings.Export_Title_PartsShiftOrder_ApproverTime,ErrorStrings.Export_Title_PartsSalesOrder_SubmitterName,ErrorStrings.Export_Title_PartsSalesOrder_SubmitTime,ErrorStrings.Export_Title_Partsoutboundplan_Stoper,ErrorStrings.Export_Title_Partsoutboundplan_StopTime,
                                    ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode, ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName, ErrorStrings.Export_Title_PartsShiftOrder_OriginalWarehouseAreaCode, ErrorStrings.Export_Title_PartsShiftOrder_DestWarehouseAreaCode,ErrorStrings.Export_Title_WarehouseArea_Quantity,ErrorStrings.Export_Title_PartsPurchaseOrder_DetailRemark,ErrorStrings.Export_PartsShiftOrderDetail_DownShelfQty,ErrorStrings.Export_PartsShiftOrderDetail_DownSIHCode,ErrorStrings.Export_PartsShiftOrderDetail_UpShelfQty,ErrorStrings.Export_PartsShiftOrderDetail_SIHCode
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }
    }
}