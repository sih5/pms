﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        /// <summary>
        /// 合并导出配件调拨单主清单
        /// </summary>
        public bool ExportpartsTransferOrderWithDetail(int? id, int storageCompanyId, string partsTransferOrderCode, string originalBillCode, int? originalWarehouseId, int? destWarehouseId, int? type, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, DateTime? approveTimeBegin, DateTime? approveTimeEnd, string SAPPurchasePlanCode, string eRPSourceOrderCode, out string fileName) {
            fileName = GetExportFilePath("配件调拨单主清单_" + Guid.NewGuid() + ".xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    #region 查询SQL
                    sql.Append(string.Format(@"Select a.Code,
                                       a.Originalwarehousename,
                                       a.Destwarehousename,
                                       (select value from keyvalueitem where NAME = 'PartsTransferOrder_Status'and key=a.Status) As Status,
                                       (select value from keyvalueitem where NAME = 'PartsTransferOrder_Type'and key=a.Type) As Type,
                                       (select value from keyvalueitem where NAME = 'PartsShipping_Method'and key=a.Shippingmethod) As Shippingmethod,
                                       a.Totalamount,
                                       --a.TotalPlanAmount,
                                       --a.OriginalBillCode,
                                       --a.ERPSourceOrderCode,
                                       --a.GPMSPurOrderCode,
                                       a.Remark,
                                       a.Creatorname,
                                       a.Createtime,a.InitialApprovername,
                                       a.InitialApprovetime,
                                       a.Approvername,
                                       a.Approvetime,
                                       a.Abandonername,
                                       a.Abandontime,
                                       a.Modifiername,
                                       a.Modifytime,
                                       b.Sparepartcode,
                                       b.Sparepartname,
                                       b.Plannedamount,
                                       b.Confirmedamount,
                                       c.MInPackingAmount,
                                       --b.Price,
                                       --b.PlannPrice,
                                       --(case a.Status
                                       --when 1 then nvl(b.PlannedAmount,0)*nvl(b.PlannPrice,0)
                                       --when 2 then nvl(b.ConfirmedAmount,0)*nvl(b.PlannPrice,0)
                                       --when 99 then nvl(b.PlannedAmount,0)*nvl(b.PlannPrice,0)
                                       --end) AS DetailPlannedAmount,
                                       b.Remark 
                                       --a.SAPPurchasePlanCode,
                                       --b.POCode
                                  From Partstransferorder a
                                 Inner Join Partstransferorderdetail b
                                    On b.Partstransferorderid = a.Id
                                 Inner Join sparepart c on c.id = b.SparePartId
                                    where a.StorageCompanyId = {0}", storageCompanyId));
                    #endregion
                    var dbParameters = new List<DbParameter>();
                    if(id.HasValue) {
                        sql.Append(" and a.id = {0}id");
                        dbParameters.Add(db.CreateDbParameter("id", id.Value));
                    } else {
                        if(!string.IsNullOrEmpty(partsTransferOrderCode)) {
                            sql.Append("and a.Code like {0}partsTransferOrderCode ");
                            dbParameters.Add(db.CreateDbParameter("partsTransferOrderCode", "%" + partsTransferOrderCode + "%"));
                        }
                        if(!string.IsNullOrEmpty(originalBillCode)) {
                            sql.Append("and a.OriginalBillCode like {0}originalBillCode ");
                            dbParameters.Add(db.CreateDbParameter("originalBillCode", "%" + originalBillCode + "%"));
                        }
                        if(originalWarehouseId.HasValue) {
                            sql.Append("and a.originalWarehouseId = {0}originalWarehouseId ");
                            dbParameters.Add(db.CreateDbParameter("originalWarehouseId", originalWarehouseId.Value));
                        }
                        if(destWarehouseId.HasValue) {
                            sql.Append("and a.destWarehouseId = {0}destWarehouseId ");
                            dbParameters.Add(db.CreateDbParameter("destWarehouseId", destWarehouseId.Value));
                        }
                        if(type.HasValue) {
                            sql.Append("and a.type ={0}type ");
                            dbParameters.Add(db.CreateDbParameter("type", type.Value));
                        }
                        if(status.HasValue) {
                            sql.Append("and a.status ={0}status ");
                            dbParameters.Add(db.CreateDbParameter("status", status.Value));
                        }
                        if(createTimeBegin.HasValue) {
                            sql.Append(@" and a.createTime >=To_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(@" and a.createTime <=To_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }

                        if(approveTimeBegin.HasValue) {
                            sql.Append(@" and a.approveTime >=To_date({0}approveTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = approveTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("approveTimeBegin", tempTime.ToString("G")));
                        }
                        if(approveTimeEnd.HasValue) {
                            sql.Append(@" and a.approveTime <=To_date({0}approveTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = approveTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("approveTimeEnd", tempTime.ToString("G")));
                        }
                        if(!string.IsNullOrEmpty(SAPPurchasePlanCode)) {
                            sql.Append("and a.SAPPurchasePlanCode like {0}SAPPurchasePlanCode ");
                            dbParameters.Add(db.CreateDbParameter("SAPPurchasePlanCode", "%" + SAPPurchasePlanCode + "%"));
                        }


                    }
                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    //需要手动改中文列名
                                    //ErrorStrings.Export_Title_PartsTransferOrder_Code, ErrorStrings.Export_Title_PartsTransferOrder_OriginalWarehouseName, ErrorStrings.Export_Title_PartsTransferOrder_DestWarehouseName, ErrorStrings.Export_Title_AccountPeriod_Status, ErrorStrings.Export_Title_PartsTransferOrder_Type,ErrorStrings.Export_Title_PartsPurchaseOrder_ShippingMethod,ErrorStrings.Export_Title_PartsTransferOrder_TotalAmount,ErrorStrings.Export_Title_PartsInboundCheckBill_PlanPriceAmount,"原始订单编号",ErrorStrings.Export_Title_SupplierShippingOrder_ERPSourceOrderCode,ErrorStrings.Export_Title_PartsPurchaseOrder_GPMSPurOrderCode, ErrorStrings.Export_Title_PartsBranch_Remark, ErrorStrings.Export_Title_AccountPeriod_CreatorName, ErrorStrings.Export_Title_AccountPeriod_CreateTime, 
                                    //ErrorStrings.Export_Title_PackingPropertyApp_ApproverName, ErrorStrings.Export_Title_PackingPropertyApp_ApproveTime, ErrorStrings.Export_Title_PartsBranch_AbandonerName, ErrorStrings.Export_Title_PartsBranch_AbandonerTime, ErrorStrings.Export_Title_AccountPeriod_ModifierName, ErrorStrings.Export_Title_AccountPeriod_ModifyTime,ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode,ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName,ErrorStrings.Export_Title_PartsPurchasePlanDetail_PlanAmount,ErrorStrings.Export_Title_PartsPurchaseOrder_ConfirmedAmount,ErrorStrings.Export_Title_PartsPurReturnOrder_Price,ErrorStrings.Export_Title_PartsPlannedPrice_PlannedPrice,ErrorStrings.Export_Title_PartsInboundCheckBill_PlanAmount,ErrorStrings.Export_Title_PartsPurchaseOrder_DetailRemark,ErrorStrings.Export_Title_PartsInboundCheckBill_SAPPurchasePlanCode,ErrorStrings.Export_Title_PartsInboundPlan_POCode
                                    ErrorStrings.Export_Title_PartsTransferOrder_Code, ErrorStrings.Export_Title_PartsTransferOrder_OriginalWarehouseName, ErrorStrings.Export_Title_PartsTransferOrder_DestWarehouseName, ErrorStrings.Export_Title_AccountPeriod_Status, ErrorStrings.Export_Title_PartsTransferOrder_Type,ErrorStrings.Export_Title_PartsPurchaseOrder_ShippingMethod,ErrorStrings.Export_Title_PartsTransferOrder_TotalAmount, ErrorStrings.Export_Title_PartsBranch_Remark, ErrorStrings.Export_Title_AccountPeriod_CreatorName, ErrorStrings.Export_Title_AccountPeriod_CreateTime, 
                                    ErrorStrings.Export_Title_Dealerpartsinventorybill_ApproverName,ErrorStrings.Export_Title_Dealerpartsinventorybill_ApproverTime, ErrorStrings.Export_Title_PackingPropertyApp_ApproverName, ErrorStrings.Export_Title_PackingPropertyApp_ApproveTime, ErrorStrings.Export_Title_PartsBranch_AbandonerName, ErrorStrings.Export_Title_PartsBranch_AbandonerTime, ErrorStrings.Export_Title_AccountPeriod_ModifierName, ErrorStrings.Export_Title_AccountPeriod_ModifyTime,ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode,ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName,ErrorStrings.Export_Title_PartsPurchasePlanDetail_PlanAmount,ErrorStrings.Export_Title_PartsPurchaseOrder_ConfirmedAmount,ErrorStrings.Export_Title_PackingPropertyApp_Minsalequantity,ErrorStrings.Export_Title_PartsPurchaseOrder_DetailRemark
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();

                }
                return true;
            } catch(Exception) {
                return false;
            }
        }

        /// <summary>
        /// 校验导入配件调拨单清单
        /// </summary>
        public bool ImportpartsTransferOrderDetail(string fileName, int originalWarehouseId, int storageCompanyId, out int excelImportNum, out List<PartsTransferOrderDetailExtend> rightData, out List<PartsTransferOrderDetailExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<PartsTransferOrderDetailExtend>();
            var allList = new List<PartsTransferOrderDetailExtend>();
            var rightList = new List<PartsTransferOrderDetailExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("PartsTransferOrderDetail", out notNullableFields, out fieldLenght);
                List<object> excelColumns;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    #region 指定文件中的列对应的名称 例如 excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode, "PartCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode, "SparePartCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName, "SparePartName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_WarehouseArea_Quantity, "PlannedAmount");
                    //excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsInboundPlan_POCode, "POCode");
                    #endregion
                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();

                    #region 获取对应枚举
                    //var keyValuePairs = new[] { new KeyValuePair<string, string>("BaseDataStatus", "BaseData_Status") };
                    //tempExcelOperator.ImportHelper.LoadEnumFromDb(null,null, keyValuePairs);
                    #endregion
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var tempImportObj = new PartsTransferOrderDetailExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        #region 根据导入的字段给对应的实体属性赋值 tempImportObj.PartsSalesCategoryNameStr = row["PartsSalesCategoryName"].Trim();
                        tempImportObj.SparePartCodeStr = newRow["SparePartCode"];
                        tempImportObj.SparePartNameStr = newRow["SparePartName"];
                        tempImportObj.PlannedAmountStr = newRow["PlannedAmount"];
                        //tempImportObj.POCode = newRow["POCode"];
                        #endregion

                        var tempErrorMessage = new List<string>();
                        #region 导入的数据基本检查
                        //配件编号检查
                        var fieldIndex = notNullableFields.IndexOf("SPAREPARTCODE");
                        if(string.IsNullOrEmpty(tempImportObj.SparePartCodeStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchaseOrderDetail_Validation1);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.SparePartCodeStr) > fieldLenght["SPAREPARTCODE"])
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchaseOrderDetail_Validation2);
                        }
                        //配件名称检查
                        fieldIndex = notNullableFields.IndexOf("SPAREPARTNAME");
                        if(string.IsNullOrEmpty(tempImportObj.SparePartNameStr)) {
                            //if(fieldIndex > -1)
                                //tempErrorMessage.Add(ErrorStrings.Export_Validation_Sparepart_Name);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.SparePartNameStr) > fieldLenght["SPAREPARTNAME"])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Sparepart_NameIsLong);
                        }
                        //数量检查
                        fieldIndex = notNullableFields.IndexOf("PLANNEDAMOUNT");
                        if(string.IsNullOrEmpty(tempImportObj.PlannedAmountStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_InternalAcquisitionDetail_QuantityIsNull);
                        } else {
                            int amount;
                            if(int.TryParse(tempImportObj.PlannedAmountStr, out amount))
                                tempImportObj.PlannedAmount = amount;
                            else
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_InternalAcquisitionDetail_QuantityNumber);
                        }
                        #endregion
                        if(tempErrorMessage.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });
                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    #region 剩下的数据进行业务检查
                    //校验导入的数据配件编号重复
                    var groups = allList.GroupBy(r =>
                        r.SparePartCodeStr
                    ).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        groupItem.ErrorMsg = ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode + groupItem.SparePartCodeStr + ErrorStrings.Export_Validation_PartsBranch_Validation5;
                    }
                    //校验配件编号，在配件信息中存在
                    var sparePartCodeNeedCheck = tempRightList.Select(r => r.SparePartCodeStr.ToUpper()).Distinct().ToArray();
                    var dbSpareParts = new List<SparePartExtend>();
                    Func<string[], bool> getDbSparePartCodes = value => {
                        var dbObj = new SparePartExtend {
                            Id = Convert.ToInt32(value[0]),
                            Code = value[1],
                            Name = value[2],
                            MInPackingAmount = Convert.ToInt32(value[3])
                        };
                        dbSpareParts.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select Id,Code,Name,MInPackingAmount from SparePart where status=1 ", "Code", true, sparePartCodeNeedCheck, getDbSparePartCodes);
                    foreach(var tempRight in tempRightList) {
                        var sparePart = dbSpareParts.FirstOrDefault(v => v.Code == tempRight.SparePartCodeStr);
                        if(sparePart == null) {
                            tempRight.ErrorMsg = String.Format(ErrorStrings.Export_Validation_PartsTransferOrder_Validation1, tempRight.SparePartCodeStr);
                            continue;
                        }
                        tempRight.SparePartId = sparePart.Id;
                        tempRight.SparePartCode = sparePart.Code;
                        tempRight.SparePartName = sparePart.Name;
                        tempRight.MInPackingAmount = sparePart.MInPackingAmount;
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    //虚拟仓库库存WarehousePartsStock
                    var ids = dbSpareParts.Select(r => r.Id).Distinct().ToArray();
                    if(ids.Any()) {
                        var dbWarehousePartsStocks = new List<WarehousePartsStockExtend>();
                        Func<string[], bool> getDbIds = values => {
                            dbWarehousePartsStocks.Add(new WarehousePartsStockExtend {
                                SparePartId = Convert.ToInt32(values[0]),
                                UsableQuantity = Convert.ToInt32(values[1])
                            });
                            return false;
                        };
                        #region 查询仓库库存
                        var sql = string.Format(@"SELECT 
Project3.PartId,
Project3.C2 AS UsableQuantity
FROM ( SELECT 
  Distinct1.C1,
  Distinct1.WarehouseId,
  Distinct1.StorageCompanyId,
  Distinct1.BranchId,
  Distinct1.PartId,
  Distinct1.Code,
  Distinct1.Name,
  Distinct1.MeasureUnit,
  Distinct1.Code1,
  Distinct1.Name1,
  Distinct1.Code2,
  Distinct1.Name2,
  Distinct1.C2,
  Distinct1.PartsSalesCategoryId,
  Distinct1.CostPrice,
  Distinct1.C3,
  Distinct1.C4,
  Distinct1.RetailGuidePrice
  FROM ( SELECT DISTINCT 
    Filter3.A1 AS C1,
    Filter3.K1 AS WarehouseId,
    Filter3.K2 AS StorageCompanyId,
    Filter3.K3 AS BranchId,
    Filter3.K4 AS PartId,
    Filter3.Code1 AS Code,
    Filter3.Name1 AS Name,
    Filter3.MeasureUnit,
    Filter3.Code2 AS Code1,
    Filter3.Name2 AS Name1,
    Extent5.Code AS Code2,
    Extent5.Name AS Name2,
    (Filter3.A1 - (CASE WHEN Extent6.Id IS NULL THEN 0 ELSE Extent6.LockedQuantity END)) - (CASE WHEN Extent7.Id IS NULL THEN 0 WHEN Extent7.StockQty IS NULL THEN 0 ELSE Extent7.StockQty END) AS C2,
    Extent8.PartsSalesCategoryId,
    Extent8.CostPrice,
    1 AS C3,
    TO_CHAR(NULL) AS C4,
    Extent9.RetailGuidePrice
    FROM       (SELECT 
      GroupBy1.K1,
      GroupBy1.K2,
      GroupBy1.K3,
      GroupBy1.K4,
      GroupBy1.A1,
      Extent3.Id AS Id1,
      Extent3.Code AS Code1,
      Extent3.Name AS Name1,
      Extent3.LastSubstitute,
      Extent3.NextSubstitute,
      Extent3.ShelfLife,
      Extent3.EnglishName,
      Extent3.PinyinCode,
      Extent3.ReferenceCode,
      Extent3.ReferenceName,
      Extent3.CADCode,
      Extent3.CADName,
      Extent3.PartType,
      Extent3.Specification,
      Extent3.Feature,
      Extent3.Status AS Status1,
      Extent3.Length,
      Extent3.Width,
      Extent3.Height,
      Extent3.Volume,
      Extent3.Weight,
      Extent3.Material,
      Extent3.PackingAmount,
      Extent3.PackingSpecification,
      Extent3.PartsOutPackingCode,
      Extent3.PartsInPackingCode,
      Extent3.MeasureUnit,
      Extent3.CreatorId AS CreatorId1,
      Extent3.CreatorName AS CreatorName1,
      Extent3.CreateTime AS CreateTime1,
      Extent3.ModifierId AS ModifierId1,
      Extent3.ModifierName AS ModifierName1,
      Extent3.ModifyTime AS ModifyTime1,
      Extent3.AbandonerId,
      Extent3.AbandonerName,
      Extent3.AbandonTime,
      Extent3.RowVersion AS RowVersion1,
      Extent3.MInPackingAmount,
      Extent3.GroupABCCategory,
      Extent3.IMSCompressionNumber,
      Extent3.IMSManufacturerNumber,
      Extent3.ProductBrand,
      Extent3.SubstandardName,
      Extent3.TotalNumber,
      Extent3.Factury,
      Extent3.IsOriginal,
      Extent3.CategoryCode,
      Extent3.OverseasPartsFigure,
      Extent3.CategoryName,
      Extent3.ExchangeIdentification,
      Extent3.GoldenTaxClassifyId,
      Extent3.GoldenTaxClassifyCode,
      Extent3.GoldenTaxClassifyName,
      Extent3.ExpandLanguageNAME1,
      Extent3.ExpandLanguageCode1,
      Extent3.ExpandLanguageCode13,
      Extent3.ExpandLanguageNAME13,
      Extent3.ExpandLanguageCode12,
      Extent3.ExpandLanguageNAME12,
      Extent3.ExpandLanguageCode11,
      Extent3.ExpandLanguageNAME11,
      Extent3.ExpandLanguageCode10,
      Extent3.ExpandLanguageNAME10,
      Extent3.ExpandLanguageCode9,
      Extent3.ExpandLanguageNAME9,
      Extent3.ExpandLanguageCode8,
      Extent3.ExpandLanguageNAME8,
      Extent3.ExpandLanguageCode7,
      Extent3.ExpandLanguageNAME7,
      Extent3.ExpandLanguageCode6,
      Extent3.ExpandLanguageNAME6,
      Extent3.ExpandLanguageCode5,
      Extent3.ExpandLanguageNAME5,
      Extent3.ExpandLanguageCode4,
      Extent3.ExpandLanguageNAME4,
      Extent3.ExpandLanguageCode3,
      Extent3.ExpandLanguageNAME3,
      Extent3.ExpandLanguageCode2,
      Extent3.ExpandLanguageNAME2,
      Extent3.StandardCode,
      Extent3.StandardName,
      Extent3.IsNotWarrantyTransfer,
      Extent3.OMSparePartMark,
      Extent3.DeclareElement,
      Extent3.Path,
      Extent4.Id AS Id2,
      Extent4.Code AS Code2,
      Extent4.Name AS Name2,
      Extent4.Abbreviation,
      Extent4.MainBrand,
      Extent4.Status AS Status2,
      Extent4.CreatorId AS CreatorId2,
      Extent4.CreatorName AS CreatorName2,
      Extent4.CreateTime AS CreateTime2,
      Extent4.ModifierId AS ModifierId2,
      Extent4.ModifierName AS ModifierName2,
      Extent4.ModifyTime AS ModifyTime2,
      Extent4.RowVersion AS RowVersion2
      FROM    (SELECT Extent1.WarehouseId AS K1, Extent1.StorageCompanyId AS K2, Extent1.BranchId AS K3, Extent1.PartId AS K4, Sum(Extent1.Quantity) AS A1
        FROM PartsStock Extent1
        WHERE (((Extent1.StorageCompanyId = {0}) AND ( EXISTS (SELECT 
          1 AS C1
          FROM WarehouseAreaCategory Extent2
          WHERE (Extent2.Id = Extent1.WarehouseAreaCategoryId) AND (Extent2.Category = 1)
        ))) AND (Extent1.WarehouseId = {1})) AND (Extent1.PartId in ({2}))
        GROUP BY Extent1.WarehouseId, Extent1.StorageCompanyId, Extent1.BranchId, Extent1.PartId ) GroupBy1
      INNER JOIN SparePart Extent3 ON Extent3.Id = GroupBy1.K4
      INNER JOIN Branch Extent4 ON Extent4.Id = GroupBy1.K3
      WHERE (Extent3.Status <> 99) AND (Extent4.Status <> 99) ) Filter3
    INNER JOIN Warehouse Extent5 ON Extent5.Id = Filter3.K1
    LEFT OUTER JOIN PartsLockedStock Extent6 ON ((Filter3.K4 = Extent6.PartId) AND (Filter3.K2 = Extent6.StorageCompanyId)) AND (Filter3.K1 = Extent6.WarehouseId)
    LEFT OUTER JOIN BottomStock Extent7 ON (Extent7.Status = 1) AND (((Filter3.K4 = Extent7.SparePartId) AND (Filter3.K2 = Extent7.CompanyID)) AND (Filter3.K1 = Extent7.WarehouseID))
    INNER JOIN EnterprisePartsCost Extent8 ON (Filter3.K4 = Extent8.SparePartId) AND (Extent8.OwnerCompanyId = Filter3.K2)
    LEFT OUTER JOIN PartsRetailGuidePrice Extent9 ON (Filter3.K4 = Extent9.SparePartId) AND (Extent8.PartsSalesCategoryId = Extent9.PartsSalesCategoryId)
    WHERE Extent5.Status <> 99 
  )  Distinct1
)  Project3 where 1 = 1 
 ", storageCompanyId, originalWarehouseId, string.Join(",", ids));
                        #endregion
                        db.QueryDataWithInOperator(sql, "1", false, new[] { "1" }, getDbIds);
                        var dbPartssalesprices = new List<WarehousePartsStockExtend>();
                        Func<string[], bool> getPartssalesprices = values => {
                            dbPartssalesprices.Add(new WarehousePartsStockExtend {
                                SparePartId = Convert.ToInt32(values[0]),
                                PartsSalesPrice = Convert.ToDecimal(values[1]),
                                PartsPlannedPrice = Convert.ToDecimal(values[2]),
                                PartsSalesCategoryId = Convert.ToInt32(values[3]),
                            });
                            return false;
                        };
                        //PartsSalesPrice改为取配件成本价
                        var sql1 = string.Format(@"Select a.SparePartId, a.CostPrice,a.CostPrice,a.Partssalescategoryid
  From EnterprisePartsCost a where a.OwnerCompanyId = {0} And a.Sparepartid In ({1}) ", storageCompanyId, string.Join(",", ids));
                        db.QueryDataWithInOperator(sql1, "1", false, new[] { "1" }, getPartssalesprices);
                        foreach(var tempRight in tempRightList) {
                            var warehousePartsStock = dbWarehousePartsStocks.FirstOrDefault(v => v.SparePartId == tempRight.SparePartId);
                            var salesprice = dbPartssalesprices.FirstOrDefault(r => r.SparePartId == tempRight.SparePartId);
                            tempRight.UsableQuantity = warehousePartsStock != null ? warehousePartsStock.UsableQuantity : 0;
                            tempRight.Price = salesprice != null ? salesprice.PartsSalesPrice : 0;
                            tempRight.PlanedPrice = salesprice != null ? salesprice.PartsPlannedPrice : 0;
                            tempRight.PartsSalesCategoryId = salesprice != null ? salesprice.PartsSalesCategoryId : 0;
                        }
                    }
                    #endregion
                    //获取所有不合格数据
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    //获取所有合格数据
                    rightList = allList.Except(errorList).ToList();
                    #region 将合格数据的值填上
                    //foreach(var rightItem in rightList) {
                    //    rightItem.SparePartCode = rightItem.SparePartCodeStr;
                    //}
                    #endregion
                }
                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempObj = list[index - 1];
                            var values = new object[] {
                                #region 设置错误信息导出的列的值
                                tempObj.SparePartCodeStr,tempObj.SparePartNameStr,tempObj.PlannedAmountStr,tempObj.POCode, tempObj.ErrorMsg
                                #endregion                     
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }
    }
}
