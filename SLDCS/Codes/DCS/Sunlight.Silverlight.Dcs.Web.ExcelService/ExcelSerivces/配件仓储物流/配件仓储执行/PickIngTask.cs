﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService
{
    public partial class ExcelService
    {
        public bool ExportPickingTask(int[] ids, string code, int? warehouseId, int? partsSalesCategoryId, string status, string sourceCode, string partsOutboundPlanCode, string orderTypeName, string counterpartCompanyName, DateTime? bCreateTime, DateTime? eCreateTime, DateTime? bPickingfinishtime, DateTime? ePickingfinishtime,string sparePartCode,string sparePartName,int?orderTypeId, out string fileName)
        {
            fileName = GetExportFilePath("导出拣货任务单.xlsx");
            try
            {
                if (!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var userInfo = Utils.GetCurrentUserInfo();
                var db = DbHelper.GetDbHelp(ConnectionString);
                using (var conn = db.CreateDbConnection())
                {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.Append(@"select (select value from keyvalueitem where name='PickingTaskStatus' and key=p.Status),
                                 p.code,p.warehousename,p.counterpartcompanyname,p.OrderTypeName,p.creatorname,p.createtime,p.modifiername,p.modifytime,p.pickingfinishtime,
                                p.warehousecode, p.counterpartcompanycode,p.remark,(select name from partssalescategory where id=p.partssalescategoryid),
                                d.PartsOutboundPlanCode,d.SourceCode,d.SparePartCode,d.SparePartName,  " +
                               "d.MeasureUnit,top.code,d.WarehouseAreaCode,d.PlanQty,d.PickingQty,(select value from keyvalueitem where name='ShortPickingReason' and key=d.ShortPickingReason)" +
                              " from Pickingtask p left join PickingTaskDetail d on p.id=d.PickingtaskId " +
                    "left join WarehouseArea w on w.id = d.warehouseareaid  left join WarehouseAreaManager m on m.warehouseareaid = w.toplevelwarehouseareaid join warehousearea wh on d.warehouseareaid =wh.id  left join WarehouseArea top on wh.toplevelwarehouseareaid =top.id where  p.BranchId=" + userInfo.EnterpriseId + " and m.ManagerId =" + userInfo.Id);
                    var dbParameters = new List<DbParameter>();
                    if (ids != null && ids.Length > 0)
                    {
                        sql.Append(" and p.id in (");
                        for (var i = 0; i < ids.Length; i++)
                        {
                            if (ids.Length == i + 1)
                            {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                            else
                            {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                        }
                        sql.Append(")");
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(code))
                        {
                            sql.Append("and LOWER(p.Code) like {0}code ");
                            dbParameters.Add(db.CreateDbParameter("code", "%" + code.ToLower() + "%"));
                        }
                      
                        if (warehouseId.HasValue)
                        {
                            sql.Append(@" and p.warehouseId = {0}status ");
                            dbParameters.Add(db.CreateDbParameter("status", warehouseId.Value));
                        }
                        if (partsSalesCategoryId.HasValue)
                        {
                            sql.Append(@" and p.partsSalesCategoryId = {0}partsSalesCategoryId ");
                            dbParameters.Add(db.CreateDbParameter("partsSalesCategoryId", partsSalesCategoryId.Value));
                        }
                        if (!string.IsNullOrEmpty(status))
                        {
                           sql.Append(string.Format("and p.status in ({0}) ", status)); 
                        }
                        if(!string.IsNullOrEmpty(sourceCode)) {
                            sql.Append(" and  LOWER(d.sourceCode) like {0}sourceCode ");
                            dbParameters.Add(db.CreateDbParameter("sourceCode", "%" + sourceCode.ToLower() + "%"));
                        }
                        if(!string.IsNullOrEmpty(partsOutboundPlanCode)) {
                            sql.Append(" and   LOWER(d.partsOutboundPlanCode) like {0}partsOutboundPlanCode ");
                            dbParameters.Add(db.CreateDbParameter("partsOutboundPlanCode", "%" + partsOutboundPlanCode.ToLower() + "%"));
                        }

                        if (!string.IsNullOrEmpty(orderTypeName))
                        {
                            sql.Append(" and LOWER(p.orderTypeName) like {0}orderTypeName");
                            dbParameters.Add(db.CreateDbParameter("orderTypeName", "%" + orderTypeName.ToLower() + "%"));
                        }
                        if (!string.IsNullOrEmpty(counterpartCompanyName))
                        {
                            sql.Append(" and LOWER(p.counterpartCompanyName) like {0}counterpartCompanyName");
                            dbParameters.Add(db.CreateDbParameter("counterpartCompanyName", "%" + counterpartCompanyName.ToLower() + "%"));
                        }
                        if (bPickingfinishtime.HasValue)
                        {
                            sql.Append(@" and p.Pickingfinishtime >=To_date({0}bPickingfinishtime,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = bPickingfinishtime.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("bPickingfinishtime", tempTime.ToString("G")));
                        }
                        if (ePickingfinishtime.HasValue)
                        {
                            sql.Append(@" and p.Pickingfinishtime <=To_date({0}ePickingfinishtime,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = ePickingfinishtime.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("ePickingfinishtime", tempTime.ToString("G")));
                        }
                        if (bCreateTime.HasValue)
                        {
                            sql.Append(@" and p.createTime >=To_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = bCreateTime.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if (eCreateTime.HasValue)
                        {
                            sql.Append(@" and p.createTime <=To_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = eCreateTime.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                        if (!string.IsNullOrEmpty(sparePartCode))
                        {
                            sql.Append(" and d.sparePartCode=  {0}sparePartCode");
                            dbParameters.Add(db.CreateDbParameter("sparePartCode", sparePartCode));
                        }
                        if (!string.IsNullOrEmpty(sparePartName))
                        {
                            sql.Append(" and d.sparePartName like {0}sparePartName");
                            dbParameters.Add(db.CreateDbParameter("sparePartName", "%" + sparePartName + "%"));
                        }
                        if (orderTypeId.HasValue)
                        {
                            sql.Append(@" and p.OrderTypeId = {0}orderTypeId ");
                            dbParameters.Add(db.CreateDbParameter("orderTypeId", orderTypeId.Value));
                        }
                    }
                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using (var excelExport = new ExcelExport(fileName))
                    {
                        excelExport.ExportByRow(index =>
                        {
                            if (index == 0)
                            {
                                return new object[] {
                                    ErrorStrings.Export_Title_AccountPeriod_Status,ErrorStrings.Export_Title_PickingTask_Code, ErrorStrings.Export_Title_Company_WarehouseName, ErrorStrings.Export_Title_BoxUpTask_CounterpartCompanyName,ErrorStrings.Export_Title_PartsPurchaseOrder_OrderType,ErrorStrings.Export_Title_AccountPeriod_CreatorName, ErrorStrings.Export_Title_AccountPeriod_CreateTime,ErrorStrings.Export_Title_AccountPeriod_ModifierName,ErrorStrings.Export_Title_AccountPeriod_ModifyTime,ErrorStrings.Export_Title_PickingTask_PickingFinishTime,ErrorStrings.Export_Title_Company_WarehouseCode,  ErrorStrings.Export_Title_BoxUpTask_CounterpartCompanyCode,  ErrorStrings.Export_Title_PartsBranch_Remark,ErrorStrings.Export_Title_Partssalescategory_Name, ErrorStrings.Export_Title_PartsOutboundBill_PartsOutboundPlanCode,ErrorStrings.Export_Title_PartsOuterPurchaseChange_SourceCode,ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode,ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName,ErrorStrings.Export_Title_PackingPropertyApp_MeasureUnit,"库区",ErrorStrings.Export_Title_PartsTransferOrder_WarehouseArea,ErrorStrings.Export_Title_PartsPurchasePlanDetail_PlanAmount,ErrorStrings.Export_Title_PartsTransferOrder_PickingQty,ErrorStrings.Export_Title_PartsTransferOrder_ShortPickingReason
                                };
                            }
                            if (reader.Read())
                            {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if (num != reader.FieldCount)
                                {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool ExportPickingTaskAll(int[] ids, string code, int? warehouseId, int? partsSalesCategoryId, int? status, string sourceCode, string partsOutboundPlanCode, string orderTypeName, string counterpartCompanyName, DateTime? bCreateTime, DateTime? eCreateTime, DateTime? bPickingfinishtime, DateTime? ePickingfinishtime, out string fileName)
        {
            fileName = GetExportFilePath("导出拣货任务单.xlsx");
            try
            {
                if (!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);
                var userInfo = Utils.GetCurrentUserInfo();
                using (var conn = db.CreateDbConnection())
                {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.Append(@"select p.ResponsiblePersonName,p.code, (select name from partssalescategory where id=p.partssalescategoryid),p.warehousecode,p.warehousename,
                              (select value from keyvalueitem where name='PickingTaskStatus' and key=p.Status),p.counterpartcompanycode,p.counterpartcompanyname,p.ResponsiblePersonName,
                              p.creatorname,p.createtime,p.modifiername,p.modifytime,p.pickingfinishtime,d.PartsOutboundPlanCode,d.SourceCode,d.SparePartCode,d.SparePartName,  " +
                               "d.MeasureUnit,d.WarehouseAreaCode,d.PlanQty,d.PickingQty,(select value from keyvalueitem where name='ShortPickingReason' and key=d.ShortPickingReason)" +
                              " from Pickingtask p left join PickingTaskDetail d on p.id=d.PickingtaskId where p.BranchId=" + userInfo.EnterpriseId);
                    var dbParameters = new List<DbParameter>();
                    if (ids != null && ids.Length > 0)
                    {
                        sql.Append(" and p.id in (");
                        for (var i = 0; i < ids.Length; i++)
                        {
                            if (ids.Length == i + 1)
                            {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                            else
                            {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                        }
                        sql.Append(")");
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(code))
                        {
                            sql.Append("and LOWER(p.Code) like {0}code ");
                            dbParameters.Add(db.CreateDbParameter("code", "%" + code.ToLower() + "%"));
                        }

                        if (warehouseId.HasValue)
                        {
                            sql.Append(@" and p.warehouseId = {0}status ");
                            dbParameters.Add(db.CreateDbParameter("status", warehouseId.Value));
                        }
                        if (partsSalesCategoryId.HasValue)
                        {
                            sql.Append(@" and p.partsSalesCategoryId = {0}partsSalesCategoryId ");
                            dbParameters.Add(db.CreateDbParameter("partsSalesCategoryId", status.Value));
                        }
                        if (status.HasValue)
                        {
                            sql.Append(@" and p.Status = {0}status ");
                            dbParameters.Add(db.CreateDbParameter("status", status.Value));
                        }
                        if (!string.IsNullOrEmpty(sourceCode))
                        {
                            sql.Append(" and  LOWER(d.sourceCode) like {0}sourceCode ");
                            dbParameters.Add(db.CreateDbParameter("sourceCode", "%" + sourceCode.ToLower() + "%"));
                        }
                        if (!string.IsNullOrEmpty(partsOutboundPlanCode))
                        {
                            sql.Append(" and   LOWER(d.partsOutboundPlanCode) like {0}partsOutboundPlanCode ");
                            dbParameters.Add(db.CreateDbParameter("partsOutboundPlanCode", "%" + partsOutboundPlanCode.ToLower() + "%"));
                        }


                        if (!string.IsNullOrEmpty(orderTypeName))
                        {
                            sql.Append(" and LOWER(p.orderTypeName) like {0}orderTypeName");
                            dbParameters.Add(db.CreateDbParameter("orderTypeName", "%" + partsOutboundPlanCode.ToLower() + "%"));
                        }
                        if (!string.IsNullOrEmpty(counterpartCompanyName))
                        {
                            sql.Append(" and LOWER(p.counterpartCompanyName) like {0}counterpartCompanyName");
                            dbParameters.Add(db.CreateDbParameter("counterpartCompanyName", "%" + partsOutboundPlanCode.ToLower() + "%"));
                        }
                        if (bPickingfinishtime.HasValue)
                        {
                            sql.Append(@" and p.Pickingfinishtime >=To_date({0}Pickingfinishtime,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = bPickingfinishtime.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("Pickingfinishtime", tempTime.ToString("G")));
                        }
                        if (ePickingfinishtime.HasValue)
                        {
                            sql.Append(@" and p.Pickingfinishtime <=To_date({0}Pickingfinishtime,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = ePickingfinishtime.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("Pickingfinishtime", tempTime.ToString("G")));
                        }
                        if (bCreateTime.HasValue)
                        {
                            sql.Append(@" and p.createTime >=To_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = bCreateTime.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if (eCreateTime.HasValue)
                        {
                            sql.Append(@" and p.createTime <=To_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = eCreateTime.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }

                    }
                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using (var excelExport = new ExcelExport(fileName))
                    {
                        excelExport.ExportByRow(index =>
                        {
                            if (index == 0)
                            {
                                return new object[] {

                                    "责任人",ErrorStrings.Export_Title_PickingTask_Code, ErrorStrings.Export_Title_Partssalescategory_Name, ErrorStrings.Export_Title_Company_WarehouseCode, ErrorStrings.Export_Title_Company_WarehouseName, ErrorStrings.Export_Title_AccountPeriod_Status, ErrorStrings.Export_Title_BoxUpTask_CounterpartCompanyCode, ErrorStrings.Export_Title_BoxUpTask_CounterpartCompanyName, "责任人", ErrorStrings.Export_Title_AccountPeriod_CreatorName, ErrorStrings.Export_Title_AccountPeriod_CreateTime,ErrorStrings.Export_Title_AccountPeriod_ModifierName,ErrorStrings.Export_Title_AccountPeriod_ModifyTime,ErrorStrings.Export_Title_PickingTask_PickingFinishTime,ErrorStrings.Export_Title_PartsOutboundBill_PartsOutboundPlanCode,ErrorStrings.Export_Title_PartsOuterPurchaseChange_SourceCode,ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode,ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName,ErrorStrings.Export_Title_PackingPropertyApp_MeasureUnit,ErrorStrings.Export_Title_PartsTransferOrder_WarehouseArea,ErrorStrings.Export_Title_PartsPurchasePlanDetail_PlanAmount,ErrorStrings.Export_Title_PartsTransferOrder_PickingQty,ErrorStrings.Export_Title_PartsTransferOrder_ShortPickingReason
                                };
                            }
                            if (reader.Read())
                            {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if (num != reader.FieldCount)
                                {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
