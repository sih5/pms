﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Globalization;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        /// <summary>
        /// 供应商发运管理-供应商  合并导出
        /// </summary>
        public bool ExportSupplierShippingOrderForSupplier(int[] ids, /*int? branchId,*/ string code,/* int? partsSalesCategoryId,*/ int? receivingWarehouseId, string partsPurchaseOrderCode, /*string ERPSourceOrderCode,*/int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName)
        {
            fileName = GetExportFilePath("供应商发运主清单.xlsx");

            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);
                var userInfo = Utils.GetCurrentUserInfo();
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.Append(string.Format(@"select  (select value from keyvalueitem where key =  a.status and name = 'SupplierShippingOrder_Status') as status,
                                                    --a.branchname,
                                                    --a.PartsSalesCategoryName,
                                                    a.code,
                                                    a.ReceivingWarehouseName,
                                                    a.PartsSupplierCode,
                                                    a.PartsSupplierName,
                                                    a.PartsPurchaseOrderCode,
                                                    a.PlanSource,
                                                    a.ReceivingAddress,
                                                    a.ReceivingCompanyName,
                                                    case when a.IfDirectProvision = 1 then cast ('是' as varchar2(100)) else cast ('否' as varchar2(100)) end as IsDirectProvision,
                                                    a.OriginalRequirementBillCode,
                                                    case when a.DirectProvisionFinished = 1 then cast ('是' as varchar2(100)) else cast ('否' as varchar2(100)) end as IsDirectProvisionFinished,
                                                    a.DeliveryBillNumber,
                                                    a.RequestedDeliveryTime,
                                                    a.PlanDeliveryTime,
                                                    a.ShippingDate,
                                                    a.ArrivalDate,
                                                    (select value from keyvalueitem where key =  a.ShippingMethod and name = 'PartsShipping_Method') as ShippingMethod,
                                                    a.LogisticCompany,
                                                    a.Driver,
                                                    a.Phone,
                                                    a.VehicleLicensePlate,
                                                    a.Remark,
                                                    --a.ERPSourceOrderCode,
                                                    a.creatorname,
                                                    a.createtime,
                                                    b.SupplierPartCode,
                                                    --b.SparePartCode,
                                                    b.SparePartName,
                                                    b.MeasureUnit,
                                                    b.Quantity,
                                                    b.Remark
                                                from SupplierShippingOrder a
                                                inner join suppliershippingdetail b
                                                on b.SupplierShippingOrderId = a.id
                                                where 1=1 "));
                    var dbParameters = new List<DbParameter>();
                    sql.Append("and a.PartsSupplierId =" + Utils.GetCurrentUserInfo().EnterpriseId);
                    if (ids != null && ids.Length > 0)
                    {
                        sql.Append(" and a.id in (");
                        for (var i = 0; i < ids.Length; i++)
                        {
                            if (ids.Length == i + 1)
                            {
                                sql.Append("{0}" + "id" + ids[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                            else
                            {
                                sql.Append("{0}" + "id" + ids[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                        }
                        sql.Append(")");
                    }
                    else
                    {
                        //if (branchId.HasValue)
                        //{
                        //    sql.Append("and a.branchId = {0}branchId ");
                        //    dbParameters.Add(db.CreateDbParameter("branchId", branchId.Value));
                        //}
                        if (!string.IsNullOrEmpty(code))
                        {
                            sql.Append("and a.Code like {0}code ");
                            dbParameters.Add(db.CreateDbParameter("code", "%" + code + "%"));
                        }
                        if (receivingWarehouseId.HasValue)
                        {
                            sql.Append("and a.receivingWarehouseId = {0}receivingWarehouseId ");
                            dbParameters.Add(db.CreateDbParameter("receivingWarehouseId", receivingWarehouseId));
                        }
                        //if (partsSalesCategoryId.HasValue)
                        //{
                        //    sql.Append("and a.partsSalesCategoryId = {0}partsSalesCategoryId ");
                        //    dbParameters.Add(db.CreateDbParameter("partsSalesCategoryId", partsSalesCategoryId.Value));
                        //}
                        if (status.HasValue)
                        {
                            sql.Append("and a.status = {0}status ");
                            dbParameters.Add(db.CreateDbParameter("status", status.Value));
                        }
                        if (!string.IsNullOrEmpty(partsPurchaseOrderCode))
                        {
                            sql.Append("and a.partsPurchaseOrderCode like {0}partsPurchaseOrderCode ");
                            dbParameters.Add(db.CreateDbParameter("partsPurchaseOrderCode", "%" + partsPurchaseOrderCode + "%"));
                        }
                        //if (!string.IsNullOrEmpty(ERPSourceOrderCode))
                        //{
                        //    sql.Append("and a.ERPSourceOrderCode like {0}ERPSourceOrderCode ");
                        //    dbParameters.Add(db.CreateDbParameter("ERPSourceOrderCode", "%" + ERPSourceOrderCode + "%"));
                        //}
                        if (createTimeBegin.HasValue)
                        {
                            sql.Append(@" and a.createTime >=To_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if (createTimeEnd.HasValue)
                        {
                            sql.Append(@" and a.createTime <=To_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                    }

                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                   ErrorStrings.Export_Title_AccountPeriod_Status,/*ErrorStrings.Export_Title_Branch_BranchName, ErrorStrings.Export_Title_Partssalescategory_Name,*/ ErrorStrings.Export_Title_SupplierShippingOrder_Code,"收货仓库", ErrorStrings.Export_Title_FactoryPurchacePrice_SupplierCode,ErrorStrings.Export_Title_FactoryPurchacePrice_SupplierName,ErrorStrings.Export_Title_PartsPurchaseOrder_Code, ErrorStrings.Export_Title_PartsPurchaseOrder_PlanSource, ErrorStrings.Export_Title_PartsPurchaseOrder_ReceiveAddress, ErrorStrings.Export_Title_PartsPurchaseOrder_ReceivingCompanyName, ErrorStrings.Export_Title_PartsPurchaseOrder_IfDirectProvision, ErrorStrings.Export_Title_PartsInboundCheckBill_OriginalRequirementBillCode,
                                   ErrorStrings.Export_Title_SupplierShippingOrder_DirectProvisionFinishe,ErrorStrings.Export_Title_SupplierShippingOrder_DdeliverybillNumber, ErrorStrings.Export_Title_PartsPurchaseOrder_RequestedDeliveryTime,ErrorStrings.Export_Title_SupplierShippingOrder_PlandeliveryTime,ErrorStrings.Export_Title_SupplierShippingOrder_ShippingDates,ErrorStrings.Export_Title_SupplierShippingOrder_ArriveDate,ErrorStrings.Export_Title_PartsPurchaseOrder_ShippingMethod,ErrorStrings.Export_Title_SupplierShippingOrder_LogisticCompany,ErrorStrings.Export_Title_SupplierShippingOrder_Driver,ErrorStrings.Export_Title_SupplierShippingOrder_DriverPhone, ErrorStrings.Export_Title_SupplierShippingOrder_VehicleLicensePlate,ErrorStrings.Export_Title_PartsBranch_Remark,/*ErrorStrings.Export_Title_SupplierShippingOrder_ERPSourceOrderCode,*/ErrorStrings.Export_Title_AccountPeriod_CreatorName,ErrorStrings.Export_Title_AccountPeriod_CreateTime,
                                    ErrorStrings.Export_Title_SupplierShippingOrder_SupplierpartCode,/*ErrorStrings.Export_Title_PartsBranch_Code,*/ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName,ErrorStrings.Export_Title_PackingPropertyApp_MeasureUnit,ErrorStrings.Export_Title_PartsPurchaseOrder_ShippingAmount,ErrorStrings.Export_Title_PartsBranch_Remark};
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();

                }
                return true;
            } catch(Exception) {
                return false;
            }
        }

    }
}
