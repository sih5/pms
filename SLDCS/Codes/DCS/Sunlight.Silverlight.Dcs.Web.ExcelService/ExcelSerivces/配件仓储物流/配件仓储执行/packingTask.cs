﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {

    public partial class ExcelService 
    {
        public bool ExportPackingTask(int[] ids, string code, string partsInboundPlanCode, string partsInboundCheckBillCode, string sourceCode, int? status, DateTime? expectedPlaceDateBegin, DateTime? expectedPlaceDateEnd, int? warehouseId, DateTime? createTimeBegin, DateTime? createTimeEnd,string sparePartCode, string sparePartName,int? inboundType,string modifierName, bool? hasDifference, out string fileName)
        {
            fileName = GetExportFilePath("导出配件包装单.xlsx");
            try
            {
                if (!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);
                var userInfo = Utils.GetCurrentUserInfo();
                using (var conn = db.CreateDbConnection())
                {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.Append(@"select * from (select a.Code, a.PartsInboundPlanCode,a.PartsInboundCheckBillCode,(select value from keyvalueitem kv where kv.name='Parts_InboundType' and kv.key=pp.inboundtype),a.SparePartCode,a.SparePartName,(select value from keyvalueitem where name='TraceProperty' and key=sp.TraceProperty),a.WarehouseCode,a.WarehouseName,a.PlanQty,a.PackingQty,a.BatchNumber,a.SourceCode,
                        (select value from keyvalueitem where name='PackingTaskStatus' and key=a.Status)," +
                        "a.CounterpartCompanyCode,a.CounterpartCompanyName, (select PackingMaterial from PartsBranchPackingProp where SparePartId=a.sparepartid and PackingType=(select max(PackingType) from PartsBranchPackingProp  where SparePartId=a.sparepartid)  ) as PackingMaterial," +      
                        "(select MAX(ReferenceCode) from sparepart where code = (select PackingMaterial from PartsBranchPackingProp where SparePartId=a.sparepartid and PackingType=(select max(PackingType) from PartsBranchPackingProp  where SparePartId=a.sparepartid)  )) as ReferenceCode, "+
                        "ceil(PlanQty /(select PackingCoefficient from PartsBranchPackingProp where SparePartId = a.sparepartid and PackingType =(select max(PackingType) from PartsBranchPackingProp where SparePartId = a.sparepartid))) as PackNum ," +
                        "yy.WarehouseAreaCode,a.CreatorName,a.CreateTime,(select max(t.PhyPackingMaterial) from PackingTaskDetail t where a.id = t.PackingTaskId),(select sum(PhyQty) from PackingTaskDetail p where a.id = p.PackingTaskId),a.ModifierName,a.ModifyTime, case (select count(1) from PartsDifferenceBackBill d inner join PartsDifferenceBackBillDtl dd on d.Id=dd.PartsDifferenceBackBillId where d.type =2 and d.status<>99 and rownum=1 and dd.TaskId=a.Id) when 1 then cast('是' as varchar2(100)) else cast('否' as varchar2(100)) end as HasDifference   from PackingTask a  join SparePart sp on a.SparePartId=sp.id join  PartsInboundPlan pp  on   a.partsinboundplanid =pp.id " 
                    +@"left  join (select WarehouseArea.code as WarehouseAreaCode,
                           Warehouse.id as warehouseid,
                           partsstock.partid as partid,
                           row_number() over(partition by Warehouse.code, partsstock.partid order by WarehouseArea.Id) my_rank
                      from WarehouseArea
                     inner join Warehouse
                        on WarehouseArea.WarehouseId = Warehouse.Id
                       and warehouse.storagecompanytype = 1
                     inner join WarehouseAreaCategory
                        on WarehouseArea.AreaCategoryId =
                           WarehouseAreaCategory.Id
                       AND WarehouseAreaCategory.Category = 1
                        inner join partsstock
                        on partsstock.warehouseareaid = warehousearea.id) yy
                          on yy.warehouseid =  a.warehouseid
                     and yy.partid = a.sparepartid and my_rank = 1where 1=1 ");
                    var dbParameters = new List<DbParameter>();
                    if (ids != null && ids.Length > 0)
                    {
                        sql.Append(" and a.id in (");
                        for (var i = 0; i < ids.Length; i++)
                        {
                            if (ids.Length == i + 1)
                            {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                            else
                            {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                        }
                        sql.Append("))tt");
                    }
                    else
                    {
                        if(hasDifference == true) {
                            sql.Append(" and exists (select 1 from PartsDifferenceBackBill where type=2 and status <>99 and SourceCode=a.code) ");
                        }
                        if(hasDifference == false) {
                            sql.Append(" and not exists (select 1 from PartsDifferenceBackBill where type=2 and status <>99 and SourceCode=a.code) ");
                        }
                        if (!string.IsNullOrEmpty(code))
                        {
                            sql.Append("and LOWER(a.Code) like {0}code ");
                            dbParameters.Add(db.CreateDbParameter("code", "%" + code.ToLower() + "%"));
                        }
                        if(!string.IsNullOrEmpty(sparePartCode)) {
                            sql.Append("and LOWER(a.sparePartCode) like {0}sparePartCode ");
                            dbParameters.Add(db.CreateDbParameter("sparePartCode", "%" + sparePartCode.ToLower() + "%"));
                        }
                        if(!string.IsNullOrEmpty(sparePartName)) {
                            sql.Append("and LOWER(a.sparePartName) like {0}sparePartName ");
                            dbParameters.Add(db.CreateDbParameter("sparePartName", "%" + sparePartName.ToLower() + "%"));
                        }
                        if (!string.IsNullOrEmpty(partsInboundPlanCode))
                        {
                            sql.Append(" and LOWER(a.PartsInboundPlanCode) like {0}partsInboundPlanCode");
                            dbParameters.Add(db.CreateDbParameter("partsInboundPlanCode", "%" + partsInboundPlanCode.ToLower() + "%"));
                        }
                        if (!string.IsNullOrEmpty(partsInboundCheckBillCode))
                        {
                            sql.Append(" and LOWER(a.PartsInboundCheckBillCode) like {0}partsInboundCheckBillCode");
                            dbParameters.Add(db.CreateDbParameter("partsInboundCheckBillCode", "%" + partsInboundCheckBillCode.ToLower() + "%"));
                        }
                        if(!string.IsNullOrEmpty(modifierName)) {
                            sql.Append(" and LOWER(a.modifierName) like {0}modifierName");
                            dbParameters.Add(db.CreateDbParameter("modifierName", "%" + modifierName.ToLower() + "%"));
                       }
                        if (status.HasValue)
                        {
                            sql.Append(@" and a.Status = {0}status ");
                            dbParameters.Add(db.CreateDbParameter("status", status.Value));
                        }
                        if(inboundType.HasValue) {
                            sql.Append(@" and pp.InboundType = {0}InboundType ");
                            dbParameters.Add(db.CreateDbParameter("InboundType", inboundType.Value));
                        }
                        if (expectedPlaceDateBegin.HasValue)
                        {
                            sql.Append(@" and a.ModifyTime >=To_date({0}expectedPlaceDateBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = expectedPlaceDateBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("expectedPlaceDateBegin", tempTime.ToString("G")));
                        }
                        if (expectedPlaceDateEnd.HasValue)
                        {
                            sql.Append(@" and a.ModifyTime <=To_date({0}expectedPlaceDateEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = expectedPlaceDateEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("expectedPlaceDateEnd", tempTime.ToString("G")));
                        }
                        if (createTimeBegin.HasValue)
                        {
                            sql.Append(@" and a.createTime >=To_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if (createTimeEnd.HasValue)
                        {
                            sql.Append(@" and a.createTime <=To_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                        if (warehouseId.HasValue)
                        {
                            sql.Append(@" and a.warehouseId = {0}warehouseId ");
                            dbParameters.Add(db.CreateDbParameter("warehouseId", warehouseId.Value));
                        }
                        sql.Append(" )tt ");
                        if(!string.IsNullOrEmpty(sourceCode)) {
                            sql.Append(" where  tt.ReferenceCode  like {0}sourceCode");
                            dbParameters.Add(db.CreateDbParameter("sourceCode", "%" + sourceCode + "%"));
                        }                  
                    }
                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using (var excelExport = new ExcelExport(fileName))
                    {
                        excelExport.ExportByRow(index =>
                        {
                            if (index == 0)
                            {
                                return new object[] {

                                    ErrorStrings.Export_Title_PackingTask_Code, ErrorStrings.Export_Title_PackingTask_PartsInboundPlanCode, ErrorStrings.Export_Title_PackingTask_PartsInboundCheckBillCode,ErrorStrings.Export_Title_PackingTask_InboundType, ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode, ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName,"追溯属性", ErrorStrings.Export_Title_Company_WarehouseCode, ErrorStrings.Export_Title_Company_WarehouseName, ErrorStrings.Export_Title_PartsPurchasePlanDetail_PlanAmount,
                                    ErrorStrings.Export_Title_PackingTask_PackingQty,ErrorStrings.Export_Title_PackingTask_BatchNumber, ErrorStrings.Export_Title_PartsOuterPurchaseChange_SourceCode,ErrorStrings.Export_Title_AccountPeriod_Status, ErrorStrings.Export_Title_BoxUpTask_CounterpartCompanyCode, ErrorStrings.Export_Title_BoxUpTask_CounterpartCompanyName,
                                    ErrorStrings.Export_Title_PackingTask_RecomPackingMaterial,ErrorStrings.Export_Title_PackingTask_RecomPackingReference ,ErrorStrings.Export_Title_PackingTask_PhyQty,"推荐库位",ErrorStrings.Export_Title_AccountPeriod_CreatorName, ErrorStrings.Export_Title_AccountPeriod_CreateTime,ErrorStrings.Export_Title_PackingTask_PhyPackingMaterial,ErrorStrings.Export_Title_PackingTask_RecomQty,ErrorStrings.Export_Title_AccountPeriod_ModifierName,ErrorStrings.Export_Title_AccountPeriod_ModifyTime,ErrorStrings.Export_Title_PackingTask_HasDifference
                                };
                            }
                            if (reader.Read())
                            {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if (num != reader.FieldCount)
                                {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
