﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        /// <summary>
        /// 导出经销商配件库存
        /// </summary>
        public bool ExportDealerPartsStock(int[] ids, int dealerId, int? salesCategoryId, string sparePartCode, string sparePartName, out string fileName) {
            fileName = GetExportFilePath("经销商配件库存_" + ".xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.AppendFormat(@"Select a.Dealercode,
                                               a.Dealername,
                                               b.Code,
                                               b.Name,
                                               a.Salescategoryname,
                                               a.Sparepartcode,
                                               c.Name,
                                               a.Quantity,
                                            Nvl((Select t.Salesprice
                                                        From Partssalesprice t
                                                        Where t.Partssalescategoryid = a.Salescategoryid
                                                        And t.Sparepartid = a.Sparepartid),0)
                                          From Dealerpartsstock a
                                          Left Join Subdealer b
                                            On a.Subdealerid = b.Id
                                          Left Join Sparepart c
                                            On a.Sparepartid = c.Id
                                            where a.dealerId = {0} ", dealerId);
                    var dbParameters = new List<DbParameter>();
                    if(ids != null && ids.Length > 0) {
                        sql.Append(" and a.id in (");
                        for(var i = 0; i < ids.Length; i++) {
                            if(ids.Length == i + 1) {
                                sql.Append("{0}id" + i);
                                dbParameters.Add(db.CreateDbParameter("id" + i, ids[i]));
                            } else {
                                sql.Append("{0}id" + i + ",");
                                dbParameters.Add(db.CreateDbParameter("id" + i, ids[i]));
                            }
                        }
                        sql.Append(")");
                    } else {
                        if(salesCategoryId.HasValue) {
                            sql.Append(@" and a.salesCategoryId = {0}salesCategoryId ");
                            dbParameters.Add(db.CreateDbParameter("salesCategoryId", salesCategoryId.Value));
                        }
                        if(!String.IsNullOrEmpty(sparePartCode)) {
                            sql.Append(@" and Upper(a.sparePartCode) like Upper({0}sparePartCode) ");
                            dbParameters.Add(db.CreateDbParameter("sparePartCode", "%" + sparePartCode + "%"));
                        }
                        if(!String.IsNullOrEmpty(sparePartName)) {
                            sql.Append(@" and c.Name like {0}sparePartName ");
                            dbParameters.Add(db.CreateDbParameter("sparePartName", "%" + sparePartName + "%"));
                        }

                    }

                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    ErrorStrings.Export_Title_Dealer_Code, ErrorStrings.Export_Title_Dealer_Name, "二级站编号", ErrorStrings.Export_Title_DealerPartsRetailOrder_SubDealerName,ErrorStrings.Export_Title_Partssalescategory_Name, ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode, ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName,ErrorStrings.Export_Title_WarehouseArea_Quantity,ErrorStrings.Export_Title_DealerPartsStockQueryView_SalesPrice
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }
    }
}