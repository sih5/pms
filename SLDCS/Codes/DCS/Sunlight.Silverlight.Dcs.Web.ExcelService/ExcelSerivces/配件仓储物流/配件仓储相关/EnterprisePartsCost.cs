﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Globalization;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        /// <summary>
        /// 合并导出企业配件成本
        /// </summary>
        public bool ExportEnterprisePartsCost(int[] ids, int? partsSalesCategoryId, string sparePartCode, string sparePartName, string quantity, string costPrice, string costAmount,
            int? partABC,DateTime? createTimeBegin, DateTime? createTimeEnd, DateTime? modifyTimeBegin, DateTime? modifyTimeEnd, out string fileName)
        {
            fileName = GetExportFilePath("企业库存.xlsx");

            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);
                var userInfo = Utils.GetCurrentUserInfo();
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.Append(string.Format(@"select psc.name as PartsSalesCategoryName,sp.partcode as SparePartCode,sp.partname as SparePartName,(select value  from keyvalueitem where name ='ABCStrategy_Category' and key = sp.partabc) as abc,sp.ReferenceCode,e.Quantity,e.CostPrice,e.CostAmount,e.CreatorName,e.CreateTime,e.ModifierName,e.ModifyTime
                            from EnterprisePartsCost e inner join partsbranch sp on sp.partid=e.sparepartid and sp.status=1 inner join PartsSalesCategory psc on psc.id=e.partssalescategoryid 
                            where 1=1 "));
                    var dbParameters = new List<DbParameter>();
                    sql.Append("and e.OwnerCompanyId = {0}ownerCompanyId ");
                    dbParameters.Add(db.CreateDbParameter("ownerCompanyId",Utils.GetCurrentUserInfo().EnterpriseId));
                    if (ids != null && ids.Length > 0)
                    {
                        sql.Append(" and e.id in (");
                        for (var i = 0; i < ids.Length; i++)
                        {
                            if (ids.Length == i + 1)
                            {
                                sql.Append("{0}" + ids[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter(ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                            else
                            {
                                sql.Append("{0}" + ids[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter(ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                        }
                        sql.Append(")");
                    }
                    else
                    {
                        if (partsSalesCategoryId.HasValue)
                        {
                            sql.Append("and e.partsSalesCategoryId = {0}partsSalesCategoryId ");
                            dbParameters.Add(db.CreateDbParameter("partsSalesCategoryId", partsSalesCategoryId));
                        }

                        if (!string.IsNullOrEmpty(sparePartCode))
                        {
                            sql.Append("and sp.code like {0}sparePartCode ");
                            dbParameters.Add(db.CreateDbParameter("sparePartCode", "%" + sparePartCode + "%"));
                        }
                        if (!string.IsNullOrEmpty(sparePartName))
                        {
                            sql.Append("and sp.name like {0}sparePartName ");
                            dbParameters.Add(db.CreateDbParameter("sparePartName", "%" + sparePartName + "%"));
                        }
                        if(partABC.HasValue)
                        {
                            sql.Append("and sp.partabc = {0}partABC ");
                            dbParameters.Add(db.CreateDbParameter("partABC", partABC ));
                        }
                        if (!string.IsNullOrEmpty(quantity))
                        {
                            if ("1".Equals(quantity))
                            {
                                sql.Append("and e.quantity > 0 ");
                            }
                            else {
                                sql.Append("and e.quantity <= 0 ");
                            }
                        }

                        if (!string.IsNullOrEmpty(costPrice))
                        {
                            sql.Append("and e.costPrice like {0}costPrice ");
                            dbParameters.Add(db.CreateDbParameter("costPrice", "%" + costPrice + "%"));
                        }
                        if (!string.IsNullOrEmpty(costAmount))
                        {
                            sql.Append("and e.costAmount like {0}costAmount ");
                            dbParameters.Add(db.CreateDbParameter("costAmount", "%" + costAmount + "%"));
                        }

                        if (createTimeBegin.HasValue)
                        {
                            sql.Append(@" and e.createTime >=To_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if (createTimeEnd.HasValue)
                        {
                            sql.Append(@" and e.createTime <=To_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                        if (modifyTimeBegin.HasValue)
                        {
                            sql.Append(@" and e.modifyTime >=To_date({0}modifyTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = modifyTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("modifyTimeBegin", tempTime.ToString("G")));
                        }
                        if (modifyTimeEnd.HasValue)
                        {
                            sql.Append(@" and e.modifyTime <=To_date({0}modifyTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = modifyTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("modifyTimeEnd", tempTime.ToString("G")));
                        }
                    }

                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                   ErrorStrings.Export_Title_Partssalescategory_Name, ErrorStrings.Export_Title_PartsBranch_Code,ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName,ErrorStrings.Export_Title_PartsBranch_PartABC, ErrorStrings.Export_Title_PartsBranch_SpareReferenceCode,ErrorStrings.Export_Title_WarehouseArea_Quantity,ErrorStrings.Export_Title_PartsBranch_CostPrice, ErrorStrings.Export_Title_PartsBranch_CostAmount, ErrorStrings.Export_Title_AccountPeriod_CreatorName, ErrorStrings.Export_Title_AccountPeriod_CreateTime, ErrorStrings.Export_Title_AccountPeriod_ModifierName, ErrorStrings.Export_Title_AccountPeriod_ModifyTime};
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();

                }
                return true;
            } catch(Exception) {
                return false;
            }
        }

    }
}
