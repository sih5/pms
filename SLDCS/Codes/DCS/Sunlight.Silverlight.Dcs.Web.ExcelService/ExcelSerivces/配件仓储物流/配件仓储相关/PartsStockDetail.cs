﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Data.Common;
using System.Text;
using System.Linq;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using System.Data;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        //导出配件库存明细 
        public bool ExportPartsStockDetail(int storageCompanyId, int? warehouseId, string partsName, string partsCode, int? warehouseAreaCategoryId, string warehouseAreaCode, out string fileName) {
            fileName = GetExportFilePath("配件库存明细_" + ".xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated) {
                    throw new Exception(ErrorStrings.File_Validation_User);
                }
                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.Append(@"SELECT TOP.Name,TOP.Category,TOP.Code,TOP.SparePartCode,TOP.SparePartName,TOP.Quantity FROM(SELECT 
                                Filter1.Id2 AS Id,
                                Filter1.WarehouseId1 AS WarehouseId,
                                Filter1.StorageCompanyId1 AS StorageCompanyId,
                                Filter1.StorageCompanyType1 AS StorageCompanyType,
                                Filter1.BranchId1 AS BranchId,
                                Filter1.WarehouseAreaId,
                                Filter1.Quantity,
                                Filter1.Code4 AS Code,
                                Filter1.Code5 AS Code1,
                                Filter1.Code6 AS Code2,
                                Filter1.Name,
                                Extent5.Id AS Id1,
                                Extent5.Code AS SparePartCode,
                                Extent5.Name AS SparePartName,
                                Extent5.MeasureUnit,
                                Extent5.Status,
                                (select value from keyvalueitem where NAME = 'Area_Category'and key=Extent6.Category) As Category,
                                CASE WHEN Extent7.Quantity IS NULL THEN Filter1.Quantity ELSE Extent7.Quantity END AS C1,
                                CASE WHEN Extent7.BatchNumber IS NULL THEN '' ELSE Extent7.BatchNumber END AS C2,
                                Extent7.InboundTime AS C3
                                FROM     (SELECT 
                                  Extent1.Id AS Id2,
                                  Extent1.WarehouseId AS WarehouseId1,
                                  Extent1.StorageCompanyId AS StorageCompanyId1,
                                  Extent1.StorageCompanyType AS StorageCompanyType1,
                                  Extent1.BranchId AS BranchId1,
                                  Extent1.WarehouseAreaId,
                                  Extent1.WarehouseAreaCategoryId,
                                  Extent1.PartId,
                                  Extent1.Quantity,
                                  Extent1.Remark AS Remark1,
                                  Extent1.CreatorId AS CreatorId1,
                                  Extent1.CreatorName AS CreatorName1,
                                  Extent1.CreateTime AS CreateTime1,
                                  Extent1.ModifierId AS ModifierId1,
                                  Extent1.ModifierName AS ModifierName1,
                                  Extent1.ModifyTime AS ModifyTime1,
                                  Extent2.Id AS Id3,
                                  Extent2.WarehouseId AS WarehouseId2,
                                  Extent2.ParentId AS ParentId1,
                                  Extent2.Code AS Code4,
                                  Extent2.TopLevelWarehouseAreaId AS TopLevelWarehouseAreaId1,
                                  Extent2.AreaCategoryId AS AreaCategoryId1,
                                  Extent2.Status AS Status1,
                                  Extent2.Remark AS Remark2,
                                  Extent2.AreaKind AS AreaKind1,
                                  Extent2.CreatorId AS CreatorId2,
                                  Extent2.CreatorName AS CreatorName2,
                                  Extent2.CreateTime AS CreateTime2,
                                  Extent2.ModifierId AS ModifierId2,
                                  Extent2.ModifierName AS ModifierName2,
                                  Extent2.ModifyTime AS ModifyTime2,
                                  Extent2.RowVersion AS RowVersion1,
                                  Extent3.Id AS Id4,
                                  Extent3.WarehouseId AS WarehouseId3,
                                  Extent3.ParentId AS ParentId2,
                                  Extent3.Code AS Code5,
                                  Extent3.TopLevelWarehouseAreaId AS TopLevelWarehouseAreaId2,
                                  Extent3.AreaCategoryId AS AreaCategoryId2,
                                  Extent3.Status AS Status2,
                                  Extent3.Remark AS Remark3,
                                  Extent3.AreaKind AS AreaKind2,
                                  Extent3.CreatorId AS CreatorId3,
                                  Extent3.CreatorName AS CreatorName3,
                                  Extent3.CreateTime AS CreateTime3,
                                  Extent3.ModifierId AS ModifierId3,
                                  Extent3.ModifierName AS ModifierName3,
                                  Extent3.ModifyTime AS ModifyTime3,
                                  Extent3.RowVersion AS RowVersion2,
                                  Extent4.Id AS Id5,
                                  Extent4.Code AS Code6,
                                  Extent4.Name,
                                  Extent4.Type,
                                  Extent4.Status AS Status3,
                                  Extent4.Address,
                                  Extent4.RegionId,
                                  Extent4.PhoneNumber,
                                  Extent4.Contact,
                                  Extent4.Fax,
                                  Extent4.Email,
                                  Extent4.StorageStrategy,
                                  Extent4.Remark AS Remark4,
                                  Extent4.Picture,
                                  Extent4.BranchId AS BranchId2,
                                  Extent4.StorageCompanyId AS StorageCompanyId2,
                                  Extent4.StorageCompanyType AS StorageCompanyType2,
                                  Extent4.StorageCenter,
                                  Extent4.WmsInterface,
                                  Extent4.CreatorId AS CreatorId4,
                                  Extent4.CreatorName AS CreatorName4,
                                  Extent4.CreateTime AS CreateTime4,
                                  Extent4.ModifierId AS ModifierId4,
                                  Extent4.ModifierName AS ModifierName4,
                                  Extent4.ModifyTime AS ModifyTime4,
                                  Extent4.RowVersion AS RowVersion3
                                  FROM    PartsStock Extent1
                                  INNER JOIN WarehouseArea Extent2 ON Extent1.WarehouseAreaId = Extent2.Id
                                  INNER JOIN WarehouseArea Extent3 ON Extent2.TopLevelWarehouseAreaId = Extent3.Id
                                  INNER JOIN Warehouse Extent4 ON Extent1.WarehouseId = Extent4.Id
                                  WHERE ((((Extent2.AreaKind = 3) AND (Extent2.Status = 1)) ) AND (Extent3.Status = 1)) AND (Extent4.Status = 1) ) Filter1
                                INNER JOIN SparePart Extent5 ON Filter1.PartId = Extent5.Id
                                INNER JOIN WarehouseAreaCategory Extent6 ON Filter1.WarehouseAreaCategoryId = Extent6.Id
                                LEFT OUTER JOIN PartsStockBatchDetail Extent7 ON Filter1.Id2 = Extent7.PartsStockId");
                    var dbParameters = new List<DbParameter>();
                    if(warehouseAreaCategoryId.HasValue) {
                        sql.Append(@" where Extent6.Category like {0}warehouseAreaCategoryId ");
                        dbParameters.Add(db.CreateDbParameter("warehouseAreaCategoryId", warehouseAreaCategoryId.Value));
                    }
                    sql.AppendFormat(" )TOP WHERE TOP.StorageCompanyId={0} ", storageCompanyId);
                    if(warehouseId.HasValue) {
                        sql.Append(@" and TOP.WarehouseId like {0}warehouseId");
                        dbParameters.Add(db.CreateDbParameter("warehouseId", warehouseId.Value));
                    }
                    if(!string.IsNullOrEmpty(partsCode)) {
                        sql.Append(@" and TOP.SparePartCode like {0}partsCode");
                        dbParameters.Add(db.CreateDbParameter("partsCode", "%" + partsCode + "%"));
                    }
                    if(!string.IsNullOrEmpty(partsName)) {
                        sql.Append(@" and TOP.SparePartName like {0}partsName");
                        dbParameters.Add(db.CreateDbParameter("partsName", "%" + partsName + "%"));
                    }

                    if(!string.IsNullOrEmpty(warehouseAreaCode)) {
                        sql.Append(@" and TOP.Code like {0}warehouseAreaCode");
                        dbParameters.Add(db.CreateDbParameter("warehouseAreaCode", "%" + warehouseAreaCode + "%"));
                    }

                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var execlExport = new ExcelExport(fileName)) {
                        execlExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    ErrorStrings.Export_Title_Company_WarehouseName, ErrorStrings.Export_Title_WarehouseAreaCategory_Category, ErrorStrings.Export_Title_WarehouseArea_WarehouseAreaCode, ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode, ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName, ErrorStrings.Export_Title_WarehouseArea_Quantity
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }

                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }

        public bool ImportWarehouseAreaForPartsStock(string fileName,int parentId, out int excelImportNum, out List<WarehouseAreaForStockExtend> rightData, out List<WarehouseAreaForStockExtend> errorData, out string errorDataFileName, out string errorMessage)
        {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<WarehouseAreaForStockExtend>();
            var allList = new List<WarehouseAreaForStockExtend>();
            var rightList = new List<WarehouseAreaForStockExtend>();
            try {
                if (!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                List<object> excelColumns;
                using (var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    #region 指定文件中的列对应的名称 例如 excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode, "PartCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_WarehouseArea_WarehouseAreaCode, "WarehouseAreaCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode, "PartCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName, "PartName");
                    #endregion
                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    excelImportNum = excelOperator.LoadExcelRow(row =>
                    {
                        var tempImportObj = new WarehouseAreaForStockExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        #region 根据导入的字段给对应的实体属性赋值 tempImportObj.PartsSalesCategoryNameStr = row["PartsSalesCategoryName"].Trim();
                        tempImportObj.WarehouseAreaCodeStr = newRow["WarehouseAreaCode"];
                        tempImportObj.PartCodeStr = newRow["PartCode"];
                        tempImportObj.PartNameStr = newRow["PartName"];
                        #endregion

                        var tempErrorMessage = new List<string>();
                        #region 导入的数据基本检查
                        //库位检查
                        if (string.IsNullOrEmpty(tempImportObj.WarehouseAreaCodeStr)) {
                            tempErrorMessage.Add("库位不能为空");
                        }
                        //配件编号检查
                        if (string.IsNullOrEmpty(tempImportObj.PartCodeStr)) {
                            tempErrorMessage.Add(ErrorStrings.ImpPartsPurchaseOrderDetail_Validation1);
                        }
                        #endregion
                        if (tempErrorMessage.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });
                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    #region 剩下的数据进行业务检查
                    //校验导入的数据重复
                    var groups = allList.GroupBy(r => new
                    {
                        r.PartCodeStr,
                        r.WarehouseAreaCodeStr
                    }
                    ).Where(r => r.Count() > 1);
                    foreach (var groupItem in groups.SelectMany(r => r)) {
                        groupItem.ErrorMsg = "数据重复";
                    }
                    #region 校验配件编号
                    var PartCodeNeedCheck = tempRightList.Select(r => r.PartCodeStr.ToUpper()).Distinct().ToArray();
                    var dbPartCodes = new List<WarehouseAreaForStockExtend>();
                    Func<string[], bool> getDbPartCodes = value =>
                    {
                        var dbObj = new WarehouseAreaForStockExtend
                        {
                            PartId = Convert.ToInt32(value[0]),
                            PartCode = value[1],
                            PartName = value[2]
                        };
                        dbPartCodes.Add(dbObj);
                        return false;
                    };
                    const string SQL = @"Select id,code,name from sparepart a where a.Status=1 ";
                    db.QueryDataWithInOperator(SQL, "a.code", true, PartCodeNeedCheck, getDbPartCodes);
                    #endregion

                    #region 校验库区库位
                    var WarehouseAreaNeedCheck = tempRightList.Select(r => r.WarehouseAreaCodeStr.ToUpper()).Distinct().ToArray();
                    var dbWarehouseAreas = new List<WarehouseAreaForStockExtend>();
                    Func<string[], bool> getDbWarehouseAreas = value =>
                    {
                        var dbObj = new WarehouseAreaForStockExtend
                        {
                            WarehouseAreaId = Convert.ToInt32(value[0]),
                            WarehouseAreaCode = value[1],
                            StorageCompanyId = Convert.ToInt32(value[2]),
                            StorageCompanyType = Convert.ToInt32(value[3]),
                            BranchId = Convert.ToInt32(value[4]),
                            AreaCategoryId = Convert.ToInt32(value[5]),
                            WarehouseId = Convert.ToInt32(value[6])
                        };
                        dbWarehouseAreas.Add(dbObj);
                        return false;
                    };
                    string QueryAreaSQL = @"Select a.id,a.code,b.StorageCompanyId,b.StorageCompanyType,b.BranchId,a.AreaCategoryId,a.warehouseid from WarehouseArea a inner join warehouse b on a.warehouseid=b.id where a.Status=1 and b.Status=1 and a.AreaKind =3 and a.ParentId =" + parentId;
                    db.QueryDataWithInOperator(QueryAreaSQL, "a.code", true, WarehouseAreaNeedCheck, getDbWarehouseAreas);
                    #endregion

                    #region 校验库存
                    var PartsStockNeedCheck = tempRightList.Select(r => r.PartCodeStr).Distinct().ToArray();
                    var dbPartsStocks = new List<WarehouseAreaForStockExtend>();
                    Func<string[], bool> getDbPartsStock = value =>
                    {
                        var dbObj = new WarehouseAreaForStockExtend
                        {
                            PartId = Convert.ToInt32(value[0]),
                            WarehouseAreaId = Convert.ToInt32(value[1])
                        };
                        dbPartsStocks.Add(dbObj);
                        return false;
                    };
                    string QueryPartsStockSQL = @"Select a.PartId,a.WarehouseAreaId from PartsStock a inner join SparePart b on a.PartId=b.id";
                    db.QueryDataWithInOperator(QueryPartsStockSQL, "b.code", true, PartsStockNeedCheck, getDbPartsStock);
                    #endregion

                    foreach (var tempRight in tempRightList) {
                        var part = dbPartCodes.FirstOrDefault(v => v.PartCode == tempRight.PartCodeStr);
                        if (part == null) {
                            tempRight.ErrorMsg = String.Format(ErrorStrings.Export_Validation_PartsTransferOrder_Validation1, tempRight.PartCodeStr);
                            continue;
                        }
                        else {
                            tempRight.PartCode = part.PartCode;
                            tempRight.PartName = part.PartName;
                            tempRight.PartId = part.PartId;
                        }
                        var area = dbWarehouseAreas.FirstOrDefault(v => v.WarehouseAreaCode == tempRight.WarehouseAreaCodeStr);
                        if (area == null) {
                            tempRight.ErrorMsg = "库位信息在指定库区节点不存在";
                            continue;
                        }
                        else {
                            tempRight.WarehouseAreaCode = area.WarehouseAreaCodeStr;
                            tempRight.WarehouseAreaId = area.WarehouseAreaId;
                            tempRight.StorageCompanyId = area.StorageCompanyId;
                            tempRight.StorageCompanyType = area.StorageCompanyType;
                            tempRight.BranchId = area.BranchId;
                            tempRight.AreaCategoryId = area.AreaCategoryId;
                            tempRight.WarehouseId = area.WarehouseId;

                        }
                        var stock = dbPartsStocks.FirstOrDefault(v => v.PartId == tempRight.PartId && v.WarehouseAreaId == tempRight.WarehouseAreaId);
                        if (stock != null) {
                            tempRight.ErrorMsg = "配件库位库存分配关系已存在";
                            continue;
                        }
                    }
                    #endregion

                    //获取所有不合格数据
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    //获取所有合格数据
                    rightList = allList.Except(errorList).ToList();

                    
                }
                //导出所有不合格数据
                if (errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using (var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index =>
                        {
                            if (index == list.Count + 1)
                                return null;
                            if (index == 0)
                                return excelColumns.ToArray();
                            var tempObj = list[index - 1];
                            var values = new object[] {
                                #region 设置错误信息导出的列的值
                                tempObj.WarehouseAreaCodeStr,tempObj.PartCodeStr,tempObj.PartNameStr,tempObj.ErrorMsg
                                #endregion                     
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }
                //导入所有合格数据
                if (!rightList.Any())
                    return true;
                using (var conn = db.CreateDbConnection()) {
                    conn.Open();
                    //开启事务，新增更新配件在一个事务内
                    var ts = conn.BeginTransaction();
                    try {
                        //新增配件
                        if (rightList.Any()) {
                            #region 获取对应的插入的SQL 通过db.GetInsertSql方法
                            //获取新增数据的sql语句，Id为主键
                            var sqlInsert = db.GetInsertSql("PartsStock", "Id", new[] {
                                    "WarehouseId",
                                    "StorageCompanyId",
                                    "StorageCompanyType",
                                    "BranchId",
                                    "WarehouseAreaId",
                                    "WarehouseAreaCategoryId",
                                    "PartId",
                                    "Quantity",
                                    "CreatorId",
                                    "CreatorName",
                                    "CreateTime"
                            });

                            var sqlhistoryInsert = db.GetInsertSql("WarehouseAreaHistory", "Id", new[] {
                                    "WarehouseId",
                                    "StorageCompanyId",
                                    "StorageCompanyType",
                                    "BranchId",
                                    "WarehouseAreaId",
                                    "WarehouseAreaCategoryId",
                                    "PartId",
                                    "Quantity",
                                    "CreatorId",
                                    "CreatorName",
                                    "CreateTime"
                            });

                            #endregion
                            var userInfo = Utils.GetCurrentUserInfo();
                            //往数据库增加库存
                            foreach (var item in rightList) {
                                #region 添加Sql的参数
                                var command = db.CreateDbCommand(sqlInsert, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("WarehouseId", item.WarehouseId));
                                command.Parameters.Add(db.CreateDbParameter("StorageCompanyId", item.StorageCompanyId));
                                command.Parameters.Add(db.CreateDbParameter("StorageCompanyType", item.StorageCompanyType));
                                command.Parameters.Add(db.CreateDbParameter("BranchId", item.BranchId));
                                command.Parameters.Add(db.CreateDbParameter("WarehouseAreaId", item.WarehouseAreaId));
                                command.Parameters.Add(db.CreateDbParameter("WarehouseAreaCategoryId", item.AreaCategoryId));
                                command.Parameters.Add(db.CreateDbParameter("PartId", item.PartId));
                                command.Parameters.Add(db.CreateDbParameter("Quantity",0));
                                command.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                command.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                command.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                #endregion
                                command.ExecuteNonQuery();
                                #region 添加Sql的参数
                                var historycommand = db.CreateDbCommand(sqlhistoryInsert, conn, ts);
                                historycommand.Parameters.Add(db.CreateDbParameter("WarehouseId", item.WarehouseId));
                                historycommand.Parameters.Add(db.CreateDbParameter("StorageCompanyId", item.StorageCompanyId));
                                historycommand.Parameters.Add(db.CreateDbParameter("StorageCompanyType", item.StorageCompanyType));
                                historycommand.Parameters.Add(db.CreateDbParameter("BranchId", item.BranchId));
                                historycommand.Parameters.Add(db.CreateDbParameter("WarehouseAreaId", item.WarehouseAreaId));
                                historycommand.Parameters.Add(db.CreateDbParameter("WarehouseAreaCategoryId", item.AreaCategoryId));
                                historycommand.Parameters.Add(db.CreateDbParameter("PartId", item.PartId));
                                historycommand.Parameters.Add(db.CreateDbParameter("Quantity", 0));
                                historycommand.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                historycommand.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                historycommand.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                #endregion
                                historycommand.ExecuteNonQuery();
                            }
                        }
                        //无异常提交
                        ts.Commit();
                    }
                    catch (Exception) {
                        //报错回滚
                        ts.Rollback();
                        throw;
                    }
                    finally {
                        if (conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                }
                return true;
            }
            catch (Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            }
            finally {
                errorData = errorList;
                rightData = rightList;
            }
        }

    }
}
