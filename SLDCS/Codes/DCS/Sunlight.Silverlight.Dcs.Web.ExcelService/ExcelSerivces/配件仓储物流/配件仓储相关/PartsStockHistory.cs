﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Globalization;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        /// <summary>
        /// 合并导出库存变更日志
        /// </summary>
        public bool ExportPartsStockHistory(int[] ids,int? warehouseId, string sparePartCode, string sparePartName,string warehouseAreaCode,int? warehouseAreaCategory,DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName)
        {
            fileName = GetExportFilePath("库存变更日志.xlsx");

            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);
                var userInfo = Utils.GetCurrentUserInfo();
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.Append(string.Format(@" select b.code as WarehouseCode,b.name as WarehouseName,d.code as WarehouseAreaCode,(select value from keyvalueitem where name = 'Area_Category' and key = e.category) as WarehouseAreaCategory,c.code as SparePartCode,c.name as SparePartName,
                            a.OldQuantity,a.Quantity,(a.quantity - a.oldquantity) as ChangedQuantity,a.LockedQty,a.Remark,a.CreatorName,a.CreateTime,a.ModifierName,a.ModifyTime
                              from PartsStockHistory a
                             inner join warehouse b
                                on a.warehouseid = b.id
                             inner join sparepart c
                                on c.id = a.partid
                             inner join warehousearea d
                                on a.WarehouseAreaId = d.id
                             inner join WarehouseAreaCategory e
                                on e.id = a.WarehouseAreaCategoryId where a.storagecompanyid = " + Utils.GetCurrentUserInfo().EnterpriseId));
                    var dbParameters = new List<DbParameter>();
                    if (ids != null && ids.Length > 0)
                    {
                        sql.Append(" and a.id in (");
                        for (var i = 0; i < ids.Length; i++)
                        {
                            if (ids.Length == i + 1)
                            {
                                sql.Append("{0}" + "id" + ids[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                            else
                            {
                                sql.Append("{0}" + "id" + ids[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                        }
                        sql.Append(")");
                    }
                    else
                    {
                        if (warehouseId.HasValue)
                        {
                            sql.Append("and a.warehouseId = {0}warehouseId ");
                            dbParameters.Add(db.CreateDbParameter("warehouseId", warehouseId));
                        }

                        if (!string.IsNullOrEmpty(sparePartCode))
                        {
                            sql.Append("and c.code like {0}sparePartCode ");
                            dbParameters.Add(db.CreateDbParameter("sparePartCode", "%" + sparePartCode + "%"));
                        }
                        if (!string.IsNullOrEmpty(sparePartName))
                        {
                            sql.Append("and c.name like {0}sparePartName ");
                            dbParameters.Add(db.CreateDbParameter("sparePartName", "%" + sparePartName + "%"));
                        }
                        if (!string.IsNullOrEmpty(warehouseAreaCode))
                        {
                            sql.Append("and d.code like {0}warehouseAreaCode ");
                            dbParameters.Add(db.CreateDbParameter("warehouseAreaCode", "%" + warehouseAreaCode + "%"));
                        }
                        if (warehouseAreaCategory.HasValue)
                        {
                            sql.Append("and e.Category = {0}warehouseAreaCategory ");
                            dbParameters.Add(db.CreateDbParameter("warehouseAreaCategory", warehouseAreaCategory));
                        }

                        if (createTimeBegin.HasValue)
                        {
                            sql.Append(@" and a.createTime >=To_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if (createTimeEnd.HasValue)
                        {
                            sql.Append(@" and a.createTime <=To_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                    }

                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                   ErrorStrings.Export_Title_Company_WarehouseCode,ErrorStrings.Export_Title_Company_WarehouseName,ErrorStrings.Export_Title_WarehouseAreaCategory_AreaKindCode,ErrorStrings.Export_Title_WarehouseArea_Category, ErrorStrings.Export_Title_PartsBranch_Code,ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName,ErrorStrings.Export_Title_PartsStockHistory_OldQuantity,ErrorStrings.Export_Title_WarehouseArea_Quantity,ErrorStrings.Export_Title_PartsStockHistory_ChangeQuantity,ErrorStrings.Export_Title_PartsStockHistory_LockedQty,ErrorStrings.Export_Title_PartsBranch_Remark, ErrorStrings.Export_Title_AccountPeriod_CreatorName, ErrorStrings.Export_Title_AccountPeriod_CreateTime, ErrorStrings.Export_Title_AccountPeriod_ModifierName, ErrorStrings.Export_Title_AccountPeriod_ModifyTime};
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();

                }
                return true;
            } catch(Exception) {
                return false;
            }
        }

    }
}
