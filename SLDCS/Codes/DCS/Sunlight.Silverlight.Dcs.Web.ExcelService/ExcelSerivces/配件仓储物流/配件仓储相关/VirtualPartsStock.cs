﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        //导出配件库位库存
        public bool ExportPartsStockWithWarehouseArea(int? partsOutPlanId, int? warehouseAreaCategoryValue, int? operatorId, int? managerId, string warehouseRegionCode, out string fileName) {
            fileName = GetExportFilePath("配件库位库存_" + ".xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.Append(@"SELECT top.Id,
                        top.WarehouseId,
                        top.Code2 AS Code,
                        top.Name,
                        top.StorageCompanyId,
                        top.StorageCompanyType,
                        top.BranchId,
                        top.Id1,
                        top.Code3 AS Code1,
                        top.Name1,
                        top.MeasureUnit,
                        top.Category,
                        top.Code AS Code2,
                        top.WarehouseAreaId,
                        top.Code1 AS Code3,
                        top.Quantity,
                        top.C1,
                        top.C2,
                        null as standardpackamount,
                        null as standardpack,
                        top.C3
                    FROM ( SELECT 
                        Project1.Id,
                        Project1.WarehouseId,
                        Project1.StorageCompanyId,
                        Project1.StorageCompanyType,
                        Project1.BranchId,
                        Project1.WarehouseAreaId,
                        Project1.Quantity,
                        Project1.Code,
                        Project1.Code1,
                        Project1.Code2,
                        Project1.Name,
                        Project1.Id1,
                        Project1.Code3,
                        Project1.Name1,
                        Project1.MeasureUnit,
                        Project1.Category,
                        Project1.C1,
                        Project1.C2,
                        Project1.C3
                        FROM ( SELECT 
                            Filter3.Id2 AS Id,
                            Filter3.WarehouseId1 AS WarehouseId,
                            Filter3.StorageCompanyId1 AS StorageCompanyId,
                            Filter3.StorageCompanyType1 AS StorageCompanyType,
                            Filter3.BranchId1 AS BranchId,
                            Filter3.WarehouseAreaId,
                            Filter3.Quantity,
                            Filter3.Code4 AS Code,
                            Filter3.Code5 AS Code1,
                            Filter3.Code6 AS Code2,
                            Filter3.Name2 AS Name,
                            Filter3.Id3 AS Id1,
                            Filter3.Code7 AS Code3,
                            Filter3.Name3 AS Name1,
                            Filter3.MeasureUnit,
                            Filter3.Category,
                            CASE WHEN Extent7.Quantity IS NULL THEN Filter3.Quantity ELSE Extent7.Quantity END AS C1,
                            CASE WHEN Extent7.BatchNumber IS NULL THEN '' ELSE Extent7.BatchNumber END AS C2,
                            Extent7.InboundTime AS C3
                            FROM   (SELECT Filter2.Id2, Filter2.WarehouseId1, Filter2.StorageCompanyId1, Filter2.StorageCompanyType1, Filter2.BranchId1, Filter2.WarehouseAreaId, Filter2.WarehouseAreaCategoryId, Filter2.PartId, Filter2.Quantity, Filter2.Remark1, Filter2.CreatorId1, Filter2.CreatorName1, Filter2.CreateTime1, Filter2.ModifierId1, Filter2.ModifierName1, Filter2.ModifyTime1, Filter2.Id4, Filter2.WarehouseId2, Filter2.ParentId1, Filter2.Code4, Filter2.TopLevelWarehouseAreaId1, Filter2.AreaCategoryId1, Filter2.Status1, Filter2.Remark2, Filter2.AreaKind1, Filter2.CreatorId2, Filter2.CreatorName2, Filter2.CreateTime2, Filter2.ModifierId2, Filter2.ModifierName2, Filter2.ModifyTime2, Filter2.RowVersion1, Filter2.Id5, Filter2.WarehouseId3, Filter2.ParentId2, Filter2.Code5, Filter2.TopLevelWarehouseAreaId2, Filter2.AreaCategoryId2, Filter2.Status2, Filter2.Remark3, Filter2.AreaKind2, Filter2.CreatorId3, Filter2.CreatorName3, Filter2.CreateTime3, Filter2.ModifierId3, Filter2.ModifierName3, Filter2.ModifyTime3, Filter2.RowVersion2, Filter2.Id6, Filter2.Code6, Filter2.Name2, Filter2.Type, Filter2.Status3, Filter2.Address, Filter2.RegionId, Filter2.PhoneNumber, Filter2.Contact, Filter2.Fax, Filter2.Email, Filter2.StorageStrategy, Filter2.Remark4, Filter2.Picture, Filter2.BranchId2, Filter2.StorageCompanyId2, Filter2.StorageCompanyType2, Filter2.StorageCenter, Filter2.WmsInterface, Filter2.CreatorId4, Filter2.CreatorName4, Filter2.CreateTime4, Filter2.ModifierId4, Filter2.ModifierName4, Filter2.ModifyTime4, Filter2.RowVersion3, Filter2.Id3, Filter2.Code7, Filter2.Name3, Filter2.LastSubstitute, Filter2.NextSubstitute, Filter2.ShelfLife, Filter2.EnglishName, Filter2.PinyinCode, Filter2.ReferenceCode, Filter2.ReferenceName, Filter2.CADCode, Filter2.CADName, Filter2.PartType, Filter2.Specification, Filter2.Feature, Filter2.Status4, Filter2.Length, Filter2.Width, Filter2.Height, Filter2.Volume, Filter2.Weight, Filter2.Material, Filter2.PackingAmount, Filter2.PackingSpecification, Filter2.PartsOutPackingCode, Filter2.PartsInPackingCode, Filter2.MeasureUnit, Filter2.CreatorId5, Filter2.CreatorName5, Filter2.CreateTime5, Filter2.ModifierId5, Filter2.ModifierName5, Filter2.ModifyTime5, Filter2.AbandonerId, Filter2.AbandonerName, Filter2.AbandonTime, Filter2.RowVersion4, 
                          Extent6.Id AS Id7,
                          Extent6.Category
                          FROM   (SELECT Filter1.Id2, Filter1.WarehouseId1, Filter1.StorageCompanyId1, Filter1.StorageCompanyType1, Filter1.BranchId1, Filter1.WarehouseAreaId, Filter1.WarehouseAreaCategoryId, Filter1.PartId, Filter1.Quantity, Filter1.Remark1, Filter1.CreatorId1, Filter1.CreatorName1, Filter1.CreateTime1, Filter1.ModifierId1, Filter1.ModifierName1, Filter1.ModifyTime1, Filter1.Id4, Filter1.WarehouseId2, Filter1.ParentId1, Filter1.Code4, Filter1.TopLevelWarehouseAreaId1, Filter1.AreaCategoryId1, Filter1.Status1, Filter1.Remark2, Filter1.AreaKind1, Filter1.CreatorId2, Filter1.CreatorName2, Filter1.CreateTime2, Filter1.ModifierId2, Filter1.ModifierName2, Filter1.ModifyTime2, Filter1.RowVersion1, Filter1.Id5, Filter1.WarehouseId3, Filter1.ParentId2, Filter1.Code5, Filter1.TopLevelWarehouseAreaId2, Filter1.AreaCategoryId2, Filter1.Status2, Filter1.Remark3, Filter1.AreaKind2, Filter1.CreatorId3, Filter1.CreatorName3, Filter1.CreateTime3, Filter1.ModifierId3, Filter1.ModifierName3, Filter1.ModifyTime3, Filter1.RowVersion2, 
                            Extent4.Id AS Id6,
                            Extent4.Code AS Code6,
                            Extent4.Name AS Name2,
                            Extent4.Type,
                            Extent4.Status AS Status3,
                            Extent4.Address,
                            Extent4.RegionId,
                            Extent4.PhoneNumber,
                            Extent4.Contact,
                            Extent4.Fax,
                            Extent4.Email,
                            Extent4.StorageStrategy,
                            Extent4.Remark AS Remark4,
                            Extent4.Picture,
                            Extent4.BranchId AS BranchId2,
                            Extent4.StorageCompanyId AS StorageCompanyId2,
                            Extent4.StorageCompanyType AS StorageCompanyType2,
                            Extent4.StorageCenter,
                            Extent4.WmsInterface,
                            Extent4.CreatorId AS CreatorId4,
                            Extent4.CreatorName AS CreatorName4,
                            Extent4.CreateTime AS CreateTime4,
                            Extent4.ModifierId AS ModifierId4,
                            Extent4.ModifierName AS ModifierName4,
                            Extent4.ModifyTime AS ModifyTime4,
                            Extent4.RowVersion AS RowVersion3,
                            Extent5.Id AS Id3,
                            Extent5.Code AS Code7,
                            Extent5.Name AS Name3,
                            Extent5.LastSubstitute,
                            Extent5.NextSubstitute,
                            Extent5.ShelfLife,
                            Extent5.EnglishName,
                            Extent5.PinyinCode,
                            Extent5.ReferenceCode,
                            Extent5.ReferenceName,
                            Extent5.CADCode,
                            Extent5.CADName,
                            Extent5.PartType,
                            Extent5.Specification,
                            Extent5.Feature,
                            Extent5.Status AS Status4,
                            Extent5.Length,
                            Extent5.Width,
                            Extent5.Height,
                            Extent5.Volume,
                            Extent5.Weight,
                            Extent5.Material,
                            Extent5.PackingAmount,
                            Extent5.PackingSpecification,
                            Extent5.PartsOutPackingCode,
                            Extent5.PartsInPackingCode,
                            Extent5.MeasureUnit,
                            Extent5.CreatorId AS CreatorId5,
                            Extent5.CreatorName AS CreatorName5,
                            Extent5.CreateTime AS CreateTime5,
                            Extent5.ModifierId AS ModifierId5,
                            Extent5.ModifierName AS ModifierName5,
                            Extent5.ModifyTime AS ModifyTime5,
                            Extent5.AbandonerId,
                            Extent5.AbandonerName,
                            Extent5.AbandonTime,
                            Extent5.RowVersion AS RowVersion4
                            FROM    (SELECT 
                              Extent1.Id AS Id2,
                              Extent1.WarehouseId AS WarehouseId1,
                              Extent1.StorageCompanyId AS StorageCompanyId1,
                              Extent1.StorageCompanyType AS StorageCompanyType1,
                              Extent1.BranchId AS BranchId1,
                              Extent1.WarehouseAreaId,
                              Extent1.WarehouseAreaCategoryId,
                              Extent1.PartId,
                              Extent1.Quantity,
                              Extent1.Remark AS Remark1,
                              Extent1.CreatorId AS CreatorId1,
                              Extent1.CreatorName AS CreatorName1,
                              Extent1.CreateTime AS CreateTime1,
                              Extent1.ModifierId AS ModifierId1,
                              Extent1.ModifierName AS ModifierName1,
                              Extent1.ModifyTime AS ModifyTime1,
                              Extent2.Id AS Id4,
                              Extent2.WarehouseId AS WarehouseId2,
                              Extent2.ParentId AS ParentId1,
                              Extent2.Code AS Code4,
                              Extent2.TopLevelWarehouseAreaId AS TopLevelWarehouseAreaId1,
                              Extent2.AreaCategoryId AS AreaCategoryId1,
                              Extent2.Status AS Status1,
                              Extent2.Remark AS Remark2,
                              Extent2.AreaKind AS AreaKind1,
                              Extent2.CreatorId AS CreatorId2,
                              Extent2.CreatorName AS CreatorName2,
                              Extent2.CreateTime AS CreateTime2,
                              Extent2.ModifierId AS ModifierId2,
                              Extent2.ModifierName AS ModifierName2,
                              Extent2.ModifyTime AS ModifyTime2,
                              Extent2.RowVersion AS RowVersion1,
                              Extent3.Id AS Id5,
                              Extent3.WarehouseId AS WarehouseId3,
                              Extent3.ParentId AS ParentId2,
                              Extent3.Code AS Code5,
                              Extent3.TopLevelWarehouseAreaId AS TopLevelWarehouseAreaId2,
                              Extent3.AreaCategoryId AS AreaCategoryId2,
                              Extent3.Status AS Status2,
                              Extent3.Remark AS Remark3,
                              Extent3.AreaKind AS AreaKind2,
                              Extent3.CreatorId AS CreatorId3,
                              Extent3.CreatorName AS CreatorName3,
                              Extent3.CreateTime AS CreateTime3,
                              Extent3.ModifierId AS ModifierId3,
                              Extent3.ModifierName AS ModifierName3,
                              Extent3.ModifyTime AS ModifyTime3,
                              Extent3.RowVersion AS RowVersion2
                              FROM   PartsStock Extent1
                              INNER JOIN WarehouseArea Extent2 ON Extent1.WarehouseAreaId = Extent2.Id
                              INNER JOIN WarehouseArea Extent3 ON Extent2.TopLevelWarehouseAreaId = Extent3.Id
                        ");

                    var dbParameters = new List<DbParameter>();
                    sql.Append(string.Format(@" WHERE Extent2.AreaKind = {0} AND  Extent2.Status = {1} "
                        , (int)DcsAreaKind.库位, (int)DcsBaseDataStatus.有效));
                    //出库计划Id 不为空
                    if(partsOutPlanId.HasValue) {
                        sql.Append(@" AND (EXISTS(SELECT pobpd1.SparePartId FROM (SELECT pobpd.PartsOutboundPlanId,pobpd.SparePartId FROM PartsOutboundPlanDetail pobpd WHERE pobpd.PartsOutboundPlanId = {0}partsOutPlanId) pobpd1 WHERE Extent1.PartId = pobpd1.SparePartId) ");
                        dbParameters.Add(db.CreateDbParameter("partsOutPlanId", partsOutPlanId));
                        sql.Append(@" AND EXISTS(SELECT pobp.WarehouseId FROM PartsOutboundPlan pobp WHERE pobp.Id = {0}partsOutPlanId AND  Extent1.WarehouseId = pobp.WarehouseId )) ");
                        dbParameters.Add(db.CreateDbParameter("partsOutPlanId", partsOutPlanId));
                    }
                    //仓库人员Id 不为空
                    if(operatorId.HasValue) {
                        sql.Append(@" AND EXISTS( SELECT wp1.WarehouseId FROM ( SELECT wp.WarehouseId FROM WarehouseOperator wp WHERE wp.OperatorId = {0}operatorId ) wp1 WHERE Extent1.WarehouseId = wp1.WarehouseId ) ");
                        dbParameters.Add(db.CreateDbParameter("operatorId", operatorId));
                    }
                    //库区负责人Id 不为空 
                    if(managerId.HasValue) {
                        sql.Append(@" AND EXISTS(SELECT wam.ManagerId,wam.WarehouseAreaId FROM WarehouseAreaManager wam WHERE wam.ManagerId = {0}managerId AND Extent3.TopLevelWarehouseAreaId = wam.WarehouseAreaId) ");
                        dbParameters.Add(db.CreateDbParameter("managerId", managerId));
                    }
                    //库区编号 不为空
                    if(!string.IsNullOrWhiteSpace(warehouseRegionCode)) {
                        sql.Append(@" AND Extent2.Code = {0}warehouseRegionCode ");
                        dbParameters.Add(db.CreateDbParameter("warehouseRegionCode", warehouseRegionCode));
                    }
                    sql.Append(string.Format(@" AND Extent3.Status = {0} Filter1 INNER JOIN Warehouse Extent4 ON Filter1.WarehouseId1 = Extent4.Id INNER JOIN SparePart Extent5 ON Filter1.PartId = Extent5.Id
                            WHERE (Extent4.Status = {1}) AND (Extent5.Status = {2}) ) Filter2
                            INNER JOIN WarehouseAreaCategory Extent6 ON Filter2.WarehouseAreaCategoryId = Extent6.Id
                            ", (int)DcsBaseDataStatus.有效, (int)DcsBaseDataStatus.有效, (int)DcsBaseDataStatus.有效));
                    //库区用途 不为空 
                    if(warehouseAreaCategoryValue.HasValue) {
                        sql.Append(@" WHERE Extent6.Category = {0}warehouseAreaCategoryValue ");
                        dbParameters.Add(db.CreateDbParameter("warehouseAreaCategoryValue", warehouseAreaCategoryValue));
                    }
                    sql.Append(" ) Filter3 LEFT OUTER JOIN PartsStockBatchDetail Extent7 ON Filter3.Id2 = Extent7.PartsStockId ");
                    //库区用途 不为空 
                    if(warehouseAreaCategoryValue.HasValue) {
                        sql.Append(@" WHERE Filter3.Category = {0}warehouseAreaCategoryValue ");
                        dbParameters.Add(db.CreateDbParameter("warehouseAreaCategoryValue", warehouseAreaCategoryValue));
                    }
                    sql.Append(@" )  Project1 ORDER BY Project1.Id ASC )  top");



                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                   "配件库存Id","仓库Id",ErrorStrings.Export_Title_Company_WarehouseCode,ErrorStrings.Export_Title_Company_WarehouseName,"仓储企业Id","仓储企业类型","营销分公司Id","配件Id",ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode,ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName,ErrorStrings.Export_Title_PackingPropertyApp_MeasureUnit,ErrorStrings.Export_Title_WarehouseAreaCategory_Category,ErrorStrings.Export_Title_WarehouseArea_CodeNew,"库位Id",ErrorStrings.Export_Title_WarehouseArea_WarehouseAreaCode,"库存数量",ErrorStrings.Export_Title_WarehouseArea_Quantity,ErrorStrings.Export_Title_PackingTask_BatchNumber,"标准包装数量","标准包装规格",ErrorStrings.Export_Title_PartsInboundCheckBill_InBoundTime
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }

                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }
    }
}
