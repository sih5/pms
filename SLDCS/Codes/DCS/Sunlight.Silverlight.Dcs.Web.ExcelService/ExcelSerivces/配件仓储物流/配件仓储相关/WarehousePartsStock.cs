﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Globalization;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        //导出仓库库存 
        public bool ExportWarehousePartsStock(int[] ids, int[] warehouseIds, int storageCompanyId, int? warehouseId, string partsName, string[] partsCodes, int? partsSalesCategoryId, int? storageCenter, string referenceCode,bool? isExactExport, out string fileName) {
            fileName = GetExportFilePath("仓库库存_" + ".xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {
                    conn.Open(); //,PartsCode,PartsName,PriceType,SalesPrice,Quantity,UseableQuantity  --,PartsSalesCategoryId
                    var sql = new StringBuilder();
                    string tempSql = @"
        select  warehousecode ,warehousename , (select value from keyvalueitem where name ='Storage_Center' and key= storagecenter) , SparePartCode,SparePartName ,referencecode ,retailguideprice ,
        totalqty ,dfqty,actualqty ,inlockqty ,(nvl(cgqty,0) - nvl(inlockqty,0) - nvl(actualqty,0)) as ShelfQty, (nvl(actualqty,0) - nvl(orderlockqty,0)) as   ActualAvailableStock ,       (case when StockQty is null then  0 when (nvl(actualqty, 0) - nvl(orderlockqty, 0)) > nvl(StockQty, 0) then  nvl(StockQty, 0)  else  (nvl(actualqty, 0) - nvl(orderlockqty, 0)) end) as StockQty
,orderlockqty ,
       (nvl(cgqty,0) - nvl(orderlockqty,0)) as cgqty ,branchcode ,branchname ,Categoryname 
       from(select warehouseid,warehousecode,warehousename, categoryid, Categoryname,storagecenter,SparePartId,SparePartCode, SparePartName, referencecode, retailguideprice,
       storagecompanyid, branchid, branchcode, branchname,zmqty as totalqty,
       (select sum(tmp.quantity)from partsstock tmp inner join warehouseareacategory b on tmp.warehouseareacategoryid = b.id where tmp.partid =a.SparePartId and tmp.warehouseid = a.warehouseid and b.category = 4) as dfqty,
       (select sum(quantity) from partsstock tmp inner join warehouseareacategory b on tmp.warehouseareacategoryid = b.id  where tmp.partid = a.SparePartId and tmp.warehouseid = a.warehouseid and b.category = 1) actualqty,
       (select sum(lockedquantity) from partslockedstock tmp where tmp.partid = a.SparePartId and tmp.warehouseid = a.warehouseid) as orderlockqty,StockQty,
       (select sum(quantity) from partsstock tmp inner join warehouseareacategory b   on tmp.warehouseareacategoryid = b.id where tmp.partid = a.SparePartId and tmp.warehouseid = a.warehouseid and (b.category = 1 or b.category = 3)) cgqty,
       (select sum(tmp.lockedqty)   from partsstock tmp  inner join warehouseareacategory b   on tmp.warehouseareacategoryid = b.id  where tmp.partid = a.SparePartId  and tmp.warehouseid = a.warehouseid and b.category = 3) as inlockqty
  from (select b.id    as warehouseid,  b.code   warehousecode, b.name  warehousename, pcg.id  as categoryid, pcg.name  Categoryname,
               b.storagecenter,  sp.id as SparePartId,  sp.code  SparePartCode, sp.name  SparePartName, nvl(sp.referencecode,sp.code) as referencecode,  f.retailguideprice,
               b.storagecompanyid, b.branchid,  pcg.branchcode,  pcg.branchname, sum(a.quantity) as zmqty,bs.StockQty
          from partsstock a  inner join warehouse b on a.warehouseid = b.id inner join warehouseareacategory c  on c.id = a.warehouseareacategoryid
         inner join sparepart sp on sp.id = a.partid  left join salesunitaffiwarehouse d  on d.warehouseid = b.id left join salesunit e  on e.id = d.salesunitid  left join partssalescategory pcg
        on pcg.id = e.partssalescategoryid left join partsretailguideprice f  on f.partssalescategoryid = pcg.id and f.sparepartid = a.partid  and f.status = 1 
left join BottomStock bs on a.PartId = bs.SparePartId and pcg.id = bs.PartsSalesCategoryId and a.WarehouseId=bs.WarehouseID and b.branchid=bs.CompanyID  and bs.status=1
where c.category <> 2 and a.StorageCompanyId=" + storageCompanyId;
                   
                    var dbParameters = new List<DbParameter>();
                    if(ids != null && ids.Length > 0) {
                        sql.Append(tempSql);
                        sql.Append(" and a.PartId in (");
                        for(var i = 0;i < ids.Length;i++) {
                            if(ids.Length == i + 1) {
                                sql.Append(ids[i]);
                            } else {
                                sql.Append(ids[i].ToString() + ",");
                            }
                        }
                        sql.Append(")");
                        sql.Append(" and a.WarehouseId in (");
                        for (var i = 0; i < warehouseIds.Length; i++)
                        {
                            if (warehouseIds.Length == i + 1)
                            {
                                sql.Append(warehouseIds[i]);
                            }
                            else
                            {
                                sql.Append(warehouseIds[i].ToString() + ",");
                            }
                        }
                        sql.Append(")");
                    } else {                    
                        if (warehouseId != null)
                        {
                            tempSql += "AND (a.WarehouseId = " + warehouseId + ")";
                        }                     
                        if (storageCenter != null)
                        {
                            tempSql += "AND (b.StorageCenter = " + storageCenter + ")";
                        }
                        if (partsCodes != null && partsCodes.Length > 0) {
                            if (isExactExport.HasValue && (bool)isExactExport == true) {
                                if (partsCodes.Length == 1) {
                                    tempSql += "AND (sp.Code ='" + partsCodes[0] + "')";
                                } else {
                                    for (var i = 0; i < partsCodes.Length; i++) {
                                        partsCodes[i] = "'" + partsCodes[i] + "'";
                                    }
                                    string codes = string.Join(",", partsCodes);
                                    tempSql += "AND (sp.Code in (" + codes + "))";
                                }
                            } else {
                                if (partsCodes.Length == 1) {
                                    tempSql += "AND (sp.Code LIKE '%" + partsCodes[0] + "%')";
                                } else {
                                    for (var i = 0; i < partsCodes.Length; i++) {
                                        partsCodes[i] = "'" + partsCodes[i] + "'";
                                    }
                                    string codes = string.Join(",", partsCodes);
                                    tempSql += "AND (sp.Code in (" + codes + "))";
                                }
                            }
                        }
                        if (!string.IsNullOrEmpty(partsName))
                        {
                            tempSql += "AND (sp.Name LIKE '%" + partsName + "%')";
                        }
                        if(!string.IsNullOrEmpty(referenceCode)) {
                            tempSql += "AND (sp.referenceCode LIKE '%" + referenceCode + "%')";
                        }
                        sql.Append(tempSql);
                    }
                    sql.Append(" group by b.id,  b.code, b.name, pcg.id, pcg.name,   b.storagecenter,  sp.id,   sp.code,  sp.name,  sp.referencecode,   f.retailguideprice,  b.storagecompanyid,  b.branchid, pcg.branchcode,pcg.branchname,bs.StockQty) a)b");
                    var command = db.CreateDbCommand(sql.ToString(), conn, null);
                    if(dbParameters.Count > 0) {
                        command.Parameters.AddRange(dbParameters.ToArray());
                    }
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    //ErrorStrings.Export_Title_Company_WarehouseCode, ErrorStrings.Export_Title_Company_WarehouseName,ErrorStrings.Export_Title_Warehouse_StorageCompany, ErrorStrings.Export_Title_PartsBranch_BranchName, ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode, ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName,ErrorStrings.Export_Title_PartsPurchaseOrder_PriceType,ErrorStrings.Export_Title_PartsExchangeGroupExtend_SalesPrice, ErrorStrings.Export_Title_WarehouseArea_Quantity, "可用库存"
                                  ErrorStrings.Export_Title_Company_WarehouseCode, ErrorStrings.Export_Title_Company_WarehouseName,ErrorStrings.Export_Title_Warehouse_StorageCompany, ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode,ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName,ErrorStrings.Export_Title_Accessinfo_Tmp_ReferenceCode,ErrorStrings.Export_Title_PartSstock_RetailGuidePrice,ErrorStrings.Export_Title_PartSstock_BookInventory,ErrorStrings.Export_Title_PartSstock_ShippingQutity,ErrorStrings.Export_Title_PartSstock_ActualInventory,ErrorStrings.Export_Title_PartSstock_UnPackingQty,
                                  ErrorStrings.Export_Title_PartSstock_UnShelvesQty,ErrorStrings.Export_Title_PartSstock_AvailableQty,ErrorStrings.Export_Title_BottomStock_StockQty,ErrorStrings.Export_Title_PartSstock_ApprovedQty,ErrorStrings.Export_Title_PartSstock_PurchaseQty,ErrorStrings.Export_Title_PartSstock_BranchCode, ErrorStrings.Export_Title_PartSstock_BranchName, ErrorStrings.Export_Title_Partssalescategory_Name
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();

                    return true;
                }
            } catch(Exception) {
                return false;
            }
        }
    }
}
