﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Entities;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        public bool 快递公司单据导出(int[] ids, string expressCode, string expressName, int? status, out string fileName) {
            fileName = GetExportFilePath(string.Format("快递公司信息_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var userinfo = Utils.GetCurrentUserInfo();
                var db = DbHelper.GetDbHelp(ConnectionString);
                using(var conn = db.CreateDbConnection()) {
                    var sql = new StringBuilder();
                    sql.AppendFormat(@"SELECT 
                                       ExpressCode,ExpressName,Contacts,ContactsNumber,Address,E_Mail,FixedTelephone,CreatorName,CreateTime,ModifierName,ModifyTime,CancelName,CancelTime 
                                       From Express where 1=1 ");
                    var dbParameters = new List<DbParameter>();
                    if(ids != null && ids.Length > 0) {
                        sql.Append(" and Id in (");
                        for(var i = 0; i < ids.Length; i++) {
                            if(ids.Length == i + 1) {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            } else {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                        }
                        sql.Append(")");
                    } else {
                        if(!string.IsNullOrEmpty(expressCode)) {
                            sql.Append(@" and ExpressCode = :ExpressCode ");
                            dbParameters.Add(db.CreateDbParameter("ExpressCode", expressCode));
                        }
                        if(!string.IsNullOrEmpty(expressName)) {
                            sql.Append(@" and ExpressName = :ExpressName ");
                            dbParameters.Add(db.CreateDbParameter("ExpressName", expressName));
                        }
                        if(status.HasValue) {
                            sql.Append(@" and Status = :Status ");
                            dbParameters.Add(db.CreateDbParameter("Status", status));
                        }
                    }
                    var sqlStr = sql.ToString();
                    if(sqlStr.IndexOf(" where 1=1  and ") > 0)
                        sqlStr = sqlStr.Replace(" where 1=1  and ", " where ");
                    else
                        sqlStr = sqlStr.Replace(" where 1=1 ", "");
                    var command = db.CreateDbCommand(string.Format(sqlStr, db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    conn.Open();
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                       ErrorStrings.Export_Title_AgencyPartsShippingOrder_ExpressCompanyCode,ErrorStrings.Export_Title_AgencyPartsShippingOrder_ExpressCompanyName,ErrorStrings.Export_Title_Agency_LinkMan,"联系人电话",ErrorStrings.Export_Title_Warehouse_Address,"E-Mail",ErrorStrings.Export_Title_Dealer_ContactPhone,ErrorStrings.Export_Title_AccountPeriod_CreatorName,ErrorStrings.Export_Title_AccountPeriod_CreateTime,ErrorStrings.Export_Title_AccountPeriod_ModifierName,ErrorStrings.Export_Title_AccountPeriod_ModifyTime,ErrorStrings.Export_Title_PartsBranch_AbandonerName,ErrorStrings.Export_Title_PartsBranch_AbandonerTime
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }


        public bool 快递公司导入(string fileName, out int excelImportNum, out List<ExpressExtend> rightData, out List<ExpressExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var allList = new List<ExpressExtend>();
            var errorList = new List<ExpressExtend>();
            var rightList = new List<ExpressExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);
                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("Express", out notNullableFields, out fieldLenght);
                List<object> excelColumns;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_AgencyPartsShippingOrder_ExpressCompanyCode, "ExpressCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_AgencyPartsShippingOrder_ExpressCompanyName, "ExpressName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Agency_LinkMan, "Contacts");
                    excelOperator.AddColumnDataSource("联系人电话", "ContactsNumber");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Warehouse_Address, "Address");
                    excelOperator.AddColumnDataSource("E-Mail", "E_Mail");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealer_ContactPhone, "FixedTelephone");
                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var tempImportObj = new ExpressExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        #region
                        tempImportObj.ExpressCode = newRow["ExpressCode"];
                        tempImportObj.ExpressName = newRow["ExpressName"];
                        tempImportObj.Contacts = newRow["Contacts"];
                        tempImportObj.ContactsNumber = newRow["ContactsNumber"];
                        tempImportObj.Address = newRow["Address"];
                        tempImportObj.E_Mail = newRow["E_Mail"];
                        tempImportObj.FixedTelephone = newRow["FixedTelephone"];
                        #endregion
                        var tempErrorMessage = new List<string>();
                        #region 导入的内容基本检查


                        var fieldIndex = notNullableFields.IndexOf("ExpressCode".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.ExpressCode)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_ExpressCode);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.ExpressCode) > fieldLenght["ExpressCode".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_ExpressCodeLong);
                        }

                        fieldIndex = notNullableFields.IndexOf("ExpressName".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.ExpressName)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add("快递公司名称不能为空");
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.ExpressName) > fieldLenght["ExpressName".ToUpper()])
                                tempErrorMessage.Add("快递公司名称过长");
                        }
                        if(!string.IsNullOrEmpty(tempImportObj.E_Mail)) {
                            string strEmail = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
                            Regex regexstEmail = new Regex(strEmail);
                            if(regexstEmail.IsMatch(tempImportObj.E_Mail) == false) {
                                tempErrorMessage.Add("邮件格式错误");
                            }
                        }
                        if(!string.IsNullOrEmpty(tempImportObj.Contacts)) {
                            if(Encoding.Default.GetByteCount(tempImportObj.Contacts) > fieldLenght["Contacts".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_CustomerInformation_ContactPersonIsLong);
                        }
                        if(!string.IsNullOrEmpty(tempImportObj.ContactsNumber)) {
                            if(Encoding.Default.GetByteCount(tempImportObj.ContactsNumber) > fieldLenght["ContactsNumber".ToUpper()])
                                tempErrorMessage.Add("联系人电话过长");
                        }
                        if(!string.IsNullOrEmpty(tempImportObj.Address)) {
                            if(Encoding.Default.GetByteCount(tempImportObj.Address) > fieldLenght["Address".ToUpper()])
                                tempErrorMessage.Add("地址过长");
                        }
                        if(!string.IsNullOrEmpty(tempImportObj.E_Mail)) {
                            if(Encoding.Default.GetByteCount(tempImportObj.E_Mail) > fieldLenght["E_Mail".ToUpper()])
                                tempErrorMessage.Add("E-Mail过长");
                        }
                        if(!string.IsNullOrEmpty(tempImportObj.FixedTelephone)) {
                            if(Encoding.Default.GetByteCount(tempImportObj.FixedTelephone) > fieldLenght["FixedTelephone".ToUpper()])
                                tempErrorMessage.Add("固定电话过长");
                        }
                        #endregion
                        if(tempErrorMessage.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });

                    //校验导入的数据业务校验
                    var groupsNeedCheckUnique = allList.Where(r => r.ErrorMsg == null).GroupBy(r => new {
                        r.ExpressCode,
                    }).Where(r => r.Count() > 1);
                    foreach(var group in groupsNeedCheckUnique) {
                        foreach(var groupItem in group) {
                            if(groupItem.ErrorMsg == null) {
                                groupItem.ErrorMsg = ErrorStrings.ImpPartsSupplierRelation_Validation31;
                            } else {
                                groupItem.ErrorMsg = groupItem.ErrorMsg + ";文件内数据重复";
                            }
                        }
                    }

                    //获取所有不合格数据
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    rightList = allList.Except(errorList).ToList();
                    //导出所有不合格数据
                    if(errorList.Any()) {
                        excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                        errorDataFileName = GetErrorFilePath(fileName);
                        using(var excelExport = new ExcelExport(errorDataFileName)) {
                            var list = errorList;
                            excelExport.ExportByRow(index => {
                                if(index == list.Count + 1)
                                    return null;
                                if(index == 0)
                                    return excelColumns.ToArray();
                                var tempObj = list[index - 1];
                                var values = new object[] {
                                tempObj.ExpressCode ,
                                tempObj.ExpressName ,
                                tempObj.Contacts,
                                tempObj.ContactsNumber ,
                                tempObj.Address ,
                                tempObj.E_Mail,
                                tempObj.FixedTelephone,
                                tempObj.ErrorMsg
                            };
                                return values;
                            });
                        }
                        errorList = null;
                    }

                    //更新导入所有合格数据
                    if(!rightList.Any())
                        return true;
                    using(var conn = db.CreateDbConnection()) {
                        conn.Open();
                        //开启事务，新增更新配件在一个事务内
                        var ts = conn.BeginTransaction();
                        try {
                            //新增配件
                            if(rightList.Any()) {
                                var expressCodes = new List<string>();
                                foreach(var item in rightList) {
                                    expressCodes.Add(item.ExpressCode);
                                }
                                var codeStr = string.Join("','", expressCodes.ToArray());
                                var codeSql = string.Format("select ExpressCode from Express where ExpressCode in ('{0}') and Status=1", codeStr);
                                var cmdexpressCodes = db.CreateDbCommand(codeSql, conn, null);
                                DataTable tbExpresses = new DataTable();
                                tbExpresses.Load(cmdexpressCodes.ExecuteReader());


                                var sqlInsertExpress = db.GetInsertSql("Express", "Id", new[] {
                                    "ExpressCode","ExpressName","Contacts","ContactsNumber","FixedTelephone","E_Mail","Status","Address","CreatorId","CreatorName","CreateTime"
                                });
                                var userInfo = Utils.GetCurrentUserInfo();
                                //往数据库增加配件信息
                                foreach(var item in rightList) {
                                    var trExpresses = tbExpresses.Select(" ExpressCode='" + item.ExpressCode + "'");
                                    if(trExpresses.Length == 0) {
                                        var command = db.CreateDbCommand(sqlInsertExpress, conn, ts);
                                        command.Parameters.Add(db.CreateDbParameter("ExpressCode", item.ExpressCode));
                                        command.Parameters.Add(db.CreateDbParameter("ExpressName", item.ExpressName));
                                        command.Parameters.Add(db.CreateDbParameter("Contacts", item.Contacts));
                                        command.Parameters.Add(db.CreateDbParameter("ContactsNumber", item.ContactsNumber));
                                        command.Parameters.Add(db.CreateDbParameter("FixedTelephone", item.FixedTelephone));
                                        command.Parameters.Add(db.CreateDbParameter("E_Mail", item.E_Mail));
                                        command.Parameters.Add(db.CreateDbParameter("Address", item.Address));
                                        command.Parameters.Add(db.CreateDbParameter("Status", (int)DcsMasterDataStatus.有效));
                                        command.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                        command.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                        command.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                        command.ExecuteNonQuery();
                                    } else {
                                        List<string> fields = new List<string>();
                                        fields.Add("ModifyTime");
                                        fields.Add("ModifierId");
                                        fields.Add("ModifierName");
                                        string sqlUpdateExpress = null;
                                        var command = db.CreateDbCommand(sqlUpdateExpress, conn, ts);
                                        if(!string.IsNullOrEmpty(item.ExpressName)) {
                                            fields.Add("ExpressName");
                                            command.Parameters.Add(db.CreateDbParameter("ExpressName", item.ExpressName));
                                        }
                                        if(!string.IsNullOrEmpty(item.Contacts)) {
                                            fields.Add("Contacts");
                                            command.Parameters.Add(db.CreateDbParameter("Contacts", item.Contacts));
                                        }
                                        if(!string.IsNullOrEmpty(item.ContactsNumber)) {
                                            fields.Add("ContactsNumber");
                                            command.Parameters.Add(db.CreateDbParameter("ContactsNumber", item.ContactsNumber));
                                        }
                                        if(!string.IsNullOrEmpty(item.FixedTelephone)) {
                                            fields.Add("FixedTelephone");
                                            command.Parameters.Add(db.CreateDbParameter("FixedTelephone", item.FixedTelephone));
                                        }
                                        if(!string.IsNullOrEmpty(item.E_Mail)) {
                                            fields.Add("E_Mail");
                                            command.Parameters.Add(db.CreateDbParameter("E_Mail", item.E_Mail));
                                        }
                                        if(!string.IsNullOrEmpty(item.Address)) {
                                            fields.Add("Address");
                                            command.Parameters.Add(db.CreateDbParameter("Address", item.Address));
                                        }
                                        sqlUpdateExpress = db.GetUpdateSql("Express", fields.ToArray(), new[] { "ExpressCode" });
                                        command.Parameters.Add(db.CreateDbParameter("ExpressCode", item.ExpressCode));
                                        command.Parameters.Add(db.CreateDbParameter("ModifyTime", DateTime.Now));
                                        command.Parameters.Add(db.CreateDbParameter("ModifierId", userInfo.Id));
                                        command.Parameters.Add(db.CreateDbParameter("ModifierName", userInfo.Name));
                                        command.CommandText = sqlUpdateExpress;
                                        command.ExecuteNonQuery();
                                    }

                                }
                            }
                            //无异常提交
                            ts.Commit();
                        } catch(Exception) {
                            //报错回滚
                            ts.Rollback();
                            throw;
                        } finally {
                            if(conn.State == System.Data.ConnectionState.Open) {
                                conn.Close();
                            }
                        }
                    }
                    return true;
                }
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }
    }

}
