﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Entities;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        public bool 导出快递物流关系(int[] ids, int? focufingCore, string logisticsCompanyCode, string logisticsCompanyName, string expressCode, string expressName, out string fileName) {
            fileName = GetExportFilePath(string.Format("快递物流关系_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var userinfo = Utils.GetCurrentUserInfo();
                var db = DbHelper.GetDbHelp(ConnectionString);
                using(var conn = db.CreateDbConnection()) {
                    var sql = new StringBuilder();
                    sql.AppendFormat(@"SELECT 
                                       ExpressCode,ExpressName,FocufingCoreName,LogisticsCompanyCode,LogisticsCompanyName,CreatorName,CreateTime,ModifierName,ModifyTime,CancelName,CancelTime 
                                       From ExpressToLogistics where 1=1 ");
                    var dbParameters = new List<DbParameter>();
                    if(ids != null && ids.Length > 0) {
                        sql.Append(" and Id in (");
                        for(var i = 0; i < ids.Length; i++) {
                            if(ids.Length == i + 1) {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            } else {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                        }
                        sql.Append(")");
                    } else {
                        if(focufingCore.HasValue) {
                            sql.Append(@" and FocufingCoreId = :focufingCore ");
                            dbParameters.Add(db.CreateDbParameter("focufingCore", focufingCore));
                        }
                        if(!string.IsNullOrEmpty(logisticsCompanyCode)) {
                            sql.Append(@" and LogisticsCompanyCode = :logisticsCompanyCode ");
                            dbParameters.Add(db.CreateDbParameter("logisticsCompanyCode", logisticsCompanyCode));
                        }
                        if(!string.IsNullOrEmpty(logisticsCompanyName)) {
                            sql.Append(@" and LogisticsCompanyName = :logisticsCompanyName ");
                            dbParameters.Add(db.CreateDbParameter("logisticsCompanyName", logisticsCompanyName));
                        }
                        if(!string.IsNullOrEmpty(expressCode)) {
                            sql.Append(@" and ExpressCode = :expressCode ");
                            dbParameters.Add(db.CreateDbParameter("expressCode", expressCode));
                        }
                        if(!string.IsNullOrEmpty(expressName)) {
                            sql.Append(@" and ExpressName = :expressName ");
                            dbParameters.Add(db.CreateDbParameter("expressName", expressName));
                        }
                    }
                    var sqlStr = sql.ToString();
                    if(sqlStr.IndexOf(" where 1=1  and ") > 0)
                        sqlStr = sqlStr.Replace(" where 1=1  and ", " where ");
                    else
                        sqlStr = sqlStr.Replace(" where 1=1 ", "");
                    var command = db.CreateDbCommand(string.Format(sqlStr, db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    conn.Open();
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                       ErrorStrings.Export_Title_AgencyPartsShippingOrder_ExpressCompanyCode,ErrorStrings.Export_Title_AgencyPartsShippingOrder_ExpressCompanyName,ErrorStrings.Export_Title_Warehouse_StorageCompany,ErrorStrings.Export_Title_AgencyPartsShippingOrder_LogisticCompanyCode,ErrorStrings.Export_Title_AgencyPartsShippingOrder_LogisticCompanyName,ErrorStrings.Export_Title_AccountPeriod_CreatorName,ErrorStrings.Export_Title_AccountPeriod_CreateTime,ErrorStrings.Export_Title_AccountPeriod_ModifierName,ErrorStrings.Export_Title_AccountPeriod_ModifyTime,ErrorStrings.Export_Title_PartsBranch_AbandonerName,ErrorStrings.Export_Title_PartsBranch_AbandonerTime
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }

        public bool 快递物流关系导入(string fileName, out int excelImportNum, out List<ExpressToLogisticExtend> rightData, out List<ExpressToLogisticExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var allList = new List<ExpressToLogisticExtend>();
            var errorList = new List<ExpressToLogisticExtend>();
            var rightList = new List<ExpressToLogisticExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);
                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("ExpressToLogistics", out notNullableFields, out fieldLenght);
                List<object> excelColumns;

                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_AgencyPartsShippingOrder_ExpressCompanyCode, "ExpressCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Warehouse_StorageCompany, "FocufingCoreName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_AgencyPartsShippingOrder_LogisticCompanyCode, "LogisticsCompanyCode");
                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;
                    var keyValuePairs = new[] {
                                new KeyValuePair<string, string>("FocufingCoreName", "Storage_Center"),
                            };
                    tempExcelOperator.ImportHelper.LoadEnumFromDb(null, null, keyValuePairs);
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var tempImportObj = new ExpressToLogisticExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        #region
                        tempImportObj.ExpressCode = newRow["ExpressCode"];
                        tempImportObj.FocufingCoreName = newRow["FocufingCoreName"];
                        tempImportObj.LogisticsCompanyCode = newRow["LogisticsCompanyCode"];
                        #endregion
                        var tempErrorMessage = new List<string>();
                        #region 导入的内容基本检查


                        var fieldIndex = notNullableFields.IndexOf("ExpressCode".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.ExpressCode)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_ExpressCode);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.ExpressCode) > fieldLenght["ExpressCode".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_ExpressCodeLong);
                        }

                        fieldIndex = notNullableFields.IndexOf("FocufingCoreName".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.FocufingCoreName)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add("储运中心不能为空");
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.FocufingCoreName) > fieldLenght["FocufingCoreName".ToUpper()])
                                tempErrorMessage.Add("储运中心过长");
                            var tempStatus = tempExcelOperator.ImportHelper.GetEnumValue("FocufingCoreName", tempImportObj.FocufingCoreName.ToLower());
                            if(!tempStatus.HasValue) {
                                tempErrorMessage.Add("储运中心不正确");
                            } else {
                                tempImportObj.FocufingCoreId = tempStatus.Value;
                                switch(tempImportObj.FocufingCoreName) {
                                    case "北京CDC":
                                        tempImportObj.FocufingCoreCode = "BJ-A";
                                        break;
                                    case "山东CDC":
                                        tempImportObj.FocufingCoreCode = "SD";
                                        break;
                                    case "广东CDC":
                                        tempImportObj.FocufingCoreCode = "GD";
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }

                        fieldIndex = notNullableFields.IndexOf("LogisticsCompanyCode".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.LogisticsCompanyCode)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add("物流公司编号不能为空");
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.LogisticsCompanyCode) > fieldLenght["LogisticsCompanyCode".ToUpper()])
                                tempErrorMessage.Add("物流公司编号过长");
                        }


                        #endregion
                        if(tempErrorMessage.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });

                    //校验导入的数据业务校验
                    var groupsNeedCheckUnique = allList.Where(r => r.ErrorMsg == null).GroupBy(r => new {
                        r.ExpressCode,
                        r.FocufingCoreName
                    }).Where(r => r.Count() > 1);
                    foreach(var group in groupsNeedCheckUnique) {
                        foreach(var groupItem in group) {
                            if(groupItem.ErrorMsg == null) {
                                groupItem.ErrorMsg = ErrorStrings.ImpPartsSupplierRelation_Validation31;
                            } else {
                                groupItem.ErrorMsg = groupItem.ErrorMsg + ";文件内数据重复";
                            }
                        }
                    }

                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    var tempexpresscode = tempRightList.Select(r => r.ExpressCode.ToUpper()).Distinct().ToArray();
                    var dbExpresses = new List<ExpressToLogisticExtend>();
                    Func<string[], bool> getDbExpress = value => {
                        var dbExpress = new ExpressToLogisticExtend {
                            ExpressId = Convert.ToInt32(value[0]),
                            ExpressCode = value[1],
                            ExpressName = value[2]
                        };
                        dbExpresses.Add(dbExpress);
                        return false;
                    };
                    db.QueryDataWithInOperator(@"select Id,ExpressCode,ExpressName from Express where status=1 ", "ExpressCode", true, tempexpresscode, getDbExpress);
                    var errorExpress = tempRightList.Where(r => dbExpresses.All(v => r.ExpressCode != v.ExpressCode)).ToList();
                    foreach(var errorItem in errorExpress) {
                        if(errorItem.ErrorMsg == null) {
                            errorItem.ErrorMsg = "快递公司不存在";
                        } else {
                            errorItem.ErrorMsg = errorItem.ErrorMsg + ";快递公司不存在";
                        }
                    }

                    var templogisticsCompanyCode = tempRightList.Select(r => r.LogisticsCompanyCode.ToUpper()).Distinct().ToArray();
                    var dbLogisticsCompanyCodes = new List<ExpressToLogisticExtend>();
                    Func<string[], bool> getDbLogisticsCompanyCode = value => {
                        var dbLogisticsCompanyCode = new ExpressToLogisticExtend {
                            LogisticsCompanyId = Convert.ToInt32(value[0]),
                            LogisticsCompanyCode = value[1],
                            LogisticsCompanyName = value[2]
                        };
                        dbLogisticsCompanyCodes.Add(dbLogisticsCompanyCode);
                        return false;
                    };
                    db.QueryDataWithInOperator(@"select Id,Code,Name from LogisticCompany where Status=1 ", "Code", true, templogisticsCompanyCode, getDbLogisticsCompanyCode);
                    var errorLogisticsCompany = tempRightList.Where(r => dbLogisticsCompanyCodes.All(v => r.LogisticsCompanyCode.ToUpper() != v.LogisticsCompanyCode.ToUpper())).ToList();
                    foreach(var errorItem in errorLogisticsCompany) {
                        if(errorItem.ErrorMsg == null) {
                            errorItem.ErrorMsg = "物流公司不存在";
                        } else {
                            errorItem.ErrorMsg = errorItem.ErrorMsg + ";物流公司不存在";
                        }
                    }

                    //获取所有不合格数据
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    rightList = allList.Except(errorList).ToList();
                    //导出所有不合格数据
                    if(errorList.Any()) {
                        excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                        errorDataFileName = GetErrorFilePath(fileName);
                        using(var excelExport = new ExcelExport(errorDataFileName)) {
                            var list = errorList;
                            excelExport.ExportByRow(index => {
                                if(index == list.Count + 1)
                                    return null;
                                if(index == 0)
                                    return excelColumns.ToArray();
                                var tempObj = list[index - 1];
                                var values = new object[] {
                                tempObj.ExpressCode ,
                                tempObj.FocufingCoreName ,
                                tempObj.LogisticsCompanyCode ,
                                tempObj.ErrorMsg
                            };
                                return values;
                            });
                        }
                        errorList = null;
                    }

                    //更新导入所有合格数据
                    if(!rightList.Any())
                        return true;
                    using(var conn = db.CreateDbConnection()) {
                        conn.Open();
                        //开启事务，新增更新配件在一个事务内
                        var ts = conn.BeginTransaction();
                        try {
                            //新增配件
                            if(rightList.Any()) {
                                var expressCodes = new List<string>();
                                var focufingCoreNames = new List<string>();
                                foreach(var item in rightList) {
                                    expressCodes.Add(item.ExpressCode);
                                    focufingCoreNames.Add(item.FocufingCoreName);
                                }
                                var codeStr = string.Join("','", expressCodes.ToArray());
                                var nameStr = string.Join("','", focufingCoreNames.ToArray());
                                var codeSql = string.Format("select  ExpressCode ,FocufingCoreName  from ExpressToLogistics where ExpressCode in ('{0}') and  FocufingCoreName in ('{1}') and Status=1", codeStr, nameStr);
                                var cmdexpressCodes = db.CreateDbCommand(codeSql, conn, null);
                                DataTable tbExpresses = new DataTable();
                                tbExpresses.Load(cmdexpressCodes.ExecuteReader());


                                var sqlInsertExpress = db.GetInsertSql("ExpressToLogistics", "Id", new[] {
                                    "ExpressId","ExpressCode","ExpressName","LogisticsCompanyId","LogisticsCompanyCode","LogisticsCompanyName","Status","FocufingCoreId","FocufingCoreCode","FocufingCoreName","CreatorId","CreatorName","CreateTime"
                                });
                                string sqlUpdateExpress = db.GetUpdateSql("ExpressToLogistics",
                                            new[] { "LogisticsCompanyId", "LogisticsCompanyCode", "LogisticsCompanyName", "ModifyTime", "ModifierId", "ModifierName" }
                                            , new[] { "ExpressCode", "FocufingCoreName" });
                                var userInfo = Utils.GetCurrentUserInfo();
                                //往数据库增加配件信息
                                foreach(var item in rightList) {
                                    var trExpresses = tbExpresses.Select(" ExpressCode='" + item.ExpressCode + "' and FocufingCoreName='" + item.FocufingCoreName + "'");
                                    if(trExpresses.Length == 0) {
                                        var express = dbExpresses.Where(r => r.ExpressCode.ToUpper() == item.ExpressCode.ToUpper()).FirstOrDefault();
                                        var logisticsCompany = dbLogisticsCompanyCodes.Where(r => r.LogisticsCompanyCode.ToUpper() == item.LogisticsCompanyCode.ToUpper()).FirstOrDefault();
                                        var command = db.CreateDbCommand(sqlInsertExpress, conn, ts);
                                        command.Parameters.Add(db.CreateDbParameter("ExpressId", express.ExpressId));
                                        command.Parameters.Add(db.CreateDbParameter("ExpressCode", item.ExpressCode));
                                        command.Parameters.Add(db.CreateDbParameter("ExpressName", express.ExpressName));

                                        command.Parameters.Add(db.CreateDbParameter("LogisticsCompanyId", logisticsCompany.LogisticsCompanyId));
                                        command.Parameters.Add(db.CreateDbParameter("LogisticsCompanyCode", item.LogisticsCompanyCode));
                                        command.Parameters.Add(db.CreateDbParameter("LogisticsCompanyName", logisticsCompany.LogisticsCompanyName));

                                        command.Parameters.Add(db.CreateDbParameter("FocufingCoreId", item.FocufingCoreId));
                                        command.Parameters.Add(db.CreateDbParameter("FocufingCoreCode", item.FocufingCoreCode));
                                        command.Parameters.Add(db.CreateDbParameter("FocufingCoreName", item.FocufingCoreName));

                                        command.Parameters.Add(db.CreateDbParameter("Status", (int)DcsMasterDataStatus.有效));
                                        command.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                        command.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                        command.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                        command.ExecuteNonQuery();
                                    } else {
                                        var logisticsCompany = dbLogisticsCompanyCodes.Where(r => r.LogisticsCompanyCode.ToUpper() == item.LogisticsCompanyCode.ToUpper()).FirstOrDefault();

                                        var command = db.CreateDbCommand(sqlUpdateExpress, conn, ts);
                                        command.Parameters.Add(db.CreateDbParameter("ExpressCode", item.ExpressCode));
                                        command.Parameters.Add(db.CreateDbParameter("LogisticsCompanyId", logisticsCompany.LogisticsCompanyId));
                                        command.Parameters.Add(db.CreateDbParameter("LogisticsCompanyCode", item.LogisticsCompanyCode));
                                        command.Parameters.Add(db.CreateDbParameter("LogisticsCompanyName", logisticsCompany.LogisticsCompanyName));
                                        command.Parameters.Add(db.CreateDbParameter("FocufingCoreName", item.FocufingCoreName));
                                        command.Parameters.Add(db.CreateDbParameter("ModifyTime", DateTime.Now));
                                        command.Parameters.Add(db.CreateDbParameter("ModifierId", userInfo.Id));
                                        command.Parameters.Add(db.CreateDbParameter("ModifierName", userInfo.Name));
                                        command.ExecuteNonQuery();
                                    }

                                }
                            }
                            //无异常提交
                            ts.Commit();
                        } catch(Exception) {
                            //报错回滚
                            ts.Rollback();
                            throw;
                        } finally {
                            if(conn.State == System.Data.ConnectionState.Open) {
                                conn.Close();
                            }
                        }
                    }
                    return true;
                }
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }

    }
}
