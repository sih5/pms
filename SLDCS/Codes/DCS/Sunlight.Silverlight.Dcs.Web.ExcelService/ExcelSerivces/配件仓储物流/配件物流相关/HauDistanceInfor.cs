﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        public bool ExportHauDistanceInfor(string companyCode, string companyName, string storageCompanyCode, string storageCompanyName, string warehouseCode, string warehouseName, DateTime? createTimeBegin, DateTime? createTimeEnd, int? status, out string fileName) {
            fileName = GetExportFilePath("运距信息_" + ".xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.Append(@"select    b.code as StorageCompanyCode,
                                           b.name as StorageCompanyName,
                                           d.name as WarehouseName,
                                           c.code as CompanyCode,
                                           c.name as CompanyName,
                                           e.DetailAddress,
                                           a.HauDistance,
                                           (select value from keyvalueitem where NAME = 'MasterData_Status'and key=a.Status) As Status,
                                           a.CreatorName,
                                           a.CreateTime,
                                           a.ModifierName,
                                           a.ModifyTime,
                                           a.AbandonerName,
                                           a.AbandonTime
                                      from HauDistanceInfor a
                                     left join company b
                                        on a.storagecompanyid = b.id
                                     left join company c
                                        on a.companyid = c.id
                                     left join warehouse d
                                        on a.warehouseid = d.id
                                     left join companyAddress e
                                        on a.companyaddressid = e.id
                                     where 1 = 1");
                    var dbParameters = new List<DbParameter>();
                    if(!String.IsNullOrEmpty(companyCode)) {
                        sql.Append(@" and c.code like {0}companyCode ");
                        dbParameters.Add(db.CreateDbParameter("companyCode", "%" + companyCode + "%"));
                    }
                    if(!String.IsNullOrEmpty(companyName)) {
                        sql.Append(@" and c.name like {0}companyName ");
                        dbParameters.Add(db.CreateDbParameter("companyName", "%" + companyName + "%"));
                    }

                    if(!String.IsNullOrEmpty(storageCompanyCode)) {
                        sql.Append(@" and b.code like {0}storageCompanyCode ");
                        dbParameters.Add(db.CreateDbParameter("storageCompanyCode", "%" + storageCompanyCode + "%"));
                    }
                    if(!String.IsNullOrEmpty(storageCompanyName)) {
                        sql.Append(@" and b.name like {0}storageCompanyName ");
                        dbParameters.Add(db.CreateDbParameter("storageCompanyName", "%" + storageCompanyName + "%"));
                    }
                    if(!String.IsNullOrEmpty(warehouseCode)) {
                        sql.Append(@" and d.code like {0}warehouseCode ");
                        dbParameters.Add(db.CreateDbParameter("warehouseCode", "%" + warehouseCode + "%"));
                    }
                    if(!String.IsNullOrEmpty(warehouseName)) {
                        sql.Append(@" and d.name like {0}warehouseName ");
                        dbParameters.Add(db.CreateDbParameter("warehouseName", "%" + warehouseName + "%"));
                    }
                    if(createTimeBegin.HasValue) {
                        sql.Append(@" and a.createTime >=to_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                        var tempValue = createTimeBegin.Value;
                        var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                        dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                    }
                    if(createTimeEnd.HasValue) {
                        sql.Append(@" and a.createTime <=to_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                        var tempValue = createTimeEnd.Value;
                        var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                        dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                    }
                    if(status.HasValue) {
                        sql.Append(@" and a.status={0}status ");
                        dbParameters.Add(db.CreateDbParameter("status", status.Value));
                    }
                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                   ErrorStrings.Export_Title_PartsInboundCheckBill_StorageCompanyCode,ErrorStrings.Export_Title_Partshistorystock_StorageCompanyName,ErrorStrings.Export_Title_Company_WarehouseName,ErrorStrings.Export_Title_Credenceapplication_CompanyCode,ErrorStrings.Export_Title_Credenceapplication_CompanyName,ErrorStrings.Export_Title_PartsPurchaseOrder_ReceiveAddress,"运距",ErrorStrings.Export_Title_AccountPeriod_Status,ErrorStrings.Export_Title_AccountPeriod_CreatorName,ErrorStrings.Export_Title_AccountPeriod_CreateTime,ErrorStrings.Export_Title_AccountPeriod_ModifierName,ErrorStrings.Export_Title_AccountPeriod_ModifyTime,ErrorStrings.Export_Title_PartsBranch_AbandonerName,ErrorStrings.Export_Title_PartsBranch_AbandonerTime
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }

                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }


        public bool ImportHauDistanceInfor(string fileName, out int excelImportNum, out List<HauDistanceInforExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<HauDistanceInforExtend>();
            var allList = new List<HauDistanceInforExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("HauDistanceInfor", out notNullableFields, out fieldLenght);

                List<string> notNullableFieldsCompany;
                Dictionary<string, int> fieldLenghtCompany;
                db.GetTableSchema("Company", out notNullableFieldsCompany, out fieldLenghtCompany);

                List<string> notNullableFieldsWarehouse;
                Dictionary<string, int> fieldLenghtWarehouse;
                db.GetTableSchema("Warehouse", out notNullableFieldsWarehouse, out fieldLenghtWarehouse);

                List<string> notNullableFieldsCompanyAddress;
                Dictionary<string, int> fieldLenghtCompanyAddress;
                db.GetTableSchema("CompanyAddress", out notNullableFieldsCompanyAddress, out fieldLenghtCompanyAddress);
                List<object> excelColumns;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsInboundCheckBill_StorageCompanyCode, "StorageCompanyCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Partshistorystock_StorageCompanyName, "StorageCompanyName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Company_WarehouseName, "WarehouseName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Credenceapplication_CompanyCode, "CompanyCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Credenceapplication_CompanyName, "CompanyName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsPurchaseOrder_ReceiveAddress, "DetailAddress");
                    excelOperator.AddColumnDataSource("运距", "HauDistance");
                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;//不加这句就会报警告
                    var keyValuePairs = new[] { new KeyValuePair<string, string>("Status", "BaseData_Status") };
                    tempExcelOperator.ImportHelper.LoadEnumFromDb(null, null, keyValuePairs);
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var hauDistanceInfor = new HauDistanceInforExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        hauDistanceInfor.StorageCompanyCode = newRow["StorageCompanyCode"];
                        hauDistanceInfor.StorageCompanyName = newRow["StorageCompanyName"];
                        hauDistanceInfor.WarehouseName = newRow["WarehouseName"];
                        hauDistanceInfor.CompanyCode = newRow["CompanyCode"];
                        hauDistanceInfor.CompanyName = newRow["CompanyName"];
                        hauDistanceInfor.DetailAddress = newRow["DetailAddress"];
                        hauDistanceInfor.HauDistanceStr = newRow["HauDistance"];
                        var tempErrorMessage = new List<string>();
                        //仓储企业编号检查
                        var fieldIndex = notNullableFields.IndexOf("StorageCompanyCode".ToUpper());
                        if(string.IsNullOrEmpty(hauDistanceInfor.StorageCompanyCode)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add("仓储企业编号不能为空");
                        } else {
                            if(Encoding.Default.GetByteCount(hauDistanceInfor.StorageCompanyCode) > fieldLenghtCompany["Code".ToUpper()])
                                tempErrorMessage.Add("仓储企业编号过长");
                        }
                        //仓储企业名称检查
                        fieldIndex = notNullableFields.IndexOf("StorageCompanyName".ToUpper());
                        if(string.IsNullOrEmpty(hauDistanceInfor.StorageCompanyName)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add("仓储企业名称不能为空");
                        } else {
                            if(Encoding.Default.GetByteCount(hauDistanceInfor.StorageCompanyName) > fieldLenghtCompany["Name".ToUpper()])
                                tempErrorMessage.Add("仓储企业名称过长");
                        }
                        //仓库名称
                        fieldIndex = notNullableFields.IndexOf("WarehouseName".ToUpper());
                        if(string.IsNullOrEmpty(hauDistanceInfor.WarehouseName)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsPurchaseOrder_WarehouseNameIsNull);
                        } else {
                            if(Encoding.Default.GetByteCount(hauDistanceInfor.WarehouseName) > fieldLenghtWarehouse["Name".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsPurchaseOrder_WarehouseNameLong);
                        }
                        //客户企业编号检查
                        fieldIndex = notNullableFields.IndexOf("CompanyCode".ToUpper());
                        if(string.IsNullOrEmpty(hauDistanceInfor.CompanyCode)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validarion_CustomerOrderPriceGrade_CustomerCompanyCodeIsNull);
                        } else {
                            if(Encoding.Default.GetByteCount(hauDistanceInfor.CompanyCode) > fieldLenghtCompany["Code".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validarion_CustomerOrderPriceGrade_CustomerCompanyCodeIsLong);
                        }
                        //客户企业名称检查
                        fieldIndex = notNullableFields.IndexOf("CompanyName".ToUpper());
                        if(string.IsNullOrEmpty(hauDistanceInfor.CompanyName)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add("客户企业名称不能为空");
                        } else {
                            if(Encoding.Default.GetByteCount(hauDistanceInfor.CompanyName) > fieldLenghtCompany["Name".ToUpper()])
                                tempErrorMessage.Add("客户企业名称过长");
                        }
                        //收货地址检查
                        fieldIndex = notNullableFields.IndexOf("DetailAddress".ToUpper());
                        if(string.IsNullOrEmpty(hauDistanceInfor.DetailAddress)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add("收货地址不能为空");
                        } else {
                            if(Encoding.Default.GetByteCount(hauDistanceInfor.DetailAddress) > fieldLenghtCompanyAddress["DetailAddress".ToUpper()])
                                tempErrorMessage.Add("收货地址过长");
                        }
                        //运距检查
                        fieldIndex = notNullableFields.IndexOf("HauDistance".ToUpper());
                        if(string.IsNullOrEmpty(hauDistanceInfor.HauDistanceStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add("运距不能为空");
                        } else {
                            try {
                                var hauDistance = Convert.ToInt32(row["HauDistance"]);
                                if(hauDistance <= 0) {
                                    throw new Exception();
                                }
                                hauDistanceInfor.HauDistance = hauDistance;
                            } catch(Exception) {
                                tempErrorMessage.Add("运距必须是正整数");
                            }
                        }

                        if(tempErrorMessage.Count > 0) {
                            hauDistanceInfor.ErrorMsg = string.Join("; ", tempErrorMessage);
                        } else {
                            var tempStatus = tempExcelOperator.ImportHelper.GetEnumValue("Status", ErrorStrings.Export_Validation_PartsBranch_Used);
                            if(tempStatus.HasValue) {
                                hauDistanceInfor.Status = tempStatus.Value;
                            }
                        }
                        allList.Add(hauDistanceInfor);
                        return false;
                    });
                    //校验导入的数据业务校验
                    var groupsNeedCheckUnique = allList.Where(r => r.ErrorMsg == null).GroupBy(r => new {
                        r.StorageCompanyCode,
                        r.StorageCompanyName,
                        r.WarehouseName,
                        r.CompanyCode,
                        r.CompanyName,
                        r.DetailAddress,
                    }).Where(r => r.Count() > 1);
                    foreach(var group in groupsNeedCheckUnique) {
                        foreach(var groupItem in group) {
                            if(groupItem.ErrorMsg == null) {
                                groupItem.ErrorMsg = ErrorStrings.ImpPartsSupplierRelation_Validation31;
                            } else {
                                groupItem.ErrorMsg = groupItem.ErrorMsg + ";文件内数据重复";
                            }
                        }
                    }
                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    var storageCompanyCodeNeedCheck = tempRightList.Select(r => r.StorageCompanyCode).Distinct().ToArray();
                    var companyCodeNeedCheck = tempRightList.Select(r => r.CompanyCode).Distinct().ToArray();
                    var tempCompanyNeedCheck = storageCompanyCodeNeedCheck.Union(companyCodeNeedCheck).Distinct().ToArray();
                    var dbHauDistanceInforForCompany = new List<HauDistanceInforExtend>();
                    Func<string[], bool> getDbHauDistanceInforForCompany = value => {
                        var dbHauDistanceInfor = new HauDistanceInforExtend {
                            CompanyId = Convert.ToInt32(value[0]),
                            CompanyCode = value[1],
                            CompanyName = value[2]
                        };
                        dbHauDistanceInforForCompany.Add(dbHauDistanceInfor);
                        return false;
                    };
                    db.QueryDataWithInOperator(@"select a.Id,a.code, a.name from company a where a.status=1", "a.Code", true, tempCompanyNeedCheck, getDbHauDistanceInforForCompany);
                    var errorCompanys = tempRightList.Where(r => dbHauDistanceInforForCompany.All(v => r.CompanyCode != v.CompanyCode)).ToList();
                    foreach(var errorItem in errorCompanys) {
                        if(errorItem.ErrorMsg == null) {
                            errorItem.ErrorMsg = ErrorStrings.Export_Validation_Company_CodeNotExist;
                        } else {
                            errorItem.ErrorMsg = errorItem.ErrorMsg + ";企业编号不存在";
                        }
                    }
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();
                    var errorCompanyName = tempRightList.Where(r => dbHauDistanceInforForCompany.Any(v => r.CompanyCode == v.CompanyCode && r.CompanyName != v.CompanyName)).ToList();
                    foreach(var errorItem in errorCompanyName) {
                        if(errorItem.ErrorMsg == null) {
                            errorItem.ErrorMsg = "企业名称与编号不匹配";
                        } else {
                            errorItem.ErrorMsg = errorItem.ErrorMsg + ";企业名称与编号不匹配";
                        }
                    }
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();
                    var errorStorageCompany = tempRightList.Where(r => dbHauDistanceInforForCompany.All(v => r.StorageCompanyCode != v.CompanyCode)).ToList();
                    foreach(var errorItem in errorStorageCompany) {
                        if(errorItem.ErrorMsg == null) {
                            errorItem.ErrorMsg = "仓储企业编号不存在";
                        } else {
                            errorItem.ErrorMsg = errorItem.ErrorMsg + ";仓储企业编号不存在";
                        }
                    }
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();
                    var errorStorageCompanyName = tempRightList.Where(r => dbHauDistanceInforForCompany.Any(v => r.StorageCompanyCode == v.CompanyCode && r.StorageCompanyName != v.CompanyName)).ToList();
                    foreach(var errorItem in errorStorageCompanyName) {
                        if(errorItem.ErrorMsg == null) {
                            errorItem.ErrorMsg = "仓储企业名称与编号不匹配";
                        } else {
                            errorItem.ErrorMsg = errorItem.ErrorMsg + ";仓储企业名称与编号不匹配";
                        }
                    }
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();
                    tempCompanyNeedCheck = tempRightList.Select(r => r.CompanyCode).ToArray();
                    var dbHauDistanceInforForAddress = new List<HauDistanceInforExtend>();
                    Func<string[], bool> getDbHauDistanceInforForAddress = value => {
                        var dbHauDistanceInfor = new HauDistanceInforExtend {
                            CompanyCode = value[0],
                            CompanyAddressId = Convert.ToInt32(value[1]),
                            DetailAddress = value[2]
                        };
                        dbHauDistanceInforForAddress.Add(dbHauDistanceInfor);
                        return false;
                    };
                    db.QueryDataWithInOperator(@"select a.code, b.id, b.detailaddress
                                                  from company a
                                                 inner join CompanyAddress b on a.id = b.companyid
                                                 where a.status = 1
                                                   and b.status = 1", "a.Code", true, tempCompanyNeedCheck, getDbHauDistanceInforForAddress);
                    var errorAddresses = tempRightList.Where(r => dbHauDistanceInforForAddress.Any(v => r.CompanyCode == v.CompanyCode && r.DetailAddress != v.DetailAddress)).ToList();
                    foreach(var errorItem in errorAddresses) {
                        if(errorItem.ErrorMsg == null) {
                            errorItem.ErrorMsg = "收货地址与企业编号不匹配";
                        } else {
                            errorItem.ErrorMsg = errorItem.ErrorMsg + ";收货地址与企业编号不匹配";
                        }
                    }
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();
                    tempCompanyNeedCheck = tempRightList.Select(r => r.StorageCompanyCode).ToArray();
                    var dbHauDistanceInforForWarehouse = new List<HauDistanceInforExtend>();
                    Func<string[], bool> getDbHauDistanceInforForWarehouse = value => {
                        var dbHauDistanceInfor = new HauDistanceInforExtend {
                            StorageCompanyCode = value[0],
                            WarehouseId = Convert.ToInt32(value[1]),
                            WarehouseName = value[2]
                        };
                        dbHauDistanceInforForWarehouse.Add(dbHauDistanceInfor);
                        return false;
                    };
                    db.QueryDataWithInOperator(@"select a.code as StorageCompanyCode, b.id as WarehouseId, b.name as WarehouseName
                                                          from company a
                                                         inner join warehouse b
                                                            on a.id = b.storagecompanyid
                                                         where a.status = 1
                                                           and b.status = 1", "a.code", true, tempCompanyNeedCheck, getDbHauDistanceInforForWarehouse);
                    var errorWarehouses = tempRightList.Where(r => dbHauDistanceInforForWarehouse.All(v => r.StorageCompanyCode == v.StorageCompanyCode && r.WarehouseName.Trim() != v.WarehouseName.Trim())).ToList();
                    foreach(var errorItem in errorWarehouses) {
                        if(errorItem.ErrorMsg == null) {
                            errorItem.ErrorMsg = "仓库不属于对应的仓储企业";
                        } else {
                            errorItem.ErrorMsg = errorItem.ErrorMsg + ";仓库不属于对应的仓储企业";
                        }
                    }
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();
                    foreach(var tempItem in tempRightList) {
                        var tempCompany = dbHauDistanceInforForCompany.First(r => r.CompanyCode == tempItem.CompanyCode);
                        tempItem.CompanyId = tempCompany.CompanyId;
                        var tempStorageCompany = dbHauDistanceInforForCompany.First(r => r.CompanyCode == tempItem.StorageCompanyCode);
                        tempItem.StorageCompanyId = tempStorageCompany.CompanyId;
                        var tempWarehouse = dbHauDistanceInforForWarehouse.First(r => r.WarehouseName == tempItem.WarehouseName);
                        tempItem.WarehouseId = tempWarehouse.WarehouseId;
                        var tempAddress = dbHauDistanceInforForAddress.First(r => r.DetailAddress == tempItem.DetailAddress);
                        tempItem.CompanyAddressId = tempAddress.CompanyAddressId;
                    }

                    var companyIdsNeesCheck = tempRightList.Select(r => r.CompanyId.ToString()).Distinct().ToArray();
                    var dbHauDistanceInforForSelf = new List<HauDistanceInforExtend>();
                    Func<string[], bool> getDbHauDistanceInfor = value => {
                        var dbHauDistanceInfor = new HauDistanceInforExtend {
                            CompanyId = Convert.ToInt32(value[0]),
                            StorageCompanyId = Convert.ToInt32(value[1]),
                            WarehouseId = Convert.ToInt32(value[2]),
                            CompanyAddressId = Convert.ToInt32(value[3])
                        };
                        dbHauDistanceInforForSelf.Add(dbHauDistanceInfor);
                        return false;
                    };
                    db.QueryDataWithInOperator("select a.storageCompanyId,a.companyid,a.companyaddressid,a.warehouseid from HauDistanceInfor a where status=1 and a.companyaddressid is not null", "a.companyId", false, companyIdsNeesCheck, getDbHauDistanceInfor);
                    var errorHauDistanceInfors = tempRightList.Where(r => dbHauDistanceInforForSelf.Any(v => v.CompanyId == r.CompanyId && v.StorageCompanyId == r.StorageCompanyId && v.WarehouseId == r.WarehouseId && v.CompanyAddressId == r.CompanyAddressId)).ToList();
                    foreach(var errorItem in errorHauDistanceInfors) {
                        if(errorItem.ErrorMsg == null) {
                            errorItem.ErrorMsg = "系统内数据已经存在";
                        } else {
                            errorItem.ErrorMsg = errorItem.ErrorMsg + ";系统内数据已经存在";
                        }
                    }
                }
                //获取所有不合格数据
                errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                //获取所有合格数据
                var rightList = allList.Except(errorList).ToList();
                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempObject = list[index - 1];
                            var values = new object[] {
                                tempObject.StorageCompanyCode, tempObject.StorageCompanyName, tempObject.WarehouseName, tempObject.CompanyCode, tempObject.CompanyName, tempObject.DetailAddress, tempObject.HauDistance, tempObject.ErrorMsg
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }

                //导入所有合格数据
                if(!rightList.Any())
                    return true;
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    //开启事务，新增更新配件在一个事务内
                    var ts = conn.BeginTransaction();
                    try {
                        //新增配件
                        if(rightList.Any()) {
                            //获取新增数据的sql语句，Id为主键
                            var sqlInsert = db.GetInsertSql("HauDistanceInfor", "Id", new[] {
                                "StorageCompanyId","CompanyId","CompanyAddressId","WarehouseId","HauDistance","Status","CreatorId","CreatorName","CreateTime"
                            });

                            var userInfo = Utils.GetCurrentUserInfo();
                            //往数据库增加配件信息
                            foreach(var item in rightList) {
                                var command = db.CreateDbCommand(sqlInsert, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("StorageCompanyId", item.StorageCompanyId));
                                command.Parameters.Add(db.CreateDbParameter("CompanyId", item.CompanyId));
                                command.Parameters.Add(db.CreateDbParameter("CompanyAddressId", item.CompanyAddressId));
                                command.Parameters.Add(db.CreateDbParameter("WarehouseId", item.WarehouseId));
                                command.Parameters.Add(db.CreateDbParameter("HauDistance", item.HauDistance));
                                command.Parameters.Add(db.CreateDbParameter("Status", item.Status));
                                command.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                command.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                command.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                command.ExecuteNonQuery();
                            }

                        }
                        //无异常提交
                        ts.Commit();
                    } catch(Exception) {
                        //报错回滚
                        ts.Rollback();
                        throw;
                    } finally {
                        if(conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
            }
        }
    }
}

