﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Entities;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {


        public bool ExportLogistics(int[] ids, string shippingCode, string orderCode, DateTime? beginShippingDate, DateTime? endShippingDate, DateTime? beginApproveTime, DateTime? endApproveTime, DateTime? beginRequestedArrivalDate, DateTime? endRequestedArrivalDate, string warehouseName, string logisticName, int? signStatus, out string fileName) {
            fileName = GetExportFilePath("物流信息单_" + ".xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {
                    var sql = new StringBuilder();

                    sql.Append(@"select a.OrderCode,
                                  (select value from keyvalueitem where NAME = 'PartsSalesOrder_Status'and key=a.PartsSalesOrderStatus) As PartsSalesOrderStatus
                                ,a.ApproveTime,a.WarehouseName,a.ReceivingAddress,a.ShippingDate,a.ShippingCode,a.RequestedArrivalDate,a.LogisticName,
                                (select value from keyvalueitem where NAME = 'PartsShipping_Method'and key=a.ShippingMethod) As ShippingMethod,a.TransportDriverPhone,
                                 case b.Status when 2 then '已签收' else '在途' end as signStatus
                                from logistics a left join PartsShippingOrder b on  a.ShippingCode=b.code where 1=1 ");
                    var dbParameters = new List<DbParameter>();

                    #region 条件过滤
                    if(ids != null && ids.Length > 0) {
                        sql.Append(" and a.id in (");
                        for(var i = 0;i < ids.Length;i++) {
                            if(ids.Length == i + 1) {
                                sql.Append("{0}id" + i);
                                dbParameters.Add(db.CreateDbParameter("id" + i, ids[i]));
                            } else {
                                sql.Append("{0}id" + i + ",");
                                dbParameters.Add(db.CreateDbParameter("id" + i, ids[i]));
                            }
                        }
                        sql.Append(")");
                    } else {
                        if(!string.IsNullOrEmpty(shippingCode)) {
                            sql.Append(" and a.shippingCode like {0}shippingCode ");
                            dbParameters.Add(db.CreateDbParameter("shippingCode", "%" + shippingCode + "%"));
                        }
                        if(!string.IsNullOrEmpty(orderCode)) {
                            sql.Append(" and a.orderCode like {0}orderCode ");
                            dbParameters.Add(db.CreateDbParameter("orderCode", "%" + orderCode + "%"));
                        }
                        if(beginShippingDate.HasValue) {
                            sql.Append(" and a.beginShippingDate>=to_date({0}beginShippingDate,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = beginShippingDate.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("beginShippingDate", tempTime.ToString("G")));
                        }
                        if(endShippingDate.HasValue) {
                            sql.Append(" and a.beginApproveTime<=to_date({0}beginApproveTime,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = beginApproveTime.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("beginApproveTime", tempTime.ToString("G")));
                        }
                        if(beginApproveTime.HasValue) {
                            sql.Append(" and a.shippingDate>=to_date({0}beginApproveTime,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = beginApproveTime.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("beginApproveTime", tempTime.ToString("G")));
                        }
                        if(endApproveTime.HasValue) {
                            sql.Append(" and a.shippingDate<=to_date({0}endApproveTime,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = endApproveTime.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("endApproveTime", tempTime.ToString("G")));
                        }
                        if(beginRequestedArrivalDate.HasValue) {
                            sql.Append(" and a.beginRequestedArrivalDate>=to_date({0}beginRequestedArrivalDate,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = beginRequestedArrivalDate.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("beginRequestedArrivalDate", tempTime.ToString("G")));
                        }
                        if(endRequestedArrivalDate.HasValue) {
                            sql.Append(" and a.endRequestedArrivalDate<=to_date({0}endRequestedArrivalDate,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = endRequestedArrivalDate.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("endRequestedArrivalDate", tempTime.ToString("G")));
                        }

                        if(!string.IsNullOrEmpty(warehouseName)) {
                            sql.Append(" and a.warehouseName like {0}warehouseName ");
                            dbParameters.Add(db.CreateDbParameter("receivingCompanyCode", "%" + warehouseName + "%"));
                        }
                        if(!string.IsNullOrEmpty(logisticName)) {
                            sql.Append(" and a.logisticName like {0}logisticName ");
                            dbParameters.Add(db.CreateDbParameter("logisticName", "%" + logisticName + "%"));
                        }
                        if(signStatus.HasValue) {
                            if(signStatus == (int)DcsPartsShippingOrderStatus.收货确认) {
                                sql.Append(" and b.Status = {0}signStatus");
                                dbParameters.Add(db.CreateDbParameter("signStatus", signStatus));
                            } else {
                                sql.Append(" and b.Status <> {0}signStatus");
                                dbParameters.Add(db.CreateDbParameter("signStatus", signStatus));
                            }
                        }
                    }
                    #endregion
                    var sqlStr = sql.ToString();
                    if(sqlStr.IndexOf(" where 1=1  and ") > 0)
                        sqlStr = sqlStr.Replace(" where 1=1  and ", " where ");
                    else
                        sqlStr = sqlStr.Replace(" where 1=1 ", "");
                    var command = db.CreateDbCommand(string.Format(sqlStr, db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    conn.Open();
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                     ErrorStrings.Export_Title_SupplierShippingOrder_SalesOrderCode, "销售订单状态","订单审核时间", "发货仓库名称", "目的地名称", "出库完成时间",ErrorStrings.Export_Title_SupplierShippingOrder_Code, "计划到达时间", "承运商名称", ErrorStrings.Export_Title_PartsPurchaseOrder_ShippingMethod, "承运商联系电话","签收状态"
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception ex) {
                throw ex;
            }
        }

        public bool ExportLogisticsWidthDetial(int[] ids, string shippingCode, string orderCode, DateTime? beginShippingDate, DateTime? endShippingDate, DateTime? beginApproveTime, DateTime? endApproveTime, DateTime? beginRequestedArrivalDate, DateTime? endRequestedArrivalDate, string warehouseName, string logisticName, int? signStatus, out string fileName) {
            fileName = GetExportFilePath("物流信息主清单_" + ".xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {
                    var sql = new StringBuilder();
                    // 由于导出时需要将出库计划备注显示出来，从发运单内联到出库计划时可能含有多个出库单，导致出现重复的数据，
                    //故加distinct过滤重复数据
                    sql.Append(@"select a.OrderCode,(select value from keyvalueitem where NAME = 'PartsSalesOrder_Status'and key=a.PartsSalesOrderStatus) As PartsSalesOrderStatus,a.ApproveTime,a.WarehouseName,a.ReceivingAddress,a.ShippingDate,a.ShippingCode,a.RequestedArrivalDate,
                                (select value from keyvalueitem where NAME = 'PartsShipping_Method'and key=a.ShippingMethod) As ShippingMethod,a.LogisticName,a.TransportDriverPhone,
                                 case b.Status when 2 then '已签收' else '在途' end as signStatus,
                                c.PointTime,
                            (select value from keyvalueitem where NAME = 'PartsShipping_Method'and key=c.ShippingMethod) As DShippingMethod,
                            c.PointName,c.ScanTime,c.ScanName,c.Remark 
                                from logistics a left join 
                                PartsShippingOrder b on  a.ShippingCode=b.code
                                left join LogisticsDetail c on a.id=c.logisticsid where 1=1 ");
                    var dbParameters = new List<DbParameter>();

                    #region 条件过滤
                    if(ids != null && ids.Length > 0) {
                        sql.Append(" and a.id in (");
                        for(var i = 0;i < ids.Length;i++) {
                            if(ids.Length == i + 1) {
                                sql.Append("{0}id" + i);
                                dbParameters.Add(db.CreateDbParameter("id" + i, ids[i]));
                            } else {
                                sql.Append("{0}id" + i + ",");
                                dbParameters.Add(db.CreateDbParameter("id" + i, ids[i]));
                            }
                        }
                        sql.Append(")");
                    } else {
                        if(!string.IsNullOrEmpty(shippingCode)) {
                            sql.Append(" and a.shippingCode like {0}shippingCode ");
                            dbParameters.Add(db.CreateDbParameter("shippingCode", "%" + shippingCode + "%"));
                        }
                        if(!string.IsNullOrEmpty(orderCode)) {
                            sql.Append(" and a.orderCode like {0}orderCode ");
                            dbParameters.Add(db.CreateDbParameter("orderCode", "%" + orderCode + "%"));
                        }
                        if(beginShippingDate.HasValue) {
                            sql.Append(" and a.beginShippingDate>=to_date({0}beginShippingDate,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = beginShippingDate.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("beginShippingDate", tempTime.ToString("G")));
                        }
                        if(endShippingDate.HasValue) {
                            sql.Append(" and a.beginApproveTime<=to_date({0}beginApproveTime,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = beginApproveTime.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("beginApproveTime", tempTime.ToString("G")));
                        }
                        if(beginApproveTime.HasValue) {
                            sql.Append(" and a.shippingDate>=to_date({0}beginApproveTime,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = beginApproveTime.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("beginApproveTime", tempTime.ToString("G")));
                        }
                        if(endApproveTime.HasValue) {
                            sql.Append(" and a.shippingDate<=to_date({0}endApproveTime,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = endApproveTime.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("endApproveTime", tempTime.ToString("G")));
                        }
                        if(beginRequestedArrivalDate.HasValue) {
                            sql.Append(" and a.beginRequestedArrivalDate>=to_date({0}beginRequestedArrivalDate,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = beginRequestedArrivalDate.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("beginRequestedArrivalDate", tempTime.ToString("G")));
                        }
                        if(endRequestedArrivalDate.HasValue) {
                            sql.Append(" and a.endRequestedArrivalDate<=to_date({0}endRequestedArrivalDate,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = endRequestedArrivalDate.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("endRequestedArrivalDate", tempTime.ToString("G")));
                        }

                        if(!string.IsNullOrEmpty(warehouseName)) {
                            sql.Append(" and a.warehouseName like {0}warehouseName ");
                            dbParameters.Add(db.CreateDbParameter("receivingCompanyCode", "%" + warehouseName + "%"));
                        }
                        if(!string.IsNullOrEmpty(logisticName)) {
                            sql.Append(" and a.logisticName like {0}logisticName ");
                            dbParameters.Add(db.CreateDbParameter("logisticName", "%" + logisticName + "%"));
                        }
                        if(signStatus.HasValue) {
                            if(signStatus == (int)DcsPartsShippingOrderStatus.收货确认) {
                                sql.Append(" and b.Status = {0}signStatus");
                                dbParameters.Add(db.CreateDbParameter("signStatus", signStatus));
                            } else {
                                sql.Append(" and b.Status <> {0}signStatus");
                                dbParameters.Add(db.CreateDbParameter("signStatus", signStatus));
                            }
                        }
                    }
                    #endregion
                    var sqlStr = sql.ToString();
                    if(sqlStr.IndexOf(" where 1=1  and ") > 0)
                        sqlStr = sqlStr.Replace(" where 1=1  and ", " where ");
                    else
                        sqlStr = sqlStr.Replace(" where 1=1 ", "");
                    var command = db.CreateDbCommand(string.Format(sqlStr, db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    conn.Open();
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    ErrorStrings.Export_Title_SupplierShippingOrder_SalesOrderCode,"销售订单状态","订单审核时间", "发货仓库名称", "目的地名称", "出库完成时间",ErrorStrings.Export_Title_SupplierShippingOrder_Code,  "计划到达时间", 
                                    ErrorStrings.Export_Title_PartsPurchaseOrder_ShippingMethod,"承运商名称", "承运商联系电话","签收状态","到达站点时间",ErrorStrings.Export_Title_PartsPurchaseOrder_ShippingMethod,"到达站点名称","扫码时间","扫码签收人",ErrorStrings.Export_Title_PartsBranch_Remark
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception ex) {
                throw ex;
            }
        }

    }
}
