﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Entities;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        /// <summary>
        /// 合并导出配件发运单主清单
        /// </summary>
        public bool ExportPartsShippingOrder(int[] ids, int? settlementCompanyId, int? receivingCompanyId, string code, int? type, int? status, int? warehouseId, string warehouseName, DateTime? requestedArrivalDateBegin, DateTime? requestedArrivalDateEnd, DateTime? shippingDateBegin, DateTime? shippingDateEnd, DateTime? createTimeBegin, DateTime? createTimeEnd, DateTime? confirmedReceptionTimeBegin, DateTime? confirmedReceptionTimeEnd, string receivingCompanyCode, string receivingCompanyName, string receivingWarehouseName, int? partsSalesCategoryId, int? logisticCompanyId, string logisticCompanyCode, string logisticCompanyName, string expressCompany, string expressCompanyCode, int? closedLoopStatus, string eRPSOURCEORDERCODE, out string fileName) {
            fileName = GetExportFilePath("配件发运单主清单_" + ".xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {
                    var sql = new StringBuilder();
                    sql.Append(@"select a.Code, --配件发运单编号
                                       (select value from keyvalueitem where NAME = 'PartsShippingOrder_Type'and key=a.Type) As Type,
                                       d.partssalesordertypename,
                                        (select value from keyvalueitem where NAME = 'PartsShippingOrder_Status'and key=a.Status) As status,--状态
                                       cast(decode(a.Status,2,'闭环',3,'闭环',99,'闭环',1,'未闭环',4,'未闭环',5,'未闭环',6,'未闭环') as varchar2(20)) as ClosedLoopStatus,--闭环状态
                                       e.Name, --品牌
                                       (select sum(nvl(t.ShippingAmount,0) * nvl(t.settlementprice,0))  from PartsShippingOrderDetail t where t.PartsShippingOrderId = a.id) as TotalAmount, --总金额
                                       a.WarehouseName,
                                       --a.ShippingCompanyCode,--发货单位编号
                                       a.ShippingCompanyName, --发货单位名称
                                       --a.ReceivingCompanyCode,--收货单位编号
                                       a.ReceivingCompanyName, --收货单位名称
                                      a.ReceivingWarehouseName,--收货仓库名称
                                       --a.ReceivingAddress,--收货地址
                                       /*case a.IsTransportLosses--是否路损
                                       when 1 then '是'
                                       when 0 then '否'
                                       end  as IsTransportLosses,
                                       case a.TransportLossesDisposeStatus--路损处理状态
                                       when 1 then '未处理'
                                       when 2 then '处理完毕'
                                       end  as TransportLossesDisposeStatus,*/
                                       --a.LogisticCompanyCode,--物流公司编号
                                       --a.LogisticCompanyName, --物流公司名称
                                       --a.ExpressCompanyCode,
                                       a.ExpressCompany,
                                       (select value from keyvalueitem where NAME = 'PartsShipping_Method'and key=a.ShippingMethod) As ShippingMethod,
                                       --a.InterfaceRecordId, --WMS装载单号
                                       to_char( a.RequestedArrivalDate,'yyyy-mm-dd hh24:mi:ss') as requestedArrivalDate, --a.RequestedArrivalDate, --要求到货日期
                                       to_char( a.ShippingDate,'yyyy-mm-dd hh24:mi:ss') as shippingDate, -- a.ShippingDate, --发运日期
                                       a.LogisticArrivalDate, --承运商运达时间
                                       to_char(a.ShippingTime,'yyyy-mm-dd hh24:mi:ss') as shippingTime, --a.ShippingTime,--物流发货日期
                                       a.ConfirmedReceptionTime,
                                       --a.TransportDriverPhone,--承运司机电话
                                       --a.DeliveryBillNumber,--送货单号
                                       a.ArrivalMode,--到货模式
                                       a.Logistic,--承运商
                                       a.DeliveryBillNumber,--送货单号
                                       a.CargoTerminalMsg,--货站信息
                                       a.ArrivalTime, --到货站日期
                                       a.FlowFeedback,--超期或电商运单流水反馈
                                          /*case a.Status--状态
                                       when 1 then '新建'
                                       when 2 then '收货确认'
                                       when 3 then '回执确认'
                                       when 4 then '已发货'
                                       when 5 then '待提货'
                                       when 6 then '待收货'
                                       when 99 then '作废'
                                       end  as status,*/
                                       a.Remark, --备注
                                       a.CreatorName, --创建人
                                       a.CreateTime, --创建时间
                                       /* d.code outCode, */ --出库单编号
                                       b.PartsOutboundPlanCode, --出库计划单编号
                                       b.SparePartCode, --配件编号
                                       b.SparePartName, --配件名称
                                       b.ShippingAmount, --发运量
                                       b.ConfirmedAmount, --确认量
                                       b.settlementprice,--销售单价
                                       b.ConfirmedAmount * b.Settlementprice,--金额
                                       b.InTransitDamageLossAmount, --路损量
                                       b.Remark, --备注
                                       b.ContainerNumber --货箱号
                                  from PartsShippingOrder a
                                 inner join PartsShippingOrderDetail b
                                    on a.id = b.PartsShippingOrderId
                                    left join partssalesorder d 
                                    on a.originalrequirementbillcode=d.code
                                    Left Join PartsSalesCategory e
                                    On a.PartsSalesCategoryId = e.Id where 1=1 ");
                    var dbParameters = new List<DbParameter>();

                    #region 条件过滤
                    if(ids != null && ids.Length > 0) {
                        sql.Append(" and a.id in (");
                        for(var i = 0; i < ids.Length; i++) {
                            if(ids.Length == i + 1) {
                                sql.Append("{0}id" + i);
                                dbParameters.Add(db.CreateDbParameter("id" + i, ids[i]));
                            } else {
                                sql.Append("{0}id" + i + ",");
                                dbParameters.Add(db.CreateDbParameter("id" + i, ids[i]));
                            }
                        }
                        sql.Append(")");
                    } else {
                        if(closedLoopStatus.HasValue) {
                            if(closedLoopStatus == 1) {
                                //未闭环
                                sql.Append(" and a.Status in(1,4,5,6) ");
                            } else if(closedLoopStatus == 2) {
                                //闭环
                                sql.Append(" and a.Status in(2,3,99)  ");
                            }
                        }
                        if(settlementCompanyId.HasValue) {
                            sql.Append(" and a.settlementCompanyId={0}settlementCompanyId");
                            dbParameters.Add(db.CreateDbParameter("settlementCompanyId", settlementCompanyId.Value));
                        }
                        if(receivingCompanyId.HasValue) {
                            sql.Append(" and a.receivingCompanyId={0}receivingCompanyId");
                            dbParameters.Add(db.CreateDbParameter("receivingCompanyId", receivingCompanyId.Value));
                        }
                        if(!string.IsNullOrEmpty(code)) {
                            sql.Append(" and Upper(a.code) like Upper({0}code) ");
                            dbParameters.Add(db.CreateDbParameter("code", "%" + code + "%"));
                        }
                        if(type.HasValue) {
                            sql.Append(" and a.type={0}type");
                            dbParameters.Add(db.CreateDbParameter("type", type.Value));
                        }
                        if(status.HasValue) {
                            sql.Append(" and a.status={0}status");
                            dbParameters.Add(db.CreateDbParameter("status", status.Value));
                        }
                        if(warehouseId.HasValue) {
                            sql.Append(" and a.warehouseId={0}warehouseId");
                            dbParameters.Add(db.CreateDbParameter("warehouseId", warehouseId.Value));
                        }
                        if(!string.IsNullOrEmpty(warehouseName)) {
                            sql.Append(" and a.warehouseName like {0}warehouseName ");
                            dbParameters.Add(db.CreateDbParameter("warehouseName", "%" + warehouseName + "%"));
                        }
                        if(requestedArrivalDateBegin.HasValue) {
                            sql.Append(" and a.requestedArrivalDate>=to_date({0}requestedArrivalDateBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = requestedArrivalDateBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("requestedArrivalDateBegin", tempTime.ToString("G")));
                        }
                        if(requestedArrivalDateEnd.HasValue) {
                            sql.Append(" and a.requestedArrivalDate<=to_date({0}requestedArrivalDateEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = requestedArrivalDateEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("requestedArrivalDateEnd", tempTime.ToString("G")));
                        }
                        if(shippingDateBegin.HasValue) {
                            sql.Append(" and a.shippingDate>=to_date({0}shippingDateBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = shippingDateBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("shippingDateBegin", tempTime.ToString("G")));
                        }
                        if(shippingDateEnd.HasValue) {
                            sql.Append(" and a.shippingDate<=to_date({0}shippingDateEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = shippingDateEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("shippingDateEnd", tempTime.ToString("G")));
                        }
                        if(createTimeBegin.HasValue) {
                            sql.Append(" and a.createTime>=to_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(" and a.createTime<=to_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                        if(confirmedReceptionTimeBegin.HasValue) {
                            sql.Append(" and a.confirmedReceptionTime>=to_date({0}confirmedReceptionTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = confirmedReceptionTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("confirmedReceptionTimeBegin", tempTime.ToString("G")));
                        }
                        if(confirmedReceptionTimeEnd.HasValue) {
                            sql.Append(" and a.confirmedReceptionTime<=to_date({0}confirmedReceptionTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = confirmedReceptionTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("confirmedReceptionTimeEnd", tempTime.ToString("G")));
                        }
                        if(!string.IsNullOrEmpty(receivingCompanyCode)) {
                            sql.Append(" and Upper(a.receivingCompanyCode) like Upper({0}receivingCompanyCode) ");
                            dbParameters.Add(db.CreateDbParameter("receivingCompanyCode", "%" + receivingCompanyCode + "%"));
                        }
                        if(!string.IsNullOrEmpty(receivingCompanyName)) {
                            sql.Append(" and a.receivingCompanyName like {0}receivingCompanyName ");
                            dbParameters.Add(db.CreateDbParameter("receivingCompanyName", "%" + receivingCompanyName + "%"));
                        }
                        if(!string.IsNullOrEmpty(receivingWarehouseName)) {
                            sql.Append(" and a.receivingWarehouseName like {0}receivingWarehouseName");
                            dbParameters.Add(db.CreateDbParameter("receivingWarehouseName", "%" + receivingWarehouseName + "%"));
                        }
                        if(partsSalesCategoryId.HasValue) {
                            sql.Append(" and a.partsSalesCategoryId={0}partsSalesCategoryId");
                            dbParameters.Add(db.CreateDbParameter("partsSalesCategoryId", partsSalesCategoryId.Value));
                        }
                        if(logisticCompanyId.HasValue) {
                            sql.Append(" and a.logisticCompanyId={0}logisticCompanyId");
                            dbParameters.Add(db.CreateDbParameter("logisticCompanyId", logisticCompanyId.Value));
                        }
                        if(!string.IsNullOrEmpty(logisticCompanyCode)) {
                            sql.Append(" and a.logisticCompanyCode like {0}logisticCompanyCode ");
                            dbParameters.Add(db.CreateDbParameter("logisticCompanyCode", "%" + logisticCompanyCode + "%"));
                        }
                        if(!string.IsNullOrEmpty(logisticCompanyName)) {
                            sql.Append(" and a.logisticCompanyName like {0}logisticCompanyName ");
                            dbParameters.Add(db.CreateDbParameter("logisticCompanyName", "%" + logisticCompanyName + "%"));
                        }
                        if(!string.IsNullOrEmpty(expressCompany)) {
                            sql.Append(" and a.expressCompany like {0}expressCompany ");
                            dbParameters.Add(db.CreateDbParameter("expressCompany", "%" + expressCompany + "%"));
                        }
                        if(!string.IsNullOrEmpty(expressCompanyCode)) {
                            sql.Append(" and a.expressCompanyCode like {0}expressCompanyCode ");
                            dbParameters.Add(db.CreateDbParameter("expressCompanyCode", "%" + expressCompanyCode + "%"));
                        }
                        if(!string.IsNullOrEmpty(eRPSOURCEORDERCODE)) {
                            sql.Append(" and d.ERPSOURCEORDERCODE like {0}ERPSOURCEORDERCODE ");
                            dbParameters.Add(db.CreateDbParameter("eRPSOURCEORDERCODE", "%" + eRPSOURCEORDERCODE + "%"));
                        }
                    }


                    sql.Append(" order by a.id ");
                    #endregion

                    var sqlStr = sql.ToString();
                    if(sqlStr.IndexOf(" where 1=1  and ") > 0)
                        sqlStr = sqlStr.Replace(" where 1=1  and ", " where ");
                    else
                        sqlStr = sqlStr.Replace(" where 1=1 ", "");
                    var command = db.CreateDbCommand(string.Format(sqlStr, db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    conn.Open();
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    ErrorStrings.Export_Title_AgencyPartsShippingOrder_Code, ErrorStrings.Export_Title_AgencyPartsShippingOrder_Type, ErrorStrings.Export_Title_PartsPurchaseOrder_OrderType, ErrorStrings.Export_Title_AccountPeriod_Status, ErrorStrings.Export_Title_AgencyPartsShippingOrder_ClosedStatus, ErrorStrings.Export_Title_Partssalescategory_Name, ErrorStrings.Export_Title_InternalAllocationBill_TotalAmount, ErrorStrings.Export_Title_AgencyPartsShippingOrder_ReceivingWarehouseCode, ErrorStrings.Export_Title_PartsInboundCheckBill_CompanyName, ErrorStrings.Export_Title_PartsPurchaseOrder_ReceivingCompanyName, ErrorStrings.Export_Title_SupplierShippingOrder_ReveiveWarehouseName, ErrorStrings.Export_Title_AgencyPartsShippingOrder_LogisticCompanyName, ErrorStrings.Export_Title_PartsPurchaseOrder_ShippingMethod, ErrorStrings.Export_Title_AgencyPartsShippingOrder_RequestedArrivalDate, ErrorStrings.Export_Title_SupplierShippingOrder_ShippingDates, ErrorStrings.Export_Title_AgencyPartsShippingOrder_LogisticArrivalDate, ErrorStrings.Export_Title_AgencyPartsShippingOrder_LogisticShippingTime, ErrorStrings.Export_Title_InternalAllocationBill_ApproveTime, ErrorStrings.Export_Title_AgencyPartsShippingOrder_ArrivalMode, ErrorStrings.Export_Title_AgencyPartsShippingOrder_Logistic, ErrorStrings.Export_Title_SupplierShippingOrder_DdeliverybillNumber, ErrorStrings.Export_Title_AgencyPartsShippingOrder_CargoTerminalMsg, ErrorStrings.Export_Title_AgencyPartsShippingOrder_ArrivalTime, ErrorStrings.Export_Title_AgencyPartsShippingOrder_FlowFeedback, ErrorStrings.Export_Title_PartsBranch_Remark, ErrorStrings.Export_Title_AccountPeriod_CreatorName, ErrorStrings.Export_Title_AccountPeriod_CreateTime,ErrorStrings.Export_Title_Partsoutboundplan_Code, ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode, ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName, ErrorStrings.Export_Title_PartsPurchaseOrder_ShippingAmount, ErrorStrings.Export_Title_PartsPurchaseOrder_ConfirmedAmount, ErrorStrings.Export_Title_AgencyPartsShippingOrder_SalesPrice, ErrorStrings.Export_Title_Customertransferbill_Amount, ErrorStrings.Export_Title_AgencyPartsShippingOrder_Difference, ErrorStrings.Export_Title_PartsBranch_Remark,ErrorStrings.Export_Title_PartsOutboundBill_BoxNo
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }

        /// <summary>
        /// 导出配件发运单
        /// </summary>
        public bool ExportPartsShippingOrderNotWithDetail(int[] ids, int? settlementCompanyId, int? receivingCompanyId, string code, int? type, int? status, int? warehouseId, string warehouseName, DateTime? requestedArrivalDateBegin, DateTime? requestedArrivalDateEnd, DateTime? shippingDateBegin, DateTime? shippingDateEnd, DateTime? createTimeBegin, DateTime? createTimeEnd, DateTime? confirmedReceptionTimeBegin, DateTime? confirmedReceptionTimeEnd, string receivingCompanyCode, string receivingCompanyName, string receivingWarehouseName, int? partsSalesCategoryId, int? logisticCompanyId, string logisticCompanyCode, string logisticCompanyName, string expressCompany, string expressCompanyCode, int? closedLoopStatus, string eRPSOURCEORDERCODE, out string fileName) {
            fileName = GetExportFilePath("配件发运单_" + ".xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);
                var userInfo = Utils.GetCurrentUserInfo();
                using(var conn = db.CreateDbConnection()) {
                    var sql = new StringBuilder();
                    // 由于导出时需要将出库计划备注显示出来，从发运单内联到出库计划时可能含有多个出库单，导致出现重复的数据，
                    //故加distinct过滤重复数据
                    sql.Append(@"select distinct a.Code, --配件发运单编号
                                    (select value from keyvalueitem where NAME = 'PartsShippingOrder_Type'and key=a.Type) As Type,--配件发运单类型
                                    a.OriginalRequirementBillCode, --原始需求单据编号
                                    a.PartsOutboundPlanCode, --出库计划编号
                                    psot.name as typename,--订单类型
                                    (select value from keyvalueitem where NAME = 'PartsShippingOrder_Status'and key=a.Status) As status,--状态
                                    cast(decode(a.Status,2,'闭环',3,'闭环',99,'闭环',1,'未闭环',4,'未闭环',5,'未闭环',6,'未闭环') as varchar2(20)) as ClosedLoopStatus,--闭环状态
                                    b.Name, --品牌
                                    a.WarehouseName,--出库仓库
                                    a.ReceivingCompanyCode, --收货单位编号
                                    a.ReceivingCompanyName, --收货单位名称
                                    a.ReceivingWarehouseName,--收货仓库名称
                                    a.ReceivingAddress, --收货地址
                                    t.ct, --品种数量 = 发运清单记录条数
                                    t.ss, --数量 = sum（配件发运单清单.发运量）
                                    t.ssc, --金额 = sum（配件结算价*发运量）
                                    a.LogisticCompanyCode, --物流公司编号
                                    a.LogisticCompanyName, --物流公司名称
                                    (select value from keyvalueitem where NAME = 'ArrivalMode' and key= a.ArrivalMode) as ArrivalMode,--到货模式                                  
                                    --a.ExpressCompanyCode,
                                    a.Logistic,--承运商
                                    a.TransportDriverPhone,--承运司机电话
                                    a.DeliveryBillNumber,--送货单号
                                    a.CargoTerminalMsg,--货站信息
                                    a.ArrivalTime, --到货站日期
                                    a.FlowFeedback,--超期或电商运单流水反馈
                                     to_char( a.ShippingDate,'yyyy-mm-dd hh24:mi:ss') as shippingDate, -- a.ShippingDate, --发运日期
                                     to_char( a.RequestedArrivalDate,'yyyy-mm-dd hh24:mi:ss') as requestedArrivalDate, --a.RequestedArrivalDate, --要求到货日期
                                    to_char(a.ShippingTime,'yyyy-mm-dd hh24:mi:ss') as shippingTime, --a.ShippingTime,--物流发货日期
                                    a.ConfirmedReceptionTime,--确认时间
                                    a.Remark, --备注
                                    a.CreatorName, --创建人
                                    a.CreateTime, --创建时间
                                    a.OrderApproveComment, --订单审批意见
                                    tp.remark as outplanRemark, --出库计划备注
                                    a.ShippingCompanyCode, --发货单位编号
                                    a.ShippingCompanyName, --发货单位名称
                                    a.Transportmileage, --公里数 = 配件发运单.运输里程
                                   (select value from keyvalueitem where NAME = 'PartsShipping_Method'and key=a.ShippingMethod) As ShippingMethod,--发运方式
                                   (select value from keyvalueitem where NAME = 'IsOrNot'and key=a.IsTransportLosses) As IsTransportLosses,--是否路损
                                   (select value from keyvalueitem where NAME = 'PartsShippingOrder_TransportLossesDisposeStatus'and key=a.TransportLossesDisposeStatus) As TransportLossesDisposeStatus--路损处理状态
                                     ,pso.ERPSOURCEORDERCODE
                                from PartsShippingOrder a
                                inner join (select c.partsshippingorderid,
                                                Count(1) as ct, --品种数量 = 发运清单记录条数
                                                Sum(c.Shippingamount) as ss, --数量 = sum（配件发运单清单.发运量）
                                                Sum(c.Shippingamount * c.Settlementprice) as ssc --金额 = sum（配件结算价*发运量）
                                            from Partsshippingorderdetail c
                                            group by c.partsshippingorderid) t on a.id =
                                                                                t.partsshippingorderid
                                Left Join PartsSalesCategory b On a.PartsSalesCategoryId = b.Id
                                inner join partsshippingorderref tr on tr.partsshippingorderid = a.id
                                inner join partsoutboundbill tout on tout.id = tr.partsoutboundbillid
                                inner join partsoutboundplan tp on tp.id = tout.partsoutboundplanid
                                left join PartsSalesOrder pso on a.OriginalRequirementBillCode = pso.code
                                left join PartsSalesOrderType psot on  pso.partssalesordertypeid = psot.id where (a.receivingcompanyid ="+userInfo.EnterpriseId+" or exists (select 1 from warehouseoperator wp where wp.warehouseid=a.warehouseid and wp.operatorid="+userInfo.Id+" ))");
                    var dbParameters = new List<DbParameter>();

                    #region 条件过滤
                    if(ids != null && ids.Length > 0) {
                        sql.Append(" and a.id in (");
                        for(var i = 0; i < ids.Length; i++) {
                            if(ids.Length == i + 1) {
                                sql.Append("{0}id" + i);
                                dbParameters.Add(db.CreateDbParameter("id" + i, ids[i]));
                            } else {
                                sql.Append("{0}id" + i + ",");
                                dbParameters.Add(db.CreateDbParameter("id" + i, ids[i]));
                            }
                        }
                        sql.Append(")");
                    } else {
                        if(closedLoopStatus.HasValue) {
                            if(closedLoopStatus == 1) {
                                //未闭环
                                sql.Append(" and a.Status in(1,4,5,6) ");
                            } else if(closedLoopStatus == 2) {
                                //闭环
                                sql.Append(" and a.Status in(2,3,99)  ");
                            }
                        }
                        if(settlementCompanyId.HasValue) {
                            sql.Append(" and a.settlementCompanyId={0}settlementCompanyId");
                            dbParameters.Add(db.CreateDbParameter("settlementCompanyId", settlementCompanyId.Value));
                        }
                        if(receivingCompanyId.HasValue) {
                            sql.Append(" and a.receivingCompanyId={0}receivingCompanyId");
                            dbParameters.Add(db.CreateDbParameter("receivingCompanyId", receivingCompanyId.Value));
                        }
                        if(!string.IsNullOrEmpty(code)) {
                            sql.Append(" and a.code like {0}code ");
                            dbParameters.Add(db.CreateDbParameter("code", "%" + code + "%"));
                        }
                        if(type.HasValue) {
                            sql.Append(" and a.type={0}type");
                            dbParameters.Add(db.CreateDbParameter("type", type.Value));
                        }
                        if(status.HasValue) {
                            sql.Append(" and a.status={0}status");
                            dbParameters.Add(db.CreateDbParameter("status", status.Value));
                        }
                        if(warehouseId.HasValue) {
                            sql.Append(" and a.warehouseId={0}warehouseId");
                            dbParameters.Add(db.CreateDbParameter("warehouseId", warehouseId.Value));
                        }
                        if(!string.IsNullOrEmpty(warehouseName)) {
                            sql.Append(" and a.warehouseName like {0}warehouseName ");
                            dbParameters.Add(db.CreateDbParameter("warehouseName", "%" + warehouseName + "%"));
                        }
                        if(requestedArrivalDateBegin.HasValue) {
                            sql.Append(" and a.requestedArrivalDate>=to_date({0}requestedArrivalDateBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = requestedArrivalDateBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("requestedArrivalDateBegin", tempTime.ToString("G")));
                        }
                        if(requestedArrivalDateEnd.HasValue) {
                            sql.Append(" and a.requestedArrivalDate<=to_date({0}requestedArrivalDateEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = requestedArrivalDateEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("requestedArrivalDateEnd", tempTime.ToString("G")));
                        }
                        if(shippingDateBegin.HasValue) {
                            sql.Append(" and a.shippingDate>=to_date({0}shippingDateBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = shippingDateBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("shippingDateBegin", tempTime.ToString("G")));
                        }
                        if(shippingDateEnd.HasValue) {
                            sql.Append(" and a.shippingDate<=to_date({0}shippingDateEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = shippingDateEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("shippingDateEnd", tempTime.ToString("G")));
                        }
                        if(createTimeBegin.HasValue) {
                            sql.Append(" and a.createTime>=to_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(" and a.createTime<=to_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                        if(confirmedReceptionTimeBegin.HasValue) {
                            sql.Append(" and a.confirmedReceptionTime>=to_date({0}confirmedReceptionTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = confirmedReceptionTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("confirmedReceptionTimeBegin", tempTime.ToString("G")));
                        }
                        if(confirmedReceptionTimeEnd.HasValue) {
                            sql.Append(" and a.confirmedReceptionTime<=to_date({0}confirmedReceptionTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = confirmedReceptionTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("confirmedReceptionTimeEnd", tempTime.ToString("G")));
                        }
                        if(!string.IsNullOrEmpty(receivingCompanyCode)) {
                            sql.Append(" and a.receivingCompanyCode like {0}receivingCompanyCode ");
                            dbParameters.Add(db.CreateDbParameter("receivingCompanyCode", "%" + receivingCompanyCode + "%"));
                        }
                        if(!string.IsNullOrEmpty(receivingCompanyName)) {
                            sql.Append(" and a.receivingCompanyName like {0}receivingCompanyName ");
                            dbParameters.Add(db.CreateDbParameter("receivingCompanyName", "%" + receivingCompanyName + "%"));
                        }
                        if(!string.IsNullOrEmpty(receivingWarehouseName)) {
                            sql.Append(" and a.receivingWarehouseName like {0}receivingWarehouseName");
                            dbParameters.Add(db.CreateDbParameter("receivingWarehouseName", "%" + receivingWarehouseName + "%"));
                        }
                        if(partsSalesCategoryId.HasValue) {
                            sql.Append(" and a.partsSalesCategoryId={0}partsSalesCategoryId");
                            dbParameters.Add(db.CreateDbParameter("partsSalesCategoryId", partsSalesCategoryId.Value));
                        }
                        if(logisticCompanyId.HasValue) {
                            sql.Append(" and a.logisticCompanyId={0}logisticCompanyId");
                            dbParameters.Add(db.CreateDbParameter("logisticCompanyId", logisticCompanyId.Value));
                        }
                        if(!string.IsNullOrEmpty(logisticCompanyCode)) {
                            sql.Append(" and a.logisticCompanyCode like {0}logisticCompanyCode ");
                            dbParameters.Add(db.CreateDbParameter("logisticCompanyCode", "%" + logisticCompanyCode + "%"));
                        }
                        if(!string.IsNullOrEmpty(logisticCompanyName)) {
                            sql.Append(" and a.logisticCompanyName like {0}logisticCompanyName ");
                            dbParameters.Add(db.CreateDbParameter("logisticCompanyName", "%" + logisticCompanyName + "%"));
                        }
                        if(!string.IsNullOrEmpty(expressCompany)) {
                            sql.Append(" and a.expressCompany like {0}expressCompany ");
                            dbParameters.Add(db.CreateDbParameter("expressCompany", "%" + expressCompany + "%"));
                        }
                        if(!string.IsNullOrEmpty(expressCompanyCode)) {
                            sql.Append(" and a.expressCompanyCode like {0}expressCompanyCode ");
                            dbParameters.Add(db.CreateDbParameter("expressCompanyCode", "%" + expressCompanyCode + "%"));
                        }
                        if(!string.IsNullOrEmpty(eRPSOURCEORDERCODE)) {
                            sql.Append(" and pso.eRPSOURCEORDERCODE like {0}eRPSOURCEORDERCODE ");
                            dbParameters.Add(db.CreateDbParameter("eRPSOURCEORDERCODE", "%" + eRPSOURCEORDERCODE + "%"));
                        }
                    }
                    #endregion

                    var sqlStr = sql.ToString();
                    if(sqlStr.IndexOf(" where 1=1  and ") > 0)
                        sqlStr = sqlStr.Replace(" where 1=1  and ", " where ");
                    else
                        sqlStr = sqlStr.Replace(" where 1=1 ", "");
                    var command = db.CreateDbCommand(string.Format(sqlStr, db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    conn.Open();
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    ErrorStrings.Export_Title_AgencyPartsShippingOrder_Code, ErrorStrings.Export_Title_AgencyPartsShippingOrder_Type, ErrorStrings.Export_Title_PartsInboundCheckBill_OriginalRequirementBillCode,ErrorStrings.Export_Title_Partsoutboundplan_Code, ErrorStrings.Export_Title_PartsPurchaseOrder_OrderType, ErrorStrings.Export_Title_AccountPeriod_Status, ErrorStrings.Export_Title_AgencyPartsShippingOrder_ClosedStatus, ErrorStrings.Export_Title_Partssalescategory_Name, ErrorStrings.Export_Title_AgencyPartsShippingOrder_ReceivingWarehouseCode, ErrorStrings.Export_Title_Partsoutboundplan_ReceivingCompanyCode, ErrorStrings.Export_Title_PartsPurchaseOrder_ReceivingCompanyName, ErrorStrings.Export_Title_SupplierShippingOrder_ReveiveWarehouseName, ErrorStrings.Export_Title_PartsPurchaseOrder_ReceiveAddress, ErrorStrings.Export_Title_AgencyPartsShippingOrder_VarietyQuantity, ErrorStrings.Export_Title_WarehouseArea_Quantity, ErrorStrings.Export_Title_Customertransferbill_Amount, ErrorStrings.Export_Title_AgencyPartsShippingOrder_LogisticCompanyCode, ErrorStrings.Export_Title_AgencyPartsShippingOrder_LogisticCompanyName, ErrorStrings.Export_Title_AgencyPartsShippingOrder_ArrivalMode, ErrorStrings.Export_Title_AgencyPartsShippingOrder_Logistic, ErrorStrings.Export_Title_PartsShippingOrder_DriverPhone, ErrorStrings.Export_Title_SupplierShippingOrder_DdeliverybillNumber, ErrorStrings.Export_Title_AgencyPartsShippingOrder_CargoTerminalMsg, ErrorStrings.Export_Title_AgencyPartsShippingOrder_ArrivalTime, ErrorStrings.Export_Title_AgencyPartsShippingOrder_FlowFeedback, ErrorStrings.Export_Title_SupplierShippingOrder_ShippingDates, ErrorStrings.Export_Title_AgencyPartsShippingOrder_RequestedArrivalDate, ErrorStrings.Export_Title_AgencyPartsShippingOrder_LogisticShippingTime, ErrorStrings.Export_Title_InternalAllocationBill_ApproveTime, ErrorStrings.Export_Title_PartsBranch_Remark, ErrorStrings.Export_Title_AccountPeriod_CreatorName, ErrorStrings.Export_Title_AccountPeriod_CreateTime, ErrorStrings.Export_Title_PartsOutboundBill_OrderApproveComment, ErrorStrings.Export_Title_ShippingOrder_OutPlanRemark, ErrorStrings.Export_Title_PartsShippingOrder_ShippingCode, ErrorStrings.Export_Title_PartsInboundCheckBill_CompanyName, ErrorStrings.Export_Title_AgencyPartsShippingOrder_TransportMileage, ErrorStrings.Export_Title_PartsPurchaseOrder_ShippingMethod, ErrorStrings.Export_Title_AgencyPartsShippingOrder_IsTransportLosses, ErrorStrings.Export_Title_AgencyPartsShippingOrder_TransportLossesDisposeStatus
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception ex) {
                throw ex;
            }
        }

        /// <summary>
        /// 导出配件发运单的客户信息
        /// </summary>
        public bool ExportPartsShippingCustomerInfo(int[] ids, int? settlementCompanyId, int? receivingCompanyId, string code, int? type, int? status, int? warehouseId, string warehouseName, DateTime? requestedArrivalDateBegin, DateTime? requestedArrivalDateEnd, DateTime? shippingDateBegin, DateTime? shippingDateEnd, DateTime? createTimeBegin, DateTime? createTimeEnd, DateTime? confirmedReceptionTimeBegin, DateTime? confirmedReceptionTimeEnd, string receivingCompanyCode, string receivingCompanyName, string receivingWarehouseName, int? partsSalesCategoryId, int? logisticCompanyId, string logisticCompanyCode, string logisticCompanyName, string expressCompany, string expressCompanyCode, int? closedLoopStatus, string eRPSOURCEORDERCODE, out string fileName) {
            fileName = GetExportFilePath("客户信息_" + ".xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {
                    var sql = new StringBuilder();
                    // 由于导出时需要将出库计划备注显示出来，从发运单内联到出库计划时可能含有多个出库单，导致出现重复的数据，
                    //故加distinct过滤重复数据
                    sql.Append(@"SELECT distinct a.code, 
                                    x.ContactPerson,
                                    x.ContactPhone,
                                    x.ReceivingAddress,
                                     a.ReceivingCompanyName,--收货单位
                                     (select sum (SettlementPrice*ShippingAmount) from PartsShippingOrderDetail where a.id=PartsShippingOrderId),--
                                    a.OriginalRequirementBillCode--
                                    FROM PartsSalesOrder x
                                    inner join PartsShippingOrder a on x.Code = a.OriginalRequirementBillCode 
                                    inner join PartsSalesCategory b  on b.id=a.PartsSalesCategoryId
                                      where b.name in ('随车行','电子商务') ");
                    var dbParameters = new List<DbParameter>();

                    #region 条件过滤
                    if(ids != null && ids.Length > 0) {
                        sql.Append(" and a.id in (");
                        for(var i = 0; i < ids.Length; i++) {
                            if(ids.Length == i + 1) {
                                sql.Append("{0}id" + i);
                                dbParameters.Add(db.CreateDbParameter("id" + i, ids[i]));
                            } else {
                                sql.Append("{0}id" + i + ",");
                                dbParameters.Add(db.CreateDbParameter("id" + i, ids[i]));
                            }
                        }
                        sql.Append(")");
                    } else {
                        if(closedLoopStatus.HasValue) {
                            if(closedLoopStatus == 1) {
                                //未闭环
                                sql.Append(" and a.Status in(1,4,5,6) ");
                            } else if(closedLoopStatus == 2) {
                                //闭环
                                sql.Append(" and a.Status in(2,3,99)  ");
                            }
                        }
                        if(settlementCompanyId.HasValue) {
                            sql.Append(" and a.settlementCompanyId={0}settlementCompanyId");
                            dbParameters.Add(db.CreateDbParameter("settlementCompanyId", settlementCompanyId.Value));
                        }
                        if(receivingCompanyId.HasValue) {
                            sql.Append(" and a.receivingCompanyId={0}receivingCompanyId");
                            dbParameters.Add(db.CreateDbParameter("receivingCompanyId", receivingCompanyId.Value));
                        }
                        if(!string.IsNullOrEmpty(code)) {
                            sql.Append(" and a.code like {0}code ");
                            dbParameters.Add(db.CreateDbParameter("code", "%" + code + "%"));
                        }
                        if(type.HasValue) {
                            sql.Append(" and a.type={0}type");
                            dbParameters.Add(db.CreateDbParameter("type", type.Value));
                        }
                        if(status.HasValue) {
                            sql.Append(" and a.status={0}status");
                            dbParameters.Add(db.CreateDbParameter("status", status.Value));
                        }
                        if(warehouseId.HasValue) {
                            sql.Append(" and a.warehouseId={0}warehouseId");
                            dbParameters.Add(db.CreateDbParameter("warehouseId", warehouseId.Value));
                        }
                        if(!string.IsNullOrEmpty(warehouseName)) {
                            sql.Append(" and a.warehouseName like {0}warehouseName ");
                            dbParameters.Add(db.CreateDbParameter("warehouseName", "%" + warehouseName + "%"));
                        }
                        if(requestedArrivalDateBegin.HasValue) {
                            sql.Append(" and a.requestedArrivalDate>=to_date({0}requestedArrivalDateBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = requestedArrivalDateBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("requestedArrivalDateBegin", tempTime.ToString("G")));
                        }
                        if(requestedArrivalDateEnd.HasValue) {
                            sql.Append(" and a.requestedArrivalDate<=to_date({0}requestedArrivalDateEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = requestedArrivalDateEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("requestedArrivalDateEnd", tempTime.ToString("G")));
                        }
                        if(shippingDateBegin.HasValue) {
                            sql.Append(" and a.shippingDate>=to_date({0}shippingDateBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = shippingDateBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("shippingDateBegin", tempTime.ToString("G")));
                        }
                        if(shippingDateEnd.HasValue) {
                            sql.Append(" and a.shippingDate<=to_date({0}shippingDateEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = shippingDateEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("shippingDateEnd", tempTime.ToString("G")));
                        }
                        if(createTimeBegin.HasValue) {
                            sql.Append(" and a.createTime>=to_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(" and a.createTime<=to_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                        if(confirmedReceptionTimeBegin.HasValue) {
                            sql.Append(" and a.confirmedReceptionTime>=to_date({0}confirmedReceptionTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = confirmedReceptionTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("confirmedReceptionTimeBegin", tempTime.ToString("G")));
                        }
                        if(confirmedReceptionTimeEnd.HasValue) {
                            sql.Append(" and a.confirmedReceptionTime<=to_date({0}confirmedReceptionTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = confirmedReceptionTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("confirmedReceptionTimeEnd", tempTime.ToString("G")));
                        }
                        if(!string.IsNullOrEmpty(receivingCompanyCode)) {
                            sql.Append(" and a.receivingCompanyCode like {0}receivingCompanyCode ");
                            dbParameters.Add(db.CreateDbParameter("receivingCompanyCode", "%" + receivingCompanyCode + "%"));
                        }
                        if(!string.IsNullOrEmpty(receivingCompanyName)) {
                            sql.Append(" and a.receivingCompanyName like {0}receivingCompanyName ");
                            dbParameters.Add(db.CreateDbParameter("receivingCompanyName", "%" + receivingCompanyName + "%"));
                        }
                        if(!string.IsNullOrEmpty(receivingWarehouseName)) {
                            sql.Append(" and a.receivingWarehouseName like {0}receivingWarehouseName");
                            dbParameters.Add(db.CreateDbParameter("receivingWarehouseName", "%" + receivingWarehouseName + "%"));
                        }
                        if(partsSalesCategoryId.HasValue) {
                            sql.Append(" and a.partsSalesCategoryId={0}partsSalesCategoryId");
                            dbParameters.Add(db.CreateDbParameter("partsSalesCategoryId", partsSalesCategoryId.Value));
                        }
                        if(logisticCompanyId.HasValue) {
                            sql.Append(" and a.logisticCompanyId={0}logisticCompanyId");
                            dbParameters.Add(db.CreateDbParameter("logisticCompanyId", logisticCompanyId.Value));
                        }
                        if(!string.IsNullOrEmpty(logisticCompanyCode)) {
                            sql.Append(" and a.logisticCompanyCode like {0}logisticCompanyCode ");
                            dbParameters.Add(db.CreateDbParameter("logisticCompanyCode", "%" + logisticCompanyCode + "%"));
                        }
                        if(!string.IsNullOrEmpty(logisticCompanyName)) {
                            sql.Append(" and a.logisticCompanyName like {0}logisticCompanyName ");
                            dbParameters.Add(db.CreateDbParameter("logisticCompanyName", "%" + logisticCompanyName + "%"));
                        }
                        if(!string.IsNullOrEmpty(expressCompany)) {
                            sql.Append(" and a.expressCompany like {0}expressCompany ");
                            dbParameters.Add(db.CreateDbParameter("expressCompany", "%" + expressCompany + "%"));
                        }
                        if(!string.IsNullOrEmpty(expressCompanyCode)) {
                            sql.Append(" and a.expressCompanyCode like {0}expressCompanyCode ");
                            dbParameters.Add(db.CreateDbParameter("expressCompanyCode", "%" + expressCompanyCode + "%"));
                        }
                        if(!string.IsNullOrEmpty(eRPSOURCEORDERCODE)) {
                            sql.Append(" and x.eRPSOURCEORDERCODE like {0}eRPSOURCEORDERCODE ");
                            dbParameters.Add(db.CreateDbParameter("eRPSOURCEORDERCODE", "%" + eRPSOURCEORDERCODE + "%"));
                        }
                    }
                    #endregion

                    var sqlStr = sql.ToString();
                    if(sqlStr.IndexOf(" where 1=1  and ") > 0)
                        sqlStr = sqlStr.Replace(" where 1=1  and ", " where ");
                    else
                        sqlStr = sqlStr.Replace(" where 1=1 ", "");
                    var command = db.CreateDbCommand(string.Format(sqlStr, db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    conn.Open();
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    ErrorStrings.Export_Title_AgencyPartsShippingOrder_Code, ErrorStrings.Export_Title_DealerPartsRetailOrder_Customer, ErrorStrings.Export_Title_DealerPartsRetailOrder_CustomerPhone, ErrorStrings.Export_Title_DealerPartsRetailOrder_Address,ErrorStrings.Export_Title_SupplierShippingOrder_ReveiveCompany,ErrorStrings.Export_Title_InternalAllocationBill_TotalAmount,"原始单据号"
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception ex) {
                throw ex;
            }
        }


        public bool 批量发货确认(string fileName, out int excelImportNum, out List<PartsShippingOrderExtend> rightData, out List<PartsShippingOrderExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<PartsShippingOrderExtend>();
            var rightList = new List<PartsShippingOrderExtend>();
            var allList = new List<PartsShippingOrderExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("PartsShippingOrder", out notNullableFields, out fieldLenght);
                List<object> excelColumns;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_SupplierShippingOrder_Code, "Code");
                    excelOperator.AddColumnDataSource("发货日期", "ShippingTime");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_AgencyPartsShippingOrder_Logistic, "Logistic");
                    excelOperator.AddColumnDataSource("快递单号", "DeliveryBillNumber");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_AgencyPartsShippingOrder_ArrivalMode, "ArrivalMode");
                    //excelOperator.AddColumnDataSource("发货信息", "ShippingMsg");
                    excelOperator.AddColumnDataSource("查询网址", "QueryURL");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_AgencyPartsShippingOrder_ExpressCompanyName, "ExpressCompany");
                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;

                    var keyValuePairs = new[] {
                        new KeyValuePair<string, string>("ArrivalMode", "ArrivalMode"),
                    };
                    tempExcelOperator.ImportHelper.LoadEnumFromDb(null, null, keyValuePairs);

                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var partsshippingorder = new PartsShippingOrderExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        partsshippingorder.Code = newRow["Code"];
                        partsshippingorder.StrShippingTime = newRow["ShippingTime"];
                        partsshippingorder.Logistic = newRow["Logistic"];
                        partsshippingorder.DeliveryBillNumber = newRow["DeliveryBillNumber"];
                        partsshippingorder.StrArrivalMode = newRow["ArrivalMode"];
                        //partsshippingorder.ShippingMsg = newRow["ShippingMsg"];
                        partsshippingorder.QueryURL = newRow["QueryURL"];
                        partsshippingorder.ExpressCompany = newRow["ExpressCompany"] == null ? null : newRow["ExpressCompany"].ToUpper();
                        var tempErrorMessage = new List<string>();
                        //发运单编号检查                    
                        if(string.IsNullOrEmpty(partsshippingorder.Code)) {
                            // var fieldIndex = notNullableFields.IndexOf("CODE");
                            // if(fieldIndex > -1)
                            tempErrorMessage.Add("发运单编号不能为空");
                        } else {
                            //发运单编号不得超过50
                            if(Encoding.Default.GetByteCount(partsshippingorder.Code) > 50) //fieldLenght["CODE"]
                                tempErrorMessage.Add("发运单编号过长");
                        }
                        //发货日期检查                   
                        if(string.IsNullOrEmpty(partsshippingorder.StrShippingTime)) {
                            // var fieldIndex = notNullableFields.IndexOf("ShippingTime");
                            // if(fieldIndex > -1)
                            tempErrorMessage.Add("发货日期不能为空");
                        } else {
                            DateTime dt;
                            if(!DateTime.TryParse(partsshippingorder.StrShippingTime, out dt)) {
                                tempErrorMessage.Add("发货日期格式不正确");
                            } else {
                                partsshippingorder.ShippingTime = dt;
                            }
                        }
                        //到货模式检查                     
                        if(string.IsNullOrEmpty(partsshippingorder.StrArrivalMode)) {
                            //  var fieldIndex = notNullableFields.IndexOf("ArrivalMode");
                            //  if(fieldIndex > -1)
                            tempErrorMessage.Add("到货模式不能为空");
                        } else {
                            DcsArrivalMode mode;
                            if(!DcsArrivalMode.TryParse(partsshippingorder.StrArrivalMode, true, out mode))
                                tempErrorMessage.Add("到货模式不正确");
                            partsshippingorder.ArrivalMode = (int)mode;
                        }

                        //承运商检查
                        if(!string.IsNullOrEmpty(partsshippingorder.Logistic) && Encoding.Default.GetByteCount(partsshippingorder.Logistic) > 100)
                            tempErrorMessage.Add("承运商过长");

                        //发货信息检查
                        //if(!string.IsNullOrEmpty(partsshippingorder.ShippingMsg) && Encoding.Default.GetByteCount(partsshippingorder.ShippingMsg) > 100)
                        //    tempErrorMessage.Add("发货信息过长");

                        //查询网址检查
                        if(!string.IsNullOrEmpty(partsshippingorder.QueryURL) && Encoding.Default.GetByteCount(partsshippingorder.QueryURL) > 100)
                            tempErrorMessage.Add("查询网址过长");

                        //送货单号检查
                        if(!string.IsNullOrEmpty(partsshippingorder.DeliveryBillNumber) && Encoding.Default.GetByteCount(partsshippingorder.DeliveryBillNumber) > 50)
                            tempErrorMessage.Add("送货单号过长");


                        if(tempErrorMessage.Count > 0) {
                            partsshippingorder.ErrorMsg = string.Join("; ", tempErrorMessage);
                            allList.Add(partsshippingorder);
                        } else
                            allList.Add(partsshippingorder);
                        return false;
                    });
                    //校验导入的数据配件编号重复
                    var groups = allList.GroupBy(r => r.Code).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        if(groupItem.ErrorMsg == null) {
                            groupItem.ErrorMsg = ErrorStrings.Export_Title_SupplierShippingOrder_Code + groupItem.Code + ErrorStrings.Export_Validation_PartsBranch_Validation5;
                        } else {
                            groupItem.ErrorMsg = groupItem.ErrorMsg + ";发运单编号" + groupItem.Code + ErrorStrings.Export_Validation_PartsBranch_Validation5;
                        }
                    }
                    var partsshippingorderNeedCheck = allList.ToList();
                    var CodesNeedCheck = partsshippingorderNeedCheck.Select(r => r.Code).Distinct().ToArray();
                    var dbCodes = new List<string>();
                    Func<string[], bool> getDbCodes = vales => {
                        dbCodes.Add(vales[0]);
                        return false;
                    };
                    //根据SQL查询数据库，将查出来的结果根据定义的委托中的方法（这里是getDbCodes）放到具体的集合中（这里是dbSparePartCodes）
                    db.QueryDataWithInOperator("select Code from PartsShippingOrder where status = 1", "Code", true, CodesNeedCheck, getDbCodes);
                    if(CodesNeedCheck.Length > 0) {
                        var sparePartsExistsCode = partsshippingorderNeedCheck.Where(r => !dbCodes.Contains(r.Code)).ToArray();
                        foreach(var item in sparePartsExistsCode) {
                            if(item.ErrorMsg == null) {
                                item.ErrorMsg = "系统中发运单编号" + item.Code + "不存在或者状态不为新建";
                            } else {
                                item.ErrorMsg = item.ErrorMsg + ";系统中发运单编号" + item.Code + "不存在或者状态不为新建";
                            }
                        }
                    }
                }

                //快递公司
                var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                var tempexpresscode = tempRightList.Where(v => v.ExpressCompany != null).Select(r => r.ExpressCompany.ToUpper()).Distinct().ToArray();
                var dbExpresses = new List<ExpressToLogisticExtend>();
                Func<string[], bool> getDbExpress = value => {
                    var dbExpress = new ExpressToLogisticExtend {
                        ExpressId = Convert.ToInt32(value[0]),
                        ExpressCode = value[1],
                        ExpressName = value[2].ToUpper()
                    };
                    dbExpresses.Add(dbExpress);
                    return false;
                };
                db.QueryDataWithInOperator(@"select Id,ExpressCode,ExpressName from Express where Status=1 ", "ExpressName", true, tempexpresscode, getDbExpress);
                var temp = tempRightList.Where(r => r.ExpressCompany != null).ToList();
                var errorExpress = temp.Where(r => dbExpresses.All(v => r.ExpressCompany.ToUpper() != v.ExpressName.ToUpper() || string.IsNullOrEmpty(r.ExpressCompany.ToUpper()))).ToList();
                foreach(var errorItem in errorExpress) {
                    if(errorItem.ErrorMsg == null) {
                        errorItem.ErrorMsg = "快递公司不存在";
                    } else {
                        errorItem.ErrorMsg = errorItem.ErrorMsg + ";快递公司不存在";
                    }
                }
                //发运单查询
                tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                var codes = tempRightList.Select(r => r.Code).Distinct().ToArray();
                var dbPartsShippingOrders = new List<PartsShippingOrderExtend>();
                Func<string[], bool> getDbPartsShippingOrder = value => {
                    var dbPartsShippingOrder = new PartsShippingOrderExtend {
                        OriginalRequirementBillCode = value[0],
                        WarehouseName = value[1],
                        ReceivingAddress = value[2],
                        RequestedArrivalDate = Convert.ToDateTime(value[3]),
                        ShippingDate = Convert.ToDateTime(value[4]),
                        ShippingMethod = Convert.ToInt32(value[5]),
                        LogisticCompanyName = value[6],
                        Code = value[7],
                        Type = Convert.ToInt32(value[8])

                    };
                    dbPartsShippingOrders.Add(dbPartsShippingOrder);
                    return false;
                };
                db.QueryDataWithInOperator(@"select OriginalRequirementBillCode,WarehouseName,ReceivingAddress,RequestedArrivalDate,ShippingDate,ShippingMethod,LogisticCompanyName,Code,Type from PartsShippingOrder   ", "Code", true, codes, getDbPartsShippingOrder);

                //配件销售订单
                var originalRequirementBillCodes = dbPartsShippingOrders.Select(r => r.OriginalRequirementBillCode).Distinct().ToArray();
                var dbPartsSalesOrders = new List<PartsShippingOrderExtend>();
                Func<string[], bool> getDbPartsSalesOrder = value => {
                    var dbPartsSalesOrder = new PartsShippingOrderExtend {
                        Code = value[0],
                        ApproveTime = Convert.ToDateTime(value[1])
                    };
                    dbPartsSalesOrders.Add(dbPartsSalesOrder);
                    return false;
                };
                db.QueryDataWithInOperator(@"select Code,ApproveTime from PartsSalesOrder   ", "Code", true, originalRequirementBillCodes, getDbPartsSalesOrder);

                ////物流公司
                //var tempLogisticCompanyRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                //var tempeLogisticCompanyName = tempLogisticCompanyRightList.Where(v => v.LogisticCompanyName != null).Select(r => r.LogisticCompanyName.ToUpper()).Distinct().ToArray();
                //var dbLogisticCompanys = new List<PartsShippingOrderExtend>();
                //Func<string[], bool> getDbLogisticCompanys = value => {
                //    var dbLogisticCompany = new PartsShippingOrderExtend {
                //        LogisticCompanyId = Convert.ToInt32(value[0]),
                //        LogisticCompanyCode = value[1].ToUpper(),
                //        LogisticCompanyName = value[2].ToUpper()
                //    };
                //    dbLogisticCompanys.Add(dbLogisticCompany);
                //    return false;
                //};
                //db.QueryDataWithInOperator(@"select Id,Code,Name from LogisticCompany where Status=1 ", "Name", true, tempeLogisticCompanyName, getDbLogisticCompanys);
                //var errorExpress = tempLogisticCompanyRightList.Where(r => dbLogisticCompanys.All(v => r.LogisticCompanyName.ToUpper() != v.LogisticCompanyName.ToUpper() || string.IsNullOrEmpty(r.LogisticCompanyName.ToUpper()))).ToList();
                //foreach(var errorItem in errorExpress) {
                //    if(errorItem.ErrorMsg == null) {
                //        errorItem.ErrorMsg = "快递公司不存在";
                //    } else {
                //        errorItem.ErrorMsg = errorItem.ErrorMsg + ";快递公司不存在";
                //    }
                //}

                //获取所有不合格数据
                errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                //获取所有合格数据
                rightList = allList.Except(errorList).ToList();
                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempSparePart = list[index - 1];
                            var values = new object[] {
                                tempSparePart.Code, tempSparePart.StrShippingTime, tempSparePart.Logistic, tempSparePart.DeliveryBillNumber, tempSparePart.StrArrivalMode,
                                //tempSparePart.ShippingMsg,
                                tempSparePart.QueryURL, tempSparePart.ExpressCompany, tempSparePart.ErrorMsg
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }

                //导入所有合格数据
                if(!rightList.Any())
                    return true;
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    //开启事务，修改在一个事务内
                    var ts = conn.BeginTransaction();
                    try {
                        //修改件
                        if(rightList.Any()) {
                            //获取修改数据的sql语句，Code为主键
                            //var sqlInsert = db.GetUpdateSql("PartsShippingOrder"
                            //    , new[] { "Status", "ShippingTime", "LogisticCompanyId", "LogisticCompanyCode", "LogisticCompanyName", "ExpressCompanyId", "ExpressCompany", "ExpressCompanyCode", "DeliveryBillNumber", "ArrivalMode", "ShippingMsg", "QueryURL", "ModifierId", "ModifierName", "ModifyTime" }
                            //    , new[] { "Code" }
                            //);
                            var sqlInsert = db.GetUpdateSql("PartsShippingOrder", new[] {
                                "Status", "ShippingTime", "ExpressCompanyId", "ExpressCompany", "ExpressCompanyCode", "DeliveryBillNumber", "ArrivalMode", "QueryURL", "ModifierId", "ModifierName", "ModifyTime", "Logistic"
                            }, new[] {
                                "Code"
                            });
                            var sqlInsertLogistics = db.GetInsertSql("Logistics", "Id", new[] {
                                "ShippingCode", "OrderCode", "ApproveTime", "WarehouseName", "ReceivingAddress", "RequestedArrivalDate", "ShippingDate", "ShippingMethod", "LogisticName", "SignStatus", "Status"
                            });
                            var userInfo = Utils.GetCurrentUserInfo();
                            //往数据库增加配件信息
                            foreach(var item in rightList) {
                                var express = dbExpresses.Where(r => r.ExpressName == item.ExpressCompany).FirstOrDefault();
                                //var logisticCompany = dbLogisticCompanys.Where(r => r.LogisticCompanyName == item.LogisticCompanyName).FirstOrDefault();
                                var command = db.CreateDbCommand(sqlInsert, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("Status", (int)DcsPartsShippingOrderStatus.已发货));
                                command.Parameters.Add(db.CreateDbParameter("ShippingTime", item.ShippingTime));
                                //command.Parameters.Add(db.CreateDbParameter("LogisticCompanyId", logisticCompany == null ? 0 : logisticCompany.LogisticCompanyId));
                                //command.Parameters.Add(db.CreateDbParameter("LogisticCompanyCode", logisticCompany == null ? null : logisticCompany.LogisticCompanyCode));
                                //command.Parameters.Add(db.CreateDbParameter("LogisticCompanyName", item.LogisticCompanyName));
                                command.Parameters.Add(db.CreateDbParameter("Logistic", item.Logistic));
                                command.Parameters.Add(db.CreateDbParameter("ExpressCompanyId", express == null ? 0 : express.ExpressId));
                                command.Parameters.Add(db.CreateDbParameter("ExpressCompanyCode", express == null ? null : express.ExpressCode));
                                command.Parameters.Add(db.CreateDbParameter("ExpressCompany", item.ExpressCompany));
                                command.Parameters.Add(db.CreateDbParameter("DeliveryBillNumber", item.DeliveryBillNumber));
                                command.Parameters.Add(db.CreateDbParameter("ArrivalMode", item.ArrivalMode));
                                //command.Parameters.Add(db.CreateDbParameter("ShippingMsg", item.ShippingMsg));
                                command.Parameters.Add(db.CreateDbParameter("QueryURL", item.QueryURL));
                                command.Parameters.Add(db.CreateDbParameter("ModifierId", userInfo.Id));
                                command.Parameters.Add(db.CreateDbParameter("ModifierName", userInfo.Name));
                                command.Parameters.Add(db.CreateDbParameter("ModifyTime", DateTime.Now));
                                command.Parameters.Add(db.CreateDbParameter("Code", item.Code));
                                command.ExecuteNonQuery();

                                //var partsShippingOrder = dbPartsShippingOrders.Where(r => r.Code == item.Code).FirstOrDefault();
                                //if(partsShippingOrder.Type == (int)DcsPartsShippingOrderType.销售) {
                                //    var partsSalesOrder = dbPartsSalesOrders.Where(r => r.Code == partsShippingOrder.OriginalRequirementBillCode).FirstOrDefault();
                                //    if(partsSalesOrder != null) {
                                //        var commandLogistics = db.CreateDbCommand(sqlInsertLogistics, conn, ts);
                                //        commandLogistics.Parameters.Add(db.CreateDbParameter("ShippingCode", item.Code));
                                //        commandLogistics.Parameters.Add(db.CreateDbParameter("OrderCode", partsShippingOrder.OriginalRequirementBillCode));
                                //        commandLogistics.Parameters.Add(db.CreateDbParameter("ApproveTime", partsSalesOrder.ApproveTime));
                                //        commandLogistics.Parameters.Add(db.CreateDbParameter("WarehouseName", partsShippingOrder.WarehouseName));
                                //        commandLogistics.Parameters.Add(db.CreateDbParameter("ReceivingAddress", partsShippingOrder.ReceivingAddress));
                                //        commandLogistics.Parameters.Add(db.CreateDbParameter("RequestedArrivalDate", partsShippingOrder.RequestedArrivalDate));
                                //        commandLogistics.Parameters.Add(db.CreateDbParameter("ShippingDate", partsShippingOrder.ShippingDate));
                                //        commandLogistics.Parameters.Add(db.CreateDbParameter("ShippingMethod", partsShippingOrder.ShippingMethod));
                                //        commandLogistics.Parameters.Add(db.CreateDbParameter("LogisticName", partsShippingOrder.LogisticCompanyName));
                                //        commandLogistics.Parameters.Add(db.CreateDbParameter("SignStatus", (int)DcsSignStatus.在途));
                                //        commandLogistics.Parameters.Add(db.CreateDbParameter("Status", (int)DcsBaseDataStatus.有效));
                                //        commandLogistics.ExecuteNonQuery();
                                //    }

                                //}


                            }
                        }
                        //无异常提交
                        ts.Commit();
                    } catch(Exception ex) {
                        //报错回滚
                        ts.Rollback();
                        throw new Exception(ex.Message);
                    } finally {
                        if(conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }

        public bool 批量送达确认(string fileName, out int excelImportNum, out List<PartsShippingOrderExtend> rightData, out List<PartsShippingOrderExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<PartsShippingOrderExtend>();
            var rightList = new List<PartsShippingOrderExtend>();
            var allList = new List<PartsShippingOrderExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("PartsShippingOrder", out notNullableFields, out fieldLenght);
                List<object> excelColumns;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_SupplierShippingOrder_Code, "Code");
                    excelOperator.AddColumnDataSource("到货日期", "ArrivalTime");
                    excelOperator.AddColumnDataSource("到货信息", "CargoTerminalMsg");

                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;
                    var keyValuePairs = new[] {
                        new KeyValuePair<string, string>("ArrivalMode", "ArrivalMode"),
                    };
                    tempExcelOperator.ImportHelper.LoadEnumFromDb(null, null, keyValuePairs);
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var partsshippingorder = new PartsShippingOrderExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        partsshippingorder.Code = newRow["Code"];
                        partsshippingorder.StrArrivalTime = newRow["ArrivalTime"];
                        partsshippingorder.CargoTerminalMsg = newRow["CargoTerminalMsg"];

                        var tempErrorMessage = new List<string>();
                        //发运单编号检查

                        if(string.IsNullOrEmpty(partsshippingorder.Code)) {
                            //   var fieldIndex = notNullableFields.IndexOf("CODE");
                            //  if(fieldIndex > -1)
                            tempErrorMessage.Add("发运单编号不能为空");
                        } else {
                            //发运单编号不得超过50
                            if(Encoding.Default.GetByteCount(partsshippingorder.Code) > 50) //fieldLenght["CODE"]
                                tempErrorMessage.Add("发运单编号过长");
                        }
                        //到货日期检查

                        if(string.IsNullOrEmpty(partsshippingorder.StrArrivalTime)) {
                            //  var fieldIndex = notNullableFields.IndexOf("ArrivalTime");
                            //   if(fieldIndex > -1)
                            tempErrorMessage.Add("到货日期不能为空");
                        } else {
                            DateTime dt;
                            if(!DateTime.TryParse(partsshippingorder.StrArrivalTime, out dt)) {
                                tempErrorMessage.Add("到货日期格式不正确");
                            } else {
                                partsshippingorder.ArrivalTime = dt;
                            }
                        }

                        //到货信息检查
                        if(!string.IsNullOrEmpty(partsshippingorder.CargoTerminalMsg) && Encoding.Default.GetByteCount(partsshippingorder.CargoTerminalMsg) > 100)
                            tempErrorMessage.Add("到货信息过长");

                        if(tempErrorMessage.Count > 0) {
                            partsshippingorder.ErrorMsg = string.Join("; ", tempErrorMessage);
                            allList.Add(partsshippingorder);
                        } else
                            allList.Add(partsshippingorder);
                        return false;
                    });
                    //校验导入的数据配件编号重复
                    var groups = allList.GroupBy(r => r.Code).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        if(groupItem.ErrorMsg == null) {
                            groupItem.ErrorMsg = ErrorStrings.Export_Title_SupplierShippingOrder_Code + groupItem.Code + ErrorStrings.Export_Validation_PartsBranch_Validation5;
                        } else {
                            groupItem.ErrorMsg = groupItem.ErrorMsg + ";发运单编号" + groupItem.Code + ErrorStrings.Export_Validation_PartsBranch_Validation5;
                        }
                    }
                }

                var partsshippingorderNeedCheck = allList.ToList();
                var CodesNeedCheck = partsshippingorderNeedCheck.Select(r => r.Code).Distinct().ToArray();
                var dbCodes = new List<PartsShippingOrderExtend>();
                Func<string[], bool> getDbCodes = vales => {
                    dbCodes.Add(new PartsShippingOrderExtend() {
                        Code = vales[0],
                        StrArrivalMode = vales[1],
                        ShippingTime = DateTime.Parse(vales[2])
                    });
                    return false;
                };
                //根据SQL查询数据库，将查出来的结果根据定义的委托中的方法（这里是getDbCodes）放到具体的集合中（这里是dbSparePartCodes）
                db.QueryDataWithInOperator("select Code,ArrivalMode,ShippingTime from PartsShippingOrder where status = 4 and ArrivalMode in (1,2)", "Code", true, CodesNeedCheck, getDbCodes);
                if(CodesNeedCheck.Length > 0) {
                    var sparePartsExistsCode = partsshippingorderNeedCheck.Where(r => !dbCodes.Exists(e => e.Code == r.Code)).ToArray();
                    foreach(var item in sparePartsExistsCode) {
                        if(item.ErrorMsg == null) {
                            item.ErrorMsg = "系统中发运单编号" + item.Code + ErrorStrings.Export_Validation_Sparepart_NotExist;
                        } else {
                            item.ErrorMsg = item.ErrorMsg + ";系统中发运单编号" + item.Code + ErrorStrings.Export_Validation_Sparepart_NotExist;
                        }
                    }
                }


                //到货日期必须大于发货日期
                foreach(var item in allList.Where(e => string.IsNullOrEmpty(e.ErrorMsg))) {
                    var dbitem = dbCodes.Where(e => e.Code == item.Code).FirstOrDefault();
                    if(dbitem.ShippingTime >= item.ArrivalTime) {
                        if(item.ErrorMsg == null) {
                            item.ErrorMsg = "到货日期必须大于发货日期";
                        } else {
                            item.ErrorMsg = item.ErrorMsg + ";到货日期必须大于发货日期";
                        }
                    }
                }

                //获取所有不合格数据
                errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                //获取所有合格数据
                rightList = allList.Except(errorList).ToList();
                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempSparePart = list[index - 1];
                            var values = new object[] {
                                tempSparePart.Code, tempSparePart.StrArrivalTime, tempSparePart.CargoTerminalMsg, tempSparePart.ErrorMsg
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }

                //导入所有合格数据
                if(!rightList.Any())
                    return true;
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    //开启事务，修改在一个事务内
                    var ts = conn.BeginTransaction();
                    try {
                        //修改件
                        if(rightList.Any()) {
                            //获取修改数据的sql语句，Code为主键
                            var sqlInsert = db.GetUpdateSql("PartsShippingOrder", new[] {
                                "Status", "ArrivalTime", "CargoTerminalMsg", "ModifierId", "ModifierName", "ModifyTime"
                            }, new[] {
                                "Code"
                            });
                            var userInfo = Utils.GetCurrentUserInfo();
                            //往数据库增加配件信息
                            foreach(var item in rightList) {
                                var command = db.CreateDbCommand(sqlInsert, conn, ts);
                                var dbitem = dbCodes.Where(e => e.Code == item.Code).FirstOrDefault();
                                command.Parameters.Add(db.CreateDbParameter("Status", ((int)DcsArrivalMode.送货上门).ToString().Equals(dbitem.StrArrivalMode) ? (int)DcsPartsShippingOrderStatus.待收货 : (int)DcsPartsShippingOrderStatus.待提货));
                                command.Parameters.Add(db.CreateDbParameter("ArrivalTime", item.ArrivalTime));
                                command.Parameters.Add(db.CreateDbParameter("CargoTerminalMsg", item.CargoTerminalMsg));
                                command.Parameters.Add(db.CreateDbParameter("ModifierId", userInfo.Id));
                                command.Parameters.Add(db.CreateDbParameter("ModifierName", userInfo.Name));
                                command.Parameters.Add(db.CreateDbParameter("ModifyTime", DateTime.Now));
                                command.Parameters.Add(db.CreateDbParameter("Code", item.Code));
                                command.ExecuteNonQuery();
                            }
                        }
                        //无异常提交
                        ts.Commit();
                    } catch(Exception ex) {
                        //报错回滚
                        ts.Rollback();
                        throw new Exception(ex.Message);
                    } finally {
                        if(conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }

        //合并导出瑞沃配件发运单查询
        public bool ExportSendWayBill(int[] ids, string code, int? type, int? status, int? warehouseId, DateTime? requestedArrivalDateBegin, DateTime? requestedArrivalDateEnd, DateTime? shippingDateBegin, DateTime? shippingDateEnd, DateTime? createTimeBegin, DateTime? createTimeEnd, DateTime? confirmedReceptionTimeBegin, DateTime? confirmedReceptionTimeEnd, string receivingCompanyCode, string receivingCompanyName, string receivingWarehouseName, int? partsSalesCategoryId, string logisticCompanyName, out string fileName) {
            fileName = GetExportFilePath("配件发运单查询_" + ".xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {
                    var sql = new StringBuilder();

                    #region
                    sql.Append(@"select a.name,
                                        a.code, 
                                        (select value from keyvalueitem where NAME = 'PartsShippingOrder_Status'and key=a.Status) As status,
                                        (select value from keyvalueitem where NAME = 'PartsShippingOrder_Type'and key=a.Type) As Type,
                                        a.OriginalRequirementBillCode,
                                        a.ShippingCompanyCode,
                                        a.ShippingCompanyName,
                                        a.ReceivingCompanyCode,
                                        a.ReceivingCompanyName,
                                        a.ReceivingWarehouseName,
                                        a.zongjine,
                                        a.ReceivingAddress,
                                        (select value from keyvalueitem where NAME = 'IsOrNot'and key=a.IsTransportLosses) As IsTransportLosses,--是否路损
                                        (select value from keyvalueitem where NAME = 'PartsShippingOrder_TransportLossesDisposeStatus'and key=a.TransportLossesDisposeStatus) As TransportLossesDisposeStatus,--路损处理状态
                                        a.LogisticCompanyCode,
                                        a.LogisticCompanyName,
                                        (select value from keyvalueitem where NAME = 'PartsShipping_Method'and key=a.ShippingMethod) As ShippingMethod,
                                        a.InterfaceRecordId,
                                        a.RequestedArrivalDate,
                                        a.ShippingDate,
                                        a.TransportDriverPhone,
                                        a.DeliveryBillNumber,
                                        a.WarehouseName,
                                        a.TransportMileage,
                                        a.TransportVehiclePlate,
                                        a.TransportDriver,
                                        a.Remark,
                                        a.OrderApproveComment,
                                        a.CreatorName,
                                        a.CreateTime,
                                        a.ModifierName,
                                        a.ModifyTime,
                                        a.ConsigneeName,
                                        a.ConfirmedReceptionTime,
                                        a.ReceiptConfirmorName,
                                        a.ReceiptConfirmTime
                                        from SendWaybill a where 1=1 ");

                    #endregion

                    var dbParameters = new List<DbParameter>();

                    #region 条件过滤
                    if(ids != null && ids.Length > 0) {
                        sql.Append(" and a.PartsShippingOrderId in (");
                        for(var i = 0; i < ids.Length; i++) {
                            if(ids.Length == i + 1) {
                                sql.Append("{0}id" + i);
                                dbParameters.Add(db.CreateDbParameter("id" + i, ids[i]));
                            } else {
                                sql.Append("{0}id" + i + ",");
                                dbParameters.Add(db.CreateDbParameter("id" + i, ids[i]));
                            }
                        }
                        sql.Append(")");
                    } else {

                        if(!string.IsNullOrEmpty(code)) {
                            sql.Append(" and Upper(a.code) like Upper({0}code) ");
                            dbParameters.Add(db.CreateDbParameter("code", "%" + code + "%"));
                        }
                        if(type.HasValue) {
                            sql.Append(" and a.type={0}type");
                            dbParameters.Add(db.CreateDbParameter("type", type.Value));
                        }
                        if(status.HasValue) {
                            sql.Append(" and a.status={0}status");
                            dbParameters.Add(db.CreateDbParameter("status", status.Value));
                        }

                        if(warehouseId.HasValue) {
                            sql.Append(" and a.WarehouseId like {0}warehouseId ");
                            dbParameters.Add(db.CreateDbParameter("warehouseId", "%" + warehouseId + "%"));
                        }
                        if(requestedArrivalDateBegin.HasValue) {
                            sql.Append(" and a.requestedArrivalDate>=to_date({0}requestedArrivalDateBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = requestedArrivalDateBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("requestedArrivalDateBegin", tempTime.ToString("G")));
                        }
                        if(requestedArrivalDateEnd.HasValue) {
                            sql.Append(" and a.requestedArrivalDate<=to_date({0}requestedArrivalDateEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = requestedArrivalDateEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("requestedArrivalDateEnd", tempTime.ToString("G")));
                        }
                        if(shippingDateBegin.HasValue) {
                            sql.Append(" and a.shippingDate>=to_date({0}shippingDateBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = shippingDateBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("shippingDateBegin", tempTime.ToString("G")));
                        }
                        if(shippingDateEnd.HasValue) {
                            sql.Append(" and a.shippingDate<=to_date({0}shippingDateEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = shippingDateEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("shippingDateEnd", tempTime.ToString("G")));
                        }
                        if(createTimeBegin.HasValue) {
                            sql.Append(" and a.createTime>=to_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(" and a.createTime<=to_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                        if(confirmedReceptionTimeBegin.HasValue) {
                            sql.Append(" and a.confirmedReceptionTime>=to_date({0}confirmedReceptionTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = confirmedReceptionTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("confirmedReceptionTimeBegin", tempTime.ToString("G")));
                        }
                        if(confirmedReceptionTimeEnd.HasValue) {
                            sql.Append(" and a.confirmedReceptionTime<=to_date({0}confirmedReceptionTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = confirmedReceptionTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("confirmedReceptionTimeEnd", tempTime.ToString("G")));
                        }
                        if(!string.IsNullOrEmpty(receivingCompanyCode)) {
                            sql.Append(" and Upper(a.receivingCompanyCode) like Upper({0}receivingCompanyCode) ");
                            dbParameters.Add(db.CreateDbParameter("receivingCompanyCode", "%" + receivingCompanyCode + "%"));
                        }
                        if(!string.IsNullOrEmpty(receivingCompanyName)) {
                            sql.Append(" and a.receivingCompanyName like {0}receivingCompanyName ");
                            dbParameters.Add(db.CreateDbParameter("receivingCompanyName", "%" + receivingCompanyName + "%"));
                        }
                        if(!string.IsNullOrEmpty(receivingWarehouseName)) {
                            sql.Append(" and a.receivingWarehouseName like {0}receivingWarehouseName");
                            dbParameters.Add(db.CreateDbParameter("receivingWarehouseName", "%" + receivingWarehouseName + "%"));
                        }

                        if(partsSalesCategoryId.HasValue) {
                            sql.Append(" and a.PartsSalesCategoryId like {0}partsSalesCategoryId ");
                            dbParameters.Add(db.CreateDbParameter("partsSalesCategoryId", "%" + partsSalesCategoryId + "%"));
                        }
                        if(!string.IsNullOrEmpty(logisticCompanyName)) {
                            sql.Append(" and a.logisticCompanyName like {0}logisticCompanyName ");
                            dbParameters.Add(db.CreateDbParameter("logisticCompanyName", "%" + logisticCompanyName + "%"));
                        }

                    }
                    //sql.Append(" order by a.id ");
                    #endregion

                    var sqlStr = sql.ToString();
                    if(sqlStr.IndexOf(" where 1=1  and ") > 0)
                        sqlStr = sqlStr.Replace(" where 1=1  and ", " where ");
                    else
                        sqlStr = sqlStr.Replace(" where 1=1 ", "");
                    var command = db.CreateDbCommand(string.Format(sqlStr, db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    conn.Open();
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    ErrorStrings.Export_Title_Partssalescategory_Name, ErrorStrings.Export_Title_AgencyPartsShippingOrder_Code, ErrorStrings.Export_Title_AccountPeriod_Status, ErrorStrings.Export_Title_AgencyPartsShippingOrder_Type, ErrorStrings.Export_Title_PartsInboundCheckBill_OriginalRequirementBillCode, ErrorStrings.Export_Title_PartsShippingOrder_ShippingCode, ErrorStrings.Export_Title_PartsInboundCheckBill_CompanyName, ErrorStrings.Export_Title_Partsoutboundplan_ReceivingCompanyCode, ErrorStrings.Export_Title_PartsPurchaseOrder_ReceivingCompanyName, ErrorStrings.Export_Title_SupplierShippingOrder_ReveiveWarehouseName, ErrorStrings.Export_Title_InternalAllocationBill_TotalAmount, ErrorStrings.Export_Title_PartsPurchaseOrder_ReceiveAddress, "知否差异", "差异处理状态", ErrorStrings.Export_Title_AgencyPartsShippingOrder_LogisticCompanyCode, ErrorStrings.Export_Title_AgencyPartsShippingOrder_LogisticCompanyName, ErrorStrings.Export_Title_PartsPurchaseOrder_ShippingMethod, ErrorStrings.Export_Title_AgencyPartsShippingOrder_WMSCode, ErrorStrings.Export_Title_AgencyPartsShippingOrder_RequestedArrivalDate, "出库日期", ErrorStrings.Export_Title_PartsShippingOrder_DriverPhone, ErrorStrings.Export_Title_SupplierShippingOrder_DdeliverybillNumber, "出库仓库名称", "运输里程", "承运车辆编号", "承运司机", ErrorStrings.Export_Title_PartsBranch_Remark, ErrorStrings.Export_Title_PartsOutboundBill_OrderApproveComment, ErrorStrings.Export_Title_AccountPeriod_CreatorName, ErrorStrings.Export_Title_AccountPeriod_CreateTime, ErrorStrings.Export_Title_AccountPeriod_ModifierName, ErrorStrings.Export_Title_AccountPeriod_ModifyTime, ErrorStrings.Export_Title_InternalAllocationBill_ApproverName, ErrorStrings.Export_Title_InternalAllocationBill_ApproveTime, "完成人", "完成时间"
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }

        /// <summary>
        /// 合并导出配件发运单主清单
        /// </summary>
        public bool ExportPartsShippingOrder1(int[] ids, int? settlementCompanyId, int? receivingCompanyId, string code, int? type, int? status, int? warehouseId, string warehouseName, DateTime? requestedArrivalDateBegin, DateTime? requestedArrivalDateEnd, DateTime? shippingDateBegin, DateTime? shippingDateEnd, DateTime? createTimeBegin, DateTime? createTimeEnd, DateTime? confirmedReceptionTimeBegin, DateTime? confirmedReceptionTimeEnd, string receivingCompanyCode, string receivingCompanyName, string receivingWarehouseName, int? partsSalesCategoryId, int? logisticCompanyId, string logisticCompanyCode, string logisticCompanyName, string expressCompany, string expressCompanyCode, int? closedLoopStatus, string eRPSOURCEORDERCODE,string originalRequirementBillCode, out string fileName) {
            fileName = GetExportFilePath("配件发运单主清单_" + ".xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);
                var userInfo = Utils.GetCurrentUserInfo();
                using(var conn = db.CreateDbConnection()) {
                    var sql = new StringBuilder();
                    sql.Append(@"select a.Code, --配件发运单编号
                                       (select value from keyvalueitem where NAME = 'PartsShippingOrder_Type'and key=a.Type) As Type,
                                       d.partssalesordertypename,
                                        (select value from keyvalueitem where NAME = 'PartsShippingOrder_Status'and key=a.Status) As status,--状态
                                       cast(decode(a.Status,2,'闭环',3,'闭环',99,'闭环',1,'未闭环',4,'未闭环',5,'未闭环',6,'未闭环') as varchar2(20)) as ClosedLoopStatus,--闭环状态
                                       e.Name, --品牌
                                       (select sum(nvl(t.ShippingAmount,0) * nvl(t.settlementprice,0))  from PartsShippingOrderDetail t where t.PartsShippingOrderId = a.id) as TotalAmount, --总金额
                                       a.Weight,
                                       a.Volume,
                                       a.WarehouseName,
                                       --a.ShippingCompanyCode,--发货单位编号
                                       a.ShippingCompanyName, --发货单位名称
                                       --a.ReceivingCompanyCode,--收货单位编号
                                       a.ReceivingCompanyName, --收货单位名称
                                      a.ReceivingWarehouseName,--收货仓库名称
                                       --a.ReceivingAddress,--收货地址
                                       /*case a.IsTransportLosses--是否路损
                                       when 1 then '是'
                                       when 0 then '否'
                                       end  as IsTransportLosses,
                                       case a.TransportLossesDisposeStatus--路损处理状态
                                       when 1 then '未处理'
                                       when 2 then '处理完毕'
                                       end  as TransportLossesDisposeStatus,*/
                                       --a.LogisticCompanyCode,--物流公司编号
                                       a.LogisticCompanyName, --物流公司名称
                                       a.ExpressCompanyCode,
                                       a.ExpressCompany,
                                       (select value from keyvalueitem where NAME = 'PartsShipping_Method'and key=a.ShippingMethod) As ShippingMethod,
                                       to_char( a.RequestedArrivalDate,'yyyy-mm-dd hh24:mi:ss') as requestedArrivalDate, --a.RequestedArrivalDate, --要求到货日期
                                       to_char( a.ShippingDate,'yyyy-mm-dd hh24:mi:ss') as shippingDate, -- a.ShippingDate, --发运日期
                                       a.LogisticArrivalDate, --承运商运达时间
                                       to_char(a.ShippingTime,'yyyy-mm-dd hh24:mi:ss') as shippingTime, --a.ShippingTime,--物流发货日期
                                       a.ConfirmedReceptionTime,
                                       --a.TransportDriverPhone,--承运司机电话
                                       --a.DeliveryBillNumber,--送货单号
                                       a.ArrivalMode,--到货模式
                                       a.Logistic,--承运商
                                       a.DeliveryBillNumber,--送货单号
                                       a.CargoTerminalMsg,--货站信息

                                          /*case a.Status--状态
                                       when 1 then '新建'
                                       when 2 then '收货确认'
                                       when 3 then '回执确认'
                                       when 4 then '已发货'
                                       when 5 then '待提货'
                                       when 6 then '待收货'
                                       when 99 then '作废'
                                       end  as status,*/
                                       a.Remark, --备注
                                       a.CreatorName, --创建人
                                       a.CreateTime, --创建时间
                                       /* d.code outCode, */ --出库单编号
                                       b.PartsOutboundPlanCode, --出库计划编号
                                       case b.TaskType--任务类型
                                       when 2 then (select Code from BoxUpTask where id =b.TaskId)
                                       when 1 then (select Code from PickingTask where id =b.TaskId) end  as TaskCode,
                                       b.SparePartCode, --配件编号
                                       b.SparePartName, --配件名称
                                       b.ShippingAmount, --发运量
                                       b.ConfirmedAmount, --确认量
                                       b.settlementprice,--销售单价
                                       b.ConfirmedAmount * b.Settlementprice,--金额
                                       b.InTransitDamageLossAmount, --路损量
                                       /*case b.TransportLossesDisposeMethod--路损处理方式
                                       when 1 then '销售退回'
                                       when 2 then '销售承运商'
                                       when 3 then '正常收货'
                                       end  as TransportLossesDisposeMethod,
                                       b.SettlementPrice,--配件结算价*/
                                       d.ERPSOURCEORDERCODE,
                                       b.Remark --备注
                                  from PartsShippingOrder a
                                 inner join PartsShippingOrderDetail b
                                    on a.id = b.PartsShippingOrderId
                                    left join partssalesorder d 
                                    on a.originalrequirementbillcode=d.code
                                    Left Join PartsSalesCategory e
                                    On a.PartsSalesCategoryId = e.Id where  ((a.ReceivingCompanyId= " +userInfo.EnterpriseId +" and a.type!=4 ) or exists(select 1 from WarehouseOperator w where w.WarehouseId=a.WarehouseId and w.OperatorId= "+userInfo.Id+") ) ");
                    var dbParameters = new List<DbParameter>();

                    #region 条件过滤
                    if(ids != null && ids.Length > 0) {
                        sql.Append(" and a.id in (");
                        for(var i = 0; i < ids.Length; i++) {
                            if(ids.Length == i + 1) {
                                sql.Append("{0}id" + i);
                                dbParameters.Add(db.CreateDbParameter("id" + i, ids[i]));
                            } else {
                                sql.Append("{0}id" + i + ",");
                                dbParameters.Add(db.CreateDbParameter("id" + i, ids[i]));
                            }
                        }
                        sql.Append(")");
                    } else {
                        if(closedLoopStatus.HasValue) {
                            if(closedLoopStatus == 1) {
                                //未闭环
                                sql.Append(" and a.Status in(1,4,5,6) ");
                            } else if(closedLoopStatus == 2) {
                                //闭环
                                sql.Append(" and a.Status in(2,3,99)  ");
                            }
                        }
                        if(settlementCompanyId.HasValue) {
                            sql.Append(" and a.settlementCompanyId={0}settlementCompanyId");
                            dbParameters.Add(db.CreateDbParameter("settlementCompanyId", settlementCompanyId.Value));
                        }
                        if(receivingCompanyId.HasValue) {
                            sql.Append(" and a.receivingCompanyId={0}receivingCompanyId");
                            dbParameters.Add(db.CreateDbParameter("receivingCompanyId", receivingCompanyId.Value));
                        }
                        if(!string.IsNullOrEmpty(code)) {
                            sql.Append(" and Upper(a.code) like Upper({0}code) ");
                            dbParameters.Add(db.CreateDbParameter("code", "%" + code + "%"));
                        }
                        if(type.HasValue) {
                            sql.Append(" and a.type={0}type");
                            dbParameters.Add(db.CreateDbParameter("type", type.Value));
                        }
                        if(status.HasValue) {
                            sql.Append(" and a.status={0}status");
                            dbParameters.Add(db.CreateDbParameter("status", status.Value));
                        }
                        if(warehouseId.HasValue) {
                            sql.Append(" and a.warehouseId={0}warehouseId");
                            dbParameters.Add(db.CreateDbParameter("warehouseId", warehouseId.Value));
                        }
                        if(!string.IsNullOrEmpty(warehouseName)) {
                            sql.Append(" and a.warehouseName like {0}warehouseName ");
                            dbParameters.Add(db.CreateDbParameter("warehouseName", "%" + warehouseName + "%"));
                        }
                        if(requestedArrivalDateBegin.HasValue) {
                            sql.Append(" and a.requestedArrivalDate>=to_date({0}requestedArrivalDateBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = requestedArrivalDateBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("requestedArrivalDateBegin", tempTime.ToString("G")));
                        }
                        if(requestedArrivalDateEnd.HasValue) {
                            sql.Append(" and a.requestedArrivalDate<=to_date({0}requestedArrivalDateEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = requestedArrivalDateEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("requestedArrivalDateEnd", tempTime.ToString("G")));
                        }
                        if(shippingDateBegin.HasValue) {
                            sql.Append(" and a.shippingDate>=to_date({0}shippingDateBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = shippingDateBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("shippingDateBegin", tempTime.ToString("G")));
                        }
                        if(shippingDateEnd.HasValue) {
                            sql.Append(" and a.shippingDate<=to_date({0}shippingDateEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = shippingDateEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("shippingDateEnd", tempTime.ToString("G")));
                        }
                        if(createTimeBegin.HasValue) {
                            sql.Append(" and a.createTime>=to_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(" and a.createTime<=to_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                        if(confirmedReceptionTimeBegin.HasValue) {
                            sql.Append(" and a.confirmedReceptionTime>=to_date({0}confirmedReceptionTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = confirmedReceptionTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("confirmedReceptionTimeBegin", tempTime.ToString("G")));
                        }
                        if(confirmedReceptionTimeEnd.HasValue) {
                            sql.Append(" and a.confirmedReceptionTime<=to_date({0}confirmedReceptionTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = confirmedReceptionTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("confirmedReceptionTimeEnd", tempTime.ToString("G")));
                        }
                        if(!string.IsNullOrEmpty(receivingCompanyCode)) {
                            sql.Append(" and Upper(a.receivingCompanyCode) like Upper({0}receivingCompanyCode) ");
                            dbParameters.Add(db.CreateDbParameter("receivingCompanyCode", "%" + receivingCompanyCode + "%"));
                        }
                        if(!string.IsNullOrEmpty(receivingCompanyName)) {
                            sql.Append(" and a.receivingCompanyName like {0}receivingCompanyName ");
                            dbParameters.Add(db.CreateDbParameter("receivingCompanyName", "%" + receivingCompanyName + "%"));
                        }
                        if(!string.IsNullOrEmpty(receivingWarehouseName)) {
                            sql.Append(" and a.receivingWarehouseName like {0}receivingWarehouseName");
                            dbParameters.Add(db.CreateDbParameter("receivingWarehouseName", "%" + receivingWarehouseName + "%"));
                        }
                        if(partsSalesCategoryId.HasValue) {
                            sql.Append(" and a.partsSalesCategoryId={0}partsSalesCategoryId");
                            dbParameters.Add(db.CreateDbParameter("partsSalesCategoryId", partsSalesCategoryId.Value));
                        }
                        if(logisticCompanyId.HasValue) {
                            sql.Append(" and a.logisticCompanyId={0}logisticCompanyId");
                            dbParameters.Add(db.CreateDbParameter("logisticCompanyId", logisticCompanyId.Value));
                        }
                        if(!string.IsNullOrEmpty(logisticCompanyCode)) {
                            sql.Append(" and a.logisticCompanyCode like {0}logisticCompanyCode ");
                            dbParameters.Add(db.CreateDbParameter("logisticCompanyCode", "%" + logisticCompanyCode + "%"));
                        }
                        if(!string.IsNullOrEmpty(logisticCompanyName)) {
                            sql.Append(" and a.logisticCompanyName like {0}logisticCompanyName ");
                            dbParameters.Add(db.CreateDbParameter("logisticCompanyName", "%" + logisticCompanyName + "%"));
                        }
                        if(!string.IsNullOrEmpty(expressCompany)) {
                            sql.Append(" and a.expressCompany like {0}expressCompany ");
                            dbParameters.Add(db.CreateDbParameter("expressCompany", "%" + expressCompany + "%"));
                        }
                        if(!string.IsNullOrEmpty(expressCompanyCode)) {
                            sql.Append(" and a.expressCompanyCode like {0}expressCompanyCode ");
                            dbParameters.Add(db.CreateDbParameter("expressCompanyCode", "%" + expressCompanyCode + "%"));
                        }
                        if(!string.IsNullOrEmpty(eRPSOURCEORDERCODE)) {
                            sql.Append(" and d.ERPSOURCEORDERCODE like {0}ERPSOURCEORDERCODE ");
                            dbParameters.Add(db.CreateDbParameter("eRPSOURCEORDERCODE", "%" + eRPSOURCEORDERCODE + "%"));
                        }
                        if (!string.IsNullOrEmpty(originalRequirementBillCode))
                        {
                            sql.Append(" and a.originalRequirementBillCode like {0}originalRequirementBillCode ");
                            dbParameters.Add(db.CreateDbParameter("originalRequirementBillCode", "%" + originalRequirementBillCode + "%"));
                        }
                    }


                    sql.Append(" order by a.id ");
                    #endregion

                    var sqlStr = sql.ToString();
                    if(sqlStr.IndexOf(" where 1=1  and ") > 0)
                        sqlStr = sqlStr.Replace(" where 1=1  and ", " where ");
                    else
                        sqlStr = sqlStr.Replace(" where 1=1 ", "");
                    var command = db.CreateDbCommand(string.Format(sqlStr, db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    conn.Open();
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    ErrorStrings.Export_Title_AgencyPartsShippingOrder_Code, ErrorStrings.Export_Title_AgencyPartsShippingOrder_Type,ErrorStrings.Export_Title_PartsPurchaseOrder_OrderType, ErrorStrings.Export_Title_AccountPeriod_Status, ErrorStrings.Export_Title_AgencyPartsShippingOrder_ClosedStatus, ErrorStrings.Export_Title_Partssalescategory_Name, ErrorStrings.Export_Title_InternalAllocationBill_TotalAmount,ErrorStrings.Export_Title_PartsShippingOrder_AllWeight,ErrorStrings.Export_Title_PartsShippingOrder_AllVolue, ErrorStrings.Export_Title_AgencyPartsShippingOrder_ReceivingWarehouseCode, ErrorStrings.Export_Title_PartsInboundCheckBill_CompanyName, ErrorStrings.Export_Title_PartsPurchaseOrder_ReceivingCompanyName, ErrorStrings.Export_Title_SupplierShippingOrder_ReveiveWarehouseName, ErrorStrings.Export_Title_AgencyPartsShippingOrder_LogisticCompanyName, ErrorStrings.Export_Title_AgencyPartsShippingOrder_ExpressCompanyCode, ErrorStrings.Export_Title_AgencyPartsShippingOrder_ExpressCompanyName, ErrorStrings.Export_Title_PartsPurchaseOrder_ShippingMethod, ErrorStrings.Export_Title_AgencyPartsShippingOrder_RequestedArrivalDate, ErrorStrings.Export_Title_SupplierShippingOrder_ShippingDates, ErrorStrings.Export_Title_AgencyPartsShippingOrder_LogisticArrivalDate, ErrorStrings.Export_Title_AgencyPartsShippingOrder_LogisticShippingTime, ErrorStrings.Export_Title_InternalAllocationBill_ApproveTime, ErrorStrings.Export_Title_AgencyPartsShippingOrder_ArrivalMode, ErrorStrings.Export_Title_AgencyPartsShippingOrder_Logistic, ErrorStrings.Export_Title_SupplierShippingOrder_DdeliverybillNumber, ErrorStrings.Export_Title_AgencyPartsShippingOrder_CargoTerminalMsg, ErrorStrings.Export_Title_PartsBranch_Remark, ErrorStrings.Export_Title_AccountPeriod_CreatorName, ErrorStrings.Export_Title_AccountPeriod_CreateTime,ErrorStrings.Export_Title_PartsOutboundBill_PartsOutboundPlanCode,ErrorStrings.Export_Title_PartsShippingOrder_TaskCode, ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode, ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName, ErrorStrings.Export_Title_PartsPurchaseOrder_ShippingAmount, ErrorStrings.Export_Title_PartsPurchaseOrder_ConfirmedAmount, ErrorStrings.Export_Title_AgencyPartsShippingOrder_SalesPrice, ErrorStrings.Export_Title_Customertransferbill_Amount, ErrorStrings.Export_Title_AgencyPartsShippingOrder_Difference, ErrorStrings.Export_Title_PartsBranch_Remark
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }

        /// <summary>
        /// 导出配件发运单
        /// </summary>
        public bool ExportPartsShippingOrderNotWithDetail1(int[] ids, int? settlementCompanyId, int? receivingCompanyId, string code, int? type, int? status, int? warehouseId, string warehouseName, DateTime? requestedArrivalDateBegin, DateTime? requestedArrivalDateEnd, DateTime? shippingDateBegin, DateTime? shippingDateEnd, DateTime? createTimeBegin, DateTime? createTimeEnd, DateTime? confirmedReceptionTimeBegin, DateTime? confirmedReceptionTimeEnd, string receivingCompanyCode, string receivingCompanyName, string receivingWarehouseName, int? partsSalesCategoryId, int? logisticCompanyId, string logisticCompanyCode, string logisticCompanyName, string expressCompany, string expressCompanyCode, int? closedLoopStatus, string eRPSOURCEORDERCODE, string originalRequirementBillCode, out string fileName)
        {
            fileName = GetExportFilePath("配件发运单_" + ".xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);
                var userInfo = Utils.GetCurrentUserInfo();
                using(var conn = db.CreateDbConnection()) {
                    var sql = new StringBuilder();
                    // 由于导出时需要将出库计划备注显示出来，从发运单内联到出库计划时可能含有多个出库单，导致出现重复的数据，
                    //故加distinct过滤重复数据
                    sql.Append(@"select distinct a.Code, --配件发运单编号
                                    (select value from keyvalueitem where NAME = 'PartsShippingOrder_Type'and key=a.Type) As Type,--配件发运单类型
                                    a.PartsOutboundPlanCode, --出库计划编号
                                    a.PartsSalesOrderTypeName as typename,--订单类型
                                    (select value from keyvalueitem where NAME = 'PartsShippingOrder_Status'and key=a.Status) As status,--状态
                                    cast(decode(a.Status,2,'闭环',3,'闭环',99,'闭环',1,'未闭环',4,'未闭环',5,'未闭环',6,'未闭环') as varchar2(20)) as ClosedLoopStatus,--闭环状态
                                    b.Name, --品牌
                                    a.WarehouseName,--出库仓库
                                    a.ReceivingCompanyCode, --收货单位编号
                                    a.ReceivingCompanyName, --收货单位名称
                                    a.ReceivingWarehouseName,--收货仓库名称
                                    a.ReceivingAddress, --收货地址
                                    t.ct, --品种数量 = 发运清单记录条数
                                    t.ss, --数量 = sum（配件发运单清单.发运量）
                                    t.ssc, --金额 = sum（配件结算价*发运量）
                                    a.LogisticCompanyCode, --物流公司编号
                                    a.LogisticCompanyName, --物流公司名称
                                    a.ExpressCompanyCode,--快递公司编号
                                    a.ExpressCompany,--快递公司名称
                                    (select value from keyvalueitem where NAME = 'ArrivalMode' and key= a.ArrivalMode) as ArrivalMode,--到货模式                                  
                                    --a.ExpressCompanyCode,
                                    a.Logistic,--承运商
                                    a.TransportDriverPhone,--承运司机电话
                                    a.DeliveryBillNumber,--送货单号
                                    a.CargoTerminalMsg,--货站信息
                                    a.ArrivalTime, --到货站日期
                                    a.FlowFeedback,--超期或电商运单流水反馈
                                     to_char( a.ShippingDate,'yyyy-mm-dd hh24:mi:ss') as shippingDate, -- a.ShippingDate, --发运日期
                                     to_char( a.RequestedArrivalDate,'yyyy-mm-dd hh24:mi:ss') as requestedArrivalDate, --a.RequestedArrivalDate, --要求到货日期
                                    to_char(a.ShippingTime,'yyyy-mm-dd hh24:mi:ss') as shippingTime, --a.ShippingTime,--物流发货日期
                                    a.ConfirmedReceptionTime,--确认时间                               
                                    (select ContainerNumber from Partsshippingorderdetail pd where rownum=1 and pd.PartsShippingOrderId=a.id), --货箱号
                                    a.Remark, --备注
                                    a.CreatorName, --创建人
                                    a.CreateTime, --创建时间
                                    a.OrderApproveComment, --订单审批意见
                                    a.ShippingCompanyCode, --发货单位编号
                                    a.ShippingCompanyName, --发货单位名称
                                    a.Transportmileage, --公里数 = 配件发运单.运输里程
                                   (select value from keyvalueitem where NAME = 'PartsShipping_Method'and key=a.ShippingMethod) As ShippingMethod,--发运方式
                                   (select value from keyvalueitem where NAME = 'IsOrNot'and key=a.IsTransportLosses) As IsTransportLosses,--是否路损
                                   (select value from keyvalueitem where NAME = 'PartsShippingOrder_TransportLossesDisposeStatus'and key=a.TransportLossesDisposeStatus) As TransportLossesDisposeStatus--路损处理状态
                                    
                               
                                from PartsShippingOrder a
                                Left join (select c.partsshippingorderid,
                                                Count(1) as ct, --品种数量 = 发运清单记录条数
                                                Sum(c.Shippingamount) as ss, --数量 = sum（配件发运单清单.发运量）
                                                Sum(c.Shippingamount * c.Settlementprice) as ssc --金额 = sum（配件结算价*发运量）
                                            from Partsshippingorderdetail c
                                            group by c.partsshippingorderid) t on a.id =
                                                                                t.partsshippingorderid
                                Left Join PartsSalesCategory b On a.PartsSalesCategoryId = b.Id
                                 where  ((a.ReceivingCompanyId= " + userInfo.EnterpriseId + " and a.type!=4 ) or exists(select 1 from WarehouseOperator w where w.WarehouseId=a.WarehouseId and w.OperatorId= " + userInfo.Id + ") ) ");
                    var dbParameters = new List<DbParameter>();

                    #region 条件过滤
                    if(ids != null && ids.Length > 0) {
                        sql.Append(" and a.id in (");
                        for(var i = 0; i < ids.Length; i++) {
                            if(ids.Length == i + 1) {
                                sql.Append("{0}id" + i);
                                dbParameters.Add(db.CreateDbParameter("id" + i, ids[i]));
                            } else {
                                sql.Append("{0}id" + i + ",");
                                dbParameters.Add(db.CreateDbParameter("id" + i, ids[i]));
                            }
                        }
                        sql.Append(")");
                    } else {
                        if(closedLoopStatus.HasValue) {
                            if(closedLoopStatus == 1) {
                                //未闭环
                                sql.Append(" and a.Status in(1,4,5,6) ");
                            } else if(closedLoopStatus == 2) {
                                //闭环
                                sql.Append(" and a.Status in(2,3,99)  ");
                            }
                        }
                        if(settlementCompanyId.HasValue) {
                            sql.Append(" and a.settlementCompanyId={0}settlementCompanyId");
                            dbParameters.Add(db.CreateDbParameter("settlementCompanyId", settlementCompanyId.Value));
                        }
                        if(receivingCompanyId.HasValue) {
                            sql.Append(" and a.receivingCompanyId={0}receivingCompanyId");
                            dbParameters.Add(db.CreateDbParameter("receivingCompanyId", receivingCompanyId.Value));
                        }
                        if(!string.IsNullOrEmpty(code)) {
                            sql.Append(" and a.code like {0}code ");
                            dbParameters.Add(db.CreateDbParameter("code", "%" + code + "%"));
                        }
                        if(type.HasValue) {
                            sql.Append(" and a.type={0}type");
                            dbParameters.Add(db.CreateDbParameter("type", type.Value));
                        }
                        if(status.HasValue) {
                            sql.Append(" and a.status={0}status");
                            dbParameters.Add(db.CreateDbParameter("status", status.Value));
                        }
                        if(warehouseId.HasValue) {
                            sql.Append(" and a.warehouseId={0}warehouseId");
                            dbParameters.Add(db.CreateDbParameter("warehouseId", warehouseId.Value));
                        }
                        if(!string.IsNullOrEmpty(warehouseName)) {
                            sql.Append(" and a.warehouseName like {0}warehouseName ");
                            dbParameters.Add(db.CreateDbParameter("warehouseName", "%" + warehouseName + "%"));
                        }
                        if(requestedArrivalDateBegin.HasValue) {
                            sql.Append(" and a.requestedArrivalDate>=to_date({0}requestedArrivalDateBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = requestedArrivalDateBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("requestedArrivalDateBegin", tempTime.ToString("G")));
                        }
                        if(requestedArrivalDateEnd.HasValue) {
                            sql.Append(" and a.requestedArrivalDate<=to_date({0}requestedArrivalDateEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = requestedArrivalDateEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("requestedArrivalDateEnd", tempTime.ToString("G")));
                        }
                        if(shippingDateBegin.HasValue) {
                            sql.Append(" and a.shippingDate>=to_date({0}shippingDateBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = shippingDateBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("shippingDateBegin", tempTime.ToString("G")));
                        }
                        if(shippingDateEnd.HasValue) {
                            sql.Append(" and a.shippingDate<=to_date({0}shippingDateEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = shippingDateEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("shippingDateEnd", tempTime.ToString("G")));
                        }
                        if(createTimeBegin.HasValue) {
                            sql.Append(" and a.createTime>=to_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(" and a.createTime<=to_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                        if(confirmedReceptionTimeBegin.HasValue) {
                            sql.Append(" and a.confirmedReceptionTime>=to_date({0}confirmedReceptionTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = confirmedReceptionTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("confirmedReceptionTimeBegin", tempTime.ToString("G")));
                        }
                        if(confirmedReceptionTimeEnd.HasValue) {
                            sql.Append(" and a.confirmedReceptionTime<=to_date({0}confirmedReceptionTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = confirmedReceptionTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("confirmedReceptionTimeEnd", tempTime.ToString("G")));
                        }
                        if(!string.IsNullOrEmpty(receivingCompanyCode)) {
                            sql.Append(" and a.receivingCompanyCode like {0}receivingCompanyCode ");
                            dbParameters.Add(db.CreateDbParameter("receivingCompanyCode", "%" + receivingCompanyCode + "%"));
                        }
                        if(!string.IsNullOrEmpty(receivingCompanyName)) {
                            sql.Append(" and a.receivingCompanyName like {0}receivingCompanyName ");
                            dbParameters.Add(db.CreateDbParameter("receivingCompanyName", "%" + receivingCompanyName + "%"));
                        }
                        if(!string.IsNullOrEmpty(receivingWarehouseName)) {
                            sql.Append(" and a.receivingWarehouseName like {0}receivingWarehouseName");
                            dbParameters.Add(db.CreateDbParameter("receivingWarehouseName", "%" + receivingWarehouseName + "%"));
                        }
                        if(partsSalesCategoryId.HasValue) {
                            sql.Append(" and a.partsSalesCategoryId={0}partsSalesCategoryId");
                            dbParameters.Add(db.CreateDbParameter("partsSalesCategoryId", partsSalesCategoryId.Value));
                        }
                        if(logisticCompanyId.HasValue) {
                            sql.Append(" and a.logisticCompanyId={0}logisticCompanyId");
                            dbParameters.Add(db.CreateDbParameter("logisticCompanyId", logisticCompanyId.Value));
                        }
                        if(!string.IsNullOrEmpty(logisticCompanyCode)) {
                            sql.Append(" and a.logisticCompanyCode like {0}logisticCompanyCode ");
                            dbParameters.Add(db.CreateDbParameter("logisticCompanyCode", "%" + logisticCompanyCode + "%"));
                        }
                        if(!string.IsNullOrEmpty(logisticCompanyName)) {
                            sql.Append(" and a.logisticCompanyName like {0}logisticCompanyName ");
                            dbParameters.Add(db.CreateDbParameter("logisticCompanyName", "%" + logisticCompanyName + "%"));
                        }
                        if(!string.IsNullOrEmpty(expressCompany)) {
                            sql.Append(" and a.expressCompany like {0}expressCompany ");
                            dbParameters.Add(db.CreateDbParameter("expressCompany", "%" + expressCompany + "%"));
                        }
                        if(!string.IsNullOrEmpty(expressCompanyCode)) {
                            sql.Append(" and a.expressCompanyCode like {0}expressCompanyCode ");
                            dbParameters.Add(db.CreateDbParameter("expressCompanyCode", "%" + expressCompanyCode + "%"));
                        }
                        if(!string.IsNullOrEmpty(eRPSOURCEORDERCODE)) {
                            sql.Append(" and pso.eRPSOURCEORDERCODE like {0}eRPSOURCEORDERCODE ");
                            dbParameters.Add(db.CreateDbParameter("eRPSOURCEORDERCODE", "%" + eRPSOURCEORDERCODE + "%"));
                        }
                        if (!string.IsNullOrEmpty(originalRequirementBillCode))
                        {
                            sql.Append(" and a.originalRequirementBillCode like {0}originalRequirementBillCode ");
                            dbParameters.Add(db.CreateDbParameter("originalRequirementBillCode", "%" + originalRequirementBillCode + "%"));
                        }
                    }
                    #endregion

                    var sqlStr = sql.ToString();
                    if(sqlStr.IndexOf(" where 1=1  and ") > 0)
                        sqlStr = sqlStr.Replace(" where 1=1  and ", " where ");
                    else
                        sqlStr = sqlStr.Replace(" where 1=1 ", "");
                    var command = db.CreateDbCommand(string.Format(sqlStr, db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    conn.Open();
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    ErrorStrings.Export_Title_AgencyPartsShippingOrder_Code, ErrorStrings.Export_Title_AgencyPartsShippingOrder_Type,ErrorStrings.Export_Title_Partsoutboundplan_Code,ErrorStrings.Export_Title_PartsPurchaseOrder_OrderType, ErrorStrings.Export_Title_AccountPeriod_Status, ErrorStrings.Export_Title_AgencyPartsShippingOrder_ClosedStatus, ErrorStrings.Export_Title_Partssalescategory_Name, ErrorStrings.Export_Title_AgencyPartsShippingOrder_ReceivingWarehouseCode, ErrorStrings.Export_Title_Partsoutboundplan_ReceivingCompanyCode, ErrorStrings.Export_Title_PartsPurchaseOrder_ReceivingCompanyName, ErrorStrings.Export_Title_SupplierShippingOrder_ReveiveWarehouseName, ErrorStrings.Export_Title_PartsPurchaseOrder_ReceiveAddress, ErrorStrings.Export_Title_AgencyPartsShippingOrder_VarietyQuantity, ErrorStrings.Export_Title_WarehouseArea_Quantity, ErrorStrings.Export_Title_Customertransferbill_Amount, ErrorStrings.Export_Title_AgencyPartsShippingOrder_LogisticCompanyCode, ErrorStrings.Export_Title_AgencyPartsShippingOrder_LogisticCompanyName, ErrorStrings.Export_Title_AgencyPartsShippingOrder_ExpressCompanyCode, ErrorStrings.Export_Title_AgencyPartsShippingOrder_ExpressCompanyName, ErrorStrings.Export_Title_AgencyPartsShippingOrder_ArrivalMode, ErrorStrings.Export_Title_AgencyPartsShippingOrder_Logistic, ErrorStrings.Export_Title_PartsShippingOrder_DriverPhone, ErrorStrings.Export_Title_SupplierShippingOrder_DdeliverybillNumber, ErrorStrings.Export_Title_AgencyPartsShippingOrder_CargoTerminalMsg, ErrorStrings.Export_Title_AgencyPartsShippingOrder_ArrivalTime, ErrorStrings.Export_Title_AgencyPartsShippingOrder_FlowFeedback, ErrorStrings.Export_Title_SupplierShippingOrder_ShippingDates, ErrorStrings.Export_Title_AgencyPartsShippingOrder_RequestedArrivalDate, ErrorStrings.Export_Title_AgencyPartsShippingOrder_LogisticShippingTime, ErrorStrings.Export_Title_InternalAllocationBill_ApproveTime, ErrorStrings.Export_Title_PartsOutboundBill_BoxNo,ErrorStrings.Export_Title_PartsBranch_Remark, ErrorStrings.Export_Title_AccountPeriod_CreatorName, ErrorStrings.Export_Title_AccountPeriod_CreateTime, ErrorStrings.Export_Title_PartsOutboundBill_OrderApproveComment,  ErrorStrings.Export_Title_PartsShippingOrder_ShippingCode, ErrorStrings.Export_Title_PartsInboundCheckBill_CompanyName, ErrorStrings.Export_Title_AgencyPartsShippingOrder_TransportMileage, ErrorStrings.Export_Title_PartsPurchaseOrder_ShippingMethod, ErrorStrings.Export_Title_AgencyPartsShippingOrder_IsTransportLosses, ErrorStrings.Export_Title_AgencyPartsShippingOrder_TransportLossesDisposeStatus
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception ex) {
                throw ex;
            }
        }

    }
}