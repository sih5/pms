﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        /// <summary>
        /// 导入单车服务卡
        /// </summary>
        public bool ImportDealerPartsTransferOrderDetail(string fileName, int noWarrantyBrandId, int warrantyBrandId, int branchId, out int excelImportNum, out List<DealerPartsTransOrderDetailExtend> rightData, out List<DealerPartsTransOrderDetailExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var userInfo = Utils.GetCurrentUserInfo();
            var errorList = new List<DealerPartsTransOrderDetailExtend>();
            var rightList = new List<DealerPartsTransOrderDetailExtend>();
            var allList = new List<DealerPartsTransOrderDetailExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);
                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("DealerPartsTransOrderDetail", out notNullableFields, out fieldLenght);

                List<object> excelColumns;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    //指定文件中的列对应的名称
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode, "SparePartCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_WarehouseArea_Quantity, "Amount");

                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    //Excel单行导入
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        //接收Excel导入数据
                        var tempImportObj = new DealerPartsTransOrderDetailExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);

                        tempImportObj.SparePartCode = newRow["SparePartCode"];
                        tempImportObj.AmountStr = newRow["Amount"];

                        var errorMsgs = new List<string>();
                        //配件图号检查
                        if(string.IsNullOrEmpty(tempImportObj.SparePartCode)) {
                            errorMsgs.Add(ErrorStrings.ImpPartsPurchaseOrderDetail_Validation1);
                        }
                        if(string.IsNullOrEmpty(tempImportObj.AmountStr)) {
                            errorMsgs.Add(ErrorStrings.Export_Validation_InternalAcquisitionDetail_QuantityIsNull);
                        } else {
                            int amount = 0;
                            if(int.TryParse(tempImportObj.AmountStr, out amount)) {
                                if(amount < 1) {
                                    errorMsgs.Add(ErrorStrings.Export_Validation_InternalAcquisitionDetail_QuantityOverZero);
                                } else {
                                    tempImportObj.Amount = amount;
                                }
                            } else {
                                errorMsgs.Add("数量请输入数字");
                            }
                        }
                        if(errorMsgs.Count > 0)
                            tempImportObj.ErrorMsg = string.Join("; ", errorMsgs);
                        allList.Add(tempImportObj);
                        return false;
                    });
                    rightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    var groups = rightList.GroupBy(r => r.SparePartCode).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        groupItem.ErrorMsg = ErrorStrings.Export_Validation_PlannedPriceAppDetail_SparepartReport;
                    }
                    rightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    var sparePartCodes = rightList.Select(v => v.SparePartCode).Distinct().ToArray();
                    var spareParts = new List<SparePart>();
                    Func<string[], bool> dealSparePart = values => {
                        spareParts.Add(new SparePart {
                            Id = int.Parse(values[0]),
                            Code = values[1],
                            Name = values[2]
                        });
                        return false;
                    };
                    db.QueryDataWithInOperator(string.Format("select Id,Code,Name from SparePart where status={0}", (int)DcsMasterDataStatus.有效), "Code", false, sparePartCodes, dealSparePart);
                    foreach(var item in rightList) {
                        var sparePart = spareParts.FirstOrDefault(r => r.Code == item.SparePartCode);
                        if(sparePart == null) {
                            item.ErrorMsg = "不存在有效配件";
                        } else {
                            item.SparePartId = sparePart.Id;
                            item.SparePartName = sparePart.Name;
                            item.SparePartCode = sparePart.Code;
                        }
                    }
                    rightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    sparePartCodes = rightList.Select(v => v.SparePartCode).Distinct().ToArray();
                    var dealerPartsStocks = new List<DealerPartsStockExtend>();
                    Func<string[], bool> dealerPartsStockFunc = values => {
                        dealerPartsStocks.Add(new DealerPartsStockExtend {
                            SparePartId = int.Parse(values[0]),
                            Quantity = int.Parse(values[1])
                        });
                        return false;
                    };
                    db.QueryDataWithInOperator(string.Format("select SparePartId,Quantity from DealerPartsStock  where BranchId={0} and DealerCode='{1}' and SalesCategoryId={2} ", branchId, userInfo.EnterpriseCode, noWarrantyBrandId), "SparePartCode", false, sparePartCodes, dealerPartsStockFunc);
                    foreach(var item in rightList) {
                        var quantity = dealerPartsStocks.Where(r => r.SparePartId == item.SparePartId).Sum(r => r.Quantity);
                        if(quantity < item.Amount) {
                            item.ErrorMsg = "库存不足";
                        } else {
                            item.Quantity = quantity;
                        }
                    }
                    rightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    sparePartCodes = rightList.Select(v => v.SparePartCode).Distinct().ToArray();
                    var partsSalesPrices = new List<PartsSalesPriceExtend>();
                    Func<string[], bool> partsSalesPriceFunc = values => {
                        partsSalesPrices.Add(new PartsSalesPriceExtend {
                            SparePartId = int.Parse(values[0]),
                            SalesPrice = decimal.Parse(values[1]),
                            PartsSalesCategoryId = int.Parse(values[2]),
                        });
                        return false;
                    };
                    db.QueryDataWithInOperator(string.Format("select SparePartId,SalesPrice,PartsSalesCategoryId from PartsSalesPrice where status={0} and PartsSalesCategoryId in ({1},{2}) ", (int)DcsBaseDataStatus.有效, warrantyBrandId, noWarrantyBrandId), "SparePartCode", false, sparePartCodes, partsSalesPriceFunc);
                    foreach(var item in rightList) {
                        var noWarrantyBrand = partsSalesPrices.FirstOrDefault(r => r.SparePartId == item.SparePartId && r.PartsSalesCategoryId == noWarrantyBrandId);
                        if(noWarrantyBrand != null) {
                            item.NoWarrantyPrice = noWarrantyBrand.SalesPrice;
                        }
                        var warrantyBrand = partsSalesPrices.FirstOrDefault(r => r.SparePartId == item.SparePartId && r.PartsSalesCategoryId == warrantyBrandId);
                        if(warrantyBrand != null) {
                            item.WarrantyPrice = warrantyBrand.SalesPrice;
                        }
                    }
                    //获取所有不合格数据
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    //获取所有合格数据
                    rightList = allList.Except(errorList).ToList();
                }
                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempObj = list[index - 1];
                            var values = new object[] {
                                //设置错误信息导出的列的值
                                tempObj.SparePartCode, tempObj.AmountStr, tempObj.ErrorMsg
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }

                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }

        public bool ExportDealerPartsTransferOrder(int[] ids, int? dealerId, int? noWarrantyBrandId, int? warrantyBrandId, string code, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName) {
            fileName = GetExportFilePath("导出服务站配件调拨单.xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.Append(@"select code,
                               DealerCode,
                               DealerName,
                               BranchIdCode,
                               BranchIdName,
                               NoWarrantyBrandName,
                               WarrantyBrandName,
                               SourceCode,
                               SumNoWarrantyPrice,
                               SumWarrantyPrice,
                               DiffPrice,
                               (select value from keyvalueitem where name='DealerPartsTransferOrderStatus' and key=a.Status),
                               Remark,
                               CreatorName,
                               CreateTime,
                               CheckerName,
                               CheckTime,
                               AbandonerName,
                               AbandonTime,
                               ModifierName,
                               ModifyTime
                              from  
                            DealerPartsTransferOrder a where 1=1 ");
                    var dbParameters = new List<DbParameter>();
                    if(ids != null && ids.Length > 0) {
                        sql.Append(" and a.id in (");
                        for(var i = 0;i < ids.Length;i++) {
                            if(ids.Length == i + 1) {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            } else {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                        }
                        sql.Append(")");
                    } else {
                        if(dealerId.HasValue) {
                            sql.Append(@" and a.dealerId = {0}dealerId ");
                            dbParameters.Add(db.CreateDbParameter("dealerId", dealerId.Value));
                        }
                        if(noWarrantyBrandId.HasValue) {
                            sql.Append(@" and a.noWarrantyBrandId = {0}noWarrantyBrandId ");
                            dbParameters.Add(db.CreateDbParameter("noWarrantyBrandId", noWarrantyBrandId.Value));
                        }
                        if(warrantyBrandId.HasValue) {
                            sql.Append(@" and a.warrantyBrandId = {0}warrantyBrandId ");
                            dbParameters.Add(db.CreateDbParameter("warrantyBrandId", warrantyBrandId.Value));
                        }
                        if(!string.IsNullOrEmpty(code)) {
                            sql.Append("and LOWER(a.Code) like {0}code ");
                            dbParameters.Add(db.CreateDbParameter("code", "%" + code.ToLower() + "%"));
                        }
                        if(status.HasValue) {
                            sql.Append("and a.status = {0}status ");
                            dbParameters.Add(db.CreateDbParameter("status", status.Value));
                        }
                        if(createTimeBegin.HasValue) {
                            sql.Append(@" and a.createTime >=To_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(@" and a.createTime <=To_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                    }
                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {

                                    ErrorStrings.Export_Title_PartsTransferOrder_Code, ErrorStrings.Export_Title_Company_Code, ErrorStrings.Export_Title_Company_Name, ErrorStrings.Export_Title_PartSstock_BranchCode, ErrorStrings.Export_Title_PartSstock_BranchName, "保外品牌", "保内品牌", ErrorStrings.Export_Title_PartsOuterPurchaseChange_SourceCode, "保外销售价格合计", "保内销售价格合计", "差异金额（保外-保内）", ErrorStrings.Export_Title_AccountPeriod_Status, ErrorStrings.Export_Title_PartsBranch_Remark, ErrorStrings.Export_Title_AccountPeriod_CreatorName, ErrorStrings.Export_Title_AccountPeriod_CreateTime, ErrorStrings.Export_Title_PackingPropertyApp_ApproverName, ErrorStrings.Export_Title_PackingPropertyApp_ApproveTime, ErrorStrings.Export_Title_PartsBranch_AbandonerName, ErrorStrings.Export_Title_PartsBranch_AbandonerTime, ErrorStrings.Export_Title_AccountPeriod_ModifierName, ErrorStrings.Export_Title_AccountPeriod_ModifyTime,
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }

        public bool ExportDealerPartsTransferOrderAndDetail(int[] ids, int? dealerId, int? noWarrantyBrandId, int? warrantyBrandId, string code, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName) {
            fileName = GetExportFilePath("导出服务站配件调拨单.xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.Append(@"select code,
                               DealerCode,
                               DealerName,
                               BranchIdCode,
                               BranchIdName,
                               NoWarrantyBrandName,
                               WarrantyBrandName,
                               SourceCode,
                               SumNoWarrantyPrice,
                               SumWarrantyPrice,
                               a.DiffPrice,
                               (select value from keyvalueitem where name='DealerPartsTransferOrderStatus' and key=a.Status),
                               a.Remark,
                               CreatorName,
                               CreateTime,
                               CheckerName,
                               CheckTime,
                               AbandonerName,
                               AbandonTime,
                               ModifierName,
                               ModifyTime,SparePartCode,SparePartName,b.NoWarrantyPrice,b.WarrantyPrice,Amount,b.NoWarrantyPrice*b.Amount,b.WarrantyPrice*b.Amount,b.Remark
                              from  DealerPartsTransferOrder a  left join DealerPartsTransOrderDetail b on a.id =b.DealerPartsTransferOrderId where 1=1 ");
                    var dbParameters = new List<DbParameter>();
                    if(ids != null && ids.Length > 0) {
                        sql.Append(" and a.id in (");
                        for(var i = 0;i < ids.Length;i++) {
                            if(ids.Length == i + 1) {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            } else {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                        }
                        sql.Append(")");
                    } else {
                        if(dealerId.HasValue) {
                            sql.Append(@" and a.dealerId = {0}dealerId ");
                            dbParameters.Add(db.CreateDbParameter("dealerId", dealerId.Value));
                        }
                        if(noWarrantyBrandId.HasValue) {
                            sql.Append(@" and a.noWarrantyBrandId = {0}noWarrantyBrandId ");
                            dbParameters.Add(db.CreateDbParameter("noWarrantyBrandId", noWarrantyBrandId.Value));
                        }
                        if(warrantyBrandId.HasValue) {
                            sql.Append(@" and a.warrantyBrandId = {0}warrantyBrandId ");
                            dbParameters.Add(db.CreateDbParameter("warrantyBrandId", warrantyBrandId.Value));
                        }
                        if(!string.IsNullOrEmpty(code)) {
                            sql.Append("and LOWER(a.Code) like {0}code ");
                            dbParameters.Add(db.CreateDbParameter("code", "%" + code.ToLower() + "%"));
                        }
                        if(status.HasValue) {
                            sql.Append("and a.status = {0}status ");
                            dbParameters.Add(db.CreateDbParameter("status", status.Value));
                        }
                        if(createTimeBegin.HasValue) {
                            sql.Append(@" and a.createTime >=To_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(@" and a.createTime <=To_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                    }
                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    ErrorStrings.Export_Title_PartsTransferOrder_Code, ErrorStrings.Export_Title_Company_Code, ErrorStrings.Export_Title_Company_Name, ErrorStrings.Export_Title_PartSstock_BranchCode, ErrorStrings.Export_Title_PartSstock_BranchName, "保外品牌", "保内品牌", ErrorStrings.Export_Title_PartsOuterPurchaseChange_SourceCode, "保外销售价格合计", "保内销售价格合计", "差异金额（保外-保内）", ErrorStrings.Export_Title_AccountPeriod_Status, ErrorStrings.Export_Title_PartsBranch_Remark, ErrorStrings.Export_Title_AccountPeriod_CreatorName, ErrorStrings.Export_Title_AccountPeriod_CreateTime, ErrorStrings.Export_Title_PackingPropertyApp_ApproverName, ErrorStrings.Export_Title_PackingPropertyApp_ApproveTime, ErrorStrings.Export_Title_PartsBranch_AbandonerName, ErrorStrings.Export_Title_PartsBranch_AbandonerTime, ErrorStrings.Export_Title_AccountPeriod_ModifierName, ErrorStrings.Export_Title_AccountPeriod_ModifyTime,ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode,ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName,"保外销售价格","保内销售价格",ErrorStrings.Export_Title_OverstockPartsAdjustDetail_Quantity,"保外销售价格合计","保内销售价格合计",ErrorStrings.Export_Title_PartsBranch_Remark
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }

    }
}