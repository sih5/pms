﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using Microsoft.VisualBasic;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        /// <summary>
        /// 导入计划价申请单,校验配件有效性
        /// </summary>
        public bool ImportPlannedPriceAppDetail(PlannedPriceAppExtend plannedPriceApp, string fileName, out int excelImportNum, out List<ImpPlannedPriceAppDetail> rightData, out List<ImpPlannedPriceAppDetail> errorData, out string errorDataFileName, out string errorMessage) {
            //初始化返回数据
            rightData = null;
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            List<ImpPlannedPriceAppDetail> errorPlannedPriceAppDetails = null;

            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("PlannedPriceAppDetail", out notNullableFields, out fieldLenght);

                errorPlannedPriceAppDetails = new List<ImpPlannedPriceAppDetail>();
                var plannedPriceAppDetails = new List<ImpPlannedPriceAppDetail>();
                List<object> excelColumns;

                //Excel数据导入
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    //指定Excel列名与字段名关系
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode, "SparePartCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName, "SparePartName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PlannedPriceAppDetail_RequestedPrice, "RequestedPrice");

                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();

                    //Excel单行导入
                    List<ImpPlannedPriceAppDetail> details = errorPlannedPriceAppDetails;
                    List<ImpPlannedPriceAppDetail> appDetails = plannedPriceAppDetails;
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        //接收Excel导入数据
                        var plannedPriceAppDetail = new ImpPlannedPriceAppDetail {
                            SparePartCode = strconvsparecode(Strings.StrConv(row["SparePartCode"], VbStrConv.Narrow)).ToUpper(),
                            SparePartName = row["SparePartName"]
                        };
                        var errorMsgs = new List<string>();
                        //配件编号检查
                        var fieldIndex = notNullableFields.IndexOf("SparePartCode".ToUpper());
                        if(string.IsNullOrEmpty(plannedPriceAppDetail.SparePartCode)) {
                            if(fieldIndex > -1)
                                errorMsgs.Add(ErrorStrings.ImpPlannedPriceAppDetail_Validation1);
                        } else {
                            if(Encoding.Default.GetByteCount(plannedPriceAppDetail.SparePartCode) > fieldLenght["SparePartCode".ToUpper()])
                                errorMsgs.Add(ErrorStrings.ImpPlannedPriceAppDetail_Validation2);
                        }
                        //配件名称检查
                        fieldIndex = notNullableFields.IndexOf("SparePartName".ToUpper());
                        if(string.IsNullOrEmpty(plannedPriceAppDetail.SparePartName)) {
                            if(fieldIndex > -1)
                                errorMsgs.Add(ErrorStrings.ImpPlannedPriceAppDetail_Validation3);
                        } else {
                            if(Encoding.Default.GetByteCount(plannedPriceAppDetail.SparePartName) > fieldLenght["SparePartName".ToUpper()])
                                errorMsgs.Add(ErrorStrings.ImpPlannedPriceAppDetail_Validation4);
                        }
                        //申请价格检查
                        fieldIndex = notNullableFields.IndexOf("RequestedPrice".ToUpper());
                        if(row["RequestedPrice"] == null) {
                            if(fieldIndex > -1)
                                errorMsgs.Add(ErrorStrings.ImpPlannedPriceAppDetail_Validation5);
                        } else {
                            decimal requestedPrice;
                            if(decimal.TryParse(row["RequestedPrice"], out requestedPrice)) {
                                if(requestedPrice > 0)
                                    plannedPriceAppDetail.RequestedPrice = decimal.Round(requestedPrice, 2);
                                else
                                    errorMsgs.Add(ErrorStrings.Export_Validation_PlannedPriceAppDetail_RequestedPriceOverZero);
                            } else {
                                errorMsgs.Add(ErrorStrings.ImpPlannedPriceAppDetail_Validation6);
                            }
                        }

                        if(errorMsgs.Count > 0) {
                            plannedPriceAppDetail.ErrorMsg = string.Join("; ", errorMsgs);
                            details.Add(plannedPriceAppDetail);
                        } else
                            appDetails.Add(plannedPriceAppDetail);
                        return false;
                    });
                    plannedPriceAppDetails = plannedPriceAppDetails.Where(r => r.ErrorMsg == null).ToList();
                    var groups = plannedPriceAppDetails.GroupBy(r => r.SparePartCode).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        groupItem.ErrorMsg = ErrorStrings.Export_Validation_PlannedPriceAppDetail_SparepartReport;
                        errorPlannedPriceAppDetails.Add(groupItem);
                    }

                    //赋值配件id，配件名称
                    plannedPriceAppDetails = plannedPriceAppDetails.Where(r => r.ErrorMsg == null).ToList();
                    var sparePartCodes = plannedPriceAppDetails.Select(v => v.SparePartCode.ToUpper()).Distinct().ToArray();
                    var spareParts = new List<SparePart>();
                    Func<string[], bool> dealSparePart = values => {
                        spareParts.Add(new SparePart {
                            Id = int.Parse(values[0]),
                            Code = values[1],
                            Name = values[2]
                        });
                        return false;
                    };
                    db.QueryDataWithInOperator(string.Format("select Id,Trim(Code),Trim(Name) from SparePart where status={0}", (int)DcsMasterDataStatus.有效), "Code", true, sparePartCodes, dealSparePart);
                    for(var i = plannedPriceAppDetails.Count - 1;i >= 0;i--) {
                        try {
                            var sparePart = spareParts.SingleOrDefault(v => String.Compare(v.Code, plannedPriceAppDetails[i].SparePartCode, StringComparison.OrdinalIgnoreCase) == 0);
                            if(sparePart == null) {
                                plannedPriceAppDetails[i].ErrorMsg = string.Format(ErrorStrings.ImpPlannedPriceAppDetail_Validation7, plannedPriceAppDetails[i].SparePartCode);
                                errorPlannedPriceAppDetails.Add(plannedPriceAppDetails[i]);
                            } else {
                                plannedPriceAppDetails[i].SparePartId = sparePart.Id;
                                plannedPriceAppDetails[i].SparePartName = sparePart.Name;
                            }
                        } catch(Exception) {
                            plannedPriceAppDetails[i].ErrorMsg = string.Format(ErrorStrings.Export_Validation_PlannedPriceAppDetail_Validation1, plannedPriceAppDetails[i].SparePartCode);
                            errorPlannedPriceAppDetails.Add(plannedPriceAppDetails[i]);
                        }
                    }
                    List<ImpPlannedPriceAppDetail> ippad = new List<ImpPlannedPriceAppDetail>();
                    Func<string[], bool> dbPlannedPriceAppDetail = values => {
                        if(ippad.FirstOrDefault(r => r.SparePartCode == values[0]) == null) {
                            ippad.Add(new ImpPlannedPriceAppDetail {
                                SparePartCode = values[0],
                                SparePartName = values[1],
                                PlannedPriceAppCode = values[2]
                            });
                        }
                        return false;
                    };
                    db.QueryDataWithInOperator(string.Format("select Trim(a.SparePartCode),Trim(a.SparePartName),c.Code from PlannedPriceAppDetail a left join PlannedPriceApp c on c.id=a.plannedpriceappid where c.status = {0} and c.partssalescategoryid= {1}", (int)DcsPlannedPriceAppStatus.新增, plannedPriceApp.PartsSalesCategoryId), "SparePartCode", true, sparePartCodes, dbPlannedPriceAppDetail);
                    for(var i = plannedPriceAppDetails.Count - 1;i >= 0;i--) {
                        try {
                            var sparePart = ippad.SingleOrDefault(v => String.Compare(v.SparePartCode, plannedPriceAppDetails[i].SparePartCode, StringComparison.OrdinalIgnoreCase) == 0);
                            if(sparePart != null) {
                                plannedPriceAppDetails[i].ErrorMsg = string.Format(ErrorStrings.Export_Validation_PlannedPriceAppDetail_Validation2, plannedPriceAppDetails[i].SparePartCode, sparePart.PlannedPriceAppCode);
                                errorPlannedPriceAppDetails.Add(plannedPriceAppDetails[i]);
                            }
                        } catch(Exception) {
                            var sparePart = ippad.FirstOrDefault(v => String.Compare(v.SparePartCode, plannedPriceAppDetails[i].SparePartCode, StringComparison.OrdinalIgnoreCase) == 0);
                            plannedPriceAppDetails[i].ErrorMsg = string.Format(ErrorStrings.Export_Validation_PlannedPriceAppDetail_Validation3, plannedPriceAppDetails[i].SparePartCode, sparePart.PlannedPriceAppCode);
                            errorPlannedPriceAppDetails.Add(plannedPriceAppDetails[i]);
                        }
                    }
                    // 赋值变动前计划价
                    var sparepartids = plannedPriceAppDetails.Select(v => v.SparePartId.ToString()).ToArray();
                    List<ImpPlannedPriceAppDetail> ppad = new List<ImpPlannedPriceAppDetail>();
                    Func<string[], bool> dealplannedPrice = values => {
                        ppad.Add(new ImpPlannedPriceAppDetail {
                            SparePartId = Convert.ToInt32(values[0]),
                            PriceBeforeChange = Convert.ToDecimal(values[1])
                        });
                        return false;
                    };
                    db.QueryDataWithInOperator(string.Format(" select sparepartid,PlannedPrice from PartsPlannedPrice where PartsSalesCategoryId={0}", plannedPriceApp.PartsSalesCategoryId), "SparePartId", true, sparepartids, dealplannedPrice);
                    for(var i = plannedPriceAppDetails.Count - 1;i >= 0;i--) {
                        var pp = ppad.FirstOrDefault(r => r.SparePartId == plannedPriceAppDetails[i].SparePartId);
                        if(pp != null) {
                            plannedPriceAppDetails[i].PriceBeforeChange = pp.PriceBeforeChange;
                        } else {
                            plannedPriceAppDetails[i].PriceBeforeChange = 0m;
                        }
                    }
                    // 赋值库存
                    List<ImpPlannedPriceAppDetail> ppadstock = new List<ImpPlannedPriceAppDetail>();
                    Func<string[], bool> dealplannedPricestock = values => {
                        ppadstock.Add(new ImpPlannedPriceAppDetail {
                            SparePartId = Convert.ToInt32(values[0]),
                            StockQuantity = Convert.ToInt32(values[1])
                        });
                        return false;
                    };

                    db.QueryDataWithInOperator(string.Format(@"select PartId,Quantity from (SELECT Extent8.PartId,sum(Extent8.Quantity) as Quantity FROM PartsStock Extent8 INNER JOIN SalesUnitAffiWarehouse Extent9 ON Extent8.WarehouseId = Extent9.WarehouseId INNER JOIN SalesUnit Extent10 ON Extent9.SalesUnitId = Extent10.Id WHERE Extent8.StorageCompanyType = {0} AND Extent10.PartsSalesCategoryId = {1} group by Extent8.PartId ) where 1=1 ", (int)DcsCompanyType.分公司, plannedPriceApp.PartsSalesCategoryId), "PartId", true, sparepartids, dealplannedPricestock);
                    for(var i = plannedPriceAppDetails.Count - 1;i >= 0;i--) {
                        var ppstock = ppadstock.FirstOrDefault(r => r.SparePartId == plannedPriceAppDetails[i].SparePartId);
                        if(ppstock != null) {
                            plannedPriceAppDetails[i].StockQuantity = ppstock.StockQuantity;
                        }
                    }

                    // 赋值在途库存，计算差异金额 
                    List<ImpPlannedPriceAppDetail> ppadonroad = new List<ImpPlannedPriceAppDetail>();
                    Func<string[], bool> dealplannedPriceonroad = values => {
                        ppadonroad.Add(new ImpPlannedPriceAppDetail {
                            SparePartId = Convert.ToInt32(values[0]),
                            OnRoadQuantity = Convert.ToInt32(values[1])
                        });
                        return false;
                    };
                    db.QueryDataWithInOperator(string.Format(" SELECT GroupBy3.K1 AS partid, GroupBy3.A1 AS qty  FROM (SELECT Distinct1.C1 AS K1, Sum(Distinct1.C2) AS A1 FROM (SELECT DISTINCT UnionAll1.SparePartId AS C1, UnionAll1.C1 AS C2,UnionAll1.C2  AS C3 FROM (SELECT Join5.K31 AS SparePartId,Join5.A11 - (CASE WHEN Join5.A12 IS NULL THEN 0  ELSE  Join5.A12  END) AS C1, 1 AS C2  FROM (SELECT Extent1.Id AS Id1,  Extent3.PartsSalesCategoryId  FROM PartsTransferOrder Extent1 INNER JOIN SalesUnitAffiWarehouse Extent2 ON Extent1.DestWarehouseId = Extent2.WarehouseId  INNER JOIN SalesUnit Extent3 ON Extent2.SalesUnitId =  Extent3.Id   WHERE Extent1.StorageCompanyType = {0}) Filter1 INNER JOIN (SELECT GroupBy1.K1 AS K11,   GroupBy1.K2 AS K21,   GroupBy1.K3 AS K31,   GroupBy1.A1 AS A11,   GroupBy2.K1 AS K12, GroupBy2.K2 AS K22, GroupBy2.K3 AS K32,   GroupBy2.A1 AS A12  FROM (SELECT Extent4.OriginalRequirementBillId AS K1,   Extent4.OriginalRequirementBillCode AS K2, Extent5.SparePartId AS K3,   Sum(Extent5.OutboundAmount) AS A1  FROM PartsOutboundBill Extent4     INNER JOIN PartsOutboundBillDetail Extent5 ON Extent4.Id = Extent5.PartsOutboundBillId  WHERE ((Extent4.OriginalRequirementBillType = {1}) AND   (Extent4.OutboundType = {2})) GROUP BY Extent4.OriginalRequirementBillId,  Extent4.OriginalRequirementBillCode,  Extent5.SparePartId) GroupBy1  LEFT OUTER JOIN (SELECT Extent6.OriginalRequirementBillId AS K1,     Extent6.OriginalRequirementBillCode AS K2,     Extent7.SparePartId AS K3,     Sum(Extent7.InspectedQuantity) AS A1    FROM PartsInboundCheckBill Extent6   INNER JOIN PartsInboundCheckBillDetail Extent7 ON Extent6.Id = Extent7.PartsInboundCheckBillId   WHERE (Extent6.OriginalRequirementBillType = {1})     AND (Extent6.InboundType = {3})   GROUP BY Extent6.OriginalRequirementBillId,    Extent6.OriginalRequirementBillCode,    Extent7.SparePartId) GroupBy2 ON ((GroupBy1.K1 =     GroupBy2.K1) AND     (GroupBy1.K2 =     GroupBy2.K2))     AND (GroupBy1.K3 =     GroupBy2.K3)) Join5 ON Filter1.Id1 =        Join5.K11     WHERE Filter1.PartsSalesCategoryId = {4}    ) UnionAll1) Distinct1     GROUP BY Distinct1.C1    ) GroupBy3  where 1=1 ", (int)DcsCompanyType.分公司, (int)DcsOriginalRequirementBillType.配件调拨单, (int)DcsPartsOutboundType.配件调拨, (int)DcsPartsInboundType.配件调拨, plannedPriceApp.PartsSalesCategoryId), "K1", true, sparepartids, dealplannedPriceonroad);
                    for(var i = plannedPriceAppDetails.Count - 1;i >= 0;i--) {
                        var pponroad = ppadonroad.FirstOrDefault(r => r.SparePartId == plannedPriceAppDetails[i].SparePartId);
                        if(pponroad != null) {
                            plannedPriceAppDetails[i].OnRoadQuantity = pponroad.OnRoadQuantity;
                        }
                        plannedPriceAppDetails[i].Quantity = (plannedPriceAppDetails[i].StockQuantity ?? 0) + (plannedPriceAppDetails[i].OnRoadQuantity ?? 0);
                        plannedPriceAppDetails[i].AmountDifference = (plannedPriceAppDetails[i].RequestedPrice - (plannedPriceAppDetails[i].PriceBeforeChange ?? 0.0m)) * plannedPriceAppDetails[i].Quantity;
                    }
                }

                List<ImpPlannedPriceAppDetail> rightPlannedPriceAppDetails = plannedPriceAppDetails.Where(r => r.ErrorMsg == null).ToList();
                if(errorPlannedPriceAppDetails.Any()) {


                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        List<ImpPlannedPriceAppDetail> details = errorPlannedPriceAppDetails;
                        excelExport.ExportByRow(index => {
                            if(index == details.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var plannedPriceAppDetail = details[index - 1];
                            var values = new object[]{
                                        plannedPriceAppDetail.SparePartCode, plannedPriceAppDetail.SparePartName,
                                        plannedPriceAppDetail.RequestedPrice, plannedPriceAppDetail.ErrorMsg
                                    };
                            return values;
                        });
                    }
                    errorPlannedPriceAppDetails = null;
                }
                //if(rightPlannedPriceAppDetails.Count > 400)
                //    throw new Exception("清单品种不允许超出400");

                //导入正常结束，不存在需要写入数据库的合法数据
                if(!rightPlannedPriceAppDetails.Any())
                    return true;

                //计算主单金额差异
                plannedPriceApp.AmountBeforeChange = rightPlannedPriceAppDetails.Sum(r => r.Quantity * r.PriceBeforeChange);
                plannedPriceApp.AmountAfterChange = rightPlannedPriceAppDetails.Sum(r => r.Quantity * r.RequestedPrice);
                plannedPriceApp.AmountDifference = plannedPriceApp.AmountAfterChange - plannedPriceApp.AmountBeforeChange;

                //写数据
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    //开启事务，新增更新配件在一个事务内
                    var ts = conn.BeginTransaction();
                    try {
                        //新增配件
                        if(rightPlannedPriceAppDetails.Any()) {
                            var sqlInsert = db.GetInsertSql("PlannedPriceApp", "Id", new[] {
                                "Code", "PartsSalesCategoryId", "PartsSalesCategoryName","OwnerCompanyId","OwnerCompanyCode","OwnerCompanyName", "PlannedExecutionTime","Status","RecordStatus","CreatorId","CreatorName","CreateTime","Remark","AmountBeforeChange","AmountAfterChange","AmountDifference"
                            });

                            //获取新增数据的sql语句，Id为主键
                            var sqlInsertDetail = db.GetInsertSql("PlannedPriceAppDetail", "Id", new[] {
                                "PlannedPriceAppId","SparePartId", "SparePartCode", "SparePartName", "RequestedPrice","PriceBeforeChange","Quantity","AmountDifference"
                            });
                            var userInfo = Utils.GetCurrentUserInfo();
                            var command = db.CreateDbCommand(sqlInsert, conn, ts);
                            if(string.IsNullOrWhiteSpace(plannedPriceApp.Code))
                                plannedPriceApp.Code = CodeGenerator.Generate(db, "PlannedPriceApp", plannedPriceApp.OwnerCompanyCode);
                            command.Parameters.Add(db.CreateDbParameter("Code", plannedPriceApp.Code));
                            command.Parameters.Add(db.CreateDbParameter("PartsSalesCategoryId", plannedPriceApp.PartsSalesCategoryId));
                            command.Parameters.Add(db.CreateDbParameter("PartsSalesCategoryName", plannedPriceApp.PartsSalesCategoryName));
                            command.Parameters.Add(db.CreateDbParameter("OwnerCompanyId", plannedPriceApp.OwnerCompanyId));
                            command.Parameters.Add(db.CreateDbParameter("OwnerCompanyCode", plannedPriceApp.OwnerCompanyCode));
                            command.Parameters.Add(db.CreateDbParameter("OwnerCompanyName", plannedPriceApp.OwnerCompanyName));
                            command.Parameters.Add(db.CreateDbParameter("PlannedExecutionTime", plannedPriceApp.PlannedExecutionTime));
                            command.Parameters.Add(db.CreateDbParameter("Status", plannedPriceApp.Status));
                            command.Parameters.Add(db.CreateDbParameter("RecordStatus", plannedPriceApp.RecordStatus));
                            command.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                            command.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                            command.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                            command.Parameters.Add(db.CreateDbParameter("Remark", plannedPriceApp.Remark));
                            command.Parameters.Add(db.CreateDbParameter("AmountBeforeChange", plannedPriceApp.AmountBeforeChange));
                            command.Parameters.Add(db.CreateDbParameter("AmountAfterChange", plannedPriceApp.AmountAfterChange));
                            command.Parameters.Add(db.CreateDbParameter("AmountDifference", plannedPriceApp.AmountDifference));
                            var tempId = db.ExecuteInsert(command, "Id");
                            foreach(var plannedPriceAppDetail in rightPlannedPriceAppDetails) {
                                var commandDetail = db.CreateDbCommand(sqlInsertDetail, conn, ts);
                                commandDetail.Parameters.Add(db.CreateDbParameter("PlannedPriceAppId", tempId));
                                commandDetail.Parameters.Add(db.CreateDbParameter("SparePartId", plannedPriceAppDetail.SparePartId));
                                commandDetail.Parameters.Add(db.CreateDbParameter("SparePartCode", plannedPriceAppDetail.SparePartCode));
                                commandDetail.Parameters.Add(db.CreateDbParameter("SparePartName", plannedPriceAppDetail.SparePartName));
                                commandDetail.Parameters.Add(db.CreateDbParameter("RequestedPrice", plannedPriceAppDetail.RequestedPrice));
                                commandDetail.Parameters.Add(db.CreateDbParameter("PriceBeforeChange", plannedPriceAppDetail.PriceBeforeChange));
                                commandDetail.Parameters.Add(db.CreateDbParameter("Quantity", plannedPriceAppDetail.Quantity));
                                commandDetail.Parameters.Add(db.CreateDbParameter("AmountDifference", plannedPriceAppDetail.AmountDifference));
                                commandDetail.ExecuteNonQuery();
                            }
                        }
                        //无异常提交
                        ts.Commit();
                    } catch(Exception) {
                        //报错回滚
                        ts.Rollback();
                        throw;
                    } finally {
                        if(conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorPlannedPriceAppDetails = null;
                return false;
            } finally {
                errorData = errorPlannedPriceAppDetails;
                //rightData = rightPlannedPriceAppDetails;
            }
        }

    }
}
