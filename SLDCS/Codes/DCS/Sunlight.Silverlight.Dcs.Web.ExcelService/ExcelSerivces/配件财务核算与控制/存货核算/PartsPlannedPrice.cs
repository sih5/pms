﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        public bool ExportPartsPlannedPrice(string sparePartCode, string sparePartName, int? partsSalesCategoryId, DateTime? createTimeBegin, DateTime? createTimeEnd, int ownerCompanyId, out string fileName) {
            fileName = GetExportFilePath("配件计划价_" + ".xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.Append(@"select b.code,
                                        b.name,
                                        a.partssalescategoryname,
                                        a.plannedprice,
                                        a.createTime,
                                        a.creatorName,
                                        a.modifierName,
                                        a.modifyTime
                                        from PartsPlannedPrice a
                                        left join sparepart b
                                        on a.sparepartid=b.id where");
                    var dbParameters = new List<DbParameter>();


                    sql.Append(@" a.ownerCompanyId = {0}ownerCompanyId ");
                    dbParameters.Add(db.CreateDbParameter("ownerCompanyId", ownerCompanyId));
                    if(!String.IsNullOrEmpty(sparePartCode)) {
                        sql.Append(@" and lower(b.code) like {0}sparePartCode ");
                        dbParameters.Add(db.CreateDbParameter("sparePartCode", "%" + sparePartCode.ToLower() + "%"));
                    }
                    if(!String.IsNullOrEmpty(sparePartName)) {
                        sql.Append(@" and lower(b.name) like {0}sparePartName ");
                        dbParameters.Add(db.CreateDbParameter("sparePartName", "%" + sparePartName.ToLower() + "%"));
                    }

                    if(partsSalesCategoryId.HasValue) {
                        sql.Append(@" and a.partsSalesCategoryId = {0}partsSalesCategoryId ");
                        dbParameters.Add(db.CreateDbParameter("partsSalesCategoryId", partsSalesCategoryId.Value));
                    }
                    if(createTimeBegin.HasValue) {
                        sql.Append(@" and a.createTime >=to_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                        var tempValue = createTimeBegin.Value;
                        var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                        dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                    }
                    if(createTimeEnd.HasValue) {
                        sql.Append(@" and a.createTime <=to_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                        var tempValue = createTimeEnd.Value;
                        var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                        dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                    }
                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode, ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName, ErrorStrings.Export_Title_Partssalescategory_Name, ErrorStrings.Export_Title_PartsPlannedPrice_PlannedPrice, ErrorStrings.Export_Title_AccountPeriod_CreateTime, ErrorStrings.Export_Title_AccountPeriod_CreatorName, ErrorStrings.Export_Title_AccountPeriod_ModifyTime, ErrorStrings.Export_Title_AccountPeriod_ModifierName
                                };
                            }
                            if(reader.Read()) {
                                //var values = new object[reader.FieldCount];
                                //var num = reader.GetValues(values);
                                //if(num != reader.FieldCount) {
                                //    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                //}
                                var a = reader[0].ToString();
                                var b = reader[1].ToString();
                                var c = reader[2].ToString();
                                var d = reader[3].ToString();
                                var e = reader[4].ToString();
                                var f = reader[5].ToString();
                                var g = reader[6].ToString();
                                var h = reader[7].ToString();
                                return new object[] {
                                    a, b, c, d, e, f, g, h
                                };
                                //return values;
                            }

                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }
    }
}
