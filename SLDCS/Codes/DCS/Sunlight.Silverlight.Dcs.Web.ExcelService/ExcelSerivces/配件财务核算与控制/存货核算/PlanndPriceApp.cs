﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Globalization;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        public bool ExportPlanndPriceAppWithDetail(int? id, int ownerCompanyId, string code, int? partsSalesCategoryId, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName) {
            fileName = GetExportFilePath("配件计划价申请单.xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.AppendFormat(@"Select a.Code,
                                              a.Partssalescategoryname,
                                              a.Amountbeforechange,
                                              a.Amountdifference,
                                              a.Amountafterchange,
                                              a.Remark,
                                              (select value from keyvalueitem where NAME = 'PlannedPriceApp_Status'and key=a.Status) As Status,
                                              (select value from keyvalueitem where NAME = 'RecordStatus'and key=a.Recordstatus) As Recordstatus,
                                              a.Creatorname,
                                              a.Createtime,
                                              a.Modifiername,
                                              a.Modifytime,
                                              b.Sparepartcode,
                                              b.Sparepartname,
                                              b.Requestedprice,

                                              b.PriceBeforeChange,
                                              b.Quantity,
                                              b.AmountDifference,
                                              b.PriceFluctuationRatio

                                         From Plannedpriceapp a
                                         Left Join Plannedpriceappdetail b
                                           On a.Id = b.Plannedpriceappid
                                         Where a.OwnerCompanyId={0} ", ownerCompanyId);
                    var dbParameters = new List<DbParameter>();
                    if(id.HasValue) {
                        sql.Append(" and a.id = {0}id");
                        dbParameters.Add(db.CreateDbParameter("id", id.Value));
                    } else {
                        if(!String.IsNullOrEmpty(code)) {
                            sql.Append(@" and LOWER(a.code) like {0}code ");
                            dbParameters.Add(db.CreateDbParameter("code", "%" + code.ToLower() + "%"));
                        }
                        if(partsSalesCategoryId.HasValue) {
                            sql.Append(@" and a.partsSalesCategoryId={0}partsSalesCategoryId ");
                            dbParameters.Add(db.CreateDbParameter("partsSalesCategoryId", partsSalesCategoryId.Value));
                        }
                        if(status.HasValue) {
                            sql.Append(@" and a.status={0}status ");
                            dbParameters.Add(db.CreateDbParameter("status", status.Value));
                        }
                        if(createTimeBegin.HasValue) {
                            sql.Append(@" and a.createTime >=to_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(@" and a.createTime <=to_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                    }
                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                   "申请单编号",
                                   ErrorStrings.Export_Title_Partssalescategory_Name,
                                   "变动前金额",
                                   "差异金额",
                                   "变动后金额",
                                   ErrorStrings.Export_Title_PartsBranch_Remark,
                                   ErrorStrings.Export_Title_AccountPeriod_Status,
                                   "计账状态",
                                   ErrorStrings.Export_Title_AccountPeriod_CreatorName,
                                   ErrorStrings.Export_Title_AccountPeriod_CreateTime,
                                   ErrorStrings.Export_Title_AccountPeriod_ModifierName,
                                   ErrorStrings.Export_Title_AccountPeriod_ModifyTime,
                                   ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode,
                                   ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName,
                                   ErrorStrings.Export_Title_PlannedPriceAppDetail_RequestedPrice,

                                   "变动前价格",
                                   ErrorStrings.Export_Title_WarehouseArea_Quantity,
                                   "差异金额",
                                   "价格浮动比例",

                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }

                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }

        public bool ExportPlanndPriceApp(int[] ids, int ownerCompanyId, string code, int? partsSalesCategoryId, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName) {
            fileName = GetExportFilePath("配件计划价申请单.xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.AppendFormat(@"Select a.Code,
                                       a.Partssalescategoryname,
                                       a.Amountbeforechange,
                                       a.Amountdifference,
                                       a.Amountafterchange,
                                       a.PlannedExecutionTime,
                                       a.ActualExecutionTime,
                                       a.Remark,
                                       (select value from keyvalueitem where NAME = 'PlannedPriceApp_Status'and key=a.Status) As Status,
                                       a.Creatorname,
                                       a.Createtime,
                                       a.Modifiername,
                                       a.Modifytime
                                  From Plannedpriceapp a
                                 Where a.OwnerCompanyId={0} ", ownerCompanyId);
                    var dbParameters = new List<DbParameter>();
                    if(ids != null && ids.Length > 0) {
                        sql.Append(" and a.id in (");
                        for(var i = 0; i < ids.Length; i++) {
                            if(ids.Length == i + 1) {
                                sql.Append("{0}" + ids[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter(ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            } else {
                                sql.Append("{0}" + ids[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter(ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                        }
                        sql.Append(")");
                    } else {
                        if(!String.IsNullOrEmpty(code)) {
                            sql.Append(@" and LOWER(a.code) like {0}code ");
                            dbParameters.Add(db.CreateDbParameter("code", "%" + code.ToLower() + "%"));
                        }
                        if(partsSalesCategoryId.HasValue) {
                            sql.Append(@" and a.partsSalesCategoryId={0}partsSalesCategoryId ");
                            dbParameters.Add(db.CreateDbParameter("partsSalesCategoryId", partsSalesCategoryId.Value));
                        }
                        if(status.HasValue) {
                            sql.Append(@" and a.status={0}status ");
                            dbParameters.Add(db.CreateDbParameter("status", status.Value));
                        }
                        if(createTimeBegin.HasValue) {
                            sql.Append(@" and a.createTime >=to_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(@" and a.createTime <=to_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                    }
                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                   "申请单编号",ErrorStrings.Export_Title_Partssalescategory_Name,"变动前金额","差异金额","变动后金额","计划执行时间","实际执行时间",ErrorStrings.Export_Title_PartsBranch_Remark,ErrorStrings.Export_Title_AccountPeriod_Status,ErrorStrings.Export_Title_AccountPeriod_CreatorName,ErrorStrings.Export_Title_AccountPeriod_CreateTime,ErrorStrings.Export_Title_AccountPeriod_ModifierName,ErrorStrings.Export_Title_AccountPeriod_ModifyTime
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }

                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }
    }
}

