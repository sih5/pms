﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Globalization;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        /// <summary>
        /// 导出应付账款变更明细
        /// </summary>
        public bool ExportAccountPayableHistoryDetail(int[] ids, int? partsSalesCategoryId, int buyerCompanyId, string companyName, string companyCode, string sourceCode, int? businessType, DateTime? processDateBegin, DateTime? processDateEnd, out string fileName) {
            fileName = GetExportFilePath("应付账款变更明细_" + ".xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {
                    var sql = new StringBuilder();
                    sql.AppendFormat(@"Select c.Name,
                                       c.SupplierCode,
                                       c.Code,
                                       b.Partssalescategoryname,
                                       a.Changeamount,
                                       a.Processdate,
                                        (select value from keyvalueitem where NAME = 'AccountPayOut_BusinessType'and key=a.Businesstype) As Businesstype,
                                       a.Sourcecode,
                                       a.Summary,a.BeforeChangeAmount,a.AfterChangeAmount,a.Debit,a.Credit
                                  From Accountpayablehistorydetail a
                                  Left Join Supplieraccount b
                                    On a.Supplieraccountid = b.Id
                                  Inner Join Company c
                                    On b.Suppliercompanyid = c.Id
                                where b.buyerCompanyId={0} ", buyerCompanyId);
                    var dbParameters = new List<DbParameter>();
                    #region 条件过滤
                    if(ids != null && ids.Length > 0) {
                        sql.Append(" and a.id in (");
                        for(var i = 0; i < ids.Length; i++) {
                            if(ids.Length == i + 1) {
                                sql.Append("{0}" + ids[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter(ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            } else {
                                sql.Append("{0}" + ids[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter(ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                        }
                        sql.Append(")");
                    } else {
                        if(partsSalesCategoryId.HasValue) {
                            sql.Append(@" and b.partsSalesCategoryId = {0}partsSalesCategoryId ");
                            dbParameters.Add(db.CreateDbParameter("partsSalesCategoryId", partsSalesCategoryId.Value));
                        }
                        if(!String.IsNullOrEmpty(companyCode)) {
                            sql.Append(@" and Upper(c.Code) like Upper({0}companyCode) ");
                            dbParameters.Add(db.CreateDbParameter("companyCode", "%" + companyCode + "%"));
                        }
                        if(!String.IsNullOrEmpty(companyName)) {
                            sql.Append(@" and c.Name like {0}companyName ");
                            dbParameters.Add(db.CreateDbParameter("companyName", "%" + companyName + "%"));
                        }
                        if(!String.IsNullOrEmpty(sourceCode)) {
                            sql.Append(@" and Upper(a.sourceCode) like Upper({0}sourceCode) ");
                            dbParameters.Add(db.CreateDbParameter("sourceCode", "%" + sourceCode + "%"));
                        }
                        if(businessType.HasValue) {
                            sql.Append(@" and a.businessType = {0}businessType ");
                            dbParameters.Add(db.CreateDbParameter("businessType", businessType.Value));
                        }
                        if(processDateBegin.HasValue) {
                            sql.Append(@" and a.ProcessDate >=to_date({0}processDateBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = processDateBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("processDateBegin", tempTime.ToString("G")));
                        }
                        if(processDateEnd.HasValue) {
                            sql.Append(@" and a.ProcessDate <=to_date({0}processDateEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = processDateEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("processDateEnd", tempTime.ToString("G")));
                        }
                    }
                    sql.Append(" order by a.id");
                    #endregion

                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    conn.Open();
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                ErrorStrings.Export_Title_AccountPay_SupplierName,ErrorStrings.Export_Title_AccountPay_SupplierCode,ErrorStrings.Export_Title_AccountPay_SupplierCompanyCode,ErrorStrings.Export_Title_Partssalescategory_Name,ErrorStrings.Export_Title_AccountPay_ChangeAmount,ErrorStrings.Export_Title_AccountPay_Processdate,ErrorStrings.Export_Title_MarketingDepartment_BussinessType,
                                ErrorStrings.Export_Title_AccountPay_Sourcecode,ErrorStrings.Export_Title_AccountPay_Summary,ErrorStrings.Export_Title_AccountPay_BeforeChangeAmount,ErrorStrings.Export_Title_AccountPay_AfterChangeAmount,ErrorStrings.Export_Title_AccountPay_Debit,ErrorStrings.Export_Title_AccountPay_Credit
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }
    }
}
