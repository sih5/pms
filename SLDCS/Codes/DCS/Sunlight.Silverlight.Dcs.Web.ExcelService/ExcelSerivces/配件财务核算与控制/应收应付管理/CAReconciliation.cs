﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        public bool ImportCAReconciliation(string fileName, out int excelImportNum, out List<CAReconciliationExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<CAReconciliationExtend>();
            var allList = new List<CAReconciliationExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);
                var userInfo = Utils.GetCurrentUserInfo();
                var dbCompanyType = new List<int>();
                Func<string[], bool> getDbCompanyType = value => {
                    dbCompanyType.Add(Convert.ToInt32(value[0]));
                    return false;
                };
                var sqlCompany = string.Format("Select Type From Company Where Id={0}", userInfo.EnterpriseId);
                db.QueryDataWithInOperator(sqlCompany, "1", false, new[] {
                    "1"
                }, getDbCompanyType);
                var companyType = dbCompanyType.FirstOrDefault();

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("CAReconciliation", out notNullableFields, out fieldLenght);

                List<object> excelColumns;
                List<CAReconciliationExtend> rightList;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    #region 指定文件中的列对应的名称 例如 excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode, "PartCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Credenceapplication_AccountGroupNameNew, "AccountGroup");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Partssalescategory_Name, "PartsSalesCategory");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Company_Code, "CorporationCode");
                    excelOperator.AddColumnDataSource("对账时间", "ReconciliationTime");
                    excelOperator.AddColumnDataSource("对账方向", "ReconciliationDirection");
                    excelOperator.AddColumnDataSource("时间", "Time");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Customertransferbill_Summary, "Abstract");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Customertransferbill_Amount, "Money");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Remark, "Remark");
                    #endregion

                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;

                    #region 获取对应枚举
                    var keyValuePairs = new[] {
                        new KeyValuePair<string, string>("ReconciliationDirection", "ReconciliationDirection")
                    };
                    tempExcelOperator.ImportHelper.LoadEnumFromDb(null, null, keyValuePairs);

                    #endregion

                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var tempImportObj = new CAReconciliationExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);

                        #region 根据导入的字段给对应的实体属性赋值
                        tempImportObj.AccountGroupName = newRow["AccountGroup"];
                        tempImportObj.PartsSalesCategoryName = newRow["PartsSalesCategory"];
                        tempImportObj.CorporationCode = newRow["CorporationCode"];
                        tempImportObj.ReconciliationTimeStr = newRow["ReconciliationTime"];
                        tempImportObj.ReconciliationDirectionStr = newRow["ReconciliationDirection"];
                        tempImportObj.TimeStr = newRow["Time"];
                        tempImportObj.Abstract = newRow["Abstract"];
                        tempImportObj.MoneyStr = newRow["Money"];
                        tempImportObj.Remark = newRow["Remark"];
                        #endregion

                        var tempErrorMessage = new List<string>();

                        #region 导入的数据基本检查

                        if(string.IsNullOrEmpty(tempImportObj.CorporationCode)) {
                            tempErrorMessage.Add(ErrorStrings.Export_Validation_Company_CodeIsNull);
                        }

                        if(companyType != (int)DcsCompanyType.分公司 && tempImportObj.CorporationCode != userInfo.EnterpriseCode) {
                            tempErrorMessage.Add("非分公司业务人员只能导入当前企业的对账函");
                        }

                        if(string.IsNullOrEmpty(tempImportObj.ReconciliationTimeStr)) {
                            tempErrorMessage.Add("对账时间不能为空");
                        } else {
                            DateTime checkValue;
                            if(DateTime.TryParse(tempImportObj.ReconciliationTimeStr, out checkValue)) {
                                tempImportObj.ReconciliationTime = new DateTime(checkValue.Year, checkValue.Month, checkValue.Day, 23, 59, 59);
                                ;
                            } else {
                                tempErrorMessage.Add("对账时间格式不正确");
                            }
                        }

                        if(!string.IsNullOrEmpty(tempImportObj.ReconciliationDirectionStr)) {
                            var tempEnumValue = tempExcelOperator.ImportHelper.GetEnumValue("ReconciliationDirection", tempImportObj.ReconciliationDirectionStr);
                            if(!tempEnumValue.HasValue) {
                                tempErrorMessage.Add("对账方向不正确");
                            } else {
                                tempImportObj.ReconciliationDirection = (int)tempEnumValue;
                            }
                        }
                        if(string.IsNullOrEmpty(tempImportObj.TimeStr)) {
                            tempErrorMessage.Add("时间不能为空");
                        } else {
                            DateTime checkValue;
                            if(DateTime.TryParse(tempImportObj.TimeStr, out checkValue)) {
                                tempImportObj.Time = checkValue;
                            } else {
                                tempErrorMessage.Add("时间格式不正确");
                            }
                        }

                        if(string.IsNullOrEmpty(tempImportObj.Abstract)) {
                            tempErrorMessage.Add(ErrorStrings.Export_Validation_Customertransferbill_SummaryIsNull);
                        }
                        if(string.IsNullOrEmpty(tempImportObj.MoneyStr)) {
                            tempErrorMessage.Add(ErrorStrings.Export_Validation_Customertransferbill_AmountIsNull);
                        } else {
                            decimal checkValue;
                            if(Decimal.TryParse(tempImportObj.MoneyStr, out checkValue)) {
                                tempImportObj.Money = checkValue;
                            } else {
                                tempErrorMessage.Add("金额格式不正确");
                            }
                        }
                        #endregion

                        if(tempErrorMessage.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });
                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    #region 剩下的数据进行业务检查
                    var groups = tempRightList.GroupBy(r => r).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        groupItem.ErrorMsg = "导入数据重复";
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    //查找企业
                    var corporationCodes = tempRightList.Select(r => r.CorporationCode).Distinct().ToArray();
                    var dbCompanys = new List<CompanyExtend>();
                    Func<string[], bool> getDbCompanys = values => {
                        var item = new CompanyExtend {
                            Id = int.Parse(values[0]),
                            Name = values[1],
                            Code = values[2],
                            Type = int.Parse(values[3]),
                        };
                        dbCompanys.Add(item);
                        return false;
                    };
                    db.QueryDataWithInOperator("select Id,Name,Code,type from Company where Status=1 ", "Code", false, corporationCodes, getDbCompanys);
                    foreach(var item in tempRightList) {
                        var company = dbCompanys.FirstOrDefault(r => r.Code == item.CorporationCode);
                        if(company == null) {
                            item.ErrorMsg = "企业不存在有效数据";
                            continue;
                        }
                        item.CorporationId = company.Id;
                        item.CorporationName = company.Name;
                        item.CompanyType = company.Type;
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    foreach(var item in tempRightList) {
                        if(item.CompanyType == (int)DcsCompanyType.配件供应商) {
                            if(string.IsNullOrEmpty(item.PartsSalesCategoryName)) {
                                item.ErrorMsg = ErrorStrings.Export_Validation_MarketingDepartment_PartsSalesCategoryIsNull;
                            }
                        } else {
                            if(string.IsNullOrEmpty(item.AccountGroupName)) {
                                item.ErrorMsg = ErrorStrings.Export_Validation_Credenceapplication_AccountGroupName;
                            }
                        }
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    //查找账户组
                    var accountGroupNames = tempRightList.Where(r => r.CompanyType != (int)DcsCompanyType.配件供应商).Select(r => r.AccountGroupName).Distinct().ToArray();
                    var dbAccountGroups = new List<AccountGroupExtend>();
                    Func<string[], bool> getDbAccountGroups = values => {
                        var item = new AccountGroupExtend {
                            Id = int.Parse(values[0]),
                            Name = values[1]
                        };
                        dbAccountGroups.Add(item);
                        return false;
                    };
                    db.QueryDataWithInOperator("select Id,Name from AccountGroup where Status=1 ", "Name", false, accountGroupNames, getDbAccountGroups);
                    foreach(var item in tempRightList.Where(r => r.CompanyType != (int)DcsCompanyType.配件供应商)) {
                        var partsSalesCategory = dbAccountGroups.FirstOrDefault(r => r.Name == item.AccountGroupName);
                        if(partsSalesCategory == null) {
                            item.ErrorMsg = "账户组不存在有效数据";
                            continue;
                        }
                        item.AccountGroupId = partsSalesCategory.Id;
                        item.PartsSalesCategoryId = 0;
                        item.PartsSalesCategoryName = "";
                    }

                    //查找品牌
                    var partsSalesCategoryNames = tempRightList.Where(r => r.CompanyType == (int)DcsCompanyType.配件供应商).Select(r => r.PartsSalesCategoryName).Distinct().ToArray();
                    var dbpartsSalesCategorys = new List<PartsSalesCategoryExtend>();
                    Func<string[], bool> getDbPartsSalesCategorys = values => {
                        var item = new PartsSalesCategoryExtend {
                            Id = int.Parse(values[0]),
                            Name = values[1]
                        };
                        dbpartsSalesCategorys.Add(item);
                        return false;
                    };
                    db.QueryDataWithInOperator("select Id,Name from PartsSalesCategory where Status=1 ", "Name", false, partsSalesCategoryNames, getDbPartsSalesCategorys);
                    foreach(var item in tempRightList.Where(r => r.CompanyType == (int)DcsCompanyType.配件供应商)) {
                        var partsSalesCategory = dbpartsSalesCategorys.FirstOrDefault(r => r.Name == item.PartsSalesCategoryName);
                        if(partsSalesCategory == null) {
                            item.ErrorMsg = "品牌不存在有效数据";
                            continue;
                        }
                        item.PartsSalesCategoryId = partsSalesCategory.Id;
                        item.AccountGroupId = 0;
                        item.AccountGroupName = "";
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    var supplierCompanyId = tempRightList.Where(r => r.CompanyType == (int)DcsCompanyType.配件供应商).Select(r => r.CorporationId.ToString()).Distinct().ToArray();
                    var corporationIds = tempRightList.Where(r => r.CompanyType != (int)DcsCompanyType.配件供应商).Select(r => r.CorporationId.ToString()).Distinct().ToArray();
                    var dbCustomerAccountHisDetails = new List<CustomerAccountHisDetailExtend>();
                    Func<string[], bool> getDbCustomerAccountHisDetails = values => {
                        var item = new CustomerAccountHisDetailExtend {
                            AfterChangeAmount = values[0] == "" ? 0 : decimal.Parse(values[0]),
                            ProcessDate = DateTime.Parse(values[1]),
                            CustomerCompanyId = int.Parse(values[2]),
                            AccountGroupId = int.Parse(values[3])
                        };
                        dbCustomerAccountHisDetails.Add(item);
                        return false;
                    };
                    //供应商获取账户余额
                    db.QueryDataWithInOperator("SELECT  a.AfterChangeAmount,a.ProcessDate,b.SupplierCompanyId,b.PartsSalesCategoryId FROM AccountPayableHistoryDetail a,SupplierAccount b where a.SupplierAccountId=b.id ", "b.SupplierCompanyId", false, supplierCompanyId, getDbCustomerAccountHisDetails);
                    //非供应商获取账户余额
                    db.QueryDataWithInOperator("select a.AfterChangeAmount,a.ProcessDate,b.CustomerCompanyId,b.AccountGroupId  from CustomerAccountHisDetail a ,CustomerAccount b where a.CustomerAccountId = b.Id  ", "b.CustomerCompanyId", false, corporationIds, getDbCustomerAccountHisDetails);
                    foreach(var item in tempRightList) {
                        var customerAccountHisDetail = dbCustomerAccountHisDetails.Where(r => (r.AccountGroupId == item.AccountGroupId || item.CompanyType == (int)DcsCompanyType.配件供应商) && r.CustomerCompanyId == item.CorporationId && r.ProcessDate < item.ReconciliationTime).OrderByDescending(r => r.ProcessDate).FirstOrDefault();
                        item.YeBalance = 0;
                        if(customerAccountHisDetail != null) {
                            item.YeBalance = customerAccountHisDetail.AfterChangeAmount;
                        }
                    }
                    #endregion

                    //获取所有不合格数据
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    //获取所有合格数据
                    rightList = allList.Except(errorList).ToList();
                }
                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempObj = list[index - 1];
                            var values = new object[] {
                                #region 设置错误信息导出的列的值
                                tempObj.AccountGroupName, tempObj.PartsSalesCategoryName, tempObj.CorporationCode, tempObj.ReconciliationTimeStr, tempObj.ReconciliationDirectionStr, tempObj.TimeStr, tempObj.Abstract, tempObj.MoneyStr, tempObj.Remark, tempObj.ErrorMsg
                                #endregion
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }

                //导入所有合格数据
                if(!rightList.Any()) {
                    return true;
                }
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var ts = conn.BeginTransaction();
                    try {
                        if(rightList.Any()) {
                            var sqlInsertCAReconciliation = db.GetInsertSql("CAReconciliation", "Id", new[] {

                                "StatementCode", "AccountGroupId", "AccountGroupName","PartsSalesCategoryId","PartsSalesCategoryName", "CorporationId", "CorporationCode", "CorporationName", "YeBalance", "ReconciliationTime", "ReconciliationId", "ReconciliationName", "Status", "CreatorId", "CreatorName", "CreateTime"
                            });
                            var sqlInsertDetail = db.GetInsertSql("CAReconciliationList", "Id", new[] {
                                "CAReconciliationId", "ReconciliationDirection", "Time", "Abstract", "Money", "Remark"
                            });
                            var cAReconciliations = rightList.GroupBy(r => new {
                                r.AccountGroupId,
                                r.CorporationId,
                                r.ReconciliationTime
                            }).Select(r => new {
                                r.Key.AccountGroupId,
                                r.Key.CorporationId,
                                r.Key.ReconciliationTime
                            }).ToArray();
                            foreach(var item in cAReconciliations) {
                                var cAReconciliationLists = rightList.Where(r => r.AccountGroupId == item.AccountGroupId && r.CorporationId == item.CorporationId && r.ReconciliationTime == item.ReconciliationTime).ToArray();
                                var cAReconciliation = cAReconciliationLists.First();
                                var command = db.CreateDbCommand(sqlInsertCAReconciliation, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("StatementCode", CodeGenerator.Generate(db, "CAReconciliation")));
                                command.Parameters.Add(db.CreateDbParameter("AccountGroupId", cAReconciliation.AccountGroupId));
                                command.Parameters.Add(db.CreateDbParameter("AccountGroupName", cAReconciliation.AccountGroupName));
                                command.Parameters.Add(db.CreateDbParameter("PartsSalesCategoryId", cAReconciliation.PartsSalesCategoryId));
                                command.Parameters.Add(db.CreateDbParameter("PartsSalesCategoryName", cAReconciliation.PartsSalesCategoryName));
                                command.Parameters.Add(db.CreateDbParameter("CorporationId", cAReconciliation.CorporationId));
                                command.Parameters.Add(db.CreateDbParameter("CorporationCode", cAReconciliation.CorporationCode));
                                command.Parameters.Add(db.CreateDbParameter("CorporationName", cAReconciliation.CorporationName));
                                command.Parameters.Add(db.CreateDbParameter("YeBalance", cAReconciliation.YeBalance));
                                command.Parameters.Add(db.CreateDbParameter("ReconciliationTime", cAReconciliation.ReconciliationTime));
                                command.Parameters.Add(db.CreateDbParameter("ReconciliationId", userInfo.Id));
                                command.Parameters.Add(db.CreateDbParameter("ReconciliationName", userInfo.Name));
                                command.Parameters.Add(db.CreateDbParameter("Status", (int)DcsReconciliationStatus.新增));
                                command.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                command.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                command.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                var tempId = db.ExecuteInsert(command, "Id");
                                foreach(var cAReconciliationList in cAReconciliationLists) {
                                    var detail = db.CreateDbCommand(sqlInsertDetail, conn, ts);
                                    detail.Parameters.Add(db.CreateDbParameter("CAReconciliationId", tempId));
                                    detail.Parameters.Add(db.CreateDbParameter("ReconciliationDirection", cAReconciliationList.ReconciliationDirection));
                                    detail.Parameters.Add(db.CreateDbParameter("Time", cAReconciliationList.Time));
                                    detail.Parameters.Add(db.CreateDbParameter("Abstract", cAReconciliationList.Abstract));
                                    detail.Parameters.Add(db.CreateDbParameter("Money", cAReconciliationList.Money));
                                    detail.Parameters.Add(db.CreateDbParameter("Remark", cAReconciliationList.Remark));
                                    detail.ExecuteNonQuery();
                                }
                            }
                        }
                        //无异常提交
                        ts.Commit();
                    } catch(Exception) {
                        //报错回滚
                        ts.Rollback();
                        throw;
                    } finally {
                        if(conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
            }
        }

        public bool ExportCAReconciliation(int[] ids, string corporationCode, string corporationName, int? partsSalesCategoryId, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName) {
            fileName = GetExportFilePath("企业往来账对账函_" + ".xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);
                var userInfo = Utils.GetCurrentUserInfo();
                using(var conn = db.CreateDbConnection()) {
                    var dbCompanyType = new List<int>();
                    Func<string[], bool> getDbCompanyType = value => {
                        dbCompanyType.Add(Convert.ToInt32(value[0]));
                        return false;
                    };
                    var sqlCompany = string.Format("Select Type From Company Where Id={0}", userInfo.EnterpriseId);
                    db.QueryDataWithInOperator(sqlCompany, "1", false, new[] {
                        "1"
                    }, getDbCompanyType);
                    var companyType = dbCompanyType.FirstOrDefault();

                    var sql = new StringBuilder();
                    sql.Append(@"select  
                        StatementCode,CorporationCode,CorporationName,AccountGroupName,PartsSalesCategoryName,YeBalance,NoArrivalAdd,NoArrivalReduce,AfterAdjustmentAmount,CustomerAccountAmount,ReconciliationTime,ReconciliationName,
                        (select value from keyvalueitem where name='ReconciliationStatus' and key=a.status) status,
                        a.Remark,
                        (select value from keyvalueitem where name='ReconciliationDirection' and key=ReconciliationDirection) ReconciliationDirection,
                        Time,Abstract,Money,b.Remark,CreatorName,CreateTime,ModifierName,ModifierTime,SubmitterName,SubmitTime,ConfirmorName,ConfirmorTime,AbandonerName,AbandonerTime
                        from CAReconciliation a , CAReconciliationList b where a.Id=b.CAReconciliationId ");
                    var dbParameters = new List<DbParameter>();

                    #region 条件过滤
                    if(ids != null && ids.Length > 0) {
                        sql.Append(" and a.id in (");
                        for(var i = 0; i < ids.Length; i++) {
                            if(ids.Length == i + 1) {
                                sql.Append("{0}" + ids[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter(ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            } else {
                                sql.Append("{0}" + ids[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter(ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                        }
                        sql.Append(")");
                    } else {
                        if(companyType != (int)DcsCompanyType.分公司) {
                            sql.Append(@" and a.CorporationId = {0}CorporationId ");
                            dbParameters.Add(db.CreateDbParameter("CorporationId", userInfo.EnterpriseId));
                        }
                        if(!String.IsNullOrEmpty(corporationCode)) {
                            sql.Append(@" and Upper(a.corporationCode) like Upper({0}corporationCode) ");
                            dbParameters.Add(db.CreateDbParameter("corporationCode", "%" + corporationCode + "%"));
                        }
                        if(!String.IsNullOrEmpty(corporationName)) {
                            sql.Append(@" and a.corporationName like {0}corporationName ");
                            dbParameters.Add(db.CreateDbParameter("corporationName", "%" + corporationName + "%"));
                        }
                        if(status.HasValue) {
                            sql.Append(@" and a.status = {0}status ");
                            dbParameters.Add(db.CreateDbParameter("status", status.Value));
                        }
                        if(partsSalesCategoryId.HasValue) {
                            sql.Append(@" and a.AccountGroupId = {0}AccountGroupId ");
                            dbParameters.Add(db.CreateDbParameter("AccountGroupId", partsSalesCategoryId.Value));
                        }
                        if(createTimeBegin.HasValue) {
                            sql.Append(@" and a.Createtime >=to_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(@" and a.Createtime <=to_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                    }
                    #endregion

                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    conn.Open();
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    "对账单号", ErrorStrings.Export_Title_Company_Code, ErrorStrings.Export_Title_Company_Name, ErrorStrings.Export_Title_Credenceapplication_AccountGroupNameNew, ErrorStrings.Export_Title_Partssalescategory_Name, ErrorStrings.Export_Validation_Customeraccount_AccountAmount,"未达账项调增","未达账项调减","调整后余额","客户账户金额", "对账时间", "对账人", ErrorStrings.Export_Title_AccountPeriod_Status, ErrorStrings.Export_Title_PartsBranch_Remark, "对账方向", "时间", ErrorStrings.Export_Title_Customertransferbill_Summary, ErrorStrings.Export_Title_Customertransferbill_Amount, ErrorStrings.Export_Title_PartsBranch_Remark, ErrorStrings.Export_Title_AccountPeriod_CreatorName, ErrorStrings.Export_Title_AccountPeriod_CreateTime, ErrorStrings.Export_Title_AccountPeriod_ModifierName, ErrorStrings.Export_Title_AccountPeriod_ModifyTime, ErrorStrings.Export_Title_PartsSalesOrder_SubmitterName, ErrorStrings.Export_Title_PartsSalesOrder_SubmitTime, ErrorStrings.Export_Title_InternalAllocationBill_ApproverName, ErrorStrings.Export_Title_InternalAllocationBill_ApproveTime, ErrorStrings.Export_Title_PartsBranch_AbandonerName, ErrorStrings.Export_Title_PartsBranch_AbandonerTime
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }
    }
}