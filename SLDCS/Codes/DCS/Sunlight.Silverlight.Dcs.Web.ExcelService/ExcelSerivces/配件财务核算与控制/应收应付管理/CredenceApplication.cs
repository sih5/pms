﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        /// <summary>
        /// 导出信用申请单
        /// </summary>
        public bool ExportCredenceApplication(int[] ids, int salesCompanyId, string customerCompanyCode, string customerCompanyName, int? status, DateTime? approveTimeBegin, DateTime? approveTimeEnd, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName) {
            fileName = GetExportFilePath("信用申请单_" + ".xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {
                    var sql = new StringBuilder();
                    sql.AppendFormat(@"Select b.Code,
                                               b.Name,
                                               a.Guarantorcompanyname,
                                               c.Name,
                                               (select value from keyvalueitem where NAME = 'CredenceApplication_Status'and key=a.Status) As Status,
                                               a.Credencelimit,
                                               a.ValidationDate,
                                               a.ExpireDate,
                                               a.Creatorname,
                                               a.Createtime,
                                               a.Modifiername,
                                               a.Modifytime,
                                               a.Approvername,
                                               a.Approvetime
                                          From Credenceapplication a
                                          Left Join Company b
                                            On a.Customercompanyid = b.Id
                                          Left Join Accountgroup c
                                            On a.Accountgroupid = c.Id
                                where a.salesCompanyId={0} ", salesCompanyId);
                    var dbParameters = new List<DbParameter>();
                    #region 条件过滤
                    if(ids != null && ids.Length > 0) {
                        sql.Append(" and a.id in (");
                        for(var i = 0; i < ids.Length; i++) {
                            if(ids.Length == i + 1) {
                                sql.Append("{0}" + ids[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter(ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            } else {
                                sql.Append("{0}" + ids[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter(ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                        }
                        sql.Append(")");
                    } else {
                        if(!String.IsNullOrEmpty(customerCompanyCode)) {
                            sql.Append(@" and Upper(b.Code) like Upper({0}customerCompanyCode) ");
                            dbParameters.Add(db.CreateDbParameter("customerCompanyCode", "%" + customerCompanyCode + "%"));
                        }
                        if(!String.IsNullOrEmpty(customerCompanyName)) {
                            sql.Append(@" and b.Name like {0}customerCompanyName ");
                            dbParameters.Add(db.CreateDbParameter("customerCompanyName", "%" + customerCompanyName + "%"));
                        }
                        if(status.HasValue) {
                            sql.Append(@" and a.status = {0}status ");
                            dbParameters.Add(db.CreateDbParameter("status", status.Value));
                        }
                        if(approveTimeBegin.HasValue) {
                            sql.Append(@" and a.ApproveTime >=to_date({0}approveTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = approveTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("approveTimeBegin", tempTime.ToString("G")));
                        }
                        if(approveTimeEnd.HasValue) {
                            sql.Append(@" and a.ApproveTime <=to_date({0}approveTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = approveTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("approveTimeEnd", tempTime.ToString("G")));
                        }
                        if(createTimeBegin.HasValue) {
                            sql.Append(@" and a.Createtime >=to_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(@" and a.Createtime <=to_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                    }
                    #endregion

                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    conn.Open();
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                ErrorStrings.Export_Title_Credenceapplication_CompanyCode,ErrorStrings.Export_Title_Credenceapplication_CompanyName,ErrorStrings.Export_Title_Credenceapplication_GuarantorCompanyName,ErrorStrings.Export_Title_Credenceapplication_AccountgroupName,ErrorStrings.Export_Title_AccountPeriod_Status,ErrorStrings.Export_Title_Credenceapplication_Credencelimit,ErrorStrings.Export_Title_Credenceapplication_ValidationDate,ErrorStrings.Export_Title_Credenceapplication_ExpireDate,ErrorStrings.Export_Title_AccountPeriod_CreatorName,ErrorStrings.Export_Title_AccountPeriod_CreateTime,ErrorStrings.Export_Title_AccountPeriod_ModifierName,ErrorStrings.Export_Title_AccountPeriod_ModifyTime,ErrorStrings.Export_Title_PackingPropertyApp_ApproverName,ErrorStrings.Export_Title_PackingPropertyApp_ApproveTime
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }

        /// <summary>
        /// 导出虚拟信用申请单
        /// </summary>
        public bool ExportVirtualCredenceApplication(int[] ids, int salesCompanyId, string customerCompanyCode, string customerCompanyName, int? status, DateTime? approveTimeBegin, DateTime? approveTimeEnd, DateTime? createTimeBegin, DateTime? createTimeEnd, string businessCode,int? creditType, out string fileName) {
            fileName = GetExportFilePath("信用申请单_" + ".xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {
                    var sql = new StringBuilder();
                    sql.AppendFormat(@"Select b.Code,
                                               b.Name,
                                               a.Guarantorcompanyname,
                                               c.Name,
                                               (select value from keyvalueitem where NAME = 'CredenceApplication_Status'and key=a.Status) As Status,
                                               (select value from keyvalueitem where NAME = 'Credit_Type'and key=a.CreditType) As CreditType,
                                               a.Credencelimit,
                                               a.ValidationDate,
                                               a.ExpireDate,
                                               a.Creatorname,
                                               a.Createtime,a.InitialApproverName,a.InitialApproveTime,a.CheckerName,a.CheckTime,
                                              
                                               a.Approvername,
                                               a.Approvetime,a.UpperCheckerName,a.UpperCheckTime,a.UpperApproverName,a.UpperApproveTime,
                                                case b.type
                                                when 2 then
                                                i.businessCode
                                                when 7 then
                                                 i.businessCode
                                                when 3 then
                                                s.businessCode
                                                end,
                                               a.Modifiername,
                                               a.Modifytime 
                                          From Credenceapplication a
                                          Left Join Company b
                                            On a.Customercompanyid = b.Id
                                          Left Join Accountgroup c
                                            On a.Accountgroupid = c.Id
                                          left join SalesUnit s on a.AccountGroupId = s.AccountGroupId and c.SalesCompanyId = s.OwnerCompanyId
                                          left join DealerServiceInfo  i on s.PartsSalesCategoryId = i.PartsSalesCategoryId and a.CustomerCompanyId = i.DealerId and i.status=1
                                where a.salesCompanyId={0} ", salesCompanyId);
                    var dbParameters = new List<DbParameter>();
                    #region 条件过滤                    
                    if(ids != null && ids.Length > 0) {
                        sql.Append(" and a.Id in (");
                        for(var i = 0; i < ids.Length; i++)
                            if(ids.Length == i + 1) {
                                sql.Append("{0}" + ("Id" + i).ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter(("Id" + i).ToString(CultureInfo.InvariantCulture), ids[i]));
                            } else {
                                sql.Append("{0}" + ("Id" + i).ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter(("Id" + i).ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                        sql.Append(")");
                    } else {
                        if(!String.IsNullOrEmpty(customerCompanyCode)) {
                            sql.Append(@" and Upper(b.Code) like Upper({0}customerCompanyCode) ");
                            dbParameters.Add(db.CreateDbParameter("customerCompanyCode", "%" + customerCompanyCode + "%"));
                        }
                        if(!String.IsNullOrEmpty(customerCompanyName)) {
                            sql.Append(@" and b.Name like {0}customerCompanyName ");
                            dbParameters.Add(db.CreateDbParameter("customerCompanyName", "%" + customerCompanyName + "%"));
                        }
                        if(status.HasValue) {
                            sql.Append(@" and a.status = {0}status ");
                            dbParameters.Add(db.CreateDbParameter("status", status.Value));
                        }
                        if(approveTimeBegin.HasValue) {
                            sql.Append(@" and a.ApproveTime >=to_date({0}approveTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = approveTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("approveTimeBegin", tempTime.ToString("G")));
                        }
                        if(approveTimeEnd.HasValue) {
                            sql.Append(@" and a.ApproveTime <=to_date({0}approveTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = approveTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("approveTimeEnd", tempTime.ToString("G")));
                        }
                        if(!string.IsNullOrEmpty(businessCode)) {
                            sql.Append(" and s.businessCode like {0}businessCode and i.businessCode like {0}businessCode ");
                            dbParameters.Add(db.CreateDbParameter("businessCode", "%" + businessCode + "%"));
                        }
                        if(createTimeBegin.HasValue) {
                            sql.Append(@" and a.Createtime >=to_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(@" and a.Createtime <=to_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                        if(creditType.HasValue) {
                            sql.Append(@" and a.creditType = {0}creditType ");
                            dbParameters.Add(db.CreateDbParameter("creditType", creditType.Value));
                        }
                    }
                    #endregion

                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    conn.Open();
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                ErrorStrings.Export_Title_Credenceapplication_CompanyCode,ErrorStrings.Export_Title_Credenceapplication_CompanyName,ErrorStrings.Export_Title_Credenceapplication_GuarantorCompanyName,ErrorStrings.Export_Title_Credenceapplication_AccountgroupName,ErrorStrings.Export_Title_AccountPeriod_Status,
                                ErrorStrings.Export_Title_Credenceapplication_Type,ErrorStrings.Export_Title_Credenceapplication_Credencelimit,ErrorStrings.Export_Title_Credenceapplication_ValidationDate,ErrorStrings.Export_Title_Credenceapplication_ExpireDate,ErrorStrings.Export_Title_AccountPeriod_CreatorName,ErrorStrings.Export_Title_AccountPeriod_CreateTime,
                                ErrorStrings.Export_Title_PartsPurReturnOrder_InitialApproverName,ErrorStrings.Export_Title_PartsPurReturnOrder_InitialApproveTime,
                                ErrorStrings.Export_Title_Dealerpartsinventorybill_ApproverName,ErrorStrings.Export_Title_Dealerpartsinventorybill_ApproverTime,
                                ErrorStrings.Export_Title_PackingPropertyApp_ApproverName,ErrorStrings.Export_Title_PackingPropertyApp_ApproveTime,
                                ErrorStrings.DataGridView_ColumnItem_UpperCheckerName,ErrorStrings.DataGridView_ColumnItem_UpperCheckTime,ErrorStrings.Export_Title_Credenceapplication_BusinessCode,
                                ErrorStrings.Export_Title_PackingPropertyApp_UpperCheckerName,ErrorStrings.Export_Title_PackingPropertyApp_UpperCheckTime, ErrorStrings.Export_Title_AccountPeriod_ModifierName,ErrorStrings.Export_Title_AccountPeriod_ModifyTime,                                
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }

        /// <summary>
        /// 导入信用申请单
        /// </summary>
        public bool ImportCredenceApplication(string fileName, out int excelImportNum, out List<CredenceApplicationExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<CredenceApplicationExtend>();
            var allList = new List<CredenceApplicationExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("CredenceApplication", out notNullableFields, out fieldLenght);

                List<string> notNullableFieldsCustomerCompany;
                Dictionary<string, int> fieldLenghtCustomerCompany;
                db.GetTableSchema("Company", out notNullableFieldsCustomerCompany, out fieldLenghtCustomerCompany);

                List<string> notNullableFieldsAccountGroup;
                Dictionary<string, int> fieldLenghtAccountGroup;
                db.GetTableSchema("AccountGroup", out notNullableFieldsAccountGroup, out fieldLenghtAccountGroup);


                List<object> excelColumns;
                List<CredenceApplicationExtend> rightList;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    #region 指定文件中的列对应的名称 例如 excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode, "PartCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Credenceapplication_CustomerCode, "CustomerCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Credenceapplication_CustomerName, "CustomerName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Credenceapplication_AccountGroupNameNew, "AccountGroupName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Credenceapplication_Credencelimit, "CredenceLimit");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Credenceapplication_Type, "CreditType");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Credenceapplication_ExpireDate, "ExpireDate");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Remark, "Remark");
                    #endregion
                    var tempExcelOperator = excelOperator;
                    #region 获取对应枚举
                    var keyValuePairs = new[] {
                            new KeyValuePair<string, string>("CreditType", "Credit_Type")
                           
                        };
                    tempExcelOperator.ImportHelper.LoadEnumFromDb(null, null, keyValuePairs);
                    #endregion
                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var tempImportObj = new CredenceApplicationExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        #region 根据导入的字段给对应的实体属性赋值
                        tempImportObj.CustomerCodeStr = newRow["CustomerCode"];
                        tempImportObj.CustomerNameStr = newRow["CustomerName"];
                        tempImportObj.AccountGroupNameStr = newRow["AccountGroupName"];
                        tempImportObj.CredenceLimitStr = newRow["CredenceLimit"];
                        tempImportObj.ExpireDateStr = newRow["ExpireDate"];
                        tempImportObj.RemarkStr = newRow["Remark"];
                        tempImportObj.CreditTypeStr = newRow["CreditType"];
                        #endregion
                        var tempErrorMessage = new List<string>();
                        #region 导入的数据基本检查

                        //授信类型检查
                        var fieldIndex = notNullableFields.IndexOf("CreditType".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.CreditTypeStr)) {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Credenceapplication_CreditTypeIsNull);
                        } else {
                            var tempEnumValue = tempExcelOperator.ImportHelper.GetEnumValue("CreditType", tempImportObj.CreditTypeStr);
                            if(!tempEnumValue.HasValue) {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Credenceapplication_CreditTypeValueError);
                            } else {
                                tempImportObj.CreditType = tempEnumValue.Value;
                            }
                        }
                        //客户编号检查
                         fieldIndex = notNullableFields.IndexOf("CustomerCompanyId".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.CustomerCodeStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Credenceapplication_CustomerCodeIsNull);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.CustomerCodeStr) > fieldLenghtCustomerCompany["Code".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Credenceapplication_CustomerCodeIsLong);
                        }

                        //客户名称检查
                        fieldIndex = notNullableFields.IndexOf("CustomerCompanyId".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.CustomerNameStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Credenceapplication_CustomerNameIsNull);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.CustomerNameStr) > fieldLenghtCustomerCompany["Name".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Credenceapplication_CustomerNameIsLong);
                        }

                        //失效时间检查
                        fieldIndex = notNullableFields.IndexOf("ExpireDate".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.ExpireDateStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Credenceapplication_ExpireDate);
                        } else {
                            DateTime checkValue;
                            if(!DateTime.TryParse(tempImportObj.ExpireDateStr, out checkValue)) {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Credenceapplication_ExpireDateFormate);
                            } else if(checkValue < DateTime.Now) {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Credenceapplication_ExpireDateToDay);
                            } else {
                                tempImportObj.ExpireDate = checkValue;
                            }
                        }

                        //账户组检查
                        fieldIndex = notNullableFields.IndexOf("AccountGroupId".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.AccountGroupNameStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Credenceapplication_AccountGroupName);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.AccountGroupNameStr) > fieldLenghtAccountGroup["Name".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Credenceapplication_AccountGroupNameIsLong);
                        }
                        //信用额度检查
                        fieldIndex = notNullableFields.IndexOf("CredenceLimit".ToUpper());
                        if(!string.IsNullOrEmpty(tempImportObj.CredenceLimitStr)) {
                            Decimal checkValue;
                            if(Decimal.TryParse(tempImportObj.CredenceLimitStr, out checkValue)) {
                                if(checkValue < 0) {
                                    tempErrorMessage.Add(ErrorStrings.Export_Validation_Credenceapplication_CredenceLimitOverZero);
                                } else {
                                    tempImportObj.CredenceLimit = checkValue;
                                }
                            } else {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Credenceapplication_CredenceLimitInteger);
                            }
                        } else {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Credenceapplication_CredenceLimitIsNull);
                        }

                        //备注Remark
                        fieldIndex = notNullableFields.IndexOf("Remark".ToUpper());
                        if(!string.IsNullOrEmpty(tempImportObj.RemarkStr)) {
                            if(Encoding.Default.GetByteCount(tempImportObj.RemarkStr) > fieldLenght["Remark".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchasePricingDetail_Validation16);
                        }

                        #endregion
                        if(tempErrorMessage.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });
                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    #region 导入内容业务检查
                    //文件内，客户编号账户组组合唯一
                    var groups = tempRightList.GroupBy(r => new {
                        r.CustomerCodeStr,
                        r.AccountGroupNameStr
                    }).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        groupItem.ErrorMsg = ErrorStrings.ImpPartsSupplierRelation_Validation31;
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    //校验客户编号、客户名称组合存在于企业表中
                    var companyCodes = tempRightList.Select(r => r.CustomerCodeStr).Distinct().ToArray();
                    var dbCompanys = new List<CompanyExtend>();
                    Func<string[], bool> getDbCompanys = value => {
                        var dbObj = new CompanyExtend {
                            Id = Convert.ToInt32(value[0]),
                            Code = value[1],
                            Name = value[2]
                        };
                        dbCompanys.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select Id,Code,Name from Company where status=1 ", "Code", false, companyCodes, getDbCompanys);
                    var errorCompanys = tempRightList.Where(r => !dbCompanys.Any(v => v.Name == r.CustomerNameStr && v.Code == r.CustomerCodeStr)).ToList();
                    foreach(var errorItem in errorCompanys) {
                        errorItem.ErrorMsg = String.Format(ErrorStrings.Export_Validation_Credenceapplication_Validation1, errorItem.CustomerCodeStr, errorItem.CustomerNameStr);
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    foreach(var rightItem in tempRightList) {
                        var tempCompany = dbCompanys.Single(r => r.Code == rightItem.CustomerCodeStr);
                        rightItem.CustomerCompanyId = tempCompany.Id;
                    }

                    //校验账户组名称存在于账户组中
                    var accountGroupNames = tempRightList.Select(r => r.AccountGroupNameStr).Distinct().ToArray();
                    var dbAccountGroups = new List<AccountGroupExtend>();
                    Func<string[], bool> getDbAccountGroups = value => {
                        dbAccountGroups.Add(new AccountGroupExtend {
                            Id = Convert.ToInt32(value[0]),
                            Name = value[1]
                        });
                        return false;
                    };
                    db.QueryDataWithInOperator("select Id,Name from AccountGroup where status=1", "Name", false, accountGroupNames, getDbAccountGroups);
                    var errorAccountGroups = tempRightList.Where(r => dbAccountGroups.All(v => v.Name != r.AccountGroupNameStr)).ToList();
                    foreach(var errorItem in errorAccountGroups) {
                        errorItem.ErrorMsg = String.Format(ErrorStrings.Export_Validation_Credenceapplication_Validation2, errorItem.AccountGroupNameStr);
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    foreach(var rightItem in tempRightList) {
                        var tempAccountGroup = dbAccountGroups.Single(r => r.Name == rightItem.AccountGroupNameStr);
                        rightItem.AccountGroupId = tempAccountGroup.Id;
                    }
                    #endregion
                    //获取所有不合格数据
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    //获取所有合格数据
                    rightList = allList.Except(errorList).ToList();

                    #region 将合格数据的值填上
                    foreach(var rightItem in rightList) {
                        rightItem.ExpireDate = Convert.ToDateTime(rightItem.ExpireDateStr);
                        rightItem.CredenceLimit = Convert.ToDecimal(rightItem.CredenceLimitStr);
                        rightItem.Remark = rightItem.RemarkStr;
                    }
                    #endregion

                }
                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempObj = list[index - 1];
                            var values = new object[] {
                                #region 设置错误信息导出的列的值
                                tempObj.CustomerCodeStr, tempObj.CustomerNameStr,
                                tempObj.AccountGroupNameStr ,tempObj.CredenceLimitStr ,
                                tempObj.ExpireDateStr ,tempObj.RemarkStr,tempObj.ErrorMsg
                                #endregion                     
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }

                //导入所有合格数据
                if(!rightList.Any())
                    return true;
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    //开启事务，新增更新配件在一个事务内
                    var ts = conn.BeginTransaction();
                    try {
                        //新增配件
                        if(rightList.Any()) {
                            #region 获取对应的插入的SQL 通过db.GetInsertSql方法
                            //获取新增数据的sql语句，Id为主键
                            var sqlInsert = db.GetInsertSql("CredenceApplication", "Id", new[] {
                                "Code","SalesCompanyId","GuarantorCompanyId","GuarantorCompanyName","AccountGroupId","CustomerCompanyId",
                                "ValidationDate","ExpireDate","ApplyCondition","CredenceLimit","Status","ConsumedAmount","Remark","CreatorId",
                                "CreatorName","CreateTime","CreditType"
                            });
                            #endregion
                            var userInfo = Utils.GetCurrentUserInfo();
                            //往数据库增加配件信息
                            foreach(var item in rightList) {
                                #region 添加Sql的参数
                                var command = db.CreateDbCommand(sqlInsert, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("Code", CodeGenerator.Generate(db, "CredenceApplication", userInfo.EnterpriseCode)));
                                command.Parameters.Add(db.CreateDbParameter("SalesCompanyId", userInfo.EnterpriseId));
                                command.Parameters.Add(db.CreateDbParameter("GuarantorCompanyId", userInfo.EnterpriseId));
                                command.Parameters.Add(db.CreateDbParameter("GuarantorCompanyName", userInfo.EnterpriseName));
                                command.Parameters.Add(db.CreateDbParameter("AccountGroupId", item.AccountGroupId));
                                command.Parameters.Add(db.CreateDbParameter("CustomerCompanyId", item.CustomerCompanyId));
                                command.Parameters.Add(db.CreateDbParameter("ValidationDate", DateTime.Now));
                                command.Parameters.Add(db.CreateDbParameter("ExpireDate", item.ExpireDate));
                                command.Parameters.Add(db.CreateDbParameter("ApplyCondition", (int)DcsCredenceApplicationApplyCondition.自由支配使用));
                                command.Parameters.Add(db.CreateDbParameter("CredenceLimit", item.CredenceLimit));
                                command.Parameters.Add(db.CreateDbParameter("Status", (int)DcsCredenceApplicationStatus.新增));
                                command.Parameters.Add(db.CreateDbParameter("ConsumedAmount", 0));
                                command.Parameters.Add(db.CreateDbParameter("Remark", item.Remark));
                                command.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                command.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                command.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                command.Parameters.Add(db.CreateDbParameter("CreditType", item.CreditType));
                                command.ExecuteNonQuery();
                                #endregion
                            }
                        }
                        //无异常提交
                        ts.Commit();
                    } catch(Exception) {
                        //报错回滚
                        ts.Rollback();
                        throw;
                    } finally {
                        if(conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
            }
        }

    }
}
