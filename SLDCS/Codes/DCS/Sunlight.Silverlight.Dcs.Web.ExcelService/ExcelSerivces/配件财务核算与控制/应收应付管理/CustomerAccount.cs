﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Globalization;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        /// <summary>
        /// 导出客户账户
        /// </summary>
        public bool ExportCustomerAccount(int[] customerAccountId, int accountGroupSalesCompanyId, string companyName, string companyCode, int? companyType, int? accountGroupId, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, string businessCode, out string fileName) {
            fileName = GetExportFilePath("客户账户_" + ".xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {
                    var sql = new StringBuilder();
                    sql.AppendFormat(@"
                                        Select b.Code,
                                               b.Name,
                                               (select value from keyvalueitem where NAME = 'Company_Type'and key=b.Type) As Type,
                                              case b.type
                                                when 2 then
                                                s.businessCode
                                                when 7 then
                                                 s.businessCode
                                                when 3 then
                                                u.businessCode
                                                end,
                                               b.ProvinceName,
                                               c.Code,
                                               c.Name,
                                               round(a.Accountbalance,2),
                                               round(a.Shippedproductvalue,2),
                                               round(a.Pendingamount,2),
                                               round(a.Customercredenceamount,2),
                                               round(a.TempCreditTotalFee,2),
                                               round(d.AccountBalanceAmount,2), --返利台账
                                               round(nvl(tempStruct.unApproveAmountSum, 0),2) as unApproveAmountSum,
                                               round(a.ShippedProductValue + a.PendingAmount,2),
                                               round(a.AccountBalance - a.ShippedProductValue,2),
                                               round(a.AccountBalance + nvl(d.AccountBalanceAmount, 0) +
                                               a.CustomerCredenceAmount + nvl(a.TempCreditTotalFee, 0) + nvl(a.SIHCredit, 0) - a.ShippedProductValue - a.PendingAmount -
                                               nvl(tempStruct.unApproveAmountSum, 0),2),
                                               (select value from keyvalueitem where NAME = 'MasterData_Status'and key=a.Status) As Status,
                                               a.Creatorname,
                                               a.Createtime,
                                               a.Modifiername,
                                               a.Modifytime,a.SIHCredit
                                          From Customeraccount a
                                         Inner Join Company b
                                            On a.Customercompanyid = b.Id
                                         Inner Join Accountgroup c
                                            On c.Id = a.Accountgroupid
                                         Left Join SalesUnit u
                                            On a.AccountGroupId = u.AccountGroupId and c.SalesCompanyId = u.OwnerCompanyId
                                         Left Join DealerServiceInfo s
                                            On u.PartsSalesCategoryId = s.PartsSalesCategoryId and a.CustomerCompanyId = s.DealerId and s.status=1
                                          Left Join PartsRebateAccount d
                                            On a.AccountGroupId = d.AccountGroupId
                                           and a.CustomerCompanyId = d.CustomerCompanyId
                                          Left join (select salesunit.accountgroupid,
                                                            partssalesorder.salescategoryid as partsSalesCategoryId,
                                                            partssalesorder.submitcompanyid as customerCompanyId,
                                                            sum((partssalesorderdetail.orderedquantity -
                                                                nvl(partssalesorderdetail.approvequantity, 0)) *
                                                                partssalesorderdetail.orderprice) as unApproveAmountSum
                                                       from partssalesorder
                                                      inner join partssalesorderdetail
                                                         on partssalesorder.id =
                                                            partssalesorderdetail.partssalesorderid
                                                      inner join salesunit
                                                         on partssalesorder.salesunitid = salesunit.id
                                                      where partssalesorder.Status in (2,4)
                                                      group by salesunit.accountgroupid,
                                                               partssalesorder.salescategoryid,
                                                               partssalesorder.submitcompanyid) tempStruct
                                            on a.accountgroupid = tempStruct.accountgroupid
                                           and a.customercompanyid = tempStruct.customerCompanyId
                                          where b.Status = 1 and c.SalesCompanyId={0}  ", accountGroupSalesCompanyId);
                    var dbParameters = new List<DbParameter>();
                    if(customerAccountId != null && customerAccountId.Length > 0) {
                        sql.Append(" and a.id in (");
                        for(var i = 0; i < customerAccountId.Length; i++) {
                            if(customerAccountId.Length == i + 1) {
                                sql.Append("{0}" + customerAccountId[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter(customerAccountId[i].ToString(CultureInfo.InvariantCulture), customerAccountId[i]));
                            } else {
                                sql.Append("{0}" + customerAccountId[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter(customerAccountId[i].ToString(CultureInfo.InvariantCulture), customerAccountId[i]));
                            }
                        }
                        sql.Append(")");
                    } else {
                        #region 条件过滤
                        if(!string.IsNullOrEmpty(companyName)) {
                            sql.Append(" and b.name like {0}companyName ");
                            dbParameters.Add(db.CreateDbParameter("companyName", "%" + companyName + "%"));
                        }
                        if(!string.IsNullOrEmpty(companyCode)) {
                            sql.Append(" and b.code like {0}companyCode ");
                            dbParameters.Add(db.CreateDbParameter("companyCode", "%" + companyCode + "%"));
                        }
                        if(companyType.HasValue) {
                            sql.Append(" and b.Type = {0}companyType ");
                            dbParameters.Add(db.CreateDbParameter("companyType", companyType.Value));
                        }
                        if(accountGroupId.HasValue) {
                            sql.Append(" and a.AccountGroupId={0}accountGroupId");
                            dbParameters.Add(db.CreateDbParameter("accountGroupId", accountGroupId.Value));
                        }
                        if(status.HasValue) {
                            sql.Append(" and a.status={0}status");
                            dbParameters.Add(db.CreateDbParameter("status", status.Value));
                        }
                        if(createTimeBegin.HasValue) {
                            sql.Append(@" and a.createTime >=to_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(@" and a.createTime <=to_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                        if(!string.IsNullOrEmpty(businessCode)) {
                            sql.Append(" and s.businessCode like {0}businessCode and u.businessCode like {0}businessCode ");
                            dbParameters.Add(db.CreateDbParameter("businessCode", "%" + businessCode + "%"));
                        }
                        #endregion
                    }
                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    conn.Open();
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    ErrorStrings.Export_Title_Credenceapplication_CompanyCode, ErrorStrings.Export_Title_Credenceapplication_CompanyName, ErrorStrings.Export_Validation_Company_Type,ErrorStrings.Export_Title_Credenceapplication_BusinessCode,
                                    ErrorStrings.Export_Validation_Company_ProvinceName, ErrorStrings.Export_Validation_Customeraccount_Code, ErrorStrings.Export_Title_Credenceapplication_AccountgroupName, 
                                    ErrorStrings.Export_Validation_Customeraccount_Accountbalance, ErrorStrings.Export_Validation_Customeraccount_ShippedproductValue, ErrorStrings.Export_Validation_Customeraccount_PendingAmount, 
                                    ErrorStrings.Export_Validation_Customeraccount_CustomerCredenceAmount,ErrorStrings.Export_Validation_Customeraccount_TempCreditTotalFee, ErrorStrings.Export_Validation_Customeraccount_AccountBalanceAmount,
                                    ErrorStrings.Export_Validation_Customeraccount_UnApproveAmountSum, ErrorStrings.Export_Validation_Customeraccount_LockedAmount, ErrorStrings.Export_Validation_Customeraccount_AccountAmount, 
                                    ErrorStrings.Export_Validation_Customeraccount_UseableAmount, ErrorStrings.Export_Title_AccountPeriod_Status, ErrorStrings.Export_Title_AccountPeriod_CreatorName, ErrorStrings.Export_Title_AccountPeriod_CreateTime, ErrorStrings.Export_Title_AccountPeriod_ModifierName, ErrorStrings.Export_Title_AccountPeriod_ModifyTime,"SIH授信（服务商）(B)"
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }

        /// <summary>
        /// 导出服务站在代理库账户明细
        /// </summary>
        public bool ExportAgencyCustomerAccount(int[] ids, string dealerCode, string dealerName, int? businessType, string agencyCode, string agencyName, DateTime? processDateBegin, DateTime? processDateEnd, out string fileName) {
            fileName = GetExportFilePath("服务站在中心库账户明细_" + ".xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.AppendFormat(@" Select r.Code,
                                               r.Name,
                                               g.Name,  
                                               a.Code,
                                               a.Name,
                                               d.ProcessDate,
                                               d.ChangeAmount,
                                               (select value from keyvalueitem where NAME = 'AccountPayment_BusinessType'and key=d.BusinessType) As BusinessType,
                                               d.SourceCode,
                                               d.Summary,
                                               d.BeforeChangeAmount,
                                               d.AfterChangeAmount,
                                               d.Debit,
                                               d.Credit
                                         From CustomerAccount c
                                         inner join CustomerAccountHisDetail d
                                            on c.Id = d.CustomerAccountId
                                         inner join AccountGroup g
                                            on c.AccountGroupId = g.Id
                                         inner join Dealer r
                                            on c.CustomerCompanyId = r.Id
                                         inner join Agency a
                                            on g.SalesCompanyId = a.Id
                                         where c.Status = 1 and g.Status = 1 and r.Status = 1 and a.Status = 1");
                    var dbParameters = new List<DbParameter>();
                    if(ids != null && ids.Length > 0) {
                        var idStr = string.Join(",", ids);
                        sql.Append(" and d.id in (" + idStr + ")");
                    } else {
                        if(!string.IsNullOrEmpty(dealerName)) {
                            sql.Append(" and r.name like {0}dealerName ");
                            dbParameters.Add(db.CreateDbParameter("companyName", "%" + dealerName + "%"));
                        }
                        if(!string.IsNullOrEmpty(dealerCode)) {
                            sql.Append(" and r.code like {0}dealerCode ");
                            dbParameters.Add(db.CreateDbParameter("dealerCode", "%" + dealerCode + "%"));
                        }
                        if(!string.IsNullOrEmpty(agencyCode)) {
                            sql.Append(" and a.code like {0}agencyCode ");
                            dbParameters.Add(db.CreateDbParameter("agencyCode", "%" + agencyCode + "%"));
                        }
                        if(!string.IsNullOrEmpty(agencyName)) {
                            sql.Append(" and a.name like {0}agencyName ");
                            dbParameters.Add(db.CreateDbParameter("agencyName", "%" + agencyName + "%"));
                        }
                        if(businessType.HasValue) {
                            sql.Append(" and d.businessType={0}businessType");
                            dbParameters.Add(db.CreateDbParameter("businessType", businessType.Value));
                        }
                        if(processDateBegin.HasValue) {
                            sql.Append(@" and d.processDate >=to_date({0}processDateBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = processDateBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("processDateBegin", tempTime.ToString("G")));
                        }
                        if(processDateEnd.HasValue) {
                            sql.Append(@" and d.processDate <=to_date({0}processDateEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = processDateEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("processDateEnd", tempTime.ToString("G")));
                        }
                    }
                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    conn.Open();
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                   ErrorStrings.Export_Title_Dealer_Code, ErrorStrings.Export_Title_Dealer_Name, ErrorStrings.Export_Title_CustomerAccount_Name, ErrorStrings.Export_Title_Agency_Code,
                                   ErrorStrings.Export_Title_Agency_Name, ErrorStrings.Export_Title_Agency_ProcessDate,ErrorStrings.Export_Title_AccountPay_ChangeAmount,  ErrorStrings.Export_Title_MarketingDepartment_BussinessType, ErrorStrings.Export_Title_AccountPay_Sourcecode, ErrorStrings.Export_Title_AccountPay_Summary, ErrorStrings.Export_Title_AccountPay_BeforeChangeAmount, ErrorStrings.Export_Title_AccountPay_AfterChangeAmount,ErrorStrings.Export_Title_AccountPay_Debit, ErrorStrings.Export_Title_AccountPay_Credit
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }
    }
}
