﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Globalization;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        /// <summary>
        /// 导出客户账户变更明细
        /// </summary>
        public bool ExportCustomerAccountHisDetail(int[] ids, int enterpriseType, int? accountGroupId, int salesCompanyId, string companyName, string companyCode, string sourceCode, int? businessType, DateTime? processDateBegin, DateTime? processDateEnd, out string fileName) {
            fileName = GetExportFilePath("客户账户变更明细_" + ".xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);
                using(var conn = db.CreateDbConnection()) {
                    var sql = new StringBuilder();
                    if(enterpriseType == (int)DcsCompanyType.分公司) {
                        sql.AppendFormat(@"Select d.Name,
                                              c.Code,
                                           c.Name,
                                           a.Changeamount,
                                           a.Processdate,
                                           (select value from keyvalueitem where NAME = 'AccountPayment_BusinessType'and key=a.Businesstype) As Businesstype,
                                           a.Sourcecode,
                                           a.Summary,
                                           a.BeforeChangeAmount,
                                           a.AfterChangeAmount,
                                           a.debit,            
                                           a.credit
                                      From Customeraccounthisdetail a
                                      Left Join Customeraccount b
                                        On a.Customeraccountid = b.Id
                                      Inner Join Company c
                                        On b.Customercompanyid = c.Id
                                      Inner Join AccountGroup d
                                        On b.accountgroupid=d.id And d.status<>99
                                where d.salesCompanyId={0} And b.status<>99  ", salesCompanyId);
                    } else {
                        //服务站使用账户明细查询时salesCompanyId当做customerCompanyId使用
                        sql.AppendFormat(@"Select c.Code,
                                           c.Name,
                                           d.Name,
                                           a.Changeamount,
                                           a.Processdate,
                                           (select value from keyvalueitem where NAME = 'AccountPayment_BusinessType'and key=a.Businesstype) As Businesstype,
                                           a.Sourcecode,
                                           a.Summary,
                                           a.BeforeChangeAmount,
                                           a.AfterChangeAmount,
                                           a.debit,            
                                           a.credit
                                      From Customeraccount b
                                      inner Join Customeraccounthisdetail a
                                        On a.Customeraccountid = b.Id
                                      Inner Join Company c
                                        On b.Customercompanyid = c.Id
                                      Inner Join AccountGroup d
                                        On b.accountgroupid=d.id And d.status<>99
                                where b.CustomerCompanyId={0} And b.status<>99  ", salesCompanyId);
                    }
                    var dbParameters = new List<DbParameter>();
                    #region 条件过滤
                    if(ids != null && ids.Length > 0) {
                        sql.Append(" and a.id in (");
                        for(var i = 0; i < ids.Length; i++) {
                            if(ids.Length == i + 1) {
                                sql.Append(ids[i].ToString(CultureInfo.InvariantCulture));
                            } else {
                                sql.Append(ids[i].ToString(CultureInfo.InvariantCulture) + ",");
                            }
                        }
                        sql.Append(")");
                    } else {
                        if(accountGroupId.HasValue) {
                            sql.Append(@" and b.accountGroupId = {0}accountGroupId ");
                            dbParameters.Add(db.CreateDbParameter("accountGroupId", accountGroupId.Value));
                        }
                        if(!String.IsNullOrEmpty(companyCode)) {
                            sql.Append(@" and Upper(c.Code) like Upper({0}companyCode) ");
                            dbParameters.Add(db.CreateDbParameter("companyCode", "%" + companyCode + "%"));
                        }
                        if(!String.IsNullOrEmpty(companyName)) {
                            sql.Append(@" and c.Name like {0}companyName ");
                            dbParameters.Add(db.CreateDbParameter("companyName", "%" + companyName + "%"));
                        }
                        if(!String.IsNullOrEmpty(sourceCode)) {
                            sql.Append(@" and Upper(a.sourceCode) like Upper({0}sourceCode) ");
                            dbParameters.Add(db.CreateDbParameter("sourceCode", "%" + sourceCode + "%"));
                        }
                        if(businessType.HasValue) {
                            sql.Append(@" and a.businessType = {0}businessType ");
                            dbParameters.Add(db.CreateDbParameter("businessType", businessType.Value));
                        }
                        if(processDateBegin.HasValue) {
                            sql.Append(@" and a.ProcessDate >=to_date({0}processDateBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = processDateBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("processDateBegin", tempTime.ToString("G")));
                        }
                        if(processDateEnd.HasValue) {
                            sql.Append(@" and a.ProcessDate <=to_date({0}processDateEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = processDateEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("processDateEnd", tempTime.ToString("G")));
                        }
                    }
                    #endregion

                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    conn.Open();
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        if(enterpriseType == (int)DcsCompanyType.分公司) {
                            excelExport.ExportByRow(index => {
                                if(index == 0) {
                                    return new object[] {
                                        ErrorStrings.Export_Title_CustomerAccount_Name, ErrorStrings.Export_Title_Credenceapplication_CompanyCode, ErrorStrings.Export_Title_Credenceapplication_CompanyName, ErrorStrings.Export_Title_AccountPay_ChangeAmount, ErrorStrings.Export_Title_AccountPay_Processdate, ErrorStrings.Export_Title_MarketingDepartment_BussinessType, ErrorStrings.Export_Title_AccountPay_Sourcecode, ErrorStrings.Export_Title_AccountPay_Summary, ErrorStrings.Export_Title_AccountPay_BeforeChangeAmount, ErrorStrings.Export_Title_AccountPay_AfterChangeAmount, ErrorStrings.Export_Title_AccountPay_Debit, ErrorStrings.Export_Title_AccountPay_Credit
                                    };
                                }
                                if(reader.Read()) {
                                    var values = new object[reader.FieldCount];
                                    var num = reader.GetValues(values);
                                    if(num != reader.FieldCount) {
                                        throw new Exception(ErrorStrings.Export_Validation_DataError);
                                    }
                                    return values;
                                }
                                return null;
                            });
                        } else {
                            //服务站使用账户明细查询节点时导出字段
                            excelExport.ExportByRow(index => {
                                if(index == 0) {
                                    return new object[] {
                                        ErrorStrings.Export_Title_Credenceapplication_CompanyCode, ErrorStrings.Export_Title_Credenceapplication_CompanyName, ErrorStrings.Export_Title_CustomerAccount_Name, ErrorStrings.Export_Title_AccountPay_ChangeAmount, ErrorStrings.Export_Title_AccountPay_Processdate, ErrorStrings.Export_Title_MarketingDepartment_BussinessType, ErrorStrings.Export_Title_AccountPay_Sourcecode, ErrorStrings.Export_Title_AccountPay_Summary, ErrorStrings.Export_Title_AccountPay_BeforeChangeAmount, ErrorStrings.Export_Title_AccountPay_AfterChangeAmount, ErrorStrings.Export_Title_AccountPay_Debit, ErrorStrings.Export_Title_AccountPay_Credit
                                    };
                                }
                                if(reader.Read()) {
                                    var values = new object[reader.FieldCount];
                                    var num = reader.GetValues(values);
                                    if(num != reader.FieldCount) {
                                        throw new Exception(ErrorStrings.Export_Validation_DataError);
                                    }
                                    return values;
                                }
                                return null;
                            });
                        }
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }

        /// <summary>
        /// 导出客户账户变更明细
        /// </summary>
        public bool ExportCustomerAccountForHisDetail(int[] ids, int enterpriseType, int? accountGroupId, int salesCompanyId, string companyName, string companyCode, string sourceCode, int? businessType, DateTime? processDateBegin, DateTime? processDateEnd, string businessCode, DateTime? beginInvoiceDate, DateTime? endInvoiceDateout,int? serialType, out string fileName)
        {
            fileName = GetExportFilePath("客户账户变更明细_" + ".xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);
                using(var conn = db.CreateDbConnection()) {
                    var sql = "";
                    if(enterpriseType == (int)DcsCompanyType.分公司) {
                        sql = string.Format(@"Select d.Name  as pname,
                                              c.Code,
                                           c.Name,
                                           a.Changeamount,
                                             a.InvoiceDate,
                                           a.Processdate,
                                           (select value from keyvalueitem where NAME = 'AccountPayment_BusinessType'and key=a.Businesstype) As Businesstype,
                                           (select value from keyvalueitem where NAME = 'Serial_Type'and key=a.SerialType) As SerialType,
                                           a.Sourcecode,
                                           a.Summary,
                                           a.BeforeChangeAmount,
                                           a.AfterChangeAmount,
                                           a.debit,            
                                           a.credit,
                                           case c.type
                                                when 2 then
                                               ( select businessCode from DealerServiceInfo where PartsSalesCategoryId= u.PartsSalesCategoryId  and DealerId=b.CustomerCompanyId and status=1)
                                                when 7 then
                                                 ( select businessCode from DealerServiceInfo where PartsSalesCategoryId= u.PartsSalesCategoryId  and DealerId=b.CustomerCompanyId and status=1)
                                                when 3 then
                                                u.businessCode
                                                end as businessCode,
                                            a.CreatorName
                                      From Customeraccounthisdetail a
                                      INNER Join Customeraccount b
                                        On a.Customeraccountid = b.Id
                                      Inner Join Company c
                                        On b.Customercompanyid = c.Id
                                      Inner Join AccountGroup d
                                        On b.accountgroupid=d.id And d.status<>99
                                      Left Join SalesUnit u
                                      On b.AccountGroupId = u.AccountGroupId and d.SalesCompanyId = u.OwnerCompanyId
                               where d.SalesCompanyId={0} and b.status<>99   AND c.status<>99  ", salesCompanyId);
                    } else {
                        //服务站使用账户明细查询时salesCompanyId当做customerCompanyId使用
                        sql = string.Format(@"Select    d.Name as pname, 
                                            c.Code,
                                           c.Name,
                                           a.Changeamount,
                                            a.InvoiceDate,
                                           a.Processdate,
                                           (select value from keyvalueitem where NAME = 'AccountPayment_BusinessType'and key=a.Businesstype) As Businesstype,
                                           (select value from keyvalueitem where NAME = 'Serial_Type'and key=a.SerialType) As SerialType,
                                           a.Sourcecode,
                                           a.Summary,
                                           a.BeforeChangeAmount,
                                           a.AfterChangeAmount,
                                           a.debit,            
                                           a.credit,
                                             case c.type
                                                when 2 then
                                               ( select businessCode from DealerServiceInfo where PartsSalesCategoryId= u.PartsSalesCategoryId  and DealerId=b.CustomerCompanyId and status=1)
                                                when 7 then
                                                 ( select businessCode from DealerServiceInfo where PartsSalesCategoryId= u.PartsSalesCategoryId  and DealerId=b.CustomerCompanyId and status=1)
                                                when 3 then
                                                u.businessCode
                                                end as businessCode,
                                            a.CreatorName
                                      From Customeraccount b
                                      inner Join Customeraccounthisdetail a
                                        On a.Customeraccountid = b.Id
                                      Inner Join Company c
                                        On b.Customercompanyid = c.Id
                                      Inner Join AccountGroup d
                                        On b.accountgroupid=d.id And d.status<>99
                                      Left Join SalesUnit u
                                      On b.AccountGroupId = u.AccountGroupId and b.CustomerCompanyId = u.OwnerCompanyId
                                where b.CustomerCompanyId={0} and  b.status<>99   AND c.status<>99 ", salesCompanyId);
                    }
                    var dbParameters = new List<DbParameter>();
                    #region 条件过滤
                    if(ids != null && ids.Length > 0) {
                        //sql += (" and a.id in (");
                        //for(var i = 0; i < ids.Length; i++) {
                        //    if(ids.Length == i + 1) {
                        //        sql += ("{0}" + ids[i].ToString(CultureInfo.InvariantCulture));
                        //        dbParameters.Add(db.CreateDbParameter(ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                        //    } else {
                        //        sql += ("{0}" + ids[i].ToString(CultureInfo.InvariantCulture) + ",");
                        //        dbParameters.Add(db.CreateDbParameter(ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                        //    }
                        //}
                        //sql += (")");
                        sql += (@" and a.id in (");
                        for(var i = 0; i < ids.Length; i++) {
                            if(ids.Length == i + 1) {
                                sql += (@"{0}id" + ids[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            } else {
                                sql += (@"{0}id" + ids[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                        }
                        sql += (@")");
                    } else {
                        if(accountGroupId.HasValue) {
                            sql += (@" and b.accountGroupId = {0}accountGroupId ");
                            dbParameters.Add(db.CreateDbParameter("accountGroupId", accountGroupId.Value));
                        }
                        if(!String.IsNullOrEmpty(companyCode)) {
                            sql += (@" and Upper(c.Code) like Upper({0}companyCode) ");
                            dbParameters.Add(db.CreateDbParameter("companyCode", "%" + companyCode + "%"));
                        }
                        if(!String.IsNullOrEmpty(companyName)) {
                            sql += (@" and c.Name like {0}companyName ");
                            dbParameters.Add(db.CreateDbParameter("companyName", "%" + companyName + "%"));
                        }
                        if(!String.IsNullOrEmpty(sourceCode)) {
                            sql += (@" and Upper(a.sourceCode) like Upper({0}sourceCode) ");
                            dbParameters.Add(db.CreateDbParameter("sourceCode", "%" + sourceCode + "%"));
                        }
                        if(businessType.HasValue) {
                            sql += (@" and a.businessType = {0}businessType ");
                            dbParameters.Add(db.CreateDbParameter("businessType", businessType.Value));
                        }
                        if (serialType.HasValue)
                        {
                            sql += (@" and a.serialType = {0}serialType ");
                            dbParameters.Add(db.CreateDbParameter("serialType", serialType.Value));
                        }
                        if(processDateBegin.HasValue) {
                            sql += (@" and a.ProcessDate >=to_date({0}processDateBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = processDateBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("processDateBegin", tempTime.ToString("G")));
                        }
                        if(processDateEnd.HasValue) {
                            sql += (@" and a.ProcessDate <=to_date({0}processDateEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = processDateEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("processDateEnd", tempTime.ToString("G")));
                        }
                        if (beginInvoiceDate.HasValue)
                        {
                            sql += (@" and a.InvoiceDate >=to_date({0}beginInvoiceDate,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = beginInvoiceDate.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("beginInvoiceDate", tempTime.ToString("G")));
                        }
                        if (endInvoiceDateout.HasValue)
                        {
                            sql += (@" and a.InvoiceDate <=to_date({0}endInvoiceDateout,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = endInvoiceDateout.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("endInvoiceDateout", tempTime.ToString("G")));
                        }
                        if(!string.IsNullOrEmpty(businessCode)) {
                            sql = (@"select * from (" + sql + ") a where ");
                            sql += ("  businessCode like {0}businessCode ");
                            dbParameters.Add(db.CreateDbParameter("businessCode", "%" + businessCode + "%"));
                        }
                    }
                    #endregion

                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    conn.Open();
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        if(enterpriseType == (int)DcsCompanyType.分公司) {
                            excelExport.ExportByRow(index => {
                                if(index == 0) {
                                    return new object[] {
                                        ErrorStrings.Export_Title_CustomerAccount_Name, ErrorStrings.Export_Title_Credenceapplication_CompanyCode, ErrorStrings.Export_Title_Credenceapplication_CompanyName, ErrorStrings.Export_Title_AccountPay_ChangeAmount,ErrorStrings.Export_Title_Customeraccount_InvoiceDate, ErrorStrings.Export_Title_AccountPay_Processdate, ErrorStrings.Export_Title_MarketingDepartment_BussinessType,"变更类型", ErrorStrings.Export_Title_AccountPay_Sourcecode, ErrorStrings.Export_Title_AccountPay_Summary, ErrorStrings.Export_Title_AccountPay_BeforeChangeAmount, ErrorStrings.Export_Title_AccountPay_AfterChangeAmount, ErrorStrings.Export_Title_AccountPay_Debit, ErrorStrings.Export_Title_AccountPay_Credit,ErrorStrings.Export_Title_Credenceapplication_BusinessCode,ErrorStrings.Export_Title_AccountPeriod_CreatorName
                                    };
                                }
                                if(reader.Read()) {
                                    var values = new object[reader.FieldCount];
                                    var num = reader.GetValues(values);
                                    if(num != reader.FieldCount) {
                                        throw new Exception(ErrorStrings.Export_Validation_DataError);
                                    }
                                    return values;
                                }
                                return null;
                            });
                        } else {
                            //服务站使用账户明细查询节点时导出字段
                            excelExport.ExportByRow(index => {
                                if(index == 0) {
                                    return new object[] {
                                        ErrorStrings.Export_Title_Credenceapplication_CompanyCode, ErrorStrings.Export_Title_Credenceapplication_CompanyName, ErrorStrings.Export_Title_CustomerAccount_Name, ErrorStrings.Export_Title_AccountPay_ChangeAmount, ErrorStrings.Export_Title_AccountPay_Processdate, ErrorStrings.Export_Title_MarketingDepartment_BussinessType, ErrorStrings.Export_Title_AccountPay_Sourcecode, ErrorStrings.Export_Title_AccountPay_Summary, ErrorStrings.Export_Title_AccountPay_BeforeChangeAmount, ErrorStrings.Export_Title_AccountPay_AfterChangeAmount, ErrorStrings.Export_Title_AccountPay_Debit, ErrorStrings.Export_Title_AccountPay_Credit,ErrorStrings.Export_Title_Credenceapplication_BusinessCode,ErrorStrings.Export_Title_AccountPeriod_CreatorName
                                    };
                                }
                                if(reader.Read()) {
                                    var values = new object[reader.FieldCount];
                                    var num = reader.GetValues(values);
                                    if(num != reader.FieldCount) {
                                        throw new Exception(ErrorStrings.Export_Validation_DataError);
                                    }
                                    return values;
                                }
                                return null;
                            });
                        }
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }
    }
}
