﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Globalization;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {

        /// <summary>
        /// 导出客户账户历史
        /// </summary>
        /// <returns></returns>
        public bool ExportCustomerAccountHistory(int[] ids, int? currentCompanyType, int? accountGroupId, string companyCode, string companyName, int? companyType, DateTime? theDate, out string fileName) {
            fileName = GetExportFilePath("导出客户账户历史.xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);
                var userInfo = Utils.GetCurrentUserInfo();
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.Append(@" SELECT 
                                 a.Code,
                                 a.Name,
                                 (select value from keyvalueitem where NAME = 'AccountPayment_BusinessType'and key=a.Type) As Type,
                                 a.AccountGroupCode,
                                 a.AccountGroupName,
                                 a.AccountBalance,
                                 a.ShippedProductValue,
                                 a.PendingAmount,
                                 a.CustomerCredenceAmount,
                                 a.FlBalance,
                                 a.WshBalance,
                                 a.SdBalance,
                                 a.YeBalance,
                                 a.KyBalance,
                                 a.TheDate
                                 FROM   CustomerAccountHistory a
                                 INNER JOIN AccountGroup b ON a.AccountGroupId = b.Id where 1=1 ");
                    var dbParameters = new List<DbParameter>();
                    if(ids != null && ids.Length > 0) {
                        sql.Append(" and a.id in (");
                        for(var i = 0; i < ids.Length; i++) {
                            if(ids.Length == i + 1) {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            } else {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                        }
                        sql.Append(")");
                    } else {
                        if(currentCompanyType.HasValue) {
                            if(currentCompanyType == (int)DcsCompanyType.分公司) {
                                sql.Append(" and b.SalesCompanyId = {0}salesCompanyId ");
                                dbParameters.Add(db.CreateDbParameter("salesCompanyId", userInfo.EnterpriseId));
                            }
                            if(currentCompanyType == (int)DcsCompanyType.代理库 || currentCompanyType == (int)DcsCompanyType.服务站兼代理库) {
                                sql.Append(" and b.SalesCompanyId = {0}salesCompanyId ");
                                dbParameters.Add(db.CreateDbParameter("salesCompanyId", userInfo.EnterpriseId));
                                sql.Append(" and a.CustomerCompanyId = {0}customerCompanyId ");
                                dbParameters.Add(db.CreateDbParameter("customerCompanyId", userInfo.EnterpriseId));
                            }
                            if(currentCompanyType == (int)DcsCompanyType.服务站) {
                                sql.Append("and a.CustomerCompanyId = {0}customerCompanyId ");
                                dbParameters.Add(db.CreateDbParameter("customerCompanyId", userInfo.EnterpriseId));
                            }
                        }
                        if(accountGroupId.HasValue) {
                            sql.Append(@" and a.AccountGroupId = {0}AccountGroupId ");
                            dbParameters.Add(db.CreateDbParameter("AccountGroupId", accountGroupId.Value));
                        }
                        if(!string.IsNullOrEmpty(companyCode)) {
                            sql.Append(" and LOWER(a.Code) like {0}companyCode ");
                            dbParameters.Add(db.CreateDbParameter("companyCode", "%" + companyCode.ToLower() + "%"));
                        }
                        if(!string.IsNullOrEmpty(companyName)) {
                            sql.Append(" and LOWER(a.Name) like {0}companyName ");
                            dbParameters.Add(db.CreateDbParameter("companyName", "%" + companyName.ToLower() + "%"));
                        }
                        if(companyType.HasValue) {
                            sql.Append(" and a.type = {0}companyType ");
                            dbParameters.Add(db.CreateDbParameter("companyType", companyType.Value));
                        }
                        if(theDate.HasValue) {
                            sql.Append(@" and a.theDate =To_date({0}theDate,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = theDate.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day);
                            dbParameters.Add(db.CreateDbParameter("theDate", tempTime.ToString("d")));
                        }
                    }
                    var sqlStr = sql.ToString();
                    if(sqlStr.IndexOf(" where 1=1  and ") > 0)
                        sqlStr = sqlStr.Replace(" where 1=1  and ", " where ");
                    else
                        sqlStr = sqlStr.Replace(" where 1=1 ", "");

                    var command = db.CreateDbCommand(string.Format(sqlStr, db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    ErrorStrings.Export_Title_Credenceapplication_CompanyCode, 
                                    ErrorStrings.Export_Title_Credenceapplication_CompanyName, 
                                    ErrorStrings.Export_Validation_Company_Type, 
                                    ErrorStrings.Export_Title_Customeraccount_AccountGroupCode, 
                                    ErrorStrings.Export_Title_Credenceapplication_AccountGroupNameNew, 
                                    ErrorStrings.Export_Validation_Customeraccount_Accountbalance,
                                    ErrorStrings.Export_Validation_Customeraccount_ShippedproductValue,
                                    ErrorStrings.Export_Validation_Customeraccount_PendingAmount, 
                                    ErrorStrings.Export_Validation_Customeraccount_CustomerCredenceAmount, 
                                    ErrorStrings.Export_Validation_Customeraccount_AccountBalanceAmount, 
                                    ErrorStrings.Export_Validation_Customeraccount_UnApproveAmountSum, 
                                    ErrorStrings.Export_Validation_Customeraccount_LockedAmount,
                                    ErrorStrings.Export_Validation_Customeraccount_AccountAmount, 
                                    ErrorStrings.Export_Validation_Customeraccount_UseableAmount,
                                    ErrorStrings.Export_Title_Customeraccount_TheDate
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();

                }
                return true;
            } catch(Exception) {
                return false;
            }
        }


        /// <summary>
        /// 导出服务站在代理库账户余额历史记录
        /// </summary>
        /// <returns></returns>
        public bool ExportCustomerAccountHistoryForAgency(int[] ids, int? currentCompanyType, int? accountGroupId, string companyCode, string companyName, int? companyType, DateTime? theDate, out string fileName) {
            fileName = GetExportFilePath("导出客户账户历史.xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);
                var userInfo = Utils.GetCurrentUserInfo();
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.Append(@" SELECT 
                                 Extent1.Code,
                                 Extent1.Name,
                                 Extent4.Code as AgencyCode,
                                 Extent4.Name as AgencyName,
                                 cc.name As Type,
                                 Extent1.AccountGroupCode,
                                 Extent1.AccountGroupName,
                                 Extent1.AccountBalance,
                                 Extent1.ShippedProductValue,
                                 Extent1.PendingAmount,
                                 Extent1.CustomerCredenceAmount,
                                 Extent1.FlBalance,
                                 Extent1.WshBalance,
                                 Extent1.SdBalance,
                                 Extent1.YeBalance,
                                 Extent1.KyBalance,
                                 Extent1.TheDate
                                 FROM   CustomerAccountHistory Extent1
                                INNER JOIN AccountGroup Extent2 ON Extent1.AccountGroupId = Extent2.Id
                                INNER JOIN AgencyAffiBranch Extent3 ON Extent2.SalesCompanyId = Extent3.AgencyId
                                INNER JOIN Agency Extent4 ON Extent3.AgencyId = Extent4.Id
                                 join DealerServiceInfo ds
                                    on Extent1.CustomerCompanyId = ds.DealerId
                                   join ChannelCapability cc
                                    on ds.channelcapabilityid = cc.id
                                   and cc.status = 1
                                 Where Extent3.BranchId=" + userInfo.EnterpriseId);
                    var dbParameters = new List<DbParameter>();
                    if(ids != null && ids.Length > 0) {
                        sql.Append(" and Extent1.id in (");
                        for(var i = 0; i < ids.Length; i++) {
                            if(ids.Length == i + 1) {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            } else {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                        }
                        sql.Append(")");
                    } else {
                        if(currentCompanyType.HasValue) {
                            if(currentCompanyType == (int)DcsCompanyType.代理库) {
                                sql.Append("and Extent2.SalesCompanyId = {0}salesCompanyId ");
                                dbParameters.Add(db.CreateDbParameter("salesCompanyId", userInfo.EnterpriseId));
                            }
                        }
                        if(accountGroupId.HasValue) {
                            sql.Append(@" and Extent1.AccountGroupId = {0}AccountId ");
                            dbParameters.Add(db.CreateDbParameter("AccountId", accountGroupId.Value));
                        }
                        if(!string.IsNullOrEmpty(companyCode)) {
                            sql.Append("and LOWER(Extent1.Code) like {0}companyCode ");
                            dbParameters.Add(db.CreateDbParameter("companyCode", "%" + companyCode.ToLower() + "%"));
                        }
                        if(!string.IsNullOrEmpty(companyName)) {
                            sql.Append("and LOWER(Extent1.Name) like {0}companyName ");
                            dbParameters.Add(db.CreateDbParameter("companyName", "%" + companyName.ToLower() + "%"));
                        }
                        if(companyType.HasValue) {
                            sql.Append("and Extent1.type = {0}companyType ");
                            dbParameters.Add(db.CreateDbParameter("companyType", companyType.Value));
                        }
                        if(theDate.HasValue) {
                            sql.Append(@" and Extent1.theDate >=To_date({0}theBegionDate,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = theDate.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day);
                            dbParameters.Add(db.CreateDbParameter("theBegionDate", tempTime.ToString("d")));

                            sql.Append(@" and Extent1.theDate <To_date({0}theEndDate,'yyyy-mm-dd hh24:mi:ss')");
                            var tempendTime = new DateTime(tempValue.AddMonths(1).Year, tempValue.AddMonths(1).Month, tempValue.AddMonths(1).Day);
                            dbParameters.Add(db.CreateDbParameter("theEndDate", tempendTime.ToString("d")));
                        }
                    }
                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    ErrorStrings.Export_Title_Credenceapplication_CompanyCode, 
                                    ErrorStrings.Export_Title_Credenceapplication_CompanyName, 
                                    ErrorStrings.Export_Title_Agency_Code,
                                    ErrorStrings.Export_Title_Agency_Name,
                                    ErrorStrings.Export_Validation_Company_Type, 
                                    ErrorStrings.Export_Title_Customeraccount_AccountGroupCode, 
                                    ErrorStrings.Export_Title_Credenceapplication_AccountGroupNameNew, 
                                    ErrorStrings.Export_Validation_Customeraccount_Accountbalance,
                                    ErrorStrings.Export_Validation_Customeraccount_ShippedproductValue,
                                    ErrorStrings.Export_Validation_Customeraccount_PendingAmount, 
                                    ErrorStrings.Export_Validation_Customeraccount_CustomerCredenceAmount, 
                                    ErrorStrings.Export_Validation_Customeraccount_AccountBalanceAmount, 
                                    ErrorStrings.Export_Validation_Customeraccount_UnApproveAmountSum, 
                                    ErrorStrings.Export_Validation_Customeraccount_LockedAmount,
                                    ErrorStrings.Export_Validation_Customeraccount_AccountAmount, 
                                    ErrorStrings.Export_Validation_Customeraccount_UseableAmount,
                                    ErrorStrings.Export_Title_Customeraccount_TheDate
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();

                }
                return true;
            } catch(Exception) {
                return false;
            }
        }

    }
}
