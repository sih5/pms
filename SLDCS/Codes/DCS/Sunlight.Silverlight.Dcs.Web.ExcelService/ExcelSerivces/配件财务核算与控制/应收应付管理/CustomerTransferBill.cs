﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using System.Data.Common;
using System.Web;
using System.Globalization;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        /// <summary>
        /// 导出客户转账单
        /// </summary>
        public bool ExportCustomerTransferBill(int[] ids, int salesCompanyId, string outboundCustomerCompanyName, string outboundCustomerCompanyCode, string inboundCustomerCompanyName, string inboundCustomerCompanyCode, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, DateTime? invoiceDateBegin, DateTime? invoiceDateEnd, int? businessType,string code, out string fileName)
        {
            fileName = GetExportFilePath("客户转账单_" + ".xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {
                    var sql = new StringBuilder();
                    sql.AppendFormat(@"Select a.Code,
                                               d.name,
                                               b.Code,
                                               b.Name,
                                               e.name,
                                               c.Code,
                                               c.Name,
                                               a.Amount,
                                               a.Summary,
                                               a.Credentialdocument,
                                               a.Operator,
                                               (select value from keyvalueitem where NAME = 'DealerPartsInventoryBill_Status'and key=a.Status) As Status,
                                               (select value from keyvalueitem where NAME = 'CustomerTransferBill_BusinessType'and key=a.BusinessType) As BusinessType,
                                               a.invoiceDate,
                                               a.Remark,
                                               a.Creatorname,
                                               a.Createtime,
                                               a.Modifiername,
                                               a.Modifytime
                                          From Customertransferbill a
                                          inner Join AccountGroup d
                                            On a.OutboundAccountGroupId = d.Id
                                          inner Join AccountGroup e
                                            On a.InboundAccountGroupId = e.Id
                                          Left Join Company b
                                            On a.Outboundcustomercompanyid = b.Id
                                          Left Join Company c
                                            On a.Inboundcustomercompanyid = c.Id
                                where a.salesCompanyId={0} ", salesCompanyId);
                    var dbParameters = new List<DbParameter>();
                    #region 条件过滤
                    if(ids != null && ids.Length > 0) {
                        sql.Append(" and a.id in (");
                        for(var i = 0; i < ids.Length; i++) {
                            if(ids.Length == i + 1) {
                                sql.Append("{0}" + ids[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter(ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            } else {
                                sql.Append("{0}" + ids[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter(ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                        }
                        sql.Append(")");
                    } else {
                        if(!String.IsNullOrEmpty(code)) {
                            sql.Append(@" and Upper(a.Code) like Upper({0}code) ");
                            dbParameters.Add(db.CreateDbParameter("code", "%" + code + "%"));
                        }
                        if(!String.IsNullOrEmpty(outboundCustomerCompanyCode)) {
                            sql.Append(@" and Upper(b.Code) like Upper({0}outboundCustomerCompanyCode) ");
                            dbParameters.Add(db.CreateDbParameter("outboundCustomerCompanyCode", "%" + outboundCustomerCompanyCode + "%"));
                        }
                        if(!String.IsNullOrEmpty(outboundCustomerCompanyName)) {
                            sql.Append(@" and b.Name like {0}outboundCustomerCompanyName ");
                            dbParameters.Add(db.CreateDbParameter("outboundCustomerCompanyName", "%" + outboundCustomerCompanyName + "%"));
                        }
                        if(!String.IsNullOrEmpty(inboundCustomerCompanyCode)) {
                            sql.Append(@" and Upper(c.Code) like Upper({0}inboundCustomerCompanyCode) ");
                            dbParameters.Add(db.CreateDbParameter("inboundCustomerCompanyCode", "%" + inboundCustomerCompanyCode + "%"));
                        }
                        if(!String.IsNullOrEmpty(inboundCustomerCompanyName)) {
                            sql.Append(@" and c.Name like {0}inboundCustomerCompanyName ");
                            dbParameters.Add(db.CreateDbParameter("inboundCustomerCompanyName", "%" + inboundCustomerCompanyName + "%"));
                        }
                        if(status.HasValue) {
                            sql.Append(@" and a.status = {0}status ");
                            dbParameters.Add(db.CreateDbParameter("status", status.Value));
                        }
                        if(createTimeBegin.HasValue) {
                            sql.Append(@" and a.createTime >=to_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(@" and a.createTime <=to_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                        if (invoiceDateBegin.HasValue)
                        {
                            sql.Append(@" and a.createTime >=to_date({0}invoiceDateBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = invoiceDateBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("invoiceDateBegin", tempTime.ToString("G")));
                        }
                        if (invoiceDateEnd.HasValue)
                        {
                            sql.Append(@" and a.createTime <=to_date({0}invoiceDateEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = invoiceDateEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("invoiceDateEnd", tempTime.ToString("G")));
                        }
                        if(businessType.HasValue) {
                            sql.Append(@" and a.businessType = {0}businessType ");
                            dbParameters.Add(db.CreateDbParameter("businessType", businessType.Value));
                        }
                    }
                    #endregion

                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    conn.Open();
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                ErrorStrings.Export_Title_PackingPropertyApp_Code,ErrorStrings.Export_Title_Customertransferbill_TransferAccountGroup,ErrorStrings.Export_Title_Customertransferbill_TransferCustomerCode,
                                ErrorStrings.Export_Title_Customertransferbill_TransferCustomerName,ErrorStrings.Export_Title_Customertransferbill_TransferToAccountGroup,ErrorStrings.Export_Title_Customertransferbill_TransferToCustomerCode,
                                ErrorStrings.Export_Title_Customertransferbill_TransferToCustomerName,ErrorStrings.Export_Title_Customertransferbill_Amount,ErrorStrings.Export_Title_Customertransferbill_Summary,ErrorStrings.Export_Title_Customertransferbill_CredentialDocument,ErrorStrings.Export_Title_Customertransferbill_Operator,ErrorStrings.Export_Title_AccountPeriod_Status,ErrorStrings.Export_Title_MarketingDepartment_BussinessType,ErrorStrings.Export_Title_Customeraccount_InvoiceDate,ErrorStrings.Export_Title_PartsBranch_Remark,ErrorStrings.Export_Title_AccountPeriod_CreatorName,ErrorStrings.Export_Title_AccountPeriod_CreateTime,ErrorStrings.Export_Title_AccountPeriod_ModifierName,ErrorStrings.Export_Title_AccountPeriod_ModifyTime
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }
        /// <summary>
        /// 导入客户转账单
        /// </summary>
        public bool ImportCustomerTransferBill(string fileName, out int excelImportNum, out List<CustomerTransferBillExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<CustomerTransferBillExtend>();
            var allList = new List<CustomerTransferBillExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("CustomerTransferBill", out notNullableFields, out fieldLenght);

                List<string> notNullableFieldsCustomerCompany;
                Dictionary<string, int> fieldLenghtCustomerCompany;
                db.GetTableSchema("Company", out notNullableFieldsCustomerCompany, out fieldLenghtCustomerCompany);

                List<string> notNullableFieldsAccountGroup;
                Dictionary<string, int> fieldLenghtAccountGroup;
                db.GetTableSchema("AccountGroup", out notNullableFieldsAccountGroup, out fieldLenghtAccountGroup);
                List<object> excelColumns;
                List<CustomerTransferBillExtend> rightList;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_MarketingDepartment_BussinessType, "BusinessType");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Customertransferbill_TransferAccountGroup, "OutboundAccountGroupName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Customertransferbill_TransferCustomerCode, "OutboundCustomerCompanyCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Customertransferbill_TransferCustomerName, "OutboundCustomerCompanyName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Customertransferbill_TransferToAccountGroup, "InboundAccountGroupName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Customertransferbill_TransferToCustomerCode, "InboundCustomerCompanyCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Customertransferbill_TransferToCustomerName, "InboundCustomerCompanyName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Customertransferbill_Amount, "Amount");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Customertransferbill_Summary, "Summary");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Customeraccount_InvoiceDate, "InvoiceDate");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Customertransferbill_Operator, "Operator");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Remark, "Remark");

                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;

                    #region 获取对应枚举
                    var keyValuePairs = new[] {
                            new KeyValuePair<string, string>("BusinessType", "CustomerTransferBill_BusinessType")
                           
                        };
                    tempExcelOperator.ImportHelper.LoadEnumFromDb(null, null, keyValuePairs);
                    #endregion
                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var tempImportObj = new CustomerTransferBillExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        #region 根据导入的字段给对应的实体属性赋值
                        tempImportObj.BusinessTypeStr = newRow["BusinessType"];
                        tempImportObj.OutboundAccountGroupName = newRow["OutboundAccountGroupName"];
                        tempImportObj.OutboundCustomerCompanyCode = newRow["OutboundCustomerCompanyCode"];
                        tempImportObj.OutboundCustomerCompanyName = newRow["OutboundCustomerCompanyName"];
                        tempImportObj.InboundAccountGroupName = newRow["InboundAccountGroupName"];
                        tempImportObj.InboundCustomerCompanyCode = newRow["InboundCustomerCompanyCode"];
                        tempImportObj.InboundCustomerCompanyName = newRow["InboundCustomerCompanyName"];
                        tempImportObj.AmountStr = newRow["Amount"];
                        tempImportObj.Summary = newRow["Summary"];
                        tempImportObj.InvoiceDateStr = newRow["InvoiceDate"];
                        tempImportObj.Operator = newRow["Operator"];
                        tempImportObj.Remark = newRow["Remark"];
                        #endregion
                        var tempErrorMessage = new List<string>();
                        #region 导入的数据基本检查
                        //业务类型检查
                        var fieldIndex = notNullableFields.IndexOf("BusinessType".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.BusinessTypeStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_MarketingDepartment_BussinessTypeIsNull);
                        } else {
                            var tempEnumValue = tempExcelOperator.ImportHelper.GetEnumValue("BusinessType", tempImportObj.BusinessTypeStr);
                            if(!tempEnumValue.HasValue) {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_MarketingDepartment_BussinessTypeValueError);
                            } else {
                                tempImportObj.BusinessType = tempEnumValue.Value;
                            }
                        }

                        //转出账户组检查
                        fieldIndex = notNullableFields.IndexOf("OutboundAccountGroupName".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.OutboundAccountGroupName)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Customertransferbill_TransferAccountGroupIsNull);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.OutboundAccountGroupName) > fieldLenghtAccountGroup["Name".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Customertransferbill_TransferAccountGroupIsLong);
                        }
                        //转出客户编号检查
                        fieldIndex = notNullableFields.IndexOf("OutboundCustomerCompanyCode".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.OutboundCustomerCompanyCode)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Customertransferbill_TransferCustomerCodeIsNull);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.OutboundCustomerCompanyCode) > fieldLenghtCustomerCompany["Code".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Customertransferbill_TransferCustomerCodeIsLong);
                        }
                        ////转出客户名称检查
                        //fieldIndex = notNullableFields.IndexOf("OutboundCustomerCompanyName".ToUpper());
                        //if(string.IsNullOrEmpty(tempImportObj.OutboundCustomerCompanyName)) {
                        //    if(fieldIndex > -1)
                        //        tempErrorMessage.Add("转出客户名称不能为空");
                        //} else {
                        //    if(Encoding.Default.GetByteCount(tempImportObj.OutboundCustomerCompanyName) > fieldLenghtCustomerCompany["Name".ToUpper()])
                        //        tempErrorMessage.Add("转出客户名称过长");
                        //}
                        //转入账户组检查
                        fieldIndex = notNullableFields.IndexOf("InboundAccountGroupName".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.InboundAccountGroupName)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Customertransferbill_TransferToAccountGroupIsNull);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.InboundAccountGroupName) > fieldLenghtAccountGroup["Name".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Customertransferbill_TransferToAccountGroupIsLong);
                        }

                        //转入客户编号检查
                        fieldIndex = notNullableFields.IndexOf("InboundCustomerCompanyCode".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.InboundCustomerCompanyCode)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Customertransferbill_TransferToCustomerCodeIsNull);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.InboundCustomerCompanyCode) > fieldLenghtCustomerCompany["Code".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Customertransferbill_TransferToCustomerCodeIsLong);
                        }
                        ////转入客户名称检查
                        //fieldIndex = notNullableFields.IndexOf("InboundCustomerCompanyName".ToUpper());
                        //if(string.IsNullOrEmpty(tempImportObj.InboundCustomerCompanyName)) {
                        //    if(fieldIndex > -1)
                        //        tempErrorMessage.Add("转入客户名称不能为空");
                        //} else {
                        //    if(Encoding.Default.GetByteCount(tempImportObj.InboundCustomerCompanyName) > fieldLenghtCustomerCompany["Name".ToUpper()])
                        //        tempErrorMessage.Add("转入客户名称称过长");
                        //}
                        //金额
                        fieldIndex = notNullableFields.IndexOf("Amount".ToUpper());
                        if(null==tempImportObj.AmountStr) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Customertransferbill_AmountIsNull);
                        } else {
                            if(Convert.ToDouble(tempImportObj.AmountStr) < 0)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Customertransferbill_AmountOverZero);
                        }
                        //摘要
                        fieldIndex = notNullableFields.IndexOf("Summary".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.Summary)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Customertransferbill_SummaryIsNull);
                        }



                        //过账日期检查
                        fieldIndex = notNullableFields.IndexOf("InvoiceDateStr".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.InvoiceDateStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Customertransferbill_InvoiceDatesNull);
                        } else {
                            DateTime checkValue;
                            if(!DateTime.TryParse(tempImportObj.InvoiceDateStr, out checkValue)) {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Customertransferbill_InvoiceDateValuesError);
                            }
                        }

                        //备注Remark
                        fieldIndex = notNullableFields.IndexOf("Remark".ToUpper());
                        if(!string.IsNullOrEmpty(tempImportObj.Remark)) {
                            if(Encoding.Default.GetByteCount(tempImportObj.Remark) > fieldLenght["Remark".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchasePricingDetail_Validation16);
                        }

                        #endregion
                        if(tempErrorMessage.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });
                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    #region 导入内容业务检查                  
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    //校验转出账户组是否存在于账户组中
                    var accountGroupNames = tempRightList.Select(r => r.OutboundAccountGroupName).Distinct().ToArray();
                    var dbAccountGroups = new List<AccountGroupExtend>();
                    Func<string[], bool> getDbAccountGroups = value => {
                        dbAccountGroups.Add(new AccountGroupExtend {
                            Id = Convert.ToInt32(value[0]),
                            Name = value[1]
                        });
                        return false;
                    };
                    db.QueryDataWithInOperator("select Id,Name from AccountGroup where status=1", "Name", false, accountGroupNames, getDbAccountGroups);
                    var errorAccountGroups = tempRightList.Where(r => dbAccountGroups.All(v => v.Name != r.OutboundAccountGroupName)).ToList();
                    foreach(var errorItem in errorAccountGroups) {
                        errorItem.ErrorMsg = String.Format("转出账户组“{0}”不存在", errorItem.OutboundAccountGroupName);
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    foreach(var rightItem in tempRightList) {
                        var tempAccountGroup = dbAccountGroups.Single(r => r.Name == rightItem.OutboundAccountGroupName);
                        rightItem.OutboundAccountGroupId = tempAccountGroup.Id;
                    }
                    //校验转入账户组是否存在于账户组中
                    var iutboundAccountGroupName = tempRightList.Select(r => r.InboundAccountGroupName).Distinct().ToArray();
                    var dbinAccountGroups = new List<AccountGroupExtend>();
                    Func<string[], bool> getDbInAccountGroups = value => {
                        dbinAccountGroups.Add(new AccountGroupExtend {
                            Id = Convert.ToInt32(value[0]),
                            Name = value[1]
                        });
                        return false;
                    };
                    db.QueryDataWithInOperator("select Id,Name from AccountGroup where status=1", "Name", false, iutboundAccountGroupName, getDbInAccountGroups);
                    var errorInAccountGroups = tempRightList.Where(r => dbinAccountGroups.All(v => v.Name != r.OutboundAccountGroupName)).ToList();
                    foreach(var errorItem in errorInAccountGroups) {
                        errorItem.ErrorMsg = String.Format("转入账户组“{0}”不存在", errorItem.InboundAccountGroupName);
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    foreach(var rightItem in tempRightList) {
                        var tempAccountGroup = dbinAccountGroups.Single(r => r.Name == rightItem.InboundAccountGroupName);
                        rightItem.InboundAccountGroupId = tempAccountGroup.Id;
                    }

                    //校验转出客户企业是否存在
                    var companyCodes = tempRightList.Select(r => r.OutboundCustomerCompanyCode).Distinct().ToArray();
                    var dbCompanys = new List<CompanyExtend>();
                    Func<string[], bool> getDbCompanys = value => {
                        var dbObj = new CompanyExtend {
                            Id = Convert.ToInt32(value[0]),
                            Code = value[1],
                            Name = value[2]
                        };
                        dbCompanys.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select Id,Code,Name from Company where status=1 ", "Code", false, companyCodes, getDbCompanys);
                    var errorCompanys = tempRightList.Where(r => !dbCompanys.Any(v =>  v.Code == r.OutboundCustomerCompanyCode)).ToList();
                    foreach(var errorItem in errorCompanys) {
                        errorItem.ErrorMsg = String.Format("转出客户编号“{0}”", errorItem.OutboundCustomerCompanyCode);
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    foreach(var rightItem in tempRightList) {
                        var tempCompany = dbCompanys.Single(r => r.Code == rightItem.OutboundCustomerCompanyCode);
                        rightItem.OutboundCustomerCompanyId = tempCompany.Id;
                    }
                    //校验转入客户企业是否存在
                    var companyinCodes = tempRightList.Select(r => r.InboundCustomerCompanyCode).Distinct().ToArray();
                    var dbInCompanys = new List<CompanyExtend>();
                    Func<string[], bool> getDbInCompanys = value => {
                        var dbObj = new CompanyExtend {
                            Id = Convert.ToInt32(value[0]),
                            Code = value[1],
                            Name = value[2]
                        };
                        dbInCompanys.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select Id,Code,Name from Company where status=1 ", "Code", false, companyinCodes, getDbInCompanys);
                    var errorInCompanys = tempRightList.Where(r => !dbInCompanys.Any(v =>  v.Code == r.InboundCustomerCompanyCode)).ToList();
                    foreach(var errorItem in errorInCompanys) {
                        errorItem.ErrorMsg = String.Format("转入客户编号“{0}”", errorItem.InboundCustomerCompanyName);
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    foreach(var rightItem in tempRightList) {
                        var tempCompany = dbInCompanys.Single(r => r.Code == rightItem.InboundCustomerCompanyCode);
                        rightItem.InboundCustomerCompanyId = tempCompany.Id;
                    }
                    //校验过账日期是否存在
                    var invoiceDates = tempRightList.Select(r => Convert.ToDateTime(r.InvoiceDateStr).Year.ToString()).Distinct().ToArray();
                    var dbInvoiceDates = new List<AccountPeriodExtend>();
                    Func<string[], bool> getDbInvoiceDates = value => {
                        var dbObj = new AccountPeriodExtend {
                            Id = Convert.ToInt32(value[0]),
                            Year = value[1],
                            Month = value[2]
                        };
                        dbInvoiceDates.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select Id,Year,Month from AccountPeriod where status=1 ", "Year", false, invoiceDates, getDbInvoiceDates);
                    var errorInvoiceDates = tempRightList.Where(r => !dbInvoiceDates.Any(v=>v.Year== Convert.ToDateTime(r.InvoiceDateStr).Year.ToString()&& v.Month==Convert.ToDateTime(r.InvoiceDateStr).Month.ToString())).ToList();
                    foreach(var errorItem in errorInvoiceDates) {
                        errorItem.ErrorMsg = String.Format("过账日期“{0}”无效", errorItem.InvoiceDateStr);
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                   
                    #endregion
                    //获取所有不合格数据
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    //获取所有合格数据
                    rightList = allList.Except(errorList).ToList();

                    #region 将合格数据的值填上
                    foreach(var rightItem in rightList) {
                        rightItem.Amount =Convert.ToDouble( rightItem.AmountStr);
                        rightItem.InvoiceDate = Convert.ToDateTime(rightItem.InvoiceDateStr);
                    }
                    #endregion

                }
                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempObj = list[index - 1];
                            var values = new object[] {
                                #region 设置错误信息导出的列的值
                                tempObj.BusinessTypeStr,tempObj.OutboundAccountGroupName, tempObj.OutboundCustomerCompanyCode,tempObj.OutboundCustomerCompanyName,
                                tempObj.InboundAccountGroupName ,tempObj.InboundCustomerCompanyCode ,tempObj.InboundCustomerCompanyName,
                                tempObj.AmountStr ,tempObj.Summary, tempObj.InvoiceDateStr,tempObj.ErrorMsg
                               
                                #endregion                     
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }

                //导入所有合格数据
                if(!rightList.Any())
                    return true;
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    //开启事务，新增更新配件在一个事务内
                    var ts = conn.BeginTransaction();
                    try {
                        //新增配件
                        if(rightList.Any()) {
                            #region 获取对应的插入的SQL 通过db.GetInsertSql方法
                            //获取新增数据的sql语句，Id为主键
                            var sqlInsert = db.GetInsertSql("CustomerTransferBill", "Id", new[] {
                                "Code","SalesCompanyId","OutboundAccountGroupId","OutboundCustomerCompanyId","InboundAccountGroupId","InboundCustomerCompanyId",
                                "Summary","Amount","Operator","Remark","Status","InvoiceDate","BusinessType","CreatorId",
                                "CreatorName","CreateTime"
                            });
                            #endregion
                            var userInfo = Utils.GetCurrentUserInfo();
                            //往数据库增加配件信息
                            foreach(var item in rightList) {
                                #region 添加Sql的参数
                                var command = db.CreateDbCommand(sqlInsert, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("Code", CodeGenerator.Generate(db, "CustomerTransferBill", userInfo.EnterpriseCode)));
                                command.Parameters.Add(db.CreateDbParameter("SalesCompanyId", userInfo.EnterpriseId));
                                command.Parameters.Add(db.CreateDbParameter("OutboundAccountGroupId", item.OutboundAccountGroupId));
                                command.Parameters.Add(db.CreateDbParameter("OutboundCustomerCompanyId", item.OutboundCustomerCompanyId));
                                command.Parameters.Add(db.CreateDbParameter("InboundAccountGroupId", item.InboundAccountGroupId));
                                command.Parameters.Add(db.CreateDbParameter("InboundCustomerCompanyId", item.InboundCustomerCompanyId));
                                command.Parameters.Add(db.CreateDbParameter("Summary", item.Summary));
                                command.Parameters.Add(db.CreateDbParameter("Amount", item.Amount));
                                command.Parameters.Add(db.CreateDbParameter("Operator", item.Operator));
                                command.Parameters.Add(db.CreateDbParameter("Remark", item.Remark));
                                command.Parameters.Add(db.CreateDbParameter("Status", (int)DcsWorkflowOfSimpleApprovalStatus.新建));
                                command.Parameters.Add(db.CreateDbParameter("InvoiceDate", item.InvoiceDate));
                                command.Parameters.Add(db.CreateDbParameter("BusinessType", item.BusinessType));
                                command.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                command.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                command.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                command.ExecuteNonQuery();
                                #endregion
                            }
                        }
                        //无异常提交
                        ts.Commit();
                    } catch(Exception) {
                        //报错回滚
                        ts.Rollback();
                        throw;
                    } finally {
                        if(conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
            }
        }

    }
}

