﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Globalization;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        /// <summary>
        /// 导出财务快照设置配件历史库存
        /// </summary>
        public bool ExportPartsHistoryStock(int[] ids, string partCode, string partName, int? warehouseId, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName) {
            fileName = GetExportFilePath("配件历史库存_" + ".xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {
                    var sql = new StringBuilder();
                    sql.Append(@"Select b.Name,
                                       c.Name,
                                       d.Name,
                                       e.Code,
                                       (select value from keyvalueitem where NAME = 'Area_Category'and key=f.Category) As Category,
                                       g.Code,
                                       g.Name,
                                       a.Quantity,
                                       a.Plannedprice,
                                       a.Plannedpriceamount,
                                       a.Creatorname,
                                       a.Createtime,
                                       a.Modifiername,
                                       a.Modifytime
                                  From Partshistorystock a
                                 Inner Join Branch b
                                    On a.Branchid = b.Id
                                 Inner Join Warehouse c
                                    On a.Warehouseid = c.Id
                                 Inner Join Company d
                                    On a.Storagecompanyid = d.Id
                                 Inner Join Warehousearea e
                                    On a.Warehouseareaid = e.Id
                                 Inner Join Warehouseareacategory f
                                    On a.Warehouseareacategoryid = f.Id
                                 Inner Join Sparepart g
                                    On a.Partid = f.Id
                                where 1=1 ");
                    var dbParameters = new List<DbParameter>();
                    #region 条件过滤
                    if(ids != null && ids.Length > 0) {
                        sql.Append(" and a.id in (");
                        for(var i = 0; i < ids.Length; i++) {
                            if(ids.Length == i + 1) {
                                sql.Append("{0}" + ids[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter(ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            } else {
                                sql.Append("{0}" + ids[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter(ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                        }
                        sql.Append(")");
                    } else {
                        if(!String.IsNullOrEmpty(partCode)) {
                            sql.Append(@" and Upper(g.Code) like Upper({0}partCode) ");
                            dbParameters.Add(db.CreateDbParameter("partCode", "%" + partCode + "%"));
                        }
                        if(!String.IsNullOrEmpty(partName)) {
                            sql.Append(@" and g.Name like {0}partName ");
                            dbParameters.Add(db.CreateDbParameter("partName", "%" + partName + "%"));
                        }
                        if(warehouseId.HasValue) {
                            sql.Append(@" and a.warehouseId = {0}warehouseId ");
                            dbParameters.Add(db.CreateDbParameter("warehouseId", warehouseId.Value));
                        }
                        if(createTimeBegin.HasValue) {
                            sql.Append(@" and a.createTime >=to_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(@" and a.createTime <=to_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                    }
                    #endregion

                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    conn.Open();
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                ErrorStrings.Export_Title_Partshistorystock_BranchName,ErrorStrings.Export_Title_Company_WarehouseName,ErrorStrings.Export_Title_Partshistorystock_StorageCompanyName,
                                ErrorStrings.Export_Title_WarehouseArea_Code,ErrorStrings.Export_Title_WarehouseArea_Category,ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode,ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName,
                                ErrorStrings.Export_Title_WarehouseArea_Quantity,ErrorStrings.Export_Title_PartsPlannedPrice_PlannedPrice,ErrorStrings.Export_Title_WarehouseArea_PlannedpriceAmount,ErrorStrings.Export_Title_AccountPeriod_CreatorName,ErrorStrings.Export_Title_AccountPeriod_CreateTime,ErrorStrings.Export_Title_AccountPeriod_ModifierName,ErrorStrings.Export_Title_AccountPeriod_ModifyTime
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }
    }
}
