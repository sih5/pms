﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        /// <summary>
        /// 导入待结算出入库清单
        /// </summary>
        public bool ImpOutboundAndInboundBill(string fileName,int partsSalesCategoryId, int supplierId, out int excelImportNum, out List<ImpOutboundAndInboundBill> rightData, out List<ImpOutboundAndInboundBill> errorData, out string errorDataFileName, out string errorMessage) {
            //初始化返回数据
            excelImportNum = 0;
            errorDataFileName = "";
            errorMessage = "";
            var errorList = new List<ImpOutboundAndInboundBill>();
            var rightList = new List<ImpOutboundAndInboundBill>();
            var allList = new List<ImpOutboundAndInboundBill>();

            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var userInfo = Utils.GetCurrentUserInfo();
                var db = DbHelper.GetDbHelp(ConnectionString);

                List<object> excelColumns;

                //Excel数据导入
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    //指定Excel列名与字段名关系
                    excelOperator.AddColumnDataSource("单据编号", "Code");

                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();

                    //Excel单行导入
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        //接收Excel导入数据
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        var tempImportObj = new ImpOutboundAndInboundBill {
                            Code = newRow["Code"],
                        };
                        var tempErrorMessage = new List<string>();
                        
                        //单据编号检查
                        if (string.IsNullOrWhiteSpace(tempImportObj.Code)) {
                            tempErrorMessage.Add("单据编号不可为空");
                        }else if(tempImportObj.Code.Length>50){
                             tempErrorMessage.Add("单据编号过长");
                        }

                        if(tempErrorMessage.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });
                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    //检查是否重复
                    var codeGroups = tempRightList.GroupBy(r => new { r.Code});
                    foreach (var codeGroup in codeGroups) {
                        if (codeGroup.Count() > 1) {
                            foreach (var code in codeGroup) {
                                code.ErrorMsg = "单据编号重复";
                            }
                        }
                    }
                }

                errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                rightList = allList.Except(errorList).ToList();
                if(rightList.Any()) {
                    string sql = @"SELECT 
Project10.C1 Id,
Project10.C3 as Type,
Project10.C4 as Code,
Project10.C5 as SettlementAmount,
Project10.C6 as CostAmount,
Project10.C7 as CreateTime,
Project10.C8 as StorageCompanyId,
Project10.C9 as CounterpartCompanyId,
Project10.C11 as InboundType,
Project10.C12 as WarehouseId,
Project10.C13 as WarehouseName,
Project10.C14 as ReturnReason,
Project10.C17 as OriginalRequirementBillCode
FROM ( SELECT 
  UnionAll1.Id AS C1,
  UnionAll1.Id1 AS C2,
  UnionAll1.C1 AS C3,
  UnionAll1.Code AS C4,
  UnionAll1.C2 AS C5,
  UnionAll1.C3 AS C6,
  UnionAll1.CreateTime AS C7,
  UnionAll1.StorageCompanyId AS C8,
  UnionAll1.CounterpartCompanyId AS C9,
  UnionAll1.CustomerAccountId AS C10,
  UnionAll1.InboundType AS C11,
  UnionAll1.WarehouseId AS C12,
  UnionAll1.WarehouseName AS C13,
  UnionAll1.C4 AS C14,
  UnionAll1.CPPartsPurchaseOrderCode AS C15,
  UnionAll1.CPPartsInboundCheckCode AS C16,
  UnionAll1.OriginalRequirementBillCode AS C17
  FROM  (SELECT 
    Project3.Id,
    Project3.Id AS Id1,
    5 AS C1,
    Project3.Code,
    Project3.C1 AS C2,
    Project3.C2 AS C3,
    Project3.CreateTime,
    Project3.StorageCompanyId,
    Project3.CounterpartCompanyId,
    Project3.CustomerAccountId,
    Project3.InboundType,
    Project3.WarehouseId,
    Project3.WarehouseName,
    '' AS C4,
    Project3.CPPartsPurchaseOrderCode,
    Project3.CPPartsInboundCheckCode,
    Project3.OriginalRequirementBillCode
    FROM ( SELECT 
      Project2.Id,
      Project2.Code,
      Project2.WarehouseId,
      Project2.WarehouseName,
      Project2.StorageCompanyId,
      Project2.CounterpartCompanyId,
      Project2.InboundType,
      Project2.CustomerAccountId,
      Project2.OriginalRequirementBillCode,
      Project2.CreateTime,
      Project2.CPPartsPurchaseOrderCode,
      Project2.CPPartsInboundCheckCode,
      Project2.C1,
      (SELECT Sum(Extent4.CostPrice * Extent4.InspectedQuantity) AS A1
FROM PartsInboundCheckBillDetail Extent4
WHERE Extent4.PartsInboundCheckBillId = Project2.Id) AS C2
      FROM ( SELECT 
        Extent1.Id,
        Extent1.Code,
        Extent1.WarehouseId,
        Extent1.WarehouseName,
        Extent1.StorageCompanyId,
        Extent1.CounterpartCompanyId,
        Extent1.InboundType,
        Extent1.CustomerAccountId,
        Extent1.OriginalRequirementBillCode,
        Extent1.CreateTime,
        Extent1.CPPartsPurchaseOrderCode,
        Extent1.CPPartsInboundCheckCode,
        (SELECT Sum(Extent3.SettlementPrice * Extent3.InspectedQuantity) AS A1
FROM PartsInboundCheckBillDetail Extent3
WHERE Extent3.PartsInboundCheckBillId = Extent1.Id) AS C1
        FROM PartsInboundCheckBill Extent1
        WHERE (((((((Extent1.CPPartsInboundCheckCode IS NULL) AND (Extent1.SettlementStatus = 2)) AND (Extent1.StorageCompanyId = {0})) AND (Extent1.CounterpartCompanyId = {1})) AND ( EXISTS (SELECT 
          1 AS C1
          FROM PartsInboundPlan Extent2
          WHERE Extent1.PartsInboundPlanId = Extent2.Id
        )))) AND (Extent1.PartsSalesCategoryId = {2})) AND (Extent1.InboundType = 1)
      )  Project2
    )  Project3
  UNION ALL
    SELECT 
    Project8.Id,
    Project8.Id AS Id1,
    1 AS C1,
    Project8.Code,
    Project8.C1 AS C2,
    Project8.C2 AS C3,
    Project8.CreateTime,
    Project8.StorageCompanyId,
    Project8.CounterpartCompanyId,
    Project8.CustomerAccountId,
    Project8.OutboundType,
    Project8.WarehouseId,
    Project8.WarehouseName,
    cast('质量件退货' as varchar2(100)) AS C4,
    Project8.CPPartsPurchaseOrderCode,
    Project8.CPPartsInboundCheckCode,
    Project8.OriginalRequirementBillCode
    FROM ( SELECT 
      Project7.Id,
      Project7.Code,
      Project7.WarehouseId,
      Project7.WarehouseName,
      Project7.StorageCompanyId,
      Project7.CounterpartCompanyId,
      Project7.OutboundType,
      Project7.CustomerAccountId,
      Project7.OriginalRequirementBillCode,
      Project7.CreateTime,
      Project7.CPPartsPurchaseOrderCode,
      Project7.CPPartsInboundCheckCode,
      Project7.C1,
      (SELECT Sum( -(Extent10.CostPrice * Extent10.OutboundAmount)) AS A1
FROM PartsOutboundBillDetail Extent10
WHERE Extent10.PartsOutboundBillId = Project7.Id) AS C2
      FROM ( SELECT 
        Distinct1.Id,
        Distinct1.Code,
        Distinct1.WarehouseId,
        Distinct1.WarehouseName,
        Distinct1.StorageCompanyId,
        Distinct1.CounterpartCompanyId,
        Distinct1.OutboundType,
        Distinct1.CustomerAccountId,
        Distinct1.OriginalRequirementBillCode,
        Distinct1.CreateTime,
        Distinct1.CPPartsPurchaseOrderCode,
        Distinct1.CPPartsInboundCheckCode,
        (SELECT Sum(Extent9.SettlementPrice * Extent9.OutboundAmount) AS A1
FROM PartsOutboundBillDetail Extent9
WHERE Extent9.PartsOutboundBillId = Distinct1.Id) AS C1
        FROM ( SELECT DISTINCT 
          Extent5.Id,
          Extent5.Code,
          Extent5.PartsOutboundPlanId,
          Extent5.WarehouseId,
          Extent5.WarehouseCode,
          Extent5.WarehouseName,
          Extent5.StorageCompanyId,
          Extent5.StorageCompanyCode,
          Extent5.StorageCompanyName,
          Extent5.StorageCompanyType,
          Extent5.BranchId,
          Extent5.BranchCode,
          Extent5.BranchName,
          Extent5.PartsSalesCategoryId,
          Extent5.PartsSalesOrderTypeId,
          Extent5.PartsSalesOrderTypeName,
          Extent5.CounterpartCompanyId,
          Extent5.CounterpartCompanyCode,
          Extent5.CounterpartCompanyName,
          Extent5.ReceivingCompanyId,
          Extent5.ReceivingCompanyCode,
          Extent5.ReceivingCompanyName,
          Extent5.ReceivingWarehouseId,
          Extent5.ReceivingWarehouseCode,
          Extent5.ReceivingWarehouseName,
          Extent5.OutboundType,
          Extent5.ShippingMethod,
          Extent5.CustomerAccountId,
          Extent5.OriginalRequirementBillId,
          Extent5.OriginalRequirementBillType,
          Extent5.OriginalRequirementBillCode,
          Extent5.SettlementStatus,
          Extent5.OrderApproveComment,
          Extent5.Remark,
          Extent5.CreatorId,
          Extent5.CreatorName,
          Extent5.CreateTime,
          Extent5.ModifierId,
          Extent5.ModifierName,
          Extent5.ModifyTime,
          Extent5.RowVersion,
          Extent5.InterfaceRecordId,
          Extent5.GPMSPurOrderCode,
          Extent5.ContractCode,
          Extent5.OutboundPackPlanCode,
          Extent5.PrintTimes,
          Extent5.PurDistributionStatus,
          Extent5.SAPPurchasePlanCode,
          Extent5.ERPSourceOrderCode,
          Extent5.EcommerceMoney,
          Extent5.PWMSOutboundCode,
          Extent5.CPPartsPurchaseOrderCode,
          Extent5.CPPartsInboundCheckCode,
          Extent5.PartsPurchaseSettleCode,
          Extent5.PartsShippingOrderId
          FROM  PartsOutboundBill Extent5
          INNER JOIN  (SELECT 
            Extent6.Id AS Id1,
            Extent6.Code AS Code1,
            Extent6.PartsPurchaseOrderId,
            Extent6.PartsPurchaseOrderCode,
            Extent6.BranchId AS BranchId1,
            Extent6.BranchCode AS BranchCode1,
            Extent6.BranchName AS BranchName1,
            Extent6.PartsSupplierId,
            Extent6.PartsSupplierCode,
            Extent6.PartsSupplierName,
            Extent6.TotalAmount,
            Extent6.WarehouseId AS WarehouseId1,
            Extent6.WarehouseName AS WarehouseName1,
            Extent6.WarehouseAddress,
            Extent6.InvoiceRequirement,
            Extent6.Status AS Status1,
            Extent6.ReturnReason,
            Extent6.Remark AS Remark1,
            Extent6.CreatorId AS CreatorId1,
            Extent6.CreatorName AS CreatorName1,
            Extent6.CreateTime AS CreateTime1,
            Extent6.ModifierId AS ModifierId1,
            Extent6.ModifierName AS ModifierName1,
            Extent6.ModifyTime AS ModifyTime1,
            Extent6.AbandonerId,
            Extent6.AbandonerName,
            Extent6.AbandonTime,
            Extent6.ApproverId,
            Extent6.ApproverName,
            Extent6.ApproveTime,
            Extent6.RowVersion AS RowVersion1,
            Extent6.PartsSalesCategoryId AS PartsSalesCategoryId1,
            Extent6.PartsSalesCategoryName,
            Extent6.OutStatus,
            Extent6.SAPPurReturnOrderCode,
            Extent6.ServiceApplyCode,
            Extent6.ERPSourceOrderCode AS ERPSourceOrderCode1,
            Extent6.InitialApproverId,
            Extent6.InitialApproverName,
            Extent6.InitialApproveTime,
            Extent6.InitialApproverComment,
            Extent6.ApprovertComment,
            Extent7.Id AS Id2,
            Extent7.Code AS Code2,
            Extent7.PartsInboundPlanId,
            Extent7.WarehouseId AS WarehouseId2,
            Extent7.WarehouseCode,
            Extent7.WarehouseName AS WarehouseName2,
            Extent7.StorageCompanyId,
            Extent7.StorageCompanyCode,
            Extent7.StorageCompanyName,
            Extent7.StorageCompanyType,
            Extent7.PartsSalesCategoryId AS PartsSalesCategoryId2,
            Extent7.BranchId AS BranchId2,
            Extent7.BranchCode AS BranchCode2,
            Extent7.BranchName AS BranchName2,
            Extent7.CounterpartCompanyId,
            Extent7.CounterpartCompanyCode,
            Extent7.CounterpartCompanyName,
            Extent7.InboundType,
            Extent7.CustomerAccountId,
            Extent7.OriginalRequirementBillId,
            Extent7.OriginalRequirementBillType,
            Extent7.OriginalRequirementBillCode,
            Extent7.Status AS Status2,
            Extent7.SettlementStatus,
            Extent7.Remark AS Remark2,
            Extent7.CreatorId AS CreatorId2,
            Extent7.CreatorName AS CreatorName2,
            Extent7.CreateTime AS CreateTime2,
            Extent7.ModifierId AS ModifierId2,
            Extent7.ModifierName AS ModifierName2,
            Extent7.ModifyTime AS ModifyTime2,
            Extent7.RowVersion AS RowVersion2,
            Extent7.Objid,
            Extent7.GPMSPurOrderCode,
            Extent7.PurOrderCode,
            Extent7.ReturnContainerNumber,
            Extent7.SAPPurchasePlanCode,
            Extent7.ERPSourceOrderCode AS ERPSourceOrderCode2,
            Extent7.EcommerceMoney,
            Extent7.PWMSStorageCode,
            Extent7.CPPartsPurchaseOrderCode,
            Extent7.CPPartsInboundCheckCode
            FROM  PartsPurReturnOrder Extent6
            INNER JOIN PartsInboundCheckBill Extent7 ON Extent7.OriginalRequirementBillId = Extent6.PartsPurchaseOrderId
            WHERE ((((Extent7.CPPartsInboundCheckCode IS NULL) AND (Extent7.SettlementStatus = 2)) AND ( EXISTS (SELECT 
              1 AS C1
              FROM PartsInboundPlan Extent8
              WHERE Extent7.PartsInboundPlanId = Extent8.Id
            ))) AND (Extent7.InboundType = 1)) AND (Extent6.Status <> 99) ) Filter6 ON Extent5.OriginalRequirementBillId = Filter6.Id1
          WHERE ((((((Filter6.StorageCompanyId = {3}) AND (Filter6.CounterpartCompanyId = {4}))) AND (Filter6.PartsSalesCategoryId2 = {5})) AND (Extent5.SettlementStatus = 2)) AND (Extent5.CounterpartCompanyId = {6})) AND (Extent5.StorageCompanyId = {7})
        )  Distinct1
      )  Project7
    )  Project8) UnionAll1
)  Project10";
                    using (var conn = db.CreateDbConnection()) {
                        conn.Open();
                        //查询原库位可用库存
                        sql = string.Format(sql,userInfo.EnterpriseId,supplierId,partsSalesCategoryId,userInfo.EnterpriseId,supplierId,partsSalesCategoryId, supplierId, userInfo.EnterpriseId);

                        var details = new List<ImpOutboundAndInboundBill>();
                        var cmd = db.CreateDbCommand(sql, conn, null);
                        var odr = cmd.ExecuteReader();

                        try {
                            if(odr.HasRows) {
                                while(odr.Read()) {
                                    var item = new ImpOutboundAndInboundBill();
                                    item.Id = Convert.ToInt32(odr["Id"]);
                                    item.Type = Convert.ToInt32(odr["Type"]);
                                    item.Code = Convert.ToString(odr["Code"]);
                                    item.SettlementAmount = Convert.ToDecimal(odr["SettlementAmount"]);
                                    item.CostAmount = Convert.ToDecimal(odr["CostAmount"]);
                                    item.StorageCompanyId = Convert.ToInt32(odr["StorageCompanyId"]);
                                    item.CounterpartCompanyId = Convert.ToInt32(odr["CounterpartCompanyId"]);
                                    item.WarehouseId = Convert.ToInt32(odr["WarehouseId"]);
                                    item.WarehouseName = Convert.ToString(odr["WarehouseName"]);
                                    item.ReturnReason = Convert.ToString(odr["ReturnReason"]);
                                    item.OriginalRequirementBillCode = Convert.ToString(odr["OriginalRequirementBillCode"]);
                                    item.CreateTime = Convert.ToDateTime(odr["CreateTime"]);
                                    item.InboundType = Convert.ToInt32(odr["InboundType"]);
                                    details.Add(item);
                                }
                            }
                        } finally {
                            if(conn.State == System.Data.ConnectionState.Open) {
                                conn.Close();
                            }
                        }

                        foreach(var rightItem in rightList) {
                            var detail = details.FirstOrDefault(r => r.Code == rightItem.Code);
                            if (detail == null) {
                                rightItem.ErrorMsg = "该单据供应商不正确或已结算";
                            } else {
                                rightItem.Id = detail.Id;
                                rightItem.Type = detail.Type;
                                rightItem.SettlementAmount = detail.SettlementAmount;
                                rightItem.CostAmount = detail.CostAmount;
                                rightItem.StorageCompanyId = detail.StorageCompanyId;
                                rightItem.CounterpartCompanyId = detail.CounterpartCompanyId;
                                rightItem.WarehouseId = detail.WarehouseId;
                                rightItem.WarehouseName = detail.WarehouseName;
                                rightItem.ReturnReason = detail.ReturnReason;
                                rightItem.OriginalRequirementBillCode = detail.OriginalRequirementBillCode;
                                rightItem.CreateTime = detail.CreateTime;
                                rightItem.InboundType = detail.InboundType;
                            }
                        }
                    }
                }
                var err2 = rightList.Where(r => !string.IsNullOrEmpty(r.ErrorMsg));
                errorList = errorList.Union(err2).ToList();
                rightList = allList.Except(errorList).ToList();
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var details = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == details.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var detail = details[index - 1];
                            var values = new object[]{
                                        detail.Code,detail.ErrorMsg
                                    };
                            return values;
                        });
                    }
                    errorList = null;
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }

    }
}
