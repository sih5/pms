﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Globalization;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        /// <summary>
        /// 导出来款分割单
        /// </summary>
        public bool ExportPaymentBeneficiaryList(int[] ids, int salesCompanyId, string companyName, string companyCode, string paymentBillCode, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName) {
            fileName = GetExportFilePath("来款分割单_" + ".xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {
                    var sql = new StringBuilder();
                    sql.AppendFormat(@"Select a.Code,
                                               b.Code,
                                               b.Name,
                                               a.Amount,
                                               a.Summary,
                                               a.Paymentbillcode,
                                               a.Billnumber,
                                               d.Bankaccountnumber,
                                               a.Operator,
                                              (select value from keyvalueitem where NAME = 'PaymentBill_Status'and key=a.Status) As Status,
                                               a.Creatorname,
                                               a.Createtime,
                                               a.Modifiername,
                                               a.Modifytime
                                          From Paymentbeneficiarylist a
                                          Left Join Company b
                                            On a.Customercompanyid = b.Id
                                          Left Join Paymentbill c
                                            On a.Paymentbillid = c.Id
                                          Left Join Bankaccount d
                                            On c.Bankaccountid = d.Id
                                where a.salesCompanyId={0} ", salesCompanyId);
                    var dbParameters = new List<DbParameter>();
                    #region 条件过滤
                    if(ids != null && ids.Length > 0) {
                        sql.Append(" and a.id in (");
                        for(var i = 0; i < ids.Length; i++) {
                            if(ids.Length == i + 1) {
                                sql.Append("{0}" + ids[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter(ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            } else {
                                sql.Append("{0}" + ids[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter(ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                        }
                        sql.Append(")");
                    } else {
                        if(!String.IsNullOrEmpty(companyCode)) {
                            sql.Append(@" and Upper(b.Code) like Upper({0}companyCode) ");
                            dbParameters.Add(db.CreateDbParameter("companyCode", "%" + companyCode + "%"));
                        }
                        if(!String.IsNullOrEmpty(companyName)) {
                            sql.Append(@" and b.Name like {0}companyName ");
                            dbParameters.Add(db.CreateDbParameter("companyName", "%" + companyName + "%"));
                        }
                        if(!String.IsNullOrEmpty(paymentBillCode)) {
                            sql.Append(@" and Upper(a.Code) like Upper({0}paymentBillCode) ");
                            dbParameters.Add(db.CreateDbParameter("paymentBillCode", "%" + paymentBillCode + "%"));
                        }
                        if(status.HasValue) {
                            sql.Append(@" and a.status = {0}status ");
                            dbParameters.Add(db.CreateDbParameter("status", status.Value));
                        }
                        if(createTimeBegin.HasValue) {
                            sql.Append(@" and a.createTime >=to_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(@" and a.createTime <=to_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                    }
                    #endregion

                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    conn.Open();
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                ErrorStrings.Export_Title_PackingPropertyApp_Code,ErrorStrings.Export_Title_Credenceapplication_CustomerCode,ErrorStrings.Export_Title_Credenceapplication_CustomerName,ErrorStrings.Export_Title_Customertransferbill_Amount,ErrorStrings.Export_Title_Customertransferbill_Summary,
                                ErrorStrings.Export_Title_Paymentbeneficiarylist_Code,ErrorStrings.Export_Title_Paymentbeneficiarylist_Billnumber,ErrorStrings.Export_Title_Paymentbeneficiarylist_Bankaccountnumber,ErrorStrings.Export_Title_Customertransferbill_Operator,ErrorStrings.Export_Title_AccountPeriod_Status,ErrorStrings.Export_Title_AccountPeriod_CreatorName,ErrorStrings.Export_Title_AccountPeriod_CreateTime,ErrorStrings.Export_Title_AccountPeriod_ModifierName,ErrorStrings.Export_Title_AccountPeriod_ModifyTime
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }
    }
}
