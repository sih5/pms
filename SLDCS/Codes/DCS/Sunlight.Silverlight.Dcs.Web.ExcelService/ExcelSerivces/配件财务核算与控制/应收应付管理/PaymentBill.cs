﻿
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        /// <summary>
        /// 导出来款单管理
        /// </summary>
        public bool ExportPaymentBill(int[] ids, string customerCompanyName, string customerCompanyCode, int? paymentMethod, int? status, string creatorName, DateTime? createTimeBegin, DateTime? createTimeEnd, int salesCompanyId, string businessCode, DateTime? invoiceDateBegin, DateTime? invoiceDateEnd, out string fileName)
        {
            fileName = GetExportFilePath("来款单_" + ".xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);
                using(var conn = db.CreateDbConnection()) {
                    var sql = new StringBuilder();
                    sql.AppendFormat(@"select  a.Code,d.name,c.Name,c.Code,a.Amount,(select value from keyvalueitem where NAME = 'PaymentBill_PaymentType'and key=a.PaymentMethod) As PaymentMethod,a.Summary,b.BankAccountNumber,a.Operator,(select value from keyvalueitem where NAME = 'PaymentBill_Status'and key=a.Status) As Status,a.InvoiceDate,a.CreatorName,a.CreateTime,a.ModifierName,a.ModifyTime,case c.type
                                                when 2 then
                                                i.businessCode
                                                when 7 then
                                                 i.businessCode
                                                when 3 then
                                                s.businessCode                                                  
                                                end,
                                               a.Drawer,a.IssueDate,a.DueDate,a.PayBank,a.MoneyOrderNum
                                               from PaymentBill a
                       inner join Company c on a.CustomerCompanyId=c.id
                       left join AccountGroup d on a.AccountGroupId=d.id
                       left join BankAccount b on  a.BankAccountId=b.id 
                       left join SalesUnit s on a.AccountGroupId = s.AccountGroupId and d.SalesCompanyId = s.OwnerCompanyId
                       left join DealerServiceInfo i on s.PartsSalesCategoryId = i.PartsSalesCategoryId and a.CustomerCompanyId = i.DealerId and i.status=1
                       where a.SalesCompanyId={0} ", salesCompanyId);
                    var dbParameters = new List<DbParameter>();
                    #region  增加过滤条件
                    if(!string.IsNullOrEmpty(customerCompanyCode)) {
                        sql.Append(@" and Upper(c.Code) like Upper({0}customerCompanyCode) ");
                        dbParameters.Add(db.CreateDbParameter("customerCompanyCode", "%" + customerCompanyCode + "%"));
                    }
                    if(!string.IsNullOrEmpty(customerCompanyName)) {
                        sql.Append(@" and Upper(c.Name) like Upper({0}customerCompanyName) ");
                        dbParameters.Add(db.CreateDbParameter("customerCompanyName", "%" + customerCompanyName + "%"));
                    }
                    if(!string.IsNullOrEmpty(creatorName)) {
                        sql.Append(@" and Upper(a.CreatorName) like Upper({0}creatorName) ");
                        dbParameters.Add(db.CreateDbParameter("creatorName", "%" + creatorName + "%"));
                    }
                    if(paymentMethod.HasValue) {
                        sql.Append(@" and a.PaymentMethod = {0}paymentMethod ");
                        dbParameters.Add(db.CreateDbParameter("paymentMethod", paymentMethod.Value));
                    }
                    if(status.HasValue) {
                        sql.Append(@" and a.status = {0}status ");
                        dbParameters.Add(db.CreateDbParameter("status", status.Value));
                    }
                    if(createTimeBegin.HasValue) {
                        sql.Append(@" and a.Createtime >=to_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                        var tempValue = createTimeBegin.Value;
                        var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                        dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                    }
                    if(createTimeEnd.HasValue) {
                        sql.Append(@" and a.Createtime <=to_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                        var tempValue = createTimeEnd.Value;
                        var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                        dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                    }
                    if(!string.IsNullOrEmpty(businessCode)) {
                        sql.Append(" and s.businessCode like {0}businessCode and i.businessCode like {0}businessCode ");
                        dbParameters.Add(db.CreateDbParameter("businessCode", "%" + businessCode + "%"));
                    }
                    if (invoiceDateBegin.HasValue)
                    {
                        sql.Append(@" and a.InvoiceDate >=to_date({0}invoiceDateBegin,'yyyy-mm-dd hh24:mi:ss')");
                        var tempValue = invoiceDateBegin.Value;
                        var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                        dbParameters.Add(db.CreateDbParameter("invoiceDateBegin", tempTime.ToString("G")));
                    }
                    if (invoiceDateEnd.HasValue)
                    {
                        sql.Append(@" and a.InvoiceDate <=to_date({0}invoiceDateEnd,'yyyy-mm-dd hh24:mi:ss')");
                        var tempValue = invoiceDateEnd.Value;
                        var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                        dbParameters.Add(db.CreateDbParameter("invoiceDateEnd", tempTime.ToString("G")));
                    }
                    #endregion
                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    conn.Open();
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                ErrorStrings.Export_Title_PaymentBill_Code,ErrorStrings.Export_Title_CustomerAccount_Name,ErrorStrings.Export_Title_Credenceapplication_CustomerName,ErrorStrings.Export_Title_Credenceapplication_CustomerCode,ErrorStrings.Export_Title_Customertransferbill_Amount,ErrorStrings.Export_Title_PaymentBill_Type,ErrorStrings.Export_Title_Customertransferbill_Summary,ErrorStrings.Export_Title_Paymentbeneficiarylist_Bankaccountnumber,ErrorStrings.Export_Title_Customertransferbill_Operator,ErrorStrings.Export_Title_AccountPeriod_Status,ErrorStrings.Export_Title_Customeraccount_InvoiceDate,ErrorStrings.Export_Title_AccountPeriod_CreatorName,ErrorStrings.Export_Title_AccountPeriod_CreateTime,ErrorStrings.Export_Title_AccountPeriod_ModifierName,ErrorStrings.Export_Title_AccountPeriod_ModifyTime,ErrorStrings.Export_Title_Credenceapplication_BusinessCode,
                                ErrorStrings.Export_Title_PaymentBill_Drawer,ErrorStrings.Export_Title_PaymentBill_IssueDate,ErrorStrings.Export_Title_PaymentBill_DueDate,ErrorStrings.Export_Title_PaymentBill_PayBank,ErrorStrings.Export_Title_PaymentBill_MoneyOrderNum
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }
    }
}
