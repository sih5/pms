﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Entities;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        public bool ExportSIHCreditInfo(int[] ids, string code, string companyCode, string companyName, string dealerCode, string dealerName, int? status, DateTime? bCreateTime, DateTime? eCreateTime, out string fileName) {
            //SIH授信(服务商)信用申请单.xlsx
            fileName = GetExportFilePath("SIH授信(服务商)信用申请单.xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);
                var userInfo = Utils.GetCurrentUserInfo();
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.Append(@"select a.Code,(select value from keyvalueitem where name='SIHCreditInfoStatus' and key=a.Status),a.ATotalFee,a.BTotalFee,a.ChangeAmount,
case when a.IsAttach =1 then cast('是' as varchar2(50)) else cast('否' as varchar2(50)) end,a.CreatorName,a.CreateTime,a.InitialApproverName,a.InitialApproveTime,a.CheckerName,a.CheckTime,a.ApproverName,a.ApproveTime,a.UpperApproverName,a.UpperCheckTime,
a.RejecterName,a.RejectTime,b.MarketingDepartmentCode,b.MarketingDepartmentName,b.CompanyCode,b.CompanyName,b.DealerCode,b.DealerName,b.AccountGroupCode,b.AccountGroupName,
b.ASIHCredit,b.BSIHCredit,b.ChangeAmount,b.ValidationTime,b.ExpireTime,(select value from keyvalueitem where name='SIHCreditInfoDetailStatus' and key=b.Status)
from SIHCreditInfo a inner join SIHCreditInfoDetail b on a.id= b.SIHCreditInfoId  where a.type=1 and (not exists (select 1 from MarketDptPersonnelRelation mr
                     where mr.personnelid = " + userInfo.Id + @" and mr.status=1) or exists  (select 1  from MarketDptPersonnelRelation mr           join SIHCreditInfoDetail gh  on mr.marketdepartmentid = gh.marketingdepartmentid   where gh.sihcreditinfoid = a.id    and mr.status=1     and mr.personnelid = " + userInfo.Id + @")) ");
                    var dbParameters = new List<DbParameter>();
                    if(ids != null && ids.Length > 0) {
                        sql.Append(" and a.id in (");
                        for(var i = 0; i < ids.Length; i++) {
                            if(ids.Length == i + 1) {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            } else {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                        }
                        sql.Append(")");
                    } else {
                        if(!string.IsNullOrEmpty(code)) {
                            sql.Append("and LOWER(a.code) like {0}code ");
                            dbParameters.Add(db.CreateDbParameter("code", "%" + code.ToLower() + "%"));
                        }
                        if(!string.IsNullOrEmpty(companyCode)) {
                            sql.Append(" and LOWER(b.companyCode) like {0}companyCode");
                            dbParameters.Add(db.CreateDbParameter("companyCode", "%" + companyCode.ToLower() + "%"));
                        }
                        if(!string.IsNullOrEmpty(companyName)) {
                            sql.Append(" and b.companyName like {0}companyName");
                            dbParameters.Add(db.CreateDbParameter("companyName", "%" + companyName + "%"));
                        }
                        if(!string.IsNullOrEmpty(dealerCode)) {
                            sql.Append(" and LOWER(b.dealerCode) like {0}dealerCode");
                            dbParameters.Add(db.CreateDbParameter("dealerCode", "%" + dealerCode.ToLower() + "%"));
                        }
                        if(!string.IsNullOrEmpty(dealerName)) {
                            sql.Append(" and b.dealerName like {0}dealerName");
                            dbParameters.Add(db.CreateDbParameter("dealerName", "%" + dealerName + "%"));
                        }
                        if(status.HasValue) {
                            sql.Append(@" and a.Status = {0}status ");
                            dbParameters.Add(db.CreateDbParameter("status", status.Value));
                        }
                        if(bCreateTime.HasValue) {
                            sql.Append(@" and a.createTime >=To_date({0}bCreateTime,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = bCreateTime.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("bCreateTime", tempTime.ToString("G")));
                        }
                        if(eCreateTime.HasValue) {
                            sql.Append(@" and a.createTime <=To_date({0}eCreateTime,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = eCreateTime.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("eCreateTime", tempTime.ToString("G")));
                        }
                    }
                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    "单据编号","状态","变更后总额","变更前总额","变更额度","是否上传附件","创建人","创建时间","初审人","初审时间","审核人","审核时间","审批人","审批时间",
                                    "高级审核人","高级核时间","驳回人","驳回时间","分销中心编号","分销中心名称","中心库编号","中心库名称","服务站编号","服务站名称","账户组编号","账户组名称","变更后SIH授信（服务商）","变更前SIH授信（服务商）",
                                    "变更额度","生效时间","失效时间","清单状态"
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }

        /// <summary>
        /// 导入信用申请单
        /// </summary>
        public bool ImportSIHCreditInfo(string fileName, out int excelImportNum, out List<SIHCreditInfoExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<SIHCreditInfoExtend>();
            var allList = new List<SIHCreditInfoExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("SIHCreditInfoDetail", out notNullableFields, out fieldLenght);

                List<string> notNullableFieldsCustomerCompany;
                Dictionary<string, int> fieldLenghtCustomerCompany;
                db.GetTableSchema("Company", out notNullableFieldsCustomerCompany, out fieldLenghtCustomerCompany);

                List<string> notNullableFieldsAccountGroup;
                Dictionary<string, int> fieldLenghtAccountGroup;
                db.GetTableSchema("AccountGroup", out notNullableFieldsAccountGroup, out fieldLenghtAccountGroup);


                List<object> excelColumns;
                List<SIHCreditInfoExtend> rightList;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    #region 指定文件中的列对应的名称
                    excelOperator.AddColumnDataSource("服务站编号", "DealerCode");
                    excelOperator.AddColumnDataSource("服务站名称", "DealerName");
                    excelOperator.AddColumnDataSource("本次SIH授信额度(服务商)", "ThisSIHCredit");
                    excelOperator.AddColumnDataSource("生效时间", "ValidationTime");
                    excelOperator.AddColumnDataSource("失效时间", "ExpireTime");
                    #endregion
                    var tempExcelOperator = excelOperator;
                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var tempImportObj = new SIHCreditInfoExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        #region 根据导入的字段给对应的实体属性赋值
                        tempImportObj.DealerCode = newRow["DealerCode"];
                        tempImportObj.DealerName = newRow["DealerName"];
                        tempImportObj.ThisSIHCreditStr = newRow["ThisSIHCredit"];
                        tempImportObj.ValidationTimeStr = newRow["ValidationTime"];
                        tempImportObj.ExpireTimeStr = newRow["ExpireTime"];
                        #endregion
                        var tempErrorMessage = new List<string>();
                        #region 导入的数据基本检查
                        //服务站编号检查
                        var fieldIndex = notNullableFields.IndexOf("DealerCode".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.DealerCode)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add("服务站编号不能为空");
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.DealerCode) > fieldLenghtCustomerCompany["Code".ToUpper()])
                                tempErrorMessage.Add("服务站编号过长");
                        }

                        //信用额度检查
                        fieldIndex = notNullableFields.IndexOf("ThisSIHCredit".ToUpper());
                        if(!string.IsNullOrEmpty(tempImportObj.ThisSIHCreditStr)) {
                            Decimal checkValue;
                            if(Decimal.TryParse(tempImportObj.ThisSIHCreditStr, out checkValue)) {
                                if(checkValue < 0) {
                                    tempErrorMessage.Add("本次SIH授信额度(服务商)必须大于0");
                                } else {
                                    tempImportObj.ThisSIHCredit = checkValue;
                                }
                            } else {
                                tempErrorMessage.Add("本次SIH授信额度(服务商)必须是数字");
                            }
                        } else {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add("本次SIH授信额度(服务商)不能为空");
                        }

                        //生效时间检查
                        fieldIndex = notNullableFields.IndexOf("ValidationTime".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.ValidationTimeStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add("生效时间不能为空");
                        } else {
                            DateTime checkValue;
                            if(!DateTime.TryParse(tempImportObj.ValidationTimeStr, out checkValue)) {
                                tempErrorMessage.Add("生效时间格式不正确");
                            } else {
                                tempImportObj.ValidationTime = checkValue;
                            }
                            if(tempImportObj.ValidationTime.Value.Date < DateTime.Now.Date) {
                                tempErrorMessage.Add("生效时间必须大于等于当前时间");
                            }
                        }

                        //失效时间检查
                        fieldIndex = notNullableFields.IndexOf("ExpireTime".ToUpper());
                        if(!string.IsNullOrEmpty(tempImportObj.ExpireTimeStr)) {
                            DateTime checkValue;
                            if(!DateTime.TryParse(tempImportObj.ExpireTimeStr, out checkValue)) {
                                tempErrorMessage.Add("失效时间格式不正确");
                            } else {
                                tempImportObj.ExpireTime = checkValue;
                            }
                            if(tempImportObj.ExpireTime.HasValue && tempImportObj.ValidationTime.Value.Date >= tempImportObj.ExpireTime.Value.Date) {
                                tempErrorMessage.Add("失效时间必须大于生效时间");
                            }
                        }
                        #endregion
                        if(tempErrorMessage.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });
                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    #region 导入内容业务检查
                    //文件内，服务站必须唯一
                    var groups = tempRightList.GroupBy(r => new {
                        r.DealerCode
                    }).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        groupItem.ErrorMsg = ErrorStrings.ImpPartsSupplierRelation_Validation31;
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    //校验服务站编号存在于企业表中
                    var companyCodes = tempRightList.Select(r => r.DealerCode).Distinct().ToArray();
                    var dbCompanys = new List<CompanyExtend>();
                    Func<string[], bool> getDbCompanys = value => {
                        var dbObj = new CompanyExtend {
                            Id = Convert.ToInt32(value[0]),
                            Code = value[1],
                            Name = value[2]
                        };
                        dbCompanys.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select Id,Code,Name from Company where status=1 ", "Code", false, companyCodes, getDbCompanys);
                    var errorCompanys = tempRightList.Where(r => !dbCompanys.Any(v => v.Code == r.DealerCode)).ToList();
                    foreach(var errorItem in errorCompanys) {
                        errorItem.ErrorMsg = String.Format("服务站编号 {0} 不存在", errorItem.DealerCode);
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    foreach(var rightItem in tempRightList) {
                        var tempCompany = dbCompanys.First(r => r.Code == rightItem.DealerCode);
                        rightItem.DealerId = tempCompany.Id;
                        rightItem.DealerName = tempCompany.Name;
                    }

                    //查找市场部id
                    var dealerIds = tempRightList.Select(r => r.DealerId.ToString()).Distinct().ToArray();
                    var dbDealerServiceInfos = new List<DealerServiceInfoExtend>();
                    Func<string[], bool> getDbDealerServiceInfos = values => {
                        var item = new DealerServiceInfoExtend {
                            Id = int.Parse(values[0]),
                            DealerId = int.Parse(values[1]),
                            MarketingDepartmentId = int.Parse(values[2]),
                        };
                        dbDealerServiceInfos.Add(item);
                        return false;
                    };
                    db.QueryDataWithInOperator(@"select Id,DealerId,MarketingDepartmentId from DealerServiceInfo  where status = 1 ", "DealerId", false, dealerIds, getDbDealerServiceInfos);
                    foreach(var item in tempRightList) {
                        var dealerServiceInfo = dbDealerServiceInfos.FirstOrDefault(r => r.DealerId == item.DealerId);
                        if(dealerServiceInfo == null) {
                            item.ErrorMsg = "服务站与分公司关系不存在有效数据";
                            continue;
                        }
                        item.MarketingDepartmentId = dealerServiceInfo.MarketingDepartmentId;
                    }

                    //查找赋值市场部信息
                    var marketingDepartmentIds = tempRightList.Select(r => r.MarketingDepartmentId.ToString()).Distinct().ToArray();
                    var dbMarketingDepartments = new List<MarketingDepartmentExtend>();
                    Func<string[], bool> getDbMarketingDepartments = values => {
                        var item = new MarketingDepartmentExtend {
                            Id = int.Parse(values[0]),
                            Code = values[1],
                            Name = values[2],
                        };
                        dbMarketingDepartments.Add(item);
                        return false;
                    };
                    db.QueryDataWithInOperator(@"select Id,Code,Name from MarketingDepartment ", "Id", false, marketingDepartmentIds, getDbMarketingDepartments);
                    foreach(var item in tempRightList) {
                        var marketingDepartment = dbMarketingDepartments.FirstOrDefault(r => r.Id == item.MarketingDepartmentId);
                        if(marketingDepartment == null) {
                            item.ErrorMsg = "营销分公司市场部不存在有效数据";
                            continue;
                        }
                        item.MarketingDepartmentCode = marketingDepartment.Code;
                        item.MarketingDepartmentName = marketingDepartment.Name;
                    }

                    //查找赋值服务站所属中心库信息
                    var dbAgencyDealerRelations = new List<AgencyDealerRelationExtend>();
                    Func<string[], bool> getDbAgencyDealerRelations = values => {
                        var item = new AgencyDealerRelationExtend {
                            Id = int.Parse(values[0]),
                            AgencyId = int.Parse(values[1]),
                            AgencyCode = values[2],
                            AgencyName = values[3],
                            DealerId = int.Parse(values[4]),
                            DealerCode = values[5]
                        };
                        dbAgencyDealerRelations.Add(item);
                        return false;
                    };
                    db.QueryDataWithInOperator(@"select Id,AgencyId,AgencyCode,AgencyName,DealerId,DealerCode from AgencyDealerRelation where status=1 ", "DealerId", false, dealerIds, getDbAgencyDealerRelations);
                    foreach(var item in tempRightList) {
                        var agencyDealerRelation = dbAgencyDealerRelations.FirstOrDefault(r => r.DealerId == item.DealerId);
                        if(agencyDealerRelation == null) {
                            item.ErrorMsg = "中心库与服务站隶属关系不存在有效数据";
                            continue;
                        }
                        item.CompanyId = agencyDealerRelation.AgencyId;
                        item.CompanyCode = agencyDealerRelation.AgencyCode;
                        item.CompanyName = agencyDealerRelation.AgencyName;
                    }

                    //查找赋值服务站所属中心库信息
                    var agencyIds = tempRightList.Select(r => r.CompanyId.ToString()).Distinct().ToArray();
                    var dbAccountGroups = new List<AccountGroupExtend>();
                    Func<string[], bool> getDbAccountGroups = values => {
                        var item = new AccountGroupExtend {
                            Id = int.Parse(values[0]),
                            Code = values[1],
                            Name = values[2],
                            SalesCompanyId = int.Parse(values[3])
                        };
                        dbAccountGroups.Add(item);
                        return false;
                    };
                    db.QueryDataWithInOperator(@"select Id,Code,Name,SalesCompanyId from AccountGroup where status=1 ", "SalesCompanyId", false, agencyIds, getDbAccountGroups);
                    foreach(var item in tempRightList) {
                        var accountGroup = dbAccountGroups.FirstOrDefault(r => r.SalesCompanyId == item.CompanyId);
                        if(accountGroup == null) {
                            item.ErrorMsg = "账户组不存在有效数据";
                            continue;
                        }
                        item.AccountGroupId = accountGroup.Id;
                        item.AccountGroupCode = accountGroup.Code;
                        item.AccountGroupName = accountGroup.Name;
                    }

                    //查找赋值客户账户信息
                    var dbCustomerAccounts = new List<CustomerAccountExtend>();
                    Func<string[], bool> getDbCustomerAccounts = values => {
                        var item = new CustomerAccountExtend {
                            Id = int.Parse(values[0]),
                            AccountGroupId = int.Parse(values[1]),
                            CustomerCompanyId = int.Parse(values[2]),
                            SIHCredit = string.IsNullOrWhiteSpace(values[3]) ? 0m : decimal.Parse(values[3]),};
                        dbCustomerAccounts.Add(item);
                        return false;
                    };
                    db.QueryDataWithInOperator(@"select Id,AccountGroupId,CustomerCompanyId,SIHCredit from CustomerAccount where status=1 ", "CustomerCompanyId", false, dealerIds, getDbCustomerAccounts);
                    foreach(var item in tempRightList) {
                        var customerAccount = dbCustomerAccounts.FirstOrDefault(r => r.AccountGroupId == item.AccountGroupId && r.CustomerCompanyId == item.DealerId);
                        if(customerAccount == null) {
                            item.ErrorMsg = "客户账户不存在有效数据";
                            continue;
                        }
                        item.AccountGroupId = customerAccount.AccountGroupId;
                        item.BSIHCredit = customerAccount.SIHCredit;
                    }
                    #endregion
                    //获取所有不合格数据
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    //获取所有合格数据
                    rightList = allList.Except(errorList).ToList();
                }
                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempObj = list[index - 1];
                            var values = new object[] {
                                #region 设置错误信息导出的列的值
                                tempObj.DealerCode, tempObj.DealerName,
                                tempObj.ThisSIHCreditStr ,tempObj.ValidationTimeStr ,
                                tempObj.ExpireTimeStr ,tempObj.ErrorMsg
                                #endregion                     
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }

                //导入所有合格数据
                if(!rightList.Any())
                    return true;
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    //开启事务，新增更新配件在一个事务内
                    var ts = conn.BeginTransaction();
                    try {
                        var userInfo = Utils.GetCurrentUserInfo();
                        if(rightList.Any()) {
                            var sqlInsertSIHCreditInfo = db.GetInsertSql("SIHCreditInfo", "Id", new[] {
                                "Code","ATotalFee","BTotalFee","ChangeAmount", "Status", "CreatorId", "CreatorName", "CreateTime","Type"
                            });
                            var sqlInsertDetail = db.GetInsertSql("SIHCreditInfoDetail", "Id", new[] {
                                "SIHCreditInfoId", "MarketingDepartmentId", "MarketingDepartmentCode", "MarketingDepartmentName", "CompanyId", "CompanyCode"
                                , "CompanyName" , "DealerId" , "DealerCode" , "DealerName" , "AccountGroupId" , "AccountGroupCode" , "AccountGroupName"
                                , "ASIHCredit", "BSIHCredit", "ChangeAmount", "ValidationTime", "ExpireTime"
                            });
                            var sihCreditInfos = rightList.GroupBy(r => new {
                                r.MarketingDepartmentId,
                            }).Select(r => new {
                                r.Key.MarketingDepartmentId
                            }).ToArray();
                            foreach(var item in sihCreditInfos) {
                                var sihCreditInfoLists = rightList.Where(r => r.MarketingDepartmentId == item.MarketingDepartmentId).ToArray();
                                var command = db.CreateDbCommand(sqlInsertSIHCreditInfo, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("Code", CodeGenerator.Generate(db, "SIHCreditInfo", userInfo.EnterpriseCode)));
                                command.Parameters.Add(db.CreateDbParameter("ATotalFee", sihCreditInfoLists.Sum(o => (o.ThisSIHCredit ?? 0))));
                                command.Parameters.Add(db.CreateDbParameter("BTotalFee", sihCreditInfoLists.Sum(o => (o.BSIHCredit ?? 0))));
                                command.Parameters.Add(db.CreateDbParameter("ChangeAmount", sihCreditInfoLists.Sum(o => (o.BSIHCredit ?? 0) - (o.ThisSIHCredit ?? 0))));
                                command.Parameters.Add(db.CreateDbParameter("Status", (int)DCSSIHCreditInfoStatus.新增));
                                command.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                command.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                command.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                command.Parameters.Add(db.CreateDbParameter("Type", (int)DCSSIHCreditInfoType.配件中心));
                                var tempId = db.ExecuteInsert(command, "Id");
                                foreach(var sihCreditInfoList in sihCreditInfoLists) {
                                    var detail = db.CreateDbCommand(sqlInsertDetail, conn, ts);
                                    detail.Parameters.Add(db.CreateDbParameter("SIHCreditInfoId", tempId));
                                    detail.Parameters.Add(db.CreateDbParameter("MarketingDepartmentId", sihCreditInfoList.MarketingDepartmentId));
                                    detail.Parameters.Add(db.CreateDbParameter("MarketingDepartmentCode", sihCreditInfoList.MarketingDepartmentCode));
                                    detail.Parameters.Add(db.CreateDbParameter("MarketingDepartmentName", sihCreditInfoList.MarketingDepartmentName));
                                    detail.Parameters.Add(db.CreateDbParameter("CompanyId", sihCreditInfoList.CompanyId));
                                    detail.Parameters.Add(db.CreateDbParameter("CompanyCode", sihCreditInfoList.CompanyCode));
                                    detail.Parameters.Add(db.CreateDbParameter("CompanyName", sihCreditInfoList.CompanyName));
                                    detail.Parameters.Add(db.CreateDbParameter("DealerId", sihCreditInfoList.DealerId));
                                    detail.Parameters.Add(db.CreateDbParameter("DealerCode", sihCreditInfoList.DealerCode));
                                    detail.Parameters.Add(db.CreateDbParameter("DealerName", sihCreditInfoList.DealerName));
                                    detail.Parameters.Add(db.CreateDbParameter("AccountGroupId", sihCreditInfoList.AccountGroupId));
                                    detail.Parameters.Add(db.CreateDbParameter("AccountGroupCode", sihCreditInfoList.AccountGroupCode));
                                    detail.Parameters.Add(db.CreateDbParameter("AccountGroupName", sihCreditInfoList.AccountGroupName));
                                    detail.Parameters.Add(db.CreateDbParameter("ASIHCredit", sihCreditInfoList.ThisSIHCredit));
                                    detail.Parameters.Add(db.CreateDbParameter("BSIHCredit", sihCreditInfoList.BSIHCredit));
                                    detail.Parameters.Add(db.CreateDbParameter("ChangeAmount", (sihCreditInfoList.BSIHCredit ?? 0) - (sihCreditInfoList.ThisSIHCredit ?? 0)));
                                    detail.Parameters.Add(db.CreateDbParameter("ValidationTime", sihCreditInfoList.ValidationTime));
                                    detail.Parameters.Add(db.CreateDbParameter("ExpireTime", sihCreditInfoList.ExpireTime));
                                    detail.ExecuteNonQuery();
                                }
                            }
                        }
                        //无异常提交
                        ts.Commit();
                    } catch(Exception) {
                        //报错回滚
                        ts.Rollback();
                        throw;
                    } finally {
                        if(conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
            }
        }
    }
}
