﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
	public partial class ExcelService {
		public bool ImportInternalAcquisitionDetail(int warehouseId, string fileName, out int excelImportNum, out List<InternalAcquisitionDetailExtend> errorData, out List<InternalAcquisitionDetailExtend> rightData, out string errorDataFileName, out string errorMessage) {
			excelImportNum = 0;
			errorMessage = "";
			errorDataFileName = "";
			var errorList = new List<InternalAcquisitionDetailExtend>();
			var rightList = new List<InternalAcquisitionDetailExtend>();
			var allList = new List<InternalAcquisitionDetailExtend>();
			try {
				if(!HttpContext.Current.User.Identity.IsAuthenticated)
					throw new Exception(ErrorStrings.File_Validation_User);
				var db = DbHelper.GetDbHelp(ConnectionString);

				//获取指定表结构
				List<string> notNullableFields;
				Dictionary<string, int> fieldLenght;
				db.GetTableSchema("InternalAcquisitionDetail", out notNullableFields, out fieldLenght);
				List<object> excelColumns;
				using(var excelOperator = new ExcelImport(Path.Combine(AttachmentFolder, GetImportFilePath(fileName)), ConnectionString)) {
					var columns = new Dictionary<string, string> {
						{ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode,"SparePartCode"},
						{ErrorStrings.Export_Title_WarehouseArea_Quantity,"Quantity"},
						{ErrorStrings.Export_Title_PartsBranch_Remark,"Remark"},
					};
					foreach(var column in columns) {
						excelOperator.AddColumnDataSource(column.Key, column.Value);
					}
					excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();

					#region 获取对应枚举 var keyValuePairs = new[] { new KeyValuePair<string, string>("PartABC", "ABCStrategy_Category")};                tempExcelOperator.ImportHelper.LoadEnumFromDb(null,null, keyValuePairs);

					#endregion

					excelImportNum = excelOperator.LoadExcelRow(row => {
						#region 根据导入的字段给对应的实体属性赋值
						var tempImportObj = new InternalAcquisitionDetailExtend {
							SparePartCodeStr = row["SparePartCode"] != null ? row["SparePartCode"].Trim() : "",
							QuantityStr = row["Quantity"] != null ? row["Quantity"].Trim() : "",
							RemarkStr = row["Remark"] != null ? row["Remark"].Trim() : ""
						};

						#endregion

						var tempErrorMessage = new List<string>();

						#region 导入的数据基本检查
						//配件编号检查
						var fieldIndex = notNullableFields.IndexOf("SparePartCode".ToUpper());
						if(string.IsNullOrEmpty(tempImportObj.SparePartCodeStr)) {
							if(fieldIndex > -1)
								tempErrorMessage.Add(ErrorStrings.ImpPartsPurchaseOrderDetail_Validation1);
						} else {
							if(Encoding.Default.GetByteCount(tempImportObj.SparePartCodeStr) > fieldLenght["SparePartCode".ToUpper()])
								tempErrorMessage.Add(ErrorStrings.ImpPartsPurchaseOrderDetail_Validation2);
						}

						//数量检查
						fieldIndex = notNullableFields.IndexOf("Quantity".ToUpper());
						if(string.IsNullOrEmpty(tempImportObj.QuantityStr)) {
							if(fieldIndex > -1)
								tempErrorMessage.Add(ErrorStrings.Export_Validation_InternalAcquisitionDetail_QuantityIsNull);
						} else {
							int checkValue;
							if(int.TryParse(tempImportObj.QuantityStr, out checkValue)) {
								if(checkValue <= 0) {
									tempErrorMessage.Add(ErrorStrings.Export_Validation_InternalAcquisitionDetail_QuantityOverZero);
								}
							} else {
								tempErrorMessage.Add(ErrorStrings.Export_Validation_InternalAcquisitionDetail_QuantityNumber);
							}
						}

						//备注检查
						fieldIndex = notNullableFields.IndexOf("Remark".ToUpper());
						if(string.IsNullOrEmpty(tempImportObj.RemarkStr)) {
							if(fieldIndex > -1)
								tempErrorMessage.Add(ErrorStrings.ImpPartsPurchasePricingDetail_Validation15);
						} else {
							if(Encoding.Default.GetByteCount(tempImportObj.RemarkStr) > fieldLenght["Remark".ToUpper()])
								tempErrorMessage.Add(ErrorStrings.ImpPartsPurchasePricingDetail_Validation16);
						}

						#endregion

						if(tempErrorMessage.Count > 0) {
							tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
						}
						allList.Add(tempImportObj);
						return false;
					});

					//校验导入的数据配件编号重复
					var groups = allList.GroupBy(r =>
						r.SparePartCodeStr
					).Where(r => r.Count() > 1);
					foreach(var groupItem in groups.SelectMany(r => r)) {
						if(groupItem.ErrorMsg == null) {
							groupItem.ErrorMsg = ErrorStrings.Export_Title_PartsBranch_Code + groupItem.SparePartCodeStr + ErrorStrings.Export_Validation_PartsBranch_Validation5;
						} else {
							groupItem.ErrorMsg = groupItem.ErrorMsg + ErrorStrings.Export_Validation_PartsBranch_Validation6 + groupItem.SparePartCodeStr + ErrorStrings.Export_Validation_PartsBranch_Validation5;
						}
					}


					var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

					#region 剩下的数据进行业务检查
					//1、配件有效性校验（配件信息.配件编号=导入数据.配件编号,配件信息.配件名称=导入数据.配件名称），如果校验不通过，记录校验错误信息：配件不存在。
					var partCodesNeedCheck = tempRightList.Select(r => r.SparePartCodeStr).Distinct().ToArray();
					var dbSparePartCodes = new List<SparePartExtend>();
					Func<string[], bool> getDbSparePartCodes = value => {
						var dbObj = new SparePartExtend {
							Id = Convert.ToInt32(value[0]),
							Code = value[1],
							Name = value[2],
							MeasureUnit = value[3]
						};
						dbSparePartCodes.Add(dbObj);
						return false;
					};
					db.QueryDataWithInOperator("select Id,trim(Code),trim(Name),MeasureUnit from SparePart where status=1 ", "Code", true, partCodesNeedCheck, getDbSparePartCodes);
					tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
					foreach(var temp in tempRightList) {
						var dbvalue = dbSparePartCodes.FirstOrDefault(r => r.Code == temp.SparePartCodeStr);
						if(dbvalue == null) {
							temp.ErrorMsg = ErrorStrings.Export_Validation_InternalAcquisitionDetail_Validation1;
							continue;
						}
						temp.SparePartId = dbvalue.Id;
						temp.SparePartName = dbvalue.Name;
						temp.MeasureUnit = dbvalue.MeasureUnit;
					}
					tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();
					//2.查询销售组织仓库关系关联销售组织（销售组织仓库关系.仓库Id=领出仓库Id,销售组织.id=销售组织仓库关系.销售组织Id）返回销售组织.配件销售类型Id
					//3.查询配件计划价（配件计划价.隶属企业Id=登陆企业Id，配件计划价.配件销售类型Id=返回的配件销售类型Id,配件Id=配件信息.Id）
					var sparePartIdsNeedCheck = tempRightList.Select(r => r.SparePartId.ToString(CultureInfo.InvariantCulture)).Distinct().ToArray();
					var dbPartsPlannedPrices = new List<PartsPlannedPriceExtend>();
					Func<string[], bool> getDbPartsPlannedPrices = value => {
						var dbObj = new PartsPlannedPriceExtend {
							SparePartId = Convert.ToInt32(value[0]),
							PlannedPrice = Convert.ToDecimal(value[1])
						};
						dbPartsPlannedPrices.Add(dbObj);
						return false;
					};
					db.QueryDataWithInOperator(string.Format(@"select c.SparePartId, c.PlannedPrice
															   from PartsPlannedPrice c
															   where c.OwnerCompanyId ={0}
																	 and c.PartsSalesCategoryId =
																		   (select b.PartsSalesCategoryId
																			  from SalesUnitAffiWarehouse a
																			 inner join salesunit b
																				on a.salesunitid = b.id
																			 where a.warehouseid = {1})", Utils.GetCurrentUserInfo().EnterpriseId, warehouseId), "SparePartId", false, sparePartIdsNeedCheck, getDbPartsPlannedPrices);

					foreach(var temp in tempRightList) {
						var partsPlannedPriceExtend = dbPartsPlannedPrices.FirstOrDefault(r => r.SparePartId == temp.SparePartId);
						if(partsPlannedPriceExtend == null) {
							temp.ErrorMsg = ErrorStrings.Export_Validation_InternalAcquisitionDetail_Validation2;
						} else {
							temp.UnitPrice = partsPlannedPriceExtend.PlannedPrice;
						}
					}
					//查询配件销售价,取基准价
					tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();
					var dbSalesPrices = new List<Object>();
					Func<string[], bool> getDbSalesPrices = value => {
						var dbObj = new {
							SparePartId = Convert.ToInt32(value[0]),
							SalesPrice = Convert.ToDecimal(value[1])
						};
						dbSalesPrices.Add(dbObj);
						return false;
					};
					db.QueryDataWithInOperator(string.Format(@"select c.sparepartid, c.salesprice from partssalesprice c where c.branchid = {0}  and c.status = {1} and c.pricetype={2}
															   and c.partssalescategoryid in (select b.partssalescategoryid from salesunitaffiwarehouse a inner join salesunit b on a.salesunitid = b.id where a.warehouseid = {3} and b.status = {4})",
															 Utils.GetCurrentUserInfo().EnterpriseId, (int)DcsBaseDataStatus.有效, (int)DcsPartsSalesPricePriceType.基准销售价, warehouseId, (int)DcsMasterDataStatus.有效),
															 "SparePartId", false, sparePartIdsNeedCheck, getDbSalesPrices);
					foreach(var temp in tempRightList) {
						var tempEntities = dbSalesPrices.Where(r => Convert.ToInt32(r.GetType().GetProperty("SparePartId").GetValue(r, new object[] { })) == temp.SparePartId).ToArray();
						if(tempEntities.Length != 1) {
							temp.ErrorMsg = ErrorStrings.Export_Validation_InternalAcquisitionDetail_Validation3;
						} else {
							var entity = tempEntities[0];
							temp.SalesPrice = Convert.ToDecimal(entity.GetType().GetProperty("SalesPrice").GetValue(entity, new object[] { }));
						}
					}
					//5.新增配件信息时，将该配件在分品牌商务信息的ErrorStrings.Export_Title_PartsBranch_IsOrderableNew，如果该字段为ErrorStrings.Export_Title_PartsBranch_Yes，无需处理；如果为ErrorStrings.Export_Title_PartsBranch_No，将该条配件清单信息显现为红色
					tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
					var partIdsNeedCheck = tempRightList.Select(r => r.SparePartId.ToString()).Distinct().ToArray();
					var dbPartsBranchs = new List<PartsBranchExtend>();
					Func<string[], bool> getDbPartsBranchs = value => {
						var dbObj = new PartsBranchExtend {
							PartId = Convert.ToInt32(value[0]),
							IsOrderable = value[1] == "1"
						};
						dbPartsBranchs.Add(dbObj);
						return false;
					};
					db.QueryDataWithInOperator("select PartId,IsOrderable from PartsBranch where status=1 ", "PartId", true, partIdsNeedCheck, getDbPartsBranchs);
					foreach(var temp in tempRightList) {
						var tempEntities = dbPartsBranchs.Where(r => Convert.ToInt32(r.GetType().GetProperty("PartId").GetValue(r, new object[] { })) == temp.SparePartId).ToArray();
						if(tempEntities.Length < 1) {
							temp.ErrorMsg = ErrorStrings.Export_Validation_InternalAcquisitionDetail_Validation4;
						} else {
							var entity = tempEntities[0];
							temp.IsOrderable = Convert.ToInt32(entity.GetType().GetProperty("IsOrderable").GetValue(entity, new object[] { }));
						}
					}
					#endregion

					//获取所有不合格数据
					errorList = allList.Where(r => r.ErrorMsg != null).ToList();
					//获取所有合格数据
					rightList = allList.Except(errorList).ToList();

					#region 将合格数据的值填上
					foreach(var rightItem in rightList) {
						rightItem.SparePartCode = rightItem.SparePartCodeStr;
						rightItem.Quantity = Convert.ToInt32(rightItem.QuantityStr);
						rightItem.Remark = rightItem.RemarkStr;
					}
					#endregion
				}
				//导出所有不合格数据
				if(errorList.Any()) {
					excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
					errorDataFileName = GetErrorFilePath(fileName);
					using(var excelExport = new ExcelExport(Path.Combine(AttachmentFolder, errorDataFileName))) {
						var list = errorList;
						excelExport.ExportByRow(index => {
							if(index == list.Count + 1)
								return null;
							if(index == 0)
								return excelColumns.ToArray();
							var tempObj = list[index - 1];
							var values = new object[] {
								#region 设置错误信息导出的列的值
								tempObj.SparePartCodeStr,
								tempObj.QuantityStr,
								tempObj.RemarkStr,
								tempObj.ErrorMsg
								#endregion
							};
							return values;
						});
					}
					errorList = null;
				}
				return true;
			} catch(Exception ex) {
				errorMessage = ex.Message;
				errorList = null;
				return false;
			} finally {
				errorData = errorList;
				rightData = rightList;
			}
		}


		//导出内部领入信息
		public bool ExportInternalAcquisitionAndDetail(int[] ids, string code, int? warehouseid, string createname, string departmentName, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName) {
			fileName = GetExportFilePath(string.Format("内部领入信息_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
			try {
				if(!HttpContext.Current.User.Identity.IsAuthenticated)
					throw new Exception(ErrorStrings.File_Validation_User);


				var userinfo = Utils.GetCurrentUserInfo();

				var db = DbHelper.GetDbHelp(ConnectionString);

				using(var conn = db.CreateDbConnection()) {
					var sql = new StringBuilder();
					sql.AppendFormat(@"SELECT 
									  a.Code,
									  a.BranchName,
									  a.WarehouseName,
									  a.DepartmentName,    
									  a.TotalAmount,
									  a.Reason,
									  a.Operator,
									  a.RequestedDeliveryTime,
									  p.name,
									  (select value from keyvalueitem where NAME = 'WorkflowOfSimpleApproval_Status'and key=a.Status) As Status,
									  a.Remark,
									  a.CreatorName,
									  a.CreateTime,  
									  a.ModifierName,
									  a.ModifyTime,
									  a.ApproverName,
									  a.ApproveTime,
									  a.AbandonerName,
									  a.AbandonTime,
									  b.serialnumber,
									  b.sparepartcode,
									  b.sparepartname,
                                      c.ReferenceCode,
									  b.quantity,
									  b.unitprice,
									  b.salesprice,
									  b.measureunit,
									  b.remark
									  FROM InternalAcquisitionBill a
									   left join PartsPurchaseOrderType p
											on a.PartsPurchaseOrderTypeId=p.Id
									  left join InternalAcquisitionDetail b on a.id = b.internalacquisitionbillid
                                      left join SparePart c on b.SparePartId=c.id
		 
where a.BranchId = {0} ", userinfo.EnterpriseId);
					var dbParameters = new List<DbParameter>();
					#region 条件过滤
					if(ids != null && ids.Length > 0) {
						sql.Append(" and a.id in (");
						for(var i = 0; i < ids.Length; i++) {
							if(ids.Length == i + 1) {
								sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture));
								dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
							} else {
								sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture) + ",");
								dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
							}
						}
						sql.Append(")");
					} else {
						if(!String.IsNullOrEmpty(code)) {
							sql.Append(@" and a.code like {0}code ");
							dbParameters.Add(db.CreateDbParameter("code", "%" + code + "%"));
						}
						if(warehouseid.HasValue) {
							sql.Append(@" and a.warehouseid = {0}warehouseid ");
							dbParameters.Add(db.CreateDbParameter("warehouseid", warehouseid.Value));
						}
						if(!String.IsNullOrEmpty(createname)) {
							sql.Append(@" and a.createname like {0}createname ");
							dbParameters.Add(db.CreateDbParameter("createname", "%" + createname + "%"));
						}
						if(!String.IsNullOrEmpty(departmentName)) {
							sql.Append(@" and a.departmentName like {0}departmentName ");
							dbParameters.Add(db.CreateDbParameter("departmentName", "%" + departmentName + "%"));
						}
						if(status.HasValue) {
							sql.Append(@" and a.status = {0}status ");
							dbParameters.Add(db.CreateDbParameter("status", status.Value));
						}
						if(createTimeBegin.HasValue) {
							sql.Append(@" and a.createTime>=to_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
							var tempValue = createTimeBegin.Value;
							var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
							dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
						}
						if(createTimeEnd.HasValue) {
							sql.Append(@" and a.createTime <=to_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
							var tempValue = createTimeEnd.Value;
							var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
							dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
						}

					}
					#endregion

					var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
					command.Parameters.AddRange(dbParameters.ToArray());
					conn.Open();
					var reader = command.ExecuteReader();
					using(var excelExport = new ExcelExport(fileName)) {
						excelExport.ExportByRow(index => {
							if(index == 0) {
								return new object[] {
							   "领入单编号",ErrorStrings.Export_Title_Branch_BranchName,"领入仓库","供货部门名称",ErrorStrings.Export_Title_InternalAllocationBill_TotalAmount,"领入原因",ErrorStrings.Export_Title_Customertransferbill_Operator,ErrorStrings.Export_Title_PartsPurchaseOrder_RequestedDeliveryTime,ErrorStrings.Export_Title_PartsPurchaseOrder_OrderType,ErrorStrings.Export_Title_AccountPeriod_Status,ErrorStrings.Export_Title_PartsBranch_Remark,ErrorStrings.Export_Title_AccountPeriod_CreatorName,ErrorStrings.Export_Title_AccountPeriod_CreateTime,ErrorStrings.Export_Title_AccountPeriod_ModifierName,ErrorStrings.Export_Title_AccountPeriod_ModifyTime,ErrorStrings.Export_Title_PackingPropertyApp_ApproverName,ErrorStrings.Export_Title_PackingPropertyApp_ApproveTime,ErrorStrings.Export_Title_PartsBranch_AbandonerName,ErrorStrings.Export_Title_PartsBranch_AbandonerTime,ErrorStrings.Export_Title_InternalAllocationBill_SerialNumber,ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode,ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName,ErrorStrings.Export_Title_PartsBranch_Referencecode,ErrorStrings.Export_Title_WarehouseArea_Quantity,ErrorStrings.Export_Title_InternalAllocationBill_UnitPrice,ErrorStrings.Export_Title_DealerPartsStockQueryView_SalesPrice,ErrorStrings.Export_Title_PackingPropertyApp_MeasureUnit,ErrorStrings.Export_Title_PartsBranch_Remark
								};
							}
							if(reader.Read()) {
								var values = new object[reader.FieldCount];
								var num = reader.GetValues(values);
								if(num != reader.FieldCount) {
									throw new Exception(ErrorStrings.Export_Validation_DataError);
								}
								return values;
							}
							return null;
						});
					}
					reader.Close();
				}
				return true;
			} catch(Exception) {
				return false;
			}
		}

		//导出内部领出信息
		public bool ExportInternalallocationbillAndDetail(int[] ids, string code, int? warehouseid, string createname, string departmentName, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd,int? type, out string fileName) {
			fileName = GetExportFilePath(string.Format("内部领出信息_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
			try {
				if(!HttpContext.Current.User.Identity.IsAuthenticated)
					throw new Exception(ErrorStrings.File_Validation_User);


				var userinfo = Utils.GetCurrentUserInfo();

				var db = DbHelper.GetDbHelp(ConnectionString);

				using(var conn = db.CreateDbConnection()) {
					var sql = new StringBuilder();
					sql.AppendFormat(@"SELECT 
									  a.Code,
									  a.BranchName,
									  a.WarehouseName,
									  a.DepartmentName,    
									  a.TotalAmount,
									  a.Reason,
									  a.Operator,
									  (select value from keyvalueitem where NAME = 'WorkflowOfSimpleApproval_Status'and key=a.Status) As Status,
                                      (select value from keyvalueitem where NAME = 'InternalAllocationBill_Type'and key=a.Type) As Type,
                                      a.SourceCode,
									  a.Remark,
									  a.CreatorName,
									  a.CreateTime,  
									  a.ModifierName,
									  a.ModifyTime,
									  a.ApproverName,
									  a.ApproveTime,
									  a.AbandonerName,
									  a.AbandonTime,
									  b.serialnumber,
									  b.sparepartcode,
									  b.sparepartname,
									  b.quantity,
									  b.unitprice,
									  --b.salesprice,
									  b.measureunit,
									  b.remark
									  FROM InternalAllocationBill a
									  left join InternalAllocationDetail b on a.id = b.InternalAllocationBillId     
where a.BranchId = {0} ", userinfo.EnterpriseId);
					var dbParameters = new List<DbParameter>();
					#region 条件过滤
					if(ids != null && ids.Length > 0) {
						sql.Append(" and a.id in (");
						for(var i = 0; i < ids.Length; i++) {
							if(ids.Length == i + 1) {
								sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture));
								dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
							} else {
								sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture) + ",");
								dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
							}
						}
						sql.Append(")");
					} else {
						if(!String.IsNullOrEmpty(code)) {
							sql.Append(@" and a.code like {0}code ");
							dbParameters.Add(db.CreateDbParameter("code", "%" + code + "%"));
						}
						if(warehouseid.HasValue) {
							sql.Append(@" and a.warehouseid = {0}warehouseid ");
							dbParameters.Add(db.CreateDbParameter("warehouseid", warehouseid.Value));
						}
						if(!String.IsNullOrEmpty(createname)) {
							sql.Append(@" and a.createname like {0}createname ");
							dbParameters.Add(db.CreateDbParameter("createname", "%" + createname + "%"));
						}
						if(!String.IsNullOrEmpty(departmentName)) {
							sql.Append(@" and a.departmentName like {0}departmentName ");
							dbParameters.Add(db.CreateDbParameter("departmentName", "%" + departmentName + "%"));
						}
						if(status.HasValue) {
							sql.Append(@" and a.status = {0}status ");
							dbParameters.Add(db.CreateDbParameter("status", status.Value));
						}
						if(createTimeBegin.HasValue) {
							sql.Append(@" and a.createTime>=to_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
							var tempValue = createTimeBegin.Value;
							var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
							dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
						}
						if(createTimeEnd.HasValue) {
							sql.Append(@" and a.createTime <=to_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
							var tempValue = createTimeEnd.Value;
							var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
							dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
						}
                        if(type.HasValue) {
                            sql.Append(@" and a.type = {0}type ");
                            dbParameters.Add(db.CreateDbParameter("type", type.Value));
                        }

					}
					#endregion

					var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
					command.Parameters.AddRange(dbParameters.ToArray());
					conn.Open();
					var reader = command.ExecuteReader();
					using(var excelExport = new ExcelExport(fileName)) {
						excelExport.ExportByRow(index => {
							if(index == 0) {
								return new object[] {
							   ErrorStrings.Export_Title_InternalAllocationBill_Code,ErrorStrings.Export_Title_Branch_BranchName,ErrorStrings.Export_Title_InternalAllocationBill_WarehouseName,ErrorStrings.Export_Title_InternalAllocationBill_DepartmentName,
                               ErrorStrings.Export_Title_InternalAllocationBill_TotalAmount,ErrorStrings.Export_Title_InternalAllocationBill_Reason,ErrorStrings.Export_Title_Customertransferbill_Operator,ErrorStrings.Export_Title_AccountPeriod_Status,ErrorStrings.Export_Title_MarketingDepartment_Type,ErrorStrings.Export_Title_PartsOuterPurchaseChange_SourceCode,ErrorStrings.Export_Title_PartsBranch_Remark,ErrorStrings.Export_Title_AccountPeriod_CreatorName,ErrorStrings.Export_Title_AccountPeriod_CreateTime,ErrorStrings.Export_Title_AccountPeriod_ModifierName,ErrorStrings.Export_Title_AccountPeriod_ModifyTime,
                               ErrorStrings.Export_Title_InternalAllocationBill_ApproverName,ErrorStrings.Export_Title_InternalAllocationBill_ApproveTime,ErrorStrings.Export_Title_PartsBranch_AbandonerName,ErrorStrings.Export_Title_PartsBranch_AbandonerTime,
                               ErrorStrings.Export_Title_InternalAllocationBill_SerialNumber,ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode,ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName,ErrorStrings.Export_Title_WarehouseArea_Quantity,
                               ErrorStrings.Export_Title_InternalAllocationBill_UnitPrice,ErrorStrings.Export_Title_PackingPropertyApp_MeasureUnit,ErrorStrings.Export_Title_PartsBranch_Remark
								};
							}
							if(reader.Read()) {
								var values = new object[reader.FieldCount];
								var num = reader.GetValues(values);
								if(num != reader.FieldCount) {
									throw new Exception(ErrorStrings.Export_Validation_DataError);
								}
								return values;
							}
							return null;
						});
					}
					reader.Close();
				}
				return true;
			} catch(Exception) {
				return false;
			}
		}


	}
}
