﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        public bool ImportInternalAllocationDetail(int warehouseId, string fileName, out int excelImportNum, out List<InternalAllocationDetailExtend> errorData, out List<InternalAllocationDetailExtend> rightData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<InternalAllocationDetailExtend>();
            var rightList = new List<InternalAllocationDetailExtend>();
            var allList = new List<InternalAllocationDetailExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("InternalAllocationDetail", out notNullableFields, out fieldLenght);
                List<object> excelColumns;
                using(var excelOperator = new ExcelImport(Path.Combine(AttachmentFolder, GetImportFilePath(fileName)), ConnectionString)) {
                    var columns = new Dictionary<string, string> {
                        {ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode,"SparePartCode"},
                        {ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName,"SparePartName"},
                        {ErrorStrings.Export_Title_WarehouseArea_Quantity,"Quantity"},
                        {ErrorStrings.Export_Title_PartsBranch_Remark,"Remark"},
                    };
                    foreach(var column in columns) {
                        excelOperator.AddColumnDataSource(column.Key, column.Value);
                    }
                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();

                    #region 获取对应枚举 var keyValuePairs = new[] { new KeyValuePair<string, string>("PartABC", "ABCStrategy_Category")};                tempExcelOperator.ImportHelper.LoadEnumFromDb(null,null, keyValuePairs);

                    #endregion

                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        #region 根据导入的字段给对应的实体属性赋值
                        var tempImportObj = new InternalAllocationDetailExtend {
                            SparePartCodeStr = row["SparePartCode"] != null ? row["SparePartCode"].Trim() : "",
                            SparePartNameStr = row["SparePartName"] != null ? row["SparePartName"].Trim() : "",
                            QuantityStr = row["Quantity"] != null ? row["Quantity"].Trim() : "",
                            RemarkStr = row["Remark"] != null ? row["Remark"].Trim() : ""
                        };


                        #endregion

                        var tempErrorMessage = new List<string>();

                        #region 导入的数据基本检查
                        //配件编号检查
                        var fieldIndex = notNullableFields.IndexOf("SparePartName".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.SparePartNameStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchaseOrderDetail_Validation1);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.SparePartNameStr) > fieldLenght["SparePartName".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchaseOrderDetail_Validation2);
                        }

                        //配件名称检查
                        fieldIndex = notNullableFields.IndexOf("SparePartCode".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.SparePartCodeStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Sparepart_Name);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.SparePartCodeStr) > fieldLenght["SparePartCode".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Sparepart_NameIsLong);
                        }

                        //数量检查
                        fieldIndex = notNullableFields.IndexOf("Quantity".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.QuantityStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_InternalAcquisitionDetail_QuantityIsNull);
                        } else {
                            int checkValue;
                            if(int.TryParse(tempImportObj.QuantityStr, out checkValue)) {
                                if(checkValue <= 0) {
                                    tempErrorMessage.Add(ErrorStrings.Export_Validation_InternalAcquisitionDetail_QuantityOverZero);
                                }
                            } else {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_InternalAcquisitionDetail_QuantityNumber);
                            }
                        }

                        //备注检查
                        fieldIndex = notNullableFields.IndexOf("Remark".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.RemarkStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchasePricingDetail_Validation15);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.RemarkStr) > fieldLenght["Remark".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchasePricingDetail_Validation16);
                        }

                        #endregion

                        if(tempErrorMessage.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });
                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    #region 剩下的数据进行业务检查
                    //1、配件有效性校验（配件信息.配件编号=导入数据.配件编号,配件信息.配件名称=导入数据.配件名称），如果校验不通过，记录校验错误信息：配件不存在。
                    var partCodesNeedCheck = tempRightList.Select(r => r.SparePartCodeStr).Distinct().ToArray();
                    var dbSparePartCodes = new List<SparePartExtend>();
                    Func<string[], bool> getDbSparePartCodes = value => {
                        var dbObj = new SparePartExtend {
                            Id = Convert.ToInt32(value[0]),
                            Code = value[1],
                            Name = value[2],
                            MeasureUnit = value[3]
                        };
                        dbSparePartCodes.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select Id,trim(Code),trim(Name),MeasureUnit from SparePart where status=1 ", "Code", true, partCodesNeedCheck, getDbSparePartCodes);
                    var errorSpareParts = tempRightList.Where(r => !dbSparePartCodes.Any(v => v.Code == r.SparePartCodeStr && v.Name == r.SparePartNameStr)).ToList();
                    foreach(var errorItem in errorSpareParts) {
                        errorItem.ErrorMsg = ErrorStrings.Export_Validation_InternalAcquisitionDetail_Validation1;
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    foreach(var temp in tempRightList) {
                        var dbvalue = dbSparePartCodes.First(r => r.Code == temp.SparePartCodeStr && r.Name == temp.SparePartNameStr);
                        temp.SparePartId = dbvalue.Id;
                        temp.MeasureUnit = dbvalue.MeasureUnit;
                    }
                    //增加文件内数据重复校验
                    var groups = tempRightList.GroupBy(r => new {
                        r.SparePartId
                    }).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        groupItem.ErrorMsg = ErrorStrings.ImpPartsSupplierRelation_Validation31;
                    }
                    //2.查询销售组织仓库关系关联销售组织（销售组织仓库关系.仓库Id=领出仓库Id,销售组织.id=销售组织仓库关系.销售组织Id）返回销售组织.配件销售类型Id
                    //3.查询配件计划价（配件计划价.隶属企业Id=登陆企业Id，配件计划价.配件销售类型Id=返回的配件销售类型Id,配件Id=配件信息.Id）
                    var sparePartIdsNeedCheck = tempRightList.Select(r => r.SparePartId.ToString(CultureInfo.InvariantCulture)).Distinct().ToArray();
                    var dbPartsPlannedPrices = new List<PartsPlannedPriceExtend>();
                    Func<string[], bool> getDbPartsPlannedPrices = value => {
                        var dbObj = new PartsPlannedPriceExtend {
                            SparePartId = Convert.ToInt32(value[0]),
                            PlannedPrice = Convert.ToDecimal(value[1])
                        };
                        dbPartsPlannedPrices.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator(string.Format(@"select c.SparePartId, c.CostPrice
                                                               from EnterprisePartsCost c
                                                               where c.OwnerCompanyId ={0}", Utils.GetCurrentUserInfo().EnterpriseId), "SparePartId", false, sparePartIdsNeedCheck, getDbPartsPlannedPrices);
                    foreach(var temp in tempRightList) {
                        var cost = dbPartsPlannedPrices.FirstOrDefault(r => r.SparePartId == temp.SparePartId);
                        if (cost != null)
                            temp.UnitPrice = cost.PlannedPrice;
                        else
                            temp.ErrorMsg = ErrorStrings.Export_Validation_InternalAcquisitionDetail_Validation5;
                    }
                    //查询配件销售价,取基准价
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();
                    var dbSalesPrices = new List<Object>();
                    Func<string[], bool> getDbSalesPrices = value => {
                        var dbObj = new {
                            SparePartId = Convert.ToInt32(value[0]),
                            SalesPrice = Convert.ToDecimal(value[1])
                        };
                        dbSalesPrices.Add(dbObj);
                        return false;
                    };
//                    db.QueryDataWithInOperator(string.Format(@"select c.sparepartid, c.salesprice from partssalesprice c where c.branchid = {0}  and c.status = {1} and c.pricetype={2}
//                                                               and c.partssalescategoryid in (select b.partssalescategoryid from salesunitaffiwarehouse a inner join salesunit b on a.salesunitid = b.id where a.warehouseid = {3} and b.status = {4})",
//                                                             Utils.GetCurrentUserInfo().EnterpriseId, (int)DcsBaseDataStatus.有效, (int)DcsPartsSalesPricePriceType.基准销售价, warehouseId, (int)DcsMasterDataStatus.有效),
//                                                             "SparePartId", false, sparePartIdsNeedCheck, getDbSalesPrices);
//                    foreach(var temp in tempRightList) {
//                        var tempEntities = dbSalesPrices.Where(r => Convert.ToInt32(r.GetType().GetProperty("SparePartId").GetValue(r, new object[] { })) == temp.SparePartId).ToArray();
//                        if(tempEntities.Length != 1) {
//                            temp.ErrorMsg = ErrorStrings.Export_Validation_InternalAcquisitionDetail_Validation3;
//                        } else {
//                            var entity = tempEntities[0];
//                            temp.SalesPrice = Convert.ToDecimal(entity.GetType().GetProperty("SalesPrice").GetValue(entity, new object[] { }));
//                        }
//                    }
                    #endregion

                    //获取所有不合格数据
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    //获取所有合格数据
                    rightList = allList.Except(errorList).ToList();

                    #region 将合格数据的值填上
                    foreach(var rightItem in rightList) {
                        rightItem.SparePartCode = rightItem.SparePartCodeStr;
                        rightItem.SparePartName = rightItem.SparePartNameStr;
                        rightItem.Quantity = Convert.ToInt32(rightItem.QuantityStr);
                        rightItem.Remark = rightItem.RemarkStr;
                    }
                    #endregion
                }
                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(Path.Combine(AttachmentFolder, errorDataFileName))) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempObj = list[index - 1];
                            var values = new object[] {
                                #region 设置错误信息导出的列的值
                                tempObj.SparePartCodeStr,
                                tempObj.SparePartNameStr,
                                tempObj.QuantityStr,
                                tempObj.RemarkStr,
                                tempObj.ErrorMsg
                                #endregion
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }
    }
}
