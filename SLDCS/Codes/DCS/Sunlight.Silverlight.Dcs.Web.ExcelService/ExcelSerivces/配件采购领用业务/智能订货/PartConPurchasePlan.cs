﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        /// <summary>
        /// 校验导入修改配件常规采购计划
        /// </summary>
        public bool ImportUpdatePartConPurchasePlan(int branchId, string fileName, out int excelImportNum, out List<PartConPurchasePlanExtend> rightData, out List<PartConPurchasePlanExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<PartConPurchasePlanExtend>();
            var rightList = new List<PartConPurchasePlanExtend>();
            var allList = new List<PartConPurchasePlanExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("PartConPurchasePlan", out notNullableFields, out fieldLenght);
                List<object> excelColumns;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Partssalescategory_Name, "PartsSalesCategoryName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Code, "PartCode");
                    excelOperator.AddColumnDataSource("时间", "Date");
                    excelOperator.AddColumnDataSource("建议采购量", "AdvicePurchase");
                    excelOperator.AddColumnDataSource("实际下达供应商编码", "ActualSupplierCode");
                    excelOperator.AddColumnDataSource("实际订货仓库", "ActualOrderWarehouse");
                    excelOperator.AddColumnDataSource("实际采购量", "ActualPurchase");
                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var tempImportObj = new PartConPurchasePlanExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        #region
                        tempImportObj.PartsSalesCategoryName = newRow["PartsSalesCategoryName"];
                        tempImportObj.PartCode = newRow["PartCode"];
                        tempImportObj.AdvicePurchaseStr = newRow["AdvicePurchase"];
                        tempImportObj.ActualPurchaseStr = newRow["ActualPurchase"];
                        tempImportObj.ActualSupplierCode = newRow["ActualSupplierCode"];
                        tempImportObj.ActualOrderWarehouse = newRow["ActualOrderWarehouse"];
                        tempImportObj.DateStr = newRow["Date"];
                        #endregion
                        var tempErrorMessage = new List<string>();
                        #region 导入的内容基本检查
                        //品牌检查
                        var fieldIndex = notNullableFields.IndexOf("PartsSalesCategoryName".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.PartsSalesCategoryName)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_MarketingDepartment_PartsSalesCategoryIsNull);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.PartsSalesCategoryName) > fieldLenght["PartsSalesCategoryName".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_MarketingDepartment_PartsSalesCategoryIsLong);
                        }
                        //配件编号
                        fieldIndex = notNullableFields.IndexOf("PartCode".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.PartCode)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.OverstockPartsAppDetail__Validation1);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.PartCode) > fieldLenght["PartCode".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.ImpPartsSupplierRelation_Validation8);
                        }
                        //实际下达供应商编码
                        if(string.IsNullOrEmpty(tempImportObj.ActualSupplierCode)) {
                            tempErrorMessage.Add("实际下达供应商编码不能为空");
                        }
                        //实际订货仓库
                        if(string.IsNullOrEmpty(tempImportObj.ActualOrderWarehouse)) {
                            tempErrorMessage.Add("实际订货仓库不能为空");
                        }
                        //时间检查
                        if(string.IsNullOrEmpty(tempImportObj.DateStr)) {
                            tempErrorMessage.Add("时间不能为空");
                        } else {
                            try {
                                DateTime checkValue;
                                if(!DateTime.TryParse(tempImportObj.DateStr, out checkValue)) {
                                    tempErrorMessage.Add("时间格式不正确");
                                }
                            } catch(Exception) {
                                tempErrorMessage.Add("时间格式不正确");
                            }
                        }
                        //建议采购量
                        if(!string.IsNullOrEmpty(tempImportObj.AdvicePurchaseStr)) {
                            int checkValue;
                            if(int.TryParse(tempImportObj.AdvicePurchaseStr, out checkValue)) {
                                if(checkValue <= 0) {
                                    tempErrorMessage.Add("建议采购量必须大于0");
                                }
                            } else {
                                tempErrorMessage.Add("建议采购量必须是数字");
                            }
                        } else {
                            tempErrorMessage.Add("建议采购量不能为空");
                        }
                        //实际采购量
                        if(!string.IsNullOrEmpty(tempImportObj.ActualPurchaseStr)) {
                            int checkValue;
                            if(int.TryParse(tempImportObj.ActualPurchaseStr, out checkValue)) {
                                if(checkValue <= 0) {
                                    tempErrorMessage.Add("实际采购量必须大于0");
                                }
                            } else {
                                tempErrorMessage.Add("实际采购量必须是数字");
                            }
                        } else {
                            tempErrorMessage.Add("实际采购量不能为空");
                        }
                        #endregion
                        if(tempErrorMessage.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });
                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    #region 导入内容业务检查
                    var groups = tempRightList.GroupBy(r => new {
                        r.DateStr,
                        r.PartCode,
                        r.PartsSalesCategoryName
                    }).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        groupItem.ErrorMsg = ErrorStrings.ImpPartsSupplierRelation_Validation31;
                    }
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();
                    //校验品牌是否存在于当前分公司
                    var partsSalesCategoryNamesNeedCheck = tempRightList.Select(r => r.PartsSalesCategoryName).Distinct().ToArray();
                    var dbPartsSalesCategories = new List<PartConPurchasePlanExtend>();
                    Func<string[], bool> getDbPartsSalesCategories = value => {
                        var dbObj = new PartConPurchasePlanExtend {
                            PartsSalesCategoryId = Convert.ToInt32(value[0]),
                            PartsSalesCategoryName = value[1],
                            BranchId = Convert.ToInt32(value[2])
                        };
                        dbPartsSalesCategories.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select Id,Name,branchid from PartsSalesCategory where status=1 ", "Name", true, partsSalesCategoryNamesNeedCheck, getDbPartsSalesCategories);
                    foreach(var errorItem in tempRightList) {
                        var oldSparePart = dbPartsSalesCategories.FirstOrDefault(v => v.PartsSalesCategoryName == errorItem.PartsSalesCategoryName && v.BranchId == branchId);
                        if(oldSparePart == null) {
                            errorItem.ErrorMsg = String.Format("品牌“{0}”在品牌信息中不存在", errorItem.PartsSalesCategoryName);
                        } else {
                            errorItem.PartsSalesCategoryId = oldSparePart.PartsSalesCategoryId;
                        }
                    }
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();
                    //校验配件图号
                    var partCodesNeedCheck = tempRightList.Select(r => r.PartCode.ToUpper()).Distinct().ToArray();
                    var dbSparePartCodes = new List<PartConPurchasePlanExtend>();
                    Func<string[], bool> getDbSparePartCodes = value => {
                        var dbObj = new PartConPurchasePlanExtend {
                            PartId = Convert.ToInt32(value[0]),
                            PartCode = value[1]
                        };
                        dbSparePartCodes.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select id,Trim(Code),Trim(Name) from sparePart where status=1 ", "Code", true, partCodesNeedCheck, getDbSparePartCodes);
                    foreach(var errorItem in tempRightList) {
                        var oldSparePart = dbSparePartCodes.FirstOrDefault(v => v.PartCode == errorItem.PartCode);
                        if(oldSparePart == null) {
                            errorItem.ErrorMsg = String.Format(ErrorStrings.Export_Validation_PartsBranch_SparePartIsNull, errorItem.PartCode);
                        } else {
                            errorItem.PartId = oldSparePart.PartId;
                            //给时间赋值
                            errorItem.Date = Convert.ToDateTime(errorItem.DateStr);
                        }
                    }
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();

                    //校验实际下达供应商
                    var actualSupplierCodeCheck = tempRightList.Select(r => r.ActualSupplierCode).Distinct().ToArray();
                    var dbActualSupplierCode = new List<PartConPurchasePlanExtend>();
                    Func<string[], bool> getActualSupplierCodes = value => {
                        var dbObj = new PartConPurchasePlanExtend {
                            PartId = Convert.ToInt32(value[0]),
                            ActualSupplierId = Convert.ToInt32(value[1]),
                            ActualSupplierCode = value[2],
                            PartsSalesCategoryId = Convert.ToInt32(value[3])
                        };
                        dbActualSupplierCode.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator(string.Format("select a.PartId,a.SupplierId,b.Code,a.PartsSalesCategoryId from PartsSupplierRelation a inner join PartsSupplier b on a.SupplierId=b.id where a.STATUS=1 and a.branchid={0}", branchId), "b.Code", true, actualSupplierCodeCheck, getActualSupplierCodes);
                    foreach(var errorItem in tempRightList) {
                        var oldSupplier = dbActualSupplierCode.FirstOrDefault(v => v.ActualSupplierCode == errorItem.ActualSupplierCode && v.PartId == errorItem.PartId && v.PartsSalesCategoryId == errorItem.PartsSalesCategoryId);
                        if(oldSupplier == null) {
                            errorItem.ErrorMsg = String.Format("编号为“{0}”的供应商不存在", errorItem.ActualSupplierCode);
                        } else {
                            errorItem.ActualSupplierId = oldSupplier.ActualSupplierId;
                        }
                    }
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();
                    //校验实际订货仓库
                    var actualOrderWarehousesNeedCheck = tempRightList.Select(r => r.ActualOrderWarehouse).Distinct().ToArray();
                    var dbWarehouses = new List<PartConPurchasePlanExtend>();
                    Func<string[], bool> getDbWarehouses = value => {
                        var dbObj = new PartConPurchasePlanExtend {
                            ActualOrderWarehouseId = Convert.ToInt32(value[0]),
                            ActualOrderWarehouse = value[1],
                            PartsSalesCategoryId = Convert.ToInt32(value[2])
                        };
                        dbWarehouses.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator(string.Format("Select a.id,a.name as Name,c.partssalescategoryid From warehouse a Inner Join Salesunitaffiwarehouse b On a.id=b.warehouseid Inner Join salesunit c On b.salesunitid=c.id Where a.storagecompanytype=1 And a.status=1 And c.ownercompanyid={0}", branchId), "a.Name", true, actualOrderWarehousesNeedCheck, getDbWarehouses);
                    foreach(var errorItem in tempRightList) {
                        var oldWarehouse = dbWarehouses.FirstOrDefault(v => v.ActualOrderWarehouse == errorItem.ActualOrderWarehouse && v.PartsSalesCategoryId == errorItem.PartsSalesCategoryId);
                        if(oldWarehouse == null) {
                            errorItem.ErrorMsg = String.Format("名称为“{0}”的仓库不存在", errorItem.ActualOrderWarehouse);
                        } else {
                            errorItem.ActualOrderWarehouseId = oldWarehouse.ActualOrderWarehouseId;
                        }
                    }
                    //查询所有合格数据是否存在数据库中 提示 配件常规采购计划不存在  
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();
                    var partIdNeedCheck = tempRightList.Select(r => r.PartId.ToString()).Distinct().ToArray();
                    var dbPartConPurchasePlans = new List<PartConPurchasePlanExtend>();
                    Func<string[], bool> getDbPartConPurchasePlans = value => {
                        var dbObj = new PartConPurchasePlanExtend {
                            Id = Convert.ToInt32(value[0]),
                            PartId = Convert.ToInt32(value[1]),
                            PartsSalesCategoryId = Convert.ToInt32(value[2]),
                            Date = Convert.ToDateTime(value[3])
                        };
                        dbPartConPurchasePlans.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select Id,PartId,PartsSalesCategoryId,DateT from PartConPurchasePlan ", "PartId", true, partIdNeedCheck, getDbPartConPurchasePlans);
                    foreach(var tempObj in tempRightList) {
                        var tempobjcheck = dbPartConPurchasePlans.FirstOrDefault(r => r.PartId == tempObj.PartId && r.PartsSalesCategoryId == tempObj.PartsSalesCategoryId && r.Date == tempObj.Date);
                        if(tempobjcheck == null) {
                            tempObj.ErrorMsg = "配件常规采购计划不存在";
                        } else {
                            tempObj.Id = tempobjcheck.Id;
                        }
                    }
                    #endregion
                    //获取所有不合格数据
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    //获取所有合格数据
                    rightList = allList.Except(errorList).ToList();
                    //将合格数据的值填上
                    foreach(var rightItem in rightList) {
                        try {
                            rightItem.ActualPurchase = Convert.ToInt32(rightItem.ActualPurchaseStr);
                        } catch {
                            rightItem.ActualPurchase = null;
                        }
                        try {
                            rightItem.AdvicePurchase = Convert.ToInt32(rightItem.AdvicePurchaseStr);
                        } catch {
                            rightItem.AdvicePurchase = null;
                        }
                    }
                }

                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempObj = list[index - 1];
                            var values = new object[] {
                                tempObj.PartsSalesCategoryName, tempObj.PartCode, tempObj.DateStr, tempObj.AdvicePurchaseStr,tempObj.ActualSupplierCode,tempObj.ActualOrderWarehouse, tempObj.ActualPurchaseStr,tempObj.ErrorMsg};
                            return values;
                        });
                    }
                    errorList = null;
                }

                //更新导入所有合格数据
                if(!rightList.Any())
                    return true;
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    //开启事务，新增更新配件在一个事务内
                    var ts = conn.BeginTransaction();
                    try {
                        //新增配件
                        if(rightList.Any()) {
                            //获取更新数据的sql语句，Id为主键
                            var sqlUpdate = db.GetUpdateSql("PartConPurchasePlan", new[] {
                                "AdvicePurchase","ActualSupplierId","ActualSupplierCode","ActualOrderWarehouseId","ActualOrderWarehouse","ActualPurchase","ModifierId","ModifierName","ModifyTime"}, new[] { "Id" });

                            var userInfo = Utils.GetCurrentUserInfo();
                            //更新配件常规采购计划
                            foreach(var item in rightList) {
                                var command = db.CreateDbCommand(sqlUpdate, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("AdvicePurchase", item.AdvicePurchase));
                                command.Parameters.Add(db.CreateDbParameter("ActualSupplierId", item.ActualSupplierId));
                                command.Parameters.Add(db.CreateDbParameter("ActualSupplierCode", item.ActualSupplierCode));
                                command.Parameters.Add(db.CreateDbParameter("ActualOrderWarehouseId", item.ActualOrderWarehouseId));
                                command.Parameters.Add(db.CreateDbParameter("ActualOrderWarehouse", item.ActualOrderWarehouse));
                                command.Parameters.Add(db.CreateDbParameter("ActualPurchase", item.ActualPurchase));
                                command.Parameters.Add(db.CreateDbParameter("ModifierId", userInfo.Id));
                                command.Parameters.Add(db.CreateDbParameter("ModifierName", userInfo.Name));
                                command.Parameters.Add(db.CreateDbParameter("ModifyTime", DateTime.Now));
                                command.Parameters.Add(db.CreateDbParameter("Id", item.Id));
                                command.ExecuteNonQuery();
                            }
                        }
                        //无异常提交
                        ts.Commit();
                    } catch(Exception) {
                        //报错回滚
                        ts.Rollback();
                        throw;
                    } finally {
                        if(conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }

        public bool ExportPartConPurchasePlan(int[] ids, int? partsSalesCategoryId, string partCode, string partName, int? partAbc, string primarySupplierCode, int? pruductLifeCycle, int? plannedPriceCategory, DateTime? dateBegin, DateTime? dateEnd, out string fileName) {
            fileName = GetExportFilePath("配件常规采购计划.xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated) {
                    throw new Exception(ErrorStrings.File_Validation_User);
                }
                var db = DbHelper.GetDbHelp(ConnectionString);
                var userInfo = Utils.GetCurrentUserInfo();
                using(var conn = db.CreateDbConnection()) {

                    conn.Open();
                    var sql = new StringBuilder();
                    sql.Append(string.Format(@"SELECT 
                                Extent1.PartCode,
                                Extent1.PartName,
                                Extent1.PartsSalesCategoryName,
                                Extent1.PrimarySupplierCode,
                                Extent1.DFOrderWarehouse,
                                (select value from keyvalueitem where NAME = 'ABCStrategy_Category'and key=Extent1.PartABC) As Partabc,
                                (select value from keyvalueitem where NAME = 'PlanPriceCategory_PriceCategory'and key=Extent1.PlannedPriceType) As PlannedPriceType,
                                (select value from keyvalueitem where NAME = 'PlanPriceCategory_PriceCategory'and key=Extent1.PruductLifeCycle) As PruductLifeCycle,
                                Extent1.Near12MOutFrequency,
                                Extent1.PlannedPrice,
                                Extent1.OrderQuantity1,
                                Extent1.OrderQuantity2,
                                Extent1.OrderQuantity3,
                                Extent1.OrderQuantityFor3M,
                                Extent1.OrderQuantityFor6M,
                                Extent1.OrderQuantityFor12M,
                                Extent1.DayAvgFor1M,
                                Extent1.DayAvgFor3M,
                                Extent1.DayAvgFor6M,
                                Extent1.DayAvgFor12M,
                                Extent1.Average,
                                Extent1.ValidStock,
                                Extent1.PurchaseInLine,
                                Extent1.TransferInLine,
                                Extent1.ReStock,
                                Extent1.RePurchaseInLine,
                                Extent1.ExStock,
                                Extent1.ExPurchaseInLine,
                                Extent1.SalesPending,
                                Extent1.PurchaseToBeConfirm,
                                Extent1.ConArrivalCycle,
                                Extent1.OrderCycle,
                                Extent1.EconomicalBatch,
                                Extent1.MinPurchaseQuantity,
                                Extent1.TheoryMaxStock,
                                Extent1.PurchaseMethod1,
                                Extent1.PurchaseMethod2,
                                Extent1.PurchaseMethod3,
                                Extent1.PurchaseMethod4,
                                Extent1.AdvicePurchase,
                                Extent1.ActualSupplierCode,
                                Extent1.ActualOrderWarehouse,
                                Extent1.ActualPurchase,
                                Extent1.Datet
                                FROM PartConPurchasePlan Extent1
                                WHERE Extent1.BranchId ={0}", userInfo.EnterpriseId));
                    var dbParameters = new List<DbParameter>();

                    if(ids != null && ids.Length > 0) {
                        var idStr = string.Join(",", ids);
                        sql.Append(" and Extent1.id in (" + idStr + ")");
                    } else {
                        if(!string.IsNullOrEmpty(partCode)) {
                            sql.Append(@" and LOWER(Extent1.partCode) like {0}partCode ");
                            dbParameters.Add(db.CreateDbParameter("partCode", "%" + partCode.ToLower() + "%"));
                        }
                        if(!string.IsNullOrEmpty(partName)) {
                            sql.Append(@" and LOWER(Extent1.partName) like {0}partName ");
                            dbParameters.Add(db.CreateDbParameter("partName", "%" + partName.ToLower() + "%"));
                        }
                        if(partAbc.HasValue) {
                            sql.Append(@" and Extent1.partABC={0}partABC");
                            dbParameters.Add(db.CreateDbParameter("partABC", partAbc.Value));
                        }
                        if(!string.IsNullOrEmpty(primarySupplierCode)) {
                            sql.Append(@" and LOWER(Extent1.primarySupplierCode) like {0}primarySupplierCode ");
                            dbParameters.Add(db.CreateDbParameter("primarySupplierCode", "%" + primarySupplierCode.ToLower() + "%"));
                        }
                        if(pruductLifeCycle.HasValue) {
                            sql.Append(@" and Extent1.PruductLifeCycle ={0}pruductLifeCycle");
                            dbParameters.Add(db.CreateDbParameter("pruductLifeCycle", pruductLifeCycle.Value));
                        }
                        if(plannedPriceCategory.HasValue) {
                            sql.Append(@" and Extent1.PlannedPriceCategory ={0}plannedPriceCategory");
                            dbParameters.Add(db.CreateDbParameter("plannedPriceCategory", plannedPriceCategory.Value));
                        }
                        if(partsSalesCategoryId.HasValue) {
                            sql.Append(@" and Extent1.partsSalesCategoryId ={0}partsSalesCategoryId");
                            dbParameters.Add(db.CreateDbParameter("partsSalesCategoryId", partsSalesCategoryId.Value));
                        }
                        if(dateBegin.HasValue) {
                            sql.Append(" and Extent1.DateT>=to_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = dateBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, 18);
                            dbParameters.Add(db.CreateDbParameter("theDate", tempTime.ToString("d")));
                        }
                        if(dateEnd.HasValue) {
                            sql.Append(" and Extent1.DateT<=to_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = dateEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                    }

                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    ErrorStrings.Export_Title_PartsBranch_Code,ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName,ErrorStrings.Export_Title_Partssalescategory_Name,"首选供应商编码",ErrorStrings.Export_Title_PartsSupplierRelation_DefaultOrderWarehouseName,ErrorStrings.Export_Title_PartsBranch_PartABC,"生命周期分类","计划价分类","近12月出库频次",ErrorStrings.Export_Title_PartsPlannedPrice_PlannedPrice,"T-1月销量","T-2月销量","T-3月销量","近3个月销量","近6个月销量","近12个月销量","上月日均","近3月日均","近6月日均","近12月日均","近52周均","有效库存","采购在途","调拨在途","替换件库存"," 替换件采购在途","互换件库存","互换件采购在途","销售待审量","采购待确认量","常规到货周期","订货周期",ErrorStrings.Export_Title_PartsSupplierRelation_EconomicalBatch,"最小采购量","理论最大库存","方法一采购量","方法二采购量","方法三采购量","方法四采购量","建议采购量","实际下达供应商编码","实际订货仓库","实际采购量","时间"};
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }

    }
}
