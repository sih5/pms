﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Globalization;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        /// <summary>
        /// 导出配件外采申请主清单
        /// </summary>
        public bool ExportPartsOuterPurchaseChangeWithDetail(int[] ids, string partsOuterPurchaseChangeCode, string customerCompanyName, int? partsSalesCategoryrId, int? marketingdepartmentId, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName) {
            fileName = GetExportFilePath("配件外采申请主清单_" + ".xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);
                var userInfo = Utils.GetCurrentUserInfo();
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.Append(string.Format(@"Select a.Code,
                                               a.Customercompanycode,
                                               a.Customercompanynace,
                                               m.Name,
                                               a.BranchName,
                                               b.Name,
                                               cast(decode(a.Status,1,'新建',2,'提交',3,'生效',4,'初审通过',5,'审核通过',99,'作废',6,'确认',7,'高级审核',8,'高级审批') as varchar2(20)) as Status,
                                               a.Amount,
                                               a.SourceCode,
                                               a.SourceCategoryr,
                                               cast(decode(a.Outerpurchasecomment,1,'抱怨客户',2,'配件代理库无配件',3,'CDC配件库无配件',4,'停车待件',5,'装饰件',6,'标准件',7,'索赔单作废',10,'保养品') as varchar2(20)) as  Outerpurchasecomment,
                                               
                                               a.Creatorname,
                                               a.Createtime,
                                               a.Modifiername,
                                               a.Modifytime,
                                               a.InitialApproverName,a.InitialApproveTime,a.CheckerName,a.CheckTime,
                                               a.Approvername,
                                               a.Approvetime,
                                               a.AbandonerName,
                                               a.AbandonTime,
                                               a.RejecterName,
                                               a.RejectTime,
                                               a.Remark,a.Approvecomment,
                                               a.Abandoncomment,
                                               a.Rejectcomment 
                                          From Partsouterpurchasechange a
                                         left Join PartsSalesCategory b
                                            On a.PartsSalesCategoryrId = b.Id
                                         left join Dealerserviceinfo d on a.Customercompanyid=d.Dealerid and d.Partssalescategoryid = a.partssalescategoryrid and d.Status=1
                                         left join Marketingdepartment m on d.Marketingdepartmentid = m.id
                                        left join Branch c on a.BranchId=c.Id where 1=1  and (exists (
 select 1 from  DealerServiceInfo e
		INNER JOIN MarketDptPersonnelRelation f ON e.MarketingDepartmentId = f.MarketDepartmentId and f.personnelid = {0} where f.status<>99 and a.CustomerCompanyId = e.DealerId AND a.PartsSalesCategoryrId = e.PartsSalesCategoryId
 ) or 
  not exists (
    select id from MarketDptPersonnelRelation where personnelid = {1} and status <> 99
   )
 )", userInfo.Id, userInfo.Id));
                    var dbParameters = new List<DbParameter>();
                    if(ids != null && ids.Length > 0) {
                        sql.Append(" and a.id in (");
                        for(var i = 0; i < ids.Length; i++) {
                            if(ids.Length == i + 1) {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            } else {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                        }
                        sql.Append(")");
                    } else {
                        if(!String.IsNullOrEmpty(partsOuterPurchaseChangeCode)) {
                            sql.Append(@" and a.Code like {0}partsOuterPurchaseChangeCode ");
                            dbParameters.Add(db.CreateDbParameter("partsOuterPurchaseChangeCode", "%" + partsOuterPurchaseChangeCode + "%"));
                        }
                        if(!String.IsNullOrEmpty(customerCompanyName)) {
                            sql.Append(@" and a.Customercompanynace like {0}customerCompanyName ");
                            dbParameters.Add(db.CreateDbParameter("customerCompanyName", "%" + customerCompanyName + "%"));
                        }
                        if(partsSalesCategoryrId.HasValue) {
                            sql.Append(@" and a.PartsSalesCategoryrId = {0}partsSalesCategoryrId ");
                            dbParameters.Add(db.CreateDbParameter("partsSalesCategoryrId", partsSalesCategoryrId.Value));
                        }
                        if(marketingdepartmentId.HasValue) {
                            sql.Append(@" and m.Id = {0}marketingdepartmentId ");
                            dbParameters.Add(db.CreateDbParameter("marketingdepartmentId", marketingdepartmentId.Value));
                        }
                        if(status.HasValue) {
                            sql.Append(@" and a.status = {0}status ");
                            dbParameters.Add(db.CreateDbParameter("status", status.Value));
                        }
                        if(createTimeBegin.HasValue) {
                            sql.Append(@" and a.createTime >=to_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(@" and a.createTime <=to_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                    }
                    var sqlStr = sql.ToString();
                    if(sqlStr.IndexOf(" where 1=1  and ") > 0)
                        sqlStr = sqlStr.Replace(" where 1=1  and ", " where ");
                    else
                        sqlStr = sqlStr.Replace(" where 1=1 ", "");
                    var command = db.CreateDbCommand(string.Format(sqlStr, db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    ErrorStrings.Export_Title_PartsOuterPurchaseChange_Code, ErrorStrings.Export_Title_Company_Code, ErrorStrings.Export_Title_Company_Name, ErrorStrings.Export_Title_Agency_MarketingDepartmentName,ErrorStrings.Export_Title_Branch_BranchName,ErrorStrings.Export_Title_Partssalescategory_Name, ErrorStrings.Export_Title_AccountPeriod_Status,ErrorStrings.Export_Title_InternalAllocationBill_TotalAmount,
                                    ErrorStrings.Export_Title_PartsOuterPurchaseChange_SourceCode,ErrorStrings.Export_Title_PartsOuterPurchaseChange_SourceCategoryr,ErrorStrings.Export_Title_PartsOuterPurchaseChange_Reason,ErrorStrings.Export_Title_AccountPeriod_CreatorName,ErrorStrings.Export_Title_AccountPeriod_CreateTime,ErrorStrings.Export_Title_AccountPeriod_ModifierName,ErrorStrings.Export_Title_AccountPeriod_ModifyTime,"初审人","初审时间","审核人","审核时间",ErrorStrings.Export_Title_PackingPropertyApp_ApproverName,ErrorStrings.Export_Title_PackingPropertyApp_ApproveTime,ErrorStrings.Export_Title_PartsBranch_AbandonerName, ErrorStrings.Export_Title_PartsBranch_AbandonerTime, 
                                    ErrorStrings.Export_Title_PartsOuterPurchaseChange_RejecterName,ErrorStrings.Export_Title_PartsOuterPurchaseChange_RejectTime,ErrorStrings.Export_Title_PartsBranch_Remark,ErrorStrings.Export_Title_PartsOuterPurchaseChange_ApproveAmont,ErrorStrings.Export_Title_PartsOuterPurchaseChange_AbandonComment,ErrorStrings.Export_Title_PartsOuterPurchaseChange_RejectComment
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }

        /// <summary>
        /// 合并导出配件外采申请主清单
        /// </summary>
        public bool ExportForPartsOuterPurchaseChangeWithDetail(int[] ids, string partsOuterPurchaseChangeCode, string customerCompanyName, int? partsSalesCategoryrId, int? marketingdepartmentId, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName) {
            fileName = GetExportFilePath("配件外采申请主清单_" + ".xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);
                var userInfo = Utils.GetCurrentUserInfo();

                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.Append(string.Format(@" Select a.Code,
                                               a.Customercompanycode,
                                               a.Customercompanynace,
                                               m.Name,
                                               a.Partssalescategoryname,
                                               cast(decode(a.Status,1,'新建',2,'提交',3,'生效',4,'初审通过',5,'审核通过',99,'作废') as varchar2(20)) as Status,
                                               a.Amount,
                                               cast(decode(a.Outerpurchasecomment,1,'抱怨客户',2,'配件代理库无配件',3,'CDC配件库无配件',4,'停车待件',5,'装饰件',6,'标准件',7,'索赔单作废',10,'保养品') as varchar2(20)) as  Outerpurchasecomment,
                                               
                                               a.Creatorname,
                                               a.Createtime,
                                               a.Modifiername,
                                               a.Modifytime,
                                               a.InitialApproverName,a.InitialApproveTime,a.CheckerName,a.CheckTime,
                                               a.Approvername,
                                               a.Approvetime,
                                               b.Partscode,
                                               b.Partsname,
                                               b.Tradeprice,
                                               b.Quantity,
                                               b.Supplier,
                                               a.Remark,a.Approvecomment,
                                               a.Abandoncomment,
                                               a.Rejectcomment
                                          From Partsouterpurchasechange a
                                         Inner Join Partsouterpurchaselist b   On b.Partsouterpurchasechangeid = a.Id
                                           Left join PartsSalesCategory c ON a.PartsSalesCategoryrId = c.Id
                                               left join Dealerserviceinfo d1 on a.Customercompanyid=d1.Dealerid and d1.Partssalescategoryid = a.partssalescategoryrid and d1.Status=1
                                         left join Marketingdepartment m on d1.Marketingdepartmentid = m.id
                                          LEFT  JOIN Branch d ON a.BranchId = d.Id
                                            where 1=1 and (exists (
 select 1 from  DealerServiceInfo e
		INNER JOIN MarketDptPersonnelRelation f ON e.MarketingDepartmentId = f.MarketDepartmentId and f.personnelid = {0} where f.status<>99 and a.CustomerCompanyId = e.DealerId AND a.PartsSalesCategoryrId = e.PartsSalesCategoryId
 ) or 
  not exists (
    select id from MarketDptPersonnelRelation where personnelid = {1} and status <> 99
   )
 )", userInfo.Id, userInfo.Id));
                    var dbParameters = new List<DbParameter>();
                    if(ids != null && ids.Length > 0) {
                        sql.Append(" and a.id in (");
                        for(var i = 0; i < ids.Length; i++) {
                            if(ids.Length == i + 1) {
                                sql.Append("{0}" + "id" + ids[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            } else {
                                sql.Append("{0}" + "id" + ids[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                        }
                        sql.Append(")");
                    } else {
                        if(!String.IsNullOrEmpty(partsOuterPurchaseChangeCode)) {
                            sql.Append(@" and a.Code like {0}partsOuterPurchaseChangeCode ");
                            dbParameters.Add(db.CreateDbParameter("partsOuterPurchaseChangeCode", "%" + partsOuterPurchaseChangeCode + "%"));
                        }
                        if(!String.IsNullOrEmpty(customerCompanyName)) {
                            sql.Append(@" and a.Customercompanynace like {0}customerCompanyName ");
                            dbParameters.Add(db.CreateDbParameter("customerCompanyName", "%" + customerCompanyName + "%"));
                        }
                        if(partsSalesCategoryrId.HasValue) {
                            sql.Append(@" and a.PartsSalesCategoryrId = {0}partsSalesCategoryrId ");
                            dbParameters.Add(db.CreateDbParameter("partsSalesCategoryrId", partsSalesCategoryrId.Value));
                        }
                        if(marketingdepartmentId.HasValue) {
                            sql.Append(@" and m.Id = {0}marketingdepartmentId ");
                            dbParameters.Add(db.CreateDbParameter("marketingdepartmentId", marketingdepartmentId.Value));
                        }
                        if(status.HasValue) {
                            sql.Append(@" and a.status = {0}status ");
                            dbParameters.Add(db.CreateDbParameter("status", status.Value));
                        }
                        if(createTimeBegin.HasValue) {
                            sql.Append(@" and a.createTime >=to_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(@" and a.createTime <=to_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                    }
                    var sqlStr = sql.ToString();
                    if(sqlStr.IndexOf(" where 1=1  and ") > 0)
                        sqlStr = sqlStr.Replace(" where 1=1  and ", " where ");
                    else
                        sqlStr = sqlStr.Replace(" where 1=1 ", "");
                    var command = db.CreateDbCommand(string.Format(sqlStr, db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    ErrorStrings.Export_Title_PartsOuterPurchaseChange_Code, ErrorStrings.Export_Title_Company_Code, ErrorStrings.Export_Title_Company_Name,ErrorStrings.Export_Title_Agency_MarketingDepartmentName, ErrorStrings.Export_Title_Partssalescategory_Name, ErrorStrings.Export_Title_AccountPeriod_Status,ErrorStrings.Export_Title_InternalAllocationBill_TotalAmount,ErrorStrings.Export_Title_PartsOuterPurchaseChange_Reason,
                                    ErrorStrings.Export_Title_AccountPeriod_CreatorName,ErrorStrings.Export_Title_AccountPeriod_CreateTime,ErrorStrings.Export_Title_AccountPeriod_ModifierName,ErrorStrings.Export_Title_AccountPeriod_ModifyTime,"初审人","初审时间","审核人","审核时间",ErrorStrings.Export_Title_PackingPropertyApp_ApproverName,ErrorStrings.Export_Title_PackingPropertyApp_ApproveTime,
                                    ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode, ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName, ErrorStrings.Export_Title_PartsOuterPurchaseChange_TradePrice,ErrorStrings.Export_Title_WarehouseArea_Quantity,ErrorStrings.Export_Title_PartsOuterPurchaseChange_Supplier,ErrorStrings.Export_Title_PartsBranch_Remark,ErrorStrings.Export_Title_PartsOuterPurchaseChange_ApproveAmont,ErrorStrings.Export_Title_PartsOuterPurchaseChange_AbandonComment,ErrorStrings.Export_Title_PartsOuterPurchaseChange_RejectComment
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }



    }
}