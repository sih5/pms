﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        public bool ExportBranchSupplierRelation(int[] ids, int? branchId, string supplierCode, string supplierName, string bussinessCode, string bussinessName, int? partsSalesCategoryId, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName) {
            var userinfo = Utils.GetCurrentUserInfo();
            //供应商编号,供应商名称,品牌,状态,创建时间
            fileName = GetExportFilePath("分公司与供应商关系.xlsx");
            if(!HttpContext.Current.User.Identity.IsAuthenticated) {
                throw new Exception(ErrorStrings.File_Validation_User);
            }
            try {
                var db = DbHelper.GetDbHelp(ConnectionString);
                using(var conn = db.CreateDbConnection()) {
                    var sql = new StringBuilder();
                    sql.Append(@"select b.code as branchcode,
                                       b.name as branchname,
                                       c.name as partssalescategoryname,
                                       e.code as suppliercode,
                                       e.name as suppliername,
                                       a.businesscode,
                                       a.businessname,
                                       a.arrivalcycle,
                                       a.purchasingcycle,
                                       (select value from keyvalueitem where NAME = 'MasterData_Status'and key=a.status) As Status, --状态
                                       a.remark,
                                       a.creatorname,
                                       a.createtime,
                                       a.modifiername,
                                       a.modifytime,
                                       a.abandonername,
                                       a.abandontime
                                  from branchsupplierrelation a
                                  inner join PersonSalesCenterLink f
                                  on a.partssalescategoryid=f.partssalescategoryid
                                  left join branch b
                                    on a.branchid = b.id
                                  left join partssalescategory c
                                    on a.partssalescategoryid = c.id
                                  left join partssupplier e
                                    on a.supplierid = e.id
                                 where f.personid = " + userinfo.Id);
                    var dbParameters = new List<DbParameter>();
                    if(ids != null && ids.Length > 0) {
                        var stringParam = string.Format(" and a.Id in ({0})", string.Join(",", ids));
                        sql.Append(stringParam);
                    } else {
                        if(branchId.HasValue) {
                            sql.Append(" and a.branchId={0}branchId");
                            dbParameters.Add(db.CreateDbParameter("branchId", branchId.Value));
                        }
                        if(!string.IsNullOrEmpty(supplierCode)) {
                            sql.Append(" and e.Code like {0}supplierCode ");
                            dbParameters.Add(db.CreateDbParameter("supplierCode", "%" + supplierCode + "%"));
                        }
                        if(!string.IsNullOrEmpty(supplierName)) {
                            sql.Append(" and e.Name like {0}supplierName ");
                            dbParameters.Add(db.CreateDbParameter("supplierName", "%" + supplierName + "%"));
                        }
                        if(!string.IsNullOrEmpty(bussinessCode)) {
                            sql.Append(" and a.bussinessCode like {0}bussinessCode ");
                            dbParameters.Add(db.CreateDbParameter("bussinessCode", "%" + bussinessCode + "%"));
                        }
                        if(!string.IsNullOrEmpty(bussinessName)) {
                            sql.Append(" and a.bussinessName like {0}bussinessName ");
                            dbParameters.Add(db.CreateDbParameter("bussinessName", "%" + bussinessName + "%"));
                        }
                        if(partsSalesCategoryId.HasValue) {
                            sql.Append(" and a.partsSalesCategoryId={0}partsSalesCategoryId");
                            dbParameters.Add(db.CreateDbParameter("partsSalesCategoryId", partsSalesCategoryId.Value));
                        }
                        if(status.HasValue) {
                            sql.Append(" and a.Status={0}statusvalue");
                            dbParameters.Add(db.CreateDbParameter("statusvalue", status.Value));
                        }
                        if(createTimeBegin.HasValue) {
                            sql.Append(" and a.createTime>=To_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(" and a.createTime<=To_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                    }

                    var comm = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    if(dbParameters.Any()) {
                        comm.Parameters.AddRange(dbParameters.ToArray());
                    }
                    conn.Open();
                    var reader = comm.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0)
                                return new object[] {
                                    ErrorStrings.Export_Title_BranchSupplierRelation_Code,ErrorStrings.Export_Title_Branch_BranchName,ErrorStrings.Export_Title_Partssalescategory_Name,ErrorStrings.Export_Title_FactoryPurchacePrice_SupplierCode,ErrorStrings.Export_Title_FactoryPurchacePrice_SupplierName,ErrorStrings.Export_Title_BranchSupplierRelation_BusinessCode,ErrorStrings.Export_Title_BranchSupplierRelation_BusinessName,ErrorStrings.Export_Title_BranchSupplierRelation_ArrivalCycle,ErrorStrings.Export_Title_BranchSupplierRelation_PurchasingCycle,
                                    ErrorStrings.Export_Title_AccountPeriod_Status,ErrorStrings.Export_Title_PartsBranch_Remark,ErrorStrings.Export_Title_AccountPeriod_CreatorName,ErrorStrings.Export_Title_AccountPeriod_CreateTime,ErrorStrings.Export_Title_AccountPeriod_ModifierName,ErrorStrings.Export_Title_AccountPeriod_ModifyTime,ErrorStrings.Export_Title_PartsBranch_AbandonerName,ErrorStrings.Export_Title_PartsBranch_AbandonerTime
                                
                                };
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }


        }
        public bool ImportBranchSupplierRelation(string fileName, out int excelImportNum, out List<BranchSupplierRelationExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<BranchSupplierRelationExtend>();
            var allList = new List<BranchSupplierRelationExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("BranchSupplierRelation", out notNullableFields, out fieldLenght);

                //List<string> notNullableFieldsPartsManagementCostGrade;
                //Dictionary<string, int> fieldLenghtPartsManagementCostGrade;
                //db.GetTableSchema("PartsManagementCostGrade", out notNullableFieldsPartsManagementCostGrade, out fieldLenghtPartsManagementCostGrade); 

                List<string> notNullableFieldsBranch;
                Dictionary<string, int> fieldLenghtBranch;
                db.GetTableSchema("Branch", out notNullableFieldsBranch, out fieldLenghtBranch);


                List<string> notNullableFieldsPartsSalesCategory;
                Dictionary<string, int> fieldLenghtPartsSalesCategory;
                db.GetTableSchema("PartsSalesCategory", out notNullableFieldsPartsSalesCategory, out fieldLenghtPartsSalesCategory);


                List<string> notNullableFieldsPartsSupplier;
                Dictionary<string, int> fieldLenghtPartsSupplier;
                db.GetTableSchema("PartsSupplier", out notNullableFieldsPartsSupplier, out fieldLenghtPartsSupplier);



                List<object> excelColumns;
                List<BranchSupplierRelationExtend> rightList;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    #region 指定文件中的列对应的名称 例如 excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode, "PartCode");

                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_BranchSupplierRelation_Code, "BranchCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Branch_BranchName, "BranchName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Partssalescategory_Name, "PartsSalesCategoryName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_FactoryPurchacePrice_SupplierCode, "SupplierCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_FactoryPurchacePrice_SupplierName, "SupplierName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_BranchSupplierRelation_BusinessCode, "BusinessCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_BranchSupplierRelation_BusinessName, "BusinessName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_BranchSupplierRelation_ArrivalCycle, "ArrivalCycle");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_BranchSupplierRelation_PurchasingCycle, "PurchasingCycle");
                    //excelOperator.AddColumnDataSource("配件管理费率等级名称", "PartsManagementCostGradeName");
                    //excelOperator.AddColumnDataSource("配件索赔系数", "PartsClaimCoefficient");
                    //excelOperator.AddColumnDataSource("配件索赔价格类型", "ClaimPriceCategory");
                    //excelOperator.AddColumnDataSource("是否按工时单价索赔", "IfSPByLabor");
                    //excelOperator.AddColumnDataSource("工时单价", "LaborUnitPrice");
                    //excelOperator.AddColumnDataSource("工时系数", "LaborCoefficient");
                    //excelOperator.AddColumnDataSource("是否存在外出费用", "IfHaveOutFee");
                    //excelOperator.AddColumnDataSource("外出里程单价（元/公里）", "OutServiceCarUnitPrice");
                    //excelOperator.AddColumnDataSource("外出人员单价（元/人天)", "OutSubsidyPrice");
                    //excelOperator.AddColumnDataSource("旧件运费系数", "OldPartTransCoefficient");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Remark, "Remark");

                    #endregion
                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;
                    #region 获取对应枚举 var keyValuePairs = new[] { new KeyValuePair<string, string>("PartABC", "ABCStrategy_Category")}; tempExcelOperator.ImportHelper.LoadEnumFromDb(null,null, keyValuePairs);

                    //var keyValuePairs = new[] { 
                    //    new KeyValuePair<string, string>("ClaimPriceCategory", "BranchSupplierRelation_ClaimPriceCategory") 
                    //};
                    //tempExcelOperator.ImportHelper.LoadEnumFromDb(null, null, keyValuePairs);

                    #endregion
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var tempImportObj = new BranchSupplierRelationExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        #region 根据导入的字段给对应的实体属性赋值

                        tempImportObj.BranchCodeStr = newRow["BranchCode"];
                        tempImportObj.BranchNameStr = newRow["BranchName"];
                        tempImportObj.PartsSalesCategoryNameStr = newRow["PartsSalesCategoryName"];
                        tempImportObj.SupplierCodeStr = newRow["SupplierCode"];
                        tempImportObj.SupplierNameStr = newRow["SupplierName"];
                        tempImportObj.BusinessCodeStr = newRow["BusinessCode"];
                        tempImportObj.BusinessNameStr = newRow["BusinessName"];
                        tempImportObj.ArrivalCycleStr = newRow["ArrivalCycle"];
                        tempImportObj.PurchasingCycleStr = newRow["PurchasingCycle"];
                        //tempImportObj.PartsManagementCostGradeNameStr = newRow["PartsManagementCostGradeName"];
                        //tempImportObj.PartsClaimCoefficientStr = newRow["PartsClaimCoefficient"];
                        //tempImportObj.ClaimPriceCategoryStr = newRow["ClaimPriceCategory"];
                        //tempImportObj.IfSPByLaborStr = newRow["IfSPByLabor"];
                        //tempImportObj.LaborUnitPriceStr = newRow["LaborUnitPrice"];
                        //tempImportObj.LaborCoefficientStr = newRow["LaborCoefficient"];
                        //tempImportObj.IfHaveOutFeeStr = newRow["IfHaveOutFee"];
                        //tempImportObj.OutServiceCarUnitPriceStr = newRow["OutServiceCarUnitPrice"];
                        //tempImportObj.OutSubsidyPriceStr = newRow["OutSubsidyPrice"];
                        //tempImportObj.OldPartTransCoefficientStr = newRow["OldPartTransCoefficient"];
                        tempImportObj.RemarkStr = newRow["Remark"];

                        #endregion

                        var tempErrorMessage = new List<string>();
                        #region 导入的数据基本检查

                        //分公司编号检查
                        var fieldIndex = notNullableFields.IndexOf("BranchId".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.BranchCodeStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_BranchSupplierRelation_CodeIsNull);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.BranchCodeStr) > fieldLenghtBranch["Code".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_BranchSupplierRelation_CodeIsLong);
                        }

                        //分公司名称检查
                        fieldIndex = notNullableFields.IndexOf("BranchId".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.BranchNameStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Branch_BranchNameIsNull);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.BranchNameStr) > fieldLenghtBranch["Name".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Branch_BranchNameIsLong);
                        }

                        ////配件管理费率等级名称检查
                        //fieldIndex = notNullableFields.IndexOf("PartsManagementCostGradeId".ToUpper());
                        //if(string.IsNullOrEmpty(tempImportObj.PartsManagementCostGradeNameStr)) {
                        //    if(fieldIndex > -1)
                        //        tempErrorMessage.Add("配件管理费率等级名称不能为空");
                        //} else {
                        //    if(Encoding.Default.GetByteCount(tempImportObj.PartsManagementCostGradeNameStr) > fieldLenghtPartsManagementCostGrade["Name".ToUpper()])
                        //        tempErrorMessage.Add("配件管理费率等级名称过长");
                        //}

                        //品牌检查
                        fieldIndex = notNullableFields.IndexOf("PartsSalesCategoryId".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.PartsSalesCategoryNameStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_MarketingDepartment_PartsSalesCategoryIsNull);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.PartsSalesCategoryNameStr) > fieldLenghtPartsSalesCategory["Name".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_PartsSalescategoryNameIsLong);
                        }

                        //供应商编号检查
                        fieldIndex = notNullableFields.IndexOf("SupplierId".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.SupplierCodeStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PersonnelSupplierRelation_SupplierCode);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.SupplierCodeStr) > fieldLenghtPartsSupplier["Code".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PersonnelSupplierRelation_SupplierCodeIsLong);
                        }

                        //供应商名称检查
                        fieldIndex = notNullableFields.IndexOf("SupplierId".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.SupplierNameStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_BranchSupplierRelation_SupplierNameIsNull);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.SupplierNameStr) > fieldLenghtPartsSupplier["Name".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchaseOrderDetail_Validation16);
                        }

                        fieldIndex = notNullableFields.IndexOf("BusinessCode".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.BusinessCodeStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_BranchSupplierRelation_BusinessCodeIsNull);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.BusinessCodeStr) > fieldLenght["BusinessCode".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_BranchSupplierRelation_BusinessCodeIsLong);
                        }

                        fieldIndex = notNullableFields.IndexOf("BusinessName".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.BusinessNameStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_BranchSupplierRelation_BusinessNameIsNull);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.BusinessNameStr) > fieldLenght["BusinessName".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_BranchSupplierRelation_BusinessNameIsLong);
                        }

                        //物流周期（天）检查
                        if(!string.IsNullOrWhiteSpace(tempImportObj.ArrivalCycleStr)) {
                            int checkValue;
                            if(int.TryParse(tempImportObj.ArrivalCycleStr, out checkValue)) {
                                if(checkValue <= 0) {
                                    tempErrorMessage.Add(ErrorStrings.Export_Validation_BranchSupplierRelation_ArrivalCycleOverZero);
                                } else {
                                    tempImportObj.ArrivalCycle = checkValue;
                                }
                            } else {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_BranchSupplierRelation_ArrivalCycleNumber);
                            }
                        }

                        //采购周期（天）检查
                        if (!string.IsNullOrWhiteSpace(tempImportObj.PurchasingCycleStr))
                        {
                            int checkValue;
                            if (int.TryParse(tempImportObj.PurchasingCycleStr, out checkValue))
                            {
                                if (checkValue <= 0)
                                {
                                    tempErrorMessage.Add(ErrorStrings.Export_Validation_BranchSupplierRelation_PurchasingCycleOverZero);
                                }
                                else
                                {
                                    tempImportObj.PurchasingCycle = checkValue;
                                }
                            }
                            else
                            {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_BranchSupplierRelation_PurchasingCycleNumber);
                            }
                        }


                        ////配件索赔系数检查
                        //if (!string.IsNullOrEmpty(tempImportObj.PartsClaimCoefficientStr)) {
                        //    Decimal checkValue;
                        //    if(Decimal.TryParse(tempImportObj.PartsClaimCoefficientStr, out checkValue)) {
                        //        if(checkValue <= 0) {
                        //            tempErrorMessage.Add("配件索赔系数必须大于0");
                        //        } else {
                        //            tempImportObj.PartsClaimCoefficient = checkValue;
                        //        }
                        //    } else {
                        //        tempErrorMessage.Add("配件索赔系数必须是数字");
                        //    }
                        //}

                        ////配件索赔价格类型检查
                        //fieldIndex = notNullableFields.IndexOf("ClaimPriceCategory".ToUpper());
                        //if(string.IsNullOrEmpty(tempImportObj.ClaimPriceCategoryStr)) {
                        //    if(fieldIndex > -1)
                        //        tempErrorMessage.Add("配件索赔价格类型不能为空");
                        //} else {
                        //    try {
                        //        tempImportObj.ClaimPriceCategory = (int)Enum.Parse(typeof(DcsBranchSupplierRelationClaimPriceCategory), tempImportObj.ClaimPriceCategoryStr);
                        //    } catch(Exception) {
                        //        tempErrorMessage.Add("配件索赔价格类型不正确");
                        //    }
                        //}



                        ////是否按工时单价索赔检查
                        //fieldIndex = notNullableFields.IndexOf("IfSPByLabor".ToUpper());
                        //if(string.IsNullOrEmpty(tempImportObj.IfSPByLaborStr)) {
                        //    if(fieldIndex > -1)
                        //        tempErrorMessage.Add("是否按工时单价索赔不能为空");
                        //} else {
                        //    tempImportObj.IfSPByLabor = tempImportObj.IfSPByLaborStr == ErrorStrings.Export_Title_PartsBranch_Yes;
                        //}

                        ////工时单价检查
                        //if(!string.IsNullOrEmpty(tempImportObj.LaborUnitPriceStr)) {
                        //    Decimal checkValue;
                        //    if(Decimal.TryParse(tempImportObj.LaborUnitPriceStr, out checkValue)) {
                        //        if(checkValue <= 0) {
                        //            tempErrorMessage.Add("工时单价必须大于0");
                        //        } else {
                        //            tempImportObj.LaborUnitPrice = checkValue;
                        //        }
                        //    } else {
                        //        tempErrorMessage.Add("工时单价必须是数字");
                        //    }
                        //}

                        ////工时系数检查
                        //if(!string.IsNullOrEmpty(tempImportObj.LaborCoefficientStr)) {
                        //    double checkValue;
                        //    if(double.TryParse(tempImportObj.LaborCoefficientStr, out checkValue)) {
                        //        if(checkValue <= 0) {
                        //            tempErrorMessage.Add("工时系数必须大于0");
                        //        } else {
                        //            tempImportObj.LaborCoefficient = checkValue;
                        //        }
                        //    } else {
                        //        tempErrorMessage.Add("工时系数必须是数字");
                        //    }
                        //} else {
                        //    tempImportObj.LaborCoefficient = 1.0;
                        //}

                        ////是否存在外出费用检查
                        //fieldIndex = notNullableFields.IndexOf("IfHaveOutFee".ToUpper());
                        //if(string.IsNullOrEmpty(tempImportObj.IfHaveOutFeeStr)) {
                        //    if(fieldIndex > -1)
                        //        tempErrorMessage.Add("是否存在外出费用不能为空");
                        //} else {
                        //    tempImportObj.IfHaveOutFee = tempImportObj.IfHaveOutFeeStr == ErrorStrings.Export_Title_PartsBranch_Yes;

                        //}

                        ////外出里程单价（元/公里）检查
                        //if(!string.IsNullOrEmpty(tempImportObj.OutServiceCarUnitPriceStr)) {
                        //    Decimal checkValue;
                        //    if(Decimal.TryParse(tempImportObj.OutServiceCarUnitPriceStr, out checkValue)) {
                        //        if(checkValue <= 0) {
                        //            tempErrorMessage.Add("外出里程单价（元/公里）必须大于0");
                        //        } else {
                        //            tempImportObj.OutServiceCarUnitPrice = checkValue;
                        //        }
                        //    } else {
                        //        tempErrorMessage.Add("外出里程单价（元/公里）必须是数字");
                        //    }
                        //}

                        ////外出人员单价（元/人天)检查
                        //if(!string.IsNullOrEmpty(tempImportObj.OutSubsidyPriceStr)) {
                        //    decimal checkValue;
                        //    if(decimal.TryParse(tempImportObj.OutSubsidyPriceStr, out checkValue)) {
                        //        if(checkValue <= 0) {
                        //            tempErrorMessage.Add("外出人员单价（元/人天)必须大于0");
                        //        } else {
                        //            tempImportObj.OutSubsidyPrice = checkValue;
                        //        }
                        //    } else {
                        //        tempErrorMessage.Add("外出人员单价（元/人天)必须是数字");
                        //    }
                        //}

                        ////旧件运费系数检查
                        //if(!string.IsNullOrEmpty(tempImportObj.OldPartTransCoefficientStr)) {
                        //    double checkValue;
                        //    if(double.TryParse(tempImportObj.OldPartTransCoefficientStr, out checkValue)) {
                        //        if(checkValue <= 0) {
                        //            tempErrorMessage.Add("旧件运费系数必须大于0");
                        //        } else {
                        //            tempImportObj.OldPartTransCoefficient = checkValue;
                        //        }
                        //    } else {
                        //        tempErrorMessage.Add("旧件运费系数必须是数字");
                        //    }
                        //} else {
                        //    tempImportObj.OldPartTransCoefficient = 1;
                        //}

                        //备注Remark
                        fieldIndex = notNullableFields.IndexOf("Remark".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.RemarkStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchasePricingDetail_Validation15);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.RemarkStr) > fieldLenght["Remark".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchasePricingDetail_Validation16);
                        }

                        #endregion
                        if(tempErrorMessage.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });
                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    #region 导入内容业务检查
                    #region //文件内，分公司编号、分公司名称、品牌、供应商编号、供应商名称组合唯一

                    var groups = tempRightList.GroupBy(r => new {
                        r.BranchCodeStr,
                        r.BranchNameStr,
                        r.PartsSalesCategoryNameStr,
                        r.SupplierCodeStr,
                        r.SupplierNameStr
                    }).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        groupItem.ErrorMsg = ErrorStrings.ImpPartsSupplierRelation_Validation31;
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    #endregion
                    #region //文件内，销售类型和业务编号组合唯一
                    var groups2 = tempRightList.GroupBy(r => new {
                        r.BusinessNameStr,
                        r.PartsSalesCategoryNameStr
                    }).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups2.SelectMany(r => r)) {
                        groupItem.ErrorMsg = ErrorStrings.Export_Validation_BranchSupplierRelation_Validation1;
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    #endregion
                    #region //校验分公司编号、分公司名称组合存在于营销分公司
                    var branchCodes = tempRightList.Select(r => r.BranchCodeStr).Distinct().ToArray();
                    var dbBranches = new List<PartsBranchExtend>();
                    Func<string[], bool> getDbBranches = value => {
                        var dbObj = new PartsBranchExtend {
                            BranchId = Convert.ToInt32(value[0]),
                            BranchName = value[1],
                            BranchCode = value[2]
                        };
                        dbBranches.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select Id,Name,Code from Branch where status=1 ", "Code", false, branchCodes, getDbBranches);
                    var errorBranches = tempRightList.Where(r => !dbBranches.Any(v => v.BranchName == r.BranchNameStr && v.BranchCode == r.BranchCodeStr)).ToList();
                    foreach(var errorItem in errorBranches) {
                        errorItem.ErrorMsg = String.Format(ErrorStrings.Export_Validation_BranchSupplierRelation_Validation2, errorItem.BranchCodeStr, errorItem.BranchNameStr);
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    foreach(var rightItem in tempRightList) {
                        var tempBranch = dbBranches.Single(r => r.BranchCode == rightItem.BranchCodeStr);
                        rightItem.BranchId = tempBranch.BranchId == null ? -1 : tempBranch.BranchId.Value;
                    }
                    #endregion
                    #region //校验品牌存在于配件销售类型
                    var partsSalesCategoryNames = tempRightList.Select(r => r.PartsSalesCategoryNameStr).Distinct().ToArray();
                    var dbPartsSalesCategorys = new List<PartsSalesCategoryExtend>();
                    Func<string[], bool> getPartsSalesCategorys = value => {
                        var dbObj = new PartsSalesCategoryExtend {
                            Id = Convert.ToInt32(value[0]),
                            Name = value[1],
                            BranchId = Convert.ToInt32(value[2])
                        };
                        dbPartsSalesCategorys.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select Id,Name,branchId from PartsSalesCategory where status=1", "Name", false, partsSalesCategoryNames, getPartsSalesCategorys);
                    var errorSpareParts = tempRightList.Where(r => !dbPartsSalesCategorys.Any(v => v.Name == r.PartsSalesCategoryNameStr && v.BranchId == r.BranchId)).ToList();
                    foreach(var errorItem in errorSpareParts) {
                        errorItem.ErrorMsg = String.Format(ErrorStrings.Export_Validation_BranchSupplierRelation_Validation3, errorItem.PartsSalesCategoryNameStr, errorItem.BranchNameStr);
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    foreach(var rightItem in tempRightList) {
                        var tempPartSalesCategory = dbPartsSalesCategorys.Single(r => r.BranchId == rightItem.BranchId && r.Name == rightItem.PartsSalesCategoryNameStr);
                        rightItem.PartsSalesCategoryId = tempPartSalesCategory.Id;
                    }
                    #endregion
                    #region //校验供应商编号、供应商名称组合存在配件供应商基本信息
                    var supplierCodes = tempRightList.Select(r => r.SupplierCodeStr).Distinct().ToArray();
                    var dbPartsSuppliers = new List<PartsSupplierExtend>();
                    Func<string[], bool> getPartsSuppliers = value => {
                        var dbObj = new PartsSupplierExtend {
                            Id = Convert.ToInt32(value[0]),
                            Code = value[1],
                            Name = value[2]
                        };
                        dbPartsSuppliers.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select Id,Code,Name from PartsSupplier where status=1", "Code", false, supplierCodes, getPartsSuppliers);
                    var errorPartsSalesCategorys = tempRightList.Where(r => !dbPartsSuppliers.Any(v => v.Code == r.SupplierCodeStr && v.Name == r.SupplierNameStr)).ToList();
                    foreach(var errorItem in errorPartsSalesCategorys) {
                        errorItem.ErrorMsg = String.Format(ErrorStrings.Export_Validation_BranchSupplierRelation_Validation4, errorItem.SupplierCodeStr, errorItem.SupplierNameStr);
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    foreach(var rightItem in tempRightList) {
                        var tempPartsSupplier = dbPartsSuppliers.Single(r => r.Code == rightItem.SupplierCodeStr);
                        rightItem.SupplierId = tempPartsSupplier.Id;
                    }
                    #endregion
                    #region //校验配件管理费率等级名称存在于配件管理费率等级
                    //var partsManagementCostGradeNames = tempRightList.Where(r => !string.IsNullOrWhiteSpace(r.PartsManagementCostGradeNameStr)).Select(r => r.PartsManagementCostGradeNameStr).Distinct().ToArray();
                    //var dbPartsManagementCostGrades = new List<PartsManagementCostGradeExtend>();
                    //if(partsManagementCostGradeNames.Length > 0) {
                    //    Func<string[], bool> getPartsManagementCostGrades = value => {
                    //        var dbObj = new PartsManagementCostGradeExtend {
                    //            Id = Convert.ToInt32(value[0]),
                    //            Name = value[1],
                    //            SalesCategoryName = value[2]
                    //        };
                    //        dbPartsManagementCostGrades.Add(dbObj);
                    //        return false;
                    //    };

                    //    db.QueryDataWithInOperator("select Id,Name,SalesCategoryName from PartsManagementCostGrade where status=1", "Name", false, partsManagementCostGradeNames, getPartsManagementCostGrades);
                    //    var errorPartsManagementCostGrades = tempRightList.Where(r => dbPartsManagementCostGrades.All(v => v.Name != r.PartsManagementCostGradeNameStr)).ToList();
                    //    foreach(var errorItem in errorPartsManagementCostGrades) {
                    //        errorItem.ErrorMsg = String.Format("配件管理费率等级名称“{0}”不存在", errorItem.PartsManagementCostGradeNameStr);
                    //    }
                    //    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    //    foreach(var rightItem in tempRightList) {
                    //        var tempDbPartsManagementCostGrade = dbPartsManagementCostGrades.Single(r => r.Name == rightItem.PartsManagementCostGradeNameStr && r.SalesCategoryName == rightItem.PartsSalesCategoryNameStr);
                    //        rightItem.PartsManagementCostGradeId = Convert.ToInt32(tempDbPartsManagementCostGrade.Id);
                    //    }
                    //}
                    //tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    #endregion
                    #region //校验营销分公司Id、配件销售类型Id、供应商Id组合唯一
                    var branchIds = dbBranches.Select(r => r.BranchId.ToString().ToUpper()).Distinct().ToArray();
                    var dbBranchSupplierRelations = new List<BranchSupplierRelationExtend>();
                    Func<string[], bool> getDbBranchSupplierRelations = value => {
                        var dbObj = new BranchSupplierRelationExtend {
                            BranchId = Convert.ToInt32(value[0]),
                            PartsSalesCategoryId = Convert.ToInt32(value[1]),
                            SupplierId = Convert.ToInt32(value[2])
                        };
                        dbBranchSupplierRelations.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select BranchId,PartsSalesCategoryId,SupplierId from BranchSupplierRelation where status=1", "BranchId", false, branchIds, getDbBranchSupplierRelations);
                    var errorBranchSupplierRelations = tempRightList.Where(r => dbBranchSupplierRelations.Any(v => v.BranchId == r.BranchId && v.PartsSalesCategoryId == r.PartsSalesCategoryId && v.SupplierId == r.SupplierId)).ToList();
                    foreach(var errorItem in errorBranchSupplierRelations) {
                        errorItem.ErrorMsg = ErrorStrings.ImpPartsSupplierRelation_Validation32;
                    }
                    #endregion
                    #region //校验配件销售类型Id、业务编号组合唯一
                    var businessCodeStrs = tempRightList.Select(r => r.BusinessCodeStr).Distinct().ToArray();
                    var dbBranchSupplierRelations2 = new List<BranchSupplierRelationExtend>();
                    Func<string[], bool> getDbBranchSupplierRelations2 = value => {
                        var dbObj = new BranchSupplierRelationExtend {
                            BusinessCode = value[0],
                            PartsSalesCategoryId = Convert.ToInt32(value[1]),
                        };
                        dbBranchSupplierRelations2.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select BusinessCode,PartsSalesCategoryId from BranchSupplierRelation where status=1", "BusinessCode", false, businessCodeStrs, getDbBranchSupplierRelations2);
                    var errorBranchSupplierRelations2 = tempRightList.Where(r => dbBranchSupplierRelations2.Any(v => v.BusinessCode == r.BusinessCodeStr && v.PartsSalesCategoryId == r.PartsSalesCategoryId)).ToList();
                    foreach(var errorItem in errorBranchSupplierRelations2) {
                        errorItem.ErrorMsg = ErrorStrings.Export_Validation_BranchSupplierRelation_Validation5;
                    }
                    #endregion

                    ////8、如果是否按工时单价索赔为否，工时单价必须为空 
                    ////如果是否存在外出费用为否
                    ////外出里程单价（元/公里）
                    ////外出人员单价（元/人天)必须为空
                    //foreach(var rightItem in tempRightList) {
                    //    if(rightItem.IfSPByLabor == false && rightItem.LaborUnitPrice != null) {
                    //        rightItem.ErrorMsg = "是否按工时单价索赔为否，工时单价必须为空";
                    //    }
                    //    if(rightItem.IfHaveOutFee == false && (rightItem.OutServiceCarUnitPrice != null || rightItem.OutSubsidyPrice != null)) {
                    //        rightItem.ErrorMsg = "是否存在外出费用为否,外出里程单价（元/公里、外出人员单价（元/人天)必须为空";
                    //    }
                    //}

                    #endregion
                    //获取所有不合格数据
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    //获取所有合格数据
                    rightList = allList.Except(errorList).ToList();

                    #region 将合格数据的值填上
                    foreach(var rightItem in rightList) {
                        rightItem.BusinessCode = rightItem.BusinessCodeStr;
                        rightItem.BusinessName = rightItem.BusinessNameStr;
                        rightItem.Remark = rightItem.RemarkStr;
                    }
                    #endregion

                }
                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempObj = list[index - 1];
                            var values = new object[] {
                                #region 设置错误信息导出的列的值

                                tempObj.BranchCodeStr, tempObj.BranchNameStr,
                                tempObj.PartsSalesCategoryNameStr ,tempObj.SupplierCodeStr ,
                                tempObj.SupplierNameStr ,tempObj.BusinessCodeStr,tempObj.BusinessNameStr,tempObj.ArrivalCycleStr,
                                tempObj.PurchasingCycleStr,tempObj.RemarkStr ,
                                tempObj.ErrorMsg

                                #endregion                     
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }

                //导入所有合格数据
                if(!rightList.Any())
                    return true;
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    //开启事务，新增更新配件在一个事务内
                    var ts = conn.BeginTransaction();
                    try {
                        //新增配件
                        if(rightList.Any()) {
                            #region 获取对应的插入的SQL 通过db.GetInsertSql方法
                            //获取新增数据的sql语句，Id为主键
                            var sqlInsert = db.GetInsertSql("BranchSupplierRelation", "Id", new[] {
                                "BranchId","PartsSalesCategoryId","SupplierId","BusinessCode","BusinessName","ArrivalCycle","PurchasingCycle",
                                "Remark","Status","CreatorId",
                                "CreatorName","CreateTime"
                            });
                            #endregion
                            var userInfo = Utils.GetCurrentUserInfo();
                            //往数据库增加配件信息
                            foreach(var item in rightList) {
                                #region 添加Sql的参数

                                var command = db.CreateDbCommand(sqlInsert, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("BranchId", item.BranchId));
                                command.Parameters.Add(db.CreateDbParameter("PartsSalesCategoryId", item.PartsSalesCategoryId));
                                command.Parameters.Add(db.CreateDbParameter("SupplierId", item.SupplierId));
                                command.Parameters.Add(db.CreateDbParameter("BusinessCode", item.BusinessCode));
                                command.Parameters.Add(db.CreateDbParameter("BusinessName", item.BusinessName));
                                command.Parameters.Add(db.CreateDbParameter("ArrivalCycle", item.ArrivalCycle));
                                command.Parameters.Add(db.CreateDbParameter("PurchasingCycle", item.PurchasingCycle));
                                //command.Parameters.Add(db.CreateDbParameter("PartsManagementCostGradeId", item.PartsManagementCostGradeId));
                                //command.Parameters.Add(db.CreateDbParameter("PartsClaimCoefficient", item.PartsClaimCoefficient));
                                //command.Parameters.Add(db.CreateDbParameter("ClaimPriceCategory", item.ClaimPriceCategory));
                                //command.Parameters.Add(db.CreateDbParameter("IfSPByLabor", item.IfSPByLabor));
                                //command.Parameters.Add(db.CreateDbParameter("LaborUnitPrice", item.LaborUnitPrice));
                                //command.Parameters.Add(db.CreateDbParameter("LaborCoefficient", item.LaborCoefficient));
                                //command.Parameters.Add(db.CreateDbParameter("IfHaveOutFee", item.IfHaveOutFee));
                                //command.Parameters.Add(db.CreateDbParameter("OutServiceCarUnitPrice", item.OutServiceCarUnitPrice));
                                //command.Parameters.Add(db.CreateDbParameter("OutSubsidyPrice", item.OutSubsidyPrice));
                                //command.Parameters.Add(db.CreateDbParameter("OldPartTransCoefficient", item.OldPartTransCoefficient));
                                command.Parameters.Add(db.CreateDbParameter("Remark", item.Remark));
                                command.Parameters.Add(db.CreateDbParameter("Status", (int)DcsBaseDataStatus.有效));
                                command.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                command.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                command.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                command.ExecuteNonQuery();

                                #endregion
                            }
                        }
                        //无异常提交
                        ts.Commit();
                    } catch(Exception) {
                        //报错回滚
                        ts.Rollback();
                        throw;
                    } finally {
                        if(conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
            }
        }



        //导入修改分公司与供应商关系
        public bool ImportBranchSupplierRelationForUpdate(string fileName, out int excelImportNum, out List<BranchSupplierRelationExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";

            var errorList = new List<BranchSupplierRelationExtend>();
            var allList = new List<BranchSupplierRelationExtend>();

            try {
                #region 基本校验
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                if(string.IsNullOrWhiteSpace(fileName)) {
                    throw new Exception(ErrorStrings.Export_Validation_BranchSupplierRelation_Validation6);
                }
                #endregion
                #region 获取导入的文件路径并创建帮助类对象
                var importFilePath = GetImportFilePath(fileName);
                var db = DbHelper.GetDbHelp(ConnectionString);
                #endregion
                #region 获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("BranchSupplierRelation", out notNullableFields, out fieldLenght);

                //List<string> notNullableFieldsPartsManagementCostGrade;
                //Dictionary<string, int> fieldLenghtPartsManagementCostGrade;
                //db.GetTableSchema("PartsManagementCostGrade", out notNullableFieldsPartsManagementCostGrade, out fieldLenghtPartsManagementCostGrade);

                List<string> notNullableFieldsBranch;
                Dictionary<string, int> fieldLenghtBranch;
                db.GetTableSchema("Branch", out notNullableFieldsBranch, out fieldLenghtBranch); 


                List<string> notNullableFieldsPartsSalesCategory;
                Dictionary<string, int> fieldLenghtPartsSalesCategory;
                db.GetTableSchema("PartsSalesCategory", out notNullableFieldsPartsSalesCategory, out fieldLenghtPartsSalesCategory);


                List<string> notNullableFieldsPartsSupplier;
                Dictionary<string, int> fieldLenghtPartsSupplier;
                db.GetTableSchema("PartsSupplier", out notNullableFieldsPartsSupplier, out fieldLenghtPartsSupplier);
                #endregion

                List<object> excelColumns;
                List<BranchSupplierRelationExtend> rightList;
                using(var excelOperator = new ExcelImport(importFilePath, ConnectionString)) {
                    #region 指定文件中的列对应的名称 例如 excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode, "PartCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_BranchSupplierRelation_Code, "BranchCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Branch_BranchName, "BranchName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Partssalescategory_Name, "PartsSalesCategoryName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_FactoryPurchacePrice_SupplierCode, "SupplierCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_FactoryPurchacePrice_SupplierName, "SupplierName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_BranchSupplierRelation_BusinessCode, "BusinessCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_BranchSupplierRelation_BusinessName, "BusinessName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_BranchSupplierRelation_ArrivalCycle, "ArrivalCycle");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_BranchSupplierRelation_PurchasingCycle, "PurchasingCycle");
                    //excelOperator.AddColumnDataSource("配件管理费率等级名称", "PartsManagementCostGradeName");
                    //excelOperator.AddColumnDataSource("配件索赔系数", "PartsClaimCoefficient");
                    //excelOperator.AddColumnDataSource("配件索赔价格类型", "ClaimPriceCategory");
                    //excelOperator.AddColumnDataSource("是否按工时单价索赔", "IfSPByLabor");
                    //excelOperator.AddColumnDataSource("工时单价", "LaborUnitPrice");
                    //excelOperator.AddColumnDataSource("工时系数", "LaborCoefficient");
                    //excelOperator.AddColumnDataSource("是否存在外出费用", "IfHaveOutFee");
                    //excelOperator.AddColumnDataSource("外出里程单价（元/公里）", "OutServiceCarUnitPrice");
                    //excelOperator.AddColumnDataSource("外出人员单价（元/人天)", "OutSubsidyPrice");
                    //excelOperator.AddColumnDataSource("旧件运费系数", "OldPartTransCoefficient");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Remark, "Remark");
                    #endregion
                    #region 将对象转换为Object
                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    #endregion
                    #region 获取对应枚举 这段是从数据库获取枚举的值并加入到ImportHelper对象的eNumList属性中,但是下面用的是直接在写死的枚举所以这段不要了
                    //var keyValuePairs = new[] { 
                    //    new KeyValuePair<string, string>("ClaimPriceCategory", "BranchSupplierRelation_ClaimPriceCategory") 
                    //};
                    //var tempExcelOperator = excelOperator;
                    //tempExcelOperator.ImportHelper.LoadEnumFromDb(null, null, keyValuePairs);
                    #endregion
                    #region 将数据导入并做基本校验以及一些简单赋值
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        #region 根据导入的字段给对应的实体属性赋值
                        var tempImportObj = new BranchSupplierRelationExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        tempImportObj.BranchCodeStr = newRow["BranchCode"];
                        tempImportObj.BranchNameStr = newRow["BranchName"];
                        tempImportObj.PartsSalesCategoryNameStr = newRow["PartsSalesCategoryName"];
                        tempImportObj.SupplierCodeStr = newRow["SupplierCode"];
                        tempImportObj.SupplierNameStr = newRow["SupplierName"];
                        tempImportObj.BusinessCodeStr = newRow["BusinessCode"];
                        tempImportObj.BusinessNameStr = newRow["BusinessName"];
                        tempImportObj.ArrivalCycleStr = newRow["ArrivalCycle"];
                        tempImportObj.PurchasingCycleStr = newRow["PurchasingCycle"];
                        //tempImportObj.PartsManagementCostGradeNameStr = newRow["PartsManagementCostGradeName"];
                        //tempImportObj.PartsClaimCoefficientStr = newRow["PartsClaimCoefficient"];
                        //tempImportObj.ClaimPriceCategoryStr = newRow["ClaimPriceCategory"];
                        //tempImportObj.IfSPByLaborStr = newRow["IfSPByLabor"];
                        //tempImportObj.LaborUnitPriceStr = newRow["LaborUnitPrice"];
                        //tempImportObj.LaborCoefficientStr = newRow["LaborCoefficient"];
                        //tempImportObj.IfHaveOutFeeStr = newRow["IfHaveOutFee"];
                        //tempImportObj.OutServiceCarUnitPriceStr = newRow["OutServiceCarUnitPrice"];
                        //tempImportObj.OutSubsidyPriceStr = newRow["OutSubsidyPrice"];
                        //tempImportObj.OldPartTransCoefficientStr = newRow["OldPartTransCoefficient"];
                        tempImportObj.RemarkStr = newRow["Remark"];
                        #endregion
                        #region 导入的数据基本检查,并赋值记录错误信息,以及一些简单属性
                        var tempErrorMessage = new List<string>();
                        ////配件管理费率等级名称检查
                        //var fieldIndex = notNullableFields.IndexOf("PartsManagementCostGradeId".ToUpper());
                        //if(string.IsNullOrEmpty(tempImportObj.PartsManagementCostGradeNameStr)) {
                        //    if(fieldIndex > -1)
                        //        tempErrorMessage.Add("配件管理费率等级名称不能为空");
                        //} else {
                        //    if(Encoding.Default.GetByteCount(tempImportObj.PartsManagementCostGradeNameStr) > fieldLenghtPartsManagementCostGrade["Name".ToUpper()])
                        //        tempErrorMessage.Add("配件管理费率等级名称过长");
                        //}

                        //品牌检查
                        var fieldIndex = notNullableFields.IndexOf("PartsSalesCategoryId".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.PartsSalesCategoryNameStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_MarketingDepartment_PartsSalesCategoryIsNull);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.PartsSalesCategoryNameStr) > fieldLenghtPartsSalesCategory["Name".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_PartsSalescategoryNameIsLong);
                        }

                        //供应商编号检查
                        fieldIndex = notNullableFields.IndexOf("SupplierId".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.SupplierCodeStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PersonnelSupplierRelation_SupplierCode);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.SupplierCodeStr) > fieldLenghtPartsSupplier["Code".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PersonnelSupplierRelation_SupplierCodeIsLong);
                        }

                        //供应商名称检查
                        fieldIndex = notNullableFields.IndexOf("SupplierId".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.SupplierNameStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_BranchSupplierRelation_SupplierNameIsNull);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.SupplierNameStr) > fieldLenghtPartsSupplier["Name".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchaseOrderDetail_Validation16);
                        }

                        fieldIndex = notNullableFields.IndexOf("BusinessCode".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.BusinessCodeStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_BranchSupplierRelation_BusinessCodeIsNull);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.BusinessCodeStr) > fieldLenght["BusinessCode".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_BranchSupplierRelation_BusinessCodeIsLong);
                        }

                        fieldIndex = notNullableFields.IndexOf("BusinessName".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.BusinessNameStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_BranchSupplierRelation_BusinessNameIsNull);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.BusinessNameStr) > fieldLenght["BusinessName".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_BranchSupplierRelation_BusinessNameIsLong);
                        }

                        //物流周期（天）检查
                        if (!string.IsNullOrWhiteSpace(tempImportObj.ArrivalCycleStr))
                        {
                            int checkValue;
                            if (int.TryParse(tempImportObj.ArrivalCycleStr, out checkValue))
                            {
                                if (checkValue <= 0)
                                {
                                    tempErrorMessage.Add(ErrorStrings.Export_Validation_BranchSupplierRelation_ArrivalCycleOverZero);
                                }
                                else
                                {
                                    tempImportObj.ArrivalCycle = checkValue;
                                }
                            }
                            else
                            {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_BranchSupplierRelation_ArrivalCycleNumber);
                            }
                        }

                        //采购周期（天）检查
                        if (!string.IsNullOrWhiteSpace(tempImportObj.PurchasingCycleStr)) {
                            int checkValue;
                            if(int.TryParse(tempImportObj.PurchasingCycleStr, out checkValue)) {
                                if(checkValue <= 0) {
                                    tempErrorMessage.Add(ErrorStrings.Export_Validation_BranchSupplierRelation_PurchasingCycleOverZero);
                                } else {
                                    tempImportObj.PurchasingCycle = checkValue;
                                }
                            } else {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_BranchSupplierRelation_PurchasingCycleNumber);
                            }
                        }


                        ////配件索赔系数检查
                        //if(!string.IsNullOrEmpty(tempImportObj.PartsClaimCoefficientStr)) {
                        //    Decimal checkValue;
                        //    if(Decimal.TryParse(tempImportObj.PartsClaimCoefficientStr, out checkValue)) {
                        //        if(checkValue <= 0) {
                        //            tempErrorMessage.Add("配件索赔系数必须大于0");
                        //        } else {
                        //            tempImportObj.PartsClaimCoefficient = checkValue;
                        //        }
                        //    } else {
                        //        tempErrorMessage.Add("配件索赔系数必须是数字");
                        //    }
                        //}

                        ////配件索赔价格类型检查
                        //fieldIndex = notNullableFields.IndexOf("ClaimPriceCategory".ToUpper());
                        //if(string.IsNullOrEmpty(tempImportObj.ClaimPriceCategoryStr)) {
                        //    if(fieldIndex > -1)
                        //        tempErrorMessage.Add("配件索赔价格类型不能为空");
                        //} else {
                        //    try {
                        //        tempImportObj.ClaimPriceCategory = (int)Enum.Parse(typeof(DcsBranchSupplierRelationClaimPriceCategory), tempImportObj.ClaimPriceCategoryStr);
                        //    } catch(Exception) {
                        //        tempErrorMessage.Add("配件索赔价格类型不正确");
                        //    }
                        //}



                        ////是否按工时单价索赔检查
                        //fieldIndex = notNullableFields.IndexOf("IfSPByLabor".ToUpper());
                        //if(string.IsNullOrEmpty(tempImportObj.IfSPByLaborStr)) {
                        //    if(fieldIndex > -1)
                        //        tempErrorMessage.Add("是否按工时单价索赔不能为空");
                        //} else {
                        //    tempImportObj.IfSPByLabor = tempImportObj.IfSPByLaborStr == ErrorStrings.Export_Title_PartsBranch_Yes;
                        //}

                        ////工时单价检查
                        //if(!string.IsNullOrEmpty(tempImportObj.LaborUnitPriceStr)) {
                        //    Decimal checkValue;
                        //    if(Decimal.TryParse(tempImportObj.LaborUnitPriceStr, out checkValue)) {
                        //        if(checkValue <= 0) {
                        //            tempErrorMessage.Add("工时单价必须大于0");
                        //        } else {
                        //            tempImportObj.LaborUnitPrice = checkValue;
                        //        }
                        //    } else {
                        //        tempErrorMessage.Add("工时单价必须是数字");
                        //    }
                        //}

                        ////工时系数检查
                        //if(!string.IsNullOrEmpty(tempImportObj.LaborCoefficientStr)) {
                        //    double checkValue;
                        //    if(double.TryParse(tempImportObj.LaborCoefficientStr, out checkValue)) {
                        //        if(checkValue <= 0) {
                        //            tempErrorMessage.Add("工时系数必须大于0");
                        //        } else {
                        //            tempImportObj.LaborCoefficient = checkValue;
                        //        }
                        //    } else {
                        //        tempErrorMessage.Add("工时系数必须是数字");
                        //    }
                        //} else {
                        //    tempImportObj.LaborCoefficient = 1.0;
                        //}

                        ////是否存在外出费用检查
                        //fieldIndex = notNullableFields.IndexOf("IfHaveOutFee".ToUpper());
                        //if(string.IsNullOrEmpty(tempImportObj.IfHaveOutFeeStr)) {
                        //    if(fieldIndex > -1)
                        //        tempErrorMessage.Add("是否存在外出费用不能为空");
                        //} else {
                        //    tempImportObj.IfHaveOutFee = tempImportObj.IfHaveOutFeeStr == ErrorStrings.Export_Title_PartsBranch_Yes;

                        //}

                        ////外出里程单价（元/公里）检查
                        //if(!string.IsNullOrEmpty(tempImportObj.OutServiceCarUnitPriceStr)) {
                        //    Decimal checkValue;
                        //    if(Decimal.TryParse(tempImportObj.OutServiceCarUnitPriceStr, out checkValue)) {
                        //        if(checkValue <= 0) {
                        //            tempErrorMessage.Add("外出里程单价（元/公里）必须大于0");
                        //        } else {
                        //            tempImportObj.OutServiceCarUnitPrice = checkValue;
                        //        }
                        //    } else {
                        //        tempErrorMessage.Add("外出里程单价（元/公里）必须是数字");
                        //    }
                        //}

                        ////外出人员单价（元/人天)检查
                        //if(!string.IsNullOrEmpty(tempImportObj.OutSubsidyPriceStr)) {
                        //    decimal checkValue;
                        //    if(decimal.TryParse(tempImportObj.OutSubsidyPriceStr, out checkValue)) {
                        //        if(checkValue <= 0) {
                        //            tempErrorMessage.Add("外出人员单价（元/人天)必须大于0");
                        //        } else {
                        //            tempImportObj.OutSubsidyPrice = checkValue;
                        //        }
                        //    } else {
                        //        tempErrorMessage.Add("外出人员单价（元/人天)必须是数字");
                        //    }
                        //}

                        ////旧件运费系数检查
                        //if(!string.IsNullOrEmpty(tempImportObj.OldPartTransCoefficientStr)) {
                        //    double checkValue;
                        //    if(double.TryParse(tempImportObj.OldPartTransCoefficientStr, out checkValue)) {
                        //        if(checkValue <= 0) {
                        //            tempErrorMessage.Add("旧件运费系数必须大于0");
                        //        } else {
                        //            tempImportObj.OldPartTransCoefficient = checkValue;
                        //        }
                        //    } else {
                        //        tempErrorMessage.Add("旧件运费系数必须是数字");
                        //    }
                        //} else {
                        //    tempImportObj.OldPartTransCoefficient = 1;
                        //}

                        //备注Remark
                        fieldIndex = notNullableFields.IndexOf("Remark".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.RemarkStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchasePricingDetail_Validation15);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.RemarkStr) > fieldLenght["Remark".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchasePricingDetail_Validation16);
                        }


                        if(tempErrorMessage.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                        #endregion
                    });
                    #endregion
                    #region 通过业务校验一步一步筛选,将不合格的数据滤出
                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    //#region //如果是否按工时单价索赔为否,工时单价必须为空;如果是否存在外出费用为否,外出里程单价（元/公里),外出人员单价（元/人天)必须为空
                    //foreach(var rightItem in tempRightList) {
                    //    if(rightItem.IfSPByLabor == false && rightItem.LaborUnitPrice != null) {
                    //        rightItem.ErrorMsg = "是否按工时单价索赔为否，工时单价必须为空";
                    //    }
                    //    if(rightItem.IfHaveOutFee == false && (rightItem.OutServiceCarUnitPrice != null || rightItem.OutSubsidyPrice != null)) {
                    //        rightItem.ErrorMsg = "是否存在外出费用为否,外出里程单价（元/公里、外出人员单价（元/人天)必须为空";
                    //    }
                    //}
                    //#endregion

                    #region //文件内，分公司编号、分公司名称、品牌、供应商编号、供应商名称组合唯一
                    var groups = tempRightList.GroupBy(r => new {
                        r.BranchCodeStr,
                        r.BranchNameStr,
                        r.PartsSalesCategoryNameStr,
                        r.SupplierCodeStr,
                        r.SupplierNameStr
                    }).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        groupItem.ErrorMsg = ErrorStrings.ImpPartsSupplierRelation_Validation31;
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    #endregion

                    #region //文件内，销售类型和业务编号组合唯一
                    var groups2 = tempRightList.GroupBy(r => new {
                        r.BusinessNameStr,
                        r.PartsSalesCategoryNameStr
                    }).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups2.SelectMany(r => r)) {
                        groupItem.ErrorMsg = ErrorStrings.Export_Validation_BranchSupplierRelation_Validation1;
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    #endregion

                    #region //校验品牌存在于配件销售类型,合格的数据给PartsSalesCategoryId赋值
                    var partsSalesCategoryNames = tempRightList.Select(r => r.PartsSalesCategoryNameStr).Distinct().ToArray();
                    var dbPartsSalesCategorys = new List<PartsSalesCategoryExtend>();
                    Func<string[], bool> getPartsSalesCategorys = value => {
                        var dbObj = new PartsSalesCategoryExtend {
                            Id = Convert.ToInt32(value[0]),
                            Name = value[1],
                            BranchId = Convert.ToInt32(value[2]),
                            BranchCode = value[3].ToString(CultureInfo.InvariantCulture),
                            BranchName = value[4].ToString(CultureInfo.InvariantCulture)
                        };
                        dbPartsSalesCategorys.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select Id,Name,branchId,branchCode,branchName from PartsSalesCategory where status=1", "Name", false, partsSalesCategoryNames, getPartsSalesCategorys);
                    var errorSpareParts = tempRightList.Where(r => dbPartsSalesCategorys.All(v => v.Name != r.PartsSalesCategoryNameStr)).ToList();
                    foreach(var errorItem in errorSpareParts) {
                        errorItem.ErrorMsg = String.Format(ErrorStrings.Export_Validation_BranchSupplierRelation_Validation7, errorItem.PartsSalesCategoryNameStr);
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    foreach(var rightItem in tempRightList) {
                        var tempPartSalesCategory = dbPartsSalesCategorys.Single(r => r.Name == rightItem.PartsSalesCategoryNameStr);
                        rightItem.PartsSalesCategoryId = tempPartSalesCategory.Id;
                        rightItem.BranchId = tempPartSalesCategory.BranchId;
                    }
                    #endregion

                    #region //校验供应商编号、供应商名称组合存在配件供应商基本信息,合格的数据给SupplierId赋值
                    var supplierCodes = tempRightList.Select(r => r.SupplierCodeStr).Distinct().ToArray();
                    var dbPartsSuppliers = new List<PartsSupplierExtend>();
                    Func<string[], bool> getPartsSuppliers = value => {
                        var dbObj = new PartsSupplierExtend {
                            Id = Convert.ToInt32(value[0]),
                            Code = value[1],
                            Name = value[2]
                        };
                        dbPartsSuppliers.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select Id,Code,Name from PartsSupplier where status=1", "Code", false, supplierCodes, getPartsSuppliers);
                    var errorPartsSalesCategorys = tempRightList.Where(r => !dbPartsSuppliers.Any(v => v.Code == r.SupplierCodeStr && v.Name == r.SupplierNameStr)).ToList();
                    foreach(var errorItem in errorPartsSalesCategorys) {
                        errorItem.ErrorMsg = String.Format(ErrorStrings.Export_Validation_BranchSupplierRelation_Validation4, errorItem.SupplierCodeStr, errorItem.SupplierNameStr);
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    foreach(var rightItem in tempRightList) {
                        var tempPartsSupplier = dbPartsSuppliers.Single(r => r.Code == rightItem.SupplierCodeStr);
                        rightItem.SupplierId = tempPartsSupplier.Id;
                    }
                    #endregion

                    //#region //校验配件管理费率等级名称存在于配件管理费率等级,合格的数据给PartsManagementCostGradeId赋值
                    //var partsManagementCostGradeNames = tempRightList.Where(r => !string.IsNullOrWhiteSpace(r.PartsManagementCostGradeNameStr)).Select(r => r.PartsManagementCostGradeNameStr).Distinct().ToArray();
                    //var dbPartsManagementCostGrades = new List<PartsManagementCostGradeExtend>();
                    //if(partsManagementCostGradeNames.Length > 0) {
                    //    Func<string[], bool> getPartsManagementCostGrades = value => {
                    //        var dbObj = new PartsManagementCostGradeExtend {
                    //            Id = Convert.ToInt32(value[0]),
                    //            Name = value[1],
                    //            SalesCategoryName = value[2]
                    //        };
                    //        dbPartsManagementCostGrades.Add(dbObj);
                    //        return false;
                    //    };

                    //    db.QueryDataWithInOperator("select Id,Name,SalesCategoryName from PartsManagementCostGrade where status=1", "Name", false, partsManagementCostGradeNames, getPartsManagementCostGrades);
                    //    var errorPartsManagementCostGrades = tempRightList.Where(r => dbPartsManagementCostGrades.All(v => v.Name != r.PartsManagementCostGradeNameStr)).ToList();
                    //    foreach(var errorItem in errorPartsManagementCostGrades) {
                    //        errorItem.ErrorMsg = String.Format("配件管理费率等级名称“{0}”不存在", errorItem.PartsManagementCostGradeNameStr);
                    //    }
                    //    foreach(var rightItem in tempRightList) {
                    //        var tempDbPartsManagementCostGrade = dbPartsManagementCostGrades.Single(r => r.Name == rightItem.PartsManagementCostGradeNameStr && r.SalesCategoryName == rightItem.PartsSalesCategoryNameStr);
                    //        rightItem.PartsManagementCostGradeId = Convert.ToInt32(tempDbPartsManagementCostGrade.Id);
                    //    }
                    //}
                    //tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    //#endregion

                    #region //校验配件销售类型Id、业务编号组合唯一
                    var businessCodeStrs = tempRightList.Select(r => r.BusinessCodeStr).Distinct().ToArray();
                    var dbBranchSupplierRelations2 = new List<BranchSupplierRelationExtend>();
                    Func<string[], bool> getDbBranchSupplierRelations2 = value => {
                        var dbObj = new BranchSupplierRelationExtend {
                            BusinessCode = value[0],
                            PartsSalesCategoryId = Convert.ToInt32(value[1]),
                        };
                        dbBranchSupplierRelations2.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select BusinessCode,PartsSalesCategoryId from BranchSupplierRelation where status=1", "BusinessCode", false, businessCodeStrs, getDbBranchSupplierRelations2);
                    var errorBranchSupplierRelations2 = tempRightList.Where(r => !dbBranchSupplierRelations2.Any(v => v.BusinessCode == r.BusinessCodeStr && v.PartsSalesCategoryId == r.PartsSalesCategoryId)).ToList();
                    foreach(var errorItem in errorBranchSupplierRelations2) {
                        errorItem.ErrorMsg = ErrorStrings.Export_Validation_BranchSupplierRelation_Validation8;
                    }
                    #endregion

                    #region //校验营销分公司Id、配件销售类型Id、供应商Id的唯一组合在数据库中是否存在
                    var branchIds = tempRightList.Select(r => r.BranchId.ToString(CultureInfo.InvariantCulture).ToUpper()).Distinct().ToArray();
                    var dbBranchSupplierRelations = new List<BranchSupplierRelationExtend>();
                    Func<string[], bool> getDbBranchSupplierRelations = value => {
                        var dbObj = new BranchSupplierRelationExtend {
                            BranchId = Convert.ToInt32(value[0]),
                            PartsSalesCategoryId = Convert.ToInt32(value[1]),
                            SupplierId = Convert.ToInt32(value[2]),
                            Id = Convert.ToInt32(value[3])
                        };
                        dbBranchSupplierRelations.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select BranchId,PartsSalesCategoryId,SupplierId,Id from BranchSupplierRelation where status=1", "BranchId", false, branchIds, getDbBranchSupplierRelations);
                    var errorBranchSupplierRelations = tempRightList.Where(r => !dbBranchSupplierRelations.Any(v => v.BranchId == r.BranchId && v.PartsSalesCategoryId == r.PartsSalesCategoryId && v.SupplierId == r.SupplierId)).ToList();
                    foreach(var errorItem in errorBranchSupplierRelations) {
                        errorItem.ErrorMsg = ErrorStrings.Export_Validation_BranchSupplierRelation_Validation9;
                    }
                    #endregion

                    #region //校验需要修改的数据在数据库中是否唯一
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    foreach(var branchSupplierRelationExtend in tempRightList) {
                        var item = branchSupplierRelationExtend;
                        var tempDbBranchSupplierRelations = dbBranchSupplierRelations.Where(v => v.BranchId == item.BranchId && v.PartsSalesCategoryId == item.PartsSalesCategoryId && v.SupplierId == item.SupplierId).ToArray();
                        if(tempDbBranchSupplierRelations.Length > 1) {
                            item.ErrorMsg = ErrorStrings.Export_Validation_BranchSupplierRelation_Validation10;
                        }
                        if(tempDbBranchSupplierRelations.Length == 1) {
                            item.Id = tempDbBranchSupplierRelations[0].Id;
                        }
                    }
                    #endregion

                    #endregion
                    #region 获取所有合格数据并将合格数据其他字段赋值
                    rightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    foreach(var rightItem in rightList) {
                        //Id在最后一步校验中已经赋值了
                        rightItem.BusinessCode = rightItem.BusinessCodeStr;
                        rightItem.BusinessName = rightItem.BusinessNameStr;
                        rightItem.Remark = rightItem.RemarkStr;
                    }
                    #endregion
                    #region 导入所有合格数据
                    if(rightList.Any()) {
                        using(var conn = db.CreateDbConnection()) {
                            conn.Open();
                            //开启事务，新增更新配件在一个事务内
                            var ts = conn.BeginTransaction();
                            try {
                                //新增配件
                                if(rightList.Any()) {
                                    #region 获取对应的插入的SQL 通过db.GetInsertSql方法
                                    //获取修改数据的sql语句，Id为主键
                                    var sqlUpdate = db.GetUpdateSql("BranchSupplierRelation", new[] {
                                        "BusinessCode", "BusinessName", "ArrivalCycle", "PurchasingCycle", "Remark", "ModifierId", "ModifierName", "ModifyTime"
                                    }, new[] {
                                        "Id"
                                    });
                                    #endregion
                                    var userInfo = Utils.GetCurrentUserInfo();
                                    //往数据库增加配件信息
                                    foreach(var item in rightList) {
                                        #region 添加Sql的参数
                                        var command = db.CreateDbCommand(sqlUpdate, conn, ts);
                                        command.Parameters.Add(db.CreateDbParameter("BusinessCode", item.BusinessCode));
                                        command.Parameters.Add(db.CreateDbParameter("BusinessName", item.BusinessName));
                                        command.Parameters.Add(db.CreateDbParameter("ArrivalCycle", item.ArrivalCycle));
                                        command.Parameters.Add(db.CreateDbParameter("PurchasingCycle", item.PurchasingCycle));
                                        //command.Parameters.Add(db.CreateDbParameter("PartsManagementCostGradeId", item.PartsManagementCostGradeId));
                                        //command.Parameters.Add(db.CreateDbParameter("PartsClaimCoefficient", item.PartsClaimCoefficient));
                                        //command.Parameters.Add(db.CreateDbParameter("ClaimPriceCategory", item.ClaimPriceCategory));
                                        //command.Parameters.Add(db.CreateDbParameter("IfSPByLabor", item.IfSPByLabor));
                                        //command.Parameters.Add(db.CreateDbParameter("LaborUnitPrice", item.LaborUnitPrice));
                                        //command.Parameters.Add(db.CreateDbParameter("LaborCoefficient", item.LaborCoefficient));
                                        //command.Parameters.Add(db.CreateDbParameter("IfHaveOutFee", item.IfHaveOutFee));
                                        //command.Parameters.Add(db.CreateDbParameter("OutServiceCarUnitPrice", item.OutServiceCarUnitPrice));
                                        //command.Parameters.Add(db.CreateDbParameter("OutSubsidyPrice", item.OutSubsidyPrice));
                                        //command.Parameters.Add(db.CreateDbParameter("OldPartTransCoefficient", item.OldPartTransCoefficient));
                                        command.Parameters.Add(db.CreateDbParameter("Remark", item.Remark));
                                        command.Parameters.Add(db.CreateDbParameter("ModifierId", userInfo.Id));
                                        command.Parameters.Add(db.CreateDbParameter("ModifierName", userInfo.Name));
                                        command.Parameters.Add(db.CreateDbParameter("ModifyTime", DateTime.Now));
                                        command.Parameters.Add(db.CreateDbParameter("Id", item.Id));
                                        command.ExecuteNonQuery();
                                        #endregion
                                    }
                                }
                                //无异常提交
                                ts.Commit();
                            } catch(Exception) {
                                //报错回滚
                                ts.Rollback();
                                throw;
                            } finally {
                                if(conn.State == System.Data.ConnectionState.Open) {
                                    conn.Close();
                                }
                            }
                        }
                    }
                    #endregion
                    #region 导出所有不合格数据
                    errorList = allList.Except(rightList).ToList();
                    if(errorList.Any()) {
                        excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                        errorDataFileName = GetErrorFilePath(fileName);
                        using(var excelExport = new ExcelExport(errorDataFileName)) {
                            var list = errorList;
                            excelExport.ExportByRow(index => {
                                if(index == list.Count + 1)
                                    return null;
                                if(index == 0)
                                    return excelColumns.ToArray();
                                var tempObj = list[index - 1];
                                var values = new object[] {
                                #region 设置错误信息导出的列的值

                                tempObj.BranchCodeStr, tempObj.BranchNameStr,
                                tempObj.PartsSalesCategoryNameStr ,tempObj.SupplierCodeStr ,
                                tempObj.SupplierNameStr ,tempObj.BusinessCodeStr,tempObj.BusinessNameStr,tempObj.ArrivalCycleStr,
                                tempObj.PurchasingCycleStr,tempObj.RemarkStr ,
                                tempObj.ErrorMsg

                                #endregion                     
                            };
                                return values;
                            });
                        }
                        errorList = null;
                    }
                    #endregion
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
            }
        }


    }
}
