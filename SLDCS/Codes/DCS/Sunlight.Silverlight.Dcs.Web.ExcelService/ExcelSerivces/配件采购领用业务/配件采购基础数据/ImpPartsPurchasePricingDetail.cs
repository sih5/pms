﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        /// <summary>
        /// 校验采购价格变更申请单清单导入配件
        /// </summary>
        public bool ImpPartsPurchasePricingDetail(string fileName, int partsSalesCategoryId, int branchId, out int excelImportNum, out List<ImpPartsPurchasePricingDetail> rightData, out List<ImpPartsPurchasePricingDetail> errorData, out string errorDataFileName, out string errorMessage) {
            //初始化返回数据
            excelImportNum = 0;
            errorDataFileName = "";
            errorMessage = "";
            //List<ImpPartsPurchasePricingDetail> rightPartsPurchasePricingDetails = null;
            //List<ImpPartsPurchasePricingDetail> errorPartsPurchasePricingDetails = null;
            var errorList = new List<ImpPartsPurchasePricingDetail>();
            var rightList = new List<ImpPartsPurchasePricingDetail>();
            var allList = new List<ImpPartsPurchasePricingDetail>();

            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("PartsPurchasePricingDetail", out notNullableFields, out fieldLenght);

                //errorPartsPurchasePricingDetails = new List<ImpPartsPurchasePricingDetail>();
                //var partsPurchasePricingDetails = new List<ImpPartsPurchasePricingDetail>();
                List<object> excelColumns;

                //Excel数据导入
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    //指定Excel列名与字段名关系
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode, "PartCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName, "PartName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_FactoryPurchacePrice_SupplierCode, "SupplierCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_FactoryPurchacePrice_SupplierName, "SupplierName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_SupplierShippingOrder_SupplierpartCode, "SupplierPartCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_FactoryPurchacePrice_Istemp, "PriceType");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsPurReturnOrder_Price, "Price");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsPurchasePricingDetail_LimitQty, "LimitQty");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_SpareParts_IsPrimary, "IsPrimary");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_IsOrderableNew, "IsOrderable");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_FactoryPurchacePrice_ValidFrom, "ValidFrom");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_FactoryPurchacePrice_ValidTo, "ValidTo");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Remark, "Remark");

                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();

                    //Excel单行导入
                    //List<ImpPartsPurchasePricingDetail> details = errorPartsPurchasePricingDetails;
                    //List<ImpPartsPurchasePricingDetail> pricingDetails = partsPurchasePricingDetails;
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        //接收Excel导入数据
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        var tempImportObj = new ImpPartsPurchasePricingDetail {
                            PartCode = newRow["PartCode"],
                            PartName = newRow["PartName"],
                            SupplierCode = newRow["SupplierCode"],
                            SupplierName = newRow["SupplierName"],
                            SupplierPartCode = newRow["SupplierPartCode"],
                            PriceTypeStr = newRow["PriceType"],
                            PriceStr = newRow["Price"],
                            LimitQtyStr = newRow["LimitQty"],
                            IsPrimaryStr = newRow["IsPrimary"],
                            IsOrderableStr =  newRow["IsOrderable"],
                            ValidFromStr = newRow["ValidFrom"],
                            ValidToStr = newRow["ValidTo"],
                            Remark = newRow["Remark"]
                        };
                        var tempErrorMessage = new List<string>();
                        //配件编号检查
                        var fieldIndex = notNullableFields.IndexOf("PartCode".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.PartCode)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchasePricingDetail_Validation1);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.PartCode) > fieldLenght["PartCode".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchasePricingDetail_Validation2);
                        }
                        //配件名称检查
                        if(!string.IsNullOrEmpty(tempImportObj.PartName)) {
                            if(Encoding.Default.GetByteCount(tempImportObj.PartName) > fieldLenght["PartName".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchasePricingDetail_Validation4);
                        }
                        //供应商图号检查
                        fieldIndex = notNullableFields.IndexOf("SupplierPartCode".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.SupplierPartCode)) {
                                tempErrorMessage.Add(ErrorStrings.ImpPartsSupplierRelation_Validation15);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.SupplierPartCode) > fieldLenght["PartCode".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.ImpPartsSupplierRelation_Validation16);
                        }
                        //供应商编号检查
                        fieldIndex = notNullableFields.IndexOf("SupplierCode".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.SupplierCode)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchasePricingDetail_Validation5);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.SupplierCode) > fieldLenght["SupplierCode".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchasePricingDetail_Validation6);
                        }
                        //供应商名称检查
                        if(!string.IsNullOrEmpty(tempImportObj.SupplierName)) {
                            if(Encoding.Default.GetByteCount(tempImportObj.SupplierName) > fieldLenght["SupplierName".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchasePricingDetail_Validation8);
                        }
                        //价格类型检查
                        if(string.IsNullOrWhiteSpace(tempImportObj.PriceTypeStr)) {
                            tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsPurchasePricingDetail_PriceTypeIsNull);
                        } else {
                            int priceType;
                            if (int.TryParse(newRow["PriceType"], out priceType)) {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsPurchasePricingDetail_PriceTypeValue);
                            } else {
                                try {
                                    tempImportObj.PriceType = (int)Enum.Parse(typeof(DcsPurchasePriceType), newRow["PriceType"]);
                                } catch (Exception) {
                                    tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsPurchasePricingDetail_PriceTypeValue);
                                }
                            }
                        }
                        //价格检查
                        fieldIndex = notNullableFields.IndexOf("Price".ToUpper());
                        if(newRow["Price"] == null) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchasePricingDetail_Validation9);
                        } else {
                            Decimal price;
                            if(Decimal.TryParse(newRow["Price"], out price)) {
                                if(price > 0)
                                    tempImportObj.Price = price;
                                else
                                    tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsPurchasePricingDetail_Validation1);
                            } else {
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchasePricingDetail_Validation10);
                            }
                        }
                        //闭口数量检查
                        fieldIndex = notNullableFields.IndexOf("LimitQty".ToUpper());
                        if(newRow["LimitQty"] == null) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsPurchasePricingDetail_LimitQtyIsNull);
                        } else {
                            int limitQty;
                            if(int.TryParse(newRow["LimitQty"], out limitQty)) {
                                if(limitQty > 0)
                                    tempImportObj.LimitQty = limitQty;
                                else
                                    tempImportObj.LimitQty = 0;
                            } else {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsPurchasePricingDetail_LimitQtyNumber);
                            }
                        }
                        //是否首选检查
                        fieldIndex = notNullableFields.IndexOf("IsPrimary".ToUpper());
                        if(newRow["IsPrimary"] == null) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsPurchasePricingDetail_IsPrimaryIsNull);
                        } else {
                            string isPrimary = newRow["IsPrimary"];
                            if(!string.IsNullOrEmpty(isPrimary)) {
                                if(ErrorStrings.Export_Title_PartsBranch_Yes.Equals(isPrimary) || "1".Equals(isPrimary))
                                    tempImportObj.IsPrimary = true;
                                else
                                    tempImportObj.IsPrimary = false;
                            } else {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsPurchasePricingDetail_IsPrimaryValue);
                            }
                        }
                        //是否可采购检查
                        fieldIndex = notNullableFields.IndexOf("IsOrderable".ToUpper());
                        if(newRow["IsOrderable"] == null) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_IsOrderableIsNull);
                        } else {
                            string isOrderable = newRow["IsOrderable"];
                            if(!string.IsNullOrEmpty(isOrderable)) {
                                if(ErrorStrings.Export_Title_PartsBranch_Yes.Equals(isOrderable))
                                    tempImportObj.IsOrderable = true;
                                else if(ErrorStrings.Export_Title_PartsBranch_No.Equals(isOrderable))
                                    tempImportObj.IsOrderable = false;
                                else
                                    tempErrorMessage.Add(ErrorStrings.Export_Validation_BranchSupplierRelation_Validation11);
                            } else {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_BranchSupplierRelation_Validation11);
                            }
                        }
                        //生效时间检查
                        fieldIndex = notNullableFields.IndexOf("ValidFrom".ToUpper());
                        //tempImportObj.ValidFrom = DateTime.Now.Date;
                        if(newRow["ValidFrom"] == null) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchasePricingDetail_Validation11);
                        } else {
                            DateTime validFrom;
                            if(DateTime.TryParse(newRow["ValidFrom"], out validFrom)) {
                                tempImportObj.ValidFrom = validFrom;
                            } else {
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchasePricingDetail_Validation12);
                            }
                        }
                        //失效时间检查
                        fieldIndex = notNullableFields.IndexOf("ValidTo".ToUpper());
                        //tempImportObj.ValidTo = DateTime.Parse("2050-01-01");
                        if(newRow["ValidTo"] == null) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchasePricingDetail_Validation13);
                        } else {
                            DateTime validTo;
                            if(DateTime.TryParse(row["ValidTo"], out validTo)) {
                                tempImportObj.ValidTo = validTo;
                            } else {
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchasePricingDetail_Validation14);
                            }
                        }
                        ////备注检查
                        fieldIndex = notNullableFields.IndexOf("Remark".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.Remark)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchasePricingDetail_Validation15);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.Remark) > fieldLenght["Remark".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchasePricingDetail_Validation16);
                        }

                        if(tempErrorMessage.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });

                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    //检查配件与供应商是否重复
                    foreach(var supplierCodeGroup in tempRightList.GroupBy(v => v.SupplierCode)) {
                        var partCodeGroups = supplierCodeGroup.GroupBy(r => r.PartCode);
                        foreach(var partCodeGroup in partCodeGroups) {
                            if(partCodeGroup.Count() > 1) {
                                foreach(var partCode in partCodeGroup) {
                                    partCode.ErrorMsg = ErrorStrings.Export_Validation_BranchSupplierRelation_Validation12;
                                }
                            }
                        }
                    }

                    //获取配件信息表数据
                    var spareParts = new List<SparePart>();
                    Func<string[], bool> dealSparePart = values => {
                        spareParts.Add(new SparePart {
                            Id = int.Parse(values[0]),
                            Code = values[1],
                            Name = values[2],
                            MeasureUnit = values[3]
                        });
                        return false;
                    };
                    //获取配件供应商基本信息表数据
                    var partsSuppliers = new List<PartsSupplier>();
                    Func<string[], bool> dealPartsSupplier = values => {
                        partsSuppliers.Add(new PartsSupplier {
                            Id = int.Parse(values[0]),
                            Code = values[1],
                            Name = values[2]
                        });
                        return false;
                    };
                    //获取配件与供应商关系表数据
                    //var partsSupplierRelations = new List<PartsSupplierRelation>();
                    //Func<string[], bool> dealPartsSupplierRelation = values => {
                    //    partsSupplierRelations.Add(new PartsSupplierRelation {
                    //        PartId = int.Parse(values[0]),
                    //        SupplierId = int.Parse(values[1]),
                    //        BranchId = int.Parse(values[2]),
                    //        PartsSalesCategoryId = int.Parse(values[3]),
                    //    });
                    //    return false;
                    //};

                    var partCodes = tempRightList.Select(v => v.PartCode.ToUpper()).Distinct().ToArray();
                    var supplierCodes = tempRightList.Select(v => v.SupplierCode).Distinct().ToArray();
                    db.QueryDataWithInOperator(string.Format("select Id, Trim(Code), Trim(Name), MeasureUnit from SparePart where status={0}", (int)DcsMasterDataStatus.有效), "Code", true, partCodes, dealSparePart);

                    var sparePartIds = spareParts.Select(v => v.Id.ToString(CultureInfo.InvariantCulture)).Distinct().ToArray();
                    db.QueryDataWithInOperator(string.Format("select Id, Code, Name from PartsSupplier where status={0}", (int)DcsMasterDataStatus.有效), "Code", false, supplierCodes, dealPartsSupplier);
                    //db.QueryDataWithInOperator(string.Format("select Partid, SupplierId, BranchId,PartsSalesCategoryId from PartsSupplierRelation where status={0}", (int)DcsMasterDataStatus.有效), "Partid", false, sparePartIds, dealPartsSupplierRelation);
                    #region 循环
                    for(var i = tempRightList.Count - 1; i >= 0; i--) {
                        //配件有效性校验
                        var sparePart = spareParts.FirstOrDefault(v => String.Compare(v.Code, tempRightList[i].PartCode, StringComparison.OrdinalIgnoreCase) == 0);
                        if(sparePart == null) {
                            tempRightList[i].ErrorMsg = string.Format(ErrorStrings.ImpPartsPurchasePricingDetail_Validation17, tempRightList[i].PartCode);
                            //errorPartsPurchasePricingDetails.Add(tempRightList[i]);
                            continue;
                        }
                        //供应商有效性校验
                        var partsSupplier = partsSuppliers.FirstOrDefault(v => String.Compare(v.Code, tempRightList[i].SupplierCode, StringComparison.OrdinalIgnoreCase) == 0);
                        if(partsSupplier == null) {
                            tempRightList[i].ErrorMsg = string.Format(ErrorStrings.ImpPartsPurchasePricingDetail_Validation18, tempRightList[i].SupplierCode);
                            //errorPartsPurchasePricingDetails.Add(tempRightList[i]);
                            continue;
                        }
                        //校验配件与供应商的关系
                        //var partsSupplierRelation = partsSupplierRelations.FirstOrDefault(v => v.PartId == sparePart.Id && v.SupplierId == partsSupplier.Id && v.BranchId == branchId && v.PartsSalesCategoryId == partsSalesCategoryId);
                        //if(partsSupplierRelation == null) {
                        //    tempRightList[i].ErrorMsg = string.Format(ErrorStrings.ImpPartsPurchasePricingDetail_Validation19, sparePart.Code, partsSupplier.Code);
                        //    //errorPartsPurchasePricingDetails.Add(tempRightList[i]);
                        //    continue;
                        //}

                        tempRightList[i].PartId = sparePart.Id;
                        tempRightList[i].PartName = sparePart.Name;
                        tempRightList[i].MeasureUnit = sparePart.MeasureUnit;
                        tempRightList[i].SupplierId = partsSupplier.Id;
                        tempRightList[i].SupplierName = partsSupplier.Name;
                    }

                    #endregion
                }

                errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                rightList = allList.Except(errorList).ToList();
                if(rightList.Any()) {
                    var partIds = rightList.Select(r => r.PartId).ToArray().Distinct();
                    var supplierIds = rightList.Select(r => r.SupplierId).ToArray().Distinct();
                    //var partsIdStr = string.Join(",", partIds);
                    //var suppliersIdStr = string.Join(",", supplierIds);
                    using(var conn = db.CreateDbConnection()) {
                        conn.Open();
                        string orcalepartIds = getOracleSQLIn(partIds.ToList(), "partid");
                        string orcalesupplierIds = getOracleSQLIn(supplierIds.ToList(), "partssupplierid");
                        //var sql = string.Format("select partid,partssupplierid,purchaseprice,ValidFrom from partspurchasepricing where {0} and {1} and validfrom<=sysdate and validto>=sysdate and partssalescategoryid={2} and status=2 and pricetype=1", orcalepartIds, orcalesupplierIds, partsSalesCategoryId);
                        //查询配件所有供应商的采购价
                        string orcalepartIds2 = getOracleSQLIn(partIds.ToList(), "a.partid");
                        var sql2 = string.Format("select a.partid,a.partssupplierid,b.name,a.purchaseprice,a.ValidFrom,a.pricetype from partspurchasepricing a inner join partssupplier b on b.id = a.partssupplierid where {0} and a.validfrom<=sysdate and a.validto>=sysdate and a.partssalescategoryid={1} and a.status=2 ", orcalepartIds2, partsSalesCategoryId);
                        //查询配件与供应商关系
                        //string orcalesupplierIds3 = getOracleSQLIn(supplierIds.ToList(), "supplierId");
                        var sql3 = string.Format("select a.partid,a.supplierId,a.isPrimary,b.code,b.name from PartsSupplierRelation a inner join PartsSupplier b on a.supplierId = b.id where {0} and a.partssalescategoryid={1} and a.status=1", orcalepartIds, partsSalesCategoryId);
                        //最近一次失效的原首选供应商价格信息
                        var sql4 = string.Format("select *from (select a.partid,a.partssupplierid,b.name,a.purchaseprice,a.ValidFrom,a.pricetype,row_number() over(partition by a.partid, a.partssupplierid order by a.validto desc) as ydy from partspurchasepricing a inner join partssupplier b on b.id = a.partssupplierid inner join PartsSupplierRelation ps on ps.SupplierId=a.PartsSupplierId and ps.PartId=a.PartId where {0} and a.partssalescategoryid={1} and a.status=99  and ps.IsPrimary=1 and  a.validto < sysdate)where ydy = 1", orcalepartIds2, partsSalesCategoryId);
                        
                        //var cmd = db.CreateDbCommand(sql, conn, null);
                        //var details = new List<ImpPartsPurchasePricingDetail>();
                        //var odr = cmd.ExecuteReader();

                        var details2 = new List<ImpPartsPurchasePricingDetail>();
                        var cmd2 = db.CreateDbCommand(sql2, conn, null);
                        var odr2 = cmd2.ExecuteReader();

                        var details3 = new List<ImpPartsPurchasePricingDetail>();
                        var cmd3 = db.CreateDbCommand(sql3, conn, null);
                        var odr3 = cmd3.ExecuteReader();

                        var details4 = new List<ImpPartsPurchasePricingDetail>();
                        var cmd4 = db.CreateDbCommand(sql4, conn, null);
                        var odr4 = cmd4.ExecuteReader();

                        try {
                            //if(odr.HasRows) {
                            //    while(odr.Read()) {
                            //        var item = new ImpPartsPurchasePricingDetail();
                            //        item.PartId = Convert.ToInt32(odr["partid"]);
                            //        item.SupplierId = Convert.ToInt32(odr["partssupplierid"]);
                            //        item.ReferencePrice = Convert.ToDecimal(odr["purchaseprice"]);
                            //        item.ValidFrom = Convert.ToDateTime(odr["ValidFrom"]);
                            //        details.Add(item);
                            //    }
                            //}
                            if(odr2.HasRows) {
                                while(odr2.Read()) {
                                    var item = new ImpPartsPurchasePricingDetail();
                                    item.PartId = Convert.ToInt32(odr2["partid"]);
                                    item.SupplierId = Convert.ToInt32(odr2["partssupplierid"]);
                                    item.MinPriceSupplier = Convert.ToString(odr2["name"]);
                                    item.ReferencePrice = Convert.ToDecimal(odr2["purchaseprice"]);
                                    item.ValidFrom = Convert.ToDateTime(odr2["ValidFrom"]);
                                    item.OldPriPriceType = Convert.ToInt32(odr2["pricetype"]);
                                    details2.Add(item);
                                }
                            }
                            if(odr4.HasRows) {
                                while(odr4.Read()) {
                                    var item = new ImpPartsPurchasePricingDetail();
                                    item.PartId = Convert.ToInt32(odr4["partid"]);
                                    item.SupplierId = Convert.ToInt32(odr4["partssupplierid"]);
                                    item.MinPriceSupplier = Convert.ToString(odr4["name"]);
                                    item.ReferencePrice = Convert.ToDecimal(odr4["purchaseprice"]);
                                    item.ValidFrom = Convert.ToDateTime(odr4["ValidFrom"]);
                                    item.OldPriPriceType = Convert.ToInt32(odr4["pricetype"]);
                                    details4.Add(item);
                                }
                            }
                            if(odr3.HasRows) {
                                while(odr3.Read()) {
                                    var item = new ImpPartsPurchasePricingDetail();
                                    item.PartId = Convert.ToInt32(odr3["partid"]);
                                    item.SupplierId = Convert.ToInt32(odr3["supplierId"]);
                                    //最低价是否首选
                                    item.IsPrimaryMinPrice = Convert.ToBoolean(odr3["isPrimary"]);
                                    item.SupplierCode = Convert.ToString(odr3["code"]);
                                    item.SupplierName = Convert.ToString(odr3["name"]);
                                    details3.Add(item);
                                }
                            }
                        } finally {
                            if(conn.State == System.Data.ConnectionState.Open) {
                                conn.Close();
                            }
                        }
                        foreach(var rightItem in rightList) {
                            //参考价取首选供应商的价格
                            var primarySpllier = details3.FirstOrDefault(r => r.IsPrimaryMinPrice == true && r.PartId == rightItem.PartId);
                            if(primarySpllier != null) {
                                var referencePrice = details2.Where(r => r.PartId == rightItem.PartId && r.SupplierId == primarySpllier.SupplierId).FirstOrDefault();
                                rightItem.ReferencePrice = referencePrice != null ? referencePrice.ReferencePrice : 0;

                                rightItem.OldPriSupplierId = primarySpllier.SupplierId;
                                rightItem.OldPriSupplierCode = primarySpllier.SupplierCode;
                                rightItem.OldPriSupplierName = primarySpllier.SupplierName;
                                rightItem.OldPriPrice = rightItem.ReferencePrice;
                                if(referencePrice != null)
                                    rightItem.OldPriPriceType = referencePrice.OldPriPriceType;
                                else {
                                    var oldprimarySpllier = details4.Where(r => r.PartId == rightItem.PartId).FirstOrDefault();
                                    if(oldprimarySpllier != null) {
                                        rightItem.OldPriPrice = oldprimarySpllier.ReferencePrice;
                                        if(oldprimarySpllier != null)
                                            rightItem.OldPriPriceType = oldprimarySpllier.OldPriPriceType;
                                    }
                                }
                            } else
                                rightItem.ReferencePrice = 0;
                            //是否是新配件
                            rightItem.IsNewPart = details2.Where(r => r.PartId == rightItem.PartId).Any() ? false : true;
                            //除开本供应商之外的最低采购价
                            var all = details2.Where(x => x.PartId == rightItem.PartId && x.SupplierId != rightItem.SupplierId);
                            if(all.Any()) {
                                var minPrice = details2.FirstOrDefault(r => r.PartId == rightItem.PartId && r.ReferencePrice == all.Min(y => y.ReferencePrice));
                                if(minPrice != null && rightItem.Price >= minPrice.ReferencePrice) {
                                    rightItem.MinPurchasePrice = minPrice.ReferencePrice;
                                    rightItem.MinPriceSupplierId = minPrice.SupplierId;
                                    rightItem.MinPriceSupplier = minPrice.MinPriceSupplier;
                                    var isPrimaryMinPrice = details3.FirstOrDefault(r => r.PartId == rightItem.PartId && r.SupplierId == minPrice.SupplierId);
                                    if(isPrimaryMinPrice != null)
                                        rightItem.IsPrimaryMinPrice = isPrimaryMinPrice.IsPrimaryMinPrice;
                                }
                            }
                        }
                    }
                }

                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var details = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == details.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var partsPurchasePricingDetail = details[index - 1];
                            var values = new object[]{
                                        partsPurchasePricingDetail.PartCode, partsPurchasePricingDetail.PartName,
                                        partsPurchasePricingDetail.SupplierCode, partsPurchasePricingDetail.SupplierName,
                                        partsPurchasePricingDetail.SupplierPartCode,
                                        partsPurchasePricingDetail.PriceTypeStr, partsPurchasePricingDetail.PriceStr,
                                        partsPurchasePricingDetail.LimitQty,partsPurchasePricingDetail.IsPrimaryStr,
                                        partsPurchasePricingDetail.ValidFromStr,partsPurchasePricingDetail.ValidToStr,
                                        partsPurchasePricingDetail.Remark,partsPurchasePricingDetail.ErrorMsg
                                    };
                            return values;
                        });
                    }
                    errorList = null;
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {

                errorData = errorList;
                rightData = rightList;
            }
        }

        public bool ImpPartsPurchasePricingYXDetail(string fileName, int branchId, out int excelImportNum, out List<ImpPartsPurchasePricingDetail> rightData, out List<ImpPartsPurchasePricingDetail> errorData, out string errorDataFileName, out string errorMessage) {
            //初始化返回数据
            excelImportNum = 0;
            errorDataFileName = "";
            errorMessage = "";
            //List<ImpPartsPurchasePricingDetail> rightPartsPurchasePricingDetails = null;
            //List<ImpPartsPurchasePricingDetail> errorPartsPurchasePricingDetails = null;
            var errorList = new List<ImpPartsPurchasePricingDetail>();
            var rightList = new List<ImpPartsPurchasePricingDetail>();
            var allList = new List<ImpPartsPurchasePricingDetail>();

            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("PartsPurchasePricingDetail", out notNullableFields, out fieldLenght);

                //errorPartsPurchasePricingDetails = new List<ImpPartsPurchasePricingDetail>();
                //var partsPurchasePricingDetails = new List<ImpPartsPurchasePricingDetail>();
                List<object> excelColumns;

                //Excel数据导入
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    //指定Excel列名与字段名关系
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Partssalescategory_Name, "PartsSalesCategoryName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode, "PartCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName, "PartName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_FactoryPurchacePrice_SupplierCode, "SupplierCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_FactoryPurchacePrice_SupplierName, "SupplierName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsPurchaseOrder_PriceType, "PriceType");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsPurReturnOrder_Price, "Price");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_FactoryPurchacePrice_ValidFrom, "ValidFrom");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_FactoryPurchacePrice_ValidTo, "ValidTo");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Remark, "Remark");

                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();

                    //Excel单行导入
                    //List<ImpPartsPurchasePricingDetail> details = errorPartsPurchasePricingDetails;
                    //List<ImpPartsPurchasePricingDetail> pricingDetails = partsPurchasePricingDetails;
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        //接收Excel导入数据
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        var tempImportObj = new ImpPartsPurchasePricingDetail {
                            PartsSalesCategoryName = newRow["PartsSalesCategoryName"],
                            PartCode = newRow["PartCode"],
                            PartName = newRow["PartName"],
                            SupplierCode = newRow["SupplierCode"],
                            SupplierName = newRow["SupplierName"],
                            PriceTypeStr = newRow["PriceType"],
                            PriceStr = newRow["Price"],
                            ValidFromStr = newRow["ValidFrom"],
                            ValidToStr = newRow["ValidTo"],
                            Remark = newRow["Remark"]
                        };
                        var tempErrorMessage = new List<string>();
                        if(string.IsNullOrEmpty(tempImportObj.PartsSalesCategoryName)) {
                            tempErrorMessage.Add(ErrorStrings.Export_Validation_MarketingDepartment_PartsSalesCategoryIsNull);
                        }
                        //配件编号检查
                        var fieldIndex = notNullableFields.IndexOf("PartCode".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.PartCode)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchasePricingDetail_Validation1);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.PartCode) > fieldLenght["PartCode".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchasePricingDetail_Validation2);
                        }
                        //配件名称检查
                        fieldIndex = notNullableFields.IndexOf("PartName".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.PartName)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchasePricingDetail_Validation3);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.PartName) > fieldLenght["PartName".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchasePricingDetail_Validation4);
                        }

                        //供应商编号检查
                        fieldIndex = notNullableFields.IndexOf("SupplierCode".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.SupplierCode)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchasePricingDetail_Validation5);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.SupplierCode) > fieldLenght["SupplierCode".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchasePricingDetail_Validation6);
                        }
                        //供应商名称检查
                        if(!string.IsNullOrEmpty(tempImportObj.SupplierName)) {
                            if(Encoding.Default.GetByteCount(tempImportObj.SupplierName) > fieldLenght["SupplierName".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchasePricingDetail_Validation8);
                        }
                        //价格类型检查
                        if(string.IsNullOrWhiteSpace(tempImportObj.PriceTypeStr)) {
                            tempErrorMessage.Add(ErrorStrings.ImpVehicleDLRStartSecurityDeposit_Validation6);
                        } else {
                            try {
                                tempImportObj.PriceType = (int)Enum.Parse(typeof(DcsPurchasePriceType), newRow["PriceType"]);
                            } catch(Exception) {
                                tempErrorMessage.Add(ErrorStrings.ImpVehicleDLRStartSecurityDeposit_Validation7);
                            }
                        }
                        //价格检查
                        fieldIndex = notNullableFields.IndexOf("Price".ToUpper());
                        if(newRow["Price"] == null) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchasePricingDetail_Validation9);
                        } else {
                            Decimal price;
                            if(Decimal.TryParse(newRow["Price"], out price)) {
                                if(price > 0)
                                    tempImportObj.Price = price;
                                else
                                    tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsPurchasePricingDetail_Validation1);
                            } else {
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchasePricingDetail_Validation10);
                            }
                        }
                        //生效时间检查
                        fieldIndex = notNullableFields.IndexOf("ValidFrom".ToUpper());
                        if(newRow["ValidFrom"] == null) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchasePricingDetail_Validation11);
                        } else {
                            DateTime validFrom;
                            if(DateTime.TryParse(newRow["ValidFrom"], out validFrom)) {
                                tempImportObj.ValidFrom = validFrom;
                            } else {
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchasePricingDetail_Validation12);
                            }
                        }
                        //失效时间检查
                        fieldIndex = notNullableFields.IndexOf("ValidTo".ToUpper());
                        if(newRow["ValidTo"] == null) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchasePricingDetail_Validation13);
                        } else {
                            DateTime validTo;
                            if(DateTime.TryParse(row["ValidTo"], out validTo)) {
                                tempImportObj.ValidTo = validTo;
                            } else {
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchasePricingDetail_Validation14);
                            }
                        }
                        //备注检查
                        fieldIndex = notNullableFields.IndexOf("Remark".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.Remark)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchasePricingDetail_Validation15);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.Remark) > fieldLenght["Remark".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchasePricingDetail_Validation16);
                        }

                        if(tempErrorMessage.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });

                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    //获取配件信息表数据
                    var spareParts = new List<SparePart>();
                    Func<string[], bool> dealSparePart = values => {
                        spareParts.Add(new SparePart {
                            Id = int.Parse(values[0]),
                            Code = values[1],
                            Name = values[2],
                            MeasureUnit = values[3],
                            OverseasPartsFigure = values[4],
                        });
                        return false;
                    };
                    //获取配件供应商基本信息表数据
                    var partsSuppliers = new List<PartsSupplier>();
                    Func<string[], bool> dealPartsSupplier = values => {
                        partsSuppliers.Add(new PartsSupplier {
                            Id = int.Parse(values[0]),
                            Code = values[1],
                            Name = values[2]
                        });
                        return false;
                    };
                    //获取配件与供应商关系表数据
                    var partsSupplierRelations = new List<PartsSupplierRelation>();
                    Func<string[], bool> dealPartsSupplierRelation = values => {
                        partsSupplierRelations.Add(new PartsSupplierRelation {
                            PartId = int.Parse(values[0]),
                            SupplierId = int.Parse(values[1]),
                            BranchId = int.Parse(values[2]),
                            PartsSalesCategoryId = int.Parse(values[3]),
                        });
                        return false;
                    };
                    var partsSalesCategories = new List<PartsSalesCategory>();
                    Func<string[], bool> dealPartsSalesCategory = values => {
                        partsSalesCategories.Add(new PartsSalesCategory {
                            Id = int.Parse(values[0]),
                            Code = values[1],
                            Name = values[2]
                        });
                        return false;
                    };
                    var partsSalesCategoryNames = tempRightList.Select(v => v.PartsSalesCategoryName).Distinct().ToArray();
                    db.QueryDataWithInOperator(string.Format("select Id,Code,Name from PartsSalesCategory where status={0}", (int)DcsMasterDataStatus.有效), "Name", false, partsSalesCategoryNames, dealPartsSalesCategory);

                    foreach(var item in tempRightList) {
                        var partSalesCategory = partsSalesCategories.FirstOrDefault(r => r.Name == item.PartsSalesCategoryName);
                        item.PartsSalesCategoryId = partSalesCategory.Id.ToString();
                    }

                    var partCodes = tempRightList.Select(v => v.PartCode.ToUpper()).Distinct().ToArray();
                    var supplierCodes = tempRightList.Select(v => v.SupplierCode).Distinct().ToArray();
                    db.QueryDataWithInOperator(string.Format("select Id, Trim(Code), Trim(Name), MeasureUnit,OverseasPartsFigure from SparePart where status={0}", (int)DcsMasterDataStatus.有效), "Code", true, partCodes, dealSparePart);

                    var sparePartIds = spareParts.Select(v => v.Id.ToString(CultureInfo.InvariantCulture)).Distinct().ToArray();
                    db.QueryDataWithInOperator(string.Format("select Id, Code, Name from PartsSupplier where status={0}", (int)DcsMasterDataStatus.有效), "Code", false, supplierCodes, dealPartsSupplier);
                    db.QueryDataWithInOperator(string.Format("select Partid, SupplierId, BranchId,PartsSalesCategoryId from PartsSupplierRelation where status={0}", (int)DcsMasterDataStatus.有效), "Partid", false, sparePartIds, dealPartsSupplierRelation);
                    #region 循环
                    for(var i = tempRightList.Count - 1; i >= 0; i--) {
                        //配件有效性校验
                        var sparePart = spareParts.FirstOrDefault(v => String.Compare(v.Code, tempRightList[i].PartCode, StringComparison.OrdinalIgnoreCase) == 0);
                        if(sparePart == null) {
                            tempRightList[i].ErrorMsg = string.Format(ErrorStrings.ImpPartsPurchasePricingDetail_Validation17, tempRightList[i].PartCode);
                            //errorPartsPurchasePricingDetails.Add(tempRightList[i]);
                            continue;
                        }
                        //供应商有效性校验
                        var partsSupplier = partsSuppliers.FirstOrDefault(v => String.Compare(v.Code, tempRightList[i].SupplierCode, StringComparison.OrdinalIgnoreCase) == 0);
                        if(partsSupplier == null) {
                            tempRightList[i].ErrorMsg = string.Format(ErrorStrings.ImpPartsPurchasePricingDetail_Validation18, tempRightList[i].SupplierCode);
                            //errorPartsPurchasePricingDetails.Add(tempRightList[i]);
                            continue;
                        }
                        //校验配件与供应商的关系
                        var partsSupplierRelation = partsSupplierRelations.FirstOrDefault(v => v.PartId == sparePart.Id && v.SupplierId == partsSupplier.Id && v.BranchId == branchId && v.PartsSalesCategoryId == Convert.ToInt32(tempRightList[i].PartsSalesCategoryId));
                        if(partsSupplierRelation == null) {
                            tempRightList[i].ErrorMsg = string.Format(ErrorStrings.ImpPartsPurchasePricingDetail_Validation19, sparePart.Code, partsSupplier.Code);
                            //errorPartsPurchasePricingDetails.Add(tempRightList[i]);
                            continue;
                        }

                        tempRightList[i].PartId = sparePart.Id;
                        tempRightList[i].PartName = sparePart.Name;
                        tempRightList[i].MeasureUnit = sparePart.MeasureUnit;
                        tempRightList[i].SupplierId = partsSupplier.Id;
                        tempRightList[i].SupplierName = partsSupplier.Name;
                        tempRightList[i].OverseasPartsFigureQuery = sparePart.OverseasPartsFigure;
                    }

                    #endregion
                }

                errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                rightList = allList.Except(errorList).ToList();
                if(rightList.Any()) {
                    var partIds = rightList.Select(r => r.PartId).ToArray().Distinct();
                    var supplierIds = rightList.Select(r => r.SupplierId).ToArray().Distinct();
                    var partsSalesCategoryIds = rightList.Select(r => r.PartsSalesCategoryId).ToArray().Distinct();
                    //var partsIdStr = string.Join(",", partIds);
                    //var suppliersIdStr = string.Join(",", supplierIds);
                    using(var conn = db.CreateDbConnection()) {
                        conn.Open();
                        string orcalepartIds = getOracleSQLIn(partIds.ToList(), "partid");
                        string orcalesupplierIds = getOracleSQLIn(supplierIds.ToList(), "partssupplierid");
                        string orcalepartsSalesCategoryIds = getOracleSQLIn(partsSalesCategoryIds.ToList(), "partssalescategoryid");
                        var sql = string.Format("select partid,partssupplierid,purchaseprice,ValidFrom from partspurchasepricing where {0} and {1} and validfrom<=sysdate and validto>=sysdate and {2} and status=2 and pricetype=1", orcalepartIds, orcalesupplierIds, orcalepartsSalesCategoryIds);
                        var cmd = db.CreateDbCommand(sql, conn, null);
                        var details = new List<ImpPartsPurchasePricingDetail>();
                        var odr = cmd.ExecuteReader();
                        try {
                            if(odr.HasRows) {
                                while(odr.Read()) {
                                    var item = new ImpPartsPurchasePricingDetail();
                                    item.PartId = Convert.ToInt32(odr["partid"]);
                                    item.SupplierId = Convert.ToInt32(odr["partssupplierid"]);
                                    item.ReferencePrice = Convert.ToDecimal(odr["purchaseprice"]);
                                    item.ValidFrom = Convert.ToDateTime(odr["ValidFrom"]);
                                    details.Add(item);
                                }
                            }
                        } finally {
                            if(conn.State == System.Data.ConnectionState.Open) {
                                conn.Close();
                            }
                        }
                        foreach(var rightItem in rightList) {
                            var referencePrice = details.Where(r => r.SupplierId == rightItem.SupplierId && r.PartId == rightItem.PartId).OrderByDescending(r => r.ValidFrom).FirstOrDefault();
                            rightItem.ReferencePrice = referencePrice != null ? referencePrice.ReferencePrice : 0;
                        }
                    }
                }

                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var details = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == details.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var partsPurchasePricingDetail = details[index - 1];
                            var values = new object[]{
                                        partsPurchasePricingDetail.PartsSalesCategoryName,
                                        partsPurchasePricingDetail.PartCode, partsPurchasePricingDetail.PartName,
                                        partsPurchasePricingDetail.SupplierCode, partsPurchasePricingDetail.SupplierName,
                                        partsPurchasePricingDetail.PriceTypeStr, partsPurchasePricingDetail.PriceStr,
                                        partsPurchasePricingDetail.ValidFromStr,partsPurchasePricingDetail.ValidToStr,
                                        partsPurchasePricingDetail.Remark,partsPurchasePricingDetail.ErrorMsg
                                    };
                            return values;
                        });
                    }
                    errorList = null;
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {

                errorData = errorList;
                rightData = rightList;
            }
        }


        #region oracle 查询条件in超过1000条解决方案
        /// <summary>
        /// oracle 查询条件in超过1000条解决方案
        /// </summary>
        /// <param name="ids">值的集合</param>
        /// <param name="field">字段名</param>
        /// <returns></returns>
        private string getOracleSQLIn(List<int> ids, string field) {
            int count = Math.Min(ids.Count, 1000);
            int len = ids.Count;
            int size = len % count;
            if(size == 0) {
                size = len / count;
            } else {
                size = (len / count) + 1;
            }
            StringBuilder builder = new StringBuilder();
            for(int i = 0; i < size; i++) {
                int fromIndex = i * count;
                int toIndex = Math.Min(fromIndex + count, len);
                string productId = string.Join("','", getArrayValues(fromIndex, toIndex, ids).ToArray());
                if(i != 0) {
                    builder.Append(" or ");
                }
                builder.Append(field).Append(" in ('").Append(productId).Append("')");
            }
            return builder.ToString();
        }
        public List<int> getArrayValues(int fromindex, int toindex, List<int> array) {
            List<int> listret = new List<int>();
            for(int i = fromindex; i < toindex; i++) {
                listret.Add(array[i]);
            }
            return listret;
        }
        #endregion
    }
}
