﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        /// <summary>
        /// 导入配件与供应商关系
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="excelImportNum"></param>
        /// <param name="errorData"></param>
        /// <param name="errorDataFileName"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public bool ImpPartsSupplierRelation(string fileName, out int excelImportNum, out List<ImpPartsSupplierRelation> errorData, out string errorDataFileName, out string errorMessage) {
            //初始化返回数据
            excelImportNum = 0;
            errorDataFileName = "";
            errorMessage = "";
            List<ImpPartsSupplierRelation> errorPartsSupplierRelations = null;

            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("PartsSupplierRelation", out notNullableFields, out fieldLenght);

                List<string> notNullableFieldsBranch;
                Dictionary<string, int> fieldLenghtBranch;
                db.GetTableSchema("Branch", out notNullableFieldsBranch, out fieldLenghtBranch);


                List<string> notNullableFieldsSparePart;
                Dictionary<string, int> fieldLenghtSparePart;
                db.GetTableSchema("SparePart", out notNullableFieldsSparePart, out fieldLenghtSparePart);

                List<string> notNullableFieldsPartsSupplier;
                Dictionary<string, int> fieldLenghtPartsSupplier;
                db.GetTableSchema("PartsSupplier", out notNullableFieldsPartsSupplier, out fieldLenghtPartsSupplier);

                errorPartsSupplierRelations = new List<ImpPartsSupplierRelation>();
                var partsSupplierRelations = new List<ImpPartsSupplierRelation>();
                List<object> excelColumns;

                //Excel数据导入
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    //指定Excel列名与字段名关系
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_BranchSupplierRelation_Code, "BranchCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Branch_BranchName, "BranchName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Partssalescategory_Name, "PartsSalesCategoryName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Code, "PartCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName, "PartName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_FactoryPurchacePrice_SupplierCode, "SupplierCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_FactoryPurchacePrice_SupplierName, "SupplierName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_SupplierShippingOrder_SupplierpartCode, "SupplierPartCode");
                    //excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsPurchasePricing_IsPrimary, "IsPrimary");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsSupplierRelation_PurchasePercentage, "PurchasePercentage");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsSupplierRelation_EconomicalBatch, "EconomicalBatch");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsSupplierRelation_MinBatch, "MinBatch");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Remark, "Remark");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsSupplierRelation_MonthlyArrivalPeriod, "MonthlyArrivalPeriod");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsSupplierRelation_ArrivalReplenishmentCycle, "ArrivalReplenishmentCycle");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsSupplierRelation_EmergencyArrivalPeriod, "EmergencyArrivalPeriod");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsSupplierRelation_OrderCycle, "OrderCycle");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsSupplierRelation_DefaultOrderWarehouseName, "DefaultOrderWarehouseName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsSupplierRelation_OrderGoodsCycle, "OrderGoodsCycle");
                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();

                    //Excel单行导入
                    List<ImpPartsSupplierRelation> relations = errorPartsSupplierRelations;
                    List<ImpPartsSupplierRelation> supplierRelations = partsSupplierRelations;
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        //接收Excel导入数据
                        var partsSupplierRelation = new ImpPartsSupplierRelation {
                            BranchCode = row["BranchCode"],
                            BranchName = row["BranchName"],
                            PartsSalesCategoryName = row["PartsSalesCategoryName"],
                            PartCode = row["PartCode"],
                            PartName = row["PartName"],
                            SupplierCode = row["SupplierCode"],
                            SupplierName = row["SupplierName"],
                            SupplierPartCode = row["SupplierPartCode"],
                            Remark = row["Remark"],
                            MonthlyArrivalPeriod = Convert.ToInt32(row["MonthlyArrivalPeriod"]),
                            ArrivalReplenishmentCycle = Convert.ToInt32(row["ArrivalReplenishmentCycle"]),
                            EmergencyArrivalPeriod = Convert.ToInt32(row["EmergencyArrivalPeriod"]),
                            DefaultOrderWarehouseName = row["DefaultOrderWarehouseName"],
                            OrderCycleStr = row["OrderCycle"],
                            OrderGoodsCycleStr = row["OrderGoodsCycle"],
                        };
                        var errorMsgs = new List<string>();
                        //分公司编号检查
                        var fieldIndex = notNullableFieldsBranch.IndexOf("Code".ToUpper());
                        //if(string.IsNullOrEmpty(partsSupplierRelation.BranchCode)) {
                        //    if(fieldIndex > -1)
                        //        errorMsgs.Add(ErrorStrings.ImpPartsSupplierRelation_Validation1);
                        //} else {
                        //    if(Encoding.Default.GetByteCount(partsSupplierRelation.BranchCode) > fieldLenghtBranch["Code".ToUpper()])
                        //        errorMsgs.Add(ErrorStrings.ImpPartsSupplierRelation_Validation2);
                        //}
                        ////分公司名称检查
                        //fieldIndex = notNullableFieldsBranch.IndexOf("Name".ToUpper());
                        //if(string.IsNullOrEmpty(partsSupplierRelation.BranchCode)) {
                        //    if(fieldIndex > -1)
                        //        errorMsgs.Add(ErrorStrings.ImpPartsSupplierRelation_Validation3);
                        //} else {
                        //    if(Encoding.Default.GetByteCount(partsSupplierRelation.BranchName) > fieldLenghtBranch["Name".ToUpper()])
                        //        errorMsgs.Add(ErrorStrings.ImpPartsSupplierRelation_Validation4);
                        //}

                        //品牌：销售类型名称检查
                        fieldIndex = notNullableFields.IndexOf("PartsSalesCategoryName".ToUpper());
                        if(string.IsNullOrEmpty(partsSupplierRelation.PartsSalesCategoryName)) {
                            if(fieldIndex > -1)
                                errorMsgs.Add(ErrorStrings.ImpPartsSupplierRelation_Validation5);
                        } else {
                            if(Encoding.Default.GetByteCount(partsSupplierRelation.PartsSalesCategoryName) > fieldLenght["PartsSalesCategoryName".ToUpper()])
                                errorMsgs.Add(ErrorStrings.ImpPartsSupplierRelation_Validation6);
                        }
                        //配件图号检查
                        fieldIndex = notNullableFieldsSparePart.IndexOf("Code".ToUpper());
                        if(string.IsNullOrEmpty(partsSupplierRelation.PartCode)) {
                            if(fieldIndex > -1)
                                errorMsgs.Add(ErrorStrings.ImpPartsSupplierRelation_Validation7);
                        } else {
                            if(Encoding.Default.GetByteCount(partsSupplierRelation.PartCode) > fieldLenghtSparePart["Code".ToUpper()])
                                errorMsgs.Add(ErrorStrings.ImpPartsSupplierRelation_Validation8);
                        }
                        //配件名称检查
                        fieldIndex = notNullableFieldsSparePart.IndexOf("Name".ToUpper());
                        if(string.IsNullOrEmpty(partsSupplierRelation.PartName)) {
                            if(fieldIndex > -1)
                                errorMsgs.Add(ErrorStrings.ImpPartsSupplierRelation_Validation9);
                        } else {
                            if(Encoding.Default.GetByteCount(partsSupplierRelation.PartName) > fieldLenghtSparePart["Name".ToUpper()])
                                errorMsgs.Add(ErrorStrings.ImpPartsSupplierRelation_Validation10);
                        }
                        //供应商编号检查
                        fieldIndex = notNullableFields.IndexOf("SupplierId".ToUpper());
                        if(string.IsNullOrEmpty(partsSupplierRelation.SupplierCode)) {
                            if(fieldIndex > -1)
                                errorMsgs.Add(ErrorStrings.ImpPartsSupplierRelation_Validation11);
                        } else {
                            if(Encoding.Default.GetByteCount(partsSupplierRelation.SupplierCode) > fieldLenghtPartsSupplier["Code".ToUpper()])
                                errorMsgs.Add(ErrorStrings.ImpPartsSupplierRelation_Validation12);
                        }
                        //供应商名称检查
                        //if(!string.IsNullOrEmpty(partsSupplierRelation.SupplierName)) {
                        //    if(Encoding.Default.GetByteCount(partsSupplierRelation.SupplierName) > fieldLenghtPartsSupplier["Name".ToUpper()])
                        //        errorMsgs.Add(ErrorStrings.ImpPartsSupplierRelation_Validation14);
                        //}

                        //供应商使用图号检查
                        //fieldIndex = notNullableFields.IndexOf("SupplierPartCode".ToUpper());
                        if(string.IsNullOrEmpty(partsSupplierRelation.SupplierPartCode)) {
                            //if(fieldIndex > -1)
                                errorMsgs.Add(ErrorStrings.ImpPartsSupplierRelation_Validation15);
                        } else {
                            if(Encoding.Default.GetByteCount(partsSupplierRelation.SupplierPartCode) > fieldLenght["SupplierPartCode".ToUpper()])
                                errorMsgs.Add(ErrorStrings.ImpPartsSupplierRelation_Validation16);
                        }

                        ////是否首选供应商检查
                        //fieldIndex = notNullableFields.IndexOf("IsPrimary".ToUpper());
                        //if(row["IsPrimary"] == null) {
                        //    if(fieldIndex > -1)
                        //        errorMsgs.Add(ErrorStrings.ImpPartsSupplierRelation_Validation17);
                        //} else {
                        //    partsSupplierRelation.IsPrimaryStr = row["IsPrimary"] != null ? row["IsPrimary"].Trim() : "";

                        //    if(row["IsPrimary"] == ErrorStrings.Export_Title_PartsBranch_Yes) {
                        //        partsSupplierRelation.IsPrimary = true;
                        //    } else if(row["IsPrimary"] == ErrorStrings.Export_Title_PartsBranch_No) {
                        //        partsSupplierRelation.IsPrimary = false;
                        //    } else {
                        //        errorMsgs.Add(ErrorStrings.ImpPartsSupplierRelation_Validation18);
                        //    }
                        //}

                        //是否首选默认为否
                        partsSupplierRelation.IsPrimary = false;

                        //采购占比检查
                        fieldIndex = notNullableFields.IndexOf("PurchasePercentage".ToUpper());
                        if(row["PurchasePercentage"] == null) {
                            if(fieldIndex > -1)
                                errorMsgs.Add(ErrorStrings.ImpPartsSupplierRelation_Validation19);
                        } else {
                            partsSupplierRelation.PurchasePercentageStr = row["PurchasePercentage"] != null ? row["PurchasePercentage"].Trim() : "";
                            decimal purchasePercentage;
                            if(decimal.TryParse(row["PurchasePercentage"], out purchasePercentage)) {
                                partsSupplierRelation.PurchasePercentage = purchasePercentage;
                            } else {
                                errorMsgs.Add(ErrorStrings.ImpPartsSupplierRelation_Validation20);
                            }
                        }

                        //经济采购批量检查
                        fieldIndex = notNullableFields.IndexOf("EconomicalBatch".ToUpper());
                        if(row["EconomicalBatch"] == null) {
                            if(fieldIndex > -1)
                                errorMsgs.Add(ErrorStrings.ImpPartsSupplierRelation_Validation21);
                        } else {
                            partsSupplierRelation.EconomicalBatchStr = row["EconomicalBatch"] != null ? row["EconomicalBatch"].Trim() : "";
                            int economicalBatch;
                            if(int.TryParse(row["EconomicalBatch"], out economicalBatch)) {
                                partsSupplierRelation.EconomicalBatch = economicalBatch;
                            } else {
                                errorMsgs.Add(ErrorStrings.ImpPartsSupplierRelation_Validation22);
                            }
                        }
                        //最小采购批量检查
                        fieldIndex = notNullableFields.IndexOf("MinBatch".ToUpper());
                        if(row["MinBatch"] == null) {
                            if(fieldIndex > -1)
                                errorMsgs.Add(ErrorStrings.ImpPartsSupplierRelation_Validation23);
                        } else {
                            partsSupplierRelation.MinBatchStr = row["MinBatch"] != null ? row["MinBatch"].Trim() : "";
                            int minBatch;
                            if(int.TryParse(row["MinBatch"], out minBatch)) {
                                partsSupplierRelation.MinBatch = minBatch;
                            } else {
                                errorMsgs.Add(ErrorStrings.ImpPartsSupplierRelation_Validation24);
                            }
                        }
                        //订货周期检查
                        if(row["OrderGoodsCycle"] == null) {
                            //errorMsgs.Add("发货周期不能为空");
                        } else {
                            partsSupplierRelation.OrderGoodsCycleStr = row["OrderGoodsCycle"] != null ? row["OrderGoodsCycle"].Trim() : "";
                            int orderGoodsCycle;
                            if(int.TryParse(row["OrderGoodsCycle"], out orderGoodsCycle)) {
                                partsSupplierRelation.OrderGoodsCycle =orderGoodsCycle;
                            } else {
                                errorMsgs.Add(ErrorStrings.Export_Validation_PartsSupplierRelation_OrderGoodsCycleNumber);
                            }
                        }
                        //发货周期检查
                        if(row["OrderCycle"] == null) {
                            //errorMsgs.Add("发货周期不能为空");
                        } else {
                            partsSupplierRelation.OrderCycleStr = row["OrderCycle"] != null ? row["OrderCycle"].Trim() : "";
                            int orderCycle;
                            if(int.TryParse(row["OrderCycle"], out orderCycle)) {
                                partsSupplierRelation.OrderCycle = orderCycle;
                            } else {
                                errorMsgs.Add(ErrorStrings.Export_Validation_PartsSupplierRelation_OrderCycleNumber);
                            }
                        }
                        //备注检查
                        fieldIndex = notNullableFields.IndexOf("Remark".ToUpper());
                        if(string.IsNullOrEmpty(partsSupplierRelation.Remark)) {
                            if(fieldIndex > -1)
                                errorMsgs.Add(ErrorStrings.ImpPartsSupplierRelation_Validation25);
                        } else {
                            if(Encoding.Default.GetByteCount(partsSupplierRelation.Remark) > fieldLenght["Remark".ToUpper()])
                                errorMsgs.Add(ErrorStrings.ImpPartsSupplierRelation_Validation26);
                        }

                        //默认订货仓库检查
                        //if(string.IsNullOrEmpty(partsSupplierRelation.DefaultOrderWarehouseName)) {
                        //    errorMsgs.Add("默认旧件仓库不能为空");
                        //} else {
                        //    if(Encoding.Default.GetByteCount(partsSupplierRelation.DefaultOrderWarehouseName) > fieldLenght["DefaultOrderWarehouseName".ToUpper()])
                        //        errorMsgs.Add("默认旧件仓库过长");
                        //}

                        if(errorMsgs.Count > 0) {
                            partsSupplierRelation.ErrorMsg = string.Join("; ", errorMsgs);
                            relations.Add(partsSupplierRelation);
                        } else
                            supplierRelations.Add(partsSupplierRelation);
                        return false;
                    });
                    //获取营销分公司表数据
                    var branchs = new List<Branch>();
                    Func<string[], bool> dealBranch = values => {
                        branchs.Add(new Branch {
                            Id = int.Parse(values[0]),
                            Code = values[1],
                            Name = values[2]
                        });
                        return false;
                    };

                    //获取销售类型表数据
                    var partsSalesCategorys = new List<PartsSalesCategory>();
                    Func<string[], bool> dealPartsSalesCategory = values => {
                        partsSalesCategorys.Add(new PartsSalesCategory {
                            Id = int.Parse(values[0]),
                            Name = values[1],
                            BranchId = int.Parse(values[2]),
                            BranchCode = values[3],
                            BranchName = values[4],
                        });
                        return false;
                    };
                    //获取配件信息表数据
                    var spareParts = new List<SparePart>();
                    Func<string[], bool> dealSparePart = values => {
                        spareParts.Add(new SparePart {
                            Id = int.Parse(values[0]),
                            Code = values[1],
                            Name = values[2]
                        });
                        return false;
                    };
                    //获取配件供应商基本信息表数据
                    var partsSuppliers = new List<PartsSupplier>();
                    Func<string[], bool> dealPartsSupplier = values => {
                        partsSuppliers.Add(new PartsSupplier {
                            Id = int.Parse(values[0]),
                            Code = values[1],
                            Name = values[2]
                        });
                        return false;
                    };

                    //获取配件与供应商关系表数据
                    var impPartsSupplierRelations = new List<PartsSupplierRelation>();
                    Func<string[], bool> dealPartsSupplierRelation = values => {
                        impPartsSupplierRelations.Add(new PartsSupplierRelation {
                            BranchId = int.Parse(values[0]),
                            PartsSalesCategoryName = values[1],
                            SupplierId = int.Parse(values[2]),
                            PartsSalesCategoryId = int.Parse(values[3]),
                            PartId = int.Parse(values[4]),
                            SupplierPartName = values[5]
                        });
                        return false;
                    };

                    //获取仓库数据
                    var warehouses = new List<WarehouseExtend>();
                    Func<string[], bool> dbwarehouses = values => {
                        warehouses.Add(new WarehouseExtend {
                            StorageCompanyId = int.Parse(values[2]),
                            Id = int.Parse(values[0]),
                            Name = values[1]
                        });
                        return false;
                    };

                    var partsSupplierRelationBranchCodes = partsSupplierRelations.Select(v => v.BranchCode).Distinct().ToArray();
                    db.QueryDataWithInOperator(string.Format("select Id, Code, Name from Branch where status={0}", (int)DcsMasterDataStatus.有效), "Code", false, partsSupplierRelationBranchCodes, dealBranch);

                    var partsSalesCategoryNames = partsSupplierRelations.Select(v => v.PartsSalesCategoryName).Distinct().ToArray();
                    db.QueryDataWithInOperator(string.Format("select Id, Name,BranchId,BranchCode,BranchName from PartsSalesCategory where status={0}", (int)DcsBaseDataStatus.有效), "Name", false, partsSalesCategoryNames, dealPartsSalesCategory);

                    var partsCodes = partsSupplierRelations.Select(v => v.PartCode.ToUpper()).Distinct().ToArray();
                    db.QueryDataWithInOperator(string.Format("select Id, Trim(Code), Trim(Name) from SparePart where status={0}", (int)DcsMasterDataStatus.有效), "Code", true, partsCodes, dealSparePart);

                    var warehouseNames = partsSupplierRelations.Select(v => v.DefaultOrderWarehouseName).Distinct().ToArray();
                    db.QueryDataWithInOperator(string.Format("select Id, name, StorageCompanyId from Warehouse where status={0} and Type !=99 ", (int)DcsMasterDataStatus.有效), "Name", true, warehouseNames, dbwarehouses);

                    var partsSupplierCodes = partsSupplierRelations.Select(v => v.SupplierCode).Distinct().ToArray();
                    db.QueryDataWithInOperator(string.Format("select Id, Code, Name from PartsSupplier where status={0}", (int)DcsMasterDataStatus.有效), "Code", false, partsSupplierCodes, dealPartsSupplier);
                    db.QueryDataWithInOperator(string.Format("select BranchId, PartsSalesCategoryName, SupplierId, PartsSalesCategoryId, PartId, SupplierPartName from PartsSupplierRelation where status={0}", (int)DcsBaseDataStatus.有效), "PartsSalesCategoryName", false, partsSalesCategoryNames, dealPartsSupplierRelation);

                    //校验文件内分公司编号、分公司名称、品牌、配件图号、供应商编号组合唯一
                    var groups = partsSupplierRelations.GroupBy(r => new {
                        r.BranchCode,
                        r.BranchName,
                        r.PartsSalesCategoryName,
                        r.PartCode,
                        r.SupplierCode
                    }).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        groupItem.ErrorMsg = ErrorStrings.ImpPartsSupplierRelation_Validation31;
                        errorPartsSupplierRelations.Add(groupItem);
                    }
                    partsSupplierRelations = partsSupplierRelations.Where(r => r.ErrorMsg == null).ToList();

                    for(var i = partsSupplierRelations.Count - 1; i >= 0; i--) {
                        //校验分公司编号、分公司名称组合存在于营销分公司
                        if (!string.IsNullOrEmpty(partsSupplierRelations[i].BranchCode) && !string.IsNullOrEmpty(partsSupplierRelations[i].BranchName)) {
                            var branch = branchs.FirstOrDefault(v => v.Code == partsSupplierRelations[i].BranchCode && v.Name == partsSupplierRelations[i].BranchName);
                            if (branch == null) {
                                partsSupplierRelations[i].ErrorMsg = string.Format(ErrorStrings.ImpPartsSupplierRelation_Validation27, partsSupplierRelations[i].BranchCode, partsSupplierRelations[i].BranchName);
                                errorPartsSupplierRelations.Add(partsSupplierRelations[i]);
                                continue;
                            }
                        }

                        //校验品牌存在于配件销售类型
                        var partsSalesCategory = partsSalesCategorys.FirstOrDefault(v => v.Name == partsSupplierRelations[i].PartsSalesCategoryName);
                        if(partsSalesCategory == null) {
                            partsSupplierRelations[i].ErrorMsg = string.Format(ErrorStrings.ImpPartsSupplierRelation_Validation28, partsSupplierRelations[i].PartsSalesCategoryName);
                            errorPartsSupplierRelations.Add(partsSupplierRelations[i]);
                            continue;
                        }

                        //校验配件图号、配件名称组合存在配件信息
                        var sparePart = spareParts.FirstOrDefault(v => v.Code == partsSupplierRelations[i].PartCode);
                        if(sparePart == null) {
                            partsSupplierRelations[i].ErrorMsg = string.Format(ErrorStrings.Export_Validation_PartsBranch_SparePartIsNull, partsSupplierRelations[i].PartCode);
                            errorPartsSupplierRelations.Add(partsSupplierRelations[i]);
                            continue;
                        }
                        //校验供应商编号、供应商名称组合存在于配件供应商基本信息
                        var partsSupplier = partsSuppliers.FirstOrDefault(v => v.Code == partsSupplierRelations[i].SupplierCode);
                        if(partsSupplier == null) {
                            partsSupplierRelations[i].ErrorMsg = string.Format(ErrorStrings.ImpPartsSupplierRelation_Validation30, partsSupplierRelations[i].SupplierCode);
                            errorPartsSupplierRelations.Add(partsSupplierRelations[i]);
                            continue;
                        }
                        partsSupplierRelations[i].BranchId = partsSalesCategory.BranchId;
                        partsSupplierRelations[i].BranchCode = partsSalesCategory.BranchCode;
                        partsSupplierRelations[i].BranchName = partsSalesCategory.BranchName;
                        partsSupplierRelations[i].PartsSalesCategoryId = partsSalesCategory.Id;
                        partsSupplierRelations[i].PartsSalesCategoryName = partsSalesCategory.Name;
                        partsSupplierRelations[i].PartId = sparePart.Id;
                        partsSupplierRelations[i].PartName = sparePart.Name;
                        partsSupplierRelations[i].SupplierId = partsSupplier.Id;
                        partsSupplierRelations[i].SupplierName = partsSupplier.Name;
                        //校验默认订货仓库存在于仓库
                        if (!string.IsNullOrEmpty(partsSupplierRelations[i].DefaultOrderWarehouseName)) {
                            var warehouse = warehouses.FirstOrDefault(v => v.Name == partsSupplierRelations[i].DefaultOrderWarehouseName && v.StorageCompanyId == partsSupplierRelations[i].BranchId);
                            if (warehouse == null) {
                                partsSupplierRelations[i].ErrorMsg = string.Format(ErrorStrings.Export_Validation_PartsSupplierRelation_Validation1, partsSupplierRelations[i].DefaultOrderWarehouseName);
                                errorPartsSupplierRelations.Add(partsSupplierRelations[i]);
                                continue;
                            }
                            partsSupplierRelations[i].DefaultOrderWarehouseId = warehouse.Id;
                        }
                        //校验数据库内营销分公司Id、供应商Id、配件销售类型Id、配件Id组合唯一
                        var dbPartsSupplierRelations = impPartsSupplierRelations.FirstOrDefault(v => v.BranchId == partsSupplierRelations[i].BranchId && v.SupplierId == partsSupplierRelations[i].SupplierId && v.PartsSalesCategoryId == partsSupplierRelations[i].PartsSalesCategoryId && v.PartId == partsSupplierRelations[i].PartId);
                        if(dbPartsSupplierRelations != null) {
                            partsSupplierRelations[i].ErrorMsg = ErrorStrings.ImpPartsSupplierRelation_Validation32;
                            errorPartsSupplierRelations.Add(partsSupplierRelations[i]);
                        }

                    }
                }
                var rightPartsSupplierRelations = partsSupplierRelations.Where(r => r.ErrorMsg == null).ToArray();
                if(errorPartsSupplierRelations.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var relations = errorPartsSupplierRelations;
                        excelExport.ExportByRow(index => {
                            if(index == relations.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var partsSupplierRelation = relations[index - 1];
                            var values = new object[]{
                                        partsSupplierRelation.BranchCode , partsSupplierRelation.BranchName ,
                                        partsSupplierRelation.PartsSalesCategoryName , partsSupplierRelation.PartCode ,
                                        partsSupplierRelation.PartName , partsSupplierRelation.SupplierCode ,
                                        partsSupplierRelation.SupplierName, partsSupplierRelation.SupplierPartCode,
                                        /*partsSupplierRelation.IsPrimaryStr,*/
                                        partsSupplierRelation.PurchasePercentageStr,partsSupplierRelation.EconomicalBatchStr,
                                        partsSupplierRelation.MinBatchStr,partsSupplierRelation.Remark,
                                        partsSupplierRelation.MonthlyArrivalPeriod,partsSupplierRelation.ArrivalReplenishmentCycle,
                                        partsSupplierRelation.EmergencyArrivalPeriod,partsSupplierRelation.OrderCycleStr,partsSupplierRelation.DefaultOrderWarehouseName,partsSupplierRelation.OrderGoodsCycleStr,partsSupplierRelation.ErrorMsg
                                    };
                            return values;
                        });
                    }
                    errorPartsSupplierRelations = null;
                }

                //导入正常结束，不存在需要写入数据库的合法数据
                if(!rightPartsSupplierRelations.Any())
                    return true;
                var userInfo = Utils.GetCurrentUserInfo();
                //写数据
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    //开启事务，新增更新配件在一个事务内
                    var ts = conn.BeginTransaction();
                    try {
                        //新增配件
                        if(rightPartsSupplierRelations.Any()) {
                            //获取新增数据的sql语句，Id为主键
                            var sqlInsert = db.GetInsertSql("PartsSupplierRelation", "Id", new[] {
                                "BranchId","PartsSalesCategoryId", "PartsSalesCategoryName","PartId",
                                "SupplierId",
                                "SupplierPartCode",
                                "SupplierPartName",
                                "IsPrimary","PurchasePercentage","EconomicalBatch",
                                "MinBatch","Status","Remark","MonthlyArrivalPeriod","ArrivalReplenishmentCycle","EmergencyArrivalPeriod","DefaultOrderWarehouseId","DefaultOrderWarehouseName","OrderCycle","OrderGoodsCycle","CREATORID","CREATORNAME","CREATETIME"
                            });
                            var sqlHistoryInsert = db.GetInsertSql("PartsSupplierRelationHistory", "Id", new[] {
                                "PartsSupplierRelationId","BranchId","PartsSalesCategoryId", "PartsSalesCategoryName","PartId",
                                "SupplierId","SupplierPartCode","SupplierPartName","IsPrimary","PurchasePercentage","EconomicalBatch",
                                "MinBatch","Status","Remark","CREATORID","CREATORNAME","CREATETIME"
                                 });
                            foreach(var partsSupplierRelation in rightPartsSupplierRelations) {
                                var command = db.CreateDbCommand(sqlInsert, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("BranchId", partsSupplierRelation.BranchId));
                                command.Parameters.Add(db.CreateDbParameter("PartsSalesCategoryId", partsSupplierRelation.PartsSalesCategoryId));
                                command.Parameters.Add(db.CreateDbParameter("PartsSalesCategoryName", partsSupplierRelation.PartsSalesCategoryName));
                                command.Parameters.Add(db.CreateDbParameter("PartId", partsSupplierRelation.PartId));
                                command.Parameters.Add(db.CreateDbParameter("SupplierId", partsSupplierRelation.SupplierId));
                                command.Parameters.Add(db.CreateDbParameter("SupplierPartCode", partsSupplierRelation.SupplierPartCode));
                                command.Parameters.Add(db.CreateDbParameter("SupplierPartName", partsSupplierRelation.SupplierPartName));
                                command.Parameters.Add(db.CreateDbParameter("IsPrimary", partsSupplierRelation.IsPrimary));
                                command.Parameters.Add(db.CreateDbParameter("PurchasePercentage", partsSupplierRelation.PurchasePercentage));
                                command.Parameters.Add(db.CreateDbParameter("EconomicalBatch", partsSupplierRelation.EconomicalBatch));
                                command.Parameters.Add(db.CreateDbParameter("MinBatch", partsSupplierRelation.MinBatch));
                                command.Parameters.Add(db.CreateDbParameter("Status", (int)DcsBaseDataStatus.有效));
                                command.Parameters.Add(db.CreateDbParameter("Remark", partsSupplierRelation.Remark));
                                command.Parameters.Add(db.CreateDbParameter("MonthlyArrivalPeriod", partsSupplierRelation.MonthlyArrivalPeriod));
                                command.Parameters.Add(db.CreateDbParameter("ArrivalReplenishmentCycle", partsSupplierRelation.ArrivalReplenishmentCycle));
                                command.Parameters.Add(db.CreateDbParameter("EmergencyArrivalPeriod", partsSupplierRelation.EmergencyArrivalPeriod));
                                command.Parameters.Add(db.CreateDbParameter("DefaultOrderWarehouseId", partsSupplierRelation.DefaultOrderWarehouseId));
                                command.Parameters.Add(db.CreateDbParameter("DefaultOrderWarehouseName", partsSupplierRelation.DefaultOrderWarehouseName));
                                command.Parameters.Add(db.CreateDbParameter("OrderCycle", partsSupplierRelation.OrderCycle));
                                command.Parameters.Add(db.CreateDbParameter("OrderGoodsCycle", partsSupplierRelation.OrderGoodsCycle));
                                command.Parameters.Add(db.CreateDbParameter("CREATORID", userInfo.Id));
                                command.Parameters.Add(db.CreateDbParameter("CREATORNAME", userInfo.Name));
                                command.Parameters.Add(db.CreateDbParameter("CREATETIME", DateTime.Now));
                                var tempId = db.ExecuteInsert(command, "Id");
                                //加履历
                                var commandHistory = db.CreateDbCommand(sqlHistoryInsert, conn, ts);
                                commandHistory.Parameters.Add(db.CreateDbParameter("PartsSupplierRelationId", tempId));
                                commandHistory.Parameters.Add(db.CreateDbParameter("BranchId", partsSupplierRelation.BranchId));
                                commandHistory.Parameters.Add(db.CreateDbParameter("PartsSalesCategoryId", partsSupplierRelation.PartsSalesCategoryId));
                                commandHistory.Parameters.Add(db.CreateDbParameter("PartsSalesCategoryName", partsSupplierRelation.PartsSalesCategoryName));
                                commandHistory.Parameters.Add(db.CreateDbParameter("PartId", partsSupplierRelation.PartId));
                                commandHistory.Parameters.Add(db.CreateDbParameter("SupplierId", partsSupplierRelation.SupplierId));
                                commandHistory.Parameters.Add(db.CreateDbParameter("SupplierPartCode", partsSupplierRelation.SupplierPartCode));
                                commandHistory.Parameters.Add(db.CreateDbParameter("SupplierPartName", partsSupplierRelation.SupplierPartName));
                                commandHistory.Parameters.Add(db.CreateDbParameter("IsPrimary", partsSupplierRelation.IsPrimary));
                                commandHistory.Parameters.Add(db.CreateDbParameter("PurchasePercentage", partsSupplierRelation.PurchasePercentage));
                                commandHistory.Parameters.Add(db.CreateDbParameter("EconomicalBatch", partsSupplierRelation.EconomicalBatch));
                                commandHistory.Parameters.Add(db.CreateDbParameter("MinBatch", partsSupplierRelation.MinBatch));
                                commandHistory.Parameters.Add(db.CreateDbParameter("Status", (int)DcsBaseDataStatus.有效));
                                commandHistory.Parameters.Add(db.CreateDbParameter("Remark", partsSupplierRelation.Remark));
                                commandHistory.Parameters.Add(db.CreateDbParameter("CREATORID", userInfo.Id));
                                commandHistory.Parameters.Add(db.CreateDbParameter("CREATORNAME", userInfo.Name));
                                commandHistory.Parameters.Add(db.CreateDbParameter("CREATETIME", DateTime.Now));
                                commandHistory.ExecuteNonQuery();
                            }
                        }
                        //无异常提交
                        ts.Commit();
                    } catch(Exception) {
                        //报错回滚
                        ts.Rollback();
                        throw;
                    } finally {
                        if(conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorPartsSupplierRelations = null;
                return false;
            } finally {
                errorData = errorPartsSupplierRelations;
            }
        }

        /// <summary>
        /// 变更导入配件与供应商关系
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="excelImportNum"></param>
        /// <param name="errorData"></param>
        /// <param name="errorDataFileName"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public bool ImpUpdatePartsSupplierRelation(string fileName, out int excelImportNum, out List<ImpPartsSupplierRelation> errorData, out string errorDataFileName, out string errorMessage) {
            //初始化返回数据
            excelImportNum = 0;
            errorDataFileName = "";
            errorMessage = "";
            List<ImpPartsSupplierRelation> errorPartsSupplierRelations = null;

            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("PartsSupplierRelation", out notNullableFields, out fieldLenght);

                List<string> notNullableFieldsBranch;
                Dictionary<string, int> fieldLenghtBranch;
                db.GetTableSchema("Branch", out notNullableFieldsBranch, out fieldLenghtBranch);


                List<string> notNullableFieldsSparePart;
                Dictionary<string, int> fieldLenghtSparePart;
                db.GetTableSchema("SparePart", out notNullableFieldsSparePart, out fieldLenghtSparePart);

                List<string> notNullableFieldsPartsSupplier;
                Dictionary<string, int> fieldLenghtPartsSupplier;
                db.GetTableSchema("PartsSupplier", out notNullableFieldsPartsSupplier, out fieldLenghtPartsSupplier);

                errorPartsSupplierRelations = new List<ImpPartsSupplierRelation>();
                var partsSupplierRelations = new List<ImpPartsSupplierRelation>();
                List<object> excelColumns;

                //Excel数据导入
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    //指定Excel列名与字段名关系
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_BranchSupplierRelation_Code, "BranchCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Branch_BranchName, "BranchName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Partssalescategory_Name, "PartsSalesCategoryName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Code, "PartCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName, "PartName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_FactoryPurchacePrice_SupplierCode, "SupplierCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_FactoryPurchacePrice_SupplierName, "SupplierName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_SupplierShippingOrder_SupplierpartCode, "SupplierPartCode");
                    //excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsPurchasePricing_IsPrimary, "IsPrimary");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsSupplierRelation_PurchasePercentage, "PurchasePercentage");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsSupplierRelation_EconomicalBatch, "EconomicalBatch");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsSupplierRelation_MinBatch, "MinBatch");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Remark, "Remark");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsSupplierRelation_OrderCycle, "OrderCycle");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsSupplierRelation_MonthlyArrivalPeriod, "MonthlyArrivalPeriod");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsSupplierRelation_ArrivalReplenishmentCycle, "ArrivalReplenishmentCycle");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsSupplierRelation_EmergencyArrivalPeriod, "EmergencyArrivalPeriod");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsSupplierRelation_OrderGoodsCycle, "OrderGoodsCycle");

                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();

                    //Excel单行导入
                    List<ImpPartsSupplierRelation> relations = errorPartsSupplierRelations;
                    List<ImpPartsSupplierRelation> supplierRelations = partsSupplierRelations;
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        //接收Excel导入数据
                        var partsSupplierRelation = new ImpPartsSupplierRelation {
                            BranchCode = row["BranchCode"],
                            BranchName = row["BranchName"],
                            PartsSalesCategoryName = row["PartsSalesCategoryName"],
                            PartCode = row["PartCode"],
                            PartName = row["PartName"],
                            SupplierCode = row["SupplierCode"],
                            SupplierName = row["SupplierName"],
                            SupplierPartCode = row["SupplierPartCode"],
                            Remark = row["Remark"],
                            OrderCycle = Convert.ToInt32(row["OrderCycle"]),
                            MonthlyArrivalPeriod = Convert.ToInt32(row["MonthlyArrivalPeriod"]),
                            ArrivalReplenishmentCycle = Convert.ToInt32(row["ArrivalReplenishmentCycle"]),
                            EmergencyArrivalPeriod = Convert.ToInt32(row["EmergencyArrivalPeriod"]),
                            OrderGoodsCycle = Convert.ToInt32(row["OrderGoodsCycle"]),
                        };
                        var errorMsgs = new List<string>();
                        //分公司编号检查
                        var fieldIndex = notNullableFieldsBranch.IndexOf("Code".ToUpper());
                        if(string.IsNullOrEmpty(partsSupplierRelation.BranchCode)) {
                            if(fieldIndex > -1)
                                errorMsgs.Add(ErrorStrings.ImpPartsSupplierRelation_Validation1);
                        } else {
                            if(Encoding.Default.GetByteCount(partsSupplierRelation.BranchCode) > fieldLenghtBranch["Code".ToUpper()])
                                errorMsgs.Add(ErrorStrings.ImpPartsSupplierRelation_Validation2);
                        }
                        //分公司名称检查
                        //fieldIndex = notNullableFieldsBranch.IndexOf("Name".ToUpper());
                        //if(string.IsNullOrEmpty(partsSupplierRelation.BranchCode)) {
                        //    if(fieldIndex > -1)
                        //        errorMsgs.Add(ErrorStrings.ImpPartsSupplierRelation_Validation3);
                        //} else {
                        //    if(Encoding.Default.GetByteCount(partsSupplierRelation.BranchName) > fieldLenghtBranch["Name".ToUpper()])
                        //        errorMsgs.Add(ErrorStrings.ImpPartsSupplierRelation_Validation4);
                        //}

                        //品牌：销售类型名称检查
                        fieldIndex = notNullableFields.IndexOf("PartsSalesCategoryName".ToUpper());
                        if(string.IsNullOrEmpty(partsSupplierRelation.PartsSalesCategoryName)) {
                            if(fieldIndex > -1)
                                errorMsgs.Add(ErrorStrings.ImpPartsSupplierRelation_Validation5);
                        } else {
                            if(Encoding.Default.GetByteCount(partsSupplierRelation.PartsSalesCategoryName) > fieldLenght["PartsSalesCategoryName".ToUpper()])
                                errorMsgs.Add(ErrorStrings.ImpPartsSupplierRelation_Validation6);
                        }
                        //配件图号检查
                        fieldIndex = notNullableFieldsSparePart.IndexOf("Code".ToUpper());
                        if(string.IsNullOrEmpty(partsSupplierRelation.PartCode)) {
                            if(fieldIndex > -1)
                                errorMsgs.Add(ErrorStrings.ImpPartsSupplierRelation_Validation7);
                        } else {
                            if(Encoding.Default.GetByteCount(partsSupplierRelation.PartCode) > fieldLenghtSparePart["Code".ToUpper()])
                                errorMsgs.Add(ErrorStrings.ImpPartsSupplierRelation_Validation8);
                        }
                        //配件名称检查
                        //fieldIndex = notNullableFieldsSparePart.IndexOf("Name".ToUpper());
                        //if(string.IsNullOrEmpty(partsSupplierRelation.PartName)) {
                        //    if(fieldIndex > -1)
                        //        errorMsgs.Add(ErrorStrings.ImpPartsSupplierRelation_Validation9);
                        //} else {
                        //    if(Encoding.Default.GetByteCount(partsSupplierRelation.PartName) > fieldLenghtSparePart["Name".ToUpper()])
                        //        errorMsgs.Add(ErrorStrings.ImpPartsSupplierRelation_Validation10);
                        //}
                        //供应商编号检查
                        fieldIndex = notNullableFields.IndexOf("SupplierId".ToUpper());
                        if(string.IsNullOrEmpty(partsSupplierRelation.SupplierCode)) {
                            if(fieldIndex > -1)
                                errorMsgs.Add(ErrorStrings.ImpPartsSupplierRelation_Validation11);
                        } else {
                            if(Encoding.Default.GetByteCount(partsSupplierRelation.SupplierCode) > fieldLenghtPartsSupplier["Code".ToUpper()])
                                errorMsgs.Add(ErrorStrings.ImpPartsSupplierRelation_Validation12);
                        }
                        //供应商名称检查
                        //if(!string.IsNullOrEmpty(partsSupplierRelation.SupplierName)) {
                        //    if(Encoding.Default.GetByteCount(partsSupplierRelation.SupplierName) > fieldLenghtPartsSupplier["Name".ToUpper()])
                        //        errorMsgs.Add(ErrorStrings.ImpPartsSupplierRelation_Validation14);
                        //}

                        //供应商使用图号检查
                        //fieldIndex = notNullableFields.IndexOf("SupplierPartCode".ToUpper());
                        if(string.IsNullOrEmpty(partsSupplierRelation.SupplierPartCode)) {
                            //if(fieldIndex > -1)
                                errorMsgs.Add(ErrorStrings.ImpPartsSupplierRelation_Validation15);
                        } else {
                            if(Encoding.Default.GetByteCount(partsSupplierRelation.SupplierPartCode) > fieldLenght["SupplierPartCode".ToUpper()])
                                errorMsgs.Add(ErrorStrings.ImpPartsSupplierRelation_Validation16);
                        }

                        ////是否首选供应商检查
                        //fieldIndex = notNullableFields.IndexOf("IsPrimary".ToUpper());
                        //if(row["IsPrimary"] == null) {
                        //    if(fieldIndex > -1)
                        //        errorMsgs.Add(ErrorStrings.ImpPartsSupplierRelation_Validation17);
                        //} else {
                        //    partsSupplierRelation.IsPrimaryStr = row["IsPrimary"] != null ? row["IsPrimary"].Trim() : "";

                        //    if(row["IsPrimary"] == ErrorStrings.Export_Title_PartsBranch_Yes) {
                        //        partsSupplierRelation.IsPrimary = true;
                        //    } else if(row["IsPrimary"] == ErrorStrings.Export_Title_PartsBranch_No) {
                        //        partsSupplierRelation.IsPrimary = false;
                        //    } else {
                        //        errorMsgs.Add(ErrorStrings.ImpPartsSupplierRelation_Validation18);
                        //    }
                        //}

                        //采购占比检查
                        //fieldIndex = notNullableFields.IndexOf("PurchasePercentage".ToUpper());
                        //if(row["PurchasePercentage"] == null) {
                        //    if(fieldIndex > -1)
                        //        errorMsgs.Add(ErrorStrings.ImpPartsSupplierRelation_Validation19);
                        //} else {
                        //    partsSupplierRelation.PurchasePercentageStr = row["PurchasePercentage"] != null ? row["PurchasePercentage"].Trim() : "";
                        //    decimal purchasePercentage;
                        //    if(decimal.TryParse(row["PurchasePercentage"], out purchasePercentage)) {
                        //        partsSupplierRelation.PurchasePercentage = purchasePercentage;
                        //    } else {
                        //        errorMsgs.Add(ErrorStrings.ImpPartsSupplierRelation_Validation20);
                        //    }
                        //}
                        ////经济采购批量检查
                        //fieldIndex = notNullableFields.IndexOf("EconomicalBatch".ToUpper());
                        //if(row["EconomicalBatch"] == null) {
                        //    if(fieldIndex > -1)
                        //        errorMsgs.Add(ErrorStrings.ImpPartsSupplierRelation_Validation21);
                        //} else {
                        //    partsSupplierRelation.EconomicalBatchStr = row["EconomicalBatch"] != null ? row["EconomicalBatch"].Trim() : "";
                        //    int economicalBatch;
                        //    if(int.TryParse(row["EconomicalBatch"], out economicalBatch)) {
                        //        partsSupplierRelation.EconomicalBatch = economicalBatch;
                        //    } else {
                        //        errorMsgs.Add(ErrorStrings.ImpPartsSupplierRelation_Validation22);
                        //    }
                        //}
                        //最小采购批量检查
                        fieldIndex = notNullableFields.IndexOf("MinBatch".ToUpper());
                        if(row["MinBatch"] == null) {
                            if(fieldIndex > -1)
                                errorMsgs.Add(ErrorStrings.ImpPartsSupplierRelation_Validation23);
                        } else {
                            partsSupplierRelation.MinBatchStr = row["MinBatch"] != null ? row["MinBatch"].Trim() : "";
                            int minBatch;
                            if(int.TryParse(row["MinBatch"], out minBatch)) {
                                partsSupplierRelation.MinBatch = minBatch;
                            } else {
                                errorMsgs.Add(ErrorStrings.ImpPartsSupplierRelation_Validation24);
                            }
                        }
                        //发货周期检查
                        fieldIndex = notNullableFields.IndexOf("OrderCycle".ToUpper());
                        if(row["OrderCycle"] == null) {
                            errorMsgs.Add(ErrorStrings.Export_Validation_PartsSupplierRelation_Validation2);
                        } else {
                            partsSupplierRelation.OrderCycleStr = row["OrderCycle"] != null ? row["OrderCycle"].Trim() : "";
                            int orderCycle;
                            if(int.TryParse(row["OrderCycle"], out orderCycle)) {
                                partsSupplierRelation.OrderCycle = orderCycle;
                            } else {
                                errorMsgs.Add(ErrorStrings.Export_Validation_PartsSupplierRelation_Validation3);
                            }
                        }
                        //订货周期检查
                        fieldIndex = notNullableFields.IndexOf("OrderGoodsCycle".ToUpper());
                        if(row["OrderGoodsCycle"] == null) {
                            errorMsgs.Add(ErrorStrings.Export_Validation_PartsSupplierRelation_Order);
                        } else {
                            partsSupplierRelation.OrderGoodsCycleStr = row["OrderGoodsCycle"] != null ? row["OrderGoodsCycle"].Trim() : "";
                            int orderGoodsCycle;
                            if(int.TryParse(row["OrderGoodsCycle"], out orderGoodsCycle)) {
                                partsSupplierRelation.OrderGoodsCycle = orderGoodsCycle;
                            } else {
                                errorMsgs.Add(ErrorStrings.Export_Validation_PartsSupplierRelation_OrderGoodsCycleNumber);
                            }
                        }
                        //备注检查
                        fieldIndex = notNullableFields.IndexOf("Remark".ToUpper());
                        if(string.IsNullOrEmpty(partsSupplierRelation.Remark)) {
                            if(fieldIndex > -1)
                                errorMsgs.Add(ErrorStrings.ImpPartsSupplierRelation_Validation25);
                        } else {
                            if(Encoding.Default.GetByteCount(partsSupplierRelation.Remark) > fieldLenght["Remark".ToUpper()])
                                errorMsgs.Add(ErrorStrings.ImpPartsSupplierRelation_Validation26);
                        }

                        if(errorMsgs.Count > 0) {
                            partsSupplierRelation.ErrorMsg = string.Join("; ", errorMsgs);
                            relations.Add(partsSupplierRelation);
                        } else
                            supplierRelations.Add(partsSupplierRelation);
                        return false;
                    });
                    //获取营销分公司表数据
                    var branchs = new List<Branch>();
                    Func<string[], bool> dealBranch = values => {
                        branchs.Add(new Branch {
                            Id = int.Parse(values[0]),
                            Code = values[1],
                            Name = values[2]
                        });
                        return false;
                    };

                    //获取销售类型表数据
                    var partsSalesCategorys = new List<PartsSalesCategory>();
                    Func<string[], bool> dealPartsSalesCategory = values => {
                        partsSalesCategorys.Add(new PartsSalesCategory {
                            Id = int.Parse(values[0]),
                            Name = values[1],
                            BranchCode = values[2],
                            BranchName = values[3],
                        });
                        return false;
                    };
                    //获取配件信息表数据
                    var spareParts = new List<SparePart>();
                    Func<string[], bool> dealSparePart = values => {
                        spareParts.Add(new SparePart {
                            Id = int.Parse(values[0]),
                            Code = values[1],
                            Name = values[2]
                        });
                        return false;
                    };
                    //获取配件供应商基本信息表数据
                    var partsSuppliers = new List<PartsSupplier>();
                    Func<string[], bool> dealPartsSupplier = values => {
                        partsSuppliers.Add(new PartsSupplier {
                            Id = int.Parse(values[0]),
                            Code = values[1],
                            Name = values[2]
                        });
                        return false;
                    };

                    //获取配件与供应商关系表数据
                    var impPartsSupplierRelations = new List<PartsSupplierRelation>();
                    Func<string[], bool> dealPartsSupplierRelation = values => {
                        impPartsSupplierRelations.Add(new PartsSupplierRelation {
                            BranchId = int.Parse(values[0]),
                            PartsSalesCategoryName = values[1],
                            SupplierId = int.Parse(values[2]),
                            PartsSalesCategoryId = int.Parse(values[3]),
                            PartId = int.Parse(values[4]),
                            SupplierPartName = values[5],
                            Id = int.Parse(values[6])
                        });
                        return false;
                    };


                    var partsSupplierRelationBranchCodes = partsSupplierRelations.Select(v => v.BranchCode).Distinct().ToArray();
                    db.QueryDataWithInOperator(string.Format("select Id, Code, Name from Branch where status={0}", (int)DcsMasterDataStatus.有效), "Code", false, partsSupplierRelationBranchCodes, dealBranch);

                    var partsSalesCategoryNames = partsSupplierRelations.Select(v => v.PartsSalesCategoryName).Distinct().ToArray();
                    db.QueryDataWithInOperator(string.Format("select Id, Name,BranchCode,BranchName from PartsSalesCategory where status={0}", (int)DcsBaseDataStatus.有效), "Name", false, partsSalesCategoryNames, dealPartsSalesCategory);

                    var partsCodes = partsSupplierRelations.Select(v => v.PartCode.ToUpper()).Distinct().ToArray();
                    db.QueryDataWithInOperator(string.Format("select Id, Trim(Code), Trim(Name) from SparePart where status={0}", (int)DcsMasterDataStatus.有效), "Code", true, partsCodes, dealSparePart);

                    var partsSupplierCodes = partsSupplierRelations.Select(v => v.SupplierCode).Distinct().ToArray();
                    db.QueryDataWithInOperator(string.Format("select Id, Code, Name from PartsSupplier where status={0}", (int)DcsMasterDataStatus.有效), "Code", false, partsSupplierCodes, dealPartsSupplier);
                    db.QueryDataWithInOperator(string.Format("select BranchId, PartsSalesCategoryName, SupplierId, PartsSalesCategoryId, PartId, SupplierPartName ,Id from PartsSupplierRelation where status={0}", (int)DcsBaseDataStatus.有效), "PartsSalesCategoryName", false, partsSalesCategoryNames, dealPartsSupplierRelation);

                    //校验文件内分公司编号、分公司名称、品牌、配件图号、供应商编号组合唯一
                    var groups = partsSupplierRelations.GroupBy(r => new {
                        r.BranchCode,
                        r.BranchName,
                        r.PartsSalesCategoryName,
                        r.PartCode,
                        r.SupplierCode
                    }).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        groupItem.ErrorMsg = ErrorStrings.ImpPartsSupplierRelation_Validation31;
                        errorPartsSupplierRelations.Add(groupItem);
                    }
                    partsSupplierRelations = partsSupplierRelations.Where(r => r.ErrorMsg == null).ToList();

                    for(var i = partsSupplierRelations.Count - 1; i >= 0; i--) {
                        //校验分公司编号、分公司名称组合存在于营销分公司
                        var branch = branchs.FirstOrDefault(v => v.Code == partsSupplierRelations[i].BranchCode);
                        if(branch == null) {
                            partsSupplierRelations[i].ErrorMsg = string.Format(ErrorStrings.ImpPartsSupplierRelation_Validation27, partsSupplierRelations[i].BranchCode, partsSupplierRelations[i].BranchName);
                            errorPartsSupplierRelations.Add(partsSupplierRelations[i]);
                            continue;
                        }

                        //校验品牌存在于配件销售类型
                        var partsSalesCategory = partsSalesCategorys.FirstOrDefault(v => v.Name == partsSupplierRelations[i].PartsSalesCategoryName && v.BranchCode == partsSupplierRelations[i].BranchCode);
                        if(partsSalesCategory == null) {
                            partsSupplierRelations[i].ErrorMsg = string.Format(ErrorStrings.ImpPartsSupplierRelation_Validation28, partsSupplierRelations[i].PartsSalesCategoryName);
                            errorPartsSupplierRelations.Add(partsSupplierRelations[i]);
                            continue;
                        }

                        //校验配件图号、配件名称组合存在配件信息
                        var sparePart = spareParts.FirstOrDefault(v => v.Code == partsSupplierRelations[i].PartCode);
                        if(sparePart == null) {
                            partsSupplierRelations[i].ErrorMsg = string.Format(ErrorStrings.Export_Validation_PartsBranch_SparePartIsNull, partsSupplierRelations[i].PartCode);
                            errorPartsSupplierRelations.Add(partsSupplierRelations[i]);
                            continue;
                        }
                        //校验供应商编号、供应商名称组合存在于配件供应商基本信息
                        var partsSupplier = partsSuppliers.FirstOrDefault(v => v.Code == partsSupplierRelations[i].SupplierCode);
                        if(partsSupplier == null) {
                            partsSupplierRelations[i].ErrorMsg = string.Format(ErrorStrings.ImpPartsSupplierRelation_Validation30, partsSupplierRelations[i].SupplierCode);
                            errorPartsSupplierRelations.Add(partsSupplierRelations[i]);
                            continue;
                        }
                        partsSupplierRelations[i].BranchId = branch.Id;
                        partsSupplierRelations[i].BranchName = branch.Name;
                        partsSupplierRelations[i].PartsSalesCategoryId = partsSalesCategory.Id;
                        partsSupplierRelations[i].PartsSalesCategoryName = partsSalesCategory.Name;
                        partsSupplierRelations[i].PartId = sparePart.Id;
                        partsSupplierRelations[i].PartName = sparePart.Name;
                        partsSupplierRelations[i].SupplierId = partsSupplier.Id;
                        partsSupplierRelations[i].SupplierName = partsSupplier.Name;


                        //校验数据库内营销分公司Id、供应商Id、配件销售类型Id、配件Id组合唯一
                        var dbPartsSupplierRelations = impPartsSupplierRelations.FirstOrDefault(v => v.BranchId == partsSupplierRelations[i].BranchId && v.SupplierId == partsSupplierRelations[i].SupplierId && v.PartsSalesCategoryId == partsSupplierRelations[i].PartsSalesCategoryId && v.PartId == partsSupplierRelations[i].PartId);
                        if(dbPartsSupplierRelations == null) {
                            partsSupplierRelations[i].ErrorMsg = ErrorStrings.ImpPartsSupplierRelation_Validation35;
                            errorPartsSupplierRelations.Add(partsSupplierRelations[i]);
                        } else {
                            partsSupplierRelations[i].Id = dbPartsSupplierRelations.Id;
                        }

                    }
                }
                var rightPartsSupplierRelations = partsSupplierRelations.Where(r => r.ErrorMsg == null).ToArray();
                if(errorPartsSupplierRelations.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var relations = errorPartsSupplierRelations;
                        excelExport.ExportByRow(index => {
                            if(index == relations.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var partsSupplierRelation = relations[index - 1];
                            var values = new object[]{
                                        partsSupplierRelation.BranchCode , partsSupplierRelation.BranchName ,
                                        partsSupplierRelation.PartsSalesCategoryName , partsSupplierRelation.PartCode ,
                                        partsSupplierRelation.PartName , partsSupplierRelation.SupplierCode ,
                                        partsSupplierRelation.SupplierName, partsSupplierRelation.SupplierPartCode,
                                        /*partsSupplierRelation.IsPrimaryStr,*/
                                        partsSupplierRelation.PurchasePercentageStr,partsSupplierRelation.EconomicalBatchStr,
                                        partsSupplierRelation.MinBatchStr,partsSupplierRelation.Remark,partsSupplierRelation.OrderCycleStr,
                                        partsSupplierRelation.MonthlyArrivalPeriod,partsSupplierRelation.ArrivalReplenishmentCycle,
                                        partsSupplierRelation.EmergencyArrivalPeriod,partsSupplierRelation.OrderGoodsCycleStr,partsSupplierRelation.ErrorMsg
                                    };
                            return values;
                        });
                    }
                    errorPartsSupplierRelations = null;
                }

                //导入正常结束，不存在需要写入数据库的合法数据
                if(!rightPartsSupplierRelations.Any())
                    return true;
                var userInfo = Utils.GetCurrentUserInfo();
                //写数据
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    //开启事务，新增更新配件在一个事务内
                    var ts = conn.BeginTransaction();
                    try {
                        //新增配件
                        if(rightPartsSupplierRelations.Any()) {
                            //获取新增数据的sql语句，Id为主键
                            var sqlInsert = db.GetUpdateSql("PartsSupplierRelation", new[] {
                                "BranchId","PartsSalesCategoryId", "PartsSalesCategoryName","PartId",
                                "SupplierId",
                                "SupplierPartCode",
                                "SupplierPartName",
                                /*"IsPrimary",*/"PurchasePercentage","EconomicalBatch",
                                "MinBatch","Status","Remark","OrderCycle","MonthlyArrivalPeriod","ArrivalReplenishmentCycle","EmergencyArrivalPeriod","ModifierId","ModifierName","ModifyTime","OrderGoodsCycle"
                            }, new[] { "Id" });
                            var sqlHistoryInsert = db.GetInsertSql("PartsSupplierRelationHistory", "Id", new[] {
                                "PartsSupplierRelationId","BranchId","PartsSalesCategoryId", "PartsSalesCategoryName","PartId",
                                "SupplierId","SupplierPartCode","SupplierPartName",/*"IsPrimary",*/"PurchasePercentage","EconomicalBatch",
                                "MinBatch","Status","Remark","CREATORID","CREATORNAME","CREATETIME"
                                 });
                            foreach(var partsSupplierRelation in rightPartsSupplierRelations) {
                                var command = db.CreateDbCommand(sqlInsert, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("BranchId", partsSupplierRelation.BranchId));
                                command.Parameters.Add(db.CreateDbParameter("PartsSalesCategoryId", partsSupplierRelation.PartsSalesCategoryId));
                                command.Parameters.Add(db.CreateDbParameter("PartsSalesCategoryName", partsSupplierRelation.PartsSalesCategoryName));
                                command.Parameters.Add(db.CreateDbParameter("PartId", partsSupplierRelation.PartId));
                                command.Parameters.Add(db.CreateDbParameter("SupplierId", partsSupplierRelation.SupplierId));
                                command.Parameters.Add(db.CreateDbParameter("SupplierPartCode", partsSupplierRelation.SupplierPartCode));
                                command.Parameters.Add(db.CreateDbParameter("SupplierPartName", partsSupplierRelation.SupplierPartName));
                                //command.Parameters.Add(db.CreateDbParameter("IsPrimary", partsSupplierRelation.IsPrimary));
                                command.Parameters.Add(db.CreateDbParameter("PurchasePercentage", partsSupplierRelation.PurchasePercentage));
                                command.Parameters.Add(db.CreateDbParameter("EconomicalBatch", partsSupplierRelation.EconomicalBatch));
                                command.Parameters.Add(db.CreateDbParameter("MinBatch", partsSupplierRelation.MinBatch));
                                command.Parameters.Add(db.CreateDbParameter("Status", (int)DcsBaseDataStatus.有效));
                                command.Parameters.Add(db.CreateDbParameter("Remark", partsSupplierRelation.Remark));
                                command.Parameters.Add(db.CreateDbParameter("OrderCycle", partsSupplierRelation.OrderCycle));
                                command.Parameters.Add(db.CreateDbParameter("MonthlyArrivalPeriod", partsSupplierRelation.MonthlyArrivalPeriod));
                                command.Parameters.Add(db.CreateDbParameter("ArrivalReplenishmentCycle", partsSupplierRelation.ArrivalReplenishmentCycle));
                                command.Parameters.Add(db.CreateDbParameter("EmergencyArrivalPeriod", partsSupplierRelation.EmergencyArrivalPeriod));
                                command.Parameters.Add(db.CreateDbParameter("ModifierId", userInfo.Id));
                                command.Parameters.Add(db.CreateDbParameter("ModifierName", userInfo.Name));
                                command.Parameters.Add(db.CreateDbParameter("ModifyTime", DateTime.Now));
                                command.Parameters.Add(db.CreateDbParameter("Id", partsSupplierRelation.Id));
                                command.Parameters.Add(db.CreateDbParameter("OrderGoodsCycle", partsSupplierRelation.OrderGoodsCycle));
                                command.ExecuteNonQuery();
                                //加履历
                                var commandHistory = db.CreateDbCommand(sqlHistoryInsert, conn, ts);
                                commandHistory.Parameters.Add(db.CreateDbParameter("PartsSupplierRelationId", partsSupplierRelation.Id));
                                commandHistory.Parameters.Add(db.CreateDbParameter("BranchId", partsSupplierRelation.BranchId));
                                commandHistory.Parameters.Add(db.CreateDbParameter("PartsSalesCategoryId", partsSupplierRelation.PartsSalesCategoryId));
                                commandHistory.Parameters.Add(db.CreateDbParameter("PartsSalesCategoryName", partsSupplierRelation.PartsSalesCategoryName));
                                commandHistory.Parameters.Add(db.CreateDbParameter("PartId", partsSupplierRelation.PartId));
                                commandHistory.Parameters.Add(db.CreateDbParameter("SupplierId", partsSupplierRelation.SupplierId));
                                commandHistory.Parameters.Add(db.CreateDbParameter("SupplierPartCode", partsSupplierRelation.SupplierPartCode));
                                commandHistory.Parameters.Add(db.CreateDbParameter("SupplierPartName", partsSupplierRelation.SupplierPartName));
                                //commandHistory.Parameters.Add(db.CreateDbParameter("IsPrimary", partsSupplierRelation.IsPrimary));
                                commandHistory.Parameters.Add(db.CreateDbParameter("PurchasePercentage", partsSupplierRelation.PurchasePercentage));
                                commandHistory.Parameters.Add(db.CreateDbParameter("EconomicalBatch", partsSupplierRelation.EconomicalBatch));
                                commandHistory.Parameters.Add(db.CreateDbParameter("MinBatch", partsSupplierRelation.MinBatch));
                                commandHistory.Parameters.Add(db.CreateDbParameter("Status", (int)DcsBaseDataStatus.有效));
                                commandHistory.Parameters.Add(db.CreateDbParameter("Remark", partsSupplierRelation.Remark));
                                commandHistory.Parameters.Add(db.CreateDbParameter("CREATORID", userInfo.Id));
                                commandHistory.Parameters.Add(db.CreateDbParameter("CREATORNAME", userInfo.Name));
                                commandHistory.Parameters.Add(db.CreateDbParameter("CREATETIME", DateTime.Now));
                                commandHistory.ExecuteNonQuery();
                            }
                        }
                        //无异常提交
                        ts.Commit();
                    } catch(Exception) {
                        //报错回滚
                        ts.Rollback();
                        throw;
                    } finally {
                        if(conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorPartsSupplierRelations = null;
                return false;
            } finally {
                errorData = errorPartsSupplierRelations;
            }
        }
    }
}

