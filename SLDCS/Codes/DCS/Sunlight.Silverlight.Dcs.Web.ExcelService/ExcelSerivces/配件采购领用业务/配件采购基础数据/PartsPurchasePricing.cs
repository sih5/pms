﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Globalization;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        /// <summary>
        /// 合并导出配件采购价格
        /// </summary>
        public bool ExportPartsPurchasePricing(int[] ids, int branchId, int? partsSalesCategoryId, string sparePartCode, string sparePartName, string partsSupplierCode, string partsSupplierName, bool? isOrderable, bool? isSalable, string referenceCode, string supplierPartCode, DateTime? availibleTime,bool? isExactExport, out string fileName) {
            fileName = GetExportFilePath("配件采购价格_.xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.AppendFormat(@"Select c.Name,
                                               e.Name,
                                               b.Code,
                                               b.Name,
                                               b.ReferenceCode,
                                               d.Code,
                                               d.Name,
                                               psr.SupplierPartCode,
                                               cast(case psr.IsPrimary when 1 then '是' when 0 then '否' end as varchar2(10)) as IsPrimary,
                                               (select value from keyvalueitem where NAME = 'PurchasePriceType'and key=a.Pricetype) As Pricetype,
                                               a.Purchaseprice,a.LimitQty,a.UsedQty,
                                               a.Validfrom,
                                               a.Validto,
                                               (select value from keyvalueitem where NAME = 'PartsPurchasePricing_Status'and key=a.Status) As Status,
                                                 (select value from keyvalueitem where NAME = 'PurchasePricingDataSource'and key=a.DataSource) As DataSource,
                                               a.Remark,
                                               a.Creatorname,
                                               a.Createtime,
                                               a.Modifiername,
                                               a.Modifytime,
                                               a.Abandonername,
                                               a.Abandontime
                                          From Partspurchasepricing a
                                          Left Join Sparepart b
                                            On a.Partid = b.Id
                                          Left Join Branch c
                                            On a.Branchid = c.Id
                                          Left Join Partssupplier d
                                            On a.Partssupplierid = d.Id
                                          Left Join Partssalescategory e
                                            On a.Partssalescategoryid = e.Id
                                        left join PartsSupplierRelation psr on psr.partid = a.partid and psr.supplierid = a.PartsSupplierId and psr.status <> 99 and psr.partssalescategoryid = a.partssalescategoryid
                                            where a.Status <> 99 and a.branchId = {0}", branchId);
                    var dbParameters = new List<DbParameter>();
                    if(ids != null && ids.Length > 0) {
                        sql.Append(" and a.id in (");
                        for(var i = 0; i < ids.Length; i++) {
                            if(ids.Length == i + 1) {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            } else {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                        }
                        sql.Append(")");
                    } else {
                        if(partsSalesCategoryId.HasValue) {
                            sql.Append(@" and a.partsSalesCategoryId = {0}partsSalesCategoryId ");
                            dbParameters.Add(db.CreateDbParameter("partsSalesCategoryId", partsSalesCategoryId.Value));
                        }
                        if (!string.IsNullOrEmpty(sparePartCode)) {
                            var spareparts = sparePartCode.Split(',');
                            if (spareparts.Length == 1) {
                                var sparepartcode = spareparts[0];
                                if (isExactExport.HasValue && isExactExport.Value == true) {
                                    sql.Append(@" and b.Code= {0}sparePartCode ");
                                    dbParameters.Add(db.CreateDbParameter("sparePartCode",sparepartcode));
                                } else {
                                    sql.Append(@" and b.Code like {0}sparePartCode ");
                                    dbParameters.Add(db.CreateDbParameter("sparePartCode", "%" + sparepartcode + "%"));
                                }
                            } else {
                                for (int i = 0; i < spareparts.Length; i++) {
                                    spareparts[i] = "'" + spareparts[i] + "'";
                                }
                                string codes = string.Join(",", spareparts);
                                sql.Append(@" and b.Code in (" + codes + ")");
                            }
                        }
                        if(!String.IsNullOrEmpty(sparePartName)) {
                            sql.Append(@" and b.Name like {0}sparePartName ");
                            dbParameters.Add(db.CreateDbParameter("sparePartName", "%" + sparePartName + "%"));
                        }
                        if(!String.IsNullOrEmpty(referenceCode)) {
                            sql.Append(@" and b.referenceCode like {0}referenceCode ");
                            dbParameters.Add(db.CreateDbParameter("referenceCode", "%" + referenceCode + "%"));
                        }
                        if(!String.IsNullOrEmpty(supplierPartCode)) {
                            sql.Append(@" and psr.supplierPartCode like {0}supplierPartCode ");
                            dbParameters.Add(db.CreateDbParameter("supplierPartCode", "%" + supplierPartCode + "%"));
                        }
                        if(!String.IsNullOrEmpty(partsSupplierCode)) {
                            sql.Append(@" and d.Code like {0}partsSupplierCode ");
                            dbParameters.Add(db.CreateDbParameter("partsSupplierCode", "%" + partsSupplierCode + "%"));
                        }
                        if(!String.IsNullOrEmpty(partsSupplierName)) {
                            sql.Append(@" and d.Name like {0}partsSupplierName ");
                            dbParameters.Add(db.CreateDbParameter("partsSupplierName", "%" + partsSupplierName + "%"));
                        }
                        if(isOrderable.HasValue) {
                            sql.Append(@" and Exists (Select 1 As C1
                                              From Partsbranch f
                                             Where f.Branchid = a.Branchid
                                               And a.Partssalescategoryid = f.Partssalescategoryid
                                               And a.Partid = f.Partid
                                               And f.Isorderable = {0}isOrderable) ");
                            dbParameters.Add(db.CreateDbParameter("isOrderable", isOrderable.Value));
                        }
                        if(isSalable.HasValue) {
                            sql.Append(@" and Exists (Select 1 As C1
                                              From Partsbranch g
                                             Where g.Branchid = a.Branchid
                                               And a.Partssalescategoryid = g.Partssalescategoryid
                                               And a.Partid = g.Partid
                                               And g.Issalable = {0}isSalable) ");
                            dbParameters.Add(db.CreateDbParameter("isSalable", isSalable.Value));
                        }
                        if(availibleTime.HasValue) {
                            sql.Append(@" and a.ValidFrom <=to_date({0}availibleTime,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = availibleTime.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("availibleTime", tempTime.ToString("G")));
                        }
                        if(availibleTime.HasValue) {
                            sql.Append(@" and a.ValidTo >=to_date({0}availibleTime,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = availibleTime.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("availibleTime", tempTime.ToString("G")));
                        }

                    }

                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    ErrorStrings.Export_Title_Partshistorystock_BranchName, ErrorStrings.Export_Title_Partssalescategory_Name, ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode, ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName,ErrorStrings.Export_Title_Accessinfo_Tmp_ReferenceCode,ErrorStrings.Export_Title_FactoryPurchacePrice_SupplierCode, ErrorStrings.Export_Title_FactoryPurchacePrice_SupplierName,ErrorStrings.Export_Title_SupplierShippingOrder_SupplierpartCode,ErrorStrings.Export_Title_PartsPurchasePricing_IsPrimary, ErrorStrings.Export_Title_PartsPurchaseOrder_PriceType,ErrorStrings.Export_Title_PartsPurReturnOrder_Price,ErrorStrings.Export_Title_PartsPurchasePricingDetail_LimitQty,
                                    ErrorStrings.Export_Title_PartsPurchasePricing_UsedQutity,ErrorStrings.Export_Title_FactoryPurchacePrice_ValidFrom,ErrorStrings.Export_Title_FactoryPurchacePrice_ValidTo, ErrorStrings.Export_Title_AccountPeriod_Status, ErrorStrings.Export_Title_PartsPurchasePricing_DataSource,ErrorStrings.Export_Title_PartsBranch_Remark, ErrorStrings.Export_Title_AccountPeriod_CreatorName, ErrorStrings.Export_Title_AccountPeriod_CreateTime,ErrorStrings.Export_Title_AccountPeriod_ModifierName,ErrorStrings.Export_Title_AccountPeriod_ModifyTime,ErrorStrings.Export_Title_PartsBranch_AbandonerName,ErrorStrings.Export_Title_PartsBranch_AbandonerTime
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }
    }
}