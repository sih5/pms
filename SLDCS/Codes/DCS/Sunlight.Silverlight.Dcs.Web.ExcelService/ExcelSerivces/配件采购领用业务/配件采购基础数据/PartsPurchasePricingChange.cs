﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        public bool ExportPartsPurchasePricingChange(int[] ids, int? partsSalesCategoryId, string code, int? status,string sparePartCode,string sparePartName, DateTime? creatorTimeBegin, DateTime? creatorTimeEnd, out string fileName) {
            fileName = GetExportFilePath("配件采购价格变更申请单_" + ".xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);
                var userInfo = Utils.GetCurrentUserInfo();
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.AppendFormat(@"select b.Name, /*分公司名称*/
                           t.Code, /*申请单编号*/
                           p.Name, /*品牌*/
                           (select value from keyvalueitem where name = 'PartsPurchasePricingChange_Status' and key = t.status) status,/*状态*/
                           t.Remark, /*备注*/
                           t.CreatorName, /*创建人*/
                           t.CreateTime, /*创建时间*/
                           t.ModifierName, /*修改人*/
                           t.ModifyTime, /*修改时间*/
                            t.InitialApproverName, /*审批人*/
                           t.InitialApproveTime, /*审批时间*/
                            t.InitialApproveComment, /*初审意见*/
                            t.CheckerName, /*审核人*/
                           t.CheckTime, /*审核时间*/
                            t.CheckComment, /*审核意见*/
                           t.ApproverName, /*审批人*/
                           t.ApproveTime, /*审批时间*/
                            t.ApprovalComment, /*审批意见*/
                           t.AbandonerName, /*作废人*/
                           t.AbandonTime /*作废时间*/
                          from PartsPurchasePricingChange t
                          LEFT OUTER JOIN Branch b
                            ON t.BranchId = b.Id
                          LEFT OUTER JOIN PartsSalesCategory p
                            ON t.PartsSalesCategoryId = p.Id
                          LEFT OUTER JOIN PartsPurchasePricingDetail pd
                            ON pd.parentid = t.id
                                    where t.branchid={0} ", userInfo.EnterpriseId);
                    var tableNames = sql.ToString().GetRegexNames();
                    var dbParameters = new List<DbParameter>();
                    if(ids != null && ids.Length > 0) {
                        sql.Append(" and t.id in (");
                        for(var i = 0;i < ids.Length;i++) {
                            if(ids.Length == i + 1) {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            } else {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                        }
                        sql.Append(")");
                        sql.Append(" group by b.Name, t.Code, p.Name,  t.status,t.Remark,t.CreatorName,t.CreateTime,t.ModifierName,t.ModifyTime,t.InitialApproverName,t.InitialApproveTime,t.InitialApproveComment,t.CheckerName,t.CheckTime,t.CheckComment,t.ApproverName,t.ApproveTime,t.ApprovalComment,t.AbandonerName,t.AbandonTime");
                    } else {
                        if(!String.IsNullOrEmpty(code)) {
                            sql.Append(@" and t.Code like {0}Code ");
                            dbParameters.Add(db.CreateDbParameter("Code", "%" + code + "%"));
                        }
                        if(!String.IsNullOrEmpty(sparePartCode)) {
                            sql.Append(@" and pd.PartCode like {0}sparePartCode ");
                            dbParameters.Add(db.CreateDbParameter("sparePartCode", "%" + sparePartCode + "%"));
                        }
                        if(!String.IsNullOrEmpty(sparePartName)) {
                            sql.Append(@" and pd.PartName like {0}sparePartName ");
                            dbParameters.Add(db.CreateDbParameter("sparePartName", "%" + sparePartName + "%"));
                        }

                        if(partsSalesCategoryId.HasValue) {
                            sql.Append(@" and t.partsSalesCategoryId ={0}partsSalesCategoryId ");
                            dbParameters.Add(db.CreateDbParameter("partsSalesCategoryId", partsSalesCategoryId.Value));
                        }
                        if(status.HasValue) {
                            sql.Append(@" and t.status ={0}status ");
                            dbParameters.Add(db.CreateDbParameter("status", status.Value));
                        }
                        if(creatorTimeBegin.HasValue) {
                            sql.Append(@" and t.createTime >=To_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = creatorTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(creatorTimeEnd.HasValue) {
                            sql.Append(@" and t.createTime <=To_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = creatorTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                         sql.Append(" group by b.Name, t.Code, p.Name,  t.status,t.Remark,t.CreatorName,t.CreateTime,t.ModifierName,t.ModifyTime,t.InitialApproverName,t.InitialApproveTime,t.InitialApproveComment,t.CheckerName,t.CheckTime,t.CheckComment,t.ApproverName,t.ApproveTime,t.ApprovalComment,t.AbandonerName,t.AbandonTime");
                    }
                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return tableNames;
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }

                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }
    }
}