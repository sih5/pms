﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        public bool ExportPartsSupplierRelation(int[] ids, int branchId, string supplierCode, string supplierName, string partCode, string partName,string overseasPartsFigure, int? partsSalesCategoryId, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName) {
            fileName = GetExportFilePath("配件与供应商关系_.xlsx");
            if(!HttpContext.Current.User.Identity.IsAuthenticated) {
                throw new Exception(ErrorStrings.File_Validation_User);
            }
            try {
                var db = DbHelper.GetDbHelp(ConnectionString);
                using(var conn = db.CreateDbConnection()) {
                    var sql = new StringBuilder();
                    sql.AppendFormat(@"  SELECT B.CODE AS BRANCHCODE,
                                                B.NAME AS BRANCHNAME,
                                                A.PARTSSALESCATEGORYNAME,
                                                C.CODE AS PARTCODE,
                                                C.NAME AS PARTNAME,
                                                D.CODE AS SUPPLIERCODE,
                                                D.NAME AS SUPPLIERNAME,
                                                SUPPLIERPARTCODE,
                                                (select value from keyvalueitem where NAME = 'IsOrNot'and key=A.ISPRIMARY) As ISPRIMARY,
                                                PURCHASEPERCENTAGE,
                                                ECONOMICALBATCH,
                                                MINBATCH,
                                                A.MonthlyArrivalPeriod,
                                                A.ArrivalReplenishmentCycle,
                                                A.EmergencyArrivalPeriod,
                                                OrderCycle,
                                                (select value from keyvalueitem where NAME = 'MasterData_Status'and key=A.STATUS) As Status,
                                                A.REMARK,
                                                a.createtime,
                                                a.creatorname,
                                                a.modifytime,
                                                a.modifiername,
                                                a.OrderGoodsCycle
                                            FROM PARTSSUPPLIERRELATION A
                                            Left JOIN BRANCH B ON A.BRANCHID = B.ID
                                            Left JOIN SPAREPART C ON A.PARTID = C.ID
                                            Left JOIN PARTSSUPPLIER D ON A.SUPPLIERID = D.ID
                                            where A.BranchId = {0} ", branchId);
                    var dbParameters = new List<DbParameter>();
                    if(ids != null && ids.Length > 0) {
                        sql.Append(" and a.id in (");
                        for(var i = 0; i < ids.Length; i++) {
                            if(ids.Length == i + 1) {
                                sql.Append("{0}id" + i);
                                dbParameters.Add(db.CreateDbParameter("id" + i, ids[i]));
                            } else {
                                sql.Append("{0}id" + i + ",");
                                dbParameters.Add(db.CreateDbParameter("id" + i, ids[i]));
                            }
                        }
                        sql.Append(")");
                    } else {
                        if(!string.IsNullOrEmpty(supplierCode)) {
                            sql.Append(" and D.CODE like {0}supplierCode ");
                            dbParameters.Add(db.CreateDbParameter("supplierCode", "%" + supplierCode + "%"));
                        }
                        if(!string.IsNullOrEmpty(supplierName)) {
                            sql.Append(" and D.NAME like {0}supplierName ");
                            dbParameters.Add(db.CreateDbParameter("supplierName", "%" + supplierName + "%"));
                        }
                        if(!string.IsNullOrEmpty(partCode)) {
                            sql.Append(" and C.CODE like {0}partCode ");
                            dbParameters.Add(db.CreateDbParameter("partCode", "%" + partCode + "%"));
                        }
                        if(!string.IsNullOrEmpty(partName)) {
                            sql.Append(" and C.NAME like {0}partName ");
                            dbParameters.Add(db.CreateDbParameter("partName", "%" + partName + "%"));
                        }
                        if(!string.IsNullOrEmpty(overseasPartsFigure)) {
                            sql.Append(" and C.ReferenceCode like {0}ReferenceCode ");
                            dbParameters.Add(db.CreateDbParameter("ReferenceCode", "%" + overseasPartsFigure + "%"));
                        }
                        if(partsSalesCategoryId.HasValue) {
                            sql.Append(" and A.PARTSSALESCATEGORYID={0}partsSalesCategoryId");
                            dbParameters.Add(db.CreateDbParameter("partsSalesCategoryId", partsSalesCategoryId.Value));
                        }
                        if(status.HasValue) {
                            sql.Append(" and A.STATUS={0}status");
                            dbParameters.Add(db.CreateDbParameter("status", status.Value));
                        }
                        if(createTimeBegin.HasValue) {
                            sql.Append(" and A.CreateTime>=To_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(" and A.CreateTime<=To_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                    }
                    DbCommand comm = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    comm.Parameters.AddRange(dbParameters.ToArray());
                    conn.Open();
                    var reader = comm.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0)
                                return new object[] {
                                   ErrorStrings.Export_Title_BranchSupplierRelation_Code,ErrorStrings.Export_Title_Branch_BranchName,ErrorStrings.Export_Title_Partssalescategory_Name,ErrorStrings.Export_Title_PartsBranch_Code,ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName,ErrorStrings.Export_Title_FactoryPurchacePrice_SupplierCode,ErrorStrings.Export_Title_FactoryPurchacePrice_SupplierName,ErrorStrings.Export_Title_SupplierShippingOrder_SupplierpartCode,
                                   ErrorStrings.Export_Title_PartsPurchasePricing_IsPrimary,ErrorStrings.Export_Title_PartsSupplierRelation_PurchasePercentage,ErrorStrings.Export_Title_PartsSupplierRelation_EconomicalBatch,ErrorStrings.Export_Title_PartsSupplierRelation_MinBatch,ErrorStrings.Export_Title_PartsSupplierRelation_MonthlyArrivalPeriod,ErrorStrings.Export_Title_PartsSupplierRelation_ArrivalReplenishmentCycle,ErrorStrings.Export_Title_PartsSupplierRelation_EmergencyArrivalPeriod,ErrorStrings.Export_Title_PartsSupplierRelation_OrderCycle,ErrorStrings.Export_Title_AccountPeriod_Status,ErrorStrings.Export_Title_PartsBranch_Remark,ErrorStrings.Export_Title_AccountPeriod_CreateTime,ErrorStrings.Export_Title_AccountPeriod_CreatorName,ErrorStrings.Export_Title_AccountPeriod_ModifyTime,ErrorStrings.Export_Title_AccountPeriod_ModifierName,ErrorStrings.Export_Title_PartsSupplierRelation_OrderGoodsCycle
                                };
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }

                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }


    }
}
