﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        /// <summary>
        /// 导入配件采购退货单清单
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="supplierId">供应商Id</param>
        /// <param name="warehouseId">仓库Id</param>
        /// <param name="branchId">登陆企业Id</param>
        /// <param name="partsSalesCategoryId"></param>
        /// <param name="rightData"></param>
        /// <param name="excelImportNum"></param>
        /// <param name="errorData"></param>
        /// <param name="errorDataFileName"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public bool ImportPartsPurReturnOrderDetail(string fileName, int supplierId, int warehouseId, int branchId, int partsSalesCategoryId, out List<ImpPartsPurReturnOrderDetail> rightData, out int excelImportNum, out List<ImpPartsPurReturnOrderDetail> errorData, out string errorDataFileName, out string errorMessage) {
            //初始化返回数据
            List<ImpPartsPurReturnOrderDetail> rightPartsPurReturnOrderDetails = null;
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            List<ImpPartsPurReturnOrderDetail> errorPartsPurReturnOrderDetails = null;

            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("PartsPurReturnOrderDetail", out notNullableFields, out fieldLenght);

                errorPartsPurReturnOrderDetails = new List<ImpPartsPurReturnOrderDetail>();
                var partsPurReturnOrderDetails = new List<ImpPartsPurReturnOrderDetail>();
                List<object> excelColumns;

                //Excel数据导入
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    //指定Excel列名与字段名关系
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode, "SparePartCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName, "SparePartName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_WarehouseArea_Quantity, "Quantity");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Remark, "Remark");

                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();

                    //Excel单行导入
                    List<ImpPartsPurReturnOrderDetail> details = errorPartsPurReturnOrderDetails;
                    List<ImpPartsPurReturnOrderDetail> orderDetails = partsPurReturnOrderDetails;
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        //接收Excel导入数据
                        var partsPurReturnOrderDetail = new ImpPartsPurReturnOrderDetail {
                            SparePartCode = row["SparePartCode"],
                            SparePartName = row["SparePartName"],
                            Remark = row["Remark"]
                        };
                        var errorMsgs = new List<string>();
                        //配件编号检查
                        var fieldIndex = notNullableFields.IndexOf("SparePartCode".ToUpper());
                        if(string.IsNullOrEmpty(partsPurReturnOrderDetail.SparePartCode)) {
                            if(fieldIndex > -1)
                                errorMsgs.Add(ErrorStrings.ImpPartsPurReturnOrderDetail_Validation1);
                        } else {
                            if(Encoding.Default.GetByteCount(partsPurReturnOrderDetail.SparePartCode) > fieldLenght["SparePartCode".ToUpper()])
                                errorMsgs.Add(ErrorStrings.ImpPartsPurReturnOrderDetail_Validation2);
                        }
                        //配件名称检查
                        fieldIndex = notNullableFields.IndexOf("SparePartName".ToUpper());
                        if(string.IsNullOrEmpty(partsPurReturnOrderDetail.SparePartName)) {
                            if(fieldIndex > -1)
                                errorMsgs.Add(ErrorStrings.ImpPartsPurReturnOrderDetail_Validation3);
                        } else {
                            if(Encoding.Default.GetByteCount(partsPurReturnOrderDetail.SparePartName) > fieldLenght["SparePartName".ToUpper()])
                                errorMsgs.Add(ErrorStrings.ImpPartsPurReturnOrderDetail_Validation4);
                        }
                        //数量检查
                        fieldIndex = notNullableFields.IndexOf("Quantity".ToUpper());
                        if(row["Quantity"] == null) {
                            if(fieldIndex > -1)
                                errorMsgs.Add(ErrorStrings.ImpPartsPurReturnOrderDetail_Validation5);
                        } else {
                            int quantity;
                            if(int.TryParse(row["Quantity"], out quantity)) {
                                partsPurReturnOrderDetail.Quantity = quantity;
                            } else {
                                errorMsgs.Add(ErrorStrings.ImpPartsPurReturnOrderDetail_Validation6);
                            }
                        }
                        //备注检查
                        if(!string.IsNullOrEmpty(partsPurReturnOrderDetail.Remark)) {
                            if(Encoding.Default.GetByteCount(partsPurReturnOrderDetail.Remark) > fieldLenght["Remark".ToUpper()])
                                errorMsgs.Add(ErrorStrings.ImpPartsPurReturnOrderDetail_Validation7);
                        }

                        if(errorMsgs.Count > 0) {
                            partsPurReturnOrderDetail.ErrorMsg = string.Join("; ", errorMsgs);
                            details.Add(partsPurReturnOrderDetail);
                        } else
                            orderDetails.Add(partsPurReturnOrderDetail);
                        return false;
                    });
                    //文件内数据重复
                    var groups = partsPurReturnOrderDetails.GroupBy(r => new {
                        r.SparePartCode
                    }).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        groupItem.ErrorMsg = ErrorStrings.Export_Validation_PartsPurchaseOrder_Validation2;
                        errorPartsPurReturnOrderDetails.Add(groupItem);
                    }

                    partsPurReturnOrderDetails = partsPurReturnOrderDetails.Where(r => r.ErrorMsg == null).ToList();
                    //获取配件信息表数据
                    var sparePartCodes = partsPurReturnOrderDetails.Select(v => v.SparePartCode.ToUpper()).Distinct().ToArray();
                    var spareParts = new List<SparePart>();
                    Func<string[], bool> dealSparePart = values => {
                        spareParts.Add(new SparePart {
                            Id = int.Parse(values[0]),
                            Code = values[1],
                            Name = values[2],
                            ReferenceCode = values[3],
                            MeasureUnit = values[4]
                        });
                        return false;
                    };
                    db.QueryDataWithInOperator(string.Format("select Id,Trim(Code) as Code,Trim(Name) as Name, ReferenceCode, MeasureUnit from SparePart where status={0}", (int)DcsMasterDataStatus.有效), "Code", true, sparePartCodes, dealSparePart);

                    //获取配件与供应商关系表数据
                    var sparePartIds = spareParts.Select(v => v.Id.ToString(CultureInfo.InvariantCulture)).Distinct().ToArray();
                    var partsSupplierRelations = new List<PartsSupplierRelation>();
                    Func<string[], bool> dealPartsSupplierRelation = values => {
                        partsSupplierRelations.Add(new PartsSupplierRelation {
                            PartId = int.Parse(values[0]),
                            SupplierId = int.Parse(values[1]),
                            BranchId = int.Parse(values[2])
                        });
                        return false;
                    };
                    db.QueryDataWithInOperator(string.Format("select PartId, SupplierId, BranchId from PartsSupplierRelation where status={0}", (int)DcsBaseDataStatus.有效), "PartId", false, sparePartIds, dealPartsSupplierRelation);

                    //获取配件库存表数据
                    var partsStocks = new List<PartsStock>();
                    Func<string[], bool> dealPartsStock = values => {
                        partsStocks.Add(new PartsStock {
                            PartId = int.Parse(values[0]),
                            WarehouseId = int.Parse(values[1]),
                            StorageCompanyId = int.Parse(values[2])
                        });
                        return false;
                    };
                    db.QueryDataWithInOperator("select Partid, WarehouseId, StorageCompanyId from PartsStock", "Partid", false, sparePartIds, dealPartsStock);

                    for(var i = partsPurReturnOrderDetails.Count - 1; i >= 0; i--) {
                        //配件有效性校验
                        var sparePart = spareParts.FirstOrDefault(v => String.Compare(v.Code, partsPurReturnOrderDetails[i].SparePartCode, StringComparison.OrdinalIgnoreCase) == 0);
                        if(sparePart == null) {
                            partsPurReturnOrderDetails[i].ErrorMsg = string.Format(ErrorStrings.ImpPartsPurReturnOrderDetail_Validation8, partsPurReturnOrderDetails[i].SparePartCode);
                            errorPartsPurReturnOrderDetails.Add(partsPurReturnOrderDetails[i]);
                            continue;
                        }

                        ////校验是否指定供应商的配件
                        //var partsSupplierRelation = partsSupplierRelations.SingleOrDefault(v => v.PartId == sparePart.Id && v.SupplierId == supplierId && v.BranchId == branchId);
                        //if(partsSupplierRelation == null) {
                        //    partsPurReturnOrderDetails[i].ErrorMsg = string.Format(ErrorStrings.ImpPartsPurReturnOrderDetail_Validation9, partsPurReturnOrderDetails[i].SupplierPartCode);
                        //    errorPartsPurReturnOrderDetails.Add(partsPurReturnOrderDetails[i]);
                        //    continue;
                        //}

                        //校验导入的配件是否为对应仓库中的配件
                        var partsStock = partsStocks.FirstOrDefault(v => v.PartId == sparePart.Id && v.WarehouseId == warehouseId && v.StorageCompanyId == branchId);
                        if(partsStock == null) {
                            partsPurReturnOrderDetails[i].ErrorMsg = string.Format(ErrorStrings.ImpPartsPurReturnOrderDetail_Validation10, warehouseId);
                            errorPartsPurReturnOrderDetails.Add(partsPurReturnOrderDetails[i]);
                            continue;
                        }

                        partsPurReturnOrderDetails[i].SparePartId = sparePart.Id;
                        partsPurReturnOrderDetails[i].SparePartName = sparePart.Name;
                        partsPurReturnOrderDetails[i].SupplierPartCode = sparePart.ReferenceCode;
                        partsPurReturnOrderDetails[i].MeasureUnit = sparePart.MeasureUnit;
                    }
                }
                rightPartsPurReturnOrderDetails = partsPurReturnOrderDetails.Where(r => r.ErrorMsg == null).ToList();
                if(rightPartsPurReturnOrderDetails.Count > 0) {
                    var rightSparePartIds = rightPartsPurReturnOrderDetails.Select(r => r.SparePartId.ToString(CultureInfo.InvariantCulture)).Distinct().ToArray();
                    var partsPurchasePrices = new List<PartsPurchasePriceExtend>();
                    Func<string[], bool> getPartsPurchasePrices = values => {
                        var dbObj = new PartsPurchasePriceExtend {
                            SparePartId = int.Parse(values[0]),
                            PartsPurchasePrice = Convert.ToDecimal(values[1])
                        };
                        partsPurchasePrices.Add(dbObj);
                        return false;
                    };
                    var sqlforPurchasePrice = "select PARTID,PURCHASEPRICE from PARTSPURCHASEPRICING where status=2 and VALIDFROM<=sysdate and VALIDTO>=sysdate  and branchId=" + branchId + " and partssalescategoryId=" + partsSalesCategoryId + " and partssupplierId=" + supplierId;
                    db.QueryDataWithInOperator(sqlforPurchasePrice, "PartId", false, rightSparePartIds, getPartsPurchasePrices);
                    foreach(var item in rightPartsPurReturnOrderDetails) {
                        var tempPartsPurchasePriceExtend = partsPurchasePrices.SingleOrDefault(r => r.SparePartId == item.SparePartId);
                        item.UnitPrice = tempPartsPurchasePriceExtend == null ? 0 : tempPartsPurchasePriceExtend.PartsPurchasePrice;
                    }
                }

                if(errorPartsPurReturnOrderDetails.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        List<ImpPartsPurReturnOrderDetail> details = errorPartsPurReturnOrderDetails;
                        excelExport.ExportByRow(index => {
                            if(index == details.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var partsPurReturnOrderDetail = details[index - 1];
                            var values = new object[]{
                                        partsPurReturnOrderDetail.SparePartCode, partsPurReturnOrderDetail.SparePartName,
                                        partsPurReturnOrderDetail.Quantity, partsPurReturnOrderDetail.Remark,
                                        partsPurReturnOrderDetail.ErrorMsg
                                    };
                            return values;
                        });
                    }
                    errorPartsPurReturnOrderDetails = null;
                }

                //导入正常结束，不存在需要写入数据库的合法数据
                //if(!rightPartsPurReturnOrderDetails.Any())
                //    return true;

                ////写数据
                //using(var conn = db.CreateDbConnection()) {
                //    conn.Open();
                //    //开启事务，新增更新配件在一个事务内
                //    var ts = conn.BeginTransaction();
                //    try {
                //        //新增配件
                //        if(rightPartsPurReturnOrderDetails.Any()) {
                //            //获取新增数据的sql语句，Id为主键
                //            var sqlInsert = db.GetInsertSql("PartsPurReturnOrderDetail", "Id", new[] {
                //                "SparePartCode", "SparePartName", "Quantity", "Remark"
                //            });
                //            var userInfo = Utils.GetCurrentUserInfo();
                //            foreach(var PartsPurReturnOrderDetail in rightPartsPurReturnOrderDetails) {
                //                var command = db.CreateDbCommand(sqlInsert, conn, ts);
                //                command.Parameters.Add(db.CreateDbParameter("SparePartId", PartsPurReturnOrderDetail.SparePartId));
                //                command.Parameters.Add(db.CreateDbParameter("SparePartCode", PartsPurReturnOrderDetail.SparePartCode));
                //                command.Parameters.Add(db.CreateDbParameter("SparePartName", PartsPurReturnOrderDetail.SparePartName));
                //                command.Parameters.Add(db.CreateDbParameter("SupplierPartCode", PartsPurReturnOrderDetail.SupplierPartCode));
                //                command.Parameters.Add(db.CreateDbParameter("Quantity", PartsPurReturnOrderDetail.Quantity));
                //                command.Parameters.Add(db.CreateDbParameter("MeasureUnit", PartsPurReturnOrderDetail.MeasureUnit));
                //                command.Parameters.Add(db.CreateDbParameter("Remark", PartsPurReturnOrderDetail.Remark));
                //                command.ExecuteNonQuery();
                //            }
                //        }
                //        //无异常提交
                //        ts.Commit();
                //    } catch(Exception) {
                //        //报错回滚
                //        ts.Rollback();
                //        throw;
                //    }finally {
                //      if(conn.State == System.Data.ConnectionState.Open) {
                //          conn.Close();
                //      }
                //    }
                //}
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorPartsPurReturnOrderDetails = null;
                return false;
            } finally {
                errorData = errorPartsPurReturnOrderDetails;
                rightData = rightPartsPurReturnOrderDetails;
            }
        }
    }
}
