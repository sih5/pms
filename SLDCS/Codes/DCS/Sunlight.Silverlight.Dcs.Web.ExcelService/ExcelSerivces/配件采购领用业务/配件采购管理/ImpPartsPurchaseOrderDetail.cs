﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        /// <summary>
        /// 采购订单批量导入数据校验
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="excelImportNum"></param>
        /// <param name="branchId">营销分公司Id</param>
        /// <param name="rightData"></param>
        /// <param name="errorData"></param>
        /// <param name="errorDataFileName"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public bool ImportPartsPurchaseOrderDetail(string fileName, out int excelImportNum, int branchId, out List<PartsPurchaseOrderDetailExtend> rightData, out List<PartsPurchaseOrderDetailExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<PartsPurchaseOrderDetailExtend>();
            var allList = new List<PartsPurchaseOrderDetailExtend>();
            var rightList = new List<PartsPurchaseOrderDetailExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("PartsPurchaseOrderDetail", out notNullableFields, out fieldLenght);

                List<string> notNullableFieldsPartsPurchaseOrder;
                Dictionary<string, int> fieldLenghtPartsPurchaseOrder;
                db.GetTableSchema("PartsPurchaseOrder", out notNullableFieldsPartsPurchaseOrder, out fieldLenghtPartsPurchaseOrder);


                List<string> notNullableFieldsPartsPurchaseOrderType;
                Dictionary<string, int> fieldLenghtPartsPurchaseOrderType;
                db.GetTableSchema("PartsPurchaseOrderType", out notNullableFieldsPartsPurchaseOrderType, out fieldLenghtPartsPurchaseOrderType);

                List<string> notNullableFieldsPartsSupplier;
                Dictionary<string, int> fieldLenghtPartsSupplier;
                db.GetTableSchema("PartsSupplier", out notNullableFieldsPartsSupplier, out fieldLenghtPartsSupplier);

                List<object> excelColumns;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    #region 指定文件中的列对应的名称 例如 excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Code, "PartCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Partssalescategory_Name, "PartsSalesCategoryName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsPurchaseOrder_OrderType, "PartsPurchaseOrderTypeName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Company_WarehouseName, "WarehouseName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsPurchaseOrder_RequestedDeliveryTime, "RequestedDeliveryTime");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsPurchaseOrder_ShippingMethod, "ShippingMethod");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsPurchaseOrder_PlanSource, "PlanSource");
                    //excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsInboundCheckBill_SAPPurchasePlanCode, "SAPPurchasePlanCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Code, "SparePartCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName, "SparePartName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_FactoryPurchacePrice_SupplierCode, "PartsSupplierCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_FactoryPurchacePrice_SupplierName, "PartsSupplierName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsPurchaseOrder_OrderAmount, "OrderAmount");
                    //excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsInboundPlan_POCode, "POCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsPurchaseOrder_OrderRemark, "OrderRemark");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsPurchaseOrder_DetailRemark, "DetailRemark");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsPurchaseOrder_OrderCollectMark, "OrderCollectMark");
                    #endregion
                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;
                    #region 获取对应枚举
                    var keyValuePairs = new[] { 
                        new KeyValuePair<string, string>("ShippingMethod", "PartsShipping_Method") 
                    };
                    tempExcelOperator.ImportHelper.LoadEnumFromDb(null, null, keyValuePairs);

                    #endregion
                    #region
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var tempImportObj = new PartsPurchaseOrderDetailExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        #region 根据导入的字段给对应的实体属性赋值 tempImportObj.PartsSalesCategoryNameStr = row["PartsSalesCategoryName"].Trim();

                        tempImportObj.SparePartCodeStr = newRow["SparePartCode"];
                        tempImportObj.SparePartNameStr = newRow["SparePartName"];
                        tempImportObj.PartsSupplierCodeStr = newRow["PartsSupplierCode"];
                        tempImportObj.PartsSupplierNameStr = newRow["PartsSupplierName"];
                        tempImportObj.OrderAmountStr = newRow["OrderAmount"];
                        tempImportObj.OrderRemarkStr = newRow["OrderRemark"];
                        tempImportObj.DetailRemarkStr = newRow["DetailRemark"];
                        tempImportObj.PartsSalesCategoryNameStr = newRow["PartsSalesCategoryName"];
                        tempImportObj.WarehouseNameStr = newRow["WarehouseName"];
                        tempImportObj.PartsPurchaseOrderTypeNameStr = newRow["PartsPurchaseOrderTypeName"];
                        tempImportObj.PlanSourceStr = newRow["PlanSource"];
                        //tempImportObj.SAPPurchasePlanCodeStr = newRow["SAPPurchasePlanCode"];
                        tempImportObj.ShippingMethodStr = newRow["ShippingMethod"];
                        tempImportObj.RequestedDeliveryTimeStr = newRow["RequestedDeliveryTime"];
                        tempImportObj.OrderCollectMark = (newRow["OrderCollectMark"] ?? "").Trim();
                        //tempImportObj.POCode = (newRow["POCode"] ?? "").Trim();

                        #endregion

                        var tempErrorMessage = new List<string>();
                        #region 导入的数据基本检查
                        //配件图号检查
                        var fieldIndex = notNullableFields.IndexOf("SparePartCode".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.SparePartCodeStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.OverstockPartsAppDetail__Validation1);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.SparePartCodeStr) > fieldLenght["SparePartCode".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.ImpPartsSupplierRelation_Validation8);
                        }

                        //配件名称检查
                        if(!string.IsNullOrEmpty(tempImportObj.SparePartNameStr)) {
                            if(Encoding.Default.GetByteCount(tempImportObj.SparePartNameStr) > fieldLenght["SparePartName".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Sparepart_NameIsLong);
                        }

                        //供应商编号检查
                        if(string.IsNullOrEmpty(tempImportObj.PartsSupplierCodeStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PersonnelSupplierRelation_SupplierCode);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.PartsSupplierCodeStr) > fieldLenghtPartsSupplier["Code".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PersonnelSupplierRelation_SupplierCodeIsLong);
                        }

                        //供应商名称检查
                        if(!string.IsNullOrEmpty(tempImportObj.PartsSupplierNameStr)) {
                            if(Encoding.Default.GetByteCount(tempImportObj.PartsSupplierNameStr) > fieldLenghtPartsSupplier["Name".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchaseOrderDetail_Validation16);
                        }

                        //订货量检查
                        fieldIndex = notNullableFields.IndexOf("OrderAmount".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.OrderAmountStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchaseOrderDetail_Validation7);
                        } else {
                            int checkValue;
                            if(int.TryParse(tempImportObj.OrderAmountStr, out checkValue)) {
                                if(checkValue <= 0) {
                                    tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsPurchaseOrder_Validation1);
                                }
                            } else {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsPurchaseOrder_OrderAmountNumber);
                            }
                        }

                        //主单备注检查
                        fieldIndex = notNullableFieldsPartsPurchaseOrder.IndexOf("Remark".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.OrderRemarkStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsPurchaseOrder_OrderRemarkIsNull);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.OrderRemarkStr) > fieldLenghtPartsPurchaseOrder["Remark".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsPurchaseOrder_OrderRemarkIsLong);
                        }

                        //清单备注检查
                        fieldIndex = notNullableFields.IndexOf("Remark".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.DetailRemarkStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsPurchaseOrder_DetailRemarkIsNull);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.DetailRemarkStr) > fieldLenght["Remark".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsPurchaseOrder_DetailRemarkIsLong);
                        }

                        //品牌检查
                        fieldIndex = notNullableFieldsPartsPurchaseOrder.IndexOf("PartsSalesCategoryName".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.PartsSalesCategoryNameStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_MarketingDepartment_PartsSalesCategoryIsNull);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.PartsSalesCategoryNameStr) > fieldLenghtPartsPurchaseOrder["PartsSalesCategoryName".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_MarketingDepartment_PartsSalesCategoryIsLong);
                        }
                        //仓库名称检查
                        fieldIndex = notNullableFieldsPartsPurchaseOrder.IndexOf("WarehouseName".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.WarehouseNameStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsPurchaseOrder_WarehouseNameIsNull);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.WarehouseNameStr) > fieldLenghtPartsPurchaseOrder["WarehouseName".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsPurchaseOrder_WarehouseNameLong);
                        }
                        //订单类型检查
                        fieldIndex = notNullableFieldsPartsPurchaseOrderType.IndexOf("Name".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.PartsPurchaseOrderTypeNameStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsPurchaseOrder_OrderTypeIsNull);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.PartsPurchaseOrderTypeNameStr) > fieldLenghtPartsPurchaseOrderType["Name".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsPurchaseOrder_OrderTypeIsLong);
                        }

                        //计划来源检查
                        fieldIndex = notNullableFieldsPartsPurchaseOrderType.IndexOf("PlanSource".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.PlanSourceStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsPurchaseOrder_PlanSourceIsNull);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.PlanSourceStr) > fieldLenghtPartsPurchaseOrder["PlanSource".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsPurchaseOrder_PlanSourceIsLong);
                        }

                        //发运方式检查
                        if(!string.IsNullOrEmpty(tempImportObj.ShippingMethodStr)) {
                            var tempEnumValue = tempExcelOperator.ImportHelper.GetEnumValue("ShippingMethod", tempImportObj.ShippingMethodStr);
                            if(!tempEnumValue.HasValue) {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsPurchaseOrder_ShippingMethodValueError);
                            }
                        }
                        //要求到货时间检查
                        fieldIndex = notNullableFieldsPartsPurchaseOrder.IndexOf("RequestedDeliveryTime".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.RequestedDeliveryTimeStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsPurchaseOrder_RequestedDeliveryTimeIsNull);
                        } else {
                            try {
                                DateTime checkValue;
                                if(!DateTime.TryParse(tempImportObj.RequestedDeliveryTimeStr, out checkValue)) {
                                    tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsPurchaseOrder_RequestedDeliveryTimeFormat);
                                }
                            } catch(Exception) {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsPurchaseOrder_RequestedDeliveryTimeFormat);
                            }
                        }
                        //规格型号检查
                        //fieldIndex = notNullableFields.IndexOf("Specification".ToUpper());
                        //if(string.IsNullOrEmpty(tempImportObj.SpecificationStr)) {
                        //    if(fieldIndex > -1)
                        //        tempErrorMessage.Add("规格型号不能为空");
                        //} else {
                        //    try {
                        //        if(Encoding.Default.GetByteCount(tempImportObj.SpecificationStr) > fieldLenght["Specification".ToUpper()])
                        //            tempErrorMessage.Add(ErrorStrings.Export_Validation_Sparepart_SpecificationIsLong);
                        //    } catch(Exception) {
                        //        tempErrorMessage.Add("规格型号格式不正确");
                        //    }
                        //}

                        #endregion
                        if(tempErrorMessage.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });
                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    #region 剩下的数据进行业务检查
                    ////文件内数据重复
                    var groups = tempRightList.GroupBy(r => new {
                        r.SparePartCodeStr,
                        r.OrderCollectMark
                    }).Where(r => r.Count() > 1);
                    if(groups.Any()) {
                        foreach(var groupItem in groups.SelectMany(r => r)) {
                            if(groupItem.ErrorMsg == null) {
                                groupItem.ErrorMsg = ErrorStrings.Export_Validation_PartsPurchaseOrder_Validation2;
                            } else {
                                groupItem.ErrorMsg = groupItem.ErrorMsg + "," + ErrorStrings.Export_Validation_PartsPurchaseOrder_Validation2;
                            }
                        }
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    var partsPurchaseOrderTypeNames = tempRightList.Select(v => v.PartsPurchaseOrderTypeNameStr).Distinct().ToArray();
                    var partsPurchaseOrderTypes = new List<PartsPurchaseOrderTypeExtend>();
                    Func<string[], bool> dealPartsPurchaseOrderType = values => {
                        partsPurchaseOrderTypes.Add(new PartsPurchaseOrderTypeExtend {
                            Id = Convert.ToInt32(values[0]),
                            Name = values[1],
                            BranchId = Convert.ToInt32(values[2]),
                            PartsSalesCategoryId = Convert.ToInt32(values[3])
                        });
                        return false;
                    };
                    db.QueryDataWithInOperator(string.Format("select Id,Name, BranchId,PartsSalesCategoryId from PartsPurchaseOrderType where status={0}", (int)DcsMasterDataStatus.有效), "Name", false, partsPurchaseOrderTypeNames, dealPartsPurchaseOrderType);

                    var warehouseNames = tempRightList.Select(v => v.WarehouseNameStr).Distinct().ToArray();
                    var warehouses = new List<WarehouseExtend>();
                    Func<string[], bool> dealWarehouse = values => {
                        warehouses.Add(new WarehouseExtend {
                            Id = Convert.ToInt32(values[0]),
                            Name = values[1],
                            BranchId = Convert.ToInt32(values[2]),
                            PartssalescategoryName = values[3]
                        });
                        return false;
                    };
                    db.QueryDataWithInOperator(string.Format("select Id, Name, BranchId,(select partssalescategory.name from partssalescategory inner join salesunit on partssalescategory.id=salesunit.partssalescategoryid inner join SalesUnitAffiWarehouse on salesunit.id=SalesUnitAffiWarehouse.Salesunitid where SalesUnitAffiWarehouse.Warehouseid=warehouse.id) as PartssalescategoryName from Warehouse where status={0}", (int)DcsMasterDataStatus.有效), "Name", false, warehouseNames, dealWarehouse);

                    var partsSalesCategoryNames = tempRightList.Select(v => v.PartsSalesCategoryNameStr).Distinct().ToArray();
                    var partsSalesCategorys = new List<PartsSalesCategoryExtend>();
                    Func<string[], bool> dealPartsSalesCategory = values => {
                        partsSalesCategorys.Add(new PartsSalesCategoryExtend {
                            Id = Convert.ToInt32(values[0]),
                            Name = values[1],
                            BranchId = Convert.ToInt32(values[2]),
                            BranchCode = values[3],
                            IsOverseas = values[4]
                        });
                        return false;
                    };
                    db.QueryDataWithInOperator(string.Format("select Id, Name, BranchId,BranchCode,nvl(IsOverseas,0) IsOverseas from PartsSalesCategory where status={0}", (int)DcsMasterDataStatus.有效), "Name", false, partsSalesCategoryNames, dealPartsSalesCategory);

                    var sparePartCodes = tempRightList.Select(v => v.SparePartCodeStr.ToUpper()).Distinct().ToArray();
                    var spareParts = new List<SparePartExtend>();
                    Func<string[], bool> dealSparePart = values => {
                        spareParts.Add(new SparePartExtend {
                            Id = Convert.ToInt32(values[0]),
                            Code = values[1],
                            Name = values[2],
                            ReferenceCode = values[3],
                            MeasureUnit = values[4],
                            MInPackingAmount = Convert.ToInt32(values[5]),
                            Specification = values[6]
                        });
                        return false;
                    };
                    db.QueryDataWithInOperator(string.Format("select Id, Code, Name, ReferenceCode, MeasureUnit,nvl(MInPackingAmount,0) MInPackingAmount,Specification from SparePart where status={0}", (int)DcsMasterDataStatus.有效), "Code", true, sparePartCodes, dealSparePart);

                    var partsSupplierCodes = tempRightList.Select(v => v.PartsSupplierCodeStr.ToUpper()).Distinct().ToArray();
                    var partsSuppliers = new List<PartsSupplierExtend>();
                    Func<string[], bool> dealPartsSupplier = values => {
                        partsSuppliers.Add(new PartsSupplierExtend {
                            Id = Convert.ToInt32(values[0]),
                            Code = values[1],
                            Name = values[2]
                        });
                        return false;
                    };
                    db.QueryDataWithInOperator(string.Format("select Id, Code, Name from PartsSupplier where status={0}", (int)DcsMasterDataStatus.有效), "Code", true, partsSupplierCodes, dealPartsSupplier);

                    var branchSupplierRelations = new List<PartsSupplierRelationExtend>();
                    Func<string[], bool> dealbranchSupplierRelation = values => {
                        branchSupplierRelations.Add(new PartsSupplierRelationExtend {
                            SupplierId = Convert.ToInt32(values[0]),
                            SupplierPartCode = values[1],
                            BranchId = Convert.ToInt32(values[2]),
                            PartsSalesCategoryId = Convert.ToInt32(values[3])
                        });
                        return false;
                    };
                    db.QueryDataWithInOperator(string.Format("select bs.SupplierId,s.Code,bs.BranchId,bs.PartsSalesCategoryId from BranchSupplierRelation bs inner join PartsSupplier s on s.Id=bs.SupplierId where bs.status={0}", (int)DcsMasterDataStatus.有效), "s.Code", true, partsSupplierCodes, dealbranchSupplierRelation);


                    var sparePartIds = spareParts.Select(v => v.Id.ToString()).Distinct().ToArray();
                    var partsSupplierRelations = new List<PartsSupplierRelationExtend>();
                    Func<string[], bool> dealPartsSupplierRelation = values => {
                        partsSupplierRelations.Add(new PartsSupplierRelationExtend {
                            PartId = Convert.ToInt32(values[0]),
                            SupplierId = Convert.ToInt32(values[1]),
                            BranchId = Convert.ToInt32(values[2]),
                            PartsSalesCategoryId = Convert.ToInt32(values[3])
                        });
                        return false;
                    };
                    db.QueryDataWithInOperator(string.Format("select PartId, SupplierId, BranchId, PartsSalesCategoryId from PartsSupplierRelation where status={0}", (int)DcsBaseDataStatus.有效), "PartId", false, sparePartIds, dealPartsSupplierRelation);

                    var partsBranchs = new List<PartsBranchExtend>();
                    Func<string[], bool> dealPartsBranch = values => {
                        partsBranchs.Add(new PartsBranchExtend {
                            PartId = Convert.ToInt32(values[0]),
                            BranchId = Convert.ToInt32(values[1]),
                            PartsSalesCategoryId = Convert.ToInt32(values[2]),
                            IsOrderable = values[3] != "0"
                        });
                        return false;
                    };
                    db.QueryDataWithInOperator(string.Format("select PartId, BranchId,PartsSalesCategoryId, IsOrderable from PartsBranch where status={0}", (int)DcsBaseDataStatus.有效), "PartId", false, sparePartIds, dealPartsBranch);

                    //获取配件采购价格表数据
                    var partsPurchasePricings = new List<PartsPurchasePricingExtend>();
                    Func<string[], bool> dealPartsPurchasePricings = values => {
                        partsPurchasePricings.Add(new PartsPurchasePricingExtend {
                            ValidFrom = DateTime.Parse(values[0]),
                            ValidTo = DateTime.Parse(values[1]),
                            Status = Convert.ToInt32(values[2]),
                            PartId = Convert.ToInt32(values[3]),
                            PartsSalesCategoryId = Convert.ToInt32(values[4]),
                            PurchasePrice = Convert.ToDecimal(values[5]),
                            BranchId = Convert.ToInt32(values[6]),
                            PartsSupplierId = Convert.ToInt32(values[7])
                        });
                        return false;
                    };
                    //获取分公司策略表数据
                    var branchstrategys = new List<BranchstrategyExtend>();
                    Func<string[], bool> dealBranchstrategy = values => {
                        branchstrategys.Add(new BranchstrategyExtend {
                            BranchId = int.Parse(values[0]),
                            PartsPurchasePricingStrategy = Convert.ToInt32(values[1]),
                            PurchaseIsNeedStd = values[2] != "0"
                        });
                        return false;
                    };
                    //获取配件计划价数据
                    var partsPlannedPrices = new List<PartsPlannedPriceExtend>();
                    Func<string[], bool> getDbPartsPlannedPrices = values => {
                        partsPlannedPrices.Add(new PartsPlannedPriceExtend {
                            SparePartId = Convert.ToInt32(values[0]),
                            PlannedPrice = Convert.ToDecimal(values[1]),
                            PartsSalesCategoryId = Convert.ToInt32(values[2])

                        });
                        return false;
                    };

                    //获取分公司策略表数据(没有Salescenterstrategy的扩展类 暂时用BranchstrategyExtend)
                    var dbSalescenterstrategies = new List<BranchstrategyExtend>();
                    Func<string[], bool> salescenterstrategies = values => {
                        dbSalescenterstrategies.Add(new BranchstrategyExtend {
                            PartsSalesCategoryId = Convert.ToInt32(values[0]),
                            PartsPurchasePricingStrategy = Convert.ToInt32(values[1])
                        });
                        return false;
                    };

                    var partsSupplierIds = partsSuppliers.Select(v => v.Id.ToString()).Distinct().ToArray();
                    db.QueryDataWithInOperator(string.Format("select ValidFrom, ValidTo, Status, PartId, PartsSalesCategoryId, PurchasePrice, BranchId, PartsSupplierId from PartsPurchasePricing where status={0}", (int)DcsPartsPurchasePricingStatus.生效), "PartsSupplierId", false, partsSupplierIds, dealPartsPurchasePricings);
                    db.QueryDataWithInOperator(string.Format("select BranchId, PartsPurchasePricingStrategy,PurchaseIsNeedStd from Branchstrategy where status={0}", (int)DcsBaseDataStatus.有效), "BranchId", false, new[] { branchId.ToString() }, dealBranchstrategy);
                    db.QueryDataWithInOperator(string.Format("select SparePartId, PlannedPrice,PartsSalesCategoryId from PartsPlannedPrice where OwnerCompanyId ={0}", branchId), "SparePartId", false, sparePartIds, getDbPartsPlannedPrices);
                    db.QueryDataWithInOperator(string.Format("select PartsSalesCategoryId, PartsPurchasePricingStrategy from Salescenterstrategy where status={0}", (int)DcsBaseDataStatus.有效), "status", false, new[] { ((int)DcsBaseDataStatus.有效).ToString() }, salescenterstrategies);


                    foreach(var tempRight in tempRightList) {
                        //1.品牌有效性校验(配件销售类型.营销分公司id=参数:营销分公司id and 配件销售类型.销售类型名称=导入数据.品牌),
                        //如果校验不通过,记录校验错误信息:品牌校验不通过,请正确填写分公司下属品牌.
                        var partsSalesCategory = partsSalesCategorys.FirstOrDefault(v => v.BranchId == branchId && v.Name.ToUpper() == tempRight.PartsSalesCategoryNameStr.ToUpper());
                        if(partsSalesCategory == null) {
                            if(string.IsNullOrEmpty(tempRight.ErrorMsg)) {
                                tempRight.ErrorMsg = ErrorStrings.Export_Validation_PartsPurchaseOrder_Validation3;
                            } else {
                                tempRight.ErrorMsg = tempRight.ErrorMsg + "," + ErrorStrings.Export_Validation_PartsPurchaseOrder_Validation3;
                            }
                            continue;
                        } else {
                            if(partsSalesCategory != null) {
                                tempRight.PartsSalesCategoryId = partsSalesCategory.Id;
                                tempRight.BranchCode = partsSalesCategory.BranchCode;
                            }
                            //if(partsSalesCategory.IsOverseas.Equals("1")) {
                            //    if(string.IsNullOrEmpty(tempRight.SAPPurchasePlanCodeStr)) {
                            //        if(string.IsNullOrEmpty(tempRight.ErrorMsg)) {
                            //            tempRight.ErrorMsg = "SAP采购计划单号应为必填";
                            //        } else {
                            //            tempRight.ErrorMsg = tempRight.ErrorMsg + "," + "SAP采购计划单号应为必填";
                            //        }
                            //    }
                            //}
                        }

                        //2.仓库有效性校验(仓库.仓储企业id=参数:营销分公司id and 仓库.仓库名称=导入数据.仓库名称 ),
                        //如果校验不通过,记录校验错误信息:仓库校验不通过,请正确填写仓储企业下属仓库.
                        var warehouse = warehouses.FirstOrDefault(v => v.BranchId == branchId && v.Name == tempRight.WarehouseNameStr && v.PartssalescategoryName == tempRight.PartsSalesCategoryNameStr);
                        if(warehouse == null) {
                            if(string.IsNullOrEmpty(tempRight.ErrorMsg)) {
                                tempRight.ErrorMsg = ErrorStrings.Export_Validation_PartsPurchaseOrder_Validation4;
                            } else {
                                tempRight.ErrorMsg = tempRight.ErrorMsg + "," + ErrorStrings.Export_Validation_PartsPurchaseOrder_Validation4;
                            }
                        } else {
                            if(warehouse != null) {
                                tempRight.WarehouseId = warehouse.Id;
                            }
                        }
                        //3.订单类型有效性校验(配件采购订单类型.营销分公司Id=参数:营销分公司Id and 配件采购订单类型.配件销售类型Id=配件销售类型.id and 配件销售类型.销售类型名称=导入数据.品牌 and 配件采购订单类型.订单类型名称=导入数据.订单类型),
                        //如果校验不通过,记录校验错误信息:订单类型校验不通过,请正确填写.
                        var partsPurchaseOrderType = partsPurchaseOrderTypes.FirstOrDefault(v => v.BranchId == branchId && v.PartsSalesCategoryId == tempRight.PartsSalesCategoryId && partsSalesCategory.Name == tempRight.PartsSalesCategoryNameStr && v.Name == tempRight.PartsPurchaseOrderTypeNameStr);
                        if(partsPurchaseOrderType == null) {
                            if(string.IsNullOrEmpty(tempRight.ErrorMsg)) {
                                tempRight.ErrorMsg = ErrorStrings.Export_Validation_PartsPurchaseOrder_Validation5;
                            } else {
                                tempRight.ErrorMsg = tempRight.ErrorMsg + "," + ErrorStrings.Export_Validation_PartsPurchaseOrder_Validation5;
                            }
                        } else {
                            if(partsPurchaseOrderType != null) {
                                tempRight.PartsPurchaseOrderTypeId = partsPurchaseOrderType.Id;
                            }
                        }
                        //5.配件有效性校验（配件信息.配件图号=导入数据.配件图号），
                        //如果校验不通过，记录校验错误信息：配件不存在。
                        var sparePart = spareParts.FirstOrDefault(v => v.Code == tempRight.SparePartCodeStr);
                        if(sparePart == null) {
                            if(string.IsNullOrEmpty(tempRight.ErrorMsg)) {
                                tempRight.ErrorMsg = ErrorStrings.Export_Validation_InternalAcquisitionDetail_Validation1;
                            } else {
                                tempRight.ErrorMsg = tempRight.ErrorMsg + "," + ErrorStrings.Export_Validation_InternalAcquisitionDetail_Validation1;
                            }
                        } else {
                            if(sparePart.MInPackingAmount == 0) {
                                if(string.IsNullOrEmpty(tempRight.ErrorMsg)) {
                                    tempRight.ErrorMsg = ErrorStrings.Export_Validation_PartsPurchaseOrder_Validation6;
                                } else {
                                    tempRight.ErrorMsg = tempRight.ErrorMsg + "," + ErrorStrings.Export_Validation_PartsPurchaseOrder_Validation6;
                                }
                            } else {
                                if((Convert.ToInt32(tempRight.OrderAmountStr) % sparePart.MInPackingAmount) != 0) {
                                    if(string.IsNullOrEmpty(tempRight.ErrorMsg)) {
                                        tempRight.ErrorMsg = ErrorStrings.Export_Validation_PartsPurchaseOrder_Validation7;
                                    } else {
                                        tempRight.ErrorMsg = tempRight.ErrorMsg + "," + ErrorStrings.Export_Validation_PartsPurchaseOrder_Validation7;
                                    }
                                }
                            }
                            if(sparePart != null) {
                                tempRight.SparePartId = sparePart.Id;
                                tempRight.SparePartName = sparePart.Name;
                                tempRight.SupplierPartCode = sparePart.ReferenceCode;
                                tempRight.MeasureUnit = sparePart.MeasureUnit;
                            }
                        }

                        //6.供应商有效性校验（配件供应商基本信息.供应商编号= 导入数据.供应商编号），
                        //如果校验不通过，记录校验错误信息：供应商不存在。
                        var partsSupplier = partsSuppliers.FirstOrDefault(v => v.Code.ToUpper() == tempRight.PartsSupplierCodeStr.ToUpper());
                        if(partsSupplier == null) {
                            if(string.IsNullOrEmpty(tempRight.ErrorMsg)) {
                                tempRight.ErrorMsg = ErrorStrings.Export_Validation_PartsPurchaseOrder_Validation8;
                            } else {
                                tempRight.ErrorMsg = tempRight.ErrorMsg + "," + ErrorStrings.Export_Validation_PartsPurchaseOrder_Validation8;
                            }
                        } else {
                            if(partsSupplier != null) {
                                tempRight.PartsSupplierId = partsSupplier.Id;
                                tempRight.PartsSupplierName = partsSupplier.Name;
                            }
                        }
                        //校验供应商与分公司的关系
                        if(partsSupplier != null) {
                            var branchSupplierRelation = branchSupplierRelations.FirstOrDefault(p => p.SupplierPartCode == tempRight.PartsSupplierCodeStr && p.BranchId == branchId && p.PartsSalesCategoryId == partsSalesCategory.Id);
                            if(branchSupplierRelation == null) {
                                if(string.IsNullOrEmpty(tempRight.ErrorMsg)) {
                                    tempRight.ErrorMsg = ErrorStrings.Export_Validation_PartsPurchaseOrder_Validation9;
                                } else {
                                    tempRight.ErrorMsg = tempRight.ErrorMsg + "," + ErrorStrings.Export_Validation_PartsPurchaseOrder_Validation9;
                                }
                            }
                        }

                        //7.校验配件与供应商的关系（配件与供应商关系.配件Id=配件信息.Id and 配件与供应商关系.供应商Id=配件供应商基本信息.Id and 配件与供应商关系.营销分公司id=参数：营销分公司Id and 配件信息.配件图号=导入数据.配件图号 and 供应商基本信息.供应商编号=导入数据.供应商编号）,
                        //如果校验不通过，记录校验错误信息：配件与对应供应商关系不存在。
                        if(partsSupplier != null && sparePart != null) {
                            var partsSupplierRelation = partsSupplierRelations.FirstOrDefault(v => v.PartId == sparePart.Id && v.SupplierId == partsSupplier.Id && v.PartsSalesCategoryId == partsSalesCategory.Id && v.BranchId == branchId && sparePart.Code == tempRight.SparePartCodeStr && partsSupplier.Code == tempRight.PartsSupplierCodeStr);
                            if(partsSupplierRelation == null) {
                                if(string.IsNullOrEmpty(tempRight.ErrorMsg)) {
                                    tempRight.ErrorMsg = ErrorStrings.Export_Validation_PartsPurchaseOrder_Validation10;
                                } else {
                                    tempRight.ErrorMsg = tempRight.ErrorMsg + "," + ErrorStrings.Export_Validation_PartsPurchaseOrder_Validation10;
                                }
                            }
                        }
                        //8.校验配件是否可采购（配件营销信息.配件id=配件信息.Id and 配件营销信息.营销分公司id = 参数：营销分公司id and 配件营销信息.配件销售类型id =配件销售类型.id and 配件销售类型.销售类型名称=导入数据.品牌 and 配件营销信息.是否可采购 = true and 配件信息.配件图号=导入数据.配件图号）,
                        //如果校验不通过，记录错误信息：该配件不可采购。
                        if(sparePart != null && partsSalesCategory != null) {
                            var partsBranch = partsBranchs.FirstOrDefault(v => v.PartId == sparePart.Id && v.BranchId == branchId && v.PartsSalesCategoryId == partsSalesCategory.Id && partsSalesCategory.Name == tempRight.PartsSalesCategoryNameStr && v.IsOrderable && sparePart.Code == tempRight.SparePartCodeStr);
                            if(partsBranch == null) {
                                if(string.IsNullOrEmpty(tempRight.ErrorMsg)) {
                                    tempRight.ErrorMsg = ErrorStrings.Export_Validation_PartsPurchaseOrder_Validation11;
                                } else {
                                    tempRight.ErrorMsg = tempRight.ErrorMsg + "," + ErrorStrings.Export_Validation_PartsPurchaseOrder_Validation11;
                                }
                            }
                        }
                        //9.校验配件是否存在有效的采购价（调用方法：查询配件采购价格(供应商Id=配件供应商基本信息.Id and 配件供应商基本信息.供应商编号=导入数据.供应商编号  and 营销分公司Id=参数.营销分公司Id and 配件销售类型Id=配件销售类型.Id and 配件销售类型.销售类型名称=导入数据.品牌，时间=当前时间)，
                        //查询分公司策略，营销分公司Id=参数.营销分公司Id，如果配件采购定价策略=先定价，校验不同过，记录错误信息：该配件不存在对应的采购合同价。
                        PartsPurchasePricingExtend partsPurchasePricing = null;
                        if(sparePartIds != null && sparePartIds.Any() && partsSupplier != null && partsSalesCategory != null && partsSupplier != null) {
                            partsPurchasePricing = partsPurchasePricings.FirstOrDefault(r => r.ValidFrom <= DateTime.Now && r.ValidTo >= DateTime.Now && r.Status != (int)DcsPartsPurchasePricingStatus.作废 && sparePartIds.Contains(r.PartId.ToString()) && r.PartId == tempRight.SparePartId && partsSupplier.Code == tempRight.PartsSupplierCodeStr && r.PartsSalesCategoryId == partsSalesCategory.Id && partsSalesCategory.Name == tempRight.PartsSalesCategoryNameStr && r.BranchId == branchId && r.PartsSupplierId == partsSupplier.Id);
                        }
                        if(string.IsNullOrEmpty(tempRight.ErrorMsg)) {
                            var branchstrategy = dbSalescenterstrategies.FirstOrDefault(v => v.PartsPurchasePricingStrategy == (int)DcsBranchstrategyPartsPurchasePricingStrategy.先定价 && v.PartsSalesCategoryId == tempRight.PartsSalesCategoryId);
                            if(branchstrategy != null && partsPurchasePricing == null) {
                                if(string.IsNullOrEmpty(tempRight.ErrorMsg)) {
                                    tempRight.ErrorMsg = ErrorStrings.Export_Validation_PartsPurchaseOrder_Validation12;
                                } else {
                                    tempRight.ErrorMsg = tempRight.ErrorMsg + "," + ErrorStrings.Export_Validation_PartsPurchaseOrder_Validation12;
                                }
                            }
                        }
                        //以下判断不正确 20150601
                        //if(branchstrategy != null) {
                        //    tempRight.ErrorMsg = "该配件为先定价策略,不允许导入";
                        //    continue;
                        //}

                        // 10.查询分公司策略（分公司Id=上述服务参数 营销分公司Id（不为空）），如果分公司策略.采购订单是否需要计划价=是 
                        //查询配件计划价（配件Id in 服务获取的配件Id数组 ，配件销售类型Id=上述服务获取的配件销售类型Id,隶属企业Id=营销分公司Id）如果配件Id数组中配件获取不到计划价，则 记录错误信息：该配件不存在对应的计划价。
                        PartsPlannedPriceExtend partsPlannedPrice = null;
                        if(partsSalesCategory != null) {
                            try {
                                partsPlannedPrice = partsPlannedPrices.SingleOrDefault(r => r.SparePartId == tempRight.SparePartId && r.PartsSalesCategoryId == partsSalesCategory.Id);
                                var branchStrategy = branchstrategys.SingleOrDefault(v => v.PurchaseIsNeedStd.HasValue && (bool)v.PurchaseIsNeedStd);
                                if(branchStrategy != null && (partsPlannedPrice == null || partsPlannedPrice.PlannedPrice == default(int))) {
                                    if(string.IsNullOrEmpty(tempRight.ErrorMsg)) {
                                        tempRight.ErrorMsg = ErrorStrings.Export_Validation_PartsPurchaseOrder_Validation13;
                                    } else {
                                        tempRight.ErrorMsg = tempRight.ErrorMsg + "," + ErrorStrings.Export_Validation_PartsPurchaseOrder_Validation13;
                                    }
                                }
                                if(partsPurchasePricing != null) {
                                    tempRight.UnitPrice = partsPurchasePricing != null ? partsPurchasePricing.PurchasePrice : 0;
                                }
                            } catch(Exception) {
                                if(string.IsNullOrEmpty(tempRight.ErrorMsg)) {
                                    tempRight.ErrorMsg = ErrorStrings.Export_Validation_PartsPurchaseOrder_Validation13;
                                } else {
                                    tempRight.ErrorMsg = tempRight.ErrorMsg + "," + ErrorStrings.Export_Validation_PartsPurchaseOrder_Validation14;
                                }
                            }
                        }
                        if(tempRight.BranchCode == "2450" && tempRight.PartsSupplierCodeStr == "FT010063") {
                            var groupsparePartByReferenceCodes = spareParts.GroupBy(r =>
                      r.ReferenceCode
                  ).Where(r => r.Count() > 1);
                            if(groupsparePartByReferenceCodes.Any()) {
                                var checkReferenceCode = groupsparePartByReferenceCodes.Select(r => r.Key).ToArray();
                                foreach(var referenceCode in checkReferenceCode) {
                                    StringBuilder a = new StringBuilder();
                                    foreach(var groupItem in groupsparePartByReferenceCodes.SelectMany(r => r).Where(r => r.ReferenceCode == referenceCode)) {
                                        a.Append(groupItem.Code);
                                        a.Append(',');
                                    }
                                    if(a.Length > 0)
                                        a.Remove(a.Length - 1, 1);
                                    if(string.IsNullOrEmpty(tempRight.ErrorMsg)) {
                                        tempRight.ErrorMsg = string.Format(ErrorStrings.Export_Validation_PartsPurchaseOrder_Validation15, a, referenceCode);
                                    } else {
                                        tempRight.ErrorMsg = tempRight.ErrorMsg + "," + string.Format(ErrorStrings.Export_Validation_PartsPurchaseOrder_Validation15, a, referenceCode);
                                    }
                                }
                            }
                        }
                        if(tempRight.ShippingMethodStr != null) {
                            tempRight.ShippingMethod = tempExcelOperator.ImportHelper.GetEnumValue("ShippingMethod", tempRight.ShippingMethodStr) ?? 0;
                        }
                    }

                    #endregion
                    //获取所有不合格数据
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    //获取所有合格数据
                    rightList = allList.Except(errorList).ToList();
                    #region 将合格数据的值填上
                    foreach(var tempRight in rightList) {
                        tempRight.SparePartCode = tempRight.SparePartCodeStr;
                        tempRight.PartsSupplierCode = tempRight.PartsSupplierCodeStr;
                        tempRight.OrderAmount = Convert.ToInt32(tempRight.OrderAmountStr);
                        tempRight.OrderRemark = tempRight.OrderRemarkStr;
                        tempRight.DetailRemark = tempRight.DetailRemarkStr;
                        tempRight.PartsSalesCategoryName = tempRight.PartsSalesCategoryNameStr;
                        tempRight.WarehouseName = tempRight.WarehouseNameStr;
                        tempRight.PlanSource = tempRight.PlanSourceStr;
                        //tempRight.SAPPurchasePlanCode = tempRight.SAPPurchasePlanCodeStr;
                        tempRight.ShippingMethod = tempExcelOperator.ImportHelper.GetEnumValue("ShippingMethod", tempRight.ShippingMethodStr) ?? 0;
                        tempRight.RequestedDeliveryTime = Convert.ToDateTime(tempRight.RequestedDeliveryTimeStr);
                        tempRight.Specification = tempRight.SpecificationStr;

                    }
                    #endregion
                }
                    #endregion
                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempObj = list[index - 1];
                            var values = new object[] {
                        #region 设置错误信息导出的列的值
                        tempObj.PartsSalesCategoryNameStr,
                        tempObj.PartsPurchaseOrderTypeNameStr,
                        tempObj.WarehouseNameStr,
                        tempObj.RequestedDeliveryTimeStr,
                        tempObj.ShippingMethodStr,
                        tempObj.PlanSourceStr,
                        //tempObj.SAPPurchasePlanCodeStr,
                        tempObj.SparePartCodeStr,
                        tempObj.SparePartNameStr,
                        tempObj.PartsSupplierCodeStr,
                        tempObj.PartsSupplierNameStr,
                        tempObj.OrderAmountStr,
                        //tempObj.POCode,
                        tempObj.OrderRemarkStr,
                        tempObj.DetailRemarkStr,
                        tempObj.ErrorMsg
                        #endregion                     
                    };
                            return values;
                        });
                    }
                    errorList = null;
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }
    }
}
