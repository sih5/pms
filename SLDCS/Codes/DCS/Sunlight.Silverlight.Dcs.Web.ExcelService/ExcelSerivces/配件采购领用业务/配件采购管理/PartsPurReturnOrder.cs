﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using System.Globalization;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        /// <summary>
        /// 合并导出采购退货单主清单
        /// </summary>
        /// <param name="id">单据Id（非必填）</param>
        /// <param name="partsSupplierCode">供应商编号（非必填）</param>
        /// <param name="partsSupplierName">供应商名称（非必填）</param>
        /// <param name="warehouseId">仓库Id（非必填）</param>
        /// <param name="partsSalesCategoryId">销售类型Id（非必填）</param>
        /// <param name="partsPurchaseOrderCode">退货单编号（非必填）</param>
        /// <param name="returnReason">退货原因（非必填）</param>
        /// <param name="status">状态（非必填）</param>
        /// <param name="createTimeBegin">创建开始时间（非必填）</param>
        /// <param name="createTimeEnd">创建结束时间（非必填）</param>
        /// <param name="filename"></param>
        /// <returns></returns>

        public bool ExportPartsPurReturnOrderWithDetail(int? id, string partsSupplierCode, string partsSupplierName, int? warehouseId, int? partsSalesCategoryId, string partsPurchaseOrderCode,string code, int? returnReason, int? status, int? outStatus, DateTime? createTimeBegin, DateTime? createTimeEnd, out string filename) {
            filename = GetExportFilePath("配件采购退货单主清单_" + ".xlsx");
            if(!HttpContext.Current.User.Identity.IsAuthenticated) {
                throw new Exception(ErrorStrings.File_Validation_User);
            }
            try {
                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {
                    var sql = new StringBuilder();
                    sql.Append(@"select a.Code,
                                        a.BranchName,
                                        a.PartsPurchaseOrderCode,
                                        a.PartsSupplierCode,
                                        a.PartsSupplierName,
                                        a.WarehouseName,
                                        a.WarehouseAddress,
                                        a.InvoiceRequirement,
                                        a.TotalAmount,
                                        (select value from keyvalueitem where NAME = 'PartsPurReturnOrder_ReturnReason'and key=a.ReturnReason) As ReturnReason,
                                        a.CreatorName,
                                        a.CreateTime,
                                        a.Remark,
                                        (select value from keyvalueitem where NAME = 'PartsPurReturnOrder_Status'and key=a.Status) As Status,
                                        (select value from keyvalueitem where NAME = 'ReturnOutStatus'and key=a.OutStatus) As OutStatus,
                                        b.SparePartCode,
                                        b.SparePartName,
                                        b.SupplierPartCode,
                                        b.MeasureUnit,
                                        b.Quantity,
                                        b.UnitPrice
                                        from PartsPurReturnOrder a
                                        left join PartsPurReturnOrderDetail b
                                            on a.id = b.PartsPurReturnOrderid where 1=1 ");
                    var dbParameters = new List<DbParameter>();
                    if(id.HasValue) {
                        sql.Append(" and a.id = {0}id");
                        dbParameters.Add(db.CreateDbParameter("id", id.Value));
                    } else {
                        var param = new {
                            //partsSupplierCode,
                            warehouseId,
                            partsSalesCategoryId,
                            returnReason,
                            status,
                            outStatus,
                            createTimeBegin,
                            createTimeEnd
                        };

                        foreach(var parameterInfo in param.GetType().GetProperties()) {
                            //无法使用switch语句(typeof非静态字符串) 所以if加上了continue来实现switch功能
                            if(parameterInfo.PropertyType.FullName == typeof(string).FullName) {
                                if(!string.IsNullOrEmpty((string)parameterInfo.GetValue(param, null))) {
                                    sql.Append(" and " + parameterInfo.Name + " = {0}" + parameterInfo.Name);
                                    dbParameters.Add(db.CreateDbParameter(parameterInfo.Name, parameterInfo.GetValue(param, null)));
                                }
                                continue;
                            }
                            if(parameterInfo.PropertyType.FullName == typeof(int?).FullName) {
                                if(((int?)parameterInfo.GetValue(param, null)).HasValue) {
                                    sql.Append(" and " + parameterInfo.Name + "={0}" + parameterInfo.Name);
                                    dbParameters.Add(db.CreateDbParameter(parameterInfo.Name, ((int?)parameterInfo.GetValue(param, null)).Value));
                                }
                                continue;
                            }
                            if(parameterInfo.PropertyType.FullName == typeof(DateTime?).FullName) {
                                string op = parameterInfo.Name.Contains("Begin") ? ">=" : (parameterInfo.Name.Contains("End") ? "<=" : "=");
                                if(((DateTime?)parameterInfo.GetValue(param, null)).HasValue) {
                                    sql.Append(" and createTime" + op + "to_date({0}" + parameterInfo.Name + ",'yyyy-mm-dd hh24:mi:ss')");
                                    var tempValue = ((DateTime?)parameterInfo.GetValue(param, null)).Value;
                                    var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                                    dbParameters.Add(db.CreateDbParameter(parameterInfo.Name, tempTime.ToString("G")));
                                }
                            }
                        }

                        if(!string.IsNullOrEmpty(partsSupplierCode)) {
                            sql.Append(" and partsSupplierCode like {0}partsSupplierCode ");
                            dbParameters.Add(db.CreateDbParameter("partsSupplierCode", "%" + partsSupplierCode + "%"));
                        }


                        if(!string.IsNullOrEmpty(partsSupplierName)) {
                            sql.Append(" and partsSupplierName like {0}partsSupplierName ");
                            dbParameters.Add(db.CreateDbParameter("partsSupplierName", "%" + partsSupplierName + "%"));
                        }

                         if(!string.IsNullOrEmpty(partsPurchaseOrderCode)) {
                            sql.Append(" and partsPurchaseOrderCode like {0}partsPurchaseOrderCode ");
                            dbParameters.Add(db.CreateDbParameter("partsPurchaseOrderCode", "%" + partsPurchaseOrderCode + "%"));
                        }

                         if(!string.IsNullOrEmpty(code)) {
                            sql.Append(" and code like {0}code ");
                            dbParameters.Add(db.CreateDbParameter("code", "%" + code + "%"));
                        }
                    }
                    var sqlStr = sql.ToString();
                    if(sqlStr.IndexOf(" where 1=1  and ") > 0)
                        sqlStr = sqlStr.Replace(" where 1=1  and ", " where ");
                    else
                        sqlStr = sqlStr.Replace(" where 1=1 ", "");
                    DbCommand comm = db.CreateDbCommand(string.Format(sqlStr, db.ParamMark), conn, null);
                    comm.Parameters.AddRange(dbParameters.ToArray());
                    conn.Open();
                    var reader = comm.ExecuteReader();
                    using(var excelExport = new ExcelExport(filename)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0)
                                return new object[] {
                                    ErrorStrings.Export_Title_PartsPurReturnOrder_Code, ErrorStrings.Export_Title_PartsBranch_BranchName, ErrorStrings.Export_Title_PartsPurchaseOrder_Code, ErrorStrings.Export_Title_FactoryPurchacePrice_SupplierCode, ErrorStrings.Export_Title_FactoryPurchacePrice_SupplierName,
                                    ErrorStrings.Export_Title_Company_WarehouseName, ErrorStrings.Export_Title_PartsPurReturnOrder_WarehouseAddress, ErrorStrings.Export_Title_PartsPurReturnOrder_InvoiceRequirement, ErrorStrings.Export_Title_InternalAllocationBill_TotalAmount, ErrorStrings.Export_Title_PartsPurReturnOrder_Reason, ErrorStrings.Export_Title_AccountPeriod_CreatorName, ErrorStrings.Export_Title_AccountPeriod_CreateTime, ErrorStrings.Export_Title_PartsBranch_Remark,
                                    ErrorStrings.Export_Title_AccountPeriod_Status,ErrorStrings.Export_Title_PartsPurReturnOrder_OutStatus, ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode, ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName, ErrorStrings.Export_Title_PartsPurchaseOrder_SupplierPartCode, ErrorStrings.Export_Title_PackingPropertyApp_MeasureUnit, ErrorStrings.Export_Title_WarehouseArea_Quantity, ErrorStrings.Export_Title_PartsPurReturnOrder_Price
                                };
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }

        public bool ExportPartsPurReturnOrder(int[] ids, string partsSupplierCode, string partsSupplierName, int? warehouseId, int? partsSalesCategoryId, string partsPurchaseOrderCode, int? returnReason, int? status, int? outStatus, DateTime? createTimeBegin, DateTime? createTimeEnd, out string filename) {
            filename = GetExportFilePath("配件采购退货单_" + ".xlsx");
            if(!HttpContext.Current.User.Identity.IsAuthenticated) {
                throw new Exception(ErrorStrings.File_Validation_User);
            }
            try {
                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {
                    var sql = new StringBuilder();
                    sql.Append(@"select a.Code,
                                        a.BranchName,
                                        a.PartsPurchaseOrderCode,
                                        a.PartsSupplierCode,
                                        a.PartsSupplierName,
                                        a.WarehouseName,
                                        a.WarehouseAddress,
                                        a.InvoiceRequirement,
                                        a.TotalAmount,
                                        (select value from keyvalueitem where NAME = 'PartsPurReturnOrder_ReturnReason'and key=a.ReturnReason) As ReturnReason,
                                        (select value from keyvalueitem where NAME = 'PartsPurReturnOrder_Status'and key=a.Status) As Status,
                                        (select value from keyvalueitem where NAME = 'ReturnOutStatus'and key=a.OutStatus) As OutStatus,
                                        a.SAPPurReturnOrderCode,
                                        a.Remark,
                                        a.CreatorName,
                                        a.CreateTime,InitialApproverName,InitialApproveTime,InitialApproverComment,ApproverName,ApproveTime,ApprovertComment,ModifierName,ModifyTime,AbandonerName,AbandonTime
                                        from PartsPurReturnOrder a
                                         where 1=1 ");
                    var dbParameters = new List<DbParameter>();
                    if(ids != null && ids.Length > 0) {
                        sql.Append(@" and a.id in (");
                        for(int i = 0;i < ids.Length;i++) {
                            var tempIdWithNum = "tempIdWithNum" + i;
                            if(i == ids.Length - 1) {
                                var tempParam = "{0}" + tempIdWithNum;
                                sql.Append(tempParam);
                            } else {
                                var tempParam = "{0}" + tempIdWithNum + ",";
                                sql.Append(tempParam);
                            }
                            dbParameters.Add(db.CreateDbParameter(tempIdWithNum, ids[i]));
                        }
                        sql.Append(")");
                    } else {
                        var param = new {
                            //partsSupplierCode,
                            warehouseId,
                            partsSalesCategoryId,
                            partsPurchaseOrderCode,
                            returnReason,
                            status,
                            outStatus,
                            createTimeBegin,
                            createTimeEnd
                        };

                        foreach(var parameterInfo in param.GetType().GetProperties()) {
                            //无法使用switch语句(typeof非静态字符串) 所以if加上了continue来实现switch功能
                            if(parameterInfo.PropertyType.FullName == typeof(string).FullName) {
                                if(!string.IsNullOrEmpty((string)parameterInfo.GetValue(param, null))) {
                                    sql.Append(" and " + parameterInfo.Name + " = {0}" + parameterInfo.Name);
                                    dbParameters.Add(db.CreateDbParameter(parameterInfo.Name, parameterInfo.GetValue(param, null)));
                                }
                                continue;
                            }
                            if(parameterInfo.PropertyType.FullName == typeof(int?).FullName) {
                                if(((int?)parameterInfo.GetValue(param, null)).HasValue) {
                                    sql.Append(" and " + parameterInfo.Name + "={0}" + parameterInfo.Name);
                                    dbParameters.Add(db.CreateDbParameter(parameterInfo.Name, ((int?)parameterInfo.GetValue(param, null)).Value));
                                }
                                continue;
                            }
                            if(parameterInfo.PropertyType.FullName == typeof(DateTime?).FullName) {
                                string op = parameterInfo.Name.Contains("Begin") ? ">=" : (parameterInfo.Name.Contains("End") ? "<=" : "=");
                                if(((DateTime?)parameterInfo.GetValue(param, null)).HasValue) {
                                    sql.Append(" and createTime" + op + "to_date({0}" + parameterInfo.Name + ",'yyyy-mm-dd hh24:mi:ss')");
                                    var tempValue = ((DateTime?)parameterInfo.GetValue(param, null)).Value;
                                    var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day);
                                    dbParameters.Add(db.CreateDbParameter(parameterInfo.Name, tempTime.ToString("G")));
                                }
                            }
                        }

                        if(!string.IsNullOrEmpty(partsSupplierCode)) {
                            sql.Append(" and partsSupplierCode like {0}partsSupplierCode ");
                            dbParameters.Add(db.CreateDbParameter("partsSupplierCode", "%" + partsSupplierCode + "%"));
                        }


                        if(!string.IsNullOrEmpty(partsSupplierName)) {
                            sql.Append(" and partsSupplierName like {0}partsSupplierName ");
                            dbParameters.Add(db.CreateDbParameter("partsSupplierName", "%" + partsSupplierName + "%"));
                        }
                    }
                    var sqlStr = sql.ToString();
                    if(sqlStr.IndexOf(" where 1=1  and ") > 0)
                        sqlStr = sqlStr.Replace(" where 1=1  and ", " where ");
                    else
                        sqlStr = sqlStr.Replace(" where 1=1 ", "");
                    DbCommand comm = db.CreateDbCommand(string.Format(sqlStr, db.ParamMark), conn, null);
                    comm.Parameters.AddRange(dbParameters.ToArray());
                    conn.Open();
                    var reader = comm.ExecuteReader();
                    using(var excelExport = new ExcelExport(filename)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0)
                                return new object[] {
                                    ErrorStrings.Export_Title_PartsPurReturnOrder_Code, ErrorStrings.Export_Title_PartsBranch_BranchName, ErrorStrings.Export_Title_PartsPurchaseOrder_Code, ErrorStrings.Export_Title_FactoryPurchacePrice_SupplierCode, ErrorStrings.Export_Title_FactoryPurchacePrice_SupplierName,
                                    ErrorStrings.Export_Title_Company_WarehouseName, ErrorStrings.Export_Title_PartsPurReturnOrder_WarehouseAddress, ErrorStrings.Export_Title_PartsPurReturnOrder_InvoiceRequirement, ErrorStrings.Export_Title_InternalAllocationBill_TotalAmount, ErrorStrings.Export_Title_PartsPurReturnOrder_Reason,
                                    ErrorStrings.Export_Title_AccountPeriod_Status,ErrorStrings.Export_Title_PartsPurReturnOrder_OutStatus, ErrorStrings.Export_Title_PartsPurReturnOrder_SAPPurReturnOrderCode, ErrorStrings.Export_Title_PartsBranch_Remark,ErrorStrings.Export_Title_AccountPeriod_CreatorName, 
                                    ErrorStrings.Export_Title_AccountPeriod_CreateTime,ErrorStrings.Export_Title_PartsPurReturnOrder_InitialApproverName,ErrorStrings.Export_Title_PartsPurReturnOrder_InitialApproveTime,ErrorStrings.Export_Title_PartsPurReturnOrder_InitialApproveAment,ErrorStrings.Export_Title_PackingPropertyApp_ApproverName,ErrorStrings.Export_Title_PackingPropertyApp_ApproveTime,ErrorStrings.Export_Title_PartsPurReturnOrder_ApprovertComment,ErrorStrings.Export_Title_AccountPeriod_ModifierName,ErrorStrings.Export_Title_AccountPeriod_ModifyTime,ErrorStrings.Export_Title_PartsBranch_AbandonerName,ErrorStrings.Export_Title_PartsBranch_AbandonerTime
                                };
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }
        //配件采购退货管理-供应商 合并导出
        public bool ExportPartsPurReturnOrderForSupplierWithDetail(bool isSupplierLogin,int[] ids, string partsSupplierCode, int? BranchId, int? partsSalesCategoryId, int? warehouseId, string Code, int? returnReason, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, out string filename)
        {
            filename = GetExportFilePath("配件采购退货单（供应商）主清单_" + ".xlsx");
            if (!HttpContext.Current.User.Identity.IsAuthenticated)
            {
                throw new Exception(ErrorStrings.File_Validation_User);
            }
            try
            {
                var db = DbHelper.GetDbHelp(ConnectionString);

                using (var conn = db.CreateDbConnection())
                {
                    var sql = new StringBuilder();
                    if (isSupplierLogin)
                    {
                        sql.Append(@"select a.Code,/*退货单编号*/
                                        a.BranchName,/*营销分公司*/
                                        a.PartsSalesCategoryName,/*品牌*/
                                        a.PartsPurchaseOrderCode,/*采购订单编号*/
                                        a.PartsSupplierCode,/*供应商编号*/
                                        a.PartsSupplierName,/*供应商名称*/
                                        a.WarehouseName,/*仓库名称*/
                                        a.WarehouseAddress,/*仓库地址*/
                                        a.InvoiceRequirement,/*开票要求*/
                                        a.TotalAmount,/*总金额*/
                                        (select value from keyvalueitem where NAME = 'PartsPurReturnOrder_ReturnReason'and key=a.ReturnReason) As ReturnReason,/*退货原因*/
                                        (select value from keyvalueitem where NAME = 'PartsPurReturnOrder_Status'and key=a.Status) As Status,/*状态*/
                                        a.Remark,/*备注*/
                                        a.CreatorName,/*创建人*/
                                        a.CreateTime,/*创建时间*/
                                        a.ApproverName,/*审批人*/
                                        a.ApproveTime,/*审批时间*/
                                        a.ModifierName,/*修改人*/
                                        a.ModifyTime,/*修改时间*/
                                        a.AbandonerName,/*作废人*/
                                        a.AbandonTime,/*作废时间*/
                                        b.SupplierPartCode,/*供应商图号*/
                                        b.SparePartCode,/*配件图号*/
                                        b.SparePartName,/*配件名称*/
                                        b.MeasureUnit,/*计量单位*/
                                        b.Quantity,/*数量*/
                                        b.UnitPrice,/*退货价格*/
                                        b.remark/*备注*/
                                        from PartsPurReturnOrder a
                                        left join PartsPurReturnOrderDetail b
                                            on a.id = b.PartsPurReturnOrderid where 1=1  ");
                    }
                    var tableNames = sql.ToString().GetRegexNames();
                    var dbParameters = new List<DbParameter>();
                    if (ids != null && ids.Length > 0)
                    {
                        sql.Append(@" and a.id in (");
                        for (int i = 0; i < ids.Length; i++)
                        {
                            var tempIdWithNum = "tempIdWithNum" + i;
                            if (i == ids.Length - 1)
                            {
                                var tempParam = "{0}" + tempIdWithNum;
                                sql.Append(tempParam);
                            }
                            else
                            {
                                var tempParam = "{0}" + tempIdWithNum + ",";
                                sql.Append(tempParam);
                            }
                            dbParameters.Add(db.CreateDbParameter(tempIdWithNum, ids[i]));
                        }
                        sql.Append(")");
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(partsSupplierCode))
                        {
                            sql.Append(" and a.PartsSupplierCode = {0}PartsSupplierCode");
                            dbParameters.Add(db.CreateDbParameter("PartsSupplierCode", partsSupplierCode));
                        }
                        if (!string.IsNullOrEmpty(Code))
                        {
                            sql.Append(" and a.Code = {0}Code");
                            dbParameters.Add(db.CreateDbParameter("Code", Code));
                        }
                        if (BranchId.HasValue)
                        {
                            sql.Append(" and a.BranchId = {0} BranchId");
                            dbParameters.Add(db.CreateDbParameter("BranchId", BranchId));
                        }
                        if (partsSalesCategoryId.HasValue)
                        {
                            sql.Append(" and a.partsSalesCategoryId={0}partsSalesCategoryId");
                            dbParameters.Add(db.CreateDbParameter("partsSalesCategoryId", partsSalesCategoryId));
                        }
                        if (status.HasValue)
                        {
                            sql.Append(" and a.status={0}status");
                            dbParameters.Add(db.CreateDbParameter("status", status));
                        }
                        if (warehouseId.HasValue)
                        {
                            sql.Append(" and a.warehouseId={0}warehouseId");
                            dbParameters.Add(db.CreateDbParameter("warehouseId", warehouseId));
                        }
                        if (returnReason.HasValue)
                        {
                            sql.Append(" and a.returnReason={0}returnReason");
                            dbParameters.Add(db.CreateDbParameter("returnReason", returnReason));
                        }
                        if (createTimeBegin.HasValue)
                        {
                            sql.Append(" and a.createTime>=to_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if (createTimeEnd.HasValue)
                        {
                            sql.Append(" and a.createTime<=to_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                    }
                    sql.Append(" order by a.code");
                    DbCommand comm = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    comm.Parameters.AddRange(dbParameters.ToArray());
                    conn.Open();
                    var reader = comm.ExecuteReader();
                    using (var excelExport = new ExcelExport(filename))
                    {
                        excelExport.ExportByRow(index =>
                        {
                            if (index == 0)
                                return tableNames;
                            if (reader.Read())
                            {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if (num != reader.FieldCount)
                                {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

    }
}