﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        public bool ExportPartsPurchaseOrderWithDetailForShipping(bool isSupplierLogin, int[] partsPurchaseOrderIds, string code, string businessCode, string warehouseName, string partsSupplierCode, string partsSupplierName, int? partsPurchaseOrderTypeId, bool? ifDirectProvision, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, int? branchId, int? supplierId, int? warehouseId, int? InStatus, DateTime? RequestedDeliveryTimeBegin, DateTime? RequestedDeliveryTimeEnd, DateTime? approveTimeBegin, DateTime? approveTimeEnd, string ERPSourceOrderCode, int? ioStatus, out string filename) {
            /*
            参数：销售中心 （非必填）采购订单编号（非必填）仓库名称（非必填）供应商编号（非必填）供应商名称（非必填）订单类型（非必填）直供（非必填）
            状态（非必填）创建开始时间 （非必填）创建结束时间（非必填）分公司Id（非必填）供应商Id（非必填）订货仓库 （非必填）     
             */
            filename = GetExportFilePath("配件采购订单_" + ".xlsx");
            if(!HttpContext.Current.User.Identity.IsAuthenticated) {
                throw new Exception(ErrorStrings.File_Validation_User);
            }
            try {
                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {
                    var sql = new StringBuilder();
                    var userinfo = Utils.GetCurrentUserInfo();
                    if(isSupplierLogin) {
                        sql.Append(@" Select a.Code,/*采购订单编号*/
                           a.Branchname,/*分公司名称*/
                           a.Warehousename,/*仓库名称*/
                           a.Partssalescategoryname,/*品牌*/
                                 a.Partssuppliercode,/*供应商编号*/
                           a.Partssuppliername,/*供应商名称*/
                           c.Name As Partspurchaseordertype,/*订单类型*/
                           a.ReceivingAddress,/*收货地址*/
                          Extent4.value As IfDirectProvision,/*是否直供*/
                                 a.OriginalRequirementBillCode,/*原始需求单据编号*/
                                 a.Receivingcompanyname,/*收货单位名称*/
                           Extent5.value   As Shippingmethod,/*发运方式*/
                           Extent6.value  As Status,/*订单状态*/
                           Extent7.value As InStatus,/*入库状态*/
                           a.Plansource,/*计划来源*/
                           a.AbandonOrStopReason,/*作废或终止原因*/
                           a.ConfirmationRemark,/*确认备注*/
                           a.Remark,/*备注*/
                             CASE WHEN a.IsTransSap = 1 THEN cast('是' as varchar2(100))  ELSE cast('否' as varchar2(100)) END AS transSap,/*是否传输SAP*/
                           CASE WHEN ps.iostatus = 2 THEN cast('已传输' as varchar2(100)) WHEN  ps.iostatus = 0 THEN cast('未传输' as varchar2(100)) WHEN  ps.iostatus = 1 THEN cast('失败' as varchar2(100)) ELSE '' END AS iostatus,/*接口状态*/
                           a.Creatorname,/*创建人*/
                           a.Createtime,/*创建时间*/
                           a.SubmitterName,/*提交人*/
                           a.SubmitTime,/*提交时间*/
                           a.ApproverName,/*确认人*/
                           a.ApproveTime,/*确认时间*/
                           ps.message,/*接口消息*/                          
                           b.Sparepartname,/*配件名称*/
                           b.SupplierPartCode,/*供应商图号*/
                           b.Measureunit,/*计量单位*/
                           b.Specification,/*规格型号*/
                           b.OrderAmount,/*订货量*/
                           b.Confirmedamount,/*确认量*/
                          (select sum(nvl(inspectedquantity, 0)) as inQty
                                 from partsinboundcheckbilldetail e
                                inner join PartsInboundCheckBill d
                                   on d.id = e.partsinboundcheckbillid
                                inner join warehouse w
                                   on d.WarehouseId = w.id
                                where d.originalrequirementbillcode = a.code
                                  and e.sparepartid = b.sparepartid) as inspectedquantity,/*入库数量*/
                           b.Shippingamount,/*发运量*/
                           (select kl.value from keyvalueitem kl where kl.key=b.PriceType and kl.name='PurchasePriceType') as PriceType, /*价格类型*/
                           b.PromisedDeliveryTime,/*承诺到货时间*/
                           (select value from keyvalueitem where key = b.ShortSupReason and name = 'PartsPurchaseOrderDetail_ShortSupReason') as ShortSupReason2,/*短供原因*/
                           b.Remark,/*备注*/
                           b.ConfirmationRemark,/*确认备注*/
                           b.ABCStrategy /*配件ABC分类*/
                      From Partspurchaseorder a
                      Left Join Partspurchaseorderdetail b
                        On a.Id = b.Partspurchaseorderid
                      Left Join Partspurchaseordertype c
                        On a.Partspurchaseordertypeid = c.Id
                      Left Outer Join Branchsupplierrelation Extent3
                      On (Extent3.Status = 1)
                      And ((a.Partssupplierid = Extent3.Supplierid) And
                     (a.Partssalescategoryid = Extent3.Partssalescategoryid))
                      Left Join keyvalueitem Extent4
                        On Extent4.NAME = 'IsOrNot'
                        and Extent4.key=a.IfDirectProvision
                      Left Join keyvalueitem Extent5
                        On Extent5.NAME = 'PartsShipping_Method'
                        and Extent5.key=a.Shippingmethod
                      Left Join keyvalueitem Extent6
                        On Extent6.NAME = 'PartsPurchaseOrder_Status'
                        and Extent6.key=a.Status
                      Left Join keyvalueitem Extent7
                        On Extent7.NAME = 'PurchaseInStatus'
                        and Extent7.key=a.InStatus left join PartsPurchaseOrder_Sync ps ON a.Code = ps.Objid where 1=1 ");
                    } else {
                        sql.Append(@" Select a.Code,/*采购订单编号*/
                           a.Branchname,/*分公司名称*/
                           a.Warehousename,/*仓库名称*/
                           a.Partssalescategoryname,/*品牌*/
                            
                                 a.Partssuppliercode,/*供应商编号*/
                                 a.Partssuppliercode,/*供应商编号*/
                           a.Partssuppliername,/*供应商名称*/
                           c.Name As Partspurchaseordertype,/*订单类型*/
                           a.ReceivingAddress,/*收货地址*/
                           Extent8.value As IfDirectProvision,/*是否直供*/
                                 a.OriginalRequirementBillCode,/*原始需求单据编号*/
                                 a.Receivingcompanyname,/*收货单位名称*/
                           Extent5.value   As Shippingmethod,/*发运方式*/
                           Extent6.value  As Status,/*订单状态*/
                           Extent7.value As InStatus,/*入库状态*/
                           a.Plansource,/*计划来源*/
                           a.AbandonOrStopReason,/*作废或终止原因*/
                           a.ConfirmationRemark,/*确认备注*/
                           a.Remark,/*备注*/
                             CASE WHEN a.IsTransSap = 1 THEN cast('是' as varchar2(100))  ELSE cast('否' as varchar2(100)) END AS transSap,/*是否传输SAP*/
                          CASE WHEN ps.iostatus = 2 THEN cast('已传输' as varchar2(100)) WHEN  ps.iostatus = 0 THEN cast('未传输' as varchar2(100)) WHEN  ps.iostatus = 1 THEN cast('失败' as varchar2(100)) ELSE '' END AS iostatus,/*接口状态*/
                           a.Creatorname,/*创建人*/
                           a.Createtime,/*创建时间*/
                           a.SubmitterName,/*提交人*/
                           a.SubmitTime,/*提交时间*/
                           ps.message,/*接口消息*/
                           b.Sparepartcode,/*配件图号*/
                           b.Sparepartname,/*配件名称*/
                           b.SupplierPartCode,/*供应商图号*/
                           b.Measureunit,/*计量单位*/
                           b.Specification,/*规格型号*/
                           b.OrderAmount,/*订货量*/
                           b.Confirmedamount,/*确认量*/                          
                           (select sum(nvl(inspectedquantity, 0)) as inQty
                             from partsinboundcheckbilldetail e
                                inner join PartsInboundCheckBill d
                                   on d.id = e.partsinboundcheckbillid
                                inner join warehouse w
                                   on d.WarehouseId = w.id
                                where d.originalrequirementbillcode =
                                      a.code
                                  and e.sparepartid = b.sparepartid
                                  )
                            as inspectedquantity,/*入库数量*/
                           b.Shippingamount,/*发运量*/
                           (select kl.value from keyvalueitem kl where kl.key=b.PriceType and kl.name='PurchasePriceType') as PriceType, /*价格类型*/
                           b.PromisedDeliveryTime,/*承诺到货时间*/
                           (select value from keyvalueitem where key = b.ShortSupReason and name = 'PartsPurchaseOrderDetail_ShortSupReason') as ShortSupReason2,/*短供原因*/
                           b.Remark,/*备注*/
                           b.ConfirmationRemark,/*确认备注*/
                           b.ABCStrategy /*配件ABC分类*/
                      From Partspurchaseorder a
                      Left Join Partspurchaseorderdetail b
                        On a.Id = b.Partspurchaseorderid
                      Left Join Partspurchaseordertype c
                        On a.Partspurchaseordertypeid = c.Id
                      Left Outer Join Branchsupplierrelation Extent3
                      On (Extent3.Status = 1)
                      And ((a.Partssupplierid = Extent3.Supplierid) And
                     (a.Partssalescategoryid = Extent3.Partssalescategoryid))
                     Inner Join Personsalescenterlink Extent4
                     On a.Partssalescategoryid =Extent4.Partssalescategoryid
                      Left Join keyvalueitem Extent5
                        On Extent5.NAME = 'PartsShipping_Method'
                        and Extent5.key=a.Shippingmethod
                      Left Join keyvalueitem Extent6
                        On Extent6.NAME = 'PartsPurchaseOrder_Status'
                        and Extent6.key=a.Status
                      Left Join keyvalueitem Extent7
                        On Extent7.NAME = 'PurchaseInStatus'
                        and Extent7.key=a.InStatus
                      Left Join keyvalueitem Extent8
                        On Extent8.NAME = 'IsOrNot'
                        and Extent8.key=a.IfDirectProvision 
                        left join PartsPurchaseOrder_Sync ps ON a.Code = ps.Objid
                     Where Extent4.personid=" + userinfo.Id);
                    }

                    //配件入库检验单清单.配件入库检验单id=配件入库检验单.Id,--配件入库检验单.原始需求单据编号=采购订单.编号--,配件入库检验单清单.配件Id=配件采购订单清单.配件Id）
                    var tableNames = sql.ToString().GetRegexNames();
                    //TODO:处理oracle sql 语句in子句中（where id in (1, 2, ..., 1000, 1001)），如果子句中超过1000项就会报错。
                    //TODO:这主要是oracle考虑性能问题做的限制。如果要解决次问题，可以用 where id (1, 2, ..., 1000) or id (1001, ...)
                    var dbParameters = new List<DbParameter>();
                    if(partsPurchaseOrderIds != null && partsPurchaseOrderIds.Length > 0) {
                        sql.Append(@" and a.id in (");
                        for(var i = 0; i < partsPurchaseOrderIds.Length; i++) {
                            var tempPartsPurchaseOrderIdWithNum = "partsPurchaseOrderId" + i;
                            if(i == partsPurchaseOrderIds.Length - 1) {
                                var tempParam = "{0}" + tempPartsPurchaseOrderIdWithNum;
                                sql.Append(tempParam);
                            } else {
                                var tempParam = "{0}" + tempPartsPurchaseOrderIdWithNum + ",";
                                sql.Append(tempParam);
                            }
                            dbParameters.Add(db.CreateDbParameter(tempPartsPurchaseOrderIdWithNum, partsPurchaseOrderIds[i]));
                        }
                        sql.Append(@")");
                    } else {
                      
                        if(ioStatus.HasValue) {
                            sql.Append(" and ps.ioStatus={0}ioStatus");
                            dbParameters.Add(db.CreateDbParameter("ioStatus", ioStatus.Value));
                        }
                        if(!string.IsNullOrEmpty(code)) {
                            sql.Append(" and a.code like {0}code ");
                            dbParameters.Add(db.CreateDbParameter("code", "%" + code + "%"));
                        }
                        if(!string.IsNullOrEmpty(businessCode)) {
                            sql.Append(" and Extent3.Businesscode like {0}businessCode ");
                            dbParameters.Add(db.CreateDbParameter("businessCode", "%" + businessCode + "%"));
                        }
                        if(!string.IsNullOrEmpty(warehouseName)) {
                            sql.Append(" and a.warehouseName  like {0}warehouseName ");
                            dbParameters.Add(db.CreateDbParameter("warehouseName", "%" + warehouseName + "%"));
                        }
                    

                        if(!string.IsNullOrEmpty(partsSupplierCode)) {
                            sql.Append(" and a.partsSupplierCode like {0}partsSupplierCode ");
                            dbParameters.Add(db.CreateDbParameter("partsSupplierCode", "%" + partsSupplierCode + "%"));
                        }
                        if(!string.IsNullOrEmpty(partsSupplierName)) {
                            sql.Append(" and a.partsSupplierName like {0}partsSupplierName ");
                            dbParameters.Add(db.CreateDbParameter("partsSupplierName", "%" + partsSupplierName + "%"));
                        }

                        if(partsPurchaseOrderTypeId.HasValue) {
                            sql.Append(" and a.partsPurchaseOrderTypeId = {0}partsPurchaseOrderTypeId ");
                            dbParameters.Add(db.CreateDbParameter("partsPurchaseOrderTypeId", partsPurchaseOrderTypeId.Value));
                        }
                        if(ifDirectProvision.HasValue) {
                            sql.Append(" and a.ifDirectProvision = {0}ifDirectProvision");
                            if(ifDirectProvision == true) {
                                dbParameters.Add(db.CreateDbParameter("ifDirectProvision", 1));
                            }
                            if(ifDirectProvision == false) {
                                dbParameters.Add(db.CreateDbParameter("ifDirectProvision", 0));
                            }
                        }
                        if(status.HasValue) {
                            sql.Append(" and a.status={0}status");
                            dbParameters.Add(db.CreateDbParameter("status", status.Value));
                        }
                        if(InStatus.HasValue) {
                            sql.Append(" and a.InStatus={0}InStatus");
                            dbParameters.Add(db.CreateDbParameter("InStatus", InStatus.Value));
                        }
                        if(createTimeBegin.HasValue) {
                            sql.Append(" and a.createTime>=to_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(" and a.createTime<=to_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }

                        if(RequestedDeliveryTimeBegin.HasValue) {
                            sql.Append(" and a.RequestedDeliveryTime>=to_date({0}RequestedDeliveryTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = RequestedDeliveryTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("RequestedDeliveryTimeBegin", tempTime.ToString("G")));
                        }
                        if(RequestedDeliveryTimeEnd.HasValue) {
                            sql.Append(" and a.RequestedDeliveryTime<=to_date({0}RequestedDeliveryTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = RequestedDeliveryTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("RequestedDeliveryTimeEnd", tempTime.ToString("G")));
                        }
                        if(approveTimeBegin.HasValue) {
                            sql.Append(" and a.ApproveTime>=to_date({0}approveTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = approveTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("approveTimeBegin", tempTime.ToString("G")));
                        }
                        if(approveTimeEnd.HasValue) {
                            sql.Append(" and a.ApproveTime<=to_date({0}approveTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = approveTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("approveTimeEnd", tempTime.ToString("G")));
                        }

                        if(branchId.HasValue) {
                            sql.Append(@" and a.branchId={0}branchId");
                            dbParameters.Add(db.CreateDbParameter("branchId", branchId.Value));
                        }
                        if(supplierId.HasValue) {
                            sql.Append(" and a.PartsSupplierId={0}supplierId");
                            dbParameters.Add(db.CreateDbParameter("supplierId", supplierId.Value));
                        }
                        if(warehouseId.HasValue) {
                            sql.Append(" and a.warehouseId={0}warehouseId");
                            dbParameters.Add(db.CreateDbParameter("warehouseId", warehouseId.Value));
                        }

                        //此处ERPSourceOrderCode 为 OriginalRequirementBillCode
                        if(!string.IsNullOrEmpty(ERPSourceOrderCode)) {
                            sql.Append(" and a.OriginalRequirementBillCode like {0}OriginalRequirementBillCode ");
                            dbParameters.Add(db.CreateDbParameter("OriginalRequirementBillCode", "%" + ERPSourceOrderCode + "%"));
                        }
                    }

                    var sqlStr = sql.ToString();
                    if(sqlStr.IndexOf(" where 1=1  and ") > 0)
                        sqlStr = sqlStr.Replace(" where 1=1  and ", " where ");
                    else
                        sqlStr = sqlStr.Replace(" where 1=1 ", "");
                    DbCommand comm = db.CreateDbCommand(string.Format(sqlStr, db.ParamMark), conn, null);
                    comm.Parameters.AddRange(dbParameters.ToArray());
                    conn.Open();
                    var reader = comm.ExecuteReader();
                    using(var excelExport = new ExcelExport(filename)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0)
                                return tableNames;
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }

        public bool ExportPartsPurchaseOrderWithDetail(bool isSupplierLogin, int[] partsPurchaseOrderIds/*, int? partsSalesCategoryId*/, string code, string businessCode, string warehouseName, string partsSupplierCode, string partsSupplierName, int? partsPurchaseOrderTypeId, bool? ifDirectProvision, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, int? branchId, int? supplierId, int? warehouseId, int? InStatus, DateTime? RequestedDeliveryTimeBegin, DateTime? RequestedDeliveryTimeEnd, DateTime? approveTimeBegin, DateTime? approveTimeEnd/*, string SAPPurchasePlanCode, string hWPurOrderCode*/, string ERPSourceOrderCode/*, string CPPartsPurchaseOrderCode, string CPPartsInboundCheckCode*/,int? ioStatus,string sparePartCode, out string filename) {
            /*
            参数：销售中心 （非必填）采购订单编号（非必填）仓库名称（非必填）供应商编号（非必填）供应商名称（非必填）订单类型（非必填）直供（非必填）
            状态（非必填）创建开始时间 （非必填）创建结束时间（非必填）分公司Id（非必填）供应商Id（非必填）订货仓库 （非必填）     
             */
            filename = GetExportFilePath("配件采购订单_" + ".xlsx");
            if(!HttpContext.Current.User.Identity.IsAuthenticated) {
                throw new Exception(ErrorStrings.File_Validation_User);
            }
            try {
                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {
                    var sql = new StringBuilder();
                    var userinfo = Utils.GetCurrentUserInfo();
                    if(isSupplierLogin) {
                        sql.Append(@" Select a.Code,/*采购订单编号*/
                           a.Branchname,/*分公司名称*/
                           a.Warehousename,/*仓库名称*/
                           a.Partssalescategoryname,/*品牌*/
                                 a.Partssuppliercode,/*供应商编号*/
                           a.Partssuppliername,/*供应商名称*/
                           c.Name As Partspurchaseordertype,/*订单类型*/
                           a.ReceivingAddress,/*收货地址*/
                          Extent4.value As IfDirectProvision,/*是否直供*/
CASE WHEN a.IsPack = 1 THEN cast('是' as varchar2(100))  ELSE cast('否' as varchar2(100)) END AS transSap,/*是否需要包材*/
                                 a.OriginalRequirementBillCode,/*原始需求单据编号*/
                                 a.Receivingcompanyname,/*收货单位名称*/
                                 a.Totalamount,/*总金额*/
                           Extent5.value   As Shippingmethod,/*发运方式*/
                           Extent6.value  As Status,/*订单状态*/
                           Extent7.value As InStatus,/*入库状态*/
                           a.Plansource,/*计划来源*/
                           a.AbandonOrStopReason,/*作废或终止原因*/
                           a.ConfirmationRemark,/*确认备注*/
                           a.Remark,/*备注*/
                             CASE WHEN a.IsTransSap = 1 THEN cast('是' as varchar2(100))  ELSE cast('否' as varchar2(100)) END AS transSap,/*是否传输SAP*/
                           CASE WHEN ps.iostatus = 2 THEN cast('已传输' as varchar2(100)) WHEN  ps.iostatus = 0 THEN cast('未传输' as varchar2(100)) WHEN  ps.iostatus = 1 THEN cast('失败' as varchar2(100)) ELSE '' END AS iostatus,/*接口状态*/
                           a.Creatorname,/*创建人*/
                           a.Createtime,/*创建时间*/
                           a.SubmitterName,/*提交人*/
                           a.SubmitTime,/*提交时间*/
                           a.ApproverName,/*确认人*/
                           a.ApproveTime,/*确认时间*/
                           ps.message,/*接口消息*/
                           
                           b.Sparepartname,/*配件名称*/
                           b.SupplierPartCode,/*供应商图号*/
                           b.Measureunit,/*计量单位*/
                           b.Specification,/*规格型号*/
                           b.OrderAmount,/*订货量*/
                           b.Confirmedamount,/*确认量*/
                          (select sum(nvl(inspectedquantity, 0)) as inQty
                                 from partsinboundcheckbilldetail e
                                inner join PartsInboundCheckBill d
                                   on d.id = e.partsinboundcheckbillid
                                inner join warehouse w
                                   on d.WarehouseId = w.id
                                where d.originalrequirementbillcode = a.code
                                  and e.sparepartid = b.sparepartid) as inspectedquantity,/*入库数量*/
                           b.Shippingamount,/*发运量*/
                           b.Unitprice,/*单价*/
                           (select kl.value from keyvalueitem kl where kl.key=b.PriceType and kl.name='PurchasePriceType') as PriceType, /*价格类型*/
                           b.PromisedDeliveryTime,/*承诺到货时间*/
                           (select value from keyvalueitem where key = b.ShortSupReason and name = 'PartsPurchaseOrderDetail_ShortSupReason') as ShortSupReason2,/*短供原因*/
                           b.Remark,/*备注*/
                           b.ConfirmationRemark,/*确认备注*/
                           b.ABCStrategy /*配件ABC分类*/
                      From Partspurchaseorder a
                      Left Join Partspurchaseorderdetail b
                        On a.Id = b.Partspurchaseorderid
                      Left Join Partspurchaseordertype c
                        On a.Partspurchaseordertypeid = c.Id
                      Left Outer Join Branchsupplierrelation Extent3
                      On (Extent3.Status = 1)
                      And ((a.Partssupplierid = Extent3.Supplierid) And
                     (a.Partssalescategoryid = Extent3.Partssalescategoryid))
                      Left Join keyvalueitem Extent4
                        On Extent4.NAME = 'IsOrNot'
                        and Extent4.key=a.IfDirectProvision
                      Left Join keyvalueitem Extent5
                        On Extent5.NAME = 'PartsShipping_Method'
                        and Extent5.key=a.Shippingmethod
                      Left Join keyvalueitem Extent6
                        On Extent6.NAME = 'PartsPurchaseOrder_Status'
                        and Extent6.key=a.Status
                      Left Join keyvalueitem Extent7
                        On Extent7.NAME = 'PurchaseInStatus'
                        and Extent7.key=a.InStatus left join PartsPurchaseOrder_Sync ps ON a.Code = ps.Objid where 1=1 ");
                    } else {
                        sql.Append(@" Select a.Code,/*采购订单编号*/
                           a.Branchname,/*分公司名称*/
                           a.Warehousename,/*仓库名称*/
                           a.Partssalescategoryname,/*品牌*/
                            
                                 a.Partssuppliercode,/*供应商编号*/
                                 a.Partssuppliercode,/*供应商编号*/
                           a.Partssuppliername,/*供应商名称*/
                           c.Name As Partspurchaseordertype,/*订单类型*/
                           a.ReceivingAddress,/*收货地址*/
                           Extent8.value As IfDirectProvision,/*是否直供*/
CASE WHEN a.IsPack = 1 THEN cast('是' as varchar2(100))  ELSE cast('否' as varchar2(100)) END AS transSap,/*是否需要包材*/
                                 a.OriginalRequirementBillCode,/*原始需求单据编号*/
                                 a.Receivingcompanyname,/*收货单位名称*/
                                 a.Totalamount,/*总金额*/
                           Extent5.value   As Shippingmethod,/*发运方式*/
                           Extent6.value  As Status,/*订单状态*/
                           Extent7.value As InStatus,/*入库状态*/
                           a.Plansource,/*计划来源*/
                           a.AbandonOrStopReason,/*作废或终止原因*/
                           a.ConfirmationRemark,/*确认备注*/
                           a.Remark,/*备注*/
                             CASE WHEN a.IsTransSap = 1 THEN cast('是' as varchar2(100))  ELSE cast('否' as varchar2(100)) END AS transSap,/*是否传输SAP*/
                          CASE WHEN ps.iostatus = 2 THEN cast('已传输' as varchar2(100)) WHEN  ps.iostatus = 0 THEN cast('未传输' as varchar2(100)) WHEN  ps.iostatus = 1 THEN cast('失败' as varchar2(100)) ELSE '' END AS iostatus,/*接口状态*/
                           a.Creatorname,/*创建人*/
                           a.Createtime,/*创建时间*/
                           a.SubmitterName,/*提交人*/
                           a.SubmitTime,/*提交时间*/
                           ps.message,/*接口消息*/
                           b.Sparepartcode,/*配件图号*/
                           b.Sparepartname,/*配件名称*/
                           b.SupplierPartCode,/*供应商图号*/
                           b.Measureunit,/*计量单位*/
                           b.Specification,/*规格型号*/
                           b.OrderAmount,/*订货量*/
                           b.Confirmedamount,/*确认量*/                          
                           (select sum(nvl(inspectedquantity, 0)) as inQty
                             from partsinboundcheckbilldetail e
                                inner join PartsInboundCheckBill d
                                   on d.id = e.partsinboundcheckbillid
                                inner join warehouse w
                                   on d.WarehouseId = w.id
                                where d.originalrequirementbillcode =
                                      a.code
                                  and e.sparepartid = b.sparepartid
                                  )
                            as inspectedquantity,/*入库数量*/
                           b.Shippingamount,/*发运量*/
                           b.Unitprice,/*单价*/
                           (select kl.value from keyvalueitem kl where kl.key=b.PriceType and kl.name='PurchasePriceType') as PriceType, /*价格类型*/
                           b.PromisedDeliveryTime,/*承诺到货时间*/
                           (select value from keyvalueitem where key = b.ShortSupReason and name = 'PartsPurchaseOrderDetail_ShortSupReason') as ShortSupReason2,/*短供原因*/
                           b.Remark,/*备注*/
                           b.ConfirmationRemark,/*确认备注*/
                           b.ABCStrategy /*配件ABC分类*/
                      From Partspurchaseorder a
                      Left Join Partspurchaseorderdetail b
                        On a.Id = b.Partspurchaseorderid
                      Left Join Partspurchaseordertype c
                        On a.Partspurchaseordertypeid = c.Id
                      Left Outer Join Branchsupplierrelation Extent3
                      On (Extent3.Status = 1)
                      And ((a.Partssupplierid = Extent3.Supplierid) And
                     (a.Partssalescategoryid = Extent3.Partssalescategoryid))
                     Inner Join Personsalescenterlink Extent4
                     On a.Partssalescategoryid =Extent4.Partssalescategoryid
                      Left Join keyvalueitem Extent5
                        On Extent5.NAME = 'PartsShipping_Method'
                        and Extent5.key=a.Shippingmethod
                      Left Join keyvalueitem Extent6
                        On Extent6.NAME = 'PartsPurchaseOrder_Status'
                        and Extent6.key=a.Status
                      Left Join keyvalueitem Extent7
                        On Extent7.NAME = 'PurchaseInStatus'
                        and Extent7.key=a.InStatus
                      Left Join keyvalueitem Extent8
                        On Extent8.NAME = 'IsOrNot'
                        and Extent8.key=a.IfDirectProvision 
                        left join PartsPurchaseOrder_Sync ps ON a.Code = ps.Objid
                     Where Extent4.personid=" + userinfo.Id);
                    }

                    //配件入库检验单清单.配件入库检验单id=配件入库检验单.Id,--配件入库检验单.原始需求单据编号=采购订单.编号--,配件入库检验单清单.配件Id=配件采购订单清单.配件Id）
                    var tableNames = sql.ToString().GetRegexNames();
                    //TODO:处理oracle sql 语句in子句中（where id in (1, 2, ..., 1000, 1001)），如果子句中超过1000项就会报错。
                    //TODO:这主要是oracle考虑性能问题做的限制。如果要解决次问题，可以用 where id (1, 2, ..., 1000) or id (1001, ...)
                    var dbParameters = new List<DbParameter>();
                    if(partsPurchaseOrderIds != null && partsPurchaseOrderIds.Length > 0) {
                        sql.Append(@" and a.id in (");
                        for(var i = 0; i < partsPurchaseOrderIds.Length; i++) {
                            var tempPartsPurchaseOrderIdWithNum = "partsPurchaseOrderId" + i;
                            if(i == partsPurchaseOrderIds.Length - 1) {
                                var tempParam = "{0}" + tempPartsPurchaseOrderIdWithNum;
                                sql.Append(tempParam);
                            } else {
                                var tempParam = "{0}" + tempPartsPurchaseOrderIdWithNum + ",";
                                sql.Append(tempParam);
                            }
                            dbParameters.Add(db.CreateDbParameter(tempPartsPurchaseOrderIdWithNum, partsPurchaseOrderIds[i]));
                        }
                        sql.Append(@")");
                    } else {
                        //if(partsSalesCategoryId.HasValue) {
                        //    sql.Append(" and a.partsSalesCategoryId={0}partsSalesCategoryId");
                        //    dbParameters.Add(db.CreateDbParameter("partsSalesCategoryId", partsSalesCategoryId.Value));
                        //}
                        if(ioStatus.HasValue) {
                            sql.Append(" and ps.ioStatus={0}ioStatus");
                            dbParameters.Add(db.CreateDbParameter("ioStatus", ioStatus.Value));
                        }
                        if(!string.IsNullOrEmpty(code)) {
                            sql.Append(" and a.code like {0}code ");
                            dbParameters.Add(db.CreateDbParameter("code", "%" + code + "%"));
                        }
                        if(!string.IsNullOrEmpty(businessCode)) {
                            sql.Append(" and Extent3.Businesscode like {0}businessCode ");
                            dbParameters.Add(db.CreateDbParameter("businessCode", "%" + businessCode + "%"));
                        }
                        if(!string.IsNullOrEmpty(warehouseName)) {
                            sql.Append(" and a.warehouseName  like {0}warehouseName ");
                            dbParameters.Add(db.CreateDbParameter("warehouseName", "%" + warehouseName + "%"));
                        }
                        //if(!string.IsNullOrEmpty(hWPurOrderCode)) {
                        //    sql.Append(" and a.hWPurOrderCode  like {0}hWPurOrderCode ");
                        //    dbParameters.Add(db.CreateDbParameter("hWPurOrderCode", "%" + hWPurOrderCode + "%"));
                        //}

                        if(!string.IsNullOrEmpty(partsSupplierCode)) {
                            sql.Append(" and a.partsSupplierCode like {0}partsSupplierCode ");
                            dbParameters.Add(db.CreateDbParameter("partsSupplierCode", "%" + partsSupplierCode + "%"));
                        }
                        if(!string.IsNullOrEmpty(partsSupplierName)) {
                            sql.Append(" and a.partsSupplierName like {0}partsSupplierName ");
                            dbParameters.Add(db.CreateDbParameter("partsSupplierName", "%" + partsSupplierName + "%"));
                        }

                        if(partsPurchaseOrderTypeId.HasValue) {
                            sql.Append(" and a.partsPurchaseOrderTypeId = {0}partsPurchaseOrderTypeId ");
                            dbParameters.Add(db.CreateDbParameter("partsPurchaseOrderTypeId", partsPurchaseOrderTypeId.Value));
                        }
                        if(ifDirectProvision.HasValue) {
                            sql.Append(" and a.ifDirectProvision = {0}ifDirectProvision");
                            if(ifDirectProvision == true) {
                                dbParameters.Add(db.CreateDbParameter("ifDirectProvision", 1));
                            }
                            if(ifDirectProvision == false) {
                                dbParameters.Add(db.CreateDbParameter("ifDirectProvision", 0));
                            }
                        }
                        if(status.HasValue) {
                            sql.Append(" and a.status={0}status");
                            dbParameters.Add(db.CreateDbParameter("status", status.Value));
                        }
                        if(InStatus.HasValue) {
                            sql.Append(" and a.InStatus={0}InStatus");
                            dbParameters.Add(db.CreateDbParameter("InStatus", InStatus.Value));
                        }
                        if(createTimeBegin.HasValue) {
                            sql.Append(" and a.createTime>=to_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(" and a.createTime<=to_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }

                        if(RequestedDeliveryTimeBegin.HasValue) {
                            sql.Append(" and a.RequestedDeliveryTime>=to_date({0}RequestedDeliveryTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = RequestedDeliveryTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("RequestedDeliveryTimeBegin", tempTime.ToString("G")));
                        }
                        if(RequestedDeliveryTimeEnd.HasValue) {
                            sql.Append(" and a.RequestedDeliveryTime<=to_date({0}RequestedDeliveryTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = RequestedDeliveryTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("RequestedDeliveryTimeEnd", tempTime.ToString("G")));
                        }
                        if (approveTimeBegin.HasValue)
                        {
                            sql.Append(" and a.ApproveTime>=to_date({0}approveTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = approveTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("approveTimeBegin", tempTime.ToString("G")));
                        }
                        if (approveTimeEnd.HasValue)
                        {
                            sql.Append(" and a.ApproveTime<=to_date({0}approveTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = approveTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("approveTimeEnd", tempTime.ToString("G")));
                        }

                        if (branchId.HasValue) {
                            sql.Append(@" and a.branchId={0}branchId");
                            dbParameters.Add(db.CreateDbParameter("branchId", branchId.Value));
                        }
                        if(supplierId.HasValue) {
                            sql.Append(" and a.PartsSupplierId={0}supplierId");
                            dbParameters.Add(db.CreateDbParameter("supplierId", supplierId.Value));
                        }
                        if(warehouseId.HasValue) {
                            sql.Append(" and a.warehouseId={0}warehouseId");
                            dbParameters.Add(db.CreateDbParameter("warehouseId", warehouseId.Value));
                        }
                        //if(!string.IsNullOrEmpty(SAPPurchasePlanCode)) {
                        //    sql.Append(" and a.SAPPurchasePlanCode like {0}SAPPurchasePlanCode ");
                        //    dbParameters.Add(db.CreateDbParameter("SAPPurchasePlanCode", "%" + SAPPurchasePlanCode + "%"));
                        //}

                        //此处ERPSourceOrderCode 为 OriginalRequirementBillCode
                        if(!string.IsNullOrEmpty(ERPSourceOrderCode)) {
                            sql.Append(" and a.OriginalRequirementBillCode like {0}OriginalRequirementBillCode ");
                            dbParameters.Add(db.CreateDbParameter("OriginalRequirementBillCode", "%" + ERPSourceOrderCode + "%"));
                        }
                        //if (!string.IsNullOrEmpty(CPPartsPurchaseOrderCode))
                        //{
                        //    sql.Append(" and a.CPPartsPurchaseOrderCode like {0}CPPartsPurchaseOrderCode ");
                        //    dbParameters.Add(db.CreateDbParameter("CPPartsPurchaseOrderCode", "%" + CPPartsPurchaseOrderCode + "%"));
                        //}
                        //if (!string.IsNullOrEmpty(CPPartsInboundCheckCode))
                        //{
                        //    sql.Append(" and a.CPPartsInboundCheckCode like {0}CPPartsInboundCheckCode ");
                        //    dbParameters.Add(db.CreateDbParameter("CPPartsInboundCheckCode", "%" + CPPartsInboundCheckCode + "%"));
                        //}
                        if(!string.IsNullOrEmpty(sparePartCode)){
                            sql.Append(" and b.sparePartCode like {0}sparePartCode ");
                            dbParameters.Add(db.CreateDbParameter("sparePartCode", "%" + sparePartCode + "%"));
                        }
                    }

                    var sqlStr = sql.ToString();
                    if(sqlStr.IndexOf(" where 1=1  and ") > 0)
                        sqlStr = sqlStr.Replace(" where 1=1  and ", " where ");
                    else
                        sqlStr = sqlStr.Replace(" where 1=1 ", "");
                    DbCommand comm = db.CreateDbCommand(string.Format(sqlStr, db.ParamMark), conn, null);
                    comm.Parameters.AddRange(dbParameters.ToArray());
                    conn.Open();
                    var reader = comm.ExecuteReader();
                    using(var excelExport = new ExcelExport(filename)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0)
                                return tableNames;
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }

        public bool ExportPartsPurchaseOrder(bool isSupplierLogin, int[] partsPurchaseOrderIds/*, int? partsSalesCategoryId*/, string code, string businessCode, string warehouseName, string partsSupplierCode, string partsSupplierName, int? partsPurchaseOrderTypeId, bool? ifDirectProvision, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, int? branchId, int? supplierId, int? warehouseId, int? InStatus, DateTime? RequestedDeliveryTimeBegin, DateTime? RequestedDeliveryTimeEnd/*, string SAPPurchasePlanCode, string hWPurOrderCode*/, string ERPSourceOrderCode/*, string CPPartsPurchaseOrderCode, string CPPartsInboundCheckCode*/,int? ioStatus, out string filename) {
            /*
            参数：销售中心 （非必填）采购订单编号（非必填）仓库名称（非必填）供应商编号（非必填）供应商名称（非必填）订单类型（非必填）直供（非必填）
            状态（非必填）创建开始时间 （非必填）创建结束时间（非必填）分公司Id（非必填）供应商Id（非必填）订货仓库 （非必填）     
             */
            filename = GetExportFilePath("配件采购订单_" + ".xlsx");
            if(!HttpContext.Current.User.Identity.IsAuthenticated) {
                throw new Exception(ErrorStrings.File_Validation_User);
            }
            try {
                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {
                    var sql = new StringBuilder();
                    var userinfo = Utils.GetCurrentUserInfo();
                    if(isSupplierLogin) {
                        sql.Append(@" Select 
                                            a.Code, /*采购订单编号*/
                                            a.Branchname, /*分公司名称*/
                                            a.Warehousename, /*仓库名称*/
                                            a.Partssalescategoryname, /*品牌*/
                                            a.Partssuppliercode,/*供应商编号*/
                                            a.Partssuppliername, /*供应商名称*/
                                            Extent3.Businesscode, /*供应商业务编码*/
                                            c.Name                        As Partspurchaseordertype, /*订单类型*/
                                            a.ReceivingAddress, /*收货地址*/
                                            Extent4.value                 As IfDirectProvision, /*是否直供*/
                                            a.OriginalRequirementBillCode, /*原始需求单据编号*/
                                            a.Receivingcompanyname, /*收货单位名称*/
                                            a.Totalamount, /*总金额*/
                                            Extent5.value                 As Shippingmethod, /*发运方式*/
                                            Extent6.value                 As Status, /*订单状态*/
                                            Extent7.value                 As InStatus, /*入库状态*/
                                            a.Plansource, /*计划来源*/
                                            a.GPMSPurOrderCode, /*GPMS采购计划单号*/
                                            a.AbandonOrStopReason, /*作废或终止原因*/
                                            a.ConfirmationRemark, /*确认备注*/
                                            a.Remark, /*备注*/
                                             CASE WHEN a.IsTransSap = 1 THEN cast('是' as varchar2(100))  ELSE cast('否' as varchar2(100)) END AS transSap,/*是否传输SAP*/
                                           CASE WHEN ps.iostatus = 2 THEN cast('已传输' as varchar2(100)) WHEN  ps.iostatus = 0 THEN cast('未传输' as varchar2(100)) WHEN  ps.iostatus = 1 THEN cast('失败' as varchar2(100)) ELSE '' END AS iostatus,/*接口状态*/
                                            a.Creatorname, /*创建人*/
                                            a.ERPSourceOrderCode, /*平台单号*/
                                            a.Createtime, /*创建时间*/
                                            a.SubmitterName,/*提交人*/
                                            a.SubmitTime,/*提交时间*/
                                            ps.message /*接口消息*/
                                        From Partspurchaseorder a
                                        Left Join Partspurchaseordertype c
                                        On a.Partspurchaseordertypeid = c.Id
                                        Left Outer Join Branchsupplierrelation Extent3
                                        On (Extent3.Status = 1)
                                        And ((a.Partssupplierid = Extent3.Supplierid) And
                                            (a.Partssalescategoryid = Extent3.Partssalescategoryid))
                                        Left Join keyvalueitem Extent4
                                        On Extent4.NAME = 'IsOrNot'
                                        and Extent4.key = a.IfDirectProvision
                                        Left Join keyvalueitem Extent5
                                        On Extent5.NAME = 'PartsShipping_Method'
                                        and Extent5.key = a.Shippingmethod
                                        Left Join keyvalueitem Extent6
                                        On Extent6.NAME = 'PartsPurchaseOrder_Status'
                                        and Extent6.key = a.Status
                                        Left Join keyvalueitem Extent7
                                        On Extent7.NAME = 'PurchaseInStatus'
                                        and Extent7.key = a.InStatus
                                        left join PartsPurchaseOrder_Sync ps ON a.Code = ps.Objid
                                        where 1 = 1 ");
                    } else {
                        sql.Append(@" Select 
                                            a.Code, /*采购订单编号*/
                                            a.Branchname, /*分公司名称*/
                                            a.Warehousename, /*仓库名称*/
                                            a.Partssalescategoryname, /*品牌*/
                                            a.Partssuppliercode, /*供应商编号*/
                                            a.Partssuppliername, /*供应商名称*/
                                            Extent3.Businesscode, /*供应商业务编码*/
                                            c.Name                        As Partspurchaseordertype, /*订单类型*/
                                            a.ReceivingAddress, /*收货地址*/
                                            Extent8.value                 As IfDirectProvision, /*是否直供*/
                                            a.OriginalRequirementBillCode, /*原始需求单据编号*/
                                            a.Receivingcompanyname, /*收货单位名称*/
                                            a.Totalamount, /*总金额*/
                                            Extent5.value                 As Shippingmethod, /*发运方式*/
                                            Extent6.value                 As Status, /*订单状态*/
                                            Extent7.value                 As InStatus, /*入库状态*/
                                            a.Plansource, /*计划来源*/
                                            a.GPMSPurOrderCode, /*GPMS采购计划单号*/
                                            a.AbandonOrStopReason, /*作废或终止原因*/
                                            a.ConfirmationRemark, /*确认备注*/
                                            a.Remark, /*备注*/
                                            a.ERPSourceOrderCode, /*平台单号*/
                                             CASE WHEN a.IsTransSap = 1 THEN cast('是' as varchar2(100))  ELSE cast('否' as varchar2(100)) END AS transSap,/*是否传输SAP*/
                                           CASE WHEN ps.iostatus = 2 THEN cast('已传输' as varchar2(100)) WHEN  ps.iostatus = 0 THEN cast('未传输' as varchar2(100)) WHEN  ps.iostatus = 1 THEN cast('失败' as varchar2(100)) ELSE '' END AS iostatus,/*接口状态*/
                                            a.Creatorname, /*创建人*/
                                            a.Createtime, /*创建时间*/
                                            a.SubmitterName,/*提交人*/
                                            a.SubmitTime,/*提交时间*/
                                            ps.message /*接口消息*/
                                        From Partspurchaseorder a
                                        Left Join Partspurchaseordertype c
                                        On a.Partspurchaseordertypeid = c.Id
                                        Left Outer Join Branchsupplierrelation Extent3
                                        On (Extent3.Status = 1)
                                        And ((a.Partssupplierid = Extent3.Supplierid) And
                                            (a.Partssalescategoryid = Extent3.Partssalescategoryid))
                                        Inner Join Personsalescenterlink Extent4
                                        On a.Partssalescategoryid = Extent4.Partssalescategoryid
                                        Left Join keyvalueitem Extent5
                                        On Extent5.NAME = 'PartsShipping_Method'
                                        and Extent5.key = a.Shippingmethod
                                        Left Join keyvalueitem Extent6
                                        On Extent6.NAME = 'PartsPurchaseOrder_Status'
                                        and Extent6.key = a.Status
                                        Left Join keyvalueitem Extent7
                                        On Extent7.NAME = 'PurchaseInStatus'
                                        and Extent7.key = a.InStatus
                                        Left Join keyvalueitem Extent8
                                        On Extent8.NAME = 'IsOrNot'
                                        and Extent8.key = a.IfDirectProvision 
                                        left join PartsPurchaseOrder_Sync ps ON a.Code = ps.Objid 
                     Where Extent4.personid=" + userinfo.Id);
                    }

                    //配件入库检验单清单.配件入库检验单id=配件入库检验单.Id,--配件入库检验单.原始需求单据编号=采购订单.编号--,配件入库检验单清单.配件Id=配件采购订单清单.配件Id）
                    var tableNames = sql.ToString().GetRegexNames();
                    //TODO:处理oracle sql 语句in子句中（where id in (1, 2, ..., 1000, 1001)），如果子句中超过1000项就会报错。
                    //TODO:这主要是oracle考虑性能问题做的限制。如果要解决次问题，可以用 where id (1, 2, ..., 1000) or id (1001, ...)
                    var dbParameters = new List<DbParameter>();
                    if(partsPurchaseOrderIds != null && partsPurchaseOrderIds.Length > 0) {
                        sql.Append(@" and a.id in (");
                        for(var i = 0; i < partsPurchaseOrderIds.Length; i++) {
                            var tempPartsPurchaseOrderIdWithNum = "partsPurchaseOrderId" + i;
                            if(i == partsPurchaseOrderIds.Length - 1) {
                                var tempParam = "{0}" + tempPartsPurchaseOrderIdWithNum;
                                sql.Append(tempParam);
                            } else {
                                var tempParam = "{0}" + tempPartsPurchaseOrderIdWithNum + ",";
                                sql.Append(tempParam);
                            }
                            dbParameters.Add(db.CreateDbParameter(tempPartsPurchaseOrderIdWithNum, partsPurchaseOrderIds[i]));
                        }
                        sql.Append(@")");
                    } else {
                        //if(partsSalesCategoryId.HasValue) {
                        //    sql.Append(" and a.partsSalesCategoryId={0}partsSalesCategoryId");
                        //    dbParameters.Add(db.CreateDbParameter("partsSalesCategoryId", partsSalesCategoryId.Value));
                        //}
                        if(ioStatus.HasValue) {
                            sql.Append(" and ps.ioStatus={0}ioStatus");
                            dbParameters.Add(db.CreateDbParameter("ioStatus", ioStatus.Value));
                        }
                        if(!string.IsNullOrEmpty(code)) {
                            sql.Append(" and a.code like {0}code ");
                            dbParameters.Add(db.CreateDbParameter("code", "%" + code + "%"));
                        }
                        if(!string.IsNullOrEmpty(businessCode)) {
                            sql.Append(" and Extent3.Businesscode like {0}businessCode ");
                            dbParameters.Add(db.CreateDbParameter("businessCode", "%" + businessCode + "%"));
                        }
                        if(!string.IsNullOrEmpty(warehouseName)) {
                            sql.Append(" and a.warehouseName  like {0}warehouseName ");
                            dbParameters.Add(db.CreateDbParameter("warehouseName", "%" + warehouseName + "%"));
                        }
                        //if(!string.IsNullOrEmpty(hWPurOrderCode)) {
                        //    sql.Append(" and a.hWPurOrderCode  like {0}hWPurOrderCode ");
                        //    dbParameters.Add(db.CreateDbParameter("hWPurOrderCode", "%" + hWPurOrderCode + "%"));
                        //}

                        if(!string.IsNullOrEmpty(partsSupplierCode)) {
                            sql.Append(" and a.partsSupplierCode like {0}partsSupplierCode ");
                            dbParameters.Add(db.CreateDbParameter("partsSupplierCode", "%" + partsSupplierCode + "%"));
                        }
                        if(!string.IsNullOrEmpty(partsSupplierName)) {
                            sql.Append(" and a.partsSupplierName like {0}partsSupplierName ");
                            dbParameters.Add(db.CreateDbParameter("partsSupplierName", "%" + partsSupplierName + "%"));
                        }

                        if(partsPurchaseOrderTypeId.HasValue) {
                            sql.Append(" and a.partsPurchaseOrderTypeId = {0}partsPurchaseOrderTypeId ");
                            dbParameters.Add(db.CreateDbParameter("partsPurchaseOrderTypeId", partsPurchaseOrderTypeId.Value));
                        }
                        if(ifDirectProvision.HasValue) {
                            sql.Append(" and a.ifDirectProvision = {0}ifDirectProvision");
                            if(ifDirectProvision == true) {
                                dbParameters.Add(db.CreateDbParameter("ifDirectProvision", 1));
                            }
                            if(ifDirectProvision == false) {
                                dbParameters.Add(db.CreateDbParameter("ifDirectProvision", 0));
                            }
                        }
                        if(status.HasValue) {
                            sql.Append(" and a.status={0}status");
                            dbParameters.Add(db.CreateDbParameter("status", status.Value));
                        }
                        if(InStatus.HasValue) {
                            sql.Append(" and a.InStatus={0}InStatus");
                            dbParameters.Add(db.CreateDbParameter("InStatus", InStatus.Value));
                        }
                        if(createTimeBegin.HasValue) {
                            sql.Append(" and a.createTime>=to_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(" and a.createTime<=to_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }

                        if(RequestedDeliveryTimeBegin.HasValue) {
                            sql.Append(" and a.RequestedDeliveryTime>=to_date({0}RequestedDeliveryTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = RequestedDeliveryTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("RequestedDeliveryTimeBegin", tempTime.ToString("G")));
                        }
                        if(RequestedDeliveryTimeEnd.HasValue) {
                            sql.Append(" and a.RequestedDeliveryTime<=to_date({0}RequestedDeliveryTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = RequestedDeliveryTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("RequestedDeliveryTimeEnd", tempTime.ToString("G")));
                        }

                        if(branchId.HasValue) {
                            sql.Append(@" and a.branchId={0}branchId");
                            dbParameters.Add(db.CreateDbParameter("branchId", branchId.Value));
                        }
                        if(supplierId.HasValue) {
                            sql.Append(" and a.PartsSupplierId={0}supplierId");
                            dbParameters.Add(db.CreateDbParameter("supplierId", supplierId.Value));
                        }
                        if(warehouseId.HasValue) {
                            sql.Append(" and a.warehouseId={0}warehouseId");
                            dbParameters.Add(db.CreateDbParameter("warehouseId", warehouseId.Value));
                        }
                        //if(!string.IsNullOrEmpty(SAPPurchasePlanCode)) {
                        //    sql.Append(" and a.SAPPurchasePlanCode like {0}SAPPurchasePlanCode ");
                        //    dbParameters.Add(db.CreateDbParameter("SAPPurchasePlanCode", "%" + SAPPurchasePlanCode + "%"));
                        //}
                        if(!string.IsNullOrEmpty(ERPSourceOrderCode)) {
                            sql.Append(" and a.ERPSourceOrderCode like {0}ERPSourceOrderCode ");
                            dbParameters.Add(db.CreateDbParameter("ERPSourceOrderCode", "%" + ERPSourceOrderCode + "%"));
                        }
                        //if (!string.IsNullOrEmpty(CPPartsPurchaseOrderCode))
                        //{
                        //    sql.Append(" and a.CPPartsPurchaseOrderCode like {0}CPPartsPurchaseOrderCode ");
                        //    dbParameters.Add(db.CreateDbParameter("CPPartsPurchaseOrderCode", "%" + CPPartsPurchaseOrderCode + "%"));
                        //}
                        //if (!string.IsNullOrEmpty(CPPartsInboundCheckCode))
                        //{
                        //    sql.Append(" and a.CPPartsInboundCheckCode like {0}CPPartsInboundCheckCode ");
                        //    dbParameters.Add(db.CreateDbParameter("CPPartsInboundCheckCode", "%" + CPPartsInboundCheckCode + "%"));
                        //}
                    }

                    var sqlStr = sql.ToString();
                    if(sqlStr.IndexOf(" where 1=1  and ") > 0)
                        sqlStr = sqlStr.Replace(" where 1=1  and ", " where ");
                    else
                        sqlStr = sqlStr.Replace(" where 1=1 ", "");
                    DbCommand comm = db.CreateDbCommand(string.Format(sqlStr, db.ParamMark), conn, null);
                    comm.Parameters.AddRange(dbParameters.ToArray());
                    conn.Open();
                    var reader = comm.ExecuteReader();
                    using(var excelExport = new ExcelExport(filename)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0)
                                return tableNames;
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }

        public bool ScheduleExportPartsPurchaseOrderWithDetail(/*int? partsSalesCategoryId,*/ string code, string warehouseName/*, string partsSupplierCode*/, string partsSupplierName, int? partsPurchaseOrderTypeId, bool? ifDirectProvision, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, int? branchId, int? supplierId, int? warehouseId, string jobName, out string exportDataFileName) {
            /*
            参数：销售中心 （非必填）采购订单编号（非必填）仓库名称（非必填）供应商编号（非必填）供应商名称（非必填）订单类型（非必填）直供（非必填）
            状态（非必填）创建开始时间 （非必填）创建结束时间（非必填）分公司Id（非必填）供应商Id（非必填）订货仓库 （非必填）     
             */
            exportDataFileName = "配件采购订单_" + Guid.NewGuid() + ".xlsx";
            if(!HttpContext.Current.User.Identity.IsAuthenticated) {
                throw new Exception(ErrorStrings.File_Validation_User);
            }
            try {
                var db = DbHelper.GetDbHelp(ConnectionString);
                using(var conn = db.CreateDbConnection()) {
                    var sql = new StringBuilder();
                    sql.Append(@"select a.Code,
                                        a.BranchName,
                                        a.PartsSalesCategoryName,
                                        a.WarehouseName,
                                        a.PartsSupplierCode,
                                        a.PartsSupplierName,
                                        a.PlanSource,
                                        a.GPMSPurOrderCode,
                                        (select value from keyvalueitem where NAME = 'PartsPurchaseOrder_OrderType'and key=a.PartsPurchaseOrderTypeId) As PartsPurchaseOrderType,
                                        a.ReceivingAddress,
                                        (select value from keyvalueitem where NAME = 'IsOrNot'and key=a.IfDirectProvision) As IfDirectProvision,
                                        a.ReceivingCompanyName,
                                        a.TotalAmount,
                                        (select value from keyvalueitem where NAME = 'PartsShipping_Method'and key=a.Shippingmethod) As Shippingmethod,
                                        (select value from keyvalueitem where NAME = 'PartsPurchaseOrder_Status'and key=a.Status) As Status,
                                        (select value from keyvalueitem where NAME = 'PurchaseInStatus'and key=a.InStatus) As InStatus,
                                        a.ConfirmationRemark,
                                        a.Remark,
                                        a.CreatorName,
                                        a.CreateTime，
                                        b.SparePartCode,
                                        b.SparePartName,
                                        b.SupplierPartCode,
                                        b.MeasureUnit,
                                        b.OrderAmount,
                                        b.ConfirmedAmount,
                                        b.ShippingAmount,
                                        b.UnitPrice,
                                        (select kl.value from keyvalueitem kl where kl.key=b.PriceType and kl.name='PurchasePriceType') as PriceType,
                                        b.PackingAmount,
                                        b.PackingSpecification,
                                        b.PromisedDeliveryTime,
                                        b.Remark as DetailRemark,
                                        b.ConfirmationRemark as  DetailConfirmationRemark
                                         from PartsPurchaseOrder a
                                         left join PartsPurchaseOrderDetail b
                                            on a.id = b.Partspurchaseorderid
                                         where 1 = 1");
                    //if(partsSalesCategoryId.HasValue) {
                    //    sql.Append(" and a.partsSalesCategoryId={0}partsSalesCategoryId");
                    //}
                    if(!string.IsNullOrEmpty(code)) {
                        sql.Append(" and a.code like {0}code ");
                    }
                    if(!string.IsNullOrEmpty(warehouseName)) {
                        sql.Append(" and a.warehouseName= like {0}warehouseName ");
                    }
                    //if(!string.IsNullOrEmpty(partsSupplierCode)) {
                    //    sql.Append(" and a.partsSupplierCode like {0}partsSupplierCode ");
                    //}
                    if(!string.IsNullOrEmpty(partsSupplierName)) {
                        sql.Append(" and a.partsSupplierName like {0}partsSupplierName ");
                    }

                    if(partsPurchaseOrderTypeId.HasValue) {
                        sql.Append(" and a.partsPurchaseOrderTypeId = {0}partsPurchaseOrderTypeId");
                    }
                    if(ifDirectProvision.HasValue) {
                        sql.Append(" and a.ifDirectProvision={0}ifDirectProvision");
                    }
                    if(status.HasValue) {
                        sql.Append(" and a.status={0}status");
                    }
                    if(createTimeBegin.HasValue) {
                        sql.Append(" and a.createTime>=to_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                    }
                    if(createTimeEnd.HasValue) {
                        sql.Append(" and a.createTime<=to_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                    }
                    if(branchId.HasValue) {
                        sql.Append(" and a.branchId={0}branchId");
                    }
                    if(supplierId.HasValue) {
                        sql.Append(" and a.supplierId={0}supplierId");
                    }
                    if(warehouseId.HasValue) {
                        sql.Append(" and a.warehouseId={0}warehouseId");
                    }
                    DbCommand comm = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    //if(partsSalesCategoryId.HasValue) {
                    //    comm.Parameters.Add(db.CreateDbParameter("partsSalesCategoryId", partsSalesCategoryId.Value));
                    //}
                    if(!string.IsNullOrEmpty(warehouseName)) {
                        comm.Parameters.Add(db.CreateDbParameter("warehouseName", "%" + warehouseName + "%"));
                    }
                    if(!string.IsNullOrEmpty(code)) {
                        comm.Parameters.Add(db.CreateDbParameter("code", "%" + code + "%"));
                    }
                    //if(!string.IsNullOrEmpty(partsSupplierCode)) {
                    //    comm.Parameters.Add(db.CreateDbParameter("partsSupplierCode", "%" + partsSupplierCode + "%"));
                    //}
                    if(!string.IsNullOrEmpty(partsSupplierName)) {
                        comm.Parameters.Add(db.CreateDbParameter("partsSupplierName", "%" + partsSupplierName + "%"));
                    }
                    if(partsPurchaseOrderTypeId.HasValue) {
                        sql.Append(" and partsPurchaseOrderTypeId={0}partsPurchaseOrderTypeId");
                    }
                    if(ifDirectProvision != null) {
                        if(ifDirectProvision == true) {
                            comm.Parameters.Add(db.CreateDbParameter("ifDirectProvision", 1));
                        }
                        if(ifDirectProvision == false) {
                            comm.Parameters.Add(db.CreateDbParameter("ifDirectProvision", 0));
                        }
                    }
                    if(status.HasValue) {
                        comm.Parameters.Add(db.CreateDbParameter("status", status.Value));
                    }
                    if(createTimeBegin.HasValue) {
                        var tempValue = createTimeBegin.Value;
                        var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                        comm.Parameters.Add(db.CreateDbParameter("createTimeBegin", tempTime));
                    }
                    if(createTimeEnd.HasValue) {
                        var tempValue = createTimeEnd.Value;
                        var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                        comm.Parameters.Add(db.CreateDbParameter("createTimeEnd", tempTime));
                    }
                    if(branchId.HasValue) {
                        comm.Parameters.Add(db.CreateDbParameter("branchId", branchId.Value));
                    }
                    if(supplierId.HasValue) {
                        comm.Parameters.Add(db.CreateDbParameter("supplierId", supplierId.Value));
                    }
                    if(warehouseId.HasValue) {
                        comm.Parameters.Add(db.CreateDbParameter("warehouseId", warehouseId.Value));
                    }
                    conn.Open();
                    var reader = comm.ExecuteReader();
                    using(var excelExport = new ExcelExport(GetExportFilePath(exportDataFileName))) {
                        excelExport.ExportByRow(index => {
                            if(index == 0)
                                return new object[] {
                                    ErrorStrings.Export_Title_PartsPurchaseOrder_Code, ErrorStrings.Export_Title_Branch_BranchName, ErrorStrings.Export_Title_Partssalescategory_Name, ErrorStrings.Export_Title_Company_WarehouseName, ErrorStrings.Export_Title_FactoryPurchacePrice_SupplierCode, ErrorStrings.Export_Title_FactoryPurchacePrice_SupplierName,ErrorStrings.Export_Title_PartsPurchaseOrder_PlanSource,
                                    ErrorStrings.Export_Title_PartsPurchaseOrder_GPMSPurOrderCode, ErrorStrings.Export_Title_PartsPurchaseOrder_OrderType, ErrorStrings.Export_Title_PartsPurchaseOrder_ReceiveAddress, ErrorStrings.Export_Title_PartsPurchaseOrder_IfDirectProvision, 
                                    ErrorStrings.Export_Title_PartsPurchaseOrder_ReceivingCompanyName, ErrorStrings.Export_Title_InternalAllocationBill_TotalAmount, ErrorStrings.Export_Title_PartsPurchaseOrder_ShippingMethod, ErrorStrings.Export_Title_PartsPurchaseOrder_Status,ErrorStrings.Export_Title_PartsPurchaseOrder_InBoundStatus, ErrorStrings.Export_Title_PartsPurchaseOrder_RequestedDeliveryTime,
                                    ErrorStrings.Export_Title_PartsPurchaseOrder_ConfirmationRemark, ErrorStrings.Export_Title_PartsBranch_Remark, ErrorStrings.Export_Title_AccountPeriod_CreatorName, ErrorStrings.Export_Title_AccountPeriod_CreateTime, ErrorStrings.Export_Title_PartsBranch_Code,
                                    ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName, ErrorStrings.Export_Title_PartsPurchaseOrder_SupplierPartCode, ErrorStrings.Export_Title_PackingPropertyApp_MeasureUnit, ErrorStrings.Export_Title_PartsPurchaseOrder_OrderAmount, ErrorStrings.Export_Title_PartsPurchaseOrder_ConfirmedAmount, ErrorStrings.Export_Title_PartsPurchaseOrder_ShippingAmount, ErrorStrings.Export_Title_InternalAllocationBill_UnitPrice,
                                    ErrorStrings.Export_Title_PartsPurchaseOrder_PriceType, ErrorStrings.Export_Title_PackingPropertyApp_PackingAmount, ErrorStrings.Export_Title_PartsPurchaseOrder_PackingAmount, ErrorStrings.Export_Title_PartsPurchaseOrder_PromisedDeliveryTime, 
                                    ErrorStrings.Export_Title_PartsPurchaseOrder_OrdersRemark, ErrorStrings.Export_Title_PartsPurchaseOrder_DetailsRemark
                                };
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                UpdateScheduleExportState(jobName, exportDataFileName);
                return true;
            } catch(Exception) {
                return false;
            }
        }
    }
}
