﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        public bool ExportPartsPurchasePlanWithDetail(int[] ids, string code, int? PartsSalesCategoryId, int? status, int? PartsPlanTypeId, string WarehouseName, DateTime? createTimeBegin, DateTime? createTimeEnd, DateTime? ApproveTimeBegin, DateTime? ApproveTimeEnd, DateTime? CloseTimeBegin, DateTime? CloseTimeEnd, string sparePartCode, out string filename) {
            filename = GetExportFilePath("配件采购计划单清单_" + ".xlsx");
            if(!HttpContext.Current.User.Identity.IsAuthenticated) {
                throw new Exception(ErrorStrings.File_Validation_User);
            }
            try {
                var db = DbHelper.GetDbHelp(ConnectionString);
                using(var conn = db.CreateDbConnection()) {
                    var sql = new StringBuilder();
                    sql.Append(@"SELECT distinct T.code, /*采购计划编号*/
                                        T.Partssalescategoryname,/*品牌*/
                                        T.BranchCode, /*分公司编号*/
                                        T.BranchName, /*分公司名称*/
                                        T.Warehousename, /*仓库*/
                                        (SELECT VALUE
                                            FROM KEYVALUEITEM
                                            WHERE NAME = 'PurchasePlanOrderStatus'
                                            AND KEY = T.Status) Status, /*单据状态*/
                                        (SELECT PartsPurchaseOrderType.Name
                                            FROM PartsPurchaseOrderType
                                            WHERE PartsPurchaseOrderType.Id=T.PartsPlanTypeId) PartsPlanTypeId, /*计划类型*/
                                        CASE WHEN T.IsTransSap = 1 THEN cast('是' as varchar2(100))  ELSE cast('否' as varchar2(100)) END AS transSap,/*是否传输SAP*/
                                        CASE WHEN T.IsPack = 1 THEN cast('是' as varchar2(100))  ELSE cast('否' as varchar2(100)) END AS transSap,/*是否需要包材*/
                                        T.CreatorName, /*创建人*/
                                        T.CreateTime, /*创建时间*/
                                        T.ModifierName, /*修改人*/
                                        T.ModifyTime, /*修改时间*/
                                        T.ApproverName, /*初审人*/
                                        T.ApproveTime, /*初审时间*/
                                        T.CheckerName, /*审核人*/
                                        T.CheckTime, /*审核时间*/
                                        T.CloserName, /*审批人*/
                                        T.CloseTime, /*审批时间*/
                                        T.Memo, /*备注*/
                                        T1.SupplierPartCode, /*供应商图号*/
                                        T1.SparePartCode, /*配件编号*/
                                        T1.SparePartName, /*配件名称*/
                                        T1.PlanAmount, /*计划量*/
                                        T1.MeasureUnit, /*单位*/
                                        T1.Price, /*采购价*/
                                        T1.ABCStrategy,/*配件ABC分类*/
                                        (select sum(s.PlanAmount * s.Price) from partspurchaseplandetail s where s.PurchasePlanId = T.id ) as FeeSum, /*计划总金额*/
                                        (select sum(s.PlanAmount) from partspurchaseplandetail s where s.PurchasePlanId = T.id ) as PlanAmountSum /*计划总数量*/                                        
                            FROM partspurchaseplan T
                            inner join partspurchaseplandetail T1
                            on T.Id = T1.Purchaseplanid
                            where 1 = 1 ");
                    var tableNames = sql.ToString().GetRegexNames();
                    var dbParameters = new List<DbParameter>();
                    if(ids != null && ids.Length > 0) {
                        sql.Append(@" and T.id in (");
                        for(int i = 0; i < ids.Length; i++) {
                            var tempIdWithNum = "tempIdWithNum" + i;
                            if(i == ids.Length - 1) {
                                var tempParam = "{0}" + tempIdWithNum;
                                sql.Append(tempParam);
                            } else {
                                var tempParam = "{0}" + tempIdWithNum + ",";
                                sql.Append(tempParam);
                            }
                            dbParameters.Add(db.CreateDbParameter(tempIdWithNum, ids[i]));
                        }
                        sql.Append(")");
                    } else {
                        if(!string.IsNullOrEmpty(code)) {
                            sql.Append(" and T.code like {0}code");
                            dbParameters.Add(db.CreateDbParameter("code", code));
                        }
                        if(PartsSalesCategoryId.HasValue) {
                            sql.Append(" and T.PartsSalesCategoryId={0}PartsSalesCategoryId");
                            dbParameters.Add(db.CreateDbParameter("PartsSalesCategoryId", PartsSalesCategoryId));
                        }
                        if(status.HasValue) {
                            sql.Append(" and T.status={0}status");
                            dbParameters.Add(db.CreateDbParameter("status", status));
                        }
                        if(PartsPlanTypeId.HasValue) {
                            sql.Append(" and T.PartsPlanTypeId={0}PartsPlanTypeId");
                            dbParameters.Add(db.CreateDbParameter("PartsPlanTypeId", PartsPlanTypeId));
                        }
                        if(!string.IsNullOrEmpty(WarehouseName)) {
                            sql.Append(" and T.WarehouseName like {0}WarehouseName");
                            dbParameters.Add(db.CreateDbParameter("WarehouseName", code));
                        }
                        if(createTimeBegin.HasValue) {
                            sql.Append(" and T.createTime>=to_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(" and T.createTime<=to_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }

                        if(ApproveTimeBegin.HasValue) {
                            sql.Append(" and T.ApproveTime>=to_date({0}ApproveTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = ApproveTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("ApproveTimeBegin", tempTime.ToString("G")));
                        }
                        if(ApproveTimeEnd.HasValue) {
                            sql.Append(" and T.ApproveTime<=to_date({0}ApproveTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = ApproveTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("ApproveTimeEnd", tempTime.ToString("G")));
                        }
                        if(CloseTimeBegin.HasValue) {
                            sql.Append(" and T.CloseTime>=to_date({0}CloseTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = CloseTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("CloseTimeBegin", tempTime.ToString("G")));
                        }
                        if(CloseTimeBegin.HasValue) {
                            sql.Append(" and T.CloseTime<=to_date({0}CloseTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = CloseTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("CloseTimeBegin", tempTime.ToString("G")));
                        }
                        if(!string.IsNullOrEmpty(sparePartCode)) {
                            sql.Append("and (Exists (Select 1 From partspurchaseplandetail Where sparePartCode like {0}sparePartCode and PurchasePlanId =T.Id))");
                            dbParameters.Add(db.CreateDbParameter("sparePartCode", "%" + sparePartCode + "%"));
                        }
                    }
                    sql.Append(" order by T.code");
                    DbCommand comm = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    comm.Parameters.AddRange(dbParameters.ToArray());
                    conn.Open();
                    var reader = comm.ExecuteReader();
                    using(var excelExport = new ExcelExport(filename)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0)
                                return tableNames;
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }
    }
}
