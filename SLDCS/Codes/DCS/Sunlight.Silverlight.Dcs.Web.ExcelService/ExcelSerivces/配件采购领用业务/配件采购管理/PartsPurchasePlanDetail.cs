﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using System.Data.Common;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        public bool ImportPartsPurchasePlanDetail(string fileName, int PurchasePlanId, int branchId, int partsSalesCategoryId,string partsPurchaseOrderTypeName, out int excelImportNum, out List<PartsPurchasePlanDetailExtend> rightData, out List<PartsPurchasePlanDetailExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<PartsPurchasePlanDetailExtend>();
            var allList = new List<PartsPurchasePlanDetailExtend>();
            List<PartsPurchasePlanDetailExtend> rightList = null;
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("PartsPurchasePlanDetail", out notNullableFields, out fieldLenght);

                List<object> excelColumns;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    #region 指定文件中的列对应的名称 例如
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Code, "SparePartCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName, "SparePartName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsPurchasePlanDetail_PlanAmount, "PlanAmount");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsPurchasePlanDetail_MeasureUnit, "MeasureUnit");
                    #endregion

                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var tempImportObj = new PartsPurchasePlanDetailExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        #region 根据导入的字段给对应的实体属性赋值
                        tempImportObj.SparePartCodeStr = newRow["SparePartCode"];
                        tempImportObj.SparePartNameStr = newRow["SparePartName"];
                        tempImportObj.PlanAmountStr = newRow["PlanAmount"];
                        tempImportObj.MeasureUnitStr = newRow["MeasureUnit"];
                        #endregion

                        var tempErrorMessage = new List<string>();

                        #region 导入的数据基本检查
                        //配件图号检查
                        var fieldIndex = notNullableFields.IndexOf("SparePartCode".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.SparePartCodeStr)) {
                            if(fieldIndex > -1) {
                                tempErrorMessage.Add(ErrorStrings.OverstockPartsAppDetail__Validation1);
                            }
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.SparePartCodeStr) > fieldLenght["SparePartCode".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.ImpPartsSupplierRelation_Validation8);
                        }
                        //配件名称检查
                        if(!string.IsNullOrEmpty(tempImportObj.SparePartNameStr)) {
                            if(Encoding.Default.GetByteCount(tempImportObj.SparePartNameStr) > fieldLenght["SparePartName".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Sparepart_NameIsLong);
                        }

                        //计划量检查
                        fieldIndex = notNullableFields.IndexOf("PlanAmount".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.PlanAmountStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsPurchasePlanDetail_PlanAmountIsNull);
                        } else {
                            int checkValue;
                            if(int.TryParse(tempImportObj.PlanAmountStr, out checkValue)) {
                                if(checkValue <= 0) {
                                    tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsPurchasePlanDetail_PlanAmountOverZero);
                                } else {
                                    tempImportObj.PlanAmount = checkValue;
                                }

                            } else {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsPurchasePlanDetail_PlanAmountNumber);
                            }
                        }
                        #endregion

                        if(tempErrorMessage.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });
                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    #region 剩下的数据进行业务检查
                    //文件内数据重复
                    var groups = tempRightList.GroupBy(r => new {
                        r.SparePartCodeStr
                    }).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        groupItem.ErrorMsg = ErrorStrings.Export_Validation_PartsPurchaseOrder_Validation2;
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    //1、配件有效性校验（配件信息.配件编号=导入数据.配件图号），
                    //如果校验不通过，记录校验错误信息：配件不存在。
                    var sparePartCodesNeedCheck = tempRightList.Select(r => r.SparePartCodeStr).Distinct().ToArray();
                    var dbSpareParts = new List<SparePartExtend>();
                    Func<string[], bool> getDbSpareParts = value => {
                        var dbObj = new SparePartExtend {
                            Id = Convert.ToInt32(value[0]),
                            Code = value[1],
                            Name = value[2],
                            MeasureUnit = value[3],
                            SupplierPartCode = value[4],
                            PurchasePrice = Convert.ToDecimal(value[5]),
                            LimitQty = string.IsNullOrEmpty(value[6]) ? 0 : Convert.ToInt32(value[6]),
                            UsedQty = string.IsNullOrEmpty(value[7]) ? 0 : Convert.ToInt32(value[7]),
                            PartABC = string.IsNullOrEmpty(value[8]) ? 0 : Convert.ToInt32(value[8]),
                        };
                        dbSpareParts.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator(string.Format(@"select Id, Code, Name, MeasureUnit, supplierpartcode, purchaseprice,limitQty,usedQty,PartABC
                                                                      from(select Part.Id,
                                                                                   Part.Code,
                                                                                   Part.Name,
                                                                                   Part.MeasureUnit,
                                                                                   relation.supplierpartcode,
                                                                                   p.purchaseprice,
                                                                                   Part.status,
                                                                                   p.validfrom,
                                                                                   p.validto,
                                                                                   p.limitQty,
                                                                                   p.usedQty ,
                                                                                   pb.PartABC
                                                                              from SparePart Part
                                                                             inner join PartsPurchasePricing p
                                                                               on Part.id = p.partid
                                                                              and p.status = 2
                                                                             inner join PartsSupplierRelation relation
                                                                                on relation.partid = Part.id
                                                                               and relation.supplierid = p.partssupplierid
                                                                               and relation.status = 1
                                                                               and relation.isprimary = 1
                                                                             inner join PartsSupplier ps
                                                                                on ps.id = relation.supplierid
                                                                               and ps.status = 1
                                                                             inner join PartsBranch pb on  Part.id =pb.PartId and pb.status=1
                                                                             where Part.status = 1) temp
                                                                     where validfrom<=to_date('{0}','yyyy-mm-dd hh24:mi:ss') and validto>=to_date('{1}','yyyy-mm-dd hh24:mi:ss') ", DateTime.Now, DateTime.Now), "Code", true, sparePartCodesNeedCheck, getDbSpareParts);
                    var errorSpareParts = tempRightList.Where(r => dbSpareParts.All(v => v.Code != r.SparePartCodeStr)).ToList();
                    foreach(var errorItem in errorSpareParts) {
                        errorItem.ErrorMsg = ErrorStrings.Export_Validation_PartsPurchasePlan_Validation1;
                    }

                    //2
                    //校验计划量必须是 配件主包装属性.系数的整数倍
                    var dbPartsBranchPackingProps = new List<PartsBranchPackingPropExtend>();
                    Func<string[], bool> getDbPartsBranchPackingProps = value => {
                        var dbObj = new PartsBranchPackingPropExtend {
                            Id = Convert.ToInt32(value[0]),
                            PartsBranchId = Convert.ToInt32(value[1]),
                            PackingType = Convert.ToInt32(value[2]),
                            MeasureUnit = value[3],
                            PackingCoefficient = Convert.ToInt32(value[4]),
                            SparePartId = Convert.ToInt32(value[5]),
                            IsOrderable = Convert.ToBoolean(Convert.ToInt32(value[7]))
                        };
                        dbPartsBranchPackingProps.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator(string.Format(@"select pbpp.Id, pbpp.PartsBranchId, pbpp.PackingType, pbpp.MeasureUnit, pbpp.PackingCoefficient, pbpp.SparePartId,pb.PartCode,pb.IsOrderable from PartsBranchPackingProp pbpp
                                                               inner join PartsBranch pb on pb.id=pbpp.partsBranchId and pbpp.sparePartId=pb.partId and pb.MainPackingType=pbpp.PackingType
                                                               where pb.branchId ={0} and pb.PartsSalesCategoryId ={1} and pb.Status = 1 ", branchId, partsSalesCategoryId), "PartCode", true, sparePartCodesNeedCheck, getDbPartsBranchPackingProps);

                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    foreach(var rightItem in tempRightList) {
                        var tempSparePart = dbSpareParts.Single(r => r.Code == rightItem.SparePartCodeStr);
                        rightItem.PurchasePlanId = PurchasePlanId;
                        rightItem.SparePartId = tempSparePart.Id;
                        rightItem.SparePartCode = tempSparePart.Code;
                        rightItem.SparePartName = tempSparePart.Name;
                        rightItem.MeasureUnit = tempSparePart.MeasureUnit;
                        rightItem.SupplierPartCode = tempSparePart.SupplierPartCode;
                        rightItem.Price = tempSparePart.PurchasePrice;
                        rightItem.LimitQty = tempSparePart.LimitQty;
                        rightItem.UsedQty = tempSparePart.UsedQty;
                        rightItem.PartABC = tempSparePart.PartABC;
                        var partsBranchPackingProp = dbPartsBranchPackingProps.Where(r => r.SparePartId == tempSparePart.Id).FirstOrDefault();
                        if(null == partsBranchPackingProp) {
                            rightItem.ErrorMsg = string.Format(ErrorStrings.Export_Validation_PartsPurchasePlan_Validation2, rightItem.SparePartCode);
                            continue;
                        }
                        if((partsBranchPackingProp.IsOrderable ?? false) == false) {
                            rightItem.ErrorMsg = ErrorStrings.Export_Validation_PartsPurchaseOrder_Validation11;
                        }
                        if (partsPurchaseOrderTypeName.IndexOf("急") < 0 && partsPurchaseOrderTypeName != "中心库采购") {
                            if (Convert.ToInt32(rightItem.PlanAmountStr) % partsBranchPackingProp.PackingCoefficient > 0) {
                                rightItem.ErrorMsg = string.Format(ErrorStrings.Export_Validation_PartsPurchasePlan_Validation3, partsBranchPackingProp.PackingCoefficient);
                            }
                        }
                        
                    }

                    if (partsPurchaseOrderTypeName.IndexOf("急") < 0 && partsPurchaseOrderTypeName != "中心库采购") {
                        //3
                        //校验计划量必须大于 配件首选供应商的最小采购批量
                        var dbPartsSupplierRelations = new List<PartsSupplierRelationExtend>();
                        Func<string[], bool> getDbPartsSupplierRelations = value => {
                            var dbObj = new PartsSupplierRelationExtend {
                                Id = Convert.ToInt32(value[0]),
                                PartId = Convert.ToInt32(value[1]),
                                MinBatch = Convert.ToInt32(value[2])
                            };
                            dbPartsSupplierRelations.Add(dbObj);
                            return false;
                        };
                        db.QueryDataWithInOperator(@"select a.id,a.partid,nvl(a.MinBatch,1) from PartsSupplierRelation a inner join sparepart b on a.partid = b.id where  a.IsPrimary = 1 and a.status = 1  ", "Code", true, sparePartCodesNeedCheck, getDbPartsSupplierRelations);

                        tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                        foreach (var rightItem in tempRightList) {
                            var partsSupplierRelation = dbPartsSupplierRelations.FirstOrDefault(r => r.PartId == rightItem.SparePartId);
                            if (partsSupplierRelation != null && partsSupplierRelation.MinBatch != null && partsSupplierRelation.MinBatch > 0 && rightItem.PlanAmount < partsSupplierRelation.MinBatch) {
                                rightItem.ErrorMsg = string.Format(ErrorStrings.Export_Validation_PartsPurchasePlan_Validation4, partsSupplierRelation.MinBatch);
                            }
                        }
                    }
                    
                    //获取所有不合格数据
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    //获取所有合格数据
                    rightList = allList.Except(errorList).ToList();
                    #endregion
                }
                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempObj = list[index - 1];
                            var values = new object[] {
                                #region 设置错误信息导出的列的值
                                tempObj.SparePartCodeStr, tempObj.SparePartNameStr,
                                 tempObj.PlanAmountStr,
                                tempObj.MeasureUnitStr, tempObj.ErrorMsg
                                
                                #endregion
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }
    }
}
