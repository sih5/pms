﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        public bool ExportPartsPurchasePlan_HW(int[] ids, string code, int? partsSalesCategoryId, int? partsPurchaseOrderTypeId, int? status, string SAPPurchasePlanCode, DateTime? requestedDeliveryTimeBegin, DateTime? requestedDeliveryTimeEnd, DateTime? createTimeBegin, DateTime? createTimeEnd, DateTime? modifyTimeBegin, DateTime? modifyTimeEnd, DateTime? stopTimeBegin, DateTime? stopTimeEnd, out string filename) {
            filename = GetExportFilePath("海外采购计划单_" + ".xlsx");
            if (!HttpContext.Current.User.Identity.IsAuthenticated) {
                throw new Exception(ErrorStrings.File_Validation_User);
            }
            try {
                var db = DbHelper.GetDbHelp(ConnectionString);
                using (var conn = db.CreateDbConnection()) {
                    var sql = new StringBuilder();
                    sql.Append(@"SELECT T.code,/*海外采购计划编号*/
                                        T.Partssalescategoryname,/*品牌*/
                                        (SELECT VALUE FROM keyvalueitem WHERE NAME='PurchasePlanType' AND KEY=T.PURCHASEPLANTYPE) TYPE,/*订单类型*/
                                        T.Totalamount,/*总金额*/
                                        (SELECT VALUE FROM KEYVALUEITEM WHERE NAME='PurchasePlanStatus' AND KEY=T.Status) Status,/*状态*/
                                        T.SAPPurchasePlanCode,/*SAP采购计划单号*/
                                        T.Remark,/*备注*/
                                        T.Creatorname,/*创建人*/
                                        T.Createtime,/*创建时间*/
                                        T.Modifiername,/*修改人*/
                                        T.Modifytime,/*修改时间*/
                                        T.Stoporname,/*终止人*/
                                        T.Stoptime/*终止时间*/
                                 FROM partspurchaseplan_hw T where 1=1 ");
                    var tableNames = sql.ToString().GetRegexNames();
                    var dbParameters = new List<DbParameter>();
                    if (ids != null && ids.Length > 0) {
                        sql.Append(@" and T.id in (");
                        for(int i=0;i<ids.Length;i++){
                            var tempIdWithNum = "tempIdWithNum" + i;
                            if(i==ids.Length-1){
                                var tempParam = "{0}" + tempIdWithNum;
                                sql.Append(tempParam);
                            } else {
                                var tempParam = "{0}" + tempIdWithNum + ",";
                                sql.Append(tempParam);
                            }
                            dbParameters.Add(db.CreateDbParameter(tempIdWithNum, ids[i]));
                        }
                        sql.Append(")");
                    } else {
                        if (!string.IsNullOrEmpty(code)) {
                            sql.Append(" and T.code like {0}code");
                            dbParameters.Add(db.CreateDbParameter("code", code));
                        }
                        if (partsSalesCategoryId.HasValue) {
                            sql.Append(" and T.partsSalesCategoryId={0}partsSalesCategoryId");
                            dbParameters.Add(db.CreateDbParameter("partsSalesCategoryId", partsSalesCategoryId));
                        }
                        if (partsPurchaseOrderTypeId.HasValue) {
                            sql.Append(" and T.partsPurchaseOrderTypeId={0}partsPurchaseOrderTypeId");
                            dbParameters.Add(db.CreateDbParameter("partsPurchaseOrderTypeId", partsPurchaseOrderTypeId));
                        }
                        if (status.HasValue) {
                            sql.Append(" and T.status={0}status");
                            dbParameters.Add(db.CreateDbParameter("status", status));
                        }
                        if (!string.IsNullOrEmpty(SAPPurchasePlanCode)) {
                            sql.Append(" and T.SAPPurchasePlanCode={0}SAPPurchasePlanCode");
                            dbParameters.Add(db.CreateDbParameter("SAPPurchasePlanCode", SAPPurchasePlanCode));
                        }
                        if (requestedDeliveryTimeBegin.HasValue) {
                            sql.Append(" and T.requestedDeliveryTime>=to_date({0}requestedDeliveryTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = requestedDeliveryTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("requestedDeliveryTimeBegin", tempTime.ToString("G")));
                        }
                        if (requestedDeliveryTimeEnd.HasValue) {
                            sql.Append(" and T.requestedDeliveryTime<=to_date({0}requestedDeliveryTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = requestedDeliveryTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("requestedDeliveryTimeEnd", tempTime.ToString("G")));
                        }
                        if (createTimeBegin.HasValue) {
                            sql.Append(" and T.createTime>=to_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if (createTimeEnd.HasValue) {
                            sql.Append(" and T.createTime<=to_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                        if (modifyTimeBegin.HasValue) {
                            sql.Append(" and T.modifyTime>=to_date({0}modifyTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = modifyTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("modifyTimeBegin", tempTime.ToString("G")));
                        }
                        if (modifyTimeEnd.HasValue) {
                            sql.Append(" and T.modifyTime<=to_date({0}modifyTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = modifyTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("modifyTimeEnd", tempTime.ToString("G")));
                        }
                        if (stopTimeBegin.HasValue) {
                            sql.Append(" and T.stopTime>=to_date({0}stopTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = stopTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("stopTimeBegin", tempTime.ToString("G")));
                        }
                        if (modifyTimeEnd.HasValue) {
                            sql.Append(" and T.stopTime<=to_date({0}stopTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = stopTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("stopTimeEnd", tempTime.ToString("G")));
                        }
                    }
                    DbCommand comm = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    comm.Parameters.AddRange(dbParameters.ToArray());
                    conn.Open();
                    var reader = comm.ExecuteReader();
                    using (var excelExport = new ExcelExport(filename)) {
                        excelExport.ExportByRow(index => {
                            if (index == 0)
                                return tableNames;
                            if (reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if (num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch (Exception) {
                return false;
            }
        }

        public bool ExportPartsPurchasePlan_HWWithDetail(int[] ids, string code, int? partsSalesCategoryId, int? partsPurchaseOrderTypeId, int? status, string SAPPurchasePlanCode, DateTime? requestedDeliveryTimeBegin, DateTime? requestedDeliveryTimeEnd, DateTime? createTimeBegin, DateTime? createTimeEnd, DateTime? modifyTimeBegin, DateTime? modifyTimeEnd, DateTime? stopTimeBegin, DateTime? stopTimeEnd, out string filename) {
            filename = GetExportFilePath("海外采购计划单清单_" + ".xlsx");
            if (!HttpContext.Current.User.Identity.IsAuthenticated) {
                throw new Exception(ErrorStrings.File_Validation_User);
            }
            try {
                var db = DbHelper.GetDbHelp(ConnectionString);
                using (var conn = db.CreateDbConnection()) {
                    var sql = new StringBuilder();
                    sql.Append(@"SELECT distinct T.code,/*海外采购计划编号*/
                                        T1.Warehousename,/*仓库名称*/
                                        T.Partssalescategoryname,/*品牌*/
                                        T1.Partssuppliercode,/*供应商编号*/
                                        T1.Partssuppliername,/*供应商名称*/
                                        T1.SuplierCode,/*供应商业务编号*/
                                        (SELECT VALUE FROM keyvalueitem WHERE NAME='PurchasePlanType' AND KEY=T.PURCHASEPLANTYPE) TYPE,/*订单类型*/
                                        T.Totalamount,/*总金额*/
                                        (SELECT VALUE FROM KEYVALUEITEM WHERE NAME='PurchasePlanStatus' AND KEY=T.Status) Status,/*状态*/
                                        T.SAPPurchasePlanCode,/*SAP采购计划单号*/
                                        T.Remark,/*备注*/
                                        T1.Partcode,/*配件图号*/
                                        T1.Partname,/*配件名称*/
                                        T1.Gpmspartcode,/*海外图号*/
                                        T1.PARTSPURCHASEPLANQTY,/*采购计划数量*/
                                        T1.Untfulfilledqty as Untfulfilledqty,/*未满足量*/
                                        (nvl(T1.EndAmount,0)) as EndAmount,/*终止量*/
                                        ((nvl(T1.Untfulfilledqty,0)-nvl(T1.EndAmount,0))),/*可分解量*/
                                        T1.Unitprice,/*单价*/
                                        T1.Measureunit,/*计量单位*/
                                        T1.Gpmsrownum,/*海外行项目号*/
                                        T1.POCode,/*PO单号*/
                                        T1.OverseasSuplierCode,/*实际供应商业务编码*/
                                        T1.Faultreason,/*异常原因*/
                                        T1.Remark/*备注*/
                                 FROM partspurchaseplan_hw T
                                 inner join partspurchaseplandetail_hw T1 
                                 on T.Id=T1.PARTSPURCHASEPLANID where 1=1 
                              ");
                    var tableNames = sql.ToString().GetRegexNames();
                    var dbParameters = new List<DbParameter>();
                    if (ids != null && ids.Length > 0) {
                        sql.Append(@" and T.id in (");
                        for (int i = 0; i < ids.Length; i++) {
                            var tempIdWithNum = "tempIdWithNum" + i;
                            if (i == ids.Length - 1) {
                                var tempParam = "{0}" + tempIdWithNum;
                                sql.Append(tempParam);
                            } else {
                                var tempParam = "{0}" + tempIdWithNum + ",";
                                sql.Append(tempParam);
                            }
                            dbParameters.Add(db.CreateDbParameter(tempIdWithNum, ids[i]));
                        }
                        sql.Append(")");
                    } else {
                        if (!string.IsNullOrEmpty(code)) {
                            sql.Append(" and T.code like {0}code");
                            dbParameters.Add(db.CreateDbParameter("code", code));
                        }
                        if (partsSalesCategoryId.HasValue) {
                            sql.Append(" and T.partsSalesCategoryId={0}partsSalesCategoryId");
                            dbParameters.Add(db.CreateDbParameter("partsSalesCategoryId", partsSalesCategoryId));
                        }
                        if (partsPurchaseOrderTypeId.HasValue) {
                            sql.Append(" and T.partsPurchaseOrderTypeId={0}partsPurchaseOrderTypeId");
                            dbParameters.Add(db.CreateDbParameter("partsPurchaseOrderTypeId", partsPurchaseOrderTypeId));
                        }
                        if (status.HasValue) {
                            sql.Append(" and T.status={0}status");
                            dbParameters.Add(db.CreateDbParameter("status", status));
                        }
                        if (!string.IsNullOrEmpty(SAPPurchasePlanCode)) {
                            sql.Append(" and T.SAPPurchasePlanCode={0}SAPPurchasePlanCode");
                            dbParameters.Add(db.CreateDbParameter("SAPPurchasePlanCode", SAPPurchasePlanCode));
                        }
                        if (requestedDeliveryTimeBegin.HasValue) {
                            sql.Append(" and T.requestedDeliveryTime>=to_date({0}requestedDeliveryTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = requestedDeliveryTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("requestedDeliveryTimeBegin", tempTime.ToString("G")));
                        }
                        if (requestedDeliveryTimeEnd.HasValue) {
                            sql.Append(" and T.requestedDeliveryTime<=to_date({0}requestedDeliveryTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = requestedDeliveryTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("requestedDeliveryTimeEnd", tempTime.ToString("G")));
                        }
                        if (createTimeBegin.HasValue) {
                            sql.Append(" and T.createTime>=to_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if (createTimeEnd.HasValue) {
                            sql.Append(" and T.createTime<=to_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                        if (modifyTimeBegin.HasValue) {
                            sql.Append(" and T.modifyTime>=to_date({0}modifyTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = modifyTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("modifyTimeBegin", tempTime.ToString("G")));
                        }
                        if (modifyTimeEnd.HasValue) {
                            sql.Append(" and T.modifyTime<=to_date({0}modifyTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = modifyTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("modifyTimeEnd", tempTime.ToString("G")));
                        }
                        if (stopTimeBegin.HasValue) {
                            sql.Append(" and T.stopTime>=to_date({0}stopTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = stopTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("stopTimeBegin", tempTime.ToString("G")));
                        }
                        if (modifyTimeEnd.HasValue) {
                            sql.Append(" and T.stopTime<=to_date({0}stopTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = stopTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("stopTimeEnd", tempTime.ToString("G")));
                        }
                    }
                    sql.Append(" order by T.code");
                    DbCommand comm = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    comm.Parameters.AddRange(dbParameters.ToArray());
                    conn.Open();
                    var reader = comm.ExecuteReader();
                    using (var excelExport = new ExcelExport(filename)) {
                        excelExport.ExportByRow(index => {
                            if (index == 0)
                                return tableNames;
                            if (reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if (num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch (Exception) {
                return false;
            }
        }
    }
}
