﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;


namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        public bool ExportSupplierPreApprovedLoanWithDetail(int[] ids, string preApprovedCode, int? partsSalesCategoryId, DateTime? createTimeBegin, DateTime? createTimeEnd, out string filename) {
            filename = GetExportFilePath("供应商货款预批单清单_" + ".xlsx");
            if(!HttpContext.Current.User.Identity.IsAuthenticated) {
                throw new Exception(ErrorStrings.File_Validation_User);
            }
            try {
                var db = DbHelper.GetDbHelp(ConnectionString);
                using(var conn = db.CreateDbConnection()) {
                    var sql = new StringBuilder();
                    sql.Append(@"
                                select a.preapprovedcode, --货款预批单编号
                                       b.SupplierCode, --MDM供应商编码
                                       b.suppliercompanycode, --供应商企业编号
                                       b.suppliercompanyname, --供应商企业名称
                                       b.BankName, --开户银行
                                       b.BankCode, --开户账号
                                       b.PlannedAmountOwed, --计划欠款(元)
                                       b.ActualDebt, --实际欠款(元)
                                       b.ExceedAmount, --超出金额(万元)
                                       b.ApprovalAmount,--报批金额(万元)
                                       b.ActualPaidAmount, --实付金额(万元)
                                       (select value from keyvalueitem where NAME = 'IsOrNot'and key=b.IsOverstock) As IsOverstock,--是否有积压
                                       b.Remark --备注
                                  from SupplierPreApprovedLoan a
                                 inner join SupplierPreAppLoanDetail b
                                    on a.id = b.SupplierPreApprovedLoanId
                                 where a.status=1");

                    var dbParameters = new List<DbParameter>();
                    if(ids == null || ids.Length == 0) {
                        if(partsSalesCategoryId.HasValue) {
                            sql.Append(" and a.partsSalesCategoryId={0}partsSalesCategoryId");
                            dbParameters.Add(db.CreateDbParameter("partsSalesCategoryId", partsSalesCategoryId));
                        }
                        if(!string.IsNullOrEmpty(preApprovedCode)) {
                            sql.Append(" and a.preApprovedCode like {0}preApprovedCode ");
                            dbParameters.Add(db.CreateDbParameter("preApprovedCode", "%" + preApprovedCode + "%"));
                        }
                        if(createTimeBegin.HasValue) {
                            sql.Append(" and a.createTime>=to_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(" and a.createTime<=to_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                    } else {
                        var stringOfIds = string.Join(",", ids);
                        sql.Append(string.Format(" and a.id in({0})", stringOfIds));
                    }
                    DbCommand comm = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    comm.Parameters.AddRange(dbParameters.ToArray());
                    conn.Open();
                    var reader = comm.ExecuteReader();
                    using(var excelExport = new ExcelExport(filename)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0)
                                return new object[] {
                                    "货款预批单编号",ErrorStrings.Export_Title_AccountPay_SupplierCode, ErrorStrings.Export_Title_AccountPay_SupplierCompanyCode, ErrorStrings.Export_Title_AccountPay_SupplierName,"开户银行", "开户账号","计划欠款(元)", "实际欠款(元)", "超出金额(万元)", "报批金额(万元)","实付金额(万元)", "是否有积压",ErrorStrings.Export_Title_PartsBranch_Remark
                                };
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }
    }
}
