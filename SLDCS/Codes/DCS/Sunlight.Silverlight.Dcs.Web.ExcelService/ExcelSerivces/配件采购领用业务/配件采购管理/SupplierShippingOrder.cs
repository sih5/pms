﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        /// <summary>
        /// 合并导出供应商发运单及清单
        /// </summary>
        public bool ExportSupplierShippingOrderWithDetail(int[] ids, int? branchId, string partsSupplierCode, string partsSupplierName, int? partsSalesCategoryId, int? receivingWarehouseId, string code, string partsPurchaseOrderCode, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName) {
            fileName = GetExportFilePath("合并导出供应商发运单及清单.xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {
                    var sql = new StringBuilder();
                    #region SQL
                    sql.Append(@" select    a.code,
                                            a.branchname,
                                            a.PartsSalesCategoryName as partssalescategoryname,
                                            e.name as receivewarehousename,
                                            c.code as suppliercode,
                                            c.name as suppliername,
                                            a.partspurchaseordercode,
                                             (select sum(t.Quantity*pp.RetailGuidePrice) from suppliershippingdetail t left join PartsRetailGuidePrice pp on  t.SparePartId=pp.SparePartId and pp.status=1 where  a.PartsSalesCategoryId=pp.PartsSalesCategoryId and t.SupplierShippingOrderId=a.id ),
                                            a.plansource,
                                            a.receivingaddress,
                                            a.receivingcompanyname,
                                            (select value from keyvalueitem where NAME = 'IsOrNot'and key=a.ifdirectprovision) As ifdirectprovision,
                                            a.OriginalRequirementBillCode,
                                            (select value from keyvalueitem where NAME = 'IsOrNot'and key=a.directprovisionfinished) As directprovisionfinished,
                                            a.deliverybillnumber,
                                            a.requesteddeliverytime,
                                            a.plandeliverytime,
                                            a.shippingdate,
                                            a.arrivaldate,
                                            a.LogisticArrivalDate,
                                            (select value from keyvalueitem where NAME = 'PartsShipping_Method'and key=a.shippingmethod) As shippingmethod,       
                                            a.logisticcompany,
                                            a.driver,
                                            a.phone,
                                            a.vehiclelicenseplate,
                                            (select value from keyvalueitem where NAME = 'SupplierShippingOrder_Status'and key=a.status) As status,
                                            a.remark,
                                            a.ERPSourceOrderCode,
                                            a.creatorname,
                                            a.createtime,a.ModifyArrive,a.InboundTime,a.Approver,a.ApproveDate,
                                            (select sum(Weight) from suppliershippingdetail t  where   t.SupplierShippingOrderId=a.id ),
                                            (select sum(Volume) from suppliershippingdetail t  where   t.SupplierShippingOrderId=a.id ),
                                            b.serialnumber,
                                            b.sparepartcode,
                                            b.sparepartname,
                                            b.supplierpartcode,
                                            b.measureunit,
                                            g.OrderAmount as planAmount,
                                            g.OrderAmount - (CASE WHEN g.ShippingAmount IS NULL THEN 0 ELSE g.ShippingAmount END) AS waitShipping,
                                            --b.POCode,
                                            b.quantity,
                                            b.confirmedamount,
                                            b.remark as detailRemark,
                                            b.SpareOrderRemark,b.Weight,b.Volume
                                        from SupplierShippingOrder a
                                        Inner join suppliershippingdetail b
                                        on a.id = b.suppliershippingorderid
                                         inner join PartsPurchaseOrder f on f.id = a.partspurchaseorderid
                                         left join PartsPurchaseOrderDetail g on f.id = g.partspurchaseorderid and g.sparepartid = b.sparepartid
                                        inner join company c
                                        on a.partssupplierid = c.id
                                        and c.status = 1
                                        inner join partssalescategory d
                                        on a.partssalescategoryid = d.id
                                        and d.status = 1
                                        inner join warehouse e
                                        on a.receivingwarehouseid = e.id
                                        and e.status = 1 
                                        
                                        where 1=1 ");
                    #endregion

                    var dbParameters = new List<DbParameter>();
                    #region 条件过滤
                    if(ids != null && ids.Length > 0) {
                        sql.Append(" and a.id in (");
                        for(var i = 0; i < ids.Length; i++) {
                            if(ids.Length == i + 1) {
                                sql.Append("{0}id" + i);
                                dbParameters.Add(db.CreateDbParameter("id" + i, ids[i]));
                            } else {
                                sql.Append("{0}id" + i + ",");
                                dbParameters.Add(db.CreateDbParameter("id" + i, ids[i]));
                            }
                        }
                        sql.Append(")");
                    } else {
                        if(branchId.HasValue) {
                            sql.Append(" and a.branchId={0}branchId");
                            dbParameters.Add(db.CreateDbParameter("branchId", branchId.Value));
                        }
                        if(!string.IsNullOrEmpty(partsSupplierCode)) {
                            sql.Append(" and a.partsSupplierCode like {0}partsSupplierCode ");
                            dbParameters.Add(db.CreateDbParameter("partsSupplierCode", "%" + partsSupplierCode + "%"));
                        }
                        if(!string.IsNullOrEmpty(partsSupplierName)) {
                            sql.Append(" and a.partsSupplierName like {0}partsSupplierName ");
                            dbParameters.Add(db.CreateDbParameter("partsSupplierName", "%" + partsSupplierName + "%"));
                        }
                        if(partsSalesCategoryId.HasValue) {
                            sql.Append(" and a.partsSalesCategoryId={0}partsSalesCategoryId");
                            dbParameters.Add(db.CreateDbParameter("partsSalesCategoryId", partsSalesCategoryId.Value));
                        }
                        if(receivingWarehouseId.HasValue) {
                            sql.Append(" and a.receivingWarehouseId={0}receivingWarehouseId");
                            dbParameters.Add(db.CreateDbParameter("receivingWarehouseId", receivingWarehouseId.Value));
                        }
                        if(!string.IsNullOrEmpty(code)) {
                            sql.Append(" and a.code like {0}code ");
                            dbParameters.Add(db.CreateDbParameter("code", "%" + code + "%"));
                        }
                        if(!string.IsNullOrEmpty(partsPurchaseOrderCode)) {
                            sql.Append(" and a.partsPurchaseOrderCode = {0}partsPurchaseOrderCode ");
                            dbParameters.Add(db.CreateDbParameter("partsPurchaseOrderCode", partsPurchaseOrderCode));
                        }
                        if(status.HasValue) {
                            sql.Append(" and a.status={0}status");
                            dbParameters.Add(db.CreateDbParameter("status", status.Value));
                        }
                        if(createTimeBegin.HasValue) {
                            sql.Append(@" and a.createTime >=to_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(@" and a.createTime <=to_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }

                    }
                    #endregion
                    var sqlStr = sql.ToString();
                    if(sqlStr.IndexOf(" where 1=1  and ") > 0)
                        sqlStr = sqlStr.Replace(" where 1=1  and ", " where ");
                    else
                        sqlStr = sqlStr.Replace(" where 1=1 ", "");

                    var command = db.CreateDbCommand(string.Format(sqlStr, db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    conn.Open();
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    //列名
                                    ErrorStrings.Export_Title_SupplierShippingOrder_Code,ErrorStrings.Export_Title_Branch_BranchName,ErrorStrings.Export_Title_Partssalescategory_Name,ErrorStrings.Export_Title_SupplierShippingOrder_ReveiveWarehouseName,ErrorStrings.Export_Title_FactoryPurchacePrice_SupplierCode,ErrorStrings.Export_Title_FactoryPurchacePrice_SupplierName,ErrorStrings.Export_Title_PartsPurchaseOrder_Code,ErrorStrings.Export_Title_SupplierShippingOrder_ShippingAmount,ErrorStrings.Export_Title_PartsPurchaseOrder_PlanSource,
                                    ErrorStrings.Export_Title_PartsPurchaseOrder_ReceiveAddress,ErrorStrings.Export_Title_SupplierShippingOrder_ReveiveCompany,ErrorStrings.Export_Title_SupplierShippingOrder_Ifdirectprovision,ErrorStrings.Export_Title_SupplierShippingOrder_OriginalRequirementBillCode,ErrorStrings.Export_Title_SupplierShippingOrder_Directprovisionfinished,ErrorStrings.Export_Title_SupplierShippingOrder_DdeliverybillNumber,ErrorStrings.Export_Title_PartsPurchaseOrder_RequestedDeliveryTime,ErrorStrings.Export_Title_SupplierShippingOrder_PlandeliveryTime,
                                    ErrorStrings.Export_Title_SupplierShippingOrder_ShippingDate,ErrorStrings.Export_Title_SupplierShippingOrder_SrrivalDate,ErrorStrings.Export_Title_SupplierShippingOrder_LogisticArrivalDate,ErrorStrings.Export_Title_PartsPurchaseOrder_ShippingMethod,ErrorStrings.Export_Title_SupplierShippingOrder_LogisticCompany,ErrorStrings.Export_Title_SupplierShippingOrder_Driver,ErrorStrings.Export_Title_SupplierShippingOrder_DriverPhone,ErrorStrings.Export_Title_SupplierShippingOrder_VehicleLicensePlate,ErrorStrings.Export_Title_AccountPeriod_Status,ErrorStrings.Export_Title_PartsBranch_Remark,ErrorStrings.Export_Title_SupplierShippingOrder_ERPSourceOrderCode,ErrorStrings.Export_Title_AccountPeriod_CreatorName,ErrorStrings.Export_Title_AccountPeriod_CreateTime,
                                    "修改承运商送达时间","入库时间","审核人","审核时间","总重量","总体积",ErrorStrings.Export_Title_InternalAllocationBill_SerialNumber,ErrorStrings.Export_Title_PartsBranch_Code,ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName,ErrorStrings.Export_Title_PartsPurchaseOrder_SupplierPartCode,ErrorStrings.Export_Title_PackingPropertyApp_MeasureUnit,ErrorStrings.Export_Title_PartsPurchasePlanDetail_PlanAmount,ErrorStrings.Export_Title_SupplierShippingOrder_WaitShipping,ErrorStrings.Export_Title_PartsPurchaseOrder_ShippingAmount,ErrorStrings.Export_Title_PartsPurchaseOrder_ConfirmedAmount,ErrorStrings.Export_Title_PartsBranch_Remark,ErrorStrings.Export_Title_SupplierShippingOrder_DetailRemark,"重量","体积"
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }

        /// <summary>
        /// 合并导出供应商直供发运单及清单
        /// </summary>
        public bool ExportSupplierShippingOrderForDirectWithDetail(int[] ids, int? branchId, string partsSupplierCode, string partsSupplierName, string code, int? partsSalesCategoryId, string partsSaleOrderCode, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName) {
            fileName = GetExportFilePath("合并导出供应商直供发运单及清单.xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {
                    var sql = new StringBuilder();
                    #region SQL
                    sql.Append(@"select a.code,
                                        a.branchname,
                                        d.name as partssalescategoryname,
                                        e.name as receivewarehousename,
                                        f.Code as PartsSalesOrderCode, --销售订单编号
                                        a.receivingaddress,
                                        (select value from keyvalueitem where NAME = 'IsOrNot'and key=a.ifdirectprovision) As ifdirectprovision,
                                        (select value from keyvalueitem where NAME = 'IsOrNot'and key=a.directprovisionfinished) As directprovisionfinished,
                                        a.receivingcompanyname,
                                        a.deliverybillnumber,
                                        a.shippingdate,
                                        a.arrivaldate,
                                        (select value from keyvalueitem where NAME = 'PartsShipping_Method'and key=a.shippingmethod) As shippingmethod,    
                                        a.logisticcompany,
                                        a.driver,
                                        a.phone,
                                        a.vehiclelicenseplate,
                                        (select value from keyvalueitem where NAME = 'SupplierShippingOrder_Status'and key=a.status) As status,
                                        a.remark,
                                        a.creatorname,
                                        a.createtime,
                                        b.serialnumber,
                                        b.sparepartcode,
                                        b.sparepartname,
                                        b.supplierpartcode,
                                        b.measureunit,
                                        b.quantity,
                                        b.confirmedamount,
                                        b.remark as detailRemark
                                    from SupplierShippingOrder a
                                    Inner join suppliershippingdetail b on a.id = b.suppliershippingorderid
                                    inner join company c on a.partssupplierid = c.id
                                                        and c.status = 1
                                    inner join partssalescategory d on a.partssalescategoryid = d.id
                                                                and d.status = 1
                                    inner join warehouse e on a.receivingwarehouseid = e.id
                                                        and e.status = 1
                                    inner join PartsSalesOrder f on f.id = a.originalrequirementbillid
                                    where a.OriginalRequirementBillType = 1
                                    AND a.IfDirectProvision = 1
                                    AND a.Status <> 99");
                    #endregion

                    var dbParameters = new List<DbParameter>();
                    #region 条件过滤
                    //默认查询登录企业相关零售单
                    var userinfo = Utils.GetCurrentUserInfo();
                    sql.Append(" and a.receivingcompanyid = {0}receivingcompanyid");
                    dbParameters.Add(db.CreateDbParameter("receivingcompanyid", userinfo.EnterpriseId));

                    if(ids != null && ids.Length > 0) {
                        sql.Append(" and a.id in (");
                        for(var i = 0; i < ids.Length; i++) {
                            if(ids.Length == i + 1) {
                                sql.Append("{0}id" + i);
                                dbParameters.Add(db.CreateDbParameter("id" + i, ids[i]));
                            } else {
                                sql.Append("{0}id" + i + ",");
                                dbParameters.Add(db.CreateDbParameter("id" + i, ids[i]));
                            }
                        }
                        sql.Append(")");
                    } else {
                        if(branchId.HasValue) {
                            sql.Append(" and a.branchId={0}branchId");
                            dbParameters.Add(db.CreateDbParameter("branchId", branchId.Value));
                        }
                        if(!string.IsNullOrEmpty(partsSupplierCode)) {
                            sql.Append(" and a.partsSupplierCode like {0}partsSupplierCode ");
                            dbParameters.Add(db.CreateDbParameter("partsSupplierCode", "%" + partsSupplierCode + "%"));
                        }
                        if(!string.IsNullOrEmpty(partsSupplierName)) {
                            sql.Append(" and a.partsSupplierName like {0}partsSupplierName ");
                            dbParameters.Add(db.CreateDbParameter("partsSupplierName", "%" + partsSupplierName + "%"));
                        }
                        if(partsSalesCategoryId.HasValue) {
                            sql.Append(" and a.partsSalesCategoryId={0}partsSalesCategoryId");
                            dbParameters.Add(db.CreateDbParameter("partsSalesCategoryId", partsSalesCategoryId.Value));
                        }
                        if(!string.IsNullOrEmpty(code)) {
                            sql.Append(" and a.code like {0}code ");
                            dbParameters.Add(db.CreateDbParameter("code", "%" + code + "%"));
                        }
                        if(!string.IsNullOrEmpty(partsSaleOrderCode)) {
                            sql.Append(" and f.code = {0}partsSaleOrderCode ");
                            dbParameters.Add(db.CreateDbParameter("partsSaleOrderCode", partsSaleOrderCode));
                        }
                        if(status.HasValue) {
                            sql.Append(" and a.status={0}status");
                            dbParameters.Add(db.CreateDbParameter("status", status.Value));
                        }
                        if(createTimeBegin.HasValue) {
                            sql.Append(@" and a.createTime >=to_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(@" and a.createTime <=to_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }

                    }
                    #endregion

                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    conn.Open();
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    //列名
                                    ErrorStrings.Export_Title_SupplierShippingOrder_Code,ErrorStrings.Export_Title_Branch_BranchName,ErrorStrings.Export_Title_Partssalescategory_Name,ErrorStrings.Export_Title_SupplierShippingOrder_ReveiveWarehouseName,
                                    ErrorStrings.Export_Title_SupplierShippingOrder_SalesOrderCode,ErrorStrings.Export_Title_PartsPurchaseOrder_ReceiveAddress,ErrorStrings.Export_Title_SupplierShippingOrder_Ifdirectprovision,ErrorStrings.Export_Title_SupplierShippingOrder_Directprovisionfinished,ErrorStrings.Export_Title_SupplierShippingOrder_ReveiveCompany,ErrorStrings.Export_Title_SupplierShippingOrder_DdeliverybillNumber,ErrorStrings.Export_Title_SupplierShippingOrder_ShippingDates,ErrorStrings.Export_Title_SupplierShippingOrder_ArriveDate,ErrorStrings.Export_Title_PartsPurchaseOrder_ShippingMethod,
                                    ErrorStrings.Export_Title_SupplierShippingOrder_LogisticCompany,ErrorStrings.Export_Title_SupplierShippingOrder_Driver,ErrorStrings.Export_Title_SupplierShippingOrder_DriverPhone,ErrorStrings.Export_Title_SupplierShippingOrder_VehicleLicensePlate,ErrorStrings.Export_Title_AccountPeriod_Status,ErrorStrings.Export_Title_PartsBranch_Remark,ErrorStrings.Export_Title_AccountPeriod_CreatorName,ErrorStrings.Export_Title_AccountPeriod_CreateTime,
                                    ErrorStrings.Export_Title_InternalAllocationBill_SerialNumber,ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode,ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName,ErrorStrings.Export_Title_SupplierShippingOrder_SupplierpartCode,ErrorStrings.Export_Title_PackingPropertyApp_MeasureUnit,ErrorStrings.Export_Title_WarehouseArea_Quantity,ErrorStrings.Export_Title_PartsPurchaseOrder_ConfirmedAmount,ErrorStrings.Export_Title_PartsPurchaseOrder_DetailRemark
                                };
                            }
                            if(reader.Read()) {
                                var values = new[] {
                                    reader[0],reader[1],reader[2],reader[3],reader[4],reader[5],reader[6],reader[7],reader[8],reader[9],
                                    reader[10],reader[11],reader[12],reader[13],reader[14],reader[15],reader[16],reader[17],reader[18],
                                    reader[19],reader[20],
                                    reader[21],reader[22],reader[23],reader[24],reader[25],reader[26],reader[27],reader[28]
                                };
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }
		 /// <summary>
        /// 合并导出供应商发运单及清单
        /// </summary>
        public bool Export供应商确认(int[] ids, string code, string partsPurchaseOrderCode, string partsSupplierName, string partsSupplierCode, int? arriveMethod, int? comfirmStatus, bool? ifDirectProvision, DateTime? createTimeBegin, DateTime? createTimeEnd,int?modifyStatus, out string fileName) {
            fileName = GetExportFilePath("合并导出供应商发运单及清单.xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {
                    var sql = new StringBuilder();
                    #region SQL
                    sql.Append(@" select kv1.value,
								   detail.Code,
								   detail.LogisticArrivalDate,
								   detail.ModifyArrive,
								   kv2.value,
								   detail.InboundTime,
								   detail.PlanDeliveryTime,
								   kv3.value,
								   detail.PartsSupplierCode,
								   detail.PartsSupplierName,
								   detail.PartsPurchaseOrderCode,
								   detail.ReceivingWarehouseName,
								   detail.ReceivingAddress,
								   detail.ReceivingCompanyName,
								   decode(detail.IfDirectProvision, 1, '是', '否'),
								   detail.OriginalRequirementBillCode,
                                   detail.LogisticCompany,
								   detail.DeliveryBillNumber,
								   detail.SupplierComfirm,
								   detail.SupplierComfirmDate,
								   detail.SupplierModify,
								   detail.SupplierModifyDate,
								   detail.Approver,
								   detail.ApproveDate, kv4.value,detail.RejectReason,
								   d.sparepartcode,
								   d.sparepartname,
								   d.supplierpartcode,
								   d.measureunit,
								   d.quantity,
								   d.confirmedamount,
								   d.weight,
								   d.volume,
								   d.SpareOrderRemark,
								   d.Remark
							  from SupplierShippingOrder detail
							  join SupplierShippingDetail d
								on detail.id = d.suppliershippingorderid
							  left join keyvalueitem kv1
								on detail.ComfirmStatus = kv1.key
							   and kv1.name = 'SupplierShippingOrderComfirmStatus'
							  left join keyvalueitem kv2
								on detail.ArriveMethod = kv2.key
							   and kv2.name = 'SupplierShippingOrderArriveMethod'
							  left   join keyvalueitem kv3
								on detail.Status = kv3.key
							   and kv3.name = 'SupplierShippingOrder_Status' 
                             left  join keyvalueitem kv4
								on detail.ModifyStatus = kv4.key
							   and kv4.name = 'SupplierShippingOrderModifyStatus' 
							 where 1=1 ");
                    #endregion

                    var dbParameters = new List<DbParameter>();
                    #region 条件过滤
                    if(ids != null && ids.Length > 0) {
                        sql.Append(" and detail.id in (");
                        for(var i = 0; i < ids.Length; i++) {
                            if(ids.Length == i + 1) {
                                sql.Append("{0}id" + i);
                                dbParameters.Add(db.CreateDbParameter("id" + i, ids[i]));
                            } else {
                                sql.Append("{0}id" + i + ",");
                                dbParameters.Add(db.CreateDbParameter("id" + i, ids[i]));
                            }
                        }
                        sql.Append(")");
                    } else {
                        
                        if(!string.IsNullOrEmpty(partsSupplierCode)) {
                            sql.Append(" and detail.partsSupplierCode like {0}partsSupplierCode ");
                            dbParameters.Add(db.CreateDbParameter("partsSupplierCode", "%" + partsSupplierCode + "%"));
                        }
                        if(!string.IsNullOrEmpty(partsSupplierName)) {
                            sql.Append(" and detail.partsSupplierName like {0}partsSupplierName ");
                            dbParameters.Add(db.CreateDbParameter("partsSupplierName", "%" + partsSupplierName + "%"));
                        }
                        if(arriveMethod.HasValue) {
                            sql.Append(" and detail.arriveMethod={0}arriveMethod");
                            dbParameters.Add(db.CreateDbParameter("arriveMethod", arriveMethod.Value));
                        }
                        if(comfirmStatus.HasValue) {
                            sql.Append(" and detail.comfirmStatus={0}comfirmStatus");
                            dbParameters.Add(db.CreateDbParameter("comfirmStatus", comfirmStatus.Value));
                        }
                        if(modifyStatus.HasValue) {
                            sql.Append(" and detail.modifyStatus={0}modifyStatus");
                            dbParameters.Add(db.CreateDbParameter("modifyStatus", modifyStatus.Value));
                        }
						 if(ifDirectProvision.HasValue) {
                            sql.Append(" and detail.ifDirectProvision={0}ifDirectProvision");
                            dbParameters.Add(db.CreateDbParameter("ifDirectProvision", ifDirectProvision.Value));
                        }
                        if(!string.IsNullOrEmpty(code)) {
                            sql.Append(" and detail.code like {0}code ");
                            dbParameters.Add(db.CreateDbParameter("code", "%" + code + "%"));
                        }
                        if(!string.IsNullOrEmpty(partsPurchaseOrderCode)) {
                            sql.Append(" and detail.partsPurchaseOrderCode  like {0}partsPurchaseOrderCode ");
                            dbParameters.Add(db.CreateDbParameter("partsPurchaseOrderCode", "%" + partsPurchaseOrderCode+"%"));
                        }                        
                        if(createTimeBegin.HasValue) {
                            sql.Append(@" and detail.createTime >=to_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(@" and detail.createTime <=to_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                        var userInfo = Utils.GetCurrentUserInfo();
                        if(userInfo.EnterpriseId!=12730) {
                            sql.Append(" and detail.PartsSupplierId={0}PartsSupplierId");
                            dbParameters.Add(db.CreateDbParameter("PartsSupplierId", userInfo.EnterpriseId));
                        }
                    }
                    #endregion
                    var sqlStr = sql.ToString();
                    if(sqlStr.IndexOf(" where 1=1  and ") > 0)
                        sqlStr = sqlStr.Replace(" where 1=1  and ", " where ");
                    else
                        sqlStr = sqlStr.Replace(" where 1=1 ", "");

                    var command = db.CreateDbCommand(string.Format(sqlStr, db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    conn.Open();
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    "确认状态","发运单号","承运商送达时间","修改承运商送达时间","到货确认方式","入库时间",
									"计划到货时间","发运单状态","供应商编号","供应商名称","采购订单号","收货仓库名称",
									"收货地址","收货单位名称","是否直供","原始需求单据","物流公司","送货单号","确认人","确认时间","修改人","修改时间","审核人",
									"审核时间","修改状态","审核意见","配件图号","配件名称","供应商图号","计量单位","发运量","确认量","重量","体积","采购订单清单备注","备注"
                                };
                            }
                            if(reader.Read()) {
                                var values = new[] {
                                    reader[0],reader[1],reader[2],reader[3],reader[4],reader[5],reader[6],reader[7],reader[8],reader[9],
                                    reader[10],reader[11],reader[12],reader[13],reader[14],reader[15],reader[16],reader[17],reader[18],
                                    reader[19],reader[20],reader[21],reader[22],reader[23],reader[24],reader[25],reader[26],reader[27],
                                    reader[28],reader[29],reader[30],reader[31],reader[32],reader[33]
                                };
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }
    }
}
