﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {

    public partial class ExcelService {
        public bool ExportSupplierTraceCode(int[] ids, string code, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, string partsSupplierCode, string partsSupplierName, string sparePartCode, string partsPurchaseOrderCode, string oldTraceCode, string newTraceCode, string shippingCode, out string fileName) {

            fileName = GetExportFilePath("供应商零件永久性标识条码修正单.xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);
                var userInfo = Utils.GetCurrentUserInfo();
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.Append(@"select st.code,
                                   kv.value,
                                   st.approvecomment,
                                   st.creatorname,
                                   st.createtime,
                                   st.modifiername,
                                   st.modifytime,
                                   st.submittername,
                                   st.submittime,
                                   st.checkername,
                                   st.checktime,
                                   st.approvername,
                                   st.approvertime,
                                   std.sparepartcode,
                                   std.sparepartname,
                                   std.partssuppliercode,
                                   std.partssuppliername,
                                   std.partspurchaseordercode,
                                   std.shippingcode,
                                   kv2.value,
                                   std.oldtracecode,
                                   std.newtracecode
                              from SupplierTraceCode st
                              join SupplierTraceCodeDetail std
                                on st.id = std.suppliertracecodeid
                              join keyvalueitem kv
                                on st.status = kv.key
                               and kv.name = 'SupplierTraceCodeStatus'
                              join keyvalueitem kv2
                                on std.traceproperty = kv2.key
                               and kv2.name = 'TraceProperty'
                             where 1=1 ");
                    var dbParameters = new List<DbParameter>();
                    if(ids != null && ids.Length > 0) {
                        sql.Append(" and st.id in (");
                        for(var i = 0; i < ids.Length; i++) {
                            if(ids.Length == i + 1) {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            } else {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                        }
                        
                    } else {
                        if(!string.IsNullOrEmpty(code)) {
                            sql.Append("and LOWER(st.Code) like {0}code ");
                            dbParameters.Add(db.CreateDbParameter("code", "%" + code.ToLower() + "%"));
                        }
                        if(!string.IsNullOrEmpty(sparePartCode)) {
                            sql.Append("and LOWER(std.sparePartCode) like {0}sparePartCode ");
                            dbParameters.Add(db.CreateDbParameter("sparePartCode", "%" + sparePartCode.ToLower() + "%"));
                        }
                        if(!string.IsNullOrEmpty(partsSupplierCode)) {
                            sql.Append("and LOWER(std.partsSupplierCode) like {0}partsSupplierCode ");
                            dbParameters.Add(db.CreateDbParameter("partsSupplierCode", "%" + partsSupplierCode.ToLower() + "%"));
                        }
                        if(!string.IsNullOrEmpty(partsSupplierName)) {
                            sql.Append(" and LOWER(std.partsSupplierName) like {0}partsSupplierName");
                            dbParameters.Add(db.CreateDbParameter("partsSupplierName", "%" + partsSupplierName.ToLower() + "%"));
                        }
                        if(!string.IsNullOrEmpty(partsPurchaseOrderCode)) {
                            sql.Append(" and LOWER(std.partsPurchaseOrderCode) like {0}partsPurchaseOrderCode");
                            dbParameters.Add(db.CreateDbParameter("partsPurchaseOrderCode", "%" + partsPurchaseOrderCode.ToLower() + "%"));
                        }
                        if(!string.IsNullOrEmpty(oldTraceCode)) {
                            sql.Append(" and LOWER(std.oldTraceCode) like {0}oldTraceCode");
                            dbParameters.Add(db.CreateDbParameter("oldTraceCode", "%" + oldTraceCode.ToLower() + "%"));
                        }
                        if(!string.IsNullOrEmpty(newTraceCode)) {
                            sql.Append(" and LOWER(std.newTraceCode) like {0}newTraceCode");
                            dbParameters.Add(db.CreateDbParameter("newTraceCode", "%" + newTraceCode.ToLower() + "%"));
                        }
                        if(!string.IsNullOrEmpty(shippingCode)) {
                            sql.Append(" and LOWER(std.shippingCode) like {0}shippingCode");
                            dbParameters.Add(db.CreateDbParameter("shippingCode", "%" + shippingCode.ToLower() + "%"));
                        }
                        if(status.HasValue) {
                            sql.Append(@" and st.Status = {0}status ");
                            dbParameters.Add(db.CreateDbParameter("status", status.Value));
                        }
                       
                        if(createTimeBegin.HasValue) {
                            sql.Append(@" and st.createTime >=To_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(@" and st.createTime <=To_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                       
                    }
                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    "修正单号","状态","审核意见","创建人","创建时间","修改人","修改时间","提交人","提交时间","审核人","审核时间","审批人","审批时间",
                                    "配件图号","配件名称","供应商编号","供应商名称","采购单编号","发运单编号","追溯属性","原追溯码","新追溯码"
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }
    }
}
