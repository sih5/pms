﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        public bool Export临时采购订单(int[] ids, int personelId, string temPurchasePlanOrderCode, string suplierName, string code, int? approveStatus, int? receiveStatus, int? status, int? customerType, string receCompanyCode, string receCompanyName, DateTime? createTimeBegin, DateTime? createTimeEnd, string sparePartCode, bool? isSaleCode, bool? isPurchaseOrder, out string filename) {
            filename = GetExportFilePath("临时采购订单清单_" + ".xlsx");
            if(!HttpContext.Current.User.Identity.IsAuthenticated) {
                throw new Exception(ErrorStrings.File_Validation_User);
            }
            try {
                var db = DbHelper.GetDbHelp(ConnectionString);
                using(var conn = db.CreateDbConnection()) {
                    var sql = new StringBuilder();
                    sql.Append(@"select tod.code, /*临时订单号*/
                                   tod.tempurchaseplanordercode, /*临时采购计划单号*/
                                   tod.warehousename, /*仓库名称*/
                                   (select value
                                      from keyvalueitem kv
                                     where kv.key = tod.status
                                       and kv.name = 'TemPurchaseOrderStatus') status, /*订单状态*/
                                   (select value
                                      from keyvalueitem kv
                                     where kv.key = tod.approvestatus
                                       and kv.name = 'TemPurchaseOrderApproveStatus') approvestatus, /*审核状态*/
                                   tod.supliercode, /*供应商编号*/
                                   tod.supliername, /*供应商名称*/
                                   (select value
                                      from keyvalueitem kv
                                     where kv.key = tod.ordertype
                                       and kv.name = 'TemPurchasePlanOrderPlanType') approvestatus, /*订单类型*/
                                   CASE
                                     WHEN Tod.isturnsale = 1 THEN
                                      cast('是' as varchar2(100))
                                     ELSE
                                      cast('否' as varchar2(100))
                                   END AS isturnsale, /*是否自动转销售*/
                                   tod.rececompanycode, /*收货单位编号*/
                                   tod.rececompanyname, /*收货单位名称*/
                                   tod.ordercompanycode, /*订单单位编号*/
                                   tod.ordercompanyname, /*订货单位名称*/
                                   (select value
                                      from keyvalueitem kv
                                     where kv.key = tod.shippingmethod
                                       and kv.name = 'TemPurchasePlanOrderShippingMethod') approvestatus, /*发运方式*/
                                   (select value
                                      from keyvalueitem kv
                                     where kv.key = tod.receivestatus
                                       and kv.name = 'TemPurchaseOrderReceiveStatus') approvestatus, /*收货状态*/
                                   tod.partssalesordercode, /*销售单号*/
                                   tod.partspurchaseordercode, /*采购单号*/
                                   tod.buyer, /*采购员*/
                                   tod.creatorname, /*创建人*/
                                   tod.createtime, /*创建时间*/
                                   tod.modifiername, /*修改人*/
                                   tod.modifytime, /*修改时间*/
                                   tod.memo, /*备注*/
                                   tod.submittername, /*提交人*/
                                   tod.submittime, /*提交时间*/
                                   tod.approvername, /*审核人*/
                                   tod.approvetime, /*审核时间*/
                                   tod.checkername, /*审批人*/
                                   tod.checktime, /*审批时间*/
                                   tod.confirmer, /*确认人*/
                                   tod.confirmetime, /*确认时间*/
                                   tod.shipper, /*发运人*/
                                   tod.shipptime, /*发运时间*/
                                   tod.stopper, /*终止人*/
                                   tod.stoptime, /*终止时间*/
                                   tod.stopreason, /*终止原因*/
                                   tod.forcedder, /*强制完成人*/
                                   tod.forcedtime, /*强制完成时间*/
                                   tod.forcedreason, /*强制完成原因*/
                                   tod.receiveaddress, /*收货地址*/
                                   tod.linker, /*联系人*/
                                   tod.linkphone, /*联系电话*/
                                   (select value
                                      from keyvalueitem kv
                                     where kv.key = tod.customertype
                                       and kv.name = 'TemPurchasePlanOrderCustomerType') approvestatus, /*客户属性*/
                                   (select value
                                      from keyvalueitem kv
                                     where kv.key = tod.freighttype
                                       and kv.name = 'TemPurchasePlanOrderFreightType') approvestatus, /*运费*/
                                   tod.rejectreason, /*驳回原因*/
                                   tdp.sparepartcode, /*配件编号*/
                                   tdp.sparepartname, /*配件名称*/
                                   tdp.supplierpartcode, /*SupplierPartCode*/
                                   tdp.planamount, /*计划量*/
                                   tdp.measureunit, /*单位*/
                                   tdp.confirmedamount, /*确认量*/
                                   tdp.shippingamount, /*发运量*/
                                   tdp.remark, /*清单备注*/
                                   (select value
                                      from keyvalueitem kv
                                     where kv.key = tdp.ShortSupReason
                                       and kv.name = 'PartsPurchaseOrderDetail_ShortSupReason') approvestatus /*短供原因*/
                              from TemPurchaseOrder tod
                              join TemPurchaseOrderDetail tdp
                                on tod.id = tdp.TemPurchaseOrderId
                             where (not exists (select 1
                                                  from PersonnelSupplierRelation ps
                                                 where ps.personid = " + personelId + @"
                                                   and ps.status = 1) or exists
                                    (select 1
                                       from PersonnelSupplierRelation ps
                                      where ps.personid = " + personelId + @"
                                        and ps.status = 1
                                        and ps.supplierid = tod.suplierid)) ");
                    var tableNames = sql.ToString().GetRegexNames();
                    var dbParameters = new List<DbParameter>();
                    if(ids != null && ids.Length > 0) {
                        sql.Append(@" and tod.id in (");
                        for(int i = 0; i < ids.Length; i++) {
                            var tempIdWithNum = "tempIdWithNum" + i;
                            if(i == ids.Length - 1) {
                                var tempParam = "{0}" + tempIdWithNum;
                                sql.Append(tempParam);
                            } else {
                                var tempParam = "{0}" + tempIdWithNum + ",";
                                sql.Append(tempParam);
                            }
                            dbParameters.Add(db.CreateDbParameter(tempIdWithNum, ids[i]));
                        }
                        sql.Append(")");
                    } else {
                        if(!string.IsNullOrEmpty(code)) {
                            sql.Append(" and tod.code like {0}code");
                            dbParameters.Add(db.CreateDbParameter("code", code));
                        }
                        if(!string.IsNullOrEmpty(temPurchasePlanOrderCode)) {
                            sql.Append(" and tod.temPurchasePlanOrderCode like {0}temPurchasePlanOrderCode");
                            dbParameters.Add(db.CreateDbParameter("temPurchasePlanOrderCode", temPurchasePlanOrderCode));
                        }
                        if(!string.IsNullOrEmpty(suplierName)) {
                            sql.Append(" and tod.suplierName like {0}suplierName");
                            dbParameters.Add(db.CreateDbParameter("suplierName", suplierName));
                        }
                        if(approveStatus.HasValue) {
                            sql.Append(" and tod.approveStatus={0}approveStatus");
                            dbParameters.Add(db.CreateDbParameter("approveStatus", approveStatus));
                        }
                        if(receiveStatus.HasValue) {
                            sql.Append(" and tod.receiveStatus={0}receiveStatus");
                            dbParameters.Add(db.CreateDbParameter("receiveStatus", receiveStatus));
                        }
                        if(status.HasValue) {
                            sql.Append(" and tod.status={0}status");
                            dbParameters.Add(db.CreateDbParameter("status", status));
                        }
                        if(customerType.HasValue) {
                            sql.Append(" and tod.customerType={0}customerType");
                            dbParameters.Add(db.CreateDbParameter("customerType", customerType));
                        }
                        if(isSaleCode.HasValue && isSaleCode.Value) {
                            sql.Append(" and tod.PartsSalesOrderCode is not null");
                        } else if(isSaleCode.HasValue && !isSaleCode.Value) {
                            sql.Append(" and tod.PartsSalesOrderCode is  null");
                        }
                        if(isPurchaseOrder.HasValue && isPurchaseOrder.Value) {
                            sql.Append(" and tod.PartsPurchaseOrderCode is not null");
                        } else if(isPurchaseOrder.HasValue && !isPurchaseOrder.Value) {
                            sql.Append(" and tod.PartsPurchaseOrderCode is  null");
                        }
                        if(createTimeBegin.HasValue) {
                            sql.Append(" and tod.createTime>=to_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(" and tod.createTime<=to_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }


                        if(!string.IsNullOrEmpty(sparePartCode)) {
                            sql.Append("and (Exists (Select 1 From TemPurchaseOrderDetail Where sparePartCode like {0}sparePartCode and TemPurchaseOrderId =tod.Id))");
                            dbParameters.Add(db.CreateDbParameter("sparePartCode", "%" + sparePartCode + "%"));
                        }
                    }
                    sql.Append(" order by tod.code");
                    DbCommand comm = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    comm.Parameters.AddRange(dbParameters.ToArray());
                    conn.Open();
                    var reader = comm.ExecuteReader();
                    using(var excelExport = new ExcelExport(filename)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0)
                                return tableNames;
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }
        public   bool Export临时采购订单供应商(int[] ids, int suplierId, string temPurchasePlanOrderCode, string code, int? status,int? orderType, DateTime? createTimeBegin, DateTime? createTimeEnd, DateTime? bConfirmeTime, DateTime? eConfirmeTime, out string filename){
            filename = GetExportFilePath("临时采购订单清单_" + ".xlsx");
            if(!HttpContext.Current.User.Identity.IsAuthenticated) {
                throw new Exception(ErrorStrings.File_Validation_User);
            }
            try {
                var db = DbHelper.GetDbHelp(ConnectionString);
                using(var conn = db.CreateDbConnection()) {
                    var sql = new StringBuilder();
                    sql.Append(@"select tod.code, /*临时订单号*/
                                   tod.tempurchaseplanordercode, /*原始需求单号*/
                                   tod.warehousename, /*仓库名称*/
                                   (select value
                                      from keyvalueitem kv
                                     where kv.key = tod.status
                                       and kv.name = 'TemPurchaseOrderStatus') status, /*订单状态*/
                                  
                                   tod.supliercode, /*供应商编号*/
                                   tod.supliername, /*供应商名称*/
                                   (select value
                                      from keyvalueitem kv
                                     where kv.key = tod.ordertype
                                       and kv.name = 'TemPurchasePlanOrderPlanType') approvestatus, /*订单类型*/                                 
                                   tod.rececompanycode, /*收货单位编号*/
                                   tod.rececompanyname, /*收货单位名称*/
                                   tod.creatorname, /*创建人*/
                                   tod.createtime, /*创建时间*/
                                   tod.modifiername, /*修改人*/
                                   tod.modifytime, /*修改时间*/
                                   tod.memo, /*备注*/
                                   tod.submittername, /*提交人*/
                                   tod.submittime, /*提交时间*/                               
                                   tod.confirmer, /*确认人*/
                                   tod.confirmetime, /*确认时间*/
                                   tod.shipper, /*发运人*/
                                   tod.shipptime, /*发运时间*/
                                   tod.stopper, /*终止人*/
                                   tod.stoptime, /*终止时间*/
                                   tod.stopreason, /*终止原因*/
                                   tod.forcedder, /*强制完成人*/
                                   tod.forcedtime, /*强制完成时间*/
                                   tod.forcedreason, /*强制完成原因*/
                                   tod.receiveaddress, /*收货地址*/                                
                                   tod.rejectreason, /*驳回原因*/
                                   tdp.sparepartcode, /*配件编号*/
                                   tdp.sparepartname, /*配件名称*/
                                   tdp.supplierpartcode, /*SupplierPartCode*/
                                   tdp.planamount, /*计划量*/
                                   tdp.measureunit, /*单位*/
                                   tdp.confirmedamount, /*确认量*/
                                   tdp.shippingamount, /*发运量*/
                                   tdp.remark, /*清单备注*/
                                   (select value
                                      from keyvalueitem kv
                                     where kv.key = tdp.ShortSupReason
                                       and kv.name = 'PartsPurchaseOrderDetail_ShortSupReason') approvestatus /*短供原因*/
                              from TemPurchaseOrder tod
                              join TemPurchaseOrderDetail tdp
                                on tod.id = tdp.TemPurchaseOrderId
                             where  tod.SuplierId="+suplierId);
                    var tableNames = sql.ToString().GetRegexNames();
                    var dbParameters = new List<DbParameter>();
                    if(ids != null && ids.Length > 0) {
                        sql.Append(@" and tod.id in (");
                        for(int i = 0; i < ids.Length; i++) {
                            var tempIdWithNum = "tempIdWithNum" + i;
                            if(i == ids.Length - 1) {
                                var tempParam = "{0}" + tempIdWithNum;
                                sql.Append(tempParam);
                            } else {
                                var tempParam = "{0}" + tempIdWithNum + ",";
                                sql.Append(tempParam);
                            }
                            dbParameters.Add(db.CreateDbParameter(tempIdWithNum, ids[i]));
                        }
                        sql.Append(")");
                    } else {
                        if(!string.IsNullOrEmpty(code)) {
                            sql.Append(" and tod.code like {0}code");
                            dbParameters.Add(db.CreateDbParameter("code", code));
                        }
                        if(!string.IsNullOrEmpty(temPurchasePlanOrderCode)) {
                            sql.Append(" and tod.temPurchasePlanOrderCode like {0}temPurchasePlanOrderCode");
                            dbParameters.Add(db.CreateDbParameter("temPurchasePlanOrderCode", temPurchasePlanOrderCode));
                        }
                       
                        if(orderType.HasValue) {
                            sql.Append(" and tod.orderType={0}orderType");
                            dbParameters.Add(db.CreateDbParameter("orderType", orderType));
                        }
                      
                        if(status.HasValue) {
                            sql.Append(" and tod.status={0}status");
                            dbParameters.Add(db.CreateDbParameter("status", status));
                        }
                       
                        if(createTimeBegin.HasValue) {
                            sql.Append(" and tod.createTime>=to_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(" and tod.createTime<=to_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                        if(bConfirmeTime.HasValue) {
                            sql.Append(" and tod.ConfirmeTime>=to_date({0}bConfirmeTime,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = bConfirmeTime.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("bConfirmeTime", tempTime.ToString("G")));
                        }
                        if(eConfirmeTime.HasValue) {
                            sql.Append(" and tod.ConfirmeTime<=to_date({0}eConfirmeTime,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = eConfirmeTime.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("eConfirmeTime", tempTime.ToString("G")));
                        }

                       
                    }
                    sql.Append(" order by tod.code");
                    DbCommand comm = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    comm.Parameters.AddRange(dbParameters.ToArray());
                    conn.Open();
                    var reader = comm.ExecuteReader();
                    using(var excelExport = new ExcelExport(filename)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0)
                                return tableNames;
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }
    }
}
