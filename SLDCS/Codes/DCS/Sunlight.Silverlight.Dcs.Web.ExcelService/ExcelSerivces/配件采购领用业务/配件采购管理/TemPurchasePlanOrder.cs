﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using System.Data.Common;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Entities;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        public bool ExportTemPurchasePlanOrderWithDetail(int[] ids, string code, int? status, string receCompanyCode, string receCompanyName, DateTime? createTimeBegin, DateTime? createTimeEnd, string sparePartCode, out string filename) {
            filename = GetExportFilePath("临时采购计划单清单_" + ".xlsx");
            if(!HttpContext.Current.User.Identity.IsAuthenticated) {
                throw new Exception(ErrorStrings.File_Validation_User);
            }
            try {
                var db = DbHelper.GetDbHelp(ConnectionString);
                using(var conn = db.CreateDbConnection()) {
                    var sql = new StringBuilder();
                    sql.Append(@"select tp.code,
                                               tp.warehousename,
                                               (SELECT VALUE FROM KEYVALUEITEM WHERE NAME = 'TemPurchasePlanOrderStatus' AND KEY = Tp.status) as status,
                                               (CASE WHEN tp.isturnsale = 1 THEN cast('是' as varchar2(100))  ELSE cast('否' as varchar2(100)) END) as isturnsale,
                                               (SELECT VALUE FROM KEYVALUEITEM WHERE NAME = 'TemPurchasePlanOrderPlanType' AND KEY = Tp.plantype) as plantype,
                                               tp.rececompanycode,
                                               tp.rececompanyname,
                                               (SELECT VALUE FROM KEYVALUEITEM WHERE NAME = 'TemPurchasePlanOrderShippingMethod' AND KEY = Tp.shippingmethod) as shippingmethod,
                                                (CASE WHEN tp.isreplace = 1 THEN cast('是' as varchar2(100))  ELSE cast('否' as varchar2(100)) END) as isreplace,
                                               tp.memo,
                                                (SELECT VALUE FROM KEYVALUEITEM WHERE NAME = 'TemPurchasePlanOrderFreightType' AND KEY = Tp.freighttype) as freighttype,
                                               tp.creatorname,
                                               tp.createtime,
                                               tp.modifiername,
                                               tp.modifytime,
                                               tp.abandonername,
                                               tp.abandontime,
                                               tp.submittername,
                                               tp.submittime,
                                               tp.approvername,
                                               tp.approvetime,
                                               tp.checkername,
                                               tp.checktime,
                                               tp.confirmer,
                                               tp.confirmetime,
                                               tp.rejector,
                                               tp.rejecttime,
                                               tp.RejectReason,
                                               tp.receiveaddress,
                                               tp.linker,
                                               tp.linkphone,
                                               td.sparepartcode,
                                               td.sparepartname,
                                               td.supplierpartcode,
                                               td.referencecode,
                                               td.planamount,
                                               td.measureunit,
                                               td.supliercode,
                                               td.supliername,
                                               td.memo,
                                               (SELECT VALUE FROM KEYVALUEITEM WHERE NAME = 'TemPurchasePlanOrderFreightType' AND KEY = Td.codesource) as codesource
                                          from TemPurchasePlanOrder tp
                                          join TemPurchasePlanOrderDetail td
                                            on tp.id = td.tempurchaseplanorderid
                                          where 1=1 ");
                    var tableNames = sql.ToString().GetRegexNames();
                    var dbParameters = new List<DbParameter>();
                    if(ids != null && ids.Length > 0) {
                        sql.Append(@" and Tp.id in (");
                        for(int i = 0; i < ids.Length; i++) {
                            var tempIdWithNum = "tempIdWithNum" + i;
                            if(i == ids.Length - 1) {
                                var tempParam = "{0}" + tempIdWithNum;
                                sql.Append(tempParam);
                            } else {
                                var tempParam = "{0}" + tempIdWithNum + ",";
                                sql.Append(tempParam);
                            }
                            dbParameters.Add(db.CreateDbParameter(tempIdWithNum, ids[i]));
                        }
                        sql.Append(")");
                    } else {
                        if(!string.IsNullOrEmpty(code)) {
                            sql.Append(" and Tp.code like {0}code");
                            dbParameters.Add(db.CreateDbParameter("code", code));
                        }

                        if(status.HasValue) {
                            sql.Append(" and Tp.status={0}status");
                            dbParameters.Add(db.CreateDbParameter("status", status));
                        }
                        if(!string.IsNullOrEmpty(receCompanyCode)) {
                            sql.Append(" and Tp.receCompanyCode like {0}receCompanyCode");
                            dbParameters.Add(db.CreateDbParameter("receCompanyCode", "%" + receCompanyCode + "%"));
                        }
                        if(!string.IsNullOrEmpty(receCompanyName)) {
                            sql.Append(" and Tp.receCompanyName like {0}receCompanyName");
                            dbParameters.Add(db.CreateDbParameter("receCompanyName", "%" + receCompanyName + "%"));

                        }
                        if(createTimeBegin.HasValue) {
                            sql.Append(" and Tp.createTime>=to_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(" and Tp.createTime<=to_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                        if(!string.IsNullOrEmpty(sparePartCode)) {
                            sql.Append("and (Exists (Select 1 From TemPurchasePlanOrderDetail Where sparePartCode like {0}sparePartCode and tempurchaseplanorderid =Tp.Id))");
                            dbParameters.Add(db.CreateDbParameter("sparePartCode", "%" + sparePartCode + "%"));
                        }
                    }
                    DbCommand comm = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    comm.Parameters.AddRange(dbParameters.ToArray());
                    conn.Open();
                    var reader = comm.ExecuteReader();
                    using(var excelExport = new ExcelExport(filename)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0)
                                return new object[] {
                                    ErrorStrings.Export_Title_PartsSalesOrder_OrderNo, ErrorStrings.Export_Title_SupplierShippingOrder_ReveiveWarehouseName, ErrorStrings.Export_Title_AccountPeriod_Status, ErrorStrings.Export_TemPurchasePlanOrder_IsTurnSale, ErrorStrings.Export_TemPurchasePlanOrder_PlanType,
                                    ErrorStrings.Export_TemPurchasePlanOrder_ReceCompanyCode, ErrorStrings.Export_TemPurchasePlanOrder_ReceCompanyName, ErrorStrings.Export_TemPurchasePlanOrder_ShippingMethod, ErrorStrings.Export_TemPurchasePlanOrder_IsReplace, ErrorStrings.Export_Title_PartsBranch_Remark, ErrorStrings.Export_TemPurchasePlanOrder_FreightType, ErrorStrings.Export_Title_AccountPeriod_CreatorName, ErrorStrings.Export_Title_AccountPeriod_CreateTime,
                                    ErrorStrings.Export_Title_AccountPeriod_ModifierName,ErrorStrings.Export_Title_AccountPeriod_ModifyTime,ErrorStrings.Export_Title_PartsBranch_AbandonerName, ErrorStrings.Export_Title_PartsBranch_AbandonerTime, ErrorStrings.Export_Title_PartsSalesOrder_SubmitterName,ErrorStrings.Export_Title_PartsSalesOrder_SubmitTime,
                                    ErrorStrings.Export_Title_Dealerpartsinventorybill_ApproverName, ErrorStrings.Export_Title_Dealerpartsinventorybill_ApproverTime, ErrorStrings.Export_TemPurchasePlanOrder_CheckerName, ErrorStrings.Export_TemPurchasePlanOrder_CheckerTime, ErrorStrings.Export_Title_InternalAllocationBill_ApproverName, ErrorStrings.Export_Title_InternalAllocationBill_ApproveTime,
                                    ErrorStrings.Export_Title_PartsOuterPurchaseChange_RejecterName,ErrorStrings.Export_Title_PartsOuterPurchaseChange_RejectTime,ErrorStrings.Export_Title_PartsOuterPurchaseChange_RejectComment,ErrorStrings.Export_Title_PartsPurchaseOrder_ReceiveAddress,ErrorStrings.Export_Title_Agency_LinkMan,ErrorStrings.Export_Title_Agency_LinkManMobile,ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode,ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName,
                                    ErrorStrings.Export_Title_SupplierShippingOrder_SupplierpartCode,ErrorStrings.Export_Title_Accessinfo_Tmp_ReferenceCode,ErrorStrings.Export_Title_PartsPurchasePlanDetail_PlanAmount,ErrorStrings.Export_Title_PartsPurchasePlanDetail_MeasureUnit,ErrorStrings.Export_Title_FactoryPurchacePrice_SupplierCode,ErrorStrings.Export_Title_FactoryPurchacePrice_SupplierName,ErrorStrings.Export_Title_PartsPurchaseOrder_DetailRemark,ErrorStrings.Export_TemPurchasePlanOrder_CodeSource
                                };
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }
        public bool Export临时采购计划(int[] ids, int companyId, string code, int? status, int? customerType, string receCompanyCode, string receCompanyName, DateTime? createTimeBegin, DateTime? createTimeEnd, string sparePartCode, out string filename) {
            filename = GetExportFilePath("临时采购计划单清单_" + ".xlsx");
            if(!HttpContext.Current.User.Identity.IsAuthenticated) {
                throw new Exception(ErrorStrings.File_Validation_User);
            }
            try {
                var db = DbHelper.GetDbHelp(ConnectionString);
                using(var conn = db.CreateDbConnection()) {
                    var sql = new StringBuilder();
                    sql.Append(@"select tp.code,
                                               tp.warehousename,
                                               (SELECT VALUE FROM KEYVALUEITEM WHERE NAME = 'TemPurchasePlanOrderStatus' AND KEY = Tp.status) as status,
                                               (CASE WHEN tp.isturnsale = 1 THEN cast('是' as varchar2(100))  ELSE cast('否' as varchar2(100)) END) as isturnsale,
                                               (SELECT VALUE FROM KEYVALUEITEM WHERE NAME = 'TemPurchasePlanOrderPlanType' AND KEY = Tp.plantype) as plantype,
                                               tp.rececompanycode,
                                               tp.rececompanyname,tp.OrderCompanyCode,tp.OrderCompanyName,
                                               (SELECT VALUE FROM KEYVALUEITEM WHERE NAME = 'TemPurchasePlanOrderShippingMethod' AND KEY = Tp.shippingmethod) as shippingmethod,
                                                (CASE WHEN tp.isreplace = 1 THEN cast('是' as varchar2(100))  ELSE cast('否' as varchar2(100)) END) as isreplace,
                                               tp.memo,
                                                (SELECT VALUE FROM KEYVALUEITEM WHERE NAME = 'TemPurchasePlanOrderFreightType' AND KEY = Tp.freighttype) as freighttype,
                                               tp.creatorname,
                                               tp.createtime,
                                               tp.modifiername,
                                               tp.modifytime,
                                               tp.abandonername,
                                               tp.abandontime,
                                               tp.submittername,
                                               tp.submittime,
                                               tp.approvername,
                                               tp.approvetime,
                                               tp.checkername,
                                               tp.checktime,
                                               tp.confirmer,
                                               tp.confirmetime,
                                               tp.rejector,
                                               tp.rejecttime,
                                               tp.RejectReason,
                                               tp.receiveaddress,
                                               tp.linker,
                                               tp.linkphone,
                                               td.sparepartcode,
                                               td.sparepartname,
                                               td.supplierpartcode,
                                               td.referencecode,
                                               td.planamount,
                                               td.measureunit,
                                               td.supliercode,
                                               td.supliername,
                                               td.memo,
                                               (SELECT VALUE FROM KEYVALUEITEM WHERE NAME = 'TemPurchasePlanOrderFreightType' AND KEY = Td.codesource) as codesource
                                          from TemPurchasePlanOrder tp
                                          join TemPurchasePlanOrderDetail td
                                            on tp.id = td.tempurchaseplanorderid
                                          where   (exists(select 1 from company  where tp.OrderCompanyId is not  null and  id=" + companyId + @") or exists(select 1  from AgencyDealerRelation ag where ag.DealerId=tp.ReceCompanyId and ag.AgencyId=" + companyId + "))");
                    var tableNames = sql.ToString().GetRegexNames();
                    var dbParameters = new List<DbParameter>();
                    if(ids != null && ids.Length > 0) {
                        sql.Append(@" and Tp.id in (");
                        for(int i = 0; i < ids.Length; i++) {
                            var tempIdWithNum = "tempIdWithNum" + i;
                            if(i == ids.Length - 1) {
                                var tempParam = "{0}" + tempIdWithNum;
                                sql.Append(tempParam);
                            } else {
                                var tempParam = "{0}" + tempIdWithNum + ",";
                                sql.Append(tempParam);
                            }
                            dbParameters.Add(db.CreateDbParameter(tempIdWithNum, ids[i]));
                        }
                        sql.Append(")");
                    } else {
                        if(!string.IsNullOrEmpty(code)) {
                            sql.Append(" and Tp.code like {0}code");
                            dbParameters.Add(db.CreateDbParameter("code", code));
                        }

                        if(status.HasValue) {
                            sql.Append(" and Tp.status={0}status");
                            dbParameters.Add(db.CreateDbParameter("status", status));
                        }
                        if(!string.IsNullOrEmpty(receCompanyCode)) {
                            sql.Append(" and Tp.receCompanyCode like {0}receCompanyCode");
                            dbParameters.Add(db.CreateDbParameter("receCompanyCode", "%" + receCompanyCode + "%"));
                        }
                        if(!string.IsNullOrEmpty(receCompanyName)) {
                            sql.Append(" and Tp.receCompanyName like {0}receCompanyName");
                            dbParameters.Add(db.CreateDbParameter("receCompanyName", "%" + receCompanyName + "%"));

                        }
                        if(createTimeBegin.HasValue) {
                            sql.Append(" and Tp.createTime>=to_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(" and Tp.createTime<=to_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                        if(!string.IsNullOrEmpty(sparePartCode)) {
                            sql.Append("and (Exists (Select 1 From TemPurchasePlanOrderDetail Where sparePartCode like {0}sparePartCode and tempurchaseplanorderid =Tp.Id))");
                            dbParameters.Add(db.CreateDbParameter("sparePartCode", "%" + sparePartCode + "%"));
                        }
                    }
                    DbCommand comm = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    comm.Parameters.AddRange(dbParameters.ToArray());
                    conn.Open();
                    var reader = comm.ExecuteReader();
                    using(var excelExport = new ExcelExport(filename)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0)
                                return new object[] {
                                    ErrorStrings.Export_Title_PartsSalesOrder_OrderNo, ErrorStrings.Export_Title_SupplierShippingOrder_ReveiveWarehouseName, ErrorStrings.Export_Title_AccountPeriod_Status, ErrorStrings.Export_TemPurchasePlanOrder_IsTurnSale, ErrorStrings.Export_TemPurchasePlanOrder_PlanType,
                                    ErrorStrings.Export_TemPurchasePlanOrder_ReceCompanyCode, ErrorStrings.Export_TemPurchasePlanOrder_ReceCompanyName,ErrorStrings.Export_TemPurchasePlanOrder_OrderCompanyCode,ErrorStrings.Export_TemPurchasePlanOrder_OrderCompanyName, ErrorStrings.Export_TemPurchasePlanOrder_ShippingMethod, ErrorStrings.Export_TemPurchasePlanOrder_IsReplace, ErrorStrings.Export_Title_PartsBranch_Remark, ErrorStrings.Export_TemPurchasePlanOrder_FreightType, ErrorStrings.Export_Title_AccountPeriod_CreatorName, ErrorStrings.Export_Title_AccountPeriod_CreateTime,
                                    ErrorStrings.Export_Title_AccountPeriod_ModifierName,ErrorStrings.Export_Title_AccountPeriod_ModifyTime,ErrorStrings.Export_Title_PartsBranch_AbandonerName, ErrorStrings.Export_Title_PartsBranch_AbandonerTime, ErrorStrings.Export_Title_PartsSalesOrder_SubmitterName,ErrorStrings.Export_Title_PartsSalesOrder_SubmitTime,
                                    ErrorStrings.Export_Title_Dealerpartsinventorybill_ApproverName, ErrorStrings.Export_Title_Dealerpartsinventorybill_ApproverTime, ErrorStrings.Export_TemPurchasePlanOrder_CheckerName, ErrorStrings.Export_TemPurchasePlanOrder_CheckerTime, ErrorStrings.Export_Title_InternalAllocationBill_ApproverName, ErrorStrings.Export_Title_InternalAllocationBill_ApproveTime,
                                    ErrorStrings.Export_Title_PartsOuterPurchaseChange_RejecterName,ErrorStrings.Export_Title_PartsOuterPurchaseChange_RejectTime,ErrorStrings.Export_Title_PartsOuterPurchaseChange_RejectComment,ErrorStrings.Export_Title_PartsPurchaseOrder_ReceiveAddress,ErrorStrings.Export_Title_Agency_LinkMan,ErrorStrings.Export_Title_Agency_LinkManMobile,ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode,ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName,
                                    ErrorStrings.Export_Title_SupplierShippingOrder_SupplierpartCode,ErrorStrings.Export_Title_Accessinfo_Tmp_ReferenceCode,ErrorStrings.Export_Title_PartsPurchasePlanDetail_PlanAmount,ErrorStrings.Export_Title_PartsPurchasePlanDetail_MeasureUnit,ErrorStrings.Export_Title_FactoryPurchacePrice_SupplierCode,ErrorStrings.Export_Title_FactoryPurchacePrice_SupplierName,ErrorStrings.Export_Title_PartsPurchaseOrder_DetailRemark,ErrorStrings.Export_TemPurchasePlanOrder_CodeSource
                                };
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }
        public bool ImportTemPurchasePlanOrderDetail(string fileName, out int excelImportNum, out List<TemPurchasePlanOrderDetailExtend> rightData, out List<TemPurchasePlanOrderDetailExtend> errorData, out string errorDataFileName, out string errorMessage) {

            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<TemPurchasePlanOrderDetailExtend>();
            var allList = new List<TemPurchasePlanOrderDetailExtend>();
            List<TemPurchasePlanOrderDetailExtend> rightList = null;
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("TemPurchasePlanOrderDetail", out notNullableFields, out fieldLenght);

                List<object> excelColumns;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    #region 指定文件中的列对应的名称 例如
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode, "SparePartCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName, "SparePartName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsPurchasePlanDetail_PlanAmount, "PlanAmount");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsPurchasePlanDetail_MeasureUnit, "MeasureUnit");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_ReferenceCode, "ReferenceCode");

                    #endregion

                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var tempImportObj = new TemPurchasePlanOrderDetailExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        #region 根据导入的字段给对应的实体属性赋值
                        tempImportObj.SparePartCodeStr = newRow["SparePartCode"];
                        tempImportObj.SparePartNameStr = newRow["SparePartName"];
                        tempImportObj.PlanAmountStr = newRow["PlanAmount"];
                        tempImportObj.MeasureUnitStr = newRow["MeasureUnit"];
                        tempImportObj.ReferenceCodeStr = newRow["ReferenceCode"];
                        #endregion

                        var tempErrorMessage = new List<string>();

                        #region 导入的数据基本检查
                        //配件图号检查
                        var fieldIndex = notNullableFields.IndexOf("SparePartCode".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.SparePartCodeStr)) {
                            if(fieldIndex > -1) {
                                tempErrorMessage.Add(ErrorStrings.OverstockPartsAppDetail__Validation1);
                            }
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.SparePartCodeStr) > fieldLenght["SparePartCode".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.ImpPartsSupplierRelation_Validation8);
                            else tempImportObj.SparePartCode = tempImportObj.SparePartCodeStr;
                        }
                        //配件名称检查
                        fieldIndex = notNullableFields.IndexOf("SparePartName".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.SparePartNameStr)) {
                            if(fieldIndex > -1) {
                                tempErrorMessage.Add("配件名称不能为空");
                            }
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.SparePartNameStr) > fieldLenght["SparePartName".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Sparepart_NameIsLong);
                            else tempImportObj.SparePartName = tempImportObj.SparePartNameStr;
                        }

                        if(!string.IsNullOrEmpty(tempImportObj.MeasureUnitStr)) {
                            tempImportObj.MeasureUnit = tempImportObj.MeasureUnitStr;
                        }
                        if(!string.IsNullOrEmpty(tempImportObj.ReferenceCodeStr)) {
                            tempImportObj.ReferenceCode = tempImportObj.ReferenceCodeStr;
                        }
                        //计划量检查
                        fieldIndex = notNullableFields.IndexOf("PlanAmount".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.PlanAmountStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsPurchasePlanDetail_PlanAmountIsNull);
                        } else {
                            int checkValue;
                            if(int.TryParse(tempImportObj.PlanAmountStr, out checkValue)) {
                                if(checkValue <= 0) {
                                    tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsPurchasePlanDetail_PlanAmountOverZero);
                                } else {
                                    tempImportObj.PlanAmount = checkValue;
                                }

                            } else {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsPurchasePlanDetail_PlanAmountNumber);
                            }
                        }
                        #endregion

                        if(tempErrorMessage.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });
                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    #region 剩下的数据进行业务检查
                    //文件内数据重复
                    var groups = tempRightList.GroupBy(r => new {
                        r.SparePartCodeStr
                    }).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        groupItem.ErrorMsg = ErrorStrings.Export_Validation_PartsPurchaseOrder_Validation2;
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    //1、配件有效性校验（配件信息.配件编号=导入数据.配件图号），
                    //如果校验不通过，记录校验错误信息：配件不存在。
                    var sparePartCodesNeedCheck = tempRightList.Select(r => r.SparePartCodeStr).Distinct().ToArray();
                    var dbSpareParts = new List<SparePartExtend>();
                    Func<string[], bool> getDbSpareParts = value => {
                        var dbObj = new SparePartExtend {
                            Id = Convert.ToInt32(value[0]),
                            Code = value[1],
                            Name = value[2],
                            MeasureUnit = value[3],
                            ReferenceCode = value[4]
                        };
                        dbSpareParts.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator(string.Format(@"select Part.Id,Part.Code,
                                                                                   Part.Name,
                                                                                   Part.MeasureUnit,                                                                                 
                                                                                   Part.ReferenceCode                                                                                  
                                                                              from SparePart Part                                                                            
                                                                             where Part.status = 1  "), "Code", true, sparePartCodesNeedCheck, getDbSpareParts);
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    foreach(var rightItem in tempRightList) {
                        var tempSparePart = dbSpareParts.FirstOrDefault(r => r.Code == rightItem.SparePartCodeStr);
                        if(tempSparePart != null) {
                            rightItem.SparePartId = tempSparePart.Id;
                            rightItem.SparePartCode = tempSparePart.Code;
                            rightItem.SparePartName = tempSparePart.Name;
                            rightItem.MeasureUnit = tempSparePart.MeasureUnit;
                            rightItem.ReferenceCode = tempSparePart.ReferenceCode;
                        }
                    }

                    //获取所有不合格数据
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    //获取所有合格数据
                    rightList = allList.Except(errorList).ToList();
                    #endregion
                }
                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempObj = list[index - 1];
                            var values = new object[] {
                                #region 设置错误信息导出的列的值
                                tempObj.SparePartCodeStr, tempObj.SparePartNameStr,
                                 tempObj.PlanAmountStr,
                                tempObj.MeasureUnitStr,tempObj.ReferenceCodeStr, tempObj.ErrorMsg
                                
                                #endregion
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }
        public bool Import临时采购订单确认导入供应商(string fileName, out int excelImportNum, out List<TemPurchasePlanOrderDetailExtend> rightData, out List<TemPurchasePlanOrderDetailExtend> errorData, out string errorDataFileName, out string errorMessage) {

            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<TemPurchasePlanOrderDetailExtend>();
            var allList = new List<TemPurchasePlanOrderDetailExtend>();
            List<TemPurchasePlanOrderDetailExtend> rightList = null;
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("TemPurchasePlanOrderDetail", out notNullableFields, out fieldLenght);

                List<object> excelColumns;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    #region 指定文件中的列对应的名称 例如
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode, "SparePartCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName, "SparePartName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_FactoryPurchacePrice_SupplierCode, "SuplierCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_FactoryPurchacePrice_SupplierName, "SuplierName");

                    #endregion

                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var tempImportObj = new TemPurchasePlanOrderDetailExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        #region 根据导入的字段给对应的实体属性赋值
                        tempImportObj.SparePartCodeStr = newRow["SparePartCode"];
                        tempImportObj.SparePartNameStr = newRow["SparePartName"];
                        tempImportObj.SuplierCodeStr = newRow["SuplierCode"];
                        tempImportObj.SuplierNameStr = newRow["SuplierName"];
                        #endregion

                        var tempErrorMessage = new List<string>();

                        #region 导入的数据基本检查
                        //配件图号检查
                        var fieldIndex = notNullableFields.IndexOf("SparePartCode".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.SparePartCodeStr)) {
                            if(fieldIndex > -1) {
                                tempErrorMessage.Add(ErrorStrings.OverstockPartsAppDetail__Validation1);
                            }
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.SparePartCodeStr) > fieldLenght["SparePartCode".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.ImpPartsSupplierRelation_Validation8);
                        }

                        //供应商编号检查
                        fieldIndex = notNullableFields.IndexOf("SuplierCode".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.SuplierCodeStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PersonnelSupplierRelation_SupplierCode);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.SuplierCodeStr) > fieldLenght["SuplierCode".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PersonnelSupplierRelation_SupplierCodeIsLong);
                        }
                        #endregion

                        if(tempErrorMessage.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });
                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    #region 剩下的数据进行业务检查
                    //文件内数据重复
                    var groups = tempRightList.GroupBy(r => new {
                        r.SparePartCodeStr
                    }).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        groupItem.ErrorMsg = ErrorStrings.Export_Validation_PartsPurchaseOrder_Validation2;
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    //1、配件有效性校验（配件信息.配件编号=导入数据.配件图号），
                    //如果校验不通过，记录校验错误信息：配件不存在。
                    var sparePartCodesNeedCheck = tempRightList.Select(r => r.SparePartCodeStr).Distinct().ToArray();
                    var dbSpareParts = new List<SparePartExtend>();
                    Func<string[], bool> getDbSpareParts = value => {
                        var dbObj = new SparePartExtend {
                            Id = Convert.ToInt32(value[0]),
                            Code = value[1],
                            Name = value[2],
                            MeasureUnit = value[3],
                            ReferenceCode = value[4]
                        };
                        dbSpareParts.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator(string.Format(@"select Part.Id,Part.Code,
                                                                                   Part.Name,
                                                                                   Part.MeasureUnit,                                                                                 
                                                                                   Part.ReferenceCode                                                                                  
                                                                              from SparePart Part                                                                            
                                                                             where Part.status = 1  "), "Code", true, sparePartCodesNeedCheck, getDbSpareParts);
                    var errorSpareParts = tempRightList.Where(r => dbSpareParts.All(v => v.Code != r.SparePartCodeStr)).ToList();

                    foreach(var errorItem in errorSpareParts) {
                        errorItem.ErrorMsg = ErrorStrings.Export_Validation_InternalAcquisitionDetail_Validation1;
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    foreach(var rightItem in tempRightList) {
                        var tempSparePart = dbSpareParts.FirstOrDefault(r => r.Code == rightItem.SparePartCodeStr);
                        rightItem.SparePartId = tempSparePart.Id;
                        rightItem.SparePartCode = tempSparePart.Code;
                        rightItem.SparePartName = tempSparePart.Name;
                        rightItem.ReferenceCode = tempSparePart.ReferenceCode;
                        // 查询配件与供应商关系
                        var spareSuplierCodeNeedCheck = tempRightList.Where(t => t.SparePartCode == rightItem.SparePartCode).Select(r => r.SuplierCodeStr).Distinct().ToArray();
                        var dbSuplierCode = new List<PartsSupplierRelationExtend>();
                        Func<string[], bool> getDbSuplierCode = value => {
                            var dbObj = new PartsSupplierRelationExtend {
                                Id = Convert.ToInt32(value[0]),
                                SupplierId = Convert.ToInt32(value[1]),
                                SupplierPartCode = value[2],
                                SupplierCode = value[3],
                                SupplierName = value[4]
                            };
                            dbSuplierCode.Add(dbObj);
                            return false;
                        };
                        db.QueryDataWithInOperator(string.Format(@"select Part.Id,Part.SupplierId,
                                                                                   Part.SupplierPartCode,
                                                                                   ps.Code,                                                                                 
                                                                                   ps.Name                                                                                  
                                                                              from   PartsSupplierRelation Part    join   PartsSupplier   ps on part.SupplierId=ps.id                                                                          
                                                                             where Part.status = 1 and part.PartId={0} ", rightItem.SparePartId), "ps.Code", true, spareSuplierCodeNeedCheck, getDbSuplierCode);
                            var tempSpareParts = dbSuplierCode.FirstOrDefault();
                            if(tempSpareParts!=null) {
                                rightItem.SuplierId = tempSpareParts.SupplierId;
                                rightItem.SuplierName = tempSpareParts.SupplierName;
                                rightItem.SuplierCode = tempSpareParts.SupplierCode;
                               // rightItem.SupplierPartCode = tempSpareParts.SupplierPartCode;
                            } else {
                                var dbSuplier = new List<PartsSupplierRelationExtend>();
                                Func<string[], bool> getDbSuplier = value => {
                                    var dbObj = new PartsSupplierRelationExtend {
                                        SupplierId = Convert.ToInt32(value[0]),
                                        SupplierCode = value[1],
                                        SupplierName = value[2]
                                    };
                                    dbSuplier.Add(dbObj);
                                    return false;
                                };
                                db.QueryDataWithInOperator(string.Format(@"select ps.id,
                                                                                   ps.Code,                                                                                 
                                                                                   ps.Name                                                                                  
                                                                              from     PartsSupplier   ps                                                                         
                                                                             where ps.status = 1  "), "ps.Code", true, spareSuplierCodeNeedCheck, getDbSuplier);
                                if(dbSuplierCode.Count() == 0) {
                                    rightItem.ErrorMsg = spareSuplierCodeNeedCheck+"供应商不存在";
                                } else {
                                    var tempPartsSupplier = dbSuplier.First();
                                    rightItem.SuplierId = tempPartsSupplier.SupplierId;
                                    rightItem.SuplierName = tempPartsSupplier.SupplierName;
                                    rightItem.SuplierCode = tempPartsSupplier.SupplierCode;
                                }
                            }
                                                    
                    }

                    //获取所有不合格数据
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    //获取所有合格数据
                    rightList = allList.Except(errorList).ToList();
                    #endregion
                }
                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempObj = list[index - 1];
                            var values = new object[] {
                                #region 设置错误信息导出的列的值
                                tempObj.SparePartCodeStr, tempObj.SparePartNameStr,
                                 tempObj.SuplierCodeStr,
                                tempObj.SuplierNameStr, tempObj.ErrorMsg
                                
                                #endregion
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }
    }
}
