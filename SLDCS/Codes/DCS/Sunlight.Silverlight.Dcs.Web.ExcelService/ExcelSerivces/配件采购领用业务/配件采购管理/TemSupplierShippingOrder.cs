﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService
{
    public partial class ExcelService
    {
        public bool Export临时采购发运单(int[] ids, int? suplierId, string partsSupplierCode, string temPurchaseOrderCode, string partsSupplierName, string code, int? status, string receivingWarehouseName, DateTime? createTimeBegin, DateTime? createTimeEnd, out string filename)
        {
            filename = GetExportFilePath("临时采购发运单清单_" + ".xlsx");
            if (!HttpContext.Current.User.Identity.IsAuthenticated)
            {
                throw new Exception(ErrorStrings.File_Validation_User);
            }
            try
            {
                var db = DbHelper.GetDbHelp(ConnectionString);
                using (var conn = db.CreateDbConnection())
                {
                    var sql = new StringBuilder();
                    sql.Append(@"select tod.code ,/*发运单号*/
                                 tod.tempurchaseordercode,/*临时订单号*/
                                       tod.partssuppliercode,/*供应商编号*/
                                       tod.partssuppliername,/*供应商名称*/
                                       tod.ordercompanycode,/*订货企业编号*/
                                       tod.ordercompanyname,/*订货企业名称*/
                                       tod.receivingwarehousename,/*收货仓库*/
                                       tod.receivingcompanyname,/*收货企业名称*/
                                       tod.receivingcompanycode,/*收货企业编号*/
                                       tod.receivingaddress,/*收货地址*/
                                       tod.originalrequirementbillcode,/*原始单据编号*/
                                       tod.logisticcompany,/*物流公司*/
                                       tod.phone,/*电话*/
                                       tod.vehiclelicenseplate,/*车牌*/
                                       tod.driver,/*司机*/
                                       (select value from keyvalueitem kv where kv.key =tod.shippingmethod and kv.name='TemPurchasePlanOrderShippingMethod'),/*发运方式*/
                                       tod.deliverybillnumber,/*送货单号*/
                                       tod.totalweight,/*总重量*/
                                       tod.totalvolume,/*总体积*/
                                        (select value from keyvalueitem kv where kv.key =tod.status and kv.name='TemSupplierShippingOrderStatus'),/*订单状态*/
                                       tod.shippingdate,/*发运日期*/
                                       tod.plandeliverytime,/*预计到货时间*/
                                       tod.arrivaldate,/*抵达时间*/
                                       tod.logisticarrivaldate,/*承运商送达时间*/
                                       tod.confirmorname,/*确认人*/
                                       tod.confirmationtime,/*确认时间*/
                                       tod.firstconfirmationtime,/*第一次确认时间*/
                                       tod.remark,/*备注*/
                                       tod.creatorname,/*创建人*/
                                       tod.createtime,/*创建时间*/
                                       tod.modifiername,/*修改人*/
                                       tod.modifytime,/*修改时间*/
                                       tod.closername,/*终止人*/
                                       tod.closetime,/*终止时间*/     
                                       tod.forcename,/*强制完成人*/
                                       tod.forcetime,/*强制完成时间*/
                                       top.sparepartcode,/*配件编号*/
                                       top.sparepartname,/*配件名称*/
                                       top.supplierpartcode,/*供应商图号*/
                                       top.measureunit,/*计量单位*/
                                       top.quantity,/*数量*/
                                       top.confirmedamount,/*确认量*/
                                       top.remark,/*清单备注*/
                                       top.weight,/*重量*/
                                       top.volume/*体积*/

                                  from TemSupplierShippingOrder tod
                                  join TemShippingOrderDetail top
                                    on tod.id = top.shippingorderid where 1=1 ");
                    var tableNames = sql.ToString().GetRegexNames();
                    var dbParameters = new List<DbParameter>();
                    if (ids != null && ids.Length > 0)
                    {
                        sql.Append(@" and tod.id in (");
                        for (int i = 0; i < ids.Length; i++)
                        {
                            var tempIdWithNum = "tempIdWithNum" + i;
                            if (i == ids.Length - 1)
                            {
                                var tempParam = "{0}" + tempIdWithNum;
                                sql.Append(tempParam);
                            }
                            else
                            {
                                var tempParam = "{0}" + tempIdWithNum + ",";
                                sql.Append(tempParam);
                            }
                            dbParameters.Add(db.CreateDbParameter(tempIdWithNum, ids[i]));
                        }
                        sql.Append(")");
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(code))
                        {
                            sql.Append(" and tod.code like {0}code");
                            dbParameters.Add(db.CreateDbParameter("code", code));
                        }
                        if (suplierId.HasValue)
                        {
                            sql.Append(" and tod.PartsSupplierId={0}suplierId");
                            dbParameters.Add(db.CreateDbParameter("suplierId", suplierId));
                        }
                        if (!string.IsNullOrEmpty(temPurchaseOrderCode))
                        {
                            sql.Append(" and tod.temPurchaseOrderCode like {0}temPurchaseOrderCode");
                            dbParameters.Add(db.CreateDbParameter("temPurchaseOrderCode", temPurchaseOrderCode));
                        }
                        if (!string.IsNullOrEmpty(partsSupplierCode))
                        {
                            sql.Append(" and tod.partsSupplierCode like {0}partsSupplierCode");
                            dbParameters.Add(db.CreateDbParameter("partsSupplierCode", partsSupplierCode));
                        }
                        if (!string.IsNullOrEmpty(partsSupplierName))
                        {
                            sql.Append(" and tod.partsSupplierName like {0}partsSupplierName");
                            dbParameters.Add(db.CreateDbParameter("partsSupplierName", partsSupplierName));
                        }
                        if (!string.IsNullOrEmpty(receivingWarehouseName))
                        {
                            sql.Append(" and tod.ReceivingWarehouseName like {0}receivingWarehouseName");
                            dbParameters.Add(db.CreateDbParameter("receivingWarehouseName", receivingWarehouseName));
                        }
                        if (status.HasValue)
                        {
                            sql.Append(" and tod.status={0}status");
                            dbParameters.Add(db.CreateDbParameter("status", status));
                        }

                        if (createTimeBegin.HasValue)
                        {
                            sql.Append(" and tod.createTime>=to_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if (createTimeEnd.HasValue)
                        {
                            sql.Append(" and tod.createTime<=to_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                    }
                    sql.Append(" order by tod.code");
                    DbCommand comm = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    comm.Parameters.AddRange(dbParameters.ToArray());
                    conn.Open();
                    var reader = comm.ExecuteReader();
                    using (var excelExport = new ExcelExport(filename))
                    {
                        excelExport.ExportByRow(index =>
                        {
                            if (index == 0)
                                return tableNames;
                            if (reader.Read())
                            {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if (num != reader.FieldCount)
                                {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool Export临时采购发运单确认(int[] ids, int? orderCompanyId, string originalRequirementBillCode, string temPurchaseOrderCode, string code, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, out string filename)
        {
            filename = GetExportFilePath("临时采购发运单清单_" + ".xlsx");
            if (!HttpContext.Current.User.Identity.IsAuthenticated)
            {
                throw new Exception(ErrorStrings.File_Validation_User);
            }
            try
            {
                var userInfo = Utils.GetCurrentUserInfo();
                var db = DbHelper.GetDbHelp(ConnectionString);
                using (var conn = db.CreateDbConnection())
                {
                    var sql = new StringBuilder();
                    sql.Append(@"select tod.code ,/*发运单号*/
                                  tod.tempurchaseordercode,/*临时订单号*/
                                       tod.ordercompanycode,/*订货企业编号*/
                                       tod.ordercompanyname,/*订货企业名称*/
                                       tod.receivingwarehousename,/*收货仓库*/
                                       tod.receivingcompanyname,/*收货企业名称*/
                                       tod.receivingcompanycode,/*收货企业编号*/
                                       tod.receivingaddress,/*收货地址*/
                                       tod.originalrequirementbillcode,/*临时计划单号*/
                                       tod.logisticcompany,/*物流公司*/
                                       tod.phone,/*电话*/
                                       tod.vehiclelicenseplate,/*车牌*/
                                       tod.driver,/*司机*/
                                       (select value from keyvalueitem kv where kv.key =tod.shippingmethod and kv.name='TemPurchasePlanOrderShippingMethod'),/*发运方式*/
                                       tod.deliverybillnumber,/*送货单号*/
                                        (select value from keyvalueitem kv where kv.key =tod.status and kv.name='TemSupplierShippingOrderStatus'),/*状态*/
                                       tod.shippingdate,/*发运日期*/
                                       tod.ConfirmorName,/*确认人*/
                                       tod.arrivaldate,/*抵达时间*/
                                       tod.remark,/*备注*/
                                       tod.creatorname,/*创建人*/
                                       tod.createtime,/*创建时间*/
                                       top.sparepartcode,/*配件编号*/
                                       top.sparepartname,/*配件名称*/
                                       top.supplierpartcode,/*供应商图号*/
                                       top.measureunit,/*计量单位*/
                                       top.quantity,/*数量*/
                                       top.confirmedamount,/*确认量*/
                                       top.remark,/*清单备注*/
                                       top.weight,/*重量*/
                                       top.volume/*体积*/

                                  from TemSupplierShippingOrder tod
                                  join TemShippingOrderDetail top
                                    on tod.id = top.shippingorderid  where tod.OrderCompanyId=" + userInfo.EnterpriseId);
                    var tableNames = sql.ToString().GetRegexNames();
                    var dbParameters = new List<DbParameter>();
                    if (ids != null && ids.Length > 0)
                    {
                        sql.Append(@" and tod.id in (");
                        for (int i = 0; i < ids.Length; i++)
                        {
                            var tempIdWithNum = "tempIdWithNum" + i;
                            if (i == ids.Length - 1)
                            {
                                var tempParam = "{0}" + tempIdWithNum;
                                sql.Append(tempParam);
                            }
                            else
                            {
                                var tempParam = "{0}" + tempIdWithNum + ",";
                                sql.Append(tempParam);
                            }
                            dbParameters.Add(db.CreateDbParameter(tempIdWithNum, ids[i]));
                        }
                        sql.Append(")");
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(code))
                        {
                            sql.Append(" and tod.code like {0}code");
                            dbParameters.Add(db.CreateDbParameter("code", code));
                        }

                        if (!string.IsNullOrEmpty(temPurchaseOrderCode))
                        {
                            sql.Append(" and tod.temPurchaseOrderCode like {0}temPurchaseOrderCode");
                            dbParameters.Add(db.CreateDbParameter("temPurchaseOrderCode", temPurchaseOrderCode));
                        }
                        if (!string.IsNullOrEmpty(originalRequirementBillCode))
                        {
                            sql.Append(" and tod.OriginalRequirementBillCode like {0}OriginalRequirementBillCode");
                            dbParameters.Add(db.CreateDbParameter("OriginalRequirementBillCode", originalRequirementBillCode));
                        }

                        if (status.HasValue)
                        {
                            sql.Append(" and tod.status={0}status");
                            dbParameters.Add(db.CreateDbParameter("status", status));
                        }

                        if (createTimeBegin.HasValue)
                        {
                            sql.Append(" and tod.createTime>=to_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if (createTimeEnd.HasValue)
                        {
                            sql.Append(" and tod.createTime<=to_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                    }
                    sql.Append(" order by tod.code");
                    DbCommand comm = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    comm.Parameters.AddRange(dbParameters.ToArray());
                    conn.Open();
                    var reader = comm.ExecuteReader();
                    using (var excelExport = new ExcelExport(filename))
                    {
                        excelExport.ExportByRow(index =>
                        {
                            if (index == 0)
                                return new object[] {
                                    ErrorStrings.Export_Title_Partsoutboundplan_ShippingOrder, ErrorStrings.Export_Title_TemPurchaseOrderCode_Code, ErrorStrings.Export_TemPurchasePlanOrder_OrderCompanyCode, ErrorStrings.Export_TemPurchasePlanOrder_OrderCompanyName, ErrorStrings.Export_Title_SupplierShippingOrder_ReveiveWarehouseName,
                                    ErrorStrings.Export_TemPurchasePlanOrder_ReceCompanyCode, ErrorStrings.Export_TemPurchasePlanOrder_ReceCompanyName, ErrorStrings.Export_Title_PartsPurchaseOrder_ReceiveAddress, ErrorStrings.Export_Title_TemPurchasePlanOrder_Code, ErrorStrings.Export_Title_SupplierShippingOrder_LogisticCompany, ErrorStrings.Export_Title_SupplierShippingOrder_DriverPhone, ErrorStrings.Export_Title_SupplierShippingOrder_VehicleLicensePlate, ErrorStrings.Export_Title_SupplierShippingOrder_Driver,
                                    ErrorStrings.Export_TemPurchasePlanOrder_ShippingMethod,ErrorStrings.Export_Title_SupplierShippingOrder_DdeliverybillNumber, ErrorStrings.Export_Title_AccountPeriod_Status, ErrorStrings.Export_Title_SupplierShippingOrder_ShippingDates, ErrorStrings.Export_Title_SupplierShippingOrder_SrrivalDate,ErrorStrings.Export_Title_PartsBranch_Remark,ErrorStrings.Export_Title_AccountPeriod_CreatorName,ErrorStrings.Export_Title_AccountPeriod_CreateTime,
                                    ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode,ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName,ErrorStrings.Export_Title_SupplierShippingOrder_SupplierpartCode, ErrorStrings.Export_Title_PackingPropertyApp_MeasureUnit, ErrorStrings.Export_Title_WarehouseArea_Quantity, ErrorStrings.Export_Title_PartsPurchaseOrder_ConfirmedAmount,ErrorStrings.Export_Title_PartsPurchaseOrder_DetailRemark,ErrorStrings.Export_Title_PartsShippingOrder_Weight,ErrorStrings.Export_Title_Sparepart_Volume
                                };
                            if (reader.Read())
                            {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if (num != reader.FieldCount)
                                {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool 导出临时采购计划确认时的清单(int id, out string filename) {
            filename = GetExportFilePath("临时采购计划确认清单_" + ".xlsx");
            if(!HttpContext.Current.User.Identity.IsAuthenticated) {
                throw new Exception(ErrorStrings.File_Validation_User);
            }
            try {
                var db = DbHelper.GetDbHelp(ConnectionString);
                using(var conn = db.CreateDbConnection()) {
                    var sql = new StringBuilder();
                    sql.Append(@" select b.SparePartCode,b.SparePartName from TemPurchasePlanOrder a join TemPurchasePlanOrderDetail b on a.id =b.TemPurchasePlanOrderId where a.id=" + id);
                    var tableNames = sql.ToString().GetRegexNames();
                    var dbParameters = new List<DbParameter>();
                    DbCommand comm = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    conn.Open();
                    var reader = comm.ExecuteReader();
                    using(var excelExport = new ExcelExport(filename)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0)
                                return new object[] {
                                    ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode,ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName,"供应商编号","供应商名称"                                  
                                };
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }
    }
}
