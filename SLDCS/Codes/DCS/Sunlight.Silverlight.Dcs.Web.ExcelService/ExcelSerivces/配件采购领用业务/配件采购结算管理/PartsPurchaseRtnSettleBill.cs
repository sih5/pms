﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Globalization;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        /// <summary>
        /// 导出采购退货结算单主清单
        /// </summary>
        public bool ExportPartsPurchaseRtnSettleBillandDetail(int[] partsPurchaseRtnSettleBillId, string code, string suppliercode, string suppliername, int? warehouseid, int? status, int? partsSalesCategoryId, DateTime? createTimeBegin, DateTime? createTimeEnd, string businessCode, DateTime? invoiceDateBegin, DateTime? invoiceDateEnd, out string fileName)
        {
            fileName = GetExportFilePath("采购退货结算单主清单_" + ".xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var userinfo = Utils.GetCurrentUserInfo();
                var branchId = userinfo.EnterpriseId;
                var db = DbHelper.GetDbHelp(ConnectionString);
                using(var conn = db.CreateDbConnection()) {
                    var sql = new StringBuilder();
                    sql.AppendFormat(@" SELECT  a.Code, --结算单编号
                                                a.BranchName, --分公司名称
                                                a.WarehouseName, --仓库名称
                                                a.PartsSalesCategoryName, --品牌
                                                a.PartsSupplierCode, --供应商编号
                                                a.PartsSupplierName, --供应商名称
                                                a.TotalSettlementAmount, --结算总金额
                                                (SELECT Sum(CASE WHEN Extent5.InvoiceAmount IS NULL THEN 0 ELSE Extent5.InvoiceAmount END) AS A1
                                                FROM  PartsPurchaseRtnSettleBill Extent4
                                                LEFT OUTER JOIN InvoiceInformation Extent5 ON (Extent5.SourceType = 4) AND (Extent4.Id = Extent5.SourceId)
                                                WHERE a.Id = Extent4.Id) AS  InvoiceTotalAmount,--开票总金额
                                                a.TaxRate, --税率
                                                round(a.Tax,2), --税额
                                                a.InvoiceAmountDifference, --开票金额差异
                                                 (a.TotalSettlementAmount+a.Tax) as TotalAmount,--含税总金额
                                                --a.Status, --状态
                                                (select value from keyvalueitem where NAME = 'PartsPurchaseSettle_Status'and key=a.status) As Status,
                                                a.InvoiceDate,  --过账日期
                                                (select value from keyvalueitem where NAME = 'PartsPurchaseRtnSettleBill_InvoicePath'and key=a.InvoicePath) As InvoicePath, --开票方向
                                                (select value from keyvalueitem where NAME = 'PartsPurchaseSettleBill_SettlementPath'and key=a.SettlementPath) As SettlementPath, --结算方向
                                                a.Remark, --主单备注
                                                a.CreatorName, --创建人
                                                a.CreateTime, --创建时间
                                                b.sparepartcode, --配件编号
                                                b.sparepartname, --配件名称
                                                b.settlementprice, --结算价
                                                b.QuantityToSettle, --结算数量
                                                b.Settlementamount, -- 结算金额
                                                b.remark, --清单备注
                                                nvl(c.businesscode, ''), --业务编号
                                                pf.sourcecode --出库单号
                                            FROM PartsPurchaseRtnSettleBill a
                                            INNER JOIN partspurchasertnsettledetail b on a.id =
                                                                                        b.partspurchasertnsettlebillid
                                            left join BranchSupplierRelation c on c.partssalescategoryid =
                                                                                a.partssalescategoryid
                                                                            and c.supplierid = a.partssupplierid
                                                                            and c.status = 1
                                             join PartsPurchaseRtnSettleRef pf
                                                on a.id = pf.partspurchasertnsettlebillid
                                              join partsoutboundbilldetail pb
                                                on pf.sourceid = pb.partsoutboundbillid
                                               and b.sparepartid = pb.sparepartid
                                               and b.QuantityToSettle =pb.outboundamount 
                                               and b.settlementprice =pb.settlementprice
                                            WHERE a.BranchId = {0}", branchId
                                    );
                    var dbParameters = new List<DbParameter>();
                    if(partsPurchaseRtnSettleBillId != null && partsPurchaseRtnSettleBillId.Length > 0) {
                        sql.Append(" and a.id in (");
                        for(var i = 0; i < partsPurchaseRtnSettleBillId.Length; i++) {
                            if(partsPurchaseRtnSettleBillId.Length == i + 1) {
                                sql.Append("{0}id" + partsPurchaseRtnSettleBillId[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter("id" + partsPurchaseRtnSettleBillId[i].ToString(CultureInfo.InvariantCulture), partsPurchaseRtnSettleBillId[i]));
                            } else {
                                sql.Append("{0}id" + partsPurchaseRtnSettleBillId[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter("id" + partsPurchaseRtnSettleBillId[i].ToString(CultureInfo.InvariantCulture), partsPurchaseRtnSettleBillId[i]));
                            }
                        }
                        sql.Append(")");
                    } else {
                        #region 条件过滤
                        if(!string.IsNullOrEmpty(code)) {
                            sql.Append(" and a.code like {0}code ");
                            dbParameters.Add(db.CreateDbParameter("code", "%" + code + "%"));
                        }
                        if(!string.IsNullOrEmpty(suppliercode)) {
                            sql.Append(" and a.suppliercode like {0}suppliercode ");
                            dbParameters.Add(db.CreateDbParameter("suppliercode", "%" + suppliercode + "%"));
                        }
                        if(!string.IsNullOrEmpty(suppliername)) {
                            sql.Append(" and a.suppliername like {0}suppliername ");
                            dbParameters.Add(db.CreateDbParameter("suppliername", "%" + suppliername + "%"));
                        }
                        if(warehouseid.HasValue) {
                            sql.Append(" and a.warehouseid = {0}warehouseid ");
                            dbParameters.Add(db.CreateDbParameter("warehouseid", warehouseid.Value));
                        }
                        if(status.HasValue) {
                            sql.Append(" and a.status={0}status");
                            dbParameters.Add(db.CreateDbParameter("status", status.Value));
                        }
                        if(partsSalesCategoryId.HasValue) {
                            sql.Append(" and a.partsSalesCategoryId={0}partsSalesCategoryId");
                            dbParameters.Add(db.CreateDbParameter("partsSalesCategoryId", partsSalesCategoryId.Value));
                        }
                        if(createTimeBegin.HasValue) {
                            sql.Append(@" and a.createTime >=to_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(@" and a.createTime <=to_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                        if (invoiceDateBegin.HasValue)
                        {
                            sql.Append(@" and a.InvoiceDate >=to_date({0}invoiceDateBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = invoiceDateBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("invoiceDateBegin", tempTime.ToString("G")));
                        }
                        if (invoiceDateEnd.HasValue)
                        {
                            sql.Append(@" and a.InvoiceDate <=to_date({0}invoiceDateEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = invoiceDateEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("invoiceDateEnd", tempTime.ToString("G")));
                        }
                        if(!string.IsNullOrEmpty(businessCode)) {
                            sql.Append(" and c.businesscode like {0}businesscode ");
                            dbParameters.Add(db.CreateDbParameter("businesscode", "%" + businessCode + "%"));
                        }
                        #endregion
                    }
                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    conn.Open();
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
ErrorStrings.Export_Title_PartsPurchaseRtnSettleBill_Code,ErrorStrings.Export_Title_Branch_BranchName,ErrorStrings.Export_Title_Company_WarehouseName,ErrorStrings.Export_Title_Partssalescategory_Name,ErrorStrings.Export_Title_FactoryPurchacePrice_SupplierCode,ErrorStrings.Export_Title_FactoryPurchacePrice_SupplierName,ErrorStrings.Export_Title_PartsPurchaseRtnSettleBill_TotalSettlementAmount,
ErrorStrings.Export_Title_PartsPurchaseRtnSettleBill_InvoiceTotalAmount,ErrorStrings.Export_Title_PartsPurchaseRtnSettleBill_TaxRate,ErrorStrings.Export_Title_PartsPurchaseRtnSettleBill_Tax,ErrorStrings.Export_Title_PartsPurchaseRtnSettleBill_InvoiceAmountDifference,ErrorStrings.Export_Title_PartsPurchaseRtnSettleBill_TotalAmount,ErrorStrings.Export_Title_AccountPeriod_Status,ErrorStrings.Export_Title_Customeraccount_InvoiceDate,
ErrorStrings.Export_Title_PartsPurchaseRtnSettleBill_InvoicePath,ErrorStrings.Export_Title_PartsPurchaseRtnSettleBill_SettlementPath,ErrorStrings.Export_Title_PartsPurchaseOrder_OrderRemark,ErrorStrings.Export_Title_AccountPeriod_CreatorName,ErrorStrings.Export_Title_AccountPeriod_CreateTime,ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode,ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName,ErrorStrings.Export_Title_PartsPurchaseRtnSettleBill_SettlementPrice,ErrorStrings.Export_Title_PartsPurchaseRtnSettleBill_QuantityToSettle,ErrorStrings.Export_Title_PartsPurchaseRtnSettleBill_SettlementAmount,ErrorStrings.Export_Title_PartsPurchaseOrder_DetailRemark,ErrorStrings.Export_Title_BranchSupplierRelation_BusinessCode,"关联单号"
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }
    }
}
