﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        /// <summary>
        /// 根据关联单导出配件采购结算单清单
        /// </summary>
        public bool ExportPartsPurchaseSettleDetailByRefId(int[] inboundIds, int[] outboundIds, int? partsSalesCategoryId, int? partsSupplierId, out string fileName) {
            fileName = GetExportFilePath("配件采购结算单清单_.xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    var dbParameters = new List<DbParameter>();
                    if(!partsSalesCategoryId.HasValue) {
                        var sql1 = "";
                        var sql2 = "";
                        if(inboundIds != null && inboundIds.Length > 0) {
                            sql1 = (@" (Select a.Sparepartid        As Sparepartid,
                                                a.Sparepartcode      As Sparepartcode,
                                                a.Sparepartname      As Sparepartname,
                                                a.Settlementprice    As Settlementprice,
                                                -a.Inspectedquantity As Quantitytosettle,
                                                -(a.Settlementprice * a.Inspectedquantity) As Settlementamount
                                           From Partsinboundcheckbilldetail a
                                           Join Partsinboundcheckbill b
                                             On a.Partsinboundcheckbillid = b.Id) where b.id in (");
                            for(var i = 0; i < inboundIds.Length; i++) {
                                if(inboundIds.Length == i + 1) {
                                    sql1 += string.Format("{0})) ", inboundIds[i]);
                                } else {
                                    sql1 += string.Format("{0},", inboundIds[i]);
                                }
                            }
                        }
                        if(outboundIds != null && outboundIds.Length > 0) {
                            sql2 = (@" (Select a.Sparepartid     As Sparepartid,
                                              a.Sparepartcode   As Sparepartcode,
                                              a.Sparepartname   As Sparepartname,
                                              a.Settlementprice As Settlementprice,
                                              a.Outboundamount  As Quantitytosettle,
                                            (a.Settlementprice * a.Outboundamount) As Settlementamount
                                         From Partsoutboundbilldetail a
                                         Join Partsoutboundbill b
                                           On a.Partsoutboundbillid = b.Id) where b.id in (");
                            for(var i = 0; i < outboundIds.Length; i++) {
                                if(outboundIds.Length == i + 1) {
                                    sql2 += string.Format("{0})) ", outboundIds[i]);
                                } else {
                                    sql2 += string.Format("{0},", outboundIds[i]);
                                }
                            }
                        }
                        if(sql1 != "" && sql2 != "") {
                            sql.AppendFormat("{0} Union {1}", sql1, sql2);
                        } else {
                            if(sql1 != "") {
                                sql.Append(sql1);
                            }
                            if(sql2 != "") {
                                sql.Append(sql2);
                            }
                        }
                    } else {
                        var sql1 = "";
                        var sql2 = "";
                        if(inboundIds != null && partsSupplierId.HasValue && inboundIds.Length > 0) {
                            sql1 = string.Format(@" (Select --a.Sparepartid        As Sparepartid,
                a.Sparepartcode      As Sparepartcode,
                a.Sparepartname      As Sparepartname,
                Decode(a.Settlementprice,0,c.PurchasePrice)    As Settlementprice,
                a.Inspectedquantity As Quantitytosettle,
                (a.Settlementprice * a.Inspectedquantity) As Settlementamount
           From Partsinboundcheckbilldetail a
           Join Partsinboundcheckbill b
             On a.Partsinboundcheckbillid = b.Id
                         Join PartsPurchasePricing c
                         On c.partssupplierid={0}
                         Where a.inspectedquantity!=0 And c.partssalescategoryid={1}
                         And c.validfrom<=b.createtime And c.validto>=b.createtime And a.sparepartid=c.partid and b.id in (", partsSupplierId.Value, partsSalesCategoryId.Value);
                            for(var i = 0; i < inboundIds.Length; i++) {
                                if(inboundIds.Length == i + 1) {
                                    sql1 += string.Format("{0})) ", inboundIds[i]);
                                } else {
                                    sql1 += string.Format("{0},", inboundIds[i]);
                                }
                            }
                        }
                        if(outboundIds != null && partsSupplierId.HasValue && outboundIds.Length > 0) {
                            sql2 = string.Format(@" (Select --a.Sparepartid     As Sparepartid,
                      a.Sparepartcode   As Sparepartcode,
                      a.Sparepartname   As Sparepartname,
                      Decode(a.Settlementprice,0,c.PurchasePrice) As Settlementprice,
                      -a.Outboundamount  As Quantitytosettle,
                    -(a.Settlementprice * a.Outboundamount) As Settlementamount
                 From Partsoutboundbilldetail a
                 Join Partsoutboundbill b
                   On a.Partsoutboundbillid = b.Id
                                     Join PartsPurchasePricing c
                         On c.partssupplierid={0}
                         Where a.Outboundamount!=0 And c.partssalescategoryid={1}
                         And c.validfrom<=b.createtime And c.validto>=b.createtime And a.sparepartid=c.partid and b.id in (", partsSupplierId.Value, partsSalesCategoryId.Value);
                            for(var i = 0; i < outboundIds.Length; i++) {
                                if(outboundIds.Length == i + 1) {
                                    sql2 += string.Format("{0})) ", outboundIds[i]);
                                } else {
                                    sql2 += string.Format("{0},", outboundIds[i]);
                                }
                            }
                        }
                        if(sql1 != "" && sql2 != "") {
                            sql.AppendFormat("{0} Union {1}", sql1, sql2);
                        } else {
                            if(sql1 != "") {
                                sql.Append(sql1);
                            }
                            if(sql2 != "") {
                                sql.Append(sql2);
                            }
                        }
                    }
                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode, ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName, ErrorStrings.Export_Title_FactoryPurchacePrice_PurchasingPrice,"采购数量",ErrorStrings.Export_Title_PartsInboundCheckBill_PurChaseAmount
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }
    }
}