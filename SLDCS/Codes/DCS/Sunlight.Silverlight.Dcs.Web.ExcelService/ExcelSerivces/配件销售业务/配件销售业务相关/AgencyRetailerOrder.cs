﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using Sunlight.Silverlight.Web;
using System.Globalization;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        /// <summary>
        /// 导出代理库电商订单
        /// </summary>
        public bool ExportAgencyRetailerOrder(int[] ids, int? branchId, int? shippingCompanyId, string erpSourceOrderCode, string code, int? warehouseId, int? partsSalesCategoryId, string vehiclePartsHandleOrderCode, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName) {
            fileName = GetExportFilePath("导出代理库电商订单.xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {
                    var sql = new StringBuilder();
                    #region SQL
                    sql.Append(@"select a.BranchName,
                                        a.PartsSalesCategoryName,
                                        a.ERPSourceOrderCode,
                                        a.Code,
                                        a.VehiclePartsHandleOrderCode,
                                        a.ShippingCompanyCode,
                                        a.ShippingCompanyName,
                                        a.WarehouseCode,
                                        a.WarehouseName,
                                        b.value Status,
                                        a.CounterpartCompanyCode,
                                        a.CounterpartCompanyName,
                                        c.Receivername,
                                        c.Contactphone,
                                        c.Receivingaddress,
                                        a.CreatorName,
                                        a.CreateTime,
                                        a.ConfirmorName,
                                        a.ConfirmorTime,
                                        a.CloserName,
                                        a.CloserTime,a.Remark
                                  from Agencyretailerorder a
                                  left join VehiclePartsHandleOrder c on a.vehiclepartshandleorderid= c.id
                                  left join Keyvalueitem b on a.status=b.key and b.Name='AgencyRetailerOrderStatus' ");
                    #endregion

                    var dbParameters = new List<DbParameter>();
                    #region 条件过滤


                    if(ids != null && ids.Length > 0) {
                        sql.Append(" where a.id in (");
                        for(var i = 0;i < ids.Length;i++) {
                            if(ids.Length == i + 1) {
                                sql.Append("{0}id" + i);
                                dbParameters.Add(db.CreateDbParameter("id" + i, ids[i]));
                            } else {
                                sql.Append("{0}id" + i + ",");
                                dbParameters.Add(db.CreateDbParameter("id" + i, ids[i]));
                            }
                        }
                        sql.Append(")");
                    } else {
                        //默认查询登录企业相关零售单
                        if(branchId.HasValue) {
                            sql.Append(" where a.BranchId = {0}BranchId");
                            dbParameters.Add(db.CreateDbParameter("BranchId", branchId.Value));
                        }

                        if(shippingCompanyId.HasValue) {
                            sql.Append(" where a.ShippingCompanyId = {0}ShippingCompanyId");
                            dbParameters.Add(db.CreateDbParameter("ShippingCompanyId", shippingCompanyId.Value));
                        }
                        if(!string.IsNullOrEmpty(code)) {
                            sql.Append(" and a.Code like {0}code");
                            dbParameters.Add(db.CreateDbParameter("code", "%" + code + "%"));
                        }
                        if(warehouseId.HasValue) {
                            sql.Append(" and a.warehouseId = {0}warehouseId ");
                            dbParameters.Add(db.CreateDbParameter("warehouseId", warehouseId.Value));
                        }

                        if(partsSalesCategoryId.HasValue) {
                            sql.Append(" and a.partsSalesCategoryId = {0}partsSalesCategoryId ");
                            dbParameters.Add(db.CreateDbParameter("partsSalesCategoryId", partsSalesCategoryId.Value));
                        }
                        if(!string.IsNullOrEmpty(vehiclePartsHandleOrderCode)) {
                            sql.Append(" and a.vehiclePartsHandleOrderCode like {0}vehiclePartsHandleOrderCode ");
                            dbParameters.Add(db.CreateDbParameter("vehiclePartsHandleOrderCode", "%" + vehiclePartsHandleOrderCode + "%"));
                        }
                        if(!string.IsNullOrEmpty(erpSourceOrderCode)) {
                            sql.Append(" and a.ErpSourceOrderCode like {0}erpSourceOrderCode ");
                            dbParameters.Add(db.CreateDbParameter("erpSourceOrderCode", "%" + erpSourceOrderCode + "%"));
                        }
                        if(status.HasValue) {
                            sql.Append(" and a.status={0}status");
                            dbParameters.Add(db.CreateDbParameter("status", status.Value));
                        }
                        if(createTimeBegin.HasValue) {
                            sql.Append(@" and a.createTime >=to_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(@" and a.createTime <=to_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                    }
                    #endregion
                    var sqlStr = sql.ToString();
                    var command = db.CreateDbCommand(string.Format(sqlStr, db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    conn.Open();
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    //列名
                                    ErrorStrings.Export_Title_PartsBranch_BranchName,ErrorStrings.Export_Title_Partssalescategory_Name,ErrorStrings.Export_Title_Partsoutboundplan_ERPOrderCode,"代理库电商订单编号","随车行处理单编号","发货企业编号","发货企业名称",
                                    "发货仓库编号","发货仓库名称",ErrorStrings.Export_Title_AccountPeriod_Status,ErrorStrings.Export_Title_Partsoutboundplan_ReceivingCompanyCode,ErrorStrings.Export_Title_PartsPurchaseOrder_ReceivingCompanyName,ErrorStrings.Export_Title_Agency_LinkMan,ErrorStrings.Export_Title_Agency_LinkManMobile,ErrorStrings.Export_Title_PartsPurchaseOrder_ReceiveAddress,ErrorStrings.Export_Title_AccountPeriod_CreatorName,ErrorStrings.Export_Title_AccountPeriod_CreateTime,ErrorStrings.Export_Title_InternalAllocationBill_ApproverName,ErrorStrings.Export_Title_InternalAllocationBill_ApproveTime,ErrorStrings.Export_Title_Partsoutboundplan_Stoper,ErrorStrings.Export_Title_Partsoutboundplan_StopTime,ErrorStrings.Export_Title_PartsBranch_Remark
                                };
                            }
                            if(reader.Read()) {
                                var values = new[] {
                                    reader[0],reader[1],reader[2],reader[3],reader[4],reader[5],reader[6],reader[7],reader[8],reader[9],
                                    reader[10],reader[11],reader[12],reader[13],reader[14],reader[15],reader[16],reader[17],reader[18],reader[19],reader[20],reader[21]
                                };
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }

        public bool ExportAgencyRetailerOrderWithDetails(int[] ids, int? branchId, int? shippingCompanyId, string erpSourceOrderCode, string code, int? warehouseId, int? partsSalesCategoryId, string vehiclePartsHandleOrderCode, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName) {
            fileName = GetExportFilePath("导出代理库电商订单.xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {
                    var sql = new StringBuilder();
                    #region SQL
                    sql.Append(@"select a.BranchName,
                                        a.PartsSalesCategoryName,
                                        a.ERPSourceOrderCode,
                                        a.Code,
                                        a.VehiclePartsHandleOrderCode,
                                        a.ShippingCompanyCode,
                                        a.ShippingCompanyName,
                                        a.WarehouseCode,
                                        a.WarehouseName,
                                        b.value Status,
                                        a.CounterpartCompanyCode,
                                        a.CounterpartCompanyName,
                                        d.Receivername,
                                        d.Contactphone,
                                        d.Receivingaddress,
                                        a.CreatorName,
                                        a.CreateTime,
                                        a.ConfirmorName,
                                        a.ConfirmorTime,
                                        a.CloserName,
                                        a.CloserTime,
                                        a.Remark,
                                        c.Sparepartcode,
                                        c.Sparepartname,
                                        c.OrderedQuantity,
                                        c.Confirmedamount,
                                        c.Unitprice,
                                        c.Ordersum,
                                        c.Remark
                                  from Agencyretailerorder a
                                  left join VehiclePartsHandleOrder d on a.vehiclepartshandleorderid= d.id
                                  left join Keyvalueitem b on a.status=b.key and b.Name='AgencyRetailerOrderStatus'
                                  left join Agencyretailerlist c on a.id=c.Agencyretailerorderid ");
                    #endregion

                    var dbParameters = new List<DbParameter>();
                    #region 条件过滤


                    if(ids != null && ids.Length > 0) {
                        sql.Append(" where a.id in (");
                        for(var i = 0;i < ids.Length;i++) {
                            if(ids.Length == i + 1) {
                                sql.Append("{0}id" + i);
                                dbParameters.Add(db.CreateDbParameter("id" + i, ids[i]));
                            } else {
                                sql.Append("{0}id" + i + ",");
                                dbParameters.Add(db.CreateDbParameter("id" + i, ids[i]));
                            }
                        }
                        sql.Append(")");
                    } else {
                        //默认查询登录企业相关零售单
                        if(branchId.HasValue) {
                            sql.Append(" where a.BranchId = {0}BranchId");
                            dbParameters.Add(db.CreateDbParameter("BranchId", branchId.Value));
                        }

                        if(shippingCompanyId.HasValue) {
                            sql.Append(" where a.ShippingCompanyId = {0}ShippingCompanyId");
                            dbParameters.Add(db.CreateDbParameter("ShippingCompanyId", shippingCompanyId.Value));
                        }
                        if(!string.IsNullOrEmpty(code)) {
                            sql.Append(" and a.Code like {0}code");
                            dbParameters.Add(db.CreateDbParameter("code", "%" + code + "%"));
                        }
                        if(warehouseId.HasValue) {
                            sql.Append(" and a.warehouseId = {0}warehouseId ");
                            dbParameters.Add(db.CreateDbParameter("warehouseId", warehouseId.Value));
                        }

                        if(partsSalesCategoryId.HasValue) {
                            sql.Append(" and a.partsSalesCategoryId = {0}partsSalesCategoryId ");
                            dbParameters.Add(db.CreateDbParameter("partsSalesCategoryId", partsSalesCategoryId.Value));
                        }
                        if(!string.IsNullOrEmpty(vehiclePartsHandleOrderCode)) {
                            sql.Append(" and a.vehiclePartsHandleOrderCode like {0}vehiclePartsHandleOrderCode ");
                            dbParameters.Add(db.CreateDbParameter("vehiclePartsHandleOrderCode", "%" + vehiclePartsHandleOrderCode + "%"));
                        }
                        if(!string.IsNullOrEmpty(erpSourceOrderCode)) {
                            sql.Append(" and a.ErpSourceOrderCode like {0}erpSourceOrderCode ");
                            dbParameters.Add(db.CreateDbParameter("erpSourceOrderCode", "%" + erpSourceOrderCode + "%"));
                        }
                        if(status.HasValue) {
                            sql.Append(" and a.status={0}status");
                            dbParameters.Add(db.CreateDbParameter("status", status.Value));
                        }
                        if(createTimeBegin.HasValue) {
                            sql.Append(@" and a.createTime >=to_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(@" and a.createTime <=to_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                    }
                    #endregion
                    var sqlStr = sql.ToString();
                    var command = db.CreateDbCommand(string.Format(sqlStr, db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    conn.Open();
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    //列名
                                    ErrorStrings.Export_Title_PartsBranch_BranchName,ErrorStrings.Export_Title_Partssalescategory_Name,ErrorStrings.Export_Title_Partsoutboundplan_ERPOrderCode,"代理库电商订单编号","随车行处理单编号","发货企业编号","发货企业名称",
                                    "发货仓库编号","发货仓库名称",ErrorStrings.Export_Title_AccountPeriod_Status,ErrorStrings.Export_Title_Partsoutboundplan_ReceivingCompanyCode,ErrorStrings.Export_Title_PartsPurchaseOrder_ReceivingCompanyName,ErrorStrings.Export_Title_Agency_LinkMan,ErrorStrings.Export_Title_Agency_LinkManMobile,ErrorStrings.Export_Title_PartsPurchaseOrder_ReceiveAddress,ErrorStrings.Export_Title_AccountPeriod_CreatorName,ErrorStrings.Export_Title_AccountPeriod_CreateTime,ErrorStrings.Export_Title_InternalAllocationBill_ApproverName,ErrorStrings.Export_Title_InternalAllocationBill_ApproveTime,ErrorStrings.Export_Title_Partsoutboundplan_Stoper,ErrorStrings.Export_Title_Partsoutboundplan_StopTime,
                                    ErrorStrings.Export_Title_PartsBranch_Remark,ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode,ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName,ErrorStrings.Export_Title_PartsSalesOrder_OrderQty,"确认数量",ErrorStrings.Export_Title_PartsPurReturnOrder_Price,ErrorStrings.Export_Title_PartsSalesOrder_OrderAmount,ErrorStrings.Export_Title_PartsBranch_Remark
                                };
                            }
                            if(reader.Read()) {
                                var values = new[] {
                                    reader[0],reader[1],reader[2],reader[3],reader[4],reader[5],reader[6],reader[7],reader[8],reader[9],
                                    reader[10],reader[11],reader[12],reader[13],reader[14],reader[15],reader[16],reader[17],reader[18],reader[19],
                                    reader[20],reader[21],reader[22],reader[23],reader[24],reader[25],reader[26],reader[27],reader[28]
                                };
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }
    }
}
