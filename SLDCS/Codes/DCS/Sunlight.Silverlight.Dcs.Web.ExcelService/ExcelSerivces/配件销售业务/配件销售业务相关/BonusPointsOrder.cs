﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        public bool 导出积分订单(int[] ids, string bonusPointsOrderCode, string corporationCode, int? brandId, string corporationName, int? type, int? settlementStatus, string platFormCode, string serviceApplyCode, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName) {
            fileName = GetExportFilePath("积分订单.xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);
                using(var conn = db.CreateDbConnection()) {
                    var sql = new StringBuilder();
                    sql.AppendFormat(@"Select BonusPointsOrderCode,BrandName,CorporationCode,CorporationName,BonusPointsAmount,BonusPoints,
                                      cast(Case Type
                                               When 1 Then
                                               '积分使用'
                                               When 2 Then
                                               '积分返还'
                                           End as varchar2(50)),PlatForm_Code,ServiceApplyCode,
                                      cast(Case SettlementStatus
                                               When 1 Then
                                               '未结算'
                                               When 2 Then
                                               '已结算'
                                               When 3 Then
                                               '已汇总'
                                           End as varchar2(50)),CreatorName,CreateTime
                                     from BonusPointsOrder where 1=1 ");
                    var dbParameters = new List<DbParameter>();
                    if(ids != null && ids.Length > 0) {
                        sql.Append(" and Id in (");
                        for(var i = 0; i < ids.Length; i++) {
                            if(ids.Length == i + 1) {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            } else {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                        }
                        sql.Append(")");
                    } else {
                        if(!string.IsNullOrEmpty(bonusPointsOrderCode)) {
                            sql.Append(" and Lower(BonusPointsOrderCode) like {0}bonusPointsOrderCode ");
                            dbParameters.Add(db.CreateDbParameter("bonusPointsOrderCode", "%" + bonusPointsOrderCode.ToLower() + "%"));
                        }
                        if(!string.IsNullOrEmpty(corporationCode)) {
                            sql.Append(" and Lower(CorporationCode) like {0}corporationCode ");
                            dbParameters.Add(db.CreateDbParameter("corporationCode", "%" + corporationCode.ToLower() + "%"));
                        }
                        if(brandId.HasValue) {
                            sql.Append(" and BrandId={0}brandId");
                            dbParameters.Add(db.CreateDbParameter("brandId", brandId));
                        }
                        if(!string.IsNullOrEmpty(corporationName)) {
                            sql.Append(" and Lower(CorporationName) like {0}corporationName ");
                            dbParameters.Add(db.CreateDbParameter("corporationName", "%" + corporationName.ToLower() + "%"));
                        }
                        if(type.HasValue) {
                            sql.Append(" and Type={0}type");
                            dbParameters.Add(db.CreateDbParameter("type", type));
                        }
                        if(settlementStatus.HasValue) {
                            sql.Append(" and SettlementStatus={0}settlementStatus");
                            dbParameters.Add(db.CreateDbParameter("settlementStatus", settlementStatus));
                        }
                        if(!string.IsNullOrEmpty(platFormCode)) {
                            sql.Append(@" and Lower(PlatForm_Code) like {0}platFormCode ");
                            dbParameters.Add(db.CreateDbParameter("platFormCode", "%" + platFormCode.ToLower() + "%"));
                        }
                        if(!string.IsNullOrEmpty(serviceApplyCode)) {
                            sql.Append(" and Lower(ServiceApplyCode) like {0}serviceApplyCode ");
                            dbParameters.Add(db.CreateDbParameter("serviceApplyCode", "%" + serviceApplyCode.ToLower() + "%"));
                        }

                        if(createTimeBegin.HasValue) {
                            sql.Append(@" and CreateTime >=to_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(@" and CreateTime <=to_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                    }
                    sql.Append(" order by Id desc");
                    var sqlStr = sql.ToString();
                    if(sqlStr.IndexOf(" where 1=1  and ") > 0)
                        sqlStr = sqlStr.Replace(" where 1=1  and ", " where ");
                    else
                        sqlStr = sqlStr.Replace(" where 1=1 ", "");
                    var command = db.CreateDbCommand(string.Format(sqlStr, db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    conn.Open();
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    "积分订单编号",ErrorStrings.Export_Title_Partssalescategory_Name,"企业编码",ErrorStrings.Export_Title_Company_Name,"积分金额","积分数","单据类型",ErrorStrings.Export_Title_SupplierShippingOrder_ERPSourceOrderCode,"平台退货单号",
                                    ErrorStrings.Export_Title_PartsInboundCheckBill_SettlementStatus,ErrorStrings.Export_Title_AccountPeriod_CreatorName,ErrorStrings.Export_Title_AccountPeriod_CreateTime
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }
    }
}
