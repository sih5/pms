﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        public bool 导出积分汇总单(int[] ids, string bonusPointsSummaryCode, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, DateTime? checkTimeBegin, DateTime? checkTimeEnd, out string fileName) {
            fileName = GetExportFilePath("积分汇总单.xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);
                using(var conn = db.CreateDbConnection()) {
                    var sql = new StringBuilder();
                    sql.AppendFormat(@"Select BonusPointsSummaryCode,BrandName,SummaryAmount,SummaryBonusPoints,
                                      cast(Case Status
                                               When 1 Then
                                               '新建'
                                               When 2 Then
                                               '已审批'
                                               When 3 Then
                                               '发票登记'
                                               When 4 Then
                                               '作废'
                                           End as varchar2(50)),CreatorName,CreateTime,
                                     ApproverName,ApproveTime  
                                     from BonusPointsSummary where 1=1 ");
                    var dbParameters = new List<DbParameter>();
                    if(ids != null && ids.Length > 0) {
                        sql.Append(" and Id in (");
                        for(var i = 0; i < ids.Length; i++) {
                            if(ids.Length == i + 1) {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            } else {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                        }
                        sql.Append(")");
                    } else {
                        if(!string.IsNullOrEmpty(bonusPointsSummaryCode)) {
                            sql.Append(" and Lower(BonusPointsSummaryCode) like {0}bonusPointsSummaryCode ");
                            dbParameters.Add(db.CreateDbParameter("bonusPointsSummaryCode", "%" + bonusPointsSummaryCode.ToLower() + "%"));
                        }
                        if(status.HasValue) {
                            sql.Append(" and Status={0}status");
                            dbParameters.Add(db.CreateDbParameter("status", status));
                        }
                        if(createTimeBegin.HasValue) {
                            sql.Append(@" and CreateTime >=to_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(@" and CreateTime <=to_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                        if(checkTimeBegin.HasValue) {
                            sql.Append(@" and ApproveTime >=to_date({0}checkTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("checkTimeBegin", tempTime.ToString("G")));
                        }
                        if(checkTimeEnd.HasValue) {
                            sql.Append(@" and ApproveTime <=to_date({0}checkTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("checkTimeEnd", tempTime.ToString("G")));
                        }

                    }
                    sql.Append(" order by Id desc");
                    var sqlStr = sql.ToString();
                    if(sqlStr.IndexOf(" where 1=1  and ") > 0)
                        sqlStr = sqlStr.Replace(" where 1=1  and ", " where ");
                    else
                        sqlStr = sqlStr.Replace(" where 1=1 ", "");
                    var command = db.CreateDbCommand(string.Format(sqlStr, db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    conn.Open();
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    "积分汇总单号",ErrorStrings.Export_Title_Partssalescategory_Name,"汇总金额","汇总积分",ErrorStrings.Export_Title_AccountPeriod_Status,ErrorStrings.Export_Title_AccountPeriod_CreatorName,ErrorStrings.Export_Title_AccountPeriod_CreateTime,ErrorStrings.Export_Title_PackingPropertyApp_ApproverName,ErrorStrings.Export_Title_PackingPropertyApp_ApproveTime
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }



        public bool 合并导出积分汇总单(int[] ids, string bonusPointsSummaryCode, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, DateTime? checkTimeBegin, DateTime? checkTimeEnd, out string fileName) {
            fileName = GetExportFilePath("积分汇总单及清单.xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);
                using(var conn = db.CreateDbConnection()) {
                    var sql = new StringBuilder();
                    sql.AppendFormat(@"Select s.BonusPointsSummaryCode,s.BrandName,s.SummaryAmount,s.SummaryBonusPoints,
                                      cast(Case s.Status
                                               When 1 Then
                                               '新建'
                                               When 2 Then
                                               '已审批'
                                               When 3 Then
                                               '发票登记'
                                               When 4 Then
                                               '作废'
                                           End as varchar2(50)),s.CreatorName,s.CreateTime,
                                     s.ApproverName,s.ApproveTime,l.BonusPointsOrderCode,l.PlatForm_Code,l.ServiceApplyCode,l.BonusPoints,l.BonusPointsAmount 
                                     from BonusPointsSummary s left join BonusPointsSummaryList l on s.Id=l.BonusPointsSummaryId where 1=1 ");
                    var dbParameters = new List<DbParameter>();
                    if(ids != null && ids.Length > 0) {
                        sql.Append(" and s.Id in (");
                        for(var i = 0; i < ids.Length; i++) {
                            if(ids.Length == i + 1) {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            } else {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                        }
                        sql.Append(")");
                    } else {
                        if(!string.IsNullOrEmpty(bonusPointsSummaryCode)) {
                            sql.Append(" and Lower(s.BonusPointsSummaryCode) like {0}bonusPointsSummaryCode ");
                            dbParameters.Add(db.CreateDbParameter("bonusPointsSummaryCode", "%" + bonusPointsSummaryCode.ToLower() + "%"));
                        }
                        if(status.HasValue) {
                            sql.Append(" and s.Status={0}status");
                            dbParameters.Add(db.CreateDbParameter("status", status));
                        }
                        if(createTimeBegin.HasValue) {
                            sql.Append(@" and s.CreateTime >=to_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(@" and s.CreateTime <=to_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                        if(checkTimeBegin.HasValue) {
                            sql.Append(@" and s.ApproveTime >=to_date({0}checkTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("checkTimeBegin", tempTime.ToString("G")));
                        }
                        if(checkTimeEnd.HasValue) {
                            sql.Append(@" and s.ApproveTime <=to_date({0}checkTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("checkTimeEnd", tempTime.ToString("G")));
                        }

                    }
                    sql.Append(" order by s.Id desc");
                    var sqlStr = sql.ToString();
                    if(sqlStr.IndexOf(" where 1=1  and ") > 0)
                        sqlStr = sqlStr.Replace(" where 1=1  and ", " where ");
                    else
                        sqlStr = sqlStr.Replace(" where 1=1 ", "");
                    var command = db.CreateDbCommand(string.Format(sqlStr, db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    conn.Open();
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    "积分汇总单号",ErrorStrings.Export_Title_Partssalescategory_Name,"汇总金额","汇总积分",ErrorStrings.Export_Title_AccountPeriod_Status,ErrorStrings.Export_Title_AccountPeriod_CreatorName,ErrorStrings.Export_Title_AccountPeriod_CreateTime,ErrorStrings.Export_Title_PackingPropertyApp_ApproverName,ErrorStrings.Export_Title_PackingPropertyApp_ApproveTime,"积分订单编号",ErrorStrings.Export_Title_SupplierShippingOrder_ERPSourceOrderCode,"平台退货单号","积分数","积分金额"
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }
    }
}
