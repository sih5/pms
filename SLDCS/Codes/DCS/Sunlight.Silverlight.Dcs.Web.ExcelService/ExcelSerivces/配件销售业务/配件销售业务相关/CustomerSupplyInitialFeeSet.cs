﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Entities;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;


namespace Sunlight.Silverlight.Dcs.Web.ExcelService
{
    public partial class ExcelService
    {
        /// <summary>
        /// 导入客户直供配件清单
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="excelImportNum"></param>
        /// <param name="errorData"></param>
        /// <param name="errorDataFileName"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public bool ImportCustomerSupplyInitialFeeSet(string fileName, out int excelImportNum, out List<CustomerSupplyInitialFeeSetExtend> errorData, out string errorDataFileName, out string errorMessage)
        {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<CustomerSupplyInitialFeeSetExtend>();
            var allList = new List<CustomerSupplyInitialFeeSetExtend>();
            try
            {
                if (!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);
                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("CustomerSupplyInitialFeeSet", out notNullableFields, out fieldLenght);

                List<object> excelColumns;
                List<CustomerSupplyInitialFeeSetExtend> rightList;
                using (var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString))
                {
                    #region 指定文件中的列对应的名称 例如
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_BottomStock_PartsSalesCategoryName, "PartsSalesCategoryName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Credenceapplication_CustomerCode, "CustomerCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_FactoryPurchacePrice_SupplierCode, "SupplierCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Titile_CustomerSupplyInitialFeeSet_InitialFee, "InitialFee");
                    excelOperator.AddColumnDataSource("约定发货时间", "AppointShippingDays");
                    #endregion
                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;
                    #region 获取对应枚举
                    var keyValuePairs = new[] { new KeyValuePair<string, string>("BaseDataStatus", "BaseData_Status") };
                    tempExcelOperator.ImportHelper.LoadEnumFromDb(null, null, keyValuePairs);
                    #endregion
                    excelImportNum = excelOperator.LoadExcelRow(row =>
                    {
                        var tempImportObj = new CustomerSupplyInitialFeeSetExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        #region 根据导入的字段给对应的实体属性赋值
                        tempImportObj.PartsSalesCategoryNameStr = newRow["PartsSalesCategoryName"];
                        tempImportObj.CustomerCodeStr = newRow["CustomerCode"];
                        tempImportObj.SupplierCodeStr = newRow["SupplierCode"];
                        tempImportObj.InitialFeeStr = newRow["InitialFee"];
                        tempImportObj.AppointShippingDaysStr = newRow["AppointShippingDays"];
                        #endregion
                        var tempErrorMessage = new List<string>();
                        #region 导入的数据基本检查
                        //品牌名称检查
                        if (string.IsNullOrEmpty(tempImportObj.PartsSalesCategoryNameStr))
                        {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_PartsSalescategoryNameIsNull);
                        }
                        else
                        {
                            if (Encoding.Default.GetByteCount(tempImportObj.PartsSalesCategoryNameStr) > fieldLenght["PartsSalesCategoryName".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_PartsSalescategoryNameIsLong);
                        }
                        //客户编号检查
                        if (string.IsNullOrEmpty(tempImportObj.CustomerCodeStr))
                        {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Credenceapplication_CustomerCodeIsNull);
                        }
                        else
                        {
                            if (Encoding.Default.GetByteCount(tempImportObj.CustomerCodeStr) > fieldLenght["CustomerCode".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Credenceapplication_CustomerCodeIsLong);
                        }

                        //供应商编号检查
                        if (string.IsNullOrEmpty(tempImportObj.SupplierCodeStr))
                        {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PersonnelSupplierRelation_SupplierCode);
                        }
                        else
                        {
                            if (Encoding.Default.GetByteCount(tempImportObj.SupplierCodeStr) > fieldLenght["SupplierCode".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PersonnelSupplierRelation_SupplierCodeIsLong);
                        }
                        //起订金额
                        if (string.IsNullOrEmpty(tempImportObj.InitialFeeStr))
                        {
                            tempErrorMessage.Add(ErrorStrings.Export_Validation_CustomerSupplyInitialFeeSet_InitialFeeIsNull);
                        }
                        else
                        {
                            decimal checkValue;
                            if (decimal.TryParse(tempImportObj.InitialFeeStr, out checkValue))
                            {
                                if (checkValue < 0)
                                {
                                    tempErrorMessage.Add(ErrorStrings.Export_Validation_CustomerSupplyInitialFeeSet_InitialFeeZero);
                                }
                            }
                            else
                            {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_CustomerSupplyInitialFeeSet_InitialFeeInteger);
                            }
                        }
                        //约定发货时间
                        if(string.IsNullOrEmpty(tempImportObj.AppointShippingDaysStr)) {
                            tempErrorMessage.Add("约定发货时间不能为空");
                        } else {
                            int checkValue;
                            if(int.TryParse(tempImportObj.AppointShippingDaysStr, out checkValue)) {
                                if(checkValue < 0) {
                                    tempErrorMessage.Add("约定发货时间必须大于0");
                                }
                            } else {
                                tempErrorMessage.Add("约定发货时间必须是数字");
                            }
                        }
                        #endregion
                        if (tempErrorMessage.Count > 0)
                        {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });
                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    #region 剩下的数据进行业务检查
                    //文件中唯一性校验：品牌名称、客户编号、客户名称、配件编号组合唯一存在于文件中，否则提示：数据重复
                    var groups = tempRightList.GroupBy(r => new
                    {
                        r.PartsSalesCategoryNameStr,
                        r.CustomerCodeStr,
                        r.SupplierCodeStr,
                        r.InitialFeeStr
                       
                    }).Where(r => r.Count() > 1);
                    foreach (var groupItem in groups.SelectMany(r => r))
                    {
                        groupItem.ErrorMsg = ErrorStrings.ImpPartsSupplierRelation_Validation31;
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    //1.合法性校验：品牌名称存在于配件销售类型中，否则提示:品牌不存在
                    var partsSalesCategoryNamesNeedCheck = tempRightList.Select(r => r.PartsSalesCategoryNameStr).Distinct().ToArray();
                    var dbBrands = new List<PartsSalesCategoryExtend>();
                    Func<string[], bool> getDbBrands = value =>
                    {
                        var dbObj = new PartsSalesCategoryExtend
                        {
                            Id = Convert.ToInt32(value[0]),
                            Name = value[1]
                        };
                        dbBrands.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select Id,Name from PartsSalesCategory where status=1 ", "Name", false, partsSalesCategoryNamesNeedCheck, getDbBrands);
                    foreach (var tempRight in tempRightList)
                    {
                        var sparePartItem = dbBrands.FirstOrDefault(v => v.Name == tempRight.PartsSalesCategoryNameStr);
                        if (sparePartItem == null)
                        {
                            tempRight.ErrorMsg = ErrorStrings.ImpVehicleDLRStartSecurityDeposit_Validation8;
                            continue;
                        }
                        tempRight.PartsSalesCategoryId = sparePartItem.Id;
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    //2.合法性校验：客户编号，客户名称组合存在于企业中,否则提示:客户不存在
                    var customerCodeStrsNeedCheck = tempRightList.Select(r => r.CustomerCodeStr).Distinct().ToArray();
                    var dbCustomers = new List<CompanyExtend>();
                    Func<string[], bool> getDbCustomers = value =>
                    {
                        var dbObj = new CompanyExtend
                        {
                            Id = Convert.ToInt32(value[0]),
                            Code = value[1],
                            Name = value[2]
                        };
                        dbCustomers.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select Id,Code,Name from Company where status=1 ", "Code", false, customerCodeStrsNeedCheck, getDbCustomers);
                    foreach (var tempRight in tempRightList)
                    {
                        var sparePartItem = dbCustomers.FirstOrDefault(v => v.Code == tempRight.CustomerCodeStr);
                        if (sparePartItem == null)
                        {
                            tempRight.ErrorMsg = ErrorStrings.ImpVehicleDLRStartSecurityDeposit_Validation9;
                            continue;
                        }
                        tempRight.CustomerId = sparePartItem.Id;
                        tempRight.CustomerName = sparePartItem.Name;
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    //3.合法性校验:供应商存在于公司信息中，否则提示:供应商不存在
                    var supplierCodeStrsNeedCheck = tempRightList.Select(r => r.SupplierCodeStr).Distinct().ToArray();
                    var dbSuppliers = new List<CompanyExtend>();
                    Func<string[], bool> getDbSuppliers = value =>
                    {
                        var dbObj = new CompanyExtend
                        {
                            Id = Convert.ToInt32(value[0]),
                            Code = value[1],
                            Name = value[2]
                        };
                        dbSuppliers.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select Id,Code,Name from PartsSupplier where status=1 ", "Code", false, supplierCodeStrsNeedCheck, getDbSuppliers);
                    foreach (var tempRight in tempRightList)
                    {
                        var sparePartItem = dbSuppliers.FirstOrDefault(v => v.Code == tempRight.SupplierCodeStr);
                        if (sparePartItem == null)
                        {
                            tempRight.ErrorMsg = ErrorStrings.Export_Validation_PartsPurchaseOrder_Validation8;
                            continue;
                        }
                        tempRight.SupplierId = sparePartItem.Id;
                        tempRight.SupplierName = sparePartItem.Name;
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    #endregion
                    //获取所有不合格数据
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    //获取所有合格数据
                    rightList = allList.Except(errorList).ToList();
                    #region 将合格数据的值填上
                    var userInfo = Utils.GetCurrentUserInfo();
                    foreach (var rightItem in rightList)
                    {
                        rightItem.PartsSalesCategoryName = rightItem.PartsSalesCategoryNameStr;
                        rightItem.CustomerCode = rightItem.CustomerCodeStr;
                        rightItem.SupplierCode = rightItem.SupplierCodeStr;
                        rightItem.Status = tempExcelOperator.ImportHelper.GetEnumValue("BaseDataStatus", ErrorStrings.Export_Validation_PartsBranch_Used) ?? 0;
                        rightItem.InitialFee = Convert.ToDecimal(rightItem.InitialFeeStr);
                        rightItem.AppointShippingDays = Convert.ToInt32(rightItem.AppointShippingDaysStr);
                        rightItem.CreatorId = userInfo.Id;
                        rightItem.CreatorName = userInfo.Name;
                        rightItem.CreateTime = DateTime.Now;
                    }
                    #endregion
                }
                //导出所有不合格数据
                if (errorList.Any())
                {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using (var excelExport = new ExcelExport(errorDataFileName))
                    {
                        var list = errorList;
                        excelExport.ExportByRow(index =>
                        {
                            if (index == list.Count + 1)
                                return null;
                            if (index == 0)
                                return excelColumns.ToArray();
                            var tempObj = list[index - 1];
                            var values = new object[] {
                                #region 设置错误信息导出的列的值
                                tempObj.PartsSalesCategoryNameStr,tempObj.CustomerCodeStr,
                                tempObj.SupplierCodeStr,tempObj.InitialFeeStr,tempObj.AppointShippingDaysStr,
                                tempObj.ErrorMsg
                                #endregion                     
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }
                //导入所有合格数据
                if (!rightList.Any())
                    return true;
                //根据合格数据客户ID查询出客户直供配件清单数据。
                var customerIdNeedCheck = rightList.Select(r => r.CustomerId.ToString(CultureInfo.InvariantCulture)).Distinct().ToArray();
                var dbCustomerSupplyInitialFeeSets = new List<CustomerSupplyInitialFeeSetExtend>();
                Func<string[], bool> getCustomerSupplyInitialFeeSets = value =>
                {
                    var dbObj = new CustomerSupplyInitialFeeSetExtend
                    {
                        Id = Convert.ToInt32(value[0]),
                        PartsSalesCategoryId = Convert.ToInt32(value[1]),
                        CustomerId = Convert.ToInt32(value[2]),
                        SupplierId = Convert.ToInt32(value[3])
                    };
                    dbCustomerSupplyInitialFeeSets.Add(dbObj);
                    return false;
                };
                db.QueryDataWithInOperator("select id, PartsSalesCategoryId,CustomerId,SupplierId from CustomerSupplyInitialFeeSet ", "CustomerId", false, customerIdNeedCheck, getCustomerSupplyInitialFeeSets);

                using (var conn = db.CreateDbConnection())
                {
                    conn.Open();
                    //开启事务，新增删除客户直供配件清单在一个事务内
                    var ts = conn.BeginTransaction();
                    try
                    {
                        //新增客户直供配件清单
                        if (rightList.Any())
                        {
                            #region 获取对应的插入的SQL 通过db.GetInsertSql方法
                            //获取新增数据的sql语句，Id为主键
                            var sqlInsert = db.GetInsertSql("CustomerSupplyInitialFeeSet", "Id", new[] {
                                "PartsSalesCategoryId",
                                "PartsSalesCategoryName", 
                                "CustomerId", "CustomerCode", "CustomerName",
                                "SupplierId","SupplierCode","SupplierName",
                                "InitialFee","Status",
                                "CreatorId", "CreatorName", "CreateTime","AppointShippingDays"
                            });
                            #endregion
                            //数据库删除重复数据后再往数据库增加客户直供配件清单
                            foreach (var item in rightList)
                            {
                                //根据组合唯一查询出来的Id删除重复数据
                                var repeatData = dbCustomerSupplyInitialFeeSets.FirstOrDefault(v => v.PartsSalesCategoryId == item.PartsSalesCategoryId && v.CustomerId == item.CustomerId && v.SupplierId == item.SupplierId);
                                if (repeatData != null)
                                {
                                    //存在  删除 
                                    var commandDelete = db.CreateDbCommand(string.Format(" delete from CustomerSupplyInitialFeeSet where id ={0} ", repeatData.Id), conn, ts);
                                    commandDelete.ExecuteNonQuery();
                                }
                                #region 添加Sql的参数
                                var command = db.CreateDbCommand(sqlInsert, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("PartsSalesCategoryId", item.PartsSalesCategoryId));
                                command.Parameters.Add(db.CreateDbParameter("PartsSalesCategoryName", item.PartsSalesCategoryName));
                                command.Parameters.Add(db.CreateDbParameter("CustomerId", item.CustomerId));
                                command.Parameters.Add(db.CreateDbParameter("CustomerCode", item.CustomerCode));
                                command.Parameters.Add(db.CreateDbParameter("CustomerName", item.CustomerName));
                                command.Parameters.Add(db.CreateDbParameter("SupplierId", item.SupplierId));
                                command.Parameters.Add(db.CreateDbParameter("SupplierCode", item.SupplierCode));
                                command.Parameters.Add(db.CreateDbParameter("SupplierName", item.SupplierName));
                                command.Parameters.Add(db.CreateDbParameter("InitialFee", item.InitialFee));
                                command.Parameters.Add(db.CreateDbParameter("Status", item.Status));
                                command.Parameters.Add(db.CreateDbParameter("CreatorId", item.CreatorId));
                                command.Parameters.Add(db.CreateDbParameter("CreatorName", item.CreatorName));
                                command.Parameters.Add(db.CreateDbParameter("CreateTime", item.CreateTime));
                                command.Parameters.Add(db.CreateDbParameter("AppointShippingDays", item.AppointShippingDays));
                                command.ExecuteNonQuery();
                                #endregion
                            }
                        }
                        //无异常提交
                        ts.Commit();
                    }
                    catch (Exception)
                    {
                        //报错回滚
                        ts.Rollback();
                        throw;
                    }
                    finally
                    {
                        if (conn.State == System.Data.ConnectionState.Open)
                        {
                            conn.Close();
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            }
            finally
            {
                errorData = errorList;
            }
        }
    }
}
