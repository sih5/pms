﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        /// <summary>
        /// 校验二级站零售订单
        /// </summary>
        public bool ImportDealerPartsRetailOrder(int partsSalesCategoryId, int dealerId, int subDealerId, string fileName, out int excelImportNum, out List<DealerRetailOrderDetailExtend> errorData, out List<DealerRetailOrderDetailExtend> rightData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<DealerRetailOrderDetailExtend>();
            var rightList = new List<DealerRetailOrderDetailExtend>();
            var allList = new List<DealerRetailOrderDetailExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("DealerRetailOrderDetail", out notNullableFields, out fieldLenght);
                List<object> excelColumns;

                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    #region 指定文件中的列对应的名称 例如
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode, "PartsCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName, "PartsName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_WarehouseArea_Quantity, "Quantity");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Remark, "Remark");
                    #endregion
                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var tempImportObj = new DealerRetailOrderDetailExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        #region 根据导入的字段给对应的实体属性赋值
                        tempImportObj.PartsCodeStr = newRow["PartsCode"];
                        tempImportObj.PartsNameStr = newRow["PartsName"];
                        tempImportObj.QuantityStr = newRow["Quantity"];
                        tempImportObj.RemarkStr = newRow["Remark"];
                        #endregion

                        var tempErrorMessage = new List<string>();
                        #region 导入的数据基本检查
                        //配件编号
                        var fieldIndex = notNullableFields.IndexOf("PartsCode".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.PartsCodeStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchaseOrderDetail_Validation1);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.PartsCodeStr) > fieldLenght["PartsCode".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchaseOrderDetail_Validation2);
                        }
                        //配件名称
                        fieldIndex = notNullableFields.IndexOf("PartsName".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.PartsNameStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Sparepart_Name);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.PartsNameStr) > fieldLenght["PartsName".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Sparepart_NameIsLong);
                        }
                        //数量
                        fieldIndex = notNullableFields.IndexOf("Quantity".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.QuantityStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_InternalAcquisitionDetail_QuantityIsNull);
                        } else {
                            int quantity;
                            if(int.TryParse(tempImportObj.QuantityStr, out quantity)) {
                                if(quantity < 0)
                                    tempErrorMessage.Add(ErrorStrings.Export_Validation_InternalAcquisitionDetail_QuantityOverZero);
                                tempImportObj.Quantity = quantity;
                            } else {
                                tempErrorMessage.Add(ErrorStrings.Export_Title_DealerRetailOrderDetail_Quantity);
                            }
                        }
                        //备注
                        fieldIndex = notNullableFields.IndexOf("Remark".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.RemarkStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchasePricingDetail_Validation15);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.RemarkStr) > fieldLenght["Remark".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchasePricingDetail_Validation16);
                        }
                        #endregion
                        if(tempErrorMessage.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });
                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    #region 剩下的数据进行业务检查
                    var groups = tempRightList.GroupBy(r => new {
                        r.PartsCodeStr
                    }).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        groupItem.ErrorMsg = ErrorStrings.ImpPartsSupplierRelation_Validation31;
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    //配件有效性校验
                    var partsCodeNeedCheck = tempRightList.Select(r => r.PartsCodeStr.ToUpper()).Distinct().ToArray();
                    var dbSpareParts = new List<SparePartExtend>();
                    Func<string[], bool> getDbSpareParts = value => {
                        dbSpareParts.Add(new SparePartExtend {
                            Id = Convert.ToInt32(value[0]),
                            Code = value[1],
                            Name = value[2]
                        });
                        return false;
                    };
                    db.QueryDataWithInOperator("Select Id,Code,Name From SparePart Where status=1 ", "Code", true, partsCodeNeedCheck, getDbSpareParts);
                    //查询经销商配件库存
                    var sparePartIdNeedCheck = dbSpareParts.Select(r => r.Id.ToString(CultureInfo.InvariantCulture)).Distinct().ToArray();
                    var dbDealerPartsStocks = new List<DealerPartsStockExtend>();
                    Func<string[], bool> getDbDealerPartsStocks = value => {
                        dbDealerPartsStocks.Add(new DealerPartsStockExtend {
                            SparePartId = Convert.ToInt32(value[0]),
                            Quantity = Convert.ToInt32(value[1])
                        });
                        return false;
                    };
                    var sql = string.Format("Select SparePartId,Quantity From DealerPartsStock Where SalesCategoryId={0} And DealerId={1} And SubDealerId={2} ", partsSalesCategoryId, dealerId, subDealerId);
                    db.QueryDataWithInOperator(sql, "SparePartId", false, sparePartIdNeedCheck, getDbDealerPartsStocks);
                    //查询配件零售指导价
                    var dbPartsRetailGuidePrices = new List<PartsRetailGuidePriceExtend>();
                    Func<string[], bool> getDbPartsRetailGuidePrices = value => {
                        dbPartsRetailGuidePrices.Add(new PartsRetailGuidePriceExtend {
                            SparePartId = Convert.ToInt32(value[0]),
                            RetailGuidePrice = Convert.ToDecimal(value[1])
                        });
                        return false;
                    };
                    var sql1 = string.Format("Select SparePartId,RetailGuidePrice From PartsRetailGuidePrice Where PartsSalesCategoryId={0} ", partsSalesCategoryId);
                    db.QueryDataWithInOperator(sql1, "SparePartId", false, sparePartIdNeedCheck, getDbPartsRetailGuidePrices);
                    foreach(var tempRight in tempRightList) {
                        var sparePart = dbSpareParts.FirstOrDefault(v => v.Code == tempRight.PartsCodeStr.ToUpper());
                        if(sparePart == null) {
                            tempRight.ErrorMsg = ErrorStrings.Export_Validation_InternalAcquisitionDetail_Validation1;
                            continue;
                        }
                        var dealerPartsStock = dbDealerPartsStocks.FirstOrDefault(v => v.SparePartId == sparePart.Id);
                        if(dealerPartsStock == null) {
                            tempRight.ErrorMsg = ErrorStrings.Export_Validation_DealerRetailOrderDetail_QuantityNotExist;
                            continue;
                        }
                        if(dealerPartsStock.Quantity < tempRight.Quantity) {
                            tempRight.ErrorMsg = ErrorStrings.Export_Validation_DealerRetailOrderDetail_Validation1;
                            continue;
                        }
                        var partsRetailGuidePrice = dbPartsRetailGuidePrices.FirstOrDefault(v => v.SparePartId == sparePart.Id);
                        if(partsRetailGuidePrice == null) {
                            tempRight.ErrorMsg = ErrorStrings.Export_Validation_DealerRetailOrderDetail_Validation2;
                            continue;
                        }
                        tempRight.PartsId = sparePart.Id;
                        tempRight.PartsCode = sparePart.Code;
                        tempRight.PartsName = sparePart.Name;
                        tempRight.Price = partsRetailGuidePrice.RetailGuidePrice;
                    }

                    #endregion
                    //获取所有不合格数据
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    //获取所有合格数据
                    rightList = allList.Except(errorList).ToList();
                    #region 将合格数据的值填上
                    foreach(var rightItem in rightList) {
                        rightItem.Remark = rightItem.RemarkStr;
                    }
                    #endregion
                }
                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempObj = list[index - 1];
                            var values = new object[] {
                                #region 设置错误信息导出的列的值

                                tempObj.PartsCodeStr,tempObj.PartsNameStr,
                                tempObj.QuantityStr,tempObj.RemarkStr,
                                tempObj.ErrorMsg
                                
                                #endregion                     
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }

        /// <summary>
        /// 导出服务站配件零售订单审核清单
        /// </summary>

        public bool ExporttDealerPartsRetailOrdeForApprove(int[] ids, string dealerName, int? partsSalesCategoryId, string code, int? retailOrderType, string customer, string subDealerName, string dealerCode, int? status, DateTime? beginCreateTime, DateTime? endCreateTime, out string fileName) {

            fileName = GetExportFilePath("配件发运单查询_" + ".xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {
                    var sql = new StringBuilder();
                    #region
                    sql.Append(@"select a.Code,
                                        a.BranchName,
                                        a.DealerCode,
                                        a.DealerName, 
                                        a.SalesCategoryName,
                                       Cast(decode(a.Status,--状态
                                            1, '新建',
                                            2 , '已审核',
                                            99,  '作废'
                                      )as varchar2(20)) As  status,
                                       Cast(decode(a.RetailOrderType,
                                            1,
                                          '普通客户',
                                            2,
                                          '二级站',
                                            10,
                                          '大客户',
                                            15  ,
                                          '社会修理厂',
                                            20,
                                          '社会配件经销商'
                                       )as varchar2(20)) As RetailOrderType,
                                        a.Customer,
                                        a.CustomerPhone,
                                        a.SubDealerName,
                                        a.CustomerCellPhone,
                                        a.CustomerUnit,
                                        a.Address,
                                        a.TotalAmount,
                                        a.ApprovalComment,
                                        a.Remark,
                                        a.CreatorName,
                                        a.CreateTime,
                                        a.ModifierName,
                                        a.ModifyTime,
                                        a.ApproverName,
                                        a.ApproveTime,
                                        a.AbandonerName,
                                        a.AbandonTime
                                        from DealerPartsRetailOrder  a  where 1=1 ");

                    #endregion
                    var dbParameters = new List<DbParameter>();
                    #region 条件过滤
                    if(ids != null && ids.Length > 0) {
                        sql.Append(" and a.Id in (");
                        for(var i = 0; i < ids.Length; i++) {
                            if(ids.Length == i + 1) {
                                sql.Append("{0}id" + i);
                                dbParameters.Add(db.CreateDbParameter("id" + i, ids[i]));
                            } else {
                                sql.Append("{0}id" + i + ",");
                                dbParameters.Add(db.CreateDbParameter("id" + i, ids[i]));
                            }
                        }
                        sql.Append(")");
                    } else {

                        if(!string.IsNullOrEmpty(code)) {
                            sql.Append(" and Upper(a.Code) like Upper({0}code) ");
                            dbParameters.Add(db.CreateDbParameter("code", "%" + code + "%"));
                        }
                        if(retailOrderType.HasValue) {
                            sql.Append(" and a.RetailOrderType={0}retailOrderType");
                            dbParameters.Add(db.CreateDbParameter("retailOrderType", retailOrderType.Value));
                        }
                        if(status.HasValue) {
                            sql.Append(" and a.Status={0}status");
                            dbParameters.Add(db.CreateDbParameter("status", status.Value));
                        }
                        if(partsSalesCategoryId.HasValue) {
                            sql.Append(" and a.PartsSalesCategoryId={0}partsSalesCategoryId");
                            dbParameters.Add(db.CreateDbParameter("partsSalesCategoryId", partsSalesCategoryId.Value));
                        }
                        if(beginCreateTime.HasValue) {
                            sql.Append(" and a.CreateTime>=to_date({0}beginCreateTime,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = beginCreateTime.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("beginCreateTime", tempTime.ToString("G")));
                        }
                        if(endCreateTime.HasValue) {
                            sql.Append(" and a.createTime<=to_date({0}endCreateTime,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = endCreateTime.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("endCreateTime", tempTime.ToString("G")));
                        }
                        if(string.IsNullOrEmpty(dealerName)) {
                            sql.Append(" and a.DealerName like {0}dealerName ");
                            dbParameters.Add(db.CreateDbParameter("dealerName", "%" + dealerName + "%"));
                        }
                        if(!string.IsNullOrEmpty(customer)) {
                            sql.Append(" and Upper(a.Customer) like Upper({0}customer) ");
                            dbParameters.Add(db.CreateDbParameter("customer", "%" + customer + "%"));
                        }
                        if(!string.IsNullOrEmpty(subDealerName)) {
                            sql.Append(" and a.SubDealerName like {0}subDealerName ");
                            dbParameters.Add(db.CreateDbParameter("subDealerName", "%" + subDealerName + "%"));
                        }
                        if(!string.IsNullOrEmpty(dealerCode)) {
                            sql.Append(" and a.DealerCode like {0}dealerCode");
                            dbParameters.Add(db.CreateDbParameter("dealerCode", "%" + dealerCode + "%"));
                        }
                    }
                    sql.Append(" order by a.id ");
                    #endregion
                    var sqlStr = sql.ToString();
                    if(sqlStr.IndexOf(" where 1=1  and ") > 0)
                        sqlStr = sqlStr.Replace(" where 1=1  and ", " where ");
                    else
                        sqlStr = sqlStr.Replace(" where 1=1 ", "");
                    var command = db.CreateDbCommand(string.Format(sqlStr, db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    conn.Open();
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                ErrorStrings.Export_Title_DealerPartsRetailOrder_Code,ErrorStrings.Export_Title_Branch_BranchName,ErrorStrings.Export_Title_DealerPartsRetailOrder_DealerCode,ErrorStrings.Export_Title_PartsSalesOrder_SubmitCompanyName,ErrorStrings.Export_Title_Partssalescategory_Name,ErrorStrings.Export_Title_AccountPeriod_Status,
                                ErrorStrings.Export_Title_DealerPartsRetailOrder_RetailOrderType,ErrorStrings.Export_Title_DealerPartsRetailOrder_Customer,ErrorStrings.Export_Title_DealerPartsRetailOrder_CustomerPhone,
                                ErrorStrings.Export_Title_DealerPartsRetailOrder_SubDealerName,ErrorStrings.Export_Title_DealerPartsRetailOrder_CustomerCellPhone,ErrorStrings.Export_Title_DealerPartsRetailOrder_CustomerUnit,ErrorStrings.Export_Title_DealerPartsRetailOrder_Address,ErrorStrings.Export_Title_InternalAllocationBill_TotalAmount,ErrorStrings.Export_Title_PartsOuterPurchaseChange_ApproveAmont,ErrorStrings.Export_Title_PartsBranch_Remark,ErrorStrings.Export_Title_AccountPeriod_CreatorName,ErrorStrings.Export_Title_AccountPeriod_CreateTime,ErrorStrings.Export_Title_AccountPeriod_ModifierName,ErrorStrings.Export_Title_AccountPeriod_ModifyTime,ErrorStrings.Export_Title_PackingPropertyApp_ApproverName,ErrorStrings.Export_Title_PackingPropertyApp_ApproveTime,ErrorStrings.Export_Title_PartsBranch_AbandonerName,ErrorStrings.Export_Title_PartsBranch_AbandonerTime
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }

        }

    }
}
