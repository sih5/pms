﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using Sunlight.Silverlight.Web;
using System.Globalization;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        /// <summary>
        /// 合并导出配件零售订单及清单
        /// </summary>
        public bool ExportPartsRetailOrderWithDetail(int[] ids, string customerName, int? warehouseId, string customerCellPhone, int? status,string code,string sparePartCode, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName) {
            fileName = GetExportFilePath("合并导出配件零售订单及清单.xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {
                    var sql = new StringBuilder();
                    #region SQL
                    sql.Append(@"SELECT a.Code,
                                        a.WarehouseName,
                                        a.Salesunitname,
                                        a.Customername,
                                        a.Customercellphone,
                                        a.customeraddress,
                                        a.Originalamount,
                                        a.DiscountedAmount,
                                        (select value from keyvalueitem where NAME = 'PayOutBill_PaymentMethod'and key=a.PaymentMethod) As PaymentMethod,
                                        (select value from keyvalueitem where NAME = 'PartsRetailOrder_Status'and key=a.status) As status,
                                        a.creatorname,
                                        a.createtime,
                                        a.modifiername,
                                        a.modifytime,
                                        a.approvername,
                                        a.approvetime,
                                        a.invoiceoperatorname,
                                        a.invoicetime,
                                        a.abandonername,
                                        a.abandontime,
                                        b.Sparepartcode,
                                        b.sparepartname,
                                        b.quantity,
                                        b.salesprice,
                                        b.discountamount,
                                        b.discountedprice,
                                        b.discountedamount,b.DelaerPrice,
                                        b.remark as detailRemark
                                    FROM PartsRetailOrder a
                                    inner join PartsRetailOrderDetail b on b.partsretailorderid = a.id where 1=1 ");
                    #endregion

                    var dbParameters = new List<DbParameter>();
                    #region 条件过滤
                    //默认查询登录企业相关零售单
                    var userinfo = Utils.GetCurrentUserInfo();
                    sql.Append(" and a.SalesUnitOwnerCompanyId = {0}SalesUnitOwnerCompanyId");
                    dbParameters.Add(db.CreateDbParameter("SalesUnitOwnerCompanyId", userinfo.EnterpriseId));

                    if(ids != null && ids.Length > 0) {
                        sql.Append(" and a.id in (");
                        for(var i = 0; i < ids.Length; i++) {
                            if(ids.Length == i + 1) {
                                sql.Append("{0}id" + i);
                                dbParameters.Add(db.CreateDbParameter("id" + i, ids[i]));
                            } else {
                                sql.Append("{0}id" + i + ",");
                                dbParameters.Add(db.CreateDbParameter("id" + i, ids[i]));
                            }
                        }
                        sql.Append(")");
                    } else {
                        if(!string.IsNullOrEmpty(code)) {
                            sql.Append(" and a.code like {0}code");
                            dbParameters.Add(db.CreateDbParameter("code", "%" + code + "%"));
                        }
                        if(!string.IsNullOrEmpty(sparePartCode)) {
                            sql.Append(" and b.SparePartCode like {0}sparePartCode");
                            dbParameters.Add(db.CreateDbParameter("sparePartCode", "%" + sparePartCode + "%"));
                        }
                        if(!string.IsNullOrEmpty(customerName)) {
                            sql.Append(" and a.customerName like {0}customerName");
                            dbParameters.Add(db.CreateDbParameter("customerName", "%" + customerName + "%"));
                        }
                        if(warehouseId.HasValue) {
                            sql.Append(" and a.warehouseId = {0}warehouseId ");
                            dbParameters.Add(db.CreateDbParameter("warehouseId", warehouseId.Value));
                        }
                        if(!string.IsNullOrEmpty(customerCellPhone)) {
                            sql.Append(" and a.customerCellPhone like {0}customerCellPhone ");
                            dbParameters.Add(db.CreateDbParameter("customerCellPhone", "%" + customerCellPhone + "%"));
                        }
                        if(status.HasValue) {
                            sql.Append(" and a.status={0}status");
                            dbParameters.Add(db.CreateDbParameter("status", status.Value));
                        }
                        if(createTimeBegin.HasValue) {
                            sql.Append(@" and a.createTime >=to_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(@" and a.createTime <=to_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                    }
                    #endregion
                    var sqlStr = sql.ToString();
                    if(sqlStr.IndexOf(" where 1=1  and ") > 0)
                        sqlStr = sqlStr.Replace(" where 1=1  and ", " where ");
                    else
                        sqlStr = sqlStr.Replace(" where 1=1 ", "");
                    var command = db.CreateDbCommand(string.Format(sqlStr, db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    conn.Open();
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    //列名
                                    ErrorStrings.Export_Title_DealerPartsRetailOrder_Code,ErrorStrings.Export_Title_Company_WarehouseName,ErrorStrings.Export_Title_PartsSalesOrder_SalesUnitName,ErrorStrings.Export_Title_DealerPartsRetailOrder_Customer,ErrorStrings.Export_Title_DealerPartsRetailOrder_CustomerCellPhone,ErrorStrings.Export_Title_DealerPartsRetailOrder_Address,
                                    ErrorStrings.Export_Title_PartsRetailOrder_Originalamount,ErrorStrings.Export_Title_PartsRetailOrder_DiscountedAmount,ErrorStrings.Export_Title_Payoutbill_Paymentmethod,ErrorStrings.Export_Title_AccountPeriod_Status,
                                    ErrorStrings.Export_Title_AccountPeriod_CreatorName,ErrorStrings.Export_Title_AccountPeriod_CreateTime,ErrorStrings.Export_Title_AccountPeriod_ModifierName,ErrorStrings.Export_Title_AccountPeriod_ModifyTime,ErrorStrings.Export_Title_PackingPropertyApp_ApproverName,ErrorStrings.Export_Title_PackingPropertyApp_ApproveTime,ErrorStrings.Export_Title_PartsRetailOrder_InvoiceOperatorName,ErrorStrings.Export_Title_PartsRetailOrder_InvoiceTime,ErrorStrings.Export_Title_PartsBranch_AbandonerName,ErrorStrings.Export_Title_PartsBranch_AbandonerTime,
                                    ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode,ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName,ErrorStrings.Export_Title_WarehouseArea_Quantity,ErrorStrings.Export_Title_PartsRetailOrder_SalesPrice,ErrorStrings.Export_Titile_PartsSalesRtnSettlement_Discount,ErrorStrings.Export_Title_PartsRetailOrder_DiscountedPrice,ErrorStrings.Export_Title_PartsRetailOrder_DiscountedAmountAfter,"服务站价",ErrorStrings.Export_Title_PartsPurchaseOrder_DetailRemark
                                };
                            }
                            if(reader.Read()) {
                                var values = new[] {
                                    reader[0],reader[1],reader[2],reader[3],reader[4],reader[5],reader[6],reader[7],reader[8],reader[9],
                                    reader[10],reader[11],reader[12],reader[13],reader[14],reader[15],reader[16],reader[17],reader[18],
                                    reader[19],reader[20],reader[21],reader[22],reader[23],reader[24],reader[25],reader[26],reader[27]
                                };
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }

        public bool 导入服务站配件零售订单(int partsSalesCategoryId, int dealerId, int subDealerId, string fileName, out int excelImportNum, out List<DealerPartsRetailOrderExtend> rightData, out List<DealerPartsRetailOrderExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<DealerPartsRetailOrderExtend>();
            var rightList = new List<DealerPartsRetailOrderExtend>();
            var allList = new List<DealerPartsRetailOrderExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("DealerPartsRetailOrder", out notNullableFields, out fieldLenght);
                List<object> excelColumns;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode, "PartsCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName, "PartsName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_WarehouseArea_Quantity, "QuantityStr");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_InternalAllocationBill_UnitPrice, "PriceStr");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Remark, "Remark");
                    excelOperator.AddColumnDataSource("标签码", "SIHLabelCode");

                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var dealerPartsRetailOrder = new DealerPartsRetailOrderExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        dealerPartsRetailOrder.PartsCode = newRow["PartsCode"];
                        dealerPartsRetailOrder.PartsName = newRow["PartsName"];
                        dealerPartsRetailOrder.QuantityStr = newRow["QuantityStr"];
                        dealerPartsRetailOrder.PriceStr = newRow["PriceStr"];
                        dealerPartsRetailOrder.Remark = newRow["Remark"];
                        dealerPartsRetailOrder.SIHLabelCode = newRow["SIHLabelCode"];
                        var tempErrorMessage = new List<string>();

                        #region 导入的数据基本检查
                        //配件编号检查
                        if(string.IsNullOrEmpty(dealerPartsRetailOrder.PartsCode)) {
                            tempErrorMessage.Add(ErrorStrings.ImpPartsPurchaseOrderDetail_Validation1);
                        } else {
                            //配件长度不得超过25
                            if(Encoding.Default.GetByteCount(dealerPartsRetailOrder.PartsCode) > 25) //fieldLenght["CODE"]
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchaseOrderDetail_Validation2);
                        }

                        //数量检查
                        if(string.IsNullOrEmpty(row["QuantityStr"])) {
                            tempErrorMessage.Add(ErrorStrings.Export_Validation_InternalAcquisitionDetail_QuantityIsNull);
                        } else {
                            try {
                                var quantity = Convert.ToInt32(row["QuantityStr"]);
                                if(quantity > 0) {
                                    dealerPartsRetailOrder.Quantity = quantity;
                                } else {
                                    tempErrorMessage.Add(ErrorStrings.Export_Validation_InternalAcquisitionDetail_QuantityOverZero);
                                }
                            } catch(Exception) {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_InternalAcquisitionDetail_QuantityNumber);
                            }
                        }
                        //单价检查
                        if(string.IsNullOrEmpty(row["PriceStr"])) {
                            tempErrorMessage.Add(ErrorStrings.Export_Validatiob_PartsRetailOrder_PriceStrIsNull);
                        } else {
                            try {
                                var price = Convert.ToDecimal(row["PriceStr"]);
                                if(price > 0) {
                                    dealerPartsRetailOrder.Price = price;
                                } else {
                                    tempErrorMessage.Add(ErrorStrings.Export_Validatiob_PartsRetailOrder_PriceStrZero);
                                }
                            } catch(Exception) {
                                tempErrorMessage.Add(ErrorStrings.Export_Validatiob_PartsRetailOrder_PriceStrInteger);
                            }
                        }
                        #endregion

                        if(tempErrorMessage.Count > 0) {
                            dealerPartsRetailOrder.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(dealerPartsRetailOrder);
                        return false;
                    });

                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    #region 剩下的数据进行业务检查
                    //校验导入的数据配件编号重复
                    var groups = allList.GroupBy(r => r.PartsCode).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        if(groupItem.ErrorMsg == null) {
                            groupItem.ErrorMsg = ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode + groupItem.PartsCode + ErrorStrings.Export_Validation_PartsBranch_Validation5;
                        } else {
                            groupItem.ErrorMsg = groupItem.ErrorMsg + ";" + ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode + groupItem.PartsCode + ErrorStrings.Export_Validation_PartsBranch_Validation5;
                        }
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    //配件有效性校验
                    var partsCodeNeedCheck = tempRightList.Select(r => r.PartsCode.ToUpper()).Distinct().ToArray();
                    var dbSpareParts = new List<SparePartExtend>();
                    Func<string[], bool> getDbSpareParts = value => {
                        dbSpareParts.Add(new SparePartExtend {
                            Id = Convert.ToInt32(value[0]),
                            Code = value[1],
                            Name = value[2],
                            TraceProperty = Convert.ToInt32(value[3]),
                        });
                        return false;
                    };
                    db.QueryDataWithInOperator("Select Id,Code,Name,nvl(TraceProperty,0) From SparePart Where status=1 ", "Code", true, partsCodeNeedCheck, getDbSpareParts);
                    //查询经销商配件库存
                    var sparePartIdNeedCheck = dbSpareParts.Select(r => r.Id.ToString(CultureInfo.InvariantCulture)).Distinct().ToArray();
                    var dbDealerPartsStocks = new List<DealerPartsStockExtend>();
                    Func<string[], bool> getDbDealerPartsStocks = value => {
                        dbDealerPartsStocks.Add(new DealerPartsStockExtend {
                            SparePartId = Convert.ToInt32(value[0]),
                            Quantity = Convert.ToInt32(value[1])
                        });
                        return false;
                    };
                    var sql = string.Format("Select SparePartId,Quantity From DealerPartsStock Where SalesCategoryId={0} And DealerId={1} And SubDealerId={2} ", partsSalesCategoryId, dealerId, subDealerId);
                    db.QueryDataWithInOperator(sql, "SparePartId", false, sparePartIdNeedCheck, getDbDealerPartsStocks);
                    //查询配件零售指导价
                    var dbPartsRetailGuidePrices = new List<PartsRetailGuidePriceExtend>();
                    Func<string[], bool> getDbPartsRetailGuidePrices = value => {
                        dbPartsRetailGuidePrices.Add(new PartsRetailGuidePriceExtend {
                            SparePartId = Convert.ToInt32(value[0]),
                            RetailGuidePrice = Convert.ToDecimal(value[1])
                        });
                        return false;
                    };
                    var sql1 = string.Format("Select SparePartId,RetailGuidePrice From PartsRetailGuidePrice Where PartsSalesCategoryId={0} ", partsSalesCategoryId);
                    db.QueryDataWithInOperator(sql1, "SparePartId", false, sparePartIdNeedCheck, getDbPartsRetailGuidePrices);
                    foreach(var tempRight in tempRightList) {
                        var sparePart = dbSpareParts.FirstOrDefault(v => v.Code == tempRight.PartsCode.ToUpper());
                        if(sparePart == null) {
                            tempRight.ErrorMsg = ErrorStrings.Export_Validation_InternalAcquisitionDetail_Validation1;
                            continue;
                        }
                        var dealerPartsStock = dbDealerPartsStocks.FirstOrDefault(v => v.SparePartId == sparePart.Id);
                        if(dealerPartsStock == null) {
                            tempRight.ErrorMsg = ErrorStrings.Export_Validation_DealerRetailOrderDetail_QuantityNotExist;
                            continue;
                        }
                        if(dealerPartsStock.Quantity < tempRight.Quantity) {
                            tempRight.ErrorMsg = ErrorStrings.Export_Validation_DealerRetailOrderDetail_Validation1;
                            continue;
                        }
                        var partsRetailGuidePrice = dbPartsRetailGuidePrices.FirstOrDefault(v => v.SparePartId == sparePart.Id);
                        if(partsRetailGuidePrice == null) {
                            tempRight.ErrorMsg = ErrorStrings.Export_Validation_DealerRetailOrderDetail_Validation2;
                            continue;
                        }
                        tempRight.PartsId = sparePart.Id;
                        tempRight.PartsCode = sparePart.Code;
                        tempRight.PartsName = sparePart.Name;
                        tempRight.TraceProperty = sparePart.TraceProperty;
                        //tempRight.Price = partsRetailGuidePrice.RetailGuidePrice;
                    }

                    #endregion

                    //获取所有不合格数据
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    //获取所有合格数据
                    rightList = allList.Except(errorList).ToList();

                    #region 将合格数据的值填上
                    foreach(var rightItem in rightList) {
                        rightItem.Remark = rightItem.Remark;
                        rightItem.SIHLabelCode = rightItem.SIHLabelCode;
                    }
                    #endregion
                }

                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempSparePart = list[index - 1];
                            var values = new object[] {
                                tempSparePart.PartsCode,
                                tempSparePart.PartsName,
                                tempSparePart.QuantityStr,
                                tempSparePart.PriceStr,
                                tempSparePart.Remark,
                                tempSparePart.SIHLabelCode,
                                tempSparePart.ErrorMsg
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }

        public bool 导入配件零售订单(int storageCompanyId, int warehouseId, string fileName, out int excelImportNum, out List<PartsRetailOrderExtend> rightData, out List<PartsRetailOrderExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<PartsRetailOrderExtend>();
            var rightList = new List<PartsRetailOrderExtend>();
            var allList = new List<PartsRetailOrderExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("DealerPartsRetailOrder", out notNullableFields, out fieldLenght);
                List<object> excelColumns;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode, "PartsCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName, "PartsName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_WarehouseArea_Quantity, "QuantityStr");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Validatiob_PartsRetailOrder_DiscountPriceStr, "DiscountPriceStr");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Remark, "Remark");

                    #region 导入的数据基本检查
                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var partsRetailOrder = new PartsRetailOrderExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        partsRetailOrder.PartsCode = newRow["PartsCode"];
                        partsRetailOrder.PartsName = newRow["PartsName"];
                        partsRetailOrder.QuantityStr = newRow["QuantityStr"];
                        partsRetailOrder.DiscountPriceStr = newRow["DiscountPriceStr"];
                        partsRetailOrder.Remark = newRow["Remark"];

                        var tempErrorMessage = new List<string>();
                        #region 导入的数据基本检查
                        //配件编号检查
                        if(string.IsNullOrEmpty(partsRetailOrder.PartsCode)) {
                            tempErrorMessage.Add(ErrorStrings.ImpPartsPurchaseOrderDetail_Validation1);
                        } else {
                            //配件长度不得超过25
                            if(Encoding.Default.GetByteCount(partsRetailOrder.PartsCode) > 25) //fieldLenght["CODE"]
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchaseOrderDetail_Validation2);
                        }

                        //数量检查
                        if(string.IsNullOrEmpty(row["QuantityStr"])) {
                            tempErrorMessage.Add(ErrorStrings.Export_Validation_InternalAcquisitionDetail_QuantityIsNull);
                        } else {
                            try {
                                var quantity = Convert.ToInt32(row["QuantityStr"]);
                                if(quantity > 0) {
                                    partsRetailOrder.Quantity = quantity;
                                } else {
                                    tempErrorMessage.Add(ErrorStrings.Export_Validation_InternalAcquisitionDetail_QuantityOverZero);
                                }
                            } catch(Exception) {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_InternalAcquisitionDetail_QuantityNumber);
                            }
                        }
                        //折扣后价格检查
                        if(string.IsNullOrEmpty(row["DiscountPriceStr"])) {
                            tempErrorMessage.Add(ErrorStrings.Export_Validatiob_PartsRetailOrder_DiscountPriceStrIsNull);
                        } else {
                            try {
                                var discountPrice = Convert.ToDecimal(row["DiscountPriceStr"]);
                                if(discountPrice > 0) {
                                    partsRetailOrder.DiscountPrice = discountPrice;
                                } else {
                                    tempErrorMessage.Add(ErrorStrings.Export_Validatiob_PartsRetailOrder_DiscountPriceStrIsZero);
                                }
                            } catch(Exception) {
                                tempErrorMessage.Add(ErrorStrings.Export_Validatiob_PartsRetailOrder_DiscountPriceStrIsInteger);
                            }
                        }
                        #endregion

                        if(tempErrorMessage.Count > 0) {
                            partsRetailOrder.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(partsRetailOrder);
                        return false;
                    });
                    #endregion

                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    #region 剩下的数据进行业务检查
                    //校验导入的数据配件编号重复
                    var groups = allList.GroupBy(r => r.PartsCode).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        if(groupItem.ErrorMsg == null) {
                            groupItem.ErrorMsg = ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode + groupItem.PartsCode + ErrorStrings.Export_Validation_PartsBranch_Validation5;
                        } else {
                            groupItem.ErrorMsg = groupItem.ErrorMsg + ";" + ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode + groupItem.PartsCode + ErrorStrings.Export_Validation_PartsBranch_Validation5;
                        }
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    //配件有效性校验
                    var partsCodeNeedCheck = tempRightList.Select(r => r.PartsCode.ToUpper()).Distinct().ToArray();
                    var dbSpareParts = new List<SparePartExtend>();
                    Func<string[], bool> getDbSpareParts = value => {
                        dbSpareParts.Add(new SparePartExtend {
                            Id = Convert.ToInt32(value[0]),
                            Code = value[1],
                            Name = value[2]
                        });
                        return false;
                    };
                    db.QueryDataWithInOperator("Select Id,Code,Name From SparePart Where status=1 ", "Code", true, partsCodeNeedCheck, getDbSpareParts);
                    //查询配件库存
                    var sparePartIdNeedCheck = dbSpareParts.Select(r => r.Id.ToString(CultureInfo.InvariantCulture)).Distinct().ToArray();
                    var dbDealerPartsStocks = new List<DealerPartsStockExtend>();
                    Func<string[], bool> getDbDealerPartsStocks = value => {
                        dbDealerPartsStocks.Add(new DealerPartsStockExtend {
                            SparePartId = Convert.ToInt32(value[0]),
                            Quantity = Convert.ToInt32(value[1])
                        });
                        return false;
                    };
                    var sql = string.Format(@"Select a.PartId,a.Quantity 
                                              From PartsStock a 
                                              inner join WarehouseAreaCategory b 
                                              on a.WarehouseAreaCategoryId = b.id and b.Category in ({2},{3}) 
                                              Where a.StorageCompanyId={0} And a.WarehouseId={1}  ", storageCompanyId, warehouseId, (int)DcsAreaType.保管区, (int)DcsAreaType.检验区);
                    db.QueryDataWithInOperator(sql, "a.PartId", false, sparePartIdNeedCheck, getDbDealerPartsStocks);
                    //查询配件零售指导价
                    var dbPartsRetailGuidePrices = new List<PartsRetailGuidePriceExtend>();
                    Func<string[], bool> getDbPartsRetailGuidePrices = value => {
                        dbPartsRetailGuidePrices.Add(new PartsRetailGuidePriceExtend {
                            SparePartId = Convert.ToInt32(value[0]),
                            RetailGuidePrice = Convert.ToDecimal(value[1])
                        });
                        return false;
                    };
                    var sql1 = string.Format(@"Select a.SparePartId,a.RetailGuidePrice 
                                               From PartsRetailGuidePrice a
                                               inner join SalesUnit b
                                               on b.PartsSalesCategoryId = a.PartsSalesCategoryId
                                               inner join SalesUnitAffiWarehouse c
                                               on c.SalesUnitId = b.Id
                                               Where c.WarehouseId={0} ", warehouseId);
                    db.QueryDataWithInOperator(sql1, "a.SparePartId", false, sparePartIdNeedCheck, getDbPartsRetailGuidePrices);
                    foreach(var tempRight in tempRightList) {
                        var sparePart = dbSpareParts.FirstOrDefault(v => v.Code == tempRight.PartsCode.ToUpper());
                        if(sparePart == null) {
                            tempRight.ErrorMsg = ErrorStrings.Export_Validation_InternalAcquisitionDetail_Validation1;
                            continue;
                        }
                        var dealerPartsStock = dbDealerPartsStocks.FirstOrDefault(v => v.SparePartId == sparePart.Id);
                        if(dealerPartsStock == null) {
                            tempRight.ErrorMsg = ErrorStrings.Export_Validation_DealerRetailOrderDetail_QuantityNotExist;
                            continue;
                        }
                        if(dbDealerPartsStocks.Sum(r => r.Quantity) < tempRight.Quantity) {
                            tempRight.ErrorMsg = ErrorStrings.Export_Validation_DealerRetailOrderDetail_Validation1;
                            continue;
                        }
                        var partsRetailGuidePrice = dbPartsRetailGuidePrices.FirstOrDefault(v => v.SparePartId == sparePart.Id);
                        if(partsRetailGuidePrice == null) {
                            tempRight.ErrorMsg = ErrorStrings.Export_Validation_DealerRetailOrderDetail_Validation2;
                            continue;
                        }
                        if(tempRight.DiscountPrice > partsRetailGuidePrice.RetailGuidePrice) {
                            tempRight.ErrorMsg = ErrorStrings.Export_Validatiob_PartsRetailOrder_Validation1;
                            continue;
                        }
                        tempRight.PartsId = sparePart.Id;
                        tempRight.PartsCode = sparePart.Code;
                        tempRight.PartsName = sparePart.Name;
                        tempRight.Price = partsRetailGuidePrice.RetailGuidePrice;
                    }

                    #endregion

                    //获取所有不合格数据
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    //获取所有合格数据
                    rightList = allList.Except(errorList).ToList();

                    #region 将合格数据的值填上
                    foreach(var rightItem in rightList) {
                        rightItem.Remark = rightItem.Remark;
                    }
                    #endregion
                }

                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempSparePart = list[index - 1];
                            var values = new object[] {
                                tempSparePart.PartsCode,
                                tempSparePart.PartsName,
                                tempSparePart.QuantityStr,
                                tempSparePart.DiscountPriceStr,
                                tempSparePart.Remark,
                                tempSparePart.ErrorMsg
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }

    }
}
