﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        /// <summary>
        /// 合并导出配件零售退货单及详情
        /// </summary>
        public bool ExportPartsRetailReturnBillWithDetail(int[] ids, string customerName, string customerCellPhone, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName) {
            fileName = GetExportFilePath("合并导出配件零售退货订单及清单.xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {
                    var sql = new StringBuilder();
                    #region SQL
                    sql.Append(@"select a.Code,
                                        a.SourceCode,
                                        a.CustomerName,
                                        a.CustomerPhone,
                                        a.CustomerCellPhone,
                                        a.CustomerAddress,
                                        a.totalamount,
                                        a.returnreason,
                                        a.WarehouseName,
                                        (select value from keyvalueitem where NAME = 'PartsRetailReturnBill_Status'and key=a.status) As status,
                                        a.Remark,
                                        a.CreatorName,
                                        a.CreateTime,
                                        b.sparepartcode,
                                        b.sparepartname,
                                        b.quantity,
                                        b.originalorderprice,
                                        b.returnprice
                                    FROM PartsRetailReturnBill a
                                    LEFT OUTER JOIN PartsRetailReturnBillDetail b ON a.Id = b.PartsRetailReturnBillId where 1=1 ");
                    #endregion

                    var dbParameters = new List<DbParameter>();
                    #region 条件过滤
                    //默认查询登录企业相关零售单
                    var userinfo = Utils.GetCurrentUserInfo();
                    sql.Append(" and a.SalesUnitOwnerCompanyId = {0}SalesUnitOwnerCompanyId");
                    dbParameters.Add(db.CreateDbParameter("SalesUnitOwnerCompanyId", userinfo.EnterpriseId));

                    if(ids != null && ids.Length > 0) {
                        sql.Append(" and a.id in (");
                        for(var i = 0; i < ids.Length; i++) {
                            if(ids.Length == i + 1) {
                                sql.Append("{0}id" + i);
                                dbParameters.Add(db.CreateDbParameter("id" + i, ids[i]));
                            } else {
                                sql.Append("{0}id" + i + ",");
                                dbParameters.Add(db.CreateDbParameter("id" + i, ids[i]));
                            }
                        }
                        sql.Append(")");
                    } else {
                        if(!string.IsNullOrEmpty(customerName)) {
                            sql.Append(" and a.customerName like {0}customerName");
                            dbParameters.Add(db.CreateDbParameter("customerName", "%" + customerName + "%"));
                        }
                        if(!string.IsNullOrEmpty(customerCellPhone)) {
                            sql.Append(" and a.customerCellPhone like {0}customerCellPhone ");
                            dbParameters.Add(db.CreateDbParameter("customerCellPhone", "%" + customerCellPhone + "%"));
                        }
                        if(createTimeBegin.HasValue) {
                            sql.Append(@" and a.createTime >=to_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(@" and a.createTime <=to_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                    }
                    #endregion
                    var sqlStr = sql.ToString();
                    if(sqlStr.IndexOf(" where 1=1  and ") > 0)
                        sqlStr = sqlStr.Replace(" where 1=1  and ", " where ");
                    else
                        sqlStr = sqlStr.Replace(" where 1=1 ", "");
                    var command = db.CreateDbCommand(string.Format(sqlStr, db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    conn.Open();
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    //列名
                                    ErrorStrings.Export_Title_PackingPropertyApp_Code,ErrorStrings.Export_Title_DealerPartsSalesReturnBill_SourceCode,ErrorStrings.Export_Title_DealerPartsRetailOrder_Customer,ErrorStrings.Export_Title_DealerPartsRetailOrder_CustomerPhone,ErrorStrings.Export_Title_DealerPartsRetailOrder_CustomerCellPhone,ErrorStrings.Export_Title_DealerPartsRetailOrder_Address,
                                    ErrorStrings.Export_Title_DealerPartsSalesReturnBill_TotalAmount,ErrorStrings.Export_Title_PartsPurReturnOrder_Reason,ErrorStrings.Export_Title_Company_WarehouseName,ErrorStrings.Export_Title_AccountPeriod_Status,
                                    ErrorStrings.Export_Title_PartsBranch_Remark,ErrorStrings.Export_Title_AccountPeriod_CreatorName,ErrorStrings.Export_Title_AccountPeriod_CreateTime,
                                    ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode,ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName,ErrorStrings.Export_Title_WarehouseArea_Quantity,ErrorStrings.Export_Title_PartsRetailReturnBill_OriginalorderPrice,ErrorStrings.Export_Title_PartsSalesReturnBill_ReturnPrice
                                };
                            }
                            if(reader.Read()) {
                                var values = new[] {
                                    reader[0],reader[1],reader[2],reader[3],reader[4],reader[5],reader[6],reader[7],reader[8],reader[9],
                                    reader[10],reader[11],reader[12],reader[13],reader[14],reader[15],reader[16],reader[17]
                                };
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }
    }
}
