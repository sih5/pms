﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {

        public bool ImportCustomerOrderPriceGrade(string fileName, out int excelImportNum, out List<CustomerOrderPriceGradeExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<CustomerOrderPriceGradeExtend>();
            var allList = new List<CustomerOrderPriceGradeExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("CustomerOrderPriceGrade", out notNullableFields, out fieldLenght);
                List<object> excelColumns;
                List<CustomerOrderPriceGradeExtend> rightList;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    #region 指定文件中的列对应的名称 例如 客户企业编号、客户企业名称、品牌、订单类型名称、系数
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Credenceapplication_CompanyCode, "CustomerCompanyCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Credenceapplication_CompanyName, "CustomerCompanyName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Partssalescategory_Name, "PartsSalesCategoryName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_CustomerOrderPriceGrade_PartsSalesOrderTypeName, "PartsSalesOrderTypeName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_CustomerOrderPriceGrade_Coefficient, "Coefficient");
                    #endregion
                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;
                    #region 获取对应枚举
                    var keyValuePairs = new[] { new KeyValuePair<string, string>("MasterDataStatus", "MasterData_Status") };
                    tempExcelOperator.ImportHelper.LoadEnumFromDb(null, null, keyValuePairs);
                    #endregion
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var tempImportObj = new CustomerOrderPriceGradeExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        #region 根据导入的字段给对应的实体属性赋值
                        tempImportObj.CustomerCompanyCode = newRow["CustomerCompanyCode"];
                        tempImportObj.CustomerCompanyName = newRow["CustomerCompanyName"];
                        tempImportObj.PartsSalesCategoryName = newRow["PartsSalesCategoryName"];
                        tempImportObj.PartsSalesOrderTypeName = newRow["PartsSalesOrderTypeName"];
                        tempImportObj.CoefficientStr = newRow["Coefficient"];
                        #endregion

                        var tempErrorMessage = new List<string>();
                        #region 导入的数据基本检查
                        //客户企业编号
                        var fieldIndex = notNullableFields.IndexOf("CustomerCompanyCode".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.CustomerCompanyCode)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validarion_CustomerOrderPriceGrade_CustomerCompanyCodeIsNull);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.CustomerCompanyCode) > fieldLenght["CustomerCompanyCode".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validarion_CustomerOrderPriceGrade_CustomerCompanyCodeIsLong);
                        }
                        //品牌
                        fieldIndex = notNullableFields.IndexOf("PartsSalesCategoryName".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.PartsSalesCategoryName)) {
                            tempErrorMessage.Add(ErrorStrings.Export_Validation_MarketingDepartment_PartsSalesCategoryIsNull);
                        }
                        //订单类型名称
                        fieldIndex = notNullableFields.IndexOf("PartsSalesOrderTypeName".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.PartsSalesOrderTypeName)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validarion_CustomerOrderPriceGrade_PartsSalesOrderTypeIsNull);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.PartsSalesOrderTypeName) > fieldLenght["PartsSalesOrderTypeName".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validarion_CustomerOrderPriceGrade_PartsSalesOrderTypeLong);
                        }
                        //系数
                        fieldIndex = notNullableFields.IndexOf("Coefficient".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.CoefficientStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_CustomerOrderPriceGrade_CoefficientNull);
                        }
                        #endregion
                        if(tempErrorMessage.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });
                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    #region 剩下的数据进行业务检查
                    //4、唯一性校验：客户企业编号、品牌、订单类型名称，否则提示：数据重复
                    var groups = tempRightList.GroupBy(r => new {
                        r.CustomerCompanyCode,
                        r.PartsSalesCategoryName,
                        r.PartsSalesOrderTypeName
                    }).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        groupItem.ErrorMsg = ErrorStrings.ImpPartsSupplierRelation_Validation31;
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();


                    //根据导入数据的客户企业编号查询企业表，如果不存在，导出错误信息：企业编码***不存在。
                    var customerCompanyCodesNeedCheck = tempRightList.Select(r => r.CustomerCompanyCode).Distinct().ToArray();
                    var dbCompanys = new List<CompanyExtend>();
                    Func<string[], bool> getDbCompanys = value => {
                        var dbObj = new CompanyExtend {
                            Id = Convert.ToInt32(value[0]),
                            Code = value[1],
                            Name = value[2]
                        };
                        dbCompanys.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select Id,Code,Name from Company where status=1 ", "Code", false, customerCompanyCodesNeedCheck, getDbCompanys);
                    foreach(var tempRight in tempRightList) {
                        var repairItem = dbCompanys.FirstOrDefault(v => v.Code == tempRight.CustomerCompanyCode);
                        if(repairItem == null) {
                            tempRight.ErrorMsg = ErrorStrings.Export_Validation_CustomerOrderPriceGrade_Validation1;
                            continue;
                        }
                        tempRight.CustomerCompanyId = repairItem.Id;
                        tempRight.CustomerCompanyName = repairItem.Name;
                        tempRight.CustomerCompanyCode = repairItem.Code;

                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    //根据导入数据的品牌查询品牌表 ，如果不存在，导出错误信息：品牌***不存在。
                    var partsSalesCategoryNamesNeedCheck = tempRightList.Select(r => r.PartsSalesCategoryName).Distinct().ToArray();
                    var dbPartsSalesCategorys = new List<PartsSalesCategory>();
                    Func<string[], bool> getdbPartsSalesCategorys = value => {
                        var dbObj = new PartsSalesCategory {
                            Id = Convert.ToInt32(value[0]),
                            Name = value[1]
                        };
                        dbPartsSalesCategorys.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select Id,Name from PartsSalesCategory where status=1 ", "Name", false, partsSalesCategoryNamesNeedCheck, getdbPartsSalesCategorys);
                    foreach(var tempRight in tempRightList) {
                        var repairItem = dbPartsSalesCategorys.FirstOrDefault(v => v.Name == tempRight.PartsSalesCategoryName);
                        if(repairItem == null) {
                            tempRight.ErrorMsg = ErrorStrings.ImpVehicleDLRStartSecurityDeposit_Validation8;
                            continue;
                        }
                        tempRight.PartsSalesCategoryId = repairItem.Id;
                        tempRight.PartsSalesCategoryName = repairItem.Name;
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    //根据导入数据的订单类型名称查询配件销售订单表 ，如果不存在，导出错误信息：订单类型名称***不存在
                    var partsSalesOrderTypeNamesNeedCheck = tempRightList.Select(r => r.PartsSalesOrderTypeName).Distinct().ToArray();
                    var dbPartsSalesOrders = new List<PartsSalesOrderType>();
                    Func<string[], bool> getDbPartsSalesOrders = value => {
                        var dbObj = new PartsSalesOrderType {
                            Id = Convert.ToInt32(value[0]),
                            Code = value[1],
                            Name = value[2],
                            PartsSalesCategoryId = Convert.ToInt32(value[3])
                        };
                        dbPartsSalesOrders.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select Id,Code,Name,PartsSalesCategoryId from PartsSalesOrderType where status=1 ", "Name", false, partsSalesOrderTypeNamesNeedCheck, getDbPartsSalesOrders);
                    foreach(var tempRight in tempRightList) {
                        var repairItem = dbPartsSalesOrders.FirstOrDefault(v => v.Name == tempRight.PartsSalesOrderTypeName && v.PartsSalesCategoryId == tempRight.PartsSalesCategoryId);
                        if(repairItem == null) {
                            tempRight.ErrorMsg = ErrorStrings.Export_Validation_CustomerOrderPriceGrade_Validation2;
                            continue;
                        }
                        tempRight.PartsSalesOrderTypeId = repairItem.Id;
                        tempRight.PartsSalesOrderTypeName = repairItem.Name;
                        tempRight.PartsSalesOrderTypeCode = repairItem.Code;

                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    //3系统内重复校验，查询客户企业订单价格等级表中（客户企业编号、品牌、订单类型名称）系统内唯一，否则导出错误信息：客户企业编号***、品牌***、订单类型名称***已存在对应关系。"
                    var customerCompanyIdsNeedCheck = tempRightList.Select(r => r.CustomerCompanyId.ToString(CultureInfo.InvariantCulture)).Distinct().ToArray();
                    var dbCustomerOrderPriceGrades = new List<CustomerOrderPriceGradeExtend>();
                    Func<string[], bool> getDbCustomerOrderPriceGrades = value => {
                        var dbObj = new CustomerOrderPriceGradeExtend {
                            CustomerCompanyId = Convert.ToInt32(value[0]),
                            PartsSalesCategoryId = Convert.ToInt32(value[1]),
                            PartsSalesOrderTypeId = Convert.ToInt32(value[2])
                        };
                        dbCustomerOrderPriceGrades.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select CustomerCompanyId,PartsSalesCategoryId,PartsSalesOrderTypeId from CustomerOrderPriceGrade where status=1 ", "CustomerCompanyId", false, customerCompanyIdsNeedCheck, getDbCustomerOrderPriceGrades);
                    foreach(var tempRight in tempRightList) {
                        var usedPartsDistanceInfor = dbCustomerOrderPriceGrades.FirstOrDefault(v => v.CustomerCompanyId == tempRight.CustomerCompanyId && v.PartsSalesOrderTypeId == tempRight.PartsSalesOrderTypeId && v.PartsSalesCategoryId == tempRight.PartsSalesCategoryId);
                        if(usedPartsDistanceInfor != null) {
                            tempRight.ErrorMsg = ErrorStrings.Export_Validation_CustomerOrderPriceGrade_Validation3;
                        }
                    }
                    #endregion
                    //获取所有不合格数据
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    //获取所有合格数据
                    rightList = allList.Except(errorList).ToList();
                    #region 将合格数据的值填上
                    foreach(var rightItem in rightList) {
                        //rightItem.CustomerCompanyId = rightItem.c;
                        //rightItem.ContactPhone = rightItem.ContactPhoneStr;
                        rightItem.Status = tempExcelOperator.ImportHelper.GetEnumValue("MasterDataStatus", ErrorStrings.Export_Validation_PartsBranch_Used) ?? 0;
                        rightItem.Coefficient = Convert.ToDouble(rightItem.CoefficientStr);
                    }
                    #endregion
                }
                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempObj = list[index - 1];
                            var values = new object[] {
                                #region 设置错误信息导出的列的值

                                tempObj.CustomerCompanyCode,tempObj.CustomerCompanyName,
                                tempObj.PartsSalesCategoryName,tempObj.PartsSalesOrderTypeName,tempObj.Coefficient,
                                tempObj.ErrorMsg
                                
                                #endregion                     
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }

                //导入所有合格数据
                if(!rightList.Any())
                    return true;
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    //开启事务，新增更新配件在一个事务内
                    var ts = conn.BeginTransaction();
                    try {
                        //新增配件
                        if(rightList.Any()) {
                            #region 获取对应的插入的SQL 通过db.GetInsertSql方法
                            //获取新增数据的sql语句，Id为主键
                            var sqlInsert = db.GetInsertSql("CustomerOrderPriceGrade", "Id", new[] {
                               "BranchId", "CustomerCompanyId", "CustomerCompanyCode", "CustomerCompanyName", "PartsSalesCategoryId","PartsSalesOrderTypeId","PartsSalesOrderTypeCode","PartsSalesOrderTypeName","Coefficient", "Status", "CreatorId", "CreatorName", "CreateTime"
                            });
                            #endregion
                            var userInfo = Utils.GetCurrentUserInfo();
                            //往数据库增加市场部信息
                            foreach(var item in rightList) {
                                #region 添加Sql的参数
                                var command = db.CreateDbCommand(sqlInsert, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("BranchId", userInfo.EnterpriseId));
                                command.Parameters.Add(db.CreateDbParameter("CustomerCompanyId", item.CustomerCompanyId));
                                command.Parameters.Add(db.CreateDbParameter("CustomerCompanyCode", item.CustomerCompanyCode));
                                command.Parameters.Add(db.CreateDbParameter("CustomerCompanyName", item.CustomerCompanyName));
                                command.Parameters.Add(db.CreateDbParameter("PartsSalesCategoryId", item.PartsSalesCategoryId));
                                command.Parameters.Add(db.CreateDbParameter("PartsSalesOrderTypeId", item.PartsSalesOrderTypeId));
                                command.Parameters.Add(db.CreateDbParameter("PartsSalesOrderTypeCode", item.PartsSalesOrderTypeCode));
                                command.Parameters.Add(db.CreateDbParameter("PartsSalesOrderTypeName", item.PartsSalesOrderTypeName));
                                command.Parameters.Add(db.CreateDbParameter("Coefficient", item.Coefficient));
                                command.Parameters.Add(db.CreateDbParameter("Status", item.Status));
                                command.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                command.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                command.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                command.ExecuteNonQuery();
                                #endregion
                            }
                            var sqlInsert1 = db.GetInsertSql("CustomerOrderPriceGradeHistory", "Id", new[] {
                               "BranchId", "CustomerCompanyId", "CustomerCompanyCode", "CustomerCompanyName", "PartsSalesCategoryId","PartsSalesOrderTypeId","PartsSalesOrderTypeCode","PartsSalesOrderTypeName","Coefficient", "Status", "CreatorId", "CreatorName", "CreateTime"
                            });


                            //往数据库增加市场部信息
                            foreach(var item in rightList) {
                                #region 添加Sql的参数
                                var command1 = db.CreateDbCommand(sqlInsert1, conn, ts);
                                command1.Parameters.Add(db.CreateDbParameter("BranchId", userInfo.EnterpriseId));
                                command1.Parameters.Add(db.CreateDbParameter("CustomerCompanyId", item.CustomerCompanyId));
                                command1.Parameters.Add(db.CreateDbParameter("CustomerCompanyCode", item.CustomerCompanyCode));
                                command1.Parameters.Add(db.CreateDbParameter("CustomerCompanyName", item.CustomerCompanyName));
                                command1.Parameters.Add(db.CreateDbParameter("PartsSalesCategoryId", item.PartsSalesCategoryId));
                                command1.Parameters.Add(db.CreateDbParameter("PartsSalesOrderTypeId", item.PartsSalesOrderTypeId));
                                command1.Parameters.Add(db.CreateDbParameter("PartsSalesOrderTypeCode", item.PartsSalesOrderTypeCode));
                                command1.Parameters.Add(db.CreateDbParameter("PartsSalesOrderTypeName", item.PartsSalesOrderTypeName));
                                command1.Parameters.Add(db.CreateDbParameter("Coefficient", item.Coefficient));
                                command1.Parameters.Add(db.CreateDbParameter("Status", item.Status));
                                command1.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                command1.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                command1.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                command1.ExecuteNonQuery();
                                #endregion
                            }

                        }
                        //无异常提交
                        ts.Commit();
                    } catch(Exception) {
                        //报错回滚
                        ts.Rollback();
                        throw;
                    } finally {
                        if(conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
            }
        }

    }
}
