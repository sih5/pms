﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using Sunlight.Silverlight.Web;
using System.Data.Common;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        public bool ExportOrderapproveweekday(int? weeksName, int? partsSalesCategoryId, DateTime? createTimeBegin, DateTime? createTimeEnd, DateTime? modifyTimeBegin, DateTime? modifyTimeEnd, int? status, out string fileName) {
            fileName = GetExportFilePath("储备订单自动审核日与省份关系_" + ".xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.Append(@"select PartsSalesCategoryName,
                                   ProvinceName,
                                   k1.value as WeeksName,
                                   k2.value  as  Status,
                                   CreatorName,
                                   CreateTime,
                                   ModifierName,
                                   ModifyTime 
                            from orderapproveweekday a left join keyvalueitem k1 on k1.key=a.weeksname and k1.name='OrderAppDate'
                           left join keyvalueitem k2 on k2.key=a.status and k2.name='BaseData_Status' where 1=1 ");
                    var dbParameters = new List<DbParameter>();
                    if(partsSalesCategoryId.HasValue) {
                        sql.Append(@" and a.partsSalesCategoryId ={0}partsSalesCategoryId ");
                        dbParameters.Add(db.CreateDbParameter("partsSalesCategoryId", partsSalesCategoryId.Value));
                    }
                    if(weeksName.HasValue) {
                        sql.Append(@" and a.weeksName ={0}weeksName ");
                        dbParameters.Add(db.CreateDbParameter("weeksName", weeksName.Value));
                    }
                    if(status.HasValue) {
                        sql.Append(@" and a.status ={0}Status");
                        dbParameters.Add(db.CreateDbParameter("status", status.Value));
                    }
                    if(createTimeBegin.HasValue) {
                        sql.Append(@" and a.createTime >=To_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                        var tempValue = createTimeBegin.Value;
                        var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                        dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                    }
                    if(createTimeEnd.HasValue) {
                        sql.Append(@" and a.createTime <=To_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                        var tempValue = createTimeEnd.Value;
                        var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                        dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                    }
                    if(modifyTimeBegin.HasValue) {
                        sql.Append(@" and a.modifyTime >=To_date({0}modifyTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                        var tempValue = modifyTimeBegin.Value;
                        var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                        dbParameters.Add(db.CreateDbParameter("modifyTimeBegin", tempTime.ToString("G")));
                    }
                    if(modifyTimeEnd.HasValue) {
                        sql.Append(@" and a.modifyTime <=To_date({0}modifyTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                        var tempValue = modifyTimeEnd.Value;
                        var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                        dbParameters.Add(db.CreateDbParameter("modifyTimeEnd", tempTime.ToString("G")));
                    }
                    var sqlStr = sql.ToString();
                    if(sqlStr.IndexOf(" where 1=1  and ") > 0)
                        sqlStr = sqlStr.Replace(" where 1=1  and ", " where ");
                    else
                        sqlStr = sqlStr.Replace(" where 1=1 ", "");
                    var command = db.CreateDbCommand(string.Format(sqlStr, db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                        ErrorStrings.Export_Title_Partssalescategory_Name, ErrorStrings.Export_Validation_Company_ProvinceName, ErrorStrings.Export_Title_OrderApproveWeekday_WeeksName, ErrorStrings.Export_Title_AccountPeriod_Status, ErrorStrings.Export_Title_AccountPeriod_CreatorName, ErrorStrings.Export_Title_AccountPeriod_CreateTime, ErrorStrings.Export_Title_AccountPeriod_ModifierName, ErrorStrings.Export_Title_AccountPeriod_ModifyTime
                                    };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }

                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }

        }

        public bool ImportOrderapproveweekday(string fileName, out int excelImportNum, out List<OrderapproveweekdayExtend> rightData, out List<OrderapproveweekdayExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<OrderapproveweekdayExtend>();
            var allList = new List<OrderapproveweekdayExtend>();
            var rightList = new List<OrderapproveweekdayExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("Orderapproveweekday", out notNullableFields, out fieldLenght);

                List<object> excelColumns;

                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    #region 指定文件中的列对应的名称
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Partssalescategory_Name, "PartsSalesCategoryName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Validation_Company_ProvinceName, "ProvinceName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_OrderApproveWeekday_WeeksName, "WeeksName");
                    #endregion

                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;

                    #region 获取对应枚举
                    var keyValuePairs = new[] {
                        new KeyValuePair<string, string>("WeeksName", "OrderAppDate")
                    };
                    tempExcelOperator.ImportHelper.LoadEnumFromDb(null, null, keyValuePairs);

                    #endregion

                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var tempImportObj = new OrderapproveweekdayExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        #region 根据导入的字段给对应的实体属性赋值
                        tempImportObj.PartsSaleCategoryNameStr = newRow["PartsSalesCategoryName"].Trim();
                        tempImportObj.ProvinceNameStr = newRow["ProvinceName"].Trim();
                        tempImportObj.WeeksNameStr = newRow["WeeksName"].Trim();
                        #endregion

                        var tempErrorMessage = new List<string>();

                        #region 导入的数据基本检查

                        //品牌检查
                        var fieldIndex = notNullableFields.IndexOf("PartsSalesCategoryName".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.PartsSaleCategoryNameStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_MarketingDepartment_PartsSalesCategoryIsNull);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.PartsSaleCategoryNameStr) > fieldLenght["PartsSalesCategoryName".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_PartsSalescategoryNameIsLong);
                        }

                        //省份
                        fieldIndex = notNullableFields.IndexOf("ProvinceName".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.ProvinceNameStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_OrderApproveWeekday_ProvinceNameIsNull);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.ProvinceNameStr) > fieldLenght["ProvinceName".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_OrderApproveWeekday_ProvinceNameLong);
                        }

                        //储备订单审核日
                        fieldIndex = notNullableFields.IndexOf("WeeksName".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.WeeksNameStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_OrderApproveWeekday_WeeksNameIsNull);
                        } else {
                            var tempEnumValue = tempExcelOperator.ImportHelper.GetEnumValue("WeeksName", tempImportObj.WeeksNameStr);
                            if(!tempEnumValue.HasValue) {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_OrderApproveWeekday_WeeksNameValueError);
                            }
                        }
                        #endregion

                        if(tempErrorMessage.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });
                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    var groups = tempRightList.GroupBy(r => new {
                        r.PartsSaleCategoryNameStr,
                        r.ProvinceNameStr,
                        r.WeeksNameStr
                    }).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        groupItem.ErrorMsg = ErrorStrings.Export_Validation_OrderApproveWeekday_Validation1;
                    }
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();

                    #region 剩下的数据进行业务检查
                    //1.查询品牌信息  校验品牌是否存在
                    var partSalesCategotyNeedCheck = tempRightList.Select(r => r.PartsSaleCategoryNameStr.ToUpper()).Distinct().ToArray();
                    var dbPartsSalesCategory = new List<PartsSalesCategoryExtend>();
                    Func<string[], bool> getDbPartsSalesCategories = value => {
                        var dbObj = new PartsSalesCategoryExtend {
                            Id = Convert.ToInt32(value[0]),
                            Name = value[1]
                        };
                        dbPartsSalesCategory.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select Id,Trim(Name) from partssalescategory where status=1 ", "Name", true, partSalesCategotyNeedCheck, getDbPartsSalesCategories);
                    foreach(var tempRight in tempRightList) {
                        var partsSalesCategoty = dbPartsSalesCategory.FirstOrDefault(r => r.Name == tempRight.PartsSaleCategoryNameStr);
                        if(partsSalesCategoty == null) {
                            tempRight.ErrorMsg = String.Format(ErrorStrings.ImpPartsSupplierRelation_Validation28, tempRight.PartsSaleCategoryNameStr);
                        } else {
                            tempRight.PartsSaleCategoryId = partsSalesCategoty.Id;
                            tempRight.PartsSaleCategoryName = partsSalesCategoty.Name;
                        }
                    }
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();
                    //2.查询省份 校验省份信息
                    var provinceNeedCheck = tempRightList.Select(r => r.ProvinceNameStr.ToUpper()).Distinct().ToArray();
                    var dbRegion = new List<RegionExtend>();
                    Func<string[], bool> getDBRegions = value => {
                        var dbObj = new RegionExtend {
                            Id = Convert.ToInt32(value[0]),
                            Name = value[1]
                        };
                        dbRegion.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select Id,Trim(Name) from region where type=2 and status=2 ", "Name", true, provinceNeedCheck, getDBRegions);
                    foreach(var tempRight in tempRightList) {
                        var region = dbRegion.FirstOrDefault(r => r.Name == tempRight.ProvinceNameStr);
                        if(region == null) {
                            tempRight.ErrorMsg = String.Format(ErrorStrings.Export_Validation_OrderApproveWeekday_Validation2, tempRight.ProvinceNameStr);
                        } else {
                            tempRight.ProvinceId = region.Id;
                            tempRight.ProvinceName = region.Name;
                        }
                    }

                    #endregion

                    //获取所有不合格数据
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    //获取所有合格数据
                    rightList = allList.Except(errorList).ToList();

                    #region 将合格数据的值填上
                    foreach(var rightItem in rightList) {
                        rightItem.WeekName = tempExcelOperator.ImportHelper.GetEnumValue("WeeksName", rightItem.WeeksNameStr) ?? 0;
                    }
                    #endregion

                    //检查 数据库是否已经存在
                    var orderapproveweekdayCheck = tempRightList.Select(r => r.ProvinceNameStr.ToUpper()).Distinct().ToArray();
                    var dbOrderApproveWeekday = new List<OrderapproveweekdayExtend>();
                    Func<string[], bool> getOrderapproveweekday = value => {
                        var dbObj = new OrderapproveweekdayExtend {
                            PartsSaleCategoryName = value[0],
                            ProvinceName = value[1],
                            WeekName = Convert.ToInt32(value[2])
                        };
                        dbOrderApproveWeekday.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select PartsSalesCategoryName, ProvinceName,WeeksName from orderapproveweekday where status=1 ", "ProvinceName", true, orderapproveweekdayCheck, getOrderapproveweekday);
                    foreach(var tempRight in tempRightList) {
                        if(dbOrderApproveWeekday.Any(r => r.PartsSaleCategoryName == tempRight.PartsSaleCategoryName && r.ProvinceName == tempRight.ProvinceName && r.WeekName == tempRight.WeekName)) {
                            tempRight.ErrorMsg = ErrorStrings.Export_Validation_OrderApproveWeekday_Validation3;
                        }
                    }
                    //获取所有不合格数据
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    //获取所有合格数据
                    rightList = allList.Except(errorList).ToList();

                }
                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempObj = list[index - 1];
                            var values = new object[] {
                                #region 设置错误信息导出的列的值
                                tempObj.PartsSaleCategoryNameStr, tempObj.ProvinceNameStr, 
                                tempObj.WeeksNameStr,tempObj.ErrorMsg
                                #endregion
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }
                if(!rightList.Any())
                    return true;
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    //开启事务，新增更新配件在一个事务内
                    var ts = conn.BeginTransaction();
                    try {
                        //新增关系
                        if(rightList.Any()) {
                            #region 获取对应的插入的SQL 通过db.GetInsertSql方法
                            //获取新增数据的sql语句，Id为主键
                            var sqlInsert = db.GetInsertSql("orderapproveweekday", "Id", new[] {
                                    "PartsSalesCategoryId", "PartsSalesCategoryName", "ProvinceId","ProvinceName", "WeeksName","Status","CreatorId","CreatorName","CreateTime"
                                });
                            #endregion
                            var userinfo = Utils.GetCurrentUserInfo();
                            //往数据库增加配件信息
                            foreach(var item in rightList) {
                                #region 添加Sql的参数
                                var command = db.CreateDbCommand(sqlInsert, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("PartsSalesCategoryId", item.PartsSaleCategoryId));
                                command.Parameters.Add(db.CreateDbParameter("PartsSalesCategoryName", item.PartsSaleCategoryName));
                                command.Parameters.Add(db.CreateDbParameter("ProvinceId", item.ProvinceId));
                                command.Parameters.Add(db.CreateDbParameter("ProvinceName", item.ProvinceName));
                                command.Parameters.Add(db.CreateDbParameter("WeeksName", item.WeekName));
                                command.Parameters.Add(db.CreateDbParameter("Status", (int)DcsBaseDataStatus.有效));
                                command.Parameters.Add(db.CreateDbParameter("CreatorId", userinfo.Id));
                                command.Parameters.Add(db.CreateDbParameter("CreatorName", userinfo.Name));
                                command.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                command.ExecuteNonQuery();
                                #endregion
                            }
                        }
                        //无异常提交
                        ts.Commit();
                    } catch(Exception) {
                        //报错回滚
                        ts.Rollback();
                        throw;
                    } finally {
                        if(conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }
    }
}
