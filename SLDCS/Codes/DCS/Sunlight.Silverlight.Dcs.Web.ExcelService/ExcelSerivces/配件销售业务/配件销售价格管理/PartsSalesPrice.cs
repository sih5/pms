﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        public bool ExportPartsSalesPrice(string sparePartCode, string sparePartName, int? partsSalesCategoryId, int? status, bool? isSalable, bool? isOrderable, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName) {
            fileName = GetExportFilePath("配件销售价_" + ".xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.Append(@"select    a.PartsSalesCategoryName,
                                           a.SparePartCode,
                                           a.SparePartName,
                                           a.SalesPrice,
                                           (select value from keyvalueitem where NAME = 'PartsSalesPrice_PriceType'and key=a.PriceType) As PriceType,
                                           (select value from keyvalueitem where NAME = 'IsOrNot'and key=a.IfClaim) As IfClaim,
                                           a.ValidationTime,
                                           a.ExpireTime,
                                           (select value from keyvalueitem where NAME = 'PartsSalesPrice_Status'and key=a.Status) As Status,
                                           a.Remark,
                                           a.CreatorName,
                                           a.CreateTime,
                                           a.ModifierName,
                                           a.ModifyTime,
                                           a.ApproverName,
                                           a.ApproveTime,
                                           a.AbandonerName
                                      from PartsSalesPrice a
                                     left join Partsbranch b
                                        on a.sparepartid = b.partid
                                       and a.partssalescategoryid = b.partssalescategoryid
                                     where 1 = 1");
                    var dbParameters = new List<DbParameter>();
                    if(!String.IsNullOrEmpty(sparePartCode)) {
                        sql.Append(@" and a.sparePartCode like {0}sparePartCode ");
                        dbParameters.Add(db.CreateDbParameter("sparePartCode", "%" + sparePartCode + "%"));
                    }
                    if(!String.IsNullOrEmpty(sparePartName)) {
                        sql.Append(@" and a.sparePartName like {0}sparePartName ");
                        dbParameters.Add(db.CreateDbParameter("sparePartName", "%" + sparePartName + "%"));
                    }
                    if(partsSalesCategoryId.HasValue) {
                        sql.Append(@" and a.partsSalesCategoryId ={0}partsSalesCategoryId ");
                        dbParameters.Add(db.CreateDbParameter("partsSalesCategoryId", partsSalesCategoryId.Value));
                    }
                    if(status.HasValue) {
                        sql.Append(@" and a.status ={0}Status");
                        dbParameters.Add(db.CreateDbParameter("status", status.Value));
                    }
                    if(isSalable.HasValue) {
                        sql.Append(@" and b.isSalable={0}isSalable");
                        dbParameters.Add(isSalable.Value ? db.CreateDbParameter("isSalable", 1) : db.CreateDbParameter("isSalable", 0));
                    }
                    if(isOrderable.HasValue) {
                        sql.Append(@" and b.isOrderable={0}isOrderable");
                        dbParameters.Add(isOrderable.Value ? db.CreateDbParameter("isOrderable", 1) : db.CreateDbParameter("isOrderable", 0));
                    }
                    if(createTimeBegin.HasValue) {
                        sql.Append(@" and a.createTime >=To_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                        var tempValue = createTimeBegin.Value;
                        var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                        dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                    }
                    if(createTimeEnd.HasValue) {
                        sql.Append(@" and a.createTime <=To_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                        var tempValue = createTimeEnd.Value;
                        var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                        dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                    }

                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                        ErrorStrings.Export_Title_Partssalescategory_Name, ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode, ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName, ErrorStrings.Export_Title_PartsExchangeGroupExtend_SalesPrice, ErrorStrings.Export_Title_PartsPurchaseOrder_PriceType, ErrorStrings.Export_Title_PartsSalesPrice_IfClaim, ErrorStrings.Export_Title_FactoryPurchacePrice_ValidFrom, ErrorStrings.Export_Title_FactoryPurchacePrice_ValidTo, ErrorStrings.Export_Title_AccountPeriod_Status, ErrorStrings.Export_Title_PartsBranch_Remark, ErrorStrings.Export_Title_AccountPeriod_CreatorName, ErrorStrings.Export_Title_AccountPeriod_CreateTime, ErrorStrings.Export_Title_AccountPeriod_ModifierName, ErrorStrings.Export_Title_AccountPeriod_ModifyTime, ErrorStrings.Export_Title_PackingPropertyApp_ApproverName, ErrorStrings.Export_Title_PackingPropertyApp_ApproveTime, ErrorStrings.Export_Title_PartsBranch_AbandonerName
                                    };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }

                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }

        }
    }
}
