﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        /// <summary>
        /// 导入配件销售价格信息
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="excelImportNum"></param>
        /// <param name="rightData"></param>
        /// <param name="errorData"></param>
        /// <param name="errorDataFileName"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public bool ImportPartsSalesPriceChangeDetail(string fileName, out int excelImportNum, out List<PartsSalesPriceChangeDetailExtend> rightData, out List<PartsSalesPriceChangeDetailExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<PartsSalesPriceChangeDetailExtend>();
            var allList = new List<PartsSalesPriceChangeDetailExtend>();
            var rightList = new List<PartsSalesPriceChangeDetailExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("PartsSalesPriceChangeDetail", out notNullableFields, out fieldLenght);

                List<object> excelColumns;

                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    #region 指定文件中的列对应的名称
                    //excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsPurchaseOrder_PriceType, "PriceType");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode, "SparePartCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName, "SparePartName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsSalesPriceChangeDetail_CenterPrice, "CenterPrice");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Dealerpartsinventorybill_Dealerprice, "SalesPrice");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartSstock_RetailGuidePrice, "RetailGuidePrice");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Remark, "Remark");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsPurchaseOrder_PriceType, "PriceTypeName");
                    #endregion

                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;

                    //#region 获取对应枚举
                    //var keyValuePairs = new[] {
                    //    new KeyValuePair<string, string>("PriceType", "PartsSalesPrice_PriceType")
                    //};
                    //tempExcelOperator.ImportHelper.LoadEnumFromDb(1, null, keyValuePairs);

                    //#endregion

                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var tempImportObj = new PartsSalesPriceChangeDetailExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);

                        #region 根据导入的字段给对应的实体属性赋值 tempImportObj.PartsSalesCategoryNameStr = row["PartsSalesCategoryName"].Trim();
                        //tempImportObj.PriceTypeStr = newRow["PriceType"];
                        tempImportObj.SparePartCodeStr = newRow["SparePartCode"];
                        tempImportObj.SparePartNameStr = newRow["SparePartName"];
                        tempImportObj.CenterPriceStr = newRow["CenterPrice"]; //中心库价
                        tempImportObj.SalesPriceStr = newRow["SalesPrice"]; //服务站价
                        tempImportObj.RetailGuidePriceStr = newRow["RetailGuidePrice"]; //建议售价
                        tempImportObj.RemarkStr = newRow["Remark"];
                        tempImportObj.PriceTypeNameStr = newRow["PriceTypeName"];
                        #endregion

                        var tempErrorMessage = new List<string>();

                        #region 导入的数据基本检查

                        //配件编号检查
                        var fieldIndex = notNullableFields.IndexOf("SparePartCode".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.SparePartCodeStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchaseOrderDetail_Validation1);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.SparePartCodeStr) > fieldLenght["SparePartCode".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchaseOrderDetail_Validation2);
                        }

                        //配件名称检查
                        fieldIndex = notNullableFields.IndexOf("SparePartName".ToUpper());
                        //if(string.IsNullOrEmpty(tempImportObj.SparePartNameStr)) {
                        //    if(fieldIndex > -1)
                        //        tempErrorMessage.Add(ErrorStrings.Export_Validation_Sparepart_Name);
                        //} else {
                        //    if(Encoding.Default.GetByteCount(tempImportObj.SparePartNameStr) > fieldLenght["SparePartName".ToUpper()])
                        //        tempErrorMessage.Add(ErrorStrings.Export_Validation_Sparepart_NameIsLong);
                        //}

                        //中心库价
                        if(string.IsNullOrEmpty(tempImportObj.CenterPriceStr)) {
                            tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsSalesPriceChangeDetail_CenterPriceIsNull);
                        } else {
                            decimal checkValue;
                            if(decimal.TryParse(tempImportObj.CenterPriceStr, out checkValue)) {
                                if(checkValue < 0) {
                                    tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsSalesPriceChangeDetail_CenterPriceZero);
                                }
                            } else {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsSalesPriceChangeDetail_CenterPriceInteger);
                            }
                        }

                        //服务站价检查 --字段为销售价
                        fieldIndex = notNullableFields.IndexOf("SalesPrice".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.SalesPriceStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsSalesPriceChangeDetail_SalesPriceIsNull);
                        } else {
                            decimal checkValue;
                            if(decimal.TryParse(tempImportObj.SalesPriceStr, out checkValue)) {
                                if(checkValue <= 0) {
                                    tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsSalesPriceChangeDetail_SalesPriceZero);
                                }
                            } else {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsSalesPriceChangeDetail_SalesPriceInteger);
                            }
                        }

                        //零售指导价检查
                        if(string.IsNullOrEmpty(tempImportObj.RetailGuidePriceStr)) {
                            tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsSalesPriceChangeDetail_RetailGuidePriceNull);
                        } else {
                            decimal checkValue;
                            if(decimal.TryParse(tempImportObj.RetailGuidePriceStr, out checkValue)) {
                                if(checkValue < 0) {
                                    tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsSalesPriceChangeDetail_RetailGuidePriceIsZero);
                                }
                            } else {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsSalesPriceChangeDetail_RetailGuidePriceIsInteger);
                            }
                        }

                        //服务站批发价检查
                        //fieldIndex = notNullableFields.IndexOf("DealerSalesPrice".ToUpper());
                        //if(string.IsNullOrEmpty(tempImportObj.DealerSalesPriceStr)) {
                        //    if(fieldIndex > -1)
                        //        tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsSalesPriceChangeDetail_DealerSalesPriceIsNull);
                        //} else {
                        //    decimal checkValue;
                        //    if(decimal.TryParse(tempImportObj.DealerSalesPriceStr, out checkValue)) {
                        //        if(checkValue < 0) {
                        //            tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsSalesPriceChangeDetail_DealerSalesPriceIsZero);
                        //        }
                        //    } else {
                        //        tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsSalesPriceChangeDetail_DealerSalesPriceIsInteger);
                        //    }
                        //}

                        //价格类型
                        fieldIndex = notNullableFields.IndexOf("PriceTypeName".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.PriceTypeNameStr)) {
                                tempErrorMessage.Add(ErrorStrings.ImpVehicleDLRStartSecurityDeposit_Validation6);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.PriceTypeNameStr) > fieldLenght["PriceTypeName".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsSalesPriceChangeDetail_PriceTypeNameLong);
                        }

                        //备注检查
                        fieldIndex = notNullableFields.IndexOf("Remark".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.RemarkStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchasePricingDetail_Validation15);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.RemarkStr) > fieldLenght["Remark".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchasePricingDetail_Validation16);
                        }
                        #endregion

                        if(tempErrorMessage.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });
                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    #region 剩下的数据进行业务检查
                    //1.配件编号、配件名称组合存在配件信息中，否则提示：配件不存在
                    var sparePartCodesNeedCheck = tempRightList.Select(r => r.SparePartCodeStr.ToUpper()).Distinct().ToArray();
                    var dbSpareParts = new List<SparePartExtend>();
                    Func<string[], bool> getDbSpareParts = value => {
                        var dbObj = new SparePartExtend {
                            Id = Convert.ToInt32(value[0]),
                            Code = value[1],
                            Name = value[2]
                        };
                        dbSpareParts.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select Id,Trim(Code),Trim(Name) from SparePart where status=1 ", "Code", true, sparePartCodesNeedCheck, getDbSpareParts);
                    foreach(var tempRight in tempRightList) {
                        var oldSparePart = dbSpareParts.FirstOrDefault(v => v.Code == tempRight.SparePartCodeStr);
                        if(oldSparePart == null) {
                            tempRight.ErrorMsg = String.Format(ErrorStrings.Export_Validation_IvecoPriceChangeAppDetail_Validation1, tempRight.SparePartCodeStr);
                        } else {
                            tempRight.SparePartId = oldSparePart.Id;
                            tempRight.SparePartName = oldSparePart.Name;
                        }
                    }

                    var priceTypeNames = tempRightList.Where(r => !string.IsNullOrEmpty(r.PriceTypeNameStr)).Select(r => r.PriceTypeNameStr.ToUpper()).Distinct().ToArray();
                    if(priceTypeNames.Length > 0) {
                        var dbPartsSalePriceIncreaseRates = new List<PartsSalePriceIncreaseRateExtend>();
                        Func<string[], bool> getDbPartsSalePriceIncreaseRates = value => {
                            var dbObj = new PartsSalePriceIncreaseRateExtend {
                                Id = Convert.ToInt32(value[0]),
                                GroupCode = value[1],
                                GroupName = value[2]
                            };
                            dbPartsSalePriceIncreaseRates.Add(dbObj);
                            return false;
                        };
                        db.QueryDataWithInOperator("select Id,Trim(GroupCode),Trim(GroupName) from PartsSalePriceIncreaseRate where status=1 ", "GroupCode", true, priceTypeNames, getDbPartsSalePriceIncreaseRates);
                        foreach(var tempRight in tempRightList) {
                            var oldPartsSalePriceIncreaseRate = dbPartsSalePriceIncreaseRates.FirstOrDefault(v => v.GroupName == tempRight.PriceTypeNameStr);
                            if(oldPartsSalePriceIncreaseRate == null) {
                                tempRight.ErrorMsg = String.Format(ErrorStrings.Export_Validation_PartsSalesPriceChangeDetail_Validation5, tempRight.PriceTypeNameStr);
                            } else {
                                tempRight.PriceTypeId = oldPartsSalePriceIncreaseRate.Id;
                                tempRight.PriceTypeCode = oldPartsSalePriceIncreaseRate.GroupCode;
                                tempRight.PriceTypeName = oldPartsSalePriceIncreaseRate.GroupName;
                            }
                        }
                    }
                    #endregion

                    //获取所有不合格数据
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    //获取所有合格数据
                    rightList = allList.Except(errorList).ToList();

                    #region 将合格数据的值填上
                    foreach(var rightItem in rightList) {
                        //rightItem.PriceType = tempExcelOperator.ImportHelper.GetEnumValue("PriceType", rightItem.PriceTypeStr) ?? 0;
                        rightItem.SparePartCode = rightItem.SparePartCodeStr;
                        rightItem.CenterPrice = Convert.ToDecimal(rightItem.CenterPriceStr);//中心库价
                        rightItem.SalesPrice = Convert.ToDecimal(rightItem.SalesPriceStr); //服务站价
                        rightItem.RetailGuidePrice = Convert.ToDecimal(rightItem.RetailGuidePriceStr); //建议售价
                        rightItem.Remark = rightItem.RemarkStr;
                        rightItem.PriceTypeName = rightItem.PriceTypeNameStr;
                    }
                    #endregion
                }
                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempObj = list[index - 1];
                            var values = new object[] {
                                #region 设置错误信息导出的列的值
                                tempObj.SparePartCodeStr, tempObj.SparePartNameStr,tempObj.CenterPriceStr, tempObj.SalesPriceStr, tempObj.RetailGuidePriceStr, tempObj.RemarkStr,tempObj.PriceTypeNameStr, tempObj.ErrorMsg
                                #endregion
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }

                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }

        /// <summary>
        /// 导入配件销售价格信息 有品牌
        /// </summary>
        public bool ImportPartsSalesPriceChangeDetailPartsSalesCategory(string fileName, out int excelImportNum, out List<PartsSalesPriceChangeDetailExtend> rightData, out List<PartsSalesPriceChangeDetailExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<PartsSalesPriceChangeDetailExtend>();
            var allList = new List<PartsSalesPriceChangeDetailExtend>();
            var rightList = new List<PartsSalesPriceChangeDetailExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("PartsSalesPriceChangeDetail", out notNullableFields, out fieldLenght);

                List<object> excelColumns;

                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    #region 指定文件中的列对应的名称
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Partssalescategory_Name, "PartsSalesCategory");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsPurchaseOrder_PriceType, "PriceType");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode, "SparePartCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName, "SparePartName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsExchangeGroupExtend_SalesPrice, "SalesPrice");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsExchangeGroupExtend_RetailGuidePrice, "RetailGuidePrice");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsSalesPriceChangeDetail_DealerSalesPrice, "DealerSalesPrice");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Remark, "Remark");
                    #endregion

                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;

                    #region 获取对应枚举
                    var keyValuePairs = new[] {
                        new KeyValuePair<string, string>("PriceType", "PartsSalesPrice_PriceType")
                    };
                    tempExcelOperator.ImportHelper.LoadEnumFromDb(1, null, keyValuePairs);

                    #endregion

                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var tempImportObj = new PartsSalesPriceChangeDetailExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);

                        #region 根据导入的字段给对应的实体属性赋值 tempImportObj.PartsSalesCategoryNameStr = row["PartsSalesCategoryName"].Trim();
                        tempImportObj.PartsSalesCategoryNameStr = newRow["PartsSalesCategory"];
                        tempImportObj.PriceTypeStr = newRow["PriceType"];
                        tempImportObj.SparePartCodeStr = newRow["SparePartCode"];
                        tempImportObj.SparePartNameStr = newRow["SparePartName"];
                        tempImportObj.SalesPriceStr = newRow["SalesPrice"];
                        tempImportObj.RetailGuidePriceStr = newRow["RetailGuidePrice"];
                        tempImportObj.DealerSalesPriceStr = newRow["DealerSalesPrice"];
                        tempImportObj.RemarkStr = newRow["Remark"];
                        #endregion

                        var tempErrorMessage = new List<string>();

                        #region 导入的数据基本检查

                        //配件编号检查
                        var fieldIndex = notNullableFields.IndexOf("SparePartCode".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.SparePartCodeStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchaseOrderDetail_Validation1);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.SparePartCodeStr) > fieldLenght["SparePartCode".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchaseOrderDetail_Validation2);
                        }

                        if(string.IsNullOrEmpty(tempImportObj.PartsSalesCategoryNameStr)) {
                            tempErrorMessage.Add(ErrorStrings.Export_Validation_MarketingDepartment_PartsSalesCategoryIsNull);

                        }

                        //配件名称检查
                        fieldIndex = notNullableFields.IndexOf("SparePartName".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.SparePartNameStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Sparepart_Name);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.SparePartNameStr) > fieldLenght["SparePartName".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Sparepart_NameIsLong);
                        }

                        //销售价检查
                        fieldIndex = notNullableFields.IndexOf("SalesPrice".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.SalesPriceStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_validation_PartsSalesPriceChangeDetail_DealerSalesPriceNull);
                        } else {
                            decimal checkValue;
                            if(decimal.TryParse(tempImportObj.SalesPriceStr, out checkValue)) {
                                if(checkValue <= 0) {
                                    tempErrorMessage.Add(ErrorStrings.Export_validation_PartsSalesPriceChangeDetail_DealerSalesPriceZero);
                                }
                            } else {
                                tempErrorMessage.Add(ErrorStrings.Export_validation_PartsSalesPriceChangeDetail_DealerSalesPriceInteger);
                            }
                        }

                        //零售指导价检查
                        fieldIndex = notNullableFields.IndexOf("RetailGuidePrice".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.RetailGuidePriceStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsSalesPriceChangeDetail_RetailGuidePriceIsNull);
                        } else {
                            decimal checkValue;
                            if(decimal.TryParse(tempImportObj.RetailGuidePriceStr, out checkValue)) {
                                if(checkValue < 0) {
                                    tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsSalesPriceChangeDetail_RetailGuidePriceZero);
                                }
                            } else {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsSalesPriceChangeDetail_RetailGuidePriceInteger);
                            }
                        }

                        //服务站批发价检查
                        fieldIndex = notNullableFields.IndexOf("DealerSalesPrice".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.DealerSalesPriceStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsSalesPriceChangeDetail_DealerSalesPriceIsNull);
                        } else {
                            decimal checkValue;
                            if(decimal.TryParse(tempImportObj.DealerSalesPriceStr, out checkValue)) {
                                if(checkValue < 0) {
                                    tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsSalesPriceChangeDetail_DealerSalesPriceIsZero);
                                }
                            } else {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsSalesPriceChangeDetail_DealerSalesPriceIsInteger);
                            }
                        }

                        //价格类型
                        fieldIndex = notNullableFields.IndexOf("PriceType".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.PriceTypeStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.ImpVehicleDLRStartSecurityDeposit_Validation6);
                        } else {
                            var tempEnumValue = tempExcelOperator.ImportHelper.GetEnumValue("PriceType", tempImportObj.PriceTypeStr);
                            if(!tempEnumValue.HasValue) {
                                tempErrorMessage.Add(ErrorStrings.ImpVehicleDLRStartSecurityDeposit_Validation7);
                            }
                        }

                        //备注检查
                        fieldIndex = notNullableFields.IndexOf("Remark".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.RemarkStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchasePricingDetail_Validation15);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.RemarkStr) > fieldLenght["Remark".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchasePricingDetail_Validation16);
                        }
                        #endregion

                        if(tempErrorMessage.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });
                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    #region 剩下的数据进行业务检查
                    //校验品牌正确性
                    var partsSalesCategoryNameNeedCheck = tempRightList.Select(r => r.PartsSalesCategoryNameStr).Distinct().ToArray();
                    var dbPartsSalesCategorys = new List<PartsSalesCategoryExtend>();
                    Func<string[], bool> getDbPartsSalesCategorys = value => {
                        var dbObj = new PartsSalesCategoryExtend {
                            Id = Convert.ToInt32(value[0]),
                            Code = value[1],
                            Name = value[2]
                        };
                        dbPartsSalesCategorys.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select Id,Code,Name from PartsSalesCategory where status=1 ", "Name", true, partsSalesCategoryNameNeedCheck, getDbPartsSalesCategorys);
                    foreach(var tempRight in tempRightList) {
                        var partsSalesCategory = dbPartsSalesCategorys.FirstOrDefault(v => v.Name == tempRight.PartsSalesCategoryNameStr);
                        if(partsSalesCategory == null) {
                            tempRight.ErrorMsg = String.Format(ErrorStrings.Export_Validation_PartsSalesPriceChangeDetail_Validation1, tempRight.SparePartCodeStr);
                        } else {
                            tempRight.PartsSalesCategoryId = partsSalesCategory.Id;
                            tempRight.PartsSalesCategoryCode = partsSalesCategory.Code;
                            tempRight.PartsSalesCategoryName = partsSalesCategory.Name;
                        }
                    }

                    //1.配件编号、配件名称组合存在配件信息中，否则提示：配件不存在
                    var sparePartCodesNeedCheck = tempRightList.Select(r => r.SparePartCodeStr.ToUpper()).Distinct().ToArray();
                    var dbSpareParts = new List<SparePartExtend>();
                    Func<string[], bool> getDbSpareParts = value => {
                        var dbObj = new SparePartExtend {
                            Id = Convert.ToInt32(value[0]),
                            Code = value[1],
                            Name = value[2]
                        };
                        dbSpareParts.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select Id,Trim(Code),Trim(Name) from SparePart where status=1 ", "Code", true, sparePartCodesNeedCheck, getDbSpareParts);
                    foreach(var tempRight in tempRightList) {
                        var oldSparePart = dbSpareParts.FirstOrDefault(v => v.Code == tempRight.SparePartCodeStr);
                        if(oldSparePart == null) {
                            tempRight.ErrorMsg = String.Format(ErrorStrings.Export_Validation_IvecoPriceChangeAppDetail_Validation1, tempRight.SparePartCodeStr);
                        } else {
                            tempRight.SparePartId = oldSparePart.Id;
                            tempRight.SparePartName = oldSparePart.Name;
                        }
                    }
                    //获取互换件最高价
                    var sparePartIdsNeedCheck = tempRightList.Select(r => r.SparePartId.ToString()).Distinct().ToArray();
                    var dbPartsSalesPrices = new List<PartsSalesPriceExtend>();
                    Func<string[], bool> getDbPartsSalesPrices = value => {
                        var dbObj = new PartsSalesPriceExtend {
                            SparePartId = Convert.ToInt32(value[0]),
                            SalesPrice = value[1] == "" ? 0 : decimal.Parse(value[1]),
                        };
                        dbPartsSalesPrices.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select * from (select b.PartId,max(a.SalesPrice) from PartsSalesPrice a inner join (select a.PartId,b.PartId as  SparePartId  from PartsExchange a inner join PartsExchange b on a.ExchangeCode=b.ExchangeCode ) b  on a.SparePartId=b.SparePartId where  a.status=1  and a.ValidationTime <=sysdate  and a.ExpireTime>=sysdate group by b.PartId ) where 1=1 ", "PartId", true, sparePartIdsNeedCheck, getDbPartsSalesPrices);
                    foreach(var tempRight in tempRightList) {
                        var partsSalesPrice = dbPartsSalesPrices.FirstOrDefault(v => v.SparePartId == tempRight.SparePartId);
                        tempRight.MaxExchangeSalePrice = 0;
                        if(partsSalesPrice != null) {
                            tempRight.MaxExchangeSalePrice = partsSalesPrice.SalesPrice;
                        }
                    }


                    #endregion

                    //获取所有不合格数据
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    //获取所有合格数据
                    rightList = allList.Except(errorList).ToList();

                    #region 将合格数据的值填上
                    foreach(var rightItem in rightList) {
                        rightItem.PriceType = tempExcelOperator.ImportHelper.GetEnumValue("PriceType", rightItem.PriceTypeStr) ?? 0;
                        rightItem.SparePartCode = rightItem.SparePartCodeStr;
                        rightItem.SalesPrice = Convert.ToDecimal(rightItem.SalesPriceStr);
                        rightItem.RetailGuidePrice = Convert.ToDecimal(rightItem.RetailGuidePriceStr);
                        rightItem.DealerSalesPrice = Convert.ToDecimal(rightItem.DealerSalesPriceStr);
                        rightItem.Remark = rightItem.RemarkStr;
                    }
                    #endregion
                }
                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempObj = list[index - 1];
                            var values = new object[] {
                                #region 设置错误信息导出的列的值
                                tempObj.PartsSalesCategoryNameStr, tempObj.PriceTypeStr, tempObj.SparePartCodeStr, tempObj.SparePartNameStr, tempObj.SalesPriceStr, tempObj.RetailGuidePriceStr, tempObj.DealerSalesPriceStr, tempObj.RemarkStr, tempObj.ErrorMsg
                                #endregion
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }

        public bool ExportPartsSalesPriceChange(int[] ids, int? partsSalesCategoryId, string code, int? status,string sparePartCode,string sparePartName, DateTime? creatorTimeBegin, DateTime? creatorTimeEnd, out string fileName) {
            fileName = GetExportFilePath("配件销售价格变更申请_" + ".xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);
                var userInfo = Utils.GetCurrentUserInfo();
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.AppendFormat(@"select t.code,/*销售价变更申请单编号*/
                                        t.PartsSalesCategoryCode,/*品牌编号*/
                                        t.PartsSalesCategoryName,/*品牌*/
                                        (select value from keyvalueitem where name = 'PartsSalesPriceChange_Status' and key = t.status) status,/*状态*/
                                        t.CreatorName,/*创建人*/
                                        t.CreateTime,/*创建时间*/
                                        t.ModifierName,/*修改人*/
                                        t.ModifyTime,/*修改时间*/
                                        t.InitialApproverName,/*初审人*/
                                        t.InitialApproveTime,/*初审时间*/
                                        t.ApproverName,/*审核人*/
                                        t.ApproveTime,/*审核时间*/
                                        t.FinalApproverName,/*审批人*/
                                        t.FinalApproveTime,/*审批时间*/
                                        t.AbandonerName,/*作废人*/
                                        t.AbandonTime,/*作废时间*/
                                        t.AbandonComment,/*作废原因*/
                                        t.Rejecter,/*驳回人*/
                                        t.RejectTime,/*驳回时间*/
                                        t.Remark /*备注*/
                                   from PartsSalesPriceChange t
                                    left join PartsSalesPriceChangeDetail d on d.parentid = t.id 
                                    where branchid={0} ", userInfo.EnterpriseId);
                    var tableNames = sql.ToString().GetRegexNames();
                    var dbParameters = new List<DbParameter>();
                    if(ids != null && ids.Length > 0) {
                        sql.Append(" and t.id in (");
                        for(var i = 0; i < ids.Length; i++) {
                            if(ids.Length == i + 1) {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            } else {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                        }
                        sql.Append(")");
                        sql.Append(" group by t.code,t.PartsSalesCategoryCode, t.PartsSalesCategoryName,t.status, t.CreatorName, t.CreateTime,t.ModifierName,t.ModifyTime,t.InitialApproverName,t.InitialApproveTime,t.FinalApproverName,t.FinalApproveTime,t.AbandonerName,t.AbandonTime,t.AbandonComment,t.Rejecter,t.RejectTime,t.Remark,t.ApproverName,t.ApproveTime ");
                    } else {
                        if(!String.IsNullOrEmpty(code)) {
                            sql.Append(@" and t.Code like {0}Code ");
                            dbParameters.Add(db.CreateDbParameter("Code", "%" + code + "%"));
                        }
                        if(!String.IsNullOrEmpty(sparePartCode)) {
                            sql.Append(@" and d.SparePartCode like {0}sparePartCode ");
                            dbParameters.Add(db.CreateDbParameter("sparePartCode", "%" + sparePartCode + "%"));
                        }
                        if(!String.IsNullOrEmpty(sparePartName)) {
                            sql.Append(@" and d.SparePartName like {0}sparePartName ");
                            dbParameters.Add(db.CreateDbParameter("sparePartName", "%" + sparePartName + "%"));
                        }
                        if(status.HasValue) {
                            sql.Append(@" and t.status ={0}status ");
                            dbParameters.Add(db.CreateDbParameter("status", status.Value));
                        }
                        if(creatorTimeBegin.HasValue) {
                            sql.Append(@" and t.createTime >=To_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = creatorTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(creatorTimeEnd.HasValue) {
                            sql.Append(@" and t.createTime <=To_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = creatorTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                        sql.Append(" group by t.code,t.PartsSalesCategoryCode, t.PartsSalesCategoryName,t.status, t.CreatorName, t.CreateTime,t.ModifierName,t.ModifyTime,t.InitialApproverName,t.InitialApproveTime,t.FinalApproverName,t.FinalApproveTime,t.AbandonerName,t.AbandonTime,t.AbandonComment,t.Rejecter,t.RejectTime,t.Remark,t.ApproverName,t.ApproveTime  ");
                    }
                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return tableNames;
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }

                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }

        //        业务调整，方法屏蔽重写
        //        public bool ExportPartsSalesPriceChangeDetails(int[] ids, int? partsSalesCategoryId, string code, int? status, DateTime? creatorTimeBegin, DateTime? creatorTimeEnd, out string fileName) {
        //            fileName = GetExportFilePath("配件销售价格变更申请主清单_" + ".xlsx");
        //            try {
        //                if(!HttpContext.Current.User.Identity.IsAuthenticated)
        //                    throw new Exception(ErrorStrings.File_Validation_User);

        //                var db = DbHelper.GetDbHelp(ConnectionString);
        //                var userInfo = Utils.GetCurrentUserInfo();
        //                using(var conn = db.CreateDbConnection()) {
        //                    conn.Open();
        //                    var sql = new StringBuilder();
        //                    sql.AppendFormat(@"select t.code,/*销售价变更申请单编号*/
        //                                        t.PartsSalesCategoryCode,/*品牌编号*/
        //                                        t.PartsSalesCategoryName,/*品牌*/
        //                                        (select value from keyvalueitem where name = 'PartsSalesPriceChange_Status' and key = t.status) status,/*状态*/
        //                                        t.CreatorName,/*创建人*/
        //                                        t.CreateTime,/*创建时间*/
        //                                        t.ModifierName,/*修改人*/
        //                                        t.ModifyTime,/*修改时间*/
        //                                        t.InitialApproverName,/*初审人*/
        //                                        t.InitialApproveTime,/*初审时间*/
        //                                        t.FinalApproverName,/*终审人*/
        //                                        t.FinalApproveTime,/*终审时间*/
        //                                        t.AbandonerName,/*作废人*/
        //                                        t.AbandonTime,/*作废时间*/
        //                                        t.AbandonComment,/*作废原因*/
        //                                        t.Rejecter,/*驳回人*/
        //                                        t.RejectTime,/*驳回时间*/
        //                                        t.Remark, /*备注*/
        //                                        d.SparePartCode,/*配件编号*/
        //                                        d.SparePartName,/*配件名称*/
        //                                        d.SalesPrice,/*服务站价变更比率*/
        //                                        (select SalesPrice from PartsSalesPrice p where p.Status=1 and p.PartsSalesCategoryId=t.PartsSalesCategoryId and p.sparepartid=d.sparepartid and rownum=1) ReferencePrice,/*参考价变更比率*/
        //                                        a.PurchasePrice,/*采购价变更比率*/
        //                                        d.MaxPurchasePricing,/*采购最高价变更比率*/
        //                                        d.MaxExchangeSalePrice,/*互换件最高价变更比率*/
        //                                        (select value from keyvalueitem where name = 'IsOrNot' and key = d.IsUpsideDown) IsUpsideDown,/*是否倒挂*/
        //                                        cast((SalesPrice/a.PurchasePrice-1)*100 as number(7,2)),/*销售加价率*/
        //                                        cast((d.RetailGuidePrice/d.SalesPrice-1)*100 as number(7,2)),/*零售加价率*/
        //                                        cast((d.DealerSalesPrice/d.SalesPrice-1)*100 as number(7,2)),/*服务站批发价加价率*/
        //                                        --d.DealerSalesPrice,/*服务站批发价变更比率*/
        //                                        d.RetailGuidePrice,/*零售指导价变更比率*/
        //                                        (select value from keyvalueitem where name = 'PartsSalesPrice_PriceType' and key = d.PriceType)  PriceType，/*价格类型*/
        //                                        d.Remark /*备注*/
        //                                   from PartsSalesPriceChange t
        //                                    left join PartsSalesPriceChangeDetail d on t.id=d.ParentId
        //                                     left join ( select PartsSalesCategoryId,PurchasePrice,PartId from PartsPurchasePricing p where  p.status=2 and p.ValidFrom<= SYSDATE and p.ValidTo>= SYSDATE  ) a
        //                                     on  a.PartsSalesCategoryId=t.PartsSalesCategoryId and a.PartId=d.sparepartid
        //                                    where branchid={0} ", userInfo.EnterpriseId);
        //                    var tableNames = sql.ToString().GetRegexNames();
        //                    var dbParameters = new List<DbParameter>();
        //                    if(ids != null && ids.Length > 0) {
        //                        sql.Append(" and t.id in (");
        //                        for(var i = 0; i < ids.Length; i++) {
        //                            if(ids.Length == i + 1) {
        //                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture));
        //                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
        //                            } else {
        //                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture) + ",");
        //                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
        //                            }
        //                        }
        //                        sql.Append(")");
        //                    } else {
        //                        if(!String.IsNullOrEmpty(code)) {
        //                            sql.Append(@" and t.Code like {0}Code ");
        //                            dbParameters.Add(db.CreateDbParameter("Code", "%" + code + "%"));
        //                        }

        //                        if(status.HasValue) {
        //                            sql.Append(@" and t.status ={0}status ");
        //                            dbParameters.Add(db.CreateDbParameter("status", status.Value));
        //                        }
        //                        if(creatorTimeBegin.HasValue) {
        //                            sql.Append(@" and t.createTime >=To_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
        //                            var tempValue = creatorTimeBegin.Value;
        //                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
        //                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
        //                        }
        //                        if(creatorTimeEnd.HasValue) {
        //                            sql.Append(@" and t.createTime <=To_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
        //                            var tempValue = creatorTimeEnd.Value;
        //                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
        //                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
        //                        }
        //                    }
        //                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
        //                    command.Parameters.AddRange(dbParameters.ToArray());
        //                    var reader = command.ExecuteReader();
        //                    using(var excelExport = new ExcelExport(fileName)) {
        //                        excelExport.ExportByRow(index => {
        //                            if(index == 0) {
        //                                return tableNames;
        //                            }
        //                            if(reader.Read()) {
        //                                var values = new object[reader.FieldCount];
        //                                var num = reader.GetValues(values);
        //                                if(num != reader.FieldCount) {
        //                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
        //                                }
        //                                values[25] = SetSign(values[25].ToString());
        //                                values[26] = SetSign(values[26].ToString());
        //                                values[27] = SetSign(values[27].ToString());
        //                                return values;
        //                            }

        //                            return null;
        //                        });
        //                    }
        //                    reader.Close();
        //                }
        //                return true;
        //            } catch(Exception) {
        //                return false;
        //            }
        //        }

        private string SetSign(string value) {
            switch(value.LastIndexOf('.')) {
                case -1:
                    return "0.00%";
                case 1:
                    return value + ".0%";
                default:
                    return value + "%";
            }
        }

        public bool ExportPartsSalesPriceChangeDetail(int[] ids, int? partsSalesCategoryId, string code, int? status,string sparePartCode,string sparePartName, DateTime? creatorTimeBegin, DateTime? creatorTimeEnd, out string fileName) {
            fileName = GetExportFilePath("配件销售价格变更申请主清单_" + ".xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);
                var userInfo = Utils.GetCurrentUserInfo();
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.AppendFormat(@"select t.code,/*销售价变更申请单编号*/
                                        t.PartsSalesCategoryCode,/*品牌编号*/
                                        t.PartsSalesCategoryName,/*品牌*/
                                        (select value from keyvalueitem where name = 'PartsSalesPriceChange_Status' and key = t.status) status,/*状态*/
                                        t.CreatorName,/*创建人*/
                                        t.CreateTime,/*创建时间*/
                                        t.ModifierName,/*修改人*/
                                        t.ModifyTime,/*修改时间*/
                                        t.InitialApproverName,/*初审人*/
                                        t.InitialApproveTime,/*初审时间*/
                                        t.ApproverName,/*审核人*/
                                        t.ApproveTime,/*审核时间*/
                                        t.FinalApproverName,/*审批人*/
                                        t.FinalApproveTime,/*审批时间*/
                                        t.AbandonerName,/*作废人*/
                                        t.AbandonTime,/*作废时间*/
                                        t.AbandonComment,/*作废原因*/
                                        t.Rejecter,/*驳回人*/
                                        t.RejectTime,/*驳回时间*/
                                        t.Remark, /*备注*/
                                        d.SparePartCode,/*配件编号*/
                                        d.SparePartName,/*配件名称*/
                                        d.PriceTypeName,/*价格类型*/
                                        d.MaxPurchasePricing,/*采购价*/
                                        d.CenterPrice,/*中心库价*/
                                        round(d.CenterPriceRatio*100,2),/*加价率*/
                                        d.SalesPrice,/*服务站价*/
                                        round(d.SalesPriceFluctuationRatio*100,2),/*加价率*/
                                        d.RetailGuidePrice,/*建议售价*/
                                        round(d.RetailPriceFluctuationRatio*100,2)/*加价率*/
                                   from PartsSalesPriceChange t
                                    left join PartsSalesPriceChangeDetail d on t.id=d.ParentId
                                    where branchid={0} ", userInfo.EnterpriseId);
                    var tableNames = sql.ToString().GetRegexNames();
                    var dbParameters = new List<DbParameter>();
                    if(ids != null && ids.Length > 0) {
                        sql.Append(" and t.id in (");
                        for(var i = 0; i < ids.Length; i++) {
                            if(ids.Length == i + 1) {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            } else {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                        }
                        sql.Append(")");
                    } else {
                        if(!String.IsNullOrEmpty(code)) {
                            sql.Append(@" and t.Code like {0}Code ");
                            dbParameters.Add(db.CreateDbParameter("Code", "%" + code + "%"));
                        }
                        if(!String.IsNullOrEmpty(sparePartCode)) {
                            sql.Append(@" and d.sparePartCode like {0}sparePartCode ");
                            dbParameters.Add(db.CreateDbParameter("sparePartCode", "%" + sparePartCode + "%"));
                        }
                        if(!String.IsNullOrEmpty(sparePartName)) {
                            sql.Append(@" and d.sparePartName like {0}sparePartName ");
                            dbParameters.Add(db.CreateDbParameter("sparePartName", "%" + sparePartName + "%"));
                        }
                        if(status.HasValue) {
                            sql.Append(@" and t.status ={0}status ");
                            dbParameters.Add(db.CreateDbParameter("status", status.Value));
                        }
                        if(creatorTimeBegin.HasValue) {
                            sql.Append(@" and t.createTime >=To_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = creatorTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(creatorTimeEnd.HasValue) {
                            sql.Append(@" and t.createTime <=To_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = creatorTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                    }
                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return tableNames;
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                values[23] = SetSign(values[23].ToString());
                                values[25] = SetSign(values[25].ToString());
                                values[27] = SetSign(values[27].ToString());
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }
    }
}