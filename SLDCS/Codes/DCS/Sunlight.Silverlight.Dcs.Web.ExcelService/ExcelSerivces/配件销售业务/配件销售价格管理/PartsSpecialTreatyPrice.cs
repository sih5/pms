﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        /// <summary>
        /// 导入配件特殊协议价信息
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="excelImportNum"></param>
        /// <param name="rightData"></param>
        /// <param name="errorData"></param>
        /// <param name="errorDataFileName"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public bool ImportPartsSpecialTreatyPrice(string fileName, out int excelImportNum, out List<PartsSpecialTreatyPriceExtend> rightData, out List<PartsSpecialTreatyPriceExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<PartsSpecialTreatyPriceExtend>();
            var allList = new List<PartsSpecialTreatyPriceExtend>();
            var rightList = new List<PartsSpecialTreatyPriceExtend>();
            var rightInsertList = new List<PartsSpecialTreatyPriceExtend>();
            var rightUpdateList = new List<PartsSpecialTreatyPriceExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("PartsSpecialTreatyPrice", out notNullableFields, out fieldLenght);


                List<string> notNullableFieldsCompany;
                Dictionary<string, int> fieldLenghtCompany;
                db.GetTableSchema("Company", out notNullableFieldsCompany, out fieldLenghtCompany);

                List<object> excelColumns;

                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    #region 指定文件中的列对应的名称
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode, "SparePartCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName, "SparePartName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsSpecialTreatyPrice_TreatyPrice, "TreatyPrice");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Remark, "Remark");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Partssalescategory_Name, "PartsSalesCategoryName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Company_Code, "CompanyCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Company_Name, "CompanyName");
                    #endregion

                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;

                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var tempImportObj = new PartsSpecialTreatyPriceExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        #region 根据导入的字段给对应的实体属性赋值 tempImportObj.PartsSalesCategoryNameStr = row["PartsSalesCategoryName"].Trim();
                        tempImportObj.SparePartCodeStr = newRow["SparePartCode"];
                        tempImportObj.SparePartNameStr = newRow["SparePartName"];
                        tempImportObj.TreatyPriceStr = newRow["TreatyPrice"];
                        tempImportObj.RemarkStr = newRow["Remark"];
                        tempImportObj.PartsSalesCategoryNameStr = newRow["PartsSalesCategoryName"];
                        tempImportObj.CompanyCodeStr = newRow["CompanyCode"];
                        tempImportObj.CompanyNameStr = newRow["CompanyName"];
                        #endregion

                        var tempErrorMessage = new List<string>();

                        #region 导入的数据基本检查

                        //配件编号检查
                        var fieldIndex = notNullableFields.IndexOf("SparePartCode".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.SparePartCodeStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchaseOrderDetail_Validation1);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.SparePartCodeStr) > fieldLenght["SparePartCode".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchaseOrderDetail_Validation2);
                        }

                        //配件名称检查
                        fieldIndex = notNullableFields.IndexOf("SparePartName".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.SparePartNameStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Sparepart_Name);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.SparePartNameStr) > fieldLenght["SparePartName".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Sparepart_NameIsLong);
                        }

                        //协议价格检查
                        fieldIndex = notNullableFields.IndexOf("TreatyPrice".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.TreatyPriceStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsSpecialTreatyPrice_TreatyPriceIsNull);
                        } else {
                            decimal checkValue;
                            if(decimal.TryParse(tempImportObj.TreatyPriceStr, out checkValue)) {
                                if(checkValue <= 0) {
                                    tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsSpecialTreatyPrice_TreatyPriceIsZero);
                                }
                            } else {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsSpecialTreatyPrice_TreatyPriceIsInteger);
                            }
                        }

                        //备注检查
                        fieldIndex = notNullableFields.IndexOf("Remark".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.RemarkStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchasePricingDetail_Validation15);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.RemarkStr) > fieldLenght["Remark".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchasePricingDetail_Validation16);
                        }

                        //品牌检查
                        fieldIndex = notNullableFields.IndexOf("PartsSalesCategoryName".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.PartsSalesCategoryNameStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_MarketingDepartment_PartsSalesCategoryIsNull);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.PartsSalesCategoryNameStr) > fieldLenght["PartsSalesCategoryName".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_MarketingDepartment_PartsSalesCategoryIsLong);
                        }

                        //企业编号检查
                        fieldIndex = notNullableFieldsCompany.IndexOf("Code".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.CompanyCodeStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Company_CodeIsNull);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.CompanyCodeStr) > fieldLenghtCompany["Code".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Company_CodeIsLong);
                        }

                        //企业名称检查
                        fieldIndex = notNullableFieldsCompany.IndexOf("Name".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.CompanyNameStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsSpecialTreatyPrice_NameIsNull);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.CompanyNameStr) > fieldLenghtCompany["Name".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsSpecialTreatyPrice_NameIsLong);
                        }
                        #endregion

                        if(tempErrorMessage.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });
                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    #region 剩下的数据进行业务检查

                    //1.查询企业（企业.企业编号=导入数据.客户企业编号）如果校验不通过，记录校验错误信息：企业信息不存在。
                    var companyCodeNeedCheck = tempRightList.Select(r => r.CompanyCodeStr).Distinct().ToArray();
                    var dbCompany = new List<CompanyExtend>();
                    Func<string[], bool> getDbCompany = value => {
                        var dbCompanyInfor = new CompanyExtend {
                            Id = Convert.ToInt32(value[0]),
                            Code = value[1],
                            Name = value[2]
                        };
                        dbCompany.Add(dbCompanyInfor);
                        return false;
                    };
                    db.QueryDataWithInOperator(@"select a.Id,a.code, a.name from company a where a.status=1", "a.Code", true, companyCodeNeedCheck, getDbCompany);
                    foreach(var tempRight in tempRightList) {
                        var rightCompany = dbCompany.FirstOrDefault(v => v.Code == tempRight.CompanyCodeStr);
                        if(rightCompany == null) {
                            tempRight.ErrorMsg = ErrorStrings.Export_Validation_PartsSpecialTreatyPrice_Validation1;

                        } else {
                            tempRight.CompanyId = rightCompany.Id;
                            tempRight.CompanyCode = rightCompany.Code;
                            tempRight.CompanyName = rightCompany.Name;
                        }
                    }
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();
                    //2.查询配件销售类型(配件销售类型.配件销售类型名称=导入数据.品牌)如果校验不通过，记录校验错误信息：品牌不存在。 如果配件销售类型.营销分公司Id<>登陆人员企业Id 记录校验错误信息：品牌所属营销分公司不正确。 
                    var partsSalesCategoryNeedCheck = tempRightList.Select(r => r.PartsSalesCategoryNameStr).Distinct().ToArray();
                    var dbPartsSalesCategories = new List<PartsSalesCategoryExtend>();
                    Func<string[], bool> getDbPartsSalesCategories = value => {
                        var dbObj = new PartsSalesCategoryExtend {
                            Id = Convert.ToInt32(value[0]),
                            Code = value[1],
                            Name = value[2],
                            BranchId = Convert.ToInt32(value[3])
                        };
                        dbPartsSalesCategories.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select id,code,name,branchid from PartsSalesCategory where status=1", "Name", true, partsSalesCategoryNeedCheck, getDbPartsSalesCategories);
                    var enterpriseId = Utils.GetCurrentUserInfo().EnterpriseId;
                    foreach(var tempRight in tempRightList) {
                        var rightPartsSalesCategory = dbPartsSalesCategories.FirstOrDefault(v => v.Name == tempRight.PartsSalesCategoryNameStr);
                        if(rightPartsSalesCategory == null) {
                            tempRight.ErrorMsg = ErrorStrings.Export_Title_Partssalescategory_Name + tempRight.PartsSalesCategoryNameStr + ErrorStrings.Export_Validation_Sparepart_NotExist;
                        } else {
                            tempRight.PartsSalesCategoryId = rightPartsSalesCategory.Id;
                            tempRight.PartsSalesCategoryCode = rightPartsSalesCategory.Code;
                            tempRight.PartsSalesCategoryName = rightPartsSalesCategory.Name;
                        }

                        var oldPartsSalesCategory = dbPartsSalesCategories.FirstOrDefault(v => v.Name == tempRight.PartsSalesCategoryNameStr && v.BranchId == enterpriseId);
                        if(oldPartsSalesCategory == null) {
                            tempRight.ErrorMsg = ErrorStrings.Export_Validation_PartsSpecialTreatyPrice_Validation2;
                        } else {
                            tempRight.BranchId = oldPartsSalesCategory.BranchId;
                        }
                    }
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();

                    //3.配件编号、配件名称组合存在配件信息中，否则提示：配件不存在
                    var sparePartCodesNeedCheck = tempRightList.Select(r => r.SparePartCodeStr.ToUpper()).Distinct().ToArray();
                    var dbSpareParts = new List<SparePartExtend>();
                    Func<string[], bool> getDbSpareParts = value => {
                        var dbObj = new SparePartExtend {
                            Id = Convert.ToInt32(value[0]),
                            Code = value[1],
                            Name = value[2]
                        };
                        dbSpareParts.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select Id,Code,Name from SparePart where status=1 ", "Code", true, sparePartCodesNeedCheck, getDbSpareParts);
                    foreach(var tempRight in tempRightList) {
                        var oldSparePart = dbSpareParts.FirstOrDefault(v => v.Code == tempRight.SparePartCodeStr);
                        if(oldSparePart == null) {
                            tempRight.ErrorMsg = String.Format(ErrorStrings.Export_Validation_IvecoPriceChangeAppDetail_Validation1, tempRight.SparePartCodeStr);
                        } else {
                            tempRight.SparePartId = oldSparePart.Id;
                            tempRight.SparePartCode = oldSparePart.Code;
                            tempRight.SparePartName = oldSparePart.Name;
                        }
                    }
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();
                    //4.查询特殊协议价（特殊协议价.配件销售类型Id=步骤2返回的配件销售类型.Id,企业Id=步骤1返回的企业.Id，营销分公司id=当前登陆企业id） 如果存在一条结果，更新配件特殊协议价.协议价格，更新人=当前登录人员，更新时间=当前时间 新增特殊协议价变更履历，字段根据特殊协议价对应字段赋值
                    var sparePartIdsNeedCheck = tempRightList.Select(r => r.SparePartId.ToString(CultureInfo.InvariantCulture)).Distinct().ToArray();
                    var dbTreatyPrices = new List<PartsSpecialTreatyPriceExtend>();
                    Func<string[], bool> getDbTreatyPrices = value => {
                        var dbObj = new PartsSpecialTreatyPriceExtend {
                            Id = Convert.ToInt32(value[0]),
                            CompanyId = Convert.ToInt32(value[1]),
                            BranchId = Convert.ToInt32(value[2]),
                            PartsSalesCategoryId = Convert.ToInt32(value[3]),
                            SparePartId = Convert.ToInt32(value[4])
                        };
                        dbTreatyPrices.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select Id,CompanyId,BranchId,PartsSalesCategoryId,SparePartId from PartsSpecialTreatyPrice where status=1 ", "SparePartId", true, sparePartIdsNeedCheck, getDbTreatyPrices);

                    var groups = tempRightList.GroupBy(r => new {
                        r.SparePartCodeStr,
                        r.PartsSalesCategoryNameStr,
                        r.CompanyCodeStr
                    }).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        groupItem.ErrorMsg = ErrorStrings.Export_Validation_PartsSpecialTreatyPrice_Validation3;
                    }
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();

                    foreach(var tempRight in tempRightList) {
                        tempRight.TreatyPrice = Convert.ToDecimal(tempRight.TreatyPriceStr);
                        tempRight.Status = 1;
                        tempRight.Remark = tempRight.RemarkStr;
                        var oldTreatyPrice = dbTreatyPrices.FirstOrDefault(v => v.CompanyId == tempRight.CompanyId && v.BranchId == tempRight.BranchId && v.PartsSalesCategoryId == tempRight.PartsSalesCategoryId && v.SparePartId == tempRight.SparePartId);
                        if(oldTreatyPrice == null) {
                            rightInsertList.Add(tempRight);
                        } else {
                            tempRight.Id = oldTreatyPrice.Id;
                            rightUpdateList.Add(tempRight);
                        }
                    }
                    #endregion



                    //获取所有不合格数据
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    //获取所有合格数据
                    rightList = allList.Except(errorList).ToList();

                    //#region 将合格数据的值填上
                    //foreach(var rightItem in rightList) {
                    //    rightItem.TreatyPrice = Convert.ToDecimal(rightItem.TreatyPriceStr);
                    //    rightItem.Remark = rightItem.RemarkStr;
                    //    rightItem.Status = (int)DcsBaseDataStatus.有效;
                    //}
                    //#endregion
                }

                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempObj = list[index - 1];
                            var values = new object[] {
                                #region 设置错误信息导出的列的值
                                tempObj.SparePartCodeStr,
                                tempObj.SparePartNameStr,
                                tempObj.TreatyPriceStr,
                                tempObj.RemarkStr,
                                tempObj.PartsSalesCategoryNameStr,
                                tempObj.CompanyCodeStr,
                                tempObj.CompanyNameStr,
                                tempObj.ErrorMsg
                                #endregion
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }
                //5.如果步骤3查询结果不存在 新增配件特殊协议价 赋值： 营销分公司Id=登录人员企业Id 配件销售类型Id=步骤2中配件销售类型.Id 销售类型名称=配件销售类型.名称 销售类型编号=配件销售类型.编号 配件Id=步骤3配件信息.Id 配件编号=步骤3配件信息.编号 配件名称=步骤3配件信息.配件名称 企业Id=步骤1返回的企业Id 协议价格=导入数据.协议价格 状态=新建 创建人 创建人id 创建时间 均赋值当前登陆人员信息 新增特殊协议价变更履历，字段根据特殊协议价对应字段赋值'
                //导入所有合格数据
                if(!rightList.Any())
                    return true;
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    //开启事务，新增更新配件在一个事务内
                    var ts = conn.BeginTransaction();
                    try {
                        //新增配件
                        if(rightList.Any()) {
                            #region 获取对应的插入的SQL 通过db.GetInsertSql方法
                            //获取新增数据的sql语句，Id为主键
                            var sqlInsert = db.GetInsertSql("PartsSpecialTreatyPrice", "Id", new[] {
                                    "BranchId", "PartsSalesCategoryId", "PartsSalesCategoryCode","PartsSalesCategoryName", "SparePartId","SparePartCode","SparePartName",
                                    "CompanyId","TreatyPrice","Status","Remark","CreatorId","CreatorName","CreateTime"
                                });

                            var sqlHistoryInsert = db.GetInsertSql("PartsSpecialPriceHistory", "Id", new[] {
                                    "PartsSpecialTreatyPriceId","BranchId","PartsSalesCategoryId", "PartsSalesCategoryCode", "PartsSalesCategoryName","SparePartId", "SparePartCode","SparePartName","CompanyId","TreatyPrice","Status","Remark","CreatorId","CreatorName","CreateTime"
                                });
                            var sqlUpdate = db.GetUpdateSql("PartsSpecialTreatyPrice", new[] {
                                    "TreatyPrice","ModifierId","ModifierName", "ModifyTime"
                                }, new[] { "Id" });
                            #endregion
                            var userinfo = Utils.GetCurrentUserInfo();
                            //往数据库增加配件信息
                            foreach(var item in rightInsertList) {
                                #region 添加Sql的参数
                                var command = db.CreateDbCommand(sqlInsert, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("BranchId", item.BranchId));
                                command.Parameters.Add(db.CreateDbParameter("PartsSalesCategoryId", item.PartsSalesCategoryId));
                                command.Parameters.Add(db.CreateDbParameter("PartsSalesCategoryCode", item.PartsSalesCategoryCode));
                                command.Parameters.Add(db.CreateDbParameter("PartsSalesCategoryName", item.PartsSalesCategoryName));
                                command.Parameters.Add(db.CreateDbParameter("SparePartId", item.SparePartId));
                                command.Parameters.Add(db.CreateDbParameter("SparePartCode", item.SparePartCode));
                                command.Parameters.Add(db.CreateDbParameter("SparePartName", item.SparePartName));
                                command.Parameters.Add(db.CreateDbParameter("CompanyId", item.CompanyId));
                                command.Parameters.Add(db.CreateDbParameter("TreatyPrice", item.TreatyPrice));
                                command.Parameters.Add(db.CreateDbParameter("Status", item.Status));
                                command.Parameters.Add(db.CreateDbParameter("Remark", item.Remark));
                                command.Parameters.Add(db.CreateDbParameter("CreatorId", userinfo.Id));
                                command.Parameters.Add(db.CreateDbParameter("CreatorName", userinfo.Name));
                                command.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                var tempId = db.ExecuteInsert(command, "Id");

                                var commandHistory = db.CreateDbCommand(sqlHistoryInsert, conn, ts);
                                commandHistory.Parameters.Add(db.CreateDbParameter("PartsSpecialTreatyPriceId", tempId));
                                commandHistory.Parameters.Add(db.CreateDbParameter("BranchId", item.BranchId));
                                commandHistory.Parameters.Add(db.CreateDbParameter("PartsSalesCategoryId", item.PartsSalesCategoryId));
                                commandHistory.Parameters.Add(db.CreateDbParameter("PartsSalesCategoryCode", item.PartsSalesCategoryCode));
                                commandHistory.Parameters.Add(db.CreateDbParameter("PartsSalesCategoryName", item.PartsSalesCategoryName));
                                commandHistory.Parameters.Add(db.CreateDbParameter("SparePartId", item.SparePartId));
                                commandHistory.Parameters.Add(db.CreateDbParameter("SparePartCode", item.SparePartCode));
                                commandHistory.Parameters.Add(db.CreateDbParameter("SparePartName", item.SparePartName));
                                commandHistory.Parameters.Add(db.CreateDbParameter("CompanyId", item.CompanyId));
                                commandHistory.Parameters.Add(db.CreateDbParameter("TreatyPrice", item.TreatyPrice));
                                commandHistory.Parameters.Add(db.CreateDbParameter("Status", item.Status));
                                commandHistory.Parameters.Add(db.CreateDbParameter("Remark", item.Remark));
                                commandHistory.Parameters.Add(db.CreateDbParameter("CreatorId", userinfo.Id));
                                commandHistory.Parameters.Add(db.CreateDbParameter("CreatorName", userinfo.Name));
                                commandHistory.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                commandHistory.ExecuteNonQuery();
                                #endregion
                            }

                            foreach(var item in rightUpdateList) {
                                #region 添加Sql的参数
                                var command = db.CreateDbCommand(sqlUpdate, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("TreatyPrice", item.TreatyPrice));
                                command.Parameters.Add(db.CreateDbParameter("ModifierId", userinfo.Id));
                                command.Parameters.Add(db.CreateDbParameter("ModifierName", userinfo.Name));
                                command.Parameters.Add(db.CreateDbParameter("ModifyTime", DateTime.Now));
                                command.Parameters.Add(db.CreateDbParameter("Id", item.Id));
                                command.ExecuteNonQuery();

                                var commandHistory = db.CreateDbCommand(sqlHistoryInsert, conn, ts);
                                commandHistory.Parameters.Add(db.CreateDbParameter("PartsSpecialTreatyPriceId", item.Id));
                                commandHistory.Parameters.Add(db.CreateDbParameter("BranchId", item.BranchId));
                                commandHistory.Parameters.Add(db.CreateDbParameter("PartsSalesCategoryId", item.PartsSalesCategoryId));
                                commandHistory.Parameters.Add(db.CreateDbParameter("PartsSalesCategoryCode", item.PartsSalesCategoryCode));
                                commandHistory.Parameters.Add(db.CreateDbParameter("PartsSalesCategoryName", item.PartsSalesCategoryName));
                                commandHistory.Parameters.Add(db.CreateDbParameter("SparePartId", item.SparePartId));
                                commandHistory.Parameters.Add(db.CreateDbParameter("SparePartCode", item.SparePartCode));
                                commandHistory.Parameters.Add(db.CreateDbParameter("SparePartName", item.SparePartName));
                                commandHistory.Parameters.Add(db.CreateDbParameter("CompanyId", item.CompanyId));
                                commandHistory.Parameters.Add(db.CreateDbParameter("TreatyPrice", item.TreatyPrice));
                                commandHistory.Parameters.Add(db.CreateDbParameter("Status", item.Status));
                                commandHistory.Parameters.Add(db.CreateDbParameter("Remark", item.Remark));
                                commandHistory.Parameters.Add(db.CreateDbParameter("CreatorId", userinfo.Id));
                                commandHistory.Parameters.Add(db.CreateDbParameter("CreatorName", userinfo.Name));
                                commandHistory.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                commandHistory.ExecuteNonQuery();
                                #endregion
                            }
                        }
                        //无异常提交
                        ts.Commit();
                    } catch(Exception) {
                        //报错回滚
                        ts.Rollback();
                        throw;
                    } finally {
                        if(conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }

        /// <summary>
        /// 导入配件特殊协议价申请清单信息
        /// </summary>
        /// <param name="partsSalesCategoryId"></param>
        /// <param name="fileName"></param>
        /// <param name="excelImportNum"></param>
        /// <param name="rightData"></param>
        /// <param name="errorData"></param>
        /// <param name="errorDataFileName"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public bool ImportSpecialPriceChangeList(int partsSalesCategoryId, string fileName, out int excelImportNum, out List<SpecialPriceChangeListExtend> rightData, out List<SpecialPriceChangeListExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<SpecialPriceChangeListExtend>();
            var allList = new List<SpecialPriceChangeListExtend>();
            var rightList = new List<SpecialPriceChangeListExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("SpecialPriceChangeList", out notNullableFields, out fieldLenght);

                List<object> excelColumns;

                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    #region 指定文件中的列对应的名称
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode, "SparePartCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName, "SparePartName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_SpecialPriceChangeList_SpecialTreatyPrice, "SpecialTreatyPrice");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Remark, "Remark");
                    #endregion

                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;

                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var tempImportObj = new SpecialPriceChangeListExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        #region 根据导入的字段给对应的实体属性赋值
                        tempImportObj.Remark = (row["Remark"] ?? "").Trim();
                        tempImportObj.SparePartCode = (newRow["SparePartCode"] ?? "").Trim();
                        tempImportObj.SparePartName = (newRow["SparePartName"] ?? "").Trim();
                        tempImportObj.SpecialTreatyPriceStr = (newRow["SpecialTreatyPrice"] ?? "").Trim();
                        #endregion

                        var tempErrorMessage = new List<string>();

                        #region 导入的数据基本检查

                        //配件编号检查
                        if(string.IsNullOrEmpty(tempImportObj.SparePartCode)) {
                            tempErrorMessage.Add(ErrorStrings.ImpPartsPurchaseOrderDetail_Validation1);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.SparePartCode) > fieldLenght["SparePartCode".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchaseOrderDetail_Validation2);
                        }

                        //配件名称检查
                        var fieldIndex = notNullableFields.IndexOf("SparePartName".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.SparePartName)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Sparepart_Name);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.SparePartName) > fieldLenght["SparePartName".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Sparepart_NameIsLong);
                        }

                        //销售价检查
                        if(string.IsNullOrEmpty(tempImportObj.SpecialTreatyPriceStr)) {
                            tempErrorMessage.Add(ErrorStrings.Export_validation_PartsSalesPriceChangeDetail_DealerSalesPriceNull);
                        } else {
                            decimal checkValue;
                            if(decimal.TryParse(tempImportObj.SpecialTreatyPriceStr, out checkValue)) {
                                if(checkValue <= 0) {
                                    tempErrorMessage.Add(ErrorStrings.Export_validation_PartsSalesPriceChangeDetail_DealerSalesPriceZero);
                                }
                            } else {
                                tempErrorMessage.Add(ErrorStrings.Export_validation_PartsSalesPriceChangeDetail_DealerSalesPriceInteger);
                            }
                            tempImportObj.SpecialTreatyPrice = checkValue;
                        }

                        //备注检查
                        fieldIndex = notNullableFields.IndexOf("Remark".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.Remark)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchasePricingDetail_Validation15);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.Remark) > fieldLenght["Remark".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchasePricingDetail_Validation16);
                        }
                        #endregion

                        if(tempErrorMessage.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });
                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    var groups = tempRightList.GroupBy(r => new {
                        r.SparePartCode
                    }).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        groupItem.ErrorMsg = ErrorStrings.Export_Validation_PlannedPriceAppDetail_SparepartReport;
                    }
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();


                    #region 剩下的数据进行业务检查
                    //1.配件编号、配件名称组合存在配件信息中，否则提示：配件不存在
                    var sparePartCodesNeedCheck = tempRightList.Select(r => r.SparePartCode.ToUpper()).Distinct().ToArray();
                    var dbSpareParts = new List<SparePartExtend>();
                    Func<string[], bool> getDbSpareParts = value => {
                        var dbObj = new SparePartExtend {
                            Id = Convert.ToInt32(value[0]),
                            Code = value[1],
                            Name = value[2]
                        };
                        dbSpareParts.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select Id,Trim(Code),Trim(Name) from SparePart where status=1 ", "Code", true, sparePartCodesNeedCheck, getDbSpareParts);
                    foreach(var tempRight in tempRightList) {
                        var oldSparePart = dbSpareParts.FirstOrDefault(v => v.Code == tempRight.SparePartCode);
                        if(oldSparePart == null) {
                            tempRight.ErrorMsg = String.Format(ErrorStrings.Export_Validation_IvecoPriceChangeAppDetail_Validation1, tempRight.SparePartCode);
                        } else {
                            tempRight.SparePartId = oldSparePart.Id;
                            tempRight.SparePartName = oldSparePart.Name;
                        }
                    }
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();
                    #endregion
                    #region 获取采购价
                    var partsPurchasePricingPartsId = tempRightList.Select(r => r.SparePartId.ToString()).Distinct().ToArray();
                    var dbPartsPurchasePricings = new List<PartsPurchasePricingExtend>();
                    Func<string[], bool> getPartsPurchasePricing = value => {
                        var dbObj = new PartsPurchasePricingExtend {
                            PartId = Convert.ToInt32(value[0]),
                            PurchasePrice = Convert.ToDecimal(value[1])
                        };
                        dbPartsPurchasePricings.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select partid,purchaseprice from partspurchasepricing where validfrom<=sysdate and validto>sysdate and status<>99 and partssalescategoryid=" + partsSalesCategoryId + " ", "PartId", false, partsPurchasePricingPartsId, getPartsPurchasePricing);
                    foreach(var tempPrice in tempRightList) {
                        var priceList = dbPartsPurchasePricings.Where(r => r.PartId == tempPrice.SparePartId);
                        if(priceList.Any())
                            tempPrice.PurchasePrice = priceList.Min(r => r.PurchasePrice);
                    }
                    #endregion

                    //获取所有不合格数据
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    //获取所有合格数据
                    rightList = allList.Except(errorList).ToList();
                }
                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempObj = list[index - 1];
                            var values = new object[] {
                                #region 设置错误信息导出的列的值
                                tempObj.SparePartCode,
                                tempObj.SparePartName,
                                tempObj.SpecialTreatyPriceStr,
                                tempObj.Remark,
                                tempObj.ErrorMsg
                                #endregion
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }

                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }

        public bool ExportSpecialTreatyPriceChange(int[] Ids, string code, int? brandId, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName) {
            fileName = GetExportFilePath("配件特殊协议价申请单_" + ".xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.Append(@"select t.code,/*申请单编号*/
                                        t.brandname,/*品牌*/
                                        t.corporationcode,/*企业编号*/
                                        t.corporationname,/*企业名称*/
                                        (select value
                                           from keyvalueitem
                                          where name = 'WorkflowOfSimpleApproval_Status'
                                            and key = t.status) status,/*状态*/
                                        t.remark,/*备注*/
                                        t.ValidationTime,/*生效时间*/
                                        t.ExpireTime,/*失效时间*/
                                        t.creatorname,/*创建人*/
                                        t.createtime,/*创建时间*/
                                        t.ModifierName,/*修改人*/
                                        t.modifytime,/*修改时间*/
                                        t.InitialApproverName,/*初审人*/
                                        t.InitialApproveTime,/*初审时间*/
                                        t.InitialApproverComment,/*初审意见*/
                                        t.checkername,/*终审人*/
                                        t.checkertime,/*终审时间*/
                                        t.CheckerComment,/*终审意见*/
                                        t.abandonername,/*作废人*/
                                        t.abandonertime/*作废时间*/
                                   from SpecialTreatyPriceChange t where 1=1 ");
                    var tableNames = sql.ToString().GetRegexNames();
                    var dbParameters = new List<DbParameter>();
                    if(Ids != null && Ids.Length > 0) {
                        sql.Append(" and t.id in (");
                        for(var i = 0; i < Ids.Length; i++) {
                            if(Ids.Length == i + 1) {
                                sql.Append("{0}id" + Ids[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter("id" + Ids[i].ToString(CultureInfo.InvariantCulture), Ids[i]));
                            } else {
                                sql.Append("{0}id" + Ids[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter("id" + Ids[i].ToString(CultureInfo.InvariantCulture), Ids[i]));
                            }
                        }
                        sql.Append(")");
                    } else {
                        if(!String.IsNullOrEmpty(code)) {
                            sql.Append(@" and t.Code like {0}Code ");
                            dbParameters.Add(db.CreateDbParameter("Code", "%" + code + "%"));
                        }
                        if(brandId.HasValue) {
                            sql.Append(@" and t.BrandId like {0}BrandId ");
                            dbParameters.Add(db.CreateDbParameter("BrandId", "%" + brandId + "%"));
                        }
                        if(status.HasValue) {
                            sql.Append(@" and t.status ={0}status ");
                            dbParameters.Add(db.CreateDbParameter("status", status.Value));
                        }
                        if(createTimeBegin.HasValue) {
                            sql.Append(@" and t.createTime >=To_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(@" and t.createTime <=To_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                    }
                    var sqlStr = sql.ToString();
                    if(sqlStr.IndexOf(" where 1=1  and ") > 0)
                        sqlStr = sqlStr.Replace(" where 1=1  and ", " where ");
                    else
                        sqlStr = sqlStr.Replace(" where 1=1 ", "");
                    var command = db.CreateDbCommand(string.Format(sqlStr, db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return tableNames;
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }

                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }

        }

        public bool MergeExportSpecialTreatyPriceChange(int[] Ids, string code, int? brandId, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName) {
            fileName = GetExportFilePath("配件特殊协议价申请单_" + ".xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.Append(@"select t.code,/*申请单编号*/
                                        t.brandname,/*品牌*/
                                        t.corporationcode,/*企业编号*/
                                        t.corporationname,/*企业名称*/
                                        (select value
                                           from keyvalueitem
                                          where name = 'SpecialTreatyPriceChange_Status'
                                            and key = t.status) status,/*状态*/
                                        t.remark,/*备注*/
                                        tt.sparepartcode,/*配件编号*/
                                        tt.sparepartname,/*配件名称*/
                                        t.ValidationTime,/*生效时间*/
                                        t.ExpireTime,/*失效时间*/
                                        tt.specialtreatyprice,/*协议价*/
                                        tt.remark,/*价格备注*/
                                        t.creatorname,/*创建人*/
                                        t.createtime,/*创建时间*/
                                        t.ModifierName,/*修改人*/
                                        t.modifytime,/*修改时间*/
                                        t.InitialApproverName,/*初审人*/ 
                                        t.InitialApproveTime,/*初审时间*/ 
                                        t.InitialApproverComment,/*初审意见*/
                                        t.ApproverName,/*审核人*/
                                        t.ApproveTime,/*审核时间*/
                                        t.ApproveComment,/*审核意见*/
                                        t.checkername,/*审批人*/
                                        t.checkertime,/*审批时间*/
                                        t.CheckerComment,/*审批意见*/
                                        t.abandonername,/*作废人*/
                                        t.abandonertime/*作废时间*/
                                   from SpecialPriceChangeList tt left join SpecialTreatyPriceChange t on tt.specialtreatypricechangeid=t.id where 1=1 ");
                    var tableNames = sql.ToString().GetRegexNames();
                    var dbParameters = new List<DbParameter>();
                    if(Ids != null && Ids.Length > 0) {
                        sql.Append(" and t.id in (");
                        for(var i = 0; i < Ids.Length; i++) {
                            if(Ids.Length == i + 1) {
                                sql.Append("{0}id" + Ids[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter("id" + Ids[i].ToString(CultureInfo.InvariantCulture), Ids[i]));
                            } else {
                                sql.Append("{0}id" + Ids[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter("id" + Ids[i].ToString(CultureInfo.InvariantCulture), Ids[i]));
                            }
                        }
                        sql.Append(")");
                    } else {
                        if(!String.IsNullOrEmpty(code)) {
                            sql.Append(@" and t.Code like {0}Code ");
                            dbParameters.Add(db.CreateDbParameter("Code", "%" + code + "%"));
                        }
                        if(brandId.HasValue) {
                            sql.Append(@" and t.BrandId like {0}BrandId ");
                            dbParameters.Add(db.CreateDbParameter("BrandId", "%" + brandId + "%"));
                        }
                        if(status.HasValue) {
                            sql.Append(@" and t.status ={0}status ");
                            dbParameters.Add(db.CreateDbParameter("status", status.Value));
                        }
                        if(createTimeBegin.HasValue) {
                            sql.Append(@" and t.createTime >=To_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(@" and t.createTime <=To_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                    }
                    var sqlStr = sql.ToString();
                    if(sqlStr.IndexOf(" where 1=1  and ") > 0)
                        sqlStr = sqlStr.Replace(" where 1=1  and ", " where ");
                    else
                        sqlStr = sqlStr.Replace(" where 1=1 ", "");
                    var command = db.CreateDbCommand(string.Format(sqlStr, db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return tableNames;
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }

                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }

        }

        public bool ImportSpecialTreatyPriceChange(string fileName, out int excelImportNum, out List<SpecialTreatyPriceChangeWithDetailExtend> rightData, out List<SpecialTreatyPriceChangeWithDetailExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<SpecialTreatyPriceChangeWithDetailExtend>();
            var allList = new List<SpecialTreatyPriceChangeWithDetailExtend>();
            var rightList = new List<SpecialTreatyPriceChangeWithDetailExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                List<object> excelColumns;

                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    #region 指定文件中的列对应的名称
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Company_Code, "CorporationCodeStr");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Company_Name, "CorporationNameStr");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Partssalescategory_Name, "BrandNameStr");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_FactoryPurchacePrice_ValidFrom, "ValidationTimeStr");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_FactoryPurchacePrice_ValidTo, "ExpireTimeStr");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode, "SparePartCodeStr");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName, "SparePartNameStr");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_SpecialPriceChangeList_SpecialTreatyPriceStr, "SpecialTreatyPriceStr");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Remark, "RemarkStr");
                    #endregion

                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;

                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var tempImportObj = new SpecialTreatyPriceChangeWithDetailExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        #region 根据导入的字段给对应的实体属性赋值
                        tempImportObj.CorporationCodeStr = (row["CorporationCodeStr"] ?? "").Trim();
                        tempImportObj.CorporationNameStr = (row["CorporationNameStr"] ?? "").Trim();
                        tempImportObj.BrandNameStr = (row["BrandNameStr"] ?? "").Trim();
                        tempImportObj.ValidationTimeStr = DateTime.Parse((row["ValidationTimeStr"] ?? "1900-01-01").Trim());
                        tempImportObj.ExpireTimeStr = DateTime.Parse((row["ExpireTimeStr"] ?? "1900-01-01").Trim());
                        tempImportObj.SparePartCodeStr = (row["SparePartCodeStr"] ?? "").Trim();
                        tempImportObj.SparePartNameStr = (row["SparePartNameStr"] ?? "").Trim();
                        tempImportObj.SpecialTreatyPriceStr = (row["SpecialTreatyPriceStr"] ?? "").Trim();
                        tempImportObj.RemarkStr = (row["RemarkStr"] ?? "").Trim();
                        #endregion

                        var tempErrorMessage = new List<string>();

                        #region 导入的数据基本检查

                        //配件编号检查
                        if(string.IsNullOrEmpty(tempImportObj.CorporationCodeStr)) {
                            tempErrorMessage.Add(ErrorStrings.Export_Validation_Company_CodeIsNull);
                        }
                        if(string.IsNullOrEmpty(tempImportObj.BrandNameStr)) {
                            tempErrorMessage.Add(ErrorStrings.Export_Validation_MarketingDepartment_PartsSalesCategoryIsNull);
                        }
                        if(string.IsNullOrEmpty(tempImportObj.SparePartCodeStr)) {
                            tempErrorMessage.Add(ErrorStrings.ImpPartsPurchaseOrderDetail_Validation1);
                        }
                        if(DateTime.Parse(tempImportObj.ValidationTimeStr.ToString()) == DateTime.Parse("1900-01-01")) {
                            tempErrorMessage.Add(ErrorStrings.Export_Validation_SpecialPriceChangeList_ValidationTimeStrIsNull);
                        } else {
                            DateTime checkValue;
                            if(!DateTime.TryParse(tempImportObj.ValidationTimeStr.ToString(), out checkValue)) {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_SpecialPriceChangeList_ValidationTimeStrValueError);
                            }
                        }
                        if(DateTime.Parse(tempImportObj.ExpireTimeStr.ToString()) == DateTime.Parse("1900-01-01")) {
                            tempErrorMessage.Add(ErrorStrings.Export_Validation_Credenceapplication_ExpireDate);
                        } else {
                            DateTime checkValue;
                            if(!DateTime.TryParse(tempImportObj.ExpireTimeStr.ToString(), out checkValue)) {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_SpecialPriceChangeList_ExpireTimeStrValueError);
                            }
                        }

                        //销售价检查
                        if(string.IsNullOrEmpty(tempImportObj.SpecialTreatyPriceStr)) {
                            tempErrorMessage.Add(ErrorStrings.Export_Validation_SpecialPriceChangeList_SpecialTreatyPriceStrIsNull);
                        } else {
                            decimal checkValue;
                            if(decimal.TryParse(tempImportObj.SpecialTreatyPriceStr, out checkValue)) {
                                if(checkValue <= 0) {
                                    tempErrorMessage.Add(ErrorStrings.Export_validation_PartsSalesPriceChangeDetail_DealerSalesPriceZero);
                                }
                            } else {
                                tempErrorMessage.Add(ErrorStrings.Export_validation_PartsSalesPriceChangeDetail_DealerSalesPriceInteger);
                            }
                            tempImportObj.SpecialTreatyPrice = checkValue;
                        }
                        #endregion

                        if(tempErrorMessage.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });
                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    #region 剩下的数据进行业务检查

                    //4、唯一性校验：客户企业编号、品牌、订单类型名称，否则提示：数据重复
                    //var groups = tempRightList.GroupBy(r => new {
                    //    r.CustomerCompanyCode,
                    //    r.PartsSalesCategoryName,
                    //    r.PartsSalesOrderTypeName
                    //}).Where(r => r.Count() > 1);
                    //foreach(var groupItem in groups.SelectMany(r => r)) {
                    //    groupItem.ErrorMsg = ErrorStrings.ImpPartsSupplierRelation_Validation31;
                    //}

                    var tempGroupR = tempRightList.GroupBy(r => new {
                        r.CorporationCodeStr,
                        r.BrandNameStr
                    });
                    foreach(var item in tempGroupR) {
                        foreach(SpecialTreatyPriceChangeWithDetailExtend _temp in item) {
                            if(item.Count(i => i.SparePartCodeStr == _temp.SparePartCodeStr) > 1)
                                _temp.ErrorMsg = ErrorStrings.Export_Validation_SpecialPriceChangeList_Validation1;
                        }
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();



                    //根据导入数据的客户企业编号查询企业表，如果不存在，导出错误信息：企业编码***不存在。
                    var customerCompanyCodesNeedCheck = tempRightList.Select(r => r.CorporationCodeStr).Distinct().ToArray();
                    var dbCompanys = new List<CompanyExtend>();
                    Func<string[], bool> getDbCompanys = value => {
                        var dbObj = new CompanyExtend {
                            Id = Convert.ToInt32(value[0]),
                            Code = value[1],
                            Name = value[2]
                        };
                        dbCompanys.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select Id,Code,Name from Company where status=1 ", "Code", false, customerCompanyCodesNeedCheck, getDbCompanys);
                    foreach(var tempRight in tempRightList) {
                        var companyItem = dbCompanys.FirstOrDefault(v => v.Code == tempRight.CorporationCodeStr);
                        if(companyItem == null) {
                            tempRight.ErrorMsg = ErrorStrings.Export_Validation_CustomerOrderPriceGrade_Validation1;
                            continue;
                        } else if(!string.IsNullOrEmpty(tempRight.CorporationNameStr) && companyItem.Name != tempRight.CorporationNameStr) {
                            tempRight.ErrorMsg = string.Format(ErrorStrings.Export_Validation_SpecialPriceChangeList_Validation2, tempRight.CorporationCodeStr, tempRight.CorporationNameStr);
                            continue;
                        }
                        tempRight.CorporationId = companyItem.Id;
                        tempRight.CorporationNameStr = companyItem.Name;
                        tempRight.CorporationCodeStr = companyItem.Code;
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    //根据导入数据的品牌查询品牌表 ，如果不存在，导出错误信息：品牌***不存在。
                    var partsSalesCategoryNamesNeedCheck = tempRightList.Select(r => r.BrandNameStr).Distinct().ToArray();
                    var dbPartsSalesCategorys = new List<PartsSalesCategory>();
                    Func<string[], bool> getdbPartsSalesCategorys = value => {
                        var dbObj = new PartsSalesCategory {
                            Id = Convert.ToInt32(value[0]),
                            Name = value[1]
                        };
                        dbPartsSalesCategorys.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select Id,Name from PartsSalesCategory where status=1 ", "Name", false, partsSalesCategoryNamesNeedCheck, getdbPartsSalesCategorys);
                    foreach(var tempRight in tempRightList) {
                        var repairItem = dbPartsSalesCategorys.FirstOrDefault(v => v.Name == tempRight.BrandNameStr);
                        if(repairItem == null) {
                            tempRight.ErrorMsg = ErrorStrings.ImpVehicleDLRStartSecurityDeposit_Validation8;
                            continue;
                        }
                        tempRight.BrandId = repairItem.Id;
                        tempRight.BrandNameStr = repairItem.Name;
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    //1.配件编号、配件名称组合存在配件信息中，否则提示：配件不存在
                    var sparePartCodesNeedCheck = tempRightList.Select(r => r.SparePartCodeStr.ToUpper()).Distinct().ToArray();
                    var dbSpareParts = new List<SparePartExtend>();
                    Func<string[], bool> getDbSpareParts = value => {
                        var dbObj = new SparePartExtend {
                            Id = Convert.ToInt32(value[0]),
                            Code = value[1],
                            Name = value[2]
                        };
                        dbSpareParts.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select Id,Trim(Code),Trim(Name) from SparePart where status=1 ", "Code", true, sparePartCodesNeedCheck, getDbSpareParts);
                    foreach(var tempRight in tempRightList) {
                        var oldSparePart = dbSpareParts.FirstOrDefault(v => v.Code == tempRight.SparePartCodeStr);
                        if(oldSparePart == null) {
                            tempRight.ErrorMsg = String.Format(ErrorStrings.Export_Validation_IvecoPriceChangeAppDetail_Validation1, tempRight.SparePartCodeStr);
                        } else if(!string.IsNullOrEmpty(tempRight.SparePartNameStr) && oldSparePart.Name != tempRight.SparePartNameStr) {
                            tempRight.ErrorMsg = String.Format(ErrorStrings.Export_Validation_SpecialPriceChangeList_Validation3, tempRight.SparePartCodeStr, tempRight.SparePartNameStr);
                        } else {
                            tempRight.SparePartId = oldSparePart.Id;
                            tempRight.SparePartNameStr = oldSparePart.Name;
                            tempRight.SparePartCodeStr = oldSparePart.Code;
                        }
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    //采购价为空时，不允许生成特殊协议价
                    var sparePartPartsPurchasePricingNeedCheck = tempRightList.Select(r => r.SparePartId.ToString()).ToArray();
                    var dbPartsPurchasePricings = new List<PartsPurchasePricing>();
                    Func<string[], bool> getDbPartsPurchasePricings = value => {
                        var dbObj = new PartsPurchasePricing {
                            PartId = Convert.ToInt32(value[0]),
                            PartsSalesCategoryId = Convert.ToInt32(value[1]),
                            PurchasePrice = Convert.ToDecimal(value[2])
                        };
                        dbPartsPurchasePricings.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select partid,partssalescategoryid,purchaseprice from partspurchasepricing where ValidFrom<=sysdate and ValidTo>sysdate and status<>99 ", "partid", true, sparePartPartsPurchasePricingNeedCheck, getDbPartsPurchasePricings);
                    foreach(var tempRight in tempRightList) {
                        var partsPurchasePricings = dbPartsPurchasePricings.Where(i => i.PartId == tempRight.SparePartId && i.PartsSalesCategoryId == tempRight.BrandId);
                        if(!partsPurchasePricings.Any())
                            tempRight.ErrorMsg = ErrorStrings.Export_Validation_SpecialPriceChangeList_Validation4;
                        else if(tempRight.SpecialTreatyPrice < partsPurchasePricings.Min(i => i.PurchasePrice))
                            tempRight.ErrorMsg = ErrorStrings.Export_Validation_SpecialPriceChangeList_Validation5;
                    }

                    #endregion

                    //获取所有不合格数据
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    //获取所有合格数据
                    rightList = allList.Except(errorList).ToList();
                }
                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempObj = list[index - 1];
                            var values = new object[] {
                                #region 设置错误信息导出的列的值
                                tempObj.CorporationCodeStr,
                                tempObj.CorporationNameStr,
                                tempObj.BrandNameStr,
                                tempObj.ValidationTimeStr,
                                tempObj.ExpireTimeStr,
                                tempObj.SparePartCodeStr,
                                tempObj.SparePartNameStr,
                                tempObj.SpecialTreatyPriceStr,
                                tempObj.RemarkStr,
                                tempObj.ErrorMsg
                                #endregion
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }

                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }

        public bool ImportPartsSpecialTreatyPriceAbandon(string fileName, out int excelImportNum, out List<PartsSpecialTreatyPriceExtend> rightData, out List<PartsSpecialTreatyPriceExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<PartsSpecialTreatyPriceExtend>();
            var allList = new List<PartsSpecialTreatyPriceExtend>();
            var rightList = new List<PartsSpecialTreatyPriceExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("PartsSpecialTreatyPrice", out notNullableFields, out fieldLenght);


                List<string> notNullableFieldsCompany;
                Dictionary<string, int> fieldLenghtCompany;
                db.GetTableSchema("Company", out notNullableFieldsCompany, out fieldLenghtCompany);

                List<object> excelColumns;

                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    #region 指定文件中的列对应的名称
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Company_Code, "CompanyCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Partssalescategory_Name, "PartsSalesCategoryName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode, "SparePartCode");
                    #endregion

                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;

                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var tempImportObj = new PartsSpecialTreatyPriceExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        #region 根据导入的字段给对应的实体属性赋值 tempImportObj.PartsSalesCategoryNameStr = row["PartsSalesCategoryName"].Trim();
                        tempImportObj.SparePartCodeStr = newRow["SparePartCode"];
                        tempImportObj.PartsSalesCategoryNameStr = newRow["PartsSalesCategoryName"];
                        tempImportObj.CompanyCodeStr = newRow["CompanyCode"];
                        #endregion

                        var tempErrorMessage = new List<string>();

                        #region 导入的数据基本检查

                        //配件编号检查
                        var fieldIndex = notNullableFields.IndexOf("SparePartCode".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.SparePartCodeStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchaseOrderDetail_Validation1);
                        }
                        //品牌检查
                        fieldIndex = notNullableFields.IndexOf("PartsSalesCategoryName".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.PartsSalesCategoryNameStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_MarketingDepartment_PartsSalesCategoryIsNull);
                        }
                        //企业编号检查
                        fieldIndex = notNullableFieldsCompany.IndexOf("Code".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.CompanyCodeStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Company_CodeIsNull);
                        }
                        #endregion
                        if(tempErrorMessage.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });
                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    var groups = tempRightList.GroupBy(r => new {
                        r.SparePartCodeStr,
                        r.PartsSalesCategoryNameStr,
                        r.CompanyCodeStr
                    }).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        groupItem.ErrorMsg = ErrorStrings.Export_Validation_PartsSpecialTreatyPrice_Validation3;
                    }
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();
                    #region 剩下的数据进行业务检查

                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();
                    //4.查询特殊协议价（特殊协议价.配件销售类型Id=步骤2返回的配件销售类型.Id,企业Id=步骤1返回的企业.Id，营销分公司id=当前登陆企业id） 如果存在一条结果，更新配件特殊协议价.协议价格，更新人=当前登录人员，更新时间=当前时间 新增特殊协议价变更履历，字段根据特殊协议价对应字段赋值
                    var sparePartCodesNeedCheck = tempRightList.Select(r => r.SparePartCodeStr.ToString(CultureInfo.InvariantCulture)).Distinct().ToArray();
                    var dbTreatyPrices = new List<PartsSpecialTreatyPriceExtend>();
                    Func<string[], bool> getDbTreatyPrices = value => {
                        var dbObj = new PartsSpecialTreatyPriceExtend {
                            Id = Convert.ToInt32(value[0]),
                            CompanyCode = value[1],
                            CompanyId = Convert.ToInt32(value[2]),
                            BranchId = Convert.ToInt32(value[3]),
                            TreatyPrice = Convert.ToDecimal(value[4]),
                            PartsSalesCategoryId = Convert.ToInt32(value[5]),
                            PartsSalesCategoryCode = value[6],
                            PartsSalesCategoryName = value[7],
                            SparePartId = Convert.ToInt32(value[8]),
                            SparePartCode = value[9],
                            SparePartName = value[10],
                            //ValidationTime = value[11] == "" ? null : (DateTime)(value[11]),
                            //ExpireTime = value[12] == "" ? null : (DateTime)(value[12]),
                            Status = Convert.ToInt32(value[13]),
                            Remark = value[14],
                        };
                        dbTreatyPrices.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator(@"select a.Id,
                           c.Code,
                            a.CompanyId,
                        a.BranchId,
                        a.TreatyPrice,
                        a.PartsSalesCategoryId,
                        a.PartsSalesCategoryCode,
                        a.PartsSalesCategoryName,
                        a.SparePartId,
                        a.SparePartCode,
                        a.SparePartName,
                        a.ValidationTime,
                        a.ExpireTime,
                        a.Status,a.Remark from PartsSpecialTreatyPrice a inner join Company c on a.CompanyId=c.id where a.status=1 ", "a.SparePartCode", true, sparePartCodesNeedCheck, getDbTreatyPrices);
                    foreach(var tempRight in tempRightList) {
                        var oldTreatyPrice = dbTreatyPrices.FirstOrDefault(v => v.CompanyCode == tempRight.CompanyCodeStr && v.PartsSalesCategoryName == tempRight.PartsSalesCategoryNameStr && v.SparePartCode == tempRight.SparePartCodeStr);
                        if(oldTreatyPrice == null) {
                            tempRight.ErrorMsg = ErrorStrings.Export_Validation_SpecialPriceChangeList_Validation6;
                        } else {
                            tempRight.Id = oldTreatyPrice.Id;
                            tempRight.CompanyId = oldTreatyPrice.CompanyId;
                            tempRight.BranchId = oldTreatyPrice.BranchId;
                            tempRight.TreatyPrice = oldTreatyPrice.TreatyPrice;
                            tempRight.PartsSalesCategoryId = oldTreatyPrice.PartsSalesCategoryId;
                            tempRight.PartsSalesCategoryCode = oldTreatyPrice.PartsSalesCategoryCode;
                            tempRight.PartsSalesCategoryName = oldTreatyPrice.PartsSalesCategoryName;
                            tempRight.SparePartId = oldTreatyPrice.SparePartId;
                            tempRight.SparePartCode = oldTreatyPrice.SparePartCode;
                            tempRight.SparePartName = oldTreatyPrice.SparePartCode;
                        }
                    }
                    #endregion
                    //获取所有不合格数据
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    //获取所有合格数据
                    rightList = allList.Except(errorList).ToList();
                }

                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempObj = list[index - 1];
                            var values = new object[] {
                                #region 设置错误信息导出的列的值
                                tempObj.CompanyCodeStr,
                                tempObj.PartsSalesCategoryNameStr,
                                tempObj.SparePartCodeStr,
                                tempObj.ErrorMsg
                                #endregion
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }
                if(!rightList.Any())
                    return true;
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    //开启事务，新增更新配件在一个事务内
                    var ts = conn.BeginTransaction();
                    try {
                        //新增配件
                        if(rightList.Any()) {
                            #region 获取对应的插入的SQL 通过db.GetInsertSql方法
                            //获取修改数据的sql语句，Id为主键
                            var sqlUpdate = db.GetUpdateSql("PartsSpecialTreatyPrice", new[] {
                                "Status", "ModifierId", "ModifierName", "ModifyTime", "AbandonerId", "AbandonerName", "AbandonTime"
                            }, new[] { "Id" });

                            var sqlHistoryInsert = db.GetInsertSql("PartsSpecialPriceHistory", "Id", new[] {
                                    "PartsSpecialTreatyPriceId","BranchId","PartsSalesCategoryId", "PartsSalesCategoryCode", "PartsSalesCategoryName","SparePartId", "SparePartCode","SparePartName","CompanyId","TreatyPrice","Status","Remark","CreatorId","CreatorName","CreateTime"
                                });
                            #endregion
                            var userinfo = Utils.GetCurrentUserInfo();
                            //往数据库增加配件信息

                            foreach(var item in rightList) {
                                #region 添加Sql的参数
                                var command = db.CreateDbCommand(sqlUpdate, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("Status", (int)DcsBaseDataStatus.作废));
                                command.Parameters.Add(db.CreateDbParameter("ModifierId", userinfo.Id));
                                command.Parameters.Add(db.CreateDbParameter("ModifierName", userinfo.Name));
                                command.Parameters.Add(db.CreateDbParameter("ModifyTime", DateTime.Now));
                                command.Parameters.Add(db.CreateDbParameter("AbandonerId", userinfo.Id));
                                command.Parameters.Add(db.CreateDbParameter("AbandonerName", userinfo.Name));
                                command.Parameters.Add(db.CreateDbParameter("AbandonTime", DateTime.Now));
                                command.Parameters.Add(db.CreateDbParameter("Id", item.Id));
                                command.ExecuteNonQuery();

                                var commandHistory = db.CreateDbCommand(sqlHistoryInsert, conn, ts);
                                commandHistory.Parameters.Add(db.CreateDbParameter("PartsSpecialTreatyPriceId", item.Id));
                                commandHistory.Parameters.Add(db.CreateDbParameter("BranchId", item.BranchId));
                                commandHistory.Parameters.Add(db.CreateDbParameter("PartsSalesCategoryId", item.PartsSalesCategoryId));
                                commandHistory.Parameters.Add(db.CreateDbParameter("PartsSalesCategoryCode", item.PartsSalesCategoryCode));
                                commandHistory.Parameters.Add(db.CreateDbParameter("PartsSalesCategoryName", item.PartsSalesCategoryName));
                                commandHistory.Parameters.Add(db.CreateDbParameter("SparePartId", item.SparePartId));
                                commandHistory.Parameters.Add(db.CreateDbParameter("SparePartCode", item.SparePartCode));
                                commandHistory.Parameters.Add(db.CreateDbParameter("SparePartName", item.SparePartName));
                                commandHistory.Parameters.Add(db.CreateDbParameter("CompanyId", item.CompanyId));
                                commandHistory.Parameters.Add(db.CreateDbParameter("TreatyPrice", item.TreatyPrice));
                                commandHistory.Parameters.Add(db.CreateDbParameter("Status", (int)DcsBaseDataStatus.作废));
                                commandHistory.Parameters.Add(db.CreateDbParameter("Remark", item.Remark));
                                commandHistory.Parameters.Add(db.CreateDbParameter("CreatorId", userinfo.Id));
                                commandHistory.Parameters.Add(db.CreateDbParameter("CreatorName", userinfo.Name));
                                commandHistory.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                commandHistory.ExecuteNonQuery();
                                #endregion
                            }
                        }
                        //无异常提交
                        ts.Commit();
                    } catch(Exception) {
                        //报错回滚
                        ts.Rollback();
                        throw;
                    } finally {
                        if(conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }
    }
}
