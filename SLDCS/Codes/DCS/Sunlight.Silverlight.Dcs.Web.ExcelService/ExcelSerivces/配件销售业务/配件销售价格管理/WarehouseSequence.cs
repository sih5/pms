﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        public bool ExportWarehouseSequence(int[] ids, string customerCompanyCode, string customerCompanyName, int? partsSalesCategoryId, int? status, bool? isAutoApprove, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName) {
            fileName = GetExportFilePath("出库仓库优先顺序_" + ".xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.Append(@"select    CustomerCompanyCode,
                                           CustomerCompanyName,
                                           PartsSalesCategoryName,
                                           c.value as IsAutoApprove,
                                           DefaultWarehouseName,
                                           DefaultOutWarehouseName,
                                           FirstWarehouseName,
                                           FirstOutWarehouseName,
                                           SecoundWarehouseName,
                                           SecoundOutWarehouseName,
                                           ThirdWarehouseName,
                                           ThirdOutWarehouseName,
                                           b.value as Status,
                                           CreatorName,
                                           CreateTime,
                                           ModifierName,
                                           ModifyTime,
                                           AbandonerName,
                                           AbandonTime
                                     from WarehouseSequence a 
                                     left join keyvalueitem b on a.status=b.key and b.name='BaseData_Status'
                                     left join keyvalueitem c on a.IsAutoApprove=c.key and c.name='IsOrNot' where 1=1 ");
                    var dbParameters = new List<DbParameter>();
                    if(ids != null && ids.Length > 0) {
                        sql.Append(" and a.id in (");
                        for(var i = 0; i < ids.Length; i++) {
                            if(ids.Length == i + 1) {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            } else {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                        }
                        sql.Append(")");
                    } else {
                        if(partsSalesCategoryId.HasValue) {
                            sql.Append(@" and a.partsSalesCategoryId ={0}partsSalesCategoryId ");
                            dbParameters.Add(db.CreateDbParameter("partsSalesCategoryId", partsSalesCategoryId.Value));
                        }
                        if(status.HasValue) {
                            sql.Append(@" and a.status ={0}Status");
                            dbParameters.Add(db.CreateDbParameter("status", status.Value));
                        }
                        if(!string.IsNullOrEmpty(customerCompanyCode)) {
                            sql.Append(@" and a.customerCompanyCode ={0}customerCompanyCode ");
                            dbParameters.Add(db.CreateDbParameter("customerCompanyCode", customerCompanyCode));
                        }
                        if(!string.IsNullOrEmpty(customerCompanyName)) {
                            sql.Append(@" and a.customerCompanyName ={0}customerCompanyName ");
                            dbParameters.Add(db.CreateDbParameter("customerCompanyName", customerCompanyName));
                        }
                        if(createTimeBegin.HasValue) {
                            sql.Append(@" and a.createTime >=To_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(@" and a.createTime <=To_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                    }
                    var sqlStr = sql.ToString();
                    if(sqlStr.IndexOf(" where 1=1  and ") > 0)
                        sqlStr = sqlStr.Replace(" where 1=1  and ", " where ");
                    else
                        sqlStr = sqlStr.Replace(" where 1=1 ", "");
                    var command = db.CreateDbCommand(string.Format(sqlStr.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                       ErrorStrings.Export_Title_Credenceapplication_CustomerCode,ErrorStrings.Export_Title_Credenceapplication_CustomerName,ErrorStrings.Export_Title_Partssalescategory_Name,"是否参与自动审核","首选仓库","首选仓库出库仓库","第一仓库","第一仓库出库仓库","第二仓库","第二仓库出库仓库","第三仓库","第三仓库出库仓库",ErrorStrings.Export_Title_AccountPeriod_Status,ErrorStrings.Export_Title_AccountPeriod_CreatorName,ErrorStrings.Export_Title_AccountPeriod_CreateTime,ErrorStrings.Export_Title_AccountPeriod_ModifierName,ErrorStrings.Export_Title_AccountPeriod_ModifyTime,ErrorStrings.Export_Title_PartsBranch_AbandonerName,ErrorStrings.Export_Title_PartsBranch_AbandonerTime
                                    };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }

                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }

        }

        public bool ImportWarehouseSequence(string fileName, out int excelImportNum, out List<WarehouseSequenceExtend> rightData, out List<WarehouseSequenceExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<WarehouseSequenceExtend>();
            var allList = new List<WarehouseSequenceExtend>();
            var rightList = new List<WarehouseSequenceExtend>();
            var rightInsertList = new List<WarehouseSequenceExtend>();
            var rightUpdateList = new List<WarehouseSequenceExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("WarehouseSequence", out notNullableFields, out fieldLenght);

                List<object> excelColumns;

                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    #region 指定文件中的列对应的名称
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Credenceapplication_CustomerCode, "CustomerCompanyCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Credenceapplication_CustomerName, "CustomerCompanyName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Partssalescategory_Name, "PartsSalesCategoryName");
                    excelOperator.AddColumnDataSource("是否参与自动审核", "IsAutoApprove");
                    excelOperator.AddColumnDataSource("首选仓库", "DefaultWarehouseName");
                    excelOperator.AddColumnDataSource("首选仓库出库仓库", "DefaultOutWarehouseName");
                    excelOperator.AddColumnDataSource("第一仓库", "FirstWarehouseName");
                    excelOperator.AddColumnDataSource("第一仓库出库仓库", "FirstOutWarehouseName");
                    excelOperator.AddColumnDataSource("第二仓库", "SecoundWarehouseName");
                    excelOperator.AddColumnDataSource("第二仓库出库仓库", "SecoundOutWarehouseName");
                    excelOperator.AddColumnDataSource("第三仓库", "ThirdWarehouseName");
                    excelOperator.AddColumnDataSource("第三仓库出库仓库", "ThirdOutWarehouseName");
                    #endregion

                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var tempImportObj = new WarehouseSequenceExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        #region 根据导入的字段给对应的实体属性赋值
                        tempImportObj.CustomerCompanyCodeStr = row["CustomerCompanyCode"].Trim();
                        tempImportObj.CustomerCompanyNameStr = newRow["CustomerCompanyName"];
                        tempImportObj.PartsSalesCategoryNameStr = newRow["PartsSalesCategoryName"];
                        tempImportObj.IsAutoApproveStr = newRow["IsAutoApprove"];
                        tempImportObj.DefaultWarehouseNameStr = newRow["DefaultWarehouseName"];
                        tempImportObj.DefaultOutWarehouseNameStr = newRow["DefaultOutWarehouseName"];
                        tempImportObj.FirstWarehouseNameStr = newRow["FirstWarehouseName"];
                        tempImportObj.FirstOutWarehouseNameStr = newRow["FirstOutWarehouseName"];
                        tempImportObj.SecoundWarehouseNameStr = newRow["SecoundWarehouseName"];
                        tempImportObj.SecoundOutWarehouseNameStr = newRow["SecoundOutWarehouseName"];
                        tempImportObj.ThirdWarehouseNameStr = newRow["ThirdWarehouseName"];
                        tempImportObj.ThirdOutWarehouseNameStr = newRow["ThirdOutWarehouseName"];
                        #endregion

                        var tempErrorMessage = new List<string>();

                        #region 导入的数据基本检查

                        //客户编号检查
                        var fieldIndex = notNullableFields.IndexOf("CustomerCompanyCode".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.CustomerCompanyCodeStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Credenceapplication_CustomerCodeIsNull);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.CustomerCompanyCodeStr) > fieldLenght["CustomerCompanyCode".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Credenceapplication_CustomerCodeIsLong);
                        }
                        //客户名称检查
                        fieldIndex = notNullableFields.IndexOf("CustomerCompanyName".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.CustomerCompanyNameStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Credenceapplication_CustomerNameIsNull);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.CustomerCompanyNameStr) > fieldLenght["CustomerCompanyName".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Credenceapplication_CustomerNameIsLong);
                        }

                        //品牌检查
                        fieldIndex = notNullableFields.IndexOf("PartsSalesCategoryName".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.PartsSalesCategoryNameStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_MarketingDepartment_PartsSalesCategoryIsNull);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.PartsSalesCategoryNameStr) > fieldLenght["PartsSalesCategoryName".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_MarketingDepartment_PartsSalesCategoryIsLong);
                        }

                        //是否参与自动审核
                        fieldIndex = notNullableFields.IndexOf("IsAutoApprove".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.IsAutoApproveStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add("是否参与自动审核不能为空");
                        } else if(tempImportObj.IsAutoApproveStr.Trim() == ErrorStrings.Export_Title_PartsBranch_Yes)
                            tempImportObj.IsAutoApprove = true;

                        //首选仓库
                        fieldIndex = notNullableFields.IndexOf("DefaultWarehouseName".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.DefaultWarehouseNameStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add("首选仓库不能为空");
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.DefaultWarehouseNameStr) > fieldLenght["DefaultWarehouseName".ToUpper()])
                                tempErrorMessage.Add("首选仓库过长");
                        }

                        //首选出库仓库
                        fieldIndex = notNullableFields.IndexOf("DefaultOutWarehouseName".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.DefaultOutWarehouseNameStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add("首选出库仓库不能为空");
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.DefaultOutWarehouseNameStr) > fieldLenght["DefaultOutWarehouseName".ToUpper()])
                                tempErrorMessage.Add("首选出库仓库过长");
                        }

                        //是否参与自动审核检查
                        if(string.IsNullOrEmpty(tempImportObj.IsAutoApproveStr)) {
                            tempErrorMessage.Add("是否参与自动审核不能为空");
                        }
                        #endregion

                        if(tempErrorMessage.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });
                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    #region 剩下的数据进行业务检查
                    //1.查询企业（企业.企业编号=导入数据.客户企业编号）如果校验不通过，记录校验错误信息：企业信息不存在。
                    var companyCodeNeedCheck = tempRightList.Select(r => r.CustomerCompanyCodeStr).Distinct().ToArray();
                    var dbCompany = new List<CompanyExtend>();
                    Func<string[], bool> getDbCompany = value => {
                        var dbCompanyInfor = new CompanyExtend {
                            Id = Convert.ToInt32(value[0]),
                            Code = value[1],
                            Name = value[2]
                        };
                        dbCompany.Add(dbCompanyInfor);
                        return false;
                    };
                    db.QueryDataWithInOperator(@"select a.Id,a.code, a.name from company a where a.status=1", "a.Code", true, companyCodeNeedCheck, getDbCompany);
                    foreach(var tempRight in tempRightList) {
                        var rightCompany = dbCompany.FirstOrDefault(v => v.Code == tempRight.CustomerCompanyCodeStr);
                        if(rightCompany == null) {
                            tempRight.ErrorMsg = ErrorStrings.Export_Validation_PartsSpecialTreatyPrice_Validation1;
                        } else {
                            tempRight.CustomerCompanyId = rightCompany.Id;
                            tempRight.CustomerCompanyCode = rightCompany.Code;
                            tempRight.CustomerCompanyName = rightCompany.Name;
                        }
                    }

                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();
                    foreach(var tempRight in tempRightList) {
                        if(tempRight.PartsSalesCategoryName == "集团配件") {
                            tempRight.ErrorMsg = "统购品牌无销售业务，无需维护发货仓库信息";
                        }
                    }
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();

                    //2.查询配件销售类型(配件销售类型.配件销售类型名称=导入数据.品牌)如果校验不通过，记录校验错误信息：品牌不存在。 如果配件销售类型.营销分公司Id<>登陆人员企业Id 记录校验错误信息：品牌所属营销分公司不正确。 
                    var partsSalesCategoryNeedCheck = tempRightList.Select(r => r.PartsSalesCategoryNameStr).Distinct().ToArray();
                    var dbPartsSalesCategories = new List<PartsSalesCategoryExtend>();
                    Func<string[], bool> getDbPartsSalesCategories = value => {
                        var dbObj = new PartsSalesCategoryExtend {
                            Id = Convert.ToInt32(value[0]),
                            Code = value[1],
                            Name = value[2],
                            BranchId = Convert.ToInt32(value[3])
                        };
                        dbPartsSalesCategories.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select id,code,name,branchid from PartsSalesCategory where status=1", "Name", true, partsSalesCategoryNeedCheck, getDbPartsSalesCategories);
                    var enterpriseId = Utils.GetCurrentUserInfo().EnterpriseId;
                    foreach(var tempRight in tempRightList) {
                        var rightPartsSalesCategory = dbPartsSalesCategories.FirstOrDefault(v => v.Name == tempRight.PartsSalesCategoryNameStr);
                        if(rightPartsSalesCategory == null) {
                            tempRight.ErrorMsg = ErrorStrings.Export_Title_Partssalescategory_Name + tempRight.PartsSalesCategoryNameStr + ErrorStrings.Export_Validation_Sparepart_NotExist;
                        } else {
                            tempRight.PartsSalesCategoryId = rightPartsSalesCategory.Id;
                            tempRight.PartsSalesCategoryName = rightPartsSalesCategory.Name;
                        }
                    }
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();

                    #region 首选仓库和首选出库仓库业务校验
                    var defaultWarehouseNeedCheck = tempRightList.Select(r => r.DefaultWarehouseNameStr).Distinct().ToArray();
                    var dbWarehouses = new List<WarehouseExtend>();
                    Func<string[], bool> getDbWarehouses = value => {
                        var dbObj = new WarehouseExtend {
                            Id = Convert.ToInt32(value[0]),
                            Code = value[1],
                            Name = value[2],
                            BranchId = Convert.ToInt32(value[3]),
                            IsCentralizedPurchase = value[4] != "" ? Convert.ToBoolean(Convert.ToInt32(value[4])) : false,
                            StorageCenter = value[5] != null ? Convert.ToInt32(value[5]) : 0,
                            StorageCompanyId = Convert.ToInt32(value[6])
                        };
                        dbWarehouses.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select id,code,name,branchid,IsCentralizedPurchase,StorageCenter,storagecompanyid from Warehouse where status=1", "Name", true, defaultWarehouseNeedCheck, getDbWarehouses);
                    foreach(var tempRight in tempRightList) {
                        var rightWarehouse = dbWarehouses.FirstOrDefault(r => r.Name == tempRight.DefaultWarehouseNameStr);
                        if(rightWarehouse == null) {
                            tempRight.ErrorMsg = "首选仓库" + tempRight.DefaultWarehouseNameStr + ErrorStrings.Export_Validation_Sparepart_NotExist;
                        } else {
                            if(rightWarehouse.StorageCompanyId != enterpriseId) {
                                tempRight.ErrorMsg = "首选仓库必须为当前企业所属仓库";
                            } else {
                                tempRight.DefaultWarehouseId = rightWarehouse.Id;
                                tempRight.DefaultWarehouseName = rightWarehouse.Name;
                                tempRight.DefaultIsCentralizedPurchase = rightWarehouse.IsCentralizedPurchase;
                                tempRight.DefaultStorageCenter = rightWarehouse.StorageCenter.HasValue ? rightWarehouse.StorageCenter.Value : 0;
                            }
                        }
                    }
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();
                    var defaultOutWarehouseNeedCheck = tempRightList.Select(r => r.DefaultOutWarehouseNameStr).Distinct().ToArray();
                    var dbOutWarehouses = new List<WarehouseExtend>();
                    Func<string[], bool> getDbOutWarehouses = value => {
                        var dbObj = new WarehouseExtend {
                            Id = Convert.ToInt32(value[0]),
                            Code = value[1],
                            Name = value[2],
                            BranchId = Convert.ToInt32(value[3]),
                            StorageCenter = value[4] != "" ? Convert.ToInt32(value[4]) : 0,
                            PartssalescategoryId = Convert.ToInt32(value[5]),
                            StorageCompanyId = Convert.ToInt32(value[6])

                        };
                        dbOutWarehouses.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select w.id,w.code,w.name,w.branchid,StorageCenter,su.partssalescategoryid, w.storagecompanyid from Warehouse w inner join Salesunitaffiwarehouse suw on w.id=suw.warehouseid inner join Salesunit su on suw.salesunitid=su.id where w.status=1", "W.Name", true, defaultOutWarehouseNeedCheck, getDbOutWarehouses);
                    foreach(var tempRight in tempRightList) {
                        var rightWarehouse = dbOutWarehouses.FirstOrDefault(r => r.Name == tempRight.DefaultOutWarehouseNameStr);
                        if(rightWarehouse == null) {
                            tempRight.ErrorMsg = "首选出库仓库" + tempRight.DefaultOutWarehouseNameStr + ErrorStrings.Export_Validation_Sparepart_NotExist;
                        } else {
                            if(tempRight.DefaultIsCentralizedPurchase) {
                                if(rightWarehouse.StorageCenter == tempRight.DefaultStorageCenter && rightWarehouse.PartssalescategoryId == tempRight.PartsSalesCategoryId && rightWarehouse.StorageCompanyId == enterpriseId) {
                                    tempRight.DefaultOutWarehouseId = rightWarehouse.Id;
                                    tempRight.DefaultOutWarehouseName = rightWarehouse.Name;
                                } else {
                                    tempRight.ErrorMsg = "首选仓库出库仓库输入非法，必须为导入数据品牌仓库且储运中心与首选仓库相同";
                                }
                            } else {
                                if(rightWarehouse.Name != tempRight.DefaultWarehouseName) {
                                    tempRight.ErrorMsg = "首选仓库为非统购仓库时，首选仓库出库仓库必须与首选仓库相同";
                                } else {
                                    tempRight.DefaultOutWarehouseId = rightWarehouse.Id;
                                    tempRight.DefaultOutWarehouseName = rightWarehouse.Name;
                                }
                            }
                        }
                    }
                    #endregion

                    #region 第一仓库和第一出库仓库业务校验
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();
                    var firstWarehouseNeedCheck = tempRightList.Where(r => r.FirstWarehouseNameStr != null && r.FirstWarehouseNameStr != "").Select(r => r.FirstWarehouseNameStr).Distinct().ToArray();
                    if(firstWarehouseNeedCheck.Any()) {
                        var dbFirstWarehouses = new List<WarehouseExtend>();
                        Func<string[], bool> getFirstDbWarehouses = value => {
                            var dbObj = new WarehouseExtend {
                                Id = Convert.ToInt32(value[0]),
                                Code = value[1],
                                Name = value[2],
                                BranchId = Convert.ToInt32(value[3]),
                                IsCentralizedPurchase = value[4] != "" ? Convert.ToBoolean(Convert.ToInt32(value[4])) : false,
                                StorageCenter = value[5] != null ? Convert.ToInt32(value[5]) : 0,
                                StorageCompanyId = Convert.ToInt32(value[6])
                            };
                            dbFirstWarehouses.Add(dbObj);
                            return false;
                        };
                        db.QueryDataWithInOperator("select id,code,name,branchid,IsCentralizedPurchase,StorageCenter,storagecompanyid from Warehouse where status=1", "Name", true, firstWarehouseNeedCheck, getFirstDbWarehouses);
                        foreach(var tempRight in tempRightList) {
                            var rightWarehouse = dbFirstWarehouses.FirstOrDefault(r => r.Name == tempRight.FirstWarehouseNameStr);
                            if(rightWarehouse == null) {
                                tempRight.ErrorMsg = "第一仓库" + tempRight.FirstWarehouseNameStr + ErrorStrings.Export_Validation_Sparepart_NotExist;
                            } else {
                                if(rightWarehouse.StorageCompanyId != enterpriseId) {
                                    tempRight.ErrorMsg = "第一仓库必须为当前企业所属仓库";
                                } else {
                                    tempRight.FirstWarehouseId = rightWarehouse.Id;
                                    tempRight.FirstWarehouseName = rightWarehouse.Name;
                                    tempRight.FirstIsCentralizedPurchase = rightWarehouse.IsCentralizedPurchase;
                                    tempRight.FirstStorageCenter = rightWarehouse.StorageCenter.HasValue ? rightWarehouse.StorageCenter.Value : 0;
                                }
                            }
                        }
                        tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();
                        var firstOutWarehouseNeedCheck = tempRightList.Select(r => r.FirstOutWarehouseNameStr).Distinct().ToArray();
                        var dbFirstOutWarehouses = new List<WarehouseExtend>();
                        Func<string[], bool> getFirstDbOutWarehouses = value => {
                            var dbObj = new WarehouseExtend {
                                Id = Convert.ToInt32(value[0]),
                                Code = value[1],
                                Name = value[2],
                                BranchId = Convert.ToInt32(value[3]),
                                StorageCenter = value[4] != "" ? Convert.ToInt32(value[4]) : 0,
                                PartssalescategoryId = Convert.ToInt32(value[5]),
                                StorageCompanyId = Convert.ToInt32(value[6])

                            };
                            dbFirstOutWarehouses.Add(dbObj);
                            return false;
                        };
                        db.QueryDataWithInOperator("select w.id,w.code,w.name,w.branchid,StorageCenter,su.partssalescategoryid, w.storagecompanyid from Warehouse w inner join Salesunitaffiwarehouse suw on w.id=suw.warehouseid inner join Salesunit su on suw.salesunitid=su.id where w.status=1", "W.Name", true, firstOutWarehouseNeedCheck, getFirstDbOutWarehouses);
                        foreach(var tempRight in tempRightList) {
                            var rightWarehouse = dbFirstOutWarehouses.FirstOrDefault(r => r.Name == tempRight.FirstOutWarehouseNameStr);
                            if(rightWarehouse == null) {
                                tempRight.ErrorMsg = "第一出库仓库" + tempRight.FirstOutWarehouseNameStr + ErrorStrings.Export_Validation_Sparepart_NotExist;
                            } else {
                                if(tempRight.FirstIsCentralizedPurchase) {
                                    if(rightWarehouse.StorageCenter == tempRight.FirstStorageCenter && rightWarehouse.PartssalescategoryId == tempRight.PartsSalesCategoryId && rightWarehouse.StorageCompanyId == enterpriseId) {
                                        tempRight.FirstOutWarehouseId = rightWarehouse.Id;
                                        tempRight.FirstOutWarehouseName = rightWarehouse.Name;
                                    } else {
                                        tempRight.ErrorMsg = "第一仓库出库仓库输入非法，必须为导入数据品牌仓库且储运中心与第一仓库相同";
                                    }
                                } else {
                                    if(rightWarehouse.Name != tempRight.FirstWarehouseName) {
                                        tempRight.ErrorMsg = "第一仓库为非统购仓库时，第一仓库出库仓库必须与第一仓库相同";
                                    } else {
                                        tempRight.FirstOutWarehouseId = rightWarehouse.Id;
                                        tempRight.FirstOutWarehouseName = rightWarehouse.Name;
                                    }
                                }
                            }
                        }
                    }
                    #endregion

                    #region 第二仓库和第二出库仓库业务校验
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();
                    var secoundWarehouseNeedCheck = tempRightList.Where(r => r.SecoundWarehouseNameStr != null && r.SecoundWarehouseNameStr != "").Select(r => r.SecoundWarehouseNameStr).Distinct().ToArray();
                    if(secoundWarehouseNeedCheck.Any()) {
                        var dbSecoundWarehouses = new List<WarehouseExtend>();
                        Func<string[], bool> getSecoundDbWarehouses = value => {
                            var dbObj = new WarehouseExtend {
                                Id = Convert.ToInt32(value[0]),
                                Code = value[1],
                                Name = value[2],
                                BranchId = Convert.ToInt32(value[3]),
                                IsCentralizedPurchase = value[4] != "" ? Convert.ToBoolean(Convert.ToInt32(value[4])) : false,
                                StorageCenter = value[5] != null ? Convert.ToInt32(value[5]) : 0,
                                StorageCompanyId = Convert.ToInt32(value[6])
                            };
                            dbSecoundWarehouses.Add(dbObj);
                            return false;
                        };
                        db.QueryDataWithInOperator("select id,code,name,branchid,IsCentralizedPurchase,StorageCenter,storagecompanyid from Warehouse where status=1", "Name", true, secoundWarehouseNeedCheck, getSecoundDbWarehouses);
                        foreach(var tempRight in tempRightList) {
                            var rightWarehouse = dbSecoundWarehouses.FirstOrDefault(r => r.Name == tempRight.SecoundWarehouseNameStr);
                            if(rightWarehouse == null) {
                                tempRight.ErrorMsg = "第二仓库" + tempRight.SecoundWarehouseNameStr + ErrorStrings.Export_Validation_Sparepart_NotExist;
                            } else {
                                if(rightWarehouse.StorageCompanyId != enterpriseId) {
                                    tempRight.ErrorMsg = "第二仓库必须为当前企业所属仓库";
                                } else {
                                    tempRight.SecoundWarehouseId = rightWarehouse.Id;
                                    tempRight.SecoundWarehouseName = rightWarehouse.Name;
                                    tempRight.SecoundIsCentralizedPurchase = rightWarehouse.IsCentralizedPurchase;
                                    tempRight.SecoundStorageCenter = rightWarehouse.StorageCenter.HasValue ? rightWarehouse.StorageCenter.Value : 0;
                                }
                            }
                        }
                        tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();
                        var secoundOutWarehouseNeedCheck = tempRightList.Select(r => r.SecoundOutWarehouseNameStr).Distinct().ToArray();
                        var dbSecoundOutWarehouses = new List<WarehouseExtend>();
                        Func<string[], bool> getSecoundDbOutWarehouses = value => {
                            var dbObj = new WarehouseExtend {
                                Id = Convert.ToInt32(value[0]),
                                Code = value[1],
                                Name = value[2],
                                BranchId = Convert.ToInt32(value[3]),
                                StorageCenter = value[4] != "" ? Convert.ToInt32(value[4]) : 0,
                                PartssalescategoryId = Convert.ToInt32(value[5]),
                                StorageCompanyId = Convert.ToInt32(value[6])

                            };
                            dbSecoundOutWarehouses.Add(dbObj);
                            return false;
                        };
                        db.QueryDataWithInOperator("select w.id,w.code,w.name,w.branchid,StorageCenter,su.partssalescategoryid, w.storagecompanyid from Warehouse w inner join Salesunitaffiwarehouse suw on w.id=suw.warehouseid inner join Salesunit su on suw.salesunitid=su.id where w.status=1", "W.Name", true, secoundOutWarehouseNeedCheck, getSecoundDbOutWarehouses);
                        foreach(var tempRight in tempRightList) {
                            var rightWarehouse = dbSecoundOutWarehouses.FirstOrDefault(r => r.Name == tempRight.SecoundOutWarehouseNameStr);
                            if(rightWarehouse == null) {
                                tempRight.ErrorMsg = "第二出库仓库" + tempRight.SecoundOutWarehouseNameStr + ErrorStrings.Export_Validation_Sparepart_NotExist;
                            } else {
                                if(tempRight.SecoundIsCentralizedPurchase) {
                                    if(rightWarehouse.StorageCenter == tempRight.SecoundStorageCenter && rightWarehouse.PartssalescategoryId == tempRight.PartsSalesCategoryId && rightWarehouse.StorageCompanyId == enterpriseId) {
                                        tempRight.SecoundOutWarehouseId = rightWarehouse.Id;
                                        tempRight.SecoundOutWarehouseName = rightWarehouse.Name;
                                    } else {
                                        tempRight.ErrorMsg = "第二仓库出库仓库输入非法，必须为导入数据品牌仓库且储运中心与第二仓库相同";
                                    }
                                } else {
                                    if(rightWarehouse.Name != tempRight.SecoundWarehouseName) {
                                        tempRight.ErrorMsg = "第二仓库为非统购仓库时，第二仓库出库仓库必须与第二仓库相同";
                                    } else {
                                        tempRight.SecoundOutWarehouseId = rightWarehouse.Id;
                                        tempRight.SecoundOutWarehouseName = rightWarehouse.Name;
                                    }
                                }
                            }
                        }
                    }
                    #endregion

                    #region 第三仓库和第三出库仓库业务校验
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();
                    var thirdWarehouseNeedCheck = tempRightList.Where(r => r.ThirdWarehouseNameStr != null && r.ThirdWarehouseNameStr != "").Select(r => r.ThirdWarehouseNameStr).Distinct().ToArray();
                    if(thirdWarehouseNeedCheck.Any()) {
                        var dbThirdWarehouses = new List<WarehouseExtend>();
                        Func<string[], bool> getThirdDbWarehouses = value => {
                            var dbObj = new WarehouseExtend {
                                Id = Convert.ToInt32(value[0]),
                                Code = value[1],
                                Name = value[2],
                                BranchId = Convert.ToInt32(value[3]),
                                IsCentralizedPurchase = value[4] != "" ? Convert.ToBoolean(Convert.ToInt32(value[4])) : false,
                                StorageCenter = value[5] != null ? Convert.ToInt32(value[5]) : 0,
                                StorageCompanyId = Convert.ToInt32(value[6])
                            };
                            dbThirdWarehouses.Add(dbObj);
                            return false;
                        };
                        db.QueryDataWithInOperator("select id,code,name,branchid,IsCentralizedPurchase,StorageCenter,storagecompanyid from Warehouse where status=1", "Name", true, thirdWarehouseNeedCheck, getThirdDbWarehouses);
                        foreach(var tempRight in tempRightList) {
                            var rightWarehouse = dbThirdWarehouses.FirstOrDefault(r => r.Name == tempRight.ThirdWarehouseNameStr);
                            if(rightWarehouse == null) {
                                tempRight.ErrorMsg = "第三仓库" + tempRight.ThirdWarehouseNameStr + ErrorStrings.Export_Validation_Sparepart_NotExist;
                            } else {
                                if(rightWarehouse.StorageCompanyId != enterpriseId) {
                                    tempRight.ErrorMsg = "第三仓库必须为当前企业所属仓库";
                                } else {
                                    tempRight.ThirdWarehouseId = rightWarehouse.Id;
                                    tempRight.ThirdWarehouseName = rightWarehouse.Name;
                                    tempRight.ThirdIsCentralizedPurchase = rightWarehouse.IsCentralizedPurchase;
                                    tempRight.ThirdStorageCenter = rightWarehouse.StorageCenter.HasValue ? rightWarehouse.StorageCenter.Value : 0;
                                }
                            }
                        }
                        tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();
                        var thirdOutWarehouseNeedCheck = tempRightList.Select(r => r.ThirdOutWarehouseNameStr).Distinct().ToArray();
                        var dbThirdOutWarehouses = new List<WarehouseExtend>();
                        Func<string[], bool> getThirdDbOutWarehouses = value => {
                            var dbObj = new WarehouseExtend {
                                Id = Convert.ToInt32(value[0]),
                                Code = value[1],
                                Name = value[2],
                                BranchId = Convert.ToInt32(value[3]),
                                StorageCenter = value[4] != "" ? Convert.ToInt32(value[4]) : 0,
                                PartssalescategoryId = Convert.ToInt32(value[5]),
                                StorageCompanyId = Convert.ToInt32(value[6])

                            };
                            dbThirdOutWarehouses.Add(dbObj);
                            return false;
                        };
                        db.QueryDataWithInOperator("select w.id,w.code,w.name,w.branchid,StorageCenter,su.partssalescategoryid, w.storagecompanyid from Warehouse w inner join Salesunitaffiwarehouse suw on w.id=suw.warehouseid inner join Salesunit su on suw.salesunitid=su.id where w.status=1", "W.Name", true, thirdOutWarehouseNeedCheck, getThirdDbOutWarehouses);
                        foreach(var tempRight in tempRightList) {
                            var rightWarehouse = dbThirdOutWarehouses.FirstOrDefault(r => r.Name == tempRight.ThirdOutWarehouseNameStr);
                            if(rightWarehouse == null) {
                                tempRight.ErrorMsg = "第三出库仓库" + tempRight.ThirdOutWarehouseNameStr + ErrorStrings.Export_Validation_Sparepart_NotExist;
                            } else {
                                if(tempRight.ThirdIsCentralizedPurchase) {
                                    if(rightWarehouse.StorageCenter == tempRight.ThirdStorageCenter && rightWarehouse.PartssalescategoryId == tempRight.PartsSalesCategoryId && rightWarehouse.StorageCompanyId == enterpriseId) {
                                        tempRight.ThirdOutWarehouseId = rightWarehouse.Id;
                                        tempRight.ThirdOutWarehouseName = rightWarehouse.Name;
                                    } else {
                                        tempRight.ErrorMsg = "第三仓库出库仓库输入非法，必须为导入数据品牌仓库且储运中心与第三仓库相同";
                                    }
                                } else {
                                    if(rightWarehouse.Name != tempRight.ThirdWarehouseName) {
                                        tempRight.ErrorMsg = "第三仓库为非统购仓库时，第三仓库出库仓库必须与第三仓库相同";
                                    } else {
                                        tempRight.ThirdOutWarehouseId = rightWarehouse.Id;
                                        tempRight.ThirdOutWarehouseName = rightWarehouse.Name;
                                    }
                                }
                            }
                        }
                    }
                    #endregion

                    #region 仓库信息重复情况
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();
                    foreach(var tempRight in tempRightList) {
                        if(tempRight.DefaultWarehouseId == tempRight.FirstWarehouseId || tempRight.DefaultWarehouseId == tempRight.SecoundWarehouseId || tempRight.DefaultWarehouseId == tempRight.ThirdWarehouseId) {
                            tempRight.ErrorMsg = "仓库信息重复";
                            continue;
                        }
                        if(tempRight.FirstWarehouseId != 0 && (tempRight.FirstWarehouseId == tempRight.SecoundWarehouseId || tempRight.FirstWarehouseId == tempRight.ThirdWarehouseId)) {
                            tempRight.ErrorMsg = "仓库信息重复";
                            continue;
                        }
                        if(tempRight.SecoundWarehouseId != 0 && (tempRight.SecoundWarehouseId == tempRight.ThirdWarehouseId)) {
                            tempRight.ErrorMsg = "仓库信息重复";
                            continue;
                        }
                    }

                    var partsSalesCategoryIds = tempRightList.Select(r => r.PartsSalesCategoryId + "").Distinct().ToArray();
                    var WarehouseIdPartsSalesCategoryIds = new List<WarehouseIdPartsSalesCategoryId>();
                    int jtPartsSalesCategoryId=0;
                    Func<string[], bool> getjtPartsSalesCategoryIds = value => {
                        List<string> temp = partsSalesCategoryIds.ToList();
                        jtPartsSalesCategoryId = Convert.ToInt32(value[0]);
                        temp.Add(value[0]);
                        partsSalesCategoryIds = temp.ToArray();
                        return false;
                    };
                    //查询集团配件 品牌ID
                    db.QueryDataWithInOperator(@"select Id,Name,BranchId from PartsSalesCategory where status=1 and name='集团配件'", "BranchId",
                        true, new string[] { enterpriseId.ToString() }, getjtPartsSalesCategoryIds);
                    Func<string[], bool> getWarehouseIdPartsSalesCategoryIds = value => {
                        var dbObj = new WarehouseIdPartsSalesCategoryId {
                            WarehouseId = Convert.ToInt32(value[0]),
                            PartsSalesCategoryId = Convert.ToInt32(value[1])
                        };
                        WarehouseIdPartsSalesCategoryIds.Add(dbObj);
                        return false;
                    };

                    db.QueryDataWithInOperator(@"select w.WarehouseId,s.PartsSalesCategoryId from SalesUnitAffiWarehouse w
                                                 inner join SalesUnit s
                                                 on w.SalesUnitId = s.Id
                                                 where status = 1", "PartsSalesCategoryId", true, partsSalesCategoryIds, getWarehouseIdPartsSalesCategoryIds);



                    foreach(var tempRight in tempRightList) {
                        var listWarehouseIdPartsSalesCategoryId = WarehouseIdPartsSalesCategoryIds.Where(r => r.PartsSalesCategoryId == tempRight.PartsSalesCategoryId || r.PartsSalesCategoryId == jtPartsSalesCategoryId);
                        if(tempRight.DefaultWarehouseId > 0 && !listWarehouseIdPartsSalesCategoryId.Any(r => r.WarehouseId == tempRight.DefaultWarehouseId))
                            tempRight.ErrorMsg = tempRight.DefaultWarehouseNameStr + "非本品牌仓库或统购库";
                        if(tempRight.FirstWarehouseId > 0 && !listWarehouseIdPartsSalesCategoryId.Any(r => r.WarehouseId == tempRight.FirstWarehouseId))
                            tempRight.ErrorMsg = tempRight.FirstWarehouseNameStr + "非本品牌仓库或统购库";
                        if(tempRight.SecoundWarehouseId > 0 && !listWarehouseIdPartsSalesCategoryId.Any(r => r.WarehouseId == tempRight.SecoundWarehouseId))
                            tempRight.ErrorMsg = tempRight.SecoundWarehouseNameStr + "非本品牌仓库或统购库";
                        if(tempRight.ThirdWarehouseId > 0 && !listWarehouseIdPartsSalesCategoryId.Any(r => r.WarehouseId == tempRight.ThirdWarehouseId))
                            tempRight.ErrorMsg = tempRight.ThirdWarehouseNameStr + "非本品牌仓库或统购库";

                    }
                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();
                    #endregion
                    #endregion

                    #region 判断是新增 还是 修改
                    var warehouseSequenceNeedCheck = tempRightList.Select(r => r.CustomerCompanyCode).Distinct().ToArray();
                    var dbWarehouseSequences = new List<WarehouseSequenceExtend>();
                    Func<string[], bool> getdbWarehouseSequences = value => {
                        var dbObj = new WarehouseSequenceExtend {
                            Id = Convert.ToInt32(value[0]),
                            CustomerCompanyCode = Convert.ToString(value[1]),
                            PartsSalesCategoryId = Convert.ToInt32(value[2])
                        };
                        dbWarehouseSequences.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select Id, customercompanycode,partssalescategoryid from WarehouseSequence where status=1", "customercompanycode", true, warehouseSequenceNeedCheck, getdbWarehouseSequences);
                    foreach(var tempRight in tempRightList) {
                        var oldWarehouseSequence = dbWarehouseSequences.FirstOrDefault(r => r.CustomerCompanyCode == tempRight.CustomerCompanyCode && r.PartsSalesCategoryId == tempRight.PartsSalesCategoryId);
                        if(oldWarehouseSequence == null) {
                            rightInsertList.Add(tempRight);
                        } else {
                            tempRight.Id = oldWarehouseSequence.Id;
                            rightUpdateList.Add(tempRight);
                        }
                    }
                    #endregion



                    //获取所有不合格数据
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    //获取所有合格数据
                    rightList = allList.Except(errorList).ToList();
                }

                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempObj = list[index - 1];
                            var values = new object[] {
                                #region 设置错误信息导出的列的值
                                tempObj.CustomerCompanyCodeStr,
                                tempObj.CustomerCompanyNameStr,
                                tempObj.PartsSalesCategoryNameStr,
                                tempObj.IsAutoApproveStr,
                                tempObj.DefaultWarehouseNameStr,
                                tempObj.DefaultOutWarehouseNameStr,
                                tempObj.FirstWarehouseNameStr,
                                tempObj.FirstOutWarehouseNameStr,
                                tempObj.SecoundWarehouseNameStr,
                                tempObj.SecoundOutWarehouseNameStr,
                                tempObj.ThirdWarehouseNameStr,
                                tempObj.ThirdOutWarehouseNameStr,
                                tempObj.ErrorMsg
                                #endregion
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }
                //导入所有合格数据
                if(!rightList.Any())
                    return true;
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    //开启事务，新增更新配件在一个事务内
                    var ts = conn.BeginTransaction();
                    try {
                        //新增配件
                        if(rightList.Any()) {
                            #region 获取对应的插入的SQL 通过db.GetInsertSql方法
                            //获取新增数据的sql语句，Id为主键
                            var sqlInsert = db.GetInsertSql("WarehouseSequence", "Id", new[] {
                                    "PartsSalesCategoryId","PartsSalesCategoryName", "CustomerCompanyId","CustomerCompanyCode","CustomerCompanyName",
                                    "IsAutoApprove","DefaultWarehouseId","DefaultWarehouseName","DefaultOutWarehouseId","DefaultOutWarehouseName",
                                    "FirstWarehouseId","FirstWarehouseName","FirstOutWarehouseId","FirstOutWarehouseName",
                                    "SecoundWarehouseId","SecoundWarehouseName","SecoundOutWarehouseId","SecoundOutWarehouseName",
                                    "ThirdWarehouseId","ThirdWarehouseName","ThirdOutWarehouseId","ThirdOutWarehouseName","Status",
                                    "CreatorId","CreatorName","CreateTime"
                                });

                            var sqlUpdate = db.GetUpdateSql("WarehouseSequence", new[] {
                                    "IsAutoApprove","DefaultWarehouseId","DefaultWarehouseName","DefaultOutWarehouseId","DefaultOutWarehouseName",
                                    "FirstWarehouseId","FirstWarehouseName","FirstOutWarehouseId","FirstOutWarehouseName",
                                    "SecoundWarehouseId","SecoundWarehouseName","SecoundOutWarehouseId","SecoundOutWarehouseName",
                                    "ThirdWarehouseId","ThirdWarehouseName","ThirdOutWarehouseId","ThirdOutWarehouseName","ModifierId","ModifierName", "ModifyTime"
                                }, new[] { "Id" });
                            #endregion
                            var userinfo = Utils.GetCurrentUserInfo();
                            //往数据库增加配件信息
                            foreach(var item in rightInsertList) {
                                #region 添加Sql的参数
                                var command = db.CreateDbCommand(sqlInsert, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("PartsSalesCategoryId", item.PartsSalesCategoryId));
                                command.Parameters.Add(db.CreateDbParameter("PartsSalesCategoryName", item.PartsSalesCategoryName));
                                command.Parameters.Add(db.CreateDbParameter("CustomerCompanyId", item.CustomerCompanyId));
                                command.Parameters.Add(db.CreateDbParameter("CustomerCompanyCode", item.CustomerCompanyCode));
                                command.Parameters.Add(db.CreateDbParameter("CustomerCompanyName", item.CustomerCompanyName));
                                command.Parameters.Add(db.CreateDbParameter("IsAutoApprove", item.IsAutoApprove));
                                command.Parameters.Add(db.CreateDbParameter("DefaultWarehouseId", item.DefaultWarehouseId));
                                command.Parameters.Add(db.CreateDbParameter("DefaultWarehouseName", item.DefaultWarehouseName));
                                command.Parameters.Add(db.CreateDbParameter("DefaultOutWarehouseId", item.DefaultOutWarehouseId));
                                command.Parameters.Add(db.CreateDbParameter("DefaultOutWarehouseName", item.DefaultOutWarehouseName));

                                command.Parameters.Add(db.CreateDbParameter("FirstWarehouseId", item.FirstWarehouseId));
                                command.Parameters.Add(db.CreateDbParameter("FirstWarehouseName", item.FirstWarehouseName));
                                command.Parameters.Add(db.CreateDbParameter("FirstOutWarehouseId", item.FirstOutWarehouseId));
                                command.Parameters.Add(db.CreateDbParameter("FirstOutWarehouseName", item.FirstOutWarehouseName));

                                command.Parameters.Add(db.CreateDbParameter("SecoundWarehouseId", item.SecoundWarehouseId));
                                command.Parameters.Add(db.CreateDbParameter("SecoundWarehouseName", item.SecoundWarehouseName));
                                command.Parameters.Add(db.CreateDbParameter("SecoundOutWarehouseId", item.SecoundOutWarehouseId));
                                command.Parameters.Add(db.CreateDbParameter("SecoundOutWarehouseName", item.SecoundOutWarehouseName));

                                command.Parameters.Add(db.CreateDbParameter("ThirdWarehouseId", item.ThirdWarehouseId));
                                command.Parameters.Add(db.CreateDbParameter("ThirdWarehouseName", item.ThirdWarehouseName));
                                command.Parameters.Add(db.CreateDbParameter("ThirdOutWarehouseId", item.ThirdOutWarehouseId));
                                command.Parameters.Add(db.CreateDbParameter("ThirdOutWarehouseName", item.ThirdOutWarehouseName));

                                command.Parameters.Add(db.CreateDbParameter("Status", (int)DcsBaseDataStatus.有效));
                                command.Parameters.Add(db.CreateDbParameter("CreatorId", userinfo.Id));
                                command.Parameters.Add(db.CreateDbParameter("CreatorName", userinfo.Name));
                                command.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                var tempId = db.ExecuteInsert(command, "Id");
                                #endregion
                            }

                            foreach(var item in rightUpdateList) {
                                #region 添加Sql的参数
                                var command = db.CreateDbCommand(sqlUpdate, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("IsAutoApprove", item.IsAutoApprove));
                                command.Parameters.Add(db.CreateDbParameter("DefaultWarehouseId", item.DefaultWarehouseId));
                                command.Parameters.Add(db.CreateDbParameter("DefaultWarehouseName", item.DefaultWarehouseName));
                                command.Parameters.Add(db.CreateDbParameter("DefaultOutWarehouseId", item.DefaultOutWarehouseId));
                                command.Parameters.Add(db.CreateDbParameter("DefaultOutWarehouseName", item.DefaultOutWarehouseName));

                                command.Parameters.Add(db.CreateDbParameter("FirstWarehouseId", item.FirstWarehouseId));
                                command.Parameters.Add(db.CreateDbParameter("FirstWarehouseName", item.FirstWarehouseName));
                                command.Parameters.Add(db.CreateDbParameter("FirstOutWarehouseId", item.FirstOutWarehouseId));
                                command.Parameters.Add(db.CreateDbParameter("FirstOutWarehouseName", item.FirstOutWarehouseName));

                                command.Parameters.Add(db.CreateDbParameter("SecoundWarehouseId", item.SecoundWarehouseId));
                                command.Parameters.Add(db.CreateDbParameter("SecoundWarehouseName", item.SecoundWarehouseName));
                                command.Parameters.Add(db.CreateDbParameter("SecoundOutWarehouseId", item.SecoundOutWarehouseId));
                                command.Parameters.Add(db.CreateDbParameter("SecoundOutWarehouseName", item.SecoundOutWarehouseName));

                                command.Parameters.Add(db.CreateDbParameter("ThirdWarehouseId", item.ThirdWarehouseId));
                                command.Parameters.Add(db.CreateDbParameter("ThirdWarehouseName", item.ThirdWarehouseName));
                                command.Parameters.Add(db.CreateDbParameter("ThirdOutWarehouseId", item.ThirdOutWarehouseId));
                                command.Parameters.Add(db.CreateDbParameter("ThirdOutWarehouseName", item.ThirdOutWarehouseName));

                                command.Parameters.Add(db.CreateDbParameter("ModifierId", userinfo.Id));
                                command.Parameters.Add(db.CreateDbParameter("ModifierName", userinfo.Name));
                                command.Parameters.Add(db.CreateDbParameter("ModifyTime", DateTime.Now));
                                command.Parameters.Add(db.CreateDbParameter("Id", item.Id));
                                command.ExecuteNonQuery();
                                #endregion
                            }
                        }
                        //无异常提交
                        ts.Commit();
                    } catch(Exception) {
                        //报错回滚
                        ts.Rollback();
                        throw;
                    } finally {
                        if(conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }
    }
}
