﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        /// <summary>
        /// 导出销售结算自动任务执行结果
        /// </summary>
        /// <param name="brandId">品牌Id</param>
        /// <param name="executionTimeBegin">创建开始时间</param>
        /// <param name="executionTimeEnd">创建结束时间</param>
        /// <param name="executionStatus">状态</param>
        /// <param name="companyCode">企业编号</param>
        /// <param name="companyName">企业名称</param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public bool ExportAutoTaskExecutResult(int[] ids, int? brandId, int? executionStatus, string companyCode, string companyName, DateTime? executionTimeBegin, DateTime? executionTimeEnd, out string fileName) {
            fileName = GetExportFilePath("销售结算自动任务执行结果_" + ".xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.Append(@"select  BrandName,CompanyCode,CompanyName,case ExecutionStatus
                                           when 1 then
                                           '成功'
                                           when 2 then
                                           '部分成功'
                                           when 2 then
                                           '失败'
                                           end as ExecutionStatus,ExecutionLoseReason,ExecutionTime
                                     from AutoTaskExecutResult a where 1=1 ");
                    var dbParameters = new List<DbParameter>();
                    if(ids != null && ids.Length > 0) {
                        sql.Append(" and a.id in (");
                        for(var i = 0; i < ids.Length; i++) {
                            if(ids.Length == i + 1) {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            } else {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                        }
                        sql.Append(")");
                    } else {
                        if(brandId.HasValue) {
                            sql.Append(@" and a.brandId ={0}brandId ");
                            dbParameters.Add(db.CreateDbParameter("brandId", brandId.Value));
                        }
                        if(executionTimeBegin.HasValue) {
                            sql.Append(@" and a.ExecutionTime >=To_date({0}executionTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = executionTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("executionTimeBegin", tempTime.ToString("G")));
                        }
                        if(executionTimeEnd.HasValue) {
                            sql.Append(@" and a.ExecutionTime <=To_date({0}executionTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = executionTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("executionTimeEnd", tempTime.ToString("G")));
                        }
                        if(!string.IsNullOrEmpty(companyCode)) {
                            sql.Append(@" and a.CompanyCode ={0}companyCode ");
                            dbParameters.Add(db.CreateDbParameter("companyCode", companyCode));
                        }
                        if(!string.IsNullOrEmpty(companyName)) {
                            sql.Append(@" and a.companyName ={0}companyName ");
                            dbParameters.Add(db.CreateDbParameter("companyName", companyName));
                        }
                        if(executionStatus.HasValue) {
                            sql.Append(@" and a.executionStatus ={0}executionStatus ");
                            dbParameters.Add(db.CreateDbParameter("executionStatus", executionStatus.Value));
                        }
                    }
                    var sqlStr = sql.ToString();
                    if(sqlStr.IndexOf(" where 1=1  and ") > 0)
                        sqlStr = sqlStr.Replace(" where 1=1  and ", " where ");
                    else
                        sqlStr = sqlStr.Replace(" where 1=1 ", "");
                    var command = db.CreateDbCommand(string.Format(sqlStr, db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                        ErrorStrings.Export_Title_Partssalescategory_Name, ErrorStrings.Export_Title_Company_Code, ErrorStrings.Export_Title_Company_Name, "执行状态", "执行失败原因", "执行时间"
                                    };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }

                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }

        }
    }
}