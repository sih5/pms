﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {

        /// <summary>
        /// 导出配件返利帐户
        /// </summary>
        /// <param name="branchId">分公司Id</param>
        /// <param name="customerCompanyCode">客户企业编号</param>
        /// <param name="customerCompanyName">客户企业名称</param>
        /// <param name="createTimeBegin">创建开始时间</param>
        /// <param name="createTimeEnd">创建结束时间</param>
        /// <param name="accountGroupId">账户组Id</param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public bool ExportPartsRebateAccount(int? branchId, string customerCompanyCode, string customerCompanyName, DateTime? createTimeBegin, DateTime? createTimeEnd, int? accountGroupId, string businessCode, out string fileName) {
            fileName = GetExportFilePath("配件返利帐户_" + ".xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.Append(@"select    b.Name as BranchName,
                                           d.Code as CompanyCode,
                                           d.Name as CompanyName,
                                           c.Name as AccountGroupName,
                                           a.AccountBalanceAmount,
                                           (select value from keyvalueitem where NAME = 'MasterData_Status'and key=a.Status) As Status,
                                           a.CreatorName,
                                           a.CreateTime,
                                           a.ModifierName,
                                           a.ModifyTime,
                                           case d.type
                                           when 2 then
                                           s.businessCode
                                           when 7 then
                                           s.businessCode
                                           when 3 then
                                           u.businessCode
                                           end
                                     from PartsRebateAccount a
                                     left join Branch b on a.BranchId = b.Id
                                     left join AccountGroup c on a.AccountGroupId=c.Id
                                     Left Join Company d
                                     On a.Customercompanyid = d.Id
                                     Left Join SalesUnit u
                                     On a.AccountGroupId = u.AccountGroupId and c.SalesCompanyId = u.OwnerCompanyId
                                     Left Join DealerServiceInfo s
                                     On u.PartsSalesCategoryId = s.PartsSalesCategoryId and a.CustomerCompanyId = s.DealerId and s.status=1 where 1=1 ");
                    var dbParameters = new List<DbParameter>();
                    if(branchId.HasValue) {
                        sql.Append(@" and a.branchId ={0}branchId ");
                        dbParameters.Add(db.CreateDbParameter("branchId", branchId.Value));
                    }
                    if(!String.IsNullOrEmpty(customerCompanyCode)) {
                        sql.Append(@" and a.customerCompanyCode like {0}customerCompanyCode ");
                        dbParameters.Add(db.CreateDbParameter("customerCompanyCode", "%" + customerCompanyCode + "%"));
                    }
                    if(!String.IsNullOrEmpty(customerCompanyName)) {
                        sql.Append(@" and a.customerCompanyName like {0}customerCompanyName ");
                        dbParameters.Add(db.CreateDbParameter("customerCompanyName", "%" + customerCompanyName + "%"));
                    }
                    if(createTimeBegin.HasValue) {
                        sql.Append(@" and a.createTime >=To_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                        var tempValue = createTimeBegin.Value;
                        var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                        dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                    }
                    if(createTimeEnd.HasValue) {
                        sql.Append(@" and a.createTime <=To_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                        var tempValue = createTimeEnd.Value;
                        var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                        dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                    }
                    if(accountGroupId.HasValue) {
                        sql.Append(@" and a.accountGroupId ={0}accountGroupId ");
                        dbParameters.Add(db.CreateDbParameter("accountGroupId", accountGroupId.Value));
                    }
                    if(!string.IsNullOrEmpty(businessCode)) {
                        sql.Append(" and s.businessCode like {0}businessCode and u.businessCode like {0}businessCode ");
                        dbParameters.Add(db.CreateDbParameter("businessCode", "%" + businessCode + "%"));
                    }
                    var sqlStr = sql.ToString();
                    if(sqlStr.IndexOf(" where 1=1  and ") > 0)
                        sqlStr = sqlStr.Replace(" where 1=1  and ", " where ");
                    else
                        sqlStr = sqlStr.Replace(" where 1=1 ", "");
                    var command = db.CreateDbCommand(string.Format(sqlStr, db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                        ErrorStrings.Export_Title_Dealerserviceinfo_BranchName, ErrorStrings.Export_Title_Credenceapplication_CompanyCode, ErrorStrings.Export_Title_Credenceapplication_CompanyName, "帐户组", "结存金额", ErrorStrings.Export_Title_AccountPeriod_Status, ErrorStrings.Export_Title_AccountPeriod_CreatorName, ErrorStrings.Export_Title_AccountPeriod_CreateTime, ErrorStrings.Export_Title_AccountPeriod_ModifierName, ErrorStrings.Export_Title_AccountPeriod_ModifyTime,ErrorStrings.Export_Title_Credenceapplication_BusinessCode
                                    };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }

                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }

        }
    }
}