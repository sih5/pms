﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        /// <summary>
        /// 导入配件返利申请单
        /// </summary>
        public bool ImportPartsRebateApplication(int branchId, string fileName, out int excelImportNum, out List<PartsRebateApplicationExtend> rightData, out List<PartsRebateApplicationExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<PartsRebateApplicationExtend>();
            var rightList = new List<PartsRebateApplicationExtend>();
            var allList = new List<PartsRebateApplicationExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("PartsRebateApplication", out notNullableFields, out fieldLenght);
                List<string> notNullableFieldsPartsSalesCategory;
                Dictionary<string, int> fieldLenghtPartsSalesCategory;
                db.GetTableSchema("PartsSalesCategory", out notNullableFieldsPartsSalesCategory, out fieldLenghtPartsSalesCategory);
                List<string> notNullableFieldsPartsRebateType;
                Dictionary<string, int> fieldLenghtPartsRebateType;
                db.GetTableSchema("PartsRebateType", out notNullableFieldsPartsRebateType, out fieldLenghtPartsRebateType);
                List<string> notNullableFieldsAccountGroup;
                Dictionary<string, int> fieldLenghtAccountGroup;
                db.GetTableSchema("AccountGroup", out notNullableFieldsAccountGroup, out fieldLenghtAccountGroup);

                List<object> excelColumns;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    #region 指定文件中的列对应的名称 例如 excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode, "PartCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Credenceapplication_CompanyCode, "CustomerCompanyCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Credenceapplication_CompanyName, "CustomerCompanyName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Customertransferbill_Amount, "Amount");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Partssalescategory_Name, "PartsSalesCategoryId");
                    excelOperator.AddColumnDataSource("配件返利类型", "PartsRebateTypeId");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Credenceapplication_AccountGroupNameNew, "AccountGroupId");
                    excelOperator.AddColumnDataSource("返利方向", "RebateDirection");
                    excelOperator.AddColumnDataSource("申请原因", "Motive");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Remark, "Remark");
                    #endregion
                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;
                    #region 获取对应枚举
                    var keyValuePairs = new[] { new KeyValuePair<string, string>("RebateDirection", "PartsRebateDirection") };
                    tempExcelOperator.ImportHelper.LoadEnumFromDb(null, null, keyValuePairs);

                    #endregion
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var tempImportObj = new PartsRebateApplicationExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        #region 根据导入的字段给对应的实体属性赋值
                        tempImportObj.CustomerCompanyCodeStr = newRow["CustomerCompanyCode"];
                        tempImportObj.CustomerCompanyNameStr = newRow["CustomerCompanyName"];
                        tempImportObj.AmountStr = newRow["Amount"];
                        tempImportObj.PartsSalesCategoryStr = newRow["PartsSalesCategoryId"];
                        tempImportObj.PartsRebateTypeStr = newRow["PartsRebateTypeId"];
                        tempImportObj.AccountGroupStr = newRow["AccountGroupId"];
                        tempImportObj.RebateDirectionStr = newRow["RebateDirection"];
                        tempImportObj.MotiveStr = newRow["Motive"];
                        tempImportObj.RemarkStr = newRow["Remark"];
                        #endregion

                        var tempErrorMessage = new List<string>();
                        #region 导入的数据基本检查
                        //客户企业编号
                        var fieldIndex = notNullableFields.IndexOf("CustomerCompanyCode".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.CustomerCompanyCodeStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validarion_CustomerOrderPriceGrade_CustomerCompanyCodeIsNull);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.CustomerCompanyCodeStr) > fieldLenght["CustomerCompanyCode".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validarion_CustomerOrderPriceGrade_CustomerCompanyCodeIsLong);
                        }
                        //客户企业名称
                        fieldIndex = notNullableFields.IndexOf("CustomerCompanyName".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.CustomerCompanyNameStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add("客户企业名称不能为空");
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.CustomerCompanyNameStr) > fieldLenght["CustomerCompanyName".ToUpper()])
                                tempErrorMessage.Add("客户企业名称过长");
                        }
                        //金额
                        fieldIndex = notNullableFields.IndexOf("Amount".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.AmountStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Customertransferbill_AmountIsNull);
                        } else {
                            decimal checkValue;
                            if(decimal.TryParse(tempImportObj.AmountStr, out checkValue)) {
                                if(checkValue <= 0) {
                                    tempErrorMessage.Add("金额必须大于0");
                                }
                                decimal rodAmount = Math.Round(checkValue, 2);
                                if(rodAmount <= 0) {
                                    tempErrorMessage.Add("金额取两位小数之后必须大于0");
                                }
                                tempImportObj.Amount = rodAmount;
                            } else {
                                tempErrorMessage.Add("金额必须是数字");
                            }
                        }
                        //品牌
                        fieldIndex = notNullableFieldsPartsSalesCategory.IndexOf("Name".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.PartsSalesCategoryStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_MarketingDepartment_PartsSalesCategoryIsNull);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.PartsSalesCategoryStr) > fieldLenghtPartsSalesCategory["Name".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_MarketingDepartment_PartsSalesCategoryIsLong);
                        }
                        //配件返利类型
                        fieldIndex = notNullableFieldsPartsRebateType.IndexOf("Name".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.PartsRebateTypeStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add("配件返利类型不能为空");
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.PartsRebateTypeStr) > fieldLenghtPartsRebateType["Name".ToUpper()])
                                tempErrorMessage.Add("配件返利类型过长");
                        }
                        //账户组
                        fieldIndex = notNullableFieldsAccountGroup.IndexOf("Name".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.AccountGroupStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Credenceapplication_AccountGroupName);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.AccountGroupStr) > fieldLenghtAccountGroup["Name".ToUpper()])
                                tempErrorMessage.Add("账户组过长");
                        }
                        //返利方向
                        fieldIndex = notNullableFields.IndexOf("RebateDirection".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.RebateDirectionStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add("返利方向不能为空");
                        } else {
                            var tempEnumValue = tempExcelOperator.ImportHelper.GetEnumValue("RebateDirection", tempImportObj.RebateDirectionStr);
                            if(!tempEnumValue.HasValue) {
                                tempErrorMessage.Add("返利方向不正确");
                            } else {
                                tempImportObj.RebateDirection = tempEnumValue.Value;
                            }
                        }
                        //申请原因
                        fieldIndex = notNullableFields.IndexOf("Motive".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.MotiveStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add("申请原因不能为空");
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.MotiveStr) > fieldLenght["Motive".ToUpper()])
                                tempErrorMessage.Add("申请原因过长");
                        }
                        //备注
                        fieldIndex = notNullableFields.IndexOf("Remark".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.RemarkStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchasePricingDetail_Validation15);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.RemarkStr) > fieldLenght["Remark".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchasePricingDetail_Validation16);
                        }
                        #endregion
                        if(tempErrorMessage.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });
                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    #region 剩下的数据进行业务检查
                    var groups = tempRightList.GroupBy(r => new {
                        r.CustomerCompanyCodeStr,
                        r.CustomerCompanyNameStr,
                        r.AmountStr,
                        r.PartsSalesCategoryStr,
                        r.PartsRebateTypeStr,
                        r.AccountGroupStr,
                        r.RebateDirectionStr,
                        r.MotiveStr,
                        r.RemarkStr
                    }).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        groupItem.ErrorMsg = ErrorStrings.ImpPartsSupplierRelation_Validation31;
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    //1.查询企业
                    var companyCodeNeedCheck = tempRightList.Select(r => r.CustomerCompanyCodeStr).Distinct().ToArray();
                    var dbCompanies = new List<CompanyExtend>();
                    Func<string[], bool> getDbCompanies = value => {
                        dbCompanies.Add(new CompanyExtend {
                            Id = Convert.ToInt32(value[0]),
                            Code = value[1],
                            Name = value[2]
                        });
                        return false;
                    };
                    db.QueryDataWithInOperator("Select Id, Code, Name From Company Where Status = 1 ", "Code", true, companyCodeNeedCheck, getDbCompanies);
                    //2.查询配件销售类型
                    var partsSalesCategoryNameNeedCheck = tempRightList.Select(r => r.PartsSalesCategoryStr).Distinct().ToArray();
                    var dbPartsSalesCategorys = new List<PartsSalesCategoryExtend>();
                    Func<string[], bool> getDbPartsSalesCategorys = value => {
                        dbPartsSalesCategorys.Add(new PartsSalesCategoryExtend {
                            Id = Convert.ToInt32(value[0]),
                            Code = value[1],
                            Name = value[2],
                            BranchId = Convert.ToInt32(value[3])
                        });
                        return false;
                    };
                    db.QueryDataWithInOperator("Select Id, Code, Name, BranchId From PartsSalesCategory Where Status = 1 ", "Name", true, partsSalesCategoryNameNeedCheck, getDbPartsSalesCategorys);
                    //3.查询配件返利类型
                    var partsRebateTypeNameNeedCheck = tempRightList.Select(r => r.PartsRebateTypeStr).Distinct().ToArray();
                    var dbPartsRebateTypes = new List<PartsRebateTypeExtend>();
                    Func<string[], bool> getDbPartsRebateTypes = value => {
                        dbPartsRebateTypes.Add(new PartsRebateTypeExtend {
                            Id = Convert.ToInt32(value[0]),
                            Code = value[1],
                            Name = value[2],
                            PartsSalesCategoryId = Convert.ToInt32(value[3])
                        });
                        return false;
                    };
                    db.QueryDataWithInOperator("Select Id, Code, Name, PartsSalesCategoryId From PartsRebateType Where Status = 1 ", "Name", true, partsRebateTypeNameNeedCheck, getDbPartsRebateTypes);
                    //4.查询账户组
                    var accountGroupNameNeedCheck = tempRightList.Select(r => r.AccountGroupStr).Distinct().ToArray();
                    var dbAccountGroups = new List<AccountGroupExtend>();
                    Func<string[], bool> getDbAccountGroups = value => {
                        dbAccountGroups.Add(new AccountGroupExtend {
                            Id = Convert.ToInt32(value[0]),
                            Code = value[1],
                            Name = value[2]
                        });
                        return false;
                    };
                    var sql = string.Format(@"Select Id, Code, Name
                                          From Accountgroup
                                         Where Status = 1
                                           And (Salescompanyid = {0} Or Exists
                                                (Select 1
                                                   From Agencyaffibranch a
                                                  Where a.Agencyid = Accountgroup.Salescompanyid
                                                    And a.Branchid = {0}))", branchId);
                    db.QueryDataWithInOperator(sql, "Name", true, accountGroupNameNeedCheck, getDbAccountGroups);
                    var dbAccountGroupAndSalesUnits = new List<AccountGroupExtend>();
                    Func<string[], bool> getDbAccountGroupAndSalesUnits = value => {
                        dbAccountGroupAndSalesUnits.Add(new AccountGroupExtend {
                            Id = Convert.ToInt32(value[0]),
                            PartsSalesCategoryId = Convert.ToInt32(value[1])
                        });
                        return false;
                    };
                    db.QueryDataWithInOperator("select a.id,s.PartsSalesCategoryId from SalesUnit  s inner join  AccountGroup a on s.AccountGroupId=a.id ", "a.Name", true, accountGroupNameNeedCheck, getDbAccountGroupAndSalesUnits);

                    foreach(var tempRight in tempRightList) {
                        var company = dbCompanies.FirstOrDefault(v => v.Code == tempRight.CustomerCompanyCodeStr);
                        if(company == null) {
                            tempRight.ErrorMsg = ErrorStrings.Export_Validation_PartsSpecialTreatyPrice_Validation1;
                            continue;
                        }
                        var partsSalesCategorys = dbPartsSalesCategorys.Where(v => v.Name == tempRight.PartsSalesCategoryStr).ToArray();
                        if(!partsSalesCategorys.Any()) {
                            tempRight.ErrorMsg = ErrorStrings.ImpVehicleDLRStartSecurityDeposit_Validation8;
                            continue;
                        }
                        var partsSalesCategory = partsSalesCategorys.FirstOrDefault(r => r.BranchId == branchId);
                        if(partsSalesCategory == null) {
                            tempRight.ErrorMsg = "品牌所属营销分公司不正确";
                            continue;
                        }
                        var partsRebateType = dbPartsRebateTypes.FirstOrDefault(r => r.Name == tempRight.PartsRebateTypeStr && r.PartsSalesCategoryId == partsSalesCategory.Id);
                        if(partsRebateType == null) {
                            tempRight.ErrorMsg = "当前品牌下不存在对应配件返利类型";
                            continue;
                        }
                        var accountGroup = dbAccountGroups.FirstOrDefault(r => r.Name == tempRight.AccountGroupStr);
                        if(accountGroup == null) {
                            tempRight.ErrorMsg = "对应营销分公司，代理库下账户组不存在";
                            continue;
                        }
                        tempRight.CustomerCompanyId = company.Id;
                        tempRight.CustomerCompanyName = company.Name;
                        tempRight.PartsSalesCategoryId = partsSalesCategory.Id;
                        tempRight.PartsRebateTypeId = partsRebateType.Id;
                        tempRight.AccountGroupId = accountGroup.Id;
                        var salesUnit = dbAccountGroupAndSalesUnits.FirstOrDefault(r => r.PartsSalesCategoryId == tempRight.PartsSalesCategoryId);
                        if(salesUnit == null) {
                            tempRight.ErrorMsg = "账户组所属品牌与导入品牌不一致";
                            continue;
                        }
                    }

                    #endregion
                    //获取所有不合格数据
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    //获取所有合格数据
                    rightList = allList.Except(errorList).ToList();
                    #region 将合格数据的值填上
                    foreach(var rightItem in rightList) {
                        rightItem.CustomerCompanyCode = rightItem.CustomerCompanyCodeStr;
                        rightItem.Motive = rightItem.MotiveStr;
                        rightItem.Remark = rightItem.RemarkStr;
                    }
                    #endregion
                }
                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempObj = list[index - 1];
                            var values = new object[] {
                                #region 设置错误信息导出的列的值
                                tempObj.CustomerCompanyCodeStr, tempObj.CustomerCompanyNameStr, 
                                tempObj.AmountStr, tempObj.PartsSalesCategoryStr,
                                tempObj.PartsRebateTypeStr, tempObj.AccountGroupStr, 
                                tempObj.RebateDirectionStr, tempObj.MotiveStr,
                                tempObj.RemarkStr, tempObj.ErrorMsg
                                #endregion                     
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }

                //导入所有合格数据
                if(!rightList.Any())
                    return true;
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    //开启事务，新增更新配件在一个事务内
                    var ts = conn.BeginTransaction();
                    try {
                        //新增配件
                        if(rightList.Any()) {
                            #region 获取对应的插入的SQL 通过db.GetInsertSql方法
                            //获取新增数据的sql语句，Id为主键
                            var sqlInsert = db.GetInsertSql("PartsRebateApplication", "Id", new[] {
                                "Code","CustomerCompanyId","CustomerCompanyCode","CustomerCompanyName","BranchId","PartsSalesCategoryId","PartsRebateTypeId","AccountGroupId",
                                "RebateDirection","Amount","Motive","Status","Remark", "CreatorId", "CreatorName", "CreateTime"
                            });
                            #endregion
                            var userInfo = Utils.GetCurrentUserInfo();
                            //往数据库增加配件返利申请单
                            foreach(var item in rightList) {
                                #region 添加Sql的参数
                                var command = db.CreateDbCommand(sqlInsert, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("Code", CodeGenerator.Generate(db, "PartsRebateApplication")));
                                command.Parameters.Add(db.CreateDbParameter("CustomerCompanyId", item.CustomerCompanyId));
                                command.Parameters.Add(db.CreateDbParameter("CustomerCompanyCode", item.CustomerCompanyCode));
                                command.Parameters.Add(db.CreateDbParameter("CustomerCompanyName", item.CustomerCompanyName));
                                command.Parameters.Add(db.CreateDbParameter("BranchId", branchId));
                                command.Parameters.Add(db.CreateDbParameter("PartsSalesCategoryId", item.PartsSalesCategoryId));
                                command.Parameters.Add(db.CreateDbParameter("PartsRebateTypeId", item.PartsRebateTypeId));
                                command.Parameters.Add(db.CreateDbParameter("AccountGroupId", item.AccountGroupId));
                                command.Parameters.Add(db.CreateDbParameter("RebateDirection", item.RebateDirection));
                                command.Parameters.Add(db.CreateDbParameter("Amount", item.Amount));
                                command.Parameters.Add(db.CreateDbParameter("Motive", item.Motive));
                                command.Parameters.Add(db.CreateDbParameter("Status", (int)DcsWorkflowOfSimpleApprovalStatus.新建));
                                command.Parameters.Add(db.CreateDbParameter("Remark", item.Remark));
                                command.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                command.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                command.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                command.ExecuteNonQuery();
                                #endregion
                            }
                        }
                        //无异常提交
                        ts.Commit();
                    } catch(Exception) {
                        //报错回滚
                        ts.Rollback();
                        throw;
                    } finally {
                        if(conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }

    }
}
