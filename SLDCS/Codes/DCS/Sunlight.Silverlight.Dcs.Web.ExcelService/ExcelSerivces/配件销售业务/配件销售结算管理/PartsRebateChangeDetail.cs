﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Globalization;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        /// <summary>
        /// 导出配件返利变更明细
        /// </summary>
        public bool ExportPartsRebateChangeDetail(int[] ids, int? branchId, int? accountGroupId, string companyName, string companyCode, string sourceCode, int? sourceType, string accountGroupName, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName) {
            fileName = GetExportFilePath("配件返利变更明细_" + ".xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {
                    var sql = new StringBuilder();
                    sql.Append(@"Select d.Name as BranchName,
                                        c.Code as CompanyCode,
                                        c.Name as CompanyName,
                                        a.Sourcecode,
                                        (select value from keyvalueitem where NAME = 'PartsRebateChangeDetail_SourceType'and key=a.Sourcetype) As Sourcetype,
                                        b.Name as AccountgroupName,
                                        a.Amount,
                                        a.Creatorname,
                                        a.Createtime
                                  From Partsrebatechangedetail a
                                  Left Join Accountgroup b
                                    On a.Accountgroupid = b.Id
                                  Left Join Company c
                                    On a.Customercompanyid = c.Id
                                  Left Join Branch d
                                    On a.Branchid = d.Id where 1=1 ");
                    var dbParameters = new List<DbParameter>();
                    #region 条件过滤
                    if(ids != null && ids.Length > 0) {
                        sql.Append(" and a.id in (");
                        for(var i = 0; i < ids.Length; i++) {
                            if(ids.Length == i + 1) {
                                sql.Append("{0}" + ids[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter(ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            } else {
                                sql.Append("{0}" + ids[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter(ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                        }
                        sql.Append(")");
                    } else {
                        if(branchId.HasValue) {
                            sql.Append(@" and a.branchId = {0}branchId ");
                            dbParameters.Add(db.CreateDbParameter("branchId", branchId.Value));
                        }
                        if(accountGroupId.HasValue) {
                            sql.Append(@" and a.accountGroupId = {0}accountGroupId ");
                            dbParameters.Add(db.CreateDbParameter("accountGroupId", accountGroupId.Value));
                        }
                        if(!String.IsNullOrEmpty(companyCode)) {
                            sql.Append(@" and Upper(c.Code) like Upper({0}companyCode) ");
                            dbParameters.Add(db.CreateDbParameter("companyCode", "%" + companyCode + "%"));
                        }
                        if(!String.IsNullOrEmpty(companyName)) {
                            sql.Append(@" and c.Name like {0}companyName ");
                            dbParameters.Add(db.CreateDbParameter("companyName", "%" + companyName + "%"));
                        }
                        if(!String.IsNullOrEmpty(sourceCode)) {
                            sql.Append(@" and Upper(a.sourceCode) like Upper({0}sourceCode) ");
                            dbParameters.Add(db.CreateDbParameter("sourceCode", "%" + sourceCode + "%"));
                        }
                        if(sourceType.HasValue) {
                            sql.Append(@" and a.sourceType = {0}sourceType ");
                            dbParameters.Add(db.CreateDbParameter("sourceType", sourceType.Value));
                        }
                        if(!String.IsNullOrEmpty(accountGroupName)) {
                            sql.Append(@" and b.Name like {0}accountGroupName ");
                            dbParameters.Add(db.CreateDbParameter("accountGroupName", "%" + accountGroupName + "%"));
                        }
                        if(createTimeBegin.HasValue) {
                            sql.Append(@" and a.createTime >=to_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(@" and a.createTime <=to_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                    }
                    #endregion
                    var sqlStr = sql.ToString();
                    if(sqlStr.IndexOf(" where 1=1  and ") > 0)
                        sqlStr = sqlStr.Replace(" where 1=1  and ", " where ");
                    else
                        sqlStr = sqlStr.Replace(" where 1=1 ", "");
                    var command = db.CreateDbCommand(string.Format(sqlStr, db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    conn.Open();
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                ErrorStrings.Export_Title_Dealerserviceinfo_BranchName,ErrorStrings.Export_Title_Credenceapplication_CompanyCode,ErrorStrings.Export_Title_Credenceapplication_CompanyName,ErrorStrings.Export_Title_PartsOuterPurchaseChange_SourceCode,ErrorStrings.Export_Title_PartsOuterPurchaseChange_SourceCategoryr,ErrorStrings.Export_Title_Credenceapplication_AccountGroupNameNew,ErrorStrings.Export_Title_Customertransferbill_Amount,ErrorStrings.Export_Title_AccountPeriod_CreatorName,ErrorStrings.Export_Title_AccountPeriod_CreateTime
                                };
                            }
                            if(reader.Read()) {
                                //var values = new object[reader.FieldCount];
                                //var num = reader.GetValues(values);
                                //if (num != reader.FieldCount) {
                                //    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                //}
                                var values = new object[] {
                                    reader["BranchName"] is DBNull? "":reader["BranchName"].ToString(),
                                    reader["CompanyCode"] is DBNull? "":reader["CompanyCode"].ToString(),
                                    reader["CompanyName"] is DBNull? "":reader["CompanyName"].ToString(),
                                    reader["Sourcecode"] is DBNull? "":reader["Sourcecode"].ToString(),
                                    reader["Sourcetype"] is DBNull? "":reader["Sourcetype"].ToString(),
                                    reader["AccountgroupName"] is DBNull? "":reader["AccountgroupName"].ToString(),
                                    new StyleDataField(reader["Amount"] is DBNull? 0m : Convert.ToDecimal(reader["Amount"]),"0.00"),
                                    reader["Creatorname"] is DBNull? "":reader["Creatorname"].ToString(),
                                    Convert.ToDateTime(reader["Createtime"] is DBNull? "":reader["Createtime"].ToString())
                                };
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }

        /// <summary>
        /// 导出配件返利变更明细(虚拟实体)
        /// </summary>
        public bool ExportVirtualPartsRebateChangeDetail(int[] ids, int? branchId, int? accountGroupId, string companyName, string companyCode, string sourceCode, int? sourceType, DateTime? createTimeBegin, DateTime? createTimeEnd, string businessCode, out string fileName) {
            fileName = GetExportFilePath("配件返利变更明细_" + ".xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {
                    var sql = new StringBuilder();
                    sql.Append(@"Select d.Name as BranchName,
                                        c.Code as CompanyCode,
                                        c.Name as CompanyName,
                                        a.Sourcecode,
                                        (select value from keyvalueitem where NAME = 'PartsRebateChangeDetail_SourceType'and key=a.Sourcetype) As Sourcetype,
                                        b.Name as AccountgroupName,
                                        a.Amount,
                                        a.Creatorname,
                                        a.Createtime,
                                        case c.type
                                                when 2 then
                                                s.businessCode
                                                when 7 then
                                                 s.businessCode
                                                when 3 then
                                                u.businessCode
                                                end
                                  From Partsrebatechangedetail a
                                  Left Join Accountgroup b
                                    On a.Accountgroupid = b.Id
                                  Left Join Company c
                                    On a.Customercompanyid = c.Id
                                  Left Join Branch d
                                    On a.Branchid = d.Id
                                  Left Join SalesUnit u
                                  On a.AccountGroupId = u.AccountGroupId and b.SalesCompanyId = u.OwnerCompanyId
                                  Left Join DealerServiceInfo s
                                  On u.PartsSalesCategoryId = s.PartsSalesCategoryId and a.CustomerCompanyId = s.DealerId and s.status=1 where c.status = 1 ");
                    var dbParameters = new List<DbParameter>();
                    #region 条件过滤
                    if(ids != null && ids.Length > 0) {
                        sql.Append(" and a.id in (");
                        for(var i = 0; i < ids.Length; i++) {
                            if(ids.Length == i + 1) {
                                sql.Append("{0}" + ids[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter(ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            } else {
                                sql.Append("{0}" + ids[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter(ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                        }
                        sql.Append(")");
                    } else {
                        if(branchId.HasValue) {
                            sql.Append(@" and a.branchId = {0}branchId ");
                            dbParameters.Add(db.CreateDbParameter("branchId", branchId.Value));
                        }
                        if(accountGroupId.HasValue) {
                            sql.Append(@" and a.accountGroupId = {0}accountGroupId ");
                            dbParameters.Add(db.CreateDbParameter("accountGroupId", accountGroupId.Value));
                        }
                        if(!String.IsNullOrEmpty(companyCode)) {
                            sql.Append(@" and Upper(c.Code) like Upper({0}companyCode) ");
                            dbParameters.Add(db.CreateDbParameter("companyCode", "%" + companyCode + "%"));
                        }
                        if(!String.IsNullOrEmpty(companyName)) {
                            sql.Append(@" and c.Name like {0}companyName ");
                            dbParameters.Add(db.CreateDbParameter("companyName", "%" + companyName + "%"));
                        }
                        if(!String.IsNullOrEmpty(sourceCode)) {
                            sql.Append(@" and Upper(a.sourceCode) like Upper({0}sourceCode) ");
                            dbParameters.Add(db.CreateDbParameter("sourceCode", "%" + sourceCode + "%"));
                        }
                        if(sourceType.HasValue) {
                            sql.Append(@" and a.sourceType = {0}sourceType ");
                            dbParameters.Add(db.CreateDbParameter("sourceType", sourceType.Value));
                        }
                        //if(!String.IsNullOrEmpty(accountGroupName)) {
                        //    sql.Append(@" and b.Name like {0}accountGroupName ");
                        //    dbParameters.Add(db.CreateDbParameter("accountGroupName", "%" + accountGroupName + "%"));
                        //}
                        if(createTimeBegin.HasValue) {
                            sql.Append(@" and a.createTime >=to_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(@" and a.createTime <=to_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                        if(!string.IsNullOrEmpty(businessCode)) {
                            sql.Append(" and s.businessCode like {0}businessCode and u.businessCode like {0}businessCode ");
                            dbParameters.Add(db.CreateDbParameter("businessCode", "%" + businessCode + "%"));
                        }
                    }
                    #endregion

                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    conn.Open();
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                ErrorStrings.Export_Title_Dealerserviceinfo_BranchName,ErrorStrings.Export_Title_Credenceapplication_CompanyCode,ErrorStrings.Export_Title_Credenceapplication_CompanyName,ErrorStrings.Export_Title_PartsOuterPurchaseChange_SourceCode,ErrorStrings.Export_Title_PartsOuterPurchaseChange_SourceCategoryr,ErrorStrings.Export_Title_Credenceapplication_AccountGroupNameNew,ErrorStrings.Export_Title_Customertransferbill_Amount,ErrorStrings.Export_Title_AccountPeriod_CreatorName,ErrorStrings.Export_Title_AccountPeriod_CreateTime,ErrorStrings.Export_Title_Credenceapplication_BusinessCode
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }
    }
}
