﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Globalization;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {

        /// <summary>
        /// 合并导出配件销售结算主清单
        /// </summary>
        public bool ExportPartsSalesRtnSettlementWithDetails(int[] billIds, string code, string customerCompanyCode, int? partsSalesCategoryId, string customerCompanyName, int? accountGroupId, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, string creatorName, DateTime? invoiceDateBegin, DateTime? invoiceDateEnd, int? settleType, int? businessType, out string fileName)
        {
            fileName = GetExportFilePath("配件销售退货结算主清单_" + ".xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var userinfo = Utils.GetCurrentUserInfo();
                var salesCompanyId = userinfo.EnterpriseId;
                var personId = userinfo.Id;
                var db = DbHelper.GetDbHelp(ConnectionString);
                using(var conn = db.CreateDbConnection()) {
                    var sql = new StringBuilder();
                    #region SQL
                    sql.AppendFormat(@"
                                        select 
                                        a.Code,/*结算单编号*/
                                        a.SalesCompanyName,/*销售企业*/
                                        a.PartsSalesCategoryName,/*品牌*/
                                        a.AccountGroupName,/*客户账户*/
                                        a.CustomerCompanyCode,/*客户企业编号*/
                                        a.CustomerCompanyName,/*客户企业名称*/
                                        a.TotalSettlementAmount,/*结算总金额*/
                                        e.plannedPriceSum, /*成本价合计*/
                                        a.Discount,/*折扣金额*/
                                        a.TaxRate,/*税率*/
                                        a.Tax,/*税额*/
                                        a.InvoiceAmountDifference,/*开票差异金额*/
                                        (select value from keyvalueitem where NAME = 'PartsSalesRtnSettlement_Status'and key=a.Status) As Status,/*状态*/
                                        (select value from keyvalueitem where NAME = 'PartsPurchaseRtnSettleBill_InvoicePath'and key=a.InvoicePath) As InvoicePath,/*开票方向*/
                                        (select value from keyvalueitem where NAME = 'PartsPurchaseSettleBill_SettlementPath'and key=a.SettlementPath) As SettlementPath,/*结算方向*/
                                        a.OffsettedSettlementBillCode, /*被反冲结算单编号*/
                                        a.Remark,/*备注*/
                                        a.CreatorName,/*创建人*/
                                        a.CreateTime,/*创建时间*/
                                        b.sourcecode,/*入库检验单编号*/
                                        c.warehousename,/*仓库名称*/
                                        (select value from keyvalueitem where NAME = 'Parts_InboundType'and key=c.inboundtype) As inboundtype,/*入库类型*/
                                        d.sparepartcode,/*配件编号*/
                                        d.sparepartname,/*配件名称*/
                                        d.inspectedquantity,/*检验量*/
                                        d.settlementprice,/*结算价*/
                                        (select measureunit from sparepart where id=d.sparepartid) as measureunit,/*计量单位*/
                                        (d.settlementprice*d.inspectedquantity) as settlementamout, /*结算金额*/  
                                        d.remark,/*清单备注*/
                                        a.InvoiceDate,  /*过账日期*/
                                        (select value from keyvalueitem where NAME = 'SalesOrderType_SettleType'and key=a.SettleType) As SettleType,/*结算类型*/
                                        (select value from keyvalueitem where NAME = 'SalesOrderType_BusinessType'and key=a.BusinessType) As BusinessType/*业务类型*/
                                        FROM PartsSalesRtnSettlement a
                                        inner join partssalesrtnsettlementref b on a.id=b.partssalesrtnsettlementid
                                        inner join partsinboundcheckbill c on b.sourceid=c.id
                                        inner join partsinboundcheckbilldetail d on c.id=d.partsinboundcheckbillid

                                        left join (select partssalesrtnsettlementrefid, sum(amount) as plannedPriceSum
                                             from (select partssalesrtnsettlementref.id as partssalesrtnsettlementrefid,
                                                  ((partsinboundcheckbilldetail.costprice) *
                                                  (partsinboundcheckbilldetail.inspectedquantity )) as amount
                                          from partssalesrtnsettlementref
                                            inner join partsinboundcheckbilldetail
                                            on partsinboundcheckbilldetail.partsinboundcheckbillid =
                                            partssalesrtnsettlementref.sourceid)
                                            group by partssalesrtnsettlementrefid) e
                                      on b.id = e.partssalesrtnsettlementrefid                                        

                                        WHERE a.salesCompanyId = {1}
                                             and (Exists (Select 1
                                                    From Personsalescenterlink t
                                                   Where t.Partssalescategoryid = a.Partssalescategoryid
                                                     And t.Personid = {0}
                                                     And (Select Type
                                                            From Company
                                                           Where Id = {1}
                                                             And Status = {2}) = {3})
                                              or (Select Type From Company  Where Id = {1} And Status = 1) <> 1 )", personId, salesCompanyId, (int)DcsMasterDataStatus.有效, (int)DcsCompanyType.分公司);
                    #endregion
                    var tableNames = sql.ToString().GetRegexNames();
                    var dbParameters = new List<DbParameter>();
                    #region 条件过滤
                    if(billIds != null && billIds.Length > 0) {
                        sql.Append(" and a.id in (");
                        for(var i = 0; i < billIds.Length; i++) {
                            if(billIds.Length == i + 1) {
                                sql.Append("{0}id" + billIds[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter("id" + billIds[i].ToString(CultureInfo.InvariantCulture), billIds[i]));
                            } else {
                                sql.Append("{0}id" + billIds[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter("id" + billIds[i].ToString(CultureInfo.InvariantCulture), billIds[i]));
                            }
                        }
                        sql.Append(")");
                    } else {
                        if(!string.IsNullOrEmpty(code)) {
                            sql.Append(" and Upper(a.code) like Upper({0}code) ");
                            dbParameters.Add(db.CreateDbParameter("code", "%" + code + "%"));
                        }
                        if(!string.IsNullOrEmpty(customerCompanyCode)) {
                            sql.Append(" and Upper(a.customerCompanyCode) like Upper({0}customerCompanyCode) ");
                            dbParameters.Add(db.CreateDbParameter("customerCompanyCode", "%" + customerCompanyCode + "%"));
                        }
                        if(!string.IsNullOrEmpty(customerCompanyName)) {
                            sql.Append(" and a.customerCompanyName like {0}customerCompanyName ");
                            dbParameters.Add(db.CreateDbParameter("customerCompanyName", "%" + customerCompanyName + "%"));
                        }
                        if(partsSalesCategoryId.HasValue) {
                            sql.Append(" and a.partsSalesCategoryId={0}partsSalesCategoryId");
                            dbParameters.Add(db.CreateDbParameter("partsSalesCategoryId", partsSalesCategoryId.Value));
                        }
                        if(accountGroupId.HasValue) {
                            sql.Append(" and a.accountGroupId={0}accountGroupId");
                            dbParameters.Add(db.CreateDbParameter("accountGroupId", accountGroupId.Value));
                        }
                        if(status.HasValue) {
                            sql.Append(" and a.status={0}status");
                            dbParameters.Add(db.CreateDbParameter("status", status.Value));
                        }
                        if(!string.IsNullOrWhiteSpace(creatorName)) {
                            sql.Append(" and upper(a.creatorName) like upper({0}creatorName)");
                            dbParameters.Add(db.CreateDbParameter("creatorName", "%" + creatorName + "%"));
                        }
                        if(createTimeBegin.HasValue) {
                            sql.Append(" and a.createTime>=to_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(" and a.createTime<=to_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                        if (invoiceDateBegin.HasValue)
                        {
                            sql.Append(" and a.invoiceDate>=to_date({0}invoiceDateBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = invoiceDateBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("invoiceDateBegin", tempTime.ToString("G")));
                        }
                        if (invoiceDateEnd.HasValue)
                        {
                            sql.Append(" and a.invoiceDate<=to_date({0}invoiceDateEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = invoiceDateEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("invoiceDateEnd", tempTime.ToString("G")));
                        }
                        if(settleType.HasValue) {
                            sql.Append(" and a.settleType={0}settleType");
                            dbParameters.Add(db.CreateDbParameter("settleType", settleType.Value));
                        }
                        if(businessType.HasValue) {
                            sql.Append(" and a.businessType={0}businessType");
                            dbParameters.Add(db.CreateDbParameter("businessType", businessType.Value));
                        }
                    }
                    #endregion
                    sql.Append(" order by a.code");
                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    conn.Open();
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return tableNames;
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }


        /// <summary>
        /// 合并导出配件销售结算主清单 销售退货结算查询使用
        /// </summary>
        public bool ExportPartsSalesRtnSettlementQueryWithDetails(int[] billIds, int customerCompanyId, string code, string customerCompanyCode, int? partsSalesCategoryId, string customerCompanyName, int? accountGroupId, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName) {
            fileName = GetExportFilePath("配件销售结算主清单_" + ".xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {
                    var sql = new StringBuilder();
                    #region SQL
                    sql.AppendFormat(@" select 
                                        a.Code,/*结算单编号*/
                                        a.SalesCompanyName,/*销售企业*/
                                        a.PartsSalesCategoryName,/*品牌*/
                                        a.AccountGroupName,/*客户账户*/
                                        a.CustomerCompanyCode,/*客户企业编号*/
                                        a.CustomerCompanyName,/*客户企业名称*/
                                        a.TotalSettlementAmount,/*结算总金额*/
                                        a.Discount,/*折扣金额*/
                                        a.TaxRate,/*税率*/
                                        a.Tax,/*税额*/
                                        a.InvoiceAmountDifference,/*开票差异金额*/
                                        (select value from keyvalueitem where NAME = 'PartsSalesRtnSettlement_Status'and key=a.Status) As Status,/*状态*/
                                        (select value from keyvalueitem where NAME = 'PartsPurchaseRtnSettleBill_InvoicePath'and key=a.InvoicePath) As InvoicePath,/*开票方向*/
                                        (select value from keyvalueitem where NAME = 'PartsPurchaseSettleBill_SettlementPath'and key=a.SettlementPath) As SettlementPath,/*结算方向*/
                                        a.OffsettedSettlementBillCode, /*被反冲结算单编号*/
                                        a.Remark,/*备注*/
                                        a.CreatorName,/*创建人*/
                                        a.CreateTime,/*创建时间*/
                                        b.sourcecode,/*入库检验单编号*/
                                        c.warehousename,/*仓库名称*/
                                        (select value from keyvalueitem where NAME = 'Parts_InboundType'and key=c.inboundtype) As inboundtype,/*入库类型*/
                                        d.sparepartcode,/*配件编号*/
                                        d.sparepartname,/*配件名称*/
                                        d.inspectedquantity,/*检验量*/
                                        d.settlementprice,/*结算价*/
                                        (select measureunit from sparepart where id=d.sparepartid) as measureunit,/*计量单位*/
                                        (d.settlementprice*d.inspectedquantity) as settlementamout, /*结算金额*/  
                                        d.remark/*清单备注*/
                                        FROM PartsSalesRtnSettlement a
                                        inner join partssalesrtnsettlementref b on a.id=b.partssalesrtnsettlementid
                                        inner join partsinboundcheckbill c on b.sourceid=c.id
                                        inner join partsinboundcheckbilldetail d on c.id=d.partsinboundcheckbillid
                                       WHERE a.CustomerCompanyId = {0} ", customerCompanyId);
                    #endregion
                    var dbParameters = new List<DbParameter>();
                    #region 条件过滤
                    if(billIds != null && billIds.Length > 0) {
                        sql.Append(" and a.id in (");
                        for(var i = 0; i < billIds.Length; i++) {
                            if(billIds.Length == i + 1) {
                                sql.Append("{0}id" + billIds[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter("id" + billIds[i].ToString(CultureInfo.InvariantCulture), billIds[i]));
                            } else {
                                sql.Append("{0}id" + billIds[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter("id" + billIds[i].ToString(CultureInfo.InvariantCulture), billIds[i]));
                            }
                        }
                        sql.Append(")");
                    } else {
                        if(!string.IsNullOrEmpty(code)) {
                            sql.Append(" and Upper(a.code) like Upper({0}code) ");
                            dbParameters.Add(db.CreateDbParameter("code", "%" + code + "%"));
                        }
                        if(!string.IsNullOrEmpty(customerCompanyCode)) {
                            sql.Append(" and Upper(a.customerCompanyCode) like Upper({0}customerCompanyCode) ");
                            dbParameters.Add(db.CreateDbParameter("customerCompanyCode", "%" + customerCompanyCode + "%"));
                        }
                        if(!string.IsNullOrEmpty(customerCompanyName)) {
                            sql.Append(" and a.customerCompanyName like {0}customerCompanyName ");
                            dbParameters.Add(db.CreateDbParameter("customerCompanyName", "%" + customerCompanyName + "%"));
                        }
                        if(partsSalesCategoryId.HasValue) {
                            sql.Append(" and a.partsSalesCategoryId={0}partsSalesCategoryId");
                            dbParameters.Add(db.CreateDbParameter("partsSalesCategoryId", partsSalesCategoryId.Value));
                        }
                        if(accountGroupId.HasValue) {
                            sql.Append(" and a.accountGroupId={0}accountGroupId");
                            dbParameters.Add(db.CreateDbParameter("accountGroupId", accountGroupId.Value));
                        }
                        if(status.HasValue) {
                            sql.Append(" and a.status={0}status");
                            dbParameters.Add(db.CreateDbParameter("status", status.Value));
                        }
                        if(createTimeBegin.HasValue) {
                            sql.Append(" and a.createTime>=to_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(" and a.createTime<=to_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                    }
                    #endregion
                    sql.Append(" order by a.code");
                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    conn.Open();
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new[] { ErrorStrings.Export_Title_PartsPurchaseRtnSettleBill_Code, ErrorStrings.Export_Titile_PartsSalesRtnSettlement_SalesCompanyName, ErrorStrings.Export_Title_Partssalescategory_Name, ErrorStrings.Export_Titile_PartsSalesRtnSettlement_AccountGroupName, ErrorStrings.Export_Title_Credenceapplication_CompanyCode, ErrorStrings.Export_Title_Credenceapplication_CompanyName, ErrorStrings.Export_Title_PartsPurchaseRtnSettleBill_TotalSettlementAmount, ErrorStrings.Export_Titile_PartsSalesRtnSettlement_Discount, ErrorStrings.Export_Title_PartsPurchaseRtnSettleBill_TaxRate, ErrorStrings.Export_Title_PartsPurchaseRtnSettleBill_Tax, ErrorStrings.Export_Titile_PartsSalesRtnSettlement_InvoiceAmountDifference, ErrorStrings.Export_Title_AccountPeriod_Status, ErrorStrings.Export_Title_PartsPurchaseRtnSettleBill_InvoicePath, ErrorStrings.Export_Title_PartsPurchaseRtnSettleBill_SettlementPath, ErrorStrings.Export_Titile_PartsSalesRtnSettlement_OffsettedSettlementBillCode, ErrorStrings.Export_Title_PartsBranch_Remark, ErrorStrings.Export_Title_AccountPeriod_CreatorName, ErrorStrings.Export_Title_AccountPeriod_CreateTime, ErrorStrings.Export_Title_PartsInboundCheckBill_Code, ErrorStrings.Export_Title_Company_WarehouseName, ErrorStrings.Export_Title_PackingTask_InboundType, ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode, ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName, ErrorStrings.Export_Title_PartsInboundCheckBill_InspectedQuantity, ErrorStrings.Export_Title_PartsPurchaseRtnSettleBill_SettlementPrice, ErrorStrings.Export_Title_PackingPropertyApp_MeasureUnit, ErrorStrings.Export_Title_PartsPurchaseRtnSettleBill_SettlementAmount, ErrorStrings.Export_Title_PartsPurchaseOrder_DetailRemark };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }

        /// <summary>
        /// 根据关联单导出配件销售退货结算单清单
        /// </summary>
        public bool ExportPartsSalesRtnSettlementDetailByRefId(int[] inboundIds, out string fileName) {
            fileName = GetExportFilePath("配件销售退货结算单清单_.xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.Append(@"Select a.Sparepartcode,
                                       a.Sparepartname,
                                       a.Settlementprice,
                                       a.InspectedQuantity,
                                       (a.Settlementprice*a.InspectedQuantity)
                                  From PartsInboundCheckBillDetail a
                                  Join PartsInboundCheckBill b
                                    On a.partsinboundcheckbillid = b.Id ");
                    var dbParameters = new List<DbParameter>();
                    sql.Append(" where b.id in (");
                    for(var i = 0; i < inboundIds.Length; i++) {
                        if(inboundIds.Length == i + 1) {
                            sql.Append("{0}id" + inboundIds[i].ToString(CultureInfo.InvariantCulture));
                            dbParameters.Add(db.CreateDbParameter("id" + inboundIds[i].ToString(CultureInfo.InvariantCulture), inboundIds[i]));
                        } else {
                            sql.Append("{0}id" + inboundIds[i].ToString(CultureInfo.InvariantCulture) + ",");
                            dbParameters.Add(db.CreateDbParameter("id" + inboundIds[i].ToString(CultureInfo.InvariantCulture), inboundIds[i]));
                        }
                    }
                    sql.Append(")");
                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode, ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName, ErrorStrings.Export_Title_PartsPurchaseRtnSettleBill_SettlementPrice,ErrorStrings.Export_Title_PartsPurchaseRtnSettleBill_QuantityToSettle,ErrorStrings.Export_Title_PartsPurchaseRtnSettleBill_SettlementAmount
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }
    }
}