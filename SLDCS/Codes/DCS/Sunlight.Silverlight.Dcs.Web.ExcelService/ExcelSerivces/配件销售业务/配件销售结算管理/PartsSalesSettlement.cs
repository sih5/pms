﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        /// <summary>
        /// 合并导出配件销售结算主清单
        /// </summary>
        public bool ExportPartsSalesSettlementWithDetails(int? billId, int? personId, int salesCompanyId, string code, string customerCompanyCode, int? partsSalesCategoryId, string customerCompanyName, int? accountGroupId, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, string creatorName, DateTime? invoiceDateBegin, DateTime? invoiceDateEnd, int? settleType, int? businessType, out string fileName)
        {
            fileName = GetExportFilePath("配件销售结算主清单_" + ".xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {
                    var sql = new StringBuilder();
                    #region SQL
                    sql.AppendFormat(@"
                                    Select a.Code, /*结算单编号*/
                                           a.Salescompanyname, /*销售企业*/
                                           a.Partssalescategoryname, /*品牌*/
                                           a.Accountgroupname, /*客户账号*/
                                           a.Customercompanycode, /*客户企业编号*/
                                           a.Customercompanyname, /*客户企业名称*/
                                           a.Totalsettlementamount, /*结算总金额*/
                                           a.Rebateamount, /*折让不含税金额*/
                                           a.Taxrate, /*税率*/
                                           a.Tax, /*税额*/
                                           decode(b.sourcetype, 1, c.costprice, 5, d.costprice),/*成本价*/
                                           decode(b.sourcetype, 1, nvl(c.costprice,0)*nvl(c.outboundamount,0), 5, nvl(d.costprice,0)*Nvl(d.inspectedquantity,0)),/*成本金额*/
                                           e.plannedPriceSumOfRef,/*成本合计*/
                                          (select value from keyvalueitem where NAME = 'PartsSalesSettlement_Status'and key=a.Status) As Status,/*状态*/
                                          (select value from keyvalueitem where NAME = 'PartsPurchaseSettleBill_SettlementPath'and key=a.Settlementpath) As Settlementpath, /*结算方向*/
                                           a.Offsettedsettlementbillcode, /*被反冲结算单编号*/
                                           a.Remark, /*备注*/
                                          (select value from keyvalueitem where NAME = 'PartsAttribution'and key=pb.PartsAttribution) As PartsAttribution,/*配件所属分类*/
                                           a.Creatorname, /*创建人*/
                                           a.Createtime, /*创建时间*/
                                           a.Modifiername, /*修改人*/
                                           a.Modifytime, /*修改时间*/
                                           a.Approvername, /*审批人*/
                                           a.Approvetime, /*审批时间*/
                                           a.Invoiceregistrationoperator, /*发票登记人*/
                                           a.Invoiceregistrationtime, /*发票登记时间*/
                                           a.Abandonername, /*作废人*/
                                           a.Abandontime, /*作废时间*/
                                           b.SourceCode, /*关联单编号*/
                                          (select value from keyvalueitem where NAME = 'PartsSalesSettlementRef_SourceType'and key=b.SourceType) As SourceType,/*单据类型*/
                                           b.WarehouseName, /*仓库名称*/
                                           st.InvoiceCode, /*发票号*/
                                           decode(b.sourcetype, 1, c.sparepartcode, 5, d.sparepartcode), /*配件编号*/
                                           decode(b.sourcetype, 1, c.Sparepartname, 5, d.Sparepartname), /*配件名称*/
                                           decode(b.sourcetype, 1, (select OverseasPartsFigure from Sparepart where Id = c.SparepartId), 5, (select OverseasPartsFigure from Sparepart where Id = d.SparepartId)), /*海外配件图号*/
                                           decode(b.sourcetype, 1, c.Settlementprice, 5, d.Settlementprice), /*结算价*/
                                           decode(b.sourcetype, 1, c.OutboundAmount, 5, -d.InspectedQuantity), /*结算数量*/
                                           decode(b.sourcetype,
                                                  1,
                                                  nvl(c.Settlementprice, 0) * nvl(c.OutboundAmount, 0),
                                                  5,
                                                  -nvl(d.Settlementprice, 0) * nvl(d.InspectedQuantity, 0)), /*结算金额*/
                                             a.InvoiceDate , /*过账日期*/
                                           (select value from keyvalueitem where NAME = 'SalesOrderType_SettleType'and key=a.SettleType) As SourceType, /*结算类型*/
                                           (select value from keyvalueitem where NAME = 'SalesOrderType_BusinessType'and key=a.BusinessType) As SourceType  /*业务类型*/
                                      From Partssalessettlement a
                                      Left Join PartsSalesSettlementRef b
                                        On a.Id = b.PartsSalesSettlementId
                                      left join PartsOutboundBillDetail c
                                        on b.SourceId = c.PartsOutboundBillId
                                       and b.sourcetype = 1
                                      left join PartsInboundCheckBillDetail d
                                        on b.SourceId = d.PartsInboundCheckBillId
                                       and b.sourcetype = 5
                                      left join SalesInvoice_tmp st on a.code=st.vouchercode
                                      left join partsbranch pb
                                      on a.PartsSalesCategoryId=pb.PartsSalesCategoryId And pb.PartId= decode(b.sourcetype, 1, c.sparepartid, 5, d.sparepartid) And pb.Status = 1
                                      left join (select partssalessettlementrefId,
                                                        sum(amount) as plannedPriceSumOfRef
                                                   from (select partssalessettlementref.id as partssalessettlementrefId,
                                                                (partsoutboundbilldetail.costprice *
                                                                partsoutboundbilldetail.outboundamount) as amount
                                                           from partssalessettlementref
                                                          inner join partsoutboundbill
                                                             on partssalessettlementref.sourceid =
                                                                partsoutboundbill.id
                                                            and partssalessettlementref.sourcetype = 1
                                                          inner join partsoutboundbilldetail
                                                             on partsoutboundbill.id =
                                                                partsoutboundbilldetail.partsoutboundbillid
                                                         union all
                                                         select partssalessettlementref.id as partssalessettlementrefId,
                                                                ((-1) *
                                                                (partsinboundcheckbilldetail.costprice *
                                                                partsinboundcheckbilldetail.inspectedquantity)) as amount
                                                           from partssalessettlementref
                                                          inner join partsinboundcheckbill
                                                             on partssalessettlementref.sourceid =
                                                                partsinboundcheckbill.id
                                                            and partssalessettlementref.sourcetype = 5
                                                          inner join partsinboundcheckbilldetail
                                                             on partsinboundcheckbill.id =
                                                                partsinboundcheckbilldetail.partsinboundcheckbillid)
                                                  group by partssalessettlementrefId) e
                                        on b.id = e.partssalessettlementrefId
                                     where a.salesCompanyId = {1}
                                       and (Exists (Select 1
                                              From Personsalescenterlink t
                                             Where t.Partssalescategoryid = a.Partssalescategoryid
                                               And t.Personid = {0}
                                               And (Select Type
                                                      From Company
                                                     Where Id = {1}
                                                       And Status = {2}) = {3})
                                        or (Select Type From Company  Where Id = {1} And Status = 1) <> 1  )
                                       and ((a.IsUnifiedSettle is null) or (a.IsUnifiedSettle = 0))", personId, salesCompanyId, (int)DcsMasterDataStatus.有效, (int)DcsCompanyType.分公司);
                    #endregion
                    var tableNames = sql.ToString().GetRegexNames();
                    var dbParameters = new List<DbParameter>();
                    #region 条件过滤


                    if(billId.HasValue) {
                        sql.Append(" and a.Id={0}billId");
                        dbParameters.Add(db.CreateDbParameter("billId", billId.Value));
                    } else {
                        if(!string.IsNullOrEmpty(code)) {
                            sql.Append(" and Upper(a.code) like Upper({0}code) ");
                            dbParameters.Add(db.CreateDbParameter("code", "%" + code + "%"));
                        }
                        if(!string.IsNullOrEmpty(customerCompanyCode)) {
                            sql.Append(" and Upper(a.customerCompanyCode) like Upper({0}customerCompanyCode) ");
                            dbParameters.Add(db.CreateDbParameter("customerCompanyCode", "%" + customerCompanyCode + "%"));
                        }
                        if(!string.IsNullOrEmpty(customerCompanyName)) {
                            sql.Append(" and a.customerCompanyName like {0}customerCompanyName ");
                            dbParameters.Add(db.CreateDbParameter("customerCompanyName", "%" + customerCompanyName + "%"));
                        }
                        if(partsSalesCategoryId.HasValue) {
                            sql.Append(" and a.partsSalesCategoryId={0}partsSalesCategoryId");
                            dbParameters.Add(db.CreateDbParameter("partsSalesCategoryId", partsSalesCategoryId.Value));
                        }
                        if(accountGroupId.HasValue) {
                            sql.Append(" and a.accountGroupId={0}accountGroupId");
                            dbParameters.Add(db.CreateDbParameter("accountGroupId", accountGroupId.Value));
                        }
                        if(status.HasValue) {
                            sql.Append(" and a.status={0}status");
                            dbParameters.Add(db.CreateDbParameter("status", status.Value));
                        }
                        if(!string.IsNullOrWhiteSpace(creatorName)) {
                            sql.Append(" and upper(a.creatorName) like upper({0}creatorName)");
                            dbParameters.Add(db.CreateDbParameter("creatorName", "%" + creatorName + "%"));
                        }
                        if(createTimeBegin.HasValue) {
                            sql.Append(" and a.createTime>=to_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(" and a.createTime<=to_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                        if (invoiceDateBegin.HasValue)
                        {
                            sql.Append(" and a.invoiceDate>=to_date({0}invoiceDateBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = invoiceDateBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("invoiceDateBegin", tempTime.ToString("G")));
                        }
                        if (invoiceDateEnd.HasValue)
                        {
                            sql.Append(" and a.invoiceDate<=to_date({0}invoiceDateEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = invoiceDateEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("invoiceDateEnd", tempTime.ToString("G")));
                        }
                        if(settleType.HasValue) {
                            sql.Append(" and a.SettleType={0}settleType");
                            dbParameters.Add(db.CreateDbParameter("settleType", status.Value));
                        }
                        if(businessType.HasValue) {
                            sql.Append(" and a.BusinessType={0}businessType");
                            dbParameters.Add(db.CreateDbParameter("businessType", status.Value));
                        }
                    }
                    #endregion
                    sql.Append(" order by a.code");
                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    conn.Open();
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return tableNames;
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }

        /// <summary>
        /// 合并导出配件销售结算主清单
        /// </summary>
        public bool ExportPartsSalesSettlementForQueryWithDetails(int[] billIds, int customerCompanyId, string code, string customerCompanyCode, int? partsSalesCategoryId, string customerCompanyName, int? accountGroupId, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName) {
            fileName = GetExportFilePath("配件销售结算主清单_" + ".xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {
                    var sql = new StringBuilder();
                    #region SQL
                    sql.AppendFormat(@"
                                    Select a.Code, /*结算单编号*/
                                           a.Salescompanyname, /*销售企业*/
                                           a.Partssalescategoryname, /*品牌*/
                                           a.Accountgroupname, /*客户账号*/
                                           a.Customercompanycode, /*客户企业编号*/
                                           a.Customercompanyname, /*客户企业名称*/
                                           a.Totalsettlementamount, /*结算总金额*/
                                           a.Rebateamount, /*折让不含税金额*/
                                           a.Taxrate, /*税率*/
                                           a.Tax, /*税额*/
                                          (select value from keyvalueitem where NAME = 'PartsSalesSettlement_Status'and key=a.Status) As Status,/*状态*/
                                          (select value from keyvalueitem where NAME = 'PartsPurchaseSettleBill_SettlementPath'and key=a.Settlementpath) As Settlementpath, /*结算方向*/
                                           a.Offsettedsettlementbillcode, /*被反冲结算单编号*/
                                           a.Remark, /*备注*/
                                          (select value from keyvalueitem where NAME = 'PartsAttribution'and key=pb.PartsAttribution) As PartsAttribution,/*配件所属分类*/
                                           a.Creatorname, /*创建人*/
                                           a.Createtime, /*创建时间*/
                                           a.Modifiername, /*修改人*/
                                           a.Modifytime, /*修改时间*/
                                           a.Approvername, /*审批人*/
                                           a.Approvetime, /*审批时间*/
                                           a.Invoiceregistrationoperator, /*发票登记人*/
                                           a.Invoiceregistrationtime, /*发票登记时间*/
                                           a.Abandonername, /*作废人*/
                                           a.Abandontime, /*作废时间*/
                                           b.SourceCode, /*关联单编号*/
                                          (select value from keyvalueitem where NAME = 'PartsSalesSettlementRef_SourceType'and key=b.SourceType) As SourceType,/*单据类型*/
                                           b.WarehouseName, /*仓库名称*/
                                           decode(b.sourcetype, 1, c.sparepartcode, 5, d.sparepartcode), /*配件编号*/
                                           decode(b.sourcetype, 1, c.Sparepartname, 5, d.Sparepartname), /*配件名称*/
                                           decode(b.sourcetype, 1, c.Settlementprice, 5, d.Settlementprice), /*结算价*/
                                           decode(b.sourcetype, 1, c.OutboundAmount, 5, -d.InspectedQuantity), /*结算数量*/
                                           decode(b.sourcetype,
                                                  1,
                                                  nvl(c.Settlementprice, 0) * nvl(c.OutboundAmount, 0),
                                                  5,
                                                  -nvl(d.Settlementprice, 0) * nvl(d.InspectedQuantity, 0)) /*结算金额*/
                                      From Partssalessettlement a
                                      Left Join PartsSalesSettlementRef b
                                        On a.Id = b.PartsSalesSettlementId
                                      left join PartsOutboundBillDetail c
                                        on b.SourceId = c.PartsOutboundBillId
                                       and b.sourcetype = 1
                                      left join PartsInboundCheckBillDetail d
                                        on b.SourceId = d.PartsInboundCheckBillId
                                       and b.sourcetype = 5
                                      left join partsbranch pb
                                      on a.PartsSalesCategoryId=pb.PartsSalesCategoryId And pb.PartId= decode(b.sourcetype, 1, c.sparepartid, 5, d.sparepartid)
                                     where a.customerCompanyId = {0} ", customerCompanyId);
                    #endregion
                    var tableNames = sql.ToString().GetRegexNames();
                    var dbParameters = new List<DbParameter>();
                    #region 条件过滤


                    if(billIds != null && billIds.Length > 0) {
                        var idStr = string.Join(",", billIds);
                        sql.Append(" and a.id in (" + idStr + ")");
                    } else {
                        if(!string.IsNullOrEmpty(code)) {
                            sql.Append(" and Upper(a.code) like Upper({0}code) ");
                            dbParameters.Add(db.CreateDbParameter("code", "%" + code + "%"));
                        }
                        if(!string.IsNullOrEmpty(customerCompanyCode)) {
                            sql.Append(" and Upper(a.customerCompanyCode) like Upper({0}customerCompanyCode) ");
                            dbParameters.Add(db.CreateDbParameter("customerCompanyCode", "%" + customerCompanyCode + "%"));
                        }
                        if(!string.IsNullOrEmpty(customerCompanyName)) {
                            sql.Append(" and a.customerCompanyName like {0}customerCompanyName ");
                            dbParameters.Add(db.CreateDbParameter("customerCompanyName", "%" + customerCompanyName + "%"));
                        }
                        if(partsSalesCategoryId.HasValue) {
                            sql.Append(" and a.partsSalesCategoryId={0}partsSalesCategoryId");
                            dbParameters.Add(db.CreateDbParameter("partsSalesCategoryId", partsSalesCategoryId.Value));
                        }
                        if(accountGroupId.HasValue) {
                            sql.Append(" and a.accountGroupId={0}accountGroupId");
                            dbParameters.Add(db.CreateDbParameter("accountGroupId", accountGroupId.Value));
                        }
                        if(status.HasValue) {
                            sql.Append(" and a.status={0}status");
                            dbParameters.Add(db.CreateDbParameter("status", status.Value));
                        }
                        if(createTimeBegin.HasValue) {
                            sql.Append(" and a.createTime>=to_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(" and a.createTime<=to_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                    }
                    #endregion
                    sql.Append(" order by a.code");
                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    conn.Open();
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return tableNames;
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }


        /// <summary>
        /// 根据关联单导出配件销售结算单清单
        /// </summary>
        public bool ExportPartsSalesSettlementDetailByRefId(int[] inboundIds, int[] outboundIds, out string fileName) {
            fileName = GetExportFilePath("配件销售结算单清单_.xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    var sql1 = ""; var sql2 = "";
                    var dbParameters = new List<DbParameter>();
                    if(inboundIds != null && inboundIds.Length > 0) {
                        sql1 = (@" (Select a.Sparepartid        As Sparepartid,
                                                a.Sparepartcode      As Sparepartcode,
                                                a.Sparepartname      As Sparepartname,
                                                a.Settlementprice    As Settlementprice,
                                                a.Settlementprice    As Pricebeforerebate,
                                                -a.Inspectedquantity As Quantitytosettle
                                           From Partsinboundcheckbilldetail a
                                           Join Partsinboundcheckbill b
                                             On a.Partsinboundcheckbillid = b.Id where b.id in (");
                        for(var i = 0; i < inboundIds.Length; i++) {
                            if(inboundIds.Length == i + 1) {
                                sql1 += string.Format("{0})) ", inboundIds[i]);
                            } else {
                                sql1 += string.Format("{0},", inboundIds[i]);
                            }
                        }
                    }
                    if(outboundIds != null && outboundIds.Length > 0) {
                        sql2 = (@" (Select a.Sparepartid     As Sparepartid,
                                              a.Sparepartcode   As Sparepartcode,
                                              a.Sparepartname   As Sparepartname,
                                              a.Settlementprice As Settlementprice,
                                              a.Settlementprice As Pricebeforerebate,
                                              a.Outboundamount  As Quantitytosettle
                                         From Partsoutboundbilldetail a
                                         Join Partsoutboundbill b
                                           On a.Partsoutboundbillid = b.Id where b.id in (");
                        for(var i = 0; i < outboundIds.Length; i++) {
                            if(outboundIds.Length == i + 1) {
                                sql2 += string.Format("{0})) ", outboundIds[i]);
                            } else {
                                sql2 += string.Format("{0},", outboundIds[i]);
                            }
                        }
                    }
                    if(sql1 != "" && sql2 != "") {
                        sql.AppendFormat(@"Select t.Sparepartcode,
                                               t.Sparepartname,
                                               t.Pricebeforerebate,
                                               t.Settlementprice,
                                               --t.Settlementprice As Originalprice,
                                               Sum(Quantitytosettle),
                                               Sum(t.Quantitytosettle * t.Settlementprice)
                                          From ({0}
                                                Union {1}) t
                                         Group By t.Sparepartid,
                                                  t.Sparepartcode,
                                                  t.Sparepartname,
                                                  t.Pricebeforerebate,
                                                  t.Settlementprice", sql1, sql2);
                    } else {
                        if(sql1 != "") {
                            sql.AppendFormat(@"Select t.Sparepartcode,
                                               t.Sparepartname,
                                               t.Pricebeforerebate,
                                               t.Settlementprice,
                                               --t.Settlementprice As Originalprice,
                                               Sum(Quantitytosettle),
                                               Sum(t.Quantitytosettle * t.Settlementprice)
                                          From ({0}) t
                                         Group By t.Sparepartid,
                                                  t.Sparepartcode,
                                                  t.Sparepartname,
                                                  t.Pricebeforerebate,
                                                  t.Settlementprice", sql1);
                        }
                        if(sql2 != "") {
                            sql.AppendFormat(@"Select t.Sparepartcode,
                                               t.Sparepartname,
                                               t.Pricebeforerebate,
                                               t.Settlementprice,
                                               --t.Settlementprice As Originalprice,
                                               Sum(Quantitytosettle),
                                               Sum(t.Quantitytosettle * t.Settlementprice)
                                          From ({0}) t
                                         Group By t.Sparepartid,
                                                  t.Sparepartcode,
                                                  t.Sparepartname,
                                                  t.Pricebeforerebate,
                                                  t.Settlementprice", sql2);
                        }
                    }
                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode, ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName,ErrorStrings.Export_Titile_PartsSalesSettlement_Pricebeforerebate, ErrorStrings.Export_Title_PartsPurchaseRtnSettleBill_SettlementPrice,ErrorStrings.Export_Title_PartsPurchaseRtnSettleBill_QuantityToSettle,ErrorStrings.Export_Title_PartsPurchaseRtnSettleBill_SettlementAmount
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }
    }
}