﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using NPOI.HSSF.UserModel;
using NPOI.SS.Util;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        //合并导出配件销售订单
        public bool ExportCrossSalesOrderWithDetails(int? id, string sparePartCode,string sparePartName, string applyCompnayCode, string applyCompnayName, string subCompanyCode, string code, string subCompanyName,  int? status,  DateTime? createTimeBegin, DateTime? createTimeEnd, DateTime? submitTimeBegin, DateTime? submitTimeEnd,  out string fileName) {
            fileName = GetExportFilePath("配件销售订单主清单.xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);
                var userinfo = Utils.GetCurrentUserInfo();
                using(var conn = db.CreateDbConnection()) {

                    conn.Open();
                    var sql = new StringBuilder();
                    sql.AppendFormat(@"select (case when cs.path is null then cast('否' as varchar2(50))
                                        else cast('是' as varchar2(50)) end),/*是否有附件*/
                                         cs.code ,/*订单号*/
                                               (select value
                                                  from keyvalueitem kv
                                                 where kv.name = 'CrossSalesOrderStatus'
                                                   and kv.key = cs.status) as status,/*状态*/
                                               decode(cs.isautosales,
                                                      1,
                                                      cast('是' as varchar2(50)),
                                                      cast('否' as varchar2(50))) status ,/*是否生成销售订单*/
                                               cs.applycompnaycode ,/*申请企业编号*/
                                               cs.applycompnayname ,/*申请企业名称*/
                                               cs.subcompanycode ,/*隶属企业编号*/
                                               cs.subcompanyname ,/*隶属企业名称*/
                                               (select value
                                                  from keyvalueitem kv
                                                 where kv.name = 'CrossSalesOrderType'
                                                   and kv.key = cs.type) as type,/*品种类型*/
                                                (case when {0}=cs.SubCompanyId then cast('' as varchar2(50)) else cs.receivingname end ) receivingname,/*收货人*/
                                                 (case when {0}=cs.SubCompanyId then cast('' as varchar2(50)) else cs.receivingphone end ) receivingphone,/*收货电话*/
                                                 (case when {0}=cs.SubCompanyId then cast('' as varchar2(50)) else cs.receivingaddress end ) receivingaddress,/*收货地址*/
                                               (select value
                                                  from keyvalueitem kv
                                                 where kv.name = 'PartsShipping_Method'
                                                   and kv.key = cs.ShippingMethod) as ShippingMethod,/*发运方式*/
                                               cs.creatorname,/*创建人*/
                                               cs.createtime,/*创建时间*/
                                               cs.modifiername,/*修改人*/
                                                cs.modifytime,/*修改时间*/
                                               cs.abandonername,/*作废人*/
                                               cs.abandontime,/*作废时间*/                                              
                                               cs.submittername,/*提交人*/
                                               cs.submittime,/*提交时间*/
                                               cs.rejectername,/*驳回人*/
                                               cs.rejecttime,/*驳回时间*/
                                               cs.approvername,/*初审人*/
                                               cs.approvetime,/*初审时间*/
                                               cs.checkername,/*审核人*/
                                               cs.checktime,/*审核时间*/
                                               cs.closername,/*审批人*/
                                               cs.closetime,/*审批时间*/
                                               cs.seniorauditname,/*高级审核人*/
                                               cs.senioraudittime,/*高级审核时间*/
                                               cs.stoper,/*终止人*/
                                               cs.stoptime,/*终止时间*/
                                               cs.finisher,/*完善人*/
                                               cs.finishertime,/*完善时间*/
                                               cs.approvement,/*审核意见*/
                                               csd.sparepartcode,/*配件编号*/
                                               csd.sparepartname,/*配件名称*/
                                               csd.orderedquantity,/*订货数量*/
                                               csd.abcstrategy,/*配件ABC分类*/
                                               csd.centerprice,/*中心库价*/
                                               csd.salesprice,/*服务站价*/
                                               csd.retailguideprice,/*建议售价*/
                                               csd.pricetypename,/*价格类型*/
                                               csd.vin,/*底盘号*/
                                               csd.explain,/*跨区域说明*/
                                              decode(csd.IsHand,
                                                      1,
                                                      cast('是' as varchar2(50)),
                                                      cast('否' as varchar2(50))) IsHand ,/*是否手填*/
                                               cs.salescode,/*销售单号*/
                                               psd.orderedquantity,/*销售订货数量*/
                                               psd.approvequantity,/*销售审核数量*/
                                               psd.orderprice,/*销售订货价*/
                                               psd.salesprice,/*销售服务站价*/
                                               psb.code,/*出库单号*/
                                               pl.code,/*出库计划单号*/
                                               psb.createtime,/*出库时间*/
                                               py.outboundamount/*出库数量*/
                                          from CrossSalesOrder cs
                                          join CrossSalesOrderDetail csd
                                            on cs.id = csd.crosssalesorderid
                                           and csd.status = 1
                                          left join partssalesorder ps
                                            on cs.salescode = ps.code
                                          left join partssalesorderdetail psd
                                            on ps.id = psd.partssalesorderid
                                           and csd.sparepartid = psd.sparepartid
                                          left join partsoutboundbill psb
                                            on psb.originalrequirementbillcode = ps.code
                                          left join partsoutboundplan pl
                                            on psb.partsoutboundplanid = pl.id
                                          left join partsoutboundbilldetail py
                                            on psb.id = py.partsoutboundbillid
                                           and csd.sparepartid = py.sparepartid
                                          where  1=1 
                                        ", userinfo.EnterpriseId);
                    var tableNames = sql.ToString().GetRegexNames();
                    var dbParameters = new List<DbParameter>();
                    if(id.HasValue) {
                        sql.Append(" and cs.id = {0}id");
                        dbParameters.Add(db.CreateDbParameter("id", id.Value));
                    } else {
                        if(userinfo.EnterpriseId!=12730) {
                            sql.Append(@" and  (cs.applycompnayid={0}EnterpriseId or cs.subcompanyid ={0}EnterpriseId) ");
                            dbParameters.Add(db.CreateDbParameter("EnterpriseId", userinfo.EnterpriseId));
                        }
                        if(!string.IsNullOrEmpty(sparePartCode)) {
                            sql.Append(@" and Lower(csd.sparePartCode) like {0}sparePartCode ");
                            dbParameters.Add(db.CreateDbParameter("sparePartCode", "%" + sparePartCode.ToLower() + "%"));
                        } 
                        if(!string.IsNullOrEmpty(sparePartName)) {
                            sql.Append(@" and Lower(csd.sparePartName) like {0}sparePartName ");
                            dbParameters.Add(db.CreateDbParameter("sparePartName", "%" + sparePartName.ToLower() + "%"));
                        }
                        if(!string.IsNullOrEmpty(code)) {
                            sql.Append(@" and Lower(cs.code) like {0}code ");
                            dbParameters.Add(db.CreateDbParameter("code", "%" + code.ToLower() + "%"));
                        }
                        if(!string.IsNullOrEmpty(applyCompnayCode)) {
                            sql.Append(@" and Lower(cs.applyCompnayCode) like {0}applyCompnayCode ");
                            dbParameters.Add(db.CreateDbParameter("applyCompnayCode", "%" + applyCompnayCode.ToLower() + "%"));
                        }
                        if(!string.IsNullOrEmpty(applyCompnayName)) {
                            sql.Append(@" and Lower(cs.applyCompnayName) like {0}applyCompnayName ");
                            dbParameters.Add(db.CreateDbParameter("applyCompnayName", "%" + applyCompnayName.ToLower() + "%"));
                        }
                        if(!string.IsNullOrEmpty(subCompanyCode)) {
                            sql.Append(@" and Lower(cs.subCompanyCode) like {0}subCompanyCode ");
                            dbParameters.Add(db.CreateDbParameter("subCompanyCode", "%" + subCompanyCode.ToLower() + "%"));
                        }
                        if(!string.IsNullOrEmpty(subCompanyName)) {
                            sql.Append(@" and Lower(cs.subCompanyName) like {0}subCompanyName ");
                            dbParameters.Add(db.CreateDbParameter("subCompanyName", "%" + subCompanyName.ToLower() + "%"));
                        }
                        
                        if(status.HasValue) {
                            sql.Append(@" and cs.status={0}status");
                            dbParameters.Add(db.CreateDbParameter("status", status.Value));
                        }
                       
                        if(createTimeBegin.HasValue) {
                            sql.Append(@" and cs.createTime >=To_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(@" and cs.createTime <=To_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                        if(submitTimeBegin.HasValue) {
                            sql.Append(@" and cs.SubmitTime >=To_date({0}submitTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = submitTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("submitTimeBegin", tempTime.ToString("G")));
                        }
                        if(submitTimeEnd.HasValue) {
                            sql.Append(@" and cs.SubmitTime <=To_date({0}submitTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = submitTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("submitTimeEnd", tempTime.ToString("G")));
                        }                       
                    }
                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0)
                                return tableNames;
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }

        }
    }
}
