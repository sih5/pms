﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using System.Data.Common;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        /// <summary>
        /// 导入配件客户信息
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="excelImportNum"></param>
        /// <param name="errorData"></param>
        /// <param name="errorDataFileName"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public bool ImportCustomerInformation(string fileName, out int excelImportNum, out List<CustomerInformationExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<CustomerInformationExtend>();
            var allList = new List<CustomerInformationExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("CustomerInformation", out notNullableFields, out fieldLenght);
                List<object> excelColumns;
                List<CustomerInformationExtend> rightList;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    #region 指定文件中的列对应的名称 例如
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_CustomerInformation_SalesCompanyName, "SalesCompanyName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Credenceapplication_CompanyName, "CustomerCompanyName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Agency_LinkMan, "ContactPerson");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Agency_LinkManMobile, "ContactPhone");
                    #endregion
                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;
                    #region 获取对应枚举
                    var keyValuePairs = new[] { new KeyValuePair<string, string>("MasterDataStatus", "MasterData_Status") };
                    tempExcelOperator.ImportHelper.LoadEnumFromDb(null,null, keyValuePairs);
                    #endregion
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var tempImportObj = new CustomerInformationExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        #region 根据导入的字段给对应的实体属性赋值
                        tempImportObj.SalesCompanyNameStr = newRow["SalesCompanyName"];
                        tempImportObj.CustomerCompanyNameStr = newRow["CustomerCompanyName"];
                        tempImportObj.ContactPersonStr = newRow["ContactPerson"];
                        tempImportObj.ContactPhoneStr = newRow["ContactPhone"];
                        #endregion

                        var tempErrorMessage = new List<string>();
                        #region 导入的数据基本检查
                        //联系人检查
                        var fieldIndex = notNullableFields.IndexOf("ContactPerson".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.ContactPersonStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_CustomerInformation_ContactPersonIsNull);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.ContactPersonStr) > fieldLenght["ContactPerson".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_CustomerInformation_ContactPersonIsLong);
                        }
                        //联系电话检查
                        fieldIndex = notNullableFields.IndexOf("ContactPhone".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.ContactPhoneStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_MarketingDepartment_LinkPhoneIsNull);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.ContactPhoneStr) > fieldLenght["ContactPhone".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_MarketingDepartment_LinkPhoneIsLong);
                        }
                        #endregion
                        if(tempErrorMessage.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });
                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    #region 剩下的数据进行业务检查
                    //4、文件中销售企业名称、客户企业名称组合唯一，否则提示：数据重复
                    var groups = tempRightList.GroupBy(r => new {
                        r.SalesCompanyNameStr,
                        r.CustomerCompanyNameStr
                    }).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        groupItem.ErrorMsg = ErrorStrings.ImpPartsSupplierRelation_Validation31;
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    //1、销售企业名称存在于企业表中，否则提示：销售企业名称不存在
                    //2、客户企业名称存在于企业表中，否则提示：客户企业名称不存在
                    var salesCompanyNamesNeedCheck = tempRightList.Select(r => r.SalesCompanyNameStr).Distinct().ToArray();
                    var customerCompanyNamesNeedCheck = tempRightList.Select(r => r.CustomerCompanyNameStr).Distinct().ToArray();
                    var allCompanyNamesNeedCheck = salesCompanyNamesNeedCheck.Concat(customerCompanyNamesNeedCheck).Distinct().ToArray();
                    var dbCompanys = new List<CompanyExtend>();
                    Func<string[], bool> getDbCompanys = value => {
                        var dbObj = new CompanyExtend {
                            Id = Convert.ToInt32(value[0]),
                            Name = value[1]
                        };
                        dbCompanys.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select Id,Name from Company where status=1 ", "Name", false, allCompanyNamesNeedCheck, getDbCompanys);
                    foreach(var tempRight in tempRightList) {
                        var repairItem = dbCompanys.FirstOrDefault(v => v.Name == tempRight.SalesCompanyNameStr);
                        if(repairItem == null) {
                            tempRight.ErrorMsg = ErrorStrings.Export_Validation_CustomerInformation_Validation1;
                            continue;
                        }
                        tempRight.SalesCompanyId = repairItem.Id;
                        repairItem = dbCompanys.FirstOrDefault(v => v.Name == tempRight.CustomerCompanyNameStr);
                        if(repairItem == null) {
                            tempRight.ErrorMsg = ErrorStrings.Export_Validation_CustomerInformation_Validation2;
                            continue;
                        }
                        tempRight.CustomerCompanyId = repairItem.Id;
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    //3、系统中销售企业名称、客户企业名称组合唯一，否则提法：销售企业与客户企业关系已经存在
                    var salesCompanyIdsNeedCheck = tempRightList.Select(r => r.SalesCompanyId.ToString(CultureInfo.InvariantCulture)).Distinct().ToArray();
                    var dbCustomerInformations = new List<CustomerInformationExtend>();
                    Func<string[], bool> getDbCustomerInformations = value => {
                        var dbObj = new CustomerInformationExtend {
                            SalesCompanyId = Convert.ToInt32(value[0]),
                            CustomerCompanyId = Convert.ToInt32(value[1])
                        };
                        dbCustomerInformations.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select SalesCompanyId,CustomerCompanyId from CustomerInformation where status=1 ", "SalesCompanyId", false, salesCompanyIdsNeedCheck, getDbCustomerInformations);
                    foreach(var tempRight in tempRightList) {
                        var usedPartsDistanceInfor = dbCustomerInformations.FirstOrDefault(v => v.SalesCompanyId == tempRight.SalesCompanyId && v.CustomerCompanyId == tempRight.CustomerCompanyId);
                        if(usedPartsDistanceInfor != null) {
                            tempRight.ErrorMsg = ErrorStrings.Export_Validation_CustomerInformation_Validation3;
                        }
                    }
                    #endregion
                    //获取所有不合格数据
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    //获取所有合格数据
                    rightList = allList.Except(errorList).ToList();
                    #region 将合格数据的值填上
                    foreach(var rightItem in rightList) {
                        rightItem.ContactPerson = rightItem.ContactPersonStr;
                        rightItem.ContactPhone = rightItem.ContactPhoneStr;
                        rightItem.Status = tempExcelOperator.ImportHelper.GetEnumValue("MasterDataStatus", ErrorStrings.Export_Validation_PartsBranch_Used) ?? 0;
                    }
                    #endregion
                }
                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempObj = list[index - 1];
                            var values = new object[] {
                                #region 设置错误信息导出的列的值

                                tempObj.SalesCompanyNameStr,tempObj.CustomerCompanyNameStr,
                                tempObj.ContactPersonStr,tempObj.ContactPhoneStr,
                                tempObj.ErrorMsg
                                
                                #endregion                     
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }
                //导入所有合格数据
                if(!rightList.Any())
                    return true;
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    //开启事务，新增更新配件在一个事务内
                    var ts = conn.BeginTransaction();
                    try {
                        //新增配件
                        if(rightList.Any()) {
                            #region 获取对应的插入的SQL 通过db.GetInsertSql方法
                            //获取新增数据的sql语句，Id为主键
                            var sqlInsert = db.GetInsertSql("CustomerInformation", "Id", new[] {
                                "SalesCompanyId", "CustomerCompanyId", "ContactPerson", "ContactPhone", "Status", "CreatorId", "CreatorName", "CreateTime"
                            });
                            #endregion
                            var userInfo = Utils.GetCurrentUserInfo();
                            //往数据库增加市场部信息
                            foreach(var item in rightList) {
                                #region 添加Sql的参数
                                var command = db.CreateDbCommand(sqlInsert, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("SalesCompanyId", item.SalesCompanyId));
                                command.Parameters.Add(db.CreateDbParameter("CustomerCompanyId", item.CustomerCompanyId));
                                command.Parameters.Add(db.CreateDbParameter("ContactPerson", item.ContactPerson));
                                command.Parameters.Add(db.CreateDbParameter("ContactPhone", item.ContactPhone));
                                command.Parameters.Add(db.CreateDbParameter("Status", item.Status));
                                command.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                command.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                command.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                command.ExecuteNonQuery();
                                #endregion
                            }
                        }
                        //无异常提交
                        ts.Commit();
                    } catch(Exception) {
                        //报错回滚
                        ts.Rollback();
                        throw;
                    } finally {
                        if(conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
            }
        }


        public bool ExportCustomerInformation(int[] customerInformationId, string customerCompanyName, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName)
        {
            fileName = GetExportFilePath("配件客户信息_" + ".xlsx");
            try {
                if (!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var userinfo = Utils.GetCurrentUserInfo();
                var branchId = userinfo.EnterpriseId;

                var db = DbHelper.GetDbHelp(ConnectionString);
                using (var conn = db.CreateDbConnection()) {
                    var sql = new StringBuilder();
                    sql.AppendFormat(@"select 
                                          c.name,/*销售企业名称*/
                                          b.name,/*客户企业名称*/
                                          a.ContactPerson,/*联系人*/
                                          a.ContactPhone,/*联系电话*/
                                          (select value from keyvalueitem where NAME = 'MasterData_Status'and key=a.Status) As Status,/*状态*/
                                          a.CreatorName,/*创建人*/
                                          a.CreateTime,/*创建时间*/
                                          a.ModifierName,/*修改人*/
                                          a.ModifyTime/*修改时间*/
                                        FROM CustomerInformation a
                                        LEFT JOIN Company b ON a.CustomerCompanyId = b.Id
                                        LEFT JOIN Company c ON a.SalesCompanyId = c.Id
                                        WHERE a.SalesCompanyId = {0} ", userinfo.EnterpriseId);
                    var dbParameters = new List<DbParameter>();
                    if (customerInformationId != null && customerInformationId.Length > 0) {
                        sql.Append(" and a.id in (");
                        for (var i = 0; i < customerInformationId.Length; i++) {
                            if (customerInformationId.Length == i + 1) {
                                sql.Append("{0}id" + customerInformationId[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter("id" + customerInformationId[i].ToString(CultureInfo.InvariantCulture), customerInformationId[i]));
                            }
                            else {
                                sql.Append("{0}id" + customerInformationId[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter("id" + customerInformationId[i].ToString(CultureInfo.InvariantCulture), customerInformationId[i]));
                            }
                        }
                        sql.Append(")");
                    }
                    else {
                        #region 条件过滤
                        if (!string.IsNullOrEmpty(customerCompanyName)) {
                            sql.Append(" and b.name like {0}name ");
                            dbParameters.Add(db.CreateDbParameter("name", "%" + customerCompanyName + "%"));
                        }
                        if (createTimeBegin.HasValue) {
                            sql.Append(@" and a.createTime >=to_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if (createTimeEnd.HasValue) {
                            sql.Append(@" and a.createTime <=to_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                        #endregion
                    }
                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    conn.Open();
                    var reader = command.ExecuteReader();
                    using (var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index =>
                        {
                            if (index == 0) {
                                return new object[] {
                                    ErrorStrings.Export_Title_CustomerInformation_SalesCompanyName,ErrorStrings.Export_Title_Credenceapplication_CompanyName,ErrorStrings.Export_Title_Agency_LinkMan,ErrorStrings.Export_Title_Agency_LinkManMobile,ErrorStrings.Export_Title_AccountPeriod_Status,ErrorStrings.Export_Title_AccountPeriod_CreatorName,ErrorStrings.Export_Title_AccountPeriod_CreateTime,ErrorStrings.Export_Title_AccountPeriod_ModifierName,ErrorStrings.Export_Title_AccountPeriod_ModifyTime
                                };
                            }
                            if (reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if (num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            }
            catch (Exception) {
                return false;
            }
        }

    }
}
