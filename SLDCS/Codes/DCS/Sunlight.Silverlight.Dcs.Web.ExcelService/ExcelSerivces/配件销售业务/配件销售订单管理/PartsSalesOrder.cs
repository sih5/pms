﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using NPOI.HSSF.UserModel;
using NPOI.SS.Util;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        //合并导出配件销售订单
        public bool ExportPartsSalesOrderWithDetails(int? id, int personnelId, int? provinceId, string city, string code, string submitCompanyCode, string submitCompanyName, int? partsSalesOrderTypeId, int? salesCategoryId, int? status, int? warehouseId, bool? isDebt, bool? ifDirectProvision, DateTime? createTimeBegin, DateTime? createTimeEnd, DateTime? submitTimeBegin, DateTime? submitTimeEnd, DateTime? abandonTimeBegin, DateTime? abandonTimeEnd, DateTime? approveTimeBegin, DateTime? approveTimeEnd, out string fileName) {
            //
            fileName = GetExportFilePath("配件销售订单主清单.xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {

                    conn.Open();
                    var sql = new StringBuilder();
                    sql.AppendFormat(@"Select a.Code,
                                       a.Salescategoryname,
                                       a.Salesunitownercompanyname,
                                       (Select d.Name From Warehouse d Where d.Id=a.warehouseid),
                                       a.SubmitCompanyCode,
                                       a.SubmitCompanyName,
                                       a.Province,
                                       a.City,
                                       (select value from keyvalueitem where NAME = 'Company_Type'and key=a.Customertype) As Customertype,
                                       a.Partssalesordertypename,
                                       (select value from keyvalueitem where NAME = 'IsOrNot'and key=a.Ifdirectprovision) As Ifdirectprovision,
                                       (select value from keyvalueitem where NAME = 'PartsSalesOrder_Status'and key=a.Status) As Status,
                                       (select value from keyvalueitem where NAME = 'IsOrNot'and key=a.Isdebt) As Isdebt,
                                       a.Totalamount,
                                       a.Remark,
                                       a.Receivingaddress,
                                       (select value from keyvalueitem where NAME = 'PartsShipping_Method'and key=a.Shippingmethod) As Shippingmethod,
                                       a.Requesteddeliverytime,
                                       a.Stopcomment,
                                       a.Creatorname,
                                       a.Createtime,
                                       a.Submittername,
                                       a.Submittime,
                                       a.Approvername,
                                       a.Approvetime,
                                       a.Abandonername,
                                       a.Abandontime,
                                       a.Remark,
                                       b.Sparepartcode,
                                       b.Sparepartname,
                                       b.Orderedquantity,
                                       b.Approvequantity,
                                       b.Orderprice,
                                       b.Ordersum,
                                       b.Remark As Detailremark
                                  From Partssalesorder a
                                  Left Join Partssalesorderdetail b
                                    On a.Id = b.Partssalesorderid
                                 Where (Exists (Select 1 As C1
                                                  From Salesunitaffipersonnel Extent2
                                                 Where (Extent2.Personnelid = {0})
                                                   And (Extent2.Salesunitid = a.Salesunitid))) ", personnelId);
                    var dbParameters = new List<DbParameter>();
                    if(id.HasValue) {
                        sql.Append(" and a.id = {0}id");
                        dbParameters.Add(db.CreateDbParameter("id", id.Value));
                    } else {
                        if(!string.IsNullOrEmpty(city)) {
                            sql.Append(@" and Lower(a.city) like {0}city ");
                            dbParameters.Add(db.CreateDbParameter("city", "%" + city.ToLower() + "%"));
                        }
                        if(!string.IsNullOrEmpty(code)) {
                            sql.Append(@" and Lower(a.code) like {0}code ");
                            dbParameters.Add(db.CreateDbParameter("code", "%" + code.ToLower() + "%"));
                        }
                        if(!string.IsNullOrEmpty(submitCompanyCode)) {
                            sql.Append(@" and Lower(a.submitCompanyCode) like {0}submitCompanyCode ");
                            dbParameters.Add(db.CreateDbParameter("submitCompanyCode", "%" + submitCompanyCode.ToLower() + "%"));
                        }
                        if(!string.IsNullOrEmpty(submitCompanyName)) {
                            sql.Append(@" and Lower(a.submitCompanyName) like {0}submitCompanyName ");
                            dbParameters.Add(db.CreateDbParameter("submitCompanyName", "%" + submitCompanyName.ToLower() + "%"));
                        }
                        if(partsSalesOrderTypeId.HasValue) {
                            sql.Append(@" and a.partsSalesOrderTypeId={0}partsSalesOrderTypeId");
                            dbParameters.Add(db.CreateDbParameter("partsSalesOrderTypeId", partsSalesOrderTypeId.Value));
                        }
                        if(provinceId.HasValue) {
                            sql.Append(@" and a.provinceId={0}provinceId");
                            dbParameters.Add(db.CreateDbParameter("provinceId", provinceId.Value));
                        }
                        if(salesCategoryId.HasValue) {
                            sql.Append(@" and a.salesCategoryId={0}salesCategoryId");
                            dbParameters.Add(db.CreateDbParameter("salesCategoryId", salesCategoryId.Value));
                        }
                        if(status.HasValue) {
                            sql.Append(@" and a.status={0}status");
                            dbParameters.Add(db.CreateDbParameter("status", status.Value));
                        }
                        if(warehouseId.HasValue) {
                            sql.Append(@" and a.warehouseId={0}warehouseId");
                            dbParameters.Add(db.CreateDbParameter("warehouseId", warehouseId.Value));
                        }
                        if(isDebt.HasValue) {
                            sql.Append(@" and a.isDebt={0}isDebt");
                            dbParameters.Add(ifDirectProvision.Value ? db.CreateDbParameter("isDebt", 1) : db.CreateDbParameter("isDebt", 0));
                        }
                        if(ifDirectProvision.HasValue) {
                            sql.Append(@" and a.ifDirectProvision ={0}ifDirectProvision");
                            dbParameters.Add(ifDirectProvision.Value ? db.CreateDbParameter("ifDirectProvision", 1) : db.CreateDbParameter("ifDirectProvision", 0));
                        }
                        if(createTimeBegin.HasValue) {
                            sql.Append(@" and a.createTime >=To_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(@" and a.createTime <=To_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                        if(submitTimeBegin.HasValue) {
                            sql.Append(@" and a.submitTime >=To_date({0}submitTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = submitTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("submitTimeBegin", tempTime.ToString("G")));
                        }
                        if(submitTimeEnd.HasValue) {
                            sql.Append(@" and a.submitTime <=To_date({0}submitTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = submitTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("submitTimeEnd", tempTime.ToString("G")));
                        }

                        if(abandonTimeBegin.HasValue) {
                            sql.Append(@" and a.abandonTime >=To_date({0}abandonTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = abandonTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("abandonTimeBegin", tempTime.ToString("G")));
                        }
                        if(abandonTimeEnd.HasValue) {
                            sql.Append(@" and a.abandonTime <=To_date({0}abandonTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = abandonTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("abandonTimeEnd", tempTime.ToString("G")));
                        }

                        if(approveTimeBegin.HasValue) {
                            sql.Append(@" and a.approveTime >=To_date({0}approveTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = approveTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("approveTimeBegin", tempTime.ToString("G")));
                        }
                        if(approveTimeEnd.HasValue) {
                            sql.Append(@" and a.approveTime <=To_date({0}approveTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = approveTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("approveTimeEnd", tempTime.ToString("G")));
                        }


                    }
                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                     ErrorStrings.Export_Title_PartsSalesOrder_Code, ErrorStrings.Export_Title_Partssalescategory_Name, ErrorStrings.Export_Title_PartsSalesOrder_SalesUnitOwnerCompanyName, ErrorStrings.Export_Title_PartsSalesOrder_WarehouseId, ErrorStrings.Export_Title_Credenceapplication_CustomerCode,ErrorStrings.Export_Title_Credenceapplication_CustomerName, ErrorStrings.Export_Title_Agency_Province,ErrorStrings.Export_Title_Agency_City,ErrorStrings.Export_Validation_Company_Type, ErrorStrings.Export_Title_PartsPurchaseOrder_OrderType, ErrorStrings.Export_Title_PartsPurchaseOrder_IfDirectProvision, ErrorStrings.Export_Title_AccountPeriod_Status, ErrorStrings.Export_Title_PartsSalesOrder_IsDebt, ErrorStrings.Export_Title_PartsSalesOrder_TotalAmount, ErrorStrings.Export_Title_PartsBranch_Remark, ErrorStrings.Export_Title_PartsPurchaseOrder_ReceiveAddress, ErrorStrings.Export_Title_PartsPurchaseOrder_ShippingMethod, ErrorStrings.Export_Title_PartsPurchaseOrder_RequestedDeliveryTime, ErrorStrings.Export_Title_Partsoutboundplan_StopComment, ErrorStrings.Export_Title_AccountPeriod_CreatorName, ErrorStrings.Export_Title_AccountPeriod_CreateTime, ErrorStrings.Export_Title_PartsSalesOrder_SubmitterName, ErrorStrings.Export_Title_PartsSalesOrder_SubmitTime, ErrorStrings.Export_Title_PackingPropertyApp_ApproverName, ErrorStrings.Export_Title_PackingPropertyApp_ApproveTime, ErrorStrings.Export_Title_PartsBranch_AbandonerName, ErrorStrings.Export_Title_PartsBranch_AbandonerTime, ErrorStrings.Export_Title_PartsBranch_Remark, 
                                     ErrorStrings.Export_Title_PartsBranch_Code, ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName, ErrorStrings.Export_Title_PartsSalesOrder_OrderQty, ErrorStrings.Export_Title_PartsSalesOrder_CurrentFulfilledQuantity, ErrorStrings.Export_Title_InternalAllocationBill_UnitPrice, ErrorStrings.Export_Title_PartsSalesOrder_OrderAmount, ErrorStrings.Export_Title_PartsPurchaseOrder_DetailRemark
                                 };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }

        }

        public bool ScheduleExportPartsSalesOrderWithDetails(int? personnelId, int? ownercompanyId, string submitCompanyName, int? invoiceReceiveCompanyId, string invoiceReceiveCompanyName, bool? ifDirectProvision, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, int? partsSalesOrderTypeId, int? salesUnitId, string jobName, out string fileName) {
            fileName = GetExportFilePath("配件销售订单主清单_" + ".xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {

                    conn.Open();
                    var sql = new StringBuilder();
                    sql.Append(@"select    a.Code,
                                           a.SalesUnitName,
                                           a.SalesUnitOwnerCompanyName,
                                           a.SubmitCompanyName,
                                           a.PartsSalesOrderTypeName,
                                           (select value from keyvalueitem where NAME = 'IsOrNot'and key=a.IfDirectProvision) As IfDirectProvision,
                                           a.ModifyTime as 修改时间,
                                           (select value from keyvalueitem where NAME = 'IsOrNot'and key=b.IfCanNewPart) As IfCanNewPart,
                                           (select value from keyvalueitem where NAME = 'PartsSalesOrder_Status'and key=a.Status) As Status,
                                           a.InvoiceReceiveCompanyName,
                                           a.ReceivingCompanyName,
                                           a.ReceivingAddress,
                                           e.value as ShippingMethod,
                                           a.SalesActivityDiscountRate,
                                           a.SalesActivityDiscountAmount,
                                           a.TotalAmount,
                                           a.RequestedShippingTime,
                                           a.RequestedDeliveryTime,
                                           a.CreatorName,
                                           a.CreateTime,
                                           a.ApproverName,
                                           a.ApproveTime,
                                           a.SubmitterName,
                                           a.SubmitTime,
                                           a.AbandonerName,
                                           a.AbandonTime,
                                           a.Remark,
                                           b.SparePartCode,
                                           b.SparePartName,
                                           b.OrderedQuantity,
                                           b.ApproveQuantity,
                                           b.OriginalPrice,
                                           b.CustOrderPriceGradeCoefficient,
                                           b.DiscountedPrice,
                                           b.OrderPrice,
                                           b.OrderSum,
                                           b.EstimatedFulfillTime,
                                           b.Remark as detailRemark
                                      from PartsSalesOrder a
                                      left join Partssalesorderdetail b
                                        on a.id = b.partssalesorderid
                                      left join salesunit c
                                        on a.salesunitid = c.id
                                       and c.status = 1
                                      left join salesunitaffipersonnel d
                                        on a.salesunitid = d.salesunitid
                                      left join (select key, value
                                                   from keyvalueitem
                                                  where name = 'PartsShipping_Method'
                                                    and status = 1) e
                                        on a.shippingmethod = e.key
                                     where 1 = 1");
                    var dbParameters = new List<DbParameter>();
                    if(personnelId.HasValue) {
                        sql.Append(@" and d.personnelId={0}personnelId");
                        dbParameters.Add(db.CreateDbParameter("personnelId", personnelId.Value));
                    }
                    if(ownercompanyId.HasValue) {
                        sql.Append(@" and c.ownercompanyId={0}ownercompanyId");
                        dbParameters.Add(db.CreateDbParameter("ownercompanyId", ownercompanyId.Value));
                    }
                    if(!string.IsNullOrEmpty(submitCompanyName)) {
                        sql.Append(@" and a.submitCompanyName ={0}submitCompanyName ");
                        dbParameters.Add(db.CreateDbParameter("submitCompanyName", submitCompanyName));
                    }
                    if(invoiceReceiveCompanyId.HasValue) {
                        sql.Append(@" and a.invoiceReceiveCompanyId={0}invoiceReceiveCompanyId");
                        dbParameters.Add(db.CreateDbParameter("invoiceReceiveCompanyId", invoiceReceiveCompanyId.Value));
                    }
                    if(!string.IsNullOrEmpty(invoiceReceiveCompanyName)) {
                        sql.Append(@" and a.invoiceReceiveCompanyName ={0}invoiceReceiveCompanyName ");
                        dbParameters.Add(db.CreateDbParameter("invoiceReceiveCompanyName", invoiceReceiveCompanyName));
                    }
                    if(ifDirectProvision.HasValue) {
                        sql.Append(@" and a.ifDirectProvision ={0}ifDirectProvision");
                        dbParameters.Add(ifDirectProvision.Value ? db.CreateDbParameter("ifDirectProvision", 1) : db.CreateDbParameter("ifDirectProvision", 0));
                    }
                    if(status.HasValue) {
                        sql.Append(@" and a.status ={0}status");
                        dbParameters.Add(db.CreateDbParameter("status", status.Value));
                    }
                    if(createTimeBegin.HasValue) {
                        sql.Append(@" and a.createTime >=To_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                        var tempValue = createTimeBegin.Value;
                        var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                        dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));

                    }
                    if(createTimeEnd.HasValue) {
                        sql.Append(@" and a.createTime <=To_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                        var tempValue = createTimeEnd.Value;
                        var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                        dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                    }
                    if(partsSalesOrderTypeId.HasValue) {
                        sql.Append(@" and a.partsSalesOrderTypeId={0}partsSalesOrderTypeId");
                        dbParameters.Add(db.CreateDbParameter("partsSalesOrderTypeId", partsSalesOrderTypeId.Value));
                    }
                    if(salesUnitId.HasValue) {
                        sql.Append(@" and a.salesUnitId={0}salesUnitId");
                        dbParameters.Add(db.CreateDbParameter("salesUnitId", salesUnitId.Value));
                    }
                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                     ErrorStrings.Export_Title_PartsSalesOrder_Code, ErrorStrings.Export_Title_PartsSalesOrder_SalesUnitName, ErrorStrings.Export_Title_PartsSalesOrder_SalesUnitOwnerCompanyName, ErrorStrings.Export_Title_PartsSalesOrder_SubmitCompanyName, ErrorStrings.Export_Title_PartsPurchaseOrder_OrderType, ErrorStrings.Export_Title_PartsPurchaseOrder_IfDirectProvision, ErrorStrings.Export_Title_AccountPeriod_ModifyTime, ErrorStrings.Export_Title_PartsSalesOrder_IfCanNewPart, ErrorStrings.Export_Title_AccountPeriod_Status, ErrorStrings.Export_Title_PartsSalesOrder_InvoiceReceiveCompanyName, ErrorStrings.Export_Title_PartsPurchaseOrder_ReceivingCompanyName, ErrorStrings.Export_Title_PartsPurchaseOrder_ReceiveAddress, ErrorStrings.Export_Title_PartsPurchaseOrder_ShippingMethod, ErrorStrings.Export_Title_PartsSalesOrder_SalesActivityDiscountRate, ErrorStrings.Export_Title_PartsSalesOrder_SalesActivityDiscountAmount, ErrorStrings.Export_Title_PartsSalesOrder_TotalAmount, ErrorStrings.Export_Title_PartsSalesOrder_RequestedShippingTime, ErrorStrings.Export_Title_PartsPurchaseOrder_RequestedDeliveryTime, ErrorStrings.Export_Title_AccountPeriod_CreatorName, ErrorStrings.Export_Title_AccountPeriod_CreateTime, ErrorStrings.Export_Title_PackingPropertyApp_ApproverName, ErrorStrings.Export_Title_PackingPropertyApp_ApproveTime, ErrorStrings.Export_Title_PartsSalesOrder_SubmitterName, ErrorStrings.Export_Title_PartsSalesOrder_SubmitTime, ErrorStrings.Export_Title_PartsBranch_AbandonerName, ErrorStrings.Export_Title_PartsBranch_AbandonerTime, ErrorStrings.Export_Title_PartsBranch_Remark, ErrorStrings.Export_Title_PartsBranch_Code, ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName, ErrorStrings.Export_Title_PartsSalesOrder_OrderQty, ErrorStrings.Export_Title_PartsSalesReturnBill_ApproveQuantity, ErrorStrings.Export_Title_PartSstock_RetailGuidePrice, ErrorStrings.Export_Title_PartsSalesOrder_CustOrderPriceGradeCoefficient, ErrorStrings.Export_Title_PartsSalesOrder_DiscountedPrice, ErrorStrings.Export_Title_PartsSalesOrder_OrderPrice, ErrorStrings.Export_Title_PartsSalesOrder_OrderAmount, ErrorStrings.Export_Title_PartsSalesOrder_EstimatedFulfillTime, ErrorStrings.Export_Title_PartsPurchaseOrder_DetailRemark
                                 };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }

                            return null;
                        });
                    }
                    //ExportByRowWithHead(Path.Combine(AttachmentFolder, fileName), 1, index => {
                    //    if(index == 0) {
                    //        return new object[] {
                    //                 ErrorStrings.Export_Title_PartsSalesOrder_Code, "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ""
                    //             };
                    //    }
                    //    if(index == 1) {
                    //        return new object[] {
                    //                 ErrorStrings.Export_Title_PartsSalesOrder_Code, ErrorStrings.Export_Title_PartsSalesOrder_SalesUnitName, ErrorStrings.Export_Title_PartsSalesOrder_SalesUnitOwnerCompanyName, ErrorStrings.Export_Title_PartsSalesOrder_SubmitCompanyName, ErrorStrings.Export_Title_PartsPurchaseOrder_OrderType, ErrorStrings.Export_Title_PartsPurchaseOrder_IfDirectProvision, ErrorStrings.Export_Title_AccountPeriod_ModifyTime, ErrorStrings.Export_Title_PartsSalesOrder_IfCanNewPart, ErrorStrings.Export_Title_AccountPeriod_Status, ErrorStrings.Export_Title_PartsSalesOrder_InvoiceReceiveCompanyName, ErrorStrings.Export_Title_PartsPurchaseOrder_ReceivingCompanyName, ErrorStrings.Export_Title_PartsPurchaseOrder_ReceiveAddress, ErrorStrings.Export_Title_PartsPurchaseOrder_ShippingMethod, ErrorStrings.Export_Title_PartsSalesOrder_SalesActivityDiscountRate, ErrorStrings.Export_Title_PartsSalesOrder_SalesActivityDiscountAmount, ErrorStrings.Export_Title_PartsSalesOrder_TotalAmount, ErrorStrings.Export_Title_PartsSalesOrder_RequestedShippingTime, ErrorStrings.Export_Title_PartsPurchaseOrder_RequestedDeliveryTime, ErrorStrings.Export_Title_AccountPeriod_CreatorName, ErrorStrings.Export_Title_AccountPeriod_CreateTime, ErrorStrings.Export_Title_PackingPropertyApp_ApproverName, ErrorStrings.Export_Title_PackingPropertyApp_ApproveTime, ErrorStrings.Export_Title_PartsSalesOrder_SubmitterName, ErrorStrings.Export_Title_PartsSalesOrder_SubmitTime, ErrorStrings.Export_Title_PartsBranch_AbandonerName, ErrorStrings.Export_Title_PartsBranch_AbandonerTime, ErrorStrings.Export_Title_PartsBranch_Remark, ErrorStrings.Export_Title_PartsBranch_Code, ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName, ErrorStrings.Export_Title_PartsSalesOrder_OrderQty, ErrorStrings.Export_Title_PartsSalesReturnBill_ApproveQuantity, ErrorStrings.Export_Title_PartSstock_RetailGuidePrice, ErrorStrings.Export_Title_PartsSalesOrder_CustOrderPriceGradeCoefficient, "ErrorStrings.Export_Title_PartsSalesOrder_DiscountedPrice, ErrorStrings.Export_Title_PartsSalesOrder_OrderPrice, ErrorStrings.Export_Title_PartsSalesOrder_OrderAmount, ErrorStrings.Export_Title_PartsSalesOrder_EstimatedFulfillTime, ErrorStrings.Export_Title_PartsPurchaseOrder_DetailRemark
                    //             };
                    //    }

                    //    if(reader.Read()){
                    //          var values = new object[reader.FieldCount];
                    //          var num = reader.GetValues(values);
                    //if(num != reader.FieldCount) {
                    //    throw new Exception(ErrorStrings.Export_Validation_DataError);
                    //}
                    //          return values;
                    //    }

                    //    return null;
                    //});





                    reader.Close();
                }
                UpdateScheduleExportState(jobName, fileName);
                return true;
            } catch(Exception) {
                return false;
            }

        }

        public void ExportByRowWithHead(string path, int headRowNum, Func<int, Object[]> func) {
            var workbook = new HSSFWorkbook();
            var sheet = workbook.CreateSheet("Sheet1");
            var rowNum = 0;
            FileStream stream = null;
            try {
                while(true) {
                    var flag = rowNum == headRowNum - 1;//为false说明不是表头
                    var arrayOfObjects = func(rowNum);
                    if(arrayOfObjects == null) {
                        break;
                    }
                    var rowValues = new string[arrayOfObjects.Length];
                    for(var i = 0; i < arrayOfObjects.Length; i++) {
                        rowValues[i] = arrayOfObjects[i].ToString();
                    }
                    var row = sheet.CreateRow(rowNum);
                    for(var i = 0; i < rowValues.Length; i++) {
                        var tempValue = rowValues[i];
                        row.CreateCell(i).SetCellValue(tempValue);
                    }
                    if(flag) {
                        var cells = row.Cells;
                        var noValueCellCount = cells.Count(r => r.ToString() != "");
                        if(noValueCellCount == 1) {
                            var rangeAddress = new CellRangeAddress(rowNum, rowNum, 0, cells.Count - 1);
                            sheet.AddMergedRegion(rangeAddress);
                        } else if(noValueCellCount > 1) {
                            var m = 0;
                            for(var i = 0; i < cells.Count; i++) {
                                if(cells[i].ToString() != "") {
                                    var rangeAddress = new CellRangeAddress(rowNum, rowNum, m, i);
                                    sheet.AddMergedRegion(rangeAddress);
                                    m = i + 1;
                                }
                                if(i == cells.Count - 1 && m != i + 1) {
                                    var rangeAddress = new CellRangeAddress(rowNum, rowNum, m, i);
                                    sheet.AddMergedRegion(rangeAddress);
                                }

                            }
                        }
                    }
                    rowNum++;
                }
                stream = new FileStream(path, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                workbook.Write(stream);
                stream.Flush();
            } catch(Exception ex) {
                throw new Exception(ex.Message);
            } finally {
                if(stream != null) {
                    stream.Close();
                }
            }




        }

        //配件公司批量导出销售订单主清单
        public bool BranchExportPartsSalesOrderWhithDetails(int? personnelId, int? ownercompanyId, string code, int? orderType, string branchCode, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, string customerName, string customerCode, DateTime? submitTimeBegin, DateTime? submitTimeEnd, int? provinceId, string city, DateTime? stopTimeBegin, DateTime? stopTimeEnd, DateTime? approveTimeBegin, DateTime? approveTimeEnd, int? id, out string fileName) {
            fileName = GetExportFilePath("配件公司批量导出销售订单主清单.xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.AppendFormat(@"Select a.InvoiceReceiveCompanyCode,
                                       a.InvoiceReceiveCompanyName,
                                       a.Code,
                                       a.SalesCategoryName,
                                       b.name,
                                       a.Province,
                                       a.city,
                                       a.ReceivingCompanyCode,
                                       a.ReceivingCompanyName,
                                       c.value,
                                       a.PartsSalesOrderTypeName,
                                       (select value from keyvalueitem where NAME = 'IsOrNot'and key=a.IfDirectProvision) As IfDirectProvision,
                                       a.ModifyTime,
                                       (select value from keyvalueitem where NAME = 'PartsSalesOrder_Status'and key=a.Status) As Status,
                                       (select value from keyvalueitem where NAME = 'IsOrNot'and key=a.Isdebt) As Isdebt,
                                       a.Remark,
                                       a.TotalAmount,
                                       a.SubmitTime,
                                       a.StopComment,
                                       a.SalesUnitOwnerCompanyName,
                                       a.ReceivingAddress,
                                       (select value from keyvalueitem where NAME = 'PartsShipping_Method'and key=a.Shippingmethod) As Shippingmethod,
                                       a.RequestedDeliveryTime,
                                       a.RejectComment,
                                       a.StopComment,
                                       a.CreatorName,
                                       a.CreateTime,
                                       a.SubmitterName,
                                       a.ApproverName,
                                       a.ApproveTime,
                                       a.RejecterName,
                                       a.RejectTime,
                                       a.AbandonerName,
                                       a.AbandonTime,
                                       (select value from keyvalueitem where NAME = 'IsOrNot'and key=a.IsTransSuccess) As IsTransSuccess,
                                       d.SparePartCode,
                                       d.SparePartName,
                                       d.OrderedQuantity,
                                       d.ApproveQuantity,
                                       d.OrderPrice,
                                       d.ordersum,
                                       d.remark as detailRemark
                                  From PartsSalesOrder a
                                 inner join Warehouse b
                                    on a.WarehouseId = b.id
                                 inner join keyvalueitem c
                                    on c.name = 'Company_Type'
                                   and c.key = a.CustomerType
                                 inner join PartsSalesOrderDetail d
                                    on a.id = d.partssalesorderid where 1=1 ");
                    //1、如果人员Id不为空，过滤销售组织人员关系。
                    //    1)、销售组织人员关系.人员Id=参数：人员Id  返回值：销售组织人员
                    //          关系. 销售组织Id
                    //    2)、配件销售订单.销售组织.Id in 销售组织人员关系. 销售组织Id
                    //    如果人员Id为空，直接查询销售订单主清单数据。
                    //2、如果 隶属企业Id 不为空，过滤销售组织。
                    //    1） 销售组织.隶属企业Id= 参数： 隶属企业Id
                    //     2)、配件销售订单.销售组织.Id in 销售组织. Id

                    //3、根据传入的查询条件参数，过滤导出数据：
                    //    如果 分公司编号 不为空，则 配件销售订单.提报单位编号 = 参数：提报单位编号；

                    //    配件销售订单.状态 = 参数：单据状态
                    //    如果 创建开始日期  不为空，则 配件销售订单.创建时间 >= 参数：创建开始日期 ；
                    //    如果 创建结束日期   不为空，则 配件销售订单.创建时间 <= 参数：创建结束日期 ；
                    //    如果 订单类型  不为空，则 配件销售订单.配件销售订单类型Id= 参数：订单类型；
                    //    如果 客户编号 不为空，则 配件销售订单.收货单位编号 = 参数：提报单位编号；
                    //    如果 客户名称 不为空，则 配件销售订单.收货单位名称 = 参数：客户名称；

                    var dbParameters = new List<DbParameter>();
                    if(id.HasValue) {
                        sql.Append(" and a.id = {0}id");
                        dbParameters.Add(db.CreateDbParameter("id", id.Value));
                    } else {
                        if(personnelId.HasValue) {
                            sql.Append(@" and (Exists (Select 1 As C1
                                                  From Salesunitaffipersonnel Extent2
                                                 Where (Extent2.Personnelid = {0})
                                                   And (Extent2.Salesunitid = a.Salesunitid))) ");
                            dbParameters.Add(db.CreateDbParameter("Personnelid", personnelId.Value));
                        }
                        if(ownercompanyId.HasValue) {
                            sql.Append(@" and (Exists (select 1 As C2 
                                                   FROM SalesUnit Extent3
                                                   Where (Extent3.OwnerCompanyId = {0}) and (a.SalesUnitId = Extent3.Id)))");
                            dbParameters.Add(db.CreateDbParameter("OwnerCompanyId", ownercompanyId.Value));
                        }
                        if(!string.IsNullOrEmpty(branchCode)) {
                            sql.Append(@" and Lower(a.submitCompanyCode) like {0}submitCompanyCode ");
                            dbParameters.Add(db.CreateDbParameter("submitCompanyCode", "%" + branchCode.ToLower() + "%"));
                        }
                        if(status.HasValue) {
                            sql.Append(@" and a.status ={0}status");
                            dbParameters.Add(db.CreateDbParameter("status", status.Value));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(@" and a.createTime <=To_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(@" and a.createTime <=To_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                        if(orderType.HasValue) {
                            sql.Append(@" and a.PartsSalesOrderTypeId ={0}PartsSalesOrderTypeId");
                            dbParameters.Add(db.CreateDbParameter("PartsSalesOrderTypeId", orderType.Value));
                        }
                        if(!string.IsNullOrEmpty(customerCode)) {
                            sql.Append(@" and Lower(a.ReceivingCompanyCode) like {0}ReceivingCompanyCode ");
                            dbParameters.Add(db.CreateDbParameter("ReceivingCompanyCode", "%" + customerCode.ToLower() + "%"));
                        }
                        if(!string.IsNullOrEmpty(customerName)) {
                            sql.Append(@" and Lower(a.ReceivingCompanyName) like {0}ReceivingCompanyName ");
                            dbParameters.Add(db.CreateDbParameter("ReceivingCompanyName", "%" + customerName.ToLower() + "%"));
                        }
                    }
                    var sqlStr = sql.ToString();
                    if(sqlStr.IndexOf(" where 1=1  and ") > 0)
                        sqlStr = sqlStr.Replace(" where 1=1  and ", " where ");
                    else
                        sqlStr = sqlStr.Replace(" where 1=1 ", "");
                    var command = db.CreateDbCommand(string.Format(sqlStr, db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    //根据查询条件查询销售订单主清单数据。
                    // 4、返回销售订单主清单合并数据，返回字段：
                    //    主单：结算企业编号，结算企业名称，配件销售订单编号，品牌，订货仓库,省，市，客户编号，客户名称，客户属性，订单类型，
                    //    是否直供，修改时间，状态，是否欠款，备注，订单总金额，提交时间，终止原因，供货单位，收货地址，发运方式，要求到货时间，驳回原因，终止原因，创建人，
                    //    创建时间，提交人，审批人，审批时间，驳回人，驳回时间，作废人，作废时间，是否转化成功
                    //    清单：配件图号，配件名称，订货数据，满足数量，单价，订货金额，备注
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                     ErrorStrings.Export_Title_PartsSalesSettlement_CompanyCode, ErrorStrings.Export_Title_PartsSalesSettlement_CompanyName, ErrorStrings.Export_Title_PartsSalesOrder_Code, ErrorStrings.Export_Title_Partssalescategory_Name, ErrorStrings.Export_Title_PartsSalesOrder_WarehouseId, ErrorStrings.Export_Title_Agency_Province,ErrorStrings.Export_Title_Agency_City,ErrorStrings.Export_Title_Credenceapplication_CustomerCode,ErrorStrings.Export_Title_Credenceapplication_CustomerName, ErrorStrings.Export_Validation_Company_Type,ErrorStrings.Export_Title_PartsPurchaseOrder_OrderType, ErrorStrings.Export_Title_PartsPurchaseOrder_IfDirectProvision,ErrorStrings.Export_Title_AccountPeriod_ModifyTime, ErrorStrings.Export_Title_AccountPeriod_Status, ErrorStrings.Export_Title_PartsSalesOrder_IsDebt,ErrorStrings.Export_Title_PartsBranch_Remark, 
                                     ErrorStrings.Export_Title_PartsSalesOrder_TotalAmount,  ErrorStrings.Export_Title_PartsSalesOrder_SubmitTime, ErrorStrings.Export_Title_Partsoutboundplan_StopComment, ErrorStrings.Export_Title_PartsSalesOrder_SalesUnitOwnerCompanyName, ErrorStrings.Export_Title_PartsPurchaseOrder_ReceiveAddress, ErrorStrings.Export_Title_PartsPurchaseOrder_ShippingMethod, ErrorStrings.Export_Title_PartsPurchaseOrder_RequestedDeliveryTime, ErrorStrings.Export_Title_PartsOuterPurchaseChange_RejectComment, ErrorStrings.Export_Title_Partsoutboundplan_StopComment, ErrorStrings.Export_Title_AccountPeriod_CreatorName, ErrorStrings.Export_Title_AccountPeriod_CreateTime, ErrorStrings.Export_Title_PartsSalesOrder_SubmitterName, ErrorStrings.Export_Title_PackingPropertyApp_ApproverName, ErrorStrings.Export_Title_PackingPropertyApp_ApproveTime, 
                                     ErrorStrings.Export_Title_PartsOuterPurchaseChange_RejecterName,ErrorStrings.Export_Title_PartsOuterPurchaseChange_RejectTime,ErrorStrings.Export_Title_PartsBranch_AbandonerName,ErrorStrings.Export_Title_PartsBranch_AbandonerTime,ErrorStrings.Export_Title_PartsSalesSettlement_IsTurnSuccess,
                                     ErrorStrings.Export_Title_PartsBranch_Code, ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName, ErrorStrings.Export_Title_PartsSalesOrder_OrderQty, ErrorStrings.Export_Title_PartsSalesOrder_CurrentFulfilledQuantity, ErrorStrings.Export_Title_InternalAllocationBill_UnitPrice, ErrorStrings.Export_Title_PartsSalesOrder_OrderAmount, ErrorStrings.Export_Title_PartsBranch_Remark
                                 };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }

        //导出销售订单
        public bool ExportPartsSalesOrders(int? personnelId, int? ownercompanyId, string code, int? orderType, string branchCode, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, string customerName, string customerCode, DateTime? submitTimeBegin, DateTime? submitTimeEnd, int? provinceId, string city, DateTime? stopTimeBegin, DateTime? stopTimeEnd, DateTime? approveTimeBegin, DateTime? approveTimeEnd, int? id, out string fileName) {
            fileName = GetExportFilePath("导出销售订单.xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.AppendFormat(@"Select a.InvoiceReceiveCompanyCode,
                                       a.InvoiceReceiveCompanyName,
                                       a.Code,
                                       a.SalesCategoryName,
                                       b.name,
                                       a.Province,
                                       a.city,
                                       a.ReceivingCompanyCode,
                                       a.ReceivingCompanyName,
                                       c.value,
                                       a.PartsSalesOrderTypeName,
                                       (select value from keyvalueitem where NAME = 'IsOrNot'and key=a.IfDirectProvision) As IfDirectProvision,
                                       a.ModifyTime,
                                       (select value from keyvalueitem where NAME = 'PartsSalesOrder_Status'and key=a.Status) As Status,
                                       (select value from keyvalueitem where NAME = 'IsOrNot'and key=a.Isdebt) As Isdebt,
                                       a.Remark,
                                       a.TotalAmount,
                                       a.SubmitTime,
                                       a.StopComment,
                                       a.SalesUnitOwnerCompanyName,
                                       a.ReceivingAddress,
                                       (select value from keyvalueitem where NAME = 'PartsShipping_Method'and key=a.Shippingmethod) As Shippingmethod,
                                       a.RequestedDeliveryTime,
                                       a.RejectComment,
                                       a.StopComment,
                                       a.CreatorName,
                                       a.CreateTime,
                                       a.SubmitterName,
                                       a.ApproverName,
                                       a.ApproveTime,
                                       a.RejecterName,
                                       a.RejectTime,
                                       a.AbandonerName,
                                       a.AbandonTime,
                                       (select value from keyvalueitem where NAME = 'IsOrNot'and key=a.IsTransSuccess) As IsTransSuccess
                                  From PartsSalesOrder a
                                 inner join Warehouse b
                                    on a.WarehouseId = b.id
                                 inner join keyvalueitem c
                                    on c.name = 'Company_Type'
                                   and c.key = a.CustomerType where 1=1 ");
                    //1、如果人员Id不为空，过滤销售组织人员关系。
                    //    1)、销售组织人员关系.人员Id=参数：人员Id  返回值：销售组织人员
                    //          关系. 销售组织Id
                    //    2)、配件销售订单.销售组织.Id in 销售组织人员关系. 销售组织Id
                    //    如果人员Id为空，直接查询销售订单主清单数据。
                    //2、如果 隶属企业Id 不为空，过滤销售组织。
                    //    1） 销售组织.隶属企业Id= 参数： 隶属企业Id
                    //     2)、配件销售订单.销售组织.Id in 销售组织. Id

                    //3、根据传入的查询条件参数，过滤导出数据：
                    //    如果 分公司编号 不为空，则 配件销售订单.提报单位编号 = 参数：提报单位编号；

                    //    配件销售订单.状态 = 参数：单据状态
                    //    如果 创建开始日期  不为空，则 配件销售订单.创建时间 >= 参数：创建开始日期 ；
                    //    如果 创建结束日期   不为空，则 配件销售订单.创建时间 <= 参数：创建结束日期 ；
                    //    如果 订单类型  不为空，则 配件销售订单.配件销售订单类型Id= 参数：订单类型；
                    //    如果 客户编号 不为空，则 配件销售订单.收货单位编号 = 参数：提报单位编号；
                    //    如果 客户名称 不为空，则 配件销售订单.收货单位名称 = 参数：客户名称；

                    var dbParameters = new List<DbParameter>();
                    if(id.HasValue) {
                        sql.Append(" and a.id = {0}id");
                        dbParameters.Add(db.CreateDbParameter("id", id.Value));
                    } else {
                        if(personnelId.HasValue) {
                            sql.Append(@" and (Exists (Select 1 As C1
                                                  From Salesunitaffipersonnel Extent2
                                                 Where (Extent2.Personnelid = {0})
                                                   And (Extent2.Salesunitid = a.Salesunitid))) ");
                            dbParameters.Add(db.CreateDbParameter("Personnelid", personnelId.Value));
                        }
                        if(ownercompanyId.HasValue) {
                            sql.Append(@" and (Exists (select 1 As C2 
                                                   FROM SalesUnit Extent3
                                                   Where (Extent3.OwnerCompanyId = {0}) and (a.SalesUnitId = Extent3.Id)))");
                            dbParameters.Add(db.CreateDbParameter("OwnerCompanyId", ownercompanyId.Value));
                        }
                        if(!string.IsNullOrEmpty(branchCode)) {
                            sql.Append(@" and Lower(a.submitCompanyCode) like {0}submitCompanyCode ");
                            dbParameters.Add(db.CreateDbParameter("submitCompanyCode", "%" + branchCode.ToLower() + "%"));
                        }
                        if(status.HasValue) {
                            sql.Append(@" and a.status ={0}status");
                            dbParameters.Add(db.CreateDbParameter("status", status.Value));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(@" and a.createTime <=To_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(@" and a.createTime <=To_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                        if(orderType.HasValue) {
                            sql.Append(@" and a.PartsSalesOrderTypeId ={0}PartsSalesOrderTypeId");
                            dbParameters.Add(db.CreateDbParameter("PartsSalesOrderTypeId", orderType.Value));
                        }
                        if(!string.IsNullOrEmpty(customerCode)) {
                            sql.Append(@" and Lower(a.ReceivingCompanyCode) like {0}ReceivingCompanyCode ");
                            dbParameters.Add(db.CreateDbParameter("ReceivingCompanyCode", "%" + customerCode.ToLower() + "%"));
                        }
                        if(!string.IsNullOrEmpty(customerName)) {
                            sql.Append(@" and Lower(a.ReceivingCompanyName) like {0}ReceivingCompanyName ");
                            dbParameters.Add(db.CreateDbParameter("ReceivingCompanyName", "%" + customerName.ToLower() + "%"));
                        }
                    }
                    var sqlStr = sql.ToString();
                    if(sqlStr.IndexOf(" where 1=1  and ") > 0)
                        sqlStr = sqlStr.Replace(" where 1=1  and ", " where ");
                    else
                        sqlStr = sqlStr.Replace(" where 1=1 ", "");
                    var command = db.CreateDbCommand(string.Format(sqlStr, db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    //根据查询条件查询销售订单主清单数据。
                    // 4、返回销售订单主清单合并数据，返回字段：
                    //    主单：结算企业编号，结算企业名称，配件销售订单编号，品牌，订货仓库,省，市，客户编号，客户名称，客户属性，订单类型，
                    //    是否直供，修改时间，状态，是否欠款，备注，订单总金额，提交时间，终止原因，供货单位，收货地址，发运方式，要求到货时间，驳回原因，终止原因，创建人，
                    //    创建时间，提交人，审批人，审批时间，驳回人，驳回时间，作废人，作废时间，是否转化成功
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                     ErrorStrings.Export_Title_PartsSalesSettlement_CompanyCode, ErrorStrings.Export_Title_PartsSalesSettlement_CompanyName, ErrorStrings.Export_Title_PartsSalesOrder_Code, ErrorStrings.Export_Title_Partssalescategory_Name, ErrorStrings.Export_Title_PartsSalesOrder_WarehouseId, ErrorStrings.Export_Title_Agency_Province,ErrorStrings.Export_Title_Agency_City,ErrorStrings.Export_Title_Credenceapplication_CustomerCode,ErrorStrings.Export_Title_Credenceapplication_CustomerName, ErrorStrings.Export_Validation_Company_Type,ErrorStrings.Export_Title_PartsPurchaseOrder_OrderType, ErrorStrings.Export_Title_PartsPurchaseOrder_IfDirectProvision,ErrorStrings.Export_Title_AccountPeriod_ModifyTime, ErrorStrings.Export_Title_AccountPeriod_Status, ErrorStrings.Export_Title_PartsSalesOrder_IsDebt,ErrorStrings.Export_Title_PartsBranch_Remark, 
                                     ErrorStrings.Export_Title_PartsSalesOrder_TotalAmount,  ErrorStrings.Export_Title_PartsSalesOrder_SubmitTime, ErrorStrings.Export_Title_Partsoutboundplan_StopComment, ErrorStrings.Export_Title_PartsSalesOrder_SalesUnitOwnerCompanyName, ErrorStrings.Export_Title_PartsPurchaseOrder_ReceiveAddress, ErrorStrings.Export_Title_PartsPurchaseOrder_ShippingMethod, ErrorStrings.Export_Title_PartsPurchaseOrder_RequestedDeliveryTime, ErrorStrings.Export_Title_PartsOuterPurchaseChange_RejectComment, ErrorStrings.Export_Title_Partsoutboundplan_StopComment, ErrorStrings.Export_Title_AccountPeriod_CreatorName, ErrorStrings.Export_Title_AccountPeriod_CreateTime, ErrorStrings.Export_Title_PartsSalesOrder_SubmitterName, ErrorStrings.Export_Title_PackingPropertyApp_ApproverName, ErrorStrings.Export_Title_PackingPropertyApp_ApproveTime, 
                                     ErrorStrings.Export_Title_PartsOuterPurchaseChange_RejecterName,ErrorStrings.Export_Title_PartsOuterPurchaseChange_RejectTime,ErrorStrings.Export_Title_PartsBranch_AbandonerName,ErrorStrings.Export_Title_PartsBranch_AbandonerTime,"是否转化成功"
                                 };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }

        }
    }
}
