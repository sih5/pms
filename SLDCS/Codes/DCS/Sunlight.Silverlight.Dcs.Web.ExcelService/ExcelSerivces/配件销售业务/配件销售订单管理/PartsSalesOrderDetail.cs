﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        public bool ImportPartsSalesOrderDetail(string fileName, bool ifDirectProvision, int branchId, int partsSalesCategoryId, out int excelImportNum, out List<PartsSalesOrderDetailExtend> rightData, out List<PartsSalesOrderDetailExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<PartsSalesOrderDetailExtend>();
            var allList = new List<PartsSalesOrderDetailExtend>();
            List<PartsSalesOrderDetailExtend> rightList = null;
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("PartsSalesOrderDetail", out notNullableFields, out fieldLenght);

                List<object> excelColumns;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    #region 指定文件中的列对应的名称 例如
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Code, "SparePartCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName, "SparePartName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsSalesOrder_OrderQty, "OrderedQuantity");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsSalesOrder_EstimatedFulfillTime, "EstimatedFulfillTime", typeof(DateTime));
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Remark, "Remark");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsSalesOrder_OrderNo, "OrderNo");
                    #endregion

                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();

                    #region 获取对应枚举 var keyValuePairs = new[] { new KeyValuePair<string, string>("PartABC", "ABCStrategy_Category")}; tempExcelOperator.ImportHelper.LoadEnumFromDb(null,null, keyValuePairs);

                    #endregion

                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var tempImportObj = new PartsSalesOrderDetailExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        #region 根据导入的字段给对应的实体属性赋值
                        tempImportObj.SparePartCodeStr = newRow["SparePartCode"];
                        tempImportObj.SparePartNameStr = newRow["SparePartName"];
                        tempImportObj.OrderedQuantityStr = newRow["OrderedQuantity"];
                        tempImportObj.EstimatedFulfillTimeStr = newRow["EstimatedFulfillTime"];
                        tempImportObj.RemarkStr = newRow["Remark"];
                        tempImportObj.OrderNoStr = newRow["OrderNo"];
                        #endregion

                        var tempErrorMessage = new List<string>();

                        #region 导入的数据基本检查
                        //配件图号检查
                        var fieldIndex = notNullableFields.IndexOf("SparePartCode".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.SparePartCodeStr)) {
                            if(fieldIndex > -1) {
                                tempErrorMessage.Add(ErrorStrings.OverstockPartsAppDetail__Validation1);
                            }
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.SparePartCodeStr) > fieldLenght["SparePartCode".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.ImpPartsSupplierRelation_Validation8);
                        }
                        //配件名称检查
                        if(!string.IsNullOrEmpty(tempImportObj.SparePartNameStr)) {
                            if(Encoding.Default.GetByteCount(tempImportObj.SparePartNameStr) > fieldLenght["SparePartName".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Sparepart_NameIsLong);
                        }

                        //订货数量检查
                        fieldIndex = notNullableFields.IndexOf("OrderedQuantity".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.OrderedQuantityStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsSalesOrder_OrderQtyNull);
                        } else {
                            int checkValue;
                            if(int.TryParse(tempImportObj.OrderedQuantityStr, out checkValue)) {
                                if(checkValue <= 0) {
                                    tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsSalesOrder_OrderQtyOverZero);
                                } else {
                                    tempImportObj.OrderedQuantity = checkValue;
                                }

                            } else {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsSalesOrder_OrderQtyOverInteger);
                            }
                        }

                        //预计满足时间检查
                        fieldIndex = notNullableFields.IndexOf("EstimatedFulfillTime".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.EstimatedFulfillTimeStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsSalesOrder_EstimatedFulfillTimeNull);
                        } else {
                            DateTime checkValue;
                            if(!DateTime.TryParse(tempImportObj.EstimatedFulfillTimeStr, out checkValue)) {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsSalesOrder_EstimatedFulfillTimeInteger);
                            } else {
                                tempImportObj.EstimatedFulfillTime = checkValue;
                            }
                        }
                        #endregion

                        if(tempErrorMessage.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });
                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    #region 剩下的数据进行业务检查
                    //文件内数据重复
                    var groups = tempRightList.GroupBy(r => new {
                        r.SparePartCodeStr
                    }).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        groupItem.ErrorMsg = ErrorStrings.Export_Validation_PartsPurchaseOrder_Validation2;
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    //1、配件有效性校验（配件信息.配件编号=导入数据.配件图号），
                    //如果校验不通过，记录校验错误信息：配件不存在。
                    var sparePartCodesNeedCheck = tempRightList.Select(r => r.SparePartCodeStr).Distinct().ToArray();
                    var dbSpareParts = new List<SparePartExtend>();
                    Func<string[], bool> getDbSpareParts = value => {
                        var dbObj = new SparePartExtend {
                            Id = Convert.ToInt32(value[0]),
                            Code = value[1],
                            Name = value[2],
                            MeasureUnit = value[3]
                        };
                        dbSpareParts.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator(string.Format("Select Id,Code,Name,MeasureUnit From SparePart Where Status = {0}", (int)DcsMasterDataStatus.有效), "Code", true, sparePartCodesNeedCheck, getDbSpareParts);
                    var errorSpareParts = tempRightList.Where(r => dbSpareParts.All(v => v.Code != r.SparePartCodeStr)).ToList();
                    foreach(var errorItem in errorSpareParts) {
                        errorItem.ErrorMsg = ErrorStrings.Export_Validation_InternalAcquisitionDetail_Validation1;
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    foreach(var rightItem in tempRightList) {
                        var tempSparePart = dbSpareParts.Single(r => r.Code == rightItem.SparePartCodeStr);
                        rightItem.SparePartId = tempSparePart.Id;
                        rightItem.SparePartCode = tempSparePart.Code;
                        rightItem.SparePartName = tempSparePart.Name;
                        rightItem.MeasureUnit = tempSparePart.MeasureUnit;
                    }

                    //2、校验配件是否可销售（配件营销信息.配件Id=配件信息.id 
                    //and 配件营销信息.营销分公司id = 参数：营销分公司id 
                    //and 配件营销信息.配件销售类型id = 参数：配件销售类型id 
                    //and 配件营销信息.是否可销售=ture）,校验不通过：配件不可销售。

                    //3、如果参数：是否直供 不为空，
                    //则校验配件是否可直供（配件营销信息.配件Id=配件信息.id 
                    //and 配件营销信息.营销分公司id = 参数：营销分公司id
                    //and 配件营销信息.配件销售类型id = 参数：配件销售类型id 
                    //and 配件营销信息.是否可直供=参数：是否直供）,
                    //校验不通过：配件是否直供不满足要求。
                    var sparePartIdsNeedCheck = tempRightList.Select(r => r.SparePartId.ToString(CultureInfo.InvariantCulture)).Distinct().ToArray();
                    var dbPartsBranchs = new List<PartsBranchExtend>();
                    Func<string[], bool> getDbPartsBranchs = value => {
                        var dbObj = new PartsBranchExtend {
                            PartId = Convert.ToInt32(value[0]),
                            BranchId = Convert.ToInt32(value[1]),
                            PartsSalesCategoryId = Convert.ToInt32(value[2]),
                            IsSalable = value[3] != "0",
                            IsDirectSupply = value[4] != "0"
                        };
                        dbPartsBranchs.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator(string.Format("Select PartId,BranchId,PartsSalesCategoryId,IsSalable,IsDirectSupply From PartsBranch Where Status = {0}", (int)DcsBaseDataStatus.有效), "PartId", true, sparePartIdsNeedCheck, getDbPartsBranchs);

                    string[] brandId = {
                        branchId.ToString()
                    };
                    var dbCustomerDirectSpareListExtends = new List<CustomerDirectSpareListExtend>();
                    Func<string[], bool> getDbCustomerDirectSpareLists = value => {
                        var dbDirect = new CustomerDirectSpareListExtend {
                            PartsSalesCategoryId = Convert.ToInt32(value[0]),
                            CustomerId = Convert.ToInt32(value[1]),
                            SparePartId = Convert.ToInt32(value[2])
                        };
                        dbCustomerDirectSpareListExtends.Add(dbDirect);
                        return false;
                    };
                    db.QueryDataWithInOperator(string.Format("Select PartsSalesCategoryId,CustomerId,SparePartId From CustomerDirectSpareList Where Status = {0} and PartsSalesCategoryId ={1} ", (int)DcsBaseDataStatus.有效, partsSalesCategoryId), "CustomerId", true, brandId, getDbCustomerDirectSpareLists);
                    
                    var errorIsSalables = tempRightList.Where(r => !dbPartsBranchs.Any(v => v.PartId == r.SparePartId  && v.PartsSalesCategoryId == partsSalesCategoryId && v.IsSalable)).ToList();
                    foreach(var errorItem in errorIsSalables) {
                        errorItem.ErrorMsg = ErrorStrings.Export_Validation_PartsSalesOrder_Validation1;
                    }

                    if(ifDirectProvision == true) {
                        var errorIsDirectSupplys = tempRightList.Where(r => !dbPartsBranchs.Any(v => dbCustomerDirectSpareListExtends.Any(c => c.SparePartId == v.PartId && c.SparePartId == r.SparePartId) && v.PartsSalesCategoryId == partsSalesCategoryId && v.IsDirectSupply)).ToList();
                        foreach(var errorItem in errorIsDirectSupplys) {
                            errorItem.ErrorMsg = ErrorStrings.Export_Validation_PartsSalesOrder_Validation2;
                        }
                    }
                    if(ifDirectProvision == false) {
                        var errorIsDirectSupplys = tempRightList.Where(r => dbPartsBranchs.Any(v => v.PartsSalesCategoryId == partsSalesCategoryId && v.IsDirectSupply == true && dbCustomerDirectSpareListExtends.Any(c => c.SparePartId == v.PartId && c.SparePartId==r.SparePartId))).ToList();
                        foreach(var errorItem in errorIsDirectSupplys) {
                            errorItem.ErrorMsg = ErrorStrings.Export_Validation_PartsSalesOrder_Validation2;
                        }
                    }

                    //获取所有不合格数据
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    //获取所有合格数据
                    rightList = allList.Except(errorList).ToList();
                    #region 将合格数据的值填上
                    foreach(var rightItem in rightList) {
                        rightItem.Remark = rightItem.RemarkStr;
                        rightItem.OrderNo = rightItem.OrderNoStr;
                        rightItem.IfCanNewPart = true;
                    }
                    #endregion
                    #endregion
                }
                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempObj = list[index - 1];
                            var values = new object[] {
                                #region 设置错误信息导出的列的值
                                tempObj.SparePartCodeStr, tempObj.SparePartNameStr,
                                 tempObj.OrderedQuantityStr, tempObj.EstimatedFulfillTimeStr,
                                tempObj.RemarkStr,tempObj.OrderNoStr, tempObj.ErrorMsg
                                
                                #endregion
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }
    }
}
