using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Entities;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        /// <summary>
        /// 批量导出销售退货单主清单
        /// </summary>
        public bool ExportPartsSalesReturnBillWithDetail(int? id, int? personnelId, string returnCompanyName, int? returnCompanyId, int? ownerCompanyId, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, int? returnType, string salesUnitName, int inType, int personnelOrCompanyId, string eRPOrderCode, string code,string partsSalesOrderCode, out string fileName) {
            fileName = GetExportFilePath("配件销售退货主清单_" + ".xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {

                    conn.Open();
                    var sql = new StringBuilder();
                    sql.Append(@"select DISTINCT   a.Code,
                                           a.ReturnCompanyName,
                                           a.TotalAmount,
                                           --de.ApproveAmount,
                                           a.SalesUnitName,                                           
                                           (select value from keyvalueitem where NAME = 'IsOrNot'and key=a.isbarter) As Isdebt,
                                           a.WarehouseName,
                                           a.AbandonerName,
                                           a.AbandonTime,
                                           (select value from keyvalueitem where NAME = 'PartsSalesReturnBill_Status'and key=a.Status) As Status,
                                           a.serviceApplyCode,
                                           a.CreatorName,
                                           a.CreateTime,
                                           a.ModifierName,
                                           a.ModifyTime,
                                           a.InitialApproverName,
                                           a.InitialApproveTime,
                                           a.ApproverName,
                                           a.ApproveTime,
                                           a.FinalApproverName,
                                           a.FinalApproveTime,
                                           (select value from keyvalueitem where NAME = 'PartsSalesReturn_ReturnType'and key=a.ReturnType) As ReturnType,
                                           a.ReturnReason,
                                           a.Remark,
                                           b.SparePartCode,
                                           b.SparePartName,
                                           b.MeasureUnit,
                                           b.ReturnedQuantity,
                                           b.ApproveQuantity,
                                           b.ReturnPrice,
                                           b.DiscountRate,
                                           b.PartsSalesOrderCode,
                                           g.ERPSourceOrderCode,
                                           b.Remark as DetailRemark,
                                           (select value from keyvalueitem where NAME = 'TraceProperty'and key=b.TraceProperty) As TraceProperty,
                                           b.SIHLabelCode
                                      from PartsSalesReturnBill a                                    
                                     inner join (select d.partssalesreturnbillid,sum(d.returnprice * d.approvequantity) as ApproveAmount  from partssalesreturnbilldetail d group by d.partssalesreturnbillid) de on a.id=de.partssalesreturnbillid
                                     inner join PartsSalesReturnBillDetail b on a.id = b.partssalesreturnbillid 
                                     Left Outer Join PartsSalesOrder g On b.PartsSalesOrderId = g.Id 
                                     where 1=1 ");
                    var dbParameters = new List<DbParameter>();
                    if(id.HasValue) {
                        sql.Append(" and a.id = {0}id");
                        dbParameters.Add(db.CreateDbParameter("id", id.Value));
                    } else {
                        if(personnelId.HasValue) {
                            sql.Append(@" and c.personnelId={0}personnelId");
                            dbParameters.Add(db.CreateDbParameter("personnelId", personnelId.Value));
                        }
                        if(ownerCompanyId.HasValue) {
                            sql.Append(@" and d.ownerCompanyId={0}ownerCompanyId");
                            dbParameters.Add(db.CreateDbParameter("ownerCompanyId", ownerCompanyId.Value));
                        }

                        if(returnCompanyId.HasValue) {
                            sql.Append(@" and a.returnCompanyId={0}returnCompanyId");
                            dbParameters.Add(db.CreateDbParameter("returnCompanyId", returnCompanyId.Value));
                        }
                        if(!string.IsNullOrEmpty(salesUnitName)) {
                            sql.Append(@" and exists (select 1 from PartsSalesReturnBillDetail pp where pp.PartsSalesReturnBillId=a.id and pp.PartsSalesOrderCode like{0}salesUnitName )");
                            dbParameters.Add(db.CreateDbParameter("salesUnitName", "%" + salesUnitName + "%"));
                        }
                        if(!string.IsNullOrEmpty(code)) {
                            sql.Append(@" and a.Code like {0}code ");
                            dbParameters.Add(db.CreateDbParameter("code", "%" + code + "%"));
                        }
                        if(!string.IsNullOrEmpty(eRPOrderCode)) {
                            sql.Append(@" and g.ERPSourceOrderCode like {0}eRPOrderCode ");
                            dbParameters.Add(db.CreateDbParameter("eRPOrderCode", "%" + eRPOrderCode + "%"));
                        }
                        if(!string.IsNullOrEmpty(returnCompanyName)) {
                            sql.Append(@" and a.returnCompanyName like {0}returnCompanyName ");
                            dbParameters.Add(db.CreateDbParameter("returnCompanyName", "%" + returnCompanyName + "%"));
                            //dbParameters.Add(db.CreateDbParameter("returnCompanyName", returnCompanyName));
                        }
                        if(!string.IsNullOrEmpty(partsSalesOrderCode)) {
                            sql.Append(@" and EXISTS(select 1 from PartsSalesReturnBillDetail pd where pd.PartsSalesReturnBillId=a.id and pd.PartsSalesOrderCode like {0}partsSalesOrderCode ) ");
                            dbParameters.Add(db.CreateDbParameter("partsSalesOrderCode", "%" + partsSalesOrderCode + "%"));
                            //dbParameters.Add(db.CreateDbParameter("returnCompanyName", returnCompanyName));
                        }
                        if(status.HasValue) {
                            sql.Append(@" and a.status ={0}Status");
                            dbParameters.Add(db.CreateDbParameter("status", status.Value));
                        }
                        if(createTimeBegin.HasValue) {
                            sql.Append(@" and a.createTime >=To_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(@" and a.createTime <=To_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                        if(returnType.HasValue) {
                            sql.Append(@" and a.returnType={0}returnType");
                            dbParameters.Add(db.CreateDbParameter("returnType", returnType.Value));
                        }

                        if(inType == 1) {
                            sql.Append(@" and exists (select 1 from salesunitaffipersonnel c where  a.salesunitid = c.salesunitid and c.personnelid={0}PersonnelOrCompanyId)");
                            dbParameters.Add(db.CreateDbParameter("PersonnelOrCompanyId", personnelOrCompanyId));
                        } else {
                            sql.Append(@" and a.SubmitCompanyId={0}PersonnelOrCompanyId");
                            dbParameters.Add(db.CreateDbParameter("PersonnelOrCompanyId", personnelOrCompanyId));
                        }
                    }
                    var sqlStr = sql.ToString();
                    if(sqlStr.IndexOf(" where 1=1  and ") > 0)
                        sqlStr = sqlStr.Replace(" where 1=1  and ", " where ");
                    else
                        sqlStr = sqlStr.Replace(" where 1=1 ", "");
                    var command = db.CreateDbCommand(string.Format(sqlStr, db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    ErrorStrings.Export_Title_PackingPropertyApp_Code,ErrorStrings.Export_Title_PartsSalesReturnBill_ReturnCompanyName,ErrorStrings.Export_Title_PartsSalesReturnBill_TotalAmount,ErrorStrings.Export_Title_PartsSalesOrder_SalesUnitName,ErrorStrings.Export_Title_PartsSalesReturnBill_IsBarter,
                                    ErrorStrings.Export_Title_PartsSalesReturnBill_WarehouseName,ErrorStrings.Export_Title_PartsBranch_AbandonerName,ErrorStrings.Export_Title_PartsBranch_AbandonerTime,ErrorStrings.Export_Title_AccountPeriod_Status,ErrorStrings.Export_Title_PartsSalesReturnBill_ServiceApplyCode,ErrorStrings.Export_Title_AccountPeriod_CreatorName,ErrorStrings.Export_Title_AccountPeriod_CreateTime,
                                    ErrorStrings.Export_Title_AccountPeriod_ModifierName,ErrorStrings.Export_Title_AccountPeriod_ModifyTime,ErrorStrings.Export_Title_PartsPurReturnOrder_InitialApproverName,ErrorStrings.Export_Title_PartsPurReturnOrder_InitialApproveTime,ErrorStrings.Export_Title_Dealerpartsinventorybill_ApproverName,ErrorStrings.Export_Title_Dealerpartsinventorybill_ApproverTime,ErrorStrings.Export_Title_PackingPropertyApp_ApproverName,ErrorStrings.Export_Title_PackingPropertyApp_ApproveTime,ErrorStrings.Export_Title_PartsInboundCheckBill_ReturnType,ErrorStrings.Export_Title_PartsPurReturnOrder_Reason,ErrorStrings.Export_Title_PartsBranch_Remark,ErrorStrings.Export_Title_PartsBranch_Code,
                                    ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName,ErrorStrings.Export_Title_PackingPropertyApp_MeasureUnit,ErrorStrings.Export_Title_PartsSalesReturnBill_ReturnedQuantity,ErrorStrings.Export_Title_PartsSalesReturnBill_ApproveQuantity,ErrorStrings.Export_Title_PartsSalesReturnBill_ReturnPrice,ErrorStrings.Export_Title_PartsSalesReturnBill_DiscountRate,ErrorStrings.Export_Title_PartsSalesOrder_Code,ErrorStrings.Export_Title_PartsSalesReturnBill_ServiceCode,ErrorStrings.Export_Title_PartsPurchaseOrder_DetailRemark,
                                    "追溯属性","标签码"
                                 };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }

                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }

        /// <summary>
        /// 校验销售退货导入配件清单
        /// </summary>
        public bool ImportPartsSalesReturnBillDetail(string fileName, out int excelImportNum, int companyId, int? warehouseId, int returnType, int branchId, int salesUnitId, int partsSalesCategoryId, int companyTypes,out List<PartsSalesReturnBillDetailExtend> rightData, out List<PartsSalesReturnBillDetailExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<PartsSalesReturnBillDetailExtend>();
            var rightList = new List<PartsSalesReturnBillDetailExtend>();
            var allList = new List<PartsSalesReturnBillDetailExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("PartsSalesReturnBillDetail", out notNullableFields, out fieldLenght);
                List<object> excelColumns;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    #region 指定文件中的列对应的名称 例如
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Code, "SparePartCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName, "SparePartName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsSalesReturnBill_ReturnedQuantity, "ReturnedQuantity");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsSalesOrder_Code, "PartsSalesOrderCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Remark, "Remark");
                    excelOperator.AddColumnDataSource("标签码", "SIHLabelCode");
                    #endregion
                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();

                    #region 获取对应枚举 var keyValuePairs = new[] { new KeyValuePair<string, string>("PartABC", "ABCStrategy_Category")}; tempExcelOperator.ImportHelper.LoadEnumFromDb(null,null, keyValuePairs);

                    #endregion
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var tempImportObj = new PartsSalesReturnBillDetailExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        #region 根据导入的字段给对应的实体属性赋值
                        tempImportObj.SparePartCodeStr = newRow["SparePartCode"];
                        tempImportObj.SparePartNameStr = newRow["SparePartName"];
                        tempImportObj.ReturnedQuantityStr = newRow["ReturnedQuantity"];
                        tempImportObj.PartsSalesOrderCode = newRow["PartsSalesOrderCode"];
                        tempImportObj.RemarkStr = newRow["Remark"];
                        tempImportObj.SIHLabelCodeStr = newRow["SIHLabelCode"];
                        #endregion

                        var tempErrorMessage = new List<string>();
                        #region 导入的数据基本检查
                        //配件编号检查
                        var fieldIndex = notNullableFields.IndexOf("SparePartCode".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.SparePartCodeStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.OverstockPartsAppDetail__Validation1);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.SparePartCodeStr) > fieldLenght["SparePartCode".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.ImpPartsSupplierRelation_Validation8);
                        }
                        //配件名称检查
                        //fieldIndex = notNullableFields.IndexOf("SparePartName".ToUpper());
                        //if(string.IsNullOrEmpty(tempImportObj.SparePartNameStr)) {
                        //    if(fieldIndex > -1)
                        //        tempErrorMessage.Add(ErrorStrings.Export_Validation_Sparepart_Name);
                        //} else {
                        //    if(Encoding.Default.GetByteCount(tempImportObj.SparePartNameStr) > fieldLenght["SparePartName".ToUpper()])
                        //        tempErrorMessage.Add(ErrorStrings.Export_Validation_Sparepart_NameIsLong);
                        //}
                        //配件销售订单编号
                        if(string.IsNullOrEmpty(tempImportObj.PartsSalesOrderCode)) {
                            tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsSalesReturnBill_Validation1);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.PartsSalesOrderCode) > fieldLenght["PartsSalesOrderCode".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsSalesReturnBill_Validation2);
                        }
                        //订货数量检查
                        fieldIndex = notNullableFields.IndexOf("ReturnedQuantity".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.ReturnedQuantityStr) ) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsSalesReturnBill_Validation3);
                        } else {
                            int checkValue;
                            if(int.TryParse(tempImportObj.ReturnedQuantityStr, out checkValue)) {
                                if(checkValue <= 0) {
                                    tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsSalesReturnBill_Validation4);
                                }
                            } else {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsSalesReturnBill_Validation5);
                            }
                        }
                        //备注检查
                        fieldIndex = notNullableFields.IndexOf("Remark".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.RemarkStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchasePricingDetail_Validation15);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.RemarkStr) > fieldLenght["Remark".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchasePricingDetail_Validation16);
                        }
                        #endregion
                        if(tempErrorMessage.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });
                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    #region 剩下的数据进行业务检查
                    //配件编号,配件销售订单编号 重复
                    var sparePartCodesGroup = tempRightList.GroupBy(r => new {
                        r.SparePartCodeStr,
                        r.PartsSalesOrderCode
                    });
                    foreach(var group in sparePartCodesGroup) {
                        if(group.Count() > 1) {
                            foreach(var item in group) {
                                item.ErrorMsg = ErrorStrings.Export_Validation_PartsSalesReturnBill_Validation6;
                            }
                        }
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    //配件有效性
                    var sparePartCodesNeedCheck = tempRightList.Select(r => r.SparePartCodeStr.ToUpper()).Distinct().ToArray();
                    var dbSparePartCodes = new List<SparePartExtend>();
                    Func<string[], bool> getDbSparePartCodes = value => {
                        var dbObj = new SparePartExtend {
                            Id = Convert.ToInt32(value[0]),
                            Code = value[1],
                            Name = value[2],
                            MeasureUnit = value[3],
                            TraceProperty=Convert.ToInt32(value[4])
                        };
                        dbSparePartCodes.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select id,Trim(Code) as Code,Trim(Name) as Name,MeasureUnit,nvl(TraceProperty,0) from sparePart where status=1 ", "Code", true, sparePartCodesNeedCheck, getDbSparePartCodes);
                    foreach(var tempRight in tempRightList) {
                        var sparePart = dbSparePartCodes.FirstOrDefault(v => v.Code == tempRight.SparePartCodeStr);
                        if(sparePart == null) {
                            tempRight.ErrorMsg = ErrorStrings.Export_Validation_InternalAcquisitionDetail_Validation1;
                            continue;
                        }
                        tempRight.SparePartId = sparePart.Id;
                        tempRight.SparePartName = sparePart.Name;
                        tempRight.MeasureUnit = sparePart.MeasureUnit;
                        tempRight.TraceProperty = sparePart.TraceProperty;
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    #region 特殊退货逻辑
                    if(returnType == (int)DcsPartsSalesReturnBillReturnType.特殊退货) {
                        //获取登录企业类型
                        var dbCompanyType = new List<int>();
                        Func<string[], bool> getDbCompanyType = value => {
                            dbCompanyType.Add(Convert.ToInt32(value[0]));
                            return false;
                        };
                        var sql = string.Format("Select Type From Company Where Id={0}", companyId);
                        db.QueryDataWithInOperator(sql, "1", false, new[] { "1" }, getDbCompanyType);
                        var companyType = dbCompanyType.FirstOrDefault();
                        if(companyType == (int)DcsCompanyType.服务站) {
                            //查询经销商配件库存
                            var sparePartIds = tempRightList.Select(r => r.SparePartId.ToString(CultureInfo.InvariantCulture)).Distinct().ToArray();
                            var dbDealerPartsStocks = new List<DealerPartsStockExtend>();
                            Func<string[], bool> getDbDealerPartsStocks = value => {
                                dbDealerPartsStocks.Add(new DealerPartsStockExtend {
                                    SparePartId = Convert.ToInt32(value[0]),
                                    Quantity = Convert.ToInt32(value[1])
                                });
                                return false;
                            };
                            var sql1 = string.Format("Select SparePartId,Quantity From DealerPartsStock Where DealerId={0} And SalesCategoryId={1} And SubDealerId=-1", companyId, partsSalesCategoryId);
                            db.QueryDataWithInOperator(sql1, "SparePartId", false, sparePartIds, getDbDealerPartsStocks);
                            foreach(var tempRight in tempRightList) {
                                var dealerPartsStocks = dbDealerPartsStocks.FirstOrDefault(v => v.SparePartId == tempRight.SparePartId);
                                if(dealerPartsStocks == null) {
                                    tempRight.ErrorMsg = ErrorStrings.Export_Validation_PartsSalesReturnBill_Validation7;
                                } else if(Convert.ToInt32(tempRight.ReturnedQuantityStr) > dealerPartsStocks.Quantity) {
                                    tempRight.ErrorMsg = ErrorStrings.Export_Validation_PartsSalesReturnBill_Validation8;
                                }
                            }
                        }
                        if(companyType == (int)DcsCompanyType.代理库 || companyType == (int)DcsCompanyType.服务站兼代理库) {
                            //查询配件可用库存
                            var sparePartIds = tempRightList.Select(r => r.SparePartId.ToString(CultureInfo.InvariantCulture)).Distinct().ToArray();
                            var dbPartsStocks = new List<PartsStockExtend>();
                            Func<string[], bool> getDbPartsStocks = value => {
                                dbPartsStocks.Add(new PartsStockExtend {
                                    PartId = Convert.ToInt32(value[0]),
                                    Quantity = Convert.ToInt32(value[1])
                                });
                                return false;
                            };
                            var sql1 = string.Format(@"Select T1.Partid,
                                                       (T1.Quantity - Nvl(Congelationstockqty, 0) - Nvl(Disabledstock, 0) -
                                                       Nvl(Lockedquantity, 0)) Quantity
                                                  From (Select t.Partid,
                                                               t.Warehouseid,
                                                               t.Storagecompanyid,
                                                               t.Branchid,
                                                               Sum(t.Quantity) Quantity
                                                          From (Select *
                                                                  From Partsstock
                                                                 Where Warehouseareacategoryid In
                                                                       (Select Id
                                                                          From Warehouseareacategory
                                                                         Where Category = 1
                                                                            Or Category = 3)) t
                                                         Group By t.Warehouseid, t.Storagecompanyid, t.Branchid, t.Partid
                                                        Having t.Storagecompanyid = {0}", companyId);
                            sql1 += warehouseId == null ? ") T1 \r\n" : string.Format(" And t.Warehouseid = {0}) T1\r\n", warehouseId);
                            sql1 += string.Format(@"Left Join (Select Branchid,
                                                                    Storagecompanyid,
                                                                    Warehouseid,
                                                                    Partid,
                                                                    Sum(Lockedquantity) Lockedquantity
                                                               From Partslockedstock
                                                              Group By Branchid, Storagecompanyid, Warehouseid, Partid) T2
                                                    On T1.Warehouseid = T2.Warehouseid
                                                   And T1.Storagecompanyid = T2.Storagecompanyid
                                                   And T1.Branchid = T2.Branchid
                                                   And T1.Partid = T2.Partid
                                                  Left Join (Select Branchid,
                                                                    Storagecompanyid,
                                                                    Warehouseid,
                                                                    Sparepartid Partid,
                                                                    Sum(Congelationstockqty) Congelationstockqty,
                                                                    Sum(Disabledstock) Disabledstock
                                                               From Wmscongelationstockview
                                                              Group By Branchid, Storagecompanyid, Warehouseid, Sparepartid
                                                             Having Storagecompanyid = {0}) T3
                                                    On T1.Warehouseid = T3.Warehouseid
                                                   And T1.Storagecompanyid = T3.Storagecompanyid
                                                   And T1.Branchid = T3.Branchid
                                                   And T1.Partid = T3.Partid", companyId);
                            db.QueryDataWithInOperator(sql1, "T1.Partid", false, sparePartIds, getDbPartsStocks);
                            foreach(var tempRight in tempRightList) {
                                var partsStocks = dbPartsStocks.FirstOrDefault(v => v.PartId == tempRight.SparePartId);
                                if(partsStocks == null) {
                                    tempRight.ErrorMsg = ErrorStrings.Export_Validation_PartsSalesReturnBill_Validation9;
                                } else if(Convert.ToInt32(tempRight.ReturnedQuantityStr) > partsStocks.Quantity) {
                                    tempRight.ErrorMsg = ErrorStrings.Export_Validation_PartsSalesReturnBill_Validation10;
                                }
                            }
                        }
                    }
                    #endregion

                    var partsSalesOrderCodesNeedCheck = tempRightList.Select(r => r.PartsSalesOrderCode.ToUpper()).Distinct().ToArray();

                    var dbPartsSalesOrderExtends = new List<PartsSalesOrderExtend>();
                    Func<string[], bool> getDbPartsSalesOrders = value => {
                        var dbPartsSalesOrderExtend = new PartsSalesOrderExtend {
                            Id = Convert.ToInt32(value[0]),
                            Code = value[1],
                            PartsOutboundBillId = Convert.ToInt32(value[2]),
                            SparePartId = Convert.ToInt32(value[3]),
                            SparePartCode = value[4],
                            SparePartName = value[5]
                        };
                        dbPartsSalesOrderExtends.Add(dbPartsSalesOrderExtend);
                        return false;
                    };
                    db.QueryDataWithInOperator(string.Format(@"SELECT DISTINCT Extent1.Id,Extent1.Code ,Extent2.Id as PartsOutboundBillId,Extent3.Sparepartid,Extent3.SparePartCode,Extent3.SparePartName FROM  PartsSalesOrder Extent1 INNER JOIN PartsOutboundBill Extent2 ON Extent1.Id = Extent2.OriginalRequirementBillId inner join PartsSalesOrderDetail Extent3 on Extent1.Id = Extent3.PartsSalesOrderId  inner join partsoutboundbilldetail pob on pob.partsoutboundbillid = extent2.id and pob.sparepartcode = extent3.sparepartcode WHERE (((Extent1.Status IN (4,5,6)) AND (Extent2.OutboundType <> 4)) AND (Extent2.OriginalRequirementBillType = 1)) AND ((((Extent1.SubmitCompanyId = {0}) AND (Extent1.SalesUnitId = {1})))) ", companyId, salesUnitId), "Extent1.Code", true, partsSalesOrderCodesNeedCheck, getDbPartsSalesOrders);
                    foreach(var tempRight in tempRightList) {
                        var partsSalesOrder = dbPartsSalesOrderExtends.FirstOrDefault(v => v.Code == tempRight.PartsSalesOrderCode && v.SparePartCode == tempRight.SparePartCodeStr);
                        if(partsSalesOrder == null) {
                            tempRight.ErrorMsg = ErrorStrings.Export_Validation_PartsSalesReturnBill_Validation11;
                            continue;
                        }
                        tempRight.PartsOutboundBillId = partsSalesOrder.PartsOutboundBillId;
                        tempRight.PartsSalesOrderId = partsSalesOrder.Id;
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    var partsOutboundBillIdsNeedCheck = tempRightList.Select(r => r.PartsOutboundBillId.ToString()).Distinct().ToArray();

                    var dbPartsOutboundBillDetailExtends = new List<PartsOutboundBillDetailExtend>();
                    Func<string[], bool> getDbPartsOutboundBillDetailExtends = value => {
                        var dbPartsOutboundBillDetailExtend = new PartsOutboundBillDetailExtend {
                            Id = Convert.ToInt32(value[0]),
                            PartsOutboundBillId = Convert.ToInt32(value[1]),
                            SparePartId = Convert.ToInt32(value[2]),
                            SparePartCode = value[3],
                            OutboundAmount = Convert.ToInt32(value[4]),
                            SettlementPrice = Convert.ToDecimal(value[5]),
                            OriginalPrice = Convert.ToDecimal(value[6])
                        };
                        dbPartsOutboundBillDetailExtends.Add(dbPartsOutboundBillDetailExtend);
                        return false;
                    };
                    db.QueryDataWithInOperator("select Extent1.Id,Extent1.PartsOutboundBillId,Extent1.SparePartId,Extent1.SparePartCode,Extent1.OutboundAmount, Extent1.SettlementPrice,Extent1.OriginalPrice from PartsOutboundBillDetail Extent1 inner join PartsOutboundBill  Extent2 on Extent1.PartsOutboundBillId = Extent2.Id ", "PartsOutboundBillId", true, partsOutboundBillIdsNeedCheck, getDbPartsOutboundBillDetailExtends);

                    foreach(var tempRight in tempRightList) {
                        var partsOutboundBillDetail = dbPartsOutboundBillDetailExtends.FirstOrDefault(v => v.SparePartCode == tempRight.SparePartCodeStr && v.PartsOutboundBillId == tempRight.PartsOutboundBillId);
                        if(partsOutboundBillDetail != null) {
                            tempRight.ReturnPrice = partsOutboundBillDetail.SettlementPrice;
                            tempRight.OriginalOrderPrice = partsOutboundBillDetail.SettlementPrice;
                            tempRight.OriginalPrice = partsOutboundBillDetail.OriginalPrice;
                        }
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();


                    var dbPartsSalesReturnBillDetailExtends = new List<PartsSalesReturnBillDetailExtend>();
                    Func<string[], bool> getDbPartsSalesReturnBillDetailExtends = value => {
                        var dbPartsSalesReturnBillDetailExtend = new PartsSalesReturnBillDetailExtend {
                            Id = Convert.ToInt32(value[0]),
                            PartsSalesReturnBillId = Convert.ToInt32(value[1]),
                            SparePartId = Convert.ToInt32(value[2]),
                            SparePartCode = value[3],
                            ReturnedQuantity = Convert.ToInt32(value[4]),
                            PartsSalesOrderCode = value[5]
                        };
                        dbPartsSalesReturnBillDetailExtends.Add(dbPartsSalesReturnBillDetailExtend);
                        return false;
                    };
                    db.QueryDataWithInOperator("select Extent1.Id,Extent1.PartsSalesReturnBillId,Extent1.SparePartId,Extent1.SparePartCode,Extent1.ReturnedQuantity,Extent1.PartsSalesOrderCode from PartsSalesReturnBillDetail Extent1 inner join PartsSalesReturnBill  Extent2 on Extent1.PartsSalesReturnBillId = Extent2.Id  where  Extent2.status <>99 ", "Extent1.PartsSalesOrderCode", true, partsSalesOrderCodesNeedCheck, getDbPartsSalesReturnBillDetailExtends);

                    foreach(var tempRight in tempRightList) {
                        var partsOutboundBillDetail = dbPartsOutboundBillDetailExtends.Where(r => r.PartsOutboundBillId == tempRight.PartsOutboundBillId && r.SparePartId == tempRight.SparePartId); //出库单对应的出库数量
                        if(!partsOutboundBillDetail.Any())
                            continue;
                        var partsSalesReturnBillDetail = dbPartsSalesReturnBillDetailExtends.FirstOrDefault(r => r.PartsSalesOrderCode == tempRight.PartsSalesOrderCode && r.SparePartId == tempRight.SparePartId); //数据库已经存在的

                        if(partsOutboundBillDetail.Sum(r => r.OutboundAmount) < (partsSalesReturnBillDetail == null ? 0 : partsSalesReturnBillDetail.ReturnedQuantity) + tempRight.ReturnedQuantity) {
                            var differ = partsOutboundBillDetail.Sum(r => r.OutboundAmount) - (partsSalesReturnBillDetail == null ? 0 : partsSalesReturnBillDetail.ReturnedQuantity);
                            tempRight.ErrorMsg = string.Format(ErrorStrings.Export_Validation_PartsSalesReturnBill_Validation12, tempRight.SparePartCodeStr, tempRight.PartsSalesOrderCode, differ);
                            continue;
                        }
                    }

                    ////配件是否可销售
                    //var sparePartIdsNeedCheck = tempRightList.Select(r => r.SparePartId.ToString(CultureInfo.InvariantCulture)).Distinct().ToArray();
                    //var dbPartsBranchs = new List<PartsBranchExtend>();
                    //Func<string[], bool> getDbBranchs = value => {
                    //    var dbObj = new PartsBranchExtend();
                    //    dbObj.PartId = Convert.ToInt32(value[0]);
                    //    dbObj.BranchId = Convert.ToInt32(value[1]);
                    //    dbObj.PartsSalesCategoryId = Convert.ToInt32(value[2]);
                    //    dbObj.IsSalable = value[3] == "1";
                    //    dbObj.IsDirectSupply = value[4] == "1";
                    //    dbPartsBranchs.Add(dbObj);
                    //    return false;
                    //};
                    //db.QueryDataWithInOperator("select PartId,BranchId,PartsSalesCategoryId,IsSalable,IsDirectSupply from PartsBranch where status=1", "PartId", false, sparePartIdsNeedCheck, getDbBranchs);
                    //foreach(var tempRight in tempRightList) {
                    //    var partsBranch = dbPartsBranchs.FirstOrDefault(v => v.PartId == tempRight.SparePartId && v.BranchId == branchId && v.PartsSalesCategoryId == partsSalesCategoryId && v.IsSalable);
                    //    if(partsBranch == null) {
                    //        tempRight.ErrorMsg = ErrorStrings.Export_Validation_PartsSalesOrder_Validation1;
                    //    }
                    //}

                    #endregion

                    //var groups = tempRightList.GroupBy(r => new {
                    //    r.SparePartCodeStr,
                    //    r.PartsSalesOrderCode
                    //}).Where(r => r.Count() > 1);
                    //foreach(var groupItem in groups.SelectMany(r => r)) {
                    //    groupItem.ErrorMsg = ErrorStrings.Export_Validation_PartsPurchaseOrder_Validation2;
                    //}
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    //获取配件价格
                    //var sparePartIdsNeedGetPrice = tempRightList.Select(r => r.SparePartId.ToString(CultureInfo.InvariantCulture)).Distinct().ToArray();
                    //var dbSarePartsPrices = new List<PartsSalesPriceExtend>();
                    //Func<string[], bool> getDbSparePartPrices = value => {
                    //    var dbObj = new PartsSalesPriceExtend {
                    //        SparePartId = Convert.ToInt32(value[0]),
                    //        SalesPrice = Convert.ToDecimal(value[1])
                    //    };
                    //    dbSarePartsPrices.Add(dbObj);
                    //    return false;
                    //};
                    //string sqlForGetPrice = "select sparepartid,salesprice from partssalesprice where  branchId=" + branchId + " and partssalescategoryId=" + partsSalesCategoryId + " and status=1 ";
                    //db.QueryDataWithInOperator(sqlForGetPrice, "SparePartId", false, sparePartIdsNeedGetPrice, getDbSparePartPrices);

                    //获取所有不合格数据
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    //获取所有合格数据
                    rightList = allList.Except(errorList).ToList();
                    #region 将合格数据的值填上
                    foreach(var rightItem in rightList) {
                        rightItem.SparePartCode = rightItem.SparePartCodeStr;
                        //rightItem.SparePartName = rightItem.SparePartNameStr;
                        rightItem.ReturnedQuantity = Convert.ToInt32(rightItem.ReturnedQuantityStr);
                        rightItem.Remark = rightItem.RemarkStr;
                        rightItem.SIHLabelCode = rightItem.SIHLabelCodeStr;
                    }
                    if(companyTypes==(int)DcsCompanyType.服务站){
                        foreach(var rightItem in rightList) {
                            if(rightItem.TraceProperty.HasValue && string.IsNullOrEmpty(rightItem.SIHLabelCode)) {
                                rightItem.ErrorMsg = rightItem.ErrorMsg + "请填写标签码";
                            } else if(rightItem.TraceProperty.HasValue && !string.IsNullOrEmpty(rightItem.SIHLabelCode)) {
                                var sihCode = rightItem.SIHLabelCodeStr.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
                                if(!sihCode.All(t => t.EndsWith("X") || t.EndsWith("J"))) {
                                    rightItem.ErrorMsg = rightItem.ErrorMsg+"标签码格式不正确";
                                }  
                            }
                        }
                    }
                    #endregion

                }
                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempObj = list[index - 1];
                            var values = new object[] {
                        #region 设置错误信息导出的列的值

                        tempObj.SparePartCodeStr,tempObj.SparePartNameStr,
                        tempObj.ReturnedQuantityStr,tempObj.PartsSalesOrderCode,tempObj.RemarkStr,tempObj.SIHLabelCodeStr,
                        tempObj.ErrorMsg
                                
                        #endregion                     
                    };
                            return values;
                        });
                    }
                    errorList = null;
                }

                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }
        // 销售退货提报批量导入
        public bool ImportPartsSalesReturnBillForReport(string fileName, out int excelImportNum, out List<PartsSalesReturnBillExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<PartsSalesReturnBillExtend>();
            var allList = new List<PartsSalesReturnBillExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);
                var userInfo = Utils.GetCurrentUserInfo();
                string[] companyIds = { userInfo.EnterpriseId.ToString() };
                var dbCompanys = new List<CompanyExtend>();
                Func<string[], bool> getCompanys = value => {
                    dbCompanys.Add(new CompanyExtend {
                        Id = Convert.ToInt32(value[0]),
                        Name = value[1],
                        Code = value[2],
                        Type = Convert.ToInt32(value[3])
                    });
                    return false;
                };
                db.QueryDataWithInOperator("select Id,Name,Code,Type from Company where status=1", "Id", false, companyIds, getCompanys);
                var company = dbCompanys.Single(r => r.Id == userInfo.EnterpriseId);
                var companyType = company.Type;


                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("CustomerTransferBill", out notNullableFields, out fieldLenght);

                List<string> notNullableFieldsCustomerCompany;
                Dictionary<string, int> fieldLenghtCustomerCompany;
                db.GetTableSchema("Company", out notNullableFieldsCustomerCompany, out fieldLenghtCustomerCompany);

                List<string> notNullableFieldsAccountGroup;
                Dictionary<string, int> fieldLenghtAccountGroup;
                db.GetTableSchema("AccountGroup", out notNullableFieldsAccountGroup, out fieldLenghtAccountGroup);
                List<object> excelColumns;
                List<PartsSalesReturnBillExtend> rightList;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_BranchSupplierRelation_Code, "BranchCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsSalesOrder_SalesUnitName, "SalesUnitName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsInboundCheckBill_ReturnType, "ReturnType");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_SupplierShippingOrder_SalesOrderCode, "PartsSalesOrderCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsPurReturnOrder_InvoiceRequirement, "InvoiceRequirement");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsSalesReturnBill_BlueInvoiceNumber, "BlueInvoiceNumber");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsPurchaseOrder_ShippingMethod, "ShippingMethod");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsPurReturnOrder_Reason, "ReturnReason");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Agency_LinkMan, "ContactPerson");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Agency_LinkManMobile, "ContactPhone");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Remark, "Remark");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode, "SparePartCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName, "SparePartName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsSalesReturnBill_ReturnedQuantity, "ReturnedQuantity");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsPurchaseOrder_DetailRemark, "RemarkDetail");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsSalesReturnBill_ReturnPrice, "ReturnPrice");
                    if(companyType == (int)DcsCompanyType.代理库 || companyType == (int)DcsCompanyType.服务站兼代理库) {
                        excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsSalesReturnBill_ReturnWarehouseCode, "ReturnWarehouseCode");
                    }

                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;

                    #region 获取对应枚举
                    var keyValuePairs = new[] {
                            new KeyValuePair<string, string>("ShippingMethod", "PartsShipping_Method"), new KeyValuePair<string, string>("ReturnType", "PartsSalesReturn_ReturnType")
                            , new KeyValuePair<string, string>("InvoiceRequirement", "PartsSalesReturnBill_InvoiceRequirement")
                           
                        };
                    tempExcelOperator.ImportHelper.LoadEnumFromDb(null, null, keyValuePairs);
                    #endregion
                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var tempImportObj = new PartsSalesReturnBillExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        #region 根据导入的字段给对应的实体属性赋值
                        tempImportObj.BranchCode = newRow["BranchCode"];
                        tempImportObj.SalesUnitName = newRow["SalesUnitName"];
                        tempImportObj.ReturnTypeStr = newRow["ReturnType"];
                        tempImportObj.PartsSalesOrderCode = newRow["PartsSalesOrderCode"];
                        tempImportObj.InvoiceRequirementStr = newRow["InvoiceRequirement"];
                        tempImportObj.BlueInvoiceNumber = newRow["BlueInvoiceNumber"];
                        tempImportObj.ShippingMethodStr = newRow["ShippingMethod"];
                        tempImportObj.ReturnReason = newRow["ReturnReason"];
                        tempImportObj.ContactPerson = newRow["ContactPerson"];
                        tempImportObj.ContactPhone = newRow["ContactPhone"];
                        tempImportObj.Remark = newRow["Remark"];
                        tempImportObj.SparePartCode = newRow["SparePartCode"];
                        tempImportObj.SparePartName = newRow["SparePartName"];
                        tempImportObj.ReturnedQuantityStr = newRow["ReturnedQuantity"];
                        tempImportObj.RemarkDetail = newRow["RemarkDetail"];
                        tempImportObj.ReturnPriceStr = newRow["ReturnPrice"];
                        if(companyType == (int)DcsCompanyType.代理库 || companyType == (int)DcsCompanyType.服务站兼代理库) {
                            tempImportObj.ReturnWarehouseCode = newRow["ReturnWarehouseCode"];
                        }
                        #endregion
                        var tempErrorMessage = new List<string>();
                        #region 导入的数据基本检查
                        //分公司编号检查
                        var fieldIndex = notNullableFields.IndexOf("BranchId".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.BranchCode)) {
                            tempErrorMessage.Add(ErrorStrings.Export_Validation_BranchSupplierRelation_CodeIsNull);
                        }
                        fieldIndex = notNullableFields.IndexOf("ReturnPrice".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.ReturnPriceStr)) {
                            tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsSalesReturnBill_ReturnPriceNull);
                        } else {
                            if(Convert.ToInt32(tempImportObj.ReturnPriceStr) < 0)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsSalesReturnBill_ReturnPriceOverZero);
                            else {
                                tempImportObj.ReturnPrice = Convert.ToDecimal(tempImportObj.ReturnPriceStr);
                            }
                        }

                        //销售组织名称检查
                        fieldIndex = notNullableFields.IndexOf("SalesUnitId".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.SalesUnitName)) {
                            tempErrorMessage.Add(ErrorStrings.Export_Validarion_PartsSalesReturnBill_SalesUnitNull);
                        }
                        //退货类型检查
                        fieldIndex = notNullableFields.IndexOf("ReturnType".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.ReturnTypeStr)) {
                            tempErrorMessage.Add(ErrorStrings.Export_Validarion_PartsSalesReturnBill_ReturnTypeNull);
                        } else {
                            var tempEnumValue = tempExcelOperator.ImportHelper.GetEnumValue("ReturnType", tempImportObj.ReturnTypeStr);
                            if(!tempEnumValue.HasValue) {
                                tempErrorMessage.Add(ErrorStrings.Export_Validarion_PartsSalesReturnBill_ReturnTypeValue);
                            } else {
                                if(tempEnumValue.Value == (int)DcsPartsSalesReturn_ReturnType.特殊退货) {
                                    tempImportObj.ReturnType = tempEnumValue.Value;
                                } else {
                                    tempErrorMessage.Add(ErrorStrings.Export_Validarion_PartsSalesReturnBill_ReturnTypeValueError);
                                }
                            }
                        }
                        //销售订单编号检查
                        fieldIndex = notNullableFields.IndexOf("PartsSalesOrderCode".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.PartsSalesOrderCode)) {
                            tempErrorMessage.Add(ErrorStrings.Export_Validarion_PartsSalesReturnBill_SalesOrderIsNull);
                        }
                        //退货原因检查
                        fieldIndex = notNullableFields.IndexOf("ReturnReason".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.ReturnReason)) {
                            tempErrorMessage.Add(ErrorStrings.Export_Validarion_PartsSalesReturnBill_ReasonIsNull);
                        }

                        //配件编号检查
                        fieldIndex = notNullableFields.IndexOf("SparePartCode".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.SparePartCode)) {
                            tempErrorMessage.Add(ErrorStrings.ImpPartsPurchaseOrderDetail_Validation1);
                        }
                        //配件名称检查
                        fieldIndex = notNullableFields.IndexOf("SparePartName".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.SparePartName)) {
                            tempErrorMessage.Add(ErrorStrings.Export_Validation_Sparepart_Name);
                        }
                        //退货数量
                        fieldIndex = notNullableFields.IndexOf("ReturnedQuantity".ToUpper());
                        if(null == tempImportObj.ReturnedQuantityStr) {
                            tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsSalesReturnBill_Validation3);
                        } else {
                            if(Convert.ToInt32(tempImportObj.ReturnedQuantityStr) < 0)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsSalesReturnBill_Validation13);
                            else {
                                tempImportObj.ReturnedQuantity = Convert.ToInt32(tempImportObj.ReturnedQuantityStr);
                            }
                        }
                        //开票要求
                        if(!string.IsNullOrEmpty(tempImportObj.InvoiceRequirementStr)) {
                            var tempEnumValue = tempExcelOperator.ImportHelper.GetEnumValue("InvoiceRequirement", tempImportObj.InvoiceRequirementStr);
                            if(!tempEnumValue.HasValue) {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsSalesReturnBill_Validation14);
                            } else {
                                tempImportObj.InvoiceRequirement = tempEnumValue.Value;
                            }
                        }
                        //发运方式
                        if(!string.IsNullOrEmpty(tempImportObj.ShippingMethodStr)) {
                            var tempEnumValue = tempExcelOperator.ImportHelper.GetEnumValue("ShippingMethod", tempImportObj.ShippingMethodStr);
                            if(!tempEnumValue.HasValue) {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsPurchaseOrder_ShippingMethodValueError);
                            } else {
                                tempImportObj.ShippingMethod = tempEnumValue.Value;
                            }
                        }
                        //退货仓库编号
                        if(companyType == (int)DcsCompanyType.代理库 || companyType == (int)DcsCompanyType.服务站兼代理库) {
                            fieldIndex = notNullableFields.IndexOf("ReturnWarehouseCode".ToUpper());
                            if(null == tempImportObj.ReturnWarehouseCode) {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsSalesReturnBill_Validation15);
                            }
                        }
                        #endregion
                        if(tempErrorMessage.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });
                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    #region 导入内容业务检查
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    //分公司编号是否存在
                    var branchCodes = tempRightList.Select(r => r.BranchCode).Distinct().ToArray();
                    var dbbranchGroups = new List<CompanyExtend>();
                    Func<string[], bool> getDbAccountGroups = value => {
                        dbbranchGroups.Add(new CompanyExtend {
                            Id = Convert.ToInt32(value[0]),
                            Name = value[1],
                            Code = value[2]
                        });
                        return false;
                    };
                    db.QueryDataWithInOperator("select Id,Name,Code from Company where status=1", "Code", false, branchCodes, getDbAccountGroups);
                    var errorAccountGroups = tempRightList.Where(r => dbbranchGroups.All(v => v.Code != r.BranchCode)).ToList();
                    foreach(var errorItem in errorAccountGroups) {
                        errorItem.ErrorMsg = String.Format(ErrorStrings.Export_Validation_PartsSalesReturnBill_Validation16, errorItem.BranchCode);
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    foreach(var rightItem in tempRightList) {
                        var tempAccountGroup = dbbranchGroups.Single(r => r.Code == rightItem.BranchCode);
                        rightItem.BranchId = tempAccountGroup.Id;
                        rightItem.BranchCode = tempAccountGroup.Code;
                        rightItem.BranchName = tempAccountGroup.Name;

                    }
                    //销售组织是否存在

                    var salesUnits = tempRightList.Select(r => r.SalesUnitName).ToArray();
                    var dbSalesUnits = new List<SalesUnitExtend>();
                    Func<string[], bool> getDbSalesUnits = value => {
                        dbSalesUnits.Add(new SalesUnitExtend {
                            Id = Convert.ToInt32(value[0]),
                            Name = value[1],
                            Code = value[2],
                            OwnerCompanyId = Convert.ToInt32(value[3]),
                            SalesUnitOwnerCompanyCode = value[4],
                            SalesUnitOwnerCompanyName = value[5]
                        });
                        return false;
                    };
                    db.QueryDataWithInOperator("select s.Id,s.code,s.name,s.OwnerCompanyId,c.code,c.name from salesunit s left join company c on s.ownercompanyid=c.id where s.status=1", "s.Name", false, salesUnits, getDbSalesUnits);
                    var errorSalesUnit = tempRightList.Where(r => dbSalesUnits.All(v => v.Name != r.SalesUnitName)).ToList();
                    foreach(var errorItem in errorSalesUnit) {
                        errorItem.ErrorMsg = String.Format(ErrorStrings.Export_Validation_PartsSalesReturnBill_Validation17, errorItem.SalesUnitName);
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    foreach(var rightItem in tempRightList) {
                        var tempAccountGroup = dbSalesUnits.Single(r => r.Name == rightItem.SalesUnitName);
                        rightItem.SalesUnitId = tempAccountGroup.Id;
                        rightItem.SalesUnitOwnerCompanyId = tempAccountGroup.OwnerCompanyId;
                        rightItem.SalesUnitOwnerCompanyCode = tempAccountGroup.SalesUnitOwnerCompanyCode;
                        rightItem.SalesUnitOwnerCompanyName = tempAccountGroup.SalesUnitOwnerCompanyName;
                    }
                    //销售订单编号是否存在
                    var partsSalesOrderCode = tempRightList.Select(r => r.PartsSalesOrderCode).Distinct().ToArray();
                    var saleCodeGroups = new List<PartsSalesOrderExtend>();
                    Func<string[], bool> getsaleCodeps = value => {
                        saleCodeGroups.Add(new PartsSalesOrderExtend {
                            Id = Convert.ToInt32(value[0]),
                            Code = value[1],
                            BranchId = Convert.ToInt32(value[2]),
                            BranchCode = value[3],
                            BranchName = value[4],
                            WarehouseId = Convert.ToInt32(value[5]),
                            WarehouseCode = value[6],
                            WarehouseName = value[7]
                        });
                        return false;
                    };
                    db.QueryDataWithInOperator("select p.Id,p.Code,p.BranchId,p.BranchCode,p.BranchName,w.id,w.Code,w.Code from PartsSalesOrder p left join  Warehouse w on p.WarehouseId=w.id where p.SubmitCompanyId=" + userInfo.EnterpriseId, "p.Code", false, partsSalesOrderCode, getsaleCodeps);
                    var errorInAccountGroups = tempRightList.Where(r => saleCodeGroups.All(v => v.Code != r.PartsSalesOrderCode)).ToList();
                    foreach(var errorItem in errorInAccountGroups) {
                        errorItem.ErrorMsg = String.Format(ErrorStrings.Export_Validation_PartsSalesReturnBill_Validation18, errorItem.PartsSalesOrderCode);
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    foreach(var rightItem in tempRightList) {
                        var tempAccountGroup = saleCodeGroups.Single(r => r.Code == rightItem.PartsSalesOrderCode);
                        rightItem.PartsSalesOrderId = tempAccountGroup.Id;
                        rightItem.WarehouseId = tempAccountGroup.WarehouseId;
                        rightItem.WarehouseCode = tempAccountGroup.WarehouseCode;
                        rightItem.WarehouseName = tempAccountGroup.WarehouseName;
                    }

                    //配件是否存在是否存在
                    var sparePartCodeCodes = tempRightList.Select(r => r.SparePartCode).Distinct().ToArray();
                    var dbsparePartCodes = new List<SparePartExtend>();
                    Func<string[], bool> getsparePartCodes = value => {
                        var dbObj = new SparePartExtend {
                            Id = Convert.ToInt32(value[0]),
                            Code = value[1],
                            Name = value[2]
                        };
                        dbsparePartCodes.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select Id,Code,Name from SparePart where status=1 ", "Code", false, sparePartCodeCodes, getsparePartCodes);
                    var errorCompanys = tempRightList.Where(r => !dbsparePartCodes.Any(v => v.Code == r.SparePartCode && v.Name == r.SparePartName)).ToList();
                    foreach(var errorItem in errorCompanys) {
                        errorItem.ErrorMsg = String.Format(ErrorStrings.Export_Validation_PartsSalesReturnBill_Validation19, errorItem.SparePartCode);
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    foreach(var rightItem in tempRightList) {
                        var tempCompany = dbsparePartCodes.Single(r => r.Code == rightItem.SparePartCode && r.Name == rightItem.SparePartName);
                        rightItem.SparePartId = tempCompany.Id;
                    }
                    //校验退货仓库编号是否存在
                    if(companyType == (int)DcsCompanyType.代理库 || companyType == (int)DcsCompanyType.服务站兼代理库) {
                        var warehouses = tempRightList.Select(r => r.ReturnWarehouseCode).Distinct().ToArray();
                        var dbInWarehouse = new List<WarehouseExtend>();
                        Func<string[], bool> getDbInWarehouses = value => {
                            var dbObj = new WarehouseExtend {
                                Id = Convert.ToInt32(value[0]),
                                Code = value[1],
                                Name = value[2]
                            };
                            dbInWarehouse.Add(dbObj);
                            return false;
                        };
                        db.QueryDataWithInOperator("select Id,Code,Name from Warehouse where status=1 ", "Code", false, warehouses, getDbInWarehouses);
                        var errorInCompanys = tempRightList.Where(r => !dbInWarehouse.Any(v => v.Code == r.ReturnWarehouseCode)).ToList();
                        foreach(var errorItem in errorInCompanys) {
                            errorItem.ErrorMsg = String.Format(ErrorStrings.Export_Validation_PartsSalesReturnBill_Validation20, errorItem.ReturnWarehouseCode);
                        }
                        tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                        foreach(var rightItem in tempRightList) {
                            var tempCompany = dbInWarehouse.Single(r => r.Code == rightItem.ReturnWarehouseCode);
                            rightItem.ReturnWarehouseId = tempCompany.Id;
                            rightItem.ReturnWarehouseName = tempCompany.Name;
                        }
                    }



                    //校验数据是否存在于数据库
                    var partsSalesOrderIdList = tempRightList.Select(r => r.PartsSalesOrderId).Distinct().ToArray();
                    foreach(var id in partsSalesOrderIdList) {
                        var details = tempRightList.Where(r => r.PartsSalesOrderId == id).ToArray();
                        var spareIds = details.Select(r => r.SparePartId.ToString()).Distinct().ToArray();
                        var dbPartsSalesOrderDetail = new List<PartsSalesOrderDetailExtend>();
                        Func<string[], bool> getPartsSalesOrderDetail = value => {
                            var dbObj = new PartsSalesOrderDetailExtend {
                                Id = Convert.ToInt32(value[0]),
                                MeasureUnit = value[2],
                                OriginalPrice = Convert.ToDecimal(value[3]),
                                OrderPrice = Convert.ToDecimal(value[4]),
                                PartsSalesOrderId = Convert.ToInt32(value[5]),
                                SparePartId = Convert.ToInt32(value[6]),
                                OutboundAmount = Convert.ToInt32(value[7])
                            };
                            dbPartsSalesOrderDetail.Add(dbObj);
                            return false;
                        };
                        db.QueryDataWithInOperator("select od.Id,od.ApproveQuantity,od.MeasureUnit,od.OriginalPrice,od.OrderPrice,od.PartsSalesOrderId,od.SparePartId,d.OutboundAmount from partsoutboundbilldetail d  join partsoutboundbill p on p.id=d.partsoutboundbillid  join partssalesorder o on   o.id=p.OriginalRequirementBillId join PartsSalesOrderDetail od on o.id=od.partssalesorderid and d.sparepartid=od.sparepartid where od.PartsSalesOrderId =" + id, "od.SparePartId", false, spareIds, getPartsSalesOrderDetail);
                        var errorDetail = tempRightList.Where(r => !dbPartsSalesOrderDetail.Any(v => v.SparePartId == r.SparePartId && v.PartsSalesOrderId == r.PartsSalesOrderId)).ToList();
                        foreach(var errorItem in errorDetail) {
                            errorItem.ErrorMsg = String.Format(ErrorStrings.Export_Validation_PartsSalesReturnBill_Validation21, errorItem.SparePartCode, errorItem.PartsSalesOrderCode);
                        }
                        tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                        foreach(var rightItem in tempRightList) {
                            var tempCompany = dbPartsSalesOrderDetail.Single(r => r.SparePartId == rightItem.SparePartId && r.PartsSalesOrderId == rightItem.PartsSalesOrderId);
                            rightItem.OriginalOrderPrice = tempCompany.OrderPrice;
                            rightItem.OriginalPrice = tempCompany.OriginalPrice;
                            rightItem.MeasureUnit = tempCompany.MeasureUnit;
                            rightItem.OutboundAmount = tempCompany.OutboundAmount;
                        }
                        //判断当前退货数量是否小于出库数量-已退货数量
                        var dbReturnDetail = new List<PartsSalesReturnBillExtend>();
                        Func<string[], bool> getReturnDetail = value => {
                            var dbObj = new PartsSalesReturnBillExtend {
                                PartsSalesOrderId = Convert.ToInt32(value[0]),
                                SparePartId = Convert.ToInt32(value[1]),
                                ReturnedQuantity = Convert.ToInt32(value[2])
                            };
                            dbReturnDetail.Add(dbObj);
                            return false;
                        };
                        db.QueryDataWithInOperator("select b.partssalesorderid,p.SparePartId,p.ReturnedQuantity from PartsSalesReturnBillDetail p join partssalesreturnbill b on p.partssalesreturnbillid=b.id and b.partssalesorderid =" + id, "p.SparePartId", false, spareIds, getReturnDetail);

                        if(dbReturnDetail.Count > 0) {
                            foreach(var item in tempRightList) {
                                var sumReturn = dbReturnDetail.Where(r => item.SparePartId == r.SparePartId && item.PartsSalesOrderId == r.PartsSalesOrderId).Sum(s => s.ReturnedQuantity);
                                if((item.OutboundAmount - sumReturn) < item.ReturnedQuantity) {
                                    item.ErrorMsg = String.Format(ErrorStrings.Export_Validation_PartsSalesReturnBill_Validation22, item.SparePartCode);
                                }
                            }
                        }

                        //var errorDetailAppro = tempRightList.Where(r => !dbReturnDetail.Any(v => v.SparePartId == r.SparePartId && v.PartsSalesOrderId == r.PartsSalesOrderId && (r.OutboundAmount-v.ReturnedQuantity) < r.ReturnedQuantity)).ToList();
                        //foreach(var errorItem in errorDetailAppro) {
                        //    errorItem.ErrorMsg = String.Format(ErrorStrings.Export_Validation_PartsSalesReturnBill_Validation22, errorItem.SparePartCode);
                        //}
                        tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                        decimal totalAmountAll = 0;
                        foreach(var detail in tempRightList.Where(r => r.PartsSalesOrderId == id && r.ErrorMsg == null).ToArray()) {
                            totalAmountAll += detail.ReturnPrice * detail.ReturnedQuantity;
                        }
                        foreach(var rightItem in tempRightList.Where(r => r.PartsSalesOrderId == id && r.ErrorMsg == null).ToArray()) {
                            rightItem.TotalAmount = totalAmountAll;
                            rightItem.ReturnCompanyId = company.Id;
                            rightItem.ReturnCompanyName = company.Name;
                            rightItem.ReturnCompanyCode = company.Code;
                            rightItem.InvoiceReceiveCompanyId = company.Id;
                            rightItem.InvoiceReceiveCompanyName = company.Name;
                            rightItem.InvoiceReceiveCompanyCode = company.Code;
                            rightItem.SubmitCompanyId = company.Id;
                            rightItem.SubmitCompanyName = company.Name;
                            rightItem.SubmitCompanyCode = company.Code;
                        }


                    }
                    //查询客户账户Id
                    var salesUnitIds = tempRightList.Select(r => r.SalesUnitId.ToString()).ToArray();
                    var dbSalesUnitIds = new List<CustomerAccountHisDetailExtend>();
                    Func<string[], bool> getSalesUnitIds = value => {
                        var dbObj = new CustomerAccountHisDetailExtend {
                            Id = Convert.ToInt32(value[0]),
                            CustomerCompanyId = Convert.ToInt32(value[1]),
                            SalesUnitId = Convert.ToInt32(value[2])

                        };
                        dbSalesUnitIds.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select c.Id,c.CustomerCompanyId,s.id from CustomerAccount c  join salesunit s on c.accountgroupid=s.accountgroupid where s.status=1", "s.Id", false, salesUnitIds, getSalesUnitIds);
                    var errorSalesUnitId = tempRightList.Where(r => !dbSalesUnitIds.Any(v => v.CustomerCompanyId == r.ReturnCompanyId && v.SalesUnitId == r.SalesUnitId)).ToList();
                    foreach(var errorItem in errorSalesUnitId) {
                        errorItem.ErrorMsg = String.Format(ErrorStrings.Export_Validation_PartsSalesReturnBill_Validation23, errorItem.ReturnCompanyCode);
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    foreach(var rightItem in tempRightList) {
                        var tempCompany = dbSalesUnitIds.Single(r => r.CustomerCompanyId == rightItem.ReturnCompanyId && r.SalesUnitId == rightItem.SalesUnitId);
                        rightItem.CustomerAccountId = tempCompany.Id;
                    }

                    #endregion
                    //获取所有不合格数据
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    //获取所有合格数据
                    rightList = allList.Except(errorList).ToList();
                }
                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempObj = list[index - 1];
                            var values = new object[] {
                                #region 设置错误信息导出的列的值
                                tempObj.BranchCode , tempObj.SalesUnitName,tempObj.ReturnTypeStr , tempObj.PartsSalesOrderCode ,  tempObj.InvoiceRequirementStr ,
                                 tempObj.BlueInvoiceNumber ,tempObj.ShippingMethodStr , tempObj.ReturnReason, tempObj.ContactPerson ,tempObj.ContactPhone, tempObj.Remark , 
                                 tempObj.SparePartCode , tempObj.SparePartName, tempObj.ReturnedQuantityStr , tempObj.RemarkDetail ,tempObj.ReturnWarehouseCode ,tempObj.ErrorMsg
                               
                                #endregion                     
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }

                //导入所有合格数据
                if(!rightList.Any())
                    return true;
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    //开启事务，新增更新配件在一个事务内
                    var ts = conn.BeginTransaction();
                    try {
                        //新增配件
                        if(rightList.Any()) {
                            #region 获取对应的插入的SQL 通过db.GetInsertSql方法
                            //获取新增数据的sql语句，Id为主键
                            var sqlInsert = db.GetInsertSql("PartsSalesReturnBill", "Id", new[] {
                                "Code","ReturnCompanyId","ReturnCompanyCode","ReturnCompanyName","InvoiceReceiveCompanyId","InvoiceReceiveCompanyCode",
                                "InvoiceReceiveCompanyName","SubmitCompanyId","SubmitCompanyCode","SubmitCompanyName","TotalAmount","SalesUnitId","SalesUnitName",
                                "SalesUnitOwnerCompanyId","SalesUnitOwnerCompanyCode","SalesUnitOwnerCompanyName","CustomerAccountId","BranchId","BranchCode","BranchName","InvoiceRequirement",
                                "ContactPerson","ContactPhone","PartsSalesOrderId","PartsSalesOrderCode","Status","ReturnType","IsBarter","BlueInvoiceNumber","WarehouseId","WarehouseCode","WarehouseName",
                                "CreatorId","ShippingMethod","ReturnReason","Remark","ReturnWarehouseId","ReturnWarehouseCode","ReturnWarehouseName",
                                "CreatorName","CreateTime"
                            });
                            var sqlDetail = db.GetInsertSql("PartsSalesReturnBillDetail", "Id", new[] {
                               "PartsSalesReturnBillId","SparePartId","SparePartCode","SparePartName","MeasureUnit","ReturnedQuantity","OriginalOrderPrice",
                                "ReturnPrice","OriginalPrice","Remark"
                            });
                            #endregion
                            //ReturnWarehouseId 为 null 的
                            var orderIds = rightList.Where(s => s.ReturnWarehouseId == 0).Select(r => r.PartsSalesOrderId).Distinct().ToArray();
                            foreach(var id in orderIds) {
                                //主单                              
                                var saleOrder = rightList.Where(r => r.PartsSalesOrderId == id && r.ReturnWarehouseId == 0).First();
                                var detailList = rightList.Where(r => r.PartsSalesOrderId == id && r.ReturnWarehouseId == 0).ToList();
                                this.insert(saleOrder, sqlInsert, sqlDetail, detailList, conn, db, ts);
                            }
                            //ReturnWarehouseId 有值的
                            var orderIdsNotNull = rightList.Where(s => s.ReturnWarehouseId > 0).Select(r => r.PartsSalesOrderId).Distinct().ToArray();
                            var returnWarehouseIds = rightList.Where(s => s.ReturnWarehouseId > 0).Select(r => r.ReturnWarehouseId).Distinct().ToArray();
                            foreach(var id in orderIdsNotNull) {
                                foreach(var warehouseId in returnWarehouseIds) {
                                    var saleOrder = rightList.Where(r => r.PartsSalesOrderId == id && r.ReturnWarehouseId == warehouseId).First();
                                    var detailList = rightList.Where(r => r.PartsSalesOrderId == id && r.ReturnWarehouseId == warehouseId).ToList();
                                    this.insert(saleOrder, sqlInsert, sqlDetail, detailList, conn, db, ts);
                                }
                            }
                        }
                        //无异常提交
                        ts.Commit();
                    } catch(Exception) {
                        //报错回滚
                        ts.Rollback();
                        throw;
                    } finally {
                        if(conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
            }
        }
        private void insert(PartsSalesReturnBillExtend saleOrder, string sqlInsert, string sqlDetail, List<PartsSalesReturnBillExtend> detailList, DbConnection conn, DbHelper db, DbTransaction ts) {
            var userInfo = Utils.GetCurrentUserInfo();
            var command = db.CreateDbCommand(sqlInsert, conn, ts);
            command.Parameters.Add(db.CreateDbParameter("Code", CodeGenerator.Generate(db, "PartsSalesReturnBill", userInfo.EnterpriseCode)));
            command.Parameters.Add(db.CreateDbParameter("ReturnCompanyId", saleOrder.ReturnCompanyId));
            command.Parameters.Add(db.CreateDbParameter("ReturnCompanyCode", saleOrder.ReturnCompanyCode));
            command.Parameters.Add(db.CreateDbParameter("ReturnCompanyName", saleOrder.ReturnCompanyName));
            command.Parameters.Add(db.CreateDbParameter("InvoiceReceiveCompanyId", saleOrder.InvoiceReceiveCompanyId));
            command.Parameters.Add(db.CreateDbParameter("InvoiceReceiveCompanyCode", saleOrder.InvoiceReceiveCompanyCode));
            command.Parameters.Add(db.CreateDbParameter("InvoiceReceiveCompanyName", saleOrder.InvoiceReceiveCompanyName));
            command.Parameters.Add(db.CreateDbParameter("SubmitCompanyId", saleOrder.SubmitCompanyId));
            command.Parameters.Add(db.CreateDbParameter("SubmitCompanyCode", saleOrder.SubmitCompanyCode));
            command.Parameters.Add(db.CreateDbParameter("SubmitCompanyName", saleOrder.SubmitCompanyName));
            command.Parameters.Add(db.CreateDbParameter("TotalAmount", saleOrder.TotalAmount));
            command.Parameters.Add(db.CreateDbParameter("SalesUnitId", saleOrder.SalesUnitId));
            command.Parameters.Add(db.CreateDbParameter("SalesUnitName", saleOrder.SalesUnitName));
            command.Parameters.Add(db.CreateDbParameter("SalesUnitOwnerCompanyId", saleOrder.SalesUnitOwnerCompanyId));
            command.Parameters.Add(db.CreateDbParameter("SalesUnitOwnerCompanyCode", saleOrder.SalesUnitOwnerCompanyCode));
            command.Parameters.Add(db.CreateDbParameter("SalesUnitOwnerCompanyName", saleOrder.SalesUnitOwnerCompanyName));
            command.Parameters.Add(db.CreateDbParameter("CustomerAccountId", saleOrder.CustomerAccountId));
            command.Parameters.Add(db.CreateDbParameter("BranchId", saleOrder.BranchId));
            command.Parameters.Add(db.CreateDbParameter("BranchCode", saleOrder.BranchCode));
            command.Parameters.Add(db.CreateDbParameter("BranchName", saleOrder.BranchName));
            command.Parameters.Add(db.CreateDbParameter("InvoiceRequirement", saleOrder.InvoiceRequirement));
            command.Parameters.Add(db.CreateDbParameter("ContactPerson", saleOrder.ContactPerson));
            command.Parameters.Add(db.CreateDbParameter("ContactPhone", saleOrder.ContactPhone));
            command.Parameters.Add(db.CreateDbParameter("PartsSalesOrderId", saleOrder.PartsSalesOrderId));
            command.Parameters.Add(db.CreateDbParameter("PartsSalesOrderCode", saleOrder.PartsSalesOrderCode));
            command.Parameters.Add(db.CreateDbParameter("Status", (int)DcsPartsSalesReturnBillStatus.新增));
            command.Parameters.Add(db.CreateDbParameter("ReturnType", saleOrder.ReturnType));
            command.Parameters.Add(db.CreateDbParameter("IsBarter", 0));
            command.Parameters.Add(db.CreateDbParameter("BlueInvoiceNumber", saleOrder.BlueInvoiceNumber));
            command.Parameters.Add(db.CreateDbParameter("WarehouseId", saleOrder.WarehouseId));
            command.Parameters.Add(db.CreateDbParameter("WarehouseCode", saleOrder.WarehouseCode));
            command.Parameters.Add(db.CreateDbParameter("WarehouseName", saleOrder.WarehouseName));
            command.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
            command.Parameters.Add(db.CreateDbParameter("ShippingMethod", saleOrder.ShippingMethod));
            command.Parameters.Add(db.CreateDbParameter("ReturnReason", saleOrder.ReturnReason));
            command.Parameters.Add(db.CreateDbParameter("Remark", saleOrder.Remark));
            command.Parameters.Add(db.CreateDbParameter("ReturnWarehouseId", saleOrder.ReturnWarehouseCode));
            command.Parameters.Add(db.CreateDbParameter("ReturnWarehouseCode", saleOrder.ReturnWarehouseCode));
            command.Parameters.Add(db.CreateDbParameter("ReturnWarehouseName", saleOrder.WarehouseCode));
            command.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
            command.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
            var tempId = db.ExecuteInsert(command, "Id");
            foreach(var item in detailList) {
                var commandDetail = db.CreateDbCommand(sqlDetail, conn, ts);
                commandDetail.Parameters.Add(db.CreateDbParameter("PartsSalesReturnBillId", tempId));
                commandDetail.Parameters.Add(db.CreateDbParameter("SparePartId", item.SparePartId));
                commandDetail.Parameters.Add(db.CreateDbParameter("SparePartCode", item.SparePartCode));
                commandDetail.Parameters.Add(db.CreateDbParameter("SparePartName", item.SparePartName));
                commandDetail.Parameters.Add(db.CreateDbParameter("MeasureUnit", item.MeasureUnit));
                commandDetail.Parameters.Add(db.CreateDbParameter("ReturnedQuantity", item.ReturnedQuantity));
                commandDetail.Parameters.Add(db.CreateDbParameter("ReturnPrice", item.ReturnPrice));
                commandDetail.Parameters.Add(db.CreateDbParameter("OriginalPrice", item.OriginalPrice));
                commandDetail.Parameters.Add(db.CreateDbParameter("OriginalOrderPrice", item.OriginalOrderPrice));
                commandDetail.Parameters.Add(db.CreateDbParameter("Remark", item.RemarkDetail));
                commandDetail.ExecuteNonQuery();
            }
        }

    }
}


