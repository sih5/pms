﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        public bool ExportRetailerDeliveryWithDetail(int[] ids, DateTime? syncTimeBegin, DateTime? syncTimeEnd, string OrderNumber, out string fileName) {
            fileName = GetExportFilePath("导出新电商订单接口日志主清单_" + ".xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);
                using(var conn = db.CreateDbConnection()) {
                    var sql = new StringBuilder();
                    sql.Append(@"select
                        b.OrderNumber,--电商平台单号
                        w.Name,--仓库名称
                        ps.SubmitCompanyCode,--客户编号
                        ps.SubmitCompanyName,--客户名称
                        (select value from keyvalueitem where NAME = 'EADelivery_SyncStatus'and key=a.syncstatus) As syncstatus, -- 同步状态 
                        cast('电商订单' as varchar2(10)) as InterfaceType,--同步类型 
                        a.SyncTime,--同步时间 
                        a.GoodsCode,--配件图号 
                        s.Name,--配件名称
                        a.Quantity,--数量
                        a.Message-- 同步错误原因
                        from Retailer_DeliveryDetail a 
                        inner join Retailer_Delivery b  on  a.OrderDeliveryId=b.id
                        left join SparePart s on a.GoodsCode=s.Code
                        left join PartsSalesOrder ps on ps.ERPSourceOrderCode=b.OrderNumber
                        left join Warehouse w on w.Id=ps.WarehouseId where 1=1 ");
                    var dbParameters = new List<DbParameter>();
                    if(ids != null && ids.Length > 0) {
                        sql.Append(" and b.id in (");
                        for(var i = 0; i < ids.Length; i++) {
                            if(ids.Length == i + 1) {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            } else {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                        }
                        sql.Append(")");
                    } else {
                        #region 条件过滤
                        if(!string.IsNullOrEmpty(OrderNumber)) {
                            sql.Append(" and b.OrderNumber like {0}OrderNumber ");
                            dbParameters.Add(db.CreateDbParameter("OrderNumber", "%" + OrderNumber + "%"));
                        }
                        if(syncTimeBegin.HasValue) {
                            sql.Append(@" and a.syncTime >=to_date({0}syncTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = syncTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("syncTimeBegin", tempTime.ToString("G")));
                        }
                        if(syncTimeEnd.HasValue) {
                            sql.Append(@" and a.syncTime <=to_date({0}syncTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = syncTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("syncTimeEnd", tempTime.ToString("G")));
                        }
                        #endregion
                    }
                    var sqlStr = sql.ToString();
                    if(sqlStr.IndexOf(" where 1=1  and ") > 0)
                        sqlStr = sqlStr.Replace(" where 1=1  and ", " where ");
                    else
                        sqlStr = sqlStr.Replace(" where 1=1 ", "");
                    var command = db.CreateDbCommand(string.Format(sqlStr, db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    conn.Open();
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    "电商平台单号","仓库名称","客户编号",ErrorStrings.Export_Title_Credenceapplication_CustomerName,"同步状态","同步类型","同步时间",ErrorStrings.Export_Title_PartsBranch_Code,ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName,ErrorStrings.Export_Title_WarehouseArea_Quantity,"同步错误原因"
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }

    }
}
