﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.EntityClient;
using System.IO;
using System.ServiceModel;
using System.ServiceModel.Activation;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Entities;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService
{

    [ServiceContract(Namespace = "http://sunlight.bz/excelservice/")]
    public interface IExcelService
    {

        /// <summary>
        /// 批量导入供应商扣补款单
        /// </summary>
        [OperationContract]
        bool ImportSupplierExpenseAdjustBill(string fileName, out int excelImportNum, int branchId, out List<SupplierExpenseAdjustBillExtend> rightData, out List<SupplierExpenseAdjustBillExtend> errorData, out string errorDataFileName, out string errorMessage);

        /// <summary>
        /// 合并导出销售订单主清单
        /// </summary>
        [OperationContract]
        bool ExportPartsSalesOrderWithDetails(int? id, int personnelId, int? provi导出积分汇总单nceId, string city, string code, string submitCompanyCode, string submitCompanyName, int? partsSalesOrderTypeId, int? salesCategoryId, int? status, int? warehouseId, bool? isDebt, bool? ifDirectProvision, DateTime? createTimeBegin, DateTime? createTimeEnd, DateTime? submitTimeBegin, DateTime? submitTimeEnd, DateTime? abandonTimeBegin, DateTime? abandonTimeEnd, DateTime? approveTimeBegin, DateTime? approveTimeEnd, out string fileName);

        /// <summary>
        /// 校验销售导入配件清单
        /// </summary>
        [OperationContract]
        bool ImportPartsSalesOrderDetail(string fileName, bool ifDirectProvision, int branchId, int partsSalesCategoryId, out int excelImportNum, out List<PartsSalesOrderDetailExtend> rightData, out List<PartsSalesOrderDetailExtend> errorData, out string errorDataFileName, out string errorMessage);

        /// <summary>
        /// 校验销售导入配件清单
        /// </summary>
        [OperationContract]
        bool ImportFdWarrantyMessage(string fileName, out int excelImportNum, out List<FdWarrantyMessageExtend> errorData, out string errorDataFileName, out string errorMessage);

        /// <summary>
        /// 导入企业清单
        /// </summary>
        [OperationContract]
        bool ImportCompanyDetail(string fileName, out int excelImportNum, out List<CompanyDetailExtend> rightData, out List<CompanyDetailExtend> errorData, out string errorDataFileName, out string errorMessage);


        /// <summary>
        /// 导入信用申请单
        /// </summary>
        [OperationContract]
        bool ImportCredenceApplication(string fileName, out int excelImportNum, out List<CredenceApplicationExtend> errorData, out string errorDataFileName, out string errorMessage);

        /// <summary>
        /// 批量导出配件采购订单
        /// </summary>
        [OperationContract]
        bool ExportPartsPurchaseOrderWithDetail(bool isSupplierLogin, int[] partsPurchaseOrderIds/*, int? partsSalesCategoryId*/, string code, string businessCode, string warehouseName, string partsSupplierCode, string partsSupplierName, int? partsPurchaseOrderTypeId, bool? ifDirectProvision, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, int? branchId, int? supplierId, int? warehouseId, int? InStatus, DateTime? RequestedDeliveryTimeBegin, DateTime? RequestedDeliveryTimeEnd, DateTime? approveTimeBegin, DateTime? approveTimeEnd/*, string SAPPurchasePlanCode, string hWPurOrderCode*/, string ERPSourceOrderCode/*, string CPPartsPurchaseOrderCode, string CPPartsInboundCheckCode*/, int? ioStatus, string sparePartCode, out string filename);

        /// <summary>
        /// 导出配件采购订单
        /// </summary>
        [OperationContract]
        bool ExportPartsPurchaseOrder(bool isSupplierLogin, int[] partsPurchaseOrderIds/*, int? partsSalesCategoryId*/, string code, string businessCode, string warehouseName, string partsSupplierCode, string partsSupplierName, int? partsPurchaseOrderTypeId, bool? ifDirectProvision, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, int? branchId, int? supplierId, int? warehouseId, int? InStatus, DateTime? RequestedDeliveryTimeBegin, DateTime? RequestedDeliveryTimeEnd/*, string SAPPurchasePlanCode, string hWPurOrderCode*/, string ERPSourceOrderCode/*, string CPPartsPurchaseOrderCode, string CPPartsInboundCheckCode*/, int? ioStatus, out string filename);

        /// <summary>
        /// 导出配件与供应商关系
        /// </summary>
        [OperationContract]
        bool ExportPartsSupplierRelation(int[] ids, int branchId, string supplierCode, string supplierName, string partCode, string partName, string overseasPartsFigure, int? partsSalesCategoryId, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName);

        /// <summary>
        /// 配件索赔供应商信息
        /// </summary>
        [OperationContract]
        bool ExportPartsClaimOrderDealerInformation(int[] ids, string usedPartsCode, string usedPartsName, string claimBillCode, string repairContractCode, int? status, string serialNumber, string dealerCode, string dealerName, string claimSupplierCode, string claimSupplierName, DateTime? createTimeBegin, DateTime? createTimeEnd, int repairClaimBranchId, out string fileName);

        /// <summary>
        ///  导出分公司与供应商关系
        /// </summary>
        [OperationContract]
        bool ExportBranchSupplierRelation(int[] ids, int? branchId, string supplierCode, string supplierName, string bussinessCode, string bussinessName, int? partsSalesCategoryId, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName);

        /// <summary>
        ///  导入修改分公司与供应商关系
        /// </summary>
        [OperationContract]
        bool ImportBranchSupplierRelationForUpdate(string fileName, out int excelImportNum, out List<BranchSupplierRelationExtend> errorData, out string errorDataFileName, out string errorMessage);

        /// <summary>
        /// 导出配件销售价
        /// </summary>
        [OperationContract]
        bool ExportPartsSalesPrice(string sparePartCode, string sparePartName, int? partsSalesCategoryId, int? status, bool? isSalable, bool? isOrderable, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName);

        /// <summary>
        /// 导出配件计划价
        /// </summary>
        [OperationContract]
        bool ExportPartsPlannedPrice(string sparePartCode, string sparePartName, int? partsSalesCategoryId, DateTime? createTimeBegin, DateTime? createTimeEnd, int ownerCompanyId, out string fileName);

        /// <summary>
        /// 批量导出计划价申请单主清单
        /// </summary>
        [OperationContract]
        bool ExportPlanndPriceAppWithDetail(int? id, int ownerCompanyId, string code, int? partsSalesCategoryId, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName);

        /// <summary>
        /// 批量导出计划价申请单主单
        /// </summary>
        [OperationContract]
        bool ExportPlanndPriceApp(int[] ids, int ownerCompanyId, string code, int? partsSalesCategoryId, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName);

        /// <summary>
        /// 导出售前检查单
        /// </summary>
        [OperationContract]
        bool ExportPreSaleCheckOrder(int? branchId, string code, int? partsSalesCategoryId, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, int? settleStatus, string dealerCode, string dealerName, out string fileName);

        /// <summary>
        /// 批量导出售前检查单
        /// </summary>
        [OperationContract]
        bool ExportPreSaleCheckOrders(int? branchId, string code, int? status, int? partsSalesCategoryId, string vin, DateTime? createTimeBegin, DateTime? createTimeEnd, int? settleStatus, int[] preSaleCheckOrderIds, string dealerCode, string dealerName, out string fileName);

        /// <summary>  
        /// 导出电商配件退货订单主清单
        /// </summary>
        [OperationContract]
        bool ExportRetailerServiceApplyWithDetail(int[] ids, string OrderNumber, DateTime? syncTimeBegin, DateTime? syncTimeEnd, out string fileName);

        /// <summary>
        /// 导出配件分品牌商务信息
        /// </summary>
        [OperationContract]
        bool ExportPartsBranch(int? branchId, string partCode, string partName, int? partAbc, string partsWarrantyCategoryName, int? productLifeCycle, int? status, int? partsSalesCategoryId, string referenceCode, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName);

        /// <summary>
        /// 根据ID导出配件分品牌商务信息
        /// </summary>
        [OperationContract]
        bool ExportForPartsBranch(int[] ids, int? branchId, string partCode, string partName, int? partAbc, string partsWarrantyCategoryName, int? productLifeCycle, int? status, int? partsSalesCategoryId, int? plannedPriceCategory, string referenceCode, DateTime? createTimeBegin, DateTime? createTimeEnd, string partsSalePriceIncreaseRateGroupCode, string partsSalePriceIncreaseRateGroupName, int? partsReturnPolicy, bool? isSalable, bool? isOrderable, bool? isDirectSupply, string partsEnglishName, DateTime? modifyTimeBegin, DateTime? modifyTimeEnd, out string fileName);

        /// <summary>
        /// 导出配件替换件信息
        /// </summary>
        [OperationContract]
        bool ExportPartsReplacement(bool? isInterFace, bool? isSPM, string oldPartCode, string oldPartName, int? status, out string fileName);

        /// <summary>
        /// 更新站间距离
        /// </summary>
        [OperationContract]
        bool 更新站间距离(string EnterpriseCode, out string errorMessage);

        /// <summary>
        /// 更新站间距离分品牌
        /// </summary>
        [OperationContract]
        bool 更新站间距离分品牌(string EnterpriseCode, out string errorMessage);

        /// <summary>
        /// 导出配件拆散件信息
        /// </summary>
        [OperationContract]
        bool ExportPartDeleaveInformation(int? id, string oldPartCode, string deleavePartCode, string deleavePartName, int? partsSalesCategoryId, DateTime? createTimeBegin, DateTime? createTimeEnd, int? status, out string fileName);

        /// <summary>
        /// 导出市场部
        /// </summary>
        [OperationContract]
        bool ExportMarketingDepartment(string code, string name, int? branchId, int? partsSalesCategoryId, int? businessType, out string fileName);

        /// <summary>
        /// 导出市场部与人员关系
        /// </summary>
        [OperationContract]
        bool ExportMarketDptPersonnelRelation(string departmentCode, string departmentName, string personnelName, int? type, int? status, int companyType, out string fileName);

        /// <summary>
        /// 导出配件互换信息
        /// </summary>
        [OperationContract]
        bool ExportPartsExchange(string exchangeCode, string exchangeName, string partCode, string partName, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName);

        /// <summary>
        /// 导出运距信息
        /// </summary>
        [OperationContract]
        bool ExportHauDistanceInfor(string companyCode, string companyName, string storageCompanyCode, string storageCompanyName, string warehouseCode, string warehouseName, DateTime? createTimeBegin, DateTime? createTimeEnd, int? status, out string fileName);

        /// <summary>
        /// 导出配件信息
        /// </summary>
        [OperationContract]
        bool ExportSparePart(int[] ids, string code, string name, int? status, DateTime? createDateTimeStart, DateTime? createDateTimeEnd, DateTime? modifyDateTimeStart, DateTime? modifyDateTimeEnd, string ReferenceCode, string OverseasPartsFigure, string exchangeIdentification, string exGroupCode, bool? isExactExport, out string fileName);

        /// <summary>
        /// 校验导入配件信息
        /// </summary>
        [OperationContract]
        bool ImportSparepart(string fileName, out int excelImportNum, out List<SparePartExtend> rightData, out List<SparePartExtend> errorData, out string errorDataFileName, out string errorMessage);

        /// <summary>
        /// 导出
        /// </summary>
        [OperationContract]
        bool ExportDealer(int? id, string code, string name, int? status, DateTime? createTimeStart, DateTime? createTimeEnd, out string fileName);

        /// <summary>
        /// 批量替换配件名称
        /// </summary>
        [OperationContract]
        bool 批量替换配件名称(string fileName, out int excelImportNum, out List<SparePartExtend> rightData, out List<SparePartExtend> errorData, out string errorDataFileName, out string errorMessage);

        [OperationContract]
        bool 批量替换保外调拨(string fileName, out int excelImportNum, out List<SparePartExtend> rightData, out List<SparePartExtend> errorData, out string errorDataFileName, out string errorMessage);

        /// <summary>
        /// 批量替换标准名称
        /// </summary>
        [OperationContract]
        bool 批量替换标准名称(string fileName, out int excelImportNum, out List<SparePartExtend> rightData, out List<SparePartExtend> errorData, out string errorDataFileName, out string errorMessage);

        /// <summary>
        /// 批量替换规格型号
        /// </summary>
        [OperationContract]
        bool 批量替换规格型号(string fileName, out int excelImportNum, out List<SparePartExtend> rightData, out List<SparePartExtend> errorData, out string errorDataFileName, out string errorMessage);

        /// <summary>
        /// 批量替换英文名称
        /// </summary>
        [OperationContract]
        bool 批量替换英文名称(string fileName, out int excelImportNum, out List<SparePartExtend> rightData, out List<SparePartExtend> errorData, out string errorDataFileName, out string errorMessage);

        /// <summary>
        /// 批量替换配件类型
        /// </summary>
        [OperationContract]
        bool 批量替换配件类型(string fileName, out int excelImportNum, out List<SparePartExtend> rightData, out List<SparePartExtend> errorData, out string errorDataFileName, out string errorMessage);
        /// <summary>
        /// 批量修改追溯属性
        /// </summary>
        [OperationContract]
        bool 批量修改追溯属性(string fileName, out int excelImportNum, out List<SparePartExtend> rightData, out List<SparePartExtend> errorData, out string errorDataFileName, out string errorMessage);
        /// <summary>
        /// 批量替换特征说明
        /// </summary>
        [OperationContract]
        bool 批量替换特征说明(string fileName, out int excelImportNum, out List<SparePartExtend> rightData, out List<SparePartExtend> errorData, out string errorDataFileName, out string errorMessage);

        /// <summary>
        /// 批量替换零部件图号
        /// </summary>
        [OperationContract]
        bool 批量替换零部件图号(string fileName, out int excelImportNum, out List<SparePartExtend> rightData, out List<SparePartExtend> errorData, out string errorDataFileName, out string errorMessage);

        /// <summary>
        /// 批量替换海外配件图号
        /// </summary>
        [OperationContract]
        bool 批量替换海外配件图号(string fileName, out int excelImportNum, out List<SparePartExtend> rightData, out List<SparePartExtend> errorData, out string errorDataFileName, out string errorMessage);

        /// <summary>
        /// 批量替换审核上限
        /// </summary>
        [OperationContract]
        bool 批量替换审核上限(string fileName, out int excelImportNum, out List<PartsBranchExtend> rightData, out List<PartsBranchExtend> errorData, out string errorDataFileName, out string errorMessage);

        /// <summary>
        /// 批量替换IMS压缩号
        /// </summary>
        [OperationContract]
        bool 批量替换IMS压缩号(string fileName, out int excelImportNum, out List<SparePartExtend> rightData, out List<SparePartExtend> errorData, out string errorDataFileName, out string errorMessage);

        /// <summary>
        /// 批量替换IMS厂商号
        /// </summary>
        [OperationContract]
        bool 批量替换IMS厂商号(string fileName, out int excelImportNum, out List<SparePartExtend> rightData, out List<SparePartExtend> errorData, out string errorDataFileName, out string errorMessage);

        /// <summary>
        /// 批量替换最小包装数量
        /// </summary>
        [OperationContract]
        bool 批量替换最小包装数量(string fileName, out int excelImportNum, out List<SparePartExtend> rightData, out List<SparePartExtend> errorData, out string errorDataFileName, out string errorMessage);

        /// <summary>
        /// 批量替换下一替代件
        /// </summary>
        [OperationContract]
        bool 批量替换下一替代件(string fileName, out int excelImportNum, out List<SparePartExtend> rightData, out List<SparePartExtend> errorData, out string errorDataFileName, out string errorMessage);


        /// <summary>
        /// 单个批量导入配件基本信息
        /// </summary>
        [OperationContract]
        bool 批量导入修改配件基本信息(string fileName, out int excelImportNum, out List<SparePartExtend> rightData, out List<SparePartExtend> errorData, out string errorDataFileName, out string errorMessage);

        //批量停用
        [OperationContract]
        bool 批量停用配件信息(string fileName, out int excelImportNum, out List<SparePartExtend> rightData, out List<SparePartExtend> errorData, out string errorDataFileName, out string errorMessage);

        //批量恢复
        [OperationContract]
        bool 批量恢复配件信息(string fileName, out int excelImportNum, out List<SparePartExtend> rightData, out List<SparePartExtend> errorData, out string errorDataFileName, out string errorMessage);

        /// <summary>
        /// 批量替换产品商标
        /// </summary>
        [OperationContract]
        bool 批量替换产品商标(string fileName, out int excelImportNum, out List<SparePartExtend> rightData, out List<SparePartExtend> errorData, out string errorDataFileName, out string errorMessage);

        /// <summary>
        /// 导出经销商配件盘点单主单清单
        /// </summary>
        [OperationContract]
        bool 导出经销商配件盘点单主单清单(int[] ids, int? branchId, string code, string storageCompanyCode, string storageCompanyName, int? salesCategoryId, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, int? amountDifferenceRange, string agencyName, out string fileName);

        /// <summary>
        /// 校验运距信息
        /// </summary>
        [OperationContract]
        bool ImportHauDistanceInfor(string fileName, out int excelImportNum, out List<HauDistanceInforExtend> errorData, out string errorDataFileName, out string errorMessage);

        /// <summary>
        /// 导出旧件发运主清单
        /// </summary>
        [OperationContract]
        bool ExportUsedPartsShippingOrderWithDetail(int? billId, int? branchId, int? partsSalesCategoryId, string code, int? shippingMethod, int? destinationwarehouseid, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName);

        /// <summary>
        /// 合并导出旧件发运确认单
        /// </summary>
        [OperationContract]
        bool ExportUsedPartsShippingOrderForConfirmWithDetail(int? billId, int personnelId, int? branchId, int? partsSalesCategoryId, string dealerCode, string dealerName, string code, int? shippingMethod, int? destinationwarehouseid, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName);

        /// <summary>
        /// 校验导入配件分品牌商务信息
        /// </summary>
        [OperationContract]
        bool ImportPartsBranch(string fileName, out int excelImportNum, out List<PartsBranchExtend> rightData, out List<PartsBranchExtend> errorData, out string errorDataFileName, out string errorMessage);

        /// <summary>
        /// 校验导入修改配件分品牌商务信息
        /// </summary>
        [OperationContract]
        bool ImportUpdatePartsBranch(string fileName, out int excelImportNum, out List<PartsBranchExtend> rightData, out List<PartsBranchExtend> errorData, out string errorDataFileName, out string errorMessage);

        /// <summary>
        /// 校验导入修改配件常规采购计划
        /// </summary>
        [OperationContract]
        bool ImportUpdatePartConPurchasePlan(int branchId, string fileName, out int excelImportNum, out List<PartConPurchasePlanExtend> rightData, out List<PartConPurchasePlanExtend> errorData, out string errorDataFileName, out string errorMessage);

        /// <summary>
        /// 导出配件常规采购计划
        /// </summary>
        [OperationContract]
        bool ExportPartConPurchasePlan(int[] ids, int? partsSalesCategoryId, string partCode, string partName, int? partAbc, string primarySupplierCode, int? productLifeCycle, int? plannedPriceCategory, DateTime? dateBegin, DateTime? dateEnd, out string fileName);

        /// <summary>
        /// 批量替换可否采购
        /// </summary>
        [OperationContract]
        bool 批量替换可否采购(string fileName, out int excelImportNum, out List<PartsBranchExtend> rightData, out List<PartsBranchExtend> errorData, out string errorDataFileName, out string errorMessage);

        /// <summary>
        /// 批量替换可否销售及直供
        /// </summary>
        [OperationContract]
        bool 批量替换可否销售及直供(string fileName, out int excelImportNum, out List<PartsBranchExtend> rightData, out List<PartsBranchExtend> errorData, out string errorDataFileName, out string errorMessage);

        /// <summary>
        /// 批量替换加价率分组
        /// </summary>
        [OperationContract]
        bool 批量替换加价率分组(string fileName, out int excelImportNum, out List<PartsBranchExtend> rightData, out List<PartsBranchExtend> errorData, out string errorDataFileName, out string errorMessage);

        /// <summary>
        /// 批量替换保修分类
        /// </summary>
        [OperationContract]
        bool 批量替换保修分类(string fileName, out int excelImportNum, out List<PartsBranchExtend> rightData, out List<PartsBranchExtend> errorData, out string errorDataFileName, out string errorMessage);

        /// <summary>
        /// 批量替换旧件返回政策
        /// </summary>
        [OperationContract]
        bool 批量替换旧件返回政策(string fileName, out int excelImportNum, out List<PartsBranchExtend> rightData, out List<PartsBranchExtend> errorData, out string errorDataFileName, out string errorMessage);

        /// <summary>
        /// 批量替换损耗类型及高低限
        /// </summary>
        [OperationContract]
        bool 批量替换损耗类型及高低限(string fileName, out int excelImportNum, out List<PartsBranchExtend> rightData, out List<PartsBranchExtend> errorData, out string errorDataFileName, out string errorMessage);

        /// <summary>
        /// 批量替换备注
        /// </summary>
        [OperationContract]
        bool 批量替换备注(string fileName, out int excelImportNum, out List<PartsBranchExtend> rightData, out List<PartsBranchExtend> errorData, out string errorDataFileName, out string errorMessage);

        /// <summary>
        /// 批量导入修改配件保修分类
        /// </summary>
        [OperationContract]
        bool 批量导入修改配件保修分类(string fileName, out int excelImportNum, out List<PartsBranchExtend> rightData, out List<PartsBranchExtend> errorData, out string errorDataFileName, out string errorMessage);


        //批量停用
        [OperationContract]
        bool 批量停用配件营销信息(string fileName, out int excelImportNum, out List<PartsBranchExtend> rightData, out List<PartsBranchExtend> errorData, out string errorDataFileName, out string errorMessage);

        //批量恢复
        [OperationContract]
        bool 批量恢复配件营销信息(string fileName, out int excelImportNum, out List<PartsBranchExtend> rightData, out List<PartsBranchExtend> errorData, out string errorDataFileName, out string errorMessage);

        //批量作废
        [OperationContract]
        bool 批量作废配件营销信息(string fileName, out int excelImportNum, out List<PartsBranchExtend> rightData, out List<PartsBranchExtend> errorData, out string errorDataFileName, out string errorMessage);

        /// <summary>
        /// 批量导入客户信息个人客户
        /// </summary>
        [OperationContract]
        bool 批量导入客户信息(string fileName, out int excelImportNum, out List<RetainedCustomerExtend> rightData, out List<RetainedCustomerExtend> errorData, out string errorDataFileName, out string errorMessage);

        /// <summary>
        /// 批量导入客户信息组织客户
        /// </summary>
        [OperationContract]
        bool 批量导入组织客户信息(string fileName, out int excelImportNum, out List<RetainedCustomerExtend> rightData, out List<RetainedCustomerExtend> errorData, out string errorDataFileName, out string errorMessage);

        /// <summary>
        /// 批量导出销售退货单主清单
        /// </summary>
        [OperationContract]
        bool ExportPartsSalesReturnBillWithDetail(int? id, int? personnelId, string returnCompanyName, int? returnCompanyId, int? ownerCompanyId, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, int? returnType, string salesUnitName, int inType, int personnelOrCompanyId, string eRPOrderCode, string code, string partsSalesOrderCode, out string fileName);

        /// <summary>
        /// 批量导入配件采购退货单清单
        /// </summary>
        [OperationContract]
        bool ImportPartsPurReturnOrderDetail(string fileName, int supplierId, int warehouseId, int branchId, int partsSalesCategory, out List<ImpPartsPurReturnOrderDetail> rightData, out int excelImportNum, out List<ImpPartsPurReturnOrderDetail> errorData, out string errorDataFileName, out string errorMessage);

        /// <summary>
        /// 采购订单批量导入数据校验
        /// </summary>
        [OperationContract]
        bool ImportPartsPurchaseOrderDetail(string fileName, out int excelImportNum, int branchId, out List<PartsPurchaseOrderDetailExtend> rightData, out List<PartsPurchaseOrderDetailExtend> errorData, out string errorDataFileName, out string errorMessage);

        /// <summary>
        /// 校验采购价格变更申请单清单导入配件
        /// </summary>
        [OperationContract]
        bool ImpPartsPurchasePricingDetail(string fileName, int partsSalesCategoryId, int branchId, out int excelImportNum, out List<ImpPartsPurchasePricingDetail> rightData, out List<ImpPartsPurchasePricingDetail> errorData, out string errorDataFileName, out string errorMessage);

        /// <summary>
        /// 校验采购价格变更申请单清单营销导入配件
        /// </summary>
        [OperationContract]
        bool ImpPartsPurchasePricingYXDetail(string fileName, int branchId, out int excelImportNum, out List<ImpPartsPurchasePricingDetail> rightData, out List<ImpPartsPurchasePricingDetail> errorData, out string errorDataFileName, out string errorMessage);

        /// <summary>
        /// 导入配件与供应商关系
        /// </summary>
        [OperationContract]
        bool ImpPartsSupplierRelation(string fileName, out int excelImportNum, out List<ImpPartsSupplierRelation> errorData, out string errorDataFileName, out string errorMessage);

        /// <summary>
        /// 导入计划价申请单,校验配件有效性
        /// </summary>
        [OperationContract]
        bool ImportPlannedPriceAppDetail(PlannedPriceAppExtend plannedPriceApp, string fileName, out int excelImportNum, out List<ImpPlannedPriceAppDetail> rightData, out List<ImpPlannedPriceAppDetail> errorData, out string errorDataFileName, out string errorMessage);

        /// <summary>
        /// 导入分公司与供应商关系
        /// </summary>
        [OperationContract]
        bool ImportBranchSupplierRelation(string fileName, out int excelImportNum, out List<BranchSupplierRelationExtend> errorData, out string errorDataFileName, out string errorMessage);

        /// <summary>
        /// 导入市场部
        /// </summary>
        [OperationContract]
        bool ImportMarketingDepartment(string fileName, out int excelImportNum, out List<MarketingDepartmentExtend> errorData, out string errorDataFileName, out string errorMessage);

        /// <summary>
        /// 市场部与人员关系
        /// </summary>
        [OperationContract]
        bool ImportMarketDptPersonnelRelation(string fileName, out int excelImportNum, out List<MarketDptPersonnelRelationExtend> errorData, out string errorDataFileName, out string errorMessage);

        /// <summary>
        ///导入配件替换件
        /// </summary>
        [OperationContract]
        bool ImportPartsReplacement(string fileName, out int excelImportNum, out List<PartsReplacementExtend> errorData, out string errorDataFileName, out string errorMessage);

        /// <summary>
        ///导入配件互换件
        /// </summary>
        [OperationContract]
        bool ImportPartsExchange(string fileName, out int excelImportNum, out List<PartsExchangeExtend> errorData, out string errorDataFileName, out string errorMessage);

        /// <summary>
        ///导入配件拆散件
        /// </summary>
        [OperationContract]
        bool ImportPartDeleaveInformation(string fileName, out int excelImportNum, int branchId, out List<PartDeleaveInformationExtend> errorData, out string errorDataFileName, out string errorMessage);

        /// <summary>
        /// 批量导入保外扣补款单
        /// </summary>
        [OperationContract]
        bool ImportOutofWarrantyPayment(string fileName, out int excelImportNum, int branchId, out List<OutofWarrantyPaymentExtend> rightData, out List<OutofWarrantyPaymentExtend> errorData, out string errorDataFileName, out string errorMessage);

        /// <summary>
        /// 导入配件销售价格信息
        /// </summary>
        [OperationContract]
        bool ImportPartsSalesPriceChangeDetail(string fileName, out int excelImportNum, out List<PartsSalesPriceChangeDetailExtend> rightData, out List<PartsSalesPriceChangeDetailExtend> errorData, out string errorDataFileName, out string errorMessage);

        [OperationContract]
        bool ImportPartsSalesPriceChangeDetailPartsSalesCategory(string fileName, out int excelImportNum, out List<PartsSalesPriceChangeDetailExtend> rightData, out List<PartsSalesPriceChangeDetailExtend> errorData, out string errorDataFileName, out string errorMessage);

        /// <summary>
        /// 导入售前检查项目
        /// </summary>
        [OperationContract]
        bool ImportPreSaleItem(string fileName, out int excelImportNum, int branchId, int partsSalesCategoryId, out List<PreSaleItemExtend> rightData, out List<PreSaleItemExtend> errorData, out string errorDataFileName, out string errorMessage);

        /// <summary>
        /// 导出旧件运距信息
        /// </summary>
        [OperationContract]
        bool ExportUsedPartsDistanceInfor(string companyCode, string companyName, string storageCompanyCode, string storageCompanyName, string warehouseCode, string warehouseName, DateTime? createTimeBegin, DateTime? createTimeEnd, int? status, out string fileName);

        /// <summary>
        /// 导出旧件调拨单清单
        /// </summary>
        [OperationContract]
        bool ExportUsedPartsTransferDetail(int[] ids, int personnelId, string code, int? status, string originWarehouseCode, string destinationWarehouseCode, string originWarehouseName, string destinationWarehouseName, int? outboundStatus, int? inboundStatus, DateTime? createTimeBegin, DateTime? createTimeEnd, DateTime? approveTimeBegin, DateTime? approveTimeEnd, out string fileName);

        /// <summary>
        /// 导入旧件运距信息
        /// </summary>
        [OperationContract]
        bool ImportUsedPartsDistanceInfor(string fileName, out int excelImportNum, out List<UsedPartsDistanceInforExtend> errorData, out string errorDataFileName, out string errorMessage);

        /// <summary>
        /// 导出服务产品线与产品关系
        /// </summary>
        [OperationContract]
        bool ExportServiceProdLineProduct(string vehicleBrandCode, string vehicleBrandName, string subBrandCode, string subBrandName, string terraceCode, string terraceName, string vehicleProductLine, string vehicleProductName, out string fileName);

        /// <summary>
        /// 校验销售退货导入配件清单
        /// </summary>
        [OperationContract]
        bool ImportPartsSalesReturnBillDetail(string fileName, out int excelImportNum, int companyId, int? warehouseId, int returnType, int branchId, int salesUnitId, int partsSalesCategoryId, int companyType, out List<PartsSalesReturnBillDetailExtend> rightData, out List<PartsSalesReturnBillDetailExtend> errorData, out string errorDataFileName, out string errorMessage);

        /// <summary>
        /// 导出配件返利帐户
        /// </summary>
        [OperationContract]
        bool ExportPartsRebateAccount(int? branchId, string customerCompanyCode, string customerCompanyName, DateTime? createTimeBegin, DateTime? createTimeEnd, int? accountGroupId, string businessCode, out string fileName);

        /// <summary>
        /// 采购订单定时下载
        /// </summary>
        [OperationContract]
        bool ScheduleExportPartsPurchaseOrderWithDetail(/*int? partsSalesCategoryId,*/ string code, string warehouseName/*, string partsSupplierCode*/, string partsSupplierName, int? partsPurchaseOrderTypeId, bool? ifDirectProvision, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, int? branchId, int? supplierId, int? warehouseId, string jobName, out string exportDataFileName);

        /// <summary>
        /// 销售订单定时下载
        /// </summary>
        [OperationContract]
        bool ScheduleExportPartsSalesOrderWithDetails(int? personnelId, int? ownercompanyId, string submitCompanyName, int? invoiceReceiveCompanyId, string invoiceReceiveCompanyName, bool? ifDirectProvision, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, int? partsSalesOrderTypeId, int? salesUnitId, string jobName, out string fileName);

        /// <summary>
        /// 导入配件客户信息
        /// </summary>
        [OperationContract]
        bool ImportCustomerInformation(string fileName, out int excelImportNum, out List<CustomerInformationExtend> errorData, out string errorDataFileName, out string errorMessage);

        /// <summary>
        /// 导入配件特殊协议价
        /// </summary>
        [OperationContract]
        bool ImportSpecialTreatyPriceChange(string fileName, out int excelImportNum, out List<SpecialTreatyPriceChangeWithDetailExtend> rightData, out List<SpecialTreatyPriceChangeWithDetailExtend> errorData, out string errorDataFileName, out string errorMessage);

        /// <summary>
        /// 批量导出配件库存明细
        /// </summary>
        [OperationContract]
        bool WarehouseAreaWithPartsStock(int? warehouseId, int? areaCategory, string sparePartCode, string sparePartName, string warehouseAreaCode, out string fileName);

        /// <summary>
        /// 导入代理库与经销商隶属关系
        /// </summary>
        [OperationContract]
        bool ImportAgencyDealerRelation(string fileName, out int excelImportNum, out List<AgencyDealerRelationExtend> errorData, out string errorDataFileName, out string errorMessage);

        /// <summary>
        /// 导出代理库与经销商隶属关系
        /// </summary>
        [OperationContract]
        bool ExportAgencyDealerRelation(string agencyCode, string agencyName, string dealerCode, string dealerName, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName);

        /// <summary>
        /// 定时导出配件库存明细
        /// </summary>
        [OperationContract]
        bool ScheduleWarehouseAreaWithPartsStock(int? warehouseId, int? areaCategory, string sparePartCode, string sparePartName, string warehouseAreaCode, string jobName, out string fileName);

        /// <summary>
        /// 合并导出配件出库计划主清单
        /// </summary>
        [OperationContract]
        bool ExportPartsOutboundPlanWithDetail(int? id, int? userId, int storageCompanyId, string partsOutboundPlanCode, string sourceCode, int? warehouseId, int? outboundType, DateTime? createTimeBegin, DateTime? createTimeEnd, string counterpartCompanyCode, string counterpartCompanyName, string partsSalesOrderTypeName, int? partsSalesCategoryId, int? status, string ERPSourceOrderCode, out string fileName);

        /// <summary>
        /// 导入服务站配件盘点清单(导到界面上)
        /// </summary>
        [OperationContract]
        bool ImportDealerPartsInventoryBillForReport(int dealerId, int salesCategoryId, string fileName, out int excelImportNum, out List<DealerPartsInventoryDetail> rightData, out List<DealerPartsInventoryDetail> errorData, out string errorDataFileName, out string errorMessage);

        /// <summary>
        /// 合并导出采购退货单主清单
        /// </summary>
        [OperationContract]
        bool ExportPartsPurReturnOrderWithDetail(int? id, string partsSupplierCode, string partsSupplierName, int? warehouseId, int? partsSalesCategoryId, string partsPurchaseOrderCode, string code, int? returnReason, int? status, int? outStatus, DateTime? createTimeBegin, DateTime? createTimeEnd, out string filename);

        /// <summary>
        /// 出采购退货单主清单
        /// </summary>
        [OperationContract]
        bool ExportPartsPurReturnOrder(int[] ids, string partsSupplierCode, string partsSupplierName, int? warehouseId, int? partsSalesCategoryId, string partsPurchaseOrderCode, int? returnReason, int? status, int? outStatus, DateTime? createTimeBegin, DateTime? createTimeEnd, out string filename);

        /// <summary>
        /// 导入库区
        /// </summary>
        [OperationContract]
        bool ImportWarehouseArea(int parentId, string fileName, out int excelImportNum, out List<WarehouseAreaExtend> rightData, out List<WarehouseAreaExtend> errorData, out string errorDataFileName, out string errorMessage);

        /// <summary>
        /// 导入库位
        /// </summary>
        [OperationContract]
        bool ImportWarehouseRegion(int parentId, string fileName, out int excelImportNum, out List<WarehouseAreaExtend> rightData, out List<WarehouseAreaExtend> errorData, out string errorDataFileName, out string errorMessage);

        /// <summary>
        /// 导入旧件清退单清单
        /// </summary>
        [OperationContract]
        bool ImportUsedPartsReturnDetail(int usedWarehouseId, string fileName, out int excelImportNum, out List<UsedPartsReturnDetailExtend> rightData, out List<UsedPartsReturnDetailExtend> errorData, out string errorDataFileName, out string errorMessage);

        /// <summary>
        /// 导出旧件清退单清单
        /// </summary>
        [OperationContract]
        bool ExportUsedPartsReturnDetail(int[] usedPartsReturnOrderIds, out string fileName);

        /// <summary>
        /// 导出配件库位库存
        /// </summary>
        [OperationContract]
        bool ExportPartsStockWithWarehouseArea(int? partsOutPlanId, int? warehouseAreaCategoryValue, int? operatorId, int? managerId, string warehouseRegionCode, out string fileName);

        /// <summary>
        /// 导入旧件处理单清单
        /// </summary>
        [OperationContract]
        bool ImportUsedPartsDisposalDetail(int usedPartsWarehouseId, string fileName, out int excelImportNum, out List<UsedPartsDisposalDetailExtend> rightData, out List<UsedPartsDisposalDetailExtend> errorData, out string errorDataFileName, out string errorMessage);

        /// <summary>
        /// 导出旧件库存
        /// </summary>
        [OperationContract]
        bool ExportUsedPartsStock(int? usedPartsWarehouseId, string usedPartsBarCode, string claimBillCode, int? claimBillType, string code, string usedPartsCode, string usedPartsName, string usedPartsSupplierCode, string usedPartsSupplierName, string faultyPartsSupplierCode, string faultyPartsSupplierName, string responsibleUnitCode, string responsibleUnitName, DateTime? createTimeBegin, DateTime? createTimeEnd, bool? ifFaultyParts, out string fileName);

        /// <summary>
        /// 合并导出配件发运单
        /// </summary>
        [OperationContract]
        bool ExportPartsShippingOrder(int[] ids, int? settlementCompanyId, int? receivingCompanyId, string code, int? type, int? status, int? warehouseId, string warehouseName, DateTime? requestedArrivalDateBegin, DateTime? requestedArrivalDateEnd, DateTime? shippingDateBegin, DateTime? shippingDateEnd, DateTime? createTimeBegin, DateTime? createTimeEnd, DateTime? confirmedReceptionTimeBegin, DateTime? confirmedReceptionTimeEnd, string receivingCompanyCode, string receivingCompanyName, string receivingWarehouseName, int? partsSalesCategoryId, int? logisticCompanyId, string logisticCompanyCode, string logisticCompanyName, string expressCompany, string expressCompanyCode, int? closedLoopStatus, string eRPSOURCEORDERCODE, out string fileName);

        /// <summary>
        /// 合并导出配件发运单
        /// </summary>
        [OperationContract]
        bool ExportAgencyPartsShippingOrder(int[] ids, int? settlementCompanyId, int? receivingCompanyId, string code, int? type, int? status, int? warehouseId, string warehouseName, DateTime? requestedArrivalDateBegin, DateTime? requestedArrivalDateEnd, DateTime? shippingDateBegin, DateTime? shippingDateEnd, DateTime? createTimeBegin, DateTime? createTimeEnd, DateTime? confirmedReceptionTimeBegin, DateTime? confirmedReceptionTimeEnd, string receivingCompanyCode, string receivingCompanyName, string receivingWarehouseName, int? partsSalesCategoryId, int? logisticCompanyId, string logisticCompanyCode, string logisticCompanyName, string expressCompany, string expressCompanyCode, int? closedLoopStatus, string eRPSOURCEORDERCODE, out string fileName);

        /// <summary>
        /// 导出物流信息单
        /// </summary>
        [OperationContract]
        bool ExportLogistics(int[] ids, string shippingCode, string orderCode, DateTime? beginShippingDate, DateTime? endShippingDate, DateTime? beginApproveTime, DateTime? endApproveTime, DateTime? beginRequestedArrivalDate, DateTime? endRequestedArrivalDate, string warehouseName, string logisticName, int? signStatus, out string fileName);

        /// <summary>
        /// 合并导出物流信息单
        /// </summary>
        [OperationContract]
        bool ExportLogisticsWidthDetial(int[] ids, string shippingCode, string orderCode, DateTime? beginShippingDate, DateTime? endShippingDate, DateTime? beginApproveTime, DateTime? endApproveTime, DateTime? beginRequestedArrivalDate, DateTime? endRequestedArrivalDate, string warehouseName, string logisticName, int? signStatus, out string fileName);

        /// <summary>
        /// BO登录获取Token
        /// </summary>
        [OperationContract]
        string GetToken(string userName, string password, string cmsName, string authentication, int validMinutes, int validNumOfLogons);

        /// <summary>
        /// 导出代理库详细信息
        /// </summary>
        [OperationContract]
        bool ExportAgencyDetail(string name, string code, int? status, int? agencyId, out string fileName);

        /// <summary>
        /// 校验导入配件调拨单清单
        /// </summary>
        [OperationContract]
        bool ImportpartsTransferOrderDetail(string fileName, int originalWarehouseId, int storageCompanyId, out int excelImportNum, out List<PartsTransferOrderDetailExtend> rightData, out List<PartsTransferOrderDetailExtend> errorData, out string errorDataFileName, out string errorMessage);

        /// <summary>
        /// 导出配件库存明细
        /// </summary>
        [OperationContract]
        bool ExportPartsStockDetail(int storageCompanyId, int? warehouseId, string partsName, string partsCode, int? warehouseAreaCategoryId, string warehouseAreaCode, out string fileName);

        /// <summary>
        /// 导出仓库库存
        /// </summary>
        [OperationContract]
        bool ExportWarehousePartsStock(int[] ids, int[] warehouseIds, int storageCompanyId, int? warehouseId, string partsName, string[] partsCodes, int? partsSalesCategoryId, int? storageCenter, string referenceCode, bool? isExactExport, out string fileName);

        /// <summary>
        /// 合并导出配件入库计划主清单
        /// </summary>
        [OperationContract]
        bool ExportPartsInboundPlanWithDetail(int[] ids, int? partsSalesCategoryId, int userId, int? status, int storageCompanyId, string partsInboundPlanCode, string sourceCode, int? warehouseId, int? inboundType, DateTime? createTimeBegin, DateTime? createTimeEnd, DateTime? planDeliveryTimeBegin, DateTime? planDeliveryTimeEnd, string counterpartCompanyCode, string counterpartCompanyName, string sparePartCode, string sparePartName, string originalRequirementBillCode, out string fileName);

        /// <summary>
        /// 合并导出配件入库计划主清单ForQuery
        /// </summary>
        [OperationContract]
        bool ExportPartsInboundPlanWithDetailForQuery(int[] ids, int? partsSalesCategoryId, int? status, int? userId, int storageCompanyId, string partsInboundPlanCode, string sourceCode, int? warehouseId, int? inboundType, DateTime? createTimeBegin, DateTime? createTimeEnd, string counterpartCompanyCode, string counterpartCompanyName, int? partsPurchaseOrderTypeId, string SAPPurchasePlanCode, string sparepartCode, string sparepartName, string originalRequirementBillCode, DateTime? planDeliveryTimeBegin, DateTime? planDeliveryTimeEnd,bool? hasDifference, out string fileName);

        /// <summary>
        /// 合并导出配件出库主清单
        /// </summary>
        [OperationContract]
        //bool ExportPartsOutboundBillWithDetail(int[] ids, int? userId, int? partsSalesCategoryId, int storageCompanyId, string partsOutBoundBillCode, int? warehouseId, string counterpartcompanycode, string counterpartcompanyname, int? outBoundType, int? settlementStatus, string partsSalesOrderTypeName, string outboundPackPlanCode, string contractCode, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName);
        bool ExportPartsOutboundBillWithDetail(int[] ids, string eRPSourceOrderCode, int? userId, int? partsSalesCategoryId, int storageCompanyId, string partsOutBoundBillCode, int? warehouseId, string counterpartcompanycode, string counterpartcompanyname, int? outBoundType, int? settlementStatus, string partsSalesOrderTypeName, string partsOutboundPlanCode, string outboundPackPlanCode, string contractCode, string provinceName, DateTime? createTimeBegin, DateTime? createTimeEnd, string SAPPurchasePlanCode, string zpNumber, string cpPartsPurchaseOrderCode, string cpPartsInboundCheckCode, string sparePartCode, string sparePartName, out string fileName);

        /// <summary>
        /// 合并导出配件入库检验主清单
        /// </summary>
        [OperationContract]
        bool ExportPartsInboundCheckBillWithDetail(int[] ids, string originalRequirementBillCode, int? userId, int? storageCompanyId, string partsInboundCheckBillCode, int? warehouseId, int? counterpartCompanyId, string counterpartcompanyname, int? inboundType, int? settlementStatus, DateTime? createTimeBegin, DateTime? createTimeEnd, string sparePartCode, string sparePartName, string partsInboundPlanCode, string batchNumber,bool? hasDifference, out string fileName);
        /// <summary>
        /// 供应商合并导出配件入库检验主清单
        /// </summary>
        [OperationContract]
        bool ExportPartsInboundCheckBillWithDetailForSupplier(int[] ids, string originalRequirementBillCode, int? userId, int? partsSalesCategoryId, int? storageCompanyId, string partsInboundCheckBillCode, int? warehouseId, int? counterpartCompanyId, string counterpartcompanyname, string eRPOrderCode, string partsSalesOrderCode, int? partsSalesOrderInvoiceType, int? inboundType, int? settlementStatus, DateTime? createTimeBegin, DateTime? createTimeEnd, string sAPPurchasePlanCode, string cpPartsPurchaseOrderCode, string cpPartsInboundCheckCode, string sparePartCode, string sparePartName, out string fileName);

        /// <summary>
        /// 导出配件发运单
        /// </summary>
        [OperationContract]
        bool ExportPartsShippingOrderNotWithDetail(int[] ids, int? settlementCompanyId, int? receivingCompanyId, string code, int? type, int? status, int? warehouseId, string warehouseName, DateTime? requestedArrivalDateBegin, DateTime? requestedArrivalDateEnd, DateTime? shippingDateBegin, DateTime? shippingDateEnd, DateTime? createTimeBegin, DateTime? createTimeEnd, DateTime? confirmedReceptionTimeBegin, DateTime? confirmedReceptionTimeEnd, string receivingCompanyCode, string receivingCompanyName, string receivingWarehouseName, int? partsSalesCategoryId, int? logisticCompanyId, string logisticCompanyCode, string logisticCompanyName, string expressCompany, string expressCompanyCode, int? closedLoopStatus, string eRPSOURCEORDERCODE, out string fileName);

        /// <summary>
        /// 导出配件发运单
        /// </summary>
        [OperationContract]
        bool ExportAgencyPartsShippingOrderNotWithDetail(int[] ids, int? settlementCompanyId, int? receivingCompanyId, string code, int? type, int? status, int? warehouseId, string warehouseName, DateTime? requestedArrivalDateBegin, DateTime? requestedArrivalDateEnd, DateTime? shippingDateBegin, DateTime? shippingDateEnd, DateTime? createTimeBegin, DateTime? createTimeEnd, DateTime? confirmedReceptionTimeBegin, DateTime? confirmedReceptionTimeEnd, string receivingCompanyCode, string receivingCompanyName, string receivingWarehouseName, int? partsSalesCategoryId, int? logisticCompanyId, string logisticCompanyCode, string logisticCompanyName, string expressCompany, string expressCompanyCode, int? closedLoopStatus, string eRPSOURCEORDERCODE, out string fileName);


        /// <summary>
        /// 导出配件发运单的客户信息
        /// </summary>
        [OperationContract]
        bool ExportPartsShippingCustomerInfo(int[] ids, int? settlementCompanyId, int? receivingCompanyId, string code, int? type, int? status, int? warehouseId, string warehouseName, DateTime? requestedArrivalDateBegin, DateTime? requestedArrivalDateEnd, DateTime? shippingDateBegin, DateTime? shippingDateEnd, DateTime? createTimeBegin, DateTime? createTimeEnd, DateTime? confirmedReceptionTimeBegin, DateTime? confirmedReceptionTimeEnd, string receivingCompanyCode, string receivingCompanyName, string receivingWarehouseName, int? partsSalesCategoryId, int? logisticCompanyId, string logisticCompanyCode, string logisticCompanyName, string expressCompany, string expressCompanyCode, int? closedLoopStatus, string eRPSOURCEORDERCODE, out string fileName);

        /// <summary>
        /// 导出配件出库单清单
        /// </summary>
        [OperationContract]
        bool ExportPartsOutboundBillDetail(int partsOutboundBillId, out string fileName);

        /// <summary>
        /// 合并导出配件调拨单主清单
        /// </summary>
        [OperationContract]
        bool ExportpartsTransferOrderWithDetail(int? id, int storageCompanyId, string partsTransferOrderCode, string originalBillCode, int? originalWarehouseId, int? destWarehouseId, int? type, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, DateTime? approveTimeBegin, DateTime? approveTimeEnd, string SAPPurchasePlanCode, string eRPSourceOrderCode, out string fileName);

        /// <summary>
        /// 合并导出配件盘点主清单
        /// </summary>
        [OperationContract]
        bool ExportPartsInventoryBillWithDetail(int? id, int storageCompanyId, string partsInventoryBillCode, int? warehouseId, int? warehouseAreaCategory, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, int[] storageCompanyType, out string fileName);

        /// <summary>
        /// 导出配件盘点单
        /// </summary>
        [OperationContract]
        bool ExportPartsInventoryBill(int? id, int storageCompanyId, string partsInventoryBillCode, int? warehouseId, int? warehouseAreaCategory, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, int[] storageCompanyType, out string fileName);

        /// <summary>
        /// 导入配件盘点单清单
        /// </summary>
        [OperationContract]
        bool ImportPartsInventoryDetail(string fileName, out int excelImportNum, int warehouseId, int warehouseAreaCategory, int storageCompanyId, out List<PartsInventoryDetailExtend> rightData, out List<PartsInventoryDetailExtend> errorData, out string errorDataFileName, out string errorMessage);

        /// <summary>
        /// 合并导出配件移库主清单
        /// </summary>
        [OperationContract]
        bool ExportPartsShiftOrderWithDetail(int? id, int storageCompanyId, string partsShiftOrderCode, int? warehouseId, int? type, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd,int? shiftStatus, out string fileName);

        /// <summary>
        /// 合并导出配件外采申请主清单
        /// </summary>
        [OperationContract]
        bool ExportPartsOuterPurchaseChangeWithDetail(int[] ids, string partsOuterPurchaseChangeCode, string customerCompanyName, int? partsSalesCategoryrId, int? marketingdepartmentId, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName);

        /// <summary>
        /// 导出配件外采申请主清单
        /// </summary>
        [OperationContract]
        bool ExportForPartsOuterPurchaseChangeWithDetail(int[] ids, string partsOuterPurchaseChangeCode, string customerCompanyName, int? PartsSalesCategoryrId, int? marketingdepartmentId, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName);

        /// <summary>
        /// 结果录入导出配件盘点单清单
        /// </summary>
        [OperationContract]
        bool ExportPartsInventoryDetailByBillId(int partsInventoryBillId, out string fileName);

        /// <summary>
        /// 结果录入导入配件盘点单清单
        /// </summary>
        [OperationContract]
        bool ImportPartsInventoryDetailByBillId(int partsInventoryBillId, string fileName, out int excelImportNum, int warehouseId, int warehouseAreaCategory, int storageCompanyId, out List<PartsInventoryDetailExtend> rightData, out List<PartsInventoryDetailExtend> errorData, out string errorDataFileName, out string errorMessage);

        /// <summary>
        /// 导入配件库位分配
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="parentId"></param>
        /// <param name="excelImportNum"></param>
        /// <param name="rightData"></param>
        /// <param name="errorData"></param>
        /// <param name="errorDataFileName"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        [OperationContract]
        bool ImportWarehouseAreaForPartsStock(string fileName, int parentId, out int excelImportNum, out List<WarehouseAreaForStockExtend> rightData, out List<WarehouseAreaForStockExtend> errorData, out string errorDataFileName, out string errorMessage);

        /// <summary>
        /// 合并导出经销商配件盘点主清单
        /// </summary>
        [OperationContract]
        bool ExportDealerPartsInventoryBill(int[] ids, int? branchId, int? storageCompanyId, string code, string storageCompanyCode, string storageCompanyName, int? salesCategoryId, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName);

        /// <summary>
        /// 合并导出代理库配件盘点主清单
        /// </summary>
        [OperationContract]
        bool ExportAgentPartsInventoryBillWithDetail(int[] ids, int[] storageCompanyType, int? storageCompanyId, string storageCompanyCode, string storageCompanyName, int? warehouseId, string warehouseName, string code, int? warehouseAreaCategory, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName);

        /// <summary>
        /// 导出代理库配件出库计划单
        /// </summary>
        [OperationContract]
        bool ExportAgencyPartsOutboundPlanWithDetail(int? id, int? userId, int storageCompanyId, string partsOutboundPlanCode, string sourceCode, int? warehouseId, int? outboundType, DateTime? createTimeBegin, DateTime? createTimeEnd, string counterpartCompanyCode, string counterpartCompanyName, string partsSalesOrderTypeName, int? partsSalesCategoryId, int? status, out string fileName);

        /// <summary>
        /// 合并导出代理库配件出库计划单
        /// </summary>
        [OperationContract]
        bool ExportAgencyPartsOutboundPlanWithDetailForQuery(int[] ids, int? userId, int? partsSalesCategoryId, int storageCompanyId, string partsOutboundPlanCode, int? status, string sourceCode, int? warehouseId, int? outboundType, DateTime? createTimeBegin, DateTime? createTimeEnd, string counterpartCompanyCode, string counterpartCompanyName, string partsSalesOrderTypeName, out string fileName);

        /// <summary>
        /// 导出客户账户
        /// </summary>
        [OperationContract]
        bool ExportCustomerAccount(int[] customerAccountId, int accountGroupSalesCompanyId, string companyName, string companyCode, int? companyType, int? accountGroupId, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, string businessCode, out string fileName);

        /// <summary>
        /// 导出客户账户历史
        /// </summary>
        [OperationContract]
        bool ExportCustomerAccountHistory(int[] ids, int? currentCompanyType, int? accountGroupId, string companyCode, string companyName, int? companyType, DateTime? theDate, out string fileName);

        /// <summary>
        /// 导出服务站在代理库账户明细
        /// </summary>
        [OperationContract]
        bool ExportAgencyCustomerAccount(int[] ids, string dealerCode, string dealerName, int? businessType, string agencyCode, string agencyName, DateTime? processDateBegin, DateTime? processDateEnd, out string fileName);

        /// <summary>
        /// 导出采购退货结算单主清单
        /// </summary>
        [OperationContract]
        bool ExportPartsPurchaseRtnSettleBillandDetail(int[] partsPurchaseRtnSettleBillId, string code, string suppliercode, string suppliername, int? warehouseid, int? status, int? partsSalesCategoryId, DateTime? createTimeBegin, DateTime? createTimeEnd, string businessCode, DateTime? invoiceDateBegin, DateTime? invoiceDateEnd, out string fileName);

        /// <summary>
        /// 导出信用申请单
        /// </summary>
        [OperationContract]
        bool ExportCredenceApplication(int[] ids, int salesCompanyId, string customerCompanyCode, string customerCompanyName, int? status, DateTime? approveTimeBegin, DateTime? approveTimeEnd, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName);

        /// <summary>
        /// 导出虚拟信用申请单
        /// </summary>
        [OperationContract]
        bool ExportVirtualCredenceApplication(int[] ids, int salesCompanyId, string customerCompanyCode, string customerCompanyName, int? status, DateTime? approveTimeBegin, DateTime? approveTimeEnd, DateTime? createTimeBegin, DateTime? createTimeEnd, string businessCode, int? creditType, out string fileName);

        /// <summary>
        /// 导出来款分割单
        /// </summary>
        [OperationContract]
        bool ExportPaymentBeneficiaryList(int[] ids, int salesCompanyId, string companyName, string companyCode, string paymentBillCode, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName);

        /// <summary>
        /// 导出客户转账单
        /// </summary>
        [OperationContract]
        bool ExportCustomerTransferBill(int[] ids, int salesCompanyId, string outboundCustomerCompanyName, string outboundCustomerCompanyCode, string inboundCustomerCompanyName, string inboundCustomerCompanyCode, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, DateTime? invoiceDateBegin, DateTime? invoiceDateEnd, int? businessType, string code, out string fileName);

        /// <summary>
        /// 导出供应商账户
        /// </summary>
        [OperationContract]
        bool ExportSupplierAccount(int[] ids, int buyerCompanyId, string companyName, string companyCode, int? partsSalesCategoryId, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName);

        /// <summary>
        /// 导出账户变更明细
        /// </summary>
        [OperationContract]
        bool ExportCustomerAccountHisDetail(int[] ids, int enterpriseType, int? accountGroupId, int salesCompanyId, string companyName, string companyCode, string sourceCode, int? businessType, DateTime? processDateBegin, DateTime? processDateEnd, out string fileName);

        /// <summary>
        /// 导出客户账户变更明细
        /// </summary>
        [OperationContract]
        bool ExportCustomerAccountForHisDetail(int[] ids, int enterpriseType, int? accountGroupId, int salesCompanyId, string companyName, string companyCode, string sourceCode, int? businessType, DateTime? processDateBegin, DateTime? processDateEnd, string businessCode, DateTime? beginInvoiceDate, DateTime? endInvoiceDateout, int? serialType, out string fileName);

        /// <summary>
        /// 导出应付账款变更明细
        /// </summary>
        [OperationContract]
        bool ExportAccountPayableHistoryDetail(int[] ids, int? partsSalesCategoryId, int buyerCompanyId, string companyName, string companyCode, string sourceCode, int? businessType, DateTime? processDateBegin, DateTime? processDateEnd, out string fileName);

        /// <summary>
        /// 导出付款单
        /// </summary>
        [OperationContract]
        bool ExportPayOutBill(int[] ids, int buyerCompanyId, string companyName, string companyCode, int? partsSalesCategoryId, int? paymentMethod, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName);

        /// <summary>
        /// 导出配件返利变更明细
        /// </summary>
        [OperationContract]
        bool ExportPartsRebateChangeDetail(int[] ids, int? branchId, int? accountGroupId, string companyName, string companyCode, string sourceCode, int? sourceType, string accountGroupName, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName);

        /// <summary>
        /// 导出配件返利变更明细(虚拟实体)
        /// </summary>
        [OperationContract]
        //bool ExportVirtualPartsRebateChangeDetail(int[] ids, int? branchId, int? accountGroupId, string companyName, string companyCode, string sourceCode, int? sourceType, string accountGroupName, DateTime? createTimeBegin, DateTime? createTimeEnd, string businessCode, out string fileName);
        bool ExportVirtualPartsRebateChangeDetail(int[] ids, int? branchId, int? accountGroupId, string companyName, string companyCode, string sourceCode, int? sourceType, DateTime? createTimeBegin, DateTime? createTimeEnd, string businessCode, out string fileName);

        /// <summary>
        /// 导出财务快照设置配件历史库存
        /// </summary>
        [OperationContract]
        bool ExportPartsHistoryStock(int[] ids, string partCode, string partName, int? warehouseId, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName);

        /// <summary>
        /// 合并导出配件采购价格
        /// </summary>
        [OperationContract]
        bool ExportPartsPurchasePricing(int[] ids, int branchId, int? partsSalesCategoryId, string sparePartCode, string sparePartName, string partsSupplierCode, string partsSupplierName, bool? isOrderable, bool? isSalable, string referenceCode, string supplierPartCode, DateTime? availibleTime, bool? isExactExport, out string fileName);

        /// <summary>
        /// 导出历史出入库记录
        /// </summary>
        [OperationContract]
        bool ExportOutboundAndInboundBill(int sparePartId, int warehouseId, DateTime? createTimeBegin, DateTime? createTimeEnd, string warehouseAreaCode, out string fileName);

        /// <summary>
        /// 导出经销商配件库存
        /// </summary>
        [OperationContract]
        bool ExportDealerPartsStock(int[] ids, int dealerId, int? salesCategoryId, string sparePartCode, string sparePartName, out string fileName);

        /// <summary>
        /// 合并导出配件出库主单
        /// </summary>
        [OperationContract]
        //bool ExportPartsOutboundBill(int[] ids, int? userId, int? partsSalesCategoryId, int storageCompanyId, string partsOutBoundBillCode, int? warehouseId, string counterpartcompanycode, string counterpartcompanyname, int? outBoundType, int? settlementStatus, string partsSalesOrderTypeName, string outboundPackPlanCode, string contractCode, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName);
        bool ExportPartsOutboundBill(int[] ids, string eRPSourceOrderCode, int? userId, int? partsSalesCategoryId, int storageCompanyId, string partsOutBoundBillCode, int? warehouseId, string counterpartcompanycode, string counterpartcompanyname, int? outBoundType, int? settlementStatus, string partsSalesOrderTypeName, string partsOutBoundPlanCode, string outboundPackPlanCode, string contractCode, string provinceName, DateTime? createTimeBegin, DateTime? createTimeEnd, string SAPPurchasePlanCode, string zpNumber, string cpPartsPurchaseOrderCode, string cpPartsInboundCheckCode, out string fileName);

        /// <summary>
        /// 导出配件出库主单代理库
        /// </summary>
        [OperationContract]
        bool ExportAgencyPartsOutboundBill(int[] ids, int? userId, int? partsSalesCategoryId, int storageCompanyId, string partsOutBoundBillCode, int? warehouseId, string counterpartcompanycode, string counterpartcompanyname, int? outBoundType, int? settlementStatus, string partsSalesOrderTypeName, string partsOutBoundPlanCode, string outboundPackPlanCode, string contractCode, string provinceName, DateTime? createTimeBegin, DateTime? createTimeEnd, string ERPSourceOrderCode, out string fileName);

        /// <summary>
        /// 合并导出配件出库主清单代理库
        /// </summary>
        [OperationContract]
        bool ExportAgencyPartsOutboundBillWithDetail(int[] ids, int? userId, int? partsSalesCategoryId, int storageCompanyId, string partsOutBoundBillCode, int? warehouseId, string counterpartcompanycode, string counterpartcompanyname, int? outBoundType, int? settlementStatus, string partsSalesOrderTypeName, string partsOutboundPlanCode, string outboundPackPlanCode, string contractCode, string provinceName, DateTime? createTimeBegin, DateTime? createTimeEnd, string ERPSourceOrderCode, out string fileName);

        /// <summary>
        /// 根据关联单导出配件销售退货结算单清单
        /// </summary>
        [OperationContract]
        bool ExportPartsSalesRtnSettlementDetailByRefId(int[] inboundIds, out string fileName);

        /// <summary>
        /// 根据关联单导出配件销售结算单清单
        /// </summary>
        [OperationContract]
        bool ExportPartsSalesSettlementDetailByRefId(int[] inboundIds, int[] outboundIds, out string fileName);

        /// <summary>
        /// 根据关联单导出配件采购结算单清单
        /// </summary>
        [OperationContract]
        bool ExportPartsPurchaseSettleDetailByRefId(int[] inboundIds, int[] outboundIds, int? partsSalesCategoryId, int? partsSupplierId, out string fileName);

        /// <summary>
        ///导出经销商分公司管理信息
        /// </summary>
        [OperationContract]
        bool ExportDealerServiceInfo(int? dealerServiceInfoId, int branchId, string dealerCode, string dealerName, string businessCode, string businessName, int? partsSalesCategoryId, string marketingDepartmentName, int? usedPartsWarehouseId, DateTime? createTimeBegin, DateTime? createTimeEnd, int? status, int companyType, out string fileName);

        /// <summary>
        /// 导出保外扣补款单
        /// </summary>
        [OperationContract]
        bool ExportOutofWarrantyPayment(int[] ids, int? branchId, int? dealerId, int? panelBranchId, int? status, int? settlementStatus, int? transactionCategory, int? sourceType, string code, string sourceCode, string dealerCode, string dealerName, int? responsibleUnitId, int? partsSalesCategoryId, DateTime? createTimeBegin, DateTime? createTimeEnd, string companyCustomerCode, int? marketingDepartmentId, out string fileName);

        /// <summary>
        /// 分公司导出保外扣补款单
        /// </summary>
        [OperationContract]
        bool ExportOutofWarrantyPaymentForBranch(int[] ids, int? userId, int? branchId, int? dealerId, int? panelBranchId, int? status, int? settlementStatus, int? transactionCategory, int? sourceType, string code, string sourceCode, string dealerCode, string dealerName, int? responsibleUnitId, int? partsSalesCategoryId, DateTime? createTimeBegin, DateTime? createTimeEnd, string companyCustomerCode, int? marketingDepartmentId, out string fileName);

        /// <summary>
        /// 导出旧件发运单
        /// </summary>
        [OperationContract]
        bool ExportUsedPartsShippingOrder(int? billId, int? branchId, int? partsSalesCategoryId, string code, int? shippingMethod, int? destinationwarehouseid, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName);

        /// <summary>
        /// 导出旧件发运确认单
        /// </summary>
        [OperationContract]
        bool ExportUsedPartsShippingOrderForConfirm(int? billId, int personnelId, int? branchId, int? partsSalesCategoryId, string dealerCode, string dealerName, string code, int? shippingMethod, int? destinationwarehouseid, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName);

        /// <summary>
        /// 导出旧件出库单
        /// </summary>
        [OperationContract]
        bool ExportUsedPartsOutboundOrder(int[] ids, string code, int? outboundType, string usedPartsWarehouseCode, string usedPartsWarehouseName, DateTime? outboundTimeStart, DateTime? outboundTimeEnd, string sourceCode, string relatedCompanyCode, string relatedCompanyName, string usedPartsBarCode, string claimBillCode, bool? ifBillable, bool? ifAlreadySettled, out string fileName);

        /// <summary>
        /// 导出旧件出库单清单
        /// </summary>
        [OperationContract]
        bool ExportUsedPartsOutboundDetail(int[] usedPartsOutboundOrderIds, string code, int? outboundType, string usedPartsWarehouseCode, string usedPartsWarehouseName, DateTime? outboundTimeStart, DateTime? outboundTimeEnd, string sourceCode, string relatedCompanyCode, string relatedCompanyName, string usedPartsBarCode, string claimBillCode, bool? ifBillable, bool? ifAlreadySettled, out string fileName);

        /// <summary>
        /// 导出旧件入库单清单
        /// </summary>
        [OperationContract]
        bool ExportUsedPartsInboundDetail(int[] usedPartsInboundOrderIds, string code, int? inboundType, string usedPartsWarehouseCode, string usedPartsWarehouseName, DateTime? inboundTimeStart, DateTime? inboundTimeEnd, string sourceCode, string relatedCompanyCode, string relatedCompanyName, out string fileName);

        /// <summary>
        /// 导出服务站旧件库存
        /// </summary>
        [OperationContract]
        bool ExportSsUsedPartsStorage(int? billId, int? dealerId, int? branchId, string dealerCode, string dealerName, string claimBillCode, string usedPartsBarCode, string usedPartsCode, int? usedPartsReturnPolicy, int? partsSalesCategoryId, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName);

        /// <summary>
        /// 合并导出配件销售结算主清单
        /// </summary>
        [OperationContract]
        bool ExportPartsSalesSettlementWithDetails(int? billId, int? personId, int salesCompanyId, string code, string customerCompanyCode, int? partsSalesCategoryId, string customerCompanyName, int? accountGroupId, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, string creatorName, DateTime? invoiceDateBegin, DateTime? invoiceDateEnd, int? settleType, int? businessType, out string fileName);

        /// <summary>
        /// 合并导出配件销售结算主清单(销售结算单查询节点)
        /// </summary>
        [OperationContract]
        bool ExportPartsSalesSettlementForQueryWithDetails(int[] billIds, int customerCompanyId, string code, string customerCompanyCode, int? partsSalesCategoryId, string customerCompanyName, int? accountGroupId, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName);

        /// <summary>
        /// 合并导出配件销售退货结算主清单
        /// </summary>
        [OperationContract]
        bool ExportPartsSalesRtnSettlementWithDetails(int[] billIds, string code, string customerCompanyCode, int? partsSalesCategoryId, string customerCompanyName, int? accountGroupId, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, string creatorName, DateTime? invoiceDateBegin, DateTime? invoiceDateEnd, int? settleType, int? businessType, out string fileName);

        /// <summary>
        /// 校验内部领出单
        /// </summary>
        [OperationContract]
        bool ImportInternalAllocationDetail(int warehouseId, string fileName, out int excelImportNum, out List<InternalAllocationDetailExtend> errorData, out List<InternalAllocationDetailExtend> rightData, out string errorDataFileName, out string errorMessage);

        /// <summary>
        /// 校验内部领入单
        /// </summary>
        [OperationContract]
        bool ImportInternalAcquisitionDetail(int warehouseId, string fileName, out int excelImportNum, out List<InternalAcquisitionDetailExtend> errorData, out List<InternalAcquisitionDetailExtend> rightData, out string errorDataFileName, out string errorMessage);

        /// <summary>
        /// 导出内部领入单及清单
        /// </summary>
        [OperationContract]
        bool ExportInternalAcquisitionAndDetail(int[] ids, string code, int? warehouseid, string createname, string departmentName, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName);

        /// <summary>
        /// 导出内部领出单及清单
        /// </summary>
        [OperationContract]
        bool ExportInternalallocationbillAndDetail(int[] ids, string code, int? warehouseid, string createname, string departmentName, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, int? type, out string fileName);

        /// <summary>
        /// 导出旧件清退单
        /// </summary>
        [OperationContract]
        bool ExportUsedPartsReturnOrder(int[] usedPartsReturnOrderIds, int personnelId, string outboundWarehouseCode, string outboundWarehouseName, string returnOfficeCode, string returnOfficeName, string code, int? returnType, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName);

        /// <summary>
        /// 导出旧件调拨单
        /// </summary>
        [OperationContract]
        bool ExportUsedPartsTransferOrder(int? billId, int personnelId, string code, int? status, string originWarehouseCode, string destinationWarehouseCode, string originWarehouseName, string destinationWarehouseName, int? outboundStatus, int? inboundStatus, DateTime? createTimeBegin, DateTime? createTimeEnd, DateTime? approveTimeBegin, DateTime? approveTimeEnd, out string fileName);

        /// <summary>
        /// 校验二级站零售订单
        /// </summary>
        [OperationContract]
        bool ImportDealerPartsRetailOrder(int partsSalesCategoryId, int dealerId, int subDealerId, string fileName, out int excelImportNum, out List<DealerRetailOrderDetailExtend> errorData, out List<DealerRetailOrderDetailExtend> rightData, out string errorDataFileName, out string errorMessage);

        /// <summary>
        /// 导出旧件处理清单
        /// </summary>
        [OperationContract]
        bool ExportUsedPartsDisposalDetail(int? billId, int personnelId, string usedPartsWarehouseCode, string usedPartsWarehouseName, int? status, int? outboundStatus, string relatedCompanyName, int? usedPartsDisposalMethod, string code, DateTime? createTimeBegin, DateTime? createTimeEnd, DateTime? approveTimeBegin, DateTime? approveTimeEnd, out string fileName);

        /// <summary>
        /// 导入配件返利申请单
        /// </summary>
        [OperationContract]
        bool ImportPartsRebateApplication(int branchId, string fileName, out int excelImportNum, out List<PartsRebateApplicationExtend> rightData, out List<PartsRebateApplicationExtend> errorData, out string errorDataFileName, out string errorMessage);

        /// <summary>
        /// 导出配件客户信息
        /// </summary>
        [OperationContract]
        bool ExportCustomerInformation(int[] customerInformationId, string customerCompanyName, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName);

        /// <summary>
        /// 合并导出供应商发运单及清单
        /// </summary>
        [OperationContract]
        bool ExportSupplierShippingOrderWithDetail(int[] ids, int? branchId, string partsSupplierCode, string partsSupplierName, int? partsSalesCategoryId, int? receivingWarehouseId, string code, string partsPurchaseOrderCode, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName);

        /// <summary>
        /// 合并导出供应商直供发运单及清单
        /// </summary>
        [OperationContract]
        bool ExportSupplierShippingOrderForDirectWithDetail(int[] ids, int? branchId, string partsSupplierCode, string partsSupplierName, string code, int? partsSalesCategoryId, string partsSaleOrderCode, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName);

        /// <summary>
        /// 合并导出配件零售订单及清单
        /// </summary>
        [OperationContract]
        bool ExportPartsRetailOrderWithDetail(int[] ids, string customerName, int? warehouseId, string customerCellPhone, int? status, string code, string sparePartCode, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName);

        /// <summary>
        /// 合并导出配件零售退货单及详情
        /// </summary>
        [OperationContract]
        bool ExportPartsRetailReturnBillWithDetail(int[] ids, string customerName, string customerCellPhone, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName);

        //导入二级站
        [OperationContract]
        bool ImportSubDealer(string fileName, out int excelImportNum, int branchId, out List<SubDealerExtend> rightData, out List<SubDealerExtend> errorData, out string errorDataFileName, out string errorMessage);

        //导入经销商视频手机清单
        [OperationContract]
        bool ImportDealerMobileNumberList(string fileName, out List<DealerMobileNumberListExtend> rightData, out List<DealerMobileNumberListExtend> errorData, out string errorDataFileName, out string errorMessage);

        // 合并导出配件出库计划主清单ForQuery
        [OperationContract]
        bool ExportPartsOutboundPlanWithDetailForQuery(int[] ids, int? userId, int? partsSalesCategoryId, int storageCompanyId, string partsOutboundPlanCode, int? status, string sourceCode, int? warehouseId, int? outboundType, DateTime? createTimeBegin, DateTime? createTimeEnd, string counterpartCompanyCode, string counterpartCompanyName, string partsSalesOrderTypeName, string ERPSourceOrderCode, out string fileName);

        // 合并导出配件出库计划主清单ForQuery
        [OperationContract]
        bool ExportPartsOutboundPlanWithDetailForQuery1(int[] ids, int? userId, int storageCompanyId, string partsOutboundPlanCode, string status, string sourceCode, int? warehouseId, int? outboundType, DateTime? createTimeBegin, DateTime? createTimeEnd, string counterpartCompanyName, string partsSalesOrderTypeName, string SAPPurchasePlanCode, string ZPNUMBER, string eRPSourceOrderCode, string sparePartCode, string sparePartName, out string fileName);

        //导入特殊协议价
        [OperationContract]
        bool ImportPartsSpecialTreatyPrice(string fileName, out int excelImportNum, out List<PartsSpecialTreatyPriceExtend> rightData, out List<PartsSpecialTreatyPriceExtend> errorData, out string errorDataFileName, out string errorMessage);

        //导出来款单管理
        [OperationContract]
        bool ExportPaymentBill(int[] ids, string customerCompanyName, string customerCompanyCode, int? paymentMethod, int? status, string creatorName, DateTime? createTimeBegin, DateTime? createTimeEnd, int salesCompanyId, string businessCode, DateTime? invoiceDateBegin, DateTime? invoiceDateEnd, out string fileName);

        //导出人员与品牌关系
        [OperationContract]
        bool ExportPersonSalesCenterLink(int[] ids, int? partsSalesCategoryId, int? status, string personName, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName);

        //导出供应商货款预批单带清单
        [OperationContract]
        bool ExportSupplierPreApprovedLoanWithDetail(int[] ids, string preApprovedCode, int? partsSalesCategoryId, DateTime? createTimeBegin, DateTime? createTimeEnd, out string filename);

        //合并导出代理库配件盘点主清单(分公司用)
        [OperationContract]
        bool ExportAgentPartsInventoryBillWithDetailForBranch(int[] ids, int[] storageCompanyType, int? storageCompanyId, string storageCompanyCode, string storageCompanyName, int? warehouseId, string warehouseName, string code, int? warehouseAreaCategory, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, int? branchId, int? amountDifference, out string fileName);

        /// <summary>
        /// 服务站人员培训认证
        /// </summary>
        [OperationContract]
        bool ExportDealerPerTrainAut(int[] ids, int? partsSalesCategoryId, int? marketingDepartmentId, string dealerCode, string dealerName, DateTime? trainingTimeBegin, DateTime? trainingTimeEnd, DateTime? authenticationTimeBegin, DateTime? authenticationTimeEnd, int? positionId, int? status, string idCard, int? authenticationType, int? trainingType, string certificateId, string trainingName, out string fileName);

        /// <summary>
        /// 服务站人员培训认证导入
        /// </summary>
        [OperationContract]
        bool ImportDealerPerTrainAut(string fileName, out int excelImportNum, out List<DealerPerTrainAutExtend> rightData, out List<DealerPerTrainAutExtend> errorData, out string errorDataFileName, out string errorMessage);

        /// <summary>
        /// 修改导入配件与供应商关系
        /// </summary>
        [OperationContract]
        bool ImpUpdatePartsSupplierRelation(string fileName, out int excelImportNum, out List<ImpPartsSupplierRelation> errorData, out string errorDataFileName, out string errorMessage);

        /// <summary>
        /// 配件公司批量导出销售订单主清单
        /// </summary>
        [OperationContract]
        bool BranchExportPartsSalesOrderWhithDetails(int? personnelId, int? ownercompanyId, string code, int? orderType, string branchCode, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, string customerName, string customerCode, DateTime? submitTimeBegin, DateTime? submitTimeEnd, int? provinceId, string city, DateTime? stopTimeBegin, DateTime? stopTimeEnd, DateTime? approveTimeBegin, DateTime? approveTimeEnd, int? id, out string fileName);

        /// <summary>
        /// 导出销售订单
        /// </summary>
        [OperationContract]
        bool ExportPartsSalesOrders(int? personnelId, int? ownercompanyId, string code, int? orderType, string branchCode, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, string customerName, string customerCode, DateTime? submitTimeBegin, DateTime? submitTimeEnd, int? provinceId, string city, DateTime? stopTimeBegin, DateTime? stopTimeEnd, DateTime? approveTimeBegin, DateTime? approveTimeEnd, int? id, out string fileName);

        /// <summary>
        /// 导出配件入库检验单（计划价）
        /// </summary>
        [OperationContract]
        bool ExportPartsInboundCheckBillForPlannedPrice(int[] ids, int? storageCompanyId, string partsInboundCheckBillCode, int? partsSalesCategoryId, int? warehouseId, int? inboundType, string counterpartCompanyCode, string counterpartCompanyName, int? settlementStatus, DateTime? createTimeBegin, DateTime? createTimeEnd, string eRPSourceOrderCode, string sAPPurchasePlanCode, out string fileName);

        /// <summary>
        /// 合并导出配件入库检验单（计划价）
        /// </summary>
        [OperationContract]
        bool MergeExportPartsInboundCheckBillWithDetailForPlannedPrice(int[] ids, int? storageCompanyId, string partsInboundCheckBillCode, int? partsSalesCategoryId, int? warehouseId, int? inboundType, string counterpartCompanyCode, string counterpartCompanyName, int? settlementStatus, DateTime? createTimeBegin, DateTime? createTimeEnd, string eRPSourceOrderCode, string sAPPurchasePlanCode, out string fileName);

        /// <summary>
        /// 导入祸首件所属总成
        /// </summary>
        [OperationContract]
        bool ImportFaultyPartsSupplierAssembly(string fileName, out int excelImportNum, out List<FaultyPartsSupplierAssemblyExtend> rightData, out List<FaultyPartsSupplierAssemblyExtend> errorData, out string errorDataFileName, out string errorMessage);

        /// <summary>
        /// 导出祸首件所属总成
        /// </summary>
        [OperationContract]
        bool ExportFaultyPartsSupplierAssembly(int[] ids, string code, string name, int? status, out string fileName);

        /// <summary>  
        ///导出配件互换件价格不一致清单
        /// </summary>
        [OperationContract]
        bool ExportPartsExchangeByPrice(int[] ids, string exchangeCode, string partCode, string partName, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName);

        /// <summary>  
        ///批量导入经销商服务经营权限
        /// </summary>
        [OperationContract]
        bool ImportDealerServiceInfo(string fileName, out int excelImportNum, out List<DealerBusinessPermitExtend> rightData, out List<DealerBusinessPermitExtend> errorData, out string errorDataFileName, out string errorMessage);

        /// <summary>  
        ///批量导入修改经销商分公司管理信息
        /// </summary>
        [OperationContract]
        bool 批量导入修改经销商分公司管理信息(string fileName, out int excelImportNum, out List<DealerServiceInfoExtend> rightData, out List<DealerServiceInfoExtend> errorData, out string errorDataFileName, out string errorMessage);

        /// <summary>  
        /// 批量导入经销商分公司管理信息
        /// </summary>
        [OperationContract]
        bool 批量导入经销商分公司管理信息(string fileName, out int excelImportNum, out List<DealerServiceInfoExtend> rightData, out List<DealerServiceInfoExtend> errorData, out string errorDataFileName, out string errorMessage);

        /// <summary>  
        ///单个批量导入经销商基本信息
        /// </summary>
        [OperationContract]
        bool 批量导入修改经销商基本信息(int branchId, string fileName, out int excelImportNum, out List<DealerExtend> rightData, out List<DealerExtend> errorData, out string errorDataFileName, out string errorMessage);

        /// <summary>
        /// 批量导入经销商基本信息
        /// </summary>
        [OperationContract]
        bool 批量导入经销商基本信息(int branchId, string fileName, out int excelImportNum, out List<DealerExtend> rightData, out List<DealerExtend> errorData, out string errorDataFileName, out string errorMessage);

        /// <summary>  
        ///导出服务站在代理库账户余额历史记录
        /// </summary>
        [OperationContract]
        bool ExportCustomerAccountHistoryForAgency(int[] ids, int? currentCompanyType, int? accountGroupId, string companyCode, string companyName, int? companyType, DateTime? theDate, out string fileName);

        /// <summary>  
        ///导出客户信息 
        /// </summary>
        [OperationContract]
        bool ExportRetainedCustomer(int[] ids, string customerCustomerCode, string customerName, string customerCellPhoneNumber, string customerIdDocumentNumber, int? customerCustomerType, int? status, int? customerOccupationType, int? customerBusinessType, bool? customerIfVIP, int? vIPType, DateTime? createDateStart, DateTime? createDateEnd, DateTime? modifyDateStart, DateTime? modifyDateEnd, out string fileName);

        /// <summary>  
        ///批量导入修改经销商分公司管理信息 
        /// </summary>
        [OperationContract]
        bool ImportEditDealerServiceInfo(string fileName, out int excelImportNum, out List<DealerBusinessPermitExtend> rightData, out List<DealerBusinessPermitExtend> errorData, out string errorDataFileName, out string errorMessage);

        /// <summary>  
        ///导入客户服务评价明细 
        /// </summary>
        [OperationContract]
        bool ImportCustomerServiceEvaluate(string fileName, out int excelImportNum, int branchId, out List<CustomerServiceEvaluateExtend> rightData, out List<CustomerServiceEvaluateExtend> errorData, out string errorDataFileName, out string errorMessage);

        /// <summary>  
        ///导入客户服务评价明细 
        /// </summary>
        [OperationContract]
        bool ExportPartsSalesRtnSettlementQueryWithDetails(int[] billIds, int customerCompanyId, string code, string customerCompanyCode, int? partsSalesCategoryId, string customerCompanyName, int? accountGroupId, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName);

        /// <summary>  
        ///导入装车明细
        /// </summary>
        [OperationContract]
        bool ImportLoadingDetail(string fileName, out int excelImportNum, out List<LoadingDetailExtend> rightData, out List<LoadingDetailExtend> errorData, out string errorDataFileName, out string errorMessage);

        /// <summary>  
        /// 导出新电商订单接口日志主清单
        /// </summary>
        [OperationContract]
        bool ExportRetailerDeliveryWithDetail(int[] ids, DateTime? syncTimeBegin, DateTime? syncTimeEnd, string OrderNumber, out string fileName);

        /// <summary>  
        /// 批量送达确认
        /// </summary>
        [OperationContract]
        bool 批量送达确认(string fileName, out int excelImportNum, out List<PartsShippingOrderExtend> rightData, out List<PartsShippingOrderExtend> errorData, out string errorDataFileName, out string errorMessage);

        /// <summary>  
        /// 批量发货确认
        /// </summary>
        [OperationContract]
        bool 批量发货确认(string fileName, out int excelImportNum, out List<PartsShippingOrderExtend> rightData, out List<PartsShippingOrderExtend> errorData, out string errorDataFileName, out string errorMessage);


        /// <summary>  
        /// 批量送达确认
        /// </summary>
        [OperationContract]
        bool 批量送达确认代理库(string fileName, out int excelImportNum, out List<PartsShippingOrderExtend> rightData, out List<PartsShippingOrderExtend> errorData, out string errorDataFileName, out string errorMessage);

        /// <summary>  
        /// 批量发货确认
        /// </summary>
        [OperationContract]
        bool 批量发货确认代理库(string fileName, out int excelImportNum, out List<PartsShippingOrderExtend> rightData, out List<PartsShippingOrderExtend> errorData, out string errorDataFileName, out string errorMessage);

        [OperationContract]
        bool ExportDealerPartsStockQueryView(int[] partsSalesCategoryIds, string[] dealerCodes, string[] partCodes, int branchId, int? partsSalesCategoryId, string dealerCode, string partCode, bool? isStoreMoreThanZero, out string fileName, out string errmesage);

        //导入积压申请单清单
        [OperationContract]
        bool ImportOverstockPartsAppDetail(int branchId, int partsSalesCategoryId, int companyType, string fileName, out int excelImportNum, out List<OverstockPartsAppDetailExtend> rightData, out List<OverstockPartsAppDetailExtend> errorData, out string errorDataFileName, out string errorMessage);

        /// <summary>
        /// 导出配件计划价变更履历
        /// </summary>
        [OperationContract]
        bool ExportPlannedPriceAppHistory(string sparePartCode, string sparePartName, int? partsSalesCategoryId, DateTime? createTimeBegin, DateTime? createTimeEnd, int ownerCompanyId, out string fileName);

        //回访问卷 导出
        [OperationContract]
        bool ExportReturnVisitQuests(int[] billIds, string orderId, string dealerCode, int? partsSalesCategoryId, string dealerName, int? type, int? marketDepartmentId, DateTime? revisitDaysBegin, DateTime? revisitDaysEnd, out string fileName);

        //回访问卷 合并导出
        [OperationContract]
        bool ExportReturnVisitQuestWithDetails(int[] billIds, string orderId, string dealerCode, int? partsSalesCategoryId, string dealerName, int? type, int? marketDepartmentId, DateTime? revisitDaysBegin, DateTime? revisitDaysEnd, out string fileName);

        /// <summary>
        /// 快递公司单据导出
        /// </summary>
        [OperationContract]
        bool 快递公司单据导出(int[] ids, string expressCode, string expressName, int? status, out string fileName);

        /// <summary>
        /// 快递公司单据导入
        /// </summary>
        [OperationContract]
        bool 快递公司导入(string fileName, out int excelImportNum, out List<ExpressExtend> rightData, out List<ExpressExtend> errorData, out string errorDataFileName, out string errorMessage);

        /// <summary>
        /// 导出快递物流关系
        /// </summary>
        [OperationContract]
        bool 导出快递物流关系(int[] ids, int? focufingCore, string logisticsCompanyCode, string logisticsCompanyName, string expressCode, string expressName, out string fileName);

        /// <summary>
        /// 快递物流关系导入
        /// </summary>
        [OperationContract]
        bool 快递物流关系导入(string fileName, out int excelImportNum, out List<ExpressToLogisticExtend> rightData, out List<ExpressToLogisticExtend> errorData, out string errorDataFileName, out string errorMessage);

        /// <summary>
        /// 合并导出工程车配件出库单
        /// </summary>
        [OperationContract]
        bool 合并导出工程车配件出库单(int[] ids, string eRPSourceOrderCode, int? userId, int? partsSalesCategoryId, int storageCompanyId, string partsOutBoundBillCode, int? warehouseId, string counterpartcompanycode, string counterpartcompanyname, int? outBoundType, int? settlementStatus, string partsSalesOrderTypeName, string partsOutboundPlanCode, string outboundPackPlanCode, string contractCode, string provinceName, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName);

        //合并导出瑞沃配件发运单查询
        [OperationContract]
        bool ExportSendWayBill(int[] ids, string code, int? type, int? status, int? warehouseId, DateTime? requestedArrivalDateBegin, DateTime? requestedArrivalDateEnd, DateTime? shippingDateBegin, DateTime? shippingDateEnd, DateTime? createTimeBegin, DateTime? createTimeEnd, DateTime? confirmedReceptionTimeBegin, DateTime? confirmedReceptionTimeEnd, string receivingCompanyCode, string receivingCompanyName, string receivingWarehouseName, int? partsSalesCategoryId, string logisticCompanyName, out string fileName);

        //合并导出瑞沃配件发运单查询
        [OperationContract]
        bool ExportSendWayAgencyBill(int[] ids, string code, int? type, int? status, int? warehouseId, DateTime? requestedArrivalDateBegin, DateTime? requestedArrivalDateEnd, DateTime? shippingDateBegin, DateTime? shippingDateEnd, DateTime? createTimeBegin, DateTime? createTimeEnd, DateTime? confirmedReceptionTimeBegin, DateTime? confirmedReceptionTimeEnd, string receivingCompanyCode, string receivingCompanyName, string receivingWarehouseName, int? partsSalesCategoryId, string logisticCompanyName, out string fileName);

        /// <summary>
        /// 
        /// 导出工程车配件入库检验单查询
        /// </summary>
        [OperationContract]
        bool 导出工程车配件入库检验单查询(int[] ids, string code, string partsInboundPlanCode, string name, string warehouseName, int? inboundType, int? settlementStatus, string ERPSourceOrderCode, string partsSalesOrderCode, string counterpartCompanyCode, string counterpartCompanyName, DateTime? createTimeBegin, DateTime? createTimeEnd, int? invoiceType, string originalRequirementBillCode, out string fileName);

        /// <summary>
        /// 导出服务站配件零售订单审核清单
        /// </summary>
        [OperationContract]
        bool ExporttDealerPartsRetailOrdeForApprove(int[] ids, string dealerName, int? partsSalesCategoryId, string code, int? retailOrderType, string customer, string subDealerName, string dealerCode, int? status, DateTime? beginCreateTime, DateTime? endCreateTime, out string fileName);

        [OperationContract]
        bool ImportOrderapproveweekday(string fileName, out int excelImportNum, out List<OrderapproveweekdayExtend> rightData, out List<OrderapproveweekdayExtend> errorData, out string errorDataFileName, out string errorMessage);

        [OperationContract]
        bool ExportOrderapproveweekday(int? weeksName, int? partsSalesCategoryId, DateTime? createTimeBegin, DateTime? createTimeEnd, DateTime? modifyTimeBegin, DateTime? modifyTimeEnd, int? status, out string fileName);

        [OperationContract]
        bool ExportWarehouseSequence(int[] ids, string customerCompanyCode, string customerCompanyName, int? partsSalesCategoryId, int? status, bool? isAutoApprove, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName);

        [OperationContract]
        bool ImportWarehouseSequence(string fileName, out int excelImportNum, out List<WarehouseSequenceExtend> rightData, out List<WarehouseSequenceExtend> errorData, out string errorDataFileName, out string errorMessage);

        /// <summary>
        /// 导出配件仓库变更履历
        /// </summary>
        [OperationContract]
        bool ExportWarehouseAreaHistory(int[] ids, int? warehouseId, int? warehouseAreaCategoryId, string partCodes, string partName, string WarehouseAreaCode, out string fileName);

        [OperationContract]
        bool 导出EPC接口日志(int[] ids, string code, string name, DateTime? LastProcessTimeStart, DateTime? LastProcessTimeEnd, out string fileName);

        [OperationContract]
        bool 导出红包信息(int[] ids, string couponNo, string title, string tel, int? brandId, DateTime? createTimeBegin, DateTime? createTimeEnd, int? type, string lssuingUnit, string vin, out string fileName);

        [OperationContract]
        bool 导出积分订单(int[] ids, string bonusPointsOrderCode, string corporationCode, int? brandId, string corporationName, int? type, int? settlementStatus, string platFormCode, string serviceApplyCode, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName);

        /// <summary>
        /// 导出采购计划单
        /// </summary>
        [OperationContract]
        bool ExportPartsPurchasePlan_HW(int[] ids, string code, int? partsSalesCategoryId, int? partsPurchaseOrderTypeId, int? status, string SAPPurchasePlanCode, DateTime? requestedDeliveryTimeBegin, DateTime? requestedDeliveryTimeEnd, DateTime? createTimeBegin, DateTime? createTimeEnd, DateTime? modifyTimeBegin, DateTime? modifyTimeEnd, DateTime? stopTimeBegin, DateTime? stopTimeEnd, out string filename);

        /// <summary>
        /// 导出采购计划单清单
        /// </summary>
        [OperationContract]
        bool ExportPartsPurchasePlan_HWWithDetail(int[] ids, string code, int? partsSalesCategoryId, int? partsPurchaseOrderTypeId, int? status, string SAPPurchasePlanCode, DateTime? requestedDeliveryTimeBegin, DateTime? requestedDeliveryTimeEnd, DateTime? createTimeBegin, DateTime? createTimeEnd, DateTime? modifyTimeBegin, DateTime? modifyTimeEnd, DateTime? stopTimeBegin, DateTime? stopTimeEnd, out string filename);

        /// <summary>
        /// 导出积分汇总单
        /// </summary>
        [OperationContract]
        bool 导出积分汇总单(int[] ids, string bonusPointsSummaryCode, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, DateTime? checkTimeBegin, DateTime? checkTimeEnd, out string fileName);

        /// <summary>
        /// 合并导出积分汇总单
        /// </summary>
        [OperationContract]
        bool 合并导出积分汇总单(int[] ids, string bonusPointsSummaryCode, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, DateTime? checkTimeBegin, DateTime? checkTimeEnd, out string fileName);

        /// <summary>
        /// ExportWarehouseQueryView
        /// </summary>
        [OperationContract]
        bool ExportWarehouseQueryView(int[] warehouseIds, int StorageCompanyId, int branchId, string Code, string Name, int? StorageCenter, int? Status, out string fileName);

        /// <summary>
        /// 导出服务站配件盘点提报
        /// </summary>
        [OperationContract]
        bool 导出服务站配件盘点提报(int[] ids, int? branchId, int? storageCompanyId, string code, string storageCompanyCode, string storageCompanyName, int? salesCategoryId, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName);

        /// <summary>
        /// 导出积压件信息
        /// </summary>
        [OperationContract]
        bool 导出积压件信息(int[] ids, int EnterpriseId, string SparePartCode, string SparePartName, string StorageCompanyCode, string StorageCompanyName, string ProvinceName, string CityName, string CountyName, int? PartsSalesCategoryId, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName);

        /// <summary>
        /// 导出金税分类信息
        /// </summary>
        [OperationContract]
        bool 导出金税分类信息(int[] ids, string goldenTaxClassifyCode, string goldenTaxClassifyName, DateTime? createTimeBegin, DateTime? createTimeEnd, DateTime? modifyTimeBegin, DateTime? modifyTimeEnd, out string fileName);

        /// <summary>
        /// 导出金税分类信息
        /// </summary>
        [OperationContract]
        bool 导出加价策略信息(int[] ids, string CategoryCode, string CategoryName, int? BrandId, int? Status, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName);

        /// <summary>
        /// 导出积压件信息查询
        /// </summary>
        [OperationContract]
        bool 导出积压件信息查询(int[] ids, int EnterpriseId, string SparePartCode, string SparePartName, string StorageCompanyCode, string StorageCompanyName, string ProvinceName, string CityName, string CountyName, int? PartsSalesCategoryId, int? BranchId, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName);

        /// <summary>
        /// 金税分类信息导入
        /// </summary>
        [OperationContract]
        bool 金税分类信息导入(string fileName, out int excelImportNum, out List<GoldenTaxClassifyMsgExtend> rightData, out List<GoldenTaxClassifyMsgExtend> errorData, out string errorDataFileName, out string errorMessage);

        /// <summary>
        /// 金税分类信息导入
        /// </summary>
        [OperationContract]
        bool 加价策略信息导入(string fileName, out int excelImportNum, out List<PartsPriceIncreaseRateExtend> rightData, out List<PartsPriceIncreaseRateExtend> errorData, out string errorDataFileName, out string errorMessage);

        /// <summary>
        /// 批量导入修改金税分类编码
        /// </summary>
        [OperationContract]
        bool 批量导入修改金税分类编码(string fileName, out int excelImportNum, out List<SparePartExtend> rightData, out List<SparePartExtend> errorData, out string errorDataFileName, out string errorMessage);

        /// <summary>
        /// 导出销售结算自动任务执行结果
        /// </summary>
        [OperationContract]
        bool ExportAutoTaskExecutResult(int[] ids, int? brandId, int? executionStatus, string companyCode, string companyName, DateTime? executionTimeBegin, DateTime? executionTimeEnd, out string fileName);

        /// <summary>
        /// 导出销售结算自动任务设置
        /// </summary>
        [OperationContract]
        bool ExportSettlementAutomaticTaskSet(int[] ids, int? brandId, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName);

        /// <summary>
        /// 导入服务站配件零售订单
        /// </summary>
        [OperationContract]
        bool 导入服务站配件零售订单(int partsSalesCategoryId, int dealerId, int subDealerId, string fileName, out int excelImportNum, out List<DealerPartsRetailOrderExtend> rightData, out List<DealerPartsRetailOrderExtend> errorData, out string errorDataFileName, out string errorMessage);

        /// <summary>
        /// 导入配件零售订单
        /// </summary>
        [OperationContract]
        bool 导入配件零售订单(int storageCompanyId, int warehouseId, string fileName, out int excelImportNum, out List<PartsRetailOrderExtend> rightData, out List<PartsRetailOrderExtend> errorData, out string errorDataFileName, out string errorMessage);

        /// <summary>
        /// 导入客户企业订单价格等级
        /// </summary>
        [OperationContract]
        bool ImportCustomerOrderPriceGrade(string fileName, out int excelImportNum, out List<CustomerOrderPriceGradeExtend> errorData, out string errorDataFileName, out string errorMessage);

        /// <summary>
        ///合并导出配件入库检验主清单（无价格）
        /// </summary>
        [OperationContract]
        bool ExportPartsInboundCheckBillWithDetailNoPrice(int[] ids, string originalRequirementBillCode, int? userId, int? partsSalesCategoryId, int? storageCompanyId, string partsInboundCheckBillCode, int? warehouseId, int? counterpartCompanyId, string counterpartcompanycode, string counterpartcompanyname, string eRPOrderCode, string partsSalesOrderCode, int? partsSalesOrderInvoiceType, int? inboundType, int? settlementStatus, DateTime? createTimeBegin, DateTime? createTimeEnd, int? partsPurchaseOrderTypeId, string sAPPurchasePlanCode, out string fileName);

        /// <summary>
        ///合并导出配件入库计划主清单ForQuery(无价格)
        /// </summary>
        [OperationContract]
        bool ExportPartsInboundPlanWithDetailForQueryNoPrice(int[] ids, int? partsSalesCategoryId, int? status, int? userId, int storageCompanyId, string partsInboundPlanCode, string sourceCode, int? warehouseId, int? inboundType, DateTime? createTimeBegin, DateTime? createTimeEnd, string counterpartCompanyCode, string counterpartCompanyName, string eRPOrderCode, string partsSalesOrderCode, int? partsPurchaseOrderTypeId, out string fileName);

        /// <summary>
        /// 导出无金税分类信息的配件信息
        /// </summary>
        [OperationContract]
        bool ExportNoGoldenTaxClassify(int[] ids, string code, string name, string ReferenceCode, int? status, DateTime? createDateTimeStart, DateTime? createDateTimeEnd, DateTime? modifyDateTimeStart, DateTime? modifyDateTimeEnd, out string fileName);

        /// <summary>
        /// 导出配件特殊协议价申请单信息
        /// </summary>
        [OperationContract]
        bool ExportSpecialTreatyPriceChange(int[] Ids, string code, int? brandId, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName);

        /// <summary>
        /// 导出配件特殊协议价申请单信息及清单信息
        /// </summary>
        [OperationContract]
        bool MergeExportSpecialTreatyPriceChange(int[] Ids, string code, int? brandId, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName);

        /// <summary>
        /// 导入配件特殊协议价申请单清单信息
        /// </summary>
        [OperationContract]
        bool ImportSpecialPriceChangeList(int partsSalesCategoryId, string fileName, out int excelImportNum, out List<SpecialPriceChangeListExtend> rightData, out List<SpecialPriceChangeListExtend> errorData, out string errorDataFileName, out string errorMessage);

        /// <summary>
        /// 导出历史入库记录查询
        /// </summary>
        [OperationContract]
        bool 导出历史入库记录查询(string[] codes, int? branchId, string partsInboundOrderCode, int? inboundType, int? partsSalesCategoryId, int? partsPurchaseOrderTypeId, string originalRequirementBillCode, int? warehouseId, int? settlementstatus, string partCode, string partName, string companyName, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName);

        /// <summary>
        /// 导出代理库入库记录查询
        /// </summary>
        [OperationContract]
        bool 导出代理库入库记录查询(string[] codes, int? branchId, int? partsSalesCategoryId, int? warehouseId, int? inboundType, string storageCompanyCode, string storageCompanyName, string partCode, string partName, DateTime? startCreateTime, DateTime? endCreateTimeout, out string fileName);

        /// <summary>
        /// 导出保外会员交易接口日志
        /// </summary>
        [OperationContract]
        bool 导出保外会员交易接口日志(int[] ids, string repairCode, string memberPhone, string memberName, int? syncStatus, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName);

        /// <summary>
        /// 导入服务培训认证类型
        /// </summary>
        [OperationContract]
        bool ImportAuthenticationType(string fileName, out int excelImportNum, out List<AuthenticationTypeExtend> rightData, out List<AuthenticationTypeExtend> errorData, out string errorDataFileName, out string errorMessage);

        [OperationContract]
        bool 批量替换产品执行标准代码(string fileName, out int excelImportNum, out List<SparePartExtend> rightData, out List<SparePartExtend> errorData, out string errorDataFileName, out string errorMessage);

        [OperationContract]
        bool ImportPartsExchangeGroup(string fileName, out int excelImportNum, out List<PartsExchangeGroupExtend> rightData, out List<PartsExchangeGroupExtend> errorData, out string errorDataFileName, out string errorMessage);

        [OperationContract]
        bool ImportPartsExchangeGroupInfo(string fileName, out int excelImportNum, out List<PartsExchangeGroupExtend> errorData, out string errorDataFileName, out string errorMessage);

        [OperationContract]
        bool 批量替换配件互换识别号(string fileName, out int excelImportNum, out List<SparePartExtend> rightData, out List<SparePartExtend> errorData, out string errorDataFileName, out string errorMessage);

        [OperationContract]
        bool ExportCAReconciliation(int[] ids, string corporationCode, string corporationName, int? partsSalesCategoryId, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName);

        [OperationContract]
        bool ImportCAReconciliation(string fileName, out int excelImportNum, out List<CAReconciliationExtend> errorData, out string errorDataFileName, out string errorMessage);

        [OperationContract]
        bool ExportEWOrder(int[] ids, int? branchId, int? dealerId, int? partsSalesCategoryId, int? serviceProductLineId, string extendedWarrantyOrderCode, string VIN, int? status, string vehicleLicensePlate, string customerName, string cellNumber, string dealerCode, string dealerName, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName);

        [OperationContract]
        bool ExportMergeEWOrder(int[] ids, int? branchId, int? dealerId, int? partsSalesCategoryId, int? serviceProductLineId, string extendedWarrantyOrderCode, string VIN, int? status, string vehicleLicensePlate, string customerName, string cellNumber, string dealerCode, string dealerName, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName);

        [OperationContract]
        bool ImportDeferredDiscountType(string fileName, out int excelImportNum, out List<DeferredDiscountTypeExtend> rightData, out List<DeferredDiscountTypeExtend> errorData, out string errorDataFileName, out string errorMessage);

        [OperationContract]
        bool ExportExtendedWarrantyProduct(int[] ids, int? partsSalesCategoryId, string extendedWarrantyProductName, int? status, int? serviceProductLineId, DateTime? creatorTimeBegin, DateTime? creatorTimeEnd, out string fileName);

        [OperationContract]
        bool ImportExtendedWarrantyProduct(string fileName, out int excelImportNum, out List<ExtendedWarrantyProductExtend> rightData, out List<ExtendedWarrantyProductExtend> errorData, out string errorDataFileName, out string errorMessage);

        [OperationContract]
        bool ExportPartsSalesPriceChangeDetail(int[] ids, int? partsSalesCategoryId, string code, int? status, string sparePartCode, string sparePartName, DateTime? creatorTimeBegin, DateTime? creatorTimeEnd, out string fileName);

        [OperationContract]
        bool ExportPartsPurchasePricingChange(int[] ids, int? partsSalesCategoryId, string code, int? status, string sparePartCode, string sparePartName, DateTime? creatorTimeBegin, DateTime? creatorTimeEnd, out string fileName);

        [OperationContract]
        bool ExportPartsSalesPriceChange(int[] ids, int? partsSalesCategoryId, string code, int? status, string sparePartCode, string sparePartName, DateTime? creatorTimeBegin, DateTime? creatorTimeEnd, out string fileName);

        [OperationContract]
        bool 批量替换配件厂商(string fileName, out int excelImportNum, out List<SparePartExtend> rightData, out List<SparePartExtend> errorData, out string errorDataFileName, out string errorMessage);

        [OperationContract]
        bool 批量替换配件分类编码(string fileName, out int excelImportNum, out List<SparePartExtend> rightData, out List<SparePartExtend> errorData, out string errorDataFileName, out string errorMessage);

        [OperationContract]
        bool ExportAgencyRetailerOrder(int[] ids, int? branchId, int? shippingCompanyId, string erpSourceOrderCode, string code, int? warehouseId, int? partsSalesCategoryId, string vehiclePartsHandleOrderCode, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName);

        [OperationContract]
        bool ExportAgencyRetailerOrderWithDetails(int[] ids, int? branchId, int? shippingCompanyId, string erpSourceOrderCode, string code, int? warehouseId, int? partsSalesCategoryId, string vehiclePartsHandleOrderCode, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName);

        [OperationContract]
        void ExportPartsOrderConfirm(List<string> connStrings, string branchCode, int? partsSalesCategoryId, int? warehouseId, string partsSalesOrderCode, string spareCode, string spareName, string dealerCode, string dealerName, string marketingDepartmentName, DateTime? submitTimeStart, DateTime? submitTimeEnd, out string fileName);

        [OperationContract]
        bool ImportPartsSpecialTreatyPriceAbandon(string fileName, out int excelImportNum, out List<PartsSpecialTreatyPriceExtend> rightData, out List<PartsSpecialTreatyPriceExtend> errorData, out string errorDataFileName, out string errorMessage);
        [OperationContract]
        bool ImportDealerPartsTransferOrderDetail(string fileName, int noWarrantyBrandId, int warrantyBrandId, int branchId, out int excelImportNum, out List<DealerPartsTransOrderDetailExtend> rightData, out List<DealerPartsTransOrderDetailExtend> errorData, out string errorDataFileName, out string errorMessage);

        [OperationContract]
        bool ExportPartsInboundPlansForQuery(int[] ids, int? partsSalesCategoryId, int? status, int? userId, int storageCompanyId, string partsInboundPlanCode, string sourceCode, int? warehouseId, int? inboundType, DateTime? createTimeBegin, DateTime? createTimeEnd, string counterpartCompanyCode, string counterpartCompanyName, string eRPOrderCode, string partsSalesOrderCode, int? partsPurchaseOrderTypeId, string SAPPurchasePlanCode, out string fileName);

        /// <summary>
        /// 合并导出配件发运单
        /// </summary>
        [OperationContract]
        bool ExportPartsShippingOrder1(int[] ids, int? settlementCompanyId, int? receivingCompanyId, string code, int? type, int? status, int? warehouseId, string warehouseName, DateTime? requestedArrivalDateBegin, DateTime? requestedArrivalDateEnd, DateTime? shippingDateBegin, DateTime? shippingDateEnd, DateTime? createTimeBegin, DateTime? createTimeEnd, DateTime? confirmedReceptionTimeBegin, DateTime? confirmedReceptionTimeEnd, string receivingCompanyCode, string receivingCompanyName, string receivingWarehouseName, int? partsSalesCategoryId, int? logisticCompanyId, string logisticCompanyCode, string logisticCompanyName, string expressCompany, string expressCompanyCode, int? closedLoopStatus, string eRPSOURCEORDERCODE, string originalRequirementBillCode, out string fileName);

        /// <summary>
        /// 导出配件发运单
        /// </summary>
        [OperationContract]
        bool ExportPartsShippingOrderNotWithDetail1(int[] ids, int? settlementCompanyId, int? receivingCompanyId, string code, int? type, int? status, int? warehouseId, string warehouseName, DateTime? requestedArrivalDateBegin, DateTime? requestedArrivalDateEnd, DateTime? shippingDateBegin, DateTime? shippingDateEnd, DateTime? createTimeBegin, DateTime? createTimeEnd, DateTime? confirmedReceptionTimeBegin, DateTime? confirmedReceptionTimeEnd, string receivingCompanyCode, string receivingCompanyName, string receivingWarehouseName, int? partsSalesCategoryId, int? logisticCompanyId, string logisticCompanyCode, string logisticCompanyName, string expressCompany, string expressCompanyCode, int? closedLoopStatus, string eRPSOURCEORDERCODE, string originalRequirementBillCode, out string fileName);

        [OperationContract]
        bool ExportDealerPartsTransferOrder(int[] ids, int? dealerId, int? noWarrantyBrandId, int? warrantyBrandId, string code, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName);

        [OperationContract]
        bool ExportDealerPartsTransferOrderAndDetail(int[] ids, int? dealerId, int? noWarrantyBrandId, int? warrantyBrandId, string code, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName);
        [OperationContract]
        bool ImportIvecoPriceChangeAppDetail(int partsSalesCategoryId, string fileName, out int excelImportNum, out List<IvecoPriceChangeAppDetailExtend> rightData, out List<IvecoPriceChangeAppDetailExtend> errorData, out string errorDataFileName, out string errorMessage);
        [OperationContract]
        bool ExportPackingTask(int[] ids, string code, string partsInboundPlanCode, string partsInboundCheckBillCode, string sourceCode, int? status, DateTime? expectedPlaceDateBegin, DateTime? expectedPlaceDateEnd, int? warehouseId, DateTime? createTimeBegin, DateTime? createTimeEnd, string sparePartCode, string sparePartName, int? inboundType, string modifierName,bool? hasDifference, out string fileName);

        /// <summary>
        /// 合并导出配件上架单
        /// </summary>
        [OperationContract]
        bool ExportPartsShelvesTask(int[] ids, string code, string sourceBillCode, string partsInboundPlanCode, string partsInboundCheckBillCode, int? inboundType, string status, int? warehouseId, string sparePartCode, string sparePartName, bool? noWarehouseArea, string warehouseAreaCode, string warehouseArea,bool? hasDifference, DateTime? begainDate, DateTime? endDate, DateTime? shelvesFinishTimeBegin, DateTime? shelvesFinishTimeEnd, out string fileName);

        //配件采购计划合并导出
        [OperationContract]
        bool ExportPartsPurchasePlanWithDetail(int[] ids, string code, int? PartsSalesCategoryId, int? status, int? PartsPlanTypeId, string WarehouseName, DateTime? createTimeBegin, DateTime? createTimeEnd, DateTime? ApproveTimeBegin, DateTime? ApproveTimeEnd, DateTime? CloseTimeBegin, DateTime? CloseTimeEnd, string sparePartCode, out string filename);

        //配件采购计划清单导入
        [OperationContract]
        bool ImportPartsPurchasePlanDetail(string fileName, int PurchasePlanId, int branchId, int partsSalesCategoryId, string partsPurchaseOrderTypeName, out int excelImportNum, out List<PartsPurchasePlanDetailExtend> rightData, out List<PartsPurchasePlanDetailExtend> errorData, out string errorDataFileName, out string errorMessage);
        //配件包装属性设置 导出
        [OperationContract]
        bool ExportPartsPackingPropertyApp(int[] ids, string code, string spareCode, string spareName, int? status, int? mainPackingType, DateTime? bCreateTime, DateTime? eCreateTime, out string fileName);
        //配件装箱单  合并导出
        [OperationContract]
        bool ExportBoxUpTask(int[] ids, string code, int? warehouseId, int? partsSalesCategoryId, int? status, string sourceCode, string partsOutboundPlanCode, string counterpartCompanyCode, string counterpartCompanyName, DateTime? createTimeBegin, DateTime? createTimeEnd, DateTime? modifyTimeBegin, DateTime? modifyTimeEnd, string sparePartCode, string sparePartName, out string fileName);

        //配件采购退货管理-供应商 合并导出
        [OperationContract]
        bool ExportPartsPurReturnOrderForSupplierWithDetail(bool isSupplierLogin, int[] ids, string partsSupplierCode, int? BranchId, int? partsSalesCategoryId, int? warehouseId, string Code, int? returnReason, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, out string filename);

        //拣货单合并导出
        [OperationContract]
        bool ExportPickingTask(int[] ids, string code, int? warehouseId, int? partsSalesCategoryId, string status, string sourceCode, string partsOutboundPlanCode, string orderTypeName, string counterpartCompanyName, DateTime? bCreateTime, DateTime? eCreateTime, DateTime? bModifyTime, DateTime? eModifyTime, string sparePartCode, string sparePartName, int? orderTypeId, out string filename);
        //企业配件成本  合并导出
        [OperationContract]
        bool ExportEnterprisePartsCost(int[] ids, int? partsSalesCategoryId, string sparePartCode, string sparePartName, string quantity, string costPrice, string costAmount, int? partABC, DateTime? createTimeBegin, DateTime? createTimeEnd, DateTime? modifyTimeBegin, DateTime? modifyTimeEnd, out string filename);

        [OperationContract]
        bool ExportPickingTaskAll(int[] ids, string code, int? warehouseId, int? partsSalesCategoryId, int? status, string sourceCode, string partsOutboundPlanCode, string orderTypeName, string counterpartCompanyName, DateTime? bCreateTime, DateTime? eCreateTime, DateTime? bModifyTime, DateTime? eModifyTime, out string filename);
        //导出账期设置
        [OperationContract]
        bool ExportAccountPeriod(int[] ids, string year, string month, int? status, DateTime? bCreateTime, DateTime? eCreateTime, out string fileName);
        /// <summary>
        /// 导入客户转账单
        /// </summary>
        [OperationContract]
        bool ImportCustomerTransferBill(string fileName, out int excelImportNum, out List<CustomerTransferBillExtend> errorData, out string errorDataFileName, out string errorMessage);

        //导入人员与供应商关系
        [OperationContract]
        bool ImportPersonnelSupplierRelation(string fileName, out int excelImportNum, out List<PersonnelSupplierRelationExtend> errorData, out string errorDataFileName, out string errorMessage);

        ////导出人员与供应商关系
        [OperationContract]
        bool ExportPersonnelSupplierRelation(int[] ids, int? partsSalesCategoryId, string personName, string supplierName, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, DateTime? modifyTimeBegin, DateTime? modifyTimeEnd, out string fileName);
        // 导入销售退货单
        [OperationContract]
        bool ImportPartsSalesReturnBillForReport(string fileName, out int excelImportNum, out List<PartsSalesReturnBillExtend> errorData, out string errorDataFileName, out string errorMessage);

        ////导出新产品明细
        [OperationContract]
        bool ExportPartsPurchasePricingForQuery(int[] ids, string code, string name, DateTime? bCreateTime, DateTime? eCreateTime, DateTime? bPartCreateTime, DateTime? ePartCreateTime, bool? isprice, bool? isExactExport, string referenceCode, out string fileName);
        ////导出配件库位分配
        [OperationContract]
        bool ExportPartsLocationAssignment(int id, int areaKind, out string fileName);

        ////导出库存变更日志
        [OperationContract]
        bool ExportPartsStockHistory(int[] ids, int? warehouseId, string sparePartCode, string sparePartName, string warehouseAreaCode, int? warehouseAreaCategory, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName);
        /// <summary>
        /// 导出保底库存
        /// </summary>
        [OperationContract]
        bool ExportBottomStock(int[] ids, int? partsSalesCategoryId, int? companyType, string companyCode, string companyName, string sparePartCode, string sparePartName, DateTime? createTimeBegin, DateTime? createTimeEnd, DateTime? modifyTimeBegin, DateTime? modifyTimeEnd, int? status, out string fileName);
        /// <summary>
        /// 导入保底库存
        /// </summary>
        [OperationContract]
        bool ImportBottomStock(string fileName, out int excelImportNum, out List<BottomStockExtend> errorData, out string errorDataFileName, out string errorMessage);

        /// <summary>
        /// 供应商发运管理-供应商 合并导出
        /// </summary>
        [OperationContract]
        bool ExportSupplierShippingOrderForSupplier(int[] ids, /*int? branchId,*/ string code, /*int? partsSalesCategoryId,*/ int? receivingWarehouseId, string partsPurchaseOrderCode, /*string ERPSourceOrderCode,*/int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName);

        /// <summary>
        /// 导入客户直供配件清单
        /// </summary>
        [OperationContract]
        bool ImportCustomerDirectSpareList(string fileName, out int excelImportNum, out List<CustomerDirectSpareListExtend> errorData, out string errorDataFileName, out string errorMessage);

        /// <summary>
        /// 批量导入配件ABC类型
        /// </summary>
        [OperationContract]
        bool 批量导入配件ABC类型(string fileName, out int excelImportNum, out List<PartsBranchExtend> rightData, out List<PartsBranchExtend> errorData, out string errorDataFileName, out string errorMessage);

        /// <summary>
        /// 批量导入客户与供应商起订金额设置
        /// </summary>
        [OperationContract]
        bool ImportCustomerSupplyInitialFeeSet(string fileName, out int excelImportNum, out List<CustomerSupplyInitialFeeSetExtend> errorData, out string errorDataFileName, out string errorMessage);
        /// <summary>
        /// 导入借用单清单
        /// </summary>
        [OperationContract]
        bool ImportBorrowBillDetail(string fileName, int excelImportNum, int? warehouseId, out List<BorrowBillDetailExtend> rightData, out List<BorrowBillDetailExtend> errorData, out string errorDataFileName, out string errorMessage);
        /// <summary>
        /// 导出CSBOM配件信息
        /// </summary>
        [OperationContract]
        bool ExportCsbomParts(int[] ids, string code, string name, string spmName, string englishName, string spmEnglishName, DateTime? bCreateTime, DateTime? eCreateTime, out string fileName);

        /// <summary>
        /// 导出SAP采购价信息
        /// </summary>
        [OperationContract]
        bool ExportFactoryPurchacePrice(int[] ids, string supplierCode, string supplierName, string sparePartCode, string sparePartName, string hyCode, bool? isGenerate, DateTime? createTimeBegin, DateTime? createTimeEnd, bool? isExactExport, out string fileName);

        // 合并导出配件出库计划主清单ForQuery 仓库
        [OperationContract]
        bool ExportPartsOutboundPlanForWarehouse(int[] ids, int? userId, int storageCompanyId, string partsOutboundPlanCode, string status, string sourceCode, int? warehouseId, int? outboundType, DateTime? createTimeBegin, DateTime? createTimeEnd, string counterpartCompanyName, string partsSalesOrderTypeName, string SAPPurchasePlanCode, string ZPNUMBER, string eRPSourceOrderCode, string sparePartCode, string sparePartName, out string fileName);
        /// <summary>
        /// 合并导出配件出库主清单
        /// </summary>
        [OperationContract]
        bool ExportPartsOutboundBillWithDetailForWarehouse(int[] ids, string eRPSourceOrderCode, int? userId, int? partsSalesCategoryId, int storageCompanyId, string partsOutBoundBillCode, int? warehouseId, string counterpartcompanycode, string counterpartcompanyname, int? outBoundType, int? settlementStatus, string partsSalesOrderTypeName, string partsOutboundPlanCode, string outboundPackPlanCode, string contractCode, string provinceName, DateTime? createTimeBegin, DateTime? createTimeEnd, string SAPPurchasePlanCode, string zpNumber, string cpPartsPurchaseOrderCode, string cpPartsInboundCheckCode, string sparePartCode, string sparePartName, out string fileName);
        /// <summary>
        /// 合并导出配件入库检验主清单
        /// </summary>
        [OperationContract]
        bool ExportPartsInboundCheckBillWithDetailForWarehouse(int[] ids, string originalRequirementBillCode, int? userId, int? storageCompanyId, string partsInboundCheckBillCode, int? warehouseId, int? counterpartCompanyId, string counterpartcompanyname, int? inboundType, int? settlementStatus, DateTime? createTimeBegin, DateTime? createTimeEnd, string sparePartCode, string sparePartName, string partsInboundPlanCode, string batchNumber,bool? hasDifference, out string fileName);
        /// <summary>
        /// 合并导出配件入库计划主清单ForQuery
        /// </summary>
        [OperationContract]
        bool ExportPartsInboundPlanWithDetailForWarehouse(int[] ids, int? partsSalesCategoryId, int? status, int? userId, int storageCompanyId, string partsInboundPlanCode, string sourceCode, int? warehouseId, int? inboundType, DateTime? createTimeBegin, DateTime? createTimeEnd, string counterpartCompanyCode, string counterpartCompanyName, string eRPOrderCode, string partsSalesOrderCode, int? partsPurchaseOrderTypeId, string SAPPurchasePlanCode, string sparepartCode, string sparepartName, string originalRequirementBillCode, DateTime? planDeliveryTimeBegin, DateTime? planDeliveryTimeEnd,bool? hasDifference, out string fileName);
        /// <summary>
        /// 合并导出服务站配件零售退货单及详情
        /// </summary>
        [OperationContract]
        bool ExportDealerPartsSalesReturnBillWithDetail(int[] ids, string customer, string code, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName);
        /// <summary>
        /// 导入移库单清单
        /// </summary>
        [OperationContract]
        bool ImpPartsShiftOrderDetail(string fileName, int warehouseId, int OriginalWarehouseAreaCategory, int DestWarehouseAreaCategory, out int excelImportNum, out List<ImpPartsShiftOrderDetail> rightData, out List<ImpPartsShiftOrderDetail> errorData, out string errorDataFileName, out string errorMessage);

        [OperationContract]
        bool ImportAssemblyPartRequisitionLink(string fileName, out int excelImportNum, out List<AssemblyPartRequisitionLinkExtend> errorData, out string errorDataFileName, out string errorMessage);

        /// <summary>
        /// 导入待结算出入库清单
        /// </summary>
        [OperationContract]
        bool ImpOutboundAndInboundBill(string fileName, int partsSalesCategoryId, int supplierId, out int excelImportNum, out List<ImpOutboundAndInboundBill> rightData, out List<ImpOutboundAndInboundBill> errorData, out string errorDataFileName, out string errorMessage);
        /// <summary>
        /// 批量替换是否供应商投放
        /// </summary>
        [OperationContract]
        bool 批量替换是否供应商投放(string fileName, out int excelImportNum, out List<SparePartExtend> rightData, out List<SparePartExtend> errorData, out string errorDataFileName, out string errorMessage);

        [OperationContract]
        bool ExportOverstockTransferOrder(int[] ids, string code, int? status, string transferInCorpName, string transferInWarehouseCode, string transferOutCorpName, string transferOutWarehouseName, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName);
        /// <summary>
        /// 批量导出配件采购订单
        /// </summary>
        [OperationContract]
        bool ExportPartsPurchaseOrderWithDetailForShipping(bool isSupplierLogin, int[] partsPurchaseOrderIds, string code, string businessCode, string warehouseName, string partsSupplierCode, string partsSupplierName, int? partsPurchaseOrderTypeId, bool? ifDirectProvision, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, int? branchId, int? supplierId, int? warehouseId, int? InStatus, DateTime? RequestedDeliveryTimeBegin, DateTime? RequestedDeliveryTimeEnd, DateTime? approveTimeBegin, DateTime? approveTimeEnd, string ERPSourceOrderCode, int? ioStatus, out string filename);
        /// <summary>
        /// 中心库强制储备单清单
        /// </summary>
        [OperationContract]
        bool ImpForceReserveBillDetail(string fileName, out int excelImportNum, out List<ForceReserveBillDetailExtend> rightData, out List<ForceReserveBillDetailExtend> errorData, out string errorDataFileName, out string errorMessage);
        //强制储备单合并导出
        [OperationContract]
        bool ExportForceReserveBill(int[] ids, int? status, string subVersionCode, string colVersionCode, string reserveTypeSubItem, string companyCode, string companyName, DateTime? bCreateTime, DateTime? eCreateTime, DateTime? bApproveTime, DateTime? eApproveTime, int? orderTypeId, int companyType, string reserveType, out string fileName);

        /// <summary>
        /// 服务商强制储备单清单
        /// </summary>
        [OperationContract]
        bool ImpForceReserveBillDealer(string fileName, string reserveType, out int excelImportNum, out List<ForceReserveBillDetailExtend> rightData, out List<ForceReserveBillDetailExtend> errorData, out string errorDataFileName, out string errorMessage);
        //配件保底库存明细子集版本号导出
        [OperationContract]
        bool ExportBottomStockSubVersion(int[] ids, int? status, string subVersionCode, string colVersionCode, string sparePartCode, string sparePartName, string companyCode, string companyName, DateTime? bStartTime, DateTime? eStartTime, int? companyType, out string fileName);
        //配件保底库存合集版本号导出
        [OperationContract]
        bool ExportBottomStockColVersion(int[] ids, int? status, string colVersionCode, string sparePartCode, string sparePartName, string companyCode, string companyName, DateTime? bStartTime, DateTime? eStartTime, int? companyType, out string fileName);
        /// <summary>
        /// 导入保底库存作废
        /// </summary>
        [OperationContract]
        bool ImportBottomStockAbandon(string fileName, out int excelImportNum, out List<BottomStockExtend> errorData, out string errorDataFileName, out string errorMessage);
        /// <summary>
        /// 导入服务商业态
        /// </summary>
        [OperationContract]
        bool ImportDealerFormat(string fileName, out int excelImportNum, out List<DealerFormatExtend> errorData, out string errorDataFileName, out string errorMessage);
        //服务商业态导出
        [OperationContract]
        bool ExportDealerFormat(int[] ids, int? status, string dealerCode, string dealerName, string format, string quarter, DateTime? bCreateTime, DateTime? eCreateTime, out string fileName);

        //SIH授信(服务商)信用申请单导出
        [OperationContract]
        bool ExportSIHCreditInfo(int[] ids, string code, string companyCode, string companyName, string dealerCode, string dealerName, int? status, DateTime? bCreateTime, DateTime? eCreateTime, out string fileName);
        /// <summary>
        /// 导入SIH授信(服务商)信用申请单
        /// </summary>
        [OperationContract]
        bool ImportSIHCreditInfo(string fileName, out int excelImportNum, out List<SIHCreditInfoExtend> errorData, out string errorDataFileName, out string errorMessage);

        [OperationContract]
        bool ExportSupplierTraceCode(int[] ids, string code, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, string partsSupplierCode, string partsSupplierName, string sparePartCode, string partsPurchaseOrderCode, string oldTraceCode, string newTraceCode, string shippingCode, out string fileName);
        [OperationContract]
        bool Export供应商确认(int[] ids, string code, string partsPurchaseOrderCode, string partsSupplierName, string partsSupplierCode, int? arriveMethod, int? comfirmStatus, bool? ifDirectProvision, DateTime? createTimeBegin, DateTime? createTimeEnd, int? modifyStatus, out string fileName);
        /// <summary>
        /// 批量替换审核上限
        /// </summary>
        [OperationContract]
        bool 批量替换库存低限(string fileName, out int excelImportNum, out List<PartsBranchExtend> rightData, out List<PartsBranchExtend> errorData, out string errorDataFileName, out string errorMessage);
        /// <summary>
        /// 批导入临时采购计划清单
        /// </summary>
        [OperationContract]
        bool ImportTemPurchasePlanOrderDetail(string fileName, out int excelImportNum, out List<TemPurchasePlanOrderDetailExtend> rightData, out List<TemPurchasePlanOrderDetailExtend> errorData, out string errorDataFileName, out string errorMessage);
        //临时采购计划合并导出
        [OperationContract]
        bool ExportTemPurchasePlanOrderWithDetail(int[] ids, string code, int? status, string receCompanyCode, string receCompanyName, DateTime? createTimeBegin, DateTime? createTimeEnd, string sparePartCode, out string filename);
        /// <summary>
        /// 临时采购订单确认导入供应商
        /// </summary>
        [OperationContract]
        bool Import临时采购订单确认导入供应商(string fileName, out int excelImportNum, out List<TemPurchasePlanOrderDetailExtend> rightData, out List<TemPurchasePlanOrderDetailExtend> errorData, out string errorDataFileName, out string errorMessage);
        [OperationContract]
        bool Export临时采购计划(int[] ids, int companyId, string code, int? status, int? customerType, string receCompanyCode, string receCompanyName, DateTime? createTimeBegin, DateTime? createTimeEnd, string sparePartCode, out string filename);
        [OperationContract]
        bool Export临时采购订单(int[] ids, int personelId, string temPurchasePlanOrderCode, string suplierName, string code, int? approveStatus, int? receiveStatus, int? status, int? customerType, string receCompanyCode, string receCompanyName, DateTime? createTimeBegin, DateTime? createTimeEnd, string sparePartCode, bool? isSaleCode, bool? isPurchaseOrder, out string filename);
        [OperationContract]
        bool Export临时采购订单供应商(int[] ids, int suplierId, string temPurchasePlanOrderCode, string code, int? status, int? orderType, DateTime? createTimeBegin, DateTime? createTimeEnd, DateTime? bConfirmeTime, DateTime? eConfirmeTime, out string filename);
        [OperationContract]
        bool Export临时采购发运单(int[] ids, int? suplierId, string partsSupplierCode, string temPurchaseOrderCode, string partsSupplierName, string code, int? status, string receivingWarehouseName, DateTime? createTimeBegin, DateTime? createTimeEnd, out string filename);
        [OperationContract]
        bool Export临时采购发运单确认(int[] ids, int? orderCompanyId, string originalRequirementBillCode, string temPurchaseOrderCode, string code, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, out string filename);
        /// <summary>
        /// 合并导出配件差异回退单主清单
        /// </summary>
        [OperationContract]
        bool ExportPartsDifferenceBackBillWithDetail(int[] ids, string code, int? status, int? type, string shelvesCode, string packingCode, string inPlanCode, DateTime? createTimeBegin, DateTime? createTimeEnd, DateTime? bSubmitTime, DateTime? eSubmitTime, out string fileName);
        /// <summary>
        ///导入日均销量计算参数配置
        /// </summary>
        [OperationContract]
        bool ImportDailySalesWeight(string fileName, out int excelImportNum, out List<DailySalesWeightExtend> rightData, out List<DailySalesWeightExtend> errorData, out string errorDataFileName, out string errorMessage);
       /// <summary>
        /// 导出日均销量计算参数配置
       /// </summary>      
        [OperationContract]
        bool ExportDailySalesWeight(int[] ids, int? warehouseId, int? times, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName);
        /// <summary>
        /// 导入智能订货日历
        /// </summary>       
        /// <returns></returns>
        [OperationContract]
        bool Import智能订货日历(string fileName, out int excelImportNum,  out List<SmartOrderCalendarExtend> errorData, out string errorDataFileName, out string errorMessage);
        /// <summary>
        /// 导出智能订货日历
        /// </summary>       
        /// <returns></returns>
        [OperationContract]
        bool Export智能订货日历(int[] ids, int? warehouseId,int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName);

        /// <summary>
        /// 导出临时采购计划确认时的清单
        /// </summary>
        [OperationContract]
        bool 导出临时采购计划确认时的清单(int id, out string filename);
         [OperationContract]
        bool 批量修改安全天数最大值(string fileName, out int excelImportNum, out List<SparePartExtend> rightData, out List<SparePartExtend> errorData, out string errorDataFileName, out string errorMessage);
         [OperationContract]
         bool 批量修改库房天数(string fileName, out int excelImportNum, out List<SparePartExtend> rightData, out List<SparePartExtend> errorData, out string errorDataFileName, out string errorMessage);
         [OperationContract]
         bool 批量修改临时天数(string fileName, out int excelImportNum, out List<SparePartExtend> rightData, out List<SparePartExtend> errorData, out string errorDataFileName, out string errorMessage);
         [OperationContract]
         bool 导出储备系数(int[] ids, int? warehouseId, int? status, int? aBCStrategyId, int? reserveCoefficient, DateTime? bCreateTime, DateTime? eCreateTime, DateTime? bModifyTime, DateTime? eModifyTime, out string filename);
         /// <summary>
         /// 批量导入配件公司推荐计划表
         /// </summary>
         [OperationContract]
         bool ImportSIHRecommendPlan(string fileName, out int excelImportNum, int branchId, out List<SIHRecommendPlanExtend> errorData, out string errorDataFileName, out string errorMessage);
        //导出出库计划单主单
         [OperationContract]
        bool ExportPartsOutboundPlanForQuery1(int[] ids, int? userId, int storageCompanyId, string partsOutboundPlanCode, string status, string sourceCode, int? warehouseId, int? outboundType, DateTime? createTimeBegin, DateTime? createTimeEnd, string counterpartCompanyName, string partsSalesOrderTypeName, string SAPPurchasePlanCode, string ZPNUMBER, string eRPSourceOrderCode, string sparePartCode, string sparePartName, out string fileName);

         /// <summary>
         /// 批量导入储备系数清单
         /// </summary>
         [OperationContract]
         bool Import储备系数清单(string fileName, out int excelImportNum, out List<ReserveFactorOrderDetailExtend> rightData, out List<ReserveFactorOrderDetailExtend> errorData, out string errorDataFileName, out string errorMessage);
         [OperationContract]
         bool ExportResponsibleMembersWithDetails(int? id, int? resTem, int? status, out string fileName);
         [OperationContract]
         bool Import责任关系绑定(string fileName, out int excelImportNum, out List<ResRelationshipExtend> rightData, out List<ResRelationshipExtend> errorData, out string errorDataFileName, out string errorMessage);

         [OperationContract]
         bool Export责任关系绑定(int[] ids, string resType, int? resTem, int? status, DateTime? bCreateTime, DateTime? eCreateTime, out string fileName);
         //导出跨区域销售单主单及清单
         [OperationContract]
         bool ExportCrossSalesOrderWithDetails(int? id, string sparePartCode, string sparePartName, string applyCompnayCode, string applyCompnayName, string subCompanyCode, string code, string subCompanyName, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, DateTime? submitTimeBegin, DateTime? submitTimeEnd, out string fileName);


         /// <summary>
         /// 批量导入中心库订货仓库
         /// </summary>
         [OperationContract]
         bool Import中心库订货仓库(string fileName, out int excelImportNum,  out List<AgencyExtend> errorData, out string errorDataFileName, out string errorMessage);
    }



    /// <summary>
    /// 不支持 ASP.NET 兼容性
    /// 将 AspNetCompatibilityRequirements 属性添加到服务类型且同时将 RequirementsMode 设置为“Allowed”或“Required”。
    /// </summary>
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    public partial class ExcelService : IExcelService
    {
        private const string AttachmentFolder = "";

        public static readonly string ConnectionString;
        public static readonly string SecurityConnectionString;

        static ExcelService()
        {
            var entityConnStr = ConfigurationManager.ConnectionStrings["DcsEntities"].ConnectionString;
            var securityConnStr = ConfigurationManager.ConnectionStrings["SecurityEntities"].ConnectionString;
            if (!string.IsNullOrEmpty(entityConnStr))
            {
                var entityConnection = new EntityConnectionStringBuilder(entityConnStr);
                ConnectionString = entityConnection.ProviderConnectionString;
            }

            if (!string.IsNullOrEmpty(securityConnStr))
            {
                var entityConnection = new EntityConnectionStringBuilder(securityConnStr);
                SecurityConnectionString = entityConnection.ProviderConnectionString;
            }
        }

        public string GetErrorFilePath(string fileName)
        {
            return Path.Combine(Path.GetDirectoryName(fileName) ?? "", string.Format("{0}_ErrorData{1}", Path.GetFileNameWithoutExtension(fileName), Path.GetExtension(fileName)));
        }

        public string GetExportFilePath(string fileName)
        {
            var enterpriseCode = Utils.GetCurrentUserInfo().EnterpriseCode;
            return Path.Combine(GlobalVar.DOWNLOAD_EXPORTFILE_DIR, enterpriseCode, string.Format("{0}_{1:yyMMdd_HHmmss_fff}{2}", Path.GetFileNameWithoutExtension(fileName), DateTime.Now, Path.GetExtension(fileName)));
        }

        private string GetImportFilePath(string fileName)
        {
            return fileName;
        }

        private void UpdateScheduleExportState(string jobName, string filePath)
        {
            if (string.IsNullOrEmpty(jobName))
                throw new ArgumentNullException("jobName");
            if (string.IsNullOrEmpty(filePath))
                throw new ArgumentNullException("filePath");

            var db = DbHelper.GetDbHelp(ConnectionString);
            using (var conn = db.CreateDbConnection())
            {
                conn.Open();
                var sql = string.Format(@"UPDATE ScheduleExportState SET FilePath = {0}FilePath, ModifierId = {0}ModiferId, ModifyTime = {0}Now WHERE JobName = {0}JobName", db.ParamMark);
                var cmd = db.CreateDbCommand(sql, conn, null);
                cmd.Parameters.Add(db.CreateDbParameter("FilePath", filePath));
                cmd.Parameters.Add(db.CreateDbParameter("ModiferId", Utils.GetCurrentUserInfo().Id));
                cmd.Parameters.Add(db.CreateDbParameter("Now", DateTime.Now));
                cmd.Parameters.Add(db.CreateDbParameter("JobName", jobName));
                cmd.ExecuteNonQuery();
            }
        }
    }
}