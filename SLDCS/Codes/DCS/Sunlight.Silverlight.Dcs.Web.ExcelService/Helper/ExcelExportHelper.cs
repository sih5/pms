﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Globalization;
#if !SqlServer
using Devart.Data.Oracle;
#endif

namespace Sunlight.Silverlight.Dcs.Web.ExcelOperator {
    public class ExcelExportHelper {
        private readonly Dictionary<string, Dictionary<int, string>> eNumList;
        private readonly Dictionary<string, string> columnHeader;
        private readonly Dictionary<string, Dictionary<Boolean, string>> eNumBool;
        private readonly string connectionStr;
        private readonly string connectProvider;

        public ExcelExportHelper() {
            this.eNumList = new Dictionary<string, Dictionary<int, string>>();
            this.columnHeader = new Dictionary<string, string>();
            this.eNumBool = new Dictionary<string, Dictionary<Boolean, string>>();
        }

        public ExcelExportHelper(string connectionStr, string connectProvider)
            : this() {
            this.connectionStr = connectionStr;
            this.connectProvider = connectProvider;
        }

        public void AddEnum(string fieldName, params KeyValuePair<int, string>[] valuePair) {
            if(string.IsNullOrEmpty(fieldName) || valuePair.Length <= 0)
                return;
            if(this.eNumList.ContainsKey(fieldName)) {
                foreach(var value in valuePair) {
                    var sValue = string.IsNullOrEmpty(value.Value) ? value.Key.ToString(CultureInfo.InvariantCulture) : value.Value;
                    this.eNumList[fieldName].Add(value.Key, sValue);
                }
            } else {
                var tmpDict = new Dictionary<int, string>();
                foreach(var value in valuePair) {
                    var sValue = string.IsNullOrEmpty(value.Value) ? value.Key.ToString(CultureInfo.InvariantCulture) : value.Value;
                    tmpDict.Add(value.Key, sValue);
                }
                this.eNumList.Add(fieldName.ToLower(), tmpDict);
            }
        }

        /// <summary>
        /// 从数据库调入枚举值列表
        /// </summary>
        /// <param name="keyValueTableName">枚举值表名</param>
        /// <param name="fieldPair">枚举值列表（第一个参数为目标表中被替换的字段名，第二个参数为枚举值表名里枚举名称限制值）</param>
        /// <returns></returns>
        public Boolean LoadEnumFromDb(string keyValueTableName = "KeyValueItem", params KeyValuePair<string, string>[] fieldPair) {
            if(fieldPair.Length <= 0 || string.IsNullOrEmpty(connectionStr.Trim()))
                return false;
            if(string.IsNullOrEmpty(connectProvider) || string.IsNullOrEmpty(connectionStr))
                throw new Exception("未设置字典项来源数据库");
            if(string.IsNullOrWhiteSpace(keyValueTableName))
                keyValueTableName = "KeyValueItem";
            DbConnection conn = null;
            try {
                DbCommand cmd = null;
                var sql = "";
#if SqlServer
                var connection = new SqlConnection(connectionStr);
                cmd = new SqlCommand("", connection);
                sql = "SELECT [Key],Value FROM {0} WHERE Name='{1}'";
                conn = connection;
#else
                var connection = new OracleConnection(connectionStr);
                cmd = new OracleCommand("", connection);
                sql = "SELECT Key,Value FROM {0} WHERE Name='{1}'";
                conn = connection;
#endif

                conn.Open();
                foreach(var value in fieldPair) {
                    if(string.IsNullOrEmpty(value.Key) || string.IsNullOrEmpty(value.Value))
                        continue;
                    cmd.CommandText = string.Format(sql, keyValueTableName, value.Value);
                    var reader = cmd.ExecuteReader();
                    var tmpValue = new Dictionary<int, string>();
                    while(reader.Read()) {
                        tmpValue.Add(reader.GetInt32(0), reader.GetString(1).ToLower());
                    }
                    this.eNumList.Add(value.Key.ToLower(), tmpValue);
                    reader.Close();
                }
            } catch(Exception) {
                if(conn != null)
                    conn.Close();
                throw;
            }
            return true;
        }

        public void AddBooleanEnum(string fieldName, string trueValue, string falseValue) {
            if(string.IsNullOrEmpty(fieldName))
                return;
            if(string.IsNullOrEmpty(trueValue))
                trueValue = "真";
            if(string.IsNullOrEmpty(falseValue))
                falseValue = "假";
            if(this.eNumBool.ContainsKey(fieldName.ToLower())) {
                this.eNumBool[fieldName.ToLower()][true] = trueValue;
                this.eNumBool[fieldName.ToLower()][false] = falseValue;
            } else {
                var tmpDict = new Dictionary<Boolean, string> {
                    {
                        true, trueValue
                    }, {
                        false, falseValue
                    }
                };
                this.eNumBool.Add(fieldName.ToLower(), tmpDict);
            }
        }

        public void AddColumnHeader(params KeyValuePair<string, string>[] columnTitle) {
            foreach(var valuePair in columnTitle) {
                if(string.IsNullOrEmpty(valuePair.Key))
                    continue;
                var sValue = valuePair.Value;
                if(string.IsNullOrEmpty(valuePair.Value))
                    sValue = valuePair.Key;
                if(this.columnHeader.ContainsKey(valuePair.Key.ToLower()))
                    this.columnHeader[valuePair.Key.ToLower()] = sValue;
                else
                    this.columnHeader.Add(valuePair.Key.ToLower(), sValue);
            }
        }

        public string GetEnumValue(string fieldName, int enumKey) {
            if(!this.eNumList.ContainsKey(fieldName.ToLower()))
                return enumKey.ToString(CultureInfo.InvariantCulture);
            var tmpDict = this.eNumList[fieldName.ToLower()];
            if(!tmpDict.ContainsKey(enumKey))
                return enumKey.ToString(CultureInfo.InvariantCulture);
            return tmpDict[enumKey];
        }

        public string GetColumnHeader(string fieldName) {
            if(!this.columnHeader.ContainsKey(fieldName.ToLower()))
                return fieldName;
            return this.columnHeader[fieldName.ToLower()];
        }

        public string GetBoolEnum(string fieldName, Boolean value) {
            if(!this.eNumBool.ContainsKey(fieldName))
                return value ? "真" : "假";
            return this.eNumBool[fieldName][value];
        }
    }
}