﻿using System;
using System.Collections.Generic;
using System.IO;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService.Helper {
    public class ExcelNoDataException : Exception {
        public ExcelNoDataException()
            : base("Excel文件无数据") {
        }
    }

    public delegate string ParseValueHandle(ExcelColumns source, string valueString);

    public class ExcelColumns {
        private readonly string sName;

        /// <summary>
        /// Excel列名
        /// </summary>
        public string Name {
            get {
                return this.sName;
            }
        }

        /// <summary>
        /// 绑定字段名
        /// </summary>
        public string FieldName {
            get;
            set;
        }

        /// <summary>
        /// Excel列索引值
        /// </summary>
        public int Index {
            get;
            set;
        }

        public Type FieldType {
            get;
            set;
        }

        public ExcelColumns(string columnName, string fieldName, int index, Type fieldType) {
            if(string.IsNullOrEmpty(columnName))
                throw new ArgumentNullException("columnName");
            if(index < 0)
                throw new ArgumentOutOfRangeException("index");
            if(string.IsNullOrEmpty(fieldName))
                throw new ArgumentNullException("fieldName");
            this.sName = columnName;
            this.Index = index;
            this.FieldName = fieldName;
            this.FieldType = fieldType;
        }
    }

    public class ExcelImport : IDisposable {
        private readonly IWorkbook wb;
        private readonly ISheet sheet;
        public event ParseValueHandle OnParseValue;

        public ISheet Sheet {
            get {
                return this.sheet;
            }
        }

        /// <summary>
        /// 字段列表
        /// </summary>
        public List<ExcelColumns> Columns {
            get;
            set;
        }

        public ExcelImportHelper ImportHelper {
            get;
            set;
        }

        private int IndexOfColumnName(string columnName) {
            for(var i = 0; i < this.Columns.Count; i++) {
                if(columnName.Equals(this.Columns[i].Name, StringComparison.OrdinalIgnoreCase))
                    return i;
            }
            return -1;
        }

        public int IndexOfFieldName(string fieldName) {
            for(var i = 0; i <= this.Columns.Count - 1; i++) {
                if(fieldName.Equals(this.Columns[i].FieldName, StringComparison.OrdinalIgnoreCase))
                    return i;
            }
            return -1;
        }

        public List<Dictionary<string, string>> LoadExcel() {
            if(this.Columns.Count == 0)
                throw new Exception("请先指定Excel列对应的字段。");
            var data = new List<Dictionary<string, string>>();
            for(var i = 0; i <= this.sheet.LastRowNum; i++) {
                var row = this.sheet.GetRow(i);
                var rowData = new Dictionary<string, string>();
                foreach(var column in this.Columns) {
                    if(column.Index == -1)
                        continue;
                    var cell = row.GetCell(column.Index);
                    if(cell == null)
                        break;
                    var sValue = "";

                    if(this.OnParseValue != null) {
                        switch(cell.CellType) {
                            case CellType.NUMERIC:
                                sValue = DateUtil.IsCellDateFormatted(cell) ? cell.DateCellValue.ToString() : cell.NumericCellValue.ToString();
                                sValue = this.OnParseValue(column, sValue) ?? sValue;
                                break;
                            case CellType.BOOLEAN:
                                sValue = this.OnParseValue(column, cell.BooleanCellValue.ToString()) ?? cell.BooleanCellValue.ToString();
                                break;
                            case CellType.ERROR:
                            case CellType.BLANK:
                                throw new Exception(string.Format("Excel中第{0}行，第{1}列值有误！", i, column.Index));
                            default:
                                sValue = this.OnParseValue(column, cell.StringCellValue.Trim()) ?? cell.StringCellValue.Trim();
                                break;
                        }
                    } else {
                        switch(cell.CellType) {
                            case CellType.NUMERIC:
                                sValue = DateUtil.IsCellDateFormatted(cell) ? cell.DateCellValue.ToString() : cell.NumericCellValue.ToString();
                                break;
                            case CellType.BOOLEAN:
                                sValue = cell.BooleanCellValue.ToString();
                                break;
                            case CellType.ERROR:
                            case CellType.BLANK:
                                throw new Exception(string.Format("Excel中第{0}行，第{1}列值有误！", i, column.Index));
                            default:
                                sValue = cell.StringCellValue;
                                break;
                        }
                    }
                    rowData.Add(column.FieldName, sValue);
                }
                data.Add(rowData);
            }
            return data;
        }

        public int LoadExcelRow(Func<Dictionary<string, string>, bool> funcLoadExcelRow) {
            if(this.Columns.Count == 0)
                throw new Exception("请先指定Excel列对应的字段。");
            if(funcLoadExcelRow == null)
                throw new Exception("未定义导入数据处理方法");
            var result = this.sheet.LastRowNum - 1;
            for(var i = 1; i <= this.sheet.LastRowNum; i++) {
                var row = this.sheet.GetRow(i);
                if(row == null)
                    continue;
                var rowData = new Dictionary<string, string>();
                var hasValue = false;
                foreach(var column in this.Columns) {
                    if(column.Index == -1)
                        continue;
                    var sValue = "";
                    var cell = row.GetCell(column.Index);
                    if(cell != null) {
                        switch(cell.CellType) {
                            case CellType.NUMERIC:
                                sValue = DateUtil.IsCellDateFormatted(cell) || DateUtil.IsCellInternalDateFormatted(cell) || column.FieldType == typeof(DateTime) ? cell.DateCellValue.ToString() : cell.NumericCellValue.ToString();
                                break;
                            case CellType.BOOLEAN:
                                sValue = cell.BooleanCellValue.ToString();
                                break;
                            case CellType.ERROR:
                                throw new Exception(string.Format("Excel中第{0}行，第{1}列值有误！", i, column.Index));
                            case CellType.BLANK:
                                break;
                            default:
                                sValue = cell.StringCellValue.StartsWith("'") ? cell.StringCellValue.Substring(1) : cell.StringCellValue;
                                break;
                        }
                    }
                    if(!hasValue && !string.IsNullOrEmpty(sValue))
                        hasValue = true;
                    rowData.Add(column.FieldName, sValue);
                }
                if(!hasValue) {
                    result = i - 1;
                    break;
                }
                if(funcLoadExcelRow(rowData))
                    break;
            }
            return result;
        }

        /// <summary>
        /// 指定数据表对应列
        /// </summary>   
        /// <param name="columnName">Excel列名</param>
        /// <param name="fieldName">字段列</param>
        public void AddColumnDataSource(string columnName, string fieldName) {
            AddColumnDataSource(columnName, fieldName, null);
        }

        public void AddColumnDataSource(string columnName, string fieldName, Type fieldType) {
            if(string.IsNullOrEmpty(columnName))
                throw new ArgumentNullException("columnName");
            if(string.IsNullOrEmpty(fieldName))
                throw new ArgumentNullException("fieldName");
            var cName = columnName.ToUpper();
            var fName = fieldName.ToUpper();
            var excelIndex = this.ExcelIndexOfName(cName);
            if(excelIndex == -1)
                throw new Exception(string.Format("名为{0}的数据列不存在", cName));
            var index = this.IndexOfColumnName(cName);
            if(index != -1)
                throw new Exception(string.Format("名为{0}的数据列已定义对应字段", cName));
            index = this.IndexOfFieldName(fName);
            if(index != -1)
                throw new Exception(string.Format("名为{0}的字段已定义对应数据列", fName));
            this.Columns.Add(new ExcelColumns(columnName, fieldName, excelIndex, fieldType));
        }

        public ExcelImport(string excelFileName, string connectionStr)
            : this(excelFileName) {
            if(string.IsNullOrEmpty(connectionStr))
                throw new ArgumentNullException("connectionStr");
            ImportHelper = new ExcelImportHelper(connectionStr);
        }

        public ExcelImport(string excelFileName) {
            if(string.IsNullOrEmpty(excelFileName) || !File.Exists(excelFileName))
                throw new ArgumentException("未指定Excel文件名或找不到指定文件。", excelFileName);
            this.Columns = new List<ExcelColumns>();
            using(var xlsF = new FileStream(excelFileName, FileMode.Open, FileAccess.Read, FileShare.Read)) {
                this.wb = new HSSFWorkbook(xlsF);
                this.sheet = this.wb.GetSheetAt(0);
                //this.sheet = string.IsNullOrEmpty(sheetName) ? this.wb.GetSheetAt(0) : this.wb.GetSheet(sheetName);
                if(this.sheet == null)
                    throw new Exception("找不到符合条件的Sheet");
                if(this.sheet.LastRowNum == 0)
                    throw new ExcelNoDataException();
            }
            ImportHelper = new ExcelImportHelper();
        }

        public ExcelImport(FileStream excelFileName, string sheetName = "") {
            this.Columns = new List<ExcelColumns>();
            using(var xlsF = excelFileName) {
                this.wb = new HSSFWorkbook(xlsF);
                this.sheet = string.IsNullOrEmpty(sheetName) ? this.wb.GetSheetAt(0) : this.wb.GetSheet(sheetName);
                if(this.sheet == null)
                    throw new Exception("找不到符合条件的Sheet");
                if(this.sheet.LastRowNum == 0)
                    throw new ExcelNoDataException();
            }
            ImportHelper = new ExcelImportHelper();
        }

        public int ExcelIndexOfName(string columnName) {
            var index = -1;
            var row = this.sheet.GetRow(0);
            for(int i = 0; i <= row.LastCellNum - 1; i++) {
                var cell = row.GetCell(i);
                if(cell == null)
                    continue;
                if(cell.StringCellValue.Trim().Equals(columnName, StringComparison.OrdinalIgnoreCase)) {
                    if(index == -1)
                        index = i;
                    else
                        throw new Exception(string.Format("存在多个名为“{0}”的数据列", columnName));
                }
            }
            return index;
        }

        public void Dispose() {
            if(this.Columns != null)
                this.Columns.Clear();
        }
    }
}