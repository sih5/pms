﻿using System.Linq;
using System.Security.Principal;
using Microsoft.ServiceModel.DomainServices.Server.UnitTesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Sunlight.Silverlight.Dcs.Web.Services.Test {

    //http://msdn.microsoft.com/en-us/library/ee707368.aspx
    //http://blogs.msdn.com/b/kylemc/archive/2011/08/18/unit-testing-a-wcf-ria-domainservice-part-1-the-idomainservicefactory.aspx
    //http://www.silverlightshow.net/items/WCF-RIA-Services-Part-8-Testing-and-Debugging.aspx
    [TestClass]
    public class DcsDomainServiceTest {
        private static readonly IPrincipal authenticatedUser =
            new GenericPrincipal(new GenericIdentity("User"), new string[0]);
        private DomainServiceTestHost<DcsDomainService> dcsDomainServiceTestHost;



        [TestInitialize]
        public void CreateDcsDomainService() {
            this.dcsDomainServiceTestHost = new DomainServiceTestHost<DcsDomainService>(() => new DcsDomainService(), authenticatedUser);
        }

        [TestMethod]
        [Description("Tests that the GetBooks query returns all the books")]
        public void GetBooks_ReturnsAllBooks() {
            var books = this.dcsDomainServiceTestHost.Query(r => r.只查询仓库库存(1, 1, new int[] { 1 }));
            var truebooks = this.dcsDomainServiceTestHost.Query(r => r.GetPartsStocks()).Where(v => v.WarehouseId == 1 && v.PartId == 1 && v.StorageCompanyId == 1);
            Assert.AreEqual(truebooks.Count(), books.Count(),
                "Operation should return all books");
        }

        [TestMethod]
        [Description("Tests that the GetBooks query returns all the books")]
        public void GetBooks() {
            var t = new DcsDomainService();
            var tt = t.只查询仓库库存(1, 1, new int[] { 1 }).ToArray();
        }

        [TestMethod, Timeout(10000)]
        [Description("Tests that the GetBooks query returns all the books")]
        public void GetVehicleInformations() {
            var t = new DcsDomainService();
            //var tt = t.查询车辆信息及发动机保修信息("LVBV3JBB2EJ033183", null, 0, null).FirstOrDefault();
            //Assert.IsNotNull(tt, "查不到车辆信息");
        }

    }
}
