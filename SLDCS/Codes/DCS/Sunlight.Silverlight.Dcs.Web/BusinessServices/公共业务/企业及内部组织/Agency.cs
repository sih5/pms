﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class AgencyAch : DcsSerivceAchieveBase {
        public AgencyAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 停用代理库(Agency agency) {
            var dbagency = ObjectContext.Agencies.Where(r => r.Id == agency.Id && r.Status == (int)DcsMasterDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbagency == null)
                throw new ValidationException(ErrorStrings.Agency_Validation1);
            UpdateToDatabase(agency);
            agency.Status = (int)DcsMasterDataStatus.停用;
            this.UpdateAgencyValidate(agency);
        }

        public void 恢复代理库(Agency agency) {
            var dbagency = ObjectContext.Agencies.Where(r => r.Id == agency.Id && r.Status == (int)DcsMasterDataStatus.停用).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbagency == null)
                throw new ValidationException(ErrorStrings.Agency_Validation2);
            UpdateToDatabase(agency);
            agency.Status = (int)DcsMasterDataStatus.有效;
            this.UpdateAgencyValidate(agency);
        }

        public void 作废代理库(Agency agency) {
            var dbagency = ObjectContext.Agencies.Where(r => r.Id == agency.Id && r.Status == (int)DcsMasterDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbagency == null)
                throw new ValidationException(ErrorStrings.Agency_Validation3);
            UpdateToDatabase(agency);
            agency.Status = (int)DcsMasterDataStatus.作废;
            this.UpdateAgencyValidate(agency);
            var userInfo = Utils.GetCurrentUserInfo();
            var company = agency.Company;
            UpdateToDatabase(company);
            company.Status = (int)DcsMasterDataStatus.作废;
            company.ModifierId = userInfo.Id;
            company.ModifierName = userInfo.Name;
            company.ModifyTime = DateTime.Now;
            new CompanyAch(this.DomainService).UpdateCompanyValidate(company);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:09:49
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 停用代理库(Agency agency) {
            new AgencyAch(this).停用代理库(agency);
        }
        
        [Update(UsingCustomMethod = true)]
        public void 恢复代理库(Agency agency) {
            new AgencyAch(this).恢复代理库(agency);
        }
        
        [Update(UsingCustomMethod = true)]
        public void 作废代理库(Agency agency) {
            new AgencyAch(this).作废代理库(agency);
        }
    }
}
