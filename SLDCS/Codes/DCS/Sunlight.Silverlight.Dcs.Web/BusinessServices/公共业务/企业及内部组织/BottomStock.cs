﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web
{
    partial class BottomStockAch : DcsSerivceAchieveBase
    {
        public BottomStockAch(DcsDomainService domainService)
            : base(domainService)
        {
        }
        public void 新增保底库存(BottomStock bottomStock)
        {
            //重新赋值 品牌名称
            if (bottomStock.PartsSalesCategoryId != default(int))
            {
                var partsSalesCategory = ObjectContext.PartsSalesCategories.Where(v => v.Id == bottomStock.PartsSalesCategoryId && v.Status == (int)DcsBaseDataStatus.有效).FirstOrDefault();
                if (partsSalesCategory != null)
                {
                    bottomStock.PartsSalesCategoryName = partsSalesCategory.Name;
                }
                else
                {
                    throw new ValidationException(ErrorStrings.BottomStock_Validation1);
                }
            }

            //获取仓库编号
            if (bottomStock.WarehouseID != default(int) && bottomStock.WarehouseID != null)
            {
                var warehouse = ObjectContext.Warehouses.Where(v => v.Id == bottomStock.WarehouseID && v.Status == (int)DcsBaseDataStatus.有效).FirstOrDefault();
                if (warehouse != null)
                {
                    bottomStock.WarehouseCode = warehouse.Code;
                    bottomStock.WarehouseName = warehouse.Name;
                }
                else
                {
                    throw new ValidationException(ErrorStrings.BottomStock_Validation2);
                }
                var idAndSaleCenter = ObjectContext.BottomStocks.Where(v => v.WarehouseID == bottomStock.WarehouseID && v.CompanyID == bottomStock.CompanyID && v.SparePartId == bottomStock.SparePartId && v.Status == (int)DcsBaseDataStatus.有效).FirstOrDefault();
                var userInfo = Utils.GetCurrentUserInfo();
                if (idAndSaleCenter != null)
                {
                    //删除旧的
                    //新增新的
                    bottomStock.CreatorId = idAndSaleCenter.CreatorId;
                    bottomStock.CreatorName = idAndSaleCenter.CreatorName;
                    bottomStock.CreateTime = idAndSaleCenter.CreateTime;
                    bottomStock.ModifierId = userInfo.Id;
                    bottomStock.ModifierName = userInfo.Name;
                    bottomStock.ModifyTime = DateTime.Now;
                    bottomStock.Status = (int)DcsBaseDataStatus.有效;
                    InsertToDatabase(bottomStock);
                    InsertBottomStockValidate(bottomStock);
                    DeleteFromDatabase(idAndSaleCenter);
                }
                else
                {
                    bottomStock.CreatorId = userInfo.Id;
                    bottomStock.CreatorName = userInfo.Name;
                    bottomStock.CreateTime = DateTime.Now;
                    bottomStock.Status = (int)DcsBaseDataStatus.有效;
                    InsertToDatabase(bottomStock);
                    InsertBottomStockValidate(bottomStock);
                }
            }
            else
            {
                var idAndSaleCenter = ObjectContext.BottomStocks.Where(v => v.CompanyID == bottomStock.CompanyID && v.SparePartId == bottomStock.SparePartId && v.Status == (int)DcsBaseDataStatus.有效).FirstOrDefault();
                var userInfo = Utils.GetCurrentUserInfo();
                if (idAndSaleCenter != null)
                {
                    //删除旧的
                    //新增新的
                    bottomStock.CreatorId = idAndSaleCenter.CreatorId;
                    bottomStock.CreatorName = idAndSaleCenter.CreatorName;
                    bottomStock.CreateTime = idAndSaleCenter.CreateTime;
                    bottomStock.ModifierId = userInfo.Id;
                    bottomStock.ModifierName = userInfo.Name;
                    bottomStock.ModifyTime = DateTime.Now;
                    bottomStock.Status = (int)DcsBaseDataStatus.有效;
                    InsertToDatabase(bottomStock);
                    InsertBottomStockValidate(bottomStock);
                    DeleteFromDatabase(idAndSaleCenter);

                }
                else
                {
                    bottomStock.CreatorId = userInfo.Id;
                    bottomStock.CreatorName = userInfo.Name;
                    bottomStock.CreateTime = DateTime.Now;
                    bottomStock.Status = (int)DcsBaseDataStatus.有效;
                    InsertToDatabase(bottomStock);
                    InsertBottomStockValidate(bottomStock);
                }
            }
        }

        public void 修改保底库存(BottomStock bottomStock)
        {
            //只能对"有效"状态进行修改
            var idBottomStock = ObjectContext.BottomStocks.Where(v => v.Id == bottomStock.Id).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if (idBottomStock == null)
                throw new ValidationException(ErrorStrings.BottomStock_Validation3);
            if (idBottomStock.Status==(int)DcsBaseDataStatus.作废)
            {
                throw new ValidationException(ErrorStrings.BottomStock_Validation4);
            }
            //重新赋值 品牌名称
            if (bottomStock.PartsSalesCategoryId!=default(int))
            {
                var partsSalesCategory = ObjectContext.PartsSalesCategories.Where(v => v.Id == bottomStock.PartsSalesCategoryId && v.Status == (int)DcsBaseDataStatus.有效).FirstOrDefault();
                if (partsSalesCategory != null)
                {
                    bottomStock.PartsSalesCategoryName = partsSalesCategory.Name;
                }
                else
                {
                    throw new ValidationException(ErrorStrings.BottomStock_Validation5);
                }
            }

            //获取仓库编号
            if (bottomStock.WarehouseID != default(int) && bottomStock.WarehouseID != null)
            {
                var warehouse = ObjectContext.Warehouses.Where(v => v.Id == bottomStock.WarehouseID && v.Status == (int)DcsBaseDataStatus.有效).FirstOrDefault();
                if (warehouse != null)
                {
                    bottomStock.WarehouseCode = warehouse.Code;
                    bottomStock.WarehouseName = warehouse.Name;
                }
                else
                {
                    throw new ValidationException(ErrorStrings.BottomStock_Validation2);
                }
                var idAndSaleCenter = ObjectContext.BottomStocks.Where(v => v.WarehouseID == bottomStock.WarehouseID && v.CompanyID == bottomStock.CompanyID && v.SparePartId == bottomStock.SparePartId && v.Status == (int)DcsBaseDataStatus.有效).FirstOrDefault();
                var userInfo = Utils.GetCurrentUserInfo();
                if (idAndSaleCenter != null)
                {
                    if (idAndSaleCenter.Id == bottomStock.Id)
                    {
                        bottomStock.ModifierId = userInfo.Id;
                        bottomStock.ModifierName = userInfo.Name;
                        bottomStock.ModifyTime = DateTime.Now;
                        bottomStock.Status = (int)DcsBaseDataStatus.有效;
                        UpdateToDatabase(bottomStock);
                        UpdateBottomStockValidate(bottomStock);
                    }
                    else
                    { //删除旧的
                      //新增新的
                        bottomStock.CreatorId = idAndSaleCenter.CreatorId;
                        bottomStock.CreatorName = idAndSaleCenter.CreatorName;
                        bottomStock.CreateTime = idAndSaleCenter.CreateTime;
                        bottomStock.ModifierId = userInfo.Id;
                        bottomStock.ModifierName = userInfo.Name;
                        bottomStock.ModifyTime = DateTime.Now;
                        bottomStock.Status = (int)DcsBaseDataStatus.有效;
                        UpdateToDatabase(bottomStock);
                        UpdateBottomStockValidate(bottomStock);
                        DeleteFromDatabase(idAndSaleCenter);
                    }

                }
                else
                {
                    bottomStock.CreatorId = userInfo.Id;
                    bottomStock.CreatorName = userInfo.Name;
                    bottomStock.CreateTime = DateTime.Now;
                    bottomStock.Status = (int)DcsBaseDataStatus.有效;
                    UpdateToDatabase(bottomStock);
                    UpdateBottomStockValidate(bottomStock);
                }
            }
            else
            {
                var idAndSaleCenter = ObjectContext.BottomStocks.Where(v => v.PartsSalesCategoryId == bottomStock.PartsSalesCategoryId && v.CompanyID == bottomStock.CompanyID && v.SparePartId == bottomStock.SparePartId).FirstOrDefault();
                var userInfo = Utils.GetCurrentUserInfo();
                if (idAndSaleCenter != null)
                {
                    if (idAndSaleCenter.Id == bottomStock.Id)
                    {
                        bottomStock.ModifierId = userInfo.Id;
                        bottomStock.ModifierName = userInfo.Name;
                        bottomStock.ModifyTime = DateTime.Now;
                        bottomStock.Status = (int)DcsBaseDataStatus.有效;
                        UpdateToDatabase(bottomStock);
                        UpdateBottomStockValidate(bottomStock);
                    }
                    else
                    { //删除旧的
                      //新增新的
                        bottomStock.CreatorId = idAndSaleCenter.CreatorId;
                        bottomStock.CreatorName = idAndSaleCenter.CreatorName;
                        bottomStock.CreateTime = idAndSaleCenter.CreateTime;
                        bottomStock.ModifierId = userInfo.Id;
                        bottomStock.ModifierName = userInfo.Name;
                        bottomStock.ModifyTime = DateTime.Now;
                        bottomStock.Status = (int)DcsBaseDataStatus.有效;
                        UpdateToDatabase(bottomStock);
                        UpdateBottomStockValidate(bottomStock);
                        DeleteFromDatabase(idAndSaleCenter);
                    }

                }
                else
                {
                    bottomStock.CreatorId = userInfo.Id;
                    bottomStock.CreatorName = userInfo.Name;
                    bottomStock.CreateTime = DateTime.Now;
                    bottomStock.Status = (int)DcsBaseDataStatus.有效;
                    UpdateToDatabase(bottomStock);
                    UpdateBottomStockValidate(bottomStock);
                }
            }



        }
        public void 作废保底库存(BottomStock bottomStock)
        {
            //只能对"有效"状态进行作废
            var idBottomStock = ObjectContext.BottomStocks.Where(v => v.Id == bottomStock.Id).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if (idBottomStock == null)
                throw new ValidationException(ErrorStrings.BottomStock_Validation3);
            if (idBottomStock.Status == (int)DcsBaseDataStatus.作废)
            {
                throw new ValidationException(ErrorStrings.BottomStock_Validation4);
            }
            var userInfo = Utils.GetCurrentUserInfo();
            bottomStock.AbandonerId = userInfo.Id;
            bottomStock.AbandonerName = userInfo.Name;
            bottomStock.AbandonTime = DateTime.Now;
            bottomStock.Status = (int)DcsBaseDataStatus.作废;
            UpdateToDatabase(bottomStock);
            UpdateBottomStockValidate(bottomStock);
        }

        public void 批量作废保底库存(int[] ids) {
            if (ids == null || !ids.Any())
                return;
            //只能对"有效"状态进行作废
            var userInfo = Utils.GetCurrentUserInfo();
            var bottomStocks = ObjectContext.BottomStocks.Where(v => ids.Contains(v.Id));
            foreach (var item in bottomStocks) {
                if (item.Status == (int)DcsBaseDataStatus.作废) {
                    throw new ValidationException(ErrorStrings.BottomStock_Validation4);
                }
                item.AbandonerId = userInfo.Id;
                item.AbandonerName = userInfo.Name;
                item.AbandonTime = DateTime.Now;
                item.Status = (int)DcsBaseDataStatus.作废;
                UpdateToDatabase(item);
                UpdateBottomStockValidate(item);
            }
            ObjectContext.SaveChanges();
        }
    }
    partial class DcsDomainService
    {// 已经使用工具进行处理 2017/4/29 12:09:49
     //原分布类的函数全部转移到Ach                                                                

        [Update(UsingCustomMethod = true)]
        public void 新增保底库存(BottomStock bottomStock)
        {
            new BottomStockAch(this).新增保底库存(bottomStock);
        }

        [Update(UsingCustomMethod = true)]
        public void 修改保底库存(BottomStock bottomStock)
        {
            new BottomStockAch(this).修改保底库存(bottomStock);
        }

        [Update(UsingCustomMethod = true)]
        public void 作废保底库存(BottomStock bottomStock)
        {
            new BottomStockAch(this).作废保底库存(bottomStock);
        }

        [Invoke(HasSideEffects = true)]
        public void 批量作废保底库存(int[] ids)
        {
            new BottomStockAch(this).批量作废保底库存(ids);
        }
    }
}
