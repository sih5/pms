﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class BranchAch : DcsSerivceAchieveBase {
        public BranchAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 停用营销分公司(Branch branch) {
            var dbbranch = ObjectContext.Branches.Where(r => r.Id == branch.Id && r.Status == (int)DcsMasterDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbbranch == null)
                throw new ValidationException(ErrorStrings.Branch_Validation1);
            UpdateToDatabase(branch);
            branch.Status = (int)DcsMasterDataStatus.停用;
            this.UpdateBranchValidate(branch);
        }

        public void 恢复营销分公司(Branch branch) {
            var dbbranch = ObjectContext.Branches.Where(r => r.Id == branch.Id && r.Status == (int)DcsMasterDataStatus.停用).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbbranch == null)
                throw new ValidationException(ErrorStrings.Branch_Validation2);
            UpdateToDatabase(branch);
            branch.Status = (int)DcsMasterDataStatus.有效;
            this.UpdateBranchValidate(branch);
        }

        public void 作废营销分公司(Branch branch) {
            var dbbranch = ObjectContext.Branches.Where(r => r.Id == branch.Id && r.Status == (int)DcsMasterDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbbranch == null)
                throw new ValidationException(ErrorStrings.Branch_Validation3);
            UpdateToDatabase(branch);
            branch.Status = (int)DcsMasterDataStatus.作废;
            this.UpdateBranchValidate(branch);
            var userInfo = Utils.GetCurrentUserInfo();
            var company = branch.Company;
            UpdateToDatabase(company);
            company.Status = (int)DcsMasterDataStatus.作废;
            company.ModifierId = userInfo.Id;
            company.ModifierName = userInfo.Name;
            company.ModifyTime = DateTime.Now;
            new CompanyAch(this.DomainService).UpdateCompanyValidate(company);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:09:49
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 停用营销分公司(Branch branch) {
            new BranchAch(this).停用营销分公司(branch);
        }
        
        [Update(UsingCustomMethod = true)]
        public void 恢复营销分公司(Branch branch) {
            new BranchAch(this).恢复营销分公司(branch);
        }
        
        [Update(UsingCustomMethod = true)]
        public void 作废营销分公司(Branch branch) {
            new BranchAch(this).作废营销分公司(branch);
        }
    }
}
