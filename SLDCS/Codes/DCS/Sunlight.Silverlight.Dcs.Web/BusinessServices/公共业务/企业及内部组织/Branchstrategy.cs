﻿using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class BranchstrategyAch : DcsSerivceAchieveBase {
        public BranchstrategyAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 作废分公司策略(Branchstrategy branchstrategy) {
            var dbBranchstrategy = ObjectContext.Branchstrategies.Where(r => r.Id == branchstrategy.Id && r.Status != (int)DcsBaseDataStatus.作废).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbBranchstrategy == null)
                throw new ValidationException(ErrorStrings.Branchstrategy_Validation3);
            var userinfo = Utils.GetCurrentUserInfo();
            UpdateToDatabase(branchstrategy);
            branchstrategy.Status = (int)DcsBaseDataStatus.作废;
            branchstrategy.AbandonerId = userinfo.Id;
            branchstrategy.AbandonerName = userinfo.Name;
            branchstrategy.AbandonTime = System.DateTime.Now;
            this.UpdateBranchstrategyValidate(branchstrategy);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:09:49
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 作废分公司策略(Branchstrategy branchstrategy) {
            new BranchstrategyAch(this).作废分公司策略(branchstrategy);
        }
    }
}
