﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Web;using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class CompanyInvoiceInfoAch : DcsSerivceAchieveBase {
        public CompanyInvoiceInfoAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 生成企业开票信息(CompanyInvoiceInfo companyInvoiceInfo) {
            #region
            //新增企业票信息；
            //查询经销商基本信息（企业开票信息.企业id=经销商基本信息）；如果查到结果
            //新增经销商基本信息履历：
            //主记录Id=经销商基本信息.Id
            //经销商编号=经销商基本信息.经销商编号
            //变更后税务登记号=企业开票信息.税务登记号
            //变更后开票名称=企业开票信息.开票名称
            //变更后开票类型=企业开票信息.开票类型
            //变更后纳税人性质=企业开票信息.纳税人性质
            //变更后发票限额=企业开票信息.发票限额
            //变更后税务登记地址=企业开票信息.税务登记地址
            //变更后税务登记电话=企业开票信息.税务登记电话
            //变更后银行名称=企业开票信息.银行名称
            //变更后银行账号=企业开票信息.银行账号
            //变更后开票税率=企业开票信息.开票税率
            //变更后财务联系人=企业开票信息.财务联系人
            //变更后财务联系电话=企业开票信息.财务联系电话
            //变更后财务传真=企业开票信息.财务传真
            // 归档人=当前登录人
            //  归档时间=当前时间
            //  归档人Id=当前登录人id
            //  创建人=当前登录人
            //  创建时间=当前时间
            //  创建人Id=当前登录人id
            #endregion
            var dealer = ObjectContext.Dealers.SingleOrDefault(r => r.Id == companyInvoiceInfo.CompanyId);
            if(dealer == null) {
                return;
            }
            var originalCompanyInvoiceInfo = ObjectContext.CompanyInvoiceInfoes.Where(r => r.CompanyId == dealer.Id).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(originalCompanyInvoiceInfo != null) {
                throw new ValidationException(ErrorStrings.CompanyInvoiceInfo_Validation4);
            }
            var propertyNames = new[] {
                "TAXREGISTEREDNUMBER", "INVOICETITLE", "INVOICETYPE", "TAXPAYERQUALIFICATION", "INVOICEAMOUNTQUOTA", "TAXREGISTEREDADDRESS", "TAXREGISTEREDPHONE", "BANKNAME", "BANKACCOUNT", "INVOICETAX", "LINKMAN", "CONTACTNUMBER", "FAX"
            };
            var propertiesNeedRecord = GetPropertiesNeedRecordInRangeForInsert(companyInvoiceInfo, propertyNames);
            var dictionaryOfMapping = new Dictionary<string, PropertyChangeInfo>();
            foreach(var propertyChangeInfo in propertiesNeedRecord) {
                var tempKey = propertyNames.SingleOrDefault(r => r == propertyChangeInfo.PropertyName);
                if(tempKey == null)
                    continue;
                tempKey = "A" + tempKey;
                dictionaryOfMapping.Add(tempKey, propertyChangeInfo);
            }
            var dealerHistory = new DealerHistory();
            SetValuesToEntity(dealerHistory, dictionaryOfMapping, "INSERT");
            var userInfo = Utils.GetCurrentUserInfo();
            #region 直接赋值
            //dealerHistory.ATaxRegisteredNumber = companyInvoiceInfo.TaxRegisteredNumber;
            //dealerHistory.AInvoiceTitle = companyInvoiceInfo.InvoiceTitle;
            //dealerHistory.AInvoiceType = companyInvoiceInfo.InvoiceType;
            //dealerHistory.ATaxpayerQualification = companyInvoiceInfo.TaxpayerQualification;
            //dealerHistory.AInvoiceAmountQuota = companyInvoiceInfo.InvoiceAmountQuota;
            //dealerHistory.ATaxRegisteredAddress = companyInvoiceInfo.TaxRegisteredAddress;
            //dealerHistory.ATaxRegisteredPhone = companyInvoiceInfo.TaxRegisteredPhone;
            //dealerHistory.ABankName = companyInvoiceInfo.BankName;
            //dealerHistory.ABankAccount = companyInvoiceInfo.BankAccount;
            //dealerHistory.AInvoiceTax = companyInvoiceInfo.InvoiceTax;
            //dealerHistory.ALinkman = companyInvoiceInfo.Linkman;
            //dealerHistory.AContactNumber = companyInvoiceInfo.ContactNumber;
            //dealerHistory.AFax = companyInvoiceInfo.Fax;
            #endregion
            dealerHistory.CreatorId = userInfo.Id;
            dealerHistory.CreatorName = userInfo.Name;
            dealerHistory.CreateTime = DateTime.Now;
            dealerHistory.RecordId = dealer.Id;
            dealerHistory.Code = dealer.Code;
            dealerHistory.FilerId = userInfo.Id;
            dealerHistory.FilerName = userInfo.Name;
            dealerHistory.FileTime = DateTime.Now;
            this.InsertToDatabase(dealerHistory);
        }


        public void 修改企业开票信息(CompanyInvoiceInfo companyInvoiceInfo) {
            var dealer = ObjectContext.Dealers.SingleOrDefault(r => r.Id == companyInvoiceInfo.CompanyId);
            if(dealer == null) {
                return;
            }
            var originalCompanyInvoiceInfo = ObjectContext.CompanyInvoiceInfoes.Where(r => r.Id == companyInvoiceInfo.Id).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(originalCompanyInvoiceInfo == null) {
                throw new ValidationException(ErrorStrings.CompanyInvoiceInfo_Validation5);
            }
            //税务登记号,开票名称,开票类型,纳税人性质,发票限额,税务登记地址,税务登记电话,银行名称,银行账号,开票税率,财务联系人,财务联系电话，财务传真
            var propertyNames = new[] {
                "TAXREGISTEREDNUMBER", "INVOICETITLE", "INVOICETYPE", "TAXPAYERQUALIFICATION", "INVOICEAMOUNTQUOTA", "TAXREGISTEREDADDRESS", "TAXREGISTEREDPHONE", "BANKNAME", "BANKACCOUNT", "INVOICETAX", "LINKMAN", "CONTACTNUMBER", "FAX"
            };
            var propertiesNeedRecord = new CompanyInvoiceInfoAch(this.DomainService).GetPropertiesNeedRecordInRangeForUpdate(originalCompanyInvoiceInfo, companyInvoiceInfo, propertyNames);
            if(propertiesNeedRecord.Count == 0) {
                return;
            }
            var dictionaryOfMapping = new Dictionary<string, PropertyChangeInfo>();
            foreach(var propertyChangeInfo in propertiesNeedRecord) {
                var tempKey = propertyNames.SingleOrDefault(r => r == propertyChangeInfo.PropertyName);
                if(tempKey == null)
                    continue;
                tempKey = "A" + tempKey;
                dictionaryOfMapping.Add(tempKey, propertyChangeInfo);
            }
            var dealerHistory = new DealerHistory();
            SetValuesToEntity(dealerHistory, dictionaryOfMapping, "UPDATE");
            var userInfo = Utils.GetCurrentUserInfo();
            dealerHistory.CreatorId = userInfo.Id;
            dealerHistory.CreatorName = userInfo.Name;
            dealerHistory.CreateTime = DateTime.Now;
            dealerHistory.RecordId = dealer.Id;
            dealerHistory.Code = dealer.Code;
            dealerHistory.FilerId = userInfo.Id;
            dealerHistory.FilerName = userInfo.Name;
            dealerHistory.FileTime = DateTime.Now;
            this.InsertToDatabase(dealerHistory);
        }


        public void 生成经销商基本信息集中(Company company, Dealer dealer, DealerServiceExt dealerServiceExt, CompanyInvoiceInfo companyInvoiceInfo, DealerMobileNumberList[] dealerMobileNumberLists) {
            /*
            变更后是否整车销售开票=经销商服务扩展信息.是否整车销售开票 -------没有
            变更后是否配件销售开票=经销商服务扩展信息.是否配件销售开票 -------没有
             */
            var dbCompany = this.ObjectContext.Companies.FirstOrDefault(r => r.Code == company.Code);
            if(dbCompany != null) {
                throw new ValidationException(ErrorStrings.CompanyInvoiceInfo_Validation6);
            }
            var dbDealer = this.ObjectContext.Dealers.FirstOrDefault(r => r.Code == dealer.Code);
            if(dbDealer != null) {
                throw new ValidationException(ErrorStrings.CompanyInvoiceInfo_Validation7);
            }
            this.InsertToDatabase(dealer);
            dealer.Company = company;
            this.InsertToDatabase(company);
            var userInfo = Utils.GetCurrentUserInfo();
            var dictionaryOfMapping = new Dictionary<string, PropertyChangeInfo>();
            #region Company
            //履历表中没有company表中对应的变更后备注,company表中没有履历中的需要的变更后企业传真
            var propertyNamesOfCompany = new[] {
            "PROVINCENAME","CITYNAME","COUNTYNAME","CITYLEVEL","FOUNDDATE","CONTACTPERSON","CONTACTPHONE","CONTACTMOBILE","CONTACTPOSTCODE","CONTACTMAIL","REGISTERCODE","REGISTERNAME","CORPORATENATURE",
            "LEGALREPRESENTATIVE","LEGALREPRESENTTEL","IDDOCUMENTTYPE","IDDOCUMENTNUMBER","REGISTERCAPITAL","FIXEDASSET","REGISTERDATE","BUSINESSSCOPE","BUSINESSADDRESS","REGISTEREDADDRESS","REMARK"};
            var propertiesNeedRecordOfCompany =GetPropertiesNeedRecordInRangeForInsert(company, propertyNamesOfCompany);
            foreach(var propertyChangeInfo in propertiesNeedRecordOfCompany) {
                var tempKey = propertyNamesOfCompany.SingleOrDefault(r => r == propertyChangeInfo.PropertyName);
                if(tempKey == null) {
                    continue;
                }
                tempKey = "A" + tempKey;
                dictionaryOfMapping.Add(tempKey, propertyChangeInfo);
            }
            company.CreatorId = userInfo.Id;
            company.CreatorName = userInfo.Name;
            company.CreateTime = DateTime.Now;
            #endregion
            #region Dealer
            var propertyNamesOfDealer = new[] {
                "NAME","SHORTNAME","MANAGER","STATUS"
            };
            var propertiesNeedRecordOfDealer =GetPropertiesNeedRecordInRangeForInsert(dealer, propertyNamesOfDealer);
            foreach(var propertyChangeInfo in propertiesNeedRecordOfDealer) {
                var tempKey = propertyNamesOfDealer.SingleOrDefault(r => r == propertyChangeInfo.PropertyName);
                if(tempKey == null) {
                    continue;
                }
                tempKey = "A" + tempKey;
                dictionaryOfMapping.Add(tempKey, propertyChangeInfo);
            }
            dealer.CreatorId = userInfo.Id;
            dealer.CreatorName = userInfo.Name;
            dealer.CreateTime = DateTime.Now;
            #endregion
            #region DealerServiceExt
            if(dealerServiceExt != null) {
                dealer.DealerServiceExt = dealerServiceExt;
                this.InsertToDatabase(dealerServiceExt);
                var propertyNamesOfDealerServiceExt = new[] {
                        "REPAIRQUALIFICATION", "HASBRANCH", "DANGEROUSREPAIRQUALIFICATION", "BRANDSCOPE", "COMPETITIVEBRANDSCOPE", "PARKINGAREA", "REPAIRINGAREA", "EMPLOYEENUMBER", "PARTWAREHOUSEAREA", "GEOGRAPHICPOSITION", "OWNERCOMPANY", "MAINBUSINESSAREAS", "ANDBUSINESSAREAS", "BUILDTIME", "TRAFFICRESTRICTIONSDESCRIBE", "MANAGERPHONENUMBER", "MANAGERMOBILE", "MANAGERMAIL", "RECEPTIONROOMAREA"
                    };
                var propertiesNeedRecordOfDealerServiceExt =GetPropertiesNeedRecordInRangeForInsert(dealerServiceExt, propertyNamesOfDealerServiceExt);
                foreach(var propertyChangeInfo in propertiesNeedRecordOfDealerServiceExt) {
                    var tempKey = propertyNamesOfDealerServiceExt.SingleOrDefault(r => r == propertyChangeInfo.PropertyName);
                    if(tempKey == null) {
                        continue;
                    }
                    tempKey = "A" + tempKey;
                    dictionaryOfMapping.Add(tempKey, propertyChangeInfo);
                }
                dealerServiceExt.CreatorId = userInfo.Id;
                dealerServiceExt.CreatorName = userInfo.Name;
                dealerServiceExt.CreateTime = DateTime.Now;
            }
            #endregion
            #region CompanyInvoiceInfo
            if(companyInvoiceInfo != null) {
                dealer.CompanyInvoiceInfo.Add(companyInvoiceInfo);
                this.InsertToDatabase(companyInvoiceInfo);
                var propertyNamesOfCompanyInvoiceInfo = new[] {
                    "TAXREGISTEREDNUMBER", "INVOICETITLE", "INVOICETYPE", "TAXPAYERQUALIFICATION", "INVOICEAMOUNTQUOTA", "TAXREGISTEREDADDRESS", "TAXREGISTEREDPHONE", "BANKNAME", "BANKACCOUNT", "INVOICETAX", "LINKMAN", "CONTACTNUMBER", "FAX"
                };
                var propertiesNeedRecordOfCompanyInvoiceInfo =GetPropertiesNeedRecordInRangeForInsert(companyInvoiceInfo, propertyNamesOfCompanyInvoiceInfo);
                foreach(var propertyChangeInfo in propertiesNeedRecordOfCompanyInvoiceInfo) {
                    var tempKey = propertyNamesOfCompanyInvoiceInfo.SingleOrDefault(r => r == propertyChangeInfo.PropertyName);
                    if(tempKey == null) {
                        continue;
                    }
                    tempKey = "A" + tempKey;
                    dictionaryOfMapping.Add(tempKey, propertyChangeInfo);
                }
                companyInvoiceInfo.CreatorId = userInfo.Id;
                companyInvoiceInfo.CreatorName = userInfo.Name;
                companyInvoiceInfo.CreateTime = DateTime.Now;
            }
            #endregion
            if(dealerMobileNumberLists != null && dealerMobileNumberLists.Length > 0) {
                foreach(var dealerMobileNumberList in dealerMobileNumberLists) {
                    dealer.DealerMobileNumberLists.Add(dealerMobileNumberList);
                    this.InsertToDatabase(dealerMobileNumberList);
                }
            }
            var dealerHistory = new DealerHistory();
            SetValuesToEntity(dealerHistory, dictionaryOfMapping, "INSERT"); //由于这里可以保证都是新增的 所以不必分开用不同的dictionaryOfMapping
            dealerHistory.CreatorId = userInfo.Id;
            dealerHistory.CreatorName = userInfo.Name;
            dealerHistory.CreateTime = DateTime.Now;
            dealerHistory.RecordId = dealer.Id;
            dealerHistory.Code = dealer.Code;
            dealerHistory.FilerId = userInfo.Id;
            dealerHistory.FilerName = userInfo.Name;
            dealerHistory.FileTime = DateTime.Now;
            this.InsertToDatabase(dealerHistory);
            ObjectContext.SaveChanges();
        }

        public void 修改经销商基本信息集中(Company company, Dealer dealer, DealerServiceExt dealerServiceExt, CompanyInvoiceInfo companyInvoiceInfo, DealerMobileNumberList[] dealerMobileNumberLists) {
            var userInfo = Utils.GetCurrentUserInfo();
            var companyOriginal = ObjectContext.Companies.Where(r => r.Id == company.Id).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(companyOriginal == null) {
                throw new ValidationException(ErrorStrings.CompanyInvoiceInfo_Validation8);
            }
            UpdateToDatabase(company);
            var dealerOriginal = ObjectContext.Dealers.Where(r => r.Id == dealer.Id).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dealerOriginal == null) {
                throw new ValidationException(ErrorStrings.CompanyInvoiceInfo_Validation9);
            }
            UpdateToDatabase(dealer);

            var dealerHistory = new DealerHistory();
            dealerHistory.CreatorId = userInfo.Id;
            dealerHistory.CreatorName = userInfo.Name;
            dealerHistory.CreateTime = DateTime.Now;
            dealerHistory.RecordId = dealer.Id;
            dealerHistory.Code = dealer.Code;
            dealerHistory.FilerId = userInfo.Id;
            dealerHistory.FilerName = userInfo.Name;
            dealerHistory.FileTime = DateTime.Now;
            //this.InsertToDatabase(dealerHistory);
            #region DealerMobileNumberLists 这段不用记履历
            if(dealerMobileNumberLists != null && dealerMobileNumberLists.Length > 0) {
                var dealerIdsOfDealerMobileNumberList = dealerMobileNumberLists.Select(r => r.DealerId).Distinct().ToArray();
                if(dealerIdsOfDealerMobileNumberList.Length != 1) {
                    throw new ValidationException(ErrorStrings.CompanyInvoiceInfo_Validation10);
                }
                var dealerIdOfDealerMobileNumberList = dealerIdsOfDealerMobileNumberList[0];
                if(dealerIdOfDealerMobileNumberList != dealer.Id) {
                    throw new ValidationException(ErrorStrings.CompanyInvoiceInfo_Validation11);
                }
                var mobileNumbersNow = dealerMobileNumberLists.Select(r => r.VideoMobileNumber).ToArray();
                var dbDealerMobileNumberLists = ObjectContext.DealerMobileNumberLists.Where(r => r.DealerId == dealerIdOfDealerMobileNumberList ).SetMergeOption(MergeOption.NoTracking).ToArray();
                var mobileNumbersOriginal = dbDealerMobileNumberLists.Select(r => r.VideoMobileNumber).ToArray();
                var mobileNumbersNeedAdd = mobileNumbersNow.Except(mobileNumbersOriginal).ToArray();
                var mobileNumbersNeedDelete = mobileNumbersOriginal.Except(mobileNumbersNow).ToArray();
                var dealerMobileNumberListNeedAdd = dealerMobileNumberLists.Where(r => mobileNumbersNeedAdd.Contains(r.VideoMobileNumber)).ToArray();
                foreach(var item in dealerMobileNumberListNeedAdd) {
                    InsertToDatabase(item);
                }
                var IdsOfDealerMobileNumberListNeedDelete = dbDealerMobileNumberLists.Where(r => mobileNumbersNeedDelete.Contains(r.VideoMobileNumber)).Select(r => r.Id).ToArray();
                var dealerMobileNumberListNeedDelete = ObjectContext.DealerMobileNumberLists.Where(r => IdsOfDealerMobileNumberListNeedDelete.Contains(r.Id)).ToArray();
                foreach(var item in dealerMobileNumberListNeedDelete) {
                    DeleteFromDatabase(item);
                }
            }
            #endregion
            #region Company 肯定是修改
            //履历表中没有company表中对应的变更后备注,company表中没有履历中的需要的变更后企业传真
            var dictionaryOfMappingCompany = new Dictionary<string, PropertyChangeInfo>();
            var propertyNamesOfCompany = new[] {
            "PROVINCENAME","CITYNAME","COUNTYNAME","CITYLEVEL","FOUNDDATE","CONTACTPERSON","CONTACTPHONE","CONTACTMOBILE","CONTACTPOSTCODE","CONTACTMAIL","REGISTERCODE","REGISTERNAME","CORPORATENATURE",
            "LEGALREPRESENTATIVE","LEGALREPRESENTTEL","IDDOCUMENTTYPE","IDDOCUMENTNUMBER","REGISTERCAPITAL","FIXEDASSET","REGISTERDATE","BUSINESSSCOPE","BUSINESSADDRESS","REGISTEREDADDRESS","REMARK"};
            var propertiesNeedRecordOfCompany = new CompanyInvoiceInfoAch(this.DomainService).GetPropertiesNeedRecordInRangeForUpdate(companyOriginal, company, propertyNamesOfCompany);
            foreach(var propertyChangeInfo in propertiesNeedRecordOfCompany) {
                var tempKey = propertyNamesOfCompany.SingleOrDefault(r => r == propertyChangeInfo.PropertyName);
                if(tempKey == null) {
                    continue;
                }
                tempKey = "A" + tempKey;
                dictionaryOfMappingCompany.Add(tempKey, propertyChangeInfo);
            }
            SetValuesToEntity(dealerHistory, dictionaryOfMappingCompany, "UPDATE");
            company.ModifierId = userInfo.Id;
            company.ModifierName = userInfo.Name;
            company.ModifyTime = DateTime.Now;
            #endregion
            #region Dealer 肯定是修改
            var dictionaryOfMapping = new Dictionary<string, PropertyChangeInfo>();
            var propertyNamesOfDealer = new[] {
                "NAME", "SHORTNAME", "MANAGER", "STATUS"
            };
            var propertiesNeedRecordOfDealer = new CompanyInvoiceInfoAch(this.DomainService).GetPropertiesNeedRecordInRangeForUpdate(dealerOriginal, dealer, propertyNamesOfDealer);
            foreach(var propertyChangeInfo in propertiesNeedRecordOfDealer) {
                var tempKey = propertyNamesOfDealer.SingleOrDefault(r => r == propertyChangeInfo.PropertyName);
                if(tempKey == null) {
                    continue;
                }
                tempKey = "A" + tempKey;
                dictionaryOfMapping.Add(tempKey, propertyChangeInfo);
            }
            SetValuesToEntity(dealerHistory, dictionaryOfMapping, "UPDATE");
            dealer.ModifierId = userInfo.Id;
            dealer.ModifierName = userInfo.Name;
            dealer.ModifyTime = DateTime.Now;
            #endregion
            #region CompanyInvoiceInfo 可能是新增 可能是修改
            //CompanyInvoiceInfo
            if(companyInvoiceInfo != null) {
                var dictionaryOfMappingCompanyInvoiceInfo = new Dictionary<string, PropertyChangeInfo>();
                var flagOfInsertOrUpdate = "UPDATE";
                var companyInvoiceInfoOriginal = ObjectContext.CompanyInvoiceInfoes.Where(r => r.Id == companyInvoiceInfo.Id).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
                if(companyInvoiceInfoOriginal == null) {
                    this.InsertToDatabase(companyInvoiceInfo);
                    flagOfInsertOrUpdate = "INSERT";
                    companyInvoiceInfo.CreatorId = userInfo.Id;
                    companyInvoiceInfo.CreatorName = userInfo.Name;
                    companyInvoiceInfo.CreateTime = DateTime.Now;
                }else {
                    UpdateToDatabase(companyInvoiceInfo);
                    companyInvoiceInfo.ModifierId = userInfo.Id;
                    companyInvoiceInfo.ModifierName = userInfo.Name;
                    companyInvoiceInfo.ModifyTime = DateTime.Now;
                }
                var propertyNamesOfCompanyInvoiceInfo = new[] {
                    "TAXREGISTEREDNUMBER", "INVOICETITLE", "INVOICETYPE", "TAXPAYERQUALIFICATION", "INVOICEAMOUNTQUOTA", "TAXREGISTEREDADDRESS", "TAXREGISTEREDPHONE", "BANKNAME", "BANKACCOUNT", "INVOICETAX", "LINKMAN", "CONTACTNUMBER", "FAX"
                };
                List<PropertyChangeInfo> propertiesNeedRecordOfCompanyInvoiceInfo = null;
                propertiesNeedRecordOfCompanyInvoiceInfo = flagOfInsertOrUpdate == "INSERT" ? this.GetPropertiesNeedRecordInRangeForInsert(companyInvoiceInfo, propertyNamesOfCompanyInvoiceInfo) : this.GetPropertiesNeedRecordInRangeForUpdate(companyInvoiceInfoOriginal, companyInvoiceInfo, propertyNamesOfCompanyInvoiceInfo);
                foreach(var propertyChangeInfo in propertiesNeedRecordOfCompanyInvoiceInfo) {
                    var tempKey = propertyNamesOfCompanyInvoiceInfo.SingleOrDefault(r => r == propertyChangeInfo.PropertyName);
                    if(tempKey == null) {
                        continue;
                    }
                    tempKey = "A" + tempKey;
                    dictionaryOfMappingCompanyInvoiceInfo.Add(tempKey, propertyChangeInfo);
                }
                this.SetValuesToEntity(dealerHistory, dictionaryOfMappingCompanyInvoiceInfo, flagOfInsertOrUpdate == "INSERT" ? "INSERT" : "UPDATE");
            }
            #endregion
            #region DealerServiceExt 可能是新增 可能是修改
            //DealerServiceExt
            //这个新增的时候可以不填,修改的时候填的话就是新增 ,新增的时候填了修改的时候再填的话就是修改
            if(dealerServiceExt != null) {
                var dictionaryOfMappingDealerServiceExt = new Dictionary<string, PropertyChangeInfo>();
                var flagOfInsertOrUpdate = "UPDATE";
                var dealerServiceExtOriginal = ObjectContext.DealerServiceExts.Where(r => r.Id == dealerServiceExt.Id).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
                if(dealerServiceExtOriginal == null) {
                    this.InsertToDatabase(dealerServiceExt);
                    flagOfInsertOrUpdate = "INSERT";
                    dealerServiceExt.CreatorId = userInfo.Id;
                    dealerServiceExt.CreatorName = userInfo.Name;
                    dealerServiceExt.CreateTime = DateTime.Now;
                }else {
                    UpdateToDatabase(dealerServiceExt);
                    dealerServiceExt.ModifierId = userInfo.Id;
                    dealerServiceExt.ModifierName = userInfo.Name;
                    dealerServiceExt.ModifyTime = DateTime.Now;
                }
                var propertyNamesOfDealerServiceExt = new[] {
                    "REPAIRQUALIFICATION", "HASBRANCH", "DANGEROUSREPAIRQUALIFICATION", "BRANDSCOPE", "COMPETITIVEBRANDSCOPE", "PARKINGAREA", "REPAIRINGAREA", "EMPLOYEENUMBER", "PARTWAREHOUSEAREA", "GEOGRAPHICPOSITION", "OWNERCOMPANY", "MAINBUSINESSAREAS", "ANDBUSINESSAREAS", "BUILDTIME", "TRAFFICRESTRICTIONSDESCRIBE", "MANAGERPHONENUMBER", "MANAGERMOBILE", "MANAGERMAIL", "RECEPTIONROOMAREA"
                };
                List<PropertyChangeInfo> propertiesNeedRecordOfDealerServiceExt = null;
                propertiesNeedRecordOfDealerServiceExt = flagOfInsertOrUpdate == "INSERT" ? this.GetPropertiesNeedRecordInRangeForInsert(dealerServiceExt, propertyNamesOfDealerServiceExt) : this.GetPropertiesNeedRecordInRangeForUpdate(dealerServiceExtOriginal, dealerServiceExt, propertyNamesOfDealerServiceExt);
                if(propertiesNeedRecordOfDealerServiceExt != null) {
                    foreach(var propertyChangeInfo in propertiesNeedRecordOfDealerServiceExt) {
                        var tempKey = propertyNamesOfDealerServiceExt.SingleOrDefault(r => r == propertyChangeInfo.PropertyName);
                        if(tempKey == null) {
                            continue;
                        }
                        tempKey = "A" + tempKey;
                        dictionaryOfMappingDealerServiceExt.Add(tempKey, propertyChangeInfo);
                    }
                }
                this.SetValuesToEntity(dealerHistory, dictionaryOfMappingDealerServiceExt, flagOfInsertOrUpdate == "INSERT" ? "INSERT" : "UPDATE");
            }
            #endregion
            if(propertiesNeedRecordOfCompany.Count != 0 || propertiesNeedRecordOfDealer.Count != 0) {
                this.InsertToDatabase(dealerHistory);
            }
            ObjectContext.SaveChanges();
        }

        internal object ChangeEntityType(Object value, Type convertsionType) {
            if(convertsionType.IsGenericType && convertsionType.GetGenericTypeDefinition() == typeof(Nullable<>)) {
                if(value == null || value.ToString().Length == 0) {
                    return null;
                }
                var nullableConverter = new NullableConverter(convertsionType);
                convertsionType = nullableConverter.UnderlyingType;
            }
            var result = Convert.ChangeType(value, convertsionType);
            return result;
        }
        internal List<PropertyChangeInfo> GetPropertiesNeedRecordInRangeForUpdate<T>(T originalEntity, T nowEntity, string[] propertyNameNeedCompareInRange) where T : class {
            if(propertyNameNeedCompareInRange != null && propertyNameNeedCompareInRange.Length == 0) {
                //传空直接不比较
                return new List<PropertyChangeInfo>();
            }
            var propertiesOfEntity = typeof(T).GetProperties();
            List<PropertyInfo> propertiesNeedCompare = null;
            #region 判断哪些字段需要比较
            if(propertyNameNeedCompareInRange == null) {
                //不设定的话就按此逻辑
                var listOfPropertyNameIngoreCompare = new List<string> {
                    "CREATORID",
                    "CREATORNAME",
                    "CREATETIME",
                    "MODIFIERID",
                    "MODIFIERNAME",
                    "ROWVERSION",
                    "MODIFYTIME",
                    "ID"
                };
                if(propertiesOfEntity.Length > 0) {
                    var listOfPropertyIngoreCompareNotValueTypeOrString = propertiesOfEntity.Where(r => r.PropertyType.FullName == null || !(r.PropertyType.IsValueType && r.PropertyType.FullName.ToUpper() == "SYSTEM.STRING")).ToList();
                    var listOfPropertyNameIngoreCompareNotValueType = listOfPropertyIngoreCompareNotValueTypeOrString.Select(r => r.Name.ToUpper()).ToList();
                    listOfPropertyNameIngoreCompare = (listOfPropertyNameIngoreCompare.Union(listOfPropertyNameIngoreCompareNotValueType)).ToList();
                }
                //先找所有不要的再排除掉不要的
                propertiesNeedCompare = propertiesOfEntity.Where(r => listOfPropertyNameIngoreCompare.All(v => v != r.Name.ToUpper())).ToList();
            }
            if(propertyNameNeedCompareInRange != null && propertyNameNeedCompareInRange.Length > 0) {
                //设定的话就按此逻辑
                var propertyNameNeedCompareInRangeUpperCase = propertyNameNeedCompareInRange.Select(r => r.ToUpper()).ToList();
                var properiesNeedCompareButNotFound = propertyNameNeedCompareInRangeUpperCase.Where(r => propertiesOfEntity.All(v => v.Name.ToUpper() != r)).ToList();
                if(properiesNeedCompareButNotFound.Count > 0) {
                    throw new Exception("属性" + string.Join(",", properiesNeedCompareButNotFound) + "在类" + typeof(T).Name + "中未找到");
                }
                propertiesNeedCompare = propertiesOfEntity.Where(r => propertyNameNeedCompareInRangeUpperCase.Any(v => v == r.Name.ToUpper())).ToList();
            }
            #endregion
            if(propertiesNeedCompare == null) {
                return new List<PropertyChangeInfo>();
            }
            var listOfPropertyChangeInfoNeedRecord = new List<PropertyChangeInfo>();
            foreach(var propertyInfo in propertiesNeedCompare) {
                var objectArray = new object[] {
                };
                var objectValueOfPropertyInfoOriginal = propertyInfo.GetValue(originalEntity, objectArray);
                var objectValueOfPropertyInfoNow = propertyInfo.GetValue(nowEntity, objectArray);
                if(objectValueOfPropertyInfoOriginal == null && objectValueOfPropertyInfoNow == null) {
                    continue;
                }
                if(objectValueOfPropertyInfoOriginal == null || objectValueOfPropertyInfoNow == null) {
                    listOfPropertyChangeInfoNeedRecord.Add(new PropertyChangeInfo(propertyInfo.Name.ToUpper(), objectValueOfPropertyInfoOriginal, objectValueOfPropertyInfoNow));
                    continue;
                }
                Debug.Assert(propertyInfo.PropertyType.FullName != null, "propertyInfo.PropertyType.FullName != null");
                if(propertyInfo.PropertyType.FullName.ToUpper() == "SYSTEM.STRING") {
                    var valueOfOriginal = objectValueOfPropertyInfoOriginal.ToString();
                    var valueOfNow = objectValueOfPropertyInfoNow.ToString();
                    if(valueOfOriginal != valueOfNow) {
                        listOfPropertyChangeInfoNeedRecord.Add(new PropertyChangeInfo(propertyInfo.Name.ToUpper(), objectValueOfPropertyInfoOriginal, objectValueOfPropertyInfoNow));
                        continue;
                    }
                }else {
                    var valueOfOriginal = (ChangeEntityType(objectValueOfPropertyInfoOriginal, propertyInfo.PropertyType)).ToString();
                    var valueOfNow = (ChangeEntityType(objectValueOfPropertyInfoNow, propertyInfo.PropertyType)).ToString();
                    if(valueOfOriginal != valueOfNow) {
                        listOfPropertyChangeInfoNeedRecord.Add(new PropertyChangeInfo(propertyInfo.Name.ToUpper(), objectValueOfPropertyInfoOriginal, objectValueOfPropertyInfoNow));
                    }
                }
            }
            return listOfPropertyChangeInfoNeedRecord;
        }
        internal List<PropertyChangeInfo> GetPropertiesNeedRecordInRangeForInsert<T>(T nowEntity, string[] propertyNameNeedCompareInRange) where T : class {
            if(propertyNameNeedCompareInRange != null && propertyNameNeedCompareInRange.Length == 0) {
                return new List<PropertyChangeInfo>();
            }
            var propertiesOfEntity = typeof(T).GetProperties();
            List<PropertyInfo> propertiesNeedCompare = null;
            #region 判断哪些字段需要比较
            if(propertyNameNeedCompareInRange == null) {
                //不设定的话就按此逻辑
                var listOfPropertyNameIngoreCompare = new List<string> {
                    "CREATORID",
                    "CREATORNAME",
                    "CREATETIME",
                    "MODIFIERID",
                    "MODIFIERNAME",
                    "ROWVERSION",
                    "MODIFYTIME",
                    "ID"
                };
                if(propertiesOfEntity.Length > 0) {
                    var listOfPropertyIngoreCompareNotValueTypeOrString = propertiesOfEntity.Where(r => r.PropertyType.FullName == null || !(r.PropertyType.IsValueType && r.PropertyType.FullName.ToUpper() == "SYSTEM.STRING")).ToList();
                    var listOfPropertyNameIngoreCompareNotValueType = listOfPropertyIngoreCompareNotValueTypeOrString.Select(r => r.Name.ToUpper()).ToList();
                    listOfPropertyNameIngoreCompare = (listOfPropertyNameIngoreCompare.Union(listOfPropertyNameIngoreCompareNotValueType)).ToList();
                }
                //先找所有不要的再排除掉不要的
                propertiesNeedCompare = propertiesOfEntity.Where(r => listOfPropertyNameIngoreCompare.All(v => v != r.Name.ToUpper())).ToList();
            }
            if(propertyNameNeedCompareInRange != null && propertyNameNeedCompareInRange.Length > 0) {
                //设定的话就按此逻辑
                var propertyNameNeedCompareInRangeUpperCase = propertyNameNeedCompareInRange.Select(r => r.ToUpper()).ToList();
                var properiesNeedCompareButNotFound = propertyNameNeedCompareInRangeUpperCase.Where(r => propertiesOfEntity.All(v => v.Name.ToUpper() != r)).ToList();
                if(properiesNeedCompareButNotFound.Count > 0) {
                    throw new Exception("属性" + string.Join(",", properiesNeedCompareButNotFound) + "在类" + typeof(T).Name + "中未找到");
                }
                propertiesNeedCompare = propertiesOfEntity.Where(r => propertyNameNeedCompareInRangeUpperCase.Any(v => v == r.Name.ToUpper())).ToList();
            }
            #endregion
            if(propertiesNeedCompare == null) {
                return new List<PropertyChangeInfo>();
            }
            var listOfPropertyChangeInfoNeedRecord = new List<PropertyChangeInfo>();
            foreach(var propertyInfo in propertiesNeedCompare) {
                var objectArray = new Object[] {
                };
                var objectValueOfPropertyInfoNow = propertyInfo.GetValue(nowEntity, objectArray);
                listOfPropertyChangeInfoNeedRecord.Add(new PropertyChangeInfo(propertyInfo.Name.ToUpper(), null, objectValueOfPropertyInfoNow));
            }
            return listOfPropertyChangeInfoNeedRecord;
        }
        internal T SetValuesToEntity<T>(T entity, Dictionary<string, PropertyChangeInfo> propertyRelation, string operateFlag) {
            var propertiesOfEntity = typeof(T).GetProperties();
            if(operateFlag == "UPDATE") {
                foreach(var key in propertyRelation.Keys) {
                    var propertyChangeInfo = propertyRelation[key];
                    var tempPropertyBeforeChange = propertiesOfEntity.FirstOrDefault(r => r.Name.ToUpper() == propertyChangeInfo.PropertyName);
                    var tempPropertyAfterChange = propertiesOfEntity.FirstOrDefault(r => r.Name.ToUpper() == key);
                    if(tempPropertyBeforeChange == null) {
                        throw new ValidationException("属性" + propertyChangeInfo.PropertyName + "不存在于类" + typeof(T).Name + "中");
                    }
                    if(tempPropertyAfterChange == null) {
                        throw new ValidationException("属性" + key + "不存在于类" + typeof(T).Name + "中");
                    }
                    tempPropertyBeforeChange.SetValue(entity, propertyChangeInfo.BeforeChangeValue, new object[] {
                    });
                    tempPropertyAfterChange.SetValue(entity, propertyChangeInfo.AfterChangeValue, new object[] {
                    });
                }
            }else if(operateFlag == "INSERT") {
                foreach(var key in propertyRelation.Keys) {
                    var propertyChangeInfo = propertyRelation[key];
                    var tempPropertyAfterChange = propertiesOfEntity.FirstOrDefault(r => r.Name.ToUpper() == key);
                    if(tempPropertyAfterChange == null) {
                        throw new ValidationException("属性" + key + "不存在于类" + typeof(T).Name + "中");
                    }
                    tempPropertyAfterChange.SetValue(entity, propertyChangeInfo.AfterChangeValue, new object[] {
                    });
                }
            }else {
                throw new ValidationException("操作标识只能是INSERT或UPDATE");
            }
            return entity;
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:09:49
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 生成企业开票信息(CompanyInvoiceInfo companyInvoiceInfo) {
            new CompanyInvoiceInfoAch(this).生成企业开票信息(companyInvoiceInfo);
        }

        [Update(UsingCustomMethod = true)]
        public void 修改企业开票信息(CompanyInvoiceInfo companyInvoiceInfo) {
            new CompanyInvoiceInfoAch(this).修改企业开票信息(companyInvoiceInfo);
        }

        [Invoke]
        public void 生成经销商基本信息集中(Company company, Dealer dealer, DealerServiceExt dealerServiceExt, CompanyInvoiceInfo companyInvoiceInfo, DealerMobileNumberList[] dealerMobileNumberLists) {
            new CompanyInvoiceInfoAch(this).生成经销商基本信息集中(company,dealer,dealerServiceExt,companyInvoiceInfo,dealerMobileNumberLists);
        }

        [Invoke]
        public void 修改经销商基本信息集中(Company company, Dealer dealer, DealerServiceExt dealerServiceExt, CompanyInvoiceInfo companyInvoiceInfo, DealerMobileNumberList[] dealerMobileNumberLists) {
            new CompanyInvoiceInfoAch(this).修改经销商基本信息集中(company,dealer,dealerServiceExt,companyInvoiceInfo,dealerMobileNumberLists);
        }
    }


}
