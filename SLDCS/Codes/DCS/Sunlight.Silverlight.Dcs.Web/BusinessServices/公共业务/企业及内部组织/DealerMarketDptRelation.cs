﻿using System.Linq;
using System.ServiceModel.DomainServices.Server;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class DealerMarketDptRelationAch : DcsSerivceAchieveBase {
        public DealerMarketDptRelationAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 新增营销分公司市场部及经销商关系(DealerMarketDptRelation dealerMarketDptRelation, int businessType) {
            var conflictRelations = ObjectContext.DealerMarketDptRelations.Where(v => v.DealerId == dealerMarketDptRelation.DealerId && ObjectContext.MarketingDepartments.Any(market => market.Id == v.MarketId && market.BranchId == dealerMarketDptRelation.BranchId && market.BusinessType == businessType)).ToArray();
            foreach(var relation in conflictRelations) {
                UpdateToDatabase(relation);
                relation.Status = (int)DcsBaseDataStatus.作废;
                this.UpdateDealerMarketDptRelationValidate(relation);
            }
        }
        public IQueryable<DealerMarketDptRelation> GetDealerMarketDptRelationWithDealer() {
            return ObjectContext.DealerMarketDptRelations.Include("Dealer").OrderBy(e => e.Id);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:09:49
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 新增营销分公司市场部及经销商关系(DealerMarketDptRelation dealerMarketDptRelation, int businessType) {
            new DealerMarketDptRelationAch(this).新增营销分公司市场部及经销商关系(dealerMarketDptRelation,businessType);
        }

        public IQueryable<DealerMarketDptRelation> GetDealerMarketDptRelationWithDealer() {
            return new DealerMarketDptRelationAch(this).GetDealerMarketDptRelationWithDealer();
        }
    }
}
