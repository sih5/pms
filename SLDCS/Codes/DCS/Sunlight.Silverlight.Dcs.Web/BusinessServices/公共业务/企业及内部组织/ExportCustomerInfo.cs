﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class ExportCustomerInfosAch : DcsSerivceAchieveBase {
        public ExportCustomerInfosAch(DcsDomainService domainService)
            : base(domainService) {
        }

        public void 作废出口客户信息(ExportCustomerInfo exportCustomerInfo) {
            var dbExportCustomerInfo = this.ObjectContext.ExportCustomerInfoes.Where(r => r.Id == exportCustomerInfo.Id && r.Status == (int)DcsMasterDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbExportCustomerInfo == null)
                throw new ValidationException(ErrorStrings.ExportCustomerInfo_Validation1);
            UpdateToDatabase(dbExportCustomerInfo);
            dbExportCustomerInfo.Status = (int)DcsMasterDataStatus.作废;
            new ExportCustomerInfoAch(this.DomainService).UpdateExportCustomerInfoValidate(dbExportCustomerInfo);
            var userInfo = Utils.GetCurrentUserInfo();
            var company = dbExportCustomerInfo.Company;
            UpdateToDatabase(company);
            company.Status = (int)DcsMasterDataStatus.作废;
            company.ModifierId = userInfo.Id;
            company.ModifierName = userInfo.Name;
            company.ModifyTime = DateTime.Now;
            new CompanyAch(this.DomainService).UpdateCompanyValidate(company);
        }
    }

    partial class DcsDomainService {
        [Update(UsingCustomMethod = true)]
        public void 作废出口客户信息(ExportCustomerInfo exportCustomerInfo) {
            new ExportCustomerInfosAch(this).作废出口客户信息(exportCustomerInfo);
        }
    }
}
