﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class LogisticCompanyAch : DcsSerivceAchieveBase {
        public LogisticCompanyAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 停用物流公司(LogisticCompany logisticCompany) {
            var dblogisticCompany = ObjectContext.LogisticCompanies.Where(r => r.Id == logisticCompany.Id && r.Status == (int)DcsMasterDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dblogisticCompany == null)
                throw new ValidationException(ErrorStrings.LogisticCompany_Validation1);
            UpdateToDatabase(logisticCompany);
            logisticCompany.Status = (int)DcsMasterDataStatus.停用;
            this.UpdateLogisticCompanyValidate(logisticCompany);
        }

        public void 恢复物流公司(LogisticCompany logisticCompany) {
            var dblogisticCompany = ObjectContext.LogisticCompanies.Where(r => r.Id == logisticCompany.Id && r.Status == (int)DcsMasterDataStatus.停用).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dblogisticCompany == null)
                throw new ValidationException(ErrorStrings.LogisticCompany_Validation2);
            UpdateToDatabase(logisticCompany);
            logisticCompany.Status = (int)DcsMasterDataStatus.有效;
            this.UpdateLogisticCompanyValidate(logisticCompany);
        }

        public void 作废物流公司(LogisticCompany logisticCompany) {
            var dblogisticCompany = ObjectContext.LogisticCompanies.Where(r => r.Id == logisticCompany.Id && r.Status == (int)DcsMasterDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dblogisticCompany == null)
                throw new ValidationException(ErrorStrings.LogisticCompany_Validation3);
            UpdateToDatabase(logisticCompany);
            logisticCompany.Status = (int)DcsMasterDataStatus.作废;
            this.UpdateLogisticCompanyValidate(logisticCompany);
            var userInfo = Utils.GetCurrentUserInfo();
            var company = logisticCompany.Company;
            UpdateToDatabase(company);
            company.Status = (int)DcsMasterDataStatus.作废;
            company.ModifierId = userInfo.Id;
            company.ModifierName = userInfo.Name;
            company.ModifyTime = DateTime.Now;
            new CompanyAch(this.DomainService).UpdateCompanyValidate(company);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:09:49
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 停用物流公司(LogisticCompany logisticCompany) {
            new LogisticCompanyAch(this).停用物流公司(logisticCompany);
        }
        
        [Update(UsingCustomMethod = true)]
        public void 恢复物流公司(LogisticCompany logisticCompany) {
            new LogisticCompanyAch(this).恢复物流公司(logisticCompany);
        }
        
        [Update(UsingCustomMethod = true)]
        public void 作废物流公司(LogisticCompany logisticCompany) {
            new LogisticCompanyAch(this).作废物流公司(logisticCompany);
        }
    }
}
