﻿using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class MarketDptPersonnelRelationAch : DcsSerivceAchieveBase {
        public MarketDptPersonnelRelationAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 作废市场部与人员关系(MarketDptPersonnelRelation marketDptPersonnelRelation) {
            var dbMarketDptPersonnelRelation = ObjectContext.MarketDptPersonnelRelations.Where(r => r.Id == marketDptPersonnelRelation.Id && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbMarketDptPersonnelRelation == null)
                throw new ValidationException(ErrorStrings.MarketDptPersonnelRelation_Validation3);
            UpdateToDatabase(marketDptPersonnelRelation);
            marketDptPersonnelRelation.Status = (int)DcsBaseDataStatus.作废;
            this.UpdateMarketDptPersonnelRelationValidate(marketDptPersonnelRelation);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:09:49
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 作废市场部与人员关系(MarketDptPersonnelRelation marketDptPersonnelRelation) {
            new MarketDptPersonnelRelationAch(this).作废市场部与人员关系(marketDptPersonnelRelation);
        }
    }
}
