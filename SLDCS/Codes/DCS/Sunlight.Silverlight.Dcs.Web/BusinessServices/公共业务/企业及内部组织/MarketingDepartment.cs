﻿using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class MarketingDepartmentAch : DcsSerivceAchieveBase {
        public MarketingDepartmentAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 作废营销分公司市场部(MarketingDepartment marketingDepartment) {
            var dbmarketingDepartment = ObjectContext.MarketingDepartments.Where(r => r.Id == marketingDepartment.Id && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbmarketingDepartment == null)
                throw new ValidationException(ErrorStrings.MarketingDepartment_Validation4);
            var dbdealerMarketDptRelations = ObjectContext.DealerMarketDptRelations.Where(r => r.MarketId == marketingDepartment.Id);
            foreach(var dealerMarketDptRelation in dbdealerMarketDptRelations) {
                UpdateToDatabase(dealerMarketDptRelation);
                dealerMarketDptRelation.Status = (int)DcsBaseDataStatus.作废;
                new DealerMarketDptRelationAch(this.DomainService).UpdateDealerMarketDptRelationValidate(dealerMarketDptRelation);
            }
            UpdateToDatabase(marketingDepartment);
            marketingDepartment.Status = (int)DcsBaseDataStatus.作废;
            this.UpdateMarketingDepartmentValidate(marketingDepartment);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:09:49
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 作废营销分公司市场部(MarketingDepartment marketingDepartment) {
            new MarketingDepartmentAch(this).作废营销分公司市场部(marketingDepartment);
        }
    }
}