﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web
{
    partial class PersonnelAch : DcsSerivceAchieveBase
    {
        public PersonnelAch(DcsDomainService domainService)
            : base(domainService)
        {
        }

        [Query(HasSideEffects = true)]
        public IEnumerable<Personnel> 查询本企业人员信息()
        {
            var user = Utils.GetCurrentUserInfo();
            var result = ObjectContext.Personnels.Where(r => r.CorporationId == user.EnterpriseId).ToArray();
            return result.OrderBy(r => r.Id);
        }
    }
    partial class DcsDomainService
    {// 已经使用工具进行处理 2017/4/29 12:12:34
        //原分布类的函数全部转移到Ach                                                               
        public IEnumerable<Personnel> 查询本企业人员信息()
        {
            return new PersonnelAch(this).查询本企业人员信息();
        }
    }
}
