﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class PersonnelSupplierRelationAch : DcsSerivceAchieveBase {
        public PersonnelSupplierRelationAch(DcsDomainService domainService)
            : base(domainService) {
        }
        public void 新增人员与供应商关系(PersonnelSupplierRelation personnelSupplierRelation)
        {
            var idAndSaleCenter = ObjectContext.PersonnelSupplierRelations.Where(v => v.PartsSalesCategoryId == personnelSupplierRelation.PartsSalesCategoryId && v.PersonId == personnelSupplierRelation.PersonId && v.SupplierId == personnelSupplierRelation.SupplierId && v.Id != personnelSupplierRelation.Id && v.Status == (int)DcsBaseDataStatus.有效).FirstOrDefault();
            if (idAndSaleCenter != null)
                throw new ValidationException(ErrorStrings.PersonnelSupplierRelation_Validation1);
            var userInfo = Utils.GetCurrentUserInfo();
            personnelSupplierRelation.CreatorId = userInfo.Id;
            personnelSupplierRelation.CreatorName = userInfo.Name;
            personnelSupplierRelation.CreateTime = DateTime.Now;
            personnelSupplierRelation.Status = (int)DcsBaseDataStatus.有效;
            InsertToDatabase(personnelSupplierRelation);
            InsertPersonnelSupplierRelationValidate(personnelSupplierRelation);

        }
       
        public void 修改人员与供应商关系(PersonnelSupplierRelation personnelSupplierRelation)
        {
            var idAndSaleCenter = ObjectContext.PersonnelSupplierRelations.Where(v => v.PartsSalesCategoryId == personnelSupplierRelation.PartsSalesCategoryId && v.PersonId == personnelSupplierRelation.PersonId && v.SupplierId == personnelSupplierRelation.SupplierId && v.Id != personnelSupplierRelation.Id && v.Status == (int)DcsBaseDataStatus.有效).FirstOrDefault();
            if (idAndSaleCenter != null)
                throw new ValidationException(ErrorStrings.PersonnelSupplierRelation_Validation1);
            var dbPersonnelSupplierRelation = ObjectContext.PersonnelSupplierRelations.Where(r => r.Id == personnelSupplierRelation.Id && (r.Status == (int)DcsBaseDataStatus.有效)).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if (dbPersonnelSupplierRelation == null)
                throw new ValidationException(ErrorStrings.PersonnelSupplierRelation_Validation2);
            UpdateToDatabase(personnelSupplierRelation);
            var userInfo = Utils.GetCurrentUserInfo();
            personnelSupplierRelation.ModifierId = userInfo.Id;
            personnelSupplierRelation.ModifierName = userInfo.Name;
            personnelSupplierRelation.ModifyTime = DateTime.Now;
            personnelSupplierRelation.Status = (int)DcsBaseDataStatus.有效;
            UpdatePersonnelSupplierRelationValidate(personnelSupplierRelation);

        }

        public void 作废人员与供应商关系(PersonnelSupplierRelation personnelSupplierRelation)
        {
            var dbpersonnelSupplierRelations = ObjectContext.PersonnelSupplierRelations.Where(r => r.Id == personnelSupplierRelation.Id && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if (dbpersonnelSupplierRelations == null)
                throw new ValidationException(ErrorStrings.PersonnelSupplierRelation_Validation3);
            UpdateToDatabase(personnelSupplierRelation);
            var userInfo = Utils.GetCurrentUserInfo();
            personnelSupplierRelation.AbandonerId = userInfo.Id;
            personnelSupplierRelation.AbandonerName = userInfo.Name;
            personnelSupplierRelation.AbandonTime = DateTime.Now;
            personnelSupplierRelation.Status = (int)DcsBaseDataStatus.作废;
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:09:49
        //原分布类的函数全部转移到Ach                                                                

        [Update(UsingCustomMethod = true)]
        public void 新增人员与供应商关系(PersonnelSupplierRelation personnelSupplierRelation)
        {
            new PersonnelSupplierRelationAch(this).新增人员与供应商关系(personnelSupplierRelation);
        }                                                           

        [Update(UsingCustomMethod = true)]
        public void 修改人员与供应商关系(PersonnelSupplierRelation personnelSupplierRelation)
        {
            new PersonnelSupplierRelationAch(this).修改人员与供应商关系(personnelSupplierRelation);
        }
        [Update(UsingCustomMethod = true)]
        public void 作废人员与供应商关系(PersonnelSupplierRelation personnelSupplierRelation)
        {
            new PersonnelSupplierRelationAch(this).作废人员与供应商关系(personnelSupplierRelation);
        }
    }
}
