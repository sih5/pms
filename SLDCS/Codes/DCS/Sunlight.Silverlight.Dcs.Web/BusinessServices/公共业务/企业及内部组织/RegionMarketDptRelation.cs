﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class RegionMarketDptRelationAch : DcsSerivceAchieveBase {
        public RegionMarketDptRelationAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 作废大区与市场部关系(RegionMarketDptRelation regionMarketDptRelation) {
            var dbRegionMarketDptRelation = ObjectContext.RegionMarketDptRelations.Where(r => r.Id == regionMarketDptRelation.Id && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbRegionMarketDptRelation == null)
                throw new ValidationException(ErrorStrings.RegionMarketDptRelation_Validation3);
            UpdateToDatabase(regionMarketDptRelation);
            var userInfo = Utils.GetCurrentUserInfo();
            regionMarketDptRelation.AbandonerId = userInfo.Id;
            regionMarketDptRelation.AbandonerName = userInfo.Name;
            regionMarketDptRelation.AbandonTime = DateTime.Now;
            regionMarketDptRelation.Status = (int)DcsBaseDataStatus.作废;
            UpdateRegionMarketDptRelation(regionMarketDptRelation);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:09:49
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 作废大区与市场部关系(RegionMarketDptRelation regionMarketDptRelation) {
            new RegionMarketDptRelationAch(this).作废大区与市场部关系(regionMarketDptRelation);
        }
    }
}
