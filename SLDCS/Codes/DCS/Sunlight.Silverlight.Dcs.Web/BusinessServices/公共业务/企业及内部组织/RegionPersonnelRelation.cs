﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class RegionPersonnelRelationAch : DcsSerivceAchieveBase {
        public RegionPersonnelRelationAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 作废大区与人员关系(RegionPersonnelRelation regionPersonnelRelation) {
            var dbRegionPersonnelRelation = ObjectContext.RegionPersonnelRelations.Where(r => r.Id == regionPersonnelRelation.Id && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbRegionPersonnelRelation == null)
                throw new ValidationException(ErrorStrings.RegionPersonnelRelation_Validation3);
            UpdateToDatabase(regionPersonnelRelation);
            var userInfo = Utils.GetCurrentUserInfo();
            regionPersonnelRelation.AbandonerId = userInfo.Id;
            regionPersonnelRelation.AbandonerName = userInfo.Name;
            regionPersonnelRelation.AbandonTime = DateTime.Now;
            regionPersonnelRelation.Status = (int)DcsBaseDataStatus.作废;
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:09:49
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 作废大区与人员关系(RegionPersonnelRelation regionPersonnelRelation) {
            new RegionPersonnelRelationAch(this).作废大区与人员关系(regionPersonnelRelation);
        }
    }
}
