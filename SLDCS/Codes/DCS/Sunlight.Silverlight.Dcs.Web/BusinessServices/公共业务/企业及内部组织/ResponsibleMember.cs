﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;


namespace Sunlight.Silverlight.Dcs.Web {
    partial class ResponsibleMemberAch : DcsSerivceAchieveBase {
        public void 作废责任组人员维护(ResponsibleMember responsibleMember) {
            var dbagency = ObjectContext.ResponsibleMembers.Where(r => r.Id == responsibleMember.Id && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbagency == null)
                throw new ValidationException("只能作废有效的数据");
            UpdateToDatabase(responsibleMember);
            var userInfo = Utils.GetCurrentUserInfo();
            responsibleMember.Status = (int)DcsBaseDataStatus.作废;
            responsibleMember.AbandonerId = userInfo.Id;
            responsibleMember.AbandonerName = userInfo.Name;
            responsibleMember.AbandonTime = DateTime.Now;
            this.UpdateResponsibleMemberValidate(responsibleMember); ;
        }
    }
    partial class DcsDomainService {
        // 已经使用工具进行处理 2017/4/29 12:12:34 
        // 原分布类的函数全部转移到Ach                                                                
        public void 作废责任组人员维护(ResponsibleMember responsibleMember) {
            new ResponsibleMemberAch(this).作废责任组人员维护(responsibleMember);
        }
    }
}
