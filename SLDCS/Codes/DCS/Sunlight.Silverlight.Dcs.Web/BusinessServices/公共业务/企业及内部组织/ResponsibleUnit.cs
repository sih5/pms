﻿using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class ResponsibleUnitAch : DcsSerivceAchieveBase {
        public ResponsibleUnitAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 停用责任单位(ResponsibleUnit responsibleUnit) {
            var dbresponsibleUnit = ObjectContext.ResponsibleUnits.Where(r => r.Id == responsibleUnit.Id && r.Status == (int)DcsMasterDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbresponsibleUnit == null)
                throw new ValidationException(ErrorStrings.ResponsibleUnit_Validation1);
            UpdateToDatabase(responsibleUnit);
            responsibleUnit.Status = (int)DcsMasterDataStatus.停用;
            this.UpdateResponsibleUnitValidate(responsibleUnit);
        }

        public void 恢复责任单位(ResponsibleUnit responsibleUnit) {
            var dbresponsibleUnit = ObjectContext.ResponsibleUnits.Where(r => r.Id == responsibleUnit.Id && r.Status == (int)DcsMasterDataStatus.停用).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbresponsibleUnit == null)
                throw new ValidationException(ErrorStrings.ResponsibleUnit_Validation2);
            UpdateToDatabase(responsibleUnit);
            responsibleUnit.Status = (int)DcsMasterDataStatus.有效;
            this.UpdateResponsibleUnitValidate(responsibleUnit);
        }

        public void 作废责任单位(ResponsibleUnit responsibleUnit) {
            var dbresponsibleUnit = ObjectContext.ResponsibleUnits.Where(r => r.Id == responsibleUnit.Id && r.Status == (int)DcsMasterDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbresponsibleUnit == null)
                throw new ValidationException(ErrorStrings.ResponsibleUnit_Validation3);
            UpdateToDatabase(responsibleUnit);
            responsibleUnit.Status = (int)DcsMasterDataStatus.作废;
            this.UpdateResponsibleUnitValidate(responsibleUnit);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:09:49
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 停用责任单位(ResponsibleUnit responsibleUnit) {
            new ResponsibleUnitAch(this).停用责任单位(responsibleUnit);
        }
        
        [Update(UsingCustomMethod = true)]
        public void 恢复责任单位(ResponsibleUnit responsibleUnit) {
            new ResponsibleUnitAch(this).恢复责任单位(responsibleUnit);
        }
        
        [Update(UsingCustomMethod = true)]
        public void 作废责任单位(ResponsibleUnit responsibleUnit) {
            new ResponsibleUnitAch(this).作废责任单位(responsibleUnit);
        }
    }
}
