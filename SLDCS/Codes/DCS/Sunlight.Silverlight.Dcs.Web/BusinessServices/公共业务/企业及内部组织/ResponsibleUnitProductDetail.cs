﻿using System.Linq;
using System.ServiceModel.DomainServices.Server;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class ResponsibleUnitProductDetailAch : DcsSerivceAchieveBase {
        public ResponsibleUnitProductDetailAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 新增责任单位产品关系(ResponsibleUnitProductDetail responsibleUnitProductDetail) {
            var responsibleUnitAffiProducts = ObjectContext.ResponsibleUnitAffiProducts.Where(r => r.ProductId == responsibleUnitProductDetail.ProductId).ToArray();
            foreach(var responsibleUnitAffiProduct in responsibleUnitAffiProducts) {
                DeleteFromDatabase(responsibleUnitAffiProduct);
            }
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:09:49
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 新增责任单位产品关系(ResponsibleUnitProductDetail responsibleUnitProductDetail) {
            new ResponsibleUnitProductDetailAch(this).新增责任单位产品关系(responsibleUnitProductDetail);
        }
    }
}
