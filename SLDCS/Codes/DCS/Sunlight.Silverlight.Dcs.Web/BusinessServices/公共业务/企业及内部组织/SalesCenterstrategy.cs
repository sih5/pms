﻿using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class SalesCenterstrategyAch : DcsSerivceAchieveBase {
        public SalesCenterstrategyAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 作废销售中心策略(SalesCenterstrategy salesCenterstrategy) {
            var dbSalesRegion = ObjectContext.SalesCenterstrategies.Where(r => r.Id == salesCenterstrategy.Id && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbSalesRegion == null)
                throw new ValidationException(ErrorStrings.SalesCenterstrategy_Validation2);
            salesCenterstrategy.Status = (int)DcsBaseDataStatus.作废;
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:09:49
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 作废销售中心策略(SalesCenterstrategy salesCenterstrategy) {
            new SalesCenterstrategyAch(this).作废销售中心策略(salesCenterstrategy);
        }
    }
}
