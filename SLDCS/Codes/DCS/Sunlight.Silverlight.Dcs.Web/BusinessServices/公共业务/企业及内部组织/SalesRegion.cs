﻿using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class SalesRegionAch : DcsSerivceAchieveBase {
        public SalesRegionAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 作废销售大区(SalesRegion salesRegion) {
            var dbSalesRegion = ObjectContext.SalesRegions.Where(r => r.Id == salesRegion.Id && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbSalesRegion == null)
                throw new ValidationException(ErrorStrings.SalesRegion_Validation3);
            UpdateToDatabase(salesRegion);
            salesRegion.Status = (int)DcsBaseDataStatus.作废;
            this.UpdateSalesRegionValidate(salesRegion);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:09:49
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 作废销售大区(SalesRegion salesRegion) {
            new SalesRegionAch(this).作废销售大区(salesRegion);
        }
    }
}
