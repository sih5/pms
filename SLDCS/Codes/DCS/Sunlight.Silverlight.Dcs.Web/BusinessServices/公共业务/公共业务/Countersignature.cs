﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class CountersignatureAch : DcsSerivceAchieveBase {
        public CountersignatureAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 生成会签信息(Countersignature countersignature) {
            var userInfo = Utils.GetCurrentUserInfo();
            switch(countersignature.SourceType) {
                case (int)DcsCountersignatureApplyType.维修索赔申请:
                case (int)DcsCountersignatureApplyType.保养索赔申请:
                //case (int)DcsCountersignatureApplyType.服务活动索赔申请:
                //    var repairClaimApplication = ObjectContext.RepairClaimApplications.SingleOrDefault(r => r.Id == countersignature.SourceId);
                //    if(repairClaimApplication != null) {
                //        repairClaimApplication.Status = (int)DcsRepairClaimApplication.会签中;
                //        repairClaimApplication.InitialApproverId = userInfo.Id;
                //        repairClaimApplication.InitialApproverName = userInfo.Name;
                //        repairClaimApplication.InitialApproveTime = DateTime.Now;
                //        repairClaimApplication.ApproveCommentHistory = string.Format("审批人：{0}{1}{0}审批时间：{0}{2}{0}审批意见：{0}{3}", Environment.NewLine, repairClaimApplication.InitialApproverName, repairClaimApplication.InitialApproveTime, repairClaimApplication.InitialApproveComment);
                //        UpdateToDatabase(repairClaimApplication);
                //    }
                //    break;
                //case (int)DcsCountersignatureApplyType.外出索赔申请:
                //    var serviceTripClaimApplication = ObjectContext.ServiceTripClaimApplications.SingleOrDefault(r => r.Id == countersignature.SourceId);
                //    if(serviceTripClaimApplication != null) {
                //        serviceTripClaimApplication.Status = (int)DcsServiceTripClaimApplicationStatus.会签中;
                //        serviceTripClaimApplication.InitialApproverId = userInfo.Id;
                //        serviceTripClaimApplication.InitialApproverName = userInfo.Name;
                //        serviceTripClaimApplication.InitialApproveTime = DateTime.Now;
                //        serviceTripClaimApplication.ApproveCommentHistory = string.Format("审批人：{0}{1}{0}审批时间：{0}{2}{0}审批意见：{0}{3}", Environment.NewLine, serviceTripClaimApplication.InitialApproverName, serviceTripClaimApplication.InitialApproveTime, serviceTripClaimApplication.InitialApproveComment);
                //        UpdateToDatabase(serviceTripClaimApplication);
                //    }
                //    break;
                case (int)DcsCountersignatureApplyType.AB质量信息快报:
                    var marketAbQualityInformation = ObjectContext.MarketABQualityInformations.SingleOrDefault(r => r.Id == countersignature.SourceId);
                    if(marketAbQualityInformation != null) {
                        marketAbQualityInformation.Status = (int)DcsABQualityInformationStatus.会签中;
                        marketAbQualityInformation.InitialApproverId = userInfo.Id;
                        marketAbQualityInformation.InitialApproverName = userInfo.Name;
                        marketAbQualityInformation.InitialApproverTime = DateTime.Now;
                        marketAbQualityInformation.ApproveCommentHistory = string.Format("审批人：{0}{1}{0}审批时间：{0}{2}{0}审批意见：{0}{3}", Environment.NewLine, marketAbQualityInformation.InitialApproverName, marketAbQualityInformation.InitialApproverTime, marketAbQualityInformation.InitialApprovertComment);        
                        UpdateToDatabase(marketAbQualityInformation);
                    }
                    break;
            }
        }


        public void 会签申请单(Countersignature countersignature) {
            var dbCountersignature = ObjectContext.Countersignatures.Where(r => r.Id == countersignature.Id && r.Status == (int)DcsCountersignatureStatus.新建).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbCountersignature == null) {
                throw new ValidationException("只能操作新建状态的单据");
            }
            var countersignatureDetails = countersignature.CountersignatureDetails;
            if(countersignature.Tpye == (int)DcsCountersignatureType.串行) {
                var nextNumber = countersignature.NextNumber;
                var numbers = countersignatureDetails.Select(r => r.SerialNumber).ToArray();
                var maxNumber = numbers.Max(r => r);
                if(nextNumber >= maxNumber) {
                    countersignature.Status = (int)DcsCountersignatureStatus.会签完成;
                    switch(countersignature.SourceType) {
                        case (int)DcsCountersignatureApplyType.维修索赔申请:
                        case (int)DcsCountersignatureApplyType.保养索赔申请:
                        //case (int)DcsCountersignatureApplyType.服务活动索赔申请:
                        //    var repairClaimApplication = ObjectContext.RepairClaimApplications.SingleOrDefault(r => r.Id == countersignature.SourceId);
                        //    if(repairClaimApplication != null) {
                        //        repairClaimApplication.Status = (int)DcsRepairClaimApplication.会签完成;
                        //        UpdateToDatabase(repairClaimApplication);
                        //    }
                        //    break;
                        //case (int)DcsCountersignatureApplyType.外出索赔申请:
                        //    var serviceTripClaimApplication = ObjectContext.ServiceTripClaimApplications.SingleOrDefault(r => r.Id == countersignature.SourceId);
                        //    if(serviceTripClaimApplication != null) {
                        //        serviceTripClaimApplication.Status = (int)DcsServiceTripClaimApplicationStatus.会签完成;
                        //        UpdateToDatabase(serviceTripClaimApplication);
                        //    }
                        //    break;
                        case (int)DcsCountersignatureApplyType.AB质量信息快报:
                            var marketAbQualityInformation = ObjectContext.MarketABQualityInformations.SingleOrDefault(r => r.Id == countersignature.SourceId);
                            if(marketAbQualityInformation != null) {
                                marketAbQualityInformation.Status = (int)DcsABQualityInformationStatus.会签完成;
                                UpdateToDatabase(marketAbQualityInformation);
                            }
                            break;
                    }
                }
                countersignature.NextNumber = countersignature.NextNumber + 1;
            }
            if(countersignature.Tpye == (int)DcsCountersignatureType.并行) {
                var tempCountersignatureDetails = countersignatureDetails.Where(r => r.Status == (int)DcsCountersignatureDetailStatus.未会签).ToArray();
                if(tempCountersignatureDetails.Length == 0) {
                    switch(countersignature.SourceType) {
                        case (int)DcsCountersignatureApplyType.维修索赔申请:
                        case (int)DcsCountersignatureApplyType.保养索赔申请:
                        //case (int)DcsCountersignatureApplyType.服务活动索赔申请:
                        //    var repairClaimApplication = ObjectContext.RepairClaimApplications.SingleOrDefault(r => r.Id == countersignature.SourceId);
                        //    if(repairClaimApplication != null) {
                        //        repairClaimApplication.Status = (int)DcsRepairClaimApplication.会签完成;
                        //        UpdateToDatabase(repairClaimApplication);
                        //    }
                        //    break;
                        //case (int)DcsCountersignatureApplyType.外出索赔申请:
                        //    var serviceTripClaimApplication = ObjectContext.ServiceTripClaimApplications.SingleOrDefault(r => r.Id == countersignature.SourceId);
                        //    if(serviceTripClaimApplication != null) {
                        //        serviceTripClaimApplication.Status = (int)DcsServiceTripClaimApplicationStatus.会签完成;
                        //        UpdateToDatabase(serviceTripClaimApplication);
                        //    }
                        //    break;
                        case (int)DcsCountersignatureApplyType.AB质量信息快报:
                            var marketAbQualityInformation = ObjectContext.MarketABQualityInformations.SingleOrDefault(r => r.Id == countersignature.SourceId);
                            if(marketAbQualityInformation != null) {
                                marketAbQualityInformation.Status = (int)DcsABQualityInformationStatus.会签完成;
                                UpdateToDatabase(marketAbQualityInformation);
                            }
                            break;
                    }
                }
            }
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:09:49
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 生成会签信息(Countersignature countersignature) {
            new CountersignatureAch(this).生成会签信息(countersignature);
        }

        [Update(UsingCustomMethod = true)]
        public void 会签申请单(Countersignature countersignature) {
            new CountersignatureAch(this).会签申请单(countersignature);
        }
    }
}