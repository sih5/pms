﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Data;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using System.Text;
using Devart.Data.Oracle;
using Microsoft.Data.Extensions;
using NPOI.SS.Formula.Functions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class ReportDownloadMsgAch : DcsSerivceAchieveBase {
        public ReportDownloadMsgAch(DcsDomainService domainService)
            : base(domainService) {
        }

        public void 更新报表下载信息(ReportDownloadMsg reportDownloadMsg) {
            var userInfo = Utils.GetCurrentUserInfo();
            var dbReportDownloadMsg = this.ObjectContext.ReportDownloadMsgs.Where(r => r.Id == reportDownloadMsg.Id && r.Status != (int)DcsDownloadStatus.作废).SetMergeOption(MergeOption.NoTracking).SingleOrDefault(); ;
            if(dbReportDownloadMsg == null)
                throw new Exception("只能更新非作废状态的报表信息");
            reportDownloadMsg.Status = (int)DcsDownloadStatus.已下载;
            if(!reportDownloadMsg.ModifierId.HasValue) {
                reportDownloadMsg.ModifierId = userInfo.Id;
                reportDownloadMsg.ModifierName = userInfo.Name;
                reportDownloadMsg.ModifyTime = DateTime.Now;
            }
            UpdateToDatabase(reportDownloadMsg);
        }
    }

    partial class DcsDomainService {
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 更新报表下载信息(ReportDownloadMsg reportDownloadMsg) {
            new ReportDownloadMsgAch(this).更新报表下载信息(reportDownloadMsg);
        }
    }
}