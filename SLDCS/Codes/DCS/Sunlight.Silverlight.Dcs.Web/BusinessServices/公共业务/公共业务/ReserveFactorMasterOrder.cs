﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using System.Text;
using FTService;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class ReserveFactorMasterOrderAch : DcsSerivceAchieveBase {
        public ReserveFactorMasterOrderAch(DcsDomainService domainService)
            : base(domainService) {
        }

        public void 新增储备系数主单(ReserveFactorMasterOrder reserveFactorMasterOrder) {
            var old = ObjectContext.ReserveFactorMasterOrders.Where(t => t.WarehouseId == reserveFactorMasterOrder.WarehouseId && t.ABCStrategyId == reserveFactorMasterOrder.ABCStrategyId && t.Status == (int)DcsBaseDataStatus.有效).FirstOrDefault();
            if(old!=null){
                throw new ValidationException("已存在相同的储备系数");
            }
            var userInfo = Utils.GetCurrentUserInfo();
            reserveFactorMasterOrder.CreateTime = DateTime.Now;
            reserveFactorMasterOrder.CreatorId = userInfo.Id;
            reserveFactorMasterOrder.CreatorName = userInfo.Name;
            InsertToDatabase(reserveFactorMasterOrder);
        }
        public void 修改储备系数主单(ReserveFactorMasterOrder reserveFactorMasterOrder) {
            var old = ObjectContext.ReserveFactorMasterOrders.Where(r => r.Id == reserveFactorMasterOrder.Id && (r.Status == (int)DcsBaseDataStatus.有效)).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(old == null) {
                throw new ValidationException("未找到有效的数据");
            }
            var userInfo = Utils.GetCurrentUserInfo();
            reserveFactorMasterOrder.ModifyTime = DateTime.Now;
            reserveFactorMasterOrder.ModifierId = userInfo.Id;
            reserveFactorMasterOrder.ModifierName = userInfo.Name;
            reserveFactorMasterOrder.ReserveFactorOrderDetails.Clear();
           
            var reserveFactorOrderDetails = ChangeSet.GetAssociatedChanges(reserveFactorMasterOrder, v => v.ReserveFactorOrderDetails);
            foreach(ReserveFactorOrderDetail sihCreditInfoDetail in reserveFactorOrderDetails) {
                //获取清单实体对象的操作类型
                switch(ChangeSet.GetChangeOperation(sihCreditInfoDetail)) {
                    //新增操作
                    case ChangeOperation.Insert:
                        InsertToDatabase(sihCreditInfoDetail);
                        break;
                    //更新操作
                    case ChangeOperation.Update:
                        UpdateToDatabase(sihCreditInfoDetail);
                        break;
                    //删除操作
                    case ChangeOperation.Delete:
                        DeleteFromDatabase(sihCreditInfoDetail);
                        break;
                }
            }
            UpdateToDatabase(reserveFactorMasterOrder);
        }
        
        public void 作废储备系数主单(ReserveFactorMasterOrder reserveFactorMasterOrder) {
            var old = ObjectContext.ReserveFactorMasterOrders.Where(r => r.Id == reserveFactorMasterOrder.Id && (r.Status == (int)DcsBaseDataStatus.有效)).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(old == null) {
                throw new ValidationException("未找到有效的数据");
            }
            var userInfo = Utils.GetCurrentUserInfo();
            reserveFactorMasterOrder.Status = (int)DcsBaseDataStatus.作废;
            reserveFactorMasterOrder.ModifyTime = DateTime.Now;
            reserveFactorMasterOrder.ModifierId = userInfo.Id;
            reserveFactorMasterOrder.ModifierName = userInfo.Name;
            reserveFactorMasterOrder.AbandonTime = DateTime.Now;
            reserveFactorMasterOrder.AbandonerId = userInfo.Id;
            reserveFactorMasterOrder.AbandonerName = userInfo.Name;
            UpdateToDatabase(reserveFactorMasterOrder);
        }
    }
    partial class DcsDomainService {
         [Update(UsingCustomMethod = true)]
        public void 新增储备系数主单(ReserveFactorMasterOrder reserveFactorMasterOrder) {
            new ReserveFactorMasterOrderAch(this).新增储备系数主单(reserveFactorMasterOrder);
        }
         [Update(UsingCustomMethod = true)]
        public void 修改储备系数主单(ReserveFactorMasterOrder reserveFactorMasterOrder) {
            new ReserveFactorMasterOrderAch(this).修改储备系数主单(reserveFactorMasterOrder);
        }
         [Update(UsingCustomMethod = true)]
        public void 作废储备系数主单(ReserveFactorMasterOrder reserveFactorMasterOrder) {
            new ReserveFactorMasterOrderAch(this).作废储备系数主单(reserveFactorMasterOrder);
        }
    }
}
