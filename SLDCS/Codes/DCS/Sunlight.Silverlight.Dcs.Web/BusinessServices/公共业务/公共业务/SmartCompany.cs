﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using System.Text;
using System.Threading.Tasks;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class SmartCompanyAch : DcsSerivceAchieveBase {
        public SmartCompanyAch(DcsDomainService domainService)
            : base(domainService) {
        }
        public void 新增智能订货企业配置(SmartCompany[] smartCompanys) {
            var userInfo = Utils.GetCurrentUserInfo();
            var allsmartCompanys = ObjectContext.SmartCompanies.Where(t => t.Status == (int)DcsBaseDataStatus.有效).ToArray();
            foreach(var item in smartCompanys) {
                if(allsmartCompanys.Any(t=>t.CompanyId==item.CompanyId)) {
                    continue;
                }
                item.CreateTime = DateTime.Now;
                item.CreatorId = userInfo.Id;
                item.CreatorName = userInfo.Name;
                InsertToDatabase(item);
            }
            ObjectContext.SaveChanges();
        }
        public void 作废智能订货企业配置(SmartCompany smartCompany) {
            var old = ObjectContext.SmartCompanies.Where(t => t.Id == smartCompany.Id).FirstOrDefault();
            if(old.Status != (int)DcsBaseDataStatus.有效) {
                throw new ValidationException("数据已作废，请重新查询");
            }
            var userInfo = Utils.GetCurrentUserInfo();
            old.Status = (int)DcsBaseDataStatus.作废;
            old.AbandonerId = userInfo.Id;
            old.AbandonerName = userInfo.Name;
            old.AbandonTime = DateTime.Now;
            UpdateToDatabase(old);
            ObjectContext.SaveChanges();
        }
    }
    partial class DcsDomainService {
        [Update(UsingCustomMethod = true)]
        public void 作废智能订货企业配置(SmartCompany smartCompany) {
            new SmartCompanyAch(this).作废智能订货企业配置(smartCompany);
        }
         [Invoke]
        public void 新增智能订货企业配置(SmartCompany[] smartCompanys) {
            new SmartCompanyAch(this).新增智能订货企业配置(smartCompanys);
        }
    }
}
