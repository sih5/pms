﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using System.Text;
using System.Threading.Tasks;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class SmartOrderCalendarAch : DcsSerivceAchieveBase {
        public SmartOrderCalendarAch(DcsDomainService domainService)
            : base(domainService) {
        }
        public void 新增智能订货日历(SmartOrderCalendar smartOrderCalendar) {
            this.InsertSmartOrderCalendar(smartOrderCalendar);
        }
        public void 修改智能订货日历(SmartOrderCalendar smartOrderCalendar) {
            this.UpdateSmartOrderCalendar(smartOrderCalendar);
            UpdateToDatabase(smartOrderCalendar);
        }
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:09:49
        //原分布类的函数全部转移到Ach                                                               
        [Update(UsingCustomMethod = true)]
        public void 新增智能订货日历(SmartOrderCalendar smartOrderCalendar) {
            new SmartOrderCalendarAch(this).新增智能订货日历(smartOrderCalendar);
        }
        [Update(UsingCustomMethod = true)]
        public void 修改智能订货日历(SmartOrderCalendar smartOrderCalendar) {
            new SmartOrderCalendarAch(this).修改智能订货日历(smartOrderCalendar);
        }
    }
}
