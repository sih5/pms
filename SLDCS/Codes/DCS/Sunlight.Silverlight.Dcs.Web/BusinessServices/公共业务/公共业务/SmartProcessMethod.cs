﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using System.Text;
using System.Threading.Tasks;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class SmartProcessMethodAch : DcsSerivceAchieveBase {
        public SmartProcessMethodAch(DcsDomainService domainService)
            : base(domainService) {
        }
        public void 新增智能订货处理方式配置(SmartProcessMethod smartProcessMethod) {
            this.InsertSmartProcessMethodValidate(smartProcessMethod);
        }
        public void 作废智能订货处理方式配置(SmartProcessMethod smartProcessMethod) {
            var oldSmartProcessMethod = ObjectContext.SmartProcessMethods.Where(t=>t.Status==(int)DcsBaseDataStatus.有效 && t.Id==smartProcessMethod.Id).FirstOrDefault();
            if(oldSmartProcessMethod==null) {
                throw new ValidationException("数据已更新，请重新查询");
            }
            var userInfo = Utils.GetCurrentUserInfo();
            oldSmartProcessMethod.Status = (int)DcsBaseDataStatus.作废;
            oldSmartProcessMethod.AbandonerId = userInfo.Id;
            oldSmartProcessMethod.AbandonerName = userInfo.Name;
            oldSmartProcessMethod.AbandonTime = DateTime.Now;
            UpdateToDatabase(oldSmartProcessMethod);
            ObjectContext.SaveChanges();
        }
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:09:49
        //原分布类的函数全部转移到Ach                                                               
        [Update(UsingCustomMethod = true)]
        public void 新增智能订货处理方式配置(SmartProcessMethod smartProcessMethod) {
            new SmartProcessMethodAch(this).新增智能订货处理方式配置(smartProcessMethod);
        }
        [Update(UsingCustomMethod = true)]
        public void 作废智能订货处理方式配置(SmartProcessMethod smartProcessMethod) {
            new SmartProcessMethodAch(this).作废智能订货处理方式配置(smartProcessMethod);
        }
    }
}
