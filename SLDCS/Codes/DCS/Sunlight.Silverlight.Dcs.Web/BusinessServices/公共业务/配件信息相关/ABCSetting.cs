﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using System.Text;
using System.Threading.Tasks;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class ABCSettingAch : DcsSerivceAchieveBase {
        public ABCSettingAch(DcsDomainService domainService)
            : base(domainService) {
        }
        public void 新增配件ABC分类设置(ABCSetting setting) {
            this.validationSetting(setting);
            var userInfo = Utils.GetCurrentUserInfo();
            setting.CreateTime = DateTime.Now;
            setting.CreatorId = userInfo.Id;
            setting.CreatorName = userInfo.Name;
            InsertToDatabase(setting);
        }
        private void validationSetting(ABCSetting setting) {
            if(setting.Type == (int)DCSABCSettingType.J) {
                return;
            }
            //查询所有的abc设置
            var allset = ObjectContext.ABCSettings.ToList();
            if ((setting.Id == 0 && allset.Any(r => r.Type == setting.Type)) || (setting.Id != 0 && allset.Where(t => t.Id != setting.Id).Any(r => r.Type == setting.Type))) {
                throw new ValidationException(ErrorStrings.ErrorString_ABCSetting_Validation1);
            }
            var except = allset.Where(r => r.Id != setting.Id);
            var before = except.Where(r => r.Type.Value < setting.Type).ToList();
            var after = except.Where(r => r.Type.Value > setting.Type).ToList();
            var beforeFreMax = before.Max(r => r.SaleFrequencyTo) ?? 0d;
            var afterFreMin = after.Min(r => r.SaleFrequencyFrom) ?? 100d;
            if (!(setting.SaleFrequencyFrom >= beforeFreMax && setting.SaleFrequencyTo <= afterFreMin)) {
                throw new ValidationException(ErrorStrings.ErrorString_ABCSetting_Validation18);
            }
            var beforeFeeMax = before.Max(r => r.SaleFeeTo) ?? 0d;
            var afterFeeMin = after.Min(r => r.SaleFeeFrom) ?? 100d;
            if (!(setting.SaleFeeFrom >= beforeFeeMax && setting.SaleFeeTo <= afterFeeMin)) {
                throw new ValidationException(ErrorStrings.ErrorString_ABCSetting_Validation19);
            }
        }

        public void 更新配件ABC分类设置(ABCSetting setting) {
            this.validationSetting(setting);
            var userInfo = Utils.GetCurrentUserInfo();
            setting.ModifyTime = DateTime.Now;
            setting.ModifierId = userInfo.Id;
            setting.ModifierName = userInfo.Name;
             
        }
        public IQueryable<ABCSetting> 查询配件ABC分类设置() {
            return ObjectContext.ABCSettings.OrderBy(r=>r.Type);
        }
        public void InsertABCSetting(ABCSetting apps) {

        }
        public void UpdateABCSetting(ABCSetting apps) {
            UpdateToDatabase(apps);
        }
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:09:49
        //原分布类的函数全部转移到Ach                                                               
        [Update(UsingCustomMethod = true)]
        public void 新增配件ABC分类设置(ABCSetting setting) {
            new ABCSettingAch(this).新增配件ABC分类设置(setting);
        }
        [Update(UsingCustomMethod = true)]
        public void 更新配件ABC分类设置(ABCSetting setting) {
            new ABCSettingAch(this).更新配件ABC分类设置(setting);
        }
        public IQueryable<ABCSetting> 查询配件ABC分类设置() {
            return new ABCSettingAch(this).查询配件ABC分类设置();
        }
        public void UpdateABCSetting(ABCSetting apps) {
            new ABCSettingAch(this).UpdateABCSetting(apps);
        }

        public void InsertABCSetting(ABCSetting apps) {
            new ABCSettingAch(this).InsertABCSetting(apps);
        }
    }
}
