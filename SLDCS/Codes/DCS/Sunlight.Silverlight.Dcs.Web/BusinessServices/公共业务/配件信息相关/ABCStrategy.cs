﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class ABCStrategyAch : DcsSerivceAchieveBase {
        public ABCStrategyAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 作废配件ABC分类策略(ABCStrategy aBCStrategy) {
            var dbaBCStrategy = ObjectContext.ABCStrategies.Where(r => r.Id == aBCStrategy.Id && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbaBCStrategy == null)
                throw new ValidationException(ErrorStrings.ABCStrategy_Validation3);
            UpdateToDatabase(aBCStrategy);
            aBCStrategy.Status = (int)DcsBaseDataStatus.作废;
            var userInfo = Utils.GetCurrentUserInfo();
            aBCStrategy.AbandonerId = userInfo.Id;
            aBCStrategy.AbandonTime = DateTime.Now;
            aBCStrategy.AbandonerName = userInfo.Name;
            this.UpdateABCStrategyValidate(aBCStrategy);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:09:49
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 作废配件ABC分类策略(ABCStrategy aBCStrategy) {
            new ABCStrategyAch(this).作废配件ABC分类策略(aBCStrategy);
        }
    }
}
