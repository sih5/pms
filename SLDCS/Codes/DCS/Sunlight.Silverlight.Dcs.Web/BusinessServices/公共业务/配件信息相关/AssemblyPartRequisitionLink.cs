﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class AssemblyPartRequisitionLinkAch : DcsSerivceAchieveBase {
        public AssemblyPartRequisitionLinkAch(DcsDomainService domainService)
            : base(domainService) {
        }

        public void 作废总成件领用关系(AssemblyPartRequisitionLink assemblyPartRequisitionLink) {
            var dbAssemblyPartRequisitionLink = ObjectContext.AssemblyPartRequisitionLinks.Where(r => r.Id == assemblyPartRequisitionLink.Id && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbAssemblyPartRequisitionLink == null)
                throw new ValidationException(ErrorStrings.AssemblyPartRequisitionLink_Validation1);
            UpdateToDatabase(dbAssemblyPartRequisitionLink);
            dbAssemblyPartRequisitionLink.Status = (int)DcsBaseDataStatus.作废;
            var userInfo = Utils.GetCurrentUserInfo();
            this.updateAssemblyPartRequisitionLinkValidate(dbAssemblyPartRequisitionLink);
        }
    
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:09:49
        //原分布类的函数全部转移到Ach    
        [Update(UsingCustomMethod = true)]                                                    
        public void 作废总成件领用关系(AssemblyPartRequisitionLink assemblyPartRequisitionLink) {
            new AssemblyPartRequisitionLinkAch(this).作废总成件领用关系(assemblyPartRequisitionLink);
        }
    }
}
