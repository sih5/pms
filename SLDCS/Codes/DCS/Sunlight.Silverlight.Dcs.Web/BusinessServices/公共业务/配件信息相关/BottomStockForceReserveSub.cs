﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using System.Text;
using System.Threading.Tasks;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class BottomStockForceReserveSubAch : DcsSerivceAchieveBase {
        public BottomStockForceReserveSubAch(DcsDomainService domainService)
            : base(domainService) {
        }
        public void 新增保底库存强制储备类别子项目(BottomStockForceReserveSub bottomStockForceReserveSub) {
            if(!bottomStockForceReserveSub.ReserveType.Equals(bottomStockForceReserveSub.SubCode.Substring(0, 1))) {
                throw new ValidationException("新增异常，请重新查询后新增");
            }
            var userInfo = Utils.GetCurrentUserInfo();
            bottomStockForceReserveSub.CreateTime = DateTime.Now;
            bottomStockForceReserveSub.CreatorId = userInfo.Id;
            bottomStockForceReserveSub.CreatorName = userInfo.Name;
            InsertToDatabase(bottomStockForceReserveSub);
        }
        public void 作废保底库存强制储备类别子项目(int[] ids) {
            var userInfo = Utils.GetCurrentUserInfo();
            foreach(var id in ids) {
                var oldType = ObjectContext.BottomStockForceReserveSubs.Where(r => r.Id == id).FirstOrDefault();
                if(oldType == null || oldType.Status == (int)DcsBaseDataStatus.作废) {
                    throw new ValidationException(ErrorStrings.Action_Abandon_Using);
                }
                oldType.ModifyTime = DateTime.Now;
                oldType.ModifierId = userInfo.Id;
                oldType.ModifierName = userInfo.Name;
                oldType.Status = (int)DcsBaseDataStatus.作废;
                UpdateToDatabase(oldType);
            }
            ObjectContext.SaveChanges();
        }
        public void 更新保底库存强制储备类别子项目(BottomStockForceReserveSub bottomStockForceReserveSub) {
            var oldType = ObjectContext.BottomStockForceReserveSubs.Where(r => r.Id == bottomStockForceReserveSub.Id).FirstOrDefault();
            if(oldType == null || oldType.Status == (int)DcsBaseDataStatus.作废) {
                throw new ValidationException(ErrorStrings.Action_Abandon_Modify);
            }
            int length = bottomStockForceReserveSub.ReserveType.Length;
            if(!bottomStockForceReserveSub.ReserveType.Equals(bottomStockForceReserveSub.SubCode.Substring(0, length))) {
                throw new ValidationException("修改异常，请重新查询后修改");
            }
            this.UpdateBottomStockForceReserveSub(bottomStockForceReserveSub);
            UpdateToDatabase(bottomStockForceReserveSub);
            ObjectContext.SaveChanges();
        }
        public void InsertBottomStockForceReserveSub(BottomStockForceReserveSub bottomStockForceReserveSub) {

        }
        public void UpdateBottomStockForceReserveSub(BottomStockForceReserveSub bottomStockForceReserveSub) {
            UpdateToDatabase(bottomStockForceReserveSub);
        }
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:09:49
        [Update(UsingCustomMethod = true)]
        public void 新增保底库存强制储备类别子项目(BottomStockForceReserveSub bottomStockForceReserveSub) {
            new BottomStockForceReserveSubAch(this).新增保底库存强制储备类别子项目(bottomStockForceReserveSub);
        }
        [Invoke(HasSideEffects = true)]
        public void 作废保底库存强制储备类别子项目(int[] ids) {
            new BottomStockForceReserveSubAch(this).作废保底库存强制储备类别子项目(ids);
        }
        public void InsertBottomStockForceReserveSub(BottomStockForceReserveSub bottomStockForceReserveSub) {
            new BottomStockForceReserveSubAch(this).InsertBottomStockForceReserveSub(bottomStockForceReserveSub);
        }
        public void UpdateBottomStockForceReserveSub(BottomStockForceReserveSub bottomStockForceReserveSub) {
            new BottomStockForceReserveSubAch(this).UpdateBottomStockForceReserveSub(bottomStockForceReserveSub);
        }
        [Update(UsingCustomMethod = true)]
        public void 更新保底库存强制储备类别子项目(BottomStockForceReserveSub bottomStockForceReserveSub) {
            new BottomStockForceReserveSubAch(this).更新保底库存强制储备类别子项目(bottomStockForceReserveSub);
        }
    }
}