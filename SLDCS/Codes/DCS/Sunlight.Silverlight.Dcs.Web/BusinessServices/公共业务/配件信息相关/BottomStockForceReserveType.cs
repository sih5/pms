﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using System.Text;
using System.Threading.Tasks;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class BottomStockForceReserveTypeAch : DcsSerivceAchieveBase {
        public BottomStockForceReserveTypeAch(DcsDomainService domainService)
            : base(domainService) {
        }
        public void 新增保底库存强制储备类别(BottomStockForceReserveType bottomStockForceReserveType) {
            var count = ObjectContext.BottomStockForceReserveTypes.Where(r => r.ReserveType == bottomStockForceReserveType.ReserveType && r.Status == (int)DcsBaseDataStatus.有效).Count();
            if(count > 0) {
                throw new ValidationException(ErrorStrings.FundMonthlySettleBill_Validation1);
            }
            var userInfo = Utils.GetCurrentUserInfo();
            bottomStockForceReserveType.CreateTime = DateTime.Now;
            bottomStockForceReserveType.CreatorId = userInfo.Id;
            bottomStockForceReserveType.CreatorName = userInfo.Name;
            InsertToDatabase(bottomStockForceReserveType);
        }
        public void 修改保底库存强制储备类别(BottomStockForceReserveType bottomStockForceReserveType) {
            var oldbottomStockForceReserve = ObjectContext.BottomStockForceReserveTypes.Where(r => r.Id == bottomStockForceReserveType.Id && r.Status == (int)DcsBaseDataStatus.有效).FirstOrDefault();
            if(oldbottomStockForceReserve ==null) {
                throw new ValidationException("未找到有效的储备类别");
            }
            var userInfo = Utils.GetCurrentUserInfo();
            oldbottomStockForceReserve.ModifyTime = DateTime.Now;
            oldbottomStockForceReserve.ModifierId = userInfo.Id;
            oldbottomStockForceReserve.ModifierName = userInfo.Name;
            oldbottomStockForceReserve.ReserveName = bottomStockForceReserveType.ReserveName;
            UpdateToDatabase(oldbottomStockForceReserve);
            ObjectContext.SaveChanges();
        }
        public void 作废保底库存强制储备类别(int[] ids) {
            var userInfo = Utils.GetCurrentUserInfo();
            foreach(var id in ids) {
                var oldType = ObjectContext.BottomStockForceReserveTypes.Where(r => r.Id == id).FirstOrDefault();
                if(oldType == null || oldType.Status == (int)DcsBaseDataStatus.作废) {
                    throw new ValidationException(ErrorStrings.Action_Abandon_Using);
                }               
                oldType.ModifyTime = DateTime.Now;
                oldType.ModifierId = userInfo.Id;
                oldType.ModifierName = userInfo.Name;
                oldType.Status = (int)DcsBaseDataStatus.作废;
                this.UpdateBottomStockForceReserveType(oldType);
                UpdateToDatabase(oldType);              
            }
            ObjectContext.SaveChanges();
        }
        public void InsertBottomStockForceReserveType(BottomStockForceReserveType bottomStockForceReserveType) {

        }
        public void UpdateBottomStockForceReserveType(BottomStockForceReserveType bottomStockForceReserveType) {
            UpdateToDatabase(bottomStockForceReserveType);
        }
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:09:49
        [Update(UsingCustomMethod = true)]
        public void 新增保底库存强制储备类别(BottomStockForceReserveType bottomStockForceReserveType) {
            new BottomStockForceReserveTypeAch(this).新增保底库存强制储备类别(bottomStockForceReserveType);
        }
        [Invoke(HasSideEffects = true)]
        public void 作废保底库存强制储备类别(int[] ids) {
            new BottomStockForceReserveTypeAch(this).作废保底库存强制储备类别(ids);
        }
        public void InsertBottomStockForceReserveType(BottomStockForceReserveType bottomStockForceReserveType) {
            new BottomStockForceReserveTypeAch(this).InsertBottomStockForceReserveType(bottomStockForceReserveType);
        }
        public void UpdateBottomStockForceReserveType(BottomStockForceReserveType bottomStockForceReserveType) {
            new BottomStockForceReserveTypeAch(this).UpdateBottomStockForceReserveType(bottomStockForceReserveType);
        }
          [Update(UsingCustomMethod = true)]
        public void 修改保底库存强制储备类别(BottomStockForceReserveType bottomStockForceReserveType) {
            new BottomStockForceReserveTypeAch(this).修改保底库存强制储备类别(bottomStockForceReserveType);
        }
    }
}
