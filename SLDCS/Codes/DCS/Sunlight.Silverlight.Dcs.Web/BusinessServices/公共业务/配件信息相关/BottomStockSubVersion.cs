﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using System.Text;
using System.Threading.Tasks;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    class BottomStockSubVersionAch : DcsSerivceAchieveBase {
        public BottomStockSubVersionAch(DcsDomainService domainService)
            : base(domainService) {
        }
        public IQueryable<BottomStockSubVersion> GetBottomStockSubVersionForQuery() {
            var result = ObjectContext.BottomStockSubVersions.Where(t=>1==1);
            var userInfo = Utils.GetCurrentUserInfo();
            if(userInfo.Id == 0) {
                throw new ValidationException(ErrorStrings.User_Invalid);
            }
            var company = ObjectContext.Companies.Where(r => r.Id == userInfo.EnterpriseId).FirstOrDefault();
            if(company.Type == (int)DcsCompanyType.分公司) {
                //判断是否维护了人员与分销中心关系
                var isHas = ObjectContext.MarketDptPersonnelRelations.Where(r => r.PersonnelId == userInfo.Id && r.Status == (int)DcsBaseDataStatus.有效).Count();
                if(isHas > 0) {
                    result = from a in result.Where(r => ObjectContext.AgencyDealerRelations.Any(t => r.CompanyId == t.DealerId && t.Status == (int)DcsMasterDataStatus.有效 && ObjectContext.Agencies.Any(p => p.Id == t.AgencyId && ObjectContext.MarketDptPersonnelRelations.Any(s => s.PersonnelId == userInfo.Id && s.Status == (int)DcsMasterDataStatus.有效 && s.MarketDepartmentId==p.MarketingDepartmentId))))
                             select a;
                }
            }else if(company.Type == (int)DcsCompanyType.代理库) {
                result = from a in result.Where(t => ObjectContext.AgencyDealerRelations.Any(r => r.Status == (int)DcsMasterDataStatus.有效 && t.CompanyId == r.DealerId && r.AgencyId == userInfo.EnterpriseId)|| t.CompanyId==userInfo.EnterpriseId)
                         select a;
            }else {
                result = result.Where(r => r.CompanyId == userInfo.EnterpriseId);
            }
            return result.OrderBy(t=>t.CompanyCode);
        }
        public IQueryable<BottomStockColVersion> GetBottomStockColVersionForQuery() {
            var result = ObjectContext.BottomStockColVersions.Where(t => 1 == 1);
            var userInfo = Utils.GetCurrentUserInfo();
            if(userInfo.Id == 0) {
                throw new ValidationException(ErrorStrings.User_Invalid);
            }
            var company = ObjectContext.Companies.Where(r => r.Id == userInfo.EnterpriseId).FirstOrDefault();
            if(company.Type == (int)DcsCompanyType.分公司) {
                //判断是否维护了人员与分销中心关系
                var isHas = ObjectContext.MarketDptPersonnelRelations.Where(r => r.PersonnelId == userInfo.Id && r.Status == (int)DcsBaseDataStatus.有效).Count();
                if(isHas > 0) {
                    result = from a in result.Where(r => ObjectContext.AgencyDealerRelations.Any(t => r.CompanyId == t.DealerId && t.Status == (int)DcsMasterDataStatus.有效 && ObjectContext.Agencies.Any(p => p.Id == t.AgencyId && ObjectContext.MarketDptPersonnelRelations.Any(s => s.PersonnelId == userInfo.Id && s.Status == (int)DcsMasterDataStatus.有效 && s.MarketDepartmentId == p.MarketingDepartmentId))))
                             select a;
                }
            } else if(company.Type == (int)DcsCompanyType.代理库) {
                result = from a in result.Where(t => ObjectContext.AgencyDealerRelations.Any(r => r.Status == (int)DcsMasterDataStatus.有效 && t.CompanyId == r.DealerId && r.AgencyId == userInfo.EnterpriseId) || t.CompanyId == userInfo.EnterpriseId)
                         select a;
            } else {
                result = result.Where(r => r.CompanyId == userInfo.EnterpriseId);
            }
            return result.OrderBy(t => t.CompanyCode);
        }
        public IQueryable<BottomStockSubVersion> GetBottomStockSubVersionForExport(int[] ids, int? status, string subVersionCode, string colVersionCode, string sparePartCode, string sparePartName, string companyCode, string companyName, DateTime? bStartTime, DateTime? eStartTime, int? companyType, string reserveTypeSubItem) {
            var result = ObjectContext.BottomStockSubVersions.Where(t => 1 == 1);
            var userInfo = Utils.GetCurrentUserInfo();
            if(userInfo.Id == 0) {
                throw new ValidationException(ErrorStrings.User_Invalid);
            }
            if(ids != null && ids.Length > 0) {
                result = result.Where(t=>ids.Contains(t.Id));
            } else {
                if(status.HasValue) {
                    result = result.Where(t => t.Status == status);
                }
                if(!string.IsNullOrEmpty(subVersionCode)) {
                    result = result.Where(t => t.SubVersionCode.Contains(subVersionCode));
                }
                if(!string.IsNullOrEmpty(colVersionCode)) {
                    result = result.Where(t => t.ColVersionCode.Contains(colVersionCode));
                }
                if(!string.IsNullOrEmpty(sparePartCode)) {
                    result=result.Where(t => t.SparePartCode.Contains(sparePartCode));
                }
                if(!string.IsNullOrEmpty(reserveTypeSubItem)) {
                    result = result.Where(t => t.ReserveTypeSubItem.Contains(reserveTypeSubItem));
                }

                if(!string.IsNullOrEmpty(sparePartName)) {
                    result = result.Where(t=>t.SparePartName.Contains(sparePartName));
                }
                if(!string.IsNullOrEmpty(companyCode)) {
                    result = result.Where(t => t.CompanyCode.Contains(companyCode));
                }
                if(!string.IsNullOrEmpty(companyName)) {
                    result = result.Where(t => t.CompanyName.Contains(companyName));
                }
                if(companyType.HasValue) {
                    result = result.Where(t => t.CompanyType == companyType);
                }

                if(bStartTime.HasValue) {
                    result = result.Where(t => t.StartTime >= bStartTime);
                }
                if(eStartTime.HasValue) {
                    result = result.Where(t => t.StartTime <= eStartTime);
                }

            }
            var company = ObjectContext.Companies.Where(r => r.Id == userInfo.EnterpriseId).FirstOrDefault();
            if(company.Type == (int)DcsCompanyType.分公司) {
                //判断是否维护了人员与分销中心关系
                var isHas = ObjectContext.MarketDptPersonnelRelations.Where(r => r.PersonnelId == userInfo.Id && r.Status == (int)DcsBaseDataStatus.有效).Count();
                if(isHas > 0) {
                    result = from a in result.Where(r => ObjectContext.AgencyDealerRelations.Any(t => r.CompanyId == t.DealerId && t.Status == (int)DcsMasterDataStatus.有效 && ObjectContext.Agencies.Any(p => p.Id == t.AgencyId && ObjectContext.MarketDptPersonnelRelations.Any(s => s.PersonnelId == userInfo.Id && s.Status == (int)DcsMasterDataStatus.有效 && s.MarketDepartmentId == p.MarketingDepartmentId))))
                             select a;
                }
            } else if(company.Type == (int)DcsCompanyType.代理库) {
                result = from a in result.Where(t => ObjectContext.AgencyDealerRelations.Any(r => r.Status == (int)DcsMasterDataStatus.有效 && t.CompanyId == r.DealerId && r.AgencyId == userInfo.EnterpriseId) || t.CompanyId == userInfo.EnterpriseId)
                         select a;
            } else {
                result = result.Where(r => r.CompanyId == userInfo.EnterpriseId);
            }
            return result.OrderBy(t => t.CompanyCode);
        }
        public IQueryable<BottomStockColVersion> GetBottomStockColVersionForExport(int[] ids, int? status, string colVersionCode, string sparePartCode, string sparePartName, string companyCode, string companyName, DateTime? bStartTime, DateTime? eStartTime, int? companyType) {
            var result = ObjectContext.BottomStockColVersions.Where(t => 1 == 1);
            var userInfo = Utils.GetCurrentUserInfo();
            if(userInfo.Id == 0) {
                throw new ValidationException(ErrorStrings.User_Invalid);
            }
            if(ids != null && ids.Length > 0) {
                result = result.Where(t => ids.Contains(t.Id));
            } else {
                if(status.HasValue) {
                    result = result.Where(t => t.Status == status);
                }
                if(!string.IsNullOrEmpty(colVersionCode)) {
                    result = result.Where(t => t.ColVersionCode.Contains(colVersionCode));
                }
                if(!string.IsNullOrEmpty(sparePartCode)) {
                    result = result.Where(t => t.SparePartCode.Contains(sparePartCode));
                }

                if(!string.IsNullOrEmpty(sparePartName)) {
                    result = result.Where(t => t.SparePartName.Contains(sparePartName));
                }
                if(!string.IsNullOrEmpty(companyCode)) {
                    result = result.Where(t => t.CompanyCode.Contains(companyCode));
                }
                if(!string.IsNullOrEmpty(companyName)) {
                    result = result.Where(t => t.CompanyName.Contains(companyName));
                }
                if(companyType.HasValue) {
                    result = result.Where(t => t.CompanyType == companyType);
                }

                if(bStartTime.HasValue) {
                    result = result.Where(t => t.StartTime >= bStartTime);
                }
                if(eStartTime.HasValue) {
                    result = result.Where(t => t.StartTime <= eStartTime);
                }

            }
            var company = ObjectContext.Companies.Where(r => r.Id == userInfo.EnterpriseId).FirstOrDefault();
            if(company.Type == (int)DcsCompanyType.分公司) {
                //判断是否维护了人员与分销中心关系
                var isHas = ObjectContext.MarketDptPersonnelRelations.Where(r => r.PersonnelId == userInfo.Id && r.Status == (int)DcsBaseDataStatus.有效).Count();
                if(isHas > 0) {
                    result = from a in result.Where(r => ObjectContext.AgencyDealerRelations.Any(t => r.CompanyId == t.DealerId && t.Status == (int)DcsMasterDataStatus.有效 && ObjectContext.Agencies.Any(p => p.Id == t.AgencyId && ObjectContext.MarketDptPersonnelRelations.Any(s => s.PersonnelId == userInfo.Id && s.Status == (int)DcsMasterDataStatus.有效 && s.MarketDepartmentId == p.MarketingDepartmentId))))
                             select a;
                }
            } else if(company.Type == (int)DcsCompanyType.代理库) {
                result = from a in result.Where(t => ObjectContext.AgencyDealerRelations.Any(r => r.Status == (int)DcsMasterDataStatus.有效 && t.CompanyId == r.DealerId && r.AgencyId == userInfo.EnterpriseId) || t.CompanyId == userInfo.EnterpriseId)
                         select a;
            } else {
                result = result.Where(r => r.CompanyId == userInfo.EnterpriseId);
            }
            return result.OrderBy(t => t.CompanyCode);
        }
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:09:49
        public IQueryable<BottomStockSubVersion> GetBottomStockSubVersionForQuery() {
            return new  BottomStockSubVersionAch(this).GetBottomStockSubVersionForQuery();
        }
        public IQueryable<BottomStockColVersion> GetBottomStockColVersionForQuery() {
            return new BottomStockSubVersionAch(this).GetBottomStockColVersionForQuery();
        }
        public IQueryable<BottomStockSubVersion> GetBottomStockSubVersionForExport(int[] ids, int? status, string subVersionCode, string colVersionCode, string sparePartCode, string sparePartName, string companyCode, string companyName, DateTime? bStartTime, DateTime? eStartTime, int? companyType, string reserveTypeSubItem) {
            return new BottomStockSubVersionAch(this).GetBottomStockSubVersionForExport(ids, status, subVersionCode, colVersionCode, sparePartCode, sparePartName, companyCode, companyName, bStartTime, eStartTime, companyType, reserveTypeSubItem);
        }
        public IQueryable<BottomStockColVersion> GetBottomStockColVersionForExport(int[] ids, int? status, string colVersionCode, string sparePartCode, string sparePartName, string companyCode, string companyName, DateTime? bStartTime, DateTime? eStartTime, int? companyType) {
            return new BottomStockSubVersionAch(this).GetBottomStockColVersionForExport(ids, status, colVersionCode, sparePartCode, sparePartName, companyCode, companyName, bStartTime, eStartTime, companyType);

        }
    }
}
