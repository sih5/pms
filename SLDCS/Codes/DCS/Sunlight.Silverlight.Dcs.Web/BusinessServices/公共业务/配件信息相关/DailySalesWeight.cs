﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using System.Text;
using System.Threading.Tasks;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DailySalesWeightAch : DcsSerivceAchieveBase {
        public DailySalesWeightAch(DcsDomainService domainService)
            : base(domainService) {
        }
        public void 导入日均销量计算参数(DailySalesWeight[] DailySalesWeights) {
            var userInfo = Utils.GetCurrentUserInfo();
            if(userInfo.Id == 0) {
                throw new ValidationException(ErrorStrings.User_Invalid);
            }
            var oldWeight = ObjectContext.DailySalesWeights.ToArray();
            foreach(var item in DailySalesWeights) {
                var oldData = oldWeight.Where(t => t.WarehouseId == item.WarehouseId && t.Times == item.Times).FirstOrDefault();
                if(oldData == null) {
                    DailySalesWeight newWeight = new DailySalesWeight {
                        WarehouseCode = item.WarehouseCode,
                        WarehouseId = item.WarehouseId,
                        WarehouseName = item.WarehouseName,
                        Times = item.Times,
                        Weight = item.Weight,
                        CreateTime = DateTime.Now,
                        CreatorId = userInfo.Id,
                        CreatorName = userInfo.Name
                    };
                    InsertToDatabase(newWeight);
                } else {
                    oldData.Weight = item.Weight;
                    oldData.ModifierId = userInfo.Id;
                    oldData.ModifierName = userInfo.Name;
                    oldData.ModifyTime = DateTime.Now;
                    UpdateToDatabase(oldData);
                }
            }
            ObjectContext.SaveChanges();
        }
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:09:49
        [Invoke(HasSideEffects = true)]
        public void 导入日均销量计算参数(DailySalesWeight[] DailySalesWeights) {
            new DailySalesWeightAch(this).导入日均销量计算参数(DailySalesWeights);
        }       
    }
}
