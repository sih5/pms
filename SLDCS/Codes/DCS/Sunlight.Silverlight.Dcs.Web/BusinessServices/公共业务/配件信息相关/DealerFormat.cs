﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using System.Text;
using System.Threading.Tasks;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web{
    class DealerFormatAch : DcsSerivceAchieveBase {
        public DealerFormatAch(DcsDomainService domainService)
            : base(domainService) {
        }
        public void InsertDealerFormat(DealerFormat apps) {

        }
        public void 新增服务商业态(DealerFormat apps) {
            var userInfo = Utils.GetCurrentUserInfo();
            if(userInfo.Id == 0) {
                throw new ValidationException(ErrorStrings.User_Invalid);
            }
            //新增后，根据服务商编号，更新上次有效的 数据为作废状态
            var oldData = ObjectContext.DealerFormats.Where(t => t.DealerId == apps.DealerId && t.Status == (int)DcsBaseDataStatus.有效).ToArray();
            foreach(var item in oldData){
                item.Status = (int)DcsBaseDataStatus.作废;
                item.AbandonerId = userInfo.Id;
                item.AbandonerName = userInfo.Name;
                item.AbandonTime = DateTime.Now;
                UpdateToDatabase(item);
            }
            apps.CreateTime = DateTime.Now;
            apps.CreatorId = userInfo.Id;
            apps.CreatorName = userInfo.Name;
            InsertToDatabase(apps);
            ObjectContext.SaveChanges();
        }
        public void 作废服务商业态(int[] ids) {
            var userInfo = Utils.GetCurrentUserInfo();
            if(userInfo.Id == 0) {
                throw new ValidationException(ErrorStrings.User_Invalid);
            }
            foreach(var id in ids ){
                var dealerFormat = ObjectContext.DealerFormats.Where(t => t.Id == id).First();
                dealerFormat.AbandonTime = DateTime.Now;
                dealerFormat.AbandonerId = userInfo.Id;
                dealerFormat.AbandonerName = userInfo.Name;
                dealerFormat.Status = (int)DcsBaseDataStatus.作废;
                UpdateToDatabase(dealerFormat);
            }
          
            ObjectContext.SaveChanges();
        }
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:09:49

        public void InsertDealerFormat(DealerFormat apps) {
            new DealerFormatAch(this).InsertDealerFormat(apps);
        }
     
        [Update(UsingCustomMethod = true)]
        public void 新增服务商业态(DealerFormat bill) {
            new DealerFormatAch(this).新增服务商业态(bill);
        }
        [Invoke(HasSideEffects = true)]
        public void 作废服务商业态(int[] ids) {
            new DealerFormatAch(this).作废服务商业态(ids);
        }
    }
}
