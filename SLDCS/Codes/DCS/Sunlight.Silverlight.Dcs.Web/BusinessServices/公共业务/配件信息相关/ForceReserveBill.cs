﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using System.Text;
using System.Threading.Tasks;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class ForceReserveBillAch : DcsSerivceAchieveBase {
        public ForceReserveBillAch(DcsDomainService domainService)
            : base(domainService) {
        }

        public void InsertForceReserveBill(ForceReserveBill apps) {

        }
        public void UpdateForceReserveBill(ForceReserveBill apps) {
        }

        public void 作废强制储备单(int[] ids) {
            var userInfo = Utils.GetCurrentUserInfo();
            foreach(var id in ids) {
                var bill = ObjectContext.ForceReserveBills.Where(r => r.Id == id).FirstOrDefault();
                if(bill.Status != (int)DCSForceReserveBillStatus.新建) {
                    throw new ValidationException(ErrorStrings.Action_Abandon_New);
                }
                this.ValidateForUpdate(bill);
                bill.Status = (int)DCSForceReserveBillStatus.作废;
                bill.AbandonerId = userInfo.Id;
                bill.AbandonerName = userInfo.Name;
                bill.AbandonTime = DateTime.Now;
                UpdateToDatabase(bill);
            }
            ObjectContext.SaveChanges();
        }
        public void 提交强制储备单(int[] ids) {
            var userInfo = Utils.GetCurrentUserInfo();
            foreach(var id in ids) {
                var bill = ObjectContext.ForceReserveBills.Where(r => r.Id == id).FirstOrDefault();
                if(bill.Status != (int)DCSForceReserveBillStatus.新建) {
                    throw new ValidationException(ErrorStrings.Action_Submit_New);
                }
                this.ValidateForUpdate(bill);
                bill.SubmitterId = userInfo.Id;
                bill.SubmitterName = userInfo.Name;
                bill.SubmitTime = DateTime.Now;
                bill.Status = (int)DCSForceReserveBillStatus.提交;
                //	当储备类别是“L”类型时，判断清单中“建议强制储备数”是否等于“强制储备数” ，相等时主单状态更新为生效，跳过 审核、审批 操作，具体逻辑见 审批的逻辑
                if(bill.ForceReserveBillDetails.Where(r => r.ForceReserveQty != r.SuggestForceReserveQty).Count() == 0 && bill.ReserveType == "L" && bill.CompanyType == (int)DcsCompanyType.代理库) {
                    this.SuccessAppave(bill);
                }
                UpdateToDatabase(bill);
            }
            ObjectContext.SaveChanges();
        }
        public void 更新强制储备单(ForceReserveBill bill) {
            var oldBill = ObjectContext.ForceReserveBills.Where(r => r.Id == bill.Id).FirstOrDefault();
            if(oldBill.Status != (int)DCSForceReserveBillStatus.新建) {
                throw new ValidationException(ErrorStrings.Action_Abandon_New);
            }
            this.ValidateForUpdate(oldBill);
            bill.ForceReserveBillDetails.Clear();
            var forceReserveBillDetails = ChangeSet.GetAssociatedChanges(bill, r => r.ForceReserveBillDetails);
            foreach(ForceReserveBillDetail forceReserveBillDetail in forceReserveBillDetails) {
                switch(ChangeSet.GetChangeOperation(forceReserveBillDetail)) {
                    case ChangeOperation.Insert:
                        forceReserveBillDetail.ReserveFee = forceReserveBillDetail.ForceReserveQty * forceReserveBillDetail.CenterPrice;
                        InsertToDatabase(forceReserveBillDetail);
                        break;
                    case ChangeOperation.Update:
                        forceReserveBillDetail.ReserveFee = forceReserveBillDetail.ForceReserveQty * forceReserveBillDetail.CenterPrice;
                        UpdateToDatabase(forceReserveBillDetail);
                        break;
                    case ChangeOperation.Delete:
                        DeleteFromDatabase(forceReserveBillDetail);
                        break;
                }
            }
            oldBill.ValidateFrom = bill.ValidateFrom;
            oldBill.Path = bill.Path;
            UpdateToDatabase(oldBill);
            ObjectContext.SaveChanges();
        }
        public void 审核强制储备单(ForceReserveBill bill) {
            var oldBill = ObjectContext.ForceReserveBills.Where(r => r.Id == bill.Id).FirstOrDefault();
            if(oldBill.Status != (int)DCSForceReserveBillStatus.提交) {
                throw new ValidationException(ErrorStrings.Action_Submit_New);
            }
            var userInfo = Utils.GetCurrentUserInfo();
            if(userInfo.Id==0){
                throw new ValidationException(ErrorStrings.User_Invalid);
            }
            this.ValidateForUpdate(bill);
            if(bill.CompanyType == (int)DcsCompanyType.服务站) {
                var company = ObjectContext.Companies.Where(r => r.Id == userInfo.EnterpriseId).First();
                if(bill.ReserveType=="T"){                    
                    if(company.Type!=(int)DcsCompanyType.分公司){
                        throw new ValidationException(ErrorStrings.ForceReserveBill_Approve_T);
                    }
                } else {
                    if(company.Type == (int)DcsCompanyType.分公司) {
                        throw new ValidationException(ErrorStrings.ForceReserveBill_Approve_NotT);
                    }
                }
                bill.ForceReserveBillDetails.Clear();
                var forceReserveBillDetails = ChangeSet.GetAssociatedChanges(bill, r => r.ForceReserveBillDetails);
                foreach(ForceReserveBillDetail forceReserveBillDetail in forceReserveBillDetails) {
                    switch(ChangeSet.GetChangeOperation(forceReserveBillDetail)) {
                        case ChangeOperation.Update:
                            forceReserveBillDetail.ReserveFee = forceReserveBillDetail.ForceReserveQty * forceReserveBillDetail.CenterPrice;
                            UpdateToDatabase(forceReserveBillDetail);
                            break;

                    }
                }
            }
           
            oldBill.CheckerId = userInfo.Id;
            oldBill.CheckerName = userInfo.Name;
            oldBill.CheckTime = DateTime.Now;
            oldBill.Status = (int)DCSForceReserveBillStatus.审核通过;
            UpdateToDatabase(oldBill);
            ObjectContext.SaveChanges();
        }
        //审批逻辑
        public void SuccessAppave(ForceReserveBill bill) {
            var userInfo = Utils.GetCurrentUserInfo();
            bill.ApproverId = userInfo.Id;
            bill.ApproverName = userInfo.Name;
            bill.ApproveTime = DateTime.Now;
            bill.Status = (int)DCSForceReserveBillStatus.生效;
            //审批时 当生效时间小于等于审批时间时，生效时间赋值为审批时间，反之则保留现有逻辑
            if(bill.ValidateFrom.HasValue && bill.ValidateFrom.Value < bill.ApproveTime) {
                bill.ValidateFrom = bill.ApproveTime;
            }
            //更新合集版本号
            var versionInfoes = ObjectContext.VersionInfoes.Where(a => a.CompanyId == bill.CompanyId && a.Type == (int)DCSVersionInfoType.合集).FirstOrDefault();
            if(versionInfoes == null) {
                var version = new VersionInfo {
                    CompanyId = bill.CompanyId,
                    CompanyCode = bill.CompanyCode,
                    Type = (int)DCSVersionInfoType.合集,
                    SerialCode = 1
                };
                if(bill.CompanyType==(int)DcsCompanyType.代理库) {
                    bill.ColVersionCode = "BZ" + bill.CompanyCode + "0001";
                } else {
                    bill.ColVersionCode = "BF" + bill.CompanyCode + "0001";
                }
              
                InsertToDatabase(version);
            } else {
                if(bill.CompanyType == (int)DcsCompanyType.代理库) {
                    bill.ColVersionCode = "BZ" + bill.CompanyCode + "000" + (versionInfoes.SerialCode + 1);
                } else {
                    bill.ColVersionCode = "BF" + bill.CompanyCode + "000" + (versionInfoes.SerialCode + 1);
                }
                versionInfoes.SerialCode = versionInfoes.SerialCode + 1;
                UpdateToDatabase(versionInfoes);
            }
            UpdateToDatabase(bill);
            //审批通过后，作废相同 储备类型的单据
            var history = ObjectContext.ForceReserveBills.Where(t => t.ReserveType == bill.ReserveType && t.CompanyId == bill.CompanyId && t.Id != bill.Id && t.Status == (int)DCSForceReserveBillStatus.生效 && t.ReserveTypeSubItem == bill.ReserveTypeSubItem).ToArray();
            foreach(var item in history) {
                item.Status = (int)DCSForceReserveBillStatus.作废;
                item.AbandonTime = DateTime.Now;
                item.AbandonerName = "审核通过后作废";
            }
            ObjectContext.SaveChanges();
        }
        //批量审核
        public void 批量审核强制储备单(int[] ids) {
            foreach(var id in ids) {
                var oldBill = ObjectContext.ForceReserveBills.Where(r => r.Id == id).FirstOrDefault();
                if(oldBill.Status != (int)DCSForceReserveBillStatus.提交) {
                    throw new ValidationException(ErrorStrings.Action_Approve_Submit);
                }
                var userInfo = Utils.GetCurrentUserInfo();
                if(userInfo.Id == 0) {
                    throw new ValidationException(ErrorStrings.User_Invalid);
                }
                this.ValidateForUpdate(oldBill);
                if(oldBill.CompanyType == (int)DcsCompanyType.服务站) {
                    var company = ObjectContext.Companies.Where(r => r.Id == userInfo.EnterpriseId).First();
                    if(oldBill.ReserveType == "T") {
                        if(company.Type != (int)DcsCompanyType.分公司) {
                            throw new ValidationException(ErrorStrings.ForceReserveBill_Approve_T);
                        }
                    } else {
                        if(company.Type == (int)DcsCompanyType.分公司) {
                            throw new ValidationException(ErrorStrings.ForceReserveBill_Approve_NotT);
                        }
                    }
                }
                oldBill.CheckerId = userInfo.Id;
                oldBill.CheckerName = userInfo.Name;
                oldBill.CheckTime = DateTime.Now;
                oldBill.Status = (int)DCSForceReserveBillStatus.审核通过;
                UpdateToDatabase(oldBill);
            } 
            ObjectContext.SaveChanges();
        }
        public void 审批强制储备单(ForceReserveBill Bbill) {
            var bill = ObjectContext.ForceReserveBills.Where(r => r.Id == Bbill.Id).FirstOrDefault();
            if(bill.CompanyType == (int)DcsCompanyType.代理库) {
                if(bill.Status != (int)DCSForceReserveBillStatus.审核通过) {
                    throw new ValidationException(ErrorStrings.Action_Approve_Fail);
                } else {
                    this.SuccessAppave(bill);
                }
            } else {
                if((bill.Status == (int)DCSForceReserveBillStatus.审核通过 && bill.ReserveType == "T") || (bill.Status == (int)DCSForceReserveBillStatus.已确认 && bill.ReserveType != "T")) {
                    this.SuccessAppave(bill);
                } else {
                    throw new ValidationException(ErrorStrings.Action_Approve_Fail);
                }
            }
        }
        //批量审批
        public void 批量审批强制储备单(int[] ids) {
            var errorMessage = "";
            foreach(var id in ids) {
                var bill = ObjectContext.ForceReserveBills.Where(r => r.Id == id).FirstOrDefault();
                if(bill.CompanyType == (int)DcsCompanyType.代理库) {
                    if(bill.Status != (int)DCSForceReserveBillStatus.审核通过) {
                        errorMessage = errorMessage + bill.ReserveTypeSubItem + "、";
                    } else {
                        this.SuccessAppave(bill);

                    }
                } else {
                    if((bill.Status == (int)DCSForceReserveBillStatus.审核通过 && bill.ReserveType == "T") || (bill.Status == (int)DCSForceReserveBillStatus.已确认 && bill.ReserveType != "T")) {
                        this.SuccessAppave(bill);
                    } else {
                        errorMessage = errorMessage + bill.ReserveTypeSubItem + "、";
                    }
                }

            }
            if(!"".Equals(errorMessage)) {
                throw new ValidationException(errorMessage + ErrorStrings.Action_Approve_Fail);
            }
        }
        public void 驳回强制储备单(ForceReserveBill bill) {
            var userInfo = Utils.GetCurrentUserInfo();
            this.ValidateForUpdate(bill);
            bill.RejecterId = userInfo.Id;
            bill.RejecterName = userInfo.Name;
            bill.RejectTime = DateTime.Now;
            bill.Status = (int)DCSForceReserveBillStatus.新建;
            bill.SubmitterId = null;
            bill.SubmitterName = null;
            bill.SubmitTime = null;
            bill.CheckerId = null;
            bill.CheckerName = null;
            bill.CheckTime = null;
            UpdateToDatabase(bill);
            ObjectContext.SaveChanges();

        }
        private void ValidateForUpdate(ForceReserveBill bill) {
            var userInfo = Utils.GetCurrentUserInfo();
            bill.ModifyTime = DateTime.Now;
            bill.ModifierId = userInfo.Id;
            bill.ModifierName = userInfo.Name;
        }
        public ForceReserveBill GetForceReserveBillsWithDetailById(int id) {
            var result = ObjectContext.ForceReserveBills.Include("ForceReserveBillDetails").Where(r => r.Id == id).FirstOrDefault();
            return result;
        }
        public IQueryable<ForceReserveBill> GetForceReserveBillsForDealer(string centerName,int? status,string subVersionCode,string colVersionCode,string reserveType,string reserveTypeSubItem,string companyCode,string companyName) {
            var result = ObjectContext.ForceReserveBills.Include("ForceReserveBillDetails").Where(r => r.CompanyType == (int)DcsCompanyType.服务站);
            var userInfo = Utils.GetCurrentUserInfo();
            if(userInfo.Id == 0) {
                throw new ValidationException(ErrorStrings.User_Invalid);
            }
            var company = ObjectContext.Companies.Where(r => r.Id == userInfo.EnterpriseId).FirstOrDefault();
            if(company.Type == (int)DcsCompanyType.分公司) {
                //判断是否维护了人员与分销中心关系
                var isHas = ObjectContext.MarketDptPersonnelRelations.Where(r => r.PersonnelId == userInfo.Id && r.Status == (int)DcsBaseDataStatus.有效).Count();
                if(isHas > 0) {
                    result = from a in result.Where(r => ObjectContext.AgencyDealerRelations.Any(t => r.CompanyId == t.DealerId && t.Status == (int)DcsMasterDataStatus.有效 && ObjectContext.Agencies.Any(p => p.Id == t.AgencyId && ObjectContext.MarketDptPersonnelRelations.Any(s => s.PersonnelId == userInfo.Id && s.Status == (int)DcsMasterDataStatus.有效 && p.MarketingDepartmentId==s.MarketDepartmentId))))
                             select a;
                }
            }
            if(company.Type == (int)DcsCompanyType.代理库) {
                result = from a in result.Where(t => ObjectContext.AgencyDealerRelations.Any(r => r.Status == (int)DcsMasterDataStatus.有效 && t.CompanyId == r.DealerId && r.AgencyId == userInfo.EnterpriseId))
                         select a;
            }
            if(company.Type == (int)DcsCompanyType.服务站 || company.Type == (int)DcsCompanyType.服务站兼代理库) {
                result = result.Where(r => r.CompanyId == userInfo.EnterpriseId);
            }
            if(!string.IsNullOrEmpty(centerName)){
                result = result.Where(t => ObjectContext.AgencyDealerRelations.Any(r => r.Status == (int)DcsMasterDataStatus.有效 && t.CompanyId == r.DealerId && r.AgencyName.Contains(centerName)));
            }
			if(status.HasValue){
			result = result.Where(t=>t.Status==status);
			}
			if(!string.IsNullOrEmpty(subVersionCode)){
			result = result.Where(t=>t.SubVersionCode.Contains(subVersionCode));
			}
			if(!string.IsNullOrEmpty(colVersionCode)){
			result = result.Where(t=>t.ColVersionCode.Contains(colVersionCode));
			}
			if(!string.IsNullOrEmpty(reserveType)){
			result = result.Where(t=>t.ReserveType.Contains(reserveType));
			}
			if(!string.IsNullOrEmpty(reserveTypeSubItem)){
			result = result.Where(t=>t.ReserveTypeSubItem.Contains(reserveTypeSubItem));
			}
			if(!string.IsNullOrEmpty(companyCode)){
			result = result.Where(t=>t.CompanyCode.Contains(companyCode));
			}
			if(!string.IsNullOrEmpty(companyName)){
			result = result.Where(t=>t.CompanyName.Contains(companyName));
			}
            return result.OrderByDescending(r=>r.CreateTime);
        }
        public IQueryable<ForceReserveBillDetailQuery> GetForceReserveBillDetailsOrder(int forceReserveBillId) {
             var result = from a in ObjectContext.ForceReserveBillDetails.Where(r=>r.ForceReserveBillId==forceReserveBillId)
                          join b in ObjectContext.ForceReserveBills on a.ForceReserveBillId equals b.Id
                          select new  ForceReserveBillDetailQuery  {
                              Id=a.Id,
                              SparePartCode=a.SparePartCode,
                              SparePartName=a.SparePartName,
                              SuggestForceReserveQty=a.SuggestForceReserveQty.Value,
                              ForceReserveQty=a.ForceReserveQty.Value,
                              CenterPrice=a.CenterPrice,
                              CenterPartProperty=a.CenterPartProperty.Value,
                              ReserveFee=a.ReserveFee,
                              IsRed=b.ReserveType=="L"&&a.ForceReserveQty!=a.SuggestForceReserveQty
                          };
             return result.OrderByDescending(t => t.IsRed);

        }
        public void 确认强制储备单(ForceReserveBill Bbill) {
            var bill = ObjectContext.ForceReserveBills.Where(r => r.Id == Bbill.Id).FirstOrDefault();
            if(bill.Status != (int)DCSForceReserveBillStatus.审核通过) {
                throw new ValidationException(ErrorStrings.Action_Confirm_Approved);
            } else {
                bill.Status = (int)DCSForceReserveBillStatus.已确认;
                this.ValidateForUpdate(bill);
                UpdateToDatabase(bill);
                ObjectContext.SaveChanges();
            }
            if(bill.CompanyType == (int)DcsCompanyType.服务站) {
                Bbill.ForceReserveBillDetails.Clear();
                var forceReserveBillDetails = ChangeSet.GetAssociatedChanges(Bbill, r => r.ForceReserveBillDetails);
                foreach(ForceReserveBillDetail forceReserveBillDetail in forceReserveBillDetails) {
                    switch(ChangeSet.GetChangeOperation(forceReserveBillDetail)) {
                        case ChangeOperation.Update:
                            forceReserveBillDetail.ReserveFee = forceReserveBillDetail.ForceReserveQty * forceReserveBillDetail.CenterPrice;
                            UpdateToDatabase(forceReserveBillDetail);
                            break;

                    }
                }
            }
        }
        //批量确认
        public void 批量确认强制储备单(int[] ids) {
            var errorMessage = "";
            foreach(var id in ids) {
                var bill = ObjectContext.ForceReserveBills.Where(r => r.Id == id).FirstOrDefault();
                if(bill.Status != (int)DCSForceReserveBillStatus.审核通过) {
                    errorMessage = errorMessage + bill.ReserveTypeSubItem + "、";
                } else {
                    bill.Status = (int)DCSForceReserveBillStatus.已确认;
                    this.ValidateForUpdate(bill);
                    UpdateToDatabase(bill);
                }
            }
            if(!"".Equals(errorMessage)) {
                throw new ValidationException(errorMessage + ErrorStrings.Action_Confirm_ApprovedNot);
            }
            ObjectContext.SaveChanges();
        }
        public IEnumerable<ForceReserveBillDealerExport> GetForceReserveBillsForDealerExport(int[] ids, int? status, string subVersionCode, string colVersionCode, string reserveTypeSubItem, string companyCode, string companyName, DateTime? bCreateTime, DateTime? eCreateTime, DateTime? bApproveTime, DateTime? eApproveTime, string reserveType, string centerName) {
            var userInfo = Utils.GetCurrentUserInfo();
            if(userInfo.Id == 0) {
                throw new ValidationException(ErrorStrings.User_Invalid);
            }
            string SQL = @"select fr.companycode,
                               fr.companyname,
                               fr.companytype,
                               fr.subversioncode,
                               fr.colversioncode,
                               fr.status,
                               fr.reservetype,
                               fr.reservetypesubitem,
                               fr.creatorname,
                               fr.createtime,
                               fr.modifiername,
                               fr.modifytime,
                               fr.checkername,
                               fr.checktime,
                               fr.approvername,
                               fr.approvetime,
                               fr.rejectername,
                               fr.rejecttime,
                               fr.abandonername,
                               fr.abandontime,
                               fr.submittername,
                               fr.submittime,
                               frb.sparepartcode,
                               frb.sparepartname,
                               frb.suggestforcereserveqty,
                               frb.forcereserveqty,
                               frb.reservefee,
                               frb.centerpartproperty,
                               frb.centerprice
                          from ForceReserveBill fr
                          join ForceReserveBillDetail frb
                            on fr.id = frb.forcereservebillid
                            where fr.companytype=2 ";
            if(ids != null && ids.Length > 0) {
                SQL = SQL + " and fr.id in (";
                string statusStr = "";
                for(var i = 0; i < ids.Length; i++) {
                    statusStr = statusStr + ids[i] + ",";
                }
                statusStr = statusStr.Substring(0, statusStr.Length - 1);
                SQL = SQL + statusStr + ")";
            } else {
                if(status.HasValue) {
                    SQL = SQL + " and fr.Status ="+status;
                }
                if(!string.IsNullOrEmpty(subVersionCode)) {
                    SQL = SQL + " and  Lower(fr.SubVersionCode) like'%" + subVersionCode.ToLower() + "%'";
                }
                if(!string.IsNullOrEmpty(reserveTypeSubItem)) {
                    SQL = SQL + " and  Lower(fr.reserveTypeSubItem) like'%" + reserveTypeSubItem.ToLower() + "%'";
                }
                if(!string.IsNullOrEmpty(colVersionCode)) {
                    SQL = SQL + " and  Lower(fr.colVersionCode) like'%" + colVersionCode.ToLower() + "%'";
                }              
                if(!string.IsNullOrEmpty(companyCode)) {
                    SQL = SQL + " and  Lower(fr.companyCode) like'%" + companyCode.ToLower() + "%'";
                }
                if(!string.IsNullOrEmpty(companyName)) {
                    SQL = SQL + " and fr.companyName like'%" + companyName + "%'";
                }
                if(!string.IsNullOrEmpty(reserveType)) {
                    SQL = SQL + " and  Lower(fr.reserveType) like'%" + reserveType.ToLower() + "%'";
                }
                if(bCreateTime.HasValue) {
                    SQL = SQL + " and fr.CreateTime>= to_date('" + bCreateTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
                }
                if(eCreateTime.HasValue) {
                    SQL = SQL + " and fr.CreateTime<= to_date('" + eCreateTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
                }
                if(bApproveTime.HasValue) {
                    SQL = SQL + " and fr.ApproveTime>= to_date('" + bApproveTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
                }
                if(eApproveTime.HasValue) {
                    SQL = SQL + " and fr.ApproveTime<= to_date('" + eApproveTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
                }   
                if (!string.IsNullOrEmpty(centerName))
                {
                SQL = SQL + " and  exists(select 1 from AgencyDealerRelation ar where ar.status=1 and  fr.companyid=ar.DealerId and ar.AgencyName like '%" + centerName + "%')";
                }				
            }
            var company = ObjectContext.Companies.Where(r => r.Id == userInfo.EnterpriseId).FirstOrDefault();
            if(company.Type == (int)DcsCompanyType.分公司) {
                //判断是否维护了人员与分销中心关系
                var isHas = ObjectContext.MarketDptPersonnelRelations.Where(r => r.PersonnelId == userInfo.Id && r.Status == (int)DcsBaseDataStatus.有效).Count();
                if(isHas > 0) {
                    SQL = SQL + " and  (exists(select 1 from  MarketDptPersonnelRelation mr join Agency ag on mr.marketdepartmentid = ag.marketingdepartmentid  join AgencyDealerRelation ar on ag.id = ar.agencyid  where mr.PersonnelId = " + userInfo.Id + " and  ar.dealerid = fr.companyid and mr.status=1) )";
                }
            }else if(company.Type == (int)DcsCompanyType.代理库) {
                SQL = SQL + " and  exists(select 1 from AgencyDealerRelation ar where ar.status=1 and  fr.companyid=ar.DealerId and ar.AgencyId="+userInfo.EnterpriseId+")";
            }else {
                SQL = SQL + " fr.companyid="+userInfo.EnterpriseId;
            }
            var search = ObjectContext.ExecuteStoreQuery<ForceReserveBillDealerExport>(SQL).ToList();
            return search;
        }
        public IEnumerable<ForceReserveBillDealerMasterExport> GetForceReserveBillDealerMasterExport(int[] ids, int? status, string subVersionCode, string colVersionCode, string reserveTypeSubItem, string companyCode, string companyName, DateTime? bCreateTime, DateTime? eCreateTime, DateTime? bApproveTime, DateTime? eApproveTime, string reserveType,string centerName)
        {
            var userInfo = Utils.GetCurrentUserInfo();
            if (userInfo.Id == 0)
            {
                throw new ValidationException(ErrorStrings.User_Invalid);
            }
            string SQL = @"select fr.companycode,
                               fr.companyname,
                               fr.companytype,
                               fr.subversioncode,
                               fr.colversioncode,
                               fr.status,
                               fr.reservetype,
                               fr.reservetypesubitem,
                               fr.creatorname,
                               fr.createtime,
                               fr.modifiername,
                               fr.modifytime,
                               fr.checkername,
                               fr.checktime,
                               fr.approvername,
                               fr.approvetime,
                               fr.rejectername,
                               fr.rejecttime,
                               fr.abandonername,
                               fr.abandontime,
                               fr.submittername,
                               fr.submittime
                          from ForceReserveBill fr
                         
                            where fr.companytype=2 ";
            if (ids != null && ids.Length > 0)
            {
                SQL = SQL + " and fr.id in (";
                string statusStr = "";
                for (var i = 0; i < ids.Length; i++)
                {
                    statusStr = statusStr + ids[i] + ",";
                }
                statusStr = statusStr.Substring(0, statusStr.Length - 1);
                SQL = SQL + statusStr + ")";
            }
            else
            {
                if (status.HasValue)
                {
                    SQL = SQL + " and fr.Status =" + status;
                }
                if (!string.IsNullOrEmpty(subVersionCode))
                {
                    SQL = SQL + " and  Lower(fr.SubVersionCode) like'%" + subVersionCode.ToLower() + "%'";
                }
                if (!string.IsNullOrEmpty(reserveTypeSubItem))
                {
                    SQL = SQL + " and  Lower(fr.reserveTypeSubItem) like'%" + reserveTypeSubItem.ToLower() + "%'";
                }
                if (!string.IsNullOrEmpty(colVersionCode))
                {
                    SQL = SQL + " and  Lower(fr.colVersionCode) like'%" + colVersionCode.ToLower() + "%'";
                }
                if (!string.IsNullOrEmpty(companyCode))
                {
                    SQL = SQL + " and  Lower(fr.companyCode) like'%" + companyCode.ToLower() + "%'";
                }
                if (!string.IsNullOrEmpty(companyName))
                {
                    SQL = SQL + " and fr.companyName like'%" + companyName + "%'";
                }
                if (!string.IsNullOrEmpty(reserveType))
                {
                    SQL = SQL + " and  Lower(fr.reserveType) like'%" + reserveType.ToLower() + "%'";
                }
                if (bCreateTime.HasValue)
                {
                    SQL = SQL + " and fr.CreateTime>= to_date('" + bCreateTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
                }
                if (eCreateTime.HasValue)
                {
                    SQL = SQL + " and fr.CreateTime<= to_date('" + eCreateTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
                }
                if (bApproveTime.HasValue)
                {
                    SQL = SQL + " and fr.ApproveTime>= to_date('" + bApproveTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
                }
                if (eApproveTime.HasValue)
                {
                    SQL = SQL + " and fr.ApproveTime<= to_date('" + eApproveTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
                }
				 if (!string.IsNullOrEmpty(centerName))
                {
                SQL = SQL + " and  exists(select 1 from AgencyDealerRelation ar where ar.status=1 and  fr.companyid=ar.DealerId and ar.AgencyName like '%" + centerName + "%')";
                }
            }
            var company = ObjectContext.Companies.Where(r => r.Id == userInfo.EnterpriseId).FirstOrDefault();
            if (company.Type == (int)DcsCompanyType.分公司)
            {
                //判断是否维护了人员与分销中心关系
                var isHas = ObjectContext.MarketDptPersonnelRelations.Where(r => r.PersonnelId == userInfo.Id && r.Status == (int)DcsBaseDataStatus.有效).Count();
                if (isHas > 0)
                {
                    SQL = SQL + " and  (exists(select 1 from  MarketDptPersonnelRelation mr join Agency ag on mr.marketdepartmentid = ag.marketingdepartmentid  join AgencyDealerRelation ar on ag.id = ar.agencyid  where mr.PersonnelId = " + userInfo.Id + " and  ar.dealerid = fr.companyid and mr.status=1) )";
                }
            }
            else if (company.Type == (int)DcsCompanyType.代理库)
            {
                SQL = SQL + " and  exists(select 1 from AgencyDealerRelation ar where ar.status=1 and  fr.companyid=ar.DealerId and ar.AgencyId=" + userInfo.EnterpriseId + ")";
            }
            else
            {
                SQL = SQL + " fr.companyid=" + userInfo.EnterpriseId;
            }
            var search = ObjectContext.ExecuteStoreQuery<ForceReserveBillDealerMasterExport>(SQL).ToList();
            return search;
        }
        public IEnumerable<ForceReserveBillDealerMasterExport> GetForceReserveBillCenterMasterExport(int[] ids, int? status, string subVersionCode, string colVersionCode, string reserveTypeSubItem, string companyCode, string companyName, DateTime? bCreateTime, DateTime? eCreateTime, DateTime? bApproveTime, DateTime? eApproveTime, int companyType, string reserveType)
        {
            var userInfo = Utils.GetCurrentUserInfo();
            if (userInfo.Id == 0)
            {
                throw new ValidationException(ErrorStrings.User_Invalid);
            }
            string SQL = @"select fr.status,
                               fr.companycode,
                               fr.companyname,
                               fr.subversioncode,
                               fr.colversioncode,
                               fr.ReserveType,
                               fr.ReserveTypeSubItem,
                               fr.submittername,
                               fr.submittime,
                               fr.checkername,
                               fr.checktime,
                               fr.approvername,
                               fr.approvetime,
                               fr.rejectername,
                               fr.rejecttime,
                               fr.abandonername,
                               fr.abandontime,
                               fr.creatorname,
                               fr.createtime,
                               fr.modifiername,
                               fr.modifytime,
                               fr.ValidateFrom
                             
                          from ForceReserveBill fr
                         
                         where fr.companyType=" + companyType ;
            if (ids != null && ids.Length > 0)
            {
                SQL = SQL + " and fr.id in (";
                string statusStr = "";
                for (var i = 0; i < ids.Length; i++)
                {
                    statusStr = statusStr + ids[i] + ",";
                }
                statusStr = statusStr.Substring(0, statusStr.Length - 1);
                SQL = SQL + statusStr + ")";
            }
            else
            {
                if (status.HasValue)
                {
                    SQL = SQL + " and fr.Status =" + status;
                }
                if (!string.IsNullOrEmpty(subVersionCode))
                {
                    SQL = SQL + " and  Lower(fr.SubVersionCode) like'%" + subVersionCode.ToLower() + "%'";
                }
                if (!string.IsNullOrEmpty(reserveTypeSubItem))
                {
                    SQL = SQL + " and  Lower(fr.reserveTypeSubItem) like'%" + reserveTypeSubItem.ToLower() + "%'";
                }
                if (!string.IsNullOrEmpty(colVersionCode))
                {
                    SQL = SQL + " and  Lower(fr.colVersionCode) like'%" + colVersionCode.ToLower() + "%'";
                }
                if (!string.IsNullOrEmpty(companyCode))
                {
                    SQL = SQL + " and  Lower(fr.companyCode) like'%" + companyCode.ToLower() + "%'";
                }
                if (!string.IsNullOrEmpty(companyName))
                {
                    SQL = SQL + " and fr.companyName like'%" + companyName + "%'";
                }
                if (!string.IsNullOrEmpty(reserveType))
                {
                    SQL = SQL + " and  Lower(fr.reserveType) like'%" + reserveType.ToLower() + "%'";
                }
                if (bCreateTime.HasValue)
                {
                    SQL = SQL + " and fr.CreateTime>= to_date('" + bCreateTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
                }
                if (eCreateTime.HasValue)
                {
                    SQL = SQL + " and fr.CreateTime<= to_date('" + eCreateTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
                }
                if (bApproveTime.HasValue)
                {
                    SQL = SQL + " and fr.ApproveTime>= to_date('" + bApproveTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
                }
                if (eApproveTime.HasValue)
                {
                    SQL = SQL + " and fr.ApproveTime<= to_date('" + eApproveTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
                }
            }
            //var company = ObjectContext.Companies.Where(r => r.Id == userInfo.EnterpriseId).FirstOrDefault();
            //if (company.Type == (int)DcsCompanyType.分公司)
            //{
            //    //判断是否维护了人员与分销中心关系
            //    var isHas = ObjectContext.MarketDptPersonnelRelations.Where(r => r.PersonnelId == userInfo.Id && r.Status == (int)DcsBaseDataStatus.有效).Count();
            //    if (isHas > 0)
            //    {
            //        SQL = SQL + " and  (exists(select 1 from  MarketDptPersonnelRelation mr join Agency ag on mr.marketdepartmentid = ag.marketingdepartmentid  join AgencyDealerRelation ar on ag.id = ar.agencyid  where mr.PersonnelId = " + userInfo.Id + " and  ar.dealerid = fr.companyid and mr.status=1) )";
            //    }
            //}
            //else if (company.Type == (int)DcsCompanyType.代理库)
            //{
            //    SQL = SQL + " and  exists(select 1 from AgencyDealerRelation ar where ar.status=1 and  fr.companyid=ar.DealerId and ar.AgencyId=" + userInfo.EnterpriseId + ")";
            //}
            //else
            //{
            //    SQL = SQL + " fr.companyid=" + userInfo.EnterpriseId;
            //}
            var search = ObjectContext.ExecuteStoreQuery<ForceReserveBillDealerMasterExport>(SQL).ToList();
            return search;
        }
        public IQueryable<ForceReserveBill> GetForceReserveBillsByTime() {
            return ObjectContext.ForceReserveBills.Include("ForceReserveBillDetails").OrderByDescending(e => e.CreateTime);
        } 
 
      
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:09:49

        public void InsertForceReserveBill(ForceReserveBill apps) {
            new ForceReserveBillAch(this).InsertForceReserveBill(apps);
        }

        public void UpdateForceReserveBill(ForceReserveBill apps) {
            new ForceReserveBillAch(this).UpdateForceReserveBill(apps);
        }
        [Invoke(HasSideEffects = true)]
        public void 作废强制储备单(int[] ids) {
            new ForceReserveBillAch(this).作废强制储备单(ids);
        }
        [Invoke(HasSideEffects = true)]
        public void 提交强制储备单(int[] ids) {
            new ForceReserveBillAch(this).提交强制储备单(ids);
        }
        [Update(UsingCustomMethod = true)]
        public void 更新强制储备单(ForceReserveBill bill) {
            new ForceReserveBillAch(this).更新强制储备单(bill);
        }
        [Update(UsingCustomMethod = true)]
        public void 驳回强制储备单(ForceReserveBill bill) {
            new ForceReserveBillAch(this).驳回强制储备单(bill);
        }
        [Invoke(HasSideEffects = true)]
        public void 批量审核强制储备单(int[] ids) {
            new ForceReserveBillAch(this).批量审核强制储备单(ids);
        }
        [Update(UsingCustomMethod = true)]
        public void 审核强制储备单(ForceReserveBill Bbill) {
            new ForceReserveBillAch(this).审核强制储备单(Bbill);
        }
        [Update(UsingCustomMethod = true)]
        public void 审批强制储备单(ForceReserveBill Bbill) {
            new ForceReserveBillAch(this).审批强制储备单(Bbill);
        }
        [Invoke(HasSideEffects = true)]
        public void 批量审批强制储备单(int[] ids) {
            new ForceReserveBillAch(this).批量审批强制储备单(ids);
        }
        public ForceReserveBill GetForceReserveBillsWithDetailById(int id) {
            return new ForceReserveBillAch(this).GetForceReserveBillsWithDetailById(id);
        }
        public IQueryable<ForceReserveBill> GetForceReserveBillsForDealer(string centerName,int? status,string subVersionCode,string colVersionCode,string reserveType,string reserveTypeSubItem,string companyCode,string companyName) {
            return new ForceReserveBillAch(this).GetForceReserveBillsForDealer(centerName,status,subVersionCode,colVersionCode,reserveType,reserveTypeSubItem,companyCode,companyName);
        }
        [Update(UsingCustomMethod = true)]
        public void 确认强制储备单(ForceReserveBill Bbill) {
            new ForceReserveBillAch(this).确认强制储备单(Bbill);
        }
        [Invoke(HasSideEffects = true)]
        public void 批量确认强制储备单(int[] ids) {
            new ForceReserveBillAch(this).批量确认强制储备单(ids);
        }
        public IQueryable<ForceReserveBillDetailQuery> GetForceReserveBillDetailsOrder(int forceReserveBillId) {
            return new ForceReserveBillAch(this).GetForceReserveBillDetailsOrder(forceReserveBillId);
       }
        public IEnumerable<ForceReserveBillDealerExport> GetForceReserveBillsForDealerExport(int[] ids, int? status, string subVersionCode, string colVersionCode, string reserveTypeSubItem, string companyCode, string companyName, DateTime? bCreateTime, DateTime? eCreateTime, DateTime? bApproveTime, DateTime? eApproveTime, string reserveType,string centerName) {
            return new ForceReserveBillAch(this).GetForceReserveBillsForDealerExport(ids, status, subVersionCode, colVersionCode, reserveTypeSubItem, companyCode, companyName, bCreateTime, eCreateTime, bApproveTime, eApproveTime,  reserveType,centerName);
        }
        public IEnumerable<ForceReserveBillDealerMasterExport> GetForceReserveBillDealerMasterExport(int[] ids, int? status, string subVersionCode, string colVersionCode, string reserveTypeSubItem, string companyCode, string companyName, DateTime? bCreateTime, DateTime? eCreateTime, DateTime? bApproveTime, DateTime? eApproveTime, string reserveType,string centerName)
        {
            return new ForceReserveBillAch(this).GetForceReserveBillDealerMasterExport(ids, status, subVersionCode, colVersionCode, reserveTypeSubItem, companyCode, companyName, bCreateTime, eCreateTime, bApproveTime, eApproveTime, reserveType,centerName);
        }

        public IEnumerable<ForceReserveBillDealerMasterExport> GetForceReserveBillCenterMasterExport(int[] ids, int? status, string subVersionCode, string colVersionCode, string reserveTypeSubItem, string companyCode, string companyName, DateTime? bCreateTime, DateTime? eCreateTime, DateTime? bApproveTime, DateTime? eApproveTime, int companyType, string reserveType) 
        {
            return new ForceReserveBillAch(this).GetForceReserveBillCenterMasterExport( ids, status,  subVersionCode,  colVersionCode,  reserveTypeSubItem,  companyCode,  companyName,  bCreateTime, eCreateTime,  bApproveTime,  eApproveTime,  companyType,  reserveType);
        }
        public IQueryable<ForceReserveBill> GetForceReserveBillsByTime() {
            return new ForceReserveBillAch(this).GetForceReserveBillsByTime();
        }
    }
}
