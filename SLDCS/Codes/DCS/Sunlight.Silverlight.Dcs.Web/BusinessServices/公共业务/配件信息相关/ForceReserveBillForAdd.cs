﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
using System.Collections.Generic;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
         [Invoke]
        public void 新增强制储备单(ForceReserveBill bill, List<ForceReserveBillDetailTemp> Tempdetails) {
            var company = Tempdetails.Select(r => r.CompanyId).Distinct().ToArray(); ;
            var versionInfoes = ObjectContext.VersionInfoes.Where(a=> company.Contains(a.CompanyId) && a.Type == (int)DCSVersionInfoType.子集).ToArray();
            foreach(var companyId in company) {
                //主单
                var newBill = this.setBill(bill, Tempdetails.Where(r => r.CompanyId == companyId).First(), versionInfoes.Where(t => t.CompanyId == companyId && t.BottomStockForceReserveSubId == bill.ReserveTypeSubItemId).FirstOrDefault());
                //清单
                var details = Tempdetails.Where(r => r.CompanyId == companyId).ToArray();
                foreach(var item in details) {
                    var billDetail = new ForceReserveBillDetail {
                        SparePartId = item.SparePartId,
                        SparePartCode = item.SparePartCode,
                        SparePartName = item.SparePartName,
                        SuggestForceReserveQty = item.SuggestForceReserveQty,
                        ForceReserveQty = item.ForceReserveQty,
                        CenterPrice = item.CenterPrice,
                        ReserveFee = item.CenterPrice * item.ForceReserveQty,
                        CenterPartProperty = item.CenterPartProperty,
                    };
                    newBill.ForceReserveBillDetails.Add(billDetail);
                }
                InsertToDatabase(newBill);
            }
            ObjectContext.SaveChanges();

        }
        private ForceReserveBill setBill(ForceReserveBill bill, ForceReserveBillDetailTemp temp, VersionInfo versionInfoes) {
            var newBill = new ForceReserveBill();
            newBill.CompanyId = temp.CompanyId;
            newBill.CompanyCode = temp.CompanyCode;
            newBill.CompanyName = temp.CompanyName;
            newBill.Status = (int)DCSForceReserveBillStatus.新建;
            newBill.ReserveTypeId = bill.ReserveTypeId;
            newBill.ReserveType = bill.ReserveType;
            newBill.ReserveTypeSubItemId = bill.ReserveTypeSubItemId;
            newBill.ReserveTypeSubItem = bill.ReserveTypeSubItem;
            var userInfo = Utils.GetCurrentUserInfo();
            newBill.CreateTime = DateTime.Now;
            newBill.CreatorId = userInfo.Id;
            newBill.CreatorName = userInfo.Name;
            newBill.Path = bill.Path;
            newBill.CompanyType = bill.CompanyType;
            newBill.ValidateFrom = bill.ValidateFrom;
            if(versionInfoes == null) {
                var version = new VersionInfo {
                    CompanyId = temp.CompanyId,
                    CompanyCode = temp.CompanyCode,
                    Type = (int)DCSVersionInfoType.子集,
                    SerialCode = 1,
                    BottomStockForceReserveSubId = bill.ReserveTypeSubItemId
                };
                newBill.SubVersionCode = temp.CompanyCode + bill.ReserveTypeSubItem + "0001";
                InsertToDatabase(version);
            } else {
                newBill.SubVersionCode = temp.CompanyCode + bill.ReserveTypeSubItem + "000" + (versionInfoes.SerialCode + 1);
                versionInfoes.SerialCode = versionInfoes.SerialCode + 1;
                UpdateToDatabase(versionInfoes);
            }

            return newBill;
        }
      

    }
}
