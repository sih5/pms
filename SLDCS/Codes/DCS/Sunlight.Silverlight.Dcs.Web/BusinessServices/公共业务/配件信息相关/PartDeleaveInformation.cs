﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartDeleaveInformationAch : DcsSerivceAchieveBase {
        public PartDeleaveInformationAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 作废配件拆散信息(PartDeleaveInformation partDeleaveInformation) {
            var dbPartDeleaveInformation = ObjectContext.PartDeleaveInformations.Where(r => r.Id == partDeleaveInformation.Id && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbPartDeleaveInformation == null)
                throw new ValidationException(ErrorStrings.PartDeleaveInformation_Validation3);
            UpdateToDatabase(partDeleaveInformation);
            partDeleaveInformation.Status = (int)DcsBaseDataStatus.作废;
            var userInfo = Utils.GetCurrentUserInfo();
            partDeleaveInformation.ModifierId = userInfo.Id;
            partDeleaveInformation.ModifyTime = DateTime.Now;
            partDeleaveInformation.ModifierName = userInfo.Name;
            this.UpdatePartDeleaveInformationValidate(partDeleaveInformation);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:09:49
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 作废配件拆散信息(PartDeleaveInformation partDeleaveInformation) {
            new PartDeleaveInformationAch(this).作废配件拆散信息(partDeleaveInformation);
        }
    }
}
