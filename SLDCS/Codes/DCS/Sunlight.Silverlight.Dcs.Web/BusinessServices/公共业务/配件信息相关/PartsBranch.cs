﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsBranchAch : DcsSerivceAchieveBase {
        public PartsBranchAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 作废配件营销信息(PartsBranch partsBranch) {
            var dbpartsBranch = ObjectContext.PartsBranches.Where(r => r.Id == partsBranch.Id && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbpartsBranch == null)
                throw new ValidationException(ErrorStrings.PartsBranch_Validation3);
            UpdateToDatabase(partsBranch);
            partsBranch.Status = (int)DcsBaseDataStatus.作废;
            var userInfo = Utils.GetCurrentUserInfo();
            partsBranch.AbandonerId = userInfo.Id;
            partsBranch.AbandonTime = DateTime.Now;
            partsBranch.AbandonerName = userInfo.Name;
            this.UpdatePartsBranchValidate(partsBranch);
        }

        public void 停用配件营销信息(PartsBranch partsBranch) {
            var dbpartsBranch = ObjectContext.PartsBranches.Where(r => r.Id == partsBranch.Id && r.Status == (int)DcsMasterDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbpartsBranch == null)
                throw new ValidationException(ErrorStrings.PartsBranch_Validation3);
            UpdateToDatabase(partsBranch);
            partsBranch.Status = (int)DcsMasterDataStatus.停用;
            var userInfo = Utils.GetCurrentUserInfo();
            partsBranch.ModifierId = userInfo.Id;
            partsBranch.ModifyTime = DateTime.Now;
            partsBranch.ModifierName = userInfo.Name;
            this.UpdatePartsBranchValidate(partsBranch);
        }

        public void 恢复配件营销信息(PartsBranch partsBranch) {
            var dbpartsBranch = ObjectContext.PartsBranches.Where(r => r.Id == partsBranch.Id && r.Status == (int)DcsMasterDataStatus.停用).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbpartsBranch == null)
                throw new ValidationException(ErrorStrings.PartsBranch_Validation3);
            UpdateToDatabase(partsBranch);
            partsBranch.Status = (int)DcsMasterDataStatus.有效;
            var userInfo = Utils.GetCurrentUserInfo();
            partsBranch.ModifierId = userInfo.Id;
            partsBranch.ModifyTime = DateTime.Now;
            partsBranch.ModifierName = userInfo.Name;
            this.UpdatePartsBranchValidate(partsBranch);
        }

        internal void CreatePartsBranch(PartsBranch partsBranch, PartsSalesCategory partsSalesCategory) {
            var newPartsBranch = new PartsBranch();
            newPartsBranch.PartId = partsBranch.PartId;
            newPartsBranch.PartCode = partsBranch.PartCode;
            newPartsBranch.PartName = partsBranch.PartName;
            newPartsBranch.ReferenceCode = partsBranch.ReferenceCode;
            newPartsBranch.BranchId = partsBranch.BranchId;
            newPartsBranch.BranchName = partsBranch.BranchName;
            newPartsBranch.PartsSalesCategoryId = partsSalesCategory.Id;
            newPartsBranch.PartsSalesCategoryName = partsSalesCategory.Name;
            newPartsBranch.ProductLifeCycle = partsBranch.ProductLifeCycle;
            newPartsBranch.PartsAttribution = partsBranch.PartsAttribution;
            newPartsBranch.IsOrderable = partsBranch.IsOrderable;
            newPartsBranch.IsSalable = partsBranch.IsSalable;
            newPartsBranch.IsDirectSupply = partsBranch.IsDirectSupply;
            newPartsBranch.AutoApproveUpLimit = partsBranch.AutoApproveUpLimit;
            newPartsBranch.PartsWarhouseManageGranularity = (int)DcsPartsBranchPartsWarhouseManageGranularity.无批次管理;
            newPartsBranch.Status = (int)DcsBaseDataStatus.有效;
            newPartsBranch.Remark = partsBranch.Remark;
            newPartsBranch.PartsReturnPolicy = (int)DcsPartsWarrantyTermReturnPolicy.返回本部;
            newPartsBranch.MinSaleQuantity = 1;
            InsertToDatabase(newPartsBranch);
            this.InsertPartsBranchValidate(newPartsBranch);
        }


        public void 同步配件营销信息(PartsBranch partsBranch) {
            var code = new[] { "104011", "102012", "102112", "102111" };
            var code1 = new[] { "999998", "999999", "999990", "999991" };
            var code2 = new[] { "999998", "999990" };
            var code4 = new[] { "999999", "999990" };

            if(this.ObjectContext.PartsSalesCategories.Any(r => code.Contains(r.Code) && r.Status == (int)DcsBaseDataStatus.有效 && r.Id == partsBranch.PartsSalesCategoryId)) {
                var partsSalesCatego = ObjectContext.PartsSalesCategories.Where(x => code1.Contains(x.Code) && x.Status == (int)DcsBaseDataStatus.有效);
                if(partsSalesCatego != null) {
                    var partsSalesCategoArr = partsSalesCatego.ToArray();
                    foreach(var partsSalesCategory in partsSalesCategoArr) {
                        if(!this.ObjectContext.PartsBranches.Any(r => r.PartId == partsBranch.PartId && r.Status == (int)DcsBaseDataStatus.有效 && r.PartsSalesCategoryId == partsSalesCategory.Id)) {
                            if(partsSalesCategory.Code == "999999" || partsSalesCategory.Code == "999991")//集团配件
                            {
                                partsBranch.IsSalable = true;
                                partsBranch.IsOrderable = true;
                                partsBranch.IsDirectSupply = false;
                                //partsBranch.AutoApproveUpLimit = 100;
                            } else if(partsSalesCategory.Code == "999998" || partsSalesCategory.Code == "999990")//电子商务 红岩配件
                            {
                                partsBranch.IsSalable = true;
                                partsBranch.IsOrderable = false;
                                partsBranch.IsDirectSupply = false;
                                //partsBranch.AutoApproveUpLimit = 100;
                            }
                            CreatePartsBranch(partsBranch, partsSalesCategory);
                        }
                    }
                }
            }
            if(ObjectContext.PartsSalesCategories.Any(r => r.Id == partsBranch.PartsSalesCategoryId && r.Code == "999998" && r.Status == (int)DcsBaseDataStatus.有效)) {
                var partsSalesCategorys = ObjectContext.PartsSalesCategories.Where(x => code4.Contains(x.Code) && x.Status == (int)DcsBaseDataStatus.有效).ToArray();
                foreach(var partsSalesCategory in partsSalesCategorys) {
                    if(!this.ObjectContext.PartsBranches.Any(r => r.PartId == partsBranch.PartId && r.Status == (int)DcsBaseDataStatus.有效 && r.PartsSalesCategoryId == partsSalesCategory.Id))
                        CreatePartsBranch(partsBranch, partsSalesCategory);
                }

            }
            if(ObjectContext.PartsSalesCategories.Any(r => r.Id == partsBranch.PartsSalesCategoryId && r.Code == "999999" && r.Status == (int)DcsBaseDataStatus.有效)) {
                var partsSalesCategorys = ObjectContext.PartsSalesCategories.Where(x => code2.Contains(x.Code) && x.Status == (int)DcsBaseDataStatus.有效).ToArray();
                foreach(var partsSalesCategory in partsSalesCategorys) {
                    if(!this.ObjectContext.PartsBranches.Any(r => r.PartId == partsBranch.PartId && r.Status == (int)DcsBaseDataStatus.有效 && r.PartsSalesCategoryId == partsSalesCategory.Id))
                        CreatePartsBranch(partsBranch, partsSalesCategory);
                }
            }
            //if(ObjectContext.PartsSalesCategories.Any(r => r.Id == partsBranch.PartsSalesCategoryId && r.Code == "999990" && r.Status == (int)DcsBaseDataStatus.有效)) {
            //    var partsSalesCategorys = ObjectContext.PartsSalesCategories.Where(x => code3.Contains(x.Code) && x.Status == (int)DcsBaseDataStatus.有效).ToArray();
            //    foreach(var partsSalesCategory in partsSalesCategorys) {
            //        if(!this.ObjectContext.PartsBranches.Any(r => r.PartId == partsBranch.PartId && r.Status == (int)DcsBaseDataStatus.有效 && r.PartsSalesCategoryId == partsSalesCategory.Id))
            //            CreatePartsBranch(partsBranch, partsSalesCategory);
            //    }

            //}
        }
    
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:09:49
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 作废配件营销信息(PartsBranch partsBranch) {
            new PartsBranchAch(this).作废配件营销信息(partsBranch);
        }

        [Update(UsingCustomMethod = true)]
        public void 停用配件营销信息(PartsBranch partsBranch) {
            new PartsBranchAch(this).停用配件营销信息(partsBranch);
        }

        [Update(UsingCustomMethod = true)]
        public void 恢复配件营销信息(PartsBranch partsBranch) {
            new PartsBranchAch(this).恢复配件营销信息(partsBranch);
        }
        

        [Update(UsingCustomMethod = true)]
        public void 同步配件营销信息(PartsBranch partsBranch) {
            new PartsBranchAch(this).同步配件营销信息(partsBranch);
        }
    }
}
