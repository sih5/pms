﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
using System.Collections.Generic;

namespace Sunlight.Silverlight.Dcs.Web
{
    partial class DcsDomainService
    {
        [Update(UsingCustomMethod = true)]
        public void 作废配件互换信息虚拟(VirtualPartsExchange partsExchange)
        {
            var dbPartsExchange = ObjectContext.PartsExchanges.Where(r => r.Id == partsExchange.Id && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if (dbPartsExchange == null) {
                throw new ValidationException(ErrorStrings.PartsExchange_Validation1);
            }
            dbPartsExchange.Status = (int)DcsBaseDataStatus.作废;
            var userInfo = Utils.GetCurrentUserInfo();
            dbPartsExchange.AbandonerId = userInfo.Id;
            dbPartsExchange.AbandonerName = userInfo.Name;
            dbPartsExchange.AbandonTime = DateTime.Now;
            UpdateToDatabase(dbPartsExchange);
        }
        [Update(UsingCustomMethod = true)]
        public void 作废配件互换信息(PartsExchange partsExchange) {
            var dbPartsExchange = ObjectContext.PartsExchanges.Where(r => r.Id == partsExchange.Id && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbPartsExchange == null) {
                throw new ValidationException(ErrorStrings.PartsExchange_Validation1);
            }
            partsExchange.Status = (int)DcsBaseDataStatus.作废;
            var userInfo = Utils.GetCurrentUserInfo();
            partsExchange.AbandonerId = userInfo.Id;
            partsExchange.AbandonerName = userInfo.Name;
            partsExchange.AbandonTime = DateTime.Now;
            UpdateToDatabase(dbPartsExchange);
        }
       [Invoke]
        public void 生成配件互换信息(Dictionary<int, string> partsids)
        {
            string exchangeCode = CodeGenerator.Generate("PartsExchange");
            //2、校验配件是否存在互换信息，查询配件互换信息，
            //2.1、如果存在，返回与该配件相同互换号相同的所有配件互换信息.Id,赋值：互换号=最新生成的互换号
            //2.2、如果不存在，调用新增配件互换信息
            foreach (var id in partsids) {
                var partsExchangeCode = ObjectContext.PartsExchanges.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.PartId == id.Key).Select(r => r.ExchangeCode).ToArray();
                //var sparepart = ObjectContext.SpareParts.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.Id == id.Key).FirstOrDefault();
                if (partsExchangeCode != null && partsExchangeCode.Length > 0) {
                    var partsExchangeforUpdate = ObjectContext.PartsExchanges.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && partsExchangeCode.Contains(r.ExchangeCode)).ToArray();
                    foreach (var p in partsExchangeforUpdate) {
                        p.ExchangeCode = exchangeCode;
                        p.Remark = id.Value;
                        UpdateToDatabase(p);
                        new PartsExchangeAch(this).UpdatePartsExchangeValdate(p);
                        //sparepart.ExchangeIdentification = exchangeCode;
                        //UpdateToDatabase(sparepart);
                    }
                }
                else {
                    var newPartExchange = new PartsExchange();
                    newPartExchange.ExchangeCode = exchangeCode;
                    newPartExchange.PartId = id.Key;
                    newPartExchange.Remark = id.Value;
                    newPartExchange.Status = (int)DcsBaseDataStatus.有效;
                    InsertToDatabase(newPartExchange);
                    new PartsExchangeAch(this).InsertPartsExchangeValidate(newPartExchange);
                    //sparepart.ExchangeIdentification = exchangeCode;
                    //UpdateToDatabase(sparepart);
                }
            }
            ObjectContext.SaveChanges();
        }

       [Invoke]
       public void 调整配件互换信息(Dictionary<int, string> partsids, string exchangeCode)
       {
           //string exchangeCode = CodeGenerator.Generate("PartsExchange");
           //2、校验配件是否存在互换信息，查询配件互换信息，
           //2.1、如果存在，返回与该配件相同互换号相同的所有配件互换信息.Id,赋值：互换号=最新生成的互换号
           //2.2、如果不存在，调用新增配件互换信息
           foreach (var id in partsids) {
               var partsExchangeCode = ObjectContext.PartsExchanges.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.PartId == id.Key).Select(r => r.ExchangeCode).ToArray();
               //var sparepart = ObjectContext.SpareParts.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.Id == id.Key).FirstOrDefault();
               if (partsExchangeCode != null && partsExchangeCode.Length > 0) {
                   var partsExchangeforUpdate = ObjectContext.PartsExchanges.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && partsExchangeCode.Contains(r.ExchangeCode)).ToArray();
                   foreach (var p in partsExchangeforUpdate) {
                       p.ExchangeCode = exchangeCode;
                       p.Remark = id.Value;
                       UpdateToDatabase(p);
                       new PartsExchangeAch(this).UpdatePartsExchangeValdate(p);
                   }
               }
               else {
                   var newPartExchange = new PartsExchange();
                   newPartExchange.ExchangeCode = exchangeCode;
                   newPartExchange.PartId = id.Key;
                   newPartExchange.Remark = id.Value;
                   newPartExchange.Status = (int)DcsBaseDataStatus.有效;
                   InsertToDatabase(newPartExchange);
                   new PartsExchangeAch(this).InsertPartsExchangeValidate(newPartExchange);
               }
           }
           ObjectContext.SaveChanges();
       }



    }
}
