﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsExchangeGroupAch : DcsSerivceAchieveBase {
        public void 新增修改配件互换组信息(List<PartsExchangeGroup> partsExchangeGroups) {
            var userInfo = Utils.GetCurrentUserInfo();
            var exchangeCodes = partsExchangeGroups.Select(v => v.ExchangeCode);
            var oldPartsExchangeGroups = ObjectContext.PartsExchangeGroups.Where(r => exchangeCodes.Contains(r.ExchangeCode) && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).ToArray();
            var oldExGroups = oldPartsExchangeGroups.Select(v => v.ExGroupCode);
            var exGroupCode = partsExchangeGroups.FirstOrDefault().ExGroupCode;
            var oldExGroup = new List<String>();
            oldExGroup = oldExGroups.ToList();
            oldExGroup.Add(exGroupCode);
            var oldPartsExGroups = ObjectContext.PartsExchangeGroups.Where(r => oldExGroup.Contains(r.ExGroupCode) && r.Status == (int)DcsMasterDataStatus.有效).SetMergeOption(MergeOption.NoTracking).ToArray();

            

            if(string.IsNullOrWhiteSpace(exGroupCode) || exGroupCode == GlobalVar.ASSIGNED_BY_SERVER) {
                //var deletePartsExchangeGroups = oldPartsExchangeGroups.Where(v => exGroupCode != v.ExGroupCode);
                foreach(var deletePartsExchangeGroup in oldPartsExGroups) {
                    deletePartsExchangeGroup.Status = (int)DcsBaseDataStatus.作废;
                    deletePartsExchangeGroup.AbandonerId = userInfo.Id;
                    deletePartsExchangeGroup.AbandonTime = DateTime.Now;
                    deletePartsExchangeGroup.AbandonerName = userInfo.Name;
                    this.UpdatePartsExchangeGroup(deletePartsExchangeGroup);
                }
                var code = CodeGenerator.Generate("PartsExchange");
                //var newinsertPartsExchangeGroups = partsExchangeGroups.Where(v => !oldPartsExGroups.Select(o => o.ExchangeCode).Contains(v.ExchangeCode));
                foreach(var item in partsExchangeGroups) {
                    var partsExchangeGroup = new PartsExchangeGroup();
                    partsExchangeGroup.ExGroupCode = code;
                    partsExchangeGroup.ExchangeCode = item.ExchangeCode;
                    partsExchangeGroup.Status = (int)DcsBaseDataStatus.有效;
                    partsExchangeGroup.Remark = item.Remark;
                    InsertPartsExchangeGroup(partsExchangeGroup);
                    InsertToDatabase(partsExchangeGroup);

                }
                var oldinsertPartsExchangeGroups = oldPartsExGroups.Where(v => !exchangeCodes.Contains(v.ExchangeCode));
                foreach(var item in oldinsertPartsExchangeGroups) {
                    var partsExchangeGroup = new PartsExchangeGroup();
                    partsExchangeGroup.ExGroupCode = code;
                    partsExchangeGroup.ExchangeCode = item.ExchangeCode;
                    partsExchangeGroup.Status = (int)DcsBaseDataStatus.有效;
                    partsExchangeGroup.Remark = item.Remark;
                    InsertPartsExchangeGroup(partsExchangeGroup);
                    InsertToDatabase(partsExchangeGroup);

                }
            } else {

                var deletePartsExchangeGroups = oldPartsExGroups.Where(v => !exchangeCodes.Contains(v.ExchangeCode));
                var deleteExchangeGroups = deletePartsExchangeGroups.Select(v=>v.ExchangeCode);
                var spareParts = ObjectContext.SpareParts.Where(r => deleteExchangeGroups.Contains(r.ExchangeIdentification) && r.Status == (int)DcsBaseDataStatus.有效).ToArray();
                var partsExchanges = ObjectContext.PartsExchanges.Where(r => deleteExchangeGroups.Contains(r.ExchangeCode) && r.Status == (int)DcsBaseDataStatus.有效).ToArray();
                var result = from a in deletePartsExchangeGroups
                             join c in partsExchanges on a.ExchangeCode equals c.ExchangeCode into temptable1
                             from t in temptable1.DefaultIfEmpty()
                             join b in spareParts on t.PartId equals b.Id into temptable2
                             from t2 in temptable2.DefaultIfEmpty()
                             select new VirtualPartsExchangeGroup {
                                 Id = t2.Id,
                                 SparePartCode = t2.Code,
                                 SparePartName = t2.Name,
                                 ReferenceCode = t2.ReferenceCode,
                                 ExchangeCode = a.ExchangeCode,
                                 PartsExchangeGroupId = a.Id
                             };
                var exchangeGroup = result.GroupBy(v => v.ExchangeCode).ToArray();
                var exGroupCode1 = CodeGenerator.Generate("PartsExchange");
                foreach(var deletePartsExchangeGroup in deletePartsExchangeGroups) {
                    deletePartsExchangeGroup.Status = (int)DcsBaseDataStatus.作废;
                    deletePartsExchangeGroup.AbandonerId = userInfo.Id;
                    deletePartsExchangeGroup.AbandonTime = DateTime.Now;
                    deletePartsExchangeGroup.AbandonerName = userInfo.Name;
                    this.UpdatePartsExchangeGroup(deletePartsExchangeGroup);
                    foreach(var exchangeCode in exchangeGroup.Where(v=>v.Key == deletePartsExchangeGroup.ExchangeCode)) {
                        if(exchangeCode.Count() > 1) {
                            var newPartsExchangeGroup = new PartsExchangeGroup();
                            newPartsExchangeGroup.ExchangeCode = exchangeCode.FirstOrDefault().ExchangeCode;
                            newPartsExchangeGroup.Status = (int)DcsBaseDataStatus.有效;
                            newPartsExchangeGroup.ExGroupCode = exGroupCode1;
                            InsertPartsExchangeGroup(newPartsExchangeGroup);
                            InsertToDatabase(newPartsExchangeGroup);
                        }
                    }
                }
                //partsExchangeGroups.Where(v => oldPartsExchangeGroups.Select(o => o.ExGroupCode).Contains(v.ExGroupCode) && oldPartsExchangeGroups.Select(o => o.ExchangeCode).Contains(v.ExchangeCode));
                var updatePartsExchangeGroups = oldPartsExGroups.Where(v => !deletePartsExchangeGroups.Select(o => o.ExchangeCode).Contains(v.ExchangeCode));
                foreach(var updatePartsExchangeGroup in updatePartsExchangeGroups) {
                    var partsExchangeGroup = partsExchangeGroups.FirstOrDefault(v => v.ExchangeCode == updatePartsExchangeGroup.ExchangeCode);
                    updatePartsExchangeGroup.ExGroupCode = exGroupCode;
                    updatePartsExchangeGroup.Status = (int)DcsBaseDataStatus.有效;
                    updatePartsExchangeGroup.Remark = partsExchangeGroup.Remark;
                    this.UpdatePartsExchangeGroup(updatePartsExchangeGroup);
                }
                var insertPartsExchangeGroups = partsExchangeGroups.Where(v => !oldPartsExGroups.Select(o => o.ExchangeCode).Contains(v.ExchangeCode));
                foreach(var item in insertPartsExchangeGroups) {
                    var partsExchangeGroup = new PartsExchangeGroup();
                    partsExchangeGroup.ExGroupCode = exGroupCode;
                    partsExchangeGroup.ExchangeCode = item.ExchangeCode;
                    partsExchangeGroup.Status = (int)DcsBaseDataStatus.有效;
                    partsExchangeGroup.Remark = item.Remark;
                    InsertPartsExchangeGroupValidate(partsExchangeGroup);
                    InsertToDatabase(partsExchangeGroup);

                }
            }
            ObjectContext.SaveChanges();
        }
        public void 作废配件互换组信息(PartsExchangeGroup partsExchangeGroup) {
            var dbpartsExchangeGroups = ObjectContext.PartsExchangeGroups.Where(r => r.ExGroupCode == partsExchangeGroup.ExGroupCode && r.Status == (int)DcsMasterDataStatus.有效).SetMergeOption(MergeOption.NoTracking);
            if(dbpartsExchangeGroups == null)
                throw new ValidationException(ErrorStrings.SparePart_Validation2);

            var result = from a in dbpartsExchangeGroups
                         join c in ObjectContext.PartsExchanges.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on a.ExchangeCode equals c.ExchangeCode into temptable1
                         from t in temptable1.DefaultIfEmpty()
                         join b in ObjectContext.SpareParts.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on t.PartId equals b.Id into temptable2
                         from t2 in temptable2.DefaultIfEmpty()
                         select new VirtualPartsExchangeGroup {
                             Id = t2.Id,
                             SparePartCode = t2.Code,
                             SparePartName = t2.Name,
                             ReferenceCode = t2.ReferenceCode,
                             ExchangeCode = a.ExchangeCode,
                             PartsExchangeGroupId = a.Id
                         };

            var userInfo = Utils.GetCurrentUserInfo();
            foreach(var dbpartsExchangeGroup in dbpartsExchangeGroups) {
                dbpartsExchangeGroup.Status = (int)DcsBaseDataStatus.作废;
                
                dbpartsExchangeGroup.AbandonerId = userInfo.Id;
                dbpartsExchangeGroup.AbandonTime = DateTime.Now;
                dbpartsExchangeGroup.AbandonerName = userInfo.Name;
                this.UpdatePartsExchangeGroup(dbpartsExchangeGroup);
            }
            var exchangeCodes = result.GroupBy(v => v.ExchangeCode);
            //var exGroupCode1 = CodeGenerator.Generate("PartsExchange");
            
            foreach(var exchangeCode in exchangeCodes) {
                if(exchangeCode.Count() > 1) {
                    var exGroupCode = CodeGenerator.Generate("PartsExchange");
                    if(exchangeCode.FirstOrDefault().ExchangeCode == partsExchangeGroup.ExchangeCode) {
                        var newPartsExchangeGroup = new PartsExchangeGroup();
                        newPartsExchangeGroup.ExchangeCode = exchangeCode.FirstOrDefault().ExchangeCode;
                        newPartsExchangeGroup.Status = (int)DcsBaseDataStatus.有效;
                        newPartsExchangeGroup.ExGroupCode = exGroupCode;
                        InsertPartsExchangeGroup(newPartsExchangeGroup);
                        InsertToDatabase(newPartsExchangeGroup);
                    } else {
                        var newPartsExchangeGroup = new PartsExchangeGroup();
                        newPartsExchangeGroup.ExchangeCode = exchangeCode.FirstOrDefault().ExchangeCode;
                        newPartsExchangeGroup.Status = (int)DcsBaseDataStatus.有效;
                        newPartsExchangeGroup.ExGroupCode = exGroupCode;
                        InsertPartsExchangeGroup(newPartsExchangeGroup);
                        InsertToDatabase(newPartsExchangeGroup);
                    }
                }
            }
        }
    }
    partial class DcsDomainService {
        [Invoke]
        public void 新增修改配件互换组信息(List<PartsExchangeGroup> partsExchangeGroups) {
            new PartsExchangeGroupAch(this).新增修改配件互换组信息(partsExchangeGroups);
        }
        public void 作废配件互换组信息(PartsExchangeGroup partsExchangeGroup) {
            new PartsExchangeGroupAch(this).作废配件互换组信息(partsExchangeGroup);
        }
    }
}
