﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
using System.Configuration;
using System.Data;
using System.Text;
using Devart.Data.Oracle;
using NPOI.SS.Formula.Functions;

namespace Sunlight.Silverlight.Dcs.Web
{
    partial class PartsPackingPropertyAppAch : DcsSerivceAchieveBase
    {
        public PartsPackingPropertyAppAch(DcsDomainService domainService)
            : base(domainService)
        {

        }
        public void 驳回包装属性申请单(PartsPackingPropertyApp app) { 
            var dbPartsPackingPropertyApp = ObjectContext.PartsPackingPropertyApps.Where(v => v.Id == app.Id && v.Status == (int)DcsPackingPropertyAppStatus.提交).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if (dbPartsPackingPropertyApp == null)
                throw new ValidationException(ErrorStrings.PartsPackingPropertyApp_Validation1);
            var userInfo = Utils.GetCurrentUserInfo();
            app.ApproverId = userInfo.Id;
            app.ApproverName = userInfo.Name;
            app.ApproveTime = DateTime.Now;
            app.ModifierId = userInfo.Id;
            app.ModifierName = userInfo.Name;
            app.ModifyTime = DateTime.Now;
            app.Status = (int)DcsPackingPropertyAppStatus.审核不通过;
            UpdateToDatabase(app);
        }

        public void 新增包装属性申请单(PartsPackingPropertyApp app)
        {
            var userInfo = Utils.GetCurrentUserInfo();
            //生成申请单主单
            app.Code = CodeGenerator.Generate("PartsPackingPropertyApp", userInfo.EnterpriseCode);
            app.Status = (int)DcsPackingPropertyAppStatus.新增;
            app.CreateTime = DateTime.Now;
            app.CreatorId = userInfo.Id;
            app.CreatorName = userInfo.Name;
            //新增清单
            if (app.FirPackingCoefficient>0)
            {
                //一级包装
                PartsPackingPropAppDetail firDetail = new PartsPackingPropAppDetail();
                this.setFirPartsPackingPropAppDetail(app, firDetail);
                app.PartsPackingPropAppDetails.Add(firDetail);
            }
            if (app.SecPackingCoefficient>0)
            {
                //二级包装
                PartsPackingPropAppDetail secDetail = new PartsPackingPropAppDetail();
                this.setSecPartsPackingPropAppDetail(app, secDetail);
                app.PartsPackingPropAppDetails.Add(secDetail);
            }
            if (app.ThidPackingCoefficient>0)
            {
                //三级包装
                PartsPackingPropAppDetail thidDetail = new PartsPackingPropAppDetail();
                this.setThidPartsPackingPropAppDetail(app, thidDetail);
                app.PartsPackingPropAppDetails.Add(thidDetail);
            }
            this.InsertPartsPackingPropertyApp(app);
        }
        public IEnumerable<PartsPackingPropertyAppQuery> GetPartsPackingPropertyAppLists(string code, string spareCode, string spareName, int? status, int? mainPackingType, DateTime? bCreateTime, DateTime? eCreateTime)
        {
            string SQL = @"select  p.*,(select PackingCoefficient from PartsPackingPropAppDetail d where d.partspackingpropappid=p.id and d.PackingType=1) as FirPackingCoefficient 
                         ,(select PackingCoefficient from PartsPackingPropAppDetail d where d.partspackingpropappid=p.id and d.PackingType=2) as SecPackingCoefficient
                         ,(select PackingCoefficient from PartsPackingPropAppDetail d where d.partspackingpropappid=p.id and d.PackingType=3) as  ThidPackingCoefficient 
                         , g.retailguideprice
                         from partspackingpropertyapp p  left join PartsRetailGuidePrice g on p.sparepartid = g.sparepartid and g.status=1 where 1=1 ";
            if (!string.IsNullOrEmpty(code))
            {
                SQL = SQL + " and p.Code like '%" + code + "%'";
            }
            if(!string.IsNullOrEmpty(spareCode))
            {
                SQL = SQL + " and p.SpareCode like '%" + spareCode + "%'";
            }
            if(!string.IsNullOrEmpty(spareName))
            {
                SQL = SQL + " and p.SpareName like '%" + spareName + "%'";
            }
            if (status.HasValue)
            {
                SQL = SQL + " and p.status=" + status.Value;
            }
            if (mainPackingType.HasValue)
            {
                SQL = SQL + " and p.MainPackingType=" + mainPackingType.Value;
            }
            if (bCreateTime.HasValue)
            {
                SQL = SQL + " and p.CreateTime>= to_date('" + bCreateTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }
            if (eCreateTime.HasValue)
            {
                SQL = SQL + " and p.CreateTime<= to_date('" + eCreateTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }
            var search = ObjectContext.ExecuteStoreQuery<PartsPackingPropertyAppQuery>(SQL).ToList();
            return search;
        }

        private void setFirPartsPackingPropAppDetail(PartsPackingPropertyApp app, PartsPackingPropAppDetail detail)
        {
            detail.PartsPackingPropAppId = app.Id;
            detail.PackingType = (int)DcsPackingUnitType.一级包装;
            detail.PackingCoefficient = app.FirPackingCoefficient;
            detail.PackingMaterial = app.FirPackingMaterial;
            detail.MeasureUnit = app.FirMeasureUnit;
            detail.Volume = app.FirVolume;
            detail.Weight = app.FirWeight;
            detail.Length = app.FirLength;
            detail.Width = app.FirWidth;
            detail.Height = app.FirHeight;
            detail.SparePartId = app.SparePartId;
            detail.IsBoxStandardPrint = app.FirIsBoxStandardPrint;
            detail.IsPrintPartStandard = app.FirIsPrintPartStandard;
            detail.PackingMaterialName = app.FirPackingMaterialName;
        }
        private void setSecPartsPackingPropAppDetail(PartsPackingPropertyApp app, PartsPackingPropAppDetail detail)
        {

            detail.PartsPackingPropAppId = app.Id;
            detail.PackingType = (int)DcsPackingUnitType.二级包装;
            detail.PackingCoefficient = app.SecPackingCoefficient;
            detail.PackingMaterial = app.SecPackingMaterial;
            detail.MeasureUnit = app.SecMeasureUnit;
            detail.Volume = app.SecVolume;
            detail.Weight = app.SecWeight;
            detail.Length = app.SecLength;
            detail.Width = app.SecWidth;
            detail.Height = app.SecHeight;
            detail.SparePartId = app.SparePartId;
            detail.IsBoxStandardPrint = app.SecIsBoxStandardPrint;
            detail.IsPrintPartStandard = app.SecIsPrintPartStandard;
            detail.PackingMaterialName = app.SecPackingMaterialName;
        }
        private void setThidPartsPackingPropAppDetail(PartsPackingPropertyApp app, PartsPackingPropAppDetail detail)
        {

            detail.PartsPackingPropAppId = app.Id;
            detail.PackingType = (int)DcsPackingUnitType.三级包装;
            detail.PackingCoefficient = app.ThidPackingCoefficient;
            detail.PackingMaterial = app.ThidPackingMaterial;
            detail.MeasureUnit = app.ThidMeasureUnit;
            detail.Volume = app.ThidVolume;
            detail.Weight = app.ThidWeight;
            detail.Length = app.ThidLength;
            detail.Width = app.ThidWidth;
            detail.Height = app.ThidHeight;
            detail.SparePartId = app.SparePartId;
            detail.IsBoxStandardPrint = app.ThidIsBoxStandardPrint;
            detail.IsPrintPartStandard = app.ThidIsPrintPartStandard;
            detail.PackingMaterialName = app.ThidPackingMaterialName;
        }
        public void 修改包装属性申请单(PartsPackingPropertyApp app)
        {
            var userInfo = Utils.GetCurrentUserInfo();
            //更新主单
            app.ModifierId = userInfo.Id;
            app.ModifierName = userInfo.Name;
            app.ModifyTime = DateTime.Now;
            UpdateToDatabase(app);
            //更新清单
            PartsPackingPropAppDetail firDetail = new PartsPackingPropAppDetail();
            if (null != app.FirId)
            {
                //一级包装
                firDetail = ObjectContext.PartsPackingPropAppDetails.Where(r => r.Id == app.FirId).FirstOrDefault();
                DeleteFromDatabase(firDetail);

            }
            if (app.FirPackingCoefficient>0)
            {
                PartsPackingPropAppDetail firDetailNew = new PartsPackingPropAppDetail();
                this.setFirPartsPackingPropAppDetail(app, firDetailNew);
                InsertToDatabase(firDetailNew);
            }
            PartsPackingPropAppDetail secDetail = new PartsPackingPropAppDetail();
            if (null != app.SecId)
            {
                //二级包装
                secDetail = ObjectContext.PartsPackingPropAppDetails.Where(r => r.Id == app.SecId).FirstOrDefault();
                DeleteFromDatabase(secDetail);
            }
            if (app.SecPackingCoefficient>0)
            {
                PartsPackingPropAppDetail secDetailNew = new PartsPackingPropAppDetail();
                this.setSecPartsPackingPropAppDetail(app, secDetailNew);
                InsertToDatabase(secDetailNew);
            }
            PartsPackingPropAppDetail thidDetail = new PartsPackingPropAppDetail();
            if (null != app.ThidId)
            {
                //三级包装
                thidDetail = ObjectContext.PartsPackingPropAppDetails.Where(r => r.Id == app.ThidId).FirstOrDefault();
                DeleteFromDatabase(thidDetail);
            }
            if (app.ThidPackingCoefficient>0)
             {
                 PartsPackingPropAppDetail thidDetailNew = new PartsPackingPropAppDetail();
                 this.setThidPartsPackingPropAppDetail(app, thidDetailNew);
                 InsertToDatabase(thidDetailNew);
            }
             ObjectContext.SaveChanges();
        }
        public void 作废包装属性申请单(PartsPackingPropertyAppQuery app)
        {
            var userInfo = Utils.GetCurrentUserInfo();
            var dbapp = ObjectContext.PartsPackingPropertyApps.Where(r => r.Id == app.Id && r.Status == (int)DcsPackingPropertyAppStatus.新增).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if (dbapp == null)
            {
                throw new ValidationException(ErrorStrings.PartsPackingPropertyApp_Validation2);
            }
            dbapp.Status = (int)DcsPackingPropertyAppStatus.作废;
            dbapp.ModifierId = userInfo.Id;
            dbapp.ModifierName = userInfo.Name;
            dbapp.ModifyTime = DateTime.Now;

            UpdateToDatabase(dbapp);
        }
        public void 提交包装属性申请单(PartsPackingPropertyAppQuery app)
        {
            var userInfo = Utils.GetCurrentUserInfo();
            var dbapp = ObjectContext.PartsPackingPropertyApps.Where(r => r.Id == app.Id && r.Status == (int)DcsPackingPropertyAppStatus.新增).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if (dbapp == null)
            {
                throw new ValidationException(ErrorStrings.PartsPackingPropertyApp_Validation3);
            }
            dbapp.Status = (int)DcsPackingPropertyAppStatus.提交;
            dbapp.ModifierId = userInfo.Id;
            dbapp.ModifierName = userInfo.Name;
            dbapp.ModifyTime = DateTime.Now;

            UpdateToDatabase(dbapp);
        }
        public void 审核包装属性申请单(PartsPackingPropertyApp app)
        {
            var userInfo = Utils.GetCurrentUserInfo();
            if (app.ApprovalId.Equals(1))
            {
                //审核通过  将最小包装数量更新到配件信息，最小包装和备注更新到配件营销信息中，新增配件营销包装属性
                app.Status = (int)DcsPackingPropertyAppStatus.审核通过;
                //将最小包装数量更新到配件信息
                var sparePart = ObjectContext.SpareParts.Where(r => r.Id == app.SparePartId && r.Status == (int)DcsMasterDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
                if (sparePart == null)
                {
                    throw new ValidationException(ErrorStrings.PartsPackingPropertyApp_Validation4);
                }
                sparePart.MInPackingAmount = app.MInSalesAmount;
                sparePart.ModifierId = userInfo.Id;
                sparePart.ModifierName = userInfo.Name;
                sparePart.ModifyTime = DateTime.Now;
                sparePart.Weight = app.FirWeight;
                sparePart.Volume = app.FirVolume;
                UpdateToDatabase(sparePart);
                //根据配件查询是否存在替换件信息
                var partsReplacements = ObjectContext.PartsReplacements.Where(t => (t.OldPartId == app.SparePartId || t.NewPartId == app.SparePartId) && t.Status == (int)DcsBaseDataStatus.有效).ToArray();
                foreach(var partsReplacement in  partsReplacements){
                    if(partsReplacement.OldPartId==app.SparePartId) {
                        partsReplacement.OldMInAmount = app.MInSalesAmount;
                    } else {
                        partsReplacement.RepMInAmount = app.MInSalesAmount;
                    }
                    partsReplacement.ModifierId = userInfo.Id;
                    partsReplacement.ModifierName = userInfo.Name;
                    partsReplacement.ModifyTime = DateTime.Now;
                    UpdateToDatabase(partsReplacement);
                    var newPartsReplacementHistory = new PartsReplacementHistory {
                        PartsReplacementId = partsReplacement.Id,
                        OldPartId = partsReplacement.OldPartId,
                        OldPartCode = partsReplacement.OldPartCode,
                        OldPartName = partsReplacement.OldPartName,
                        NewPartId = partsReplacement.NewPartId,
                        NewPartCode = partsReplacement.NewPartCode,
                        NewPartName = partsReplacement.NewPartName,
                        Status = partsReplacement.Status,
                        Remark = partsReplacement.Remark,
                        CreatorId = partsReplacement.CreatorId,
                        CreatorName = partsReplacement.CreatorName,
                        CreateTime = partsReplacement.CreateTime,
                        IsSPM = partsReplacement.IsSPM,
                        OldMInAmount = partsReplacement.OldMInAmount,
                        RepMInAmount = partsReplacement.RepMInAmount
                    };
                    InsertToDatabase(newPartsReplacementHistory);
                    new PartsReplacementHistoryAch(this.DomainService).InsertPartsReplacementHistoryValidate(newPartsReplacementHistory);
                }
                //最小包装和备注更新到配件营销信息中
                var partsbranch = ObjectContext.PartsBranches.Where(r => r.PartId == app.SparePartId && r.Status == (int)DcsMasterDataStatus.有效 && r.PartsSalesCategoryId==app.PartsSalesCategoryId).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
                if (partsbranch == null)
                {
                    throw new ValidationException(ErrorStrings.PartsPackingPropertyApp_Validation5);
                }
                partsbranch.MainPackingType = app.MainPackingType;
                partsbranch.Remark = app.Remark;
                partsbranch.ModifierId = userInfo.Id;
                partsbranch.ModifierName = userInfo.Name;
                partsbranch.ModifyTime = DateTime.Now;
                UpdateToDatabase(partsbranch);
                //新增配件营销包装属性
                //先删除原配件营销包装属性
                var detailList = ObjectContext.PartsBranchPackingProps.Where(r => r.PartsBranchId == partsbranch.Id).ToArray();
                if (detailList.Count() > 0)
                {
                    foreach (var item in detailList)
                    {
                        DeleteFromDatabase(item);
                    }
                }
                //查询新的包装属性
                var newDetailLists = ObjectContext.PartsPackingPropAppDetails.Where(r => r.PartsPackingPropAppId == app.Id).ToArray();
                if (newDetailLists.Count() > 0)
                {
                    foreach (var item in newDetailLists)
                    {
                        PartsBranchPackingProp prop = new PartsBranchPackingProp
                        {
                            PartsBranchId = partsbranch.Id,
                            PackingType = item.PackingType,
                            MeasureUnit = item.MeasureUnit,
                            PackingCoefficient = item.PackingCoefficient,
                            PackingMaterial = item.PackingMaterial,
                            Volume = item.Volume,
                            Weight = item.Weight,
                            Length = item.Length,
                            Width = item.Width,
                            Height = item.Height,
                            SparePartId = item.SparePartId,
                            IsBoxStandardPrint=item.IsBoxStandardPrint,
                            IsPrintPartStandard=item.IsPrintPartStandard
                        };
                        InsertToDatabase(prop);
                    }
                }
                //包装设置审核后，把该品牌，该配件ID， 其他的非作废的 包装设置申请单全部作废掉。
                var aband = ObjectContext.PartsPackingPropertyApps.Where(r => r.SparePartId == app.SparePartId && r.PartsSalesCategoryId == app.PartsSalesCategoryId && r.Id != app.Id && r.Status == (int)DcsPackingPropertyAppStatus.审核通过).ToArray();
                foreach(var item in aband){
                    item.ModifierId = userInfo.Id;
                    item.ModifierName = userInfo.Name;
                    item.ModifyTime = DateTime.Now;
                    item.Status = (int)DcsPackingPropertyAppStatus.作废;
                    UpdateToDatabase(app);
                }
            }
            else
            {
                app.Status = (int)DcsPackingPropertyAppStatus.审核不通过;
            }
            app.ModifierId = userInfo.Id;
            app.ModifierName = userInfo.Name;
            app.ModifyTime = DateTime.Now;
            app.ApproverId = userInfo.Id;
            app.ApproverName = userInfo.Name;
            app.ApproveTime = DateTime.Now;
            UpdateToDatabase(app);

            ObjectContext.SaveChanges();
        }
        public IQueryable<PartsPackingPropertyApp> getPartsPackingPropertyAppsById(int? id)
        {
            return ObjectContext.PartsPackingPropertyApps.Where(r=>r.Id==id);
        }
    }
    partial class DcsDomainService
    {
        [Update(UsingCustomMethod = true)]
        public void 驳回包装属性申请单(PartsPackingPropertyApp app) {
            new PartsPackingPropertyAppAch(this).驳回包装属性申请单(app);
        }
        [Update(UsingCustomMethod = true)]
        public void 新增包装属性申请单(PartsPackingPropertyApp app)
        {
            new PartsPackingPropertyAppAch(this).新增包装属性申请单(app);
        }

        public IEnumerable<PartsPackingPropertyAppQuery> GetPartsPackingPropertyAppLists(string code, string spareCode, string spareName,
        int? status, int? mainPackingType, DateTime? bCreateTime, DateTime? eCreateTime)
        {
            return new PartsPackingPropertyAppAch(this).GetPartsPackingPropertyAppLists(code, spareCode, spareName, status, mainPackingType, bCreateTime, eCreateTime);
        }

        [Update(UsingCustomMethod = true)]
        public void 修改包装属性申请单(PartsPackingPropertyApp app)
        {
            new PartsPackingPropertyAppAch(this).修改包装属性申请单(app);
        }
        [Update(UsingCustomMethod = true)]
        public void 作废包装属性申请单(PartsPackingPropertyAppQuery app)
        {
            new PartsPackingPropertyAppAch(this).作废包装属性申请单(app);
        }
        public void 提交包装属性申请单(PartsPackingPropertyAppQuery app)
        {
            new PartsPackingPropertyAppAch(this).提交包装属性申请单(app);
        }
        public void 审核包装属性申请单(PartsPackingPropertyApp app)
        {
            new PartsPackingPropertyAppAch(this).审核包装属性申请单(app);
        }
        public IQueryable<PartsPackingPropertyApp> getPartsPackingPropertyAppsById(int? id)
        {
           return new PartsPackingPropertyAppAch(this).getPartsPackingPropertyAppsById(id);
        }
    }
}
