﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsPriceIncreaseRateAch : DcsSerivceAchieveBase {
        public PartsPriceIncreaseRateAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 作废配件加价策略(PartsPriceIncreaseRate partsPriceIncreaseRate) {
            var dbPartsPriceIncreaseRate = ObjectContext.PartsPriceIncreaseRates.Where(r => r.Id == partsPriceIncreaseRate.Id && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbPartsPriceIncreaseRate == null)
                throw new ValidationException(ErrorStrings.PartsPriceIncreaseRate_Error_Invalidata);
            UpdateToDatabase(partsPriceIncreaseRate);
            partsPriceIncreaseRate.Status = (int)DcsBaseDataStatus.作废;
            var userInfo = Utils.GetCurrentUserInfo();
            partsPriceIncreaseRate.AbandonerId = userInfo.Id;
            partsPriceIncreaseRate.AbandonTime = DateTime.Now;
            partsPriceIncreaseRate.AbandonerName = userInfo.Name;
            this.UpdatePartsPriceIncreaseRateValidate(partsPriceIncreaseRate);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:09:49
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 作废配件加价策略(PartsPriceIncreaseRate partsPriceIncreaseRate) {
            new PartsPriceIncreaseRateAch(this).作废配件加价策略(partsPriceIncreaseRate);
        }
    }
}
