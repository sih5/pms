﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsReplacementAch : DcsSerivceAchieveBase {
        public PartsReplacementAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 作废配件替互换信息(PartsReplacement partsReplacement) {
            var dbpartsReplacement = ObjectContext.PartsReplacements.Where(r => r.Id == partsReplacement.Id && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbpartsReplacement == null)
                throw new ValidationException(ErrorStrings.PartsReplacement_Validation2);
            UpdateToDatabase(partsReplacement);
            partsReplacement.Status = (int)DcsBaseDataStatus.作废;
            var userInfo = Utils.GetCurrentUserInfo();
            partsReplacement.AbandonerId = userInfo.Id;
            partsReplacement.AbandonTime = DateTime.Now;
            partsReplacement.AbandonerName = userInfo.Name;
            this.UpdatePartsReplacementValidate(partsReplacement);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:09:49
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 作废配件替互换信息(PartsReplacement partsReplacement) {
            new PartsReplacementAch(this).作废配件替互换信息(partsReplacement);
        }
    }
}
