﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class SparePartAch : DcsSerivceAchieveBase {
        public SparePartAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 作废配件基础信息(SparePart sparePart) {
            var dbsparePart = ObjectContext.SpareParts.Where(r => r.Id == sparePart.Id && r.Status == (int)DcsMasterDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbsparePart == null)
                throw new ValidationException(ErrorStrings.SparePart_Validation2);
            UpdateToDatabase(sparePart);
            sparePart.Status = (int)DcsMasterDataStatus.作废;
            var userInfo = Utils.GetCurrentUserInfo();
            sparePart.AbandonerId = userInfo.Id;
            sparePart.AbandonerName = userInfo.Name;
            sparePart.AbandonTime = DateTime.Now;
            this.UpdateSparePartValidate(sparePart);

            #region 同步作废配件营销信息
            var dbpartsBranch = ObjectContext.PartsBranches.Where(r => r.PartId == sparePart.Id && r.Status == (int)DcsMasterDataStatus.有效).ToArray();
            if (dbpartsBranch != null)
            {
                foreach (var partsBranch in dbpartsBranch)
                {
                    //作废配件营销信息
                    partsBranch.Status = (int)DcsMasterDataStatus.作废;
                    partsBranch.ModifierId = userInfo.Id;
                    partsBranch.ModifierName = userInfo.Name;
                    partsBranch.ModifyTime = DateTime.Now;
                    partsBranch.AbandonerId = userInfo.Id;
                    partsBranch.AbandonerName = userInfo.Name;
                    partsBranch.AbandonTime = DateTime.Now;
                    UpdateToDatabase(partsBranch);
                    //新增配件营销信息变更履历
                    var newPartsBranchHistory = new PartsBranchHistory
                    {
                        PartsBranchId = partsBranch.Id,//配件营销信息Id
                        PartId = partsBranch.PartId,//配件Id
                        PartCode = partsBranch.PartCode,//配件编号
                        PartName = partsBranch.PartName,//配件名称
                        ReferenceCode = partsBranch.ReferenceCode,//配件参图号
                        StockMaximum = partsBranch.StockMaximum,//库存高限
                        StockMinimum = partsBranch.StockMinimum,//库存低限
                        IncreaseRateGroupId = partsBranch.IncreaseRateGroupId,//加价率分组Id
                        BranchId = partsBranch.BranchId,//营销分公司Id
                        BranchName = partsBranch.BranchName,//营销分公司
                        LossType = partsBranch.LossType,//损耗类型
                        PartsAttribution = partsBranch.PartsAttribution,//配件所属分类
                        PartsSalesCategoryId = partsBranch.PartsSalesCategoryId,//配件销售类型Id
                        PartsSalesCategoryName = partsBranch.PartsSalesCategoryName,//配件销售类型名称
                        ABCStrategyId = partsBranch.ABCStrategyId,//配件ABC分类策略Id
                        ProductLifeCycle = partsBranch.ProductLifeCycle,//产品生命周期
                        IsOrderable = partsBranch.IsOrderable,//是否可采购
                        PurchaseCycle = partsBranch.PurchaseCycle,//采购周期（天）
                        IsService = partsBranch.IsService,//是否售后服务件
                        IsSalable = partsBranch.IsSalable,//是否可销售
                        PartsReturnPolicy = partsBranch.PartsReturnPolicy,//旧件返回政策
                        IsDirectSupply = partsBranch.IsDirectSupply,//是否可直供
                        WarrantySupplyStatus = partsBranch.WarrantySupplyStatus,//保内保外供货属性
                        MinSaleQuantity = partsBranch.MinSaleQuantity,//最小销售批量
                        PartABC = partsBranch.PartABC,//配件ABC分类
                        PurchaseRoute = partsBranch.PurchaseRoute,//采购路线
                        PartsWarrantyCategoryId = partsBranch.PartsWarrantyCategoryId,//配件保修分类Id
                        PartsWarrantyCategoryCode = partsBranch.PartsWarrantyCategoryCode,//配件保修分类编号
                        PartsWarrantyCategoryName = partsBranch.PartsWarrantyCategoryName,//配件保修分类名称
                        PartsWarhouseManageGranularity = partsBranch.PartsWarhouseManageGranularity,//配件仓储管理粒度
                        PartsMaterialManageCost = partsBranch.PartsMaterialManageCost,//配件材料费
                        PartsWarrantyLong = partsBranch.PartsWarrantyLong,//配件保修期
                        AutoApproveUpLimit = partsBranch.AutoApproveUpLimit,//批量审核上限
                        Status = partsBranch.Status,//状态
                        CreatorId = userInfo.Id,//创建人Id						
                        CreatorName = userInfo.Name,//创建人						
                        CreateTime = DateTime.Now,//创建时间	                    
                        Remark = partsBranch.Remark,//备注
                        RowVersion = DateTime.Now,
                        RepairMatMinUnit = partsBranch.RepairMatMinUnit//维修用料最小单位                   
                    };
                    InsertToDatabase(newPartsBranchHistory);
                }
            }
            #endregion
        }


        public void 停用配件基础信息(SparePart sparePart) {
            var userInfo = Utils.GetCurrentUserInfo();
            var dbsparePart = ObjectContext.SpareParts.Where(r => r.Id == sparePart.Id && r.Status == (int)DcsMasterDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbsparePart == null)
                throw new ValidationException(ErrorStrings.SparePart_Validation3);
            UpdateToDatabase(sparePart);
            sparePart.Status = (int)DcsMasterDataStatus.停用;
            sparePart.OMSparePartMark = 0;
            var newSparePartHistory = new SparePartHistory();
            newSparePartHistory.SparePartId = sparePart.Id;//配件信息Id						
            newSparePartHistory.Code = sparePart.Code;//配件编号						
            newSparePartHistory.Name = sparePart.Name;//配件名称						
            newSparePartHistory.LastSubstitute = sparePart.LastSubstitute;//上一替代件						
            newSparePartHistory.NextSubstitute = sparePart.NextSubstitute;//下一替代件						
            newSparePartHistory.ShelfLife = sparePart.ShelfLife;//保质期						
            newSparePartHistory.EnglishName = sparePart.EnglishName;//英文名称						
            newSparePartHistory.PinyinCode = sparePart.PinyinCode;//拼音代码						
            newSparePartHistory.ReferenceCode = sparePart.ReferenceCode;//配件参考编号						
            newSparePartHistory.ReferenceName = sparePart.ReferenceName;//配件参考名称						
            newSparePartHistory.CADCode = sparePart.CADCode;//设计图号						
            newSparePartHistory.CADName = sparePart.CADName;//设计名称						
            newSparePartHistory.PartType = sparePart.PartType;//配件类型						
            newSparePartHistory.Specification = sparePart.Specification;//规格型号						
            newSparePartHistory.Feature = sparePart.Feature;//配件特征说明						
            newSparePartHistory.Status = sparePart.Status;//状态						
            newSparePartHistory.Length = sparePart.Length;//长（cm）						
            newSparePartHistory.Width = sparePart.Width;//宽(cm)						
            newSparePartHistory.Height = sparePart.Height;//高(cm)						
            newSparePartHistory.Volume = sparePart.Volume;//体积(cm*3)						
            newSparePartHistory.Weight = sparePart.Weight;//重量(kg)						
            newSparePartHistory.Material = sparePart.Material;//材料						
            newSparePartHistory.MInPackingAmount = sparePart.MInPackingAmount;//最小包装数量						
            newSparePartHistory.PackingAmount = sparePart.PackingAmount;//包装数量						
            newSparePartHistory.PackingSpecification = sparePart.PackingSpecification;//包装规格						
            newSparePartHistory.PartsOutPackingCode = sparePart.PartsOutPackingCode;//商品外包装编号						
            newSparePartHistory.PartsInPackingCode = sparePart.PartsInPackingCode;//商品内包装编号						
            newSparePartHistory.MeasureUnit = sparePart.MeasureUnit;//计量单位						
            newSparePartHistory.CreatorId = userInfo.Id;//创建人Id						
            newSparePartHistory.CreatorName = userInfo.Name;//创建人						
            newSparePartHistory.CreateTime = DateTime.Now;//创建时间						
            newSparePartHistory.GroupABCCategory = sparePart.GroupABCCategory;//集团ABC类别						
            newSparePartHistory.RowVersion = sparePart.RowVersion;//RowVersion						
            newSparePartHistory.IMSCompressionNumber = sparePart.IMSCompressionNumber;//IMS压缩号						
            newSparePartHistory.IMSManufacturerNumber = sparePart.IMSManufacturerNumber;//IMS厂商号						
            newSparePartHistory.SubstandardName = sparePart.SubstandardName;//配件标准名称						
            newSparePartHistory.TotalNumber = sparePart.TotalNumber;//总成型号						
            newSparePartHistory.Factury = sparePart.Factury;//厂商						
            newSparePartHistory.IsOriginal = sparePart.IsOriginal;//是否原厂件						
            newSparePartHistory.CategoryCode = sparePart.CategoryCode;//分类编码						
            newSparePartHistory.OverseasPartsFigure = sparePart.OverseasPartsFigure;//海外配件图号								
            newSparePartHistory.CategoryName = sparePart.CategoryName;//分类名称	
            newSparePartHistory.StandardCode = sparePart.StandardCode;
            newSparePartHistory.StandardName = sparePart.StandardName;
            newSparePartHistory.ExchangeIdentification = sparePart.ExchangeIdentification;
            newSparePartHistory.DeclareElement = sparePart.DeclareElement;//申报要素
            InsertToDatabase(newSparePartHistory);
            this.UpdateSparePartValidate(sparePart);

            #region 同步停用配件营销信息
            var dbpartsBranch = ObjectContext.PartsBranches.Where(r => r.PartId == sparePart.Id && r.Status == (int)DcsMasterDataStatus.有效).ToArray();
            if (dbpartsBranch != null)
            {
                foreach (var partsBranch in dbpartsBranch)
                {
                    //停用配件营销信息
                    partsBranch.Status = (int)DcsMasterDataStatus.停用;
                    partsBranch.ModifierId = userInfo.Id;
                    partsBranch.ModifierName = userInfo.Name;
                    partsBranch.ModifyTime = DateTime.Now;
                    UpdateToDatabase(partsBranch);
                    //新增配件营销信息变更履历
                    var newPartsBranchHistory = new PartsBranchHistory
                    {
                        PartsBranchId = partsBranch.Id,//配件营销信息Id
                        PartId = partsBranch.PartId,//配件Id
                        PartCode = partsBranch.PartCode,//配件编号
                        PartName = partsBranch.PartName,//配件名称
                        ReferenceCode = partsBranch.ReferenceCode,//配件参图号
                        StockMaximum = partsBranch.StockMaximum,//库存高限
                        StockMinimum = partsBranch.StockMinimum,//库存低限
                        IncreaseRateGroupId = partsBranch.IncreaseRateGroupId,//加价率分组Id
                        BranchId = partsBranch.BranchId,//营销分公司Id
                        BranchName = partsBranch.BranchName,//营销分公司
                        LossType = partsBranch.LossType,//损耗类型
                        PartsAttribution = partsBranch.PartsAttribution,//配件所属分类
                        PartsSalesCategoryId = partsBranch.PartsSalesCategoryId,//配件销售类型Id
                        PartsSalesCategoryName = partsBranch.PartsSalesCategoryName,//配件销售类型名称
                        ABCStrategyId = partsBranch.ABCStrategyId,//配件ABC分类策略Id
                        ProductLifeCycle = partsBranch.ProductLifeCycle,//产品生命周期
                        IsOrderable = partsBranch.IsOrderable,//是否可采购
                        PurchaseCycle = partsBranch.PurchaseCycle,//采购周期（天）
                        IsService = partsBranch.IsService,//是否售后服务件
                        IsSalable = partsBranch.IsSalable,//是否可销售
                        PartsReturnPolicy = partsBranch.PartsReturnPolicy,//旧件返回政策
                        IsDirectSupply = partsBranch.IsDirectSupply,//是否可直供
                        WarrantySupplyStatus = partsBranch.WarrantySupplyStatus,//保内保外供货属性
                        MinSaleQuantity = partsBranch.MinSaleQuantity,//最小销售批量
                        PartABC = partsBranch.PartABC,//配件ABC分类
                        PurchaseRoute = partsBranch.PurchaseRoute,//采购路线
                        PartsWarrantyCategoryId = partsBranch.PartsWarrantyCategoryId,//配件保修分类Id
                        PartsWarrantyCategoryCode = partsBranch.PartsWarrantyCategoryCode,//配件保修分类编号
                        PartsWarrantyCategoryName = partsBranch.PartsWarrantyCategoryName,//配件保修分类名称
                        PartsWarhouseManageGranularity = partsBranch.PartsWarhouseManageGranularity,//配件仓储管理粒度
                        PartsMaterialManageCost = partsBranch.PartsMaterialManageCost,//配件材料费
                        PartsWarrantyLong = partsBranch.PartsWarrantyLong,//配件保修期
                        AutoApproveUpLimit = partsBranch.AutoApproveUpLimit,//批量审核上限
                        Status = partsBranch.Status,//状态
                        CreatorId = userInfo.Id,//创建人Id						
                        CreatorName = userInfo.Name,//创建人						
                        CreateTime = DateTime.Now,//创建时间	                    
                        Remark = partsBranch.Remark,//备注
                        RowVersion = DateTime.Now,
                        RepairMatMinUnit = partsBranch.RepairMatMinUnit//维修用料最小单位                   
                    };
                    InsertToDatabase(newPartsBranchHistory);
                }          
            }
            #endregion

            ObjectContext.SaveChanges();            
        }


        public void 批量停用配件信息(List<SparePart> spareParts) {
            var userInfo = Utils.GetCurrentUserInfo();
            foreach(var sparePart in spareParts) {
                var dbsparePart = ObjectContext.SpareParts.Where(r => r.Id == sparePart.Id && r.Status == (int)DcsMasterDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
                if(dbsparePart == null)
                    throw new ValidationException(ErrorStrings.SparePart_Validation3);
                dbsparePart.Status = (int)DcsMasterDataStatus.停用;
                dbsparePart.ModifierId = userInfo.Id;
                dbsparePart.ModifierName = userInfo.Name;
                dbsparePart.ModifyTime = DateTime.Now;
                dbsparePart.OMSparePartMark = 0;
                var newSparePartHistory = new SparePartHistory();					
                newSparePartHistory.SparePartId = dbsparePart.Id;//配件信息Id						
                newSparePartHistory.Code = dbsparePart.Code;//配件编号						
                newSparePartHistory.Name = dbsparePart.Name;//配件名称						
                newSparePartHistory.LastSubstitute = dbsparePart.LastSubstitute;//上一替代件						
                newSparePartHistory.NextSubstitute = dbsparePart.NextSubstitute;//下一替代件						
                newSparePartHistory.ShelfLife = dbsparePart.ShelfLife;//保质期						
                newSparePartHistory.EnglishName = dbsparePart.EnglishName;//英文名称						
                newSparePartHistory.PinyinCode = dbsparePart.PinyinCode;//拼音代码						
                newSparePartHistory.ReferenceCode = dbsparePart.ReferenceCode;//配件参考编号						
                newSparePartHistory.ReferenceName = dbsparePart.ReferenceName;//配件参考名称						
                newSparePartHistory.CADCode = dbsparePart.CADCode;//设计图号						
                newSparePartHistory.CADName = dbsparePart.CADName;//设计名称						
                newSparePartHistory.PartType = dbsparePart.PartType;//配件类型						
                newSparePartHistory.Specification = dbsparePart.Specification;//规格型号						
                newSparePartHistory.Feature = dbsparePart.Feature;//配件特征说明						
                newSparePartHistory.Status = dbsparePart.Status;//状态						
                newSparePartHistory.Length = dbsparePart.Length;//长（cm）						
                newSparePartHistory.Width = dbsparePart.Width;//宽(cm)						
                newSparePartHistory.Height = dbsparePart.Height;//高(cm)						
                newSparePartHistory.Volume = dbsparePart.Volume;//体积(cm*3)						
                newSparePartHistory.Weight = dbsparePart.Weight;//重量(kg)						
                newSparePartHistory.Material = dbsparePart.Material;//材料						
                newSparePartHistory.MInPackingAmount = dbsparePart.MInPackingAmount;//最小包装数量						
                newSparePartHistory.PackingAmount = dbsparePart.PackingAmount;//包装数量						
                newSparePartHistory.PackingSpecification = dbsparePart.PackingSpecification;//包装规格						
                newSparePartHistory.PartsOutPackingCode = dbsparePart.PartsOutPackingCode;//商品外包装编号						
                newSparePartHistory.PartsInPackingCode = dbsparePart.PartsInPackingCode;//商品内包装编号						
                newSparePartHistory.MeasureUnit = dbsparePart.MeasureUnit;//计量单位						
                newSparePartHistory.CreatorId =userInfo.Id;//创建人Id						
                newSparePartHistory.CreatorName = userInfo.Name;//创建人						
                newSparePartHistory.CreateTime =  DateTime.Now;//创建时间						
                newSparePartHistory.GroupABCCategory = dbsparePart.GroupABCCategory;//集团ABC类别						
                newSparePartHistory.RowVersion = dbsparePart.RowVersion;//RowVersion						
                newSparePartHistory.IMSCompressionNumber = dbsparePart.IMSCompressionNumber;//IMS压缩号						
                newSparePartHistory.IMSManufacturerNumber = dbsparePart.IMSManufacturerNumber;//IMS厂商号						
                newSparePartHistory.SubstandardName = dbsparePart.SubstandardName;//配件标准名称						
                newSparePartHistory.TotalNumber = dbsparePart.TotalNumber;//总成型号						
                newSparePartHistory.Factury = dbsparePart.Factury;//厂商						
                newSparePartHistory.IsOriginal = dbsparePart.IsOriginal;//是否原厂件						
                newSparePartHistory.CategoryCode = dbsparePart.CategoryCode;//分类编码						
                newSparePartHistory.OverseasPartsFigure = dbsparePart.OverseasPartsFigure;//海外配件图号								
                newSparePartHistory.CategoryName = dbsparePart.CategoryName;//分类名称
                newSparePartHistory.StandardCode = dbsparePart.StandardCode;
                newSparePartHistory.StandardName = dbsparePart.StandardName;
                newSparePartHistory.ExchangeIdentification = dbsparePart.ExchangeIdentification;
                newSparePartHistory.DeclareElement = dbsparePart.DeclareElement;//申报要素
                InsertToDatabase(newSparePartHistory);
                UpdateToDatabase(dbsparePart);

                #region 同步停用配件营销信息
                var dbpartsBranch = ObjectContext.PartsBranches.Where(r => r.PartId == sparePart.Id && r.Status == (int)DcsMasterDataStatus.有效).ToArray();
                if (dbpartsBranch != null)
                {
                    foreach (var partsBranch in dbpartsBranch)
                    {
                        //停用配件营销信息
                        partsBranch.Status = (int)DcsMasterDataStatus.停用;
                        partsBranch.ModifierId = userInfo.Id;
                        partsBranch.ModifierName = userInfo.Name;
                        partsBranch.ModifyTime = DateTime.Now;
                        UpdateToDatabase(partsBranch);
                        //新增配件营销信息变更履历
                        var newPartsBranchHistory = new PartsBranchHistory
                        {
                            PartsBranchId = partsBranch.Id,//配件营销信息Id
                            PartId = partsBranch.PartId,//配件Id
                            PartCode = partsBranch.PartCode,//配件编号
                            PartName = partsBranch.PartName,//配件名称
                            ReferenceCode = partsBranch.ReferenceCode,//配件参图号
                            StockMaximum = partsBranch.StockMaximum,//库存高限
                            StockMinimum = partsBranch.StockMinimum,//库存低限
                            IncreaseRateGroupId = partsBranch.IncreaseRateGroupId,//加价率分组Id
                            BranchId = partsBranch.BranchId,//营销分公司Id
                            BranchName = partsBranch.BranchName,//营销分公司
                            LossType = partsBranch.LossType,//损耗类型
                            PartsAttribution = partsBranch.PartsAttribution,//配件所属分类
                            PartsSalesCategoryId = partsBranch.PartsSalesCategoryId,//配件销售类型Id
                            PartsSalesCategoryName = partsBranch.PartsSalesCategoryName,//配件销售类型名称
                            ABCStrategyId = partsBranch.ABCStrategyId,//配件ABC分类策略Id
                            ProductLifeCycle = partsBranch.ProductLifeCycle,//产品生命周期
                            IsOrderable = partsBranch.IsOrderable,//是否可采购
                            PurchaseCycle = partsBranch.PurchaseCycle,//采购周期（天）
                            IsService = partsBranch.IsService,//是否售后服务件
                            IsSalable = partsBranch.IsSalable,//是否可销售
                            PartsReturnPolicy = partsBranch.PartsReturnPolicy,//旧件返回政策
                            IsDirectSupply = partsBranch.IsDirectSupply,//是否可直供
                            WarrantySupplyStatus = partsBranch.WarrantySupplyStatus,//保内保外供货属性
                            MinSaleQuantity = partsBranch.MinSaleQuantity,//最小销售批量
                            PartABC = partsBranch.PartABC,//配件ABC分类
                            PurchaseRoute = partsBranch.PurchaseRoute,//采购路线
                            PartsWarrantyCategoryId = partsBranch.PartsWarrantyCategoryId,//配件保修分类Id
                            PartsWarrantyCategoryCode = partsBranch.PartsWarrantyCategoryCode,//配件保修分类编号
                            PartsWarrantyCategoryName = partsBranch.PartsWarrantyCategoryName,//配件保修分类名称
                            PartsWarhouseManageGranularity = partsBranch.PartsWarhouseManageGranularity,//配件仓储管理粒度
                            PartsMaterialManageCost = partsBranch.PartsMaterialManageCost,//配件材料费
                            PartsWarrantyLong = partsBranch.PartsWarrantyLong,//配件保修期
                            AutoApproveUpLimit = partsBranch.AutoApproveUpLimit,//批量审核上限
                            Status = partsBranch.Status,//状态
                            CreatorId = userInfo.Id,//创建人Id						
                            CreatorName = userInfo.Name,//创建人						
                            CreateTime = DateTime.Now,//创建时间	                    
                            Remark = partsBranch.Remark,//备注
                            RowVersion = DateTime.Now,
                            RepairMatMinUnit = partsBranch.RepairMatMinUnit//维修用料最小单位                   
                        };
                        InsertToDatabase(newPartsBranchHistory);
                    }
                }
                #endregion

                ObjectContext.SaveChanges();
            }

        }

        public void 恢复配件基础信息(SparePart sparePart) {
            var userInfo = Utils.GetCurrentUserInfo();
            var dbsparePart = ObjectContext.SpareParts.Where(r => r.Id == sparePart.Id && r.Status == (int)DcsMasterDataStatus.停用).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbsparePart == null)
                throw new ValidationException(ErrorStrings.SparePart_Validation4);
            UpdateToDatabase(sparePart);
            sparePart.Status = (int)DcsMasterDataStatus.有效;
            sparePart.OMSparePartMark = 0;
            var newSparePartHistory = new SparePartHistory();
            newSparePartHistory.SparePartId = sparePart.Id;//配件信息Id						
            newSparePartHistory.Code = sparePart.Code;//配件编号						
            newSparePartHistory.Name = sparePart.Name;//配件名称						
            newSparePartHistory.LastSubstitute = sparePart.LastSubstitute;//上一替代件						
            newSparePartHistory.NextSubstitute = sparePart.NextSubstitute;//下一替代件						
            newSparePartHistory.ShelfLife = sparePart.ShelfLife;//保质期						
            newSparePartHistory.EnglishName = sparePart.EnglishName;//英文名称						
            newSparePartHistory.PinyinCode = sparePart.PinyinCode;//拼音代码						
            newSparePartHistory.ReferenceCode = sparePart.ReferenceCode;//配件参考编号						
            newSparePartHistory.ReferenceName = sparePart.ReferenceName;//配件参考名称						
            newSparePartHistory.CADCode = sparePart.CADCode;//设计图号						
            newSparePartHistory.CADName = sparePart.CADName;//设计名称						
            newSparePartHistory.PartType = sparePart.PartType;//配件类型						
            newSparePartHistory.Specification = sparePart.Specification;//规格型号						
            newSparePartHistory.Feature = sparePart.Feature;//配件特征说明						
            newSparePartHistory.Status = sparePart.Status;//状态						
            newSparePartHistory.Length = sparePart.Length;//长（cm）						
            newSparePartHistory.Width = sparePart.Width;//宽(cm)						
            newSparePartHistory.Height = sparePart.Height;//高(cm)						
            newSparePartHistory.Volume = sparePart.Volume;//体积(cm*3)						
            newSparePartHistory.Weight = sparePart.Weight;//重量(kg)						
            newSparePartHistory.Material = sparePart.Material;//材料						
            newSparePartHistory.MInPackingAmount = sparePart.MInPackingAmount;//最小包装数量						
            newSparePartHistory.PackingAmount = sparePart.PackingAmount;//包装数量						
            newSparePartHistory.PackingSpecification = sparePart.PackingSpecification;//包装规格						
            newSparePartHistory.PartsOutPackingCode = sparePart.PartsOutPackingCode;//商品外包装编号						
            newSparePartHistory.PartsInPackingCode = sparePart.PartsInPackingCode;//商品内包装编号						
            newSparePartHistory.MeasureUnit = sparePart.MeasureUnit;//计量单位						
            newSparePartHistory.CreatorId = userInfo.Id;//创建人Id						
            newSparePartHistory.CreatorName = userInfo.Name;//创建人						
            newSparePartHistory.CreateTime = DateTime.Now;//创建时间						
            newSparePartHistory.GroupABCCategory = sparePart.GroupABCCategory;//集团ABC类别						
            newSparePartHistory.RowVersion = sparePart.RowVersion;//RowVersion						
            newSparePartHistory.IMSCompressionNumber = sparePart.IMSCompressionNumber;//IMS压缩号						
            newSparePartHistory.IMSManufacturerNumber = sparePart.IMSManufacturerNumber;//IMS厂商号						
            newSparePartHistory.SubstandardName = sparePart.SubstandardName;//配件标准名称						
            newSparePartHistory.TotalNumber = sparePart.TotalNumber;//总成型号						
            newSparePartHistory.Factury = sparePart.Factury;//厂商						
            newSparePartHistory.IsOriginal = sparePart.IsOriginal;//是否原厂件						
            newSparePartHistory.CategoryCode = sparePart.CategoryCode;//分类编码						
            newSparePartHistory.OverseasPartsFigure = sparePart.OverseasPartsFigure;//海外配件图号								
            newSparePartHistory.CategoryName = sparePart.CategoryName;//分类名称
            newSparePartHistory.StandardCode = sparePart.StandardCode;
            newSparePartHistory.StandardName = sparePart.StandardName;
            newSparePartHistory.ExchangeIdentification = sparePart.ExchangeIdentification;
            newSparePartHistory.DeclareElement = sparePart.DeclareElement;//申报要素
            InsertToDatabase(newSparePartHistory);
            this.UpdateSparePartValidate(sparePart);

            #region 同步恢复配件营销信息
            var dbpartsBranch = ObjectContext.PartsBranches.Where(r => r.PartId == sparePart.Id && r.Status == (int)DcsMasterDataStatus.停用).ToArray();
            if (dbpartsBranch != null)
            {
                foreach (var partsBranch in dbpartsBranch)
                {
                    //恢复配件营销信息
                    partsBranch.Status = (int)DcsMasterDataStatus.有效;
                    partsBranch.ModifierId = userInfo.Id;
                    partsBranch.ModifierName = userInfo.Name;
                    partsBranch.ModifyTime = DateTime.Now;
                    UpdateToDatabase(partsBranch);
                    //新增配件营销信息变更履历
                    var newPartsBranchHistory = new PartsBranchHistory
                    {
                        PartsBranchId = partsBranch.Id,//配件营销信息Id
                        PartId = partsBranch.PartId,//配件Id
                        PartCode = partsBranch.PartCode,//配件编号
                        PartName = partsBranch.PartName,//配件名称
                        ReferenceCode = partsBranch.ReferenceCode,//配件参图号
                        StockMaximum = partsBranch.StockMaximum,//库存高限
                        StockMinimum = partsBranch.StockMinimum,//库存低限
                        IncreaseRateGroupId = partsBranch.IncreaseRateGroupId,//加价率分组Id
                        BranchId = partsBranch.BranchId,//营销分公司Id
                        BranchName = partsBranch.BranchName,//营销分公司
                        LossType = partsBranch.LossType,//损耗类型
                        PartsAttribution = partsBranch.PartsAttribution,//配件所属分类
                        PartsSalesCategoryId = partsBranch.PartsSalesCategoryId,//配件销售类型Id
                        PartsSalesCategoryName = partsBranch.PartsSalesCategoryName,//配件销售类型名称
                        ABCStrategyId = partsBranch.ABCStrategyId,//配件ABC分类策略Id
                        ProductLifeCycle = partsBranch.ProductLifeCycle,//产品生命周期
                        IsOrderable = partsBranch.IsOrderable,//是否可采购
                        PurchaseCycle = partsBranch.PurchaseCycle,//采购周期（天）
                        IsService = partsBranch.IsService,//是否售后服务件
                        IsSalable = partsBranch.IsSalable,//是否可销售
                        PartsReturnPolicy = partsBranch.PartsReturnPolicy,//旧件返回政策
                        IsDirectSupply = partsBranch.IsDirectSupply,//是否可直供
                        WarrantySupplyStatus = partsBranch.WarrantySupplyStatus,//保内保外供货属性
                        MinSaleQuantity = partsBranch.MinSaleQuantity,//最小销售批量
                        PartABC = partsBranch.PartABC,//配件ABC分类
                        PurchaseRoute = partsBranch.PurchaseRoute,//采购路线
                        PartsWarrantyCategoryId = partsBranch.PartsWarrantyCategoryId,//配件保修分类Id
                        PartsWarrantyCategoryCode = partsBranch.PartsWarrantyCategoryCode,//配件保修分类编号
                        PartsWarrantyCategoryName = partsBranch.PartsWarrantyCategoryName,//配件保修分类名称
                        PartsWarhouseManageGranularity = partsBranch.PartsWarhouseManageGranularity,//配件仓储管理粒度
                        PartsMaterialManageCost = partsBranch.PartsMaterialManageCost,//配件材料费
                        PartsWarrantyLong = partsBranch.PartsWarrantyLong,//配件保修期
                        AutoApproveUpLimit = partsBranch.AutoApproveUpLimit,//批量审核上限
                        Status = partsBranch.Status,//状态
                        CreatorId = userInfo.Id,//创建人Id						
                        CreatorName = userInfo.Name,//创建人						
                        CreateTime = DateTime.Now,//创建时间	                    
                        Remark = partsBranch.Remark,//备注
                        RowVersion = DateTime.Now,
                        RepairMatMinUnit = partsBranch.RepairMatMinUnit//维修用料最小单位                   
                    };
                    InsertToDatabase(newPartsBranchHistory);
                }
            }
            #endregion

            ObjectContext.SaveChanges();
        }

        public void 批量恢复配件信息(List<SparePart> spareParts) {
            var userInfo = Utils.GetCurrentUserInfo();
            foreach(var sparePart in spareParts) {
                var dbsparePart = ObjectContext.SpareParts.Where(r => r.Id == sparePart.Id && r.Status == (int)DcsMasterDataStatus.停用).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
                if(dbsparePart == null)
                    throw new ValidationException(ErrorStrings.SparePart_Validation4);
                dbsparePart.Status = (int)DcsMasterDataStatus.有效;
                dbsparePart.ModifierId = userInfo.Id;
                dbsparePart.ModifierName = userInfo.Name;
                dbsparePart.ModifyTime = DateTime.Now;
                dbsparePart.OMSparePartMark = 0;
                var newSparePartHistory = new SparePartHistory();					
                newSparePartHistory.SparePartId = dbsparePart.Id;//配件信息Id						
                newSparePartHistory.Code = dbsparePart.Code;//配件编号						
                newSparePartHistory.Name = dbsparePart.Name;//配件名称						
                newSparePartHistory.LastSubstitute = dbsparePart.LastSubstitute;//上一替代件						
                newSparePartHistory.NextSubstitute = dbsparePart.NextSubstitute;//下一替代件						
                newSparePartHistory.ShelfLife = dbsparePart.ShelfLife;//保质期						
                newSparePartHistory.EnglishName = dbsparePart.EnglishName;//英文名称						
                newSparePartHistory.PinyinCode = dbsparePart.PinyinCode;//拼音代码						
                newSparePartHistory.ReferenceCode = dbsparePart.ReferenceCode;//配件参考编号						
                newSparePartHistory.ReferenceName = dbsparePart.ReferenceName;//配件参考名称						
                newSparePartHistory.CADCode = dbsparePart.CADCode;//设计图号						
                newSparePartHistory.CADName = dbsparePart.CADName;//设计名称						
                newSparePartHistory.PartType = dbsparePart.PartType;//配件类型						
                newSparePartHistory.Specification = dbsparePart.Specification;//规格型号						
                newSparePartHistory.Feature = dbsparePart.Feature;//配件特征说明						
                newSparePartHistory.Status = dbsparePart.Status;//状态						
                newSparePartHistory.Length = dbsparePart.Length;//长（cm）						
                newSparePartHistory.Width = dbsparePart.Width;//宽(cm)						
                newSparePartHistory.Height = dbsparePart.Height;//高(cm)						
                newSparePartHistory.Volume = dbsparePart.Volume;//体积(cm*3)						
                newSparePartHistory.Weight = dbsparePart.Weight;//重量(kg)						
                newSparePartHistory.Material = dbsparePart.Material;//材料						
                newSparePartHistory.MInPackingAmount = dbsparePart.MInPackingAmount;//最小包装数量						
                newSparePartHistory.PackingAmount = dbsparePart.PackingAmount;//包装数量						
                newSparePartHistory.PackingSpecification = dbsparePart.PackingSpecification;//包装规格						
                newSparePartHistory.PartsOutPackingCode = dbsparePart.PartsOutPackingCode;//商品外包装编号						
                newSparePartHistory.PartsInPackingCode = dbsparePart.PartsInPackingCode;//商品内包装编号						
                newSparePartHistory.MeasureUnit = dbsparePart.MeasureUnit;//计量单位						
                newSparePartHistory.CreatorId =userInfo.Id;//创建人Id						
                newSparePartHistory.CreatorName = userInfo.Name;//创建人						
                newSparePartHistory.CreateTime =  DateTime.Now;//创建时间						
                newSparePartHistory.GroupABCCategory = dbsparePart.GroupABCCategory;//集团ABC类别						
                newSparePartHistory.RowVersion = dbsparePart.RowVersion;//RowVersion						
                newSparePartHistory.IMSCompressionNumber = dbsparePart.IMSCompressionNumber;//IMS压缩号						
                newSparePartHistory.IMSManufacturerNumber = dbsparePart.IMSManufacturerNumber;//IMS厂商号						
                newSparePartHistory.SubstandardName = dbsparePart.SubstandardName;//配件标准名称						
                newSparePartHistory.TotalNumber = dbsparePart.TotalNumber;//总成型号						
                newSparePartHistory.Factury = dbsparePart.Factury;//厂商						
                newSparePartHistory.IsOriginal = dbsparePart.IsOriginal;//是否原厂件						
                newSparePartHistory.CategoryCode = dbsparePart.CategoryCode;//分类编码						
                newSparePartHistory.OverseasPartsFigure = dbsparePart.OverseasPartsFigure;//海外配件图号								
                newSparePartHistory.CategoryName = dbsparePart.CategoryName;//分类名称	
                newSparePartHistory.StandardCode = dbsparePart.StandardCode;
                newSparePartHistory.StandardName = dbsparePart.StandardName;
                newSparePartHistory.ExchangeIdentification = dbsparePart.ExchangeIdentification;
                newSparePartHistory.DeclareElement = dbsparePart.DeclareElement;//申报要素
                InsertToDatabase(newSparePartHistory);
                UpdateToDatabase(dbsparePart);

                #region 同步恢复配件营销信息
                var dbpartsBranch = ObjectContext.PartsBranches.Where(r => r.PartId == sparePart.Id && r.Status == (int)DcsMasterDataStatus.停用).ToArray();
                if (dbpartsBranch != null)
                {
                    foreach (var partsBranch in dbpartsBranch)
                    {
                        //恢复配件营销信息
                        partsBranch.Status = (int)DcsMasterDataStatus.有效;
                        partsBranch.ModifierId = userInfo.Id;
                        partsBranch.ModifierName = userInfo.Name;
                        partsBranch.ModifyTime = DateTime.Now;
                        UpdateToDatabase(partsBranch);
                        //新增配件营销信息变更履历
                        var newPartsBranchHistory = new PartsBranchHistory
                        {
                            PartsBranchId = partsBranch.Id,//配件营销信息Id
                            PartId = partsBranch.PartId,//配件Id
                            PartCode = partsBranch.PartCode,//配件编号
                            PartName = partsBranch.PartName,//配件名称
                            ReferenceCode = partsBranch.ReferenceCode,//配件参图号
                            StockMaximum = partsBranch.StockMaximum,//库存高限
                            StockMinimum = partsBranch.StockMinimum,//库存低限
                            IncreaseRateGroupId = partsBranch.IncreaseRateGroupId,//加价率分组Id
                            BranchId = partsBranch.BranchId,//营销分公司Id
                            BranchName = partsBranch.BranchName,//营销分公司
                            LossType = partsBranch.LossType,//损耗类型
                            PartsAttribution = partsBranch.PartsAttribution,//配件所属分类
                            PartsSalesCategoryId = partsBranch.PartsSalesCategoryId,//配件销售类型Id
                            PartsSalesCategoryName = partsBranch.PartsSalesCategoryName,//配件销售类型名称
                            ABCStrategyId = partsBranch.ABCStrategyId,//配件ABC分类策略Id
                            ProductLifeCycle = partsBranch.ProductLifeCycle,//产品生命周期
                            IsOrderable = partsBranch.IsOrderable,//是否可采购
                            PurchaseCycle = partsBranch.PurchaseCycle,//采购周期（天）
                            IsService = partsBranch.IsService,//是否售后服务件
                            IsSalable = partsBranch.IsSalable,//是否可销售
                            PartsReturnPolicy = partsBranch.PartsReturnPolicy,//旧件返回政策
                            IsDirectSupply = partsBranch.IsDirectSupply,//是否可直供
                            WarrantySupplyStatus = partsBranch.WarrantySupplyStatus,//保内保外供货属性
                            MinSaleQuantity = partsBranch.MinSaleQuantity,//最小销售批量
                            PartABC = partsBranch.PartABC,//配件ABC分类
                            PurchaseRoute = partsBranch.PurchaseRoute,//采购路线
                            PartsWarrantyCategoryId = partsBranch.PartsWarrantyCategoryId,//配件保修分类Id
                            PartsWarrantyCategoryCode = partsBranch.PartsWarrantyCategoryCode,//配件保修分类编号
                            PartsWarrantyCategoryName = partsBranch.PartsWarrantyCategoryName,//配件保修分类名称
                            PartsWarhouseManageGranularity = partsBranch.PartsWarhouseManageGranularity,//配件仓储管理粒度
                            PartsMaterialManageCost = partsBranch.PartsMaterialManageCost,//配件材料费
                            PartsWarrantyLong = partsBranch.PartsWarrantyLong,//配件保修期
                            AutoApproveUpLimit = partsBranch.AutoApproveUpLimit,//批量审核上限
                            Status = partsBranch.Status,//状态
                            CreatorId = userInfo.Id,//创建人Id						
                            CreatorName = userInfo.Name,//创建人						
                            CreateTime = DateTime.Now,//创建时间	                    
                            Remark = partsBranch.Remark,//备注
                            RowVersion = DateTime.Now,
                            RepairMatMinUnit = partsBranch.RepairMatMinUnit//维修用料最小单位                   
                        };
                        InsertToDatabase(newPartsBranchHistory);
                    }
                }
                #endregion

                ObjectContext.SaveChanges();
            }
        }

        public void 新增配件基础信息(SparePart sparePart)
        {
            var user = Utils.GetCurrentUserInfo();
            //新增配件营销信息
            var partsBranch = new PartsBranch();
            var partsSalesCategorie = ObjectContext.PartsSalesCategories.Where(r => r.BranchId == user.EnterpriseId && r.Status == (int)DcsBaseDataStatus.有效).SingleOrDefault();
            partsBranch.SparePart = sparePart;
            partsBranch.PartCode = sparePart.Code;
            partsBranch.PartName = sparePart.Name;
            partsBranch.ReferenceCode = sparePart.ReferenceCode;
            partsBranch.BranchId = user.EnterpriseId;
            partsBranch.BranchName = user.EnterpriseName;
            partsBranch.PartsSalesCategoryId = partsSalesCategorie.Id;
            partsBranch.PartsSalesCategoryName = partsSalesCategorie.Name;
            partsBranch.IsOrderable = true;
            partsBranch.IsSalable = true;
            partsBranch.PartsReturnPolicy = (int)DcsPartsWarrantyTermReturnPolicy.返回本部;
            partsBranch.IsDirectSupply = false;
            partsBranch.Status = (int)DcsBaseDataStatus.有效;
            partsBranch.MainPackingType = (int)DcsPackingUnitType.一级包装;
            partsBranch.CreatorId = user.Id;
            partsBranch.CreatorName = user.Name;
            partsBranch.CreateTime = DateTime.Now;
            partsBranch.IsAutoCaclSaleProperty = true;

            //新增配件包装属性
            var partsBranchPackingProp = new PartsBranchPackingProp();
            partsBranchPackingProp.SparePart = sparePart;
            partsBranchPackingProp.PackingType = (int)DcsPackingUnitType.一级包装;
            partsBranchPackingProp.PackingCode = "1";
            partsBranchPackingProp.MeasureUnit = "件";
            partsBranchPackingProp.PackingCoefficient = 1;
            partsBranchPackingProp.PartsBranch = partsBranch;
            InsertToDatabase(partsBranchPackingProp);

        }
        public void 修改配件基础信息(SparePart sparePart) {
            var userInfo = Utils.GetCurrentUserInfo();
            CheckEntityState(sparePart);
            var dbsparePart = ObjectContext.SpareParts.Where(r => r.Id == sparePart.Id && r.Status == (int)DcsMasterDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbsparePart == null)
                throw new ValidationException(ErrorStrings.SparePart_Validation5);
            var dbPartsBranch = ObjectContext.PartsBranches.Where(r => r.PartId == sparePart.Id);
            foreach(var partsBranch in dbPartsBranch) {
                partsBranch.PartName = sparePart.Name;
                UpdateToDatabase(partsBranch);
            }
            #region 老版本
            //if(dbsparePart.ExchangeIdentification != sparePart.ExchangeIdentification) {
            //    var spareParts = ObjectContext.SpareParts.Where(r => r.ExchangeIdentification == sparePart.ExchangeIdentification).ToArray();
            //    if(spareParts.Length >= 2) {
            //        var newPartExchange = new PartsExchange {
            //            ExchangeCode = sparePart.ExchangeIdentification,
            //            ExchangeName = sparePart.ReferenceName,
            //            PartId = sparePart.Id,
            //            Status = (int)DcsBaseDataStatus.有效,
            //            Remark = "由于修改配件互换识别号导致",
            //            CreatorId = userInfo.Id,
            //            CreatorName = userInfo.Name,
            //            CreateTime = DateTime.Now
            //        };
            //        newPartExchange.PartsExchangeHistories.Add(new PartsExchangeHistory {
            //            ExchangeCode = sparePart.ExchangeIdentification,
            //            ExchangeName = sparePart.ReferenceName,
            //            PartId = sparePart.Id,
            //            Status = (int)DcsBaseDataStatus.有效,
            //            Remark = "由于修改配件互换识别号导致",
            //            CreatorId = userInfo.Id,
            //            CreatorName = userInfo.Name,
            //            CreateTime = DateTime.Now
            //        });
            //        InsertToDatabase(newPartExchange);
            //        new PartsExchangeAch(this.DomainService).InsertPartsExchangeValidate(newPartExchange);
            //    } else if(spareParts.Length == 1) {
            //        var newPartExchange = new PartsExchange {
            //            ExchangeCode = sparePart.ExchangeIdentification,
            //            ExchangeName = sparePart.ReferenceName,
            //            PartId = sparePart.Id,
            //            Status = (int)DcsBaseDataStatus.有效,
            //            Remark = "由于修改配件互换识别号导致",
            //            CreatorId = userInfo.Id,
            //            CreatorName = userInfo.Name,
            //            CreateTime = DateTime.Now
            //        };
            //        newPartExchange.PartsExchangeHistories.Add(new PartsExchangeHistory {
            //            ExchangeCode = sparePart.ExchangeIdentification,
            //            ExchangeName = sparePart.ReferenceName,
            //            PartId = sparePart.Id,
            //            Status = (int)DcsBaseDataStatus.有效,
            //            Remark = "由于修改配件互换识别号导致",
            //            CreatorId = userInfo.Id,
            //            CreatorName = userInfo.Name,
            //            CreateTime = DateTime.Now
            //        });
            //        InsertToDatabase(newPartExchange);
            //        new PartsExchangeAch(this.DomainService).InsertPartsExchangeValidate(newPartExchange);
            //        var newPartExchangeOld = new PartsExchange {
            //            ExchangeCode = spareParts.FirstOrDefault().ExchangeIdentification,
            //            ExchangeName = spareParts.FirstOrDefault().ReferenceName,
            //            PartId = spareParts.FirstOrDefault().Id,
            //            Status = (int)DcsBaseDataStatus.有效,
            //            Remark = "由于修改配件互换识别号导致",
            //            CreatorId = userInfo.Id,
            //            CreatorName = userInfo.Name,
            //            CreateTime = DateTime.Now
            //        };
            //        newPartExchangeOld.PartsExchangeHistories.Add(new PartsExchangeHistory {
            //            ExchangeCode = spareParts.FirstOrDefault().ExchangeIdentification,
            //            ExchangeName = spareParts.FirstOrDefault().ReferenceName,
            //            PartId = spareParts.FirstOrDefault().Id,
            //            Status = (int)DcsBaseDataStatus.有效,
            //            Remark = "由于修改配件互换识别号导致",
            //            CreatorId = userInfo.Id,
            //            CreatorName = userInfo.Name,
            //            CreateTime = DateTime.Now
            //        });
            //        InsertToDatabase(newPartExchangeOld);
            //        new PartsExchangeAch(this.DomainService).InsertPartsExchangeValidate(newPartExchangeOld);
            //    }
            //    var partsExchange = ObjectContext.PartsExchanges.Where(e => e.ExchangeCode == dbsparePart.ExchangeIdentification && e.Status == (int)DcsBaseDataStatus.有效).ToArray();
            //    if(partsExchange.Length > 2) {
            //        var partsExchangeForOldSparePart = ObjectContext.PartsExchanges.Where(e => e.ExchangeCode == dbsparePart.ExchangeIdentification && e.PartId == sparePart.Id && e.Status == (int)DcsBaseDataStatus.有效).FirstOrDefault();
            //        if(partsExchangeForOldSparePart != null) {
            //            partsExchangeForOldSparePart.Status = (int)DcsMasterDataStatus.作废;
            //            partsExchangeForOldSparePart.AbandonTime = DateTime.Now;
            //            partsExchangeForOldSparePart.AbandonerId = userInfo.Id;
            //            partsExchangeForOldSparePart.AbandonerName = userInfo.Name;
            //            UpdateToDatabase(partsExchangeForOldSparePart);
            //            var newPartsExchangeHistory = new PartsExchangeHistory() {
            //                PartsExchangeId = partsExchangeForOldSparePart.Id,
            //                ExchangeCode = partsExchangeForOldSparePart.ExchangeCode,
            //                ExchangeName = partsExchangeForOldSparePart.ExchangeName,
            //                PartId = partsExchangeForOldSparePart.PartId,
            //                Status = (int)DcsBaseDataStatus.有效,
            //                Remark = "由于修改配件互换识别号导致",
            //                CreatorId = userInfo.Id,
            //                CreatorName = userInfo.Name,
            //                CreateTime = DateTime.Now
            //            };
            //            InsertToDatabase(newPartsExchangeHistory);
            //        }
            //    } else if(partsExchange.Length > 0 && partsExchange.Length <= 2) {
            //        var partsExchangeForOldSparePart = ObjectContext.PartsExchanges.Where(e => e.ExchangeCode == dbsparePart.ExchangeIdentification && e.Status == (int)DcsBaseDataStatus.有效).ToArray();
            //        var partsExchangeForSparePart = ObjectContext.PartsExchanges.Where(e => e.ExchangeCode == dbsparePart.ExchangeIdentification && e.PartId == sparePart.Id && e.Status == (int)DcsBaseDataStatus.有效).FirstOrDefault();
            //        if(partsExchangeForSparePart != null) {
            //            foreach(var item in partsExchangeForOldSparePart) {
            //                item.Status = (int)DcsMasterDataStatus.作废;
            //                item.AbandonTime = DateTime.Now;
            //                item.AbandonerId = userInfo.Id;
            //                item.AbandonerName = userInfo.Name;
            //                UpdateToDatabase(item);
            //                var newPartsExchangeHistory = new PartsExchangeHistory() {
            //                    PartsExchangeId = item.Id,
            //                    ExchangeCode = item.ExchangeCode,
            //                    ExchangeName = item.ExchangeName,
            //                    PartId = item.PartId,
            //                    Status = (int)DcsBaseDataStatus.有效,
            //                    Remark = "由于修改配件互换识别号导致",
            //                    CreatorId = userInfo.Id,
            //                    CreatorName = userInfo.Name,
            //                    CreateTime = DateTime.Now
            //                };
            //                InsertToDatabase(newPartsExchangeHistory);
            //            }
            //        }
            //    }
            //} 
            #endregion

            if(dbsparePart.ExchangeIdentification != sparePart.ExchangeIdentification) {
                var spareParts = ObjectContext.SpareParts.Where(r => r.ExchangeIdentification == sparePart.ExchangeIdentification &&r.Status==(int)DcsBaseDataStatus.有效).ToArray();
                var partsExchangeForOldSparePart = ObjectContext.PartsExchanges.Where(e => e.ExchangeCode == dbsparePart.ExchangeIdentification && e.PartId == sparePart.Id && e.Status == (int)DcsBaseDataStatus.有效).FirstOrDefault();
                var partsExchangeGroups = ObjectContext.PartsExchangeGroups.Where(r => r.ExchangeCode == sparePart.ExchangeIdentification && r.Status == (int)DcsBaseDataStatus.有效).ToArray();
                //作废原有的互换号
                if(partsExchangeForOldSparePart != null) {
                    partsExchangeForOldSparePart.Status = (int)DcsMasterDataStatus.作废;
                    partsExchangeForOldSparePart.Remark = "由于修改互换识别号导致";
                    partsExchangeForOldSparePart.AbandonTime = DateTime.Now;
                    partsExchangeForOldSparePart.AbandonerId = userInfo.Id;
                    partsExchangeForOldSparePart.AbandonerName = userInfo.Name;
                    UpdateToDatabase(partsExchangeForOldSparePart);
                    var newPartsExchangeHistory = new PartsExchangeHistory() {
                        PartsExchangeId = partsExchangeForOldSparePart.Id,
                        ExchangeCode = partsExchangeForOldSparePart.ExchangeCode,
                        ExchangeName = partsExchangeForOldSparePart.ExchangeName,
                        PartId = partsExchangeForOldSparePart.PartId,
                        Status = (int)DcsBaseDataStatus.有效,
                        Remark = "由于修改配件互换识别号导致",
                        CreatorId = userInfo.Id,
                        CreatorName = userInfo.Name,
                        CreateTime = DateTime.Now
                    };
                    InsertToDatabase(newPartsExchangeHistory);

                    #region 更新互换组的修改人等信息
                    var partsExchangeGroupsOld = ObjectContext.PartsExchangeGroups.Where(r => r.ExchangeCode == partsExchangeForOldSparePart.ExchangeCode && r.Status == (int)DcsBaseDataStatus.有效).FirstOrDefault();
                    if (partsExchangeGroupsOld != null)
                    {
                        partsExchangeGroupsOld.ModifierId = userInfo.Id;
                        partsExchangeGroupsOld.ModifierName = userInfo.Name;
                        partsExchangeGroupsOld.ModifyTime = DateTime.Now;
                        UpdateToDatabase(partsExchangeGroupsOld);
                    }
                    #endregion

                }
                if (!string.IsNullOrEmpty(sparePart.ExchangeIdentification)) {
                    //新增新的互换号
                    var newPartExchange = new PartsExchange {
                        ExchangeCode = sparePart.ExchangeIdentification,
                        ExchangeName = sparePart.ReferenceName,
                        PartId = sparePart.Id,
                        Status = (int)DcsBaseDataStatus.有效,
                        Remark = "由于修改配件互换识别号导致",
                        CreatorId = userInfo.Id,
                        CreatorName = userInfo.Name,
                        CreateTime = DateTime.Now
                    };
                    newPartExchange.PartsExchangeHistories.Add(new PartsExchangeHistory {
                        ExchangeCode = sparePart.ExchangeIdentification,
                        ExchangeName = sparePart.ReferenceName,
                        PartId = sparePart.Id,
                        Status = (int)DcsBaseDataStatus.有效,
                        Remark = "由于修改配件互换识别号导致",
                        CreatorId = userInfo.Id,
                        CreatorName = userInfo.Name,
                        CreateTime = DateTime.Now
                    });
                    InsertToDatabase(newPartExchange);
                    new PartsExchangeAch(this.DomainService).InsertPartsExchangeValidate(newPartExchange);

                    #region 更新互换组的修改人等信息
                    var partsExchangeGroupsnew = ObjectContext.PartsExchangeGroups.Where(r => r.ExchangeCode == sparePart.ExchangeIdentification && r.Status == (int)DcsBaseDataStatus.有效).FirstOrDefault();
                    if (partsExchangeGroupsnew != null)
                    {
                        partsExchangeGroupsnew.ModifierId = userInfo.Id;
                        partsExchangeGroupsnew.ModifierName = userInfo.Name;
                        partsExchangeGroupsnew.ModifyTime = DateTime.Now;
                        UpdateToDatabase(partsExchangeGroupsnew);
                    }
                    #endregion

                    //查找配件互换号只有1个
                    if (spareParts.Length == 1 && !partsExchangeGroups.Any()) {
                        var partsExchangeGroup = new PartsExchangeGroup {
                            ExGroupCode = CodeGenerator.Generate("PartsExchange"),
                            ExchangeCode = sparePart.ExchangeIdentification,
                            Status = (int)DcsBaseDataStatus.有效
                        };
                        InsertToDatabase(partsExchangeGroup);
                        new PartsExchangeGroupAch(this.DomainService).InsertPartsExchangeGroupValidate(partsExchangeGroup);
                    }
             
                }

            }
        }
        public void 上传配件附件(SparePart sparePart)
        {
            var userInfo = Utils.GetCurrentUserInfo();
            sparePart.ModifierId = userInfo.Id;
            sparePart.ModifierName = userInfo.Name;
            sparePart.ModifyTime = DateTime.Now;
            sparePart.Path = sparePart.Path;
            UpdateToDatabase(sparePart);
        }
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:09:49
        //原分布类的函数全部转移到Ach                                                               
        [Update(UsingCustomMethod = true)]
        public void 新增配件基础信息(SparePart sparePart) {
            new SparePartAch(this).新增配件基础信息(sparePart);
        }

        [Update(UsingCustomMethod = true)]
        public void 作废配件基础信息(SparePart sparePart) {
            new SparePartAch(this).作废配件基础信息(sparePart);
        }


        [Invoke]
        public void 停用配件基础信息(SparePart sparePart) {
            new SparePartAch(this).停用配件基础信息(sparePart);
        }
        

        [Invoke]
        public void 批量停用配件信息(List<SparePart> spareParts) {
            new SparePartAch(this).批量停用配件信息(spareParts);
        }

        [Invoke]
        public void 恢复配件基础信息(SparePart sparePart) {
            new SparePartAch(this).恢复配件基础信息(sparePart);
        }

        [Invoke]
        public void 批量恢复配件信息(List<SparePart> spareParts) {
            new SparePartAch(this).批量恢复配件信息(spareParts);
        }

        [Update(UsingCustomMethod = true)]
        public void 修改配件基础信息(SparePart sparePart) {
            new SparePartAch(this).修改配件基础信息(sparePart);
        }

        [Update(UsingCustomMethod = true)]
        public void 上传配件附件(SparePart sparePart)
        {
            new SparePartAch(this).上传配件附件(sparePart);
        }
    }
}
