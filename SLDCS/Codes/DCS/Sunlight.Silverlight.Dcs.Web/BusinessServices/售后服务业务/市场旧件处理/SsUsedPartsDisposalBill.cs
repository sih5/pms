﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class SsUsedPartsDisposalBillAch : DcsSerivceAchieveBase {
        public SsUsedPartsDisposalBillAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 生成服务站旧件处理单(SsUsedPartsDisposalBill ssUsedPartsDisposalBill) {
            var ssUsedPartsDisposalDetails = ssUsedPartsDisposalBill.SsUsedPartsDisposalDetails.Select(r => r.UsedPartsBarCode).ToArray();
            var ssUsedPartsStorages = ObjectContext.SsUsedPartsStorages.Where(r => ssUsedPartsDisposalDetails.Contains(r.UsedPartsBarCode)).ToArray();
            var usedPartsBarCodes = ssUsedPartsDisposalDetails.Except(ssUsedPartsStorages.Select(r => r.UsedPartsBarCode.ToLower())).ToArray();
            if(usedPartsBarCodes.Length > 0)
                throw new ValidationException(string.Format(ErrorStrings.SsUsedPartsDisposalBill_Validation9, usedPartsBarCodes.First()));
            foreach(var ssUsedPartsStorage in ssUsedPartsStorages) {
                if(ssUsedPartsStorage.UsedPartsReturnPolicy == (int)DcsPartsWarrantyTermReturnPolicy.返回本部)
                    throw new ValidationException(ErrorStrings.SsUsedPartsDisposalBill_Validation2);
                if(ssUsedPartsStorage.Shipped)
                    throw new ValidationException(ErrorStrings.SsUsedPartsDisposalBill_Validation6);
                UpdateToDatabase(ssUsedPartsStorage);
                ssUsedPartsStorage.Shipped = true;
                new SsUsedPartsStorageAch(this.DomainService).UpdateSsUsedPartsStorageValidate(ssUsedPartsStorage);
            }
        }

        public void 修改服务站旧件处理单(SsUsedPartsDisposalBill ssUsedPartsDisposalBill) {
            CheckEntityState(ssUsedPartsDisposalBill);
            var dbssUsedPartsDisposalBill = ObjectContext.SsUsedPartsDisposalBills.Where(r => r.Id == ssUsedPartsDisposalBill.Id && r.Status == (int)DcsSsUsedPartsDisposalBillStatus.新增).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbssUsedPartsDisposalBill == null)
                throw new ValidationException(ErrorStrings.SsUsedPartsDisposalBill_Validation3);
            var ssUsedPartsDisposalDetails = ChangeSet.GetAssociatedChanges(ssUsedPartsDisposalBill, r => r.SsUsedPartsDisposalDetails).Cast<SsUsedPartsDisposalDetail>().ToArray();
            var repeatedssUsedPartsDisposalDetails = ssUsedPartsDisposalDetails.Where(r => ChangeSet.GetChangeOperation(r) == ChangeOperation.Insert && ssUsedPartsDisposalDetails.Any(v => ChangeSet.GetChangeOperation(v) == ChangeOperation.Delete && r.UsedPartsBarCode.ToLower() == v.UsedPartsBarCode.ToLower())).ToArray();
            //待处理的清单
            var pendingssUsedPartsDisposalDetails = ssUsedPartsDisposalDetails.Except(repeatedssUsedPartsDisposalDetails).ToArray();
            var usedPartsBarCodes = pendingssUsedPartsDisposalDetails.Select(r => r.UsedPartsBarCode.ToLower()).ToArray();
            var ssUsedPartsStorages = ObjectContext.SsUsedPartsStorages.Where(r => usedPartsBarCodes.Contains(r.UsedPartsBarCode.ToLower())).ToArray();
            foreach(var ssUsedPartsDisposalDetail in pendingssUsedPartsDisposalDetails) {
                switch(ChangeSet.GetChangeOperation(ssUsedPartsDisposalDetail)) {
                    case ChangeOperation.Insert:
                        var ssUsedPartsStorageInsert = ssUsedPartsStorages.FirstOrDefault(r => r.UsedPartsBarCode.ToLower() == ssUsedPartsDisposalDetail.UsedPartsBarCode.ToLower());
                        if(ssUsedPartsStorageInsert == null)
                            throw new ValidationException(string.Format(ErrorStrings.SsUsedPartsDisposalBill_Validation9, ssUsedPartsDisposalDetail.UsedPartsBarCode));
                        if(ssUsedPartsStorageInsert.Shipped)
                            throw new ValidationException(string.Format(ErrorStrings.SsUsedPartsDisposalBill_Validation8, ssUsedPartsStorageInsert.UsedPartsBarCode));
                        if(ssUsedPartsStorageInsert.UsedPartsReturnPolicy == (int)DcsPartsWarrantyTermReturnPolicy.返回本部)
                            throw new ValidationException(string.Format(ErrorStrings.SsUsedPartsDisposalBill_Validation7, ssUsedPartsStorageInsert.UsedPartsBarCode));
                        UpdateToDatabase(ssUsedPartsStorageInsert);
                        ssUsedPartsStorageInsert.Shipped = true;
                        new SsUsedPartsStorageAch(this.DomainService).UpdateSsUsedPartsStorageValidate(ssUsedPartsStorageInsert);
                        break;
                    case ChangeOperation.Delete:
                        var ssUsedPartsStorageDelete = ssUsedPartsStorages.FirstOrDefault(r => r.UsedPartsBarCode.ToLower() == ssUsedPartsDisposalDetail.UsedPartsBarCode.ToLower());
                        if(ssUsedPartsStorageDelete == null)
                            throw new ValidationException(string.Format(ErrorStrings.SsUsedPartsDisposalBill_Validation9, ssUsedPartsDisposalDetail.UsedPartsBarCode));
                        UpdateToDatabase(ssUsedPartsStorageDelete);
                        ssUsedPartsStorageDelete.Shipped = false;
                        new SsUsedPartsStorageAch(this.DomainService).UpdateSsUsedPartsStorageValidate(ssUsedPartsStorageDelete);
                        break;
                }
            }
        }

        public void 审批服务站旧件处理单(SsUsedPartsDisposalBill ssUsedPartsDisposalBill) {
            var dbssUsedPartsDisposalBill = ObjectContext.SsUsedPartsDisposalBills.Where(r => r.Id == ssUsedPartsDisposalBill.Id && r.Status == (int)DcsSsUsedPartsDisposalBillStatus.新增).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbssUsedPartsDisposalBill == null)
                throw new ValidationException(ErrorStrings.SsUsedPartsDisposalBill_Validation4);
            ssUsedPartsDisposalBill.Status = (int)DcsSsUsedPartsDisposalBillStatus.生效;
            this.UpdateSsUsedPartsDisposalBillValidate(ssUsedPartsDisposalBill);
            var ssUsedPartsStorages = ObjectContext.SsUsedPartsStorages.Where(r => ObjectContext.SsUsedPartsDisposalDetails.Any(v => v.SsUsedPartsDisposalBillId == ssUsedPartsDisposalBill.Id && v.UsedPartsBarCode.ToLower() == r.UsedPartsBarCode.ToLower())).ToArray();
            foreach(var ssUsedPartsStorage in ssUsedPartsStorages)
                DeleteFromDatabase(ssUsedPartsStorage);
            var userInfo = Utils.GetCurrentUserInfo();
            ssUsedPartsDisposalBill.ApproverId = userInfo.Id;
            ssUsedPartsDisposalBill.ApproverName = userInfo.Name;
            ssUsedPartsDisposalBill.ApproveTime = DateTime.Now;
        }

        public void 作废服务站旧件处理单(SsUsedPartsDisposalBill ssUsedPartsDisposalBill) {
            var dbssUsedPartsDisposalBill = ObjectContext.SsUsedPartsDisposalBills.Where(r => r.Id == ssUsedPartsDisposalBill.Id && r.Status == (int)DcsSsUsedPartsDisposalBillStatus.新增).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbssUsedPartsDisposalBill == null)
                throw new ValidationException(ErrorStrings.SsUsedPartsDisposalBill_Validation5);
            UpdateToDatabase(ssUsedPartsDisposalBill);
            ssUsedPartsDisposalBill.Status = (int)DcsSsUsedPartsDisposalBillStatus.作废;
            this.UpdateSsUsedPartsDisposalBillValidate(ssUsedPartsDisposalBill);
            var ssUsedPartsStorages = ObjectContext.SsUsedPartsStorages.Where(r => ObjectContext.SsUsedPartsDisposalDetails.Any(v => v.SsUsedPartsDisposalBillId == ssUsedPartsDisposalBill.Id && v.UsedPartsBarCode.ToLower() == r.UsedPartsBarCode.ToLower())).ToArray();
            foreach(var ssUsedPartsStorage in ssUsedPartsStorages) {
                ssUsedPartsStorage.Shipped = false;
                new SsUsedPartsStorageAch(this.DomainService).UpdateSsUsedPartsStorageValidate(ssUsedPartsStorage);
            }
            var userInfo = Utils.GetCurrentUserInfo();
            ssUsedPartsDisposalBill.AbandonerId = userInfo.Id;
            ssUsedPartsDisposalBill.AbandonerName = userInfo.Name;
            ssUsedPartsDisposalBill.AbandonTime = DateTime.Now;
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:09:49
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 生成服务站旧件处理单(SsUsedPartsDisposalBill ssUsedPartsDisposalBill) {
            new SsUsedPartsDisposalBillAch(this).生成服务站旧件处理单(ssUsedPartsDisposalBill);
        }
        
        [Update(UsingCustomMethod = true)]
        public void 修改服务站旧件处理单(SsUsedPartsDisposalBill ssUsedPartsDisposalBill) {
            new SsUsedPartsDisposalBillAch(this).修改服务站旧件处理单(ssUsedPartsDisposalBill);
        }
        
        [Update(UsingCustomMethod = true)]
        public void 审批服务站旧件处理单(SsUsedPartsDisposalBill ssUsedPartsDisposalBill) {
            new SsUsedPartsDisposalBillAch(this).审批服务站旧件处理单(ssUsedPartsDisposalBill);
        }
        
        [Update(UsingCustomMethod = true)]
        public void 作废服务站旧件处理单(SsUsedPartsDisposalBill ssUsedPartsDisposalBill) {
            new SsUsedPartsDisposalBillAch(this).作废服务站旧件处理单(ssUsedPartsDisposalBill);
        }
    }
}
