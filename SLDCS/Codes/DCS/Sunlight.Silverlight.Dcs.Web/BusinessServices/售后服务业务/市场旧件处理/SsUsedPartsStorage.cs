﻿using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class SsUsedPartsStorageAch : DcsSerivceAchieveBase {
        public SsUsedPartsStorageAch(DcsDomainService domainService)
            : base(domainService) {
        }

        /// <summary>
        /// 仅供客户端使用
        /// </summary>

        public void 修改服务站旧件库存(SsUsedPartsStorage ssUsedPartsStorage) {
            var dbSsUsedPartsStorage = ObjectContext.SsUsedPartsStorages.Where(r => r.Id == ssUsedPartsStorage.Id && r.Shipped).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbSsUsedPartsStorage != null)
                throw new ValidationException(ErrorStrings.SsUsedPartsStorage_Validation1);
            //var repairClaimBill = ObjectContext.RepairClaimBills.SingleOrDefault(r => r.ClaimBillCode == ssUsedPartsStorage.ClaimBillCode);
            //if(repairClaimBill != null) {
            //    int isreturn = 0;
            //    foreach(var repairClaimMaterialDetail in repairClaimBill.RepairClaimItemDetails.SelectMany(repairClaimItemDetail => repairClaimItemDetail.RepairClaimMaterialDetails)) {
            //        if (repairClaimMaterialDetail.UsedPartsBarCode == ssUsedPartsStorage.UsedPartsBarCode) {
            //            repairClaimMaterialDetail.UsedPartsReturnPolicy = ssUsedPartsStorage.UsedPartsReturnPolicy;
            //            if (ssUsedPartsStorage.UsedPartsReturnPolicy == (int)DcsPartsWarrantyTermReturnPolicy.返回本部) {
            //                isreturn = 1;
            //            }
            //        }
            //        else {
            //            if (repairClaimMaterialDetail.UsedPartsReturnPolicy == (int)DcsPartsWarrantyTermReturnPolicy.返回本部) {
            //                isreturn = 1;
            //            }
            //        }
            //    }
            //    if (isreturn == 1) {
            //        repairClaimBill.UsedPartsDisposalStatus = (int)DcsRepairClaimBillUsedPartsDisposalStatus.旧件未全部到达;
            //    }
            //    else {
            //        repairClaimBill.UsedPartsDisposalStatus = (int)DcsRepairClaimBillUsedPartsDisposalStatus.旧件全部到达;
            //    }
            //}
            //UpdateToDatabase(repairClaimBill);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:09:49
        //原分布类的函数全部转移到Ach                                                               
        /// <summary>
        /// 仅供客户端使用
        /// </summary>
        [Update(UsingCustomMethod = true)]
        public void 修改服务站旧件库存(SsUsedPartsStorage ssUsedPartsStorage) {
            new SsUsedPartsStorageAch(this).修改服务站旧件库存(ssUsedPartsStorage);
        }
    }
}
