﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class UsedPartsDistanceInforAch : DcsSerivceAchieveBase {
        public UsedPartsDistanceInforAch(DcsDomainService domainService)
            : base(domainService) {
        }
        

        public void 作废旧件运距信息(UsedPartsDistanceInfor usedPartsDistanceInfor) {
            var dbusedPartsDistanceInfor = ObjectContext.UsedPartsDistanceInfors.Where(r => r.Id == usedPartsDistanceInfor.Id && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbusedPartsDistanceInfor == null)
                throw new ValidationException(ErrorStrings.UsedPartsDistanceInfor_Validation2);
            UpdateToDatabase(usedPartsDistanceInfor);
            usedPartsDistanceInfor.Status = (int)DcsBaseDataStatus.作废;
            var userInfo = Utils.GetCurrentUserInfo();
            usedPartsDistanceInfor.ModifierId = userInfo.Id;
            usedPartsDistanceInfor.ModifierName = userInfo.Name;
            usedPartsDistanceInfor.ModifyTime = DateTime.Now;
            usedPartsDistanceInfor.AbandonerId = userInfo.Id;
            usedPartsDistanceInfor.AbandonerName = userInfo.Name;
            usedPartsDistanceInfor.AbandonTime = DateTime.Now;
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:09:49
        //原分布类的函数全部转移到Ach                                                               
        
        [Update(UsingCustomMethod = true)]
        public void 作废旧件运距信息(UsedPartsDistanceInfor usedPartsDistanceInfor) {
            new UsedPartsDistanceInforAch(this).作废旧件运距信息(usedPartsDistanceInfor);
        }
    }
}
