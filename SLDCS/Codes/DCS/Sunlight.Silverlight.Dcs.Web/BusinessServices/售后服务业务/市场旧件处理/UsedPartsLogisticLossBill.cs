﻿using System.Linq;
using System.ServiceModel.DomainServices.Server;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class UsedPartsLogisticLossBillAch : DcsSerivceAchieveBase {
        public UsedPartsLogisticLossBillAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 修改物流损失单(UsedPartsLogisticLossBill usedPartsLogisticLossBill) {
            CheckEntityState(usedPartsLogisticLossBill);
            var usedPartsLogisticLossDetails = usedPartsLogisticLossBill.UsedPartsLogisticLossDetails.Where(r => r.ProcessStatus == (int)DcsUsedPartsLogisticLossDetailProcessStatus.未返回).ToArray();
            var usedPartsLogisticLossDetailsId = usedPartsLogisticLossDetails.Select(r => r.Id).ToArray();
            var dbusedPartsLogisticLossDetails = ObjectContext.UsedPartsLogisticLossDetails.Where(r => usedPartsLogisticLossDetailsId.Any(v => v == r.Id));
            foreach(var usedPartsLogisticLossDetail in usedPartsLogisticLossDetails) {
                var tempUsedPartsLogisticLossDetail = dbusedPartsLogisticLossDetails.FirstOrDefault(r => r.Id == usedPartsLogisticLossDetail.Id && r.ProcessStatus == usedPartsLogisticLossDetail.ProcessStatus);
                if(tempUsedPartsLogisticLossDetail != null) {
                    continue;
                }
                var ssUsedPartsStorage = new SsUsedPartsStorage {
                    Shipped = false,
                    DealerId = usedPartsLogisticLossBill.DealerId,
                    ClaimBillId = usedPartsLogisticLossDetail.ClaimBillId,
                    ClaimBillCode = usedPartsLogisticLossDetail.ClaimBillCode,
                    ClaimBillType = usedPartsLogisticLossDetail.ClaimBillType,
                    BranchId = usedPartsLogisticLossDetail.BranchId,
                    BranchName = usedPartsLogisticLossDetail.BranchName,
                    UsedPartsId = usedPartsLogisticLossDetail.UsedPartsId,
                    UsedPartsCode = usedPartsLogisticLossDetail.UsedPartsCode,
                    UsedPartsName = usedPartsLogisticLossDetail.UsedPartsName,
                    UsedPartsSerialNumber = usedPartsLogisticLossDetail.UsedPartsSerialNumber,
                    UsedPartsSupplierId = usedPartsLogisticLossDetail.UsedPartsSupplierId,
                    UsedPartsSupplierCode = usedPartsLogisticLossDetail.UsedPartsSupplierCode,
                    UsedPartsSupplierName = usedPartsLogisticLossDetail.UsedPartsSupplierName,
                    UsedPartsReturnPolicy = usedPartsLogisticLossDetail.UsedPartsReturnPolicy,
                    Quantity = usedPartsLogisticLossDetail.Quantity,
                    UnitPrice = usedPartsLogisticLossDetail.Price,
                    PartsManagementCost = usedPartsLogisticLossDetail.PartsManagementCost,
                    UsedPartsBarCode = usedPartsLogisticLossDetail.UsedPartsBarCode,
                    FaultyPartsId = usedPartsLogisticLossDetail.FaultyPartsId,
                    FaultyPartsCode = usedPartsLogisticLossDetail.FaultyPartsCode,
                    FaultyPartsName = usedPartsLogisticLossDetail.FaultyPartsName,
                    FaultyPartsSupplierId = usedPartsLogisticLossDetail.FaultyPartsSupplierId,
                    FaultyPartsSupplierCode = usedPartsLogisticLossDetail.FaultyPartsSupplierCode,
                    FaultyPartsSupplierName = usedPartsLogisticLossDetail.FaultyPartsSupplierName,
                    IfFaultyParts = usedPartsLogisticLossDetail.IfFaultyParts,
                    ResponsibleUnitId = usedPartsLogisticLossDetail.ResponsibleUnitId,
                    ResponsibleUnitCode = usedPartsLogisticLossDetail.ResponsibleUnitCode,
                    ResponsibleUnitName = usedPartsLogisticLossDetail.ResponsibleUnitName,
                    NewPartsId = usedPartsLogisticLossDetail.NewPartsId,
                    NewPartsCode = usedPartsLogisticLossDetail.NewPartsCode,
                    NewPartsName = usedPartsLogisticLossDetail.NewPartsName,
                    NewPartsSupplierId = usedPartsLogisticLossDetail.NewPartsSupplierId,
                    NewPartsSupplierCode = usedPartsLogisticLossDetail.NewPartsSupplierCode,
                    NewPartsSupplierName = usedPartsLogisticLossDetail.NewPartsSupplierName,
                    NewPartsSerialNumber = usedPartsLogisticLossDetail.NewPartsSerialNumber,
                    NewPartsSecurityNumber = usedPartsLogisticLossDetail.NewPartsSecurityNumber,
                    PartsSalesCategoryId = usedPartsLogisticLossDetail.PartsSalesCategoryId,
                    ClaimBillCreateTime = usedPartsLogisticLossDetail.ClaimBillCreateTime
                };
                InsertToDatabase(ssUsedPartsStorage);
                new SsUsedPartsStorageAch(this.DomainService).InsertSsUsedPartsStorageValidate(ssUsedPartsStorage);
            }
            #region 同步更新索赔单和维修单
            var usedPartsBarCodes = usedPartsLogisticLossDetails.Select(r => r.UsedPartsBarCode).ToArray();

            //var repairOrderStruct = (from a in ObjectContext.RepairOrders
            //                         from b in a.RepairOrderFaultReasons
            //                         from c in b.RepairOrderItemDetails
            //                         from d in c.RepairOrderMaterialDetails
            //                         where a.Status != (int)DcsRepairStatus.作废
            //                         where usedPartsBarCodes.Contains(d.UsedPartsBarCode)
            //                         select new {
            //                             RepairOrderId = a.Id,
            //                             RepairOrderMaterialDetailId = d.Id
            //                         }).ToArray();
            //var repairClaimStruct = (from a in ObjectContext.RepairClaimBills
            //                         from b in a.RepairClaimItemDetails
            //                         from c in b.RepairClaimMaterialDetails
            //                         where a.Status != (int)DcsRepairClaimBillMaintClaimStatus.作废
            //                         where usedPartsBarCodes.Contains(c.UsedPartsBarCode)
            //                         select new {
            //                             RepairClaimBillId = a.Id,
            //                             RepairClaimMaterialDetailId = c.Id
            //                         }).ToArray();
            //var RepairOrderMaterialDetailIds = repairOrderStruct.Select(r => r.RepairOrderMaterialDetailId).ToArray();
            //var RepairClaimBillIds = repairClaimStruct.Select(r => r.RepairClaimBillId).ToArray();
            //var RepairClaimMaterialDetailIds = repairClaimStruct.Select(r => r.RepairClaimMaterialDetailId).ToArray();
            //var RepairOrderMaterialsDetailNeedChange = ObjectContext.RepairOrderMaterialDetails.Where(r => RepairOrderMaterialDetailIds.Contains(r.Id)).ToArray();
            //var RepairClaimBillsNeedChange = ObjectContext.RepairClaimBills.Where(r => RepairClaimBillIds.Contains(r.Id)).ToArray();
            //var RepairClaimMaterialDetailsNeedChange = ObjectContext.RepairClaimMaterialDetails.Where(r => RepairClaimMaterialDetailIds.Contains(r.Id)).ToArray();
            //foreach(var item in RepairClaimMaterialDetailsNeedChange) {
            //    item.UsedPartsDisposalStatus = (int)DcsRepairClaimMaterialDetailUsedPartsDisposalStatus.未发运;
            //    UpdateToDatabase(item);
            //}
            //foreach(var item in RepairOrderMaterialsDetailNeedChange) {
            //    item.UsedPartsDisposalStatus = (int)DcsRepairClaimMaterialDetailUsedPartsDisposalStatus.未发运;
            //    UpdateToDatabase(item);
            //}
            //foreach(var item in RepairClaimBillsNeedChange) {
            //    item.UsedPartsDisposalStatus = (int)DcsRepairClaimBillUsedPartsDisposalStatus.旧件未全部到达;
            //    UpdateToDatabase(item);
            //}
            #endregion


        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:09:49
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 修改物流损失单(UsedPartsLogisticLossBill usedPartsLogisticLossBill) {
            new UsedPartsLogisticLossBillAch(this).修改物流损失单(usedPartsLogisticLossBill);
        }
    }
}
