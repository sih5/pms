﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        [Update(UsingCustomMethod = true)]
        public void 生成旧件发运单(UsedPartsShippingOrder usedPartsShippingOrder) {
            var usedPartsShippingDetails = usedPartsShippingOrder.UsedPartsShippingDetails.ToArray();
            var salesUnitAffiWarehouseId = 旧件发运查询旧件仓库(usedPartsShippingOrder.PartsSalesCategoryId).Where(r => r.Id == usedPartsShippingOrder.DestinationWarehouseId).ToArray();
            if(!salesUnitAffiWarehouseId.Any()) {
                throw new ValidationException("收货仓库隶属品牌与旧件发运单品牌不符");
            }
            //维修保养索赔单
            var claimBillIds = usedPartsShippingDetails.Where(r => r.ClaimBillType == (int)DcsClaimBillType.保养索赔 || r.ClaimBillType == (int)DcsClaimBillType.维修索赔).Select(r => r.ClaimBillId).ToArray();
            if(claimBillIds.Length > 0) {
                var usedPartsBarCodesForClaim = usedPartsShippingDetails.Where(r => r.ClaimBillType == (int)DcsClaimBillType.保养索赔 || r.ClaimBillType == (int)DcsClaimBillType.维修索赔).Select(r => r.UsedPartsBarCode).ToArray();

                var ssUsedPartsStoragesForClaim = ObjectContext.SsUsedPartsStorages.Where(r => usedPartsBarCodesForClaim.Contains(r.UsedPartsBarCode)).ToArray();///////
                //var repairClaimBills = ObjectContext.RepairClaimBills.Where(r => claimBillIds.Contains(r.Id) && r.Status != (int)DcsRepairClaimBillRepairClaimStatus.作废).ToArray();
                //var allRepairClaimMaterialDetail = from a in ObjectContext.RepairClaimBills
                //                                   from b in a.RepairClaimItemDetails
                //                                   from c in b.RepairClaimMaterialDetails
                //                                   where claimBillIds.Contains(a.Id)
                //                                   select c;
                var usedPartsBarCodesForClaimCheck = usedPartsBarCodesForClaim.Except(ssUsedPartsStoragesForClaim.Select(v => v.UsedPartsBarCode)).ToArray();
                if(usedPartsBarCodesForClaimCheck.Length > 0)
                    throw new ValidationException(string.Format(ErrorStrings.UsedPartsShippingOrder_Validation9, usedPartsBarCodesForClaimCheck.First()));
                if(ssUsedPartsStoragesForClaim.Any(r => r.UsedPartsReturnPolicy != (int)DcsPartsWarrantyTermReturnPolicy.返回本部))
                    throw new ValidationException(ErrorStrings.UsedPartsShippingOrder_Validation1);
                if(ssUsedPartsStoragesForClaim.Any(r => r.Shipped))
                    throw new ValidationException(ErrorStrings.UsedPartsShippingOrder_Validation2);
                //foreach(var usedPartsShippingDetail in usedPartsShippingDetails) {
                //    var repairClaimBill = repairClaimBills.FirstOrDefault(r => r.Id == usedPartsShippingDetail.ClaimBillId);
                //    if(repairClaimBill == null || repairClaimBill.RejectStatus == (int)DcsRejectStatus.已驳回)
                //        throw new ValidationException("索赔单单据被驳回或不存在，对应的旧件不允许生成旧件发运");
                //    if(usedPartsShippingDetail.ClaimBillType == (int)DcsClaimBillType.保养索赔 && usedPartsShippingDetail.ClaimBillType == (int)DcsClaimBillType.维修索赔) {
                //        usedPartsShippingDetail.SubDealerId = repairClaimBill.FirstClassStationId;
                //        usedPartsShippingDetail.SubDealerCode = repairClaimBill.FirstClassStationCode;
                //        usedPartsShippingDetail.SubDealerName = repairClaimBill.FirstClassStationName;
                //    }

                //}
                foreach(var ssUsedPartsStorage in ssUsedPartsStoragesForClaim) {
                    UpdateToDatabase(ssUsedPartsStorage);
                    ssUsedPartsStorage.Shipped = true;
                    new SsUsedPartsStorageAch(this).UpdateSsUsedPartsStorageValidate(ssUsedPartsStorage);
                }
                //var repairOrderMaterialDetails = ObjectContext.RepairOrderMaterialDetails.Where(r => usedPartsBarCodesForClaim.Contains(r.UsedPartsBarCode)).ToArray();
                //foreach(var repairOrderMaterialDetail in repairOrderMaterialDetails) {
                //    repairOrderMaterialDetail.UsedPartsDisposalStatus = (int)DcsRepairClaimMaterialDetailUsedPartsDisposalStatus.已发运;
                //    UpdateToDatabase(repairOrderMaterialDetail);
                //    //var tempRepairClaimMaterialDetail = allRepairClaimMaterialDetail.SingleOrDefault(r => r.RepairOrderMaterialDetailId == repairOrderMaterialDetail.Id);
                //    //if(tempRepairClaimMaterialDetail != null) {
                //    //    tempRepairClaimMaterialDetail.UsedPartsDisposalStatus = (int)DcsRepairClaimMaterialDetailUsedPartsDisposalStatus.已发运;
                //    //    UpdateToDatabase(tempRepairClaimMaterialDetail);
                //    //}
                //}
            }
            //配件索赔单
            var partsClaimOrderIds = usedPartsShippingDetails.Where(r => r.ClaimBillType == (int)DcsClaimBillType.配件索赔).Select(r => r.ClaimBillId).ToArray();
            if(partsClaimOrderIds.Length > 0) {

                var usedPartsBarcodesForPartsClaim = usedPartsShippingDetails.Where(r => r.ClaimBillType == (int)DcsClaimBillType.配件索赔).Select(r => r.UsedPartsBarCode).ToArray();
                var ssUsedPartsStoragesForPartsClaim = ObjectContext.SsUsedPartsStorages.Where(r => usedPartsBarcodesForPartsClaim.Contains(r.UsedPartsBarCode)).ToArray();
                //var partsClaimOrders = ObjectContext.PartsClaimOrders.Where(r => partsClaimOrderIds.Contains(r.Id) && r.Status != (int)DcsRepairClaimBillRepairClaimStatus.作废).ToArray();
                //usedPartsBarcodesForPartsClaim = usedPartsBarcodesForPartsClaim.Except(ssUsedPartsStoragesForPartsClaim.Select(v => v.UsedPartsBarCode)).ToArray();
                //if(usedPartsBarcodesForPartsClaim.Length > 0)
                //    throw new ValidationException(string.Format(ErrorStrings.UsedPartsShippingOrder_Validation9, usedPartsBarcodesForPartsClaim.First()));
                //if(ssUsedPartsStoragesForPartsClaim.Any(r => r.UsedPartsReturnPolicy != (int)DcsPartsWarrantyTermReturnPolicy.返回本部))
                //    throw new ValidationException(ErrorStrings.UsedPartsShippingOrder_Validation1);
                //if(ssUsedPartsStoragesForPartsClaim.Any(r => r.Shipped))
                //    throw new ValidationException(ErrorStrings.UsedPartsShippingOrder_Validation2);
                //foreach(var usedPartsShippingDetail in usedPartsShippingDetails) {
                //    var partsClaimOrder = partsClaimOrders.FirstOrDefault(r => r.Id == usedPartsShippingDetail.ClaimBillId);
                //    if(partsClaimOrder == null)
                //        throw new ValidationException("索赔单单据被驳回或不存在，对应的旧件不允许生成旧件发运");
                //    if(usedPartsShippingDetail.ClaimBillType == (int)DcsClaimBillType.配件索赔) {
                //        usedPartsShippingDetail.SubDealerId = partsClaimOrder.FirstClassStationId;
                //        usedPartsShippingDetail.SubDealerCode = partsClaimOrder.FirstClassStationCode;
                //        usedPartsShippingDetail.SubDealerName = partsClaimOrder.FirstClassStationName;
                //    }
                //    foreach(var ssUsedPartsStorage in ssUsedPartsStoragesForPartsClaim) {
                //        UpdateToDatabase(ssUsedPartsStorage);
                //        ssUsedPartsStorage.Shipped = true;
                //        new SsUsedPartsStorageAch(this).UpdateSsUsedPartsStorageValidate(ssUsedPartsStorage);
                //    }
                //}
                //var partsClaimMaterialDetails = ObjectContext.PartsClaimMaterialDetails.Where(r => usedPartsBarcodesForPartsClaim.Contains(r.UsedPartsBarCode)).ToArray();
                //foreach(var partsClaimMaterialDetail in partsClaimMaterialDetails) {
                //    partsClaimMaterialDetail.UsedPartsDisposalStatus = (int)DcsRepairClaimMaterialDetailUsedPartsDisposalStatus.已发运;
                //    UpdateToDatabase(partsClaimMaterialDetail);
                //}
            }
        }

        [Update(UsingCustomMethod = true)]
        public void 修改旧件发运单(UsedPartsShippingOrder usedPartsShippingOrder) {
            CheckEntityState(usedPartsShippingOrder);
            var dbusedPartsShippingOrder = ObjectContext.UsedPartsShippingOrders.Where(r => r.Id == usedPartsShippingOrder.Id && r.Status == (int)DcsUsedPartsShippingOrderStatus.新增).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbusedPartsShippingOrder == null)
                throw new ValidationException(ErrorStrings.UsedPartsShippingOrder_Validation3);
            //增加收货仓库隶属品牌与旧件发运单品牌校验
            var salesUnitAffiWarehouseId = 旧件发运查询旧件仓库(usedPartsShippingOrder.PartsSalesCategoryId).Where(r => r.Id == usedPartsShippingOrder.DestinationWarehouseId).ToArray();
            if(!salesUnitAffiWarehouseId.Any()) {
                throw new ValidationException("收货仓库隶属品牌与旧件发运单品牌不符");
            }
            var usedPartsShippingDetails = ChangeSet.GetAssociatedChanges(usedPartsShippingOrder, r => r.UsedPartsShippingDetails).Cast<UsedPartsShippingDetail>().ToArray();
            var insertUsedPartsShippingDetails = usedPartsShippingDetails.Where(r => ChangeSet.GetChangeOperation(r) == ChangeOperation.Insert && usedPartsShippingDetails.Any(v => ChangeSet.GetChangeOperation(v) == ChangeOperation.Delete && r.UsedPartsBarCode.ToLower() == v.UsedPartsBarCode.ToLower())).ToArray();
            var deleteUsedPartsShippingDetails = usedPartsShippingDetails.Where(r => ChangeSet.GetChangeOperation(r) == ChangeOperation.Delete && usedPartsShippingDetails.Any(v => ChangeSet.GetChangeOperation(v) == ChangeOperation.Insert && r.UsedPartsBarCode.ToLower() == v.UsedPartsBarCode.ToLower())).ToArray();
            //待处理的清单
            var pendingUsedPartsShippingDetails = usedPartsShippingDetails.Except(insertUsedPartsShippingDetails).Except(deleteUsedPartsShippingDetails).ToArray();
            var usedPartsBarCodes = pendingUsedPartsShippingDetails.Select(r => r.UsedPartsBarCode.ToLower()).ToArray();
            var ssUsedPartsStorages = ObjectContext.SsUsedPartsStorages.Where(r => usedPartsBarCodes.Contains(r.UsedPartsBarCode.ToLower())).ToArray();
            var pendingBarcodes = pendingUsedPartsShippingDetails.Select(r => r.UsedPartsBarCode).ToArray();
            //ar repairClaimMaterialDetails = ObjectContext.RepairClaimMaterialDetails.Where(r => pendingBarcodes.Any(v => r.UsedPartsBarCode == v)).ToArray();
            //var partsClaimMaterialDetails = ObjectContext.PartsClaimMaterialDetails.Where(r => pendingBarcodes.Any(v => r.UsedPartsBarCode == v)).ToArray();
            foreach(var usedPartsShippingDetail in pendingUsedPartsShippingDetails) {
                switch(ChangeSet.GetChangeOperation(usedPartsShippingDetail)) {
                    case ChangeOperation.Insert:
                        if(usedPartsShippingDetail.ClaimBillType == (int)DcsClaimBillType.维修索赔 || usedPartsShippingDetail.ClaimBillType == (int)DcsClaimBillType.保养索赔) {
                            var detail = usedPartsShippingDetail;
                            //var repairClaimMaterialDetail = repairClaimMaterialDetails.SingleOrDefault(r => r.UsedPartsBarCode == detail.UsedPartsBarCode);
                            //if(repairClaimMaterialDetail != null) {
                            //    repairClaimMaterialDetail.UsedPartsDisposalStatus = (int)DcsRepairClaimMaterialDetailUsedPartsDisposalStatus.已发运;
                            //    var repairOrderMaterialDetail = repairClaimMaterialDetail.RepairOrderMaterialDetail;
                            //    if(repairOrderMaterialDetail != null) {
                            //        repairOrderMaterialDetail.UsedPartsDisposalStatus = (int)DcsRepairClaimMaterialDetailUsedPartsDisposalStatus.已发运;
                            //    }
                            //}

                        }

                        if(usedPartsShippingDetail.ClaimBillType == (int)DcsClaimBillType.维修索赔 || usedPartsShippingDetail.ClaimBillType == (int)DcsClaimBillType.保养索赔) {
                            var detail = usedPartsShippingDetail;
                            //var partsClaimMaterialDetail = partsClaimMaterialDetails.SingleOrDefault(r => r.UsedPartsBarCode == detail.UsedPartsBarCode);
                            //if(partsClaimMaterialDetail != null)
                            //    partsClaimMaterialDetail.UsedPartsDisposalStatus = (int)DcsRepairClaimMaterialDetailUsedPartsDisposalStatus.已发运;
                        }
                        var ssUsedPartsStorageInsert = ssUsedPartsStorages.FirstOrDefault(r => r.UsedPartsBarCode.ToLower() == usedPartsShippingDetail.UsedPartsBarCode.ToLower());
                        if(ssUsedPartsStorageInsert == null)
                            throw new ValidationException(string.Format(ErrorStrings.UsedPartsShippingOrder_Validation9, usedPartsShippingDetail.UsedPartsBarCode));
                        if(ssUsedPartsStorageInsert.Shipped)
                            throw new ValidationException(string.Format(ErrorStrings.UsedPartsShippingOrder_Validation8, ssUsedPartsStorageInsert.UsedPartsBarCode));
                        if(ssUsedPartsStorageInsert.UsedPartsReturnPolicy != (int)DcsPartsWarrantyTermReturnPolicy.返回本部)
                            throw new ValidationException(string.Format(ErrorStrings.UsedPartsShippingOrder_Validation7, ssUsedPartsStorageInsert.UsedPartsBarCode));
                        UpdateToDatabase(ssUsedPartsStorageInsert);
                        ssUsedPartsStorageInsert.Shipped = true;
                        new SsUsedPartsStorageAch(this).UpdateSsUsedPartsStorageValidate(ssUsedPartsStorageInsert);
                        break;
                    case ChangeOperation.Delete:
                        if(usedPartsShippingDetail.ClaimBillType == (int)DcsClaimBillType.维修索赔 || usedPartsShippingDetail.ClaimBillType == (int)DcsClaimBillType.保养索赔) {
                            var detail = usedPartsShippingDetail;
                            //var repairClaimMaterialDetail = ObjectContext.RepairClaimMaterialDetails.SingleOrDefault(r => r.UsedPartsBarCode == detail.UsedPartsBarCode);
                            //if(repairClaimMaterialDetail != null) {
                            //    repairClaimMaterialDetail.UsedPartsDisposalStatus = (int)DcsRepairClaimMaterialDetailUsedPartsDisposalStatus.未发运;
                            //    var repairOrderMaterialDetail = repairClaimMaterialDetail.RepairOrderMaterialDetail;
                            //    if(repairOrderMaterialDetail != null) {
                            //        repairOrderMaterialDetail.UsedPartsDisposalStatus = (int)DcsRepairClaimMaterialDetailUsedPartsDisposalStatus.未发运;
                            //    }
                            //}

                        }

                        if(usedPartsShippingDetail.ClaimBillType == (int)DcsClaimBillType.配件索赔) {
                            var detail = usedPartsShippingDetail;
                            //var partsClaimMaterialDetail = ObjectContext.PartsClaimMaterialDetails.SingleOrDefault(r => r.UsedPartsBarCode == detail.UsedPartsBarCode);
                            //if(partsClaimMaterialDetail != null)
                            //    partsClaimMaterialDetail.UsedPartsDisposalStatus = (int)DcsRepairClaimMaterialDetailUsedPartsDisposalStatus.未发运;
                        }
                        var ssUsedPartsStorageDelete = ssUsedPartsStorages.FirstOrDefault(r => r.UsedPartsBarCode.ToLower() == usedPartsShippingDetail.UsedPartsBarCode.ToLower());
                        if(ssUsedPartsStorageDelete == null)
                            throw new ValidationException(string.Format(ErrorStrings.UsedPartsShippingOrder_Validation9, usedPartsShippingDetail.UsedPartsBarCode));
                        UpdateToDatabase(ssUsedPartsStorageDelete);
                        ssUsedPartsStorageDelete.Shipped = false;
                        new SsUsedPartsStorageAch(this).UpdateSsUsedPartsStorageValidate(ssUsedPartsStorageDelete);
                        break;
                }
            }
        }

        [Update(UsingCustomMethod = true)]
        public void 确认旧件发运单(UsedPartsShippingOrder usedPartsShippingOrder) {
            var dbusedPartsShippingOrder = ObjectContext.UsedPartsShippingOrders.Where(r => r.Id == usedPartsShippingOrder.Id && r.Status == (int)DcsUsedPartsShippingOrderStatus.新增).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbusedPartsShippingOrder == null)
                throw new ValidationException(ErrorStrings.UsedPartsShippingOrder_Validation4);
            UpdateToDatabase(usedPartsShippingOrder);
            var userInfo = Utils.GetCurrentUserInfo();
            usedPartsShippingOrder.Status = (int)DcsUsedPartsShippingOrderStatus.已确认;
            if(usedPartsShippingOrder.UsedPartsShippingDetails.Count(v => v.ReceptionStatus == (int)DcsUsedPartsShippingDetailReceptionStatus.在途) > 0) {
                throw new ValidationException("存在验收状态为在途的清单");
            }
            if(usedPartsShippingOrder.UsedPartsShippingDetails.Count(v => v.ReceptionStatus == (int)DcsUsedPartsShippingDetailReceptionStatus.未接收 || v.ReceptionStatus == (int)DcsUsedPartsShippingDetailReceptionStatus.罚没) == usedPartsShippingOrder.UsedPartsShippingDetails.Count())
                usedPartsShippingOrder.InboundStatus = (int)DcsUsedPartsInboundStatus.终止入库;
            else
                usedPartsShippingOrder.InboundStatus = (int)DcsUsedPartsInboundStatus.待入库;
            usedPartsShippingOrder.ConfirmationTime = DateTime.Now;
            usedPartsShippingOrder.ConfirmorId = userInfo.Id;
            usedPartsShippingOrder.ConfirmorName = userInfo.Name;
            UpdateUsedPartsShippingOrderValidate(usedPartsShippingOrder);
            if(usedPartsShippingOrder.UsedPartsShippingDetails.Any(v => v.ReceptionStatus == (int)DcsUsedPartsShippingDetailReceptionStatus.未接收 || v.ReceptionStatus == (int)DcsUsedPartsShippingDetailReceptionStatus.罚没)) {
                var ssUsedPartsStorages = ObjectContext.SsUsedPartsStorages.Where(r => ObjectContext.UsedPartsShippingDetails.Any(v => v.UsedPartsShippingOrderId == usedPartsShippingOrder.Id && v.UsedPartsBarCode == r.UsedPartsBarCode)).ToArray();
                foreach(var ssUsedPartsStorage in ssUsedPartsStorages)
                    DeleteFromDatabase(ssUsedPartsStorage);
            }
            var usedPartsShippingDetais = usedPartsShippingOrder.UsedPartsShippingDetails.ToArray();
            var usedPartsShippingDetaisReceived = usedPartsShippingDetais.Where(r => r.ReceptionStatus == (int)DcsUsedPartsShippingDetailReceptionStatus.已接收 || r.ReceptionStatus == (int)DcsUsedPartsShippingDetailReceptionStatus.不合格).ToArray();
            var barCodesReceived = usedPartsShippingDetaisReceived.Select(r => r.UsedPartsBarCode).ToArray();
            #region 修改维修保养索赔单旧件处理状态
            //var repairClaimMaterialDetails = ObjectContext.RepairClaimMaterialDetails.Where(r => barCodesReceived.Any(v => v == r.UsedPartsBarCode)).ToArray();
            //var repairClaimMaterialDetailsQuery = from a in ObjectContext.RepairClaimBills.Where(r => r.Status != (int)DcsRepairClaimBillRepairClaimStatus.作废)
            //                                      from b in a.RepairClaimItemDetails
            //                                      from c in b.RepairClaimMaterialDetails
            //                                      select c;
            //var repairClaimMaterialDetails = repairClaimMaterialDetailsQuery.Where(r => barCodesReceived.Any(v => v == r.UsedPartsBarCode)).ToArray();
            //var repairClaims = repairClaimMaterialDetails.Select(r => r.RepairClaimItemDetail).Select(r => r.RepairClaimBill).ToArray();
            //var idforCheck1 = repairClaimMaterialDetails.Select(r => r.Id).ToArray();//已经收到旧件的配件所在的索赔单材料清单
            //var repairClaimIds = repairClaims.Select(r => r.Id).ToArray();
            //var barCodesOfRepairClaimMaterialDetails = repairClaimMaterialDetails.Select(r => r.UsedPartsBarCode).ToArray();
            //foreach(var repairClaimMaterialDetail in repairClaimMaterialDetails) {
            //    repairClaimMaterialDetail.UsedPartsDisposalStatus = (int)DcsRepairClaimMaterialDetailUsedPartsDisposalStatus.已到达;

            //    //更新材料费
            //    var UPShippingDetail = usedPartsShippingDetaisReceived.Where(r => r.UsedPartsBarCode == repairClaimMaterialDetail.UsedPartsBarCode).FirstOrDefault();
            //    if((repairClaimMaterialDetail.MaterialCost + (UPShippingDetail.UsedPartsEncourageAmount ?? 0)) < 0) {
            //        throw new ValidationException("旧件条码：" + repairClaimMaterialDetail.UsedPartsBarCode + "合并激励金额后，材料费为负");
            //    }
            //    repairClaimMaterialDetail.MaterialCost = repairClaimMaterialDetail.MaterialCost + (UPShippingDetail.UsedPartsEncourageAmount ?? 0);
            //    //如果不合格，更新配件管理费
            //    if(UPShippingDetail.ReceptionStatus == (int)DcsUsedPartsShippingDetailReceptionStatus.不合格) {
            //        repairClaimMaterialDetail.PartsManagementCost = 0;
            //    }

            //    var repairOrderMaterialDetail = ObjectContext.RepairOrderMaterialDetails.Where(r => r.UsedPartsBarCode == repairClaimMaterialDetail.UsedPartsBarCode &&
            //        r.Id == repairClaimMaterialDetail.RepairOrderMaterialDetailId).SingleOrDefault();
            //    if(repairOrderMaterialDetail != null) {
            //        repairOrderMaterialDetail.UsedPartsDisposalStatus = (int)DcsRepairClaimMaterialDetailUsedPartsDisposalStatus.已到达;
            //        UpdateToDatabase(repairOrderMaterialDetail);
            //    }
            //    UpdateToDatabase(repairClaimMaterialDetail);
            //}
            //foreach(var rc in repairClaims) {
            //    var rcmd = rc.RepairClaimItemDetails.SelectMany(r => r.RepairClaimMaterialDetails).ToArray();
            //    var mc = rcmd.Sum(r => r.MaterialCost);
            //    var pmc = rcmd.Sum(r => r.PartsManagementCost);
            //    rc.MaterialCost = mc;
            //    rc.PartsManagementCost = pmc;
            //    rc.TotalAmount = mc + pmc + rc.LaborCost + (rc.OtherCost ?? 0) + (rc.TrimCost ?? 0);
            //    UpdateToDatabase(rc);
            //    //倒冲维修索赔单供应商索赔清单的材料费、配件管理费  
            //    if(rc.RepairType.HasValue && rc.RepairType != (int)DcsRepairType.让度服务) {
            //        var repairClaimSupplierDetail = ObjectContext.RepairClaimSupplierDetails.FirstOrDefault(r => r.RepairClaimId == rc.Id);
            //        if(repairClaimSupplierDetail != null) {
            //            repairClaimSupplierDetail.PartsManagementCost = pmc;
            //            repairClaimSupplierDetail.MaterialCost = mc;
            //            repairClaimSupplierDetail.TotalAmount = pmc + mc + (repairClaimSupplierDetail.LaborCost ?? 0) + (repairClaimSupplierDetail.OtherCost ?? 0);
            //        }
            //    }
            //}

            //var structForRepairClaim = (from a in ObjectContext.RepairClaimBills
            //                            from b in a.RepairClaimItemDetails
            //                            from c in b.RepairClaimMaterialDetails
            //                            where repairClaimIds.Any(v => v == a.Id) && c.UsedPartsReturnPolicy == (int)DcsPartsWarrantyTermReturnPolicy.返回本部 && idforCheck1.All(r => r != c.Id)
            //                            select new {
            //                                ClaimBillId = a.Id,
            //                                c.UsedPartsBarCode,
            //                                ClaimMaterialDetailId = c.Id,
            //                                c.UsedPartsDisposalStatus
            //                            }).ToArray();
            //if(structForRepairClaim.Length > 0) {
            //    var groupsOfRepairClaim = structForRepairClaim.GroupBy(r => r.ClaimBillId);
            //    var repairClaimBillIdsNeedNotChange = new List<int>();
            //    foreach(var group in groupsOfRepairClaim) {
            //        //var flag = true;
            //        //需要返回本部但是还没收到的材料条码
            //        var tempArray = group.Where(r => r.UsedPartsDisposalStatus != (int)DcsRepairClaimMaterialDetailUsedPartsDisposalStatus.已到达 && r.UsedPartsDisposalStatus != (int)DcsRepairClaimMaterialDetailUsedPartsDisposalStatus.不再发运).ToArray();
            //        //foreach(var item in tempArray) {
            //        //    var tempCheck = barCodesOfRepairClaimMaterialDetails.All(r => r != item.UsedPartsBarCode);
            //        //    if(tempCheck) {
            //        //        flag = false;
            //        //        break;
            //        //    }
            //        //}
            //        //if(flag) {
            //        //    repairClaimBillIdsNeedChange.Add(group.Key);
            //        //}

            //        if(tempArray.Length != 0) {
            //            repairClaimBillIdsNeedNotChange.Add(group.Key);
            //        }
            //    }
            //    var repairClaimBillIdsNeedChange = repairClaimIds.Except(repairClaimBillIdsNeedNotChange);
            //    if(repairClaimBillIdsNeedChange.Count() > 0) {
            //        var repairClaimBillsNeedChange = ObjectContext.RepairClaimBills.Where(r => repairClaimBillIdsNeedChange.Contains(r.Id)).ToArray();
            //        foreach(var item in repairClaimBillsNeedChange) {
            //            item.UsedPartsDisposalStatus = (int)DcsRepairClaimBillUsedPartsDisposalStatus.旧件全部到达;
            //            UpdateToDatabase(item);
            //        }
            //    }
            //} else {
            //    foreach(var repairClaim in repairClaims) {
            //        repairClaim.UsedPartsDisposalStatus = (int)DcsRepairClaimBillUsedPartsDisposalStatus.旧件全部到达;
            //        UpdateToDatabase(repairClaim);
            //    }
            //}

            #endregion
            #region 修改配件索赔单旧件处理状态
            //var barCodesNotInBarCodesOfrRepairClaimMaterialDetails = barCodesReceived.Except(barCodesOfRepairClaimMaterialDetails).ToArray();
            //var partsClaimMaterialDetails = ObjectContext.PartsClaimMaterialDetails.Where(r => barCodesNotInBarCodesOfrRepairClaimMaterialDetails.Any(v => v == r.UsedPartsBarCode)).ToArray();

            ////var partClaims = partsClaimMaterialDetails.Select(r => r.PartsClaimRepairItemDetail).Select(r => r.PartsClaimOrder).ToArray();
            //var partClaimIds = repairClaims.Select(r => r.Id).ToArray();
            //var barCodesOfPartsClaimMaterialDetails = partsClaimMaterialDetails.Select(r => r.UsedPartsBarCode).ToArray();
            //var idforCheck2 = partsClaimMaterialDetails.Select(r => r.Id).ToArray();
            //foreach(var repairClaimMaterialDetail in partsClaimMaterialDetails) {
            //    repairClaimMaterialDetail.UsedPartsDisposalStatus = (int)DcsRepairClaimMaterialDetailUsedPartsDisposalStatus.已到达;
            //    UpdateToDatabase(repairClaimMaterialDetail);
            //}
            //var structForPartsClaim = (from a in ObjectContext.PartsClaimOrders
            //                           from b in a.PartsClaimRepairItemDetails
            //                           from c in b.PartsClaimMaterialDetails
            //                           where partClaimIds.Any(v => v == a.Id) && c.UsedPartsReturnPolicy == (int)DcsPartsWarrantyTermReturnPolicy.返回本部 && idforCheck2.All(r => r != c.Id)
            //                           select new {
            //                               ClaimBillId = a.Id,
            //                               c.UsedPartsBarCode,
            //                               MaterialDetailId = c.Id,
            //                               c.UsedPartsDisposalStatus
            //                           }).ToArray();
            //if(structForRepairClaim.Length > 0) {
            //    var groupsOfPartsClaim = structForPartsClaim.GroupBy(r => r.ClaimBillId);
            //    var partsClaimBillIdsNeedNotChange = new List<int>();
            //    foreach(var group in groupsOfPartsClaim) {
            //        //var flag = true;
            //        var tempArray = group.Where(r => r.UsedPartsDisposalStatus != (int)DcsRepairClaimMaterialDetailUsedPartsDisposalStatus.已到达 && r.UsedPartsDisposalStatus != (int)DcsRepairClaimMaterialDetailUsedPartsDisposalStatus.不再发运).ToArray();
            //        //foreach(var item in tempArray) {
            //        //    var tempCheck = barCodesOfPartsClaimMaterialDetails.All(r => r != item.UsedPartsBarCode);
            //        //    if(tempCheck) {
            //        //        flag = false;
            //        //        break;
            //        //    }
            //        //}
            //        //if(flag) {
            //        //    partsClaimBillIdsNeedChange.Add(group.Key);
            //        //}
            //        if(tempArray.Length != 0) {
            //            partsClaimBillIdsNeedNotChange.Add(group.Key);
            //        }
            //    }
            //    var partsClaimBillIdsNeedChange = partClaimIds.Except(partsClaimBillIdsNeedNotChange);
            //    if(partsClaimBillIdsNeedChange.Count() > 0) {
            //        //var partsClaimBillsNeedChange = ObjectContext.PartsClaimOrders.Where(r => partsClaimBillIdsNeedChange.Contains(r.Id)).ToArray();
            //        //foreach(var item in partsClaimBillsNeedChange) {
            //        //    item.UsedPartsDisposalStatus = (int)DcsRepairClaimBillUsedPartsDisposalStatus.旧件全部到达;
            //        //    UpdateToDatabase(item);
            //        //}
            //    }
            //} else {
            //    foreach(var pairClaim in partClaims) {
            //        pairClaim.UsedPartsDisposalStatus = (int)DcsRepairClaimBillUsedPartsDisposalStatus.旧件全部到达;
            //        UpdateToDatabase(pairClaim);
            //    }
            //}


            #endregion
            var usedPartsShippingDetailsNotReceived = usedPartsShippingOrder.UsedPartsShippingDetails.Where(r => r.ReceptionStatus == (int)DcsUsedPartsShippingDetailReceptionStatus.未接收).ToArray();
            if(usedPartsShippingDetailsNotReceived.Length > 0) {
                var usedPartsLogisticLossBill = new UsedPartsLogisticLossBill {
                    DealerId = usedPartsShippingOrder.DealerId,
                    DealerCode = usedPartsShippingOrder.DealerCode,
                    DealerName = usedPartsShippingOrder.DealerName,
                    UsedPartsShippingOrderId = usedPartsShippingOrder.Id,
                    LogisticCompanyId = usedPartsShippingOrder.LogisticCompanyId,
                    LogisticCompanyCode = usedPartsShippingOrder.LogisticCompanyCode,
                    LogisticCompanyName = usedPartsShippingOrder.LogisticCompanyName,
                    TotalAmount = usedPartsShippingOrder.TotalAmount,
                    Remark = usedPartsShippingOrder.Remark
                };
                foreach(var usedPartsShippingDetail in usedPartsShippingDetailsNotReceived) {
                    usedPartsLogisticLossBill.UsedPartsLogisticLossDetails.Add(new UsedPartsLogisticLossDetail {
                        UsedPartsLogisticLossBillId = usedPartsLogisticLossBill.Id,
                        SerialNumber = usedPartsShippingDetail.SerialNumber,
                        BranchId = usedPartsShippingDetail.BranchId,
                        BranchName = usedPartsShippingDetail.BranchName,
                        ClaimBillId = usedPartsShippingDetail.ClaimBillId,
                        ClaimBillType = usedPartsShippingDetail.ClaimBillType,
                        ClaimBillCode = usedPartsShippingDetail.ClaimBillCode,
                        UsedPartsId = usedPartsShippingDetail.UsedPartsId,
                        UsedPartsCode = usedPartsShippingDetail.UsedPartsCode,
                        UsedPartsName = usedPartsShippingDetail.UsedPartsName,
                        UsedPartsSerialNumber = usedPartsShippingDetail.UsedPartsSerialNumber,
                        UsedPartsSupplierId = usedPartsShippingDetail.UsedPartsSupplierId,
                        UsedPartsSupplierCode = usedPartsShippingDetail.UsedPartsSupplierCode,
                        UsedPartsSupplierName = usedPartsShippingDetail.UsedPartsSupplierName,
                        UsedPartsBarCode = usedPartsShippingDetail.UsedPartsBarCode,
                        ProcessStatus = (int)DcsUsedPartsLogisticLossDetailProcessStatus.丢失,
                        Quantity = usedPartsShippingDetail.Quantity,
                        Price = usedPartsShippingDetail.UnitPrice,
                        PartsManagementCost = usedPartsShippingDetail.PartsManagementCost,
                        UsedPartsReturnPolicy = usedPartsShippingDetail.UsedPartsReturnPolicy,
                        FaultyPartsId = usedPartsShippingDetail.FaultyPartsId,
                        FaultyPartsCode = usedPartsShippingDetail.FaultyPartsCode,
                        FaultyPartsName = usedPartsShippingDetail.FaultyPartsName,
                        FaultyPartsSupplierId = usedPartsShippingDetail.FaultyPartsSupplierId,
                        FaultyPartsSupplierCode = usedPartsShippingDetail.FaultyPartsSupplierCode,
                        FaultyPartsSupplierName = usedPartsShippingDetail.FaultyPartsSupplierName,
                        IfFaultyParts = usedPartsShippingDetail.IfFaultyParts,
                        ResponsibleUnitId = usedPartsShippingDetail.ResponsibleUnitId,
                        ResponsibleUnitCode = usedPartsShippingDetail.ResponsibleUnitCode,
                        ResponsibleUnitName = usedPartsShippingDetail.ResponsibleUnitName,
                        Shipped = usedPartsShippingDetail.Shipped,
                        NewPartsId = usedPartsShippingDetail.NewPartsId,
                        NewPartsCode = usedPartsShippingDetail.NewPartsCode,
                        NewPartsName = usedPartsShippingDetail.NewPartsName,
                        NewPartsSupplierId = usedPartsShippingDetail.NewPartsSupplierId,
                        NewPartsSupplierCode = usedPartsShippingDetail.NewPartsSupplierCode,
                        NewPartsSupplierName = usedPartsShippingDetail.NewPartsSupplierName,
                        NewPartsSerialNumber = usedPartsShippingDetail.NewPartsSerialNumber,
                        NewPartsSecurityNumber = usedPartsShippingDetail.NewPartsSecurityNumber,
                        Remark = usedPartsShippingDetail.ReceptionRemark,
                        PartsSalesCategoryId = usedPartsShippingDetail.PartsSalesCategoryId,
                        ClaimBillCreateTime = usedPartsShippingDetail.ClaimBillCreateTime
                    });

                }
                InsertToDatabase(usedPartsLogisticLossBill);
                new UsedPartsLogisticLossBillAch(this).InsertUsedPartsLogisticLossBillValidate(usedPartsLogisticLossBill);
            }
        }

        [Update(UsingCustomMethod = true)]
        public void 作废旧件发运单(UsedPartsShippingOrder usedPartsShippingOrder) {
            var dbusedPartsShippingOrder = ObjectContext.UsedPartsShippingOrders.Where(r => r.Id == usedPartsShippingOrder.Id && r.Status == (int)DcsUsedPartsShippingOrderStatus.新增).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbusedPartsShippingOrder == null)
                throw new ValidationException(ErrorStrings.UsedPartsShippingOrder_Validation5);
            var ssUsedPartsStorages = ObjectContext.SsUsedPartsStorages.Where(r => ObjectContext.UsedPartsShippingDetails.Any(v => v.UsedPartsShippingOrderId == usedPartsShippingOrder.Id && v.UsedPartsBarCode.ToLower() == r.UsedPartsBarCode.ToLower())).ToArray();
            UpdateToDatabase(usedPartsShippingOrder);
            usedPartsShippingOrder.Status = (int)DcsUsedPartsShippingOrderStatus.作废;
            UpdateUsedPartsShippingOrderValidate(usedPartsShippingOrder);
            foreach(var ssUsedPartsStorage in ssUsedPartsStorages) {
                ssUsedPartsStorage.Shipped = false;
                new SsUsedPartsStorageAch(this).UpdateSsUsedPartsStorageValidate(ssUsedPartsStorage);
            }
            var usedPartsBarCodes = ObjectContext.UsedPartsShippingDetails.Where(v => v.UsedPartsShippingOrderId == usedPartsShippingOrder.Id).Select(r => r.UsedPartsBarCode).ToArray();



            //var repairOrderStruct = (from a in ObjectContext.RepairOrders
            //                         from b in a.RepairOrderFaultReasons
            //                         from c in b.RepairOrderItemDetails
            //                         from d in c.RepairOrderMaterialDetails
            //                         where a.Status != (int)DcsRepairStatus.作废
            //                         where usedPartsBarCodes.Contains(d.UsedPartsBarCode)
            //                         select new {
            //                             RepairOrderId = a.Id,
            //                             RepairOrderMaterialDetailId = d.Id
            //                         }).ToArray();
            //var repairClaimStruct = (from a in ObjectContext.RepairClaimBills
            //                         from b in a.RepairClaimItemDetails
            //                         from c in b.RepairClaimMaterialDetails
            //                         where a.Status != (int)DcsRepairClaimBillMaintClaimStatus.作废
            //                         where usedPartsBarCodes.Contains(c.UsedPartsBarCode)
            //                         select new {
            //                             RepairClaimBillId = a.Id,
            //                             RepairClaimMaterialDetailId = c.Id
            //                         }).ToArray();
            //var RepairOrderMaterialDetailIds = repairOrderStruct.Select(r => r.RepairOrderMaterialDetailId).ToArray();
            //var RepairClaimBillIds = repairClaimStruct.Select(r => r.RepairClaimBillId).ToArray();
            //var RepairClaimMaterialDetailIds = repairClaimStruct.Select(r => r.RepairClaimMaterialDetailId).ToArray();
            //var RepairOrderMaterialsDetailNeedChange = ObjectContext.RepairOrderMaterialDetails.Where(r => RepairOrderMaterialDetailIds.Contains(r.Id)).ToArray();
            //var RepairClaimBillsNeedChange = ObjectContext.RepairClaimBills.Where(r => RepairClaimBillIds.Contains(r.Id)).ToArray();
            //var RepairClaimMaterialDetailsNeedChange = ObjectContext.RepairClaimMaterialDetails.Where(r => RepairClaimMaterialDetailIds.Contains(r.Id)).ToArray();
            //foreach(var item in RepairClaimMaterialDetailsNeedChange) {
            //    item.UsedPartsDisposalStatus = (int)DcsRepairClaimMaterialDetailUsedPartsDisposalStatus.未发运;
            //    UpdateToDatabase(item);
            //}
            //foreach(var item in RepairOrderMaterialsDetailNeedChange) {
            //    item.UsedPartsDisposalStatus = (int)DcsRepairClaimMaterialDetailUsedPartsDisposalStatus.未发运;
            //    UpdateToDatabase(item);
            //}
            //foreach(var item in RepairClaimBillsNeedChange) {
            //    item.UsedPartsDisposalStatus = (int)DcsRepairClaimBillUsedPartsDisposalStatus.旧件未全部到达;
            //    UpdateToDatabase(item);
            //}

            var userInfo = Utils.GetCurrentUserInfo();
            usedPartsShippingOrder.ModifierId = userInfo.Id;
            usedPartsShippingOrder.AbandonerName = userInfo.Name;
            usedPartsShippingOrder.ModifyTime = DateTime.Now;
            usedPartsShippingOrder.AbandonerId = userInfo.Id;
            usedPartsShippingOrder.AbandonerName = userInfo.Name;
            usedPartsShippingOrder.AbandonTime = DateTime.Now;
        }

        [Invoke(HasSideEffects = true)]
        public void 更新旧件发运单(int sourceId) {
            var usedPartsShippingOrder = ObjectContext.UsedPartsShippingOrders.Where(r => r.Id == sourceId).FirstOrDefault();
            if(usedPartsShippingOrder==null)
                throw new ValidationException("请选择旧件入库信息");
            usedPartsShippingOrder.InboundStatus = (int)DcsUsedPartsInboundStatus.终止入库;
            UpdateToDatabase(usedPartsShippingOrder);
            ObjectContext.SaveChanges();
        }
    }
}
