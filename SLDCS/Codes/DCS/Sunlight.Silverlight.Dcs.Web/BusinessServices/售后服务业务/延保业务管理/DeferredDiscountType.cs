﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Data.Extensions;
using Quartz.Xml;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DeferredDiscountTypeAch : DcsSerivceAchieveBase {
        //public DeferredDiscountTypeAch(DcsDomainService domainService)
        //    : base(domainService) {
        //}
        public void 延保折扣类型作废(DeferredDiscountType deferredDiscountType) {
            var deferredDiscount = ObjectContext.DeferredDiscountTypes.Where(r => r.Id == deferredDiscountType.Id).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(deferredDiscount == null)
                throw new ValidationException("单据不存在");
            var userInfo = Utils.GetCurrentUserInfo();
            deferredDiscount.Status = (int)DcsBaseDataStatus.作废;
            deferredDiscount.AbandonerId = userInfo.Id;
            deferredDiscount.AbandonerName = userInfo.Name;
            deferredDiscount.AbandonerTime = DateTime.Now;
            this.UpdateDeferredDiscountTypeValidate(deferredDiscount);
            UpdateToDatabase(deferredDiscount);
        }
    }

    partial class DcsDomainService {
        [Update(UsingCustomMethod = true)]
        public void 延保折扣类型作废(DeferredDiscountType deferredDiscountType) {
            new DeferredDiscountTypeAch(this).延保折扣类型作废(deferredDiscountType);
        }

        public IQueryable<DeferredDiscountType> GetDeferredDiscountTypesWithExtendedWarrantyProduct() {
            return ObjectContext.DeferredDiscountTypes.Include("ExtendedWarrantyProduct").OrderBy(e => e.Id);
        }
    }
}