﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.Net;
using System.ServiceModel.DomainServices.Server;
using System.Text;
using System.Transactions;
using System.Web.Configuration;
using FTService;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class ExtendedWarrantyOrderAch : DcsSerivceAchieveBase {
        public void 审核延保销售订单(ExtendedWarrantyOrder extendedWarrantyOrder) {
            var dbextendedWarrantyOrder = ObjectContext.ExtendedWarrantyOrders.Where(r => r.Id == extendedWarrantyOrder.Id && r.Status != (int)DcsExtendedWarrantyStatus.作废).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbextendedWarrantyOrder == null)
                throw new ValidationException("延保销售订单不存在");
            var extend = this.ObjectContext.ExtendedWarrantyOrders.FirstOrDefault(r => r.Id == extendedWarrantyOrder.Id && r.Status != (int)DcsExtendedWarrantyStatus.作废);
            if(extend == null)
                return;
            if(extendedWarrantyOrder.CheckerResult == (int)DcsCheckerResult.通过)
                extend.Status = (int)DcsExtendedWarrantyStatus.生效;
            if(extendedWarrantyOrder.CheckerResult == (int)DcsCheckerResult.驳回)
                extend.Status = (int)DcsExtendedWarrantyStatus.驳回;
            if(extendedWarrantyOrder.CheckerResult == (int)DcsCheckerResult.不通过)
                extend.Status = (int)DcsExtendedWarrantyStatus.审核不通过;
            extend.CheckerResult = extendedWarrantyOrder.CheckerResult;
            var userInfo = Utils.GetCurrentUserInfo();
            extendedWarrantyOrder.CheckerId = userInfo.Id;
            extendedWarrantyOrder.CheckerName = userInfo.Name;
            extendedWarrantyOrder.CheckerTime = DateTime.Now;
            if(extendedWarrantyOrder.CheckerResult == (int)DcsCheckerResult.通过) {
                var datetimenow = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss");
                延保扣减(extendedWarrantyOrder, datetimenow);
            }
        }
        public void 新增延保销售订单(ExtendedWarrantyOrder extendedWarrantyOrder) {
            extendedWarrantyOrder.Status = (int)DcsExtendedWarrantyStatus.新增;
            //ExtendedWarrantyOrderValidate(extendedWarrantyOrder);
            var extendedWarrantyOrderCode = new StringBuilder("YB");
            switch(extendedWarrantyOrder.PartsSalesCategoryName) {
                case "奥铃":
                    extendedWarrantyOrderCode.Append("AL");
                    break;
                case "拓陆者":
                    extendedWarrantyOrderCode.Append("TL");
                    break;
                case "欧马可":
                    extendedWarrantyOrderCode.Append("OK");
                    break;
                case "商务汽车":
                    extendedWarrantyOrderCode.Append("SW");
                    break;
                case "伽途":
                    extendedWarrantyOrderCode.Append("JT");
                    break;
                case "欧曼":
                    extendedWarrantyOrderCode.Append("OM");
                    break;
                case "时代":
                    extendedWarrantyOrderCode.Append("SD");
                    break;
                case "瑞沃":
                    extendedWarrantyOrderCode.Append("RW");
                    break;
            }
            if(string.IsNullOrWhiteSpace(extendedWarrantyOrder.ExtendedWarrantyOrderCode) || extendedWarrantyOrder.ExtendedWarrantyOrderCode == GlobalVar.ASSIGNED_BY_SERVER)
                extendedWarrantyOrder.ExtendedWarrantyOrderCode = extendedWarrantyOrderCode.Append(CodeGenerator.Generate("ExtendedWarrantyOrder")).ToString();
        }

        public void 更新延保销售订单(ExtendedWarrantyOrder extendedWarrantyOrder) {
            var dbExtendedWarrantyOrder = ObjectContext.ExtendedWarrantyOrders.Where(r => (r.Status == (int)DcsExtendedWarrantyStatus.驳回 || r.Status == (int)DcsExtendedWarrantyStatus.新增) && r.Id == extendedWarrantyOrder.Id).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbExtendedWarrantyOrder == null)
                throw new ValidationException("只能更新新建或者驳回状态的保销售订单");
            extendedWarrantyOrder.Status = (int)DcsExtendedWarrantyStatus.新增;

            //ExtendedWarrantyOrderValidate(extendedWarrantyOrder);
        }

        public void 提交延保销售订单(ExtendedWarrantyOrder extendedWarrantyOrder) {
            var dbExtendedWarrantyOrder = ObjectContext.ExtendedWarrantyOrders.Where(r => r.Id == extendedWarrantyOrder.Id).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbExtendedWarrantyOrder == null)
                throw new ValidationException("只能提交新建状态的保销售订单");
            var automaticAuditPeriod = ObjectContext.ExtendedWarrantyProducts.Any(x => ObjectContext.ExtendedWarrantyOrderLists.Any(r => x.AutomaticAuditPeriod != null && x.Id == r.ExtendedWarrantyProductId && r.ExtendedWarrantyOrderId == extendedWarrantyOrder.Id));
            extendedWarrantyOrder.Status = (int)DcsExtendedWarrantyStatus.提交;
            if(automaticAuditPeriod && extendedWarrantyOrder.SalesDate < extendedWarrantyOrder.CarSalesDate.AddMonths(6)) {
                extendedWarrantyOrder.Status = (int)DcsExtendedWarrantyStatus.生效;
            }
            var userInfo = Utils.GetCurrentUserInfo();
            extendedWarrantyOrder.SubmitterId = userInfo.Id;
            extendedWarrantyOrder.SubmitterName = userInfo.Name;
            extendedWarrantyOrder.SubmitterTime = DateTime.Now;
        }

        public void 作废延保销售订单(ExtendedWarrantyOrder extendedWarrantyOrder) {
            var dbExtendedWarrantyOrder = ObjectContext.ExtendedWarrantyOrders.Where(r => r.Id == extendedWarrantyOrder.Id).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbExtendedWarrantyOrder == null)
                throw new ValidationException("只能作废新建状态的保销售订单");
            var userInfo = Utils.GetCurrentUserInfo();
            extendedWarrantyOrder.Status = (int)DcsExtendedWarrantyStatus.作废;
            extendedWarrantyOrder.AbandonerId = userInfo.Id;
            extendedWarrantyOrder.AbandonerName = userInfo.Name;
            extendedWarrantyOrder.AbandonerTime = DateTime.Now;
            //this.UpdateExtendedWarrantyOrderValidate(extendedWarrantyOrder);
        }


        public void 驳回延保销售订单(ExtendedWarrantyOrder extendedWarrantyOrder) {
            var dbExtendedWarrantyOrder = ObjectContext.ExtendedWarrantyOrders.Where(r => r.Id == extendedWarrantyOrder.Id && (r.Status == (int)DcsExtendedWarrantyStatus.生效) || r.Status == (int)DcsExtendedWarrantyStatus.提交).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbExtendedWarrantyOrder == null)
                throw new ValidationException("只能驳回 生效或者提交 状态的延保销售订单");
            var userInfo = Utils.GetCurrentUserInfo();
            extendedWarrantyOrder.Status = (int)DcsExtendedWarrantyStatus.驳回;
            extendedWarrantyOrder.ModifierId = userInfo.Id;
            extendedWarrantyOrder.ModifierName = userInfo.Name;
            extendedWarrantyOrder.ModifierTime = DateTime.Now;
        }
        //    private void ExtendedWarrantyOrderValidate(ExtendedWarrantyOrder extendedWarrantyOrder) {
        //        var extendedWarrantyProductIds = extendedWarrantyOrder.ExtendedWarrantyOrderLists.Select(r => r.ExtendedWarrantyProductId).ToArray();
        //        var extendedWarrantyProducts = this.ObjectContext.ExtendedWarrantyProducts.Where(r => extendedWarrantyProductIds.Contains(r.Id)).ToArray();
        //        var days = (extendedWarrantyOrder.SalesDate - extendedWarrantyOrder.CarSalesDate).Days;
        //        foreach(var extendedWarrantyOrderList in extendedWarrantyOrder.ExtendedWarrantyOrderLists) {
        //            var extendedWarrantyProduct = extendedWarrantyProducts.SingleOrDefault(r => r.Id == extendedWarrantyOrderList.ExtendedWarrantyProductId);
        //            if(extendedWarrantyProduct == null)
        //                throw new ValidationException("找不到对应的延保产品");
        //            var payStartTimeCondition = extendedWarrantyOrder.CarSalesDate.AddMonths((int)extendedWarrantyProduct.PayStartTimeCondition);
        //            var payEndTimeCondition = extendedWarrantyOrder.CarSalesDate.AddMonths((int)extendedWarrantyProduct.PayEndTimeCondition);
        //            //var payStartTimeCondition = Math.Ceiling(extendedWarrantyProduct.PayStartTimeCondition * 30);
        //            //var payEndTimeCondition = Math.Ceiling(extendedWarrantyProduct.PayEndTimeCondition * 30);
        //            //if(payStartTimeCondition > days || days > payEndTimeCondition) {
        //            //    throw new ValidationException(string.Format("车辆不在延保产品:{0},在保时间上下限范围内", extendedWarrantyProduct.ExtendedWarrantyProductName));
        //            //}
        //            if(extendedWarrantyProduct.PayStartTimeMileage.HasValue && extendedWarrantyProduct.PayStartTimeMileage > extendedWarrantyOrder.Mileage) {
        //                throw new ValidationException(string.Format("车辆行驶里程小于延保产品:{0},在保里程下限", extendedWarrantyProduct.ExtendedWarrantyProductName));
        //            }
        //            if(extendedWarrantyProduct.PayEndTimeMileage.HasValue && extendedWarrantyProduct.PayEndTimeMileage < extendedWarrantyOrder.Mileage) {
        //                throw new ValidationException(string.Format("车辆行驶里程大于延保产品:{0},在保里程上限", extendedWarrantyProduct.ExtendedWarrantyProductName));
        //            }
        //        }
        //    }

        internal void 延保扣减(ExtendedWarrantyOrder extendedWarrantyOrder, string datetimenow) {
            var tempContext = new DcsEntities(this.ObjectContext.Connection.ConnectionString);
            #region 映射
            string subjuctName = "";
            string subjectCode = "";

            switch(extendedWarrantyOrder.PartsSalesCategoryName) {
                case "拓陆者萨普":
                    subjuctName = "佛山红岩汽车科技有限公司";
                    subjectCode = "6120";
                    break;
                case "蒙派克风景":
                    subjuctName = "潍坊红岩汽车科技有限公司";
                    subjectCode = "2600";
                    break;
                case "欧马可":
                    subjuctName = "北汽红岩汽车股份有限公司北京欧马可汽车销售分公司";
                    subjectCode = "2220";
                    break;
                case "奥铃":
                    subjuctName = "北汽红岩汽车股份有限公司北京汽车销售分公司";
                    subjectCode = "1100";
                    break;
                case "伽途":
                    subjuctName = "潍坊红岩汽车科技有限公司";
                    subjectCode = "2601";
                    break;
                case "时代":
                    subjuctName = "北汽红岩汽车股份有限公司诸城奥铃汽车厂";
                    subjectCode = "2290";
                    break;
                case "瑞沃":
                    subjuctName = "北汽红岩汽车股份有限公司诸城奥铃汽车厂";
                    subjectCode = "2290";
                    break;
                case "欧曼":
                    subjuctName = "北京红岩戴姆勒汽车有限公司";
                    subjectCode = "2450";
                    break;
                case "欧曼保外":
                    subjuctName = "北京红岩戴姆勒汽车有限公司";
                    subjectCode = "2450";
                    break;
                default:
                    break;
            }
            #endregion
            int tempcount = 1;
            string databaseName = WebConfigurationManager.AppSettings["DataBaseName"];
            var extendedWarrantyOrderLists = this.ObjectContext.ExtendedWarrantyOrderLists.Where(r => r.ExtendedWarrantyOrderId == extendedWarrantyOrder.Id).ToArray();
            var agioTypes = extendedWarrantyOrderLists.Select(v => v.AgioType);
            var deferredDiscountTypes = this.ObjectContext.DeferredDiscountTypes.Where(r => agioTypes.Contains(r.Id)).ToArray();
            var deferredDiscountType = deferredDiscountTypes.Where(r => r.ExtendedWarrantyAgioType == "会员购买延保产品折扣");
            if(deferredDiscountType.Any()) {
                StringBuilder sb1 = new StringBuilder();
                sb1.Append("<DATA>");
                sb1.Append("<HEAD>");
                sb1.Append("<BIZTRANSACTIONID>PMS_SYC_480_" + DateTime.Now.ToString("yyyyMMddhhmmssfff") + "</BIZTRANSACTIONID>");
                sb1.Append("<COUNT>" + tempcount + "</COUNT>");
                sb1.Append("<CONSUMER>PMS</CONSUMER>");
                sb1.Append("<SRVLEVEL>1</SRVLEVEL>");
                sb1.Append("<ACCOUNT></ACCOUNT>");
                sb1.Append("<PASSWORD></PASSWORD>");
                sb1.Append("</HEAD>");
                sb1.Append("<LIST>");
                sb1.Append("<ITEM>");
                sb1.Append("<RepairWorkOrderId>" + databaseName + extendedWarrantyOrder.Id + "YB" + "</RepairWorkOrderId>");
                sb1.Append("<MemberChannel>02</MemberChannel>");
                sb1.Append("<MemberNumber>" + extendedWarrantyOrder.MemberCode + "</MemberNumber>");
                sb1.Append("<CellPhoneNumber>" + extendedWarrantyOrder.CellNumber + "</CellPhoneNumber>");
                sb1.Append("<Points></Points>");
                sb1.Append("<TIME>" + datetimenow + "</TIME>");
                sb1.Append("<TransactionSubType>01</TransactionSubType>");
                sb1.Append("<BenefitsName>会员购买延保产品折扣</BenefitsName>");
                sb1.Append("<VIN>" + extendedWarrantyOrder.VIN + "</VIN>");
                sb1.Append("<Code>" + extendedWarrantyOrder.DealerCode + "</Code>");
                sb1.Append("<SubjectCode>" + subjectCode + "</SubjectCode>");
                sb1.Append("<SubjuctName>" + subjuctName + "</SubjuctName>");
                sb1.Append("<BUSINESSUNIT></BUSINESSUNIT>");
                sb1.Append("<RepairWorkOrder>" + extendedWarrantyOrder.ExtendedWarrantyOrderCode + "</RepairWorkOrder>");
                sb1.Append("<OrderType>01</OrderType>");
                sb1.Append("<OrderCategory>02</OrderCategory>");
                sb1.Append("<Warranty_Status>02</Warranty_Status>");
                sb1.Append("<BrandName>" + extendedWarrantyOrder.PartsSalesCategoryName + "</BrandName>");
                sb1.Append("<MaterialCost>0</MaterialCost>");
                sb1.Append("<LaborCost>0</LaborCost>");
                sb1.Append("<FieldServiceCharge>0</FieldServiceCharge>");
                sb1.Append("<TotalAmount>0</TotalAmount>");
                sb1.Append("<PrMaterialCost></PrMaterialCost>");
                sb1.Append("<PrLaborCost></PrLaborCost>");
                sb1.Append("<PrFieldServiceCharge></PrFieldServiceCharge>");
                sb1.Append("<PrTotalAmount>0</PrTotalAmount>");
                sb1.Append("<ActualCost>0</ActualCost>");
                sb1.Append("<OtherCost>0</OtherCost>");
                sb1.Append("<Accessories>0</Accessories>");
                sb1.Append("<Accrued>0</Accrued>");
                sb1.Append("<ManHour>0</ManHour>");
                sb1.Append("<CostDiscount>" + Convert.ToDecimal(deferredDiscountType.FirstOrDefault().ExtendedWarrantyAgio) * 10 + "</CostDiscount>");
                sb1.Append("</ITEM>");
                sb1.Append("</LIST>");
                sb1.Append("</DATA>");
                var integralClaimBilllist = sb1.ToString();
                var nccMemberInfo = new PMS_SYC_480_SendPointsDeductionInformationService_pttbindingQSService();
                nccMemberInfo.Credentials = new NetworkCredential("PMS", "pms201603111515");
                var error = "";
                var report = nccMemberInfo.PMS_SYC_480_SendPointsDeductionInformationService(integralClaimBilllist, out error);
                using(TransactionScope scope = new TransactionScope(TransactionScopeOption.RequiresNew)) {
                    try {
                        //var nccesb = new ESBLogNCC();
                        //nccesb.InterfaceName = "延保扣减";
                        //nccesb.Message = integralClaimBilllist + "\r\n" + error;
                        //nccesb.RNO = 1;
                        //nccesb.SyncNumberBegin = 0;
                        //nccesb.SyncNumberEnd = 0;
                        //nccesb.TheDate = DateTime.Now;
                        //tempContext.ESBLogNCCs.AddObject(nccesb);
                        //tempContext.SaveChanges();
                        scope.Complete();
                    } catch(Exception ex) {
                        throw new Exception("发送信息异常,原因:" + ex.Message);
                    } finally {
                        //释放资源
                        scope.Dispose();
                    }
                }
            }
        }
    }

    partial class DcsDomainService {
        [Update(UsingCustomMethod = true)]
        public void 审核延保销售订单(ExtendedWarrantyOrder extendedWarrantyOrder) {
            new ExtendedWarrantyOrderAch(this).审核延保销售订单(extendedWarrantyOrder);
        }
        [Update(UsingCustomMethod = true)]
        public void 新增延保销售订单(ExtendedWarrantyOrder extendedWarrantyOrder) {
            new ExtendedWarrantyOrderAch(this).新增延保销售订单(extendedWarrantyOrder);
        }
        [Update(UsingCustomMethod = true)]
        public void 提交延保销售订单(ExtendedWarrantyOrder extendedWarrantyOrder) {
            new ExtendedWarrantyOrderAch(this).提交延保销售订单(extendedWarrantyOrder);
        }
        [Update(UsingCustomMethod = true)]
        public void 更新延保销售订单(ExtendedWarrantyOrder extendedWarrantyOrder) {
            new ExtendedWarrantyOrderAch(this).更新延保销售订单(extendedWarrantyOrder);
        }
        [Update(UsingCustomMethod = true)]
        public void 作废延保销售订单(ExtendedWarrantyOrder extendedWarrantyOrder) {
            new ExtendedWarrantyOrderAch(this).作废延保销售订单(extendedWarrantyOrder);
        }

        [Update(UsingCustomMethod = true)]
        public void 驳回延保销售订单(ExtendedWarrantyOrder extendedWarrantyOrder) {
            new ExtendedWarrantyOrderAch(this).驳回延保销售订单(extendedWarrantyOrder);
        }

    }
}