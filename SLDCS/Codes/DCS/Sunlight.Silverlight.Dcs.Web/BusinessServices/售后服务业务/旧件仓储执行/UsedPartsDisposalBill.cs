﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class UsedPartsDisposalBillAch : DcsSerivceAchieveBase {
        public UsedPartsDisposalBillAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 生成旧件处理单(UsedPartsDisposalBill usedPartsDisposalBill) {
            var usedPartsDisposalDetails = usedPartsDisposalBill.UsedPartsDisposalDetails;
            var usedPartsBarCodes = usedPartsDisposalDetails.Select(r => r.UsedPartsBarCode.ToLower()).ToArray();
            var usedPartsStocks = ObjectContext.UsedPartsStocks.Where(r => usedPartsBarCodes.Contains(r.UsedPartsBarCode.ToLower()) && r.UsedPartsWarehouseId == usedPartsDisposalBill.UsedPartsWarehouseId).ToArray();
            usedPartsBarCodes = usedPartsBarCodes.Except(usedPartsStocks.Select(r => r.UsedPartsBarCode.ToLower())).ToArray();
            if(usedPartsBarCodes.Length > 0)
                throw new ValidationException(string.Format(ErrorStrings.UsedPartsDisposalBill_Validation6, usedPartsBarCodes.First()));
            if(usedPartsDisposalDetails.Any(r => r.PlannedAmount < 1))
                throw new ValidationException(ErrorStrings.UsedPartsDisposalBill_Validation1);
            //var dbusedPartsStock = usedPartsStocks.FirstOrDefault(v => usedPartsDisposalDetails.Any(r => r.UsedPartsBarCode.ToLower() == v.UsedPartsBarCode.ToLower() && (v.StorageQuantity - v.LockedQuantity) < r.PlannedAmount));
            var dbusedPartsStock = usedPartsStocks.FirstOrDefault(v => usedPartsDisposalDetails.Any(r => r.UsedPartsBarCode.ToLower() == v.UsedPartsBarCode.ToLower() && (v.StorageQuantity - v.LockedQuantity) < r.PlannedAmount) && v.UsedPartsWarehouseId == usedPartsDisposalBill.UsedPartsWarehouseId);
            if(dbusedPartsStock != null)
                throw new ValidationException(string.Format(ErrorStrings.UsedPartsDisposalBill_Validation2, dbusedPartsStock.UsedPartsBarCode));
        }

        public void 修改旧件处理单(UsedPartsDisposalBill usedPartsDisposalBill) {
            CheckEntityState(usedPartsDisposalBill);
            var usedPartsDisposalDetails = usedPartsDisposalBill.UsedPartsDisposalDetails;
            var usedPartsBarCodes = usedPartsDisposalDetails.Select(r => r.UsedPartsBarCode.ToLower()).ToArray();
            var usedPartsStocks = ObjectContext.UsedPartsStocks.Where(r => usedPartsBarCodes.Contains(r.UsedPartsBarCode.ToLower())).ToArray();
            usedPartsBarCodes = usedPartsBarCodes.Except(usedPartsStocks.Select(r => r.UsedPartsBarCode.ToLower())).ToArray();
            if(usedPartsBarCodes.Length > 0)
                throw new ValidationException(string.Format(ErrorStrings.UsedPartsDisposalBill_Validation6, usedPartsBarCodes.First()));
            if(usedPartsDisposalDetails.Any(r => r.PlannedAmount < 1))
                throw new ValidationException(ErrorStrings.UsedPartsDisposalBill_Validation1);
            //var dbusedPartsStock = usedPartsStocks.FirstOrDefault(v => usedPartsDisposalDetails.Any(r => r.UsedPartsBarCode.ToLower() == v.UsedPartsBarCode.ToLower() && (v.StorageQuantity - v.LockedQuantity) < r.PlannedAmount));
            var dbusedPartsStock = usedPartsStocks.FirstOrDefault(v => usedPartsDisposalDetails.Any(r => r.UsedPartsBarCode.ToLower() == v.UsedPartsBarCode.ToLower() && (v.StorageQuantity - v.LockedQuantity) < r.PlannedAmount) && v.UsedPartsWarehouseId == usedPartsDisposalBill.UsedPartsWarehouseId);
            if(dbusedPartsStock != null)
                throw new ValidationException(string.Format(ErrorStrings.UsedPartsDisposalBill_Validation2, dbusedPartsStock.UsedPartsBarCode));
        }

        public void 审批旧件处理单(UsedPartsDisposalBill usedPartsDisposalBill) {
            var usedPartsDisposalDetails = usedPartsDisposalBill.UsedPartsDisposalDetails;
            if(usedPartsDisposalDetails.Any(v => !v.ConfirmedAmount.HasValue))
                throw new ValidationException(ErrorStrings.UsedPartsDisposalBill_Validation8);
            var usedPartsBarCodes = usedPartsDisposalDetails.Select(r => r.UsedPartsBarCode.ToLower()).ToArray();
            var usedPartsStocks = ObjectContext.UsedPartsStocks.Where(r => usedPartsBarCodes.Contains(r.UsedPartsBarCode.ToLower())).ToArray();
            usedPartsBarCodes = usedPartsBarCodes.Except(usedPartsStocks.Select(r => r.UsedPartsBarCode.ToLower())).ToArray();
            if(usedPartsBarCodes.Length > 0)
                throw new ValidationException(string.Format(ErrorStrings.UsedPartsDisposalBill_Validation6, usedPartsBarCodes.First()));
            if(usedPartsDisposalDetails.Any(r => r.PlannedAmount < 1))
                throw new ValidationException(ErrorStrings.UsedPartsDisposalBill_Validation1);
            if(usedPartsDisposalDetails.Any(r => r.ConfirmedAmount > r.PlannedAmount))
                throw new ValidationException(ErrorStrings.UsedPartsDisposalBill_Validation3);
            //if(usedPartsStocks.Any(v => usedPartsDisposalDetails.Any(r => r.UsedPartsBarCode.ToLower() == v.UsedPartsBarCode.ToLower() && (v.StorageQuantity - v.LockedQuantity) < r.ConfirmedAmount)))
            //    throw new ValidationException(ErrorStrings.UsedPartsDisposalBill_Validation2);
            var dbusedPartsStocks = usedPartsStocks.FirstOrDefault(v => usedPartsDisposalDetails.Any(r => r.UsedPartsBarCode.ToLower() == v.UsedPartsBarCode.ToLower() && (v.StorageQuantity - v.LockedQuantity) < r.ConfirmedAmount) && v.UsedPartsWarehouseId == usedPartsDisposalBill.UsedPartsWarehouseId);
            if(dbusedPartsStocks != null){
            throw new ValidationException(string.Format(ErrorStrings.UsedPartsDisposalBill_Validation2, dbusedPartsStocks.UsedPartsBarCode));
            }
            var dbusedPartsDisposalBill = ObjectContext.UsedPartsDisposalBills.Where(r => r.Id == usedPartsDisposalBill.Id && r.Status == (int)DcsUsedPartsDisposalBillStatus.新建).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbusedPartsDisposalBill == null)
                throw new ValidationException(ErrorStrings.UsedPartsDisposalBill_Validation4);
            UpdateToDatabase(usedPartsDisposalBill);
            usedPartsDisposalBill.Status = (int)DcsUsedPartsDisposalBillStatus.生效;
            this.UpdateUsedPartsDisposalBillValidate(usedPartsDisposalBill);
            foreach(var usedPartsStock in usedPartsStocks) {
                var usedartsDisposalDetail = usedPartsDisposalDetails.FirstOrDefault(r => r.UsedPartsBarCode.ToLower() == usedPartsStock.UsedPartsBarCode.ToLower());
                UpdateToDatabase(usedPartsStock);
                if(usedartsDisposalDetail != null)
                    usedPartsStock.LockedQuantity = usedartsDisposalDetail.ConfirmedAmount.HasValue ? usedartsDisposalDetail.ConfirmedAmount.Value : 0;
                new UsedPartsStockAch(this.DomainService).UpdateUsedPartsStockValidate(usedPartsStock);
            }
            var userInfo = Utils.GetCurrentUserInfo();
            usedPartsDisposalBill.ApproverId = userInfo.Id;
            usedPartsDisposalBill.ApproverName = userInfo.Name;
            usedPartsDisposalBill.ApproveTime = DateTime.Now;
        }

        public void 作废旧件处理单(UsedPartsDisposalBill usedPartsDisposalBill) {
            var dbusedPartsDisposalBill = ObjectContext.UsedPartsDisposalBills.Where(r => r.Id == usedPartsDisposalBill.Id && r.Status == (int)DcsUsedPartsDisposalBillStatus.新建).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbusedPartsDisposalBill == null)
                throw new ValidationException(ErrorStrings.UsedPartsDisposalBill_Validation5);
            UpdateToDatabase(usedPartsDisposalBill);
            usedPartsDisposalBill.Status = (int)DcsUsedPartsDisposalBillStatus.作废;
            this.UpdateUsedPartsDisposalBillValidate(usedPartsDisposalBill);
            var userInfo = Utils.GetCurrentUserInfo();
            usedPartsDisposalBill.AbandonerId = userInfo.Id;
            usedPartsDisposalBill.AbandonerName = userInfo.Name;
            usedPartsDisposalBill.AbandonTime = DateTime.Now;
        }

        public void 终止旧件处理单(UsedPartsDisposalBill usedPartsDisposalBill) {
            var dbusedPartsDisposalBill = ObjectContext.UsedPartsDisposalBills.Where(r => r.Id == usedPartsDisposalBill.Id && (r.Status == (int)DcsUsedPartsDisposalBillStatus.生效 || r.Status == (int)DcsUsedPartsDisposalBillStatus.新建)).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbusedPartsDisposalBill == null)
                throw new ValidationException(ErrorStrings.UsedPartsDisposalBill_Validation7);
            UpdateToDatabase(usedPartsDisposalBill);
            usedPartsDisposalBill.OutboundStatus = (int)DcsUsedPartsOutboundStatus.终止出库;
            this.UpdateUsedPartsDisposalBillValidate(usedPartsDisposalBill);
            var usedPartsDisposalDetails = usedPartsDisposalBill.UsedPartsDisposalDetails;
            var usedPartsBarCodes = usedPartsDisposalDetails.Select(r => r.UsedPartsBarCode.ToLower());
            var usedPartsStocks = ObjectContext.UsedPartsStocks.Where(r => usedPartsBarCodes.Contains(r.UsedPartsBarCode.ToLower())).ToArray();
            foreach(var usedPartsStock in usedPartsStocks) {
                var usedPartsDisposalDetail = usedPartsDisposalDetails.FirstOrDefault(r => r.UsedPartsBarCode.ToLower() == usedPartsStock.UsedPartsBarCode.ToLower() && r.ConfirmedAmount.HasValue && r.ConfirmedAmount > r.OutboundAmount && r.OutboundAmount == 0);
                if(usedPartsDisposalDetail == null)
                    continue;
                UpdateToDatabase(usedPartsStock);
                usedPartsStock.LockedQuantity = 0;
                new UsedPartsStockAch(this.DomainService).UpdateUsedPartsStockValidate(usedPartsStock);
            }
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:09:49
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 生成旧件处理单(UsedPartsDisposalBill usedPartsDisposalBill) {
            new UsedPartsDisposalBillAch(this).生成旧件处理单(usedPartsDisposalBill);
        }
        
        [Update(UsingCustomMethod = true)]
        public void 修改旧件处理单(UsedPartsDisposalBill usedPartsDisposalBill) {
            new UsedPartsDisposalBillAch(this).修改旧件处理单(usedPartsDisposalBill);
        }
        
        [Update(UsingCustomMethod = true)]
        public void 审批旧件处理单(UsedPartsDisposalBill usedPartsDisposalBill) {
            new UsedPartsDisposalBillAch(this).审批旧件处理单(usedPartsDisposalBill);
        }
        
        [Update(UsingCustomMethod = true)]
        public void 作废旧件处理单(UsedPartsDisposalBill usedPartsDisposalBill) {
            new UsedPartsDisposalBillAch(this).作废旧件处理单(usedPartsDisposalBill);
        }
        
        [Update(UsingCustomMethod = true)]
        public void 终止旧件处理单(UsedPartsDisposalBill usedPartsDisposalBill) {
            new UsedPartsDisposalBillAch(this).终止旧件处理单(usedPartsDisposalBill);
        }
    }
}
