﻿using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Sunlight.Silverlight.Dcs.Web.Resources;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class UsedPartsInboundOrderAch : DcsSerivceAchieveBase {
        public UsedPartsInboundOrderAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 旧件入库(UsedPartsInboundOrder usedPartsInboundOrder, int inboundType) {
            var usedPartsInboundDetails = usedPartsInboundOrder.UsedPartsInboundDetails.ToArray();
            usedPartsInboundOrder.TotalQuantity = usedPartsInboundDetails.Sum(r => r.Quantity);
            usedPartsInboundOrder.TotalAmount = usedPartsInboundDetails.Sum(r => r.Quantity * r.SettlementPrice + ((r.UsedPartsEncourageAmount ?? 0) * r.Quantity));
            var usedPartsBarCodes = usedPartsInboundDetails.Select(r => r.UsedPartsBarCode).ToArray();
            var usedPartsIds = usedPartsInboundDetails.Select(r => r.UsedPartsId);
            var usedPartsWarehouse = ObjectContext.UsedPartsWarehouses.SingleOrDefault(r => r.Id == usedPartsInboundOrder.UsedPartsWarehouseId && r.Status == (int)DcsBaseDataStatus.有效);
            if(usedPartsWarehouse == null) {
                throw new ValidationException("未找到对应旧件仓库");
            }
            var partsPlannedPrices = ObjectContext.PartsPlannedPrices.Where(r => ObjectContext.UsedPartsWarehouses.Any(v => v.Id == usedPartsInboundOrder.UsedPartsWarehouseId && v.BranchId == r.OwnerCompanyId) && r.PartsSalesCategoryId == usedPartsWarehouse.PartsSalesCategoryId && r.OwnerCompanyType == (int)DcsCompanyType.分公司 && usedPartsIds.Contains(r.SparePartId)).ToArray();
            var usedPartsStocks = ObjectContext.UsedPartsStocks.Where(r => usedPartsBarCodes.Any(v => v == r.UsedPartsBarCode) && r.UsedPartsWarehouseId == usedPartsInboundOrder.UsedPartsWarehouseId).ToArray();
            //var structForUsedPartsStocks = (from a in ObjectContext.RepairClaimBills
            //                                join b in ObjectContext.RepairClaimItemDetails on a.Id equals b.RepairClaimBillId
            //                                join c in ObjectContext.RepairClaimMaterialDetails.Where(r => usedPartsBarCodes.Any(v => v == r.UsedPartsBarCode)) on b.Id equals c.RepairClaimItemDetailId
            //                                join d in ObjectContext.ResponsibleUnits on a.ResponsibleUnitId equals d.Id
            //                                select new {
            //                                    c.UsedPartsBarCode,
            //                                    ClaimBillId = a.Id,
            //                                    ClaimBillType = a.ClaimType,
            //                                    a.ClaimBillCode,
            //                                    c.UsedPartsSupplierId,
            //                                    c.UsedPartsSupplierCode,
            //                                    c.UsedPartsSupplierName,
            //                                    a.FaultyPartsSupplierId,
            //                                    a.FaultyPartsSupplierCode,
            //                                    a.FaultyPartsSupplierName,
            //                                    a.ResponsibleUnitId,
            //                                    ResponsibleUnitCode = d.Code,
            //                                    ResponsibleUnitName = d.Name,
            //                                    a.FaultyPartsId,
            //                                    a.FaultyPartsCode,
            //                                    a.FaultyPartsName,
            //                                    IfFaultyParts = a.FaultyPartsId == c.UsedPartsId,
            //                                }).ToArray();
            foreach(var usedPartsInboundDetail in usedPartsInboundDetails) {
                var usedPartsStock = usedPartsStocks.FirstOrDefault(r => r.UsedPartsBarCode == usedPartsInboundDetail.UsedPartsBarCode);
                if(usedPartsStock != null) {
                    throw new ValidationException(string.Format(ErrorStrings.UsedPartsInboundOrder_Validation1, usedPartsInboundDetail.UsedPartsBarCode));
                }
                if(usedPartsInboundDetail.Quantity != 0) {
                    var partsPlannedPrice = partsPlannedPrices.Where(r => r.SparePartId == usedPartsInboundDetail.UsedPartsId).ToArray();
                    if(partsPlannedPrice.Length > 1)
                        throw new ValidationException(string.Format(ErrorStrings.UsedPartsInboundOrder_Validation3, usedPartsInboundDetail.UsedPartsBarCode));
                    if(partsPlannedPrice.Length == 1)
                        usedPartsInboundDetail.CostPrice = partsPlannedPrice[0].PlannedPrice;
                    var tempUsedPartsStock = new UsedPartsStock {
                        UsedPartsWarehouseId = usedPartsInboundOrder.UsedPartsWarehouseId,
                        UsedPartsWarehouseAreaId = usedPartsInboundDetail.UsedPartsWarehouseAreaId,
                        UsedPartsId = usedPartsInboundDetail.UsedPartsId,
                        UsedPartsCode = usedPartsInboundDetail.UsedPartsCode,
                        UsedPartsName = usedPartsInboundDetail.UsedPartsName,
                        UsedPartsBarCode = usedPartsInboundDetail.UsedPartsBarCode,
                        UsedPartsSerialNumber = usedPartsInboundDetail.UsedPartsSerialNumber,
                        UsedPartsBatchNumber = usedPartsInboundDetail.UsedPartsBatchNumber,
                        StorageQuantity = usedPartsInboundDetail.Quantity,
                        LockedQuantity = 0,
                        SettlementPrice = usedPartsInboundDetail.SettlementPrice,
                        CostPrice = usedPartsInboundDetail.CostPrice,
                        BranchId = usedPartsInboundDetail.BranchId,
                        ReceptionStatus = usedPartsInboundDetail.ReceptionStatus
                    };
                    //var tempStruct = structForUsedPartsStocks.FirstOrDefault(r => r.UsedPartsBarCode == usedPartsInboundDetail.UsedPartsBarCode);
                    //if(tempStruct != null) {
                    //    tempUsedPartsStock.ClaimBillId = tempStruct.ClaimBillId;
                    //    tempUsedPartsStock.ClaimBillType = tempStruct.ClaimBillType;
                    //    tempUsedPartsStock.ClaimBillCode = tempStruct.ClaimBillCode;
                    //    tempUsedPartsStock.IfFaultyParts = tempStruct.IfFaultyParts;
                    //    tempUsedPartsStock.UsedPartsSupplierId = tempStruct.UsedPartsSupplierId;
                    //    tempUsedPartsStock.UsedPartsSupplierCode = tempStruct.UsedPartsSupplierCode;
                    //    tempUsedPartsStock.UsedPartsSupplierName = tempStruct.UsedPartsSupplierName;
                    //    tempUsedPartsStock.FaultyPartsSupplierId = tempStruct.FaultyPartsSupplierId;
                    //    tempUsedPartsStock.FaultyPartsSupplierCode = tempStruct.FaultyPartsSupplierCode;
                    //    tempUsedPartsStock.FaultyPartsSupplierName = tempStruct.FaultyPartsSupplierName;
                    //    tempUsedPartsStock.FaultyPartsId = tempStruct.FaultyPartsId;
                    //    tempUsedPartsStock.FaultyPartsCode = tempStruct.FaultyPartsCode;
                    //    tempUsedPartsStock.FaultyPartsName = tempStruct.FaultyPartsName;
                    //    tempUsedPartsStock.ResponsibleUnitId = tempStruct.ResponsibleUnitId;
                    //    tempUsedPartsStock.ResponsibleUnitCode = tempStruct.ResponsibleUnitCode;
                    //    tempUsedPartsStock.ResponsibleUnitName = tempStruct.ResponsibleUnitName;
                    //}
                    InsertToDatabase(tempUsedPartsStock);
                    new UsedPartsStockAch(this.DomainService).InsertUsedPartsStockValidate(tempUsedPartsStock);
                }
            }
            switch(inboundType) {
                case (int)DcsUsedPartsInboundOrderInboundType.借用归还:
                    var usedPartsLoanDetails = ObjectContext.UsedPartsLoanDetails.Where(r => r.UsedPartsLoanBillId == usedPartsInboundOrder.SourceId).ToArray();
                    var usedPartsLoanBill = ObjectContext.UsedPartsLoanBills.SingleOrDefault(r => r.Id == usedPartsInboundOrder.SourceId);
                    foreach(var usedPartsLoanDetail in usedPartsLoanDetails) {
                        var usedPartsInboundDetail = usedPartsInboundOrder.UsedPartsInboundDetails.FirstOrDefault(r => r.UsedPartsBarCode == usedPartsLoanDetail.UsedPartsBarCode);
                        if(usedPartsInboundDetail == null) {
                            continue;
                        }
                        usedPartsLoanDetail.InboundAmount = usedPartsLoanDetail.InboundAmount.HasValue ? usedPartsLoanDetail.InboundAmount + usedPartsInboundDetail.Quantity : usedPartsInboundDetail.Quantity;
                        UpdateToDatabase(usedPartsLoanDetail);
                    }
                    if(usedPartsLoanBill != null) {
                        if(usedPartsLoanDetails.Count() == usedPartsLoanDetails.Count(v => v.OutboundAmount.HasValue && v.InboundAmount.HasValue && v.OutboundAmount == v.InboundAmount)) {
                            usedPartsLoanBill.InboundStatus = (int)DcsUsedPartsInboundStatus.入库完成;
                        }else {
                            usedPartsLoanBill.InboundStatus = (int)DcsUsedPartsInboundStatus.部分入库;
                        }
                        UpdateToDatabase(usedPartsLoanBill);
                        new UsedPartsLoanBillAch(this.DomainService).UpdateUsedPartsLoanBillValidate(usedPartsLoanBill);
                    }
                    break;
                case (int)DcsUsedPartsInboundOrderInboundType.加工入库:
                    var usedPartsRefitReturnDetails = ObjectContext.UsedPartsRefitReturnDetails.Where(r => r.UsedPartsRefitBillId == usedPartsInboundOrder.SourceId).ToArray();
                    var usedPartsRefitBill = ObjectContext.UsedPartsRefitBills.SingleOrDefault(r => r.Id == usedPartsInboundOrder.SourceId);
                    foreach(var usedPartsRefitReturnDetail in usedPartsRefitReturnDetails) {
                        var usedPartsInboundDetail = usedPartsInboundOrder.UsedPartsInboundDetails.FirstOrDefault(r => r.UsedPartsBarCode == usedPartsRefitReturnDetail.UsedPartsBarCode);
                        if(usedPartsInboundDetail == null) {
                            continue;
                        }
                        usedPartsRefitReturnDetail.InboundAmount = usedPartsRefitReturnDetail.InboundAmount.HasValue ? usedPartsRefitReturnDetail.InboundAmount + usedPartsInboundDetail.Quantity : usedPartsInboundDetail.Quantity;
                        UpdateToDatabase(usedPartsRefitReturnDetail);
                    }
                    if(usedPartsRefitBill != null) {
                        if(usedPartsRefitReturnDetails.Count() == usedPartsRefitReturnDetails.Count(v => v.ConfirmedAmount.HasValue && v.InboundAmount.HasValue && v.ConfirmedAmount == v.InboundAmount)) {
                            usedPartsRefitBill.InboundStatus = (int)DcsUsedPartsInboundStatus.入库完成;
                        }else {
                            usedPartsRefitBill.InboundStatus = (int)DcsUsedPartsInboundStatus.部分入库;
                        }
                        UpdateToDatabase(usedPartsRefitBill);
                        new UsedPartsRefitBillAch(this.DomainService).UpdateUsedPartsRefitBillValidate(usedPartsRefitBill);
                    }
                    break;
                case (int)DcsUsedPartsInboundOrderInboundType.调拨入库:
                    var usedPartsTransferDetails = ObjectContext.UsedPartsTransferDetails.Where(r => r.UsedPartsTransferOrderId == usedPartsInboundOrder.SourceId).ToArray();
                    var usedPartsTransferOrder = ObjectContext.UsedPartsTransferOrders.SingleOrDefault(r => r.Id == usedPartsInboundOrder.SourceId);
                    foreach(var usedPartsTransferDetail in usedPartsTransferDetails) {
                        var usedPartsInboundDetail = usedPartsInboundOrder.UsedPartsInboundDetails.FirstOrDefault(r => r.UsedPartsBarCode == usedPartsTransferDetail.UsedPartsBarCode);
                        if(usedPartsInboundDetail == null) {
                            continue;
                        }
                        usedPartsTransferDetail.InboundAmount = usedPartsTransferDetail.InboundAmount.HasValue ? usedPartsTransferDetail.InboundAmount + usedPartsInboundDetail.Quantity : usedPartsInboundDetail.Quantity;
                        UpdateToDatabase(usedPartsTransferDetail);
                    }
                    if(usedPartsTransferOrder != null) {
                        if(usedPartsTransferDetails.Count() == usedPartsTransferDetails.Count(v => v.ConfirmedAmount.HasValue && v.InboundAmount.HasValue && v.ConfirmedAmount == v.InboundAmount))
                            usedPartsTransferOrder.InboundStatus = (int)DcsUsedPartsInboundStatus.入库完成;
                        else {
                            usedPartsTransferOrder.InboundStatus = (int)DcsUsedPartsInboundStatus.部分入库;
                        }
                        UpdateToDatabase(usedPartsTransferOrder);
                        new UsedPartsTransferOrderAch(this.DomainService).UpdateUsedPartsTransferOrderValidate(usedPartsTransferOrder);
                    }
                    break;
                case (int)DcsUsedPartsInboundOrderInboundType.服务站返件入库:
                    var usedPartsShippingOrder = ObjectContext.UsedPartsShippingOrders.SingleOrDefault(r => r.Id == usedPartsInboundOrder.SourceId);
                    if(usedPartsShippingOrder == null) {
                        throw new ValidationException("未找到对应的旧件发运单");
                    }
                    var flag = false;
                    UsedPartsLogisticLossBill usedPartsLogisticLossBill = null;
                    var usedPartsShippingDetails = ObjectContext.UsedPartsShippingDetails.Where(r => r.UsedPartsShippingOrderId == usedPartsInboundOrder.SourceId).ToArray();
                    foreach(var usedPartsShippingDetail in usedPartsShippingDetails) {
                        var usedPartsInboundDetail = usedPartsInboundOrder.UsedPartsInboundDetails.FirstOrDefault(r => r.UsedPartsBarCode == usedPartsShippingDetail.UsedPartsBarCode);
                        if(usedPartsInboundDetail == null) {
                            continue;
                        }
                        usedPartsShippingDetail.InboundAmount = usedPartsShippingDetail.InboundAmount.HasValue ? usedPartsShippingDetail.InboundAmount + usedPartsInboundDetail.Quantity : usedPartsInboundDetail.Quantity;
                        if(usedPartsInboundDetail.Quantity == 0) {
                            usedPartsShippingDetail.ReceptionStatus = (int)DcsUsedPartsShippingDetailReceptionStatus.未接收;
                            if(!flag) {
                                usedPartsLogisticLossBill = new UsedPartsLogisticLossBill {
                                    DealerId = usedPartsShippingOrder.DealerId,
                                    DealerCode = usedPartsShippingOrder.DealerCode,
                                    DealerName = usedPartsShippingOrder.DealerName,
                                    UsedPartsShippingOrderId = usedPartsShippingOrder.Id,
                                    LogisticCompanyId = usedPartsShippingOrder.LogisticCompanyId,
                                    LogisticCompanyCode = usedPartsShippingOrder.LogisticCompanyCode,
                                    LogisticCompanyName = usedPartsShippingOrder.LogisticCompanyName,
                                    TotalAmount = usedPartsShippingOrder.TotalAmount,
                                    Remark = usedPartsShippingOrder.Remark
                                };
                                flag = true;
                            }
                            usedPartsLogisticLossBill.UsedPartsLogisticLossDetails.Add(new UsedPartsLogisticLossDetail {
                                UsedPartsLogisticLossBillId = usedPartsLogisticLossBill.Id,
                                SerialNumber = usedPartsShippingDetail.SerialNumber,
                                BranchId = usedPartsShippingDetail.BranchId,
                                BranchName = usedPartsShippingDetail.BranchName,
                                ClaimBillId = usedPartsShippingDetail.ClaimBillId,
                                ClaimBillType = usedPartsShippingDetail.ClaimBillType,
                                ClaimBillCode = usedPartsShippingDetail.ClaimBillCode,
                                UsedPartsId = usedPartsShippingDetail.UsedPartsId,
                                UsedPartsCode = usedPartsShippingDetail.UsedPartsCode,
                                UsedPartsName = usedPartsShippingDetail.UsedPartsName,
                                UsedPartsSerialNumber = usedPartsShippingDetail.UsedPartsSerialNumber,
                                UsedPartsSupplierId = usedPartsShippingDetail.UsedPartsSupplierId,
                                UsedPartsSupplierCode = usedPartsShippingDetail.UsedPartsSupplierCode,
                                UsedPartsSupplierName = usedPartsShippingDetail.UsedPartsSupplierName,
                                UsedPartsBarCode = usedPartsShippingDetail.UsedPartsBarCode,
                                ProcessStatus = (int)DcsUsedPartsLogisticLossDetailProcessStatus.丢失,
                                Quantity = usedPartsShippingDetail.Quantity,
                                Price = usedPartsShippingDetail.UnitPrice,
                                PartsManagementCost = usedPartsShippingDetail.PartsManagementCost,
                                UsedPartsReturnPolicy = usedPartsShippingDetail.UsedPartsReturnPolicy,
                                FaultyPartsId = usedPartsShippingDetail.FaultyPartsId,
                                FaultyPartsCode = usedPartsShippingDetail.FaultyPartsCode,
                                FaultyPartsName = usedPartsShippingDetail.FaultyPartsName,
                                FaultyPartsSupplierId = usedPartsShippingDetail.FaultyPartsSupplierId,
                                FaultyPartsSupplierCode = usedPartsShippingDetail.FaultyPartsSupplierCode,
                                FaultyPartsSupplierName = usedPartsShippingDetail.FaultyPartsSupplierName,
                                IfFaultyParts = usedPartsShippingDetail.IfFaultyParts,
                                ResponsibleUnitId = usedPartsShippingDetail.ResponsibleUnitId,
                                ResponsibleUnitCode = usedPartsShippingDetail.ResponsibleUnitCode,
                                ResponsibleUnitName = usedPartsShippingDetail.ResponsibleUnitName,
                                Shipped = usedPartsShippingDetail.Shipped,
                                NewPartsId = usedPartsShippingDetail.NewPartsId,
                                NewPartsCode = usedPartsShippingDetail.NewPartsCode,
                                NewPartsName = usedPartsShippingDetail.NewPartsName,
                                NewPartsSupplierId = usedPartsShippingDetail.NewPartsSupplierId,
                                NewPartsSupplierCode = usedPartsShippingDetail.NewPartsSupplierCode,
                                NewPartsSupplierName = usedPartsShippingDetail.NewPartsSupplierName,
                                NewPartsSerialNumber = usedPartsShippingDetail.NewPartsSerialNumber,
                                NewPartsSecurityNumber = usedPartsShippingDetail.NewPartsSecurityNumber,
                                Remark = "发运单已确认，但未入库"
                            });
                        }
                    }
                    if(usedPartsShippingDetails.Count(r => (r.ReceptionStatus == (int)DcsUsedPartsShippingDetailReceptionStatus.已接收 || r.ReceptionStatus == (int)DcsUsedPartsShippingDetailReceptionStatus.不合格)) == usedPartsShippingDetails.Count(v => (v.ReceptionStatus == (int)DcsUsedPartsShippingDetailReceptionStatus.已接收 || v.ReceptionStatus == (int)DcsUsedPartsShippingDetailReceptionStatus.不合格) && v.InboundAmount.HasValue && v.Quantity == v.InboundAmount)) {
                        usedPartsShippingOrder.InboundStatus = (int)DcsUsedPartsInboundStatus.入库完成;
                    }else {
                        usedPartsShippingOrder.InboundStatus = (int)DcsUsedPartsInboundStatus.部分入库;
                    }
                    UpdateToDatabase(usedPartsShippingOrder);
                    DomainService.UpdateUsedPartsShippingOrderValidate(usedPartsShippingOrder);
                    if(usedPartsLogisticLossBill != null) {
                        InsertToDatabase(usedPartsLogisticLossBill);
                        new UsedPartsLogisticLossBillAch(this.DomainService).InsertUsedPartsLogisticLossBillValidate(usedPartsLogisticLossBill);
                    }
                    var ssUsedPartsStorages = ObjectContext.SsUsedPartsStorages.Where(r => usedPartsBarCodes.Contains(r.UsedPartsBarCode)).ToArray();
                    foreach(var ssUsedPartsStorage in ssUsedPartsStorages) {
                        DeleteFromDatabase(ssUsedPartsStorage);
                    }
                    break;
                default:
                    throw new ValidationException(ErrorStrings.UsedPartsInboundOrder_Validation2);
            }
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:09:49
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 旧件入库(UsedPartsInboundOrder usedPartsInboundOrder, int inboundType) {
            new UsedPartsInboundOrderAch(this).旧件入库(usedPartsInboundOrder,inboundType);
        }
    }
}
