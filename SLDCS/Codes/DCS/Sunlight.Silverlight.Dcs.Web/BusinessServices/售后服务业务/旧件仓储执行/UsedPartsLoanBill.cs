﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class UsedPartsLoanBillAch : DcsSerivceAchieveBase {
        public UsedPartsLoanBillAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 生成旧件借用单(UsedPartsLoanBill usedPartsLoanBill) {
            var usedPartsLoanDetails = usedPartsLoanBill.UsedPartsLoanDetails;
            var usedPartsBarCodes = usedPartsLoanDetails.Select(r => r.UsedPartsBarCode.ToLower()).ToArray();
            var usedPartsStocks = ObjectContext.UsedPartsStocks.Where(r => usedPartsBarCodes.Contains(r.UsedPartsBarCode.ToLower())).ToArray();
            usedPartsBarCodes = usedPartsBarCodes.Except(usedPartsStocks.Select(r => r.UsedPartsBarCode.ToLower())).ToArray();
            if(usedPartsBarCodes.Length > 0)
                throw new ValidationException(string.Format(ErrorStrings.UsedPartsLoanBill_Validation6, usedPartsBarCodes.First()));
            if(usedPartsLoanDetails.Any(r => r.PlannedAmount < 1))
                throw new ValidationException(ErrorStrings.UsedPartsLoanBill_Validation1);
            var dbusedPartsStock = usedPartsStocks.FirstOrDefault(v => usedPartsLoanDetails.Any(r => r.UsedPartsBarCode.ToLower() == v.UsedPartsBarCode.ToLower() && (v.StorageQuantity - v.LockedQuantity) < r.PlannedAmount));
            if(dbusedPartsStock != null)
                throw new ValidationException(string.Format(ErrorStrings.UsedPartsLoanBill_Validation2, dbusedPartsStock.UsedPartsBarCode));
        }

        public void 修改旧件借用单(UsedPartsLoanBill usedPartsLoanBill) {
            CheckEntityState(usedPartsLoanBill);
            var usedPartsLoanDetails = usedPartsLoanBill.UsedPartsLoanDetails;
            var usedPartsBarCodes = usedPartsLoanDetails.Select(r => r.UsedPartsBarCode.ToLower()).ToArray();
            var usedPartsStocks = ObjectContext.UsedPartsStocks.Where(r => usedPartsBarCodes.Contains(r.UsedPartsBarCode.ToLower())).ToArray();
            usedPartsBarCodes = usedPartsBarCodes.Except(usedPartsStocks.Select(r => r.UsedPartsBarCode.ToLower())).ToArray();
            if(usedPartsBarCodes.Length > 0)
                throw new ValidationException(string.Format(ErrorStrings.UsedPartsLoanBill_Validation6, usedPartsBarCodes.First()));
            if(usedPartsLoanDetails.Any(r => r.PlannedAmount < 1))
                throw new ValidationException(ErrorStrings.UsedPartsLoanBill_Validation1);
            var dbusedPartsStock = usedPartsStocks.FirstOrDefault(v => usedPartsLoanDetails.Any(r => r.UsedPartsBarCode.ToLower() == v.UsedPartsBarCode.ToLower() && (v.StorageQuantity - v.LockedQuantity) < r.PlannedAmount));
            if(dbusedPartsStock != null)
                throw new ValidationException(string.Format(ErrorStrings.UsedPartsLoanBill_Validation2, dbusedPartsStock.UsedPartsBarCode));
        }

        public void 审批旧件借用单(UsedPartsLoanBill usedPartsLoanBill) {
            var usedPartsLoanDetails = usedPartsLoanBill.UsedPartsLoanDetails;
            if(usedPartsLoanDetails.Any(v => !v.ConfirmedAmount.HasValue))
                throw new ValidationException(ErrorStrings.UsedPartsLoanBill_Validation8);
            var usedPartsBarCodes = usedPartsLoanDetails.Select(r => r.UsedPartsBarCode.ToLower()).ToArray();
            var usedPartsStocks = ObjectContext.UsedPartsStocks.Where(r => usedPartsBarCodes.Contains(r.UsedPartsBarCode.ToLower())).ToArray();
            usedPartsBarCodes = usedPartsBarCodes.Except(usedPartsStocks.Select(r => r.UsedPartsBarCode.ToLower())).ToArray();
            if(usedPartsBarCodes.Length > 0)
                throw new ValidationException(string.Format(ErrorStrings.UsedPartsLoanBill_Validation6, usedPartsBarCodes.First()));
            if(usedPartsLoanDetails.Any(r => r.PlannedAmount < 1))
                throw new ValidationException(ErrorStrings.UsedPartsLoanBill_Validation1);
            if(usedPartsLoanDetails.Any(r => r.ConfirmedAmount > r.PlannedAmount))
                throw new ValidationException(ErrorStrings.UsedPartsLoanBill_Validation3);
            var dbusedPartsStock = usedPartsStocks.FirstOrDefault(v => usedPartsLoanDetails.Any(r => r.UsedPartsBarCode.ToLower() == v.UsedPartsBarCode.ToLower() && (v.StorageQuantity - v.LockedQuantity) < r.ConfirmedAmount));
            if(dbusedPartsStock != null)
                throw new ValidationException(string.Format(ErrorStrings.UsedPartsLoanBill_Validation2, dbusedPartsStock.UsedPartsBarCode));
            var dbusedPartsLoanBill = ObjectContext.UsedPartsLoanBills.Where(r => r.Id == usedPartsLoanBill.Id && r.Status == (int)DcsUsedPartsLoanBillStatus.新建).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbusedPartsLoanBill == null)
                throw new ValidationException(ErrorStrings.UsedPartsLoanBill_Validation4);
            UpdateToDatabase(usedPartsLoanBill);
            usedPartsLoanBill.Status = (int)DcsUsedPartsLoanBillStatus.生效;
            this.UpdateUsedPartsLoanBillValidate(usedPartsLoanBill);
            foreach(var usedPartsStock in usedPartsStocks) {
                var usedPartsTransferDetail = usedPartsLoanDetails.FirstOrDefault(r => r.UsedPartsBarCode.ToLower() == usedPartsStock.UsedPartsBarCode.ToLower());
                UpdateToDatabase(usedPartsStock);
                if(usedPartsTransferDetail != null)
                    usedPartsStock.LockedQuantity = usedPartsTransferDetail.ConfirmedAmount.HasValue ? usedPartsTransferDetail.ConfirmedAmount.Value : 0;
                new UsedPartsStockAch(this.DomainService).UpdateUsedPartsStockValidate(usedPartsStock);
            }
            var userInfo = Utils.GetCurrentUserInfo();
            usedPartsLoanBill.ApproverId = userInfo.Id;
            usedPartsLoanBill.ApproverName = userInfo.Name;
            usedPartsLoanBill.ApproveTime = DateTime.Now;
        }

        public void 作废旧件借用单(UsedPartsLoanBill usedPartsLoanBill) {
            var dbusedPartsLoanBill = ObjectContext.UsedPartsLoanBills.Where(r => r.Id == usedPartsLoanBill.Id && r.Status == (int)DcsUsedPartsLoanBillStatus.新建).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbusedPartsLoanBill == null)
                throw new ValidationException(ErrorStrings.UsedPartsLoanBill_Validation5);
            UpdateToDatabase(usedPartsLoanBill);
            usedPartsLoanBill.Status = (int)DcsUsedPartsLoanBillStatus.作废;
            this.UpdateUsedPartsLoanBillValidate(usedPartsLoanBill);
            var userInfo = Utils.GetCurrentUserInfo();
            usedPartsLoanBill.AbandonerId = userInfo.Id;
            usedPartsLoanBill.AbandonerName = userInfo.Name;
            usedPartsLoanBill.AbandonTime = DateTime.Now;
        }

        public void 终止旧件借用单(UsedPartsLoanBill usedPartsLoanBill) {
            var dbusedPartsLoanBill = ObjectContext.UsedPartsLoanBills.Where(r => r.Id == usedPartsLoanBill.Id && r.Status == (int)DcsUsedPartsLoanBillStatus.生效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbusedPartsLoanBill == null)
                throw new ValidationException(ErrorStrings.UsedPartsLoanBill_Validation7);
            UpdateToDatabase(usedPartsLoanBill);
            usedPartsLoanBill.OutboundStatus = (int)DcsUsedPartsOutboundStatus.终止出库;
            usedPartsLoanBill.InboundStatus = (int)DcsUsedPartsInboundStatus.终止入库;
            this.UpdateUsedPartsLoanBillValidate(usedPartsLoanBill);
            var usedPartsLoanDetails = usedPartsLoanBill.UsedPartsLoanDetails;
            var usedPartsBarCodes = usedPartsLoanDetails.Select(r => r.UsedPartsBarCode.ToLower());
            var usedPartsStocks = ObjectContext.UsedPartsStocks.Where(r => usedPartsBarCodes.Contains(r.UsedPartsBarCode.ToLower())).ToArray();
            foreach(var usedPartsStock in usedPartsStocks) {
                var usedPartsLoanDetail = usedPartsLoanDetails.FirstOrDefault(r => r.UsedPartsBarCode.ToLower() == usedPartsStock.UsedPartsBarCode.ToLower() && r.ConfirmedAmount.HasValue && r.ConfirmedAmount > r.OutboundAmount && r.OutboundAmount == 0);
                if(usedPartsLoanDetail == null)
                    continue;
                UpdateToDatabase(usedPartsStock);
                usedPartsStock.LockedQuantity = 0;
                new UsedPartsStockAch(this.DomainService).UpdateUsedPartsStockValidate(usedPartsStock);
            }
        }

        public void 借用归还(UsedPartsLoanBill usedPartsLoanBill) {
            var dbusedPartsLoanBill = ObjectContext.UsedPartsLoanBills.Where(r => r.Id == usedPartsLoanBill.Id && r.Status == (int)DcsUsedPartsLoanBillStatus.生效 && r.OutboundStatus == (int)DcsUsedPartsOutboundStatus.出库完成).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbusedPartsLoanBill == null)
                throw new ValidationException("只能处理生效状态和出库完成的处理单");
            UpdateToDatabase(usedPartsLoanBill);
            usedPartsLoanBill.InboundStatus = (int)DcsUsedPartsInboundStatus.待入库;
            this.UpdateUsedPartsLoanBillValidate(usedPartsLoanBill);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:09:49
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 生成旧件借用单(UsedPartsLoanBill usedPartsLoanBill) {
            new UsedPartsLoanBillAch(this).生成旧件借用单(usedPartsLoanBill);
        }
        
        [Update(UsingCustomMethod = true)]
        public void 修改旧件借用单(UsedPartsLoanBill usedPartsLoanBill) {
            new UsedPartsLoanBillAch(this).修改旧件借用单(usedPartsLoanBill);
        }
        
        [Update(UsingCustomMethod = true)]
        public void 审批旧件借用单(UsedPartsLoanBill usedPartsLoanBill) {
            new UsedPartsLoanBillAch(this).审批旧件借用单(usedPartsLoanBill);
        }
        
        [Update(UsingCustomMethod = true)]
        public void 作废旧件借用单(UsedPartsLoanBill usedPartsLoanBill) {
            new UsedPartsLoanBillAch(this).作废旧件借用单(usedPartsLoanBill);
        }
        
        [Update(UsingCustomMethod = true)]
        public void 终止旧件借用单(UsedPartsLoanBill usedPartsLoanBill) {
            new UsedPartsLoanBillAch(this).终止旧件借用单(usedPartsLoanBill);
        }
        
        [Update(UsingCustomMethod = true)]
        public void 借用归还(UsedPartsLoanBill usedPartsLoanBill) {
            new UsedPartsLoanBillAch(this).借用归还(usedPartsLoanBill);
        }
    }
}
