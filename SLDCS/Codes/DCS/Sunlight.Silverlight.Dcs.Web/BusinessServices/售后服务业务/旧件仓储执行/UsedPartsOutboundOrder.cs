﻿using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Sunlight.Silverlight.Dcs.Web.Resources;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class UsedPartsOutboundOrderAch : DcsSerivceAchieveBase {
        public UsedPartsOutboundOrderAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 旧件出库(UsedPartsOutboundOrder usedPartsOutboundOrder, int outboundType) {
            var usedPartsOutboundDetails = usedPartsOutboundOrder.UsedPartsOutboundDetails.Select(r => r.UsedPartsBarCode.ToLower()).ToArray();
            usedPartsOutboundOrder.TotalQuantity = usedPartsOutboundOrder.UsedPartsOutboundDetails.Sum(r => r.Quantity);
            usedPartsOutboundOrder.TotalAmount = usedPartsOutboundOrder.UsedPartsOutboundDetails.Sum(r => r.Quantity * r.SettlementPrice);
            var usedPartsStocks = ObjectContext.UsedPartsStocks.Where(r => usedPartsOutboundDetails.Contains(r.UsedPartsBarCode.ToLower()) && r.UsedPartsWarehouseId == usedPartsOutboundOrder.UsedPartsWarehouseId).ToArray();
            var usedPartsBarCodes = usedPartsOutboundDetails.Except(usedPartsStocks.Select(r => r.UsedPartsBarCode.ToLower())).ToArray();
            if(usedPartsBarCodes.Length > 0)
                throw new ValidationException(string.Format(ErrorStrings.UsedPartsOutboundOrder_Validation2, usedPartsBarCodes.First()));
            foreach(var usedPartsStock in usedPartsStocks) {
                var usedPartsOutboundDetail = usedPartsOutboundOrder.UsedPartsOutboundDetails.FirstOrDefault(r => r.UsedPartsBarCode.ToLower() == usedPartsStock.UsedPartsBarCode.ToLower() && r.Quantity != usedPartsStock.StorageQuantity);
                if(usedPartsOutboundDetail != null)
                    throw new ValidationException(string.Format(ErrorStrings.UsedPartsOutboundOrder_Validation1, usedPartsStock.UsedPartsBarCode));
                DeleteFromDatabase(usedPartsStock);
            }
            switch(outboundType) {
                case (int)DcsUsedPartsOutboundOrderOutboundType.借用出库:
                    var usedPartsLoanDetails = ObjectContext.UsedPartsLoanDetails.Where(r => r.UsedPartsLoanBillId == usedPartsOutboundOrder.SourceId).ToArray();
                    var usedPartsLoanBill = ObjectContext.UsedPartsLoanBills.SingleOrDefault(r => r.Id == usedPartsOutboundOrder.SourceId);
                    UpdateToDatabase(usedPartsLoanBill);
                    foreach(var usedPartsLoanDetail in usedPartsLoanDetails) {
                        var usedPartsOutboundDetail = usedPartsOutboundOrder.UsedPartsOutboundDetails.FirstOrDefault(r => r.UsedPartsBarCode.ToLower() == usedPartsLoanDetail.UsedPartsBarCode.ToLower());
                        if(usedPartsOutboundDetail == null)
                            continue;
                        usedPartsLoanDetail.OutboundAmount = usedPartsLoanDetail.OutboundAmount.HasValue ? usedPartsLoanDetail.OutboundAmount + usedPartsOutboundDetail.Quantity : usedPartsOutboundDetail.Quantity;
                    }
                    if(usedPartsLoanBill != null) {
                        if(usedPartsLoanDetails.Count() == usedPartsLoanDetails.Count(v => v.ConfirmedAmount.HasValue && v.OutboundAmount.HasValue && v.ConfirmedAmount == v.OutboundAmount)) {
                            usedPartsLoanBill.OutboundStatus = (int)DcsUsedPartsOutboundStatus.出库完成;
                        }else if(usedPartsLoanDetails.Any(v => v.ConfirmedAmount != 0)) {
                            usedPartsLoanBill.OutboundStatus = (int)DcsUsedPartsOutboundStatus.部分出库;
                        }else {
                            usedPartsLoanBill.OutboundStatus = (int)DcsUsedPartsOutboundStatus.待出库;
                        }
                    }
                    new UsedPartsLoanBillAch(this.DomainService).UpdateUsedPartsLoanBillValidate(usedPartsLoanBill);
                    break;
                case (int)DcsUsedPartsOutboundOrderOutboundType.加工领料出库:
                    var usedPartsRefitReqDetails = ObjectContext.UsedPartsRefitReqDetails.Where(r => r.UsedPartsRefitBillId == usedPartsOutboundOrder.SourceId).ToArray();
                    var usedPartsRefitBill = ObjectContext.UsedPartsRefitBills.SingleOrDefault(r => r.Id == usedPartsOutboundOrder.SourceId);
                    UpdateToDatabase(usedPartsRefitBill);
                    foreach(var usedPartsRefitReqDetail in usedPartsRefitReqDetails) {
                        var usedPartsOutboundDetail = usedPartsOutboundOrder.UsedPartsOutboundDetails.FirstOrDefault(r => r.UsedPartsBarCode.ToLower() == usedPartsRefitReqDetail.UsedPartsBarCode.ToLower());
                        if(usedPartsOutboundDetail == null)
                            continue;
                        usedPartsRefitReqDetail.OutboundAmount = usedPartsRefitReqDetail.OutboundAmount.HasValue ? usedPartsRefitReqDetail.OutboundAmount + usedPartsOutboundDetail.Quantity : usedPartsOutboundDetail.Quantity;
                    }
                    if(usedPartsRefitBill != null) {
                        if(usedPartsRefitReqDetails.Count() == usedPartsRefitReqDetails.Count(v => v.ConfirmedAmount.HasValue && v.OutboundAmount.HasValue && v.ConfirmedAmount == v.OutboundAmount)) {
                            usedPartsRefitBill.OutboundStatus = (int)DcsUsedPartsOutboundStatus.出库完成;
                        }else if(usedPartsRefitReqDetails.Any(v => v.ConfirmedAmount != 0)) {
                            usedPartsRefitBill.OutboundStatus = (int)DcsUsedPartsOutboundStatus.部分出库;
                        }else {
                            usedPartsRefitBill.OutboundStatus = (int)DcsUsedPartsOutboundStatus.待出库;
                        }
                    }
                    new UsedPartsRefitBillAch(this.DomainService).UpdateUsedPartsRefitBillValidate(usedPartsRefitBill);
                    break;
                case (int)DcsUsedPartsOutboundOrderOutboundType.旧件处理出库:
                    var usedPartsDisposalDetails = ObjectContext.UsedPartsDisposalDetails.Where(r => r.UsedPartsDisposalBillId == usedPartsOutboundOrder.SourceId).ToArray();
                    var usedPartsDisposalBill = ObjectContext.UsedPartsDisposalBills.SingleOrDefault(r => r.Id == usedPartsOutboundOrder.SourceId);
                    UpdateToDatabase(usedPartsDisposalBill);
                    foreach(var usedPartsDisposalDetail in usedPartsDisposalDetails) {
                        var usedPartsOutboundDetail = usedPartsOutboundOrder.UsedPartsOutboundDetails.FirstOrDefault(r => r.UsedPartsBarCode.ToLower() == usedPartsDisposalDetail.UsedPartsBarCode.ToLower());
                        if(usedPartsOutboundDetail == null)
                            continue;
                        usedPartsDisposalDetail.OutboundAmount = usedPartsDisposalDetail.OutboundAmount.HasValue ? usedPartsDisposalDetail.OutboundAmount + usedPartsOutboundDetail.Quantity : usedPartsOutboundDetail.Quantity;
                    }
                    if(usedPartsDisposalBill != null) {
                        if(usedPartsDisposalDetails.Count() == usedPartsDisposalDetails.Count(v => v.ConfirmedAmount.HasValue && v.OutboundAmount.HasValue && v.ConfirmedAmount == v.OutboundAmount)) {
                            usedPartsDisposalBill.OutboundStatus = (int)DcsUsedPartsOutboundStatus.出库完成;
                        }else if(usedPartsDisposalDetails.Any(v => v.ConfirmedAmount != 0)) {
                            usedPartsDisposalBill.OutboundStatus = (int)DcsUsedPartsOutboundStatus.部分出库;
                        }else {
                            usedPartsDisposalBill.OutboundStatus = (int)DcsUsedPartsOutboundStatus.待出库;
                        }
                    }
                    new UsedPartsDisposalBillAch(this.DomainService).UpdateUsedPartsDisposalBillValidate(usedPartsDisposalBill);
                    break;
                case (int)DcsUsedPartsOutboundOrderOutboundType.调拨出库:
                    var usedPartsTransferDetails = ObjectContext.UsedPartsTransferDetails.Where(r => r.UsedPartsTransferOrderId == usedPartsOutboundOrder.SourceId).ToArray();
                    var usedPartsTransferOrder = ObjectContext.UsedPartsTransferOrders.SingleOrDefault(r => r.Id == usedPartsOutboundOrder.SourceId);
                    UpdateToDatabase(usedPartsTransferOrder);
                    foreach(var usedPartsTransferDetail in usedPartsTransferDetails) {
                        var usedPartsOutboundDetail = usedPartsOutboundOrder.UsedPartsOutboundDetails.FirstOrDefault(r => r.UsedPartsBarCode.ToLower() == usedPartsTransferDetail.UsedPartsBarCode.ToLower());
                        if(usedPartsOutboundDetail == null)
                            continue;
                        usedPartsTransferDetail.OutboundAmount = usedPartsTransferDetail.OutboundAmount.HasValue ? usedPartsTransferDetail.OutboundAmount + usedPartsOutboundDetail.Quantity : usedPartsOutboundDetail.Quantity;
                    }
                    if(usedPartsTransferOrder != null) {
                        if(!usedPartsTransferDetails.All(r => (r.ConfirmedAmount ?? 0) == 0 && (r.OutboundAmount ?? 0) == 0)) {
                            if(usedPartsTransferDetails.Count() == usedPartsTransferDetails.Count(v => v.ConfirmedAmount.HasValue && v.OutboundAmount.HasValue && v.ConfirmedAmount == v.OutboundAmount)) {
                                usedPartsTransferOrder.OutboundStatus = (int)DcsUsedPartsOutboundStatus.出库完成;
                            }else if(usedPartsTransferDetails.Any(v => v.ConfirmedAmount != 0)) {
                                usedPartsTransferOrder.OutboundStatus = (int)DcsUsedPartsOutboundStatus.部分出库;
                            }else {
                                usedPartsTransferOrder.OutboundStatus = (int)DcsUsedPartsOutboundStatus.待出库;
                            }
                        }
                    }
                    new UsedPartsTransferOrderAch(this.DomainService).UpdateUsedPartsTransferOrderValidate(usedPartsTransferOrder);
                    break;
                case (int)DcsUsedPartsOutboundOrderOutboundType.清退供应商出库:
                    var usedPartsReturnDetails = ObjectContext.UsedPartsReturnDetails.Where(r => r.UsedPartsReturnOrderId == usedPartsOutboundOrder.SourceId).ToArray();
                    var usedPartsReturnOrder = ObjectContext.UsedPartsReturnOrders.SingleOrDefault(r => r.Id == usedPartsOutboundOrder.SourceId);
                    UpdateToDatabase(usedPartsReturnOrder);
                    foreach(var usedPartsTransferDetail in usedPartsReturnDetails) {
                        var usedPartsOutboundDetail = usedPartsOutboundOrder.UsedPartsOutboundDetails.FirstOrDefault(r => r.UsedPartsBarCode.ToLower() == usedPartsTransferDetail.UsedPartsBarCode.ToLower());
                        if(usedPartsOutboundDetail == null)
                            continue;
                        usedPartsTransferDetail.OutboundAmount = usedPartsTransferDetail.OutboundAmount + usedPartsOutboundDetail.Quantity;
                    }
                    if(usedPartsReturnOrder != null) {
                        if(usedPartsReturnDetails.Count() == usedPartsReturnDetails.Count(v => v.ConfirmedAmount == v.OutboundAmount)) {
                            usedPartsReturnOrder.OutboundStatus = (int)DcsUsedPartsOutboundStatus.出库完成;
                        }else if(usedPartsReturnDetails.Any(v => v.ConfirmedAmount != 0)) {
                            usedPartsReturnOrder.OutboundStatus = (int)DcsUsedPartsOutboundStatus.部分出库;
                        }else {
                            usedPartsReturnOrder.OutboundStatus = (int)DcsUsedPartsOutboundStatus.待出库;
                        }
                    }
                    new UsedPartsReturnOrderAch(this.DomainService).UpdateUsedPartsReturnOrderValidate(usedPartsReturnOrder);
                    break;
                default:
                    throw new ValidationException(ErrorStrings.UsedPartsOutboundOrder_Validation3);
            }
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:09:49
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 旧件出库(UsedPartsOutboundOrder usedPartsOutboundOrder, int outboundType) {
            new UsedPartsOutboundOrderAch(this).旧件出库(usedPartsOutboundOrder,outboundType);
        }
    }
}
