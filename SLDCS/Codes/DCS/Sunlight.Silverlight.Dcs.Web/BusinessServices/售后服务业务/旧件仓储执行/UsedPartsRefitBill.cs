﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class UsedPartsRefitBillAch : DcsSerivceAchieveBase {
        public UsedPartsRefitBillAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 生成旧件加工单(UsedPartsRefitBill usedPartsRefitBill) {
            var usedPartsBarCodes = usedPartsRefitBill.UsedPartsRefitReqDetails.Select(r => r.UsedPartsBarCode.ToLower());
            var usedPartsStocks = ObjectContext.UsedPartsStocks.Where(r => usedPartsBarCodes.Contains(r.UsedPartsBarCode.ToLower())).ToArray();
            foreach(var usedPartsRefitReqDetail in usedPartsRefitBill.UsedPartsRefitReqDetails) {
                var usedPartsStock = usedPartsStocks.FirstOrDefault(r => r.UsedPartsBarCode.ToLower() == usedPartsRefitReqDetail.UsedPartsBarCode.ToLower());
                if(usedPartsStock == null)
                    throw new ValidationException(string.Format(ErrorStrings.UsedPartsRefitBill_Validation8, usedPartsRefitReqDetail.UsedPartsBarCode));
                if(usedPartsRefitReqDetail.PlannedAmount < 1)
                    throw new ValidationException(ErrorStrings.UsedPartsRefitBill_Validation1);
                if((usedPartsStock.StorageQuantity - usedPartsStock.LockedQuantity) < usedPartsRefitReqDetail.PlannedAmount)
                    throw new ValidationException(string.Format(ErrorStrings.UsedPartsRefitBill_Validation2, usedPartsStock.UsedPartsBarCode));
            }
        }

        public void 修改旧件加工单(UsedPartsRefitBill usedPartsRefitBill) {
            CheckEntityState(usedPartsRefitBill);
            var usedPartsBarCodes = usedPartsRefitBill.UsedPartsRefitReqDetails.Select(r => r.UsedPartsBarCode.ToLower());
            var usedPartsStocks = ObjectContext.UsedPartsStocks.Where(r => usedPartsBarCodes.Contains(r.UsedPartsBarCode.ToLower())).ToArray();
            foreach(var usedPartsRefitReqDetail in usedPartsRefitBill.UsedPartsRefitReqDetails) {
                var usedPartsStock = usedPartsStocks.FirstOrDefault(r => r.UsedPartsBarCode.ToLower() == usedPartsRefitReqDetail.UsedPartsBarCode.ToLower());
                if(usedPartsStock == null)
                    throw new ValidationException(string.Format(ErrorStrings.UsedPartsRefitBill_Validation8, usedPartsRefitReqDetail.UsedPartsBarCode.ToLower()));
                var plannedAmount = usedPartsRefitReqDetail.PlannedAmount;
                if(plannedAmount < 1)
                    throw new ValidationException(ErrorStrings.UsedPartsRefitBill_Validation1);
                if((usedPartsStock.StorageQuantity - usedPartsStock.LockedQuantity) < plannedAmount)
                    throw new ValidationException(string.Format(ErrorStrings.UsedPartsRefitBill_Validation2, usedPartsStock.UsedPartsBarCode));
            }
        }

        public void 作废旧件加工单(UsedPartsRefitBill usedPartsRefitBill) {
            var dbusedPartsRefitBill = ObjectContext.UsedPartsRefitBills.Where(r => r.Id == usedPartsRefitBill.Id && r.Status == (int)DcsUsedPartsRefitBillStatus.新建).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbusedPartsRefitBill == null)
                throw new ValidationException(ErrorStrings.UsedPartsRefitBill_Validation3);
            var userInfo = Utils.GetCurrentUserInfo();
            UpdateToDatabase(usedPartsRefitBill);
            usedPartsRefitBill.Status = (int)DcsUsedPartsRefitBillStatus.作废;
            usedPartsRefitBill.AbandonerId = userInfo.Id;
            usedPartsRefitBill.AbandonerName = userInfo.Name;
            usedPartsRefitBill.AbandonTime = DateTime.Now;
            this.UpdateUsedPartsRefitBillValidate(usedPartsRefitBill);
        }

        public void 审批旧件加工单(UsedPartsRefitBill usedPartsRefitBill) {
            var dbusedPartsRefitBill = ObjectContext.UsedPartsRefitBills.Where(r => r.Id == usedPartsRefitBill.Id && r.Status == (int)DcsUsedPartsRefitBillStatus.新建).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbusedPartsRefitBill == null)
                throw new ValidationException(ErrorStrings.UsedPartsRefitBill_Validation6);
            var usedPartsBarCodes = usedPartsRefitBill.UsedPartsRefitReqDetails.Select(r => r.UsedPartsBarCode.ToLower());
            var usedPartsStocks = ObjectContext.UsedPartsStocks.Where(r => usedPartsBarCodes.Contains(r.UsedPartsBarCode.ToLower())).ToArray();
            foreach(var usedPartsRefitReqDetail in usedPartsRefitBill.UsedPartsRefitReqDetails) {
                var plannedAmount = usedPartsRefitReqDetail.PlannedAmount;
                var confirmedAmount = usedPartsRefitReqDetail.ConfirmedAmount.HasValue ? usedPartsRefitReqDetail.ConfirmedAmount.Value : 0;
                var usedPartsStock = usedPartsStocks.FirstOrDefault(r => r.UsedPartsBarCode.ToLower() == usedPartsRefitReqDetail.UsedPartsBarCode.ToLower());
                if(usedPartsStock == null)
                    throw new ValidationException(string.Format(ErrorStrings.UsedPartsRefitBill_Validation8, usedPartsRefitReqDetail.UsedPartsBarCode));
                if(plannedAmount < 1)
                    throw new ValidationException(ErrorStrings.UsedPartsRefitBill_Validation1);
                if(confirmedAmount > plannedAmount)
                    throw new ValidationException(ErrorStrings.UsedPartsRefitBill_Validation4);
                if((usedPartsStock.StorageQuantity - usedPartsStock.LockedQuantity) < confirmedAmount)
                    throw new ValidationException(string.Format(ErrorStrings.UsedPartsRefitBill_Validation5, usedPartsStock.UsedPartsBarCode));
                var userInfo = Utils.GetCurrentUserInfo();
                UpdateToDatabase(usedPartsRefitBill);
                usedPartsRefitBill.Status = (int)DcsUsedPartsRefitBillStatus.生效;
                usedPartsRefitBill.ApproverId = userInfo.Id;
                usedPartsRefitBill.ApproverName = userInfo.Name;
                usedPartsRefitBill.ApproveTime = DateTime.Now;
                this.UpdateUsedPartsRefitBillValidate(usedPartsRefitBill);
                UpdateToDatabase(usedPartsStock);
                usedPartsStock.LockedQuantity = confirmedAmount;
                new UsedPartsStockAch(this.DomainService).UpdateUsedPartsStockValidate(usedPartsStock);
            }
        }

        public void 终止旧件加工单(UsedPartsRefitBill usedPartsRefitBill) {
            var dbusedPartsRefitBill = ObjectContext.UsedPartsRefitBills.Where(r => r.Id == usedPartsRefitBill.Id && r.Status == (int)DcsUsedPartsRefitBillStatus.生效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbusedPartsRefitBill == null)
                throw new ValidationException(ErrorStrings.UsedPartsRefitBill_Validation7);
            UpdateToDatabase(usedPartsRefitBill);
            usedPartsRefitBill.OutboundStatus = (int)DcsUsedPartsOutboundStatus.终止出库;
            usedPartsRefitBill.InboundStatus = (int)DcsUsedPartsInboundStatus.终止入库;
            this.UpdateUsedPartsRefitBillValidate(usedPartsRefitBill);
            var usedPartsBarCodes = usedPartsRefitBill.UsedPartsRefitReqDetails.Select(r => r.UsedPartsBarCode.ToLower());
            var usedPartsStocks = ObjectContext.UsedPartsStocks.Where(r => usedPartsBarCodes.Contains(r.UsedPartsBarCode.ToLower())).ToArray();
            foreach(var usedPartsRefitReqDetail in usedPartsRefitBill.UsedPartsRefitReqDetails) {
                var outboundAmount = usedPartsRefitReqDetail.OutboundAmount.HasValue ? usedPartsRefitReqDetail.OutboundAmount.Value : 0;
                var confirmedAmount = usedPartsRefitReqDetail.ConfirmedAmount.HasValue ? usedPartsRefitReqDetail.ConfirmedAmount.Value : 0;
                var usedPartsStock = usedPartsStocks.FirstOrDefault(r => r.UsedPartsBarCode.ToLower() == usedPartsRefitReqDetail.UsedPartsBarCode.ToLower());
                if(usedPartsStock == null)
                    throw new ValidationException(string.Format(ErrorStrings.UsedPartsRefitBill_Validation8, usedPartsRefitReqDetail.UsedPartsBarCode));
                if(confirmedAmount <= outboundAmount || outboundAmount != 0)
                    return;
                UpdateToDatabase(usedPartsStock);
                usedPartsStock.LockedQuantity = 0;
                new UsedPartsStockAch(this.DomainService).UpdateUsedPartsStockValidate(usedPartsStock);
            }
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:09:49
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 生成旧件加工单(UsedPartsRefitBill usedPartsRefitBill) {
            new UsedPartsRefitBillAch(this).生成旧件加工单(usedPartsRefitBill);
        }
        
        [Update(UsingCustomMethod = true)]
        public void 修改旧件加工单(UsedPartsRefitBill usedPartsRefitBill) {
            new UsedPartsRefitBillAch(this).修改旧件加工单(usedPartsRefitBill);
        }
        
        [Update(UsingCustomMethod = true)]
        public void 作废旧件加工单(UsedPartsRefitBill usedPartsRefitBill) {
            new UsedPartsRefitBillAch(this).作废旧件加工单(usedPartsRefitBill);
        }
        
        [Update(UsingCustomMethod = true)]
        public void 审批旧件加工单(UsedPartsRefitBill usedPartsRefitBill) {
            new UsedPartsRefitBillAch(this).审批旧件加工单(usedPartsRefitBill);
        }
        
        [Update(UsingCustomMethod = true)]
        public void 终止旧件加工单(UsedPartsRefitBill usedPartsRefitBill) {
            new UsedPartsRefitBillAch(this).终止旧件加工单(usedPartsRefitBill);
        }
    }
}
