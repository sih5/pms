﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class UsedPartsReturnOrderAch : DcsSerivceAchieveBase {
        public UsedPartsReturnOrderAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 修改旧件清退单(UsedPartsReturnOrder usedPartsReturnOrder) {
            CheckEntityState(usedPartsReturnOrder);
            var usedPartsReturnOrders = ObjectContext.UsedPartsReturnOrders.Where(r => r.Id == usedPartsReturnOrder.Id && r.Status == (int)DcsUsedPartsReturnOrderStatus.新建).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(usedPartsReturnOrders == null)
                throw new ValidationException(ErrorStrings.UsedPartsReturnOrder_Validation1);
        }

        public void 作废旧件清退单(UsedPartsReturnOrder usedPartsReturnOrder) {
            var usedPartsReturnOrders = ObjectContext.UsedPartsReturnOrders.Where(r => r.Id == usedPartsReturnOrder.Id && r.Status == (int)DcsUsedPartsReturnOrderStatus.新建).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(usedPartsReturnOrders == null)
                throw new ValidationException(ErrorStrings.UsedPartsReturnOrder_Validation2);
            UpdateToDatabase(usedPartsReturnOrder);
            usedPartsReturnOrder.Status = (int)DcsUsedPartsReturnOrderStatus.作废;
            var userInfo = Utils.GetCurrentUserInfo();
            usedPartsReturnOrder.AbandonerId = userInfo.Id;
            usedPartsReturnOrder.AbandonerName = userInfo.Name;
            usedPartsReturnOrder.AbandonTime = DateTime.Now;
            this.UpdateUsedPartsReturnOrderValidate(usedPartsReturnOrder);
        }

        public void 审批旧件清退单(UsedPartsReturnOrder usedPartsReturnOrder) {
            var usedPartsReturnOrders = ObjectContext.UsedPartsReturnOrders.Where(r => r.Id == usedPartsReturnOrder.Id && r.Status == (int)DcsUsedPartsReturnOrderStatus.新建).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(usedPartsReturnOrders == null)
                throw new ValidationException(ErrorStrings.UsedPartsReturnOrder_Validation3);
            var usedPartsBarCodes = ObjectContext.UsedPartsReturnDetails.Where(r => r.UsedPartsReturnOrderId == usedPartsReturnOrder.Id).Select(r => r.UsedPartsBarCode);
            var usedPartsStocks = ObjectContext.UsedPartsStocks.Where(r => r.UsedPartsWarehouseId == usedPartsReturnOrder.OutboundWarehouseId && usedPartsBarCodes.Contains(r.UsedPartsBarCode)).ToArray();
            if(usedPartsStocks.Any(r => r.LockedQuantity == 1)) {
                var errorUsedPart = usedPartsStocks.First(r => r.LockedQuantity == 1);
                throw new ValidationException("条码" + errorUsedPart.UsedPartsBarCode + "库存已经被锁定请修改清退单");
            }
            foreach(var usedPartsStock in usedPartsStocks) {
                UpdateToDatabase(usedPartsStock);
                usedPartsStock.LockedQuantity = 1;
                new UsedPartsStockAch(this.DomainService).UpdateUsedPartsStockValidate(usedPartsStock);
            }
            UpdateToDatabase(usedPartsReturnOrder);
            usedPartsReturnOrder.Status = (int)DcsUsedPartsReturnOrderStatus.已审批;
            usedPartsReturnOrder.OutboundStatus = (int)DcsUsedPartsOutboundStatus.待出库;
            foreach(var usedPartsReturnDetail in usedPartsReturnOrder.UsedPartsReturnDetails) {
                var usedPartsStock = usedPartsStocks.FirstOrDefault(r => r.UsedPartsBarCode == usedPartsReturnDetail.UsedPartsBarCode);
                if(usedPartsStock == null) {
                    usedPartsReturnDetail.ConfirmedAmount = 0;
                }else {
                    usedPartsReturnDetail.ConfirmedAmount = 1;
                }
                var userInfo = Utils.GetCurrentUserInfo();
                usedPartsReturnOrder.ApproverId = userInfo.Id;
                usedPartsReturnOrder.ApproverName = userInfo.Name;
                usedPartsReturnOrder.ApproveTime = DateTime.Now;
            }
            this.UpdateUsedPartsReturnOrderValidate(usedPartsReturnOrder);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:09:49
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 修改旧件清退单(UsedPartsReturnOrder usedPartsReturnOrder) {
            new UsedPartsReturnOrderAch(this).修改旧件清退单(usedPartsReturnOrder);
        }
        
        [Update(UsingCustomMethod = true)]
        public void 作废旧件清退单(UsedPartsReturnOrder usedPartsReturnOrder) {
            new UsedPartsReturnOrderAch(this).作废旧件清退单(usedPartsReturnOrder);
        }
        
        [Update(UsingCustomMethod = true)]
        public void 审批旧件清退单(UsedPartsReturnOrder usedPartsReturnOrder) {
            new UsedPartsReturnOrderAch(this).审批旧件清退单(usedPartsReturnOrder);
        }
    }
}
