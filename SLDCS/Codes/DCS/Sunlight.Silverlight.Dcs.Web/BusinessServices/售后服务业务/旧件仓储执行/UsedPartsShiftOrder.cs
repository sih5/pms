﻿using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class UsedPartsShiftOrderAch : DcsSerivceAchieveBase {
        public UsedPartsShiftOrderAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 生成旧件移库单(UsedPartsShiftOrder usedPartsShiftOrder) {
            var usedPartsBarCodes = usedPartsShiftOrder.UsedPartsShiftDetails.Select(r => r.UsedPartsBarCode.ToLower()).ToArray();
            var destiStoragePositionIds = usedPartsShiftOrder.UsedPartsShiftDetails.Select(r => r.DestiStoragePositionId).ToArray();
            var usedPartsStocks = ObjectContext.UsedPartsStocks.Where(r => usedPartsBarCodes.Contains(r.UsedPartsBarCode.ToLower())).ToArray();

            var usedPartsWarehouseAreas = ObjectContext.UsedPartsWarehouseAreas.Where(r => destiStoragePositionIds.Contains(r.Id) && r.Status == (int)DcsBaseDataStatus.有效).Select(v => v.Id).ToArray();

            foreach(var usedPartsShiftDetail in usedPartsShiftOrder.UsedPartsShiftDetails) {
                if(!usedPartsWarehouseAreas.Contains(usedPartsShiftDetail.DestiStoragePositionId))
                    throw new ValidationException(string.Format(ErrorStrings.UsedPartsShiftOrder_Validation2, usedPartsShiftDetail.DestiStoragePositionCode));
                var usedPartsStock = usedPartsStocks.FirstOrDefault(r => r.UsedPartsBarCode.ToLower() == usedPartsShiftDetail.UsedPartsBarCode.ToLower() && r.UsedPartsWarehouseId == usedPartsShiftOrder.UsedPartsWarehouseId);
                if(usedPartsStock == null)
                    throw new ValidationException(string.Format(ErrorStrings.UsedPartsShiftOrder_Validation1, usedPartsShiftDetail.UsedPartsBarCode));
                UpdateToDatabase(usedPartsStock);
                usedPartsStock.UsedPartsWarehouseAreaId = usedPartsShiftDetail.DestiStoragePositionId;
                new UsedPartsStockAch(this.DomainService).UpdateUsedPartsStockValidate(usedPartsStock);
            }

        }

    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:09:49
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 生成旧件移库单(UsedPartsShiftOrder usedPartsShiftOrder) {
            new UsedPartsShiftOrderAch(this).生成旧件移库单(usedPartsShiftOrder);
        }
    }
}
