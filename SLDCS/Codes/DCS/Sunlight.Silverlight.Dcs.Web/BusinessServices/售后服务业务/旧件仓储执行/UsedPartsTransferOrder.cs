﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class UsedPartsTransferOrderAch : DcsSerivceAchieveBase {
        public UsedPartsTransferOrderAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 生成旧件调拨单(UsedPartsTransferOrder usedPartsTransferOrder) {
            var usedPartsTransferDetails = usedPartsTransferOrder.UsedPartsTransferDetails;
            var usedPartsBarCodes = usedPartsTransferDetails.Select(r => r.UsedPartsBarCode.ToLower()).ToArray();
            var usedPartsStocks = ObjectContext.UsedPartsStocks.Where(r => usedPartsBarCodes.Contains(r.UsedPartsBarCode.ToLower())).ToArray();
            usedPartsBarCodes = usedPartsBarCodes.Except(usedPartsStocks.Select(r => r.UsedPartsBarCode.ToLower())).ToArray();
            if(usedPartsBarCodes.Length > 0)
                throw new ValidationException(string.Format(ErrorStrings.UsedPartsTransferOrder_Validation8, usedPartsBarCodes.First()));
            if(usedPartsTransferDetails.Any(r => r.PlannedAmount < 1))
                throw new ValidationException(ErrorStrings.UsedPartsTransferOrder_Validation1);
            var dbusedPartsStock = usedPartsStocks.FirstOrDefault(v => usedPartsTransferDetails.Any(r => r.UsedPartsBarCode.ToLower() == v.UsedPartsBarCode.ToLower() && (v.StorageQuantity - v.LockedQuantity) < r.PlannedAmount));
            if(dbusedPartsStock != null)
                throw new ValidationException(string.Format(ErrorStrings.UsedPartsTransferOrder_Validation2, dbusedPartsStock.UsedPartsBarCode));
            校验旧件条码不可多次调拨(usedPartsTransferOrder);
            InsertToDatabase(usedPartsTransferOrder);
            usedPartsTransferOrder.TotalAmount = usedPartsTransferOrder.UsedPartsTransferDetails.Sum(r => r.PlannedAmount * r.Price);
            usedPartsTransferOrder.TotalQuantity = usedPartsTransferOrder.UsedPartsTransferDetails.Sum(r => r.PlannedAmount);
            InsertUsedPartsTransferOrder(usedPartsTransferOrder);
        }

        public void 修改旧件调拨单(UsedPartsTransferOrder usedPartsTransferOrder) {
            CheckEntityState(usedPartsTransferOrder);
            var usedPartsTransferDetails = usedPartsTransferOrder.UsedPartsTransferDetails;
            var usedPartsBarCodes = usedPartsTransferDetails.Select(r => r.UsedPartsBarCode.ToLower()).ToArray();
            var usedPartsStocks = ObjectContext.UsedPartsStocks.Where(r => usedPartsBarCodes.Contains(r.UsedPartsBarCode.ToLower())).ToArray();
            usedPartsBarCodes = usedPartsBarCodes.Except(usedPartsStocks.Select(r => r.UsedPartsBarCode.ToLower())).ToArray();
            if(usedPartsBarCodes.Length > 0)
                throw new ValidationException(string.Format(ErrorStrings.UsedPartsTransferOrder_Validation8, usedPartsBarCodes.First()));
            if(usedPartsTransferDetails.Any(r => r.PlannedAmount < 1))
                throw new ValidationException(ErrorStrings.UsedPartsTransferOrder_Validation1);
            var dbusedPartsStock = usedPartsStocks.FirstOrDefault(v => usedPartsTransferDetails.Any(r => r.UsedPartsBarCode.ToLower() == v.UsedPartsBarCode.ToLower() && (v.StorageQuantity - v.LockedQuantity) < r.PlannedAmount));
            if(dbusedPartsStock != null)
                throw new ValidationException(string.Format(ErrorStrings.UsedPartsTransferOrder_Validation2, dbusedPartsStock.UsedPartsBarCode));
            校验旧件条码不可多次调拨(usedPartsTransferOrder);
            UpdateToDatabase(usedPartsTransferOrder);
            usedPartsTransferOrder.TotalAmount = usedPartsTransferOrder.UsedPartsTransferDetails.Sum(r => r.PlannedAmount * r.Price);
            usedPartsTransferOrder.TotalQuantity = usedPartsTransferOrder.UsedPartsTransferDetails.Sum(r => r.PlannedAmount);
        }

        public void 审批旧件调拨单(UsedPartsTransferOrder usedPartsTransferOrder) {
            var usedPartsTransferDetails = usedPartsTransferOrder.UsedPartsTransferDetails;
            if(usedPartsTransferDetails.Any(v => !v.ConfirmedAmount.HasValue))
                throw new ValidationException(ErrorStrings.UsedPartsTransferOrder_Validation9);
            var usedPartsBarCodes = usedPartsTransferDetails.Select(r => r.UsedPartsBarCode.ToLower()).ToArray();
            var usedPartsStocks = ObjectContext.UsedPartsStocks.Where(r => usedPartsBarCodes.Contains(r.UsedPartsBarCode.ToLower())).ToArray();
            usedPartsBarCodes = usedPartsBarCodes.Except(usedPartsStocks.Select(r => r.UsedPartsBarCode.ToLower())).ToArray();
            if(usedPartsBarCodes.Length > 0)
                throw new ValidationException(string.Format(ErrorStrings.UsedPartsTransferOrder_Validation8, usedPartsBarCodes.First()));
            if(usedPartsTransferDetails.Any(r => r.PlannedAmount < 1))
                throw new ValidationException(ErrorStrings.UsedPartsTransferOrder_Validation1);
            if(usedPartsTransferDetails.Any(r => r.PlannedAmount < r.ConfirmedAmount))
                throw new ValidationException(ErrorStrings.UsedPartsTransferOrder_Validation3);
            var dbusedPartsStock = usedPartsStocks.FirstOrDefault(v => usedPartsTransferDetails.Any(r => r.UsedPartsBarCode.ToLower() == v.UsedPartsBarCode.ToLower() && (v.StorageQuantity - v.LockedQuantity) < r.PlannedAmount));
            if(dbusedPartsStock != null)
                throw new ValidationException(string.Format(ErrorStrings.UsedPartsTransferOrder_Validation2, dbusedPartsStock.UsedPartsBarCode));
            var dbusedPartsTransferOrder = ObjectContext.UsedPartsTransferOrders.Where(r => r.Id == usedPartsTransferOrder.Id && r.Status == (int)DcsUsedPartsTransferOrderStatus.新建).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbusedPartsTransferOrder == null)
                throw new ValidationException(ErrorStrings.UsedPartsTransferOrder_Validation4);
            UpdateToDatabase(usedPartsTransferOrder);
            usedPartsTransferOrder.Status = (int)DcsUsedPartsTransferOrderStatus.生效;
            this.UpdateUsedPartsTransferOrderValidate(usedPartsTransferOrder);
            foreach(var usedPartsStock in usedPartsStocks) {
                var usedPartsTransferDetail = usedPartsTransferDetails.FirstOrDefault(r => r.UsedPartsBarCode.ToLower() == usedPartsStock.UsedPartsBarCode.ToLower());
                UpdateToDatabase(usedPartsStock);
                if(usedPartsTransferDetail != null)
                    usedPartsStock.LockedQuantity = usedPartsTransferDetail.ConfirmedAmount.HasValue ? usedPartsTransferDetail.ConfirmedAmount.Value : 0;
                new UsedPartsStockAch(this.DomainService).UpdateUsedPartsStockValidate(usedPartsStock);
            }
            var userInfo = Utils.GetCurrentUserInfo();
            usedPartsTransferOrder.ApproverId = userInfo.Id;
            usedPartsTransferOrder.ApproverName = userInfo.Name;
            usedPartsTransferOrder.ApproveTime = DateTime.Now;
        }

        public void 作废旧件调拨单(UsedPartsTransferOrder usedPartsTransferOrder) {
            var dbusedPartsTransferOrder = ObjectContext.UsedPartsTransferOrders.Where(r => r.Id == usedPartsTransferOrder.Id && r.Status == (int)DcsUsedPartsTransferOrderStatus.新建).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbusedPartsTransferOrder == null)
                throw new ValidationException(ErrorStrings.UsedPartsTransferOrder_Validation5);
            UpdateToDatabase(usedPartsTransferOrder);
            usedPartsTransferOrder.Status = (int)DcsUsedPartsTransferOrderStatus.作废;
            this.UpdateUsedPartsTransferOrderValidate(usedPartsTransferOrder);
            var userInfo = Utils.GetCurrentUserInfo();
            usedPartsTransferOrder.AbandonerId = userInfo.Id;
            usedPartsTransferOrder.AbandonerName = userInfo.Name;
            usedPartsTransferOrder.AbandonTime = DateTime.Now;
        }

        public void 终止旧件调拨单(UsedPartsTransferOrder usedPartsTransferOrder) {
            var usedPartsTransferDetails = usedPartsTransferOrder.UsedPartsTransferDetails;
            if(usedPartsTransferDetails.Any(r => r.OutboundAmount != r.InboundAmount))
                throw new ValidationException(ErrorStrings.UsedPartsTransferOrder_Validation6);
            var dbusedPartsTransferOrder = ObjectContext.UsedPartsTransferOrders.Where(r => r.Id == usedPartsTransferOrder.Id && (r.Status == (int)DcsUsedPartsTransferOrderStatus.生效 || r.Status == (int)DcsUsedPartsTransferOrderStatus.新建)).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbusedPartsTransferOrder == null)
                throw new ValidationException(ErrorStrings.UsedPartsTransferOrder_Validation7);
            UpdateToDatabase(usedPartsTransferOrder);
            usedPartsTransferOrder.OutboundStatus = (int)DcsUsedPartsOutboundStatus.终止出库;
            usedPartsTransferOrder.InboundStatus = (int)DcsUsedPartsInboundStatus.终止入库;
            this.UpdateUsedPartsTransferOrderValidate(usedPartsTransferOrder);
            var usedPartsBarCodes = usedPartsTransferDetails.Select(v => v.UsedPartsBarCode.ToLower()).ToArray();
            var usedPartsStocks = ObjectContext.UsedPartsStocks.Where(r => usedPartsBarCodes.Contains(r.UsedPartsBarCode.ToLower())).ToArray();
            foreach(var usedPartsStock in usedPartsStocks) {
                var usedPartsTransferDetail = usedPartsTransferDetails.FirstOrDefault(r => r.UsedPartsBarCode.ToLower() == usedPartsStock.UsedPartsBarCode.ToLower() && r.ConfirmedAmount.HasValue && r.ConfirmedAmount > r.OutboundAmount && r.OutboundAmount == 0);
                if(usedPartsTransferDetail == null)
                    continue;
                UpdateToDatabase(usedPartsStock);
                usedPartsStock.LockedQuantity = 0;
                new UsedPartsStockAch(this.DomainService).UpdateUsedPartsStockValidate(usedPartsStock);
            }
        }
        public void 校验旧件条码不可多次调拨(UsedPartsTransferOrder usedPartsTransferOrder) {
            var usedPartsTransferOrderUsedPartsBarCodes = usedPartsTransferOrder.UsedPartsTransferDetails.Select(r => r.UsedPartsBarCode);
            var dbUsedPartsTransferOrderUsedPartsBarCodes = ObjectContext.UsedPartsTransferOrders.Where(r => r.Status == (int)DcsUsedPartsTransferOrderStatus.新建 && r.OriginWarehouseId == usedPartsTransferOrder.OriginWarehouseId && r.Id != usedPartsTransferOrder.Id).SelectMany(r => r.UsedPartsTransferDetails).Where(r => usedPartsTransferOrderUsedPartsBarCodes.Any(v => v == r.UsedPartsBarCode)).Select(r => r.UsedPartsBarCode);
            var dbUsedPartsLoanBillUsedPartsBarCodes = ObjectContext.UsedPartsLoanBills.Where(r => r.Status == (int)DcsUsedPartsLoanBillStatus.新建 && r.UsedPartsWarehouseId == usedPartsTransferOrder.OriginWarehouseId).SelectMany(r => r.UsedPartsLoanDetails).Where(r => usedPartsTransferOrderUsedPartsBarCodes.Any(v => v == r.UsedPartsBarCode)).Select(r => r.UsedPartsBarCode);
            var dbUsedPartsDisposalBillUsedPartsBarCodes = ObjectContext.UsedPartsDisposalBills.Where(r => r.Status == (int)DcsUsedPartsLoanBillStatus.新建 && r.UsedPartsWarehouseId == usedPartsTransferOrder.OriginWarehouseId).SelectMany(r => r.UsedPartsDisposalDetails).Where(r => usedPartsTransferOrderUsedPartsBarCodes.Any(v => v == r.UsedPartsBarCode)).Select(r => r.UsedPartsBarCode);
            var dbUsedPartsReturnOrderUsedPartsBarCodes = ObjectContext.UsedPartsReturnOrders.Where(r => r.Status == (int)DcsUsedPartsLoanBillStatus.新建 && r.OutboundWarehouseId == usedPartsTransferOrder.OriginWarehouseId).SelectMany(r => r.UsedPartsReturnDetails).Where(r => usedPartsTransferOrderUsedPartsBarCodes.Any(v => v == r.UsedPartsBarCode)).Select(r => r.UsedPartsBarCode);
            if(dbUsedPartsTransferOrderUsedPartsBarCodes.Any() || dbUsedPartsLoanBillUsedPartsBarCodes.Any() || dbUsedPartsDisposalBillUsedPartsBarCodes.Any() || dbUsedPartsReturnOrderUsedPartsBarCodes.Any()) {
                throw new ValidationException(string.Format("以下旧件条码：{0}旧件条码已生成旧件调拨单，不可再次调拨", string.Join(",", dbUsedPartsTransferOrderUsedPartsBarCodes.Union(dbUsedPartsLoanBillUsedPartsBarCodes).Union(dbUsedPartsDisposalBillUsedPartsBarCodes).Union(dbUsedPartsReturnOrderUsedPartsBarCodes))));
            }
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:09:49
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 生成旧件调拨单(UsedPartsTransferOrder usedPartsTransferOrder) {
            new UsedPartsTransferOrderAch(this).生成旧件调拨单(usedPartsTransferOrder);
        }
        
        [Update(UsingCustomMethod = true)]
        public void 修改旧件调拨单(UsedPartsTransferOrder usedPartsTransferOrder) {
            new UsedPartsTransferOrderAch(this).修改旧件调拨单(usedPartsTransferOrder);
        }
        
        [Update(UsingCustomMethod = true)]
        public void 审批旧件调拨单(UsedPartsTransferOrder usedPartsTransferOrder) {
            new UsedPartsTransferOrderAch(this).审批旧件调拨单(usedPartsTransferOrder);
        }
        
        [Update(UsingCustomMethod = true)]
        public void 作废旧件调拨单(UsedPartsTransferOrder usedPartsTransferOrder) {
            new UsedPartsTransferOrderAch(this).作废旧件调拨单(usedPartsTransferOrder);
        }
        
        [Update(UsingCustomMethod = true)]
        public void 终止旧件调拨单(UsedPartsTransferOrder usedPartsTransferOrder) {
            new UsedPartsTransferOrderAch(this).终止旧件调拨单(usedPartsTransferOrder);
        }

        public void 校验旧件条码不可多次调拨(UsedPartsTransferOrder usedPartsTransferOrder) {
            new UsedPartsTransferOrderAch(this).校验旧件条码不可多次调拨(usedPartsTransferOrder);
        }
    }
}
