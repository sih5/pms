﻿using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class UsedPartsWarehouseAch : DcsSerivceAchieveBase {
        public UsedPartsWarehouseAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 作废旧件仓库(UsedPartsWarehouse usedPartsWarehouse) {
            var dbusedPartsWarehouse = ObjectContext.UsedPartsWarehouses.Where(r => r.Id == usedPartsWarehouse.Id && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbusedPartsWarehouse == null)
                throw new ValidationException(ErrorStrings.UsedPartsWarehouse_Validation2);
            if(ObjectContext.UsedPartsStocks.Any(r => r.UsedPartsWarehouseId == usedPartsWarehouse.Id && r.StorageQuantity > 0))
                throw new ValidationException(ErrorStrings.UsedPartsWarehouse_Validation3);
            UpdateToDatabase(usedPartsWarehouse);
            usedPartsWarehouse.Status = (int)DcsBaseDataStatus.作废;
            this.UpdateUsedPartsWarehouseValidate(usedPartsWarehouse);
            var dbUsedPartsWarehouseAreas = ObjectContext.UsedPartsWarehouseAreas.Where(r => r.UsedPartsWarehouseId == usedPartsWarehouse.Id);
            foreach(var usedPartsWarehouseArea in dbUsedPartsWarehouseAreas) {
                UpdateToDatabase(usedPartsWarehouseArea);
                usedPartsWarehouseArea.Status = (int)DcsBaseDataStatus.作废;
                new UsedPartsWarehouseAreaAch(this.DomainService).UpdateUsedPartsWarehouseAreaValidate(usedPartsWarehouseArea);
            }
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:09:49
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 作废旧件仓库(UsedPartsWarehouse usedPartsWarehouse) {
            new UsedPartsWarehouseAch(this).作废旧件仓库(usedPartsWarehouse);
        }
    }
}
