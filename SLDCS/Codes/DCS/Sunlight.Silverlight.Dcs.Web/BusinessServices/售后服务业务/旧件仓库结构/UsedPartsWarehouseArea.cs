﻿using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Sunlight.Silverlight.Dcs.Web.Resources;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class UsedPartsWarehouseAreaAch : DcsSerivceAchieveBase {
        public UsedPartsWarehouseAreaAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 作废旧件库区库位(UsedPartsWarehouseArea usedPartsWarehouseArea) {
            var childUsedPartsWarehouseAreas = ObjectContext.GetChildUsedPartsWhseAreas(usedPartsWarehouseArea.Id).ToArray();
            var usedPartsWarehouseAreaIds = childUsedPartsWarehouseAreas.Select(r => r.Id);
            if(ObjectContext.UsedPartsStocks.Any(r => usedPartsWarehouseAreaIds.Contains(r.UsedPartsWarehouseAreaId) && r.StorageQuantity > 0))
                throw new ValidationException(ErrorStrings.UsedPartsWarehouseArea_Validation2);
            foreach(var childUsedPartsWarehouseArea in childUsedPartsWarehouseAreas) {
                UpdateToDatabase(childUsedPartsWarehouseArea);
                childUsedPartsWarehouseArea.Status = (int)DcsBaseDataStatus.作废;
                this.UpdateUsedPartsWarehouseAreaValidate(childUsedPartsWarehouseArea);
            }
        }
        /// <summary>
        /// 仅供客户端使用
        /// </summary>
        /// <param name="usedPartsWarehouseArea"></param>

        public void 修改旧件库区库位(UsedPartsWarehouseArea usedPartsWarehouseArea) {
            CheckEntityState(usedPartsWarehouseArea);
            var originalUsedPartsWarehouseArea = ChangeSet.GetOriginal(usedPartsWarehouseArea);
            if(!usedPartsWarehouseArea.IfDefaultStoragePosition.HasValue) {
                usedPartsWarehouseArea.IfDefaultStoragePosition = false;
            }
            if(!(bool)usedPartsWarehouseArea.IfDefaultStoragePosition) {
                var DBUsedPartsWarehouseArea = ObjectContext.UsedPartsWarehouseAreas.FirstOrDefault(u => u.UsedPartsWarehouseId == usedPartsWarehouseArea.UsedPartsWarehouseId && u.Id != usedPartsWarehouseArea.Id && u.IfDefaultStoragePosition == true);
                if(DBUsedPartsWarehouseArea == null) {
                    throw new ValidationException("该旧件仓库下至少有一个默认库位");
                }
            }
            if(originalUsedPartsWarehouseArea == null || originalUsedPartsWarehouseArea.StorageAreaType == usedPartsWarehouseArea.StorageAreaType)
                return;
            if(usedPartsWarehouseArea.StorageAreaType == (int)DcsAreaKind.库区) {
                if(ObjectContext.UsedPartsStocks.Any(r => r.UsedPartsWarehouseAreaId == usedPartsWarehouseArea.Id && r.StorageQuantity > 0))
                    throw new ValidationException(ErrorStrings.UsedPartsWarehouseArea_Validation3);
            }
            if(usedPartsWarehouseArea.StorageAreaType == (int)DcsAreaKind.库位) {
                if(usedPartsWarehouseArea.TopLevelUsedPartsWhseAreaId.HasValue)
                    throw new ValidationException(ErrorStrings.UsedPartsWarehouseArea_Validation4);
                if(ObjectContext.UsedPartsWarehouseAreas.Any(r => r.ParentId == usedPartsWarehouseArea.Id))
                    throw new ValidationException(ErrorStrings.UsedPartsWarehouseArea_Validation5);

            }
        }


        public void 指定为默认库位(UsedPartsWarehouseArea usedPartsWarehouseArea) {
            //合法性校验：库区库位类型=库位，是否默认库位=否
            var dbusedPartsWarehouseArea = ObjectContext.UsedPartsWarehouseAreas.First(r => r.Id == usedPartsWarehouseArea.Id);
            if(dbusedPartsWarehouseArea.StorageAreaType != (int)DcsAreaKind.库位) {
                throw new ValidationException("当前修改非库位类型");
            }
            if(dbusedPartsWarehouseArea.IfDefaultStoragePosition == null || (dbusedPartsWarehouseArea.IfDefaultStoragePosition != null && (bool)dbusedPartsWarehouseArea.IfDefaultStoragePosition)) {
                throw new ValidationException("指定当前库位已是默认库位");
            }
            //更新旧件库区库位：是否默认库位=是，
            UpdateToDatabase(dbusedPartsWarehouseArea);
            dbusedPartsWarehouseArea.IfDefaultStoragePosition = true;
            this.UpdateUsedPartsWarehouseAreaValidate(dbusedPartsWarehouseArea);
            //查询旧件库区库位（仓库id=当前库区库位.仓库Id，且是否默认库位=是）,如果存在数据，更新该数据,是否默认库位=否
            var otherusedPartsWarehouseArea = ObjectContext.UsedPartsWarehouseAreas.FirstOrDefault(r => r.UsedPartsWarehouseId == usedPartsWarehouseArea.UsedPartsWarehouseId && r.IfDefaultStoragePosition == true && r.Id != usedPartsWarehouseArea.Id);
            if(otherusedPartsWarehouseArea != null) {
                UpdateToDatabase(otherusedPartsWarehouseArea);
                otherusedPartsWarehouseArea.IfDefaultStoragePosition = false;
                this.UpdateUsedPartsWarehouseAreaValidate(otherusedPartsWarehouseArea);
            }
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:09:49
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 作废旧件库区库位(UsedPartsWarehouseArea usedPartsWarehouseArea) {
            new UsedPartsWarehouseAreaAch(this).作废旧件库区库位(usedPartsWarehouseArea);
        }
        /// <summary>
        /// 仅供客户端使用
        /// </summary>
        /// <param name="usedPartsWarehouseArea"></param>
        [Update(UsingCustomMethod = true)]
        public void 修改旧件库区库位(UsedPartsWarehouseArea usedPartsWarehouseArea) {
            new UsedPartsWarehouseAreaAch(this).修改旧件库区库位(usedPartsWarehouseArea);
        }

        [Update(UsingCustomMethod = true)]
        public void 指定为默认库位(UsedPartsWarehouseArea usedPartsWarehouseArea) {
            new UsedPartsWarehouseAreaAch(this).指定为默认库位(usedPartsWarehouseArea);
        }
    }
}
