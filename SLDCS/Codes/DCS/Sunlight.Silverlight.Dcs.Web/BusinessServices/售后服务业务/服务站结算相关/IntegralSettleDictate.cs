﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class IntegralSettleDictateAch : DcsSerivceAchieveBase {
        public IntegralSettleDictateAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 作废服务站积分索赔结算指令(IntegralSettleDictate integralSettleDictate) {
            var dbIntegralSettleDictate = ObjectContext.IntegralSettleDictates.Where(r => r.Id == integralSettleDictate.Id && r.Status == (int)DcsIntegralSettleDictateStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbIntegralSettleDictate == null)
                throw new ValidationException(ErrorStrings.SsClaimSettleInstruction_Validation1);
            var userInfo = Utils.GetCurrentUserInfo();
            UpdateToDatabase(integralSettleDictate);
            integralSettleDictate.Status = (int)DcsIntegralSettleDictateStatus.作废;
            integralSettleDictate.AbandonerId = userInfo.Id;
            integralSettleDictate.AbandonerName = userInfo.Name;
            integralSettleDictate.AbandonerTime = DateTime.Now;
            this.UpdateIntegralSettleDictateValidate(integralSettleDictate);
        }

        public void AbandonIntegralSettleDictateById(int id) {
            var dbIntegralSettleDictate = ObjectContext.IntegralSettleDictates.SingleOrDefault(r => r.Id == id);
            if(dbIntegralSettleDictate == null) {
                throw new ValidationException(ErrorStrings.Common_Validation4);
            }
            if(dbIntegralSettleDictate.Status != (int)DcsIntegralSettleDictateStatus.有效) {
                throw new ValidationException(ErrorStrings.Common_Validation5);
            }
            var userInfo = Utils.GetCurrentUserInfo();
            dbIntegralSettleDictate.Status = (int)DcsIntegralSettleDictateStatus.作废;
            dbIntegralSettleDictate.AbandonerId = userInfo.Id;
            dbIntegralSettleDictate.AbandonerName = userInfo.Name;
            dbIntegralSettleDictate.AbandonerTime = DateTime.Now;
            UpdateToDatabase(dbIntegralSettleDictate);
            ObjectContext.SaveChanges();
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:09:49
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 作废服务站积分索赔结算指令(IntegralSettleDictate integralSettleDictate) {
            new IntegralSettleDictateAch(this).作废服务站积分索赔结算指令(integralSettleDictate);
        }
        
        [Invoke]
        public void AbandonIntegralSettleDictateById(int id) {
            new IntegralSettleDictateAch(this).AbandonIntegralSettleDictateById(id);
        }
    }
}
