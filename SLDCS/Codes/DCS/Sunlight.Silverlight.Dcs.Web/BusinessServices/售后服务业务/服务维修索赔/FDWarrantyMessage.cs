﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class FdWarrantyMessageAch : DcsSerivceAchieveBase {
        public FdWarrantyMessageAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 作废福戴三包信息(FDWarrantyMessage fdWarrantyMessage) {
            var dbFaultyPartsSupplierAssembly = ObjectContext.FDWarrantyMessages.Where(r => r.Id == fdWarrantyMessage.Id && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbFaultyPartsSupplierAssembly == null)
                throw new ValidationException("只能作废处于“有效”状态的福戴三包信息");
            UpdateToDatabase(fdWarrantyMessage);
            fdWarrantyMessage.Status = (int)DcsBaseDataStatus.作废;
            var userInfo = Utils.GetCurrentUserInfo();
            fdWarrantyMessage.ModifierId = userInfo.Id;
            fdWarrantyMessage.ModifyTime = DateTime.Now;
            fdWarrantyMessage.ModifierName = userInfo.Name;
            this.UpdateFDWarrantyMessageValidate(fdWarrantyMessage);
        }

    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:09:50
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 作废福戴三包信息(FDWarrantyMessage fdWarrantyMessage) {
            new FdWarrantyMessageAch(this).作废福戴三包信息(fdWarrantyMessage);
        }
    }
}
