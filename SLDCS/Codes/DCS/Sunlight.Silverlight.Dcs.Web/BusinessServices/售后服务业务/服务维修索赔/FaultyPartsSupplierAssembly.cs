﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class FaultyPartsSupplierAssemblyAch : DcsSerivceAchieveBase {
        public FaultyPartsSupplierAssemblyAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 作废祸首件所属总成(FaultyPartsSupplierAssembly faultyPartsSupplierAssembly) {
            var dbFaultyPartsSupplierAssembly = ObjectContext.FaultyPartsSupplierAssemblies.Where(r => r.Id == faultyPartsSupplierAssembly.Id && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbFaultyPartsSupplierAssembly == null)
                throw new ValidationException("只能作废处于“有效”状态的祸首件所属总成");
            UpdateToDatabase(faultyPartsSupplierAssembly);
            faultyPartsSupplierAssembly.Status = (int)DcsBaseDataStatus.作废;
            var userInfo = Utils.GetCurrentUserInfo();
            faultyPartsSupplierAssembly.AbandonerId = userInfo.Id;
            faultyPartsSupplierAssembly.AbandonTime = DateTime.Now;
            faultyPartsSupplierAssembly.AbandonerName = userInfo.Name;
            this.UpdateFaultyPartsSupplierAssemblyValidate(faultyPartsSupplierAssembly);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:09:50
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 作废祸首件所属总成(FaultyPartsSupplierAssembly faultyPartsSupplierAssembly) {
            new FaultyPartsSupplierAssemblyAch(this).作废祸首件所属总成(faultyPartsSupplierAssembly);
        }
    }
}
