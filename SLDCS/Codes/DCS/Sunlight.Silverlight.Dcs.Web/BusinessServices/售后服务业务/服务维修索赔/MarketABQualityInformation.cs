﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Web;


namespace Sunlight.Silverlight.Dcs.Web {
    partial class MarketABQualityInformationAch : DcsSerivceAchieveBase {
        public MarketABQualityInformationAch(DcsDomainService domainService)
            : base(domainService) {
        }



        public void 初审市场AB质量信息快报(MarketABQualityInformation marketABQualityInformation, string comment) {
            var dbMarketABQualityInformation = ObjectContext.MarketABQualityInformations.Where(r => r.Status == (int)DcsABQualityInformationStatus.提交 && r.Id == marketABQualityInformation.Id).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbMarketABQualityInformation == null) {
                throw new ValidationException("只能审核状态为提交的单据");
            }

            marketABQualityInformation.Status = (int)DcsABQualityInformationStatus.初审通过;
            var userInfo = Utils.GetCurrentUserInfo();
            marketABQualityInformation.InitialApproverId = userInfo.Id;
            marketABQualityInformation.InitialApproverName = userInfo.Name;
            marketABQualityInformation.InitialApproverTime = DateTime.Now;
            marketABQualityInformation.ApproveCommentHistory = "审批人：" + userInfo.Name + Environment.NewLine + "审批时间:" + DateTime.Now + Environment.NewLine + "审批意见:" + comment + Environment.NewLine + Environment.NewLine;
            UpdateToDatabase(marketABQualityInformation);
            new MarketABQualityInformationAch(this.DomainService).PMS_SYC_1035_SendGradeABQualityInfoService(marketABQualityInformation);
        }


        public void 终审市场AB质量信息快报(MarketABQualityInformation marketABQualityInformation, string comment) {
            var statuses = new[] {
                (int)DcsABQualityInformationStatus.提交, (int)DcsABQualityInformationStatus.初审通过, (int)DcsABQualityInformationStatus.会签完成
            };
            var dbMarketABQualityInformation = ObjectContext.MarketABQualityInformations.Where(r => statuses.Contains(r.Status) && r.Id == marketABQualityInformation.Id).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbMarketABQualityInformation == null) {
                throw new ValidationException("只能审核状态为提交,初审通过,会签完成的单据");
            }
            marketABQualityInformation.Status = (int)DcsABQualityInformationStatus.终审通过;
            var userInfo = Utils.GetCurrentUserInfo();
            marketABQualityInformation.ApproverId = userInfo.Id;
            marketABQualityInformation.ApproverName = userInfo.Name;
            marketABQualityInformation.ApproveTime = DateTime.Now;
            marketABQualityInformation.ApproveCommentHistory = "审批人：" + userInfo.Name + Environment.NewLine + "审批时间:" + DateTime.Now + Environment.NewLine + "审批意见:" + comment + Environment.NewLine + Environment.NewLine;
            UpdateToDatabase(marketABQualityInformation);
        }


        public void 驳回市场AB质量信息快报(MarketABQualityInformation marketABQualityInformation, string comment) {
            var statuses = new[] {
                (int)DcsABQualityInformationStatus.提交, (int)DcsABQualityInformationStatus.初审通过, (int)DcsABQualityInformationStatus.会签完成
            };
            var dbMarketABQualityInformation = ObjectContext.MarketABQualityInformations.Where(r => statuses.Contains(r.Status) && r.Id == marketABQualityInformation.Id).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbMarketABQualityInformation == null) {
                throw new ValidationException("只能驳回状态为提交,初审通过,会签完成的单据");
            }
            marketABQualityInformation.Status = (int)DcsABQualityInformationStatus.新增;
            var userInfo = Utils.GetCurrentUserInfo();
            marketABQualityInformation.RejectId = userInfo.Id;
            marketABQualityInformation.RejectName = userInfo.Name;
            marketABQualityInformation.RejectTime = DateTime.Now;
            marketABQualityInformation.RejectReason = marketABQualityInformation.RejectReason + comment + Environment.NewLine;
            marketABQualityInformation.ApproveCommentHistory = "审批人：" + userInfo.Name + Environment.NewLine + "审批时间" + DateTime.Now + Environment.NewLine + "审批意见:" + comment + Environment.NewLine + Environment.NewLine;
            UpdateToDatabase(marketABQualityInformation);
        }


        public void 作废市场AB质量信息快报(MarketABQualityInformation marketABQualityInformation) {
            var statuses = new[] {
                (int)DcsABQualityInformationStatus.新增
            };
            var dbMarketABQualityInformation = ObjectContext.MarketABQualityInformations.Where(r => statuses.Contains(r.Status) && r.Id == marketABQualityInformation.Id).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbMarketABQualityInformation == null) {
                throw new ValidationException("只能操作状态为新增的单据");
            }
            marketABQualityInformation.Status = (int)DcsABQualityInformationStatus.作废;
            var userInfo = Utils.GetCurrentUserInfo();
            marketABQualityInformation.AbandonerId = userInfo.Id;
            marketABQualityInformation.AbandonerName = userInfo.Name;
            marketABQualityInformation.AbandonTime = DateTime.Now;
            UpdateToDatabase(marketABQualityInformation);
        }

        public void 提交市场AB质量信息快报(MarketABQualityInformation marketABQualityInformation) {
            var statuses = new[] {
                (int)DcsABQualityInformationStatus.新增
            };
            var dbMarketABQualityInformation = ObjectContext.MarketABQualityInformations.Where(r => statuses.Contains(r.Status) && r.Id == marketABQualityInformation.Id).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbMarketABQualityInformation == null) {
                throw new ValidationException("只能操作状态为新增的单据");
            }
            marketABQualityInformation.Status = (int)DcsABQualityInformationStatus.提交;
            UpdateToDatabase(marketABQualityInformation);

        }

        public void 初审不通过市场AB质量信息快报(MarketABQualityInformation marketABQualityInformation, string comment) {
            var dbMarketABQualityInformation = ObjectContext.MarketABQualityInformations.Where(r => r.Status == (int)DcsABQualityInformationStatus.提交 && r.Id == marketABQualityInformation.Id).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbMarketABQualityInformation == null) {
                throw new ValidationException("只能审核不通过 状态为提交的单据");
            }

            marketABQualityInformation.Status = (int)DcsABQualityInformationStatus.初审不通过;
            var userInfo = Utils.GetCurrentUserInfo();
            marketABQualityInformation.InitialApproverId = userInfo.Id;
            marketABQualityInformation.InitialApproverName = userInfo.Name;
            marketABQualityInformation.InitialApproverTime = DateTime.Now;
            marketABQualityInformation.ApproveCommentHistory = "审批人：" + userInfo.Name + Environment.NewLine + "审批时间:" + DateTime.Now + Environment.NewLine + "审批意见:" + comment + Environment.NewLine + Environment.NewLine;
            UpdateToDatabase(marketABQualityInformation);
        }

        public void 终审不通过市场AB质量信息快报(MarketABQualityInformation marketABQualityInformation, string comment) {
            var statuses = new[] {
                (int)DcsABQualityInformationStatus.提交, (int)DcsABQualityInformationStatus.初审通过, (int)DcsABQualityInformationStatus.会签完成
            };
            var dbMarketABQualityInformation = ObjectContext.MarketABQualityInformations.Where(r => statuses.Contains(r.Status) && r.Id == marketABQualityInformation.Id).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbMarketABQualityInformation == null) {
                throw new ValidationException("只能审核不通过 状态为提交,初审通过,会签完成的单据");
            }
            marketABQualityInformation.Status = (int)DcsABQualityInformationStatus.终审不通过;
            var userInfo = Utils.GetCurrentUserInfo();
            marketABQualityInformation.ApproverId = userInfo.Id;
            marketABQualityInformation.ApproverName = userInfo.Name;
            marketABQualityInformation.ApproveTime = DateTime.Now;
            marketABQualityInformation.ApproveCommentHistory = "审批人：" + userInfo.Name + Environment.NewLine + "审批时间:" + DateTime.Now + Environment.NewLine + "审批意见:" + comment + Environment.NewLine + Environment.NewLine;
            UpdateToDatabase(marketABQualityInformation);
        }

    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:09:50
        //原分布类的函数全部转移到Ach                                                               


        [Update(UsingCustomMethod = true)]
        public void 初审市场AB质量信息快报(MarketABQualityInformation marketABQualityInformation, string comment) {
            new MarketABQualityInformationAch(this).初审市场AB质量信息快报(marketABQualityInformation, comment);
        }


        [Update(UsingCustomMethod = true)]
        public void 终审市场AB质量信息快报(MarketABQualityInformation marketABQualityInformation, string comment) {
            new MarketABQualityInformationAch(this).终审市场AB质量信息快报(marketABQualityInformation, comment);
        }


        [Update(UsingCustomMethod = true)]
        public void 驳回市场AB质量信息快报(MarketABQualityInformation marketABQualityInformation, string comment) {
            new MarketABQualityInformationAch(this).驳回市场AB质量信息快报(marketABQualityInformation, comment);
        }


        [Update(UsingCustomMethod = true)]
        public void 作废市场AB质量信息快报(MarketABQualityInformation marketABQualityInformation) {
            new MarketABQualityInformationAch(this).作废市场AB质量信息快报(marketABQualityInformation);
        }

        public void 提交市场AB质量信息快报(MarketABQualityInformation marketABQualityInformation) {
            new MarketABQualityInformationAch(this).提交市场AB质量信息快报(marketABQualityInformation);
        }
        [Update(UsingCustomMethod = true)]
        public void 初审不通过市场AB质量信息快报(MarketABQualityInformation marketABQualityInformation, string comment) {
            new MarketABQualityInformationAch(this).初审不通过市场AB质量信息快报(marketABQualityInformation, comment);
        }
        [Update(UsingCustomMethod = true)]
        public void 终审不通过市场AB质量信息快报(MarketABQualityInformation marketABQualityInformation, string comment) {
            new MarketABQualityInformationAch(this).终审不通过市场AB质量信息快报(marketABQualityInformation, comment);
        }
    }

}
