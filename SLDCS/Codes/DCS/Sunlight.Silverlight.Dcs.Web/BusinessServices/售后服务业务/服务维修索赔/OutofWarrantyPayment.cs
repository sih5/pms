﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class OutofWarrantyPaymentAch : DcsSerivceAchieveBase {
        public OutofWarrantyPaymentAch(DcsDomainService domainService)
            : base(domainService) {
        }

        //
        //public void 生成保外扣补款单(OutofWarrantyPayment outofWarrantyPayment, bool flag) {//是否同时扣补配件管理费
        //    InsertToDatabase(outofWarrantyPayment);
        //    if(flag && outofWarrantyPayment.TransactionCategory == (int)DcsExpenseAdjustmentBillTransactionCategory.材料费) {
        //        //var companyInvoiceInfo = ObjectContext.CompanyInvoiceInfoes.Single(r => r.InvoiceCompanyId == outofWarrantyPayment.BranchId && r.CompanyId == outofWarrantyPayment.DealerId);
        //        var dealerServiceInfo = ObjectContext.DealerServiceInfoes.Single(r => r.BranchId == outofWarrantyPayment.BranchId && r.PartsSalesCategoryId == outofWarrantyPayment.PartsSalesCategoryId && r.DealerId == outofWarrantyPayment.DealerId);
        //        var partsManagementCostRates = ObjectContext.PartsManagementCostRates.Where(r => r.PartsManagementCostGradeId == dealerServiceInfo.PartsManagingFeeGradeId).ToList();
        //        var partsManagementCostRate = partsManagementCostRates.FirstOrDefault(r => r.PartsPriceUpperLimit >= outofWarrantyPayment.TransactionAmount && r.PartsPriceLowerLimit <= outofWarrantyPayment.TransactionAmount);
        //        if(partsManagementCostRate == null) {
        //            throw new ValidationException("不存在对应的配件管理费率标准,请检查!");
        //        }
        //        var newOutofWarrantyPayment = new OutofWarrantyPayment();
        //        newOutofWarrantyPayment.Status = outofWarrantyPayment.Status;
        //        newOutofWarrantyPayment.SettlementStatus = outofWarrantyPayment.SettlementStatus;
        //        newOutofWarrantyPayment.TransactionCategory = (int)DcsExpenseAdjustmentBillTransactionCategory.配件管理费;
        //        newOutofWarrantyPayment.BranchId = outofWarrantyPayment.BranchId;
        //        newOutofWarrantyPayment.DealerId = outofWarrantyPayment.DealerId;
        //        newOutofWarrantyPayment.DealerCode = outofWarrantyPayment.DealerCode;
        //        newOutofWarrantyPayment.DealerName = outofWarrantyPayment.DealerName;
        //        newOutofWarrantyPayment.SourceId = outofWarrantyPayment.SourceId;
        //        newOutofWarrantyPayment.SourceCode = outofWarrantyPayment.SourceCode;
        //        newOutofWarrantyPayment.DealerContactPerson = outofWarrantyPayment.DealerContactPerson;
        //        newOutofWarrantyPayment.DealerPhoneNumber = outofWarrantyPayment.DealerPhoneNumber;
        //        newOutofWarrantyPayment.TransactionAmount = outofWarrantyPayment.TransactionAmount * Convert.ToDecimal(partsManagementCostRate.Rate);
        //        newOutofWarrantyPayment.TransactionReason = outofWarrantyPayment.TransactionReason;
        //        newOutofWarrantyPayment.IfClaimToResponsible = outofWarrantyPayment.IfClaimToResponsible;
        //        newOutofWarrantyPayment.ResponsibleUnitId = outofWarrantyPayment.ResponsibleUnitId;
        //        newOutofWarrantyPayment.DebitOrReplenish = outofWarrantyPayment.DebitOrReplenish;
        //        newOutofWarrantyPayment.ServiceProductLineId = outofWarrantyPayment.ServiceProductLineId;
        //        newOutofWarrantyPayment.PartsSalesCategoryId = outofWarrantyPayment.PartsSalesCategoryId;
        //        newOutofWarrantyPayment.SourceType = outofWarrantyPayment.SourceType;
        //        newOutofWarrantyPayment.RepairClaimBillCode = outofWarrantyPayment.RepairClaimBillCode;
        //        newOutofWarrantyPayment.Remark = "根据材料费扣补款单自动生成";
        //        InsertOutofWarrantyPayment(newOutofWarrantyPayment);
        //    }
        //}

        public void 批量审批保外扣补款单(bool isPassThrough, int[] Ids) {
            var dboutofWarrantyPayments = ObjectContext.OutofWarrantyPayments.Where(r => Ids.Contains(r.Id)).ToArray();
            if(dboutofWarrantyPayments.Any(r => r.Status != (int)DcsExpenseAdjustmentBillStatus.新增))
                throw new ValidationException(ErrorStrings.ExpenseAdjustmentBill_Validation1);
            var userInfo = Utils.GetCurrentUserInfo();
            //if(isPassThrough) {
            //    foreach(var outofWarrantyPayment in dboutofWarrantyPayments) {
            //        UpdateToDatabase(outofWarrantyPayment);
            //        outofWarrantyPayment.ModifierId = userInfo.Id;
            //        outofWarrantyPayment.ModifierName = userInfo.Name;
            //        outofWarrantyPayment.ModifyTime = DateTime.Now;
            //        outofWarrantyPayment.ApproverId = userInfo.Id;
            //        outofWarrantyPayment.ApproverName = userInfo.Name;
            //        outofWarrantyPayment.ApproveTime = DateTime.Now;
            //        outofWarrantyPayment.Status = (int)DcsExpenseAdjustmentBillStatus.生效;
            //        var integralClaimBill = ObjectContext.IntegralClaimBills.Where(r => r.Id == outofWarrantyPayment.SourceId).FirstOrDefault();
            //        if(integralClaimBill != null) {
            //            if(outofWarrantyPayment.DebitOrReplenish == (int)DcsExpenseAdjustmentBillDebitOrReplenish.扣款)
            //                integralClaimBill.DebitAmount = (integralClaimBill.DebitAmount ?? 0) + outofWarrantyPayment.TransactionAmount;
            //            if(outofWarrantyPayment.DebitOrReplenish == (int)DcsExpenseAdjustmentBillDebitOrReplenish.补款)
            //                integralClaimBill.ComplementAmount = (integralClaimBill.ComplementAmount ?? 0) + outofWarrantyPayment.TransactionAmount;
            //            UpdateToDatabase(integralClaimBill);
            //        }
            //    }
            //}else {
            //    foreach(var outofWarrantyPayment in dboutofWarrantyPayments) {
            //        UpdateToDatabase(outofWarrantyPayment);
            //        outofWarrantyPayment.ModifierId = userInfo.Id;
            //        outofWarrantyPayment.ModifierName = userInfo.Name;
            //        outofWarrantyPayment.ModifyTime = DateTime.Now;
            //        outofWarrantyPayment.AbandonerId = userInfo.Id;
            //        outofWarrantyPayment.AbandonerName = userInfo.Name;
            //        outofWarrantyPayment.AbandonTime = DateTime.Now;
            //        outofWarrantyPayment.Status = (int)DcsExpenseAdjustmentBillStatus.作废;
            //    }
            //}
            ObjectContext.SaveChanges();
        }

        public void ApproveoutofWarrantyPayment(int outofWarrantyPaymentid) {
            var outofWarrantyPayment = ObjectContext.OutofWarrantyPayments.Where(r => r.Id == outofWarrantyPaymentid && r.Status == (int)DcsExpenseAdjustmentBillStatus.新增).FirstOrDefault();
            if(outofWarrantyPayment == null)
                throw new ValidationException(ErrorStrings.ExpenseAdjustmentBill_Validation1);
            var userInfo = Utils.GetCurrentUserInfo();
            UpdateToDatabase(outofWarrantyPayment);
            outofWarrantyPayment.ApproverId = userInfo.Id;
            outofWarrantyPayment.ApproverName = userInfo.Name;
            outofWarrantyPayment.ApproveTime = DateTime.Now;
            outofWarrantyPayment.Status = (int)DcsExpenseAdjustmentBillStatus.生效;
            this.UpdateOutofWarrantyPaymentValidate(outofWarrantyPayment);
            //var integralClaimBill = ObjectContext.IntegralClaimBills.Where(r => r.Id == outofWarrantyPayment.SourceId).FirstOrDefault();
            //if(integralClaimBill != null) {
            //    if(outofWarrantyPayment.DebitOrReplenish == (int)DcsExpenseAdjustmentBillDebitOrReplenish.扣款)
            //        integralClaimBill.DebitAmount = (integralClaimBill.DebitAmount ?? 0) + outofWarrantyPayment.TransactionAmount;
            //    if(outofWarrantyPayment.DebitOrReplenish == (int)DcsExpenseAdjustmentBillDebitOrReplenish.补款)
            //        integralClaimBill.ComplementAmount = (integralClaimBill.ComplementAmount ?? 0) + outofWarrantyPayment.TransactionAmount;
            //    UpdateToDatabase(integralClaimBill);
            //}
            ObjectContext.SaveChanges();
        }

        public void AbandonoutofWarrantyPayment(int outofWarrantyPaymentid) {
            var outofWarrantyPayment = ObjectContext.OutofWarrantyPayments.Where(r => r.Id == outofWarrantyPaymentid && r.Status == (int)DcsExpenseAdjustmentBillStatus.新增).FirstOrDefault();
            if(outofWarrantyPayment == null)
                throw new ValidationException(ErrorStrings.ExpenseAdjustmentBill_Validation2);
            var userInfo = Utils.GetCurrentUserInfo();
            UpdateToDatabase(outofWarrantyPayment);
            outofWarrantyPayment.AbandonerId = userInfo.Id;
            outofWarrantyPayment.AbandonerName = userInfo.Name;
            outofWarrantyPayment.AbandonTime = DateTime.Now;
            outofWarrantyPayment.Status = (int)DcsExpenseAdjustmentBillStatus.作废;
            this.UpdateOutofWarrantyPaymentValidate(outofWarrantyPayment);
            ObjectContext.SaveChanges();
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:09:50
        //原分布类的函数全部转移到Ach                                                               
        
        [Invoke(HasSideEffects = true)]
        public void 批量审批保外扣补款单(bool isPassThrough, int[] Ids) {
            new OutofWarrantyPaymentAch(this).批量审批保外扣补款单(isPassThrough,Ids);
        }
        
        [Invoke(HasSideEffects = true)]
        public void ApproveoutofWarrantyPayment(int outofWarrantyPaymentid) {
            new OutofWarrantyPaymentAch(this).ApproveoutofWarrantyPayment(outofWarrantyPaymentid);
        }
        
        [Invoke(HasSideEffects = true)]
        public void AbandonoutofWarrantyPayment(int outofWarrantyPaymentid) {
            new OutofWarrantyPaymentAch(this).AbandonoutofWarrantyPayment(outofWarrantyPaymentid);
        }
    }
}
