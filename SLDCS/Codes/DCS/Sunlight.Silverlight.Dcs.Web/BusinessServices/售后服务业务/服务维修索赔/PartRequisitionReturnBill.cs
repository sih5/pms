﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Web;using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartRequisitionReturnBill : System.ICloneable {
        public object Clone() {
            return this.MemberwiseClone();
        }
    }
    partial class PartRequisitionReturnBillAch : DcsSerivceAchieveBase {
        public PartRequisitionReturnBillAch(DcsDomainService domainService)
            : base(domainService) {
        }
        
        public object Clone() {
            return this.MemberwiseClone();
        }
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:09:50
        [Update(UsingCustomMethod = true)]
        public void 生成领料单(PartRequisitionReturnBill partRequisitionReturnBill) {
            var userInfo = Utils.GetCurrentUserInfo();
            //var dbRepairOrder = ObjectContext.RepairOrders.Where(r => r.Code == partRequisitionReturnBill.RepairCode && r.Status != (int)DcsRepairStatus.作废).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            //if(dbRepairOrder == null) {
            //    throw new ValidationException("未找对应维修单");
            //}
            //只有油才有拆散件
            //var partDeleaveInformations = ObjectContext.PartDeleaveInformations.SingleOrDefault(r => r.OldPartId == partRequisitionReturnBill.NewPartsId && r.PartsSalesCategoryId == partRequisitionReturnBill.PartsSalesCategoryId && r.BranchId == dbRepairOrder.BranchId && r.Status == (int)DcsBaseDataStatus.有效);
            var dealerPartsStocksQuery = ObjectContext.DealerPartsStocks.Where(r => this.ObjectContext.PartsSalesCategories.Any(x => x.Id == r.SalesCategoryId && !x.IsNotWarranty) && r.DealerId == partRequisitionReturnBill.DealerId && r.SparePartId == partRequisitionReturnBill.NewPartsId && r.SubDealerId == partRequisitionReturnBill.SubDealerId);
            var check = dealerPartsStocksQuery.GroupBy(r => new {
                r.SubDealer,
                r.SparePartId,
                r.SalesCategoryId
            }).Any(r => r.Count() > 1);
            if(check) {
                throw new ValidationException(ErrorStrings.PartRequisitionReturnBill_Validation1);
            }
            var remainDeductStock = partRequisitionReturnBill.Quantity;
            var thisSaleCategoryDealerPartsStock = dealerPartsStocksQuery.SingleOrDefault(r => r.SalesCategoryId == partRequisitionReturnBill.PartsSalesCategoryId);
            if(thisSaleCategoryDealerPartsStock != null && thisSaleCategoryDealerPartsStock.Quantity > 0) {
                if(thisSaleCategoryDealerPartsStock.Quantity > partRequisitionReturnBill.Quantity) {
                    remainDeductStock = 0;
                    thisSaleCategoryDealerPartsStock.Quantity = thisSaleCategoryDealerPartsStock.Quantity - partRequisitionReturnBill.Quantity;
                }else {
                    remainDeductStock = partRequisitionReturnBill.Quantity - thisSaleCategoryDealerPartsStock.Quantity;
                    partRequisitionReturnBill.Quantity = thisSaleCategoryDealerPartsStock.Quantity;
                    thisSaleCategoryDealerPartsStock.Quantity = 0;
                }
                UpdateToDatabase(thisSaleCategoryDealerPartsStock);
                thisSaleCategoryDealerPartsStock.ModifierId = userInfo.Id;
                thisSaleCategoryDealerPartsStock.ModifierName = userInfo.Name;
                thisSaleCategoryDealerPartsStock.ModifyTime = DateTime.Now;
                InsertPartRequisitionReturnBill(partRequisitionReturnBill);
            }

            if(remainDeductStock > 0) {
                var otherDealerPartsStocks = dealerPartsStocksQuery.Where(r => r.SalesCategoryId != partRequisitionReturnBill.PartsSalesCategoryId).OrderBy(r => r.Quantity).ToArray();
                for(var i = 0; i < otherDealerPartsStocks.Count(); i++) {
                    var currentDealerPartsStock = otherDealerPartsStocks[i];
                    var nowNeedDeductQuantity = currentDealerPartsStock.Quantity > remainDeductStock ? remainDeductStock : currentDealerPartsStock.Quantity;
                    remainDeductStock = remainDeductStock - nowNeedDeductQuantity;
                    currentDealerPartsStock.Quantity = currentDealerPartsStock.Quantity - nowNeedDeductQuantity;
                    UpdateToDatabase(currentDealerPartsStock);
                    currentDealerPartsStock.ModifierId = userInfo.Id;
                    currentDealerPartsStock.ModifierName = userInfo.Name;
                    currentDealerPartsStock.ModifyTime = DateTime.Now;
                    //创建一个新的领料,数量和品牌用当前的
                    var tempPartRequisitionReturnBill = new PartRequisitionReturnBill();
                    tempPartRequisitionReturnBill.RepairCode = partRequisitionReturnBill.RepairCode;
                    tempPartRequisitionReturnBill.RepairMaterialId = partRequisitionReturnBill.RepairMaterialId;
                    tempPartRequisitionReturnBill.RequisitionReturnStatus = partRequisitionReturnBill.RequisitionReturnStatus;
                    tempPartRequisitionReturnBill.DealerId = partRequisitionReturnBill.DealerId;
                    tempPartRequisitionReturnBill.UsedPartsId = partRequisitionReturnBill.UsedPartsId;
                    tempPartRequisitionReturnBill.UsedPartsCode = partRequisitionReturnBill.UsedPartsCode;
                    tempPartRequisitionReturnBill.UsedPartsName = partRequisitionReturnBill.UsedPartsName;
                    tempPartRequisitionReturnBill.PartsWarrantyCategoryId = partRequisitionReturnBill.PartsWarrantyCategoryId;
                    tempPartRequisitionReturnBill.UsedPartsSerialNumber = partRequisitionReturnBill.UsedPartsSerialNumber;
                    tempPartRequisitionReturnBill.UsedPartsBatchNumber = partRequisitionReturnBill.UsedPartsBatchNumber;
                    tempPartRequisitionReturnBill.UsedPartsBarCode = partRequisitionReturnBill.UsedPartsBarCode;
                    tempPartRequisitionReturnBill.UsedPartsSupplierId = partRequisitionReturnBill.UsedPartsSupplierId;
                    tempPartRequisitionReturnBill.UsedPartsSupplierCode = partRequisitionReturnBill.UsedPartsSupplierCode;
                    tempPartRequisitionReturnBill.UsedPartsSupplierName = partRequisitionReturnBill.UsedPartsSupplierName;
                    tempPartRequisitionReturnBill.NewPartsId = partRequisitionReturnBill.NewPartsId;
                    tempPartRequisitionReturnBill.NewPartsCode = partRequisitionReturnBill.NewPartsCode;
                    tempPartRequisitionReturnBill.NewPartsName = partRequisitionReturnBill.NewPartsName;
                    tempPartRequisitionReturnBill.NewPartsSerialNumber = partRequisitionReturnBill.NewPartsSerialNumber;
                    tempPartRequisitionReturnBill.NewPartsSupplierId = partRequisitionReturnBill.NewPartsSupplierId;
                    tempPartRequisitionReturnBill.NewPartsSupplierCode = partRequisitionReturnBill.NewPartsSupplierCode;
                    tempPartRequisitionReturnBill.NewPartsSupplierName = partRequisitionReturnBill.NewPartsSupplierName;
                    tempPartRequisitionReturnBill.NewPartsBatchNumber = partRequisitionReturnBill.NewPartsBatchNumber;
                    tempPartRequisitionReturnBill.NewPartsSecurityNumber = partRequisitionReturnBill.NewPartsSecurityNumber;
                    tempPartRequisitionReturnBill.SubDealerId = partRequisitionReturnBill.SubDealerId;
                    tempPartRequisitionReturnBill.PartsSalesCategoryId = currentDealerPartsStock.SalesCategoryId;
                    tempPartRequisitionReturnBill.Quantity = nowNeedDeductQuantity;
                    InsertToDatabase(tempPartRequisitionReturnBill);
                    new PartRequisitionReturnBillAch(this).InsertPartRequisitionReturnBillValidate(tempPartRequisitionReturnBill);
                    if(remainDeductStock <= 0) {
                        break;
                    }
                }
                if(remainDeductStock > 0) {
                    throw new ValidationException(string.Format(ErrorStrings.CompanyTransferOrder_Validation11, partRequisitionReturnBill.NewPartsCode));
                }
            }
        }
        [Update(UsingCustomMethod = true)]
        public void 生成领料单保外(PartRequisitionReturnBill partRequisitionReturnBill) {
            var userInfo = Utils.GetCurrentUserInfo();
            var salesCenterstrategies = this.ObjectContext.SalesCenterstrategies.Where(p => p.AllowUseInWarrantyPart == true);
            var salesCenterstrategyIds = salesCenterstrategies.Select(p => p.PartsSalesCategoryId).ToArray();
            //var dbRepairOrder = ObjectContext.RepairOrders.Where(r => r.Code == partRequisitionReturnBill.RepairCode && r.Status != (int)DcsRepairStatus.作废).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            //if(dbRepairOrder == null) {
            //    throw new ValidationException("未找对应维修单");
            //}
            //只有油才有拆散件
            //var partDeleaveInformations = ObjectContext.PartDeleaveInformations.SingleOrDefault(r => r.OldPartId == partRequisitionReturnBill.NewPartsId && r.PartsSalesCategoryId == partRequisitionReturnBill.PartsSalesCategoryId && r.BranchId == dbRepairOrder.BranchId && r.Status == (int)DcsBaseDataStatus.有效);
            var dealerPartsStocksQuery = ObjectContext.DealerPartsStocks.Where(r => salesCenterstrategyIds.Contains(r.SalesCategoryId) && r.DealerId == partRequisitionReturnBill.DealerId && r.SparePartId == partRequisitionReturnBill.NewPartsId && r.SubDealerId == partRequisitionReturnBill.SubDealerId);
            var check = dealerPartsStocksQuery.GroupBy(r => new {
                r.SubDealer,
                r.SparePartId,
                r.SalesCategoryId
            }).Any(r => r.Count() > 1);
            if(check) {
                throw new ValidationException(ErrorStrings.PartRequisitionReturnBill_Validation1);
            }
            var remainDeductStock = partRequisitionReturnBill.Quantity;
            var thisSaleCategoryDealerPartsStock = dealerPartsStocksQuery.SingleOrDefault(r => r.SalesCategoryId == partRequisitionReturnBill.PartsSalesCategoryId);
            if(thisSaleCategoryDealerPartsStock != null && thisSaleCategoryDealerPartsStock.Quantity > 0) {
                if(thisSaleCategoryDealerPartsStock.Quantity > partRequisitionReturnBill.Quantity) {
                    remainDeductStock = 0;
                    thisSaleCategoryDealerPartsStock.Quantity = thisSaleCategoryDealerPartsStock.Quantity - partRequisitionReturnBill.Quantity;
                }else {
                    remainDeductStock = partRequisitionReturnBill.Quantity - thisSaleCategoryDealerPartsStock.Quantity;
                    partRequisitionReturnBill.Quantity = thisSaleCategoryDealerPartsStock.Quantity;
                    thisSaleCategoryDealerPartsStock.Quantity = 0;
                }
                UpdateToDatabase(thisSaleCategoryDealerPartsStock);
                thisSaleCategoryDealerPartsStock.ModifierId = userInfo.Id;
                thisSaleCategoryDealerPartsStock.ModifierName = userInfo.Name;
                thisSaleCategoryDealerPartsStock.ModifyTime = DateTime.Now;
                InsertPartRequisitionReturnBill(partRequisitionReturnBill);
            }

            if(remainDeductStock > 0) {
                var otherDealerPartsStocks = dealerPartsStocksQuery.Where(r => r.SalesCategoryId != partRequisitionReturnBill.PartsSalesCategoryId).OrderBy(r => r.Quantity).ToArray();
                for(var i = 0; i < otherDealerPartsStocks.Count(); i++) {
                    var currentDealerPartsStock = otherDealerPartsStocks[i];
                    var nowNeedDeductQuantity = currentDealerPartsStock.Quantity > remainDeductStock ? remainDeductStock : currentDealerPartsStock.Quantity;
                    remainDeductStock = remainDeductStock - nowNeedDeductQuantity;
                    currentDealerPartsStock.Quantity = currentDealerPartsStock.Quantity - nowNeedDeductQuantity;
                    UpdateToDatabase(currentDealerPartsStock);
                    currentDealerPartsStock.ModifierId = userInfo.Id;
                    currentDealerPartsStock.ModifierName = userInfo.Name;
                    currentDealerPartsStock.ModifyTime = DateTime.Now;
                    //创建一个新的领料,数量和品牌用当前的
                    var tempPartRequisitionReturnBill = new PartRequisitionReturnBill();
                    tempPartRequisitionReturnBill.RepairCode = partRequisitionReturnBill.RepairCode;
                    tempPartRequisitionReturnBill.RepairMaterialId = partRequisitionReturnBill.RepairMaterialId;
                    tempPartRequisitionReturnBill.RequisitionReturnStatus = partRequisitionReturnBill.RequisitionReturnStatus;
                    tempPartRequisitionReturnBill.DealerId = partRequisitionReturnBill.DealerId;
                    tempPartRequisitionReturnBill.UsedPartsId = partRequisitionReturnBill.UsedPartsId;
                    tempPartRequisitionReturnBill.UsedPartsCode = partRequisitionReturnBill.UsedPartsCode;
                    tempPartRequisitionReturnBill.UsedPartsName = partRequisitionReturnBill.UsedPartsName;
                    tempPartRequisitionReturnBill.PartsWarrantyCategoryId = partRequisitionReturnBill.PartsWarrantyCategoryId;
                    tempPartRequisitionReturnBill.UsedPartsSerialNumber = partRequisitionReturnBill.UsedPartsSerialNumber;
                    tempPartRequisitionReturnBill.UsedPartsBatchNumber = partRequisitionReturnBill.UsedPartsBatchNumber;
                    tempPartRequisitionReturnBill.UsedPartsBarCode = partRequisitionReturnBill.UsedPartsBarCode;
                    tempPartRequisitionReturnBill.UsedPartsSupplierId = partRequisitionReturnBill.UsedPartsSupplierId;
                    tempPartRequisitionReturnBill.UsedPartsSupplierCode = partRequisitionReturnBill.UsedPartsSupplierCode;
                    tempPartRequisitionReturnBill.UsedPartsSupplierName = partRequisitionReturnBill.UsedPartsSupplierName;
                    tempPartRequisitionReturnBill.NewPartsId = partRequisitionReturnBill.NewPartsId;
                    tempPartRequisitionReturnBill.NewPartsCode = partRequisitionReturnBill.NewPartsCode;
                    tempPartRequisitionReturnBill.NewPartsName = partRequisitionReturnBill.NewPartsName;
                    tempPartRequisitionReturnBill.NewPartsSerialNumber = partRequisitionReturnBill.NewPartsSerialNumber;
                    tempPartRequisitionReturnBill.NewPartsSupplierId = partRequisitionReturnBill.NewPartsSupplierId;
                    tempPartRequisitionReturnBill.NewPartsSupplierCode = partRequisitionReturnBill.NewPartsSupplierCode;
                    tempPartRequisitionReturnBill.NewPartsSupplierName = partRequisitionReturnBill.NewPartsSupplierName;
                    tempPartRequisitionReturnBill.NewPartsBatchNumber = partRequisitionReturnBill.NewPartsBatchNumber;
                    tempPartRequisitionReturnBill.NewPartsSecurityNumber = partRequisitionReturnBill.NewPartsSecurityNumber;
                    tempPartRequisitionReturnBill.SubDealerId = partRequisitionReturnBill.SubDealerId;
                    tempPartRequisitionReturnBill.PartsSalesCategoryId = currentDealerPartsStock.SalesCategoryId;
                    tempPartRequisitionReturnBill.Quantity = nowNeedDeductQuantity;
                    InsertToDatabase(tempPartRequisitionReturnBill);
                    new PartRequisitionReturnBillAch(this).InsertPartRequisitionReturnBillValidate(tempPartRequisitionReturnBill);
                    if(remainDeductStock <= 0) {
                        break;
                    }
                }
                if(remainDeductStock > 0) {
                    throw new ValidationException(string.Format(ErrorStrings.CompanyTransferOrder_Validation11, partRequisitionReturnBill.NewPartsCode));
                }
            }
        }
        [Update(UsingCustomMethod = true)]
        public void 生成退料单(PartRequisitionReturnBill partRequisitionReturnBill) {
            var userInfo = Utils.GetCurrentUserInfo();
            InsertPartRequisitionReturnBill(partRequisitionReturnBill);
            //var dbRepairOrder = ObjectContext.RepairOrders.Where(r => r.Code == partRequisitionReturnBill.RepairCode && r.Status != (int)DcsRepairStatus.作废).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            //if(dbRepairOrder == null) {
            //    throw new ValidationException("未找对应维修单");
            //}
            var dealerPartsStock = ObjectContext.DealerPartsStocks.SingleOrDefault(r => this.ObjectContext.PartsSalesCategories.Any(x => x.Id == r.SalesCategoryId && !x.IsNotWarranty) && r.DealerId == partRequisitionReturnBill.DealerId && r.SalesCategoryId == partRequisitionReturnBill.PartsSalesCategoryId && r.SparePartId == partRequisitionReturnBill.NewPartsId && r.SubDealerId == partRequisitionReturnBill.SubDealerId);
            if(dealerPartsStock == null) {
                var dealer = ObjectContext.Dealers.FirstOrDefault(r => r.Id == partRequisitionReturnBill.DealerId);
                var partsSalesCategory = ObjectContext.PartsSalesCategories.FirstOrDefault(r => r.Id == partRequisitionReturnBill.PartsSalesCategoryId);
                var newDealerPartsStock = new DealerPartsStock() {
                    DealerId = partRequisitionReturnBill.DealerId,
                    DealerCode = dealer.Code,
                    DealerName = dealer.Name,
                    BranchId = partsSalesCategory.BranchId,
                    SalesCategoryId = partRequisitionReturnBill.PartsSalesCategoryId,
                    SalesCategoryName = partsSalesCategory.Name,
                    SparePartId = partRequisitionReturnBill.NewPartsId,
                    SparePartCode = partRequisitionReturnBill.NewPartsCode,
                    SubDealerId = partRequisitionReturnBill.SubDealerId,
                    Quantity = partRequisitionReturnBill.Quantity,
                };
                InsertToDatabase(newDealerPartsStock);
                newDealerPartsStock.CreatorId = userInfo.Id;
                newDealerPartsStock.CreatorName = userInfo.Name;
                newDealerPartsStock.CreateTime = DateTime.Now;
            }else {
                dealerPartsStock.Quantity = dealerPartsStock.Quantity + partRequisitionReturnBill.Quantity;
                UpdateToDatabase(dealerPartsStock);
                dealerPartsStock.ModifierId = userInfo.Id;
                dealerPartsStock.ModifierName = userInfo.Name;
                dealerPartsStock.ModifyTime = DateTime.Now;
            }
        }

        internal void 驳回修改维修单生成领料(PartRequisitionReturnBill partRequisitionReturnBill) {
            var userInfo = Utils.GetCurrentUserInfo();
            //var dbRepairOrder = ObjectContext.RepairOrders.Where(r => r.Code == partRequisitionReturnBill.RepairCode && r.Status != (int)DcsRepairStatus.作废).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            //if(dbRepairOrder == null) {
            //    throw new ValidationException("未找对应维修单");
            //}
            var dealerPartsStocksQuery = ObjectContext.DealerPartsStocks.Where(r => r.DealerId == partRequisitionReturnBill.DealerId && r.SparePartId == partRequisitionReturnBill.NewPartsId && r.SubDealerId == partRequisitionReturnBill.SubDealerId && this.ObjectContext.PartsSalesCategories.Any(x => x.Id == r.SalesCategoryId && !x.IsNotWarranty));
            var remainDeductStock = partRequisitionReturnBill.Quantity;
            var thisSaleCategoryDealerPartsStock = dealerPartsStocksQuery.FirstOrDefault(r => r.SalesCategoryId == partRequisitionReturnBill.PartsSalesCategoryId);
            if(thisSaleCategoryDealerPartsStock != null && thisSaleCategoryDealerPartsStock.Quantity > 0) {
                if(thisSaleCategoryDealerPartsStock.Quantity > partRequisitionReturnBill.Quantity) {
                    remainDeductStock = 0;
                    thisSaleCategoryDealerPartsStock.Quantity = thisSaleCategoryDealerPartsStock.Quantity - partRequisitionReturnBill.Quantity;
                }else {
                    remainDeductStock = partRequisitionReturnBill.Quantity - thisSaleCategoryDealerPartsStock.Quantity;
                    partRequisitionReturnBill.Quantity = thisSaleCategoryDealerPartsStock.Quantity;
                    thisSaleCategoryDealerPartsStock.Quantity = 0;
                }
                UpdateToDatabase(thisSaleCategoryDealerPartsStock);
                thisSaleCategoryDealerPartsStock.ModifierId = userInfo.Id;
                thisSaleCategoryDealerPartsStock.ModifierName = userInfo.Name;
                thisSaleCategoryDealerPartsStock.ModifyTime = DateTime.Now;
                InsertPartRequisitionReturnBill(partRequisitionReturnBill);
            }
            if(remainDeductStock > 0) {
                var otherDealerPartsStocks = dealerPartsStocksQuery.Where(r => r.SalesCategoryId != partRequisitionReturnBill.PartsSalesCategoryId).OrderBy(r => r.Quantity).ToArray();
                for(var i = 0; i < otherDealerPartsStocks.Count(); i++) {
                    var currentDealerPartsStock = otherDealerPartsStocks[i];
                    var nowNeedDeductQuantity = currentDealerPartsStock.Quantity > remainDeductStock ? remainDeductStock : currentDealerPartsStock.Quantity;
                    remainDeductStock = remainDeductStock - nowNeedDeductQuantity;
                    currentDealerPartsStock.Quantity = currentDealerPartsStock.Quantity - nowNeedDeductQuantity;
                    UpdateToDatabase(currentDealerPartsStock);
                    currentDealerPartsStock.ModifierId = userInfo.Id;
                    currentDealerPartsStock.ModifierName = userInfo.Name;
                    currentDealerPartsStock.ModifyTime = DateTime.Now;
                    //创建一个新的领料,数量和品牌用当前的
                    var tempPartRequisitionReturnBill = new PartRequisitionReturnBill();
                    tempPartRequisitionReturnBill.RepairCode = partRequisitionReturnBill.RepairCode;
                    tempPartRequisitionReturnBill.RepairMaterialId = partRequisitionReturnBill.RepairMaterialId;
                    tempPartRequisitionReturnBill.RequisitionReturnStatus = partRequisitionReturnBill.RequisitionReturnStatus;
                    tempPartRequisitionReturnBill.DealerId = partRequisitionReturnBill.DealerId;
                    tempPartRequisitionReturnBill.UsedPartsId = partRequisitionReturnBill.UsedPartsId;
                    tempPartRequisitionReturnBill.UsedPartsCode = partRequisitionReturnBill.UsedPartsCode;
                    tempPartRequisitionReturnBill.UsedPartsName = partRequisitionReturnBill.UsedPartsName;
                    tempPartRequisitionReturnBill.PartsWarrantyCategoryId = partRequisitionReturnBill.PartsWarrantyCategoryId;
                    tempPartRequisitionReturnBill.UsedPartsSerialNumber = partRequisitionReturnBill.UsedPartsSerialNumber;
                    tempPartRequisitionReturnBill.UsedPartsBatchNumber = partRequisitionReturnBill.UsedPartsBatchNumber;
                    tempPartRequisitionReturnBill.UsedPartsBarCode = partRequisitionReturnBill.UsedPartsBarCode;
                    tempPartRequisitionReturnBill.UsedPartsSupplierId = partRequisitionReturnBill.UsedPartsSupplierId;
                    tempPartRequisitionReturnBill.UsedPartsSupplierCode = partRequisitionReturnBill.UsedPartsSupplierCode;
                    tempPartRequisitionReturnBill.UsedPartsSupplierName = partRequisitionReturnBill.UsedPartsSupplierName;
                    tempPartRequisitionReturnBill.NewPartsId = partRequisitionReturnBill.NewPartsId;
                    tempPartRequisitionReturnBill.NewPartsCode = partRequisitionReturnBill.NewPartsCode;
                    tempPartRequisitionReturnBill.NewPartsName = partRequisitionReturnBill.NewPartsName;
                    tempPartRequisitionReturnBill.NewPartsSerialNumber = partRequisitionReturnBill.NewPartsSerialNumber;
                    tempPartRequisitionReturnBill.NewPartsSupplierId = partRequisitionReturnBill.NewPartsSupplierId;
                    tempPartRequisitionReturnBill.NewPartsSupplierCode = partRequisitionReturnBill.NewPartsSupplierCode;
                    tempPartRequisitionReturnBill.NewPartsSupplierName = partRequisitionReturnBill.NewPartsSupplierName;
                    tempPartRequisitionReturnBill.NewPartsBatchNumber = partRequisitionReturnBill.NewPartsBatchNumber;
                    tempPartRequisitionReturnBill.NewPartsSecurityNumber = partRequisitionReturnBill.NewPartsSecurityNumber;
                    tempPartRequisitionReturnBill.SubDealerId = partRequisitionReturnBill.SubDealerId;
                    tempPartRequisitionReturnBill.PartsSalesCategoryId = currentDealerPartsStock.SalesCategoryId;
                    tempPartRequisitionReturnBill.Quantity = nowNeedDeductQuantity;
                    InsertToDatabase(tempPartRequisitionReturnBill);
                    new PartRequisitionReturnBillAch(this).InsertPartRequisitionReturnBillValidate(tempPartRequisitionReturnBill);
                    if(remainDeductStock <= 0) {
                        break;
                    }
                }
                if(remainDeductStock > 0) {
                    throw new ValidationException(string.Format(ErrorStrings.CompanyTransferOrder_Validation11, partRequisitionReturnBill.NewPartsCode));
                }
            }
        }


    }
}
