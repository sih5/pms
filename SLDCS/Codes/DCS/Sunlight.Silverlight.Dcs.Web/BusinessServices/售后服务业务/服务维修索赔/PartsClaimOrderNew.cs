﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsClaimOrderNewAch : DcsSerivceAchieveBase {
        public PartsClaimOrderNewAch(DcsDomainService domainService)
            : base(domainService) {
        }

        public void 修改配件索赔单新(PartsClaimOrderNew partsClaimOrderNew) {
            UpdatePartsClaimOrderNew(partsClaimOrderNew);

            var rightStatusArray = new int[] { (int)DcsPartsClaimBillStatusNew.新增 };
            var dbpartsClaimOrder = ObjectContext.PartsClaimOrderNews.Where(r => r.Id == partsClaimOrderNew.Id && rightStatusArray.Contains(r.Status)).SingleOrDefault();
            if(dbpartsClaimOrder == null)
                throw new ValidationException(ErrorStrings.PartsClaimOrderNew_Validation1);
            var userInfo = Utils.GetCurrentUserInfo();
            partsClaimOrderNew.ModifierId = userInfo.Id;
            partsClaimOrderNew.ModifierName = userInfo.Name;
            partsClaimOrderNew.ModifyTime = DateTime.Now;

            UpdateToDatabase(partsClaimOrderNew);
        }

        public void 提交配件索赔单新(PartsClaimOrderNew partsClaimOrderNew) {
            UpdatePartsClaimOrderNew(partsClaimOrderNew);

            var rightStatusArray = new int[] { (int)DcsPartsClaimBillStatusNew.新增 };
            var dbpartsClaimOrder = ObjectContext.PartsClaimOrderNews.Where(r => r.Id == partsClaimOrderNew.Id && rightStatusArray.Contains(r.Status)).SingleOrDefault();
            if(dbpartsClaimOrder == null)
                throw new ValidationException(ErrorStrings.PartsClaimOrderNew_Validation1);

            var userInfo = Utils.GetCurrentUserInfo();
            var company = ObjectContext.Companies.Where(r => r.Id == userInfo.EnterpriseId).FirstOrDefault();
            if(company.Type == (int)DcsCompanyType.代理库) {
                partsClaimOrderNew.Status = (int)DcsPartsClaimBillStatusNew.待配件公司审核;
            } else if(company.Type == (int)DcsCompanyType.服务站 || company.Type == (int)DcsCompanyType.服务站兼代理库) {
                if(partsClaimOrderNew.RWarehouseCompanyType == (int)DcsCompanyType.分公司) {
                    partsClaimOrderNew.Status = (int)DcsPartsClaimBillStatusNew.待配件公司审核;
                } else if(partsClaimOrderNew.RWarehouseCompanyType == (int)DcsCompanyType.代理库) {
                    partsClaimOrderNew.Status = (int)DcsPartsClaimBillStatusNew.待中心库审核;
                }
            }

            partsClaimOrderNew.ModifierId = userInfo.Id;
            partsClaimOrderNew.ModifierName = userInfo.Name;
            partsClaimOrderNew.ModifyTime = DateTime.Now;
            partsClaimOrderNew.SubmitterId = userInfo.Id;

            UpdateToDatabase(partsClaimOrderNew);
        }

        public void 作废配件索赔单新(PartsClaimOrderNew partsClaimOrderNew) {
            UpdatePartsClaimOrderNew(partsClaimOrderNew);
            var rightStatusArray = new int[] { (int)DcsPartsClaimBillStatusNew.新增 };
            var dbpartsClaimOrder = ObjectContext.PartsClaimOrderNews.Where(r => r.Id == partsClaimOrderNew.Id && rightStatusArray.Contains(r.Status)).SingleOrDefault();
            if(dbpartsClaimOrder == null)
                throw new ValidationException(ErrorStrings.PartsClaimOrderNew_Validation1);
            var userInfo = Utils.GetCurrentUserInfo();
            partsClaimOrderNew.Status = (int)DcsPartsClaimBillStatusNew.作废;
            partsClaimOrderNew.ModifierId = userInfo.Id;
            partsClaimOrderNew.ModifierName = userInfo.Name;
            partsClaimOrderNew.ModifyTime = DateTime.Now;
            partsClaimOrderNew.AbandonerId = userInfo.Id;
            partsClaimOrderNew.AbandonerName = userInfo.Name;
            partsClaimOrderNew.AbandonTime = DateTime.Now;
            UpdateToDatabase(partsClaimOrderNew);
        }
        public void 驳回配件索赔单新(PartsClaimOrderNew partsClaimOrderNew, string rejectReason) {
            UpdatePartsClaimOrderNew(partsClaimOrderNew);
            var rightStatusArray = new int[] { (int)DcsPartsClaimBillStatusNew.待配件公司审核, (int)DcsPartsClaimBillStatusNew.待中心库审核, (int)DcsPartsClaimBillStatusNew.初审通过, (int)DcsPartsClaimBillStatusNew.审核通过, (int)DcsPartsClaimBillStatusNew.中心库审核通过 };
            var dbpartsClaimOrder = ObjectContext.PartsClaimOrderNews.Where(r => r.Id == partsClaimOrderNew.Id && rightStatusArray.Contains(r.Status)).SingleOrDefault();
            if(dbpartsClaimOrder == null)
                throw new ValidationException(ErrorStrings.PartsClaimOrderNew_Validation1);
            var userInfo = Utils.GetCurrentUserInfo();
            partsClaimOrderNew.Status = (int)DcsPartsClaimBillStatusNew.新增;
            partsClaimOrderNew.ModifierId = userInfo.Id;
            partsClaimOrderNew.ModifierName = userInfo.Name;
            partsClaimOrderNew.ModifyTime = DateTime.Now;
            partsClaimOrderNew.RejectId = userInfo.Id;
            partsClaimOrderNew.RejectName = userInfo.Name;
            partsClaimOrderNew.RejectTime = DateTime.Now;
            partsClaimOrderNew.RejectReason = rejectReason;
            var tempQty = partsClaimOrderNew.RejectQty == null ? 0 : partsClaimOrderNew.RejectQty.Value;
            tempQty++;
            partsClaimOrderNew.RejectQty = tempQty;

            UpdateToDatabase(partsClaimOrderNew);
        }
        public void 终止配件索赔单新(PartsClaimOrderNew partsClaimOrderNew) {
            UpdatePartsClaimOrderNew(partsClaimOrderNew);
            partsClaimOrderNew.Status = (int)DcsPartsClaimBillStatusNew.终止;
            var userInfo = Utils.GetCurrentUserInfo();
            partsClaimOrderNew.ModifierId = userInfo.Id;
            partsClaimOrderNew.ModifierName = userInfo.Name;
            partsClaimOrderNew.ModifyTime = DateTime.Now;
            partsClaimOrderNew.CancelId = userInfo.Id;
            partsClaimOrderNew.CancelName = userInfo.Name;
            partsClaimOrderNew.CancelTime = DateTime.Now;

            UpdateToDatabase(partsClaimOrderNew);
        }
        public void 审核配件索赔单新(PartsClaimOrderNew partsClaimOrderNew) {
            UpdatePartsClaimOrderNew(partsClaimOrderNew);
            var rightStatusArray = new int[] { (int)DcsPartsClaimBillStatusNew.待中心库审核 };
            var dbpartsClaimOrder = ObjectContext.PartsClaimOrderNews.Where(r => r.Id == partsClaimOrderNew.Id && rightStatusArray.Contains(r.Status)).SingleOrDefault();
            if(dbpartsClaimOrder == null)
                throw new ValidationException(ErrorStrings.PartsClaimOrderNew_Validation1);
            var userInfo = Utils.GetCurrentUserInfo();
            if (partsClaimOrderNew.ApprovalId == 1) {
                //校验索赔件的次数是否超过中心库选择的销售订单审核数量。
                var dbPartsClaimOrderNews = ObjectContext.PartsClaimOrderNews.Where(r => r.FaultyPartsId == partsClaimOrderNew.FaultyPartsId && r.RWarehouseCompanyId == userInfo.EnterpriseId
                    && r.Status != (int)DcsPartsClaimBillStatusNew.终止 && r.Status != (int)DcsPartsClaimBillStatusNew.作废 && r.CenterPartsSalesOrderId == partsClaimOrderNew.CenterPartsSalesOrderId);

                var partsSalesOrderDetails = ObjectContext.PartsSalesOrderDetails.Where(r => r.PartsSalesOrderId == partsClaimOrderNew.CenterPartsSalesOrderId && r.SparePartId == partsClaimOrderNew.FaultyPartsId);

                var dbsum = dbPartsClaimOrderNews.Count();
                var approveQuantity = partsSalesOrderDetails.Select(r => r.ApproveQuantity).Sum();

                if (approveQuantity == null || approveQuantity == 0 || dbsum + 1 > approveQuantity) {
                    throw new ValidationException(ErrorStrings.PartsClaimOrderNew_Validation2);
                }
                partsClaimOrderNew.Status = (int)DcsPartsClaimBillStatusNew.中心库审核通过;
            } else {
                partsClaimOrderNew.Status = (int)DcsPartsClaimBillStatusNew.新增;
                partsClaimOrderNew.RejectId = userInfo.Id;
                partsClaimOrderNew.RejectName = userInfo.Name;
                partsClaimOrderNew.RejectTime = DateTime.Now;
                partsClaimOrderNew.RejectReason = partsClaimOrderNew.ApproveComment;
                var tempQty = partsClaimOrderNew.RejectQty == null ? 0 : partsClaimOrderNew.RejectQty.Value;
                tempQty++;
                partsClaimOrderNew.RejectQty = tempQty;
            }

            partsClaimOrderNew.ModifierId = userInfo.Id;
            partsClaimOrderNew.ModifierName = userInfo.Name;
            partsClaimOrderNew.ModifyTime = DateTime.Now;
            partsClaimOrderNew.ApproverId = userInfo.Id;
            partsClaimOrderNew.ApproverName = userInfo.Name;
            partsClaimOrderNew.ApproveTime = DateTime.Now;
            var tempComment = ErrorStrings.PartsClaimOrderNew_Text_Approver + userInfo.Name + Environment.NewLine + ErrorStrings.PartsClaimOrderNew_Text_ApproveTime + DateTime.Now + Environment.NewLine + ErrorStrings.PartsClaimOrderNew_Text_ApproveComment + partsClaimOrderNew.ApproveComment + Environment.NewLine;
            partsClaimOrderNew.ApproveCommentHistory = partsClaimOrderNew.ApproveCommentHistory + Environment.NewLine + Environment.NewLine + tempComment;

            UpdateToDatabase(partsClaimOrderNew);
        }

        public void 总部审核配件索赔单新(PartsClaimOrderNew partsClaimOrderNew) {
            UpdatePartsClaimOrderNew(partsClaimOrderNew);

            var rightStatusArray = new int[] { (int)DcsPartsClaimBillStatusNew.待配件公司审核, (int)DcsPartsClaimBillStatusNew.中心库审核通过 };
            var dbpartsClaimOrder = ObjectContext.PartsClaimOrderNews.Where(r => r.Id == partsClaimOrderNew.Id && rightStatusArray.Contains(r.Status)).SingleOrDefault();
            if(dbpartsClaimOrder == null)
                throw new ValidationException(ErrorStrings.PartsClaimOrderNew_Validation1);

            var returnCompany = ObjectContext.Companies.Where(r => r.Id == partsClaimOrderNew.ReturnCompanyId).FirstOrDefault();
            var userInfo = Utils.GetCurrentUserInfo();
            if((partsClaimOrderNew.ApprovalId ?? 1) == 1) {
                var claimApproverStrategies = ObjectContext.ClaimApproverStrategies.Where(r => r.PartsSalesCategoryId == partsClaimOrderNew.PartsSalesCategoryId && r.Branchid == userInfo.EnterpriseId);
                //如果供货单位是中心库或者提报单位是中心库，材料费 = 索赔单.材料费2
                if(partsClaimOrderNew.RWarehouseCompanyType == (int)DcsCompanyType.代理库 || returnCompany.Type == (int)DcsCompanyType.代理库) {
                    var strategies = claimApproverStrategies.Where(r => partsClaimOrderNew.MaterialManagementCost > r.FeeFrom && partsClaimOrderNew.MaterialManagementCost <= r.FeeTo);
                    if(strategies == null || strategies.Count() == 0 || strategies.Count() > 1) {
                        throw new ValidationException(ErrorStrings.PartsClaimOrderNew_Validation3);
                    }
                    var strategie = strategies.FirstOrDefault();
                    partsClaimOrderNew.Status = (int)strategie.AfterApproverStatus;
                    //如果提报单位是中心库，并且审核后状态是终审通过，则将状态改为 区域已收货
                    if(returnCompany.Type == (int)DcsCompanyType.代理库 && partsClaimOrderNew.Status == (int)DcsPartsClaimBillStatusNew.终审通过) {
                        partsClaimOrderNew.Status = (int)DcsPartsClaimBillStatusNew.区域已收货;
                    }
                }
                    //如果供货单位是分公司，并且提报单位不是中心库，材料费 = 索赔单.材料费
                else if(partsClaimOrderNew.RWarehouseCompanyType == (int)DcsCompanyType.分公司 && returnCompany.Type != (int)DcsCompanyType.代理库) {
                    var strategies1 = claimApproverStrategies.Where(r => partsClaimOrderNew.MaterialCost > r.FeeFrom && partsClaimOrderNew.MaterialCost <= r.FeeTo);
                    if(strategies1 == null || strategies1.Count() == 0 || strategies1.Count() > 1) {
                        throw new ValidationException(ErrorStrings.PartsClaimOrderNew_Validation3);
                    }
                    var strategie1 = strategies1.FirstOrDefault();
                    partsClaimOrderNew.Status = (int)strategie1.AfterApproverStatus;
                }

            } else {
                partsClaimOrderNew.Status = (int)DcsPartsClaimBillStatusNew.新增;
                partsClaimOrderNew.RejectReason = partsClaimOrderNew.ApproveComment;
                partsClaimOrderNew.RejectId = userInfo.Id;
                partsClaimOrderNew.RejectName = userInfo.Name;
                partsClaimOrderNew.RejectTime = DateTime.Now;
                var tempQty = partsClaimOrderNew.RejectQty == null ? 0 : partsClaimOrderNew.RejectQty.Value;
                tempQty++;
                partsClaimOrderNew.RejectQty = tempQty;
            }

            partsClaimOrderNew.ModifierId = userInfo.Id;
            partsClaimOrderNew.ModifierName = userInfo.Name;
            partsClaimOrderNew.ModifyTime = DateTime.Now;
            partsClaimOrderNew.ApproverId = userInfo.Id;
            partsClaimOrderNew.ApproverName = userInfo.Name;
            partsClaimOrderNew.ApproveTime = DateTime.Now;
            var tempComment = ErrorStrings.PartsClaimOrderNew_Text_Approver + userInfo.Name + Environment.NewLine + ErrorStrings.PartsClaimOrderNew_Text_ApproveTime + DateTime.Now + Environment.NewLine + ErrorStrings.PartsClaimOrderNew_Text_ApproveComment + partsClaimOrderNew.ApproveComment + Environment.NewLine;
            partsClaimOrderNew.ApproveCommentHistory = partsClaimOrderNew.ApproveCommentHistory + Environment.NewLine + Environment.NewLine + tempComment;

            UpdateToDatabase(partsClaimOrderNew);
        }

        public void 总部初审配件索赔单新(PartsClaimOrderNew partsClaimOrderNew) {
            UpdatePartsClaimOrderNew(partsClaimOrderNew);
            var rightStatusArray = new int[] { (int)DcsPartsClaimBillStatusNew.审核通过 };
            var dbpartsClaimOrder = ObjectContext.PartsClaimOrderNews.Where(r => r.Id == partsClaimOrderNew.Id && rightStatusArray.Contains(r.Status)).SingleOrDefault();
            if(dbpartsClaimOrder == null)
                throw new ValidationException(ErrorStrings.PartsClaimOrderNew_Validation1);

            var userInfo = Utils.GetCurrentUserInfo();
            var returnCompany = ObjectContext.Companies.Where(r => r.Id == partsClaimOrderNew.ReturnCompanyId).FirstOrDefault();

            if((partsClaimOrderNew.ApprovalId ?? 1) == 1) {
                var claimApproverStrategies = ObjectContext.ClaimApproverStrategies.Where(r => r.PartsSalesCategoryId == partsClaimOrderNew.PartsSalesCategoryId && r.Branchid == userInfo.EnterpriseId);
                //如果供货单位是中心库或者提报单位是中心库，材料费 = 索赔单.材料费2
                if(partsClaimOrderNew.RWarehouseCompanyType == (int)DcsCompanyType.代理库 || returnCompany.Type == (int)DcsCompanyType.代理库) {
                    var strategies = claimApproverStrategies.Where(r => partsClaimOrderNew.MaterialManagementCost > r.FeeFrom && partsClaimOrderNew.MaterialManagementCost <= r.FeeTo);
                    if(strategies == null || strategies.Count() == 0 || strategies.Count() > 1) {
                        throw new ValidationException(ErrorStrings.PartsClaimOrderNew_Validation3);
                    }
                    var strategie = strategies.FirstOrDefault();
                    partsClaimOrderNew.Status = (int)strategie.InitialApproverStatus;
                    //如果提报单位是中心库，并且初审后状态是终审通过，则将状态改为 区域已收货
                    if(returnCompany.Type == (int)DcsCompanyType.代理库 && partsClaimOrderNew.Status == (int)DcsPartsClaimBillStatusNew.终审通过) {
                        partsClaimOrderNew.Status = (int)DcsPartsClaimBillStatusNew.区域已收货;
                    }
                }
                    //如果供货单位是分公司，并且提报单位不是中心库，材料费 = 索赔单.材料费
                else if(partsClaimOrderNew.RWarehouseCompanyType == (int)DcsCompanyType.分公司 && returnCompany.Type != (int)DcsCompanyType.代理库) {
                    var strategies1 = claimApproverStrategies.Where(r => partsClaimOrderNew.MaterialCost > r.FeeFrom && partsClaimOrderNew.MaterialCost <= r.FeeTo);
                    if(strategies1 == null || strategies1.Count() == 0 || strategies1.Count() > 1) {
                        throw new ValidationException(ErrorStrings.PartsClaimOrderNew_Validation3);
                    }
                    var strategie1 = strategies1.FirstOrDefault();
                    partsClaimOrderNew.Status = (int)strategie1.InitialApproverStatus;
                }

            } else {
                partsClaimOrderNew.Status = (int)DcsPartsClaimBillStatusNew.新增;
                partsClaimOrderNew.RejectReason = partsClaimOrderNew.ApproveComment;
                partsClaimOrderNew.RejectId = userInfo.Id;
                partsClaimOrderNew.RejectName = userInfo.Name;
                partsClaimOrderNew.RejectTime = DateTime.Now;
                var tempQty = partsClaimOrderNew.RejectQty == null ? 0 : partsClaimOrderNew.RejectQty.Value;
                tempQty++;
                partsClaimOrderNew.RejectQty = tempQty;
            }

            partsClaimOrderNew.ModifierId = userInfo.Id;
            partsClaimOrderNew.ModifierName = userInfo.Name;
            partsClaimOrderNew.ModifyTime = DateTime.Now;
            partsClaimOrderNew.ApproverId = userInfo.Id;
            partsClaimOrderNew.ApproverName = userInfo.Name;
            partsClaimOrderNew.ApproveTime = DateTime.Now;
            var tempComment = ErrorStrings.PartsClaimOrderNew_Text_Approver + userInfo.Name + Environment.NewLine + ErrorStrings.PartsClaimOrderNew_Text_ApproveTime + DateTime.Now + Environment.NewLine + ErrorStrings.PartsClaimOrderNew_Text_ApproveComment + partsClaimOrderNew.ApproveComment + Environment.NewLine;
            partsClaimOrderNew.ApproveCommentHistory = partsClaimOrderNew.ApproveCommentHistory + Environment.NewLine + Environment.NewLine + tempComment;

            UpdateToDatabase(partsClaimOrderNew);
        }


        public void 总部终审配件索赔单新(PartsClaimOrderNew partsClaimOrderNew) {
            UpdatePartsClaimOrderNew(partsClaimOrderNew);
            var rightStatusArray = new int[] { (int)DcsPartsClaimBillStatusNew.初审通过 };
            var dbpartsClaimOrder = ObjectContext.PartsClaimOrderNews.Where(r => r.Id == partsClaimOrderNew.Id && rightStatusArray.Contains(r.Status)).SingleOrDefault();
            if(dbpartsClaimOrder == null)
                throw new ValidationException(ErrorStrings.PartsClaimOrderNew_Validation1);

            var userInfo = Utils.GetCurrentUserInfo();
            if((partsClaimOrderNew.ApprovalId ?? 1) == 1) {
                var returnCompany = ObjectContext.Companies.Where(r => r.Id == partsClaimOrderNew.ReturnCompanyId).FirstOrDefault();
                //如果中心库提报的索赔单，终审通过后状态为 区域已入库
                if(returnCompany.Type == (int)DcsCompanyType.代理库) {
                    partsClaimOrderNew.Status = (int)DcsPartsClaimBillStatusNew.区域已收货;
                } else {
                    partsClaimOrderNew.Status = (int)DcsPartsClaimBillStatusNew.终审通过;
                }

            } else {
                partsClaimOrderNew.Status = (int)DcsPartsClaimBillStatusNew.新增;
                partsClaimOrderNew.RejectReason = partsClaimOrderNew.ApproveComment;
                partsClaimOrderNew.RejectId = userInfo.Id;
                partsClaimOrderNew.RejectName = userInfo.Name;
                partsClaimOrderNew.RejectTime = DateTime.Now;
                var tempQty = partsClaimOrderNew.RejectQty == null ? 0 : partsClaimOrderNew.RejectQty.Value;
                tempQty++;
                partsClaimOrderNew.RejectQty = tempQty;
            }
            partsClaimOrderNew.ModifierId = userInfo.Id;
            partsClaimOrderNew.ModifierName = userInfo.Name;
            partsClaimOrderNew.ModifyTime = DateTime.Now;
            partsClaimOrderNew.ApproverId = userInfo.Id;
            partsClaimOrderNew.ApproverName = userInfo.Name;
            partsClaimOrderNew.ApproveTime = DateTime.Now;
            var tempComment =ErrorStrings.PartsClaimOrderNew_Text_Approver + userInfo.Name + Environment.NewLine + ErrorStrings.PartsClaimOrderNew_Text_ApproveTime + DateTime.Now + Environment.NewLine + ErrorStrings.PartsClaimOrderNew_Text_ApproveComment + partsClaimOrderNew.ApproveComment + Environment.NewLine;
            partsClaimOrderNew.ApproveCommentHistory = partsClaimOrderNew.ApproveCommentHistory + Environment.NewLine + Environment.NewLine + tempComment;

            UpdateToDatabase(partsClaimOrderNew);
        }


        public void 出库配件索赔单新(PartsClaimOrderNew partsClaimOrderNew) {
            UpdatePartsClaimOrderNew(partsClaimOrderNew);//为了防止客户端直接提交 导致不走数据服务,加这一句
            #region 各种校验
            var rightStatusArray = new int[] { (int)DcsPartsClaimBillStatusNew.服务站已发运 };
            var dbpartsClaimOrder = ObjectContext.PartsClaimOrderNews.Where(r => r.Id == partsClaimOrderNew.Id && rightStatusArray.Contains(r.Status)).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbpartsClaimOrder == null) {
                throw new ValidationException("操作失败,单据状态不正确或已被修改");
            }
            var returnCompany = ObjectContext.Companies.SingleOrDefault(r => r.Id == partsClaimOrderNew.ReturnCompanyId && r.Status == (int)DcsMasterDataStatus.有效);
            if(returnCompany == null) {
                throw new ValidationException("操作失败,退货企业不存在");
            }
            var companyOfBranch = ObjectContext.Companies.SingleOrDefault(r => r.Id == partsClaimOrderNew.Branchid && r.Status == (int)DcsMasterDataStatus.有效 && r.Type == (int)DcsCompanyType.分公司);
            if(companyOfBranch == null) {
                throw new ValidationException("分公司在企业信息不存在");
            }
            var returnWarehouse = ObjectContext.Warehouses.SingleOrDefault(r => r.Id == partsClaimOrderNew.ReturnWarehouseId && r.Status == (int)DcsMasterDataStatus.有效);
            if(returnWarehouse == null) {
                throw new ValidationException("退货仓库不存在");
            }
            var storageCompanyOfReturnWarehouse = ObjectContext.Companies.SingleOrDefault(r => r.Id == partsClaimOrderNew.RWarehouseCompanyId && r.Status == (int)DcsMasterDataStatus.有效);
            if(storageCompanyOfReturnWarehouse == null) {
                throw new ValidationException("退货仓库的仓储企业不存在");
            }
            partsClaimOrderNew.Status = (int)DcsPartsClaimBillStatusNew.区域已发运;
            UpdateToDatabase(partsClaimOrderNew);//以防万一被坑写上也没错
            if(returnCompany.Type == (int)DcsCompanyType.代理库) {
                if(partsClaimOrderNew.AgencyOutWarehouseId == null) {
                    throw new ValidationException("代理库出库仓库为空");
                }
                var agencyOutWarehouse = ObjectContext.Warehouses.SingleOrDefault(r => r.Id == partsClaimOrderNew.AgencyOutWarehouseId && r.Status == (int)DcsMasterDataStatus.有效);
                if(agencyOutWarehouse == null) {
                    throw new ValidationException("代理库出库仓库不存在");
                }
                var storageCompanyOfAgencyOutWarehouse = ObjectContext.Companies.SingleOrDefault(r => r.Id == agencyOutWarehouse.StorageCompanyId && r.Status == (int)DcsMasterDataStatus.有效);
                if(storageCompanyOfAgencyOutWarehouse == null) {
                    throw new ValidationException("代理库出库仓库的仓储企业不存在");
                }
                if(storageCompanyOfAgencyOutWarehouse.Type != agencyOutWarehouse.StorageCompanyType) {
                    throw new ValidationException("代理库出库仓库的仓储企业类型与企业信息中不一致");
                }
                var partsOutboundPlan = CreatePartsOutboundPlanByPartsClaimOrderNewForAgency(partsClaimOrderNew, agencyOutWarehouse, storageCompanyOfAgencyOutWarehouse, companyOfBranch, returnWarehouse, storageCompanyOfReturnWarehouse);
                InsertToDatabase(partsOutboundPlan);//以防万一被坑写上也没错
                DomainService.生成配件出库计划(partsOutboundPlan);
            } else {
                if(partsClaimOrderNew.CDCReturnWarehouseId == null) {
                    throw new ValidationException("CDC出库仓库为空");
                }
                var cDCReturnWarehouse = ObjectContext.Warehouses.SingleOrDefault(r => r.Id == partsClaimOrderNew.CDCReturnWarehouseId && r.Status == (int)DcsMasterDataStatus.有效);
                if(cDCReturnWarehouse == null) {
                    throw new ValidationException("CDC出库仓库不存在");
                }
                var storageCompanyOfCDCReturnWarehouse = ObjectContext.Companies.SingleOrDefault(r => r.Id == cDCReturnWarehouse.StorageCompanyId && r.Status == (int)DcsMasterDataStatus.有效);
                if(storageCompanyOfCDCReturnWarehouse == null) {
                    throw new ValidationException("CDC出库仓库的仓储企业不存在");
                }
                if(storageCompanyOfCDCReturnWarehouse.Type != cDCReturnWarehouse.StorageCompanyType) {
                    throw new ValidationException("CDC出库仓库的仓储企业类型与企业信息中不一致");
                }
                var partsOutboundPlan = CreatePartsOutboundPlanByPartsClaimOrderNewForNotAgency(partsClaimOrderNew, companyOfBranch, returnWarehouse, storageCompanyOfReturnWarehouse, cDCReturnWarehouse);
                InsertToDatabase(partsOutboundPlan);//以防万一被坑写上也没错
                DomainService.生成配件出库计划(partsOutboundPlan);
            }
            #endregion

        }
        public void 发运配件索赔单新(PartsClaimOrderNew partsClaimOrderNew) {
            UpdatePartsClaimOrderNew(partsClaimOrderNew);
            #region 各种校验
            var rightStatusArray = new int[] { (int)DcsPartsClaimBillStatusNew.审核通过 };
            var dbpartsClaimOrder = ObjectContext.PartsClaimOrderNews.Where(r => r.Id == partsClaimOrderNew.Id && rightStatusArray.Contains(r.Status)).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbpartsClaimOrder == null)
                throw new ValidationException("操作失败,单据状态不正确或已被修改");
            var companyOfBranch = ObjectContext.Companies.SingleOrDefault(r => r.Id == partsClaimOrderNew.Branchid && r.Status == (int)DcsMasterDataStatus.有效 && r.Type == (int)DcsCompanyType.分公司);
            if(companyOfBranch == null) {
                throw new ValidationException("分公司在企业信息不存在");
            }
            var returnWarehouse = ObjectContext.Warehouses.SingleOrDefault(r => r.Id == partsClaimOrderNew.ReturnWarehouseId && r.Status == (int)DcsMasterDataStatus.有效);
            if(returnWarehouse == null) {
                throw new ValidationException("退货仓库不存在");
            }
            var storageCompanyOfReturnWarehouse = ObjectContext.Companies.SingleOrDefault(r => r.Id == partsClaimOrderNew.RWarehouseCompanyId && r.Status == (int)DcsMasterDataStatus.有效);
            if(storageCompanyOfReturnWarehouse == null) {
                throw new ValidationException("退货仓库的仓储企业不存在");
            }
            var returnCompany = ObjectContext.Companies.SingleOrDefault(r => r.Id == partsClaimOrderNew.ReturnCompanyId && r.Status == (int)DcsMasterDataStatus.有效);
            if(returnCompany == null) {
                throw new ValidationException("退货企业不存在");
            }

            var userInfo = Utils.GetCurrentUserInfo();

            #endregion

            switch(returnCompany.Type) {
                case (int)DcsCompanyType.服务站:
                case (int)DcsCompanyType.服务站兼代理库: {
                        #region
                        //1.如果  企业.企业类型=服务站/服务站兼代理库
                        //查询配件拆散信息（原配件Id=配件索赔单（新）.祸首件id,配件拆散信息.配件销售类型Id=配件索赔单（新）.配件销售类型Id,配件拆散信息.营销分公司Id=配件索赔单（新）.营销分公司id）
                        //1.1如果存在拆散信息
                        //更新经销商配件库存（经销商库存.经销商Id=配件索赔单（新）.提报单位Id、经销商库存.配件Id=配件索赔单（新）.祸首件id,配件拆散信息.配件销售类型Id=配件索赔单（新）.配件销售类型Id）：数量=数量-配件索赔单（新）.数量*配件拆散信息.拆散数量
                        //如果不存在拆散信息
                        //更新经销商配件库存（经销商库存.经销商Id=配件索赔单（新）.提报单位Id、经销商库存.配件Id=配件索赔单（新）.祸首件id,配件拆散信息.配件销售类型Id=配件索赔单（新）.配件销售类型Id）数量=数量-配件索赔单（新）.数量
                        //数量不足请报错：服务站库存不足
                        var qtyForDealerPartStock = partsClaimOrderNew.Quantity == null ? 0 : partsClaimOrderNew.Quantity.Value;
                        var deleaveInfoOfFaultyParts = ObjectContext.PartDeleaveInformations.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.OldPartId == partsClaimOrderNew.FaultyPartsId && r.PartsSalesCategoryId == partsClaimOrderNew.PartsSalesCategoryId && r.BranchId == partsClaimOrderNew.Branchid).ToArray();
                        if(deleaveInfoOfFaultyParts.Length > 1) {
                            throw new ValidationException("获取到多条拆散件信息");
                        } else if(deleaveInfoOfFaultyParts.Length == 1) {
                            if(deleaveInfoOfFaultyParts[0].DeleaveAmount <= 0) {
                                throw new ValidationException("获取到的拆散件信息中拆散数量小于或者等于0");
                            }
                            qtyForDealerPartStock = qtyForDealerPartStock * deleaveInfoOfFaultyParts[0].DeleaveAmount;
                        }

                        var dealerPartsStrocks = ObjectContext.DealerPartsStocks.Where(r => r.DealerId == partsClaimOrderNew.ReturnCompanyId && r.SubDealerId == -1 && r.SparePartId == partsClaimOrderNew.FaultyPartsId && r.SalesCategoryId == partsClaimOrderNew.PartsSalesCategoryId).ToArray();
                        if(dealerPartsStrocks.Length != 1) {
                            throw new ValidationException("未获取到或找到多条经销商库存信息");
                        }
                        if(dealerPartsStrocks[0].Quantity < qtyForDealerPartStock) {
                            throw new ValidationException("服务站库存不足");
                        }
                        dealerPartsStrocks[0].Quantity = dealerPartsStrocks[0].Quantity - qtyForDealerPartStock;
                        UpdateToDatabase(dealerPartsStrocks[0]);



                        var customerAccounts = (from a in ObjectContext.CustomerAccounts.Where(r => r.Status == (int)DcsBaseDataStatus.有效)
                                                join b in ObjectContext.SalesUnits.Where(r => r.Status == (int)DcsMasterDataStatus.有效) on a.AccountGroupId equals b.AccountGroupId
                                                where a.CustomerCompanyId == returnCompany.Id && b.OwnerCompanyId == storageCompanyOfReturnWarehouse.Id && b.PartsSalesCategoryId == partsClaimOrderNew.PartsSalesCategoryId
                                                select a).ToArray();
                        if(customerAccounts.Length != 1) {
                            throw new ValidationException("未找到或找到多个客户账户");
                        }
                        var customerAccount = customerAccounts[0];
                        var partsInboundPlan = CreatePartsInboundPlanByPartsClaimOrderNew(partsClaimOrderNew, companyOfBranch, returnCompany, customerAccount, returnWarehouse, storageCompanyOfReturnWarehouse);
                        InsertToDatabase(partsInboundPlan);
                        new PartsInboundPlanAch(this.DomainService).InsertPartsInboundPlanValidate(partsInboundPlan);
                        if(storageCompanyOfReturnWarehouse.Type == (int)DcsCompanyType.代理库 || storageCompanyOfReturnWarehouse.Type == (int)DcsCompanyType.服务站兼代理库) {//1.3 如果 配件索赔单（新）.退货仓库隶属企业类型=代理库或服务站兼代理库//更新配件索赔单（新），状态=服务站已发运 //如果 配件索赔单（新）.退货仓库隶属企业类型=分公司//更新配件索赔单（新），状态=生效
                            partsClaimOrderNew.Status = (int)DcsPartsClaimBillStatusNew.服务站已发运;
                        }
                        if(storageCompanyOfReturnWarehouse.Type == (int)DcsCompanyType.分公司) {
                            partsClaimOrderNew.Status = (int)DcsPartsClaimBillStatusNew.入库完成;
                        }
                        #endregion
                    }
                    break;
                case (int)DcsCompanyType.代理库: {
                        #region
                        if(partsClaimOrderNew.AgencyOutWarehouseId == null) {
                            throw new ValidationException("代理库出库仓库为空");
                        }
                        var agencyOutWarehouse = ObjectContext.Warehouses.SingleOrDefault(r => r.Id == partsClaimOrderNew.AgencyOutWarehouseId && r.Status == (int)DcsMasterDataStatus.有效);
                        if(agencyOutWarehouse == null) {
                            throw new ValidationException("代理库出库仓库不存在");
                        }
                        var storageCompanyOfAgencyOutWarehouse = ObjectContext.Companies.SingleOrDefault(r => r.Id == agencyOutWarehouse.StorageCompanyId && r.Status == (int)DcsMasterDataStatus.有效);
                        if(storageCompanyOfAgencyOutWarehouse == null) {
                            throw new ValidationException("代理库出库仓库的仓储企业不存在");
                        }
                        if(storageCompanyOfAgencyOutWarehouse.Type != agencyOutWarehouse.StorageCompanyType) {
                            throw new ValidationException("代理库出库仓库的仓储企业类型与企业信息中不一致");
                        }
                        //2.如果 企业.企业类型=代理库//2.1 调用服务【配件出库计划.生成配件出库计划】
                        var partsOutboundPlan = CreatePartsOutboundPlanByPartsClaimOrderNewForAgency(partsClaimOrderNew, agencyOutWarehouse, storageCompanyOfAgencyOutWarehouse, companyOfBranch, returnWarehouse, storageCompanyOfReturnWarehouse);
                        InsertToDatabase(partsOutboundPlan);
                        new PartsOutboundPlanAch(this.DomainService).InsertPartsOutboundPlanValidate(partsOutboundPlan);
                        DomainService.生成配件出库计划(partsOutboundPlan);
                        partsClaimOrderNew.Status = (int)DcsPartsClaimBillStatusNew.区域已发运;
                        #endregion
                    }
                    break;
                default:
                    throw new ValidationException("退货企业类型只能是代理库,服务站兼代理库,服务站");
            }
        }

        public void 服务站发运配件索赔单新(PartsClaimOrderNew partsClaimOrderNew) {
            UpdatePartsClaimOrderNew(partsClaimOrderNew);
            var user = Utils.GetCurrentUserInfo();
            var rightStatusArray = new int[] { (int)DcsPartsClaimBillStatusNew.终审通过 };
            var dbpartsClaimOrder = ObjectContext.PartsClaimOrderNews.Where(r => r.Id == partsClaimOrderNew.Id && rightStatusArray.Contains(r.Status)).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbpartsClaimOrder == null)
                throw new ValidationException(ErrorStrings.PartsClaimOrderNew_Validation1);

            partsClaimOrderNew.Status = (int)DcsPartsClaimBillStatusNew.服务站已发运;
            partsClaimOrderNew.ModifierId = user.Id;
            partsClaimOrderNew.ModifierName = user.Name;
            partsClaimOrderNew.ModifyTime = DateTime.Now;
            UpdateToDatabase(partsClaimOrderNew);
        }

        public void 中心库确认收货配件索赔单新(PartsClaimOrderNew partsClaimOrderNew) {
            UpdatePartsClaimOrderNew(partsClaimOrderNew);

            var user = Utils.GetCurrentUserInfo();
            //当前中心库
            var company = ObjectContext.Companies.Where(r => r.Id == user.EnterpriseId).First();
            //提报企业
            var returnCompany = ObjectContext.Companies.Where(r => r.Id == partsClaimOrderNew.ReturnCompanyId).First();

            if(company.Type != (int)DcsCompanyType.代理库) {
                throw new ValidationException(ErrorStrings.PartsClaimOrderNew_Validation4);
            }

            var dbpartsClaimOrder = ObjectContext.PartsClaimOrderNews.Where(r => r.Id == partsClaimOrderNew.Id).SingleOrDefault();
            if(dbpartsClaimOrder == null || dbpartsClaimOrder.Status != (int)DcsPartsClaimBillStatusNew.服务站已发运)
                throw new ValidationException(ErrorStrings.PartsClaimOrderNew_Validation1);

            //如果索赔单 供货单位 = 本中心库
            //1.生成质量件仓库的入库计划单，入库类型：销售退货
            //2.生成入库检验单
            //3.更新配件库存
            //4.更新企业库存
            if(partsClaimOrderNew.RWarehouseCompanyId == user.EnterpriseId) {
                var branch = ObjectContext.Branches.Where(r => r.Id == partsClaimOrderNew.Branchid).SingleOrDefault();
                //生成中心库的入库计划单
                var newPartsInboundPlan = new PartsInboundPlan();
                newPartsInboundPlan.WarehouseId = partsClaimOrderNew.ReturnWarehouseId;
                newPartsInboundPlan.WarehouseCode = partsClaimOrderNew.ReturnWarehouseCode;
                newPartsInboundPlan.WarehouseName = partsClaimOrderNew.ReturnWarehouseName;
                newPartsInboundPlan.StorageCompanyId = user.EnterpriseId;
                newPartsInboundPlan.StorageCompanyCode = user.EnterpriseCode;
                newPartsInboundPlan.StorageCompanyName = user.EnterpriseName;
                newPartsInboundPlan.StorageCompanyType = (int)company.Type;
                newPartsInboundPlan.BranchId = branch.Id;
                newPartsInboundPlan.BranchCode = branch.Code;
                newPartsInboundPlan.BranchName = branch.Name;
                newPartsInboundPlan.PartsSalesCategoryId = partsClaimOrderNew.PartsSalesCategoryId;
                //对方单位为服务站
                newPartsInboundPlan.CounterpartCompanyId = returnCompany.Id;
                newPartsInboundPlan.CounterpartCompanyCode = returnCompany.Code;
                newPartsInboundPlan.CounterpartCompanyName = returnCompany.Name;
                //源单据编号为索赔单编号
                newPartsInboundPlan.SourceId = (int)partsClaimOrderNew.Id;
                newPartsInboundPlan.SourceCode = partsClaimOrderNew.Code;
                newPartsInboundPlan.InboundType = (int)DcsPartsInboundType.销售退货;

                newPartsInboundPlan.Status = (int)DcsPartsInboundPlanStatus.上架完成;
                newPartsInboundPlan.IfWmsInterface = false;
                newPartsInboundPlan.ArrivalDate = DateTime.Now;
                newPartsInboundPlan.CreatorId = user.Id;
                newPartsInboundPlan.CreatorName = user.Name;
                newPartsInboundPlan.CreateTime = DateTime.Now;

                new PartsInboundPlanAch(this.DomainService).InsertPartsInboundPlanValidate(newPartsInboundPlan);

                var newPartsInboundPlanDetail = new PartsInboundPlanDetail();
                newPartsInboundPlanDetail.SparePartId = partsClaimOrderNew.FaultyPartsId;
                newPartsInboundPlanDetail.SparePartCode = partsClaimOrderNew.FaultyPartsCode;
                newPartsInboundPlanDetail.SparePartName = partsClaimOrderNew.FaultyPartsName;
                newPartsInboundPlanDetail.PlannedAmount = partsClaimOrderNew.Quantity ?? 0;
                newPartsInboundPlanDetail.InspectedQuantity = partsClaimOrderNew.Quantity ?? 0;
                newPartsInboundPlanDetail.Price = (decimal)partsClaimOrderNew.MaterialCost;

                newPartsInboundPlan.PartsInboundPlanDetails.Add(newPartsInboundPlanDetail);


                //生成入库检验单 
                var partsInboundCheckBill = new PartsInboundCheckBill();
                partsInboundCheckBill.PartsInboundPlan = newPartsInboundPlan;
                partsInboundCheckBill.WarehouseName = newPartsInboundPlan.WarehouseName;
                partsInboundCheckBill.InboundType = newPartsInboundPlan.InboundType;
                partsInboundCheckBill.WarehouseCode = newPartsInboundPlan.WarehouseCode;
                partsInboundCheckBill.Code = CodeGenerator.Generate("PartsInboundCheckBill", user.EnterpriseCode);
                partsInboundCheckBill.WarehouseId = newPartsInboundPlan.WarehouseId;
                partsInboundCheckBill.StorageCompanyId = newPartsInboundPlan.StorageCompanyId;
                partsInboundCheckBill.StorageCompanyCode = newPartsInboundPlan.StorageCompanyCode;
                partsInboundCheckBill.StorageCompanyName = newPartsInboundPlan.StorageCompanyName;
                partsInboundCheckBill.StorageCompanyType = newPartsInboundPlan.StorageCompanyType;
                partsInboundCheckBill.BranchId = newPartsInboundPlan.BranchId;
                partsInboundCheckBill.BranchCode = newPartsInboundPlan.BranchCode;
                partsInboundCheckBill.BranchName = newPartsInboundPlan.BranchName;
                partsInboundCheckBill.CounterpartCompanyId = newPartsInboundPlan.CounterpartCompanyId;
                partsInboundCheckBill.CounterpartCompanyCode = newPartsInboundPlan.CounterpartCompanyCode;
                partsInboundCheckBill.CounterpartCompanyName = newPartsInboundPlan.CounterpartCompanyName;

                partsInboundCheckBill.Status = (int)DcsPartsInboundCheckBillStatus.上架完成;
                partsInboundCheckBill.SettlementStatus = (int)DcsPartsSettlementStatus.待结算;
                partsInboundCheckBill.GPMSPurOrderCode = newPartsInboundPlan.GPMSPurOrderCode;
                partsInboundCheckBill.PartsSalesCategoryId = newPartsInboundPlan.PartsSalesCategoryId;
                partsInboundCheckBill.PartsSalesCategory = newPartsInboundPlan.PartsSalesCategory;
                partsInboundCheckBill.SAPPurchasePlanCode = newPartsInboundPlan.SAPPurchasePlanCode;
                partsInboundCheckBill.ERPSourceOrderCode = newPartsInboundPlan.ERPSourceOrderCode;
                partsInboundCheckBill.CreatorId = user.Id;
                partsInboundCheckBill.CreatorName = user.Name;
                partsInboundCheckBill.CreateTime = DateTime.Now;

                //库位选择保管区的任意一个库位
                var warehouseArea = ObjectContext.WarehouseAreas.Where(r => ObjectContext.WarehouseAreaCategories.Any(d => d.Category == (int)DcsAreaType.保管区 && d.Id == r.AreaCategoryId) && r.WarehouseId == newPartsInboundPlan.WarehouseId && r.AreaKind == (int)DcsAreaKind.库位 && r.Status == (int)DcsBaseDataStatus.有效).FirstOrDefault();
                if(warehouseArea == null) {
                    throw new ValidationException(ErrorStrings.PartsClaimOrderNew_Validation5);
                }

                //企业库存
                var enterprisePartsCost = new EnterprisePartsCostAch(this.DomainService).GetEnterprisePartsCostsWithLock(ObjectContext.EnterprisePartsCosts.Where
                    (r => r.OwnerCompanyId == newPartsInboundPlan.StorageCompanyId && r.SparePartId == newPartsInboundPlanDetail.SparePartId && r.PartsSalesCategoryId == newPartsInboundPlan.PartsSalesCategoryId)).SingleOrDefault();
                if(enterprisePartsCost == null) {
                    throw new ValidationException(ErrorStrings.PartsClaimOrderNew_Validation6);
                }

                //入库检验单清单
                partsInboundCheckBill.PartsInboundCheckBillDetails.Add(new PartsInboundCheckBillDetail {
                    PartsInboundCheckBill = partsInboundCheckBill,
                    SparePartId = newPartsInboundPlanDetail.SparePartId,
                    SparePartCode = newPartsInboundPlanDetail.SparePartCode,
                    SparePartName = newPartsInboundPlanDetail.SparePartName,
                    SettlementPrice = newPartsInboundPlanDetail.Price,
                    CostPrice = enterprisePartsCost.CostPrice,
                    POCode = newPartsInboundPlanDetail.POCode,
                    WarehouseAreaId = warehouseArea.Id,
                    WarehouseAreaCode = warehouseArea.Code,
                    InspectedQuantity = newPartsInboundPlanDetail.PlannedAmount,
                    IsPacking = false
                });


                //更新库位库存
                var partsStock = new PartsStockAch(this.DomainService).GetPartsStocksWithLock(ObjectContext.PartsStocks.Where(r => r.WarehouseAreaId == warehouseArea.Id && r.PartId == newPartsInboundPlanDetail.SparePartId)).SingleOrDefault();
                if(partsStock == null) {
                    var ps = new PartsStock();
                    ps.WarehouseId = newPartsInboundPlan.WarehouseId;
                    ps.StorageCompanyId = newPartsInboundPlan.StorageCompanyId;
                    ps.StorageCompanyType = newPartsInboundPlan.StorageCompanyType;
                    ps.BranchId = newPartsInboundPlan.BranchId;
                    ps.WarehouseAreaId = warehouseArea.Id;
                    ps.WarehouseAreaCategory = new WarehouseAreaCategory {
                        Category = (int)DcsAreaType.保管区
                    };
                    ps.PartId = newPartsInboundPlanDetail.SparePartId;
                    ps.Quantity = newPartsInboundPlanDetail.PlannedAmount;
                    ps.CreatorId = user.Id;
                    ps.CreatorName = user.Name;
                    ps.CreateTime = DateTime.Now;

                    InsertToDatabase(ps);
                } else {
                    partsStock.Quantity += newPartsInboundPlanDetail.PlannedAmount;
                    partsStock.ModifierId = user.Id;
                    partsStock.ModifierName = user.Name;
                    partsStock.ModifyTime = DateTime.Now;
                    UpdateToDatabase(partsStock);
                }

                //更新企业库存
                enterprisePartsCost.Quantity += newPartsInboundPlanDetail.PlannedAmount;
                enterprisePartsCost.CostAmount = enterprisePartsCost.CostAmount + (enterprisePartsCost.CostPrice * newPartsInboundPlanDetail.PlannedAmount);
                enterprisePartsCost.ModifierId = user.Id;
                enterprisePartsCost.ModifierName = user.Name;
                enterprisePartsCost.ModifyTime = DateTime.Now;
                UpdateToDatabase(enterprisePartsCost);


                //查询销售订单
                PartsSalesOrder partsSalesOrder;
                if(partsClaimOrderNew.PartsSalesOrderId != null && partsClaimOrderNew.PartsSalesOrderId != default(int)) {
                    partsSalesOrder = ObjectContext.PartsSalesOrders.Where(r => r.Id == partsClaimOrderNew.PartsSalesOrderId).Include("PartsSalesOrderDetails").FirstOrDefault();
                } else {
                    throw new ValidationException(ErrorStrings.PartsClaimOrderNew_Validation7);
                }

                //生成服务站向中心库的销售退货单
                var partsSalesReturnBill = new PartsSalesReturnBill();
                partsSalesReturnBill.Code = CodeGenerator.Generate("PartsSalesReturnBill", newPartsInboundPlan.StorageCompanyCode);
                //退货单位为索赔单的提报单位
                partsSalesReturnBill.ReturnCompanyId = returnCompany.Id;
                partsSalesReturnBill.ReturnCompanyCode = returnCompany.Code;
                partsSalesReturnBill.ReturnCompanyName = returnCompany.Name;
                partsSalesReturnBill.InvoiceReceiveCompanyId = returnCompany.Id;
                partsSalesReturnBill.InvoiceReceiveCompanyCode = returnCompany.Code;
                partsSalesReturnBill.InvoiceReceiveCompanyName = returnCompany.Name;
                partsSalesReturnBill.SubmitCompanyId = returnCompany.Id;
                partsSalesReturnBill.SubmitCompanyCode = returnCompany.Code;
                partsSalesReturnBill.SubmitCompanyName = returnCompany.Name;
                partsSalesReturnBill.TotalAmount = (decimal)partsClaimOrderNew.MaterialCost;
                partsSalesReturnBill.ShippingMethod = (int)DcsPartsShippingMethod.公路物流;
                partsSalesReturnBill.SalesUnitId = partsSalesOrder.SalesUnitId;
                partsSalesReturnBill.SalesUnitName = partsSalesOrder.SalesUnitName;
                partsSalesReturnBill.SalesUnitOwnerCompanyId = partsSalesOrder.SalesUnitOwnerCompanyId;
                partsSalesReturnBill.SalesUnitOwnerCompanyCode = partsSalesOrder.SalesUnitOwnerCompanyCode;
                partsSalesReturnBill.SalesUnitOwnerCompanyName = partsSalesOrder.SalesUnitOwnerCompanyName;
                partsSalesReturnBill.CustomerAccountId = partsSalesOrder.CustomerAccountId;

                partsSalesReturnBill.BranchId = branch.Id;
                partsSalesReturnBill.BranchCode = branch.Code;
                partsSalesReturnBill.BranchName = branch.Name;
                partsSalesReturnBill.InvoiceRequirement = (int)DcsPartsSalesReturnBillInvoiceRequirement.出库合并开票;
                partsSalesReturnBill.WarehouseId = partsClaimOrderNew.ReturnWarehouseId;
                partsSalesReturnBill.WarehouseCode = partsClaimOrderNew.ReturnWarehouseCode;
                partsSalesReturnBill.WarehouseName = partsClaimOrderNew.ReturnWarehouseName;
                partsSalesReturnBill.Status = (int)DcsPartsSalesReturnBillStatus.审核通过;
                partsSalesReturnBill.ReturnType = (int)DcsPartsSalesReturnBillReturnType.特殊退货;
                partsSalesReturnBill.ReturnReason = "配件索赔";
                partsSalesReturnBill.CreatorId = user.Id;
                partsSalesReturnBill.CreatorName = user.Name;
                partsSalesReturnBill.CreateTime = DateTime.Now;

                //销售退货单清单
                var faultyPart = ObjectContext.SpareParts.Single(r => r.Id == partsClaimOrderNew.FaultyPartsId);
                var partsSalesReturnBillDetail = new PartsSalesReturnBillDetail();
                partsSalesReturnBillDetail.SparePartId = partsClaimOrderNew.FaultyPartsId;
                partsSalesReturnBillDetail.SparePartCode = partsClaimOrderNew.FaultyPartsCode;
                partsSalesReturnBillDetail.SparePartName = partsClaimOrderNew.FaultyPartsName;
                partsSalesReturnBillDetail.MeasureUnit = faultyPart.MeasureUnit;
                partsSalesReturnBillDetail.ReturnedQuantity = (int)partsClaimOrderNew.Quantity;
                partsSalesReturnBillDetail.ApproveQuantity = partsClaimOrderNew.Quantity;
                partsSalesReturnBillDetail.OriginalOrderPrice = partsClaimOrderNew.MaterialCost;
                partsSalesReturnBillDetail.ReturnPrice = (decimal)partsSalesReturnBillDetail.OriginalOrderPrice;
                partsSalesReturnBillDetail.PartsSalesOrderId = partsSalesOrder.Id;
                partsSalesReturnBillDetail.OriginalRequirementBillId = partsSalesOrder.Id;
                partsSalesReturnBillDetail.OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.配件销售订单;
                partsSalesReturnBillDetail.PartsSalesOrderCode = partsSalesOrder.Code;
                //零售价 = 销售订单清单.原始价格
                var partsSalesOrderDetail = partsSalesOrder.PartsSalesOrderDetails.FirstOrDefault(r => r.SparePartId == partsClaimOrderNew.FaultyPartsId);
                partsSalesReturnBillDetail.OriginalPrice = partsSalesOrderDetail.OriginalPrice;
                partsSalesReturnBill.PartsSalesReturnBillDetails.Add(partsSalesReturnBillDetail);

                InsertToDatabase(partsSalesReturnBill);
                ObjectContext.SaveChanges();

                newPartsInboundPlan.OriginalRequirementBillId = partsSalesReturnBill.Id;
                newPartsInboundPlan.OriginalRequirementBillCode = partsSalesReturnBill.Code;
                newPartsInboundPlan.OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.配件销售退货单;
                newPartsInboundPlan.CustomerAccountId = partsSalesOrder.CustomerAccountId;
                InsertToDatabase(newPartsInboundPlan);

                partsInboundCheckBill.OriginalRequirementBillId = partsSalesReturnBill.Id;
                partsInboundCheckBill.OriginalRequirementBillCode = partsSalesReturnBill.Code;
                partsInboundCheckBill.OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.配件销售退货单;
                partsInboundCheckBill.CustomerAccountId = partsSalesOrder.CustomerAccountId;
                InsertToDatabase(partsInboundCheckBill);
            }

            partsClaimOrderNew.Status = (int)DcsPartsClaimBillStatusNew.区域已收货;
            partsClaimOrderNew.ModifierId = user.Id;
            partsClaimOrderNew.ModifierName = user.Name;
            partsClaimOrderNew.ModifyTime = DateTime.Now;

            UpdateToDatabase(partsClaimOrderNew);
        }

        public void 中心库发运配件索赔单新(PartsClaimOrderNew partsClaimOrderNew) {
            UpdatePartsClaimOrderNew(partsClaimOrderNew);

            var user = Utils.GetCurrentUserInfo();
            //分公司
            var branch = ObjectContext.Branches.Where(r => r.Id == partsClaimOrderNew.Branchid).First();
            //本中心库
            var company = ObjectContext.Companies.Where(r => r.Id == user.EnterpriseId).First();
            //提报单位
            var returnCompany = ObjectContext.Companies.Where(r => r.Id == partsClaimOrderNew.ReturnCompanyId).First();
            //分公司的质量件仓库
            var branchQualityWarehouse = ObjectContext.Warehouses.Where(r => r.StorageCompanyId == branch.Id && r.IsQualityWarehouse == true && r.Status == (int)DcsBaseDataStatus.有效).FirstOrDefault();
            //
            var faultyPart = ObjectContext.SpareParts.Single(r => r.Id == partsClaimOrderNew.FaultyPartsId);

            if(company.Type != (int)DcsCompanyType.代理库) {
                throw new ValidationException(ErrorStrings.PartsClaimOrderNew_Validation4);
            }

            var rightStatusArray = new int[] { (int)DcsPartsClaimBillStatusNew.区域已收货 };
            var dbpartsClaimOrder = ObjectContext.PartsClaimOrderNews.Where(r => r.Id == partsClaimOrderNew.Id && rightStatusArray.Contains(r.Status)).SingleOrDefault();
            if(dbpartsClaimOrder == null)
                throw new ValidationException(ErrorStrings.PartsClaimOrderNew_Validation1);

            //查询销售订单
            PartsSalesOrder partsSalesOrder;
            if(partsClaimOrderNew.CenterPartsSalesOrderId != null && partsClaimOrderNew.CenterPartsSalesOrderId != default(int)) {
                partsSalesOrder = ObjectContext.PartsSalesOrders.Where(r => r.Id == partsClaimOrderNew.CenterPartsSalesOrderId).Include("PartsSalesOrderDetails").FirstOrDefault();
            } else {
                partsSalesOrder = ObjectContext.PartsSalesOrders.Where(r => r.Id == partsClaimOrderNew.PartsSalesOrderId).Include("PartsSalesOrderDetails").FirstOrDefault();
            }


            //生成总部的销售退货单
            var partsSalesReturnBill = new PartsSalesReturnBill();
            partsSalesReturnBill.Code = CodeGenerator.Generate("PartsSalesReturnBill", branch.Code);
            //如果索赔单供货单位是中心库
            if(partsClaimOrderNew.RWarehouseCompanyId == user.EnterpriseId) {
                partsSalesReturnBill.ReturnCompanyId = company.Id;
                partsSalesReturnBill.ReturnCompanyCode = company.Code;
                partsSalesReturnBill.ReturnCompanyName = company.Name;
                partsSalesReturnBill.InvoiceReceiveCompanyId = company.Id;
                partsSalesReturnBill.InvoiceReceiveCompanyCode = company.Code;
                partsSalesReturnBill.InvoiceReceiveCompanyName = company.Name;
                partsSalesReturnBill.SubmitCompanyId = company.Id;
                partsSalesReturnBill.SubmitCompanyCode = company.Code;
                partsSalesReturnBill.SubmitCompanyName = company.Name;
            } else {
                partsSalesReturnBill.ReturnCompanyId = returnCompany.Id;
                partsSalesReturnBill.ReturnCompanyCode = returnCompany.Code;
                partsSalesReturnBill.ReturnCompanyName = returnCompany.Name;
                partsSalesReturnBill.InvoiceReceiveCompanyId = returnCompany.Id;
                partsSalesReturnBill.InvoiceReceiveCompanyCode = returnCompany.Code;
                partsSalesReturnBill.InvoiceReceiveCompanyName = returnCompany.Name;
                partsSalesReturnBill.SubmitCompanyId = returnCompany.Id;
                partsSalesReturnBill.SubmitCompanyCode = returnCompany.Code;
                partsSalesReturnBill.SubmitCompanyName = returnCompany.Name;
            }

            partsSalesReturnBill.TotalAmount = (decimal)(partsClaimOrderNew.MaterialManagementCost == null ? partsClaimOrderNew.MaterialCost : partsClaimOrderNew.MaterialManagementCost);
            partsSalesReturnBill.ShippingMethod = (int)DcsPartsShippingMethod.公路物流;

            partsSalesReturnBill.SalesUnitId = partsSalesOrder.SalesUnitId;
            partsSalesReturnBill.SalesUnitName = partsSalesOrder.SalesUnitName;
            partsSalesReturnBill.SalesUnitOwnerCompanyId = partsSalesOrder.SalesUnitOwnerCompanyId;
            partsSalesReturnBill.SalesUnitOwnerCompanyCode = partsSalesOrder.SalesUnitOwnerCompanyCode;
            partsSalesReturnBill.SalesUnitOwnerCompanyName = partsSalesOrder.SalesUnitOwnerCompanyName;
            partsSalesReturnBill.CustomerAccountId = partsSalesOrder.CustomerAccountId;

            partsSalesReturnBill.BranchId = branch.Id;
            partsSalesReturnBill.BranchCode = branch.Code;
            partsSalesReturnBill.BranchName = branch.Name;
            partsSalesReturnBill.InvoiceRequirement = (int)DcsPartsSalesReturnBillInvoiceRequirement.出库合并开票;
            partsSalesReturnBill.WarehouseId = branchQualityWarehouse.Id;
            partsSalesReturnBill.WarehouseCode = branchQualityWarehouse.Code;
            partsSalesReturnBill.WarehouseName = branchQualityWarehouse.Name;
            partsSalesReturnBill.Status = (int)DcsPartsSalesReturnBillStatus.审批通过;
            partsSalesReturnBill.ReturnType = (int)DcsPartsSalesReturnBillReturnType.特殊退货;
            partsSalesReturnBill.ReturnReason = "配件索赔";
            partsSalesReturnBill.CreatorId = user.Id;
            partsSalesReturnBill.CreatorName = user.Name;
            partsSalesReturnBill.CreateTime = DateTime.Now;

            //销售退货单清单
            var partsSalesReturnBillDetail = new PartsSalesReturnBillDetail();
            partsSalesReturnBillDetail.SparePartId = partsClaimOrderNew.FaultyPartsId;
            partsSalesReturnBillDetail.SparePartCode = partsClaimOrderNew.FaultyPartsCode;
            partsSalesReturnBillDetail.SparePartName = partsClaimOrderNew.FaultyPartsName;
            partsSalesReturnBillDetail.MeasureUnit = faultyPart.MeasureUnit;
            partsSalesReturnBillDetail.ReturnedQuantity = (int)partsClaimOrderNew.Quantity;
            partsSalesReturnBillDetail.ApproveQuantity = partsClaimOrderNew.Quantity;
            partsSalesReturnBillDetail.OriginalOrderPrice = partsClaimOrderNew.MaterialManagementCost == null ? partsClaimOrderNew.MaterialCost : partsClaimOrderNew.MaterialManagementCost;
            partsSalesReturnBillDetail.ReturnPrice = (decimal)partsSalesReturnBillDetail.OriginalOrderPrice;
            partsSalesReturnBillDetail.PartsSalesOrderId = partsSalesOrder.Id;
            partsSalesReturnBillDetail.OriginalRequirementBillId = partsSalesOrder.Id;
            partsSalesReturnBillDetail.OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.配件销售订单;
            partsSalesReturnBillDetail.PartsSalesOrderCode = partsSalesOrder.Code;
            //零售价 = 销售订单清单.原始价格
            var partsSalesOrderDetail = partsSalesOrder.PartsSalesOrderDetails.FirstOrDefault(r => r.SparePartId == partsClaimOrderNew.FaultyPartsId);
            partsSalesReturnBillDetail.OriginalPrice = partsSalesOrderDetail.OriginalPrice;
            partsSalesReturnBill.PartsSalesReturnBillDetails.Add(partsSalesReturnBillDetail);

            InsertToDatabase(partsSalesReturnBill);

            //如果索赔单 供货单位 = 本中心库 并且 提报单位 != 本中心库
            //1.生成出库计划单
            //2.生成出库单
            //3.更新配件库存
            //4.更新企业库存
            if(partsClaimOrderNew.RWarehouseCompanyId == user.EnterpriseId && partsClaimOrderNew.ReturnCompanyId != user.EnterpriseId) {
                //配件出库计划
                var partsOutboundPlan = new PartsOutboundPlan();
                partsOutboundPlan.Code = CodeGenerator.Generate("PartsOutboundPlan", partsOutboundPlan.StorageCompanyCode);
                partsOutboundPlan.PartsSalesCategoryId = partsClaimOrderNew.PartsSalesCategoryId;
                partsOutboundPlan.WarehouseId = partsClaimOrderNew.ReturnWarehouseId;
                partsOutboundPlan.WarehouseCode = partsClaimOrderNew.ReturnWarehouseCode;
                partsOutboundPlan.WarehouseName = partsClaimOrderNew.ReturnWarehouseName;
                partsOutboundPlan.StorageCompanyId = company.Id;
                partsOutboundPlan.StorageCompanyCode = company.Code;
                partsOutboundPlan.StorageCompanyName = company.Name;
                partsOutboundPlan.StorageCompanyType = company.Type;
                //对方单位，收货单位均为总部
                partsOutboundPlan.BranchId = branch.Id;
                partsOutboundPlan.BranchCode = branch.Code;
                partsOutboundPlan.BranchName = branch.Name;
                partsOutboundPlan.CounterpartCompanyId = branch.Id;
                partsOutboundPlan.CounterpartCompanyCode = branch.Code;
                partsOutboundPlan.CounterpartCompanyName = branch.Name;
                partsOutboundPlan.ReceivingCompanyId = branch.Id;
                partsOutboundPlan.ReceivingCompanyCode = branch.Code;
                partsOutboundPlan.ReceivingCompanyName = branch.Name;
                if(branchQualityWarehouse != null) {
                    partsOutboundPlan.ReceivingWarehouseId = branchQualityWarehouse.Id;
                    partsOutboundPlan.ReceivingWarehouseCode = branchQualityWarehouse.Code;
                    partsOutboundPlan.ReceivingWarehouseName = branchQualityWarehouse.Name;
                }

                //源单据编号为索赔单编号
                partsOutboundPlan.SourceId = (int)partsClaimOrderNew.Id;
                partsOutboundPlan.SourceCode = partsClaimOrderNew.Code;
                partsOutboundPlan.OutboundType = (int)DcsPartsOutboundType.采购退货;
                partsOutboundPlan.Status = (int)DcsPartsOutboundPlanStatus.发运完成;
                partsOutboundPlan.IfWmsInterface = false;
                partsOutboundPlan.CreatorId = user.Id;
                partsOutboundPlan.CreatorName = user.Name;
                partsOutboundPlan.CreateTime = DateTime.Now;

                //出库计划单清单
                var partsOutboundPlanDetail = new PartsOutboundPlanDetail();
                partsOutboundPlanDetail.SparePartId = partsClaimOrderNew.FaultyPartsId;
                partsOutboundPlanDetail.SparePartCode = partsClaimOrderNew.FaultyPartsCode;
                partsOutboundPlanDetail.SparePartName = partsClaimOrderNew.FaultyPartsName;
                partsOutboundPlanDetail.PlannedAmount = partsClaimOrderNew.Quantity ?? 0;
                partsOutboundPlanDetail.OutboundFulfillment = partsClaimOrderNew.Quantity ?? 0;
                partsOutboundPlanDetail.Price = (decimal)partsClaimOrderNew.MaterialManagementCost;
                partsOutboundPlan.PartsOutboundPlanDetails.Add(partsOutboundPlanDetail);

                //生成出库单
                var newPartsOutboundBill = new PartsOutboundBill();
                newPartsOutboundBill.Code = CodeGenerator.Generate("PartsOutboundBill", partsOutboundPlan.StorageCompanyCode);
                newPartsOutboundBill.PartsOutboundPlanId = partsOutboundPlan.Id;
                newPartsOutboundBill.WarehouseId = partsOutboundPlan.WarehouseId;
                newPartsOutboundBill.WarehouseCode = partsOutboundPlan.WarehouseCode;
                newPartsOutboundBill.WarehouseName = partsOutboundPlan.WarehouseName;
                newPartsOutboundBill.StorageCompanyId = partsOutboundPlan.StorageCompanyId;
                newPartsOutboundBill.StorageCompanyCode = partsOutboundPlan.StorageCompanyCode;
                newPartsOutboundBill.StorageCompanyName = partsOutboundPlan.StorageCompanyName;
                newPartsOutboundBill.StorageCompanyType = partsOutboundPlan.StorageCompanyType;
                newPartsOutboundBill.BranchId = partsOutboundPlan.BranchId;
                newPartsOutboundBill.BranchCode = partsOutboundPlan.BranchCode;
                newPartsOutboundBill.BranchName = partsOutboundPlan.BranchName;
                newPartsOutboundBill.PartsSalesCategoryId = partsOutboundPlan.PartsSalesCategoryId;
                newPartsOutboundBill.PartsSalesOrderTypeId = partsOutboundPlan.PartsSalesOrderTypeId;
                newPartsOutboundBill.PartsSalesOrderTypeName = partsOutboundPlan.PartsSalesOrderTypeName;
                newPartsOutboundBill.CounterpartCompanyId = partsOutboundPlan.CounterpartCompanyId;
                newPartsOutboundBill.CounterpartCompanyCode = partsOutboundPlan.CounterpartCompanyCode;
                newPartsOutboundBill.CounterpartCompanyName = partsOutboundPlan.CounterpartCompanyName;
                newPartsOutboundBill.ReceivingCompanyId = partsOutboundPlan.ReceivingCompanyId;
                newPartsOutboundBill.ReceivingCompanyCode = partsOutboundPlan.ReceivingCompanyCode;
                newPartsOutboundBill.ReceivingCompanyName = partsOutboundPlan.ReceivingCompanyName;
                newPartsOutboundBill.ReceivingWarehouseId = partsOutboundPlan.ReceivingWarehouseId;
                newPartsOutboundBill.ReceivingWarehouseCode = partsOutboundPlan.ReceivingWarehouseCode;
                newPartsOutboundBill.ReceivingWarehouseName = partsOutboundPlan.ReceivingWarehouseName;
                newPartsOutboundBill.OutboundType = partsOutboundPlan.OutboundType;
                newPartsOutboundBill.ShippingMethod = partsOutboundPlan.ShippingMethod;
                newPartsOutboundBill.CustomerAccountId = partsOutboundPlan.CustomerAccountId;
                newPartsOutboundBill.SettlementStatus = (int)DcsPartsSettlementStatus.已结算;
                newPartsOutboundBill.CreatorId = user.Id;
                newPartsOutboundBill.CreatorName = user.Name;
                newPartsOutboundBill.CreateTime = DateTime.Now;

                //企业库存
                var enterprisePartsCost = new EnterprisePartsCostAch(this.DomainService).GetEnterprisePartsCostsWithLock(ObjectContext.EnterprisePartsCosts.Where
                    (r => r.OwnerCompanyId == partsOutboundPlan.StorageCompanyId && r.SparePartId == partsOutboundPlanDetail.SparePartId && r.PartsSalesCategoryId == partsOutboundPlan.PartsSalesCategoryId)).SingleOrDefault();
                if(enterprisePartsCost == null) {
                    throw new ValidationException(ErrorStrings.PartsClaimOrderNew_Validation6);
                }

                //根据索赔单id，找入库检验单，获取库位
                var partsInboundPlan = ObjectContext.PartsInboundPlans.Where(r => r.SourceId == partsClaimOrderNew.Id && r.SourceCode == partsClaimOrderNew.Code).FirstOrDefault();
                var partsInboundCheckBill = ObjectContext.PartsInboundCheckBills.Where(r => r.PartsInboundPlanId == partsInboundPlan.Id).FirstOrDefault();
                var partsInboundCheckBillDetail = ObjectContext.PartsInboundCheckBillDetails.Where(r => r.PartsInboundCheckBillId == partsInboundCheckBill.Id && r.SparePartId == partsClaimOrderNew.FaultyPartsId).FirstOrDefault();

                //更新库位库存
                var partsStock = new PartsStockAch(this.DomainService).GetPartsStocksWithLock(ObjectContext.PartsStocks.Where(r => r.WarehouseAreaId == partsInboundCheckBillDetail.WarehouseAreaId && r.PartId == partsOutboundPlanDetail.SparePartId)).SingleOrDefault();
                if(partsStock == null) {
                    throw new ValidationException(ErrorStrings.PartsClaimOrderNew_Validation8);
                } else {
                    partsStock.Quantity -= partsOutboundPlanDetail.PlannedAmount;
                    if(partsStock.Quantity < 0) throw new ValidationException(ErrorStrings.PartsClaimOrderNew_Validation8);
                    partsStock.ModifierId = UserInfo.Id;
                    partsStock.ModifierName = UserInfo.Name;
                    partsStock.ModifyTime = DateTime.Now;
                    UpdateToDatabase(partsStock);
                }

                //新增出库单清单
                var newPartsOutboundBillDetail = new PartsOutboundBillDetail();
                newPartsOutboundBillDetail.SparePartId = partsOutboundPlanDetail.SparePartId;
                newPartsOutboundBillDetail.SparePartCode = partsOutboundPlanDetail.SparePartCode;
                newPartsOutboundBillDetail.SparePartName = partsOutboundPlanDetail.SparePartName;
                newPartsOutboundBillDetail.OutboundAmount = partsOutboundPlanDetail.PlannedAmount;
                newPartsOutboundBillDetail.WarehouseAreaId = partsInboundCheckBillDetail.WarehouseAreaId;
                newPartsOutboundBillDetail.WarehouseAreaCode = partsInboundCheckBillDetail.WarehouseAreaCode;
                newPartsOutboundBillDetail.SettlementPrice = (decimal)partsClaimOrderNew.MaterialManagementCost;
                newPartsOutboundBillDetail.CostPrice = enterprisePartsCost.CostPrice;
                newPartsOutboundBill.PartsOutboundBillDetails.Add(newPartsOutboundBillDetail);

                //更新企业库存
                enterprisePartsCost.Quantity -= newPartsOutboundBillDetail.OutboundAmount;
                enterprisePartsCost.CostAmount = enterprisePartsCost.CostAmount - (enterprisePartsCost.CostPrice * newPartsOutboundBillDetail.OutboundAmount);
                enterprisePartsCost.ModifierId = user.Id;
                enterprisePartsCost.ModifierName = user.Name;
                enterprisePartsCost.ModifyTime = DateTime.Now;
                UpdateToDatabase(enterprisePartsCost);

                ObjectContext.SaveChanges();

                partsOutboundPlan.OriginalRequirementBillId = partsSalesReturnBill.Id;
                partsOutboundPlan.OriginalRequirementBillCode = partsSalesReturnBill.Code;
                partsOutboundPlan.OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.配件销售退货单;
                partsOutboundPlan.CustomerAccountId = partsSalesOrder.CustomerAccountId;
                InsertToDatabase(partsOutboundPlan);

                newPartsOutboundBill.OriginalRequirementBillId = partsSalesReturnBill.Id;
                newPartsOutboundBill.OriginalRequirementBillCode = partsSalesReturnBill.Code;
                newPartsOutboundBill.OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.配件销售退货单;
                newPartsOutboundBill.CustomerAccountId = partsSalesOrder.CustomerAccountId;
                InsertToDatabase(newPartsOutboundBill);

            }

            //如果索赔单  提报单位 = 本中心库
                //生成出库计划单
                //生成出库单
                //更新配件库存
                //更新企业库存
            else if(partsClaimOrderNew.ReturnCompanyId == user.EnterpriseId) {

                //配件出库计划
                var partsOutboundPlan = new PartsOutboundPlan();
                partsOutboundPlan.Code = CodeGenerator.Generate("PartsOutboundPlan", partsOutboundPlan.StorageCompanyCode);
                partsOutboundPlan.PartsSalesCategoryId = partsClaimOrderNew.PartsSalesCategoryId;
                partsOutboundPlan.WarehouseId = partsClaimOrderNew.ReturnWarehouseId;
                partsOutboundPlan.WarehouseCode = partsClaimOrderNew.ReturnWarehouseCode;
                partsOutboundPlan.WarehouseName = partsClaimOrderNew.ReturnWarehouseName;
                partsOutboundPlan.StorageCompanyId = company.Id;
                partsOutboundPlan.StorageCompanyCode = company.Code;
                partsOutboundPlan.StorageCompanyName = company.Name;
                partsOutboundPlan.StorageCompanyType = company.Type;
                //对方单位，收货单位均为总部
                partsOutboundPlan.BranchId = branch.Id;
                partsOutboundPlan.BranchCode = branch.Code;
                partsOutboundPlan.BranchName = branch.Name;
                partsOutboundPlan.CounterpartCompanyId = branch.Id;
                partsOutboundPlan.CounterpartCompanyCode = branch.Code;
                partsOutboundPlan.CounterpartCompanyName = branch.Name;
                partsOutboundPlan.ReceivingCompanyId = branch.Id;
                partsOutboundPlan.ReceivingCompanyCode = branch.Code;
                partsOutboundPlan.ReceivingCompanyName = branch.Name;
                if(branchQualityWarehouse != null) {
                    partsOutboundPlan.ReceivingWarehouseId = branchQualityWarehouse.Id;
                    partsOutboundPlan.ReceivingWarehouseCode = branchQualityWarehouse.Code;
                    partsOutboundPlan.ReceivingWarehouseName = branchQualityWarehouse.Name;
                }
                //源单据编号为索赔单编号
                partsOutboundPlan.SourceId = (int)partsClaimOrderNew.Id;
                partsOutboundPlan.SourceCode = partsClaimOrderNew.Code;
                partsOutboundPlan.OutboundType = (int)DcsPartsOutboundType.采购退货;
                partsOutboundPlan.Status = (int)DcsPartsOutboundPlanStatus.发运完成;
                partsOutboundPlan.IfWmsInterface = false;
                partsOutboundPlan.CreatorId = user.Id;
                partsOutboundPlan.CreatorName = user.Name;
                partsOutboundPlan.CreateTime = DateTime.Now;

                //出库计划单清单
                var partsOutboundPlanDetail = new PartsOutboundPlanDetail();
                partsOutboundPlanDetail.SparePartId = partsClaimOrderNew.FaultyPartsId;
                partsOutboundPlanDetail.SparePartCode = partsClaimOrderNew.FaultyPartsCode;
                partsOutboundPlanDetail.SparePartName = partsClaimOrderNew.FaultyPartsName;
                partsOutboundPlanDetail.PlannedAmount = partsClaimOrderNew.Quantity ?? 0;
                partsOutboundPlanDetail.OutboundFulfillment = partsClaimOrderNew.Quantity ?? 0;
                partsOutboundPlanDetail.Price = (decimal)partsClaimOrderNew.MaterialManagementCost;
                partsOutboundPlan.PartsOutboundPlanDetails.Add(partsOutboundPlanDetail);

                //生成出库单
                var newPartsOutboundBill = new PartsOutboundBill();
                newPartsOutboundBill.Code = CodeGenerator.Generate("PartsOutboundBill", partsOutboundPlan.StorageCompanyCode);
                newPartsOutboundBill.PartsOutboundPlanId = partsOutboundPlan.Id;
                newPartsOutboundBill.WarehouseId = partsOutboundPlan.WarehouseId;
                newPartsOutboundBill.WarehouseCode = partsOutboundPlan.WarehouseCode;
                newPartsOutboundBill.WarehouseName = partsOutboundPlan.WarehouseName;
                newPartsOutboundBill.StorageCompanyId = partsOutboundPlan.StorageCompanyId;
                newPartsOutboundBill.StorageCompanyCode = partsOutboundPlan.StorageCompanyCode;
                newPartsOutboundBill.StorageCompanyName = partsOutboundPlan.StorageCompanyName;
                newPartsOutboundBill.StorageCompanyType = partsOutboundPlan.StorageCompanyType;
                newPartsOutboundBill.BranchId = partsOutboundPlan.BranchId;
                newPartsOutboundBill.BranchCode = partsOutboundPlan.BranchCode;
                newPartsOutboundBill.BranchName = partsOutboundPlan.BranchName;
                newPartsOutboundBill.PartsSalesCategoryId = partsOutboundPlan.PartsSalesCategoryId;
                newPartsOutboundBill.PartsSalesOrderTypeId = partsOutboundPlan.PartsSalesOrderTypeId;
                newPartsOutboundBill.PartsSalesOrderTypeName = partsOutboundPlan.PartsSalesOrderTypeName;
                newPartsOutboundBill.CounterpartCompanyId = partsOutboundPlan.CounterpartCompanyId;
                newPartsOutboundBill.CounterpartCompanyCode = partsOutboundPlan.CounterpartCompanyCode;
                newPartsOutboundBill.CounterpartCompanyName = partsOutboundPlan.CounterpartCompanyName;
                newPartsOutboundBill.ReceivingCompanyId = partsOutboundPlan.ReceivingCompanyId;
                newPartsOutboundBill.ReceivingCompanyCode = partsOutboundPlan.ReceivingCompanyCode;
                newPartsOutboundBill.ReceivingCompanyName = partsOutboundPlan.ReceivingCompanyName;
                newPartsOutboundBill.ReceivingWarehouseId = partsOutboundPlan.ReceivingWarehouseId;
                newPartsOutboundBill.ReceivingWarehouseCode = partsOutboundPlan.ReceivingWarehouseCode;
                newPartsOutboundBill.ReceivingWarehouseName = partsOutboundPlan.ReceivingWarehouseName;
                newPartsOutboundBill.OutboundType = partsOutboundPlan.OutboundType;
                newPartsOutboundBill.ShippingMethod = partsOutboundPlan.ShippingMethod;
                newPartsOutboundBill.SettlementStatus = (int)DcsPartsSettlementStatus.已结算;
                newPartsOutboundBill.CreatorId = user.Id;
                newPartsOutboundBill.CreatorName = user.Name;
                newPartsOutboundBill.CreateTime = DateTime.Now;

                //企业库存
                var enterprisePartsCost = new EnterprisePartsCostAch(this.DomainService).GetEnterprisePartsCostsWithLock(ObjectContext.EnterprisePartsCosts.Where
                    (r => r.OwnerCompanyId == partsOutboundPlan.StorageCompanyId && r.SparePartId == partsOutboundPlanDetail.SparePartId && r.PartsSalesCategoryId == partsOutboundPlan.PartsSalesCategoryId)).SingleOrDefault();
                if(enterprisePartsCost == null) {
                    throw new ValidationException(ErrorStrings.PartsClaimOrderNew_Validation6);
                }

                //查找中心库质量件仓库下的保留区库位
                var qualityWarehouse = ObjectContext.Warehouses.Where(r => r.StorageCompanyId == user.EnterpriseId && r.Status == (int)DcsBaseDataStatus.有效 && r.IsQualityWarehouse == true).FirstOrDefault();
                var warehouseArea = ObjectContext.WarehouseAreas.Where(r => ObjectContext.WarehouseAreaCategories.Any(d => d.Category == (int)DcsAreaType.保管区 && d.Id == r.AreaCategoryId) && r.WarehouseId == qualityWarehouse.Id && r.AreaKind == (int)DcsAreaKind.库位 && r.Status == (int)DcsBaseDataStatus.有效).FirstOrDefault();
                if(warehouseArea == null) {
                    throw new ValidationException(string.Format(ErrorStrings.PartsClaimOrderNew_Validation9, qualityWarehouse.Name));
                }

                //更新库位库存
                //var partsStock = new PartsStockAch(this.DomainService).GetPartsStocksWithLock(ObjectContext.PartsStocks.Where(r => r.WarehouseAreaId == warehouseArea.Id && r.PartId == partsOutboundPlanDetail.SparePartId)).SingleOrDefault();
                //if(partsStock == null) {
                //    throw new ValidationException("未找到有效的库存");
                //} else {
                //    partsStock.Quantity -= partsOutboundPlanDetail.PlannedAmount;
                //    if(partsStock.Quantity < 0) throw new ValidationException("未找到有效的库存");
                //    UpdateToDatabase(partsStock);
                //}

                //新增出库单清单
                var newPartsOutboundBillDetail = new PartsOutboundBillDetail();
                newPartsOutboundBillDetail.SparePartId = partsOutboundPlanDetail.SparePartId;
                newPartsOutboundBillDetail.SparePartCode = partsOutboundPlanDetail.SparePartCode;
                newPartsOutboundBillDetail.SparePartName = partsOutboundPlanDetail.SparePartName;
                newPartsOutboundBillDetail.OutboundAmount = partsOutboundPlanDetail.PlannedAmount;
                newPartsOutboundBillDetail.WarehouseAreaId = warehouseArea.Id;
                newPartsOutboundBillDetail.WarehouseAreaCode = warehouseArea.Code;
                newPartsOutboundBillDetail.SettlementPrice = (decimal)partsClaimOrderNew.MaterialManagementCost;
                newPartsOutboundBillDetail.CostPrice = enterprisePartsCost.CostPrice;
                newPartsOutboundBill.PartsOutboundBillDetails.Add(newPartsOutboundBillDetail);

                //更新企业库存
                //enterprisePartsCost.Quantity -= newPartsOutboundBillDetail.OutboundAmount;
                //enterprisePartsCost.CostAmount = enterprisePartsCost.CostAmount - (enterprisePartsCost.CostPrice * newPartsOutboundBillDetail.OutboundAmount);
                //enterprisePartsCost.ModifierId = user.Id;
                //enterprisePartsCost.ModifierName = user.Name;
                //enterprisePartsCost.ModifyTime = DateTime.Now;
                //UpdateToDatabase(enterprisePartsCost);

                ObjectContext.SaveChanges();

                partsOutboundPlan.OriginalRequirementBillId = partsSalesReturnBill.Id;
                partsOutboundPlan.OriginalRequirementBillCode = partsSalesReturnBill.Code;
                partsOutboundPlan.OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.配件销售退货单;
                partsOutboundPlan.CustomerAccountId = partsSalesOrder.CustomerAccountId;
                InsertToDatabase(partsOutboundPlan);

                newPartsOutboundBill.OriginalRequirementBillId = partsSalesReturnBill.Id;
                newPartsOutboundBill.OriginalRequirementBillCode = partsSalesReturnBill.Code;
                newPartsOutboundBill.OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.配件销售退货单;
                newPartsOutboundBill.CustomerAccountId = partsSalesOrder.CustomerAccountId;
                InsertToDatabase(newPartsOutboundBill);
            }

            //生成总部入库计划单
            var newPartsInboundPlan = new PartsInboundPlan();
            //如果供货单位是总部，退货仓库取索赔单中的供货仓库，否则取质量件仓库
            if(partsClaimOrderNew.RWarehouseCompanyId == user.EnterpriseId) {
                newPartsInboundPlan.WarehouseId = branchQualityWarehouse.Id;
                newPartsInboundPlan.WarehouseCode = branchQualityWarehouse.Code;
                newPartsInboundPlan.WarehouseName = branchQualityWarehouse.Name;

            } else {
                newPartsInboundPlan.WarehouseId = partsClaimOrderNew.ReturnWarehouseId;
                newPartsInboundPlan.WarehouseCode = partsClaimOrderNew.ReturnWarehouseCode;
                newPartsInboundPlan.WarehouseName = partsClaimOrderNew.ReturnWarehouseName;
            }
            newPartsInboundPlan.StorageCompanyId = branch.Id;
            newPartsInboundPlan.StorageCompanyCode = branch.Code;
            newPartsInboundPlan.StorageCompanyName = branch.Name;
            newPartsInboundPlan.StorageCompanyType = (int)DcsCompanyType.分公司;
            newPartsInboundPlan.BranchId = branch.Id;
            newPartsInboundPlan.BranchCode = branch.Code;
            newPartsInboundPlan.BranchName = branch.Name;
            newPartsInboundPlan.PartsSalesCategoryId = partsClaimOrderNew.PartsSalesCategoryId;
            //对方单位   如果索赔单供货单位是中心库，对方单位为中心库，否则为提报单位
            if(partsClaimOrderNew.RWarehouseCompanyId == user.EnterpriseId) {
                newPartsInboundPlan.CounterpartCompanyId = user.EnterpriseId;
                newPartsInboundPlan.CounterpartCompanyCode = user.EnterpriseCode;
                newPartsInboundPlan.CounterpartCompanyName = user.EnterpriseName;
            } else {
                newPartsInboundPlan.CounterpartCompanyId = returnCompany.Id;
                newPartsInboundPlan.CounterpartCompanyCode = returnCompany.Code;
                newPartsInboundPlan.CounterpartCompanyName = returnCompany.Name;
            }

            //源单据编号为索赔单编号
            newPartsInboundPlan.SourceId = (int)partsClaimOrderNew.Id;
            newPartsInboundPlan.SourceCode = partsClaimOrderNew.Code;
            newPartsInboundPlan.InboundType = (int)DcsPartsInboundType.销售退货;
            //原始需求单据编号为销售退货单编号
            newPartsInboundPlan.OriginalRequirementBillId = partsSalesReturnBill.Id;
            newPartsInboundPlan.OriginalRequirementBillCode = partsSalesReturnBill.Code;
            newPartsInboundPlan.OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.配件销售退货单;

            newPartsInboundPlan.Status = (int)DcsPartsInboundPlanStatus.新建;
            newPartsInboundPlan.IfWmsInterface = false;
            newPartsInboundPlan.ArrivalDate = DateTime.Now;
            newPartsInboundPlan.CreatorId = user.Id;
            newPartsInboundPlan.CreatorName = user.Name;
            newPartsInboundPlan.CreateTime = DateTime.Now;

            new PartsInboundPlanAch(this.DomainService).InsertPartsInboundPlanValidate(newPartsInboundPlan);
            newPartsInboundPlan.CustomerAccountId = partsSalesOrder.CustomerAccountId;
            var newPartsInboundPlanDetail = new PartsInboundPlanDetail();
            newPartsInboundPlanDetail.SparePartId = partsClaimOrderNew.FaultyPartsId;
            newPartsInboundPlanDetail.SparePartCode = partsClaimOrderNew.FaultyPartsCode;
            newPartsInboundPlanDetail.SparePartName = partsClaimOrderNew.FaultyPartsName;
            newPartsInboundPlanDetail.PlannedAmount = partsClaimOrderNew.Quantity ?? 0;
            newPartsInboundPlanDetail.Price = (decimal)(partsClaimOrderNew.MaterialManagementCost == null ? partsClaimOrderNew.MaterialCost : partsClaimOrderNew.MaterialManagementCost);

            newPartsInboundPlan.PartsInboundPlanDetails.Add(newPartsInboundPlanDetail);
            InsertToDatabase(newPartsInboundPlan);

            //更新索赔单状态
            partsClaimOrderNew.Status = (int)DcsPartsClaimBillStatusNew.区域已发运;
            partsClaimOrderNew.ModifierId = user.Id;
            partsClaimOrderNew.ModifierName = user.Name;
            partsClaimOrderNew.ModifyTime = DateTime.Now;

            UpdateToDatabase(partsClaimOrderNew);
        }


        public void 校验配件是否允许提报配件索赔单(PartsClaimOrderNew partsClaimOrderNew) {
            //查询配件索赔单(新)依据 配件索赔单.祸首件ID = 当前索赔单.祸首件ID，并且配件索赔单.退货企业ID = 当前索赔单.退货企业ID，并且 配件索赔单.状态<> 作废\终止。 
            //汇总查询到的条数+1， 不能大于 选择的配件销售订单清单 该配件审批数量。 也不能大于 选择的零售订单清单该 配件的 数量。 
            //如果大于给予报错: 该配件索赔次数已超出销售订单审核 \ 零售订单 审核 数量。
            var dbPartsClaimOrderNews = ObjectContext.PartsClaimOrderNews.Where(r => r.FaultyPartsId == partsClaimOrderNew.FaultyPartsId && r.ReturnCompanyId == partsClaimOrderNew.ReturnCompanyId
                && r.Status != (int)DcsPartsClaimBillStatusNew.终止 && r.Status != (int)DcsPartsClaimBillStatusNew.作废 && r.Id != partsClaimOrderNew.Id && r.PartsSalesOrderId == partsClaimOrderNew.PartsSalesOrderId);
            var returnCompany = ObjectContext.Companies.Where(r => r.Id == partsClaimOrderNew.ReturnCompanyId).FirstOrDefault();
            var partsSalesOrderDetails = ObjectContext.PartsSalesOrderDetails.Where(r => r.PartsSalesOrderId == partsClaimOrderNew.PartsSalesOrderId && r.SparePartId == partsClaimOrderNew.FaultyPartsId);

            //中心库零售订单和服务站零售订单是不同的表
            int quantity = 0;
            if(returnCompany.Type == (int)DcsCompanyType.代理库) {
                var partsRetailOrderDetails = ObjectContext.PartsRetailOrderDetails.Where(r => r.PartsRetailOrderId == partsClaimOrderNew.PartsRetailOrderId && r.SparePartId == partsClaimOrderNew.FaultyPartsId);
                quantity = partsRetailOrderDetails.Select(r => r.Quantity).Sum();
            } else {
                var partsRetailOrderDetails1 = ObjectContext.DealerRetailOrderDetails.Where(r => r.DealerPartsRetailOrderId == partsClaimOrderNew.PartsRetailOrderId && r.PartsId == partsClaimOrderNew.FaultyPartsId);
                quantity = partsRetailOrderDetails1.Select(r => r.Quantity).Sum();
            }

            var dbsum = dbPartsClaimOrderNews.Count();
            var approveQuantity = partsSalesOrderDetails.Select(r => r.ApproveQuantity).Sum();

            if(approveQuantity == null || approveQuantity == 0 || dbsum + 1 > approveQuantity || dbsum + 1 > quantity) {
                throw new ValidationException(ErrorStrings.PartsClaimOrderNew_Validation10);
            }

        }


        internal PartsOutboundPlan CreatePartsOutboundPlanByPartsClaimOrderNewForAgency(PartsClaimOrderNew partsClaimOrderNew, Warehouse agencyOutWarehouse, Company storageCompanyOfAgencyOutWarehouse, Company companyOfBranch, Warehouse returnWarehouse, Company storageCompanyOfReturnWarehouse) {
            var partsOutboundPlan = new PartsOutboundPlan();
            #region 主单赋值
            partsOutboundPlan.WarehouseId = partsClaimOrderNew.AgencyOutWarehouseId.Value;//配件出库计划.配件仓库Id=配件索赔单（新）.代理库出库仓库Id
            partsOutboundPlan.WarehouseCode = agencyOutWarehouse.Code;//配件出库计划.配件仓库编号=仓库.编号（仓库.id=配件索赔单（新）.代理库出库仓库Id）
            partsOutboundPlan.WarehouseName = agencyOutWarehouse.Name;//配件出库计划.配件仓库名称=仓库.名称（仓库.id=配件索赔单（新）.代理库出库仓库Id）
            partsOutboundPlan.StorageCompanyId = agencyOutWarehouse.StorageCompanyId;//配件出库计划.仓储企业Id=仓库.仓储企业id（仓库.id=配件索赔单（新）.代理库出库仓库Id）
            partsOutboundPlan.StorageCompanyCode = storageCompanyOfAgencyOutWarehouse.Code;//配件出库计划.仓储企业编号=企业.编号（企业.id=仓库.仓储企业id，仓库.id=配件索赔单（新）.代理库出库仓库Id）
            partsOutboundPlan.StorageCompanyName = storageCompanyOfAgencyOutWarehouse.Name;//配件出库计划.仓储企业名称=企业.名称（企业.id=仓库.仓储企业id，仓库.id=配件索赔单（新）.代理库出库仓库Id）
            partsOutboundPlan.StorageCompanyType = storageCompanyOfAgencyOutWarehouse.Type;//配件出库计划.仓储企业类型=企业.类型（企业.id=仓库.仓储企业id，仓库.id=配件索赔单（新）.代理库出库仓库Id）
            partsOutboundPlan.BranchId = partsClaimOrderNew.Branchid;//配件出库计划.营销分公司Id=配件索赔单（新）.分公司Id
            partsOutboundPlan.BranchCode = companyOfBranch.Code;//配件出库计划.营销分公司编号=企业.编号（配件索赔单（新）.分公司Id=企业.ID）
            partsOutboundPlan.BranchName = companyOfBranch.Name;//配件出库计划.营销分公司名称=企业.名称（配件索赔单（新）.分公司Id=企业.ID）
            partsOutboundPlan.CounterpartCompanyId = storageCompanyOfReturnWarehouse.Id;//配件出库计划.对方单位Id=配件索赔单（新）.退货仓库隶属企业id
            partsOutboundPlan.CounterpartCompanyCode = storageCompanyOfReturnWarehouse.Code;//配件出库计划.对方单位编号=配件索赔单（新）.退货仓库隶属企业编号
            partsOutboundPlan.CounterpartCompanyName = storageCompanyOfReturnWarehouse.Name;//配件出库计划.对方单位名称=企业.名称（配件销售退货单.销售组织隶属企业Id=企业.id）
            partsOutboundPlan.ReceivingCompanyId = storageCompanyOfReturnWarehouse.Id;//配件出库计划.收货单位Id=配件索赔单（新）.退货仓库隶属企业id
            partsOutboundPlan.ReceivingCompanyCode = storageCompanyOfReturnWarehouse.Code;//配件出库计划.收货单位编号=配件索赔单（新）.退货仓库隶属企业编号
            partsOutboundPlan.ReceivingCompanyName = storageCompanyOfReturnWarehouse.Name;//配件出库计划.收货单位名称=企业.名称（配件销售退货单.销售组织隶属企业Id=企业.id）
            partsOutboundPlan.ReceivingWarehouseId = returnWarehouse.Id;//配件出库计划.收货仓库Id=配件索赔单（新）.退货仓库id
            partsOutboundPlan.ReceivingWarehouseCode = returnWarehouse.Code;//配件出库计划.收货仓库编号=配件索赔单（新）.退货仓库编号
            partsOutboundPlan.ReceivingWarehouseName = returnWarehouse.Name;//配件出库计划.收货仓库名称=配件索赔单（新）.退货仓库名称
            partsOutboundPlan.SourceId = partsClaimOrderNew.Id;//配件出库计划.源单据Id=配件索赔单（新）.Id
            partsOutboundPlan.SourceCode = partsClaimOrderNew.Code;//配件出库计划.源单据编号=配件索赔单（新）.配件索赔单编号
            partsOutboundPlan.OutboundType = (int)DcsPartsOutboundType.质量件索赔;//配件出库计划.出库类型=质量件索赔
            partsOutboundPlan.CustomerAccountId = null;//配件出库计划.客户账户Id=''
            partsOutboundPlan.OriginalRequirementBillId = partsClaimOrderNew.Id;//配件出库计划.原始需求单据Id=配件索赔单（新）.Id
            partsOutboundPlan.OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.配件索赔单;//配件出库计划.原始需求单据类型=（配件索赔单）14
            partsOutboundPlan.OriginalRequirementBillCode = partsClaimOrderNew.Code;//配件出库计划.原始需求单据编号=配件索赔单（新）.配件索赔单编号
            partsOutboundPlan.Status = (int)DcsPartsOutboundPlanStatus.新建;//配件出库计划.状态=新建
            partsOutboundPlan.Remark = "";//配件出库计划.备注=""
            partsOutboundPlan.PartsSalesCategoryId = partsClaimOrderNew.PartsSalesCategoryId;//配件出库计划.配件销售类型Id=配件索赔单（新）.配件销售类型id
            partsOutboundPlan.IfWmsInterface = agencyOutWarehouse.WmsInterface;//(查询 BO仓库（仓库.ID=出库计划单.配件仓库Id
            var userInfo = Utils.GetCurrentUserInfo();
            partsOutboundPlan.CreatorId = userInfo.Id;
            partsOutboundPlan.CreatorName = userInfo.Name;
            partsOutboundPlan.CreateTime = DateTime.Now;
            #endregion
            var partsOutboundPlanDetail = new PartsOutboundPlanDetail();
            #region 清单赋值
            partsOutboundPlanDetail.SparePartId = partsClaimOrderNew.FaultyPartsId;//配件出库计划清单.配件Id=配件索赔单（新）.祸首件Id
            partsOutboundPlanDetail.SparePartCode = partsClaimOrderNew.FaultyPartsCode;//配件出库计划清单.配件编号=配件索赔单（新）.祸首件编号
            partsOutboundPlanDetail.SparePartName = partsClaimOrderNew.FaultyPartsName;//配件出库计划清单.配件名称=配件索赔单（新）.祸首件名称
            partsOutboundPlanDetail.PlannedAmount = partsClaimOrderNew.Quantity == null ? 0 : partsClaimOrderNew.Quantity.Value;//配件出库计划清单.计划量=配件索赔单（新）.数量
            partsOutboundPlanDetail.OutboundFulfillment = null;//配件出库计划清单.出库完成量=''
            partsOutboundPlanDetail.Price = partsClaimOrderNew.TotalAmount == null ? 0 : partsClaimOrderNew.TotalAmount.Value;//配件出库计划清单.价格=配件索赔单（新）.费用合计
            partsOutboundPlanDetail.Remark = "";//配件出库计划清单.备注=''
            #endregion
            partsOutboundPlan.PartsOutboundPlanDetails.Add(partsOutboundPlanDetail);
            return partsOutboundPlan;
        }

        internal PartsOutboundPlan CreatePartsOutboundPlanByPartsClaimOrderNewForNotAgency(PartsClaimOrderNew partsClaimOrderNew, Company companyOfBranch, Warehouse returnWarehouse, Company storageCompanyOfReturnWarehouse, Warehouse cDCReturnWarehouse) {
            var partsOutboundPlan = new PartsOutboundPlan();
            #region 主单赋值
            var company = this.ObjectContext.Companies.SingleOrDefault(r => r.Id == cDCReturnWarehouse.StorageCompanyId);
            partsOutboundPlan.WarehouseId = returnWarehouse.Id;//配件出库计划.配件仓库Id=配件索赔单（新）.代理库出库仓库Id
            partsOutboundPlan.WarehouseCode = returnWarehouse.Code;//配件出库计划.配件仓库编号=仓库.编号（仓库.id=配件索赔单（新）.代理库出库仓库Id）
            partsOutboundPlan.WarehouseName = returnWarehouse.Name;//配件出库计划.配件仓库名称=仓库.名称（仓库.id=配件索赔单（新）.代理库出库仓库Id）
            partsOutboundPlan.StorageCompanyId = returnWarehouse.StorageCompanyId;//配件出库计划.仓储企业Id=仓库.仓储企业id（仓库.id=配件索赔单（新）.代理库出库仓库Id）
            partsOutboundPlan.StorageCompanyCode = storageCompanyOfReturnWarehouse.Code;//配件出库计划.仓储企业编号=企业.编号（企业.id=仓库.仓储企业id，仓库.id=配件索赔单（新）.代理库出库仓库Id）
            partsOutboundPlan.StorageCompanyName = storageCompanyOfReturnWarehouse.Name;//配件出库计划.仓储企业名称=企业.名称（企业.id=仓库.仓储企业id，仓库.id=配件索赔单（新）.代理库出库仓库Id）
            partsOutboundPlan.StorageCompanyType = storageCompanyOfReturnWarehouse.Type;//配件出库计划.仓储企业类型=企业.类型（企业.id=仓库.仓储企业id，仓库.id=配件索赔单（新）.代理库出库仓库Id）
            partsOutboundPlan.BranchId = partsClaimOrderNew.Branchid;//配件出库计划.营销分公司Id=配件索赔单（新）.分公司Id
            partsOutboundPlan.BranchCode = companyOfBranch.Code;//配件出库计划.营销分公司编号=企业.编号（配件索赔单（新）.分公司Id=企业.ID）
            partsOutboundPlan.BranchName = companyOfBranch.Name;//配件出库计划.营销分公司名称=企业.名称（配件索赔单（新）.分公司Id=企业.ID）
            partsOutboundPlan.CounterpartCompanyId = company.Id;//配件出库计划.对方单位Id=配件索赔单（新）.退货仓库隶属企业id
            partsOutboundPlan.CounterpartCompanyCode = company.Code;//配件出库计划.对方单位编号=配件索赔单（新）.退货仓库隶属企业编号
            partsOutboundPlan.CounterpartCompanyName = company.Name;//配件出库计划.对方单位名称=企业.名称（配件销售退货单.销售组织隶属企业Id=企业.id）

            partsOutboundPlan.ReceivingCompanyId = company.Id;//配件出库计划.收货单位Id=配件索赔单（新）.退货仓库隶属企业id
            partsOutboundPlan.ReceivingCompanyCode = company.Code;//配件出库计划.收货单位编号=配件索赔单（新）.退货仓库隶属企业编号
            partsOutboundPlan.ReceivingCompanyName = company.Name;//配件出库计划.收货单位名称=企业.名称（配件销售退货单.销售组织隶属企业Id=企业.id）

            partsOutboundPlan.ReceivingWarehouseId = cDCReturnWarehouse.Id;//配件出库计划.收货仓库Id=配件索赔单（新）.退货仓库id
            partsOutboundPlan.ReceivingWarehouseCode = cDCReturnWarehouse.Code;//配件出库计划.收货仓库编号=配件索赔单（新）.退货仓库编号
            partsOutboundPlan.ReceivingWarehouseName = cDCReturnWarehouse.Name;//配件出库计划.收货仓库名称=配件索赔单（新）.退货仓库名称

            partsOutboundPlan.SourceId = partsClaimOrderNew.Id;//配件出库计划.源单据Id=配件索赔单（新）.Id

            partsOutboundPlan.SourceCode = partsClaimOrderNew.Code;//配件出库计划.源单据编号=配件索赔单（新）.配件索赔单编号
            partsOutboundPlan.OutboundType = (int)DcsPartsOutboundType.质量件索赔;//配件出库计划.出库类型=质量件索赔
            partsOutboundPlan.CustomerAccountId = null;//配件出库计划.客户账户Id=''
            partsOutboundPlan.OriginalRequirementBillId = partsClaimOrderNew.Id;//配件出库计划.原始需求单据Id=配件索赔单（新）.Id
            partsOutboundPlan.OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.配件索赔单;//配件出库计划.原始需求单据类型=（配件索赔单）14
            partsOutboundPlan.OriginalRequirementBillCode = partsClaimOrderNew.Code;//配件出库计划.原始需求单据编号=配件索赔单（新）.配件索赔单编号
            partsOutboundPlan.Status = (int)DcsPartsOutboundPlanStatus.新建;//配件出库计划.状态=新建
            partsOutboundPlan.Remark = "";//配件出库计划.备注=""
            partsOutboundPlan.PartsSalesCategoryId = partsClaimOrderNew.PartsSalesCategoryId;//配件出库计划.配件销售类型Id=配件索赔单（新）.配件销售类型id
            partsOutboundPlan.IfWmsInterface = returnWarehouse.WmsInterface;//(查询 BO仓库（仓库.ID=出库计划单.配件仓库Id
            var userInfo = Utils.GetCurrentUserInfo();
            partsOutboundPlan.CreatorId = userInfo.Id;
            partsOutboundPlan.CreatorName = userInfo.Name;
            partsOutboundPlan.CreateTime = DateTime.Now;
            #endregion
            var partsOutboundPlanDetail = new PartsOutboundPlanDetail();
            #region 清单赋值
            partsOutboundPlanDetail.SparePartId = partsClaimOrderNew.FaultyPartsId;//配件出库计划清单.配件Id=配件索赔单（新）.祸首件Id
            partsOutboundPlanDetail.SparePartCode = partsClaimOrderNew.FaultyPartsCode;//配件出库计划清单.配件编号=配件索赔单（新）.祸首件编号
            partsOutboundPlanDetail.SparePartName = partsClaimOrderNew.FaultyPartsName;//配件出库计划清单.配件名称=配件索赔单（新）.祸首件名称
            partsOutboundPlanDetail.PlannedAmount = partsClaimOrderNew.Quantity == null ? 0 : partsClaimOrderNew.Quantity.Value;//配件出库计划清单.计划量=配件索赔单（新）.数量
            partsOutboundPlanDetail.OutboundFulfillment = null;//配件出库计划清单.出库完成量=''
            partsOutboundPlanDetail.Price = partsClaimOrderNew.TotalAmount == null ? 0 : partsClaimOrderNew.TotalAmount.Value;//配件出库计划清单.价格=配件索赔单（新）.费用合计
            partsOutboundPlanDetail.Remark = "";//配件出库计划清单.备注=''
            #endregion
            partsOutboundPlan.PartsOutboundPlanDetails.Add(partsOutboundPlanDetail);
            return partsOutboundPlan;
        }




        internal PartsInboundPlan CreatePartsInboundPlanByPartsClaimOrderNew(PartsClaimOrderNew partsClaimOrderNew, Company companyOfBranch, Company returnCompany, CustomerAccount customerAccount, Warehouse returnWarehouse, Company storageCompanyOfReturnWarehouse) {
            var partsInboundPlan = new PartsInboundPlan();
            #region 主单赋值
            partsInboundPlan.WarehouseId = returnWarehouse.Id;//配件入库计划.配件仓库Id=配件索赔单（新）.退货仓库id
            partsInboundPlan.WarehouseCode = returnWarehouse.Code;//配件入库计划.配件仓库编号=配件索赔单（新）.退货仓库编号
            partsInboundPlan.WarehouseName = returnWarehouse.Name;//配件入库计划.配件仓库名称=配件索赔单（新）.退货仓库名称
            partsInboundPlan.StorageCompanyId = storageCompanyOfReturnWarehouse.Id;//配件入库计划.仓储企业Id=配件索赔单（新）.退货仓库隶属企业id
            partsInboundPlan.StorageCompanyCode = storageCompanyOfReturnWarehouse.Code;//配件入库计划.仓储企业编号=配件索赔单（新）.退货仓库隶属企业编号
            partsInboundPlan.StorageCompanyName = storageCompanyOfReturnWarehouse.Name;//配件入库计划.仓储企业名称=企业.名称（配件销售退货单.销售组织隶属企业Id=企业.id）
            partsInboundPlan.StorageCompanyType = storageCompanyOfReturnWarehouse.Type;//配件入库计划.仓储企业类型=配件索赔单（新）.退货仓库隶属企业类型
            partsInboundPlan.BranchId = partsClaimOrderNew.Branchid;//配件入库计划.营销分公司Id=配件索赔单（新）.分公司Id
            partsInboundPlan.BranchCode = companyOfBranch.Code;//配件入库计划.营销分公司编号=企业.编号（配件索赔单（新）.分公司Id=企业.ID）
            partsInboundPlan.BranchName = companyOfBranch.Name;//配件入库计划.营销分公司名称=企业.名称（配件索赔单（新）.分公司Id=企业.ID）
            partsInboundPlan.CounterpartCompanyId = returnCompany.Id;//配件入库计划.对方单位Id=配件销售退货单.提报企业Id
            partsInboundPlan.CounterpartCompanyCode = returnCompany.Code;//配件入库计划.对方单位编号=企业.编号(配件销售退货单.提报企业Id=企业.id)
            partsInboundPlan.CounterpartCompanyName = returnCompany.Name;//配件入库计划.对方单位单位名称=企业.名称（配件销售退货单.提报企业Id=企业.id）
            partsInboundPlan.SourceId = partsClaimOrderNew.Id;//配件入库计划.入库源单据Id==配件索赔单（新）.Id
            partsInboundPlan.SourceCode = partsClaimOrderNew.Code;//配件入库计划.入库源单据编号=配件索赔单（新）.配件索赔单编号
            partsInboundPlan.InboundType = (int)DcsPartsInboundType.质量件索赔;//配件入库计划.入库类型=质量件索赔
            partsInboundPlan.CustomerAccountId = customerAccount.Id;//配件入库计划.客户账户Id=客户账户.id(客户账户.账户组id=销售组织.账户组id,销售组织.配件销售类型id=配件索赔单（新）.配件销售类型id,销售组织.隶属企业Id=配件索赔单（新）.退货仓库隶属企业id,客户账户.客户企业id=配件索赔单（新）.提报企业id)
            partsInboundPlan.OriginalRequirementBillId = partsClaimOrderNew.Id;//配件入库计划.原始需求单据Id=配件索赔单（新）.Id
            partsInboundPlan.OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.配件索赔单;//配件入库计划.原始需求单据类型=（配件索赔单）14
            partsInboundPlan.OriginalRequirementBillCode = partsClaimOrderNew.Code;//配件入库计划.原始需求单据类型=配件索赔单（新）.配件索赔单编号
            partsInboundPlan.Status = (int)DcsPartsInboundPlanStatus.新建;//配件入库计划.状态=1(新建)
            partsInboundPlan.Remark = "";//配件入库计划.备注=''
            partsInboundPlan.PartsSalesCategoryId = partsClaimOrderNew.PartsSalesCategoryId;//配件入库计划.配件销售类型Id=配件索赔单（新）.配件销售类型Id
            var userInfo = Utils.GetCurrentUserInfo();
            partsInboundPlan.CreatorId = userInfo.Id;
            partsInboundPlan.CreatorName = userInfo.Name;
            partsInboundPlan.CreateTime = DateTime.Now;
            #endregion
            var partsInboundPlanDetail = new PartsInboundPlanDetail();
            #region 清单赋值
            partsInboundPlanDetail.SparePartId = partsClaimOrderNew.FaultyPartsId;//配件出库计划清单.配件Id=配件索赔单（新）.祸首件Id
            partsInboundPlanDetail.SparePartCode = partsClaimOrderNew.FaultyPartsCode;//配件出库计划清单.配件编号=配件索赔单（新）.祸首件编号
            partsInboundPlanDetail.SparePartName = partsClaimOrderNew.FaultyPartsName;//配件出库计划清单.配件名称=配件索赔单（新）.祸首件名称
            partsInboundPlanDetail.PlannedAmount = partsClaimOrderNew.Quantity == null ? 0 : partsClaimOrderNew.Quantity.Value;//配件出库计划清单.计划量=配件索赔单（新）.数量
            partsInboundPlanDetail.InspectedQuantity = null;//配件出库计划清单.出库完成量=''
            partsInboundPlanDetail.Price = partsClaimOrderNew.TotalAmount == null ? 0 : partsClaimOrderNew.TotalAmount.Value;//配件出库计划清单.价格=配件索赔单（新）.费用合计
            partsInboundPlanDetail.Remark = "";//配件出库计划清单.备注=''
            #endregion
            partsInboundPlan.PartsInboundPlanDetails.Add(partsInboundPlanDetail);
            return partsInboundPlan;
        }

        public void 校验配件索赔次数(PartsClaimOrderNew partsClaimOrderNew) {
            var partsClaimOrderNews = this.ObjectContext.PartsClaimOrderNews.Where(r => r.Status != (int)DcsPartsClaimBillStatusNew.终止 && r.Status != (int)DcsPartsClaimBillStatusNew.作废 && r.PartsRetailOrderId == partsClaimOrderNew.PartsRetailOrderId && partsClaimOrderNew.FaultyPartsId == r.FaultyPartsId).ToArray().Count();

            int query = 0;
            var company = this.ObjectContext.Companies.SingleOrDefault(r => r.Id == partsClaimOrderNew.ReturnCompanyId);
            if(company.Type == (int)DcsCompanyType.服务站兼代理库 || company.Type == (int)DcsCompanyType.服务站) {
                query = this.ObjectContext.DealerRetailOrderDetails.Where(r => r.DealerPartsRetailOrderId == partsClaimOrderNew.PartsRetailOrderId && r.PartsId == partsClaimOrderNew.FaultyPartsId).ToArray().Sum(r => r.Quantity);

            } else if(company.Type == (int)DcsCompanyType.代理库) {
                query = this.ObjectContext.PartsRetailOrderDetails.Where(r => r.PartsRetailOrderId == partsClaimOrderNew.PartsRetailOrderId && r.SparePartId == partsClaimOrderNew.FaultyPartsId).ToArray().Sum(r => r.Quantity);
            }
            if(partsClaimOrderNews >= query && query != 0) {
                throw new ValidationException(string.Format(ErrorStrings.PartsClaimOrderNew_Validation11, partsClaimOrderNew.FaultyPartsCode));
            }
        }

    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:09:50
        //原分布类的函数全部转移到Ach                                                               

        public void 修改配件索赔单新(PartsClaimOrderNew partsClaimOrderNew) {
            new PartsClaimOrderNewAch(this).修改配件索赔单新(partsClaimOrderNew);
        }
        public void 提交配件索赔单新(PartsClaimOrderNew partsClaimOrderNew) {
            new PartsClaimOrderNewAch(this).提交配件索赔单新(partsClaimOrderNew);
        }

        public void 作废配件索赔单新(PartsClaimOrderNew partsClaimOrderNew) {
            new PartsClaimOrderNewAch(this).作废配件索赔单新(partsClaimOrderNew);
        }
        public void 驳回配件索赔单新(PartsClaimOrderNew partsClaimOrderNew, string rejectReason) {
            new PartsClaimOrderNewAch(this).驳回配件索赔单新(partsClaimOrderNew, rejectReason);
        }
        public void 终止配件索赔单新(PartsClaimOrderNew partsClaimOrderNew) {
            new PartsClaimOrderNewAch(this).终止配件索赔单新(partsClaimOrderNew);
        }
        public void 审核配件索赔单新(PartsClaimOrderNew partsClaimOrderNew) {
            new PartsClaimOrderNewAch(this).审核配件索赔单新(partsClaimOrderNew);
        }
        public void 总部审核配件索赔单新(PartsClaimOrderNew partsClaimOrderNew) {
            new PartsClaimOrderNewAch(this).总部审核配件索赔单新(partsClaimOrderNew);
        }
        public void 总部初审配件索赔单新(PartsClaimOrderNew partsClaimOrderNew) {
            new PartsClaimOrderNewAch(this).总部初审配件索赔单新(partsClaimOrderNew);
        }
        public void 总部终审配件索赔单新(PartsClaimOrderNew partsClaimOrderNew) {
            new PartsClaimOrderNewAch(this).总部终审配件索赔单新(partsClaimOrderNew);
        }
        public void 服务站发运配件索赔单新(PartsClaimOrderNew partsClaimOrderNew) {
            new PartsClaimOrderNewAch(this).服务站发运配件索赔单新(partsClaimOrderNew);
        }

        public void 中心库确认收货配件索赔单新(PartsClaimOrderNew partsClaimOrderNew) {
            new PartsClaimOrderNewAch(this).中心库确认收货配件索赔单新(partsClaimOrderNew);
        }

        public void 中心库发运配件索赔单新(PartsClaimOrderNew partsClaimOrderNew) {
            new PartsClaimOrderNewAch(this).中心库发运配件索赔单新(partsClaimOrderNew);
        }
        public void 出库配件索赔单新(PartsClaimOrderNew partsClaimOrderNew) {
            new PartsClaimOrderNewAch(this).出库配件索赔单新(partsClaimOrderNew);
        }
        public void 发运配件索赔单新(PartsClaimOrderNew partsClaimOrderNew) {
            new PartsClaimOrderNewAch(this).发运配件索赔单新(partsClaimOrderNew);
        }
        public void 校验配件是否允许提报配件索赔单(PartsClaimOrderNew partsClaimOrderNew) {
            new PartsClaimOrderNewAch(this).校验配件是否允许提报配件索赔单(partsClaimOrderNew);
        }

        public void 校验配件索赔次数(PartsClaimOrderNew partsClaimOrderNew) {
            new PartsClaimOrderNewAch(this).校验配件索赔次数(partsClaimOrderNew);
        }
    }
}
