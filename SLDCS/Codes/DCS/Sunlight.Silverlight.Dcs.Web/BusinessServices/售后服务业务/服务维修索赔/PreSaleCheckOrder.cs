﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class PreSaleCheckOrderAch : DcsSerivceAchieveBase {
        public PreSaleCheckOrderAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 修改售前检查单(PreSaleCheckOrder preSaleCheckOrder) {
            var dbpreSaleCheckOrder = ObjectContext.PreSaleCheckOrders.SingleOrDefault(r => r.Id == preSaleCheckOrder.Id);
            if(dbpreSaleCheckOrder == null) {
                throw new ValidationException(ErrorStrings.PreSaleCheckOrder_Validation1);
            }
            if(dbpreSaleCheckOrder.Status != (int)DcsPreSaleStatus.新建) {
                throw new ValidationException(ErrorStrings.PreSaleCheckOrder_Validation2);
            }
        }

        public void 提交售前检查单(PreSaleCheckOrder preSaleCheckOrder) {
            var dbpreSaleCheckOrder = ObjectContext.PreSaleCheckOrders.SingleOrDefault(r => r.Id == preSaleCheckOrder.Id);
            if(dbpreSaleCheckOrder == null) {
                throw new ValidationException(ErrorStrings.PreSaleCheckOrder_Validation1);
            }
            if(dbpreSaleCheckOrder.Status != (int)DcsPreSaleStatus.新建) {
                throw new ValidationException(ErrorStrings.PreSaleCheckOrder_Validation3);
            }
            var branchstrategy = ObjectContext.Branchstrategies.SingleOrDefault(r => r.BranchId == preSaleCheckOrder.BranchId && r.Status == (int)DcsBaseDataStatus.有效);
            //var vehicleInformation = ObjectContext.VehicleInformations.SingleOrDefault(r => r.Id == preSaleCheckOrder.VehicleId);
            //if(branchstrategy == null) {
            //    throw new ValidationException("找不到对应的分公司策略");
            //}
            //if(vehicleInformation == null) {
            //    throw new ValidationException("找不到对应的车辆信息");
            //}
            //if(vehicleInformation.SalesDate != null) {
            //    if(preSaleCheckOrder.CreateTime == null) {
            //        throw new ValidationException("找不到对应的车辆信息");
            //    }
            //    if(branchstrategy.PreSaleSaleTimeStrategy < (preSaleCheckOrder.CreateTime.Value.Date - vehicleInformation.SalesDate.Value.Date).Days) {
            //        throw new ValidationException(string.Format("检查日期{0}与实销日期{1}相差{2}天以上不能提交售前检查单", preSaleCheckOrder.CreateTime.Value, vehicleInformation.SalesDate.Value, branchstrategy.PreSaleSaleTimeStrategy));
            //    }
            //}

            //var checktime = ObjectContext.PreSaleCheckOrders.Where(r => r.Id != preSaleCheckOrder.Id && r.VehicleId == preSaleCheckOrder.VehicleId && (r.Status == (int)DcsPreSaleStatus.提交 || r.Status == (int)DcsPreSaleStatus.新建));
            //var flag = (from a in ObjectContext.VehicleInformations
            //            where a.Id == preSaleCheckOrder.VehicleId
            //            select new {
            //                times = (a.PreSaleMaxTime ?? 0) - (a.PreSaleTime ?? 0) - (checktime.Count())
            //            }).Select(r => r.times).SingleOrDefault();
            //if(flag <= 0) {
            //    throw new ValidationException("售前检查次数超过最大检查次数");
            //}
            var userInfo = Utils.GetCurrentUserInfo();
            UpdateToDatabase(preSaleCheckOrder);
            preSaleCheckOrder.submittedId = userInfo.Id;
            preSaleCheckOrder.submittedName = userInfo.Name;
            preSaleCheckOrder.SubmitTime = DateTime.Now;
            preSaleCheckOrder.Status = (int)DcsPreSaleStatus.提交;
            this.UpdatePreSaleCheckOrderValidate(preSaleCheckOrder);
        }

        public void 审核售前检查单(PreSaleCheckOrder preSaleCheckOrder, bool isPassThrough) {
            UpdateToDatabase(preSaleCheckOrder);
            var dbPreSaleCheckOrder = ObjectContext.PreSaleCheckOrders.Where(r => r.Id == preSaleCheckOrder.Id && r.Status == (int)DcsPreSaleStatus.提交).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbPreSaleCheckOrder == null) {
                throw new ValidationException(ErrorStrings.PreSaleCheckOrder_Validation5);
            }
            var userInfo = Utils.GetCurrentUserInfo();
            preSaleCheckOrder.ApproverId = userInfo.Id;
            preSaleCheckOrder.ApproverName = userInfo.Name;
            preSaleCheckOrder.ApproveTime = DateTime.Now;
            if(isPassThrough == false) {
                preSaleCheckOrder.Status = (int)DcsPreSaleStatus.审核不通过;
            }else {
                preSaleCheckOrder.Status = (int)DcsPreSaleStatus.已审核;
                //var vehicleInfo = ObjectContext.VehicleInformations.Single(r => r.Id == preSaleCheckOrder.VehicleId);
                //if(vehicleInfo.PreSaleMaxTime == null) {
                //    throw new ValidationException("未正确设置售前检查次数，请联系相关管理人员");
                //}else {
                //    if((vehicleInfo.PreSaleTime ?? 0) < vehicleInfo.PreSaleMaxTime) {
                //        if(vehicleInfo.PreSaleTime == null)
                //            vehicleInfo.PreSaleTime = 0;
                //        vehicleInfo.PreSaleTime++;
                //        UpdateToDatabase(vehicleInfo);
                //    }else {
                //        throw new ValidationException(ErrorStrings.PreSaleCheckOrder_Validation6);
                //    }
                //}
            }
        }

        public string 批量审核售前检查单(bool isPassThrough, int[] ids) {
            var preSaleCheckOrders = ObjectContext.PreSaleCheckOrders.Where(r => ids.Contains(r.Id)).ToList();
            if(preSaleCheckOrders.Any(r => r.Status != (int)DcsPreSaleStatus.提交)) {
                throw new ValidationException(ErrorStrings.PreSaleCheckOrder_Validation5);
            }
            string result = null;
            var userInfo = Utils.GetCurrentUserInfo();
            var errorMsg = new List<string>();
            if(isPassThrough == false) {
                foreach(var preSaleCheckOrder in preSaleCheckOrders) {
                    UpdateToDatabase(preSaleCheckOrder);
                    preSaleCheckOrder.Status = (int)DcsPreSaleStatus.审核不通过;
                    preSaleCheckOrder.ModifierId = userInfo.Id;
                    preSaleCheckOrder.ModifierName = userInfo.Name;
                    preSaleCheckOrder.ModifyTime = DateTime.Now;
                    preSaleCheckOrder.ApproverId = userInfo.Id;
                    preSaleCheckOrder.ApproverName = userInfo.Name;
                    preSaleCheckOrder.ApproveTime = DateTime.Now;
                }
            }else {
                var vehicleIds = preSaleCheckOrders.Select(r => r.VehicleId).Distinct().ToArray();
                //var vehicleInfos = ObjectContext.VehicleInformations.Where(r => vehicleIds.Contains(r.Id)).ToArray();
                //foreach(var preSaleCheckOrder in preSaleCheckOrders) {
                //    var tempVehicleInfo = vehicleInfos.SingleOrDefault(r => r.Id == preSaleCheckOrder.VehicleId);
                //    if(tempVehicleInfo != null) {
                //        if((tempVehicleInfo.PreSaleTime ?? 0) < tempVehicleInfo.PreSaleMaxTime) {
                //            preSaleCheckOrder.Status = (int)DcsPreSaleStatus.已审核;
                //            preSaleCheckOrder.ModifierId = userInfo.Id;
                //            preSaleCheckOrder.ModifierName = userInfo.Name;
                //            preSaleCheckOrder.ModifyTime = DateTime.Now;
                //            preSaleCheckOrder.ApproverId = userInfo.Id;
                //            preSaleCheckOrder.ApproverName = userInfo.Name;
                //            preSaleCheckOrder.ApproveTime = DateTime.Now;
                //            UpdateToDatabase(preSaleCheckOrder);
                //            if(tempVehicleInfo.PreSaleTime == null)
                //                tempVehicleInfo.PreSaleTime = 0;
                //            tempVehicleInfo.PreSaleTime++;
                //            UpdateToDatabase(tempVehicleInfo);
                //        }else {
                //            errorMsg.Add(tempVehicleInfo.VIN);
                //        }
                //    }
                //}
            }
            if(errorMsg.Count > 0) {
                result = "售前检查次数超出最大允许次数的车辆信息:" + string.Join(", ", errorMsg);
            }
            ObjectContext.SaveChanges();
            return result;
        }

        public void 作废售前检查单(PreSaleCheckOrder preSaleCheckOrder) {
            var dbpreSaleCheckOrder = ObjectContext.PreSaleCheckOrders.Where(r => r.Id == preSaleCheckOrder.Id).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbpreSaleCheckOrder == null) {
                throw new ValidationException(ErrorStrings.PreSaleCheckOrder_Validation1);
            }
            if(dbpreSaleCheckOrder.Status != (int)DcsPreSaleStatus.新建 && dbpreSaleCheckOrder.Status != (int)DcsPreSaleStatus.提交) {
                throw new ValidationException(ErrorStrings.PreSaleCheckOrder_Validation4);
            }
            var userInfo = Utils.GetCurrentUserInfo();
            UpdateToDatabase(preSaleCheckOrder);
            preSaleCheckOrder.Status = (int)DcsPreSaleStatus.作废;
            preSaleCheckOrder.AbandonerId = userInfo.Id;
            preSaleCheckOrder.AbandonerName = userInfo.Name;
            preSaleCheckOrder.AbandonTime = DateTime.Now;
            this.UpdatePreSaleCheckOrderValidate(preSaleCheckOrder);
            //if(preSaleCheckOrder.RepairWorkOrderId.HasValue) {
            //    var repairWorkOrder = ObjectContext.RepairWorkOrders.SingleOrDefault(r => r.Id == preSaleCheckOrder.RepairWorkOrderId.Value);
            //    if(repairWorkOrder != null && repairWorkOrder.Status == (int)DcsRepairWorkOrderStatus.已维修) {
            //        repairWorkOrder.Status = (int)DcsRepairWorkOrderStatus.新建;
            //        DomainService.UpdateRepairWorkOrder(repairWorkOrder);
            //    }else {
            //        throw new ValidationException("维修工单状态不正确，无法作废");
            //    }
            //}
        }

        public void 维修工单生成售前检查(PreSaleCheckOrder preSaleCheckOrder) {
            var repairWorkOrderId = preSaleCheckOrder.RepairWorkOrderId == null ? -1 : preSaleCheckOrder.RepairWorkOrderId.Value;
            //var repairWorkOrder = ObjectContext.RepairWorkOrders.SingleOrDefault(r => r.Id == repairWorkOrderId);
            //if(repairWorkOrder != null && repairWorkOrder.Status == (int)DcsRepairWorkOrderStatus.新建) {
            //    repairWorkOrder.Status = (int)DcsRepairWorkOrderStatus.已维修;
            //    DomainService.UpdateRepairWorkOrder(repairWorkOrder);
            //}else {
            //    throw new ValidationException("维修工单状态不正确，无法生成售前检查");
            //}
        }

        public void 驳回售前检查单(PreSaleCheckOrder preSaleCheckOrder) {
            var dbpreSaleCheckOrder = ObjectContext.PreSaleCheckOrders.Where(r => r.Id == preSaleCheckOrder.Id && (r.Status == (int)DcsPreSaleStatus.提交)).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbpreSaleCheckOrder == null)
                throw new ValidationException(ErrorStrings.PreSaleCheckOrder_Validation7);
            UpdateToDatabase(preSaleCheckOrder);
            var userInfo = Utils.GetCurrentUserInfo();
            preSaleCheckOrder.RejectId = userInfo.Id;
            preSaleCheckOrder.RejectName = userInfo.Name;
            preSaleCheckOrder.RejectTime = DateTime.Now;
            preSaleCheckOrder.Status = (int)DcsServiceTripClaimApplicationStatus.新增;
            this.UpdatePreSaleCheckOrderValidate(preSaleCheckOrder);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:09:50
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 修改售前检查单(PreSaleCheckOrder preSaleCheckOrder) {
            new PreSaleCheckOrderAch(this).修改售前检查单(preSaleCheckOrder);
        }
        
        [Update(UsingCustomMethod = true)]
        public void 提交售前检查单(PreSaleCheckOrder preSaleCheckOrder) {
            new PreSaleCheckOrderAch(this).提交售前检查单(preSaleCheckOrder);
        }
        
        [Update(UsingCustomMethod = true)]
        public void 审核售前检查单(PreSaleCheckOrder preSaleCheckOrder, bool isPassThrough) {
            new PreSaleCheckOrderAch(this).审核售前检查单(preSaleCheckOrder,isPassThrough);
        }
        
        [Invoke(HasSideEffects = true)]
        public string 批量审核售前检查单(bool isPassThrough, int[] ids) {
            return new PreSaleCheckOrderAch(this).批量审核售前检查单(isPassThrough,ids);
        }
        
        [Update(UsingCustomMethod = true)]
        public void 作废售前检查单(PreSaleCheckOrder preSaleCheckOrder) {
            new PreSaleCheckOrderAch(this).作废售前检查单(preSaleCheckOrder);
        }
        
        [Update(UsingCustomMethod = true)]
        public void 维修工单生成售前检查(PreSaleCheckOrder preSaleCheckOrder) {
            new PreSaleCheckOrderAch(this).维修工单生成售前检查(preSaleCheckOrder);
        }
        
        [Update(UsingCustomMethod = true)]
        public void 驳回售前检查单(PreSaleCheckOrder preSaleCheckOrder) {
            new PreSaleCheckOrderAch(this).驳回售前检查单(preSaleCheckOrder);
        }
    }
}
