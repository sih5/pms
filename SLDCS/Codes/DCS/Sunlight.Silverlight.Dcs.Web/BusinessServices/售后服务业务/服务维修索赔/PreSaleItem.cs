﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class PreSaleItemAch : DcsSerivceAchieveBase {
        public void 修改售前检查项目(PreSaleItem preSaleItem) {
            var dbpreSaleItems = ObjectContext.PreSaleItems.Where(r => r.Id == preSaleItem.Id && r.BranchId == preSaleItem.BranchId && r.PartsSalesCategoryId == preSaleItem.PartsSalesCategoryId).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbpreSaleItems == null) {
                throw new ValidationException(ErrorStrings.PreSaleItem_Validation1);
            }
            if(dbpreSaleItems.Status != (int)DcsBaseDataStatus.有效) {
                throw new ValidationException(ErrorStrings.PreSaleItem_Validation2);
            }
            UpdatePreSaleItemValidate(dbpreSaleItems);
        }
        public void 作废售前检查项目(PreSaleItem preSaleItem) {
            var dbpreSaleItems = ObjectContext.PreSaleItems.Where(r => r.Id == preSaleItem.Id).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbpreSaleItems == null) {
                throw new ValidationException(ErrorStrings.PreSaleItem_Validation1);
            }
            if(dbpreSaleItems.Status != (int)DcsBaseDataStatus.有效) {
                throw new ValidationException(ErrorStrings.PreSaleItem_Validation2);
            }
            UpdateToDatabase(dbpreSaleItems);
            dbpreSaleItems.Status = (int)DcsBaseDataStatus.作废;
            UpdatePreSaleItemValidate(dbpreSaleItems);
        }
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 16:05:07
        [Update(UsingCustomMethod = true)]
        public void 修改售前检查项目(PreSaleItem preSaleItem) {
            new PreSaleItemAch(this).修改售前检查项目(preSaleItem);
        }
        [Update(UsingCustomMethod = true)]
        public void 作废售前检查项目(PreSaleItem preSaleItem) {
            new PreSaleItemAch(this).作废售前检查项目(preSaleItem);
        }
    }
}
