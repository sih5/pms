﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class SupplierExpenseAdjustBillAch : DcsSerivceAchieveBase {
        public SupplierExpenseAdjustBillAch(DcsDomainService domainService)
            : base(domainService) {
        }

        public void 审批供应商扣补款单(SupplierExpenseAdjustBill supplierExpenseAdjustBill) {
            var dbSupplierExpenseAdjustBill = ObjectContext.SupplierExpenseAdjustBills.Where(r => r.Id == supplierExpenseAdjustBill.Id && r.Status == (int)DcsExpenseAdjustmentBillStatus.新增).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbSupplierExpenseAdjustBill == null) {
                throw new ValidationException("只能对新增状态的单据进行操作");
            }
            var userInfo = Utils.GetCurrentUserInfo();
            supplierExpenseAdjustBill.ModifierId = userInfo.Id;
            supplierExpenseAdjustBill.ModifierName = userInfo.Name;
            supplierExpenseAdjustBill.ModifyTime = DateTime.Now;
            supplierExpenseAdjustBill.ApproverId = userInfo.Id;
            supplierExpenseAdjustBill.ApproverName = userInfo.Name;
            supplierExpenseAdjustBill.ApproveTime = DateTime.Now;
            supplierExpenseAdjustBill.Status = (int)DcsExpenseAdjustmentBillStatus.生效;
            UpdateToDatabase(supplierExpenseAdjustBill);
        }

        public void 作废供应商扣补款单(SupplierExpenseAdjustBill supplierExpenseAdjustBill) {
            var dbSupplierExpenseAdjustBill = ObjectContext.SupplierExpenseAdjustBills.Where(r => r.Id == supplierExpenseAdjustBill.Id && r.Status == (int)DcsExpenseAdjustmentBillStatus.新增).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbSupplierExpenseAdjustBill == null) {
                throw new ValidationException("只能对新增状态的单据进行操作");
            }
            var userInfo = Utils.GetCurrentUserInfo();
            supplierExpenseAdjustBill.ModifierId = userInfo.Id;
            supplierExpenseAdjustBill.ModifierName = userInfo.Name;
            supplierExpenseAdjustBill.ModifyTime = DateTime.Now;
            supplierExpenseAdjustBill.AbandonerId = userInfo.Id;
            supplierExpenseAdjustBill.AbandonerName = userInfo.Name;
            supplierExpenseAdjustBill.AbandonTime = DateTime.Now;
            supplierExpenseAdjustBill.Status = (int)DcsExpenseAdjustmentBillStatus.作废;
            UpdateToDatabase(supplierExpenseAdjustBill);
        }

        public void 批量审批供应商扣补款单(bool isPassThrough, int[] ids) {
            var nowTime = DateTime.Now;
            var dbsupplierExpenseAdjustBills = ObjectContext.SupplierExpenseAdjustBills.Where(r => ids.Contains(r.Id)).ToArray();
            if(dbsupplierExpenseAdjustBills.Any(r => r.Status != (int)DcsExpenseAdjustmentBillStatus.新增))
                throw new ValidationException("只能审批新增状态供应商扣补款单");
            var userInfo = Utils.GetCurrentUserInfo();
            if(isPassThrough) {
                foreach(var supplierExpenseAdjustBill in dbsupplierExpenseAdjustBills) {
                    UpdateToDatabase(supplierExpenseAdjustBill);
                    supplierExpenseAdjustBill.ModifierId = userInfo.Id;
                    supplierExpenseAdjustBill.ModifierName = userInfo.Name;
                    supplierExpenseAdjustBill.ModifyTime = nowTime;
                    supplierExpenseAdjustBill.ApproverId = userInfo.Id;
                    supplierExpenseAdjustBill.ApproverName = userInfo.Name;
                    supplierExpenseAdjustBill.ApproveTime = nowTime;
                    supplierExpenseAdjustBill.Status = (int)DcsExpenseAdjustmentBillStatus.生效;
                }
            }else {
                foreach(var supplierExpenseAdjustBill in dbsupplierExpenseAdjustBills) {
                    UpdateToDatabase(supplierExpenseAdjustBill);
                    supplierExpenseAdjustBill.ModifierId = userInfo.Id;
                    supplierExpenseAdjustBill.ModifierName = userInfo.Name;
                    supplierExpenseAdjustBill.ModifyTime = nowTime;
                    supplierExpenseAdjustBill.AbandonerId = userInfo.Id;
                    supplierExpenseAdjustBill.AbandonerName = userInfo.Name;
                    supplierExpenseAdjustBill.AbandonTime = nowTime;
                    supplierExpenseAdjustBill.Status = (int)DcsExpenseAdjustmentBillStatus.作废;
                }
            }
            ObjectContext.SaveChanges();
        }
        public SupplierExpenseAdjustBill GetSupplierExpenseAdjustBillById(int id) {
            return ObjectContext.SupplierExpenseAdjustBills.Include("ResponsibleUnit").Include("PartsSalesCategory").Include("ServiceProductLine").SingleOrDefault(r => r.Id == id);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 14:21:36
        //原分布类的函数全部转移到Ach                                                               

        public void 审批供应商扣补款单(SupplierExpenseAdjustBill supplierExpenseAdjustBill) {
            new SupplierExpenseAdjustBillAch(this).审批供应商扣补款单(supplierExpenseAdjustBill);
        }
        
        public void 作废供应商扣补款单(SupplierExpenseAdjustBill supplierExpenseAdjustBill) {
            new SupplierExpenseAdjustBillAch(this).作废供应商扣补款单(supplierExpenseAdjustBill);
        }
        
        [Invoke(HasSideEffects = true)]
        public void 批量审批供应商扣补款单(bool isPassThrough, int[] ids) {
            new SupplierExpenseAdjustBillAch(this).批量审批供应商扣补款单(isPassThrough,ids);
        }

        public SupplierExpenseAdjustBill GetSupplierExpenseAdjustBillById(int id) {
            return new SupplierExpenseAdjustBillAch(this).GetSupplierExpenseAdjustBillById(id);
        }
    }
}
