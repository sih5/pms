﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using System.Text;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class TransactionInterfaceLogAch : DcsSerivceAchieveBase {
        public TransactionInterfaceLogAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 手动设置会员单据(int id) {
            var transactionInterfaceLog = ObjectContext.TransactionInterfaceLogs.Where(r => r.Id==id).FirstOrDefault();
            if(transactionInterfaceLog == null) {
                return;
            }
            //var repairOrder = ObjectContext.RepairOrders.FirstOrDefault(r => r.Id == transactionInterfaceLog.RepairOrderID);
            //if(repairOrder == null)
            //    return;
            //var partsSalesCategory = ObjectContext.PartsSalesCategories.FirstOrDefault(r => r.Id == repairOrder.PartsSalesCategoryId);
            //if(partsSalesCategory == null)
            //    return;
    
            if(Convert.ToDecimal(transactionInterfaceLog.MemberUsedIntegral) != 0 || transactionInterfaceLog.TransactionInterfaceLists.Any(r => r.IsRights == true)) {
                //new RepairOrderForSecondaryStationAch(this.DomainService).扣减积分权益(repairOrder, partsSalesCategory);
                var integralClaimBill = new IntegralClaimBill();
                //if(string.IsNullOrWhiteSpace(integralClaimBill.IntegralClaimBillCode) || integralClaimBill.IntegralClaimBillCode == GlobalVar.ASSIGNED_BY_SERVER)
                //    integralClaimBill.IntegralClaimBillCode = CodeGenerator.Generate("IntegralClaimBill", repairOrder.DealerCode);
                //integralClaimBill.RedPacketsCode = null;
                //integralClaimBill.RedPacketsPhone = null;
                //integralClaimBill.RedPacketsUsedPrice = null;
                //integralClaimBill.IntegralClaimBillType = (int)DcsDealerSettleType.积分类型;
                //integralClaimBill.RepairOrderId = Convert.ToInt32(transactionInterfaceLog.RepairOrderID);
                //integralClaimBill.RepairCode = repairOrder.Code;
                //integralClaimBill.DealerId = repairOrder.DealerId;
                //integralClaimBill.DealerCode = repairOrder.DealerCode;
                //integralClaimBill.DealerName = repairOrder.DealerName;
                //integralClaimBill.DealerBusinessCode = repairOrder.Company.CustomerCode;
                //integralClaimBill.IntegralStatus = (int)DcsIntegralStatus.新增;
                //integralClaimBill.SettlementStatus = (int)DcsSettlementStatus.未结算;
                //integralClaimBill.BranchCode = repairOrder.Branch.Code;
                //integralClaimBill.BranchId = repairOrder.Branch.Id;
                //integralClaimBill.BranchName = repairOrder.Branch.Name;
                //integralClaimBill.BrandId = repairOrder.PartsSalesCategory.Id;
                //integralClaimBill.BrandName = repairOrder.PartsSalesCategory.Name;
                //integralClaimBill.MemberCode = transactionInterfaceLog.MemberNumber;
                //integralClaimBill.MemberName = transactionInterfaceLog.MemberName;
                //integralClaimBill.MemberPhone = transactionInterfaceLog.MemberPhone;
                //integralClaimBill.IDType = transactionInterfaceLog.IDType;
                //integralClaimBill.IDNumer = transactionInterfaceLog.IDNumer;
                //integralClaimBill.MemberRank = transactionInterfaceLog.MemberRank;
                //integralClaimBill.MemberUsedIntegral = transactionInterfaceLog.MemberUsedIntegral;
                //integralClaimBill.MemberUsedPrice = Convert.ToDecimal(transactionInterfaceLog.MemberUsedIntegral) / 100 + Convert.ToDecimal(transactionInterfaceLog.MemberRightsCost);
                //integralClaimBill.MemberRightsCost = transactionInterfaceLog.MemberRightsCost;
                //integralClaimBill.DeductionTotalAmount = transactionInterfaceLog.DeductionTotalAmount;
                //integralClaimBill.DeductionLaborCost = (Convert.ToDecimal(transactionInterfaceLog.LaborCost) - Convert.ToDecimal(transactionInterfaceLog.CostDiscount));
                //integralClaimBill.CostDiscount = transactionInterfaceLog.CostDiscount;
                //integralClaimBill.Discount = transactionInterfaceLog.Discount;
                //integralClaimBill.ExistingIntegral = transactionInterfaceLog.MemberValue;
                //foreach(var item in transactionInterfaceLog.TransactionInterfaceLists.ToList()) {
                //    var integralClaimBillDetail = new IntegralClaimBillDetail();
                //    integralClaimBillDetail.RightsItem = item.RightsItem;
                //    integralClaimBillDetail.IsRights = item.IsRights;
                //    integralClaimBillDetail.BenefiType = item.BenefiType;
                //    integralClaimBillDetail.BenefiUom = item.BenefiUom;
                //    integralClaimBillDetail.DiscountNum = item.DiscountNum;
                //    integralClaimBillDetail.Time = item.Time;
                //    integralClaimBillDetail.RightsCode = item.RightsCode;
                //    integralClaimBill.IntegralClaimBillDetails.Add(integralClaimBillDetail);
                //}
                //DomainService.InsertIntegralClaimBill(integralClaimBill);
            }
            //new RepairOrderForSecondaryStationAch(this.DomainService).积分累加(repairOrder, partsSalesCategory);
            //repairOrder.BonusPointsStatus = (int)DcsBonusPointsStatus.已使用积分;
            //repairOrder.MemberHandStatus = (int)DcsMemberHandStatus.处理成功;
            //repairOrder.FrozenStatus = (int)DcsFrozenStatus.未冻结;
            //UpdateToDatabase(repairOrder);
            transactionInterfaceLog.SyncStatus = (int)DcsSynchronousState.处理成功;
            transactionInterfaceLog.FrozenStatus = (int)DcsFrozenStatus.未冻结;
            UpdateToDatabase(transactionInterfaceLog);
            this.ObjectContext.SaveChanges();
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 14:21:36
        //原分布类的函数全部转移到Ach                                                               

        [Invoke(HasSideEffects = true)]
        public void 手动设置会员单据(int id) {
            new TransactionInterfaceLogAch(this).手动设置会员单据(id);
        }
    }
}
