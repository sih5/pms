﻿using System;
using System.ServiceModel.DomainServices.Server;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class GradeCoefficientAch : DcsSerivceAchieveBase {
        public GradeCoefficientAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 作废星级系数(GradeCoefficient gradeCoefficient) {
            gradeCoefficient.Status = (int)DcsBaseDataStatus.作废;
            var userInfo = Utils.GetCurrentUserInfo();
            gradeCoefficient.AbandonerId = userInfo.Id;
            gradeCoefficient.AbandonerName = userInfo.Name;
            gradeCoefficient.AbandonTime = DateTime.Now;
            UpdateToDatabase(gradeCoefficient);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:06
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 作废星级系数(GradeCoefficient gradeCoefficient) {
            new GradeCoefficientAch(this).作废星级系数(gradeCoefficient);
        }
    }
}
