﻿using System.ComponentModel.DataAnnotations;
using System.ServiceModel.DomainServices.Server;
using Sunlight.Silverlight.Dcs.Web.Resources;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class LaborHourUnitPriceRateAch : DcsSerivceAchieveBase {
        public LaborHourUnitPriceRateAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 作废工时单价标准(LaborHourUnitPriceRate laborHourUnitPriceRate) {
            if(laborHourUnitPriceRate.Status != (int)DcsBaseDataStatus.有效) {
                throw new ValidationException(ErrorStrings.LaborHourUnitPriceRate_Validation1);
            }
            UpdateToDatabase(laborHourUnitPriceRate);
            laborHourUnitPriceRate.Status = (int)DcsBaseDataStatus.作废;
            this.UpdateLaborHourUnitPriceRateValidate(laborHourUnitPriceRate);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:06
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 作废工时单价标准(LaborHourUnitPriceRate laborHourUnitPriceRate) {
            new LaborHourUnitPriceRateAch(this).作废工时单价标准(laborHourUnitPriceRate);
        }
    }
}
