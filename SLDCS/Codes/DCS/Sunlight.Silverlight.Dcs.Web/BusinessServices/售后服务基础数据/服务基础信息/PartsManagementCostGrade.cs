﻿using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Sunlight.Silverlight.Dcs.Web.Resources;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsManagementCostGradeAch : DcsSerivceAchieveBase {
        public PartsManagementCostGradeAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 作废索赔配件管理费率等级(PartsManagementCostGrade partsManagementCostGrade) {
            if(partsManagementCostGrade.Status != (int)DcsBaseDataStatus.有效) {
                throw new ValidationException(ErrorStrings.PartsManagementCostGrade_Validation3);
            }
            var partsManagementCostRates = ObjectContext.PartsManagementCostRates.Where(r => r.PartsManagementCostGradeId == partsManagementCostGrade.Id && r.Status == (int)DcsBaseDataStatus.有效);
            foreach(var partsManagementCostRate in partsManagementCostRates) {
                partsManagementCostRate.Status = (int)DcsBaseDataStatus.作废;
                UpdateToDatabase(partsManagementCostRate);
            }
            UpdateToDatabase(partsManagementCostGrade);
            partsManagementCostGrade.Status = (int)DcsBaseDataStatus.作废;
            this.UpdatePartsManagementCostGradeValidate(partsManagementCostGrade);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:06
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 作废索赔配件管理费率等级(PartsManagementCostGrade partsManagementCostGrade) {
            new PartsManagementCostGradeAch(this).作废索赔配件管理费率等级(partsManagementCostGrade);
        }
    }
}
