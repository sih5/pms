﻿using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsManagementCostRateAch : DcsSerivceAchieveBase {
        public PartsManagementCostRateAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 校验配件管理费率标准(PartsManagementCostRate partsManagementCostRate) {
            if(partsManagementCostRate.EntityState == EntityState.Added) {
                var dbPartsManagementCostRate = ObjectContext.PartsManagementCostRates.Where(r => r.PartsManagementCostGradeId == partsManagementCostRate.PartsManagementCostGradeId && !(r.PartsPriceLowerLimit >= partsManagementCostRate.PartsPriceUpperLimit || r.PartsPriceUpperLimit <= partsManagementCostRate.PartsPriceLowerLimit) && r.InvoiceType == partsManagementCostRate.InvoiceType && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
                if(dbPartsManagementCostRate != null) {
                    throw new ValidationException(ErrorStrings.PartsManagementCostRate_Validation1);
                }
            }
        }

        public void 作废配件管理费率标准(PartsManagementCostRate partsManagementCostRate) {
            if(partsManagementCostRate.Status != (int)DcsBaseDataStatus.有效) {
                throw new ValidationException(ErrorStrings.PartsManagementCostRate_Validation5);
            }
            UpdateToDatabase(partsManagementCostRate);
            partsManagementCostRate.Status = (int)DcsBaseDataStatus.作废;
            this.UpdatePartsManagementCostRateValidate(partsManagementCostRate);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:06
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 校验配件管理费率标准(PartsManagementCostRate partsManagementCostRate) {
            new PartsManagementCostRateAch(this).校验配件管理费率标准(partsManagementCostRate);
        }
        
        [Update(UsingCustomMethod = true)]
        public void 作废配件管理费率标准(PartsManagementCostRate partsManagementCostRate) {
            new PartsManagementCostRateAch(this).作废配件管理费率标准(partsManagementCostRate);
        }
    }
}
