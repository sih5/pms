﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class AuthenticationTypeAch : DcsSerivceAchieveBase {
        public AuthenticationTypeAch(DcsDomainService domainService)
            : base(domainService) {
        }
        
        public void 作废认证类型(AuthenticationType authenticationType) {
            var dbauthenticationType = ObjectContext.AuthenticationTypes.Where(r => r.Id == authenticationType.Id && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbauthenticationType == null)
                throw new ValidationException("只能作废处于“有效”状态的培训类型信息");
            UpdateToDatabase(authenticationType);
            var userInfo = Utils.GetCurrentUserInfo();
            authenticationType.Status = (int)DcsBaseDataStatus.作废;
            authenticationType.CancelTime = DateTime.Now;
            authenticationType.CancelId = userInfo.Id;
            authenticationType.CancelName = userInfo.Name;
            this.UpdateAuthenticationTypeValidate(authenticationType);
        }

        public void 修改认证类型(AuthenticationType authenticationType) {
            var dbauthenticationType = ObjectContext.AuthenticationTypes.Where(r => r.Id == authenticationType.Id && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbauthenticationType == null)
                throw new ValidationException("只能修改处于“有效”状态的培训类型信息");
            UpdateToDatabase(authenticationType);
            authenticationType.Status = (int)DcsBaseDataStatus.有效;
            this.UpdateAuthenticationTypeValidate(authenticationType);
        }
    
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:15
        //原分布类的函数全部转移到Ach                                                               
        

        [Update(UsingCustomMethod = true)]
        public void 作废认证类型(AuthenticationType authenticationType) {
            new AuthenticationTypeAch(this).作废认证类型(authenticationType);
        }        

        [Update(UsingCustomMethod = true)]
        public void 修改认证类型(AuthenticationType authenticationType) {
            new AuthenticationTypeAch(this).修改认证类型(authenticationType);
        }
    }
}
