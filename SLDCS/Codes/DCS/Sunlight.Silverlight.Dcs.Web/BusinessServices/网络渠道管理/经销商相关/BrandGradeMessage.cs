﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using System.Text;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class BrandGradeMessageAch : DcsSerivceAchieveBase {
        public BrandGradeMessageAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 作废品牌星级信息(BrandGradeMessage brandGradeMessage) {
            brandGradeMessage.Status = (int)DcsBaseDataStatus.作废;
            var userInfo = Utils.GetCurrentUserInfo();
            brandGradeMessage.AbandonerId = userInfo.Id;
            brandGradeMessage.AbandonerName = userInfo.Name;
            brandGradeMessage.AbandonTime = DateTime.Now;
            UpdateToDatabase(brandGradeMessage);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:15
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 作废品牌星级信息(BrandGradeMessage brandGradeMessage) {
            new BrandGradeMessageAch(this).作废品牌星级信息(brandGradeMessage);
        }
    }
}
