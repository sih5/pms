﻿using Sunlight.Silverlight.Web;
using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class ChannelCapabilityAch : DcsSerivceAchieveBase {
        public ChannelCapabilityAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 修改网络业务能力(ChannelCapability channelCapability) {
            CheckEntityState(channelCapability);
            var dbchannelCapability = ObjectContext.ChannelCapabilities.Where(r => r.Id == channelCapability.Id && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbchannelCapability == null)
                throw new ValidationException(ErrorStrings.ChannelCapability_Validation1);
            if(ObjectContext.DealerServiceInfoes.Any(r => r.ChannelCapabilityId == channelCapability.Id && r.Status == (int)DcsMasterDataStatus.有效))
                throw new ValidationException(ErrorStrings.ChannelCapability_Validation2);
        }

        public void 作废网络业务能力(ChannelCapability channelCapability) {
            var dbchannelCapability = ObjectContext.ChannelCapabilities.Where(r => r.Id == channelCapability.Id && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbchannelCapability == null)
                throw new ValidationException(ErrorStrings.ChannelCapability_Validation3);
            if(ObjectContext.DealerServiceInfoes.Any(r => r.ChannelCapabilityId == channelCapability.Id && r.Status == (int)DcsMasterDataStatus.有效))
                throw new ValidationException(ErrorStrings.ChannelCapability_Validation4);
            UpdateToDatabase(channelCapability);
            channelCapability.Status = (int)DcsBaseDataStatus.作废;
            var userInfo = Utils.GetCurrentUserInfo();
            channelCapability.AbandonerId = userInfo.Id;
            channelCapability.AbandonerName = userInfo.Name;
            channelCapability.AbandonTime = DateTime.Now;
            this.UpdateChannelCapabilityValidate(channelCapability);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:15
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 修改网络业务能力(ChannelCapability channelCapability) {
            new ChannelCapabilityAch(this).修改网络业务能力(channelCapability);
        }
        
        [Update(UsingCustomMethod = true)]
        public void 作废网络业务能力(ChannelCapability channelCapability) {
            new ChannelCapabilityAch(this).作废网络业务能力(channelCapability);
        }
    }
}
