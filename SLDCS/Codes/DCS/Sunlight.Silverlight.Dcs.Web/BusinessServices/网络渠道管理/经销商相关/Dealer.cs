﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class DealerAch : DcsSerivceAchieveBase {
        public DealerAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 停用经销商信息(Dealer dealer) {
            var dbdealer = ObjectContext.Dealers.Where(r => r.Id == dealer.Id && r.Status == (int)DcsMasterDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbdealer == null)
                throw new ValidationException(ErrorStrings.Dealer_Validation3);
            UpdateToDatabase(dealer);
            dealer.Status = (int)DcsMasterDataStatus.停用;
            this.UpdateDealerValidate(dealer);
            创建经销商基本信息履历_修改(dbdealer, dealer);
        }

        public void 恢复经销商基本信息(Dealer dealer) {
            var dbdealer = ObjectContext.Dealers.Where(r => r.Id == dealer.Id && r.Status == (int)DcsMasterDataStatus.停用).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbdealer == null)
                throw new ValidationException(ErrorStrings.Dealer_Validation4);
            UpdateToDatabase(dealer);
            dealer.Status = (int)DcsMasterDataStatus.有效;
            this.UpdateDealerValidate(dealer);
            创建经销商基本信息履历_修改(dbdealer, dealer);
        }

        public void 修改经销商基本信息(Dealer dealer) {
            CheckEntityState(dealer);
            var dbdealer = ObjectContext.Dealers.Where(r => r.Id == dealer.Id && r.Status == (int)DcsMasterDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbdealer == null)
                throw new ValidationException(ErrorStrings.Dealer_Validation2);
            创建经销商基本信息履历_修改(dbdealer, dealer);
        }

        public void 作废经销商基本信息(Dealer dealer) {
            var dbdealer = ObjectContext.Dealers.Where(r => r.Id == dealer.Id && r.Status != (int)DcsMasterDataStatus.作废).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbdealer == null)
                throw new ValidationException(ErrorStrings.Dealer_Validation5);
            UpdateToDatabase(dealer);
            dealer.Status = (int)DcsMasterDataStatus.作废;
            this.UpdateDealerValidate(dealer);
            var userInfo = Utils.GetCurrentUserInfo();
            var company = dealer.Company;
            UpdateToDatabase(company);
            company.Status = (int)DcsMasterDataStatus.作废;
            company.ModifierId = userInfo.Id;
            company.ModifierName = userInfo.Name;
            company.ModifyTime = DateTime.Now;
            new CompanyAch(this.DomainService).UpdateCompanyValidate(company);
            创建经销商基本信息履历_修改(dbdealer, dealer);
        }
        internal void InsertDealerHistoryValidate(DealerHistory dealerHistory) {
            var userInfo = Utils.GetCurrentUserInfo();
            dealerHistory.FilerId = userInfo.Id;
            dealerHistory.FilerName = userInfo.Name;
            dealerHistory.FileTime = DateTime.Now;
        }

        internal void 生成经销商基本信息履历(Dealer dbdealer) {
            var userInfo = Utils.GetCurrentUserInfo();
            var dealerHistory = new DealerHistory {
                RecordId = dbdealer.Id,
                Code = dbdealer.Code,
                Name = dbdealer.Name,
                ShortName = dbdealer.ShortName,
                CreateTime = DateTime.Now,
                CreatorId = userInfo.Id,
                CreatorName = userInfo.Name,
                FilerId = userInfo.Id,
                FilerName = userInfo.Name,
                FileTime = DateTime.Now,
                Status = dbdealer.Status
            };
            InsertToDatabase(dealerHistory);
            this.InsertDealerHistoryValidate(dealerHistory);
        }
        internal void 创建经销商基本信息履历_新增(Dealer dealerNow) {
            var dictionaryOfMapping = new Dictionary<string, PropertyChangeInfo>();
            var propertyNamesOfDealer = new[] {
                "NAME","SHORTNAME","MANAGER","STATUS"
            };
            var propertiesNeedRecordOfDealer = new CompanyInvoiceInfoAch(this.DomainService).GetPropertiesNeedRecordInRangeForInsert(dealerNow, propertyNamesOfDealer);
            foreach(var propertyChangeInfo in propertiesNeedRecordOfDealer) {
                var tempKey = propertyNamesOfDealer.SingleOrDefault(r => r == propertyChangeInfo.PropertyName);
                if(tempKey == null)
                    continue;
                tempKey = "A" + tempKey;
                dictionaryOfMapping.Add(tempKey, propertyChangeInfo);
            }
            var userInfo = Utils.GetCurrentUserInfo();
            var dealerHistory = new DealerHistory {
                RecordId = dealerNow.Id,
                Code = dealerNow.Code,
                CreateTime = DateTime.Now,
                CreatorId = userInfo.Id,
                CreatorName = userInfo.Name,
                FilerId = userInfo.Id,
                FilerName = userInfo.Name,
                FileTime = DateTime.Now,
            };
            new CompanyInvoiceInfoAch(this.DomainService).SetValuesToEntity(dealerHistory, dictionaryOfMapping, "INSERT");
            dealerHistory.Dealer = dealerNow;
            InsertToDatabase(dealerHistory);
        }
        internal void 创建经销商基本信息履历_修改(Dealer dealerOriginal, Dealer dealerNow) {
            var dictionaryOfMapping = new Dictionary<string, PropertyChangeInfo>();
            var propertyNamesOfDealer = new[] {
                "NAME","SHORTNAME","MANAGER","STATUS"
            };
            var propertiesNeedRecordOfDealer = new CompanyInvoiceInfoAch(this.DomainService).GetPropertiesNeedRecordInRangeForUpdate(dealerOriginal, dealerNow, propertyNamesOfDealer);
            foreach(var propertyChangeInfo in propertiesNeedRecordOfDealer) {
                var tempKey = propertyNamesOfDealer.SingleOrDefault(r => r == propertyChangeInfo.PropertyName);
                if(tempKey == null)
                    continue;
                tempKey = "A" + tempKey;
                dictionaryOfMapping.Add(tempKey, propertyChangeInfo);
            }
            var userInfo = Utils.GetCurrentUserInfo();
            var dealerHistory = new DealerHistory {
                RecordId = dealerNow.Id,
                Code = dealerNow.Code,
                CreateTime = DateTime.Now,
                CreatorId = userInfo.Id,
                CreatorName = userInfo.Name,
                FilerId = userInfo.Id,
                FilerName = userInfo.Name,
                FileTime = DateTime.Now,
            };

            new CompanyInvoiceInfoAch(this.DomainService).SetValuesToEntity(dealerHistory, dictionaryOfMapping, "UPDATE");
            dealerHistory.Dealer = dealerNow;
            InsertToDatabase(dealerHistory);
        }




    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:15
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 停用经销商信息(Dealer dealer) {
            new DealerAch(this).停用经销商信息(dealer);
        }
        
        [Update(UsingCustomMethod = true)]
        public void 恢复经销商基本信息(Dealer dealer) {
            new DealerAch(this).恢复经销商基本信息(dealer);
        }
        
        [Update(UsingCustomMethod = true)]
        public void 修改经销商基本信息(Dealer dealer) {
            new DealerAch(this).修改经销商基本信息(dealer);
        }
        
        [Update(UsingCustomMethod = true)]
        public void 作废经销商基本信息(Dealer dealer) {
            new DealerAch(this).作废经销商基本信息(dealer);
        }
    }
}
