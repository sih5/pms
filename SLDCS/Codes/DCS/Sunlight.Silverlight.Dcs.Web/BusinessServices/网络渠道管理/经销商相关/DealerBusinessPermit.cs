﻿using Sunlight.Silverlight.Web;
using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class DealerBusinessPermitAch : DcsSerivceAchieveBase {
        public DealerBusinessPermitAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 取消经销商服务经营权限(DealerBusinessPermit dealerBusinessPermit) {
            var originalDealerBusinessPermit = ObjectContext.DealerBusinessPermits.Where(r => r.Id == dealerBusinessPermit.Id).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(originalDealerBusinessPermit == null)
                throw new ValidationException(ErrorStrings.DealerBusinessPermit_Validation2);
            var userInfo = Utils.GetCurrentUserInfo();
            var dealerBusinessPermitHistory = new DealerBusinessPermitHistory {
                DealerBusinessPermitId = dealerBusinessPermit.Id,
                DealerId = dealerBusinessPermit.DealerId,
                ServiceProductLineId = dealerBusinessPermit.ServiceProductLineId,
                BranchId = dealerBusinessPermit.BranchId,
                CreateTime = DateTime.Now,
                CreatorId = userInfo.Id,
                CreatorName = userInfo.Name,
                ProductLineType = dealerBusinessPermit.ProductLineType,
                EngineRepairPower = dealerBusinessPermit.EngineRepairPower,
                Status = (int)DcsDealerBusinessPermitHistoryStatus.删除
            };
            InsertToDatabase(dealerBusinessPermitHistory);
            new DealerBusinessPermitHistoryAch(this.DomainService).InsertDealerBusinessPermitHistoryValidate(dealerBusinessPermitHistory);
            DeleteFromDatabase(dealerBusinessPermit);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:15
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 取消经销商服务经营权限(DealerBusinessPermit dealerBusinessPermit) {
            new DealerBusinessPermitAch(this).取消经销商服务经营权限(dealerBusinessPermit);
        }
    }
}
