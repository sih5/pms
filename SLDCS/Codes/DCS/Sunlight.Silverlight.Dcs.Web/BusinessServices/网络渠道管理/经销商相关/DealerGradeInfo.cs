﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using System.Text;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DealerGradeInfoAch : DcsSerivceAchieveBase {
        public DealerGradeInfoAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 作废品牌分级信息(DealerGradeInfo dealerGradeInfo) {
            dealerGradeInfo.Status = (int)DcsBaseDataStatus.作废;
            var userInfo = Utils.GetCurrentUserInfo();
            dealerGradeInfo.AbandonerId = userInfo.Id;
            dealerGradeInfo.AbandonerName = userInfo.Name;
            dealerGradeInfo.AbandonTime = DateTime.Now;
            UpdateToDatabase(dealerGradeInfo);

        }

    
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:15
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 作废品牌分级信息(DealerGradeInfo dealerGradeInfo) {
            new DealerGradeInfoAch(this).作废品牌分级信息(dealerGradeInfo);
        }
    }
}
