﻿using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using System;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class DealerKeyEmployeeAch : DcsSerivceAchieveBase {
        public DealerKeyEmployeeAch(DcsDomainService domainService)
            : base(domainService) {
        }
        internal void InsertDealerKeyEmployeeHistoryValidate(DealerKeyEmployeeHistory dealerKeyEmployeeHistory) {
            var userInfo = Utils.GetCurrentUserInfo();
            dealerKeyEmployeeHistory.FilerId = userInfo.Id;
            dealerKeyEmployeeHistory.FilerName = userInfo.Name;
            dealerKeyEmployeeHistory.FileTime = DateTime.Now;
        }

        public void 取消经销商关键岗位人员(DealerKeyEmployee dealerKeyEmployee) {
            var dbdealerKeyEmployee = ObjectContext.DealerKeyEmployees.Where(r => r.Id == dealerKeyEmployee.Id).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbdealerKeyEmployee == null)
                throw new ValidationException(ErrorStrings.DealerKeyEmployee_Validation1);
            var userInfo = Utils.GetCurrentUserInfo();
            var dealerKeyEmployeeHistory = new DealerKeyEmployeeHistory();
            dealerKeyEmployeeHistory.DealerId = dealerKeyEmployee.DealerId;
            dealerKeyEmployeeHistory.PartsSalesCategoryId = dealerKeyEmployee.PartsSalesCategoryId;
            dealerKeyEmployeeHistory.KeyPositionId = dealerKeyEmployee.KeyPositionId;
            dealerKeyEmployeeHistory.Name = dealerKeyEmployee.Name;
            dealerKeyEmployeeHistory.Sex = dealerKeyEmployee.Sex;
            dealerKeyEmployeeHistory.PhoneNumber = dealerKeyEmployee.PhoneNumber;
            dealerKeyEmployeeHistory.Address = dealerKeyEmployee.Address;
            dealerKeyEmployeeHistory.PostCode = dealerKeyEmployee.PostCode;
            dealerKeyEmployeeHistory.Mail = dealerKeyEmployee.Mail;
            dealerKeyEmployeeHistory.Remark = dealerKeyEmployee.Remark;
            dealerKeyEmployeeHistory.Status = (int)DcsDealerBusinessPermitHistoryStatus.删除;
            dealerKeyEmployeeHistory.CreateTime = dealerKeyEmployee.CreateTime;
            dealerKeyEmployeeHistory.CreatorId = dealerKeyEmployee.CreatorId;
            dealerKeyEmployeeHistory.CreatorName = dealerKeyEmployee.CreatorName;
            dealerKeyEmployeeHistory.ModifyTime = dealerKeyEmployee.ModifyTime;
            dealerKeyEmployeeHistory.ModifierId = dealerKeyEmployee.ModifierId;
            dealerKeyEmployeeHistory.ModifierName = dealerKeyEmployee.ModifierName;
            dealerKeyEmployeeHistory.AbandonTime = dealerKeyEmployee.AbandonTime;
            dealerKeyEmployeeHistory.AbandonerId = dealerKeyEmployee.AbandonerId;
            dealerKeyEmployeeHistory.AbandonerName = dealerKeyEmployee.AbandonerName;
            dealerKeyEmployeeHistory.MobileNumber = dealerKeyEmployee.MobileNumber;
            dealerKeyEmployeeHistory.FilerId = userInfo.Id;
            dealerKeyEmployeeHistory.FilerName = userInfo.Name;
            dealerKeyEmployeeHistory.FileTime = DateTime.Now;
            InsertToDatabase(dealerKeyEmployeeHistory);
            this.InsertDealerKeyEmployeeHistoryValidate(dealerKeyEmployeeHistory);
            DeleteFromDatabase(dealerKeyEmployee);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:15
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 取消经销商关键岗位人员(DealerKeyEmployee dealerKeyEmployee) {
            new DealerKeyEmployeeAch(this).取消经销商关键岗位人员(dealerKeyEmployee);
        }
    }
}
