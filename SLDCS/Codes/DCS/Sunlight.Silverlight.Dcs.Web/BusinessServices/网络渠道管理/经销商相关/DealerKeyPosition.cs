﻿using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using System;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class DealerKeyPositionAch : DcsSerivceAchieveBase {
        public void 是否关键岗位修改(DealerKeyPosition dealerKeyPosition) {

            var dealerKeyEmployee = ObjectContext.DealerKeyEmployees.Where(r => r.KeyPositionId == dealerKeyPosition.Id).ToArray(); ;
            UpdateToDatabase(dealerKeyPosition);
            UpdateDealerKeyPositionValidate(dealerKeyPosition);
            if(dealerKeyEmployee == null)
                return;
            foreach(var item in dealerKeyEmployee) {
                item.IsKeyPositions = dealerKeyPosition.IsKeyPositions;
                UpdateToDatabase(item);
                new DealerKeyEmployeeAch(this.DomainService).UpdateDealerKeyEmployeeValidate(item);
            }

        }
    }

    partial class DcsDomainService {
        [Update(UsingCustomMethod = true)]
        public void 是否关键岗位修改(DealerKeyPosition dealerKeyPosition) {
            new DealerKeyPositionAch(this).是否关键岗位修改(dealerKeyPosition);
        }
    }
}
