﻿using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class DealerMobileNumberListAch : DcsSerivceAchieveBase {
        public DealerMobileNumberListAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 修改经销商视频手机清单(DealerMobileNumberList dealerMobileNumberList) {
            CheckEntityState(dealerMobileNumberList);
            var dbdealerMobileNumberList = ObjectContext.DealerMobileNumberLists.Where(r => r.Id != dealerMobileNumberList.Id && r.DealerId == dealerMobileNumberList.DealerId && r.VideoMobileNumber == dealerMobileNumberList.VideoMobileNumber).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbdealerMobileNumberList != null)
                throw new ValidationException(string.Format(ErrorStrings.DealerMobileNumberList_Validation1, dealerMobileNumberList.VideoMobileNumber));
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:15
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 修改经销商视频手机清单(DealerMobileNumberList dealerMobileNumberList) {
            new DealerMobileNumberListAch(this).修改经销商视频手机清单(dealerMobileNumberList);
        }
    }
}
