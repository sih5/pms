﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class DealerPerTrainAutAch : DcsSerivceAchieveBase {
        public DealerPerTrainAutAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 服务站人员培训及认证作废(DealerPerTrainAut dealerPerTrainAut) {
            UpdateToDatabase(dealerPerTrainAut);
            dealerPerTrainAut.Status = (int)DcsBaseDataStatus.作废;
            this.UpdateDealerPerTrainAutValidate(dealerPerTrainAut);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:15
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 服务站人员培训及认证作废(DealerPerTrainAut dealerPerTrainAut) {
            new DealerPerTrainAutAch(this).服务站人员培训及认证作废(dealerPerTrainAut);
        }
    }
}
