﻿using System;
using System.ServiceModel.DomainServices.Server;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class DealerServiceExtAch : DcsSerivceAchieveBase {
        public DealerServiceExtAch(DcsDomainService domainService)
            : base(domainService) {
        }

        internal void InsertDealerServiceExtHistoryValidate(DealerServiceExtHistory dealerServiceExtHistory) {
            var userInfo = Utils.GetCurrentUserInfo();
            dealerServiceExtHistory.FilerId = userInfo.Id;
            dealerServiceExtHistory.FilerName = userInfo.Name;
            dealerServiceExtHistory.FileTime = DateTime.Now;
        }
        /// <summary>
        /// 仅供客户端使用
        /// </summary>

        public void 修改经销商服务扩展信息(DealerServiceExt dealerServiceExt) {
            CheckEntityState(dealerServiceExt);
            var dealerServiceExtHistory = new DealerServiceExtHistory {
                RecordId = dealerServiceExt.Id,
                RepairQualification = dealerServiceExt.RepairQualification,
                HasBranch = dealerServiceExt.HasBranch,
                DangerousRepairQualification = dealerServiceExt.DangerousRepairQualification,
                BrandScope = dealerServiceExt.BrandScope,
                CompetitiveBrandScope = dealerServiceExt.CompetitiveBrandScope,
                ParkingArea = dealerServiceExt.ParkingArea,
                RepairingArea = dealerServiceExt.RepairingArea,
                EmployeeNumber = dealerServiceExt.EmployeeNumber,
                PartWarehouseArea = dealerServiceExt.PartWarehouseArea,
                ManagerPhoneNumber = dealerServiceExt.ManagerPhoneNumber,
                ManagerMobile = dealerServiceExt.ManagerMobile,
                ManagerMail = dealerServiceExt.ManagerMail,
                ReceptionRoomArea = dealerServiceExt.ReceptionRoomArea,
                Status = dealerServiceExt.Status,
                CreateTime = dealerServiceExt.CreateTime,
                CreatorId = dealerServiceExt.CreatorId,
                CreatorName = dealerServiceExt.CreatorName,
                ModifierId = dealerServiceExt.ModifierId,
                ModifyTime = dealerServiceExt.ModifyTime,
                ModifierName = dealerServiceExt.ModifierName,
                GeographicPosition = dealerServiceExt.GeographicPosition,
                OwnerCompany = dealerServiceExt.OwnerCompany,
                MainBusinessAreas = dealerServiceExt.MainBusinessAreas,
                AndBusinessAreas = dealerServiceExt.AndBusinessAreas,
                BuildTime = dealerServiceExt.BuildTime,
                TrafficRestrictionsdescribe = dealerServiceExt.TrafficRestrictionsdescribe,
            };
            InsertToDatabase(dealerServiceExtHistory);
            this.InsertDealerServiceExtHistoryValidate(dealerServiceExtHistory);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:15
        //原分布类的函数全部转移到Ach                                                               
        /// <summary>
        /// 仅供客户端使用
        /// </summary>
        [Update(UsingCustomMethod = true)]
        public void 修改经销商服务扩展信息(DealerServiceExt dealerServiceExt) {
            new DealerServiceExtAch(this).修改经销商服务扩展信息(dealerServiceExt);
        }
    }
}
