﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Data;
using System.Data.EntityClient;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using System.Text;
using Devart.Data.Oracle;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DealerServiceInfoAch : DcsSerivceAchieveBase {
        public DealerServiceInfoAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 生成经销商分公司管理信息(DealerServiceInfo dealerServiceInfo) {

            //var mDMPartsSalesCategoryMap = this.ObjectContext.MDMPartsSalesCategoryMaps.Where(r => r.MDMPartsSalesCategory == "空").Select(r => r.PMSPartsSalesCategoryName).ToArray();
            //if(!mDMPartsSalesCategoryMap.Contains(dealerServiceInfo.PartsSalesCategory.Name)) {
            //    DealerServiceInfoValid(dealerServiceInfo.Dealer.Code, dealerServiceInfo.PartsSalesCategory.Name);
            //}

            var dbdealerServiceInfo = ObjectContext.DealerServiceInfoes.Where(r => r.BranchId == dealerServiceInfo.BranchId && r.PartsSalesCategoryId == dealerServiceInfo.PartsSalesCategoryId && r.DealerId == dealerServiceInfo.DealerId && r.Status == (int)DcsMasterDataStatus.有效).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbdealerServiceInfo != null)
                throw new ValidationException(ErrorStrings.DealerServiceInfo_Validation1);
            var channelCapability = ObjectContext.ChannelCapabilities.SingleOrDefault(r => r.Id == dealerServiceInfo.ChannelCapabilityId && r.Status == (int)DcsBaseDataStatus.有效);
            if(channelCapability == null)
                throw new ValidationException(ErrorStrings.DealerServiceInfo_Validation6);
            if(channelCapability.IsSale || channelCapability.IsPart) {
                var customerInformation = ObjectContext.CustomerInformations.FirstOrDefault(r => r.SalesCompanyId == dealerServiceInfo.BranchId && r.CustomerCompanyId == dealerServiceInfo.DealerId);
                if(customerInformation == null) {
                    var cInformation = new CustomerInformation {
                        SalesCompanyId = dealerServiceInfo.BranchId,
                        CustomerCompanyId = dealerServiceInfo.DealerId,
                        Status = (int)DcsMasterDataStatus.有效
                    };
                    InsertToDatabase(cInformation);
                    new CustomerInformationAch(this.DomainService).InsertCustomerInformationValidate(cInformation);
                }
            } else {
                var customerInformation = ObjectContext.CustomerInformations.FirstOrDefault(r => r.SalesCompanyId == dealerServiceInfo.BranchId && r.CustomerCompanyId == dealerServiceInfo.DealerId);
                if(customerInformation != null) {
                    DeleteFromDatabase(customerInformation);
                }
            }
            InsertToDatabase(dealerServiceInfo);
            dealerServiceInfo.Status = (int)DcsMasterDataStatus.有效;
            this.InsertDealerServiceInfoValidate(dealerServiceInfo);
            创建经销商分公司的履历_新增(dealerServiceInfo);
            //如果是否保外=否，则生成该保内品牌的经销商分公司管理信息后，查该服务站是否存在保外品牌，方法如下：查配件销售类型（条件：营销分公司id=界面传入的营销分公司id、状态=有效、是否保外=是）
            var partsSalesCategory = ObjectContext.PartsSalesCategories.SingleOrDefault(r => r.Id == dealerServiceInfo.PartsSalesCategoryId && r.Status == (int)DcsBaseDataStatus.有效);
            var warrantyPartsSalesCategory = ObjectContext.PartsSalesCategories.Where(r => r.BranchId == dealerServiceInfo.BranchId && r.Status == (int)DcsBaseDataStatus.有效 && r.IsNotWarranty);
            string originalMarketingDepartmentName = ObjectContext.MarketingDepartments.Single(r => r.Id == dealerServiceInfo.MarketingDepartmentId && r.Status == (int)DcsBaseDataStatus.有效).Name;
            //var originalGradeCoefficientName = ObjectContext.GradeCoefficients.Single(r => r.Id == dealerServiceInfo.GradeCoefficientId && r.Status == (int)DcsBaseDataStatus.有效).Grade;

            if(warrantyPartsSalesCategory.Any()) {
                if(partsSalesCategory != null && (!partsSalesCategory.IsNotWarranty)) {
                    foreach(var item in warrantyPartsSalesCategory) {
                        var warrantydealerServiceInfo = ObjectContext.DealerServiceInfoes.Where(r => r.BranchId == dealerServiceInfo.BranchId && r.PartsSalesCategoryId == item.Id && r.DealerId == dealerServiceInfo.DealerId && r.Status == (int)DcsMasterDataStatus.有效).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
                        if(warrantydealerServiceInfo != null)
                            continue;
                        var marketingDepartment = ObjectContext.MarketingDepartments.SingleOrDefault(r => r.PartsSalesCategoryId == item.Id && r.Status == (int)DcsBaseDataStatus.有效 && r.Name == originalMarketingDepartmentName);
                        var gradeCoefficient = ObjectContext.GradeCoefficients.SingleOrDefault(r => r.PartsSalesCategoryId == item.Id && r.Status == (int)DcsBaseDataStatus.有效/* && r.Grade == originalGradeCoefficientName*/);
                        var insertWarrantydealerServiceInfo = new DealerServiceInfo {
                            PartsSalesCategoryId = item.Id,
                            MarketingDepartmentId = marketingDepartment == null ? 0 : marketingDepartment.Id,
                            UsedPartsWarehouseId = dealerServiceInfo.UsedPartsWarehouseId,
                            ChannelCapabilityId = dealerServiceInfo.ChannelCapabilityId,
                            BranchId = dealerServiceInfo.BranchId,
                            DealerId = dealerServiceInfo.DealerId,
                            BusinessCode = dealerServiceInfo.BusinessCode,
                            BusinessName = dealerServiceInfo.BusinessName,
                            HotLine = dealerServiceInfo.HotLine,
                            Fix = dealerServiceInfo.Fix,
                            Fax = dealerServiceInfo.Fax,
                            OutServiceradii = dealerServiceInfo.OutServiceradii,
                            PartsManagingFeeGradeId = dealerServiceInfo.PartsManagingFeeGradeId,
                            OutFeeGradeId = dealerServiceInfo.OutFeeGradeId,

                            GradeCoefficientId = gradeCoefficient == null ? 0 : gradeCoefficient.Id,
                            RepairAuthorityGrade = dealerServiceInfo.RepairAuthorityGrade,
                            Remark = dealerServiceInfo.Remark,
                            Status = dealerServiceInfo.Status,
                            BusinessDivision = dealerServiceInfo.BusinessDivision,
                            AccreditTime = dealerServiceInfo.AccreditTime,
                            ServicePermission = dealerServiceInfo.ServicePermission,
                            ServiceStationType = dealerServiceInfo.ServiceStationType,
                            Area = dealerServiceInfo.Area,
                            RegionType = dealerServiceInfo.RegionType,
                            MaterialCostInvoiceType = dealerServiceInfo.MaterialCostInvoiceType,
                            LaborCostCostInvoiceType = dealerServiceInfo.LaborCostCostInvoiceType,
                            LaborCostInvoiceRatio = dealerServiceInfo.LaborCostInvoiceRatio,
                            InvoiceTypeInvoiceRatio = dealerServiceInfo.InvoiceTypeInvoiceRatio,
                            CancellingDate = dealerServiceInfo.CancellingDate,
                            IsOnDuty = dealerServiceInfo.IsOnDuty,
                            WarehouseId = dealerServiceInfo.WarehouseId,
                            PartReserveAmount = dealerServiceInfo.PartReserveAmount,
                            SaleandServicesiteLayout = dealerServiceInfo.SaleandServicesiteLayout,
                            Grade = dealerServiceInfo.Grade

                        };
                        InsertToDatabase(insertWarrantydealerServiceInfo);
                        this.InsertDealerServiceInfoValidate(insertWarrantydealerServiceInfo);
                        创建经销商分公司的履历_新增(insertWarrantydealerServiceInfo);
                    }
                }
            }
        }


        public void 修改经销商分公司管理信息(DealerServiceInfo dealerServiceInfo) {
            CheckEntityState(dealerServiceInfo);
            var dbdealerServiceInfo = ObjectContext.DealerServiceInfoes.Where(r => r.Id == dealerServiceInfo.Id && r.Status == (int)DcsMasterDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbdealerServiceInfo == null)
                throw new ValidationException(ErrorStrings.DealerServiceInfo_Validation2);
            var dbdealerServiceInfos = ObjectContext.DealerServiceInfoes.Where(r => r.Id != dealerServiceInfo.Id && r.BranchId == dealerServiceInfo.BranchId && r.PartsSalesCategoryId == dealerServiceInfo.PartsSalesCategoryId && r.DealerId == dealerServiceInfo.DealerId && r.Status == (int)DcsMasterDataStatus.有效).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbdealerServiceInfos != null)
                throw new ValidationException(ErrorStrings.DealerServiceInfo_Validation1);
            var channelCapability = ObjectContext.ChannelCapabilities.SingleOrDefault(r => r.Id == dealerServiceInfo.ChannelCapabilityId && r.Status == (int)DcsBaseDataStatus.有效);
            if(channelCapability == null)
                throw new ValidationException(ErrorStrings.DealerServiceInfo_Validation6);
            if(channelCapability.IsSale || channelCapability.IsPart) {
                var customerInformation = ObjectContext.CustomerInformations.FirstOrDefault(r => r.SalesCompanyId == dealerServiceInfo.BranchId && r.CustomerCompanyId == dealerServiceInfo.DealerId);
                if(customerInformation == null) {
                    var cInformation = new CustomerInformation {
                        SalesCompanyId = dealerServiceInfo.BranchId,
                        CustomerCompanyId = dealerServiceInfo.DealerId,
                        Status = (int)DcsMasterDataStatus.有效
                    };
                    InsertToDatabase(cInformation);
                    new CustomerInformationAch(this.DomainService).InsertCustomerInformationValidate(cInformation);
                }
            } else {
                var customerInformation = ObjectContext.CustomerInformations.FirstOrDefault(r => r.SalesCompanyId == dealerServiceInfo.BranchId && r.CustomerCompanyId == dealerServiceInfo.DealerId);
                if(customerInformation != null) {
                    DeleteFromDatabase(customerInformation);
                }
            }

            //生成经销商服务管理信息履历(dealerServiceInfo);
            创建经销商分公司的履历_修改(dbdealerServiceInfo, dealerServiceInfo);

        }


        public void 停用经销商分公司管理信息(DealerServiceInfo dealerServiceInfo) {
            //var partsSalesCategory = this.ObjectContext.PartsSalesCategories.FirstOrDefault(r => r.Id == dealerServiceInfo.PartsSalesCategoryId);

            //var mDMPartsSalesCategoryMap = this.ObjectContext.MDMPartsSalesCategoryMaps.Where(r => r.MDMPartsSalesCategory == "空").Select(r => r.PMSPartsSalesCategoryName).ToArray();
            //if(!mDMPartsSalesCategoryMap.Contains(partsSalesCategory.Name)) {
            //    var dealer = this.ObjectContext.Dealers.FirstOrDefault(r => r.Id == dealerServiceInfo.DealerId);
            //    DealerServiceInfoValid1(dealer.Code, partsSalesCategory.Name);
            //}
            var dbdealerServiceInfo = ObjectContext.DealerServiceInfoes.Where(r => r.Id == dealerServiceInfo.Id && r.Status == (int)DcsMasterDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbdealerServiceInfo == null)
                throw new ValidationException(ErrorStrings.DealerServiceInfo_Validation3);
            //更新履历
            //生成经销商服务管理信息履历(dealerServiceInfo);
            UpdateToDatabase(dealerServiceInfo);
            dealerServiceInfo.Status = (int)DcsMasterDataStatus.停用;
            dealerServiceInfo.ExternalState = (int)DcsExternalState.停用;
            this.UpdateDealerServiceInfoValidate(dealerServiceInfo);
            创建经销商分公司的履历_修改(dbdealerServiceInfo, dealerServiceInfo);
        }


        public void 恢复经销商分公司管理信息(DealerServiceInfo dealerServiceInfo) {
            var dbdealerServiceInfo = ObjectContext.DealerServiceInfoes.Where(r => r.Id == dealerServiceInfo.Id && r.Status == (int)DcsMasterDataStatus.停用).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbdealerServiceInfo == null)
                throw new ValidationException(ErrorStrings.DealerServiceInfo_Validation4);
            //更新履历
            //生成经销商服务管理信息履历(dealerServiceInfo);
            UpdateToDatabase(dealerServiceInfo);
            dealerServiceInfo.Status = (int)DcsMasterDataStatus.有效;
            this.UpdateDealerServiceInfoValidate(dealerServiceInfo);
            创建经销商分公司的履历_修改(dbdealerServiceInfo, dealerServiceInfo);
        }


        public void 作废经销商分公司管理信息(DealerServiceInfo dealerServiceInfo) {
            var dbdealerServiceInfo = ObjectContext.DealerServiceInfoes.Where(r => r.Id == dealerServiceInfo.Id && r.Status != (int)DcsMasterDataStatus.作废).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbdealerServiceInfo == null)
                throw new ValidationException(ErrorStrings.DealerServiceInfo_Validation5);
            //更新履历
            //生成经销商服务管理信息履历(dealerServiceInfo);
            UpdateToDatabase(dealerServiceInfo);
            dealerServiceInfo.Status = (int)DcsMasterDataStatus.作废;
            dealerServiceInfo.ExternalState = (int)DcsExternalState.停用;
            this.UpdateDealerServiceInfoValidate(dealerServiceInfo);
            //var serviceProductLinesQuery = ObjectContext.ServiceProductLineViews.Where(r => r.BranchId == dealerServiceInfo.BranchId && r.PartsSalesCategoryId == dealerServiceInfo.PartsSalesCategoryId);
            //var dealerBusinessPermits = ObjectContext.DealerBusinessPermits.Where(r => r.DealerId == dealerServiceInfo.DealerId && r.BranchId == dealerServiceInfo.BranchId && serviceProductLinesQuery.Any(v => v.ProductLineId == r.ServiceProductLineId)).ToArray();
            //var userInfo = Utils.GetCurrentUserInfo();
            //if(dealerBusinessPermits.Length > 0) {
            //    foreach(var dealerBusinessPermit in dealerBusinessPermits) {
            //        int dealerId = dealerBusinessPermit.DealerId;
            //        int serviceProductLineId = dealerBusinessPermit.ServiceProductLineId;
            //        int branchId = dealerBusinessPermit.BranchId;
            //        int serviceFeeGradeId = dealerBusinessPermit.ServiceFeeGradeId;
            //        int productLineType = dealerBusinessPermit.ProductLineType;
            //        var newDealerBusinessPermitHistory = new DealerBusinessPermitHistory {
            //            DealerId = dealerId,
            //            ServiceProductLineId = serviceProductLineId,
            //            BranchId = branchId,
            //            ServiceFeeGradeId = serviceFeeGradeId,
            //            ProductLineType = productLineType,
            //            Status = (int)DcsDealerBusinessPermitHistoryStatus.删除,
            //            CreatorId = userInfo.Id,
            //            CreatorName = userInfo.Name,
            //            CreateTime = DateTime.Now,
            //        };
            //        DomainService.InsertDealerBusinessPermitHistory(newDealerBusinessPermitHistory);
            //        DeleteFromDatabase(dealerBusinessPermit);
            //    }
            //}
            创建经销商分公司的履历_修改(dbdealerServiceInfo, dealerServiceInfo);
        }
        internal void InsertDealerServiceInfoHistoryValidate(DealerServiceInfoHistory dealerServiceInfoHistory) {
            var userInfo = Utils.GetCurrentUserInfo();
            dealerServiceInfoHistory.FilerId = userInfo.Id;
            dealerServiceInfoHistory.FilerName = userInfo.Name;
            dealerServiceInfoHistory.FileTime = DateTime.Now;
        }

        internal void 生成经销商服务管理信息履历(DealerServiceInfo dealerServiceInfo) {
            var dealerServiceInfoHistory = new DealerServiceInfoHistory {
                ChannelCapabilityId = dealerServiceInfo.ChannelCapabilityId,
                RecordId = dealerServiceInfo.Id,
                BranchId = dealerServiceInfo.BranchId,
                DealerId = dealerServiceInfo.DealerId,
                HotLine = dealerServiceInfo.HotLine,
                Fix = dealerServiceInfo.Fix,
                Fax = dealerServiceInfo.Fax,
                PartsManagingFeeGradeId = dealerServiceInfo.PartsManagingFeeGradeId,
                OutFeeGradeId = dealerServiceInfo.OutFeeGradeId,
                RepairAuthorityGrade = dealerServiceInfo.RepairAuthorityGrade,
                Remark = dealerServiceInfo.Remark,
                Status = dealerServiceInfo.Status,
                CreateTime = dealerServiceInfo.CreateTime,
                CreatorId = dealerServiceInfo.CreatorId,
                CreatorName = dealerServiceInfo.CreatorName,
                ModifyTime = dealerServiceInfo.ModifyTime,
                ModifierId = dealerServiceInfo.ModifierId,
                ModifierName = dealerServiceInfo.ModifierName,
                BusinessDivision = dealerServiceInfo.BusinessDivision,
                AccreditTime = dealerServiceInfo.AccreditTime,
                ServicePermission = dealerServiceInfo.ServicePermission,
                ServiceStationType = dealerServiceInfo.ServiceStationType,
                Area = dealerServiceInfo.Area,
                RegionType = dealerServiceInfo.RegionType,
                MaterialCostInvoiceType = dealerServiceInfo.MaterialCostInvoiceType,
                LaborCostCostInvoiceType = dealerServiceInfo.LaborCostCostInvoiceType,
                LaborCostInvoiceRatio = dealerServiceInfo.LaborCostInvoiceRatio,
                InvoiceTypeInvoiceRatio = dealerServiceInfo.InvoiceTypeInvoiceRatio,
                CancellingDate = dealerServiceInfo.CancellingDate,
                IsOnDuty = dealerServiceInfo.IsOnDuty,
                WarehouseId = dealerServiceInfo.WarehouseId,
                PartReserveAmount = dealerServiceInfo.PartReserveAmount,
                SaleandServicesiteLayout = dealerServiceInfo.SaleandServicesiteLayout
            };
            InsertToDatabase(dealerServiceInfoHistory);
            this.InsertDealerServiceInfoHistoryValidate(dealerServiceInfoHistory);
        }


        internal void 创建经销商分公司的履历_修改(DealerServiceInfo dealerServiceInfoOrigianl, DealerServiceInfo dealerServiceInfoNow) {
            var propertiesOfDealerServiceInfoNeedCompare = new[] {
                "BUSINESSCODE", "BUSINESSNAME", "MARKETINGDEPARTMENTID", "USEDPARTSWAREHOUSEID", "WAREHOUSEID", "CHANNELCAPABILITYID", "PARTSMANAGINGFEEGRADEID", "GRADECOEFFICIENTID", "OUTFEEGRADEID", "OUTSERVICERADII", "BUSINESSDIVISION", "ACCREDITTIME", "SERVICEPERMISSION", "SERVICESTATIONTYPE", "AREA", "REGIONTYPE", "MATERIALCOSTINVOICETYPE", "LABORCOSTCOSTINVOICETYPE", "LABORCOSTINVOICERATIO", "INVOICETYPEINVOICERATIO", "CANCELLINGDATE", "ISONDUTY","GRADE", "REPAIRAUTHORITYGRADE", "HOTLINE", "FIX", "FAX", "REMARK", "STATUS", "PARTRESERVEAMOUNT", "SALEANDSERVICESITELAYOUT","HOURPHONE24","TRUNKNETWORKTYPE","CENTERSTACK"
            };

            var propertiesNeedRecordOfDealerServiceInfo = new CompanyInvoiceInfoAch(this.DomainService).GetPropertiesNeedRecordInRangeForUpdate(dealerServiceInfoOrigianl, dealerServiceInfoNow, propertiesOfDealerServiceInfoNeedCompare);
            var dictionaryOfMapping = new Dictionary<string, PropertyChangeInfo>();
            foreach(var propertyChangeInfo in propertiesNeedRecordOfDealerServiceInfo) {
                var tempKey = propertiesOfDealerServiceInfoNeedCompare.SingleOrDefault(r => r.ToUpper() == propertyChangeInfo.PropertyName);
                if(tempKey == null)
                    continue;
                if(tempKey == "HOURPHONE24")           //////把HOURPHONE转换成A24HOURPHONE，够够的:(
                    tempKey = "A24HOURPHONE";
                else
                    tempKey = "A" + tempKey;
                dictionaryOfMapping.Add(tempKey, propertyChangeInfo);
            }
            var dealerServiceInfoHistory = new DealerServiceInfoHistory();
            new CompanyInvoiceInfoAch(this.DomainService).SetValuesToEntity(dealerServiceInfoHistory, dictionaryOfMapping, "UPDATE");
            var userInfo = Utils.GetCurrentUserInfo();
            dealerServiceInfoHistory.RecordId = dealerServiceInfoNow.Id;
            dealerServiceInfoHistory.PartsSalesCategoryId = dealerServiceInfoNow.PartsSalesCategoryId;
            dealerServiceInfoHistory.BranchId = dealerServiceInfoNow.BranchId;
            dealerServiceInfoHistory.DealerId = dealerServiceInfoNow.DealerId;
            dealerServiceInfoHistory.Grade = dealerServiceInfoNow.Grade;
            dealerServiceInfoHistory.CreatorId = userInfo.Id;
            dealerServiceInfoHistory.CreatorName = userInfo.Name;
            dealerServiceInfoHistory.CreateTime = DateTime.Now;
            dealerServiceInfoHistory.FilerId = userInfo.Id;
            dealerServiceInfoHistory.FilerName = userInfo.Name;
            dealerServiceInfoHistory.FileTime = DateTime.Now;
            this.InsertToDatabase(dealerServiceInfoHistory);
        }
        internal void 创建经销商分公司的履历_新增(DealerServiceInfo dealerServiceInfoNow) {
            var propertiesOfDealerServiceInfo = new[] {
                "BUSINESSCODE", "BUSINESSNAME", "MARKETINGDEPARTMENTID", "USEDPARTSWAREHOUSEID", "WAREHOUSEID", "CHANNELCAPABILITYID", "PARTSMANAGINGFEEGRADEID", "GRADECOEFFICIENTID", "OUTFEEGRADEID", "OUTSERVICERADII", "BUSINESSDIVISION", "ACCREDITTIME", "SERVICEPERMISSION", "SERVICESTATIONTYPE", "AREA", "REGIONTYPE", "MATERIALCOSTINVOICETYPE", "LABORCOSTCOSTINVOICETYPE", "LABORCOSTINVOICERATIO", "INVOICETYPEINVOICERATIO", "CANCELLINGDATE", "ISONDUTY","GRADE", "REPAIRAUTHORITYGRADE", "HOTLINE", "FIX", "FAX", "REMARK", "STATUS", "PARTRESERVEAMOUNT", "SALEANDSERVICESITELAYOUT"
            };

            var propertiesNeedRecordOfDealerServiceInfo = new CompanyInvoiceInfoAch(this.DomainService).GetPropertiesNeedRecordInRangeForInsert(dealerServiceInfoNow, propertiesOfDealerServiceInfo);
            var dictionaryOfMapping = new Dictionary<string, PropertyChangeInfo>();
            foreach(var propertyChangeInfo in propertiesNeedRecordOfDealerServiceInfo) {
                var tempKey = propertiesOfDealerServiceInfo.SingleOrDefault(r => r == propertyChangeInfo.PropertyName);
                if(tempKey == null)
                    continue;
                tempKey = "A" + tempKey;
                dictionaryOfMapping.Add(tempKey, propertyChangeInfo);
            }
            var dealerServiceInfoHistory = new DealerServiceInfoHistory();
            new CompanyInvoiceInfoAch(this.DomainService).SetValuesToEntity(dealerServiceInfoHistory, dictionaryOfMapping, "INSERT");
            var userInfo = Utils.GetCurrentUserInfo();
            dealerServiceInfoHistory.DealerServiceInfo = dealerServiceInfoNow;
            dealerServiceInfoHistory.RecordId = dealerServiceInfoNow.Id;
            dealerServiceInfoHistory.PartsSalesCategoryId = dealerServiceInfoNow.PartsSalesCategoryId;
            dealerServiceInfoHistory.BranchId = dealerServiceInfoNow.BranchId;
            dealerServiceInfoHistory.DealerId = dealerServiceInfoNow.DealerId;
            dealerServiceInfoHistory.Grade = dealerServiceInfoNow.Grade;
            dealerServiceInfoHistory.CreatorId = userInfo.Id;
            dealerServiceInfoHistory.CreatorName = userInfo.Name;
            dealerServiceInfoHistory.CreateTime = DateTime.Now;
            dealerServiceInfoHistory.FilerId = userInfo.Id;
            dealerServiceInfoHistory.FilerName = userInfo.Name;
            dealerServiceInfoHistory.FileTime = DateTime.Now;
            this.InsertToDatabase(dealerServiceInfoHistory);

        }

        private void DealerServiceInfoValid(string dealerCode, string partsSalesCategoryName) {
            string connectionString = "";
            StringBuilder sql = new StringBuilder();
            var entityConnStr = ConfigurationManager.ConnectionStrings["DcsEntities"].ConnectionString;
            if(!string.IsNullOrEmpty(entityConnStr)) {
                var entityConnection = new EntityConnectionStringBuilder(entityConnStr);
                connectionString = entityConnection.ProviderConnectionString;
            }
            OracleConnection conn = new OracleConnection(connectionString);
            conn.Open();
            OracleCommand command = new OracleCommand(null, conn);
            sql.Append(@"select Customer_Brand from dcs.Cs_Stibo where Cs_Number='" + dealerCode + "'");

            command.CommandText = sql.ToString();
            OracleDataAdapter orda = new OracleDataAdapter(command);
            DataSet ds = new DataSet();
            orda.Fill(ds);

            DataTable dtVirtualPartsExchange = ds.Tables[0];
            string brandStr = "";
            foreach(DataRow item in dtVirtualPartsExchange.Rows) {
                brandStr += item["Customer_Brand"].ToString() + "/";
            }

            //处理字符串先用"/"分割，在用";"分割
            var brandArray = brandStr.Split(new char[] { '/', ';' });
            //查询映射关系
            //var mDMPartsSalesCategoryMap = this.ObjectContext.MDMPartsSalesCategoryMaps.FirstOrDefault(r => r.PMSPartsSalesCategoryName == partsSalesCategoryName);
            //if(mDMPartsSalesCategoryMap == null)
            //    throw new ValidationException("未找到品牌[" + partsSalesCategoryName + "]的MDM品牌映射");

            //if(!brandArray.Contains(mDMPartsSalesCategoryMap.MDMPartsSalesCategory))
            //    throw new ValidationException("请在MDM系统增加品牌[" + partsSalesCategoryName + "]");
        }

        [Invoke]
        public string DealerServiceInfoValid1(string dealerCode, string partsSalesCategoryName) {
            string connectionString = "";
            StringBuilder sql = new StringBuilder();
            var entityConnStr = ConfigurationManager.ConnectionStrings["DcsEntities"].ConnectionString;
            if(!string.IsNullOrEmpty(entityConnStr)) {
                var entityConnection = new EntityConnectionStringBuilder(entityConnStr);
                connectionString = entityConnection.ProviderConnectionString;
            }
            OracleConnection conn = new OracleConnection(connectionString);
            conn.Open();
            OracleCommand command = new OracleCommand(null, conn);
            sql.Append(@"select Customer_Brand from dcs.Cs_Stibo where Cs_Number='" + dealerCode + "'");

            command.CommandText = sql.ToString();
            OracleDataAdapter orda = new OracleDataAdapter(command);
            DataSet ds = new DataSet();
            orda.Fill(ds);

            DataTable dtVirtualPartsExchange = ds.Tables[0];
            string brandStr = "";
            foreach(DataRow item in dtVirtualPartsExchange.Rows) {
                brandStr += item["Customer_Brand"].ToString() + "/";
            }

            //处理字符串先用"/"分割，在用";"分割
            var brandArray = brandStr.Split(new char[] { '/', ';' });
            //查询映射关系
            //var mDMPartsSalesCategoryMap = this.ObjectContext.MDMPartsSalesCategoryMaps.FirstOrDefault(r => r.PMSPartsSalesCategoryName == partsSalesCategoryName);
            //if(mDMPartsSalesCategoryMap == null) {
            //    return "未找到当前品牌的MDM品牌映射";
            //} else {
            //    if(brandArray.Contains(mDMPartsSalesCategoryMap.MDMPartsSalesCategory)) {
            //        return "请在MDM系统停用当前品牌";
            //    } else
            //        return null;
            //}
            return null;
        }
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:15
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 生成经销商分公司管理信息(DealerServiceInfo dealerServiceInfo) {
            new DealerServiceInfoAch(this).生成经销商分公司管理信息(dealerServiceInfo);
        }


        [Update(UsingCustomMethod = true)]
        public void 修改经销商分公司管理信息(DealerServiceInfo dealerServiceInfo) {
            new DealerServiceInfoAch(this).修改经销商分公司管理信息(dealerServiceInfo);
        }


        [Update(UsingCustomMethod = true)]
        public void 停用经销商分公司管理信息(DealerServiceInfo dealerServiceInfo) {
            new DealerServiceInfoAch(this).停用经销商分公司管理信息(dealerServiceInfo);
        }


        [Update(UsingCustomMethod = true)]
        public void 恢复经销商分公司管理信息(DealerServiceInfo dealerServiceInfo) {
            new DealerServiceInfoAch(this).恢复经销商分公司管理信息(dealerServiceInfo);
        }


        [Update(UsingCustomMethod = true)]
        public void 作废经销商分公司管理信息(DealerServiceInfo dealerServiceInfo) {
            new DealerServiceInfoAch(this).作废经销商分公司管理信息(dealerServiceInfo);
        }

        [Invoke]
        public string DealerServiceInfoValid1(string dealerCode, string partsSalesCategoryName) {
            return new DealerServiceInfoAch(this).DealerServiceInfoValid1(dealerCode, partsSalesCategoryName);
        }
    }
}
