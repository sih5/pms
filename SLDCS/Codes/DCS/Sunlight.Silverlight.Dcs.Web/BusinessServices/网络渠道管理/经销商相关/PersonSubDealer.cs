﻿using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class PersonSubDealerAch : DcsSerivceAchieveBase {
        public PersonSubDealerAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 修改登陆人员与二级站关系(PersonSubDealer personSubDealer) {
            CheckEntityState(personSubDealer);
            var dbPersonSubDealer = ObjectContext.PersonSubDealers.Where(r => r.Id != personSubDealer.Id && r.PersonId == personSubDealer.PersonId && r.SubDealerId == personSubDealer.SubDealerId).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbPersonSubDealer != null)
                throw new ValidationException(ErrorStrings.PersonSubDealer_Validation1);
        }

        public void 删除登陆人员与二级站关系(PersonSubDealer personSubDealer) {
            DeleteFromDatabase(personSubDealer);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:15
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 修改登陆人员与二级站关系(PersonSubDealer personSubDealer) {
            new PersonSubDealerAch(this).修改登陆人员与二级站关系(personSubDealer);
        }
        
        [Update(UsingCustomMethod = true)]
        public void 删除登陆人员与二级站关系(PersonSubDealer personSubDealer) {
            new PersonSubDealerAch(this).删除登陆人员与二级站关系(personSubDealer);
        }
    }
}
