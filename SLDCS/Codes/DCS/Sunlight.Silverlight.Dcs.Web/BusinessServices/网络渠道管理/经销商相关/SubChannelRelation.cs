﻿using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class SubChannelRelationAch : DcsSerivceAchieveBase {
        public SubChannelRelationAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 作废二级网点关系(SubChannelRelation subChannelRelation) {
            var dbSubChannelRelation = ObjectContext.SubChannelRelations.Where(r => r.Id == subChannelRelation.Id && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbSubChannelRelation == null) {
                throw new ValidationException(ErrorStrings.SubChannelRelation_Validation3);
            }
            subChannelRelation.Status = (int)DcsBaseDataStatus.作废;
            UpdateSubChannelRelation(subChannelRelation);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:15
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 作废二级网点关系(SubChannelRelation subChannelRelation) {
            new SubChannelRelationAch(this).作废二级网点关系(subChannelRelation);
        }
    }
}
