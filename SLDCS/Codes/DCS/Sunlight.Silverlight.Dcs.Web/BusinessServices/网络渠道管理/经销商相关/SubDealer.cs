﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class SubDealerAch : DcsSerivceAchieveBase {
        public SubDealerAch(DcsDomainService domainService)
            : base(domainService) {
        }
        

        public void 作废二级站信息(SubDealer subDealer) {
            var dbdealer = ObjectContext.SubDealers.Where(r => r.Id == subDealer.Id && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbdealer == null)
                throw new ValidationException(ErrorStrings.SubDealer_Validation1);
            UpdateToDatabase(subDealer);
            var userInfo = Utils.GetCurrentUserInfo();
            subDealer.Status = (int)DcsBaseDataStatus.作废;
            subDealer.AbandonTime = DateTime.Now;
            subDealer.AbandonerId = userInfo.Id;
            subDealer.AbandonerName = userInfo.Name;
            this.UpdateSubDealerValidate(subDealer);
        }

        public void 修改二级站信息(SubDealer subDealer) {
            var dbdealer = ObjectContext.SubDealers.Where(r => r.Id == subDealer.Id && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbdealer == null)
                throw new ValidationException(ErrorStrings.SubDealer_Validation3);
            var dbSubDealer = ObjectContext.SubDealers.Where(r => r.Id != subDealer.Id && r.Code.ToLower() == subDealer.Code.ToLower() && r.DealerId == subDealer.DealerId).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbSubDealer != null)
                throw new ValidationException(string.Format(ErrorStrings.SubDealer_Validation2, subDealer.Code));
            UpdateToDatabase(subDealer);
            subDealer.Status = (int)DcsBaseDataStatus.有效;
            this.UpdateSubDealerValidate(subDealer);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:15
        //原分布类的函数全部转移到Ach                                                               
        
        [Update(UsingCustomMethod = true)]
        public void 作废二级站信息(SubDealer subDealer) {
            new SubDealerAch(this).作废二级站信息(subDealer);
        }
        
        [Update(UsingCustomMethod = true)]
        public void 修改二级站信息(SubDealer subDealer) {
            new SubDealerAch(this).修改二级站信息(subDealer);
        }
    }
}
