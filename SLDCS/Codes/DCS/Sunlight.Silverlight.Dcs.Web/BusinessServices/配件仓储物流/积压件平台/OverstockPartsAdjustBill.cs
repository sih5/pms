﻿using Sunlight.Silverlight.Web;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class OverstockPartsAdjustBillAch : DcsSerivceAchieveBase {
        public OverstockPartsAdjustBillAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 作废积压件调剂单(OverstockPartsAdjustBill overstockPartsAdjustBill) {
            var dboverstockPartsAdjustBill = ObjectContext.OverstockPartsAdjustBills.Where(r => r.Id == overstockPartsAdjustBill.Id && r.Status == (int)DcsOverstockPartsAdjustBillStatus.新建).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dboverstockPartsAdjustBill == null)
                throw new ValidationException(ErrorStrings.OverstockPartsAdjustBill_Validation1);
            UpdateToDatabase(overstockPartsAdjustBill);
            overstockPartsAdjustBill.Status = (int)DcsOverstockPartsAdjustBillStatus.作废;
            var userInfo = Utils.GetCurrentUserInfo();
            overstockPartsAdjustBill.AbandonerId = userInfo.Id;
            overstockPartsAdjustBill.AbandonerName = userInfo.Name;
            overstockPartsAdjustBill.AbandonTime = DateTime.Now;
            this.UpdateOverstockPartsAdjustBillValidate(overstockPartsAdjustBill);
        }

        public void 审批积压件调剂单(OverstockPartsAdjustBill overstockPartsAdjustBill) {
            var dboverstockPartsAdjustBill = ObjectContext.OverstockPartsAdjustBills.Where(r => r.Id == overstockPartsAdjustBill.Id && r.Status == (int)DcsOverstockPartsAdjustBillStatus.新建).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dboverstockPartsAdjustBill == null)
                throw new ValidationException(ErrorStrings.OverstockPartsAdjustBill_Validation2);
            UpdateToDatabase(overstockPartsAdjustBill);
            overstockPartsAdjustBill.Status = (int)DcsOverstockPartsAdjustBillStatus.生效;
            var userInfo = Utils.GetCurrentUserInfo();
            overstockPartsAdjustBill.ApproverId = userInfo.Id;
            overstockPartsAdjustBill.ApproverName = userInfo.Name;
            overstockPartsAdjustBill.ApproveTime = DateTime.Now;
            this.UpdateOverstockPartsAdjustBillValidate(overstockPartsAdjustBill);
        }

        public void 确认积压件调剂单(OverstockPartsAdjustBill overstockPartsAdjustBill) {
            var dboverstockPartsAdjustBill = ObjectContext.OverstockPartsAdjustBills.Where(r => r.Id == overstockPartsAdjustBill.Id && r.Status == (int)DcsOverstockPartsAdjustBillStatus.生效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dboverstockPartsAdjustBill == null)
                throw new ValidationException(ErrorStrings.OverstockPartsAdjustBill_Validation3);
            UpdateToDatabase(overstockPartsAdjustBill);
            overstockPartsAdjustBill.Status = (int)DcsOverstockPartsAdjustBillStatus.确认;
            var userInfo = Utils.GetCurrentUserInfo();
            overstockPartsAdjustBill.ConfirmorId = userInfo.Id;
            overstockPartsAdjustBill.ConfirmorName = userInfo.Name;
            overstockPartsAdjustBill.ConfirmationTime = DateTime.Now;
            this.UpdateOverstockPartsAdjustBillValidate(overstockPartsAdjustBill);
            var overstockPartsAdjustDetails = overstockPartsAdjustBill.OverstockPartsAdjustDetails.ToArray();
            if(overstockPartsAdjustBill.StorageCompanyType == (int)DcsCompanyType.代理库) {
                var warehouses = ObjectContext.Warehouses.Where(p => ObjectContext.SalesUnitAffiWarehouses.Any(r =>
                    ObjectContext.SalesUnits.Any(t => t.BranchId == overstockPartsAdjustBill.BranchId && t.OwnerCompanyId == overstockPartsAdjustBill.OriginalStorageCompanyId &&
                    ObjectContext.SalesUnits.Any(v => v.Id == overstockPartsAdjustBill.SalesUnitId && v.PartsSalesCategoryId == t.PartsSalesCategoryId && v.Status == (int)DcsMasterDataStatus.有效)
                    && t.Id == r.SalesUnitId && t.Status == (int)DcsMasterDataStatus.有效) && r.WarehouseId == p.Id) && p.Status == (int)DcsBaseDataStatus.有效).ToArray();
                if(warehouses.Length == 0)
                    throw new ValidationException(ErrorStrings.OverstockPartsAdjustBill_Validation8);
                if(warehouses.Length > 1)
                    throw new ValidationException(ErrorStrings.OverstockPartsAdjustBill_Validation11);
                var company = ObjectContext.Companies.SingleOrDefault(r => r.Id == overstockPartsAdjustBill.SalesUnitOwnerCompanyId && r.Status == (int)DcsMasterDataStatus.有效);
                if(company == null)
                    throw new ValidationException(ErrorStrings.OverstockPartsAdjustBill_Validation9);
                var dbwarehouses = ObjectContext.Warehouses.Where(p => ObjectContext.SalesUnitAffiWarehouses.Any(r =>
                    ObjectContext.SalesUnits.Any(t => t.BranchId == overstockPartsAdjustBill.BranchId && t.OwnerCompanyId == overstockPartsAdjustBill.DestStorageCompanyId &&
                    ObjectContext.SalesUnits.Any(v => v.Id == overstockPartsAdjustBill.SalesUnitId && v.PartsSalesCategoryId == t.PartsSalesCategoryId && v.Status == (int)DcsMasterDataStatus.有效)
                    && t.Id == r.SalesUnitId && t.Status == (int)DcsMasterDataStatus.有效) && r.WarehouseId == p.Id) && p.Status == (int)DcsBaseDataStatus.有效).ToArray();
                if(dbwarehouses.Length == 0)
                    throw new ValidationException(ErrorStrings.OverstockPartsAdjustBill_Validation8);
                if(dbwarehouses.Length > 1)
                    throw new ValidationException(ErrorStrings.OverstockPartsAdjustBill_Validation11);
                var partsOutboundPlan = new PartsOutboundPlan {
                    WarehouseId = warehouses.Single().Id,
                    WarehouseCode = warehouses.Single().Code,
                    WarehouseName = warehouses.Single().Name,
                    StorageCompanyId = overstockPartsAdjustBill.OriginalStorageCompanyId,
                    StorageCompanyCode = overstockPartsAdjustBill.OriginalStorageCompanyCode,
                    StorageCompanyName = overstockPartsAdjustBill.OriginalStorageCompanyName,
                    StorageCompanyType = overstockPartsAdjustBill.StorageCompanyType,
                    BranchId = overstockPartsAdjustBill.BranchId,
                    BranchCode = overstockPartsAdjustBill.BranchCode,
                    BranchName = overstockPartsAdjustBill.BranchName,
                    CounterpartCompanyId = overstockPartsAdjustBill.SalesUnitOwnerCompanyId,
                    CounterpartCompanyCode = company.Code,
                    CounterpartCompanyName = company.Name,
                    SourceId = overstockPartsAdjustBill.Id,
                    SourceCode = overstockPartsAdjustBill.Code,
                    OutboundType = (int)DcsPartsOutboundType.积压件调剂,
                    OriginalRequirementBillId = overstockPartsAdjustBill.OriginalRequirementBillId,
                    OriginalRequirementBillCode = overstockPartsAdjustBill.OriginalRequirementBillCode,
                    OriginalRequirementBillType = overstockPartsAdjustBill.OriginalRequirementBillType,
                    Status = (int)DcsPartsOutboundPlanStatus.新建,
                    Remark = overstockPartsAdjustBill.Remark,
                    ReceivingCompanyId = overstockPartsAdjustBill.DestStorageCompanyId,
                    ReceivingCompanyCode = overstockPartsAdjustBill.DestStorageCompanyCode,
                    ReceivingCompanyName = overstockPartsAdjustBill.DestStorageCompanyName,
                    ReceivingWarehouseId = dbwarehouses.Single().Id,
                    ReceivingWarehouseCode = dbwarehouses.Single().Code,
                    ReceivingWarehouseName = dbwarehouses.Single().Name
                };
                var salesUnits = ObjectContext.SalesUnits.Where(r => ObjectContext.SalesUnitAffiWarehouses.Any(v => v.SalesUnitId == r.Id && v.WarehouseId == partsOutboundPlan.WarehouseId)).ToArray();
                if(salesUnits.Length != 1) {
                    throw new ValidationException(ErrorStrings.OverstockPartsAdjustBill_Error_SalesUnits);
                }
                var salesUnit = salesUnits[0];
                partsOutboundPlan.PartsSalesCategoryId = salesUnit.PartsSalesCategoryId;
                foreach(var overstockPartsAdjustDetail in overstockPartsAdjustDetails) {
                    partsOutboundPlan.PartsOutboundPlanDetails.Add(new PartsOutboundPlanDetail {
                        SparePartId = overstockPartsAdjustDetail.SparePartId,
                        SparePartCode = overstockPartsAdjustDetail.SparePartCode,
                        SparePartName = overstockPartsAdjustDetail.SparePartName,
                        PlannedAmount = overstockPartsAdjustDetail.Quantity,
                        OutboundFulfillment = 0,
                        Price = overstockPartsAdjustDetail.dealprice,
                        Remark = overstockPartsAdjustDetail.Remark
                    });
                }
                DomainService.生成配件出库计划(partsOutboundPlan);
            }
        }

        public void 完成积压件调剂单(OverstockPartsAdjustBill overstockPartsAdjustBill) {
            var dboverstockPartsAdjustBill = ObjectContext.OverstockPartsAdjustBills.Where(r => r.Id == overstockPartsAdjustBill.Id && r.Status == (int)DcsOverstockPartsAdjustBillStatus.确认).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dboverstockPartsAdjustBill == null)
                throw new ValidationException(ErrorStrings.OverstockPartsAdjustBill_Validation4);
            var overstockPartsAdjustDetails = overstockPartsAdjustBill.OverstockPartsAdjustDetails;
            var warehouse = ObjectContext.Warehouses.FirstOrDefault(r => r.BranchId == overstockPartsAdjustBill.BranchId && r.StorageCompanyId == overstockPartsAdjustBill.SalesUnitOwnerCompanyId && r.Type == (int)DcsWarehouseType.虚拟库 && r.Status == (int)DcsBaseDataStatus.有效);

            if(warehouse == null)
                throw new ValidationException(ErrorStrings.OverstockPartsAdjustBill_Validation7);
            var company = ObjectContext.Companies.SingleOrDefault(r => r.Id == overstockPartsAdjustBill.SalesUnitOwnerCompanyId && r.Status == (int)DcsMasterDataStatus.有效);
            if(company == null)
                throw new ValidationException(ErrorStrings.OverstockPartsAdjustBill_Validation9);
            if(overstockPartsAdjustBill.StorageCompanyType != (int)DcsCompanyType.代理库 && overstockPartsAdjustBill.StorageCompanyType != (int)DcsCompanyType.服务站 && overstockPartsAdjustBill.StorageCompanyType != (int)DcsCompanyType.服务站兼代理库)
                throw new ValidationException(ErrorStrings.OverstockPartsAdjustBill_Validation13);
            var sourcePartsSalesOrder = ObjectContext.PartsSalesOrders.SingleOrDefault(r => r.Id == overstockPartsAdjustBill.SourceId);
            if(sourcePartsSalesOrder == null) {
                throw new ValidationException(ErrorStrings.OverstockPartsAdjustBill_Error_SalesOrder);
            }
            UpdateToDatabase(overstockPartsAdjustBill);
            var userInfo = Utils.GetCurrentUserInfo();
            overstockPartsAdjustBill.AccomplisherId = userInfo.Id;
            overstockPartsAdjustBill.AccomplisherName = userInfo.Name;
            overstockPartsAdjustBill.AccomplishTime = DateTime.Now;
            #region 代理库
            //代理库
            if(overstockPartsAdjustBill.StorageCompanyType == (int)DcsCompanyType.代理库 || overstockPartsAdjustBill.StorageCompanyType == (int)DcsCompanyType.服务站兼代理库) {
                var quantity = overstockPartsAdjustDetails.Sum(r => r.Quantity);
                var partsOutboundBillIds = ObjectContext.PartsOutboundBills.Where(v => ObjectContext.PartsOutboundPlans.Any(r => r.SourceId == overstockPartsAdjustBill.Id && r.Id == v.PartsOutboundPlanId)).Select(r => r.Id).ToArray();
                var outboundAmount = ObjectContext.PartsOutboundBillDetails.Where(e => partsOutboundBillIds.Contains(e.PartsOutboundBillId)).Sum(v => v.OutboundAmount);
                var inboundAmount = ObjectContext.PartsLogisticBatchItemDetails.
                    Where(r => ObjectContext.PartsLogisticBatchBillDetails.Any(v => v.BillType == (int)DcsPartsLogisticBatchBillDetailBillType.配件出库单 && partsOutboundBillIds.Contains(v.BillId) && v.PartsLogisticBatchId == r.PartsLogisticBatchId)).Sum(r => r.InboundAmount);
                if(outboundAmount != inboundAmount)
                    throw new ValidationException(ErrorStrings.OverstockPartsAdjustBill_Validation6);
                if(quantity == inboundAmount)
                    overstockPartsAdjustBill.Status = (int)DcsOverstockPartsAdjustBillStatus.完成;
                else
                    overstockPartsAdjustBill.Status = (int)DcsOverstockPartsAdjustBillStatus.终止;
            }
            #endregion
            this.UpdateOverstockPartsAdjustBillValidate(overstockPartsAdjustBill);
            #region 不同企业类型相同的部分
            //var partsDetails = overstockPartsAdjustDetails.Select(v => new EnterprisePartsCostAch.PartsDetail {
            //    SparePartId = v.SparePartId,
            //    Quantity = v.Quantity,
            //    Price = v.TransferOutPrice
            //}).ToArray();
            //新增入库计划
            var partsInboundPlan = new PartsInboundPlan {
                WarehouseId = warehouse.Id,
                WarehouseCode = warehouse.Code,
                WarehouseName = warehouse.Name,
                StorageCompanyId = overstockPartsAdjustBill.SalesUnitOwnerCompanyId,
                StorageCompanyCode = company.Code,
                StorageCompanyName = company.Name,
                StorageCompanyType = company.Type,
                BranchId = overstockPartsAdjustBill.BranchId,
                BranchCode = overstockPartsAdjustBill.BranchCode,
                BranchName = overstockPartsAdjustBill.BranchName,
                CounterpartCompanyId = overstockPartsAdjustBill.OriginalStorageCompanyId,
                CounterpartCompanyCode = overstockPartsAdjustBill.OriginalStorageCompanyCode,
                CounterpartCompanyName = overstockPartsAdjustBill.OriginalStorageCompanyName,
                SourceId = overstockPartsAdjustBill.Id,
                SourceCode = overstockPartsAdjustBill.Code,
                InboundType = (int)DcsPartsInboundType.销售退货,
                CustomerAccountId = overstockPartsAdjustBill.OriginalStorageCoCustAccountId,
                OriginalRequirementBillId = overstockPartsAdjustBill.OriginalRequirementBillId,
                OriginalRequirementBillCode = overstockPartsAdjustBill.OriginalRequirementBillCode,
                OriginalRequirementBillType = overstockPartsAdjustBill.OriginalRequirementBillType,
                Status = (int)DcsPartsInboundPlanStatus.检验完成,
                Remark = overstockPartsAdjustBill.Remark,
                PartsSalesCategoryId = sourcePartsSalesOrder.SalesCategoryId
            };
            //新增入库检验单
            var partsInboundCheckBill = new PartsInboundCheckBill {
                WarehouseId = warehouse.Id,
                WarehouseCode = warehouse.Code,
                WarehouseName = warehouse.Name,
                StorageCompanyId = overstockPartsAdjustBill.SalesUnitOwnerCompanyId,
                StorageCompanyCode = company.Code,
                StorageCompanyName = company.Name,
                StorageCompanyType = company.Type,
                BranchId = overstockPartsAdjustBill.BranchId,
                BranchCode = overstockPartsAdjustBill.BranchCode,
                BranchName = overstockPartsAdjustBill.BranchName,
                CounterpartCompanyId = overstockPartsAdjustBill.OriginalStorageCompanyId,
                CounterpartCompanyCode = overstockPartsAdjustBill.OriginalStorageCompanyCode,
                CounterpartCompanyName = overstockPartsAdjustBill.OriginalStorageCompanyName,
                InboundType = (int)DcsPartsInboundType.销售退货,
                CustomerAccountId = overstockPartsAdjustBill.OriginalStorageCoCustAccountId,
                OriginalRequirementBillId = overstockPartsAdjustBill.OriginalRequirementBillId,
                OriginalRequirementBillCode = overstockPartsAdjustBill.OriginalRequirementBillCode,
                OriginalRequirementBillType = overstockPartsAdjustBill.OriginalRequirementBillType,
                Status = (int)DcsPartsInboundCheckBillStatus.上架完成,
                Remark = overstockPartsAdjustBill.Remark,
                SettlementStatus = (int)DcsPartsSettlementStatus.待结算,
                PartsSalesCategoryId = sourcePartsSalesOrder.SalesCategoryId
            };
            //新增出库计划
            var partsOutboundPlan = new PartsOutboundPlan {
                WarehouseId = warehouse.Id,
                WarehouseCode = warehouse.Code,
                WarehouseName = warehouse.Name,
                StorageCompanyId = overstockPartsAdjustBill.SalesUnitOwnerCompanyId,
                StorageCompanyCode = company.Code,
                StorageCompanyName = company.Name,
                StorageCompanyType = company.Type,
                BranchId = overstockPartsAdjustBill.BranchId,
                BranchCode = overstockPartsAdjustBill.BranchCode,
                BranchName = overstockPartsAdjustBill.BranchName,
                CounterpartCompanyId = overstockPartsAdjustBill.DestStorageCompanyId,
                CounterpartCompanyCode = overstockPartsAdjustBill.DestStorageCompanyCode,
                CounterpartCompanyName = overstockPartsAdjustBill.DestStorageCompanyName,
                SourceId = overstockPartsAdjustBill.SourceId,
                SourceCode = overstockPartsAdjustBill.SourceCode,
                OutboundType = (int)DcsPartsOutboundType.配件销售,
                CustomerAccountId = overstockPartsAdjustBill.DestStorageCoCustomerAccountId,
                OriginalRequirementBillId = overstockPartsAdjustBill.OriginalRequirementBillId,
                OriginalRequirementBillCode = overstockPartsAdjustBill.OriginalRequirementBillCode,
                OriginalRequirementBillType = overstockPartsAdjustBill.OriginalRequirementBillType,
                Status = (int)DcsPartsOutboundPlanStatus.出库完成,
                Remark = overstockPartsAdjustBill.Remark,
                ReceivingCompanyId = overstockPartsAdjustBill.DestStorageCompanyId,
                ReceivingCompanyCode = overstockPartsAdjustBill.DestStorageCompanyCode,
                ReceivingCompanyName = overstockPartsAdjustBill.DestStorageCompanyName,
                PartsSalesCategoryId = sourcePartsSalesOrder.SalesCategoryId
            };
            //新增出库单
            var partsOutboundBill = new PartsOutboundBill {
                WarehouseId = warehouse.Id,
                WarehouseCode = warehouse.Code,
                WarehouseName = warehouse.Name,
                StorageCompanyId = overstockPartsAdjustBill.SalesUnitOwnerCompanyId,
                StorageCompanyCode = company.Code,
                StorageCompanyName = company.Name,
                StorageCompanyType = company.Type,
                BranchId = overstockPartsAdjustBill.BranchId,
                BranchCode = overstockPartsAdjustBill.BranchCode,
                BranchName = overstockPartsAdjustBill.BranchName,
                CounterpartCompanyId = overstockPartsAdjustBill.DestStorageCompanyId,
                CounterpartCompanyCode = overstockPartsAdjustBill.DestStorageCompanyCode,
                CounterpartCompanyName = overstockPartsAdjustBill.DestStorageCompanyName,
                OutboundType = (int)DcsPartsOutboundType.配件销售,
                CustomerAccountId = overstockPartsAdjustBill.DestStorageCoCustomerAccountId,
                OriginalRequirementBillId = overstockPartsAdjustBill.OriginalRequirementBillId,
                OriginalRequirementBillCode = overstockPartsAdjustBill.OriginalRequirementBillCode,
                OriginalRequirementBillType = overstockPartsAdjustBill.OriginalRequirementBillType,
                Remark = overstockPartsAdjustBill.Remark,
                SettlementStatus = (int)DcsPartsSettlementStatus.待结算,
                ReceivingCompanyId = overstockPartsAdjustBill.DestStorageCompanyId,
                ReceivingCompanyCode = overstockPartsAdjustBill.DestStorageCompanyCode,
                ReceivingCompanyName = overstockPartsAdjustBill.DestStorageCompanyName,
                PartsSalesCategoryId = sourcePartsSalesOrder.SalesCategoryId
            };
            var sparePartCostPrices = new Dictionary<int, decimal>();
            //if(company.Type == (int)DcsCompanyType.分公司) {
            //    var tempDictionary = new EnterprisePartsCostAch(this.DomainService).提交积压件调剂事务(overstockPartsAdjustBill.SalesUnitOwnerCompanyId, partsDetails);
            //    sparePartCostPrices = tempDictionary;
            //}else {
            //    foreach(var partsDetail in partsDetails) {
            //        sparePartCostPrices = new Dictionary<int, decimal>();
            //        sparePartCostPrices.Add(partsDetail.SparePartId, 0);
            //    }
            //}
            foreach(var overstockPartsAdjustDetail in overstockPartsAdjustDetails) {
                if(!sparePartCostPrices.ContainsKey(overstockPartsAdjustDetail.SparePartId))
                    throw new ValidationException(string.Format(ErrorStrings.OverstockPartsAdjustBill_Validation14, overstockPartsAdjustDetail.SparePartCode));
                //新增入库计划清单
                partsInboundPlan.PartsInboundPlanDetails.Add(new PartsInboundPlanDetail {
                    SparePartId = overstockPartsAdjustDetail.SparePartId,
                    SparePartCode = overstockPartsAdjustDetail.SparePartCode,
                    SparePartName = overstockPartsAdjustDetail.SparePartName,
                    PlannedAmount = overstockPartsAdjustDetail.Quantity,
                    InspectedQuantity = overstockPartsAdjustDetail.Quantity,
                    Price = overstockPartsAdjustDetail.TransferOutPrice,
                    Remark = overstockPartsAdjustDetail.Remark
                });
                //新增入库检验单清单
                partsInboundCheckBill.PartsInboundCheckBillDetails.Add(new PartsInboundCheckBillDetail {
                    SparePartId = overstockPartsAdjustDetail.SparePartId,
                    SparePartCode = overstockPartsAdjustDetail.SparePartCode,
                    SparePartName = overstockPartsAdjustDetail.SparePartName,
                    InspectedQuantity = overstockPartsAdjustDetail.Quantity,
                    SettlementPrice = overstockPartsAdjustDetail.TransferOutPrice,
                    CostPrice = sparePartCostPrices[overstockPartsAdjustDetail.SparePartId],
                    WarehouseAreaId = GlobalVar.UNREAL_WAREHOUSEAREA_ID,
                    WarehouseAreaCode = GlobalVar.UNREAL_WAREHOUSEAREA_CODE,
                    Remark = overstockPartsAdjustDetail.Remark
                });
                //新增出库计划清单
                partsOutboundPlan.PartsOutboundPlanDetails.Add(new PartsOutboundPlanDetail {
                    SparePartId = overstockPartsAdjustDetail.SparePartId,
                    SparePartCode = overstockPartsAdjustDetail.SparePartCode,
                    SparePartName = overstockPartsAdjustDetail.SparePartName,
                    PlannedAmount = overstockPartsAdjustDetail.Quantity,
                    OutboundFulfillment = overstockPartsAdjustDetail.Quantity,
                    Price = overstockPartsAdjustDetail.TransferInPrice,
                    Remark = overstockPartsAdjustDetail.Remark
                });
                //新增出库单清单
                partsOutboundBill.PartsOutboundBillDetails.Add(new PartsOutboundBillDetail {
                    SparePartId = overstockPartsAdjustDetail.SparePartId,
                    SparePartCode = overstockPartsAdjustDetail.SparePartCode,
                    SparePartName = overstockPartsAdjustDetail.SparePartName,
                    OutboundAmount = overstockPartsAdjustDetail.Quantity,
                    SettlementPrice = overstockPartsAdjustDetail.TransferInPrice,
                    CostPrice = sparePartCostPrices[overstockPartsAdjustDetail.SparePartId],
                    WarehouseAreaId = GlobalVar.UNREAL_WAREHOUSEAREA_ID,
                    WarehouseAreaCode = GlobalVar.UNREAL_WAREHOUSEAREA_CODE,
                    Remark = overstockPartsAdjustDetail.Remark
                });
            }
            partsInboundPlan.PartsInboundCheckBills.Add(partsInboundCheckBill);
            partsOutboundPlan.PartsOutboundBills.Add(partsOutboundBill);
            InsertToDatabase(partsInboundPlan);
            new PartsInboundPlanAch(this.DomainService).InsertPartsInboundPlanValidate(partsInboundPlan);
            InsertToDatabase(partsInboundCheckBill);
            new PartsInboundCheckBillAch(this.DomainService).InsertPartsInboundCheckBillValidate(partsInboundCheckBill);
            InsertToDatabase(partsOutboundPlan);
            new PartsOutboundPlanAch(this.DomainService).InsertPartsOutboundPlanValidate(partsOutboundPlan);
            InsertToDatabase(partsOutboundBill);
            new PartsOutboundBillAch(this.DomainService).InsertPartsOutboundBillValidate(partsOutboundBill);
            #endregion
            #region 经销商
            //经销商
            if(overstockPartsAdjustBill.StorageCompanyType == (int)DcsCompanyType.服务站) {
                overstockPartsAdjustBill.Status = (int)DcsOverstockPartsAdjustBillStatus.完成;
                //新增配件发运单
                var partsShippingOrder = new PartsShippingOrder {
                    BranchId = overstockPartsAdjustBill.BranchId,
                    ShippingCompanyId = overstockPartsAdjustBill.OriginalStorageCompanyId,
                    ShippingCompanyCode = overstockPartsAdjustBill.OriginalStorageCompanyCode,
                    ShippingCompanyName = overstockPartsAdjustBill.OriginalStorageCompanyName,
                    SettlementCompanyId = overstockPartsAdjustBill.SalesUnitOwnerCompanyId,
                    SettlementCompanyCode = overstockPartsAdjustBill.SalesUnitOwnerCompanyCode,
                    SettlementCompanyName = overstockPartsAdjustBill.SalesUnitOwnerCompanyName,
                    ReceivingCompanyId = overstockPartsAdjustBill.DestStorageCompanyId,
                    ReceivingCompanyCode = overstockPartsAdjustBill.DestStorageCompanyCode,
                    ReceivingCompanyName = overstockPartsAdjustBill.DestStorageCompanyName,
                    OriginalRequirementBillId = overstockPartsAdjustBill.Id,
                    OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.积压件调剂单,
                    Status = (int)DcsPartsShippingOrderStatus.收货确认,
                    Remark = overstockPartsAdjustBill.Remark,
                    Type = (int)DcsPartsShippingOrderType.调剂
                };
                //新增配件发运单清单
                var partsShippingOrderDetails = overstockPartsAdjustDetails.GroupBy(v => v.SparePartId).Select(v => new PartsShippingOrderDetail {
                    SparePartId = v.Key,
                    SparePartCode = v.First().SparePartCode,
                    SparePartName = v.First().SparePartName,
                    ShippingAmount = v.Sum(r => r.Quantity),
                    ConfirmedAmount = v.Sum(r => r.Quantity),
                    InTransitDamageLossAmount = 0,
                    SettlementPrice = 0,
                }).ToArray();
                foreach(var partsShippingOrderDetail in partsShippingOrderDetails)
                    partsShippingOrder.PartsShippingOrderDetails.Add(partsShippingOrderDetail);
                //新增配件发运单关联单
                var partsShippingOrderRef = new PartsShippingOrderRef();
                partsShippingOrder.PartsShippingOrderRefs.Add(partsShippingOrderRef);
                partsOutboundBill.PartsShippingOrderRefs.Add(partsShippingOrderRef);
                InsertToDatabase(partsShippingOrder);
                DomainService.InsertPartsShippingOrderValidate(partsShippingOrder);
                //服务站出入库
                //调出服务站出库,更新经销商配件库存
                foreach(var overstockPartsAdjustDetail in overstockPartsAdjustDetails) {
                    var dealerPartsStock = ObjectContext.DealerPartsStocks.SingleOrDefault(r => r.DealerId == overstockPartsAdjustBill.OriginalStorageCompanyId && r.SubDealerId == -1 && r.SalesCategoryId == sourcePartsSalesOrder.SalesCategoryId && r.SparePartId == overstockPartsAdjustDetail.SparePartId);
                    if(dealerPartsStock == null) {
                        throw new ValidationException(ErrorStrings.OverstockPartsAdjustBill_Error_DealerStock);
                    }
                    if(overstockPartsAdjustDetail.finishQuantity == default(int) || overstockPartsAdjustDetail.finishQuantity == null) {
                        var quantity = dealerPartsStock.Quantity;
                        if(quantity < 0)
                            throw new ValidationException(ErrorStrings.OverstockPartsAdjustBill_Error_Quantity);
                        dealerPartsStock.Quantity = quantity;
                    }else {
                        var quantity = dealerPartsStock.Quantity - overstockPartsAdjustDetail.finishQuantity.Value;
                        if(quantity < 0)
                            throw new ValidationException(ErrorStrings.OverstockPartsAdjustBill_Error_Quantity);
                        dealerPartsStock.Quantity = quantity;
                    }
                    DomainService.UpdateDealerPartsStock(dealerPartsStock);
                }
                //调入服务站入库
                foreach(var overstockPartsAdjustDetail in overstockPartsAdjustDetails) {
                    var tmpSparePartIds = overstockPartsAdjustDetails.Select(r => r.SparePartId);
                    var dealerPartsStocks = ObjectContext.DealerPartsStocks.SingleOrDefault
                        (r => r.DealerId == overstockPartsAdjustBill.DestStorageCompanyId && r.SubDealerId == -1 && tmpSparePartIds.Any(v => v == r.SparePartId) && r.SalesCategoryId == sourcePartsSalesOrder.SalesCategoryId && r.SparePartId == overstockPartsAdjustDetail.SparePartId);
                    if(dealerPartsStocks != null) {
                        if(overstockPartsAdjustDetail.finishQuantity == default(int) || overstockPartsAdjustDetail.finishQuantity == null) {
                            var quantity = dealerPartsStocks.Quantity;
                            if(quantity < 0)
                                throw new ValidationException(ErrorStrings.OverstockPartsAdjustBill_Error_Quantity);
                            dealerPartsStocks.Quantity = quantity;
                        }else {
                            var quantity = dealerPartsStocks.Quantity + overstockPartsAdjustDetail.finishQuantity.Value;
                            if(quantity < 0)
                                throw new ValidationException(ErrorStrings.OverstockPartsAdjustBill_Error_Quantity);
                            dealerPartsStocks.Quantity = quantity;
                        }
                        DomainService.UpdateDealerPartsStock(dealerPartsStocks);
                    }else {
                        var dealerPartsSto = new DealerPartsStock {
                            DealerId = overstockPartsAdjustBill.DestStorageCompanyId,
                            DealerCode = overstockPartsAdjustBill.DestStorageCompanyCode,
                            DealerName = overstockPartsAdjustBill.DestStorageCompanyName,
                            SubDealerId = -1,
                            BranchId = overstockPartsAdjustBill.BranchId,
                            SalesCategoryId = sourcePartsSalesOrder.SalesCategoryId,
                            SalesCategoryName = sourcePartsSalesOrder.SalesCategoryName,
                            SparePartId = overstockPartsAdjustDetail.SparePartId,
                            SparePartCode = overstockPartsAdjustDetail.SparePartCode,
                            Quantity = overstockPartsAdjustDetail.Quantity
                        };
                        InsertToDatabase(dealerPartsSto);
                        new DealerPartsStockAch(this.DomainService).InsertDealerPartsStockValidate(dealerPartsSto);
                    }
                }
            }
            #endregion
        }

        public void 调入单位完成积压件调剂单(OverstockPartsAdjustBill overstockPartsAdjustBill) {
            var dboverstockPartsAdjustBill = ObjectContext.OverstockPartsAdjustBills.Where(r => r.Id == overstockPartsAdjustBill.Id && r.Status == (int)DcsOverstockPartsAdjustBillStatus.确认).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dboverstockPartsAdjustBill == null)
                throw new ValidationException(ErrorStrings.OverstockPartsAdjustBill_Validation4);
            var overstockPartsAdjustDetails = overstockPartsAdjustBill.OverstockPartsAdjustDetails;
            var warehouse = ObjectContext.Warehouses.FirstOrDefault(r => r.BranchId == overstockPartsAdjustBill.BranchId && r.StorageCompanyId == overstockPartsAdjustBill.SalesUnitOwnerCompanyId && r.Type == (int)DcsWarehouseType.虚拟库 && r.Status == (int)DcsBaseDataStatus.有效);

            if(warehouse == null)
                throw new ValidationException(ErrorStrings.OverstockPartsAdjustBill_Validation7);
            var company = ObjectContext.Companies.SingleOrDefault(r => r.Id == overstockPartsAdjustBill.SalesUnitOwnerCompanyId && r.Status == (int)DcsMasterDataStatus.有效);
            if(company == null)
                throw new ValidationException(ErrorStrings.OverstockPartsAdjustBill_Validation9);
            if(overstockPartsAdjustBill.StorageCompanyType != (int)DcsCompanyType.代理库 && overstockPartsAdjustBill.StorageCompanyType != (int)DcsCompanyType.服务站 && overstockPartsAdjustBill.StorageCompanyType != (int)DcsCompanyType.服务站兼代理库)
                throw new ValidationException(ErrorStrings.OverstockPartsAdjustBill_Validation13);
            var sourcePartsSalesOrder = ObjectContext.PartsSalesOrders.SingleOrDefault(r => r.Id == overstockPartsAdjustBill.SourceId);
            if(sourcePartsSalesOrder == null) {
                throw new ValidationException(ErrorStrings.OverstockPartsAdjustBill_Error_SalesOrder);
            }
            UpdateToDatabase(overstockPartsAdjustBill);
            var userInfo = Utils.GetCurrentUserInfo();
            overstockPartsAdjustBill.AccomplisherId = userInfo.Id;
            overstockPartsAdjustBill.AccomplisherName = userInfo.Name;
            overstockPartsAdjustBill.AccomplishTime = DateTime.Now;
            #region 代理库
            //代理库
            if(overstockPartsAdjustBill.StorageCompanyType == (int)DcsCompanyType.代理库 || overstockPartsAdjustBill.StorageCompanyType == (int)DcsCompanyType.服务站兼代理库) {
                var quantity = overstockPartsAdjustDetails.Sum(r => r.Quantity);
                var partsOutboundBillIds = ObjectContext.PartsOutboundBills.Where(v => ObjectContext.PartsOutboundPlans.Any(r => r.SourceId == overstockPartsAdjustBill.Id && r.Id == v.PartsOutboundPlanId)).Select(r => r.Id).ToArray();
                var outboundAmount = ObjectContext.PartsOutboundBillDetails.Where(e => partsOutboundBillIds.Contains(e.PartsOutboundBillId)).Sum(v => v.OutboundAmount);
                var inboundAmount = ObjectContext.PartsLogisticBatchItemDetails.
                    Where(r => ObjectContext.PartsLogisticBatchBillDetails.Any(v => v.BillType == (int)DcsPartsLogisticBatchBillDetailBillType.配件出库单 && partsOutboundBillIds.Contains(v.BillId) && v.PartsLogisticBatchId == r.PartsLogisticBatchId)).Sum(r => r.InboundAmount);
                if(!(quantity == inboundAmount && quantity == outboundAmount)) {
                    throw new ValidationException("存在部分在途，流程暂不能关闭");
                }
            }
            #endregion
            this.UpdateOverstockPartsAdjustBillValidate(overstockPartsAdjustBill);
            #region 不同企业类型相同的部分
            //var partsDetails = overstockPartsAdjustDetails.Select(v => new EnterprisePartsCostAch.PartsDetail {
            //    SparePartId = v.SparePartId,
            //    Quantity = v.Quantity,
            //    Price = v.TransferOutPrice
            //}).ToArray();
            //新增入库计划
            var partsInboundPlan = new PartsInboundPlan {
                WarehouseId = warehouse.Id,
                WarehouseCode = warehouse.Code,
                WarehouseName = warehouse.Name,
                StorageCompanyId = overstockPartsAdjustBill.SalesUnitOwnerCompanyId,
                StorageCompanyCode = company.Code,
                StorageCompanyName = company.Name,
                StorageCompanyType = company.Type,
                BranchId = overstockPartsAdjustBill.BranchId,
                BranchCode = overstockPartsAdjustBill.BranchCode,
                BranchName = overstockPartsAdjustBill.BranchName,
                CounterpartCompanyId = overstockPartsAdjustBill.OriginalStorageCompanyId,
                CounterpartCompanyCode = overstockPartsAdjustBill.OriginalStorageCompanyCode,
                CounterpartCompanyName = overstockPartsAdjustBill.OriginalStorageCompanyName,
                SourceId = overstockPartsAdjustBill.Id,
                SourceCode = overstockPartsAdjustBill.Code,
                InboundType = (int)DcsPartsInboundType.销售退货,
                CustomerAccountId = overstockPartsAdjustBill.OriginalStorageCoCustAccountId,
                OriginalRequirementBillId = overstockPartsAdjustBill.OriginalRequirementBillId,
                OriginalRequirementBillCode = overstockPartsAdjustBill.OriginalRequirementBillCode,
                OriginalRequirementBillType = overstockPartsAdjustBill.OriginalRequirementBillType,
                Status = (int)DcsPartsInboundPlanStatus.检验完成,
                Remark = overstockPartsAdjustBill.Remark,
                PartsSalesCategoryId = sourcePartsSalesOrder.SalesCategoryId
            };
            //新增入库检验单
            var partsInboundCheckBill = new PartsInboundCheckBill {
                WarehouseId = warehouse.Id,
                WarehouseCode = warehouse.Code,
                WarehouseName = warehouse.Name,
                StorageCompanyId = overstockPartsAdjustBill.SalesUnitOwnerCompanyId,
                StorageCompanyCode = company.Code,
                StorageCompanyName = company.Name,
                StorageCompanyType = company.Type,
                BranchId = overstockPartsAdjustBill.BranchId,
                BranchCode = overstockPartsAdjustBill.BranchCode,
                BranchName = overstockPartsAdjustBill.BranchName,
                CounterpartCompanyId = overstockPartsAdjustBill.OriginalStorageCompanyId,
                CounterpartCompanyCode = overstockPartsAdjustBill.OriginalStorageCompanyCode,
                CounterpartCompanyName = overstockPartsAdjustBill.OriginalStorageCompanyName,
                InboundType = (int)DcsPartsInboundType.销售退货,
                CustomerAccountId = overstockPartsAdjustBill.OriginalStorageCoCustAccountId,
                OriginalRequirementBillId = overstockPartsAdjustBill.OriginalRequirementBillId,
                OriginalRequirementBillCode = overstockPartsAdjustBill.OriginalRequirementBillCode,
                OriginalRequirementBillType = overstockPartsAdjustBill.OriginalRequirementBillType,
                Status = (int)DcsPartsInboundCheckBillStatus.上架完成,
                Remark = overstockPartsAdjustBill.Remark,
                SettlementStatus = (int)DcsPartsSettlementStatus.待结算,
                PartsSalesCategoryId = sourcePartsSalesOrder.SalesCategoryId
            };
            //新增出库计划
            var partsOutboundPlan = new PartsOutboundPlan {
                WarehouseId = warehouse.Id,
                WarehouseCode = warehouse.Code,
                WarehouseName = warehouse.Name,
                StorageCompanyId = overstockPartsAdjustBill.SalesUnitOwnerCompanyId,
                StorageCompanyCode = company.Code,
                StorageCompanyName = company.Name,
                StorageCompanyType = company.Type,
                BranchId = overstockPartsAdjustBill.BranchId,
                BranchCode = overstockPartsAdjustBill.BranchCode,
                BranchName = overstockPartsAdjustBill.BranchName,
                CounterpartCompanyId = overstockPartsAdjustBill.DestStorageCompanyId,
                CounterpartCompanyCode = overstockPartsAdjustBill.DestStorageCompanyCode,
                CounterpartCompanyName = overstockPartsAdjustBill.DestStorageCompanyName,
                SourceId = overstockPartsAdjustBill.SourceId,
                SourceCode = overstockPartsAdjustBill.SourceCode,
                OutboundType = (int)DcsPartsOutboundType.配件销售,
                CustomerAccountId = overstockPartsAdjustBill.DestStorageCoCustomerAccountId,
                OriginalRequirementBillId = overstockPartsAdjustBill.OriginalRequirementBillId,
                OriginalRequirementBillCode = overstockPartsAdjustBill.OriginalRequirementBillCode,
                OriginalRequirementBillType = overstockPartsAdjustBill.OriginalRequirementBillType,
                Status = (int)DcsPartsOutboundPlanStatus.出库完成,
                Remark = overstockPartsAdjustBill.Remark,
                ReceivingCompanyId = overstockPartsAdjustBill.DestStorageCompanyId,
                ReceivingCompanyCode = overstockPartsAdjustBill.DestStorageCompanyCode,
                ReceivingCompanyName = overstockPartsAdjustBill.DestStorageCompanyName,
                PartsSalesCategoryId = sourcePartsSalesOrder.SalesCategoryId
            };
            //新增出库单
            var partsOutboundBill = new PartsOutboundBill {
                WarehouseId = warehouse.Id,
                WarehouseCode = warehouse.Code,
                WarehouseName = warehouse.Name,
                StorageCompanyId = overstockPartsAdjustBill.SalesUnitOwnerCompanyId,
                StorageCompanyCode = company.Code,
                StorageCompanyName = company.Name,
                StorageCompanyType = company.Type,
                BranchId = overstockPartsAdjustBill.BranchId,
                BranchCode = overstockPartsAdjustBill.BranchCode,
                BranchName = overstockPartsAdjustBill.BranchName,
                CounterpartCompanyId = overstockPartsAdjustBill.DestStorageCompanyId,
                CounterpartCompanyCode = overstockPartsAdjustBill.DestStorageCompanyCode,
                CounterpartCompanyName = overstockPartsAdjustBill.DestStorageCompanyName,
                OutboundType = (int)DcsPartsOutboundType.配件销售,
                CustomerAccountId = overstockPartsAdjustBill.DestStorageCoCustomerAccountId,
                OriginalRequirementBillId = overstockPartsAdjustBill.OriginalRequirementBillId,
                OriginalRequirementBillCode = overstockPartsAdjustBill.OriginalRequirementBillCode,
                OriginalRequirementBillType = overstockPartsAdjustBill.OriginalRequirementBillType,
                Remark = overstockPartsAdjustBill.Remark,
                SettlementStatus = (int)DcsPartsSettlementStatus.待结算,
                ReceivingCompanyId = overstockPartsAdjustBill.DestStorageCompanyId,
                ReceivingCompanyCode = overstockPartsAdjustBill.DestStorageCompanyCode,
                ReceivingCompanyName = overstockPartsAdjustBill.DestStorageCompanyName,
                PartsSalesCategoryId = sourcePartsSalesOrder.SalesCategoryId
            };
            var sparePartCostPrices = new Dictionary<int, decimal>();
            //if(company.Type == (int)DcsCompanyType.分公司) {
            //    var tempDictionary = new EnterprisePartsCostAch(this.DomainService).提交积压件调剂事务(overstockPartsAdjustBill.SalesUnitOwnerCompanyId, partsDetails);
            //    sparePartCostPrices = tempDictionary;
            //}else {
            //    foreach(var partsDetail in partsDetails) {
            //        sparePartCostPrices = new Dictionary<int, decimal>();
            //        sparePartCostPrices.Add(partsDetail.SparePartId, 0);
            //    }
            //}
            foreach(var overstockPartsAdjustDetail in overstockPartsAdjustDetails) {
                if(!sparePartCostPrices.ContainsKey(overstockPartsAdjustDetail.SparePartId))
                    throw new ValidationException(string.Format(ErrorStrings.OverstockPartsAdjustBill_Validation14, overstockPartsAdjustDetail.SparePartCode));
                //新增入库计划清单
                partsInboundPlan.PartsInboundPlanDetails.Add(new PartsInboundPlanDetail {
                    SparePartId = overstockPartsAdjustDetail.SparePartId,
                    SparePartCode = overstockPartsAdjustDetail.SparePartCode,
                    SparePartName = overstockPartsAdjustDetail.SparePartName,
                    PlannedAmount = overstockPartsAdjustDetail.Quantity,
                    InspectedQuantity = overstockPartsAdjustDetail.Quantity,
                    Price = overstockPartsAdjustDetail.TransferOutPrice,
                    Remark = overstockPartsAdjustDetail.Remark
                });
                //新增入库检验单清单
                partsInboundCheckBill.PartsInboundCheckBillDetails.Add(new PartsInboundCheckBillDetail {
                    SparePartId = overstockPartsAdjustDetail.SparePartId,
                    SparePartCode = overstockPartsAdjustDetail.SparePartCode,
                    SparePartName = overstockPartsAdjustDetail.SparePartName,
                    InspectedQuantity = overstockPartsAdjustDetail.Quantity,
                    SettlementPrice = overstockPartsAdjustDetail.TransferOutPrice,
                    CostPrice = sparePartCostPrices[overstockPartsAdjustDetail.SparePartId],
                    WarehouseAreaId = GlobalVar.UNREAL_WAREHOUSEAREA_ID,
                    WarehouseAreaCode = GlobalVar.UNREAL_WAREHOUSEAREA_CODE,
                    Remark = overstockPartsAdjustDetail.Remark
                });
                //新增出库计划清单
                partsOutboundPlan.PartsOutboundPlanDetails.Add(new PartsOutboundPlanDetail {
                    SparePartId = overstockPartsAdjustDetail.SparePartId,
                    SparePartCode = overstockPartsAdjustDetail.SparePartCode,
                    SparePartName = overstockPartsAdjustDetail.SparePartName,
                    PlannedAmount = overstockPartsAdjustDetail.Quantity,
                    OutboundFulfillment = overstockPartsAdjustDetail.Quantity,
                    Price = overstockPartsAdjustDetail.TransferInPrice,
                    Remark = overstockPartsAdjustDetail.Remark
                });
                //新增出库单清单
                partsOutboundBill.PartsOutboundBillDetails.Add(new PartsOutboundBillDetail {
                    SparePartId = overstockPartsAdjustDetail.SparePartId,
                    SparePartCode = overstockPartsAdjustDetail.SparePartCode,
                    SparePartName = overstockPartsAdjustDetail.SparePartName,
                    OutboundAmount = overstockPartsAdjustDetail.Quantity,
                    SettlementPrice = overstockPartsAdjustDetail.TransferInPrice,
                    CostPrice = sparePartCostPrices[overstockPartsAdjustDetail.SparePartId],
                    WarehouseAreaId = GlobalVar.UNREAL_WAREHOUSEAREA_ID,
                    WarehouseAreaCode = GlobalVar.UNREAL_WAREHOUSEAREA_CODE,
                    Remark = overstockPartsAdjustDetail.Remark
                });
            }
            partsInboundPlan.PartsInboundCheckBills.Add(partsInboundCheckBill);
            partsOutboundPlan.PartsOutboundBills.Add(partsOutboundBill);
            InsertToDatabase(partsInboundPlan);
            new PartsInboundPlanAch(this.DomainService).InsertPartsInboundPlanValidate(partsInboundPlan);
            InsertToDatabase(partsInboundCheckBill);
            new PartsInboundCheckBillAch(this.DomainService).InsertPartsInboundCheckBillValidate(partsInboundCheckBill);
            InsertToDatabase(partsOutboundPlan);
            new PartsOutboundPlanAch(this.DomainService).InsertPartsOutboundPlanValidate(partsOutboundPlan);
            InsertToDatabase(partsOutboundBill);
            new PartsOutboundBillAch(this.DomainService).InsertPartsOutboundBillValidate(partsOutboundBill);
            #endregion
            #region 经销商
            //经销商
            if(overstockPartsAdjustBill.StorageCompanyType == (int)DcsCompanyType.服务站) {
                overstockPartsAdjustBill.Status = (int)DcsOverstockPartsAdjustBillStatus.完成;
                //新增配件发运单
                var partsShippingOrder = new PartsShippingOrder {
                    BranchId = overstockPartsAdjustBill.BranchId,
                    ShippingCompanyId = overstockPartsAdjustBill.OriginalStorageCompanyId,
                    ShippingCompanyCode = overstockPartsAdjustBill.OriginalStorageCompanyCode,
                    ShippingCompanyName = overstockPartsAdjustBill.OriginalStorageCompanyName,
                    SettlementCompanyId = overstockPartsAdjustBill.SalesUnitOwnerCompanyId,
                    SettlementCompanyCode = overstockPartsAdjustBill.SalesUnitOwnerCompanyCode,
                    SettlementCompanyName = overstockPartsAdjustBill.SalesUnitOwnerCompanyName,
                    ReceivingCompanyId = overstockPartsAdjustBill.DestStorageCompanyId,
                    ReceivingCompanyCode = overstockPartsAdjustBill.DestStorageCompanyCode,
                    ReceivingCompanyName = overstockPartsAdjustBill.DestStorageCompanyName,
                    OriginalRequirementBillId = overstockPartsAdjustBill.Id,
                    OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.积压件调剂单,
                    Status = (int)DcsPartsShippingOrderStatus.收货确认,
                    Remark = overstockPartsAdjustBill.Remark,
                    Type = (int)DcsPartsShippingOrderType.调剂
                };
                //新增配件发运单清单
                var partsShippingOrderDetails = overstockPartsAdjustDetails.GroupBy(v => v.SparePartId).Select(v => new PartsShippingOrderDetail {
                    SparePartId = v.Key,
                    SparePartCode = v.First().SparePartCode,
                    SparePartName = v.First().SparePartName,
                    ShippingAmount = v.Sum(r => r.Quantity),
                    ConfirmedAmount = v.Sum(r => r.Quantity),
                    InTransitDamageLossAmount = 0,
                    SettlementPrice = 0,
                }).ToArray();
                foreach(var partsShippingOrderDetail in partsShippingOrderDetails)
                    partsShippingOrder.PartsShippingOrderDetails.Add(partsShippingOrderDetail);
                //新增配件发运单关联单
                var partsShippingOrderRef = new PartsShippingOrderRef();
                partsShippingOrder.PartsShippingOrderRefs.Add(partsShippingOrderRef);
                partsOutboundBill.PartsShippingOrderRefs.Add(partsShippingOrderRef);
                InsertToDatabase(partsShippingOrder);
                DomainService.InsertPartsShippingOrderValidate(partsShippingOrder);
                //服务站出入库
                //调出服务站出库,更新经销商配件库存
                foreach(var overstockPartsAdjustDetail in overstockPartsAdjustDetails) {
                    var dealerPartsStock = ObjectContext.DealerPartsStocks.SingleOrDefault(r => r.DealerId == overstockPartsAdjustBill.OriginalStorageCompanyId && r.SubDealerId == -1 && r.SalesCategoryId == sourcePartsSalesOrder.SalesCategoryId && r.SparePartId == overstockPartsAdjustDetail.SparePartId);
                    if(dealerPartsStock == null) {
                        throw new ValidationException(ErrorStrings.OverstockPartsAdjustBill_Error_DealerStock);
                    }
                    var quantity = dealerPartsStock.Quantity - overstockPartsAdjustDetail.Quantity;
                    if(quantity < 0)
                        throw new ValidationException(ErrorStrings.OverstockPartsAdjustBill_Error_Quantity);
                    dealerPartsStock.Quantity = quantity;
                    DomainService.UpdateDealerPartsStock(dealerPartsStock);
                }
                //调入服务站入库
                foreach(var overstockPartsAdjustDetail in overstockPartsAdjustDetails) {
                    var tmpSparePartIds = overstockPartsAdjustDetails.Select(r => r.SparePartId);
                    var dealerPartsStocks = ObjectContext.DealerPartsStocks.SingleOrDefault
                        (r => r.DealerId == overstockPartsAdjustBill.DestStorageCompanyId && r.SubDealerId == -1 && tmpSparePartIds.Any(v => v == r.SparePartId) && r.SalesCategoryId == sourcePartsSalesOrder.SalesCategoryId && r.SparePartId == overstockPartsAdjustDetail.SparePartId);
                    if(dealerPartsStocks != null) {
                        var quantity = dealerPartsStocks.Quantity + overstockPartsAdjustDetail.Quantity;
                        if(quantity < 0)
                            throw new ValidationException(ErrorStrings.OverstockPartsAdjustBill_Error_Quantity);
                        dealerPartsStocks.Quantity = quantity;
                        DomainService.UpdateDealerPartsStock(dealerPartsStocks);
                    }else {
                        var dealerPartsSto = new DealerPartsStock {
                            DealerId = overstockPartsAdjustBill.DestStorageCompanyId,
                            DealerCode = overstockPartsAdjustBill.DestStorageCompanyCode,
                            DealerName = overstockPartsAdjustBill.DestStorageCompanyName,
                            SubDealerId = -1,
                            BranchId = overstockPartsAdjustBill.BranchId,
                            SalesCategoryId = sourcePartsSalesOrder.SalesCategoryId,
                            SalesCategoryName = sourcePartsSalesOrder.SalesCategoryName,
                            SparePartId = overstockPartsAdjustDetail.SparePartId,
                            SparePartCode = overstockPartsAdjustDetail.SparePartCode,
                            Quantity = overstockPartsAdjustDetail.Quantity
                        };
                        InsertToDatabase(dealerPartsSto);
                        new DealerPartsStockAch(this.DomainService).InsertDealerPartsStockValidate(dealerPartsSto);
                    }
                }
            }
            #endregion




        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:11:45
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 作废积压件调剂单(OverstockPartsAdjustBill overstockPartsAdjustBill) {
            new OverstockPartsAdjustBillAch(this).作废积压件调剂单(overstockPartsAdjustBill);
        }
        
        [Update(UsingCustomMethod = true)]
        public void 审批积压件调剂单(OverstockPartsAdjustBill overstockPartsAdjustBill) {
            new OverstockPartsAdjustBillAch(this).审批积压件调剂单(overstockPartsAdjustBill);
        }
        
        [Update(UsingCustomMethod = true)]
        public void 确认积压件调剂单(OverstockPartsAdjustBill overstockPartsAdjustBill) {
            new OverstockPartsAdjustBillAch(this).确认积压件调剂单(overstockPartsAdjustBill);
        }
        
        [Update(UsingCustomMethod = true)]
        public void 完成积压件调剂单(OverstockPartsAdjustBill overstockPartsAdjustBill) {
            new OverstockPartsAdjustBillAch(this).完成积压件调剂单(overstockPartsAdjustBill);
        }
        
        [Update(UsingCustomMethod = true)]
        public void 调入单位完成积压件调剂单(OverstockPartsAdjustBill overstockPartsAdjustBill) {
            new OverstockPartsAdjustBillAch(this).调入单位完成积压件调剂单(overstockPartsAdjustBill);
        }
    }
}