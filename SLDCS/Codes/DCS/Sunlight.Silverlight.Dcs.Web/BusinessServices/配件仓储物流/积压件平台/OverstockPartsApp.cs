﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class OverstockPartsAppAch : DcsSerivceAchieveBase {
        public OverstockPartsAppAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 作废积压件申请单(OverstockPartsApp overstockPartsApp) {
            var dboverstockPartsApp = ObjectContext.OverstockPartsApps.Where(r => r.Id == overstockPartsApp.Id && r.Status == (int)DcsOverstockPartsAppStatus.新建).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dboverstockPartsApp == null)
                throw new ValidationException(ErrorStrings.OverstockPartsApp_Validation1);
            UpdateToDatabase(overstockPartsApp);
            var userInfo = Utils.GetCurrentUserInfo();
            overstockPartsApp.AbandonerId = userInfo.Id;
            overstockPartsApp.AbandonerName = userInfo.Name;
            overstockPartsApp.AbandonTime = DateTime.Now;
            overstockPartsApp.Status = (int)DcsOverstockPartsAppStatus.作废;
            this.UpdateOverstockPartsAppValidate(overstockPartsApp);
        }

        public void 审批积压件申请单(OverstockPartsApp overstockPartsApp) {
            var dboverstockPartsApp = ObjectContext.OverstockPartsApps.Where(r => r.Id == overstockPartsApp.Id && r.Status == (int)DcsOverstockPartsAppStatus.已提交).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dboverstockPartsApp == null)
                throw new ValidationException(ErrorStrings.OverstockPartsApp_Validation2);
            UpdateToDatabase(overstockPartsApp);
            overstockPartsApp.Status = (int)DcsOverstockPartsAppStatus.已审批;
            var overstockPartsAppDetails = overstockPartsApp.OverstockPartsAppDetails.Where(r => r.IfConfirmed.HasValue && r.IfConfirmed.Value).ToArray();
            var sparePartIds = overstockPartsAppDetails.Select(r => r.SparePartId);
            var overstockPartsInformations = ObjectContext.OverstockPartsInformations.Where(r => r.StorageCompanyId == overstockPartsApp.StorageCompanyId && r.BranchId == overstockPartsApp.BranchId && sparePartIds.Contains(r.SparePartId));
            var overstockPartsInformationIds = overstockPartsInformations.Select(v => v.SparePartId).ToArray();
            overstockPartsAppDetails = overstockPartsAppDetails.ToArray();
            foreach(var overstockPartsAppDetail in overstockPartsAppDetails) {
                if(!overstockPartsInformationIds.Contains(overstockPartsAppDetail.SparePartId)) {
                    var overstockPartsInformation = new OverstockPartsInformation {
                        StorageCompanyId = overstockPartsApp.StorageCompanyId,
                        StorageCompanyType = overstockPartsApp.StorageCompanyType,
                        BranchId = overstockPartsApp.BranchId,
                        SparePartId = overstockPartsAppDetail.SparePartId,
                        PartsSalesCategoryId = overstockPartsApp.PartsSalesCategoryId,
                        PartsSalesCategoryName = overstockPartsApp.PartsSalesCategoryName,
                        ContactPerson = overstockPartsApp.ContactPerson,
                        ContactPhone = overstockPartsApp.ContactPhone,
                        dealprice = overstockPartsAppDetail.DealPrice,
                        SalePrice = overstockPartsAppDetail.SalePrice
                    };
                    InsertToDatabase(overstockPartsInformation);
                    new OverstockPartsInformationAch(this.DomainService).InsertOverstockPartsInformationValidate(overstockPartsInformation);
                }else {
                    var detail = overstockPartsAppDetail;
                    var overstockPartsInformation = overstockPartsInformations.FirstOrDefault(r => r.SparePartId == detail.SparePartId);
                    if(overstockPartsInformation == null) {
                        throw new ValidationException(ErrorStrings.Overstock_NotExist);
                    }
                    overstockPartsInformation.StorageCompanyId = overstockPartsApp.StorageCompanyId;
                    overstockPartsInformation.StorageCompanyType = overstockPartsApp.StorageCompanyType;
                    overstockPartsInformation.BranchId = overstockPartsApp.BranchId;
                    overstockPartsInformation.SparePartId = overstockPartsAppDetail.SparePartId;
                    overstockPartsInformation.PartsSalesCategoryId = overstockPartsApp.PartsSalesCategoryId;
                    overstockPartsInformation.PartsSalesCategoryName = overstockPartsApp.PartsSalesCategoryName;
                    overstockPartsInformation.ContactPerson = overstockPartsApp.ContactPerson;
                    overstockPartsInformation.ContactPhone = overstockPartsApp.ContactPhone;
                    overstockPartsInformation.dealprice = overstockPartsAppDetail.DealPrice;
                    overstockPartsInformation.SalePrice = overstockPartsAppDetail.SalePrice;
                    UpdateToDatabase(overstockPartsInformation);
                }
            }
            var userInfo = Utils.GetCurrentUserInfo();
            overstockPartsApp.ApproverId = userInfo.Id;
            overstockPartsApp.ApproverName = userInfo.Name;
            overstockPartsApp.ApproveTime = DateTime.Now;
            this.UpdateOverstockPartsAppValidate(overstockPartsApp);
        }

        public void 驳回积压件申请单(OverstockPartsApp overstockPartsApp) {
            var dboverstockPartsApp = ObjectContext.OverstockPartsApps.Where(r => r.Id == overstockPartsApp.Id && r.Status == (int)DcsOverstockPartsAppStatus.已提交).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dboverstockPartsApp == null)
                throw new ValidationException(ErrorStrings.OverstockPartsApp_Validation3);
            UpdateToDatabase(overstockPartsApp);
            var userInfo = Utils.GetCurrentUserInfo();
            overstockPartsApp.RejecterName = userInfo.Name;
            overstockPartsApp.RejecterTime = DateTime.Now;
            overstockPartsApp.Status = (int)DcsOverstockPartsAppStatus.新建;
            this.UpdateOverstockPartsAppValidate(overstockPartsApp);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:11:45
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 作废积压件申请单(OverstockPartsApp overstockPartsApp) {
            new OverstockPartsAppAch(this).作废积压件申请单(overstockPartsApp);
        }
        
        [Update(UsingCustomMethod = true)]
        public void 审批积压件申请单(OverstockPartsApp overstockPartsApp) {
            new OverstockPartsAppAch(this).审批积压件申请单(overstockPartsApp);
        }
        
        [Update(UsingCustomMethod = true)]
        public void 驳回积压件申请单(OverstockPartsApp overstockPartsApp) {
            new OverstockPartsAppAch(this).驳回积压件申请单(overstockPartsApp);
        }
    }
}
