﻿using Sunlight.Silverlight.Web;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class OverstockPartsPlatFormBillAch : DcsSerivceAchieveBase {
        public OverstockPartsPlatFormBillAch(DcsDomainService domainService)
            : base(domainService) {
        }

        public void 上传积压件明细附件(OverstockPartsPlatFormBill overstockPartsPlatFormBill) {
            UpdateToDatabase(overstockPartsPlatFormBill);
        }

        public void EditOverstockPartsPlatFormBill(OverstockPartsPlatFormBill overstockPartsPlatFormBill) {
            var price = ObjectContext.PartsRetailGuidePrices.FirstOrDefault(r => r.Status == (int)DcsBaseDataStatus.有效 && r.SparePartId == overstockPartsPlatFormBill.SparePartId);
            if (price == null)
                throw new ValidationException(ErrorStrings.OverstockPartsPlatFormBill_Validation_Price);
            overstockPartsPlatFormBill.DiscountedPrice = decimal.Round(price.RetailGuidePrice * (decimal)overstockPartsPlatFormBill.DiscountRate, 2);
            UpdateToDatabase(overstockPartsPlatFormBill);
        }

        public void Delete(VirtualOverstockPartsPlatFormBill[] virtualOverstockPartsPlatFormBills) {
            var user = Utils.GetCurrentUserInfo();
            var userCompany = ObjectContext.Companies.First(r => r.Id == user.EnterpriseId);

            var ids = virtualOverstockPartsPlatFormBills.Select(r => r.Id).ToArray();
            var entities = ObjectContext.OverstockPartsPlatFormBills.Where(r => ids.Contains(r.Id)).ToArray();
            foreach (var entity in entities) {
                DeleteFromDatabase(entity);

                //不符合积压件推荐明细条件的不添加到推荐明细中
                if (userCompany.Type == (int)DcsCompanyType.代理库 || userCompany.Type == (int)DcsCompanyType.服务站兼代理库) {
                   var partsStock = getPartsStockForOverstock(user.EnterpriseId, entity.WarehouseId.Value, entity.SparePartId.Value);
                   if (partsStock == null || !partsStock.Any())
                       continue;
                } else if (userCompany.Type == (int)DcsCompanyType.服务站) {
                    var dealerPartsStock = getDealerPartsStockForOverstock(user.EnterpriseId, entity.SparePartId.Value);
                    if (dealerPartsStock == null || !dealerPartsStock.Any())
                        continue;
                } else {
                    throw new ValidationException(ErrorStrings.OverstockPartsRecommendBill_CompanyType);
                }

                var overstockPartsRecommendBill = new OverstockPartsRecommendBill {
                    CenterId = entity.CenterId,
                    CenterName = entity.CenterName,
                    DealerId = entity.DealerId,
                    DealerCode = entity.DealerCode,
                    DealerName = entity.DealerName,
                    WarehouseId = entity.WarehouseId,
                    WarehouseCode = entity.WarehouseCode,
                    WarehouseName = entity.WarehouseName,
                    SparePartId = entity.SparePartId,
                    SparePartCode = entity.SparePartCode,
                    SparePartName = entity.SparePartName,
                    CreateTime = DateTime.Now
                };
                InsertToDatabase(overstockPartsRecommendBill);
            }
            ObjectContext.SaveChanges();
        }


        public IEnumerable<PartsStock> getPartsStockForOverstock(int companyId, int warehouseId, int partId) {
            string SQL = string.Format(@"select ps.*  from partsstock ps
       inner join company c on c.id=ps.StorageCompanyId
       inner join Warehouse w on w.id=ps.WarehouseId
       inner join SparePart s on s.id=ps.partid
       where c.type=3 and s.status=1 and ps.StorageCompanyId = {0} and ps.warehouseid = {1} and ps.partid = {2} and 
       exists(select 1 FROM  PartsInboundCheckBill picb
                     inner join partsinboundcheckbilldetail picbd on picb.id=picbd.partsinboundcheckbillid
                     where picb.createtime > sysdate-180 and picb.InboundType =1 and picb.warehouseid =ps.warehouseid
                     and picbd.sparepartid=ps.partid) and 
       not exists(select 1 from OverstockPartsRecommendBill where CenterId =ps.storagecompanyid and SparePartid=ps.partid and warehouseid =ps.warehouseid) and
       exists(select 1 from WarehouseAreaCategory wac
                     where wac.id=ps.WarehouseAreaCategoryId and wac.category=1)", companyId, warehouseId, partId);
            var search = ObjectContext.ExecuteStoreQuery<PartsStock>(SQL).ToList();
            return search;
        }

        public IEnumerable<DealerPartsStock> getDealerPartsStockForOverstock(int companyId, int partId) {
            string SQL = string.Format(@"select dps.* from DealerPartsStock dps
       inner join AgencyDealerRelation adr on dps.dealerid = adr.dealerid
       inner join SparePart s on s.id=dps.SparePartId
       where s.status=1 and dps.dealerid = {0} and dps.SparePartId={1} and 
       exists(select 1 FROM  PartsShippingOrder pso
                     inner join PartsShippingOrderDetail psod on pso.id=psod.PartsShippingOrderid
                     where pso.ConfirmedReceptionTime > sysdate-180 and pso.Type =1 and pso.ReceivingCompanyId =dps.dealerid
                     and psod.sparepartid=dps.sparepartid and pso.status=2) and 
       not exists(select 1 from OverstockPartsRecommendBill where centerid = adr.agencyid and dealerid =dps.dealerid and SparePartid=dps.SparePartId)", companyId, partId);
            var search = ObjectContext.ExecuteStoreQuery<DealerPartsStock>(SQL).ToList();
            return search;
        }

        public void Buy(VirtualOverstockPartsPlatFormBill[] virtualOverstockPartsPlatFormBills) {
            var user = Utils.GetCurrentUserInfo();
            if (virtualOverstockPartsPlatFormBills.Any(r => r.DealerId == user.EnterpriseId) || virtualOverstockPartsPlatFormBills.Any(r => r.DealerId == null && r.CenterId == user.EnterpriseId)) {
                throw new ValidationException(ErrorStrings.OverstockPartsPlatFormBill_Validation1);
            }
            var incompany = ObjectContext.Companies.Single(r => r.Id == user.EnterpriseId);
            if (incompany.Type != (int)DcsCompanyType.服务站 && incompany.Type != (int)DcsCompanyType.服务站兼代理库 && incompany.Type != (int)DcsCompanyType.代理库) {
                throw new ValidationException(ErrorStrings.OverstockPartsRecommendBill_CompanyType);
            }
            var warehouse = ObjectContext.Warehouses.FirstOrDefault(r => r.StorageCompanyId == user.EnterpriseId && r.Status == (int)DcsBaseDataStatus.有效 && r.Type == (int)DcsWarehouseType.总库);
            if (incompany.Type != (int)DcsCompanyType.服务站 && warehouse == null)
                throw new ValidationException(ErrorStrings.OverstockPartsPlatFormBill_Validation2);

            var ids = virtualOverstockPartsPlatFormBills.Select(r => r.Id).ToList();
            var origs = this.查询积压件平台明细(null).Where(r => ids.Contains(r.Id)).ToList();
            foreach (var bill in virtualOverstockPartsPlatFormBills) {
                var orig = origs.FirstOrDefault(r => r.Id == bill.Id);
                if (orig == null)
                    throw new ValidationException(ErrorStrings.OverstockPartsPlatFormBill_Validation3);
                if (orig.Quantity <= default(int))
                    throw new ValidationException(string.Format(ErrorStrings.OverstockPartsRecommendBill_QtyIsZero, orig.SparePartCode));

                bill.WarehouseId = orig.WarehouseId;
                bill.WarehouseCode = orig.WarehouseCode;
                bill.WarehouseName = orig.WarehouseName;
                bill.Quantity = orig.Quantity;
            }
            var centerOverStockBills = virtualOverstockPartsPlatFormBills.Where(r => r.CenterId.HasValue && r.WarehouseId.HasValue).ToList();
            var dealerOverStockBills = virtualOverstockPartsPlatFormBills.Where(r => r.DealerId.HasValue && r.WarehouseId == null).ToList();
            if (centerOverStockBills.GroupBy(r => new {
                CenterId = r.CenterId.Value,
                WarehouseId = r.WarehouseId.Value,
                r.SparePartId
            }).Where(r => r.Count() > 1).Any(x => x.Any())) {
                throw new ValidationException(ErrorStrings.OverstockPartsRecommendBill_Detail_Same);
            }
            if (dealerOverStockBills.GroupBy(r => new {
                DealerId = r.DealerId.Value,
                r.SparePartId
            }).Where(r => r.Count() > 1).Any(x => x.Any())) {
                throw new ValidationException(ErrorStrings.OverstockPartsRecommendBill_Detail_Same);
            }

            var companyAddress = ObjectContext.CompanyAddresses.FirstOrDefault(r => r.Status == (int)DcsMasterDataStatus.有效 && r.CompanyId == incompany.Id);
            if (companyAddress == null)
                throw new ValidationException(ErrorStrings.OverstockPartsPlatFormBill_Validation4);

            if (centerOverStockBills.Any()) {
                //中心库 或者 服务站兼中心库 按积压件仓库拆分
                var outWarehouseIds = centerOverStockBills.Select(r => r.WarehouseId).Distinct();
                var outWarehouses = ObjectContext.Warehouses.Include("Company").Where(r => outWarehouseIds.Contains(r.Id)).ToList();

                foreach (var warehouseId in outWarehouseIds) {
                    var outWarehouse = outWarehouses.Single(r => r.Id == warehouseId);
                    var overstockTransferOrder = new OverstockTransferOrder();
                    overstockTransferOrder.Code = CodeGenerator.Generate("OverstockTransferOrder", user.EnterpriseCode);
                    overstockTransferOrder.TransferInWarehouseId = warehouse == null ? null : (int?)warehouse.Id;
                    overstockTransferOrder.TransferInWarehouseCode = warehouse == null ? null : warehouse.Code;
                    overstockTransferOrder.TransferInWarehouseName = warehouse == null ? null : warehouse.Name;
                    overstockTransferOrder.TransferInCorpId = incompany.Id;
                    overstockTransferOrder.TransferInCorpCode = incompany.Code;
                    overstockTransferOrder.TransferInCorpName = incompany.Name;
                    overstockTransferOrder.TransferOutWarehouseId = warehouseId.Value;
                    overstockTransferOrder.TransferOutWarehouseCode = outWarehouse.Code;
                    overstockTransferOrder.TransferOutWarehouseName = outWarehouse.Name;
                    overstockTransferOrder.TransferOutCorpId = outWarehouse.StorageCompanyId;
                    overstockTransferOrder.TransferOutCorpCode = outWarehouse.Company.Code;
                    overstockTransferOrder.TransferOutCorpName = outWarehouse.Company.Name;
                    overstockTransferOrder.CustomerType = incompany.Type;
                    overstockTransferOrder.ReceivingAddress = companyAddress.DetailAddress;
                    overstockTransferOrder.Status = (int)DCSOverstockPartsTransferOrderStatus.新建;
                    overstockTransferOrder.CreatorId = user.Id;
                    overstockTransferOrder.CreatorName = user.Name;
                    overstockTransferOrder.CreateTime = DateTime.Now;

                    var bills = centerOverStockBills.Where(r => r.WarehouseId == warehouseId).ToList();
                    foreach (var detail in bills) {
                        overstockTransferOrder.OverstockTransferOrderDetails.Add(new OverstockTransferOrderDetail {
                            SparePartId = detail.SparePartId,
                            SparePartCode = detail.SparePartCode,
                            SparePartName = detail.SparePartName,
                            TransferQty = detail.Quantity,
                            RetailPrice = detail.RetailGuidePrice,
                            DiscountRate = detail.DiscountRate,
                            DiscountedPrice = detail.RetailGuidePrice * (decimal)(detail.DiscountRate??1d)
                        });
                    }
                    overstockTransferOrder.TotalAmount = overstockTransferOrder.OverstockTransferOrderDetails.Sum(r => r.DiscountedPrice * r.TransferQty);
                    InsertToDatabase(overstockTransferOrder);
                }
            }

            if (dealerOverStockBills.Any()) {
                var dealerIds = dealerOverStockBills.Select(r => r.DealerId).Distinct().ToList();
                foreach (var dealerId in dealerIds) {
                    var dealerOverStockBill = dealerOverStockBills.FirstOrDefault(r => r.DealerId == dealerId);
                    var overstockTransferOrder = new OverstockTransferOrder();
                    overstockTransferOrder.Code = CodeGenerator.Generate("OverstockTransferOrder", user.EnterpriseCode);
                    overstockTransferOrder.TransferInWarehouseId = warehouse == null ? null : (int?)warehouse.Id;
                    overstockTransferOrder.TransferInWarehouseCode = warehouse == null ? null : warehouse.Code;
                    overstockTransferOrder.TransferInWarehouseName = warehouse == null ? null : warehouse.Name;
                    overstockTransferOrder.TransferInCorpId = incompany.Id;
                    overstockTransferOrder.TransferInCorpCode = incompany.Code;
                    overstockTransferOrder.TransferInCorpName = incompany.Name;
                    overstockTransferOrder.TransferOutCorpId = dealerOverStockBill.DealerId.Value;
                    overstockTransferOrder.TransferOutCorpCode = dealerOverStockBill.DealerCode;
                    overstockTransferOrder.TransferOutCorpName = dealerOverStockBill.DealerName;
                    overstockTransferOrder.CustomerType = incompany.Type;
                    overstockTransferOrder.ReceivingAddress = companyAddress.DetailAddress;
                    overstockTransferOrder.Status = (int)DCSOverstockPartsTransferOrderStatus.新建;
                    overstockTransferOrder.CreatorId = user.Id;
                    overstockTransferOrder.CreatorName = user.Name;
                    overstockTransferOrder.CreateTime = DateTime.Now;

                    var bills = dealerOverStockBills.Where(r => r.DealerId == dealerId).ToList();
                    foreach (var detail in bills) {
                        overstockTransferOrder.OverstockTransferOrderDetails.Add(new OverstockTransferOrderDetail {
                            SparePartId = detail.SparePartId,
                            SparePartCode = detail.SparePartCode,
                            SparePartName = detail.SparePartName,
                            TransferQty = detail.Quantity,
                            RetailPrice = detail.RetailGuidePrice,
                            DiscountRate = detail.DiscountRate,
                            DiscountedPrice = detail.RetailGuidePrice * (decimal)(detail.DiscountRate??1d)
                        });
                    }
                    overstockTransferOrder.TotalAmount = overstockTransferOrder.OverstockTransferOrderDetails.Sum(r => r.DiscountedPrice * r.TransferQty);
                    InsertToDatabase(overstockTransferOrder);
                }
            }
            ObjectContext.SaveChanges();
        }
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:11:45
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 上传积压件明细附件(OverstockPartsPlatFormBill overstockPartsPlatFormBill) {
            new OverstockPartsPlatFormBillAch(this).上传积压件明细附件(overstockPartsPlatFormBill);
        }
        [Update(UsingCustomMethod = true)]
        public void EditOverstockPartsPlatFormBill(OverstockPartsPlatFormBill overstockPartsPlatFormBill) {
            new OverstockPartsPlatFormBillAch(this).EditOverstockPartsPlatFormBill(overstockPartsPlatFormBill);
        }
        [Invoke(HasSideEffects = true)]
        public void Buy(VirtualOverstockPartsPlatFormBill[] virtualOverstockPartsPlatFormBills) {
            new OverstockPartsPlatFormBillAch(this).Buy(virtualOverstockPartsPlatFormBills);
        }
        [Invoke(HasSideEffects = true)]
        public void Delete(VirtualOverstockPartsPlatFormBill[] virtualOverstockPartsPlatFormBills) { 
            new OverstockPartsPlatFormBillAch(this).Delete(virtualOverstockPartsPlatFormBills);
        }
    }
}