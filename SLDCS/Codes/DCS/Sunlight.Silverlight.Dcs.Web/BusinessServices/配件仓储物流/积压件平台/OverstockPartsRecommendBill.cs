﻿
using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class OverstockPartsRecommendBillAch : DcsSerivceAchieveBase {
        public OverstockPartsRecommendBillAch(DcsDomainService domainService)
            : base(domainService) {
        }

        public void 生成积压件平台(OverstockPartsRecommendBill[] overstockPartsRecommendBills) {
            var userInfo = Utils.GetCurrentUserInfo();

            var centerOverStockBills = overstockPartsRecommendBills.Where(r => r.CenterId.HasValue && r.WarehouseId.HasValue).ToList();
            var dealerOverStockBills = overstockPartsRecommendBills.Where(r => r.DealerId.HasValue && r.WarehouseId == null).ToList();
            if (centerOverStockBills.GroupBy(r => new {
                CenterId = r.CenterId.Value,
                WarehouseId = r.WarehouseId.Value,
                r.SparePartId
            }).Where(r => r.Count() > 1).Any(x => x.Any())) {
                throw new ValidationException(ErrorStrings.OverstockPartsRecommendBill_Detail_Same);
            }
            if (dealerOverStockBills.GroupBy(r => new {
                DealerId = r.DealerId.Value,
                r.SparePartId
            }).Where(r => r.Count() > 1).Any(x => x.Any())) {
                throw new ValidationException(ErrorStrings.OverstockPartsRecommendBill_Detail_Same);
            }

            //积压件明细只能提交一次
            var partIds = overstockPartsRecommendBills.Select(r => r.SparePartId).Distinct().ToList();
            var dboverstockPartsPlatFormBills = ObjectContext.OverstockPartsPlatFormBills.Where(r => ((r.CenterId == userInfo.EnterpriseId && r.DealerId == null) || (r.DealerId == userInfo.EnterpriseId)) && partIds.Contains(r.SparePartId)).ToList();
            foreach (var part in overstockPartsRecommendBills) {
                var dboverstockPartsPlatFormBill = dboverstockPartsPlatFormBills.FirstOrDefault(r => r.SparePartId == part.SparePartId && r.WarehouseId == part.WarehouseId);
                if(dboverstockPartsPlatFormBill != null)
                    throw new ValidationException(string.Format(ErrorStrings.OverstockPartsRecommendBill_Detail_IsExist, part.SparePartCode));
            }

            var overstockPartsPlatFormBills = overstockPartsRecommendBills.Select(o => new OverstockPartsPlatFormBill{
                CenterId = o.CenterId,
                CenterName = o.CenterName,
                DealerId = o.DealerId,
                DealerCode = o.DealerCode,
                DealerName = o.DealerName,
                SparePartId = o.SparePartId,
                SparePartCode = o.SparePartCode,
                SparePartName = o.SparePartName,
                WarehouseId = o.WarehouseId,
                WarehouseCode = o.WarehouseCode,
                WarehouseName = o.WarehouseName,
                DiscountRate = o.DiscountRate,
                DiscountedPrice = decimal.Round(decimal.Multiply((decimal)o.DiscountRate.Value, o.Price),2),
                Status = (int)DcsOverstockPartsPlatFormBillStatus.生效,
                CreatorId = userInfo.Id,
                CreatorName = userInfo.Name,
                CreateTime = DateTime.Now
            });
            foreach(var overstockPartsPlatFormBill in overstockPartsPlatFormBills) {
                ObjectContext.OverstockPartsPlatFormBills.AddObject(overstockPartsPlatFormBill);
                InsertToDatabase(overstockPartsPlatFormBill);
            }
            foreach (var item in overstockPartsRecommendBills) {
                DeleteFromDatabase(item);
            }
            ObjectContext.SaveChanges();
        }
    }

    partial class DcsDomainService {
        [Invoke]
        public void 生成积压件平台(OverstockPartsRecommendBill[] overstockPartsRecommendBills) {
            new OverstockPartsRecommendBillAch(this).生成积压件平台(overstockPartsRecommendBills);
        }
    }
}
