﻿using Sunlight.Silverlight.Web;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class OverstockTransferOrderAch : DcsSerivceAchieveBase {
        public OverstockTransferOrderAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 作废积压件调拨单(OverstockTransferOrder overstockTransferOrder) {
            var dbOverstockTransferOrder = ObjectContext.OverstockTransferOrders.Where(r => r.Id == overstockTransferOrder.Id && (r.Status == (int)DCSOverstockPartsTransferOrderStatus.新建 ||r.Status == (int)DCSOverstockPartsTransferOrderStatus.提交 )).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbOverstockTransferOrder == null)
                throw new ValidationException(ErrorStrings.OverstockPartsRecommendBill_Abandon);
            var user = Utils.GetCurrentUserInfo();
            if(dbOverstockTransferOrder.TransferInCorpId != user.EnterpriseId)
                throw new ValidationException(ErrorStrings.OverstockPartsRecommendBill_AbandonCompany);

            overstockTransferOrder.Status = (int)DcsDealerPartsTransferOrderStatus.作废;
            overstockTransferOrder.ModifierId = user.Id;
            overstockTransferOrder.ModifierName = user.Name;
            overstockTransferOrder.ModifyTime = DateTime.Now;

            UpdateToDatabase(overstockTransferOrder);
        }
        public void 修改积压件调拨单(OverstockTransferOrder overstockTransferOrder,OverstockTransferOrderDetail[] details) {
            var dbOverstockTransferOrder = ObjectContext.OverstockTransferOrders.Where(r => r.Id == overstockTransferOrder.Id && r.Status == (int)DCSOverstockPartsTransferOrderStatus.新建).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbOverstockTransferOrder == null)
                throw new ValidationException(ErrorStrings.OverstockPartsRecommendBill_EditNew);
            var user = Utils.GetCurrentUserInfo();
            var outCompany = ObjectContext.Companies.SingleOrDefault(r => r.Id == overstockTransferOrder.TransferOutCorpId && r.Status == (int)DcsMasterDataStatus.有效);
            if (outCompany == null)
                throw new ValidationException(ErrorStrings.OverstockPartsRecommendBill_OutCpompany_NotExist);
            if(dbOverstockTransferOrder.TransferInCorpId != user.EnterpriseId)
                throw new ValidationException(ErrorStrings.OverstockPartsRecommendBill_InCompany_Edit);
            //校验可用库存
            var partIds = details.Select(r => r.SparePartId).ToArray();
            if(details.Any(r => r.TransferQty == null || r.TransferQty.Value < 0))
                throw new ValidationException(ErrorStrings.OverstockPartsRecommendBill_TransferQtyIsNull);

            if (outCompany.Type == (int)DcsCompanyType.服务站) {
                 var dealerStock = ObjectContext.DealerPartsStocks.Where(r => r.SubDealerId == -1 && r.DealerId == outCompany.Id && partIds.Contains(r.SparePartId)).ToList();
                 foreach (var detail in details) {
                     var stock = dealerStock.FirstOrDefault(r => r.SparePartId == detail.SparePartId);
                     if (stock == null)
                         throw new ValidationException(string.Format(ErrorStrings.OverstockPartsRecommendBill_DealerStock_NotExist, detail.SparePartCode));
                     if (stock.Quantity < detail.TransferQty)
                         throw new ValidationException(string.Format(ErrorStrings.OverstockPartsRecommendBill_DealerStock_UnFull, detail.SparePartCode));
                 }
            } else { 
                var stocks = DomainService.配件调拨查询仓库库存(overstockTransferOrder.TransferOutCorpId, overstockTransferOrder.TransferOutWarehouseId, partIds).ToList();
                foreach (var detail in details) {
                     var stock = stocks.FirstOrDefault(r => r.SparePartId == detail.SparePartId);
                     if (stock == null)
                         throw new ValidationException(string.Format(ErrorStrings.OverstockPartsRecommendBill_Stock_NotExist, detail.SparePartCode));
                     if (stock.UsableQuantity < detail.TransferQty)
                         throw new ValidationException(string.Format(ErrorStrings.OverstockPartsRecommendBill_Stock_UnFull, detail.SparePartCode));
                 }
            }

            overstockTransferOrder.ModifierId = user.Id;
            overstockTransferOrder.ModifierName = user.Name;
            overstockTransferOrder.ModifyTime = DateTime.Now;

            UpdateToDatabase(overstockTransferOrder);
            foreach (var detail in details) {
                UpdateToDatabase(detail);
            }
            overstockTransferOrder.TotalAmount = details.Sum(r => (r.EnoughQty ?? r.TransferQty) * r.DiscountedPrice);
            ObjectContext.SaveChanges();
        }

        public void 提交积压件调拨单(OverstockTransferOrder[] overstockTransferOrders) {
            var ids = overstockTransferOrders.Select(r => r.Id).Distinct().ToList();
            var dbOverstockTransferOrders = ObjectContext.OverstockTransferOrders.Include("OverstockTransferOrderDetails").Where(r => ids.Contains(r.Id) && r.Status == (int)DCSOverstockPartsTransferOrderStatus.新建).SetMergeOption(MergeOption.NoTracking).ToList();

            foreach (var id in ids) {
                var dbOverstockTransferOrder = dbOverstockTransferOrders.FirstOrDefault(r => r.Id == id);
                if (dbOverstockTransferOrder == null)
                    throw new ValidationException("只能提交“新建”状态下的积压件调拨单");
                var overstockTransferOrder = overstockTransferOrders.First(r => r.Id == id);
                var user = Utils.GetCurrentUserInfo();
                var details = dbOverstockTransferOrder.OverstockTransferOrderDetails.ToList();
                var outCompany = ObjectContext.Companies.SingleOrDefault(r => r.Id == overstockTransferOrder.TransferOutCorpId && r.Status == (int)DcsMasterDataStatus.有效);
                if (outCompany == null)
                    throw new ValidationException("调出方企业不存在");
                if (dbOverstockTransferOrder.TransferInCorpId != user.EnterpriseId)
                    throw new ValidationException("只能提交调入方企业为当前登录企业的积压件调拨单");
                //校验可用库存
                var partIds = details.Select(r => r.SparePartId).ToArray();
                if (details.Any(r => r.TransferQty == null || r.TransferQty.Value < 0))
                    throw new ValidationException("调拨数量不可为空");

                if (outCompany.Type == (int)DcsCompanyType.服务站) {
                    var dealerStock = ObjectContext.DealerPartsStocks.Where(r => r.SubDealerId == -1 && r.DealerId == outCompany.Id && partIds.Contains(r.SparePartId)).ToList();
                    foreach (var detail in details) {
                        var stock = dealerStock.FirstOrDefault(r => r.SparePartId == detail.SparePartId);
                        if (stock == null)
                            throw new ValidationException(string.Format("配件{0}服务站库存不存在", detail.SparePartCode));
                        if (stock.Quantity < detail.TransferQty)
                            throw new ValidationException(string.Format("配件{0}服务站库存不足", detail.SparePartCode));
                    }
                } else {
                    var stocks = DomainService.配件调拨查询仓库库存(overstockTransferOrder.TransferOutCorpId, overstockTransferOrder.TransferOutWarehouseId, partIds).ToList();
                    foreach (var detail in details) {
                        var stock = stocks.FirstOrDefault(r => r.SparePartId == detail.SparePartId);
                        if (stock == null)
                            throw new ValidationException(string.Format("配件{0}库存不存在", detail.SparePartCode));
                        if (stock.UsableQuantity < detail.TransferQty)
                            throw new ValidationException(string.Format("配件{0}库存不足", detail.SparePartCode));
                    }
                }

                overstockTransferOrder.ModifierId = user.Id;
                overstockTransferOrder.ModifierName = user.Name;
                overstockTransferOrder.ModifyTime = DateTime.Now;
                overstockTransferOrder.Status = (int)DcsDealerPartsTransferOrderStatus.提交;
                UpdateToDatabase(overstockTransferOrder);
            }
            ObjectContext.SaveChanges();
        }

        public void 确认积压件调拨单(OverstockTransferOrder order, OverstockTransferOrderDetail[] details) {
            //满足数量暂时直接赋值调拨数量
            foreach (var detail in details) {
                detail.EnoughQty = detail.TransferQty;
            }
            var user = Utils.GetCurrentUserInfo();
            var newOrder = ObjectContext.OverstockTransferOrders.Where(r => r.Id == order.Id).SetMergeOption(MergeOption.NoTracking).First();
            if (newOrder.Status != (int)DCSOverstockPartsTransferOrderStatus.提交) {
                throw new ValidationException(ErrorStrings.OverstockPartsRecommendBill_Submit_Confirm);
            }
            //分公司信息
            var company = ObjectContext.Companies.Where(e => e.Type == (int)DcsCompanyType.分公司 && e.Status == (int)DcsBaseDataStatus.有效).First();
            //销售类型
            var category = ObjectContext.PartsSalesCategories.Where(r => r.Status == (int)DcsBaseDataStatus.有效).First();
            //调入方企业信息
            var inBoundCompany = ObjectContext.Companies.Where(r => r.Id == order.TransferInCorpId).First();
            //调出方企业信息
            var outBoundCompany = ObjectContext.Companies.Where(r => r.Id == order.TransferOutCorpId).First();
            var customerAccount = ObjectContext.CustomerAccounts.Where(r => r.CustomerCompanyId == order.TransferInCorpId && r.Status == (int)DcsMasterDataStatus.有效).FirstOrDefault();
            if (null == customerAccount) {
                throw new ValidationException(ErrorStrings.OverstockPartsRecommendBill_CompanyIsNotExist);
            }
            var pids = details.Select(r => r.SparePartId).Distinct().ToList();
            if (inBoundCompany.Type == (int)DcsCompanyType.服务站 && outBoundCompany.Type == (int)DcsCompanyType.服务站) {
                var dealerPartsStocks = ObjectContext.DealerPartsStocks.Where(r => r.DealerId == order.TransferInCorpId && r.SubDealerId == -1 && pids.Contains(r.SparePartId)).ToList();
                //如果调出单位和调入单位都是服务站：自动扣减调出单位库存，增加调入单位库存
                foreach (var detail in details) {
                    var dealerPartsStock = dealerPartsStocks.SingleOrDefault(r => r.SparePartId == detail.SparePartId);
                    if (dealerPartsStock != null) {
                        dealerPartsStock.Quantity += detail.EnoughQty.Value;
                        DomainService.UpdateDealerPartsStock(dealerPartsStock);
                    } else {
                        var dealerPartsSto = new DealerPartsStock {
                            DealerId = order.TransferInCorpId,
                            DealerCode = order.TransferInCorpCode,
                            DealerName = order.TransferInCorpName,
                            SubDealerId = -1,
                            BranchId = company.Id,
                            SalesCategoryId = category.Id,
                            SalesCategoryName = category.Name,
                            SparePartId = detail.SparePartId,
                            SparePartCode = detail.SparePartCode,
                            Quantity = detail.EnoughQty.Value
                        };
                        InsertToDatabase(dealerPartsSto);
                        new DealerPartsStockAch(this.DomainService).InsertDealerPartsStockValidate(dealerPartsSto);
                    }
                }
            } else if (inBoundCompany.Type == (int)DcsCompanyType.代理库 && outBoundCompany.Type == (int)DcsCompanyType.服务站) {
                //新增入库计划
                var partsInboundPlan = new PartsInboundPlan {
                    WarehouseId = order.TransferInWarehouseId.Value,
                    WarehouseCode = order.TransferInWarehouseCode,
                    WarehouseName = order.TransferInWarehouseName,
                    StorageCompanyId = order.TransferInCorpId,
                    StorageCompanyCode = order.TransferInCorpCode,
                    StorageCompanyName = order.TransferInCorpName,
                    StorageCompanyType = inBoundCompany.Type,
                    BranchId = company.Id,
                    BranchCode = company.Code,
                    BranchName = company.Name,
                    CounterpartCompanyId = outBoundCompany.Id,
                    CounterpartCompanyCode = outBoundCompany.Code,
                    CounterpartCompanyName = outBoundCompany.Name,
                    SourceId = order.Id,
                    SourceCode = order.Code,
                    InboundType = (int)DcsPartsInboundType.积压件调剂,
                    CustomerAccountId = customerAccount.Id,
                    OriginalRequirementBillId = order.Id,
                    OriginalRequirementBillCode = order.Code,
                    OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.积压件调剂单,
                    Status = (int)DcsPartsInboundPlanStatus.新建,
                    PartsSalesCategoryId = category.Id,
                    IfWmsInterface = false
                };
                foreach (var detail in details) {
                    //新增入库计划清单
                    partsInboundPlan.PartsInboundPlanDetails.Add(new PartsInboundPlanDetail {
                        SparePartId = detail.SparePartId,
                        SparePartCode = detail.SparePartCode,
                        SparePartName = detail.SparePartName,
                        PlannedAmount = detail.EnoughQty == null ? detail.TransferQty.Value : detail.EnoughQty.Value,
                        Price = detail.DiscountedPrice.Value
                    });
                }
                InsertToDatabase(partsInboundPlan);
                new PartsInboundPlanAch(this.DomainService).InsertPartsInboundPlanValidate(partsInboundPlan);
            }

            if (outBoundCompany.Type == (int)DcsCompanyType.服务站) {
                var outDealerPartsStocks = ObjectContext.DealerPartsStocks.Where(r => r.DealerId == outBoundCompany.Id && r.SubDealerId == -1 && pids.Contains(r.SparePartId)).ToList();
                foreach (var detail in details) {
                    var dealerPartsStock = outDealerPartsStocks.SingleOrDefault(r => r.SparePartId == detail.SparePartId);
                    if (dealerPartsStock == null) {
                        throw new ValidationException(ErrorStrings.OverstockPartsAdjustBill_Error_DealerStock);
                    }
                    var quantity = dealerPartsStock.Quantity - detail.EnoughQty;
                    if (quantity < 0)
                        throw new ValidationException(string.Format(ErrorStrings.OverstockPartsRecommendBill_DealerStock_UnFull, detail.SparePartCode));
                    dealerPartsStock.Quantity = quantity.Value;
                    DomainService.UpdateDealerPartsStock(dealerPartsStock);
                }

            } else if (outBoundCompany.Type == (int)DcsCompanyType.代理库 || outBoundCompany.Type == (int)DcsCompanyType.服务站兼代理库) {
                var partsOutboundPlan = new PartsOutboundPlan {
                    WarehouseId = order.TransferOutWarehouseId.Value,
                    WarehouseCode = order.TransferOutWarehouseCode,
                    WarehouseName = order.TransferOutWarehouseName,
                    StorageCompanyId = order.TransferOutCorpId,
                    StorageCompanyCode = order.TransferOutCorpCode,
                    StorageCompanyName = order.TransferOutCorpName,
                    StorageCompanyType = outBoundCompany.Type,
                    BranchId = company.Id,
                    BranchCode = company.Code,
                    BranchName = company.Name,
                    CounterpartCompanyId = inBoundCompany.Id,
                    CounterpartCompanyCode = inBoundCompany.Code,
                    CounterpartCompanyName = inBoundCompany.Name,
                    ReceivingCompanyId = inBoundCompany.Id,
                    ReceivingCompanyCode = inBoundCompany.Code,
                    ReceivingCompanyName = inBoundCompany.Name,
                    ReceivingWarehouseId = order.TransferInWarehouseId,
                    ReceivingWarehouseCode = order.TransferInWarehouseCode,
                    ReceivingWarehouseName = order.TransferInWarehouseName,
                    ReceivingAddress = order.ReceivingAddress,
                    OriginalRequirementBillId = order.Id,
                    OriginalRequirementBillCode = order.Code,
                    OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.积压件调剂单,
                    SourceId = order.Id,
                    SourceCode = order.Code,
                    OutboundType = (int)DcsPartsOutboundType.积压件调剂,
                    Status = (int)DcsPartsOutboundPlanStatus.新建,
                    PartsSalesCategoryId = category.Id,
                    CustomerAccountId = customerAccount.Id,
                    IfWmsInterface = false
                };
                var partIds = details.Select(r => r.SparePartId).ToArray();
                //校验代理库库存
                //获取清单中配件的可用库存
                var warehousePartsStocks = DomainService.配件调拨查询仓库库存(outBoundCompany.Id, order.TransferOutWarehouseId, partIds).ToList();
                if (warehousePartsStocks.Count <= 0) {
                    throw new ValidationException(ErrorStrings.WarehousePartsStock_Validation1);
                }
                foreach (var detail in details) {
                    //校验调拨了是否大于可用库存 
                    var checkPartsStock = warehousePartsStocks.SingleOrDefault(r => r.SparePartId == detail.SparePartId);
                    if (checkPartsStock == null) {
                        throw new ValidationException(string.Format(ErrorStrings.OverstockPartsRecommendBill_Stock_NotExist, detail.SparePartCode));
                    }
                    var qty = detail.EnoughQty == null ? detail.TransferQty : detail.EnoughQty;
                    if (checkPartsStock.UsableQuantity < qty) {
                        throw new ValidationException(string.Format(ErrorStrings.OverstockPartsRecommendBill_Stock_UnFull, detail.SparePartCode));
                    } else {
                        checkPartsStock.UsableQuantity = checkPartsStock.UsableQuantity - qty.Value;
                    }

                    partsOutboundPlan.PartsOutboundPlanDetails.Add(new PartsOutboundPlanDetail {
                        SparePartId = detail.SparePartId,
                        SparePartCode = detail.SparePartCode,
                        SparePartName = detail.SparePartName,
                        PlannedAmount = qty.Value,
                        Price = detail.DiscountedPrice.Value,
                        OriginalPrice = detail.RetailPrice
                    });
                }
                DomainService.生成配件出库计划(partsOutboundPlan);
            } else {
                throw new ValidationException(ErrorStrings.OverstockPartsRecommendBill_CompanyOut_TypeError);
            }
            order.ApproverId = user.Id;
            order.ApproverName = user.Name;
            order.ApproveTime = DateTime.Now;
            order.Status = (int)DCSOverstockPartsTransferOrderStatus.生效;
            UpdateToDatabase(order);
            foreach (var detail in details) {
                UpdateToDatabase(detail);
            }
            ObjectContext.SaveChanges();
        }
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:11:45
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 作废积压件调拨单(OverstockTransferOrder overstockTransferOrder) {
            new OverstockTransferOrderAch(this).作废积压件调拨单(overstockTransferOrder);
        }

        [Invoke]
        public void 修改积压件调拨单(OverstockTransferOrder overstockTransferOrder,OverstockTransferOrderDetail[] details) {
               new OverstockTransferOrderAch(this).修改积压件调拨单(overstockTransferOrder,details);
        }

        [Invoke]
        public void 提交积压件调拨单(OverstockTransferOrder[] overstockTransferOrders) {
               new OverstockTransferOrderAch(this).提交积压件调拨单(overstockTransferOrders);
        }

        [Invoke]
        public void 确认积压件调拨单(OverstockTransferOrder overstockTransferOrder,OverstockTransferOrderDetail[] details) {
               new OverstockTransferOrderAch(this).确认积压件调拨单(overstockTransferOrder,details);
        }
    }
}