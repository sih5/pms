﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class AgencyDifferenceBackBillAch : DcsSerivceAchieveBase {
        public void 作废中心库收货差异处理单(AgencyDifferenceBackBill agencyDifferenceBackBill) {
            var dbagencyDifferenceBackBill = ObjectContext.AgencyDifferenceBackBills.Where(r => r.Id == agencyDifferenceBackBill.Id && r.Status == (int)DCSAgencyDifferenceBackBillStatus.新增).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbagencyDifferenceBackBill == null)
                throw new ValidationException("只能作废新建的数据");
            var userInfo = Utils.GetCurrentUserInfo();
            agencyDifferenceBackBill.AbandonerId = userInfo.Id;
            agencyDifferenceBackBill.AbandonerName = userInfo.Name;
            agencyDifferenceBackBill.AbandonTime = DateTime.Now;
            agencyDifferenceBackBill.Status = (int)DCSAgencyDifferenceBackBillStatus.作废;
            this.UpdateAgencyDifferenceBackBillValidate(agencyDifferenceBackBill);
            UpdateToDatabase(agencyDifferenceBackBill);
        }
        public void 提交中心库收货差异处理单(AgencyDifferenceBackBill agencyDifferenceBackBill) {
            var dbagencyDifferenceBackBill = ObjectContext.AgencyDifferenceBackBills.Where(r => r.Id == agencyDifferenceBackBill.Id && r.Status == (int)DCSAgencyDifferenceBackBillStatus.新增).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbagencyDifferenceBackBill == null)
                throw new ValidationException("只能提交新建的数据");
            //判断可差异处理数量是否满足
            this.validDtaQty(agencyDifferenceBackBill);

            var userInfo = Utils.GetCurrentUserInfo();
            agencyDifferenceBackBill.SubmitterId = userInfo.Id;
            agencyDifferenceBackBill.SubmitterName = userInfo.Name;
            agencyDifferenceBackBill.SubmitTime = DateTime.Now;
            agencyDifferenceBackBill.Status = (int)DCSAgencyDifferenceBackBillStatus.提交;
            this.UpdateAgencyDifferenceBackBillValidate(agencyDifferenceBackBill);
            UpdateToDatabase(agencyDifferenceBackBill);
        }
        public void 判定中心库收货差异处理单(AgencyDifferenceBackBill agencyDifferenceBackBill) {
            var dbagencyDifferenceBackBill = ObjectContext.AgencyDifferenceBackBills.Where(r => r.Id == agencyDifferenceBackBill.Id && r.Status == (int)DCSAgencyDifferenceBackBillStatus.提交).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbagencyDifferenceBackBill == null)
                throw new ValidationException("只能判定提交的数据");
            var userInfo = Utils.GetCurrentUserInfo();
            agencyDifferenceBackBill.DetermineId = userInfo.Id;
            agencyDifferenceBackBill.DetermineName = userInfo.Name;
            agencyDifferenceBackBill.DetermineTime = DateTime.Now;
            agencyDifferenceBackBill.Status = (int)DCSAgencyDifferenceBackBillStatus.判定通过;
            this.UpdateAgencyDifferenceBackBillValidate(agencyDifferenceBackBill);
            UpdateToDatabase(agencyDifferenceBackBill);
        }
        public void 确认通过中心库收货差异处理单(AgencyDifferenceBackBill agencyDifferenceBackBill) {
            var dbagencyDifferenceBackBill = ObjectContext.AgencyDifferenceBackBills.Where(r => r.Id == agencyDifferenceBackBill.Id && r.Status == (int)DCSAgencyDifferenceBackBillStatus.判定通过).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbagencyDifferenceBackBill == null)
                throw new ValidationException("只能确认判定通过的数据");
            var userInfo = Utils.GetCurrentUserInfo();
            agencyDifferenceBackBill.ConfirmId = userInfo.Id;
            agencyDifferenceBackBill.ConfirmName = userInfo.Name;
            agencyDifferenceBackBill.ConfirmTime = DateTime.Now;
            agencyDifferenceBackBill.Status = (int)DCSAgencyDifferenceBackBillStatus.确认通过;
            this.UpdateAgencyDifferenceBackBillValidate(agencyDifferenceBackBill);
            UpdateToDatabase(agencyDifferenceBackBill);
        }
        public void 确认驳回中心库收货差异处理单(AgencyDifferenceBackBill agencyDifferenceBackBill) {
            var dbagencyDifferenceBackBill = ObjectContext.AgencyDifferenceBackBills.Where(r => r.Id == agencyDifferenceBackBill.Id && r.Status == (int)DCSAgencyDifferenceBackBillStatus.判定通过).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbagencyDifferenceBackBill == null)
                throw new ValidationException("只能确认判定通过的数据");
            var userInfo = Utils.GetCurrentUserInfo();
            //agencyDifferenceBackBill.ConfirmId = userInfo.Id;
            //agencyDifferenceBackBill.ConfirmName = userInfo.Name;
            //agencyDifferenceBackBill.ConfirmTime = DateTime.Now;
            agencyDifferenceBackBill.Status = (int)DCSAgencyDifferenceBackBillStatus.提交;
            this.UpdateAgencyDifferenceBackBillValidate(agencyDifferenceBackBill);
            UpdateToDatabase(agencyDifferenceBackBill);
        }
        public void 处理中心库收货差异处理单(AgencyDifferenceBackBill agencyDifferenceBackBill) {
            var dbagencyDifferenceBackBill = ObjectContext.AgencyDifferenceBackBills.Where(r => r.Id == agencyDifferenceBackBill.Id && r.Status == (int)DCSAgencyDifferenceBackBillStatus.确认通过).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbagencyDifferenceBackBill == null)
                throw new ValidationException("只能处理确认通过的数据");
            var userInfo = Utils.GetCurrentUserInfo();
            agencyDifferenceBackBill.HandlerId = userInfo.Id;
            agencyDifferenceBackBill.Handler = userInfo.Name;
            agencyDifferenceBackBill.HandlTime = DateTime.Now;
            agencyDifferenceBackBill.Status = (int)DCSAgencyDifferenceBackBillStatus.处理通过;
            this.UpdateAgencyDifferenceBackBillValidate(agencyDifferenceBackBill);
            UpdateToDatabase(agencyDifferenceBackBill);
        }
        public void 驳回中心库收货差异处理单(AgencyDifferenceBackBill agencyDifferenceBackBill) {
            var dbagencyDifferenceBackBill = ObjectContext.AgencyDifferenceBackBills.Where(r => r.Id == agencyDifferenceBackBill.Id && (r.Status == (int)DCSAgencyDifferenceBackBillStatus.处理通过 || r.Status == (int)DCSAgencyDifferenceBackBillStatus.审核通过)).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbagencyDifferenceBackBill == null)
                throw new ValidationException("当前状态不可驳回");
            var userInfo = Utils.GetCurrentUserInfo();

            agencyDifferenceBackBill.ConfirmId = null;
            agencyDifferenceBackBill.ConfirmName = null;
            agencyDifferenceBackBill.ConfirmTime = null;
            agencyDifferenceBackBill.HandlerId = null;
            agencyDifferenceBackBill.Handler = null;
            agencyDifferenceBackBill.HandlTime = null;
            agencyDifferenceBackBill.Status = (int)DCSAgencyDifferenceBackBillStatus.判定通过;
            this.UpdateAgencyDifferenceBackBillValidate(agencyDifferenceBackBill);
            UpdateToDatabase(agencyDifferenceBackBill);
        }
        public void 终止中心库收货差异处理单(AgencyDifferenceBackBill agencyDifferenceBackBill) {
            var dbagencyDifferenceBackBill = ObjectContext.AgencyDifferenceBackBills.Where(r => r.Id == agencyDifferenceBackBill.Id && (r.Status != (int)DCSAgencyDifferenceBackBillStatus.新增 && r.Status != (int)DCSAgencyDifferenceBackBillStatus.审批通过 && r.Status != (int)DCSAgencyDifferenceBackBillStatus.作废 && r.Status != (int)DCSAgencyDifferenceBackBillStatus.终止)).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbagencyDifferenceBackBill == null)
                throw new ValidationException("当前状态不可终止");
            var userInfo = Utils.GetCurrentUserInfo();
            agencyDifferenceBackBill.CloserId = userInfo.Id;
            agencyDifferenceBackBill.CloserName = userInfo.Name;
            agencyDifferenceBackBill.CloseTime = DateTime.Now;
            agencyDifferenceBackBill.Status = (int)DCSAgencyDifferenceBackBillStatus.终止;
            this.UpdateAgencyDifferenceBackBillValidate(agencyDifferenceBackBill);
            UpdateToDatabase(agencyDifferenceBackBill);
        }
        public void 审核中心库收货差异处理单(AgencyDifferenceBackBill agencyDifferenceBackBill) {
            var dbagencyDifferenceBackBill = ObjectContext.AgencyDifferenceBackBills.Where(r => r.Id == agencyDifferenceBackBill.Id && r.Status == (int)DCSAgencyDifferenceBackBillStatus.处理通过).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbagencyDifferenceBackBill == null)
                throw new ValidationException("当前状态不可审核");
            var userInfo = Utils.GetCurrentUserInfo();
            agencyDifferenceBackBill.CheckerId = userInfo.Id;
            agencyDifferenceBackBill.CheckerName = userInfo.Name;
            agencyDifferenceBackBill.CheckTime = DateTime.Now;
            agencyDifferenceBackBill.Status = (int)DCSAgencyDifferenceBackBillStatus.审核通过;
            this.UpdateAgencyDifferenceBackBillValidate(agencyDifferenceBackBill);
            UpdateToDatabase(agencyDifferenceBackBill);
        }
        public void 审批中心库收货差异处理单(AgencyDifferenceBackBill agencyDifferenceBackBill) {
            var dbagencyDifferenceBackBill = ObjectContext.AgencyDifferenceBackBills.Where(r => r.Id == agencyDifferenceBackBill.Id && r.Status == (int)DCSAgencyDifferenceBackBillStatus.审核通过).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbagencyDifferenceBackBill == null)
                throw new ValidationException("当前状态不可审批");
            var userInfo = Utils.GetCurrentUserInfo();
            agencyDifferenceBackBill.ApproverId = userInfo.Id;
            agencyDifferenceBackBill.ApproverName = userInfo.Name;
            agencyDifferenceBackBill.ApproveTime = DateTime.Now;
            agencyDifferenceBackBill.Status = (int)DCSAgencyDifferenceBackBillStatus.审批通过;
            this.UpdateAgencyDifferenceBackBillValidate(agencyDifferenceBackBill);
            UpdateToDatabase(agencyDifferenceBackBill);
            //如责任类型=物流供应商运输问题时，不生成任何数据，当责任类型≠物流供应商运输问题时
            if(!agencyDifferenceBackBill.ResType.Equals("物流供应商运输问题")) {
                //判断可差异处理数量是否满足
                this.validDtaQty(agencyDifferenceBackBill);
                //判断中心库库存是否足够
                int[] spids = { agencyDifferenceBackBill.SparePartId };
                var warehousePartsStocks = DomainService.查询仓库库存(agencyDifferenceBackBill.StorageCompanyId, agencyDifferenceBackBill.InWarehouseId, spids).ToList();
                if(warehousePartsStocks.Count <= 0) {
                    throw new ValidationException(ErrorStrings.WarehousePartsStock_Validation1);
                }
                var checkPartsStock = warehousePartsStocks.SingleOrDefault(r => r.SparePartId == agencyDifferenceBackBill.SparePartId);
                if(checkPartsStock == null) {
                    throw new ValidationException(string.Format("配件编号为{0}的库存不存在", agencyDifferenceBackBill.SparePartCode));
                }
                if(checkPartsStock.UsableQuantity < agencyDifferenceBackBill.DifferQty.Value) {
                    throw new ValidationException(string.Format("配件编号为{0}的可用库存不足够", agencyDifferenceBackBill.SparePartCode));
                }
                //销售组织 
                var company = ObjectContext.Companies.Where(t => t.Type == (int)DcsCompanyType.分公司 && t.Status == (int)DcsMasterDataStatus.有效).First();

                var salesUnit = ObjectContext.SalesUnits.Where(t => t.OwnerCompanyId == company.Id && t.Status == (int)DcsMasterDataStatus.有效 && t.BranchId == company.Id).FirstOrDefault();
                if(salesUnit == null) {
                    throw new ValidationException("未找到" + company.Code + "的销售组织");
                }
                //客户账户 
                var customerAccount = ObjectContext.CustomerAccounts.Where(t => t.CustomerCompanyId == agencyDifferenceBackBill.StorageCompanyId && t.Status == (int)DcsMasterDataStatus.有效 && ObjectContext.AccountGroups.Any(p => p.Id == t.AccountGroupId && p.SalesCompanyId == company.Id)).FirstOrDefault();
                if(customerAccount == null) {
                    throw new ValidationException("未找到提报单位的账户信息");
                }
                //收货仓库 
                var orderWarehouse = ObjectContext.Warehouses.Where(t => t.Name == "双桥总库" && t.Status == (int)DcsBaseDataStatus.有效 && t.Type == (int)DcsWarehouseType.总库).FirstOrDefault();
                if(orderWarehouse == null) {
                    throw new ValidationException("未找到订货仓库'双桥总库'");
                }
                //生成销售退货单
                var partsSalesReturnBill = new PartsSalesReturnBill {
                    Code = CodeGenerator.Generate("PartsSalesReturnBill", agencyDifferenceBackBill.StorageCompanyCode),
                    ReturnCompanyId = agencyDifferenceBackBill.StorageCompanyId,
                    ReturnCompanyCode = agencyDifferenceBackBill.StorageCompanyCode,
                    ReturnCompanyName = agencyDifferenceBackBill.StorageCompanyName,
                    InvoiceReceiveCompanyId = agencyDifferenceBackBill.StorageCompanyId,
                    InvoiceReceiveCompanyCode = agencyDifferenceBackBill.StorageCompanyCode,
                    InvoiceReceiveCompanyName = agencyDifferenceBackBill.StorageCompanyName,
                    SubmitCompanyId = agencyDifferenceBackBill.StorageCompanyId,
                    SubmitCompanyCode = agencyDifferenceBackBill.StorageCompanyCode,
                    SubmitCompanyName = agencyDifferenceBackBill.StorageCompanyName,
                    SalesUnitId = salesUnit.Id,
                    SalesUnitName = salesUnit.Name,
                    SalesUnitOwnerCompanyId = company.Id,
                    SalesUnitOwnerCompanyCode = company.Code,
                    SalesUnitOwnerCompanyName = company.Name,
                    CustomerAccountId = customerAccount.Id,
                    BranchId = company.Id,
                    BranchCode = company.Code,
                    BranchName = company.Name,
                    ReturnWarehouseId = agencyDifferenceBackBill.InWarehouseId,
                    ReturnWarehouseCode = agencyDifferenceBackBill.InWarehouseCode,
                    ReturnWarehouseName = agencyDifferenceBackBill.InWarehouseName,
                    WarehouseId = orderWarehouse.Id,
                    WarehouseCode = orderWarehouse.Code,
                    WarehouseName = orderWarehouse.Name,
                    Status = (int)DcsPartsSalesReturnBillStatus.审批通过,
                    ReturnType = (int)DcsPartsSalesReturn_ReturnType.特殊退货,
                    ReturnReason = agencyDifferenceBackBill.ResType,
                    IsBarter = false,
                    DiscountRate = 1,
                    CreateTime = DateTime.Now,
                    CreatorId = userInfo.Id,
                    CreatorName = userInfo.Name,
                };
                var partsSalesReturnBillDetail = new PartsSalesReturnBillDetail {
                    SparePartId = agencyDifferenceBackBill.SparePartId,
                    SparePartCode = agencyDifferenceBackBill.SparePartCode,
                    SparePartName = agencyDifferenceBackBill.SparePartName,
                    ReturnedQuantity = agencyDifferenceBackBill.DifferQty.Value,
                    ApproveQuantity = agencyDifferenceBackBill.DifferQty,
                    OriginalOrderPrice = agencyDifferenceBackBill.OrderPrice,
                    ReturnPrice = agencyDifferenceBackBill.OrderPrice.Value,
                    DiscountRate = 1,
                    PartsSalesOrderId = agencyDifferenceBackBill.PartsSalesOrderId,
                    PartsSalesOrderCode = agencyDifferenceBackBill.PartsSalesOrderCode,
                    MeasureUnit = agencyDifferenceBackBill.MeasureUnit
                };
                partsSalesReturnBill.TotalAmount = partsSalesReturnBillDetail.ReturnedQuantity * partsSalesReturnBillDetail.ReturnPrice;
                partsSalesReturnBill.PartsSalesReturnBillDetails.Add(partsSalesReturnBillDetail);
                InsertToDatabase(partsSalesReturnBill);
                ObjectContext.SaveChanges();
                var dbSalesUnit = ObjectContext.SalesUnits.SingleOrDefault(r => r.Id == partsSalesReturnBill.SalesUnitId && r.Status != (int)DcsMasterDataStatus.作废);
                if(dbSalesUnit == null)
                    throw new ValidationException(ErrorStrings.PartsSalesReturnBill_Validation11);
                //生成出库计划
                var partsOutboundPlan = new PartsOutboundPlan {
                    WarehouseId = partsSalesReturnBill.ReturnWarehouseId.Value,
                    WarehouseCode = partsSalesReturnBill.ReturnWarehouseCode,
                    WarehouseName = partsSalesReturnBill.ReturnWarehouseName,
                    StorageCompanyId = partsSalesReturnBill.ReturnCompanyId,
                    StorageCompanyCode = partsSalesReturnBill.ReturnCompanyCode,
                    StorageCompanyName = partsSalesReturnBill.ReturnCompanyName,
                    CustomerAccountId = partsSalesReturnBill.CustomerAccountId,
                    StorageCompanyType = (int)DcsCompanyType.代理库,
                    BranchId = partsSalesReturnBill.BranchId,
                    BranchCode = partsSalesReturnBill.BranchCode,
                    BranchName = partsSalesReturnBill.BranchName,
                    CounterpartCompanyId = partsSalesReturnBill.SalesUnitOwnerCompanyId,
                    CounterpartCompanyCode = partsSalesReturnBill.SalesUnitOwnerCompanyCode,
                    CounterpartCompanyName = partsSalesReturnBill.SalesUnitOwnerCompanyName,
                    ReceivingCompanyId = partsSalesReturnBill.SalesUnitOwnerCompanyId,
                    ReceivingCompanyCode = partsSalesReturnBill.SalesUnitOwnerCompanyCode,
                    ReceivingCompanyName = partsSalesReturnBill.SalesUnitOwnerCompanyName,
                    ReceivingWarehouseId = partsSalesReturnBill.WarehouseId,
                    ReceivingWarehouseCode = partsSalesReturnBill.WarehouseCode,
                    ReceivingWarehouseName = partsSalesReturnBill.WarehouseName,
                    OriginalRequirementBillId = partsSalesReturnBill.Id,
                    OriginalRequirementBillCode = partsSalesReturnBill.Code,
                    OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.配件销售退货单,
                    SourceId = partsSalesReturnBill.Id,
                    SourceCode = partsSalesReturnBill.Code,
                    OutboundType = (int)DcsPartsOutboundType.采购退货,
                    Status = (int)DcsPartsOutboundPlanStatus.新建,
                    PartsSalesCategoryId = dbSalesUnit.PartsSalesCategoryId
                };
                partsOutboundPlan.PartsOutboundPlanDetails.Add(new PartsOutboundPlanDetail {
                    SparePartId = partsSalesReturnBillDetail.SparePartId,
                    SparePartCode = partsSalesReturnBillDetail.SparePartCode,
                    SparePartName = partsSalesReturnBillDetail.SparePartName,
                    PlannedAmount = partsSalesReturnBillDetail.ApproveQuantity.Value,
                    Price = partsSalesReturnBillDetail.ReturnPrice,
                    OriginalPrice = partsSalesReturnBillDetail.OriginalPrice
                });
                DomainService.生成配件出库计划(partsOutboundPlan);
            }
        }
        public IEnumerable<AgencyDifferenceBackBillForSparepart> 查询可差异退货配件(string partsInboundPlanCode, int? warehouseId, string partsShippingCode, string partsSalesOrderCode,
            string sparePartCode, string sparePartName,DateTime? bCreateTime, DateTime? eCreateTime ) {
            var userInfo = Utils.GetCurrentUserInfo();
            string SQL = @"select pl.code                        as PartsInboundPlanCode,
                           pl.id                          as PartsInboundPlanId,
                           pl.warehouseid                 as InWarehouseId,
                           pl.warehousecode               as InWarehouseCode,
                           pl.warehousename               as InWarehouseName,
                           pl.sourceid                    as PartsShippingId,
                           pl.sourcecode                  as PartsShippingCode,
                           pl.originalrequirementbillcode as PartsSalesOrderCode,
                           pl.originalrequirementbillid   as PartsSalesOrderId,
                           pd.sparepartid,
                           pd.sparepartcode,
                           pd.sparepartname,
                           pd.inspectedquantity as InboundQty,
                           pt.orderprice,
                           pt.measureunit,
                           pl.storagecompanyid,
                           pl.storagecompanycode,
                           pl.storagecompanyname,
                           pd.inspectedquantity-nvl(tt.differqty,0) as CanDIffer,
                           Rownum ,
						   pl.CreateTime
                      from partsinboundplan pl
                      join partsinboundplandetail pd
                        on pl.id = pd.partsinboundplanid
                      join partssalesorder ps
                        on pl.originalrequirementbillid = ps.id
                      join partssalesorderdetail pt
                        on ps.id = pt.partssalesorderid
                       and pd.sparepartid = pt.sparepartid
                      left join (select ag.partsinboundplanid,
                                        ag.sparepartid,
                                        sum(decode(ag.status, 8, 0, 99, 0, ag.differqty)) as differqty
                                   from AgencyDifferenceBackBill ag
                                     
                                  group by ag.partsinboundplanid, ag.sparepartid) tt
                        on pl.id = tt.partsinboundplanid
                       and pd.sparepartid = tt.sparepartid
                     where pl.storagecompanytype = 3
                       and pl.inboundtype = 1
                       and pd.inspectedquantity > 0 
                       and pd.inspectedquantity-nvl(tt.differqty,0)>0 
                        and pl.storagecompanyid=" + userInfo.EnterpriseId;
            if(!string.IsNullOrEmpty(partsInboundPlanCode)) {
                SQL += " and pl.code like '%" + partsInboundPlanCode + "%'";
            }
            if(warehouseId.HasValue){
                SQL += " and pl.warehouseId=" + warehouseId.Value;
            }
            if(!string.IsNullOrEmpty(partsShippingCode)) {
                SQL += " and pl.sourcecode like '%" + partsShippingCode + "%'";
            }
            if(!string.IsNullOrEmpty(partsSalesOrderCode)) {
                SQL += " and pl.originalrequirementbillcode like '%" + partsSalesOrderCode + "%'";
            }
            if(!string.IsNullOrEmpty(sparePartCode)) {
                SQL += " and pd.sparepartcode like '%" + sparePartCode + "%'";
            }
            if(!string.IsNullOrEmpty(sparePartName)) {
                SQL += " and pd.sparepartName like '%" + sparePartName + "%'";
            }
            if(bCreateTime.HasValue) {
                SQL = SQL + " and pl.CreateTime>= to_date('" + bCreateTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }
            if(eCreateTime.HasValue) {
                SQL = SQL + " and pl.CreateTime<= to_date('" + eCreateTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }
            var search = ObjectContext.ExecuteStoreQuery<AgencyDifferenceBackBillForSparepart>(SQL).ToList();
            return search;
        }
        private void validDtaQty(AgencyDifferenceBackBill agencyDifferenceBackBill) {
            int[] spids = { agencyDifferenceBackBill.SparePartId };
            int?[] partsSalesOrderIds = { agencyDifferenceBackBill.PartsSalesOrderId.Value };
            var canReturnDetail = new PartsOutboundBillDetailAch(this.DomainService).退货节点选取配件销售单校验(partsSalesOrderIds, spids);
            if(canReturnDetail == null || canReturnDetail.Count() == 0) {
                throw new ValidationException(string.Format("配件已做销售退货"));
            }
            var outboundAmount = canReturnDetail.Where(r => r.SparePartId == agencyDifferenceBackBill.SparePartId && r.PartsSalesOrderId == agencyDifferenceBackBill.PartsSalesOrderId).FirstOrDefault();
            if(outboundAmount == null || outboundAmount.ReturnedQuantity < agencyDifferenceBackBill.DifferQty) {
                throw new ValidationException(string.Format("实收量大于可差异退货量，有销售退货单"));
            }
            var canDifferS = this.校验查询可差异退货配件(agencyDifferenceBackBill.PartsInboundPlanCode, agencyDifferenceBackBill.InWarehouseId, agencyDifferenceBackBill.PartsShippingCode, agencyDifferenceBackBill.PartsSalesOrderCode, agencyDifferenceBackBill.SparePartCode,  agencyDifferenceBackBill.Id).FirstOrDefault();
            if(canDifferS == null || canDifferS.CanDIffer   < agencyDifferenceBackBill.DifferQty) {
                throw new ValidationException(string.Format("实收量大于可差异退货量"));
             }
        }
        private IEnumerable<AgencyDifferenceBackBillForSparepart> 校验查询可差异退货配件( string partsInboundPlanCode, int? warehouseId, string partsShippingCode, string partsSalesOrderCode,  string sparePartCode,int differId) {
            var userInfo = Utils.GetCurrentUserInfo();
            string SQL = @"select pl.code                        as PartsInboundPlanCode,
                           pl.id                          as PartsInboundPlanId,
                           pl.warehouseid                 as InWarehouseId,
                           pl.warehousecode               as InWarehouseCode,
                           pl.warehousename               as InWarehouseName,
                           pl.sourceid                    as PartsShippingId,
                           pl.sourcecode                  as PartsShippingCode,
                           pl.originalrequirementbillcode as PartsSalesOrderCode,
                           pl.originalrequirementbillid   as PartsSalesOrderId,
                           pd.sparepartid,
                           pd.sparepartcode,
                           pd.sparepartname,
                           pd.inspectedquantity as InboundQty,
                           pt.orderprice,
                           pt.measureunit,
                           pl.storagecompanyid,
                           pl.storagecompanycode,
                           pl.storagecompanyname,
                           pd.inspectedquantity-nvl(tt.differqty,0) as CanDIffer,
                           Rownum ,
						   pl.CreateTime
                      from partsinboundplan pl
                      join partsinboundplandetail pd
                        on pl.id = pd.partsinboundplanid
                      join partssalesorder ps
                        on pl.originalrequirementbillid = ps.id
                      join partssalesorderdetail pt
                        on ps.id = pt.partssalesorderid
                       and pd.sparepartid = pt.sparepartid
                      left join (select ag.partsinboundplanid,
                                        ag.sparepartid,
                                        sum(decode(ag.status, 8, 0, 99, 0, ag.differqty)) as differqty
                                   from AgencyDifferenceBackBill ag
                                 where  ag.id !=
                                     "+differId+@"
                                  group by ag.partsinboundplanid, ag.sparepartid) tt
                        on pl.id = tt.partsinboundplanid
                       and pd.sparepartid = tt.sparepartid
                     where pl.storagecompanytype = 3
                       and pl.inboundtype = 1
                       and pd.inspectedquantity > 0                        
                         " ;
            if(!string.IsNullOrEmpty(partsInboundPlanCode)) {
                SQL += " and pl.code like '%" + partsInboundPlanCode + "%'";
            }
            if(warehouseId.HasValue) {
                SQL += " and pl.warehouseId=" + warehouseId.Value;
            }
            if(!string.IsNullOrEmpty(partsShippingCode)) {
                SQL += " and pl.sourcecode like '%" + partsShippingCode + "%'";
            }
            if(!string.IsNullOrEmpty(partsSalesOrderCode)) {
                SQL += " and pl.originalrequirementbillcode like '%" + partsSalesOrderCode + "%'";
            }
            if(!string.IsNullOrEmpty(sparePartCode)) {
                SQL += " and pd.sparepartcode like '%" + sparePartCode + "%'";
            }
            var search = ObjectContext.ExecuteStoreQuery<AgencyDifferenceBackBillForSparepart>(SQL).ToList();
            return search;
        }
        public void 修改中心库收货差异处理单(AgencyDifferenceBackBill agencyDifferenceBackBill) {
            var dbagencyDifferenceBackBill = ObjectContext.AgencyDifferenceBackBills.Where(r => r.Id == agencyDifferenceBackBill.Id && r.Status == (int)DCSAgencyDifferenceBackBillStatus.新增).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbagencyDifferenceBackBill == null)
                throw new ValidationException("只能修改新建的数据");
            //判断可差异处理数量是否满足
            this.validDtaQty(agencyDifferenceBackBill);
            this.UpdateAgencyDifferenceBackBillValidate(agencyDifferenceBackBill);
            UpdateToDatabase(agencyDifferenceBackBill);
            ObjectContext.SaveChanges();
        }
        public void 新建中心库收货差异处理单(AgencyDifferenceBackBill agencyDifferenceBackBill) {
            InsertToDatabase(agencyDifferenceBackBill);
            //判断可差异处理数量是否满足
            this.validDtaQty(agencyDifferenceBackBill);
            this.InsertAgencyDifferenceBackBillValidate(agencyDifferenceBackBill);
        }
       
    }
    partial class DcsDomainService {
        // 已经使用工具进行处理 2017/4/29 12:12:32
        //原分布类的函数全部转移到Ach    
        [Update(UsingCustomMethod = true)]
        public void 修改中心库收货差异处理单(AgencyDifferenceBackBill agencyDifferenceBackBill) {
            new AgencyDifferenceBackBillAch(this).修改中心库收货差异处理单(agencyDifferenceBackBill);
        }
        [Update(UsingCustomMethod = true)]
        public void 新建中心库收货差异处理单(AgencyDifferenceBackBill agencyDifferenceBackBill) {
            new AgencyDifferenceBackBillAch(this).新建中心库收货差异处理单(agencyDifferenceBackBill);
        }
        [Update(UsingCustomMethod = true)]
        public void 作废中心库收货差异处理单(AgencyDifferenceBackBill agencyDifferenceBackBill) {
            new AgencyDifferenceBackBillAch(this).作废中心库收货差异处理单(agencyDifferenceBackBill);
        }
        [Update(UsingCustomMethod = true)]
        public void 提交中心库收货差异处理单(AgencyDifferenceBackBill agencyDifferenceBackBill) {
            new AgencyDifferenceBackBillAch(this).提交中心库收货差异处理单(agencyDifferenceBackBill);
        }
        [Update(UsingCustomMethod = true)]
        public void 判定中心库收货差异处理单(AgencyDifferenceBackBill agencyDifferenceBackBill) {
            new AgencyDifferenceBackBillAch(this).判定中心库收货差异处理单(agencyDifferenceBackBill);
        }
        [Update(UsingCustomMethod = true)]
        public void 确认通过中心库收货差异处理单(AgencyDifferenceBackBill agencyDifferenceBackBill) {
            new AgencyDifferenceBackBillAch(this).确认通过中心库收货差异处理单(agencyDifferenceBackBill);
        }
        [Update(UsingCustomMethod = true)]
        public void 确认驳回中心库收货差异处理单(AgencyDifferenceBackBill agencyDifferenceBackBill) {
            new AgencyDifferenceBackBillAch(this).确认驳回中心库收货差异处理单(agencyDifferenceBackBill);
        }
        [Update(UsingCustomMethod = true)]
        public void 处理中心库收货差异处理单(AgencyDifferenceBackBill agencyDifferenceBackBill) {
            new AgencyDifferenceBackBillAch(this).处理中心库收货差异处理单(agencyDifferenceBackBill);
        }
        [Update(UsingCustomMethod = true)]
        public void 驳回中心库收货差异处理单(AgencyDifferenceBackBill agencyDifferenceBackBill) {
            new AgencyDifferenceBackBillAch(this).驳回中心库收货差异处理单(agencyDifferenceBackBill);
        }
        [Update(UsingCustomMethod = true)]
        public void 审核中心库收货差异处理单(AgencyDifferenceBackBill agencyDifferenceBackBill) {
            new AgencyDifferenceBackBillAch(this).审核中心库收货差异处理单(agencyDifferenceBackBill);
        }
        [Update(UsingCustomMethod = true)]
        public void 审批中心库收货差异处理单(AgencyDifferenceBackBill agencyDifferenceBackBill) {
            new AgencyDifferenceBackBillAch(this).审批中心库收货差异处理单(agencyDifferenceBackBill);
        }
        [Update(UsingCustomMethod = true)]
        public void 终止中心库收货差异处理单(AgencyDifferenceBackBill agencyDifferenceBackBill) {
            new AgencyDifferenceBackBillAch(this).终止中心库收货差异处理单(agencyDifferenceBackBill);
        }
        public IEnumerable<AgencyDifferenceBackBillForSparepart> 查询可差异退货配件(string partsInboundPlanCode, int? warehouseId, string partsShippingCode, string partsSalesOrderCode,
          string sparePartCode, string sparePartName, DateTime? bCreateTime, DateTime? eCreateTime) {
              return new AgencyDifferenceBackBillAch(this).查询可差异退货配件(partsInboundPlanCode, warehouseId, partsShippingCode, partsSalesOrderCode, sparePartCode, sparePartName, bCreateTime, eCreateTime);
        }
    }
}
