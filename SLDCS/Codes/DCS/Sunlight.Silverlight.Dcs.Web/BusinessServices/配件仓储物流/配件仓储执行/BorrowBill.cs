﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class BorrowBillsAch : DcsSerivceAchieveBase {
        public BorrowBillsAch(DcsDomainService domainService)
            : base(domainService) {
        }

        public void 作废配件借用单(BorrowBill borrowBill) {
            var dbBorrowBill = this.ObjectContext.BorrowBills.Where(r => r.Id == borrowBill.Id && r.Status == (int)DcsBorrowBillStatus.新增).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbBorrowBill == null)
                throw new ValidationException(ErrorStrings.BorrowBill_Validation1);
            UpdateToDatabase(borrowBill);
            borrowBill.Status = (int)DcsBorrowBillStatus.作废;
            new BorrowBillAch(this.DomainService).UpdateBorrowBillValidate(borrowBill);
        }

        public void 提交配件借用单(BorrowBill borrowBill) {
            var dbBorrowBill = this.ObjectContext.BorrowBills.Where(r => r.Id == borrowBill.Id && r.Status == (int)DcsBorrowBillStatus.新增).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbBorrowBill == null)
                throw new ValidationException(ErrorStrings.BorrowBill_Validation2);
            var userInfo = Utils.GetCurrentUserInfo();
            UpdateToDatabase(borrowBill);
            borrowBill.Status = (int)DcsBorrowBillStatus.提交;
            borrowBill.SubmitterId = userInfo.Id;
            borrowBill.SubmitterName = userInfo.Name;
            borrowBill.SubmitTime = DateTime.Now;
            new BorrowBillAch(this.DomainService).UpdateBorrowBillValidate(borrowBill);
        }

        public void 出库配件借用单(BorrowBill borrowBill) {
            var dbBorrowBill = this.ObjectContext.BorrowBills.Where(r => r.Id == borrowBill.Id && r.Status == (int)DcsBorrowBillStatus.终审通过).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbBorrowBill == null)
                throw new ValidationException(ErrorStrings.BorrowBill_Validation3);
            UpdateToDatabase(borrowBill);
            foreach(var detail in borrowBill.BorrowBillDetails) {
                detail.OutboundAmount = detail.BorrowQty;
            }
            borrowBill.Status = (int)DcsBorrowBillStatus.已出库;
            borrowBill.OutBoundTime = DateTime.Now;
            new BorrowBillAch(this.DomainService).UpdateBorrowBillValidate(borrowBill);
        }

        public void 初审配件借用单(BorrowBill borrowBill) {
            var dbBorrowBill = this.ObjectContext.BorrowBills.Where(r => r.Id == borrowBill.Id && r.Status == (int)DcsBorrowBillStatus.提交).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbBorrowBill == null)
                throw new ValidationException(ErrorStrings.BorrowBill_Validation4);
            var userInfo = Utils.GetCurrentUserInfo();
            var partsIds = borrowBill.BorrowBillDetails.Select(r => r.SparePartId).ToArray();
            var wareHouseAreaIds = borrowBill.BorrowBillDetails.Select(r => r.WarehouseAreaId).ToArray();
            var partsStocks = new PartsStockAch(this.DomainService).GetPartsStocksWithLock(this.ObjectContext.PartsStocks.Where(r => r.WarehouseId == borrowBill.WarehouseId && partsIds.Contains(r.PartId) && wareHouseAreaIds.Contains(r.WarehouseAreaId))).ToArray();
            var partsLockedStocks = new PartsLockedStockAch(this.DomainService).GetPartsLockedStocksWithLock(this.ObjectContext.PartsLockedStocks.Where(r => r.WarehouseId == borrowBill.WarehouseId && partsIds.Contains(r.PartId))).ToArray();
            foreach(var borrowBillDetail in borrowBill.BorrowBillDetails) {
                var partsStock = partsStocks.FirstOrDefault(r => r.WarehouseAreaId == borrowBillDetail.WarehouseAreaId && r.PartId == borrowBillDetail.SparePartId);
                if(partsStock == null)
                    throw new ValidationException(string.Format(ErrorStrings.PartsOutboundBill_Validation6, borrowBillDetail.SparePartCode, borrowBillDetail.WarehouseAreaCode));
                if (borrowBillDetail.BorrowQty > (partsStock.Quantity - (partsStock.LockedQty??0)))
                    throw new ValidationException(string.Format(ErrorStrings.BorrowBill_Validation7, borrowBillDetail.SparePartCode, borrowBillDetail.WarehouseAreaCode));
                partsStock.LockedQty   = (partsStock.LockedQty ?? 0)+(borrowBillDetail.BorrowQty ?? 0);
                new PartsStockAch(this.DomainService).UpdatePartsStockValidate(partsStock);
                var partsLockedStock = partsLockedStocks.FirstOrDefault(r => r.PartId == borrowBillDetail.SparePartId);
                if(partsLockedStock == null) {
                    var newpartsLockedStock = new PartsLockedStock {
                        WarehouseId = borrowBill.WarehouseId ?? 0,
                        PartId = borrowBillDetail.SparePartId,
                        LockedQuantity = borrowBillDetail.BorrowQty ?? 0,
                        StorageCompanyId = userInfo.EnterpriseId,
                        BranchId = userInfo.EnterpriseId
                    };
                    InsertToDatabase(newpartsLockedStock);
                    new PartsLockedStockAch(this.DomainService).InsertPartsLockedStockValidate(newpartsLockedStock);
                } else {                  
                    partsLockedStock.LockedQuantity  += borrowBillDetail.BorrowQty ?? 0;
                    new PartsLockedStockAch(this.DomainService).UpdatePartsLockedStockValidate(partsLockedStock);
                }
            }

            UpdateToDatabase(borrowBill);
            borrowBill.Status = (int)DcsBorrowBillStatus.初审通过;
            borrowBill.InitialApproverId = userInfo.Id;
            borrowBill.InitialApproverName = userInfo.Name;
            borrowBill.InitialApproveTime = DateTime.Now;
            new BorrowBillAch(this.DomainService).UpdateBorrowBillValidate(borrowBill);
        }

        public void 初审驳回配件借用单(BorrowBill borrowBill) {
            var dbBorrowBill = this.ObjectContext.BorrowBills.Where(r => r.Id == borrowBill.Id && r.Status == (int)DcsBorrowBillStatus.提交).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbBorrowBill == null)
                throw new ValidationException(ErrorStrings.BorrowBill_Validation5);
            UpdateToDatabase(borrowBill);
            borrowBill.Status = (int)DcsBorrowBillStatus.新增;
            new BorrowBillAch(this.DomainService).UpdateBorrowBillValidate(borrowBill);
        }

        public void 终审配件借用单(BorrowBill borrowBill) {
            var dbBorrowBill = this.ObjectContext.BorrowBills.Where(r => r.Id == borrowBill.Id && r.Status == (int)DcsBorrowBillStatus.初审通过).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbBorrowBill == null)
                throw new ValidationException(ErrorStrings.BorrowBill_Validation6);
            var userInfo = Utils.GetCurrentUserInfo();
            UpdateToDatabase(borrowBill);
            borrowBill.Status = (int)DcsBorrowBillStatus.终审通过;
            borrowBill.ApproverId = userInfo.Id;
            borrowBill.ApproverName = userInfo.Name;
            borrowBill.ApproveTime = DateTime.Now;
            new BorrowBillAch(this.DomainService).UpdateBorrowBillValidate(borrowBill);
        }

        public void 终审驳回配件借用单(BorrowBill borrowBill) {
            var dbBorrowBill = this.ObjectContext.BorrowBills.Where(r => r.Id == borrowBill.Id && r.Status == (int)DcsBorrowBillStatus.初审通过).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbBorrowBill == null)
                throw new ValidationException(ErrorStrings.BorrowBill_Validation8);
            var userInfo = Utils.GetCurrentUserInfo();
            var partsIds = borrowBill.BorrowBillDetails.Select(r => r.SparePartId).ToArray();
            var wareHouseAreaIds = borrowBill.BorrowBillDetails.Select(r => r.WarehouseAreaId).ToArray();
            var partsStocks = new PartsStockAch(this.DomainService).GetPartsStocksWithLock(this.ObjectContext.PartsStocks.Where(r => r.WarehouseId == borrowBill.WarehouseId && partsIds.Contains(r.PartId) && wareHouseAreaIds.Contains(r.WarehouseAreaId))).ToArray();
            var partsLockedStocks = new PartsLockedStockAch(this.DomainService).GetPartsLockedStocksWithLock(this.ObjectContext.PartsLockedStocks.Where(r => r.WarehouseId == borrowBill.WarehouseId && partsIds.Contains(r.PartId))).ToArray();
            foreach(var borrowBillDetail in borrowBill.BorrowBillDetails) {
                var partsStock = partsStocks.FirstOrDefault(r => r.WarehouseAreaId == borrowBillDetail.WarehouseAreaId && r.PartId == borrowBillDetail.SparePartId);
                if(partsStock == null)
                    throw new ValidationException(string.Format(ErrorStrings.PartsOutboundBill_Validation6, borrowBillDetail.SparePartCode, borrowBillDetail.WarehouseAreaCode));
                partsStock.LockedQty -= (borrowBillDetail.BorrowQty ?? 0);
                new PartsStockAch(this.DomainService).UpdatePartsStockValidate(partsStock);
                var partsLockedStock = partsLockedStocks.FirstOrDefault(r => r.PartId == borrowBillDetail.SparePartId);
                if(partsLockedStock == null) {
                    throw new ValidationException(ErrorStrings.PartsLockedStock_NotExist);
                } else {
                    partsLockedStock.LockedQuantity -= borrowBillDetail.BorrowQty ?? 0;
                    new PartsLockedStockAch(this.DomainService).UpdatePartsLockedStockValidate(partsLockedStock);
                }
            }

            UpdateToDatabase(borrowBill);
            borrowBill.Status = (int)DcsBorrowBillStatus.新增;
            new BorrowBillAch(this.DomainService).UpdateBorrowBillValidate(borrowBill);
        }

        public void 归还保存配件借用单(BorrowBill borrowBill) {
            var dbBorrowBill = this.ObjectContext.BorrowBills.Where(r => r.Id == borrowBill.Id && (r.Status == (int)DcsBorrowBillStatus.已出库 || r.Status == (int)DcsBorrowBillStatus.部分确认)).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbBorrowBill == null)
                throw new ValidationException(ErrorStrings.BorrowBill_Validation9);
            var dbBorrowBillDetails = this.ObjectContext.BorrowBillDetails.Where(r => r.BorrowBillId == borrowBill.Id);
            foreach(var borrowBillDetail in borrowBill.BorrowBillDetails) {
                var dbborrowBillDetail = dbBorrowBillDetails.First(r => r.Id == borrowBillDetail.Id);
                var beforeQty = (dbborrowBillDetail.BorrowQty ?? 0) - (dbborrowBillDetail.ConfirmedQty ?? 0) - (dbborrowBillDetail.ConfirmingQty ?? 0);
                if(beforeQty - (borrowBillDetail.BeforeQty ?? 0) < 0) {
                    throw new ValidationException(string.Format(ErrorStrings.BorrowBill_Validation10, borrowBillDetail.SparePartCode, beforeQty));
                }
                borrowBillDetail.ConfirmingQty = (dbborrowBillDetail.ConfirmingQty ?? 0) + (borrowBillDetail.BeforeQty ?? 0);
            }
            UpdateToDatabase(borrowBill);
            var userInfo = Utils.GetCurrentUserInfo();
            borrowBill.ReturnerName = userInfo.Name;
            borrowBill.ReturnTime = DateTime.Now;
            borrowBill.Status = (int)DcsBorrowBillStatus.归还待确认;
            new BorrowBillAch(this.DomainService).UpdateBorrowBillValidate(borrowBill);
        }

        public void 归还审核配件借用单(BorrowBill borrowBill) {
            var dbBorrowBill = this.ObjectContext.BorrowBills.Where(r => r.Id == borrowBill.Id && r.Status == (int)DcsBorrowBillStatus.归还待确认).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbBorrowBill == null)
                throw new ValidationException(ErrorStrings.BorrowBill_Validation11);
            var dbBorrowBillDetails = this.ObjectContext.BorrowBillDetails.Where(r => r.BorrowBillId == borrowBill.Id);
            var partsIds = borrowBill.BorrowBillDetails.Select(r => r.SparePartId).ToArray();
            var partsLockedStocks = new PartsLockedStockAch(this.DomainService).GetPartsLockedStocksWithLock(this.ObjectContext.PartsLockedStocks.Where(r => r.WarehouseId == borrowBill.WarehouseId && partsIds.Contains(r.PartId))).ToArray();
            var wareHouseAreaIds = borrowBill.BorrowBillDetails.Select(r => r.WarehouseAreaId).ToArray();
            var partsStocks = new PartsStockAch(this.DomainService).GetPartsStocksWithLock(this.ObjectContext.PartsStocks.Where(r => r.WarehouseId == borrowBill.WarehouseId && partsIds.Contains(r.PartId) && wareHouseAreaIds.Contains(r.WarehouseAreaId))).ToArray();
            foreach(var borrowBillDetail in borrowBill.BorrowBillDetails) {
                if(!borrowBillDetail.ConfirmBeforeQty.HasValue || borrowBillDetail.ConfirmBeforeQty == 0)
                    continue;
                var dbborrowBillDetail = dbBorrowBillDetails.First(r => r.Id == borrowBillDetail.Id);
                if(dbborrowBillDetail.ConfirmingQty != borrowBillDetail.ConfirmingQty)
                    throw new ValidationException(string.Format(ErrorStrings.BorrowBill_Validation12, borrowBillDetail.SparePartCode));
                var partsStock = partsStocks.FirstOrDefault(r => r.WarehouseAreaId == borrowBillDetail.WarehouseAreaId && r.PartId == borrowBillDetail.SparePartId);
                if(partsStock == null)
                    throw new ValidationException(string.Format(ErrorStrings.PartsOutboundBill_Validation6, borrowBillDetail.SparePartCode, borrowBillDetail.WarehouseAreaCode));
                var partsLockedStock = partsLockedStocks.FirstOrDefault(r => r.PartId == borrowBillDetail.SparePartId);
                borrowBillDetail.ConfirmedQty = (dbborrowBillDetail.ConfirmedQty ?? 0) + (borrowBillDetail.ConfirmBeforeQty ?? 0);
                borrowBillDetail.ConfirmingQty = (dbborrowBillDetail.ConfirmingQty ?? 0) - (borrowBillDetail.ConfirmBeforeQty ?? 0);
                partsStock.LockedQty -= (borrowBillDetail.ConfirmBeforeQty ?? 0);
                new PartsStockAch(this.DomainService).UpdatePartsStockValidate(partsStock);
                if(partsLockedStock == null)
                    continue;
                partsLockedStock.LockedQuantity -= (borrowBillDetail.ConfirmBeforeQty ?? 0);
                new PartsLockedStockAch(this.DomainService).UpdatePartsLockedStockValidate(partsLockedStock);
            }
            UpdateToDatabase(borrowBill);
            borrowBill.Status = (int)DcsBorrowBillStatus.部分确认;
            //待确认量 和已确认量等于借用量，就不用操作归还了，只需要审核归还了。
            if(borrowBill.BorrowBillDetails.All(r => (r.ConfirmingQty ?? 0) != 0 && (r.ConfirmingQty ?? 0) + (r.ConfirmedQty ?? 0) == (r.BorrowQty ?? 0)))
                borrowBill.Status = (int)DcsBorrowBillStatus.归还待确认;
            if(borrowBill.BorrowBillDetails.All(r => r.ConfirmedQty == r.BorrowQty))
                borrowBill.Status = (int)DcsBorrowBillStatus.确认完成;
            new BorrowBillAch(this.DomainService).UpdateBorrowBillValidate(borrowBill);
        }
    }

    partial class DcsDomainService {
        [Update(UsingCustomMethod = true)]
        public void 作废配件借用单(BorrowBill borrowBill) {
            new BorrowBillsAch(this).作废配件借用单(borrowBill);
        }

        [Update(UsingCustomMethod = true)]
        public void 提交配件借用单(BorrowBill borrowBill) {
            new BorrowBillsAch(this).提交配件借用单(borrowBill);
        }

        [Update(UsingCustomMethod = true)]
        public void 出库配件借用单(BorrowBill borrowBill) {
            new BorrowBillsAch(this).出库配件借用单(borrowBill);
        }

        [Update(UsingCustomMethod = true)]
        public void 初审配件借用单(BorrowBill borrowBill) {
            new BorrowBillsAch(this).初审配件借用单(borrowBill);
        }

        [Update(UsingCustomMethod = true)]
        public void 初审驳回配件借用单(BorrowBill borrowBill) {
            new BorrowBillsAch(this).初审驳回配件借用单(borrowBill);
        }

        [Update(UsingCustomMethod = true)]
        public void 终审配件借用单(BorrowBill borrowBill) {
            new BorrowBillsAch(this).终审配件借用单(borrowBill);
        }

        [Update(UsingCustomMethod = true)]
        public void 终审驳回配件借用单(BorrowBill borrowBill) {
            new BorrowBillsAch(this).终审驳回配件借用单(borrowBill);
        }

        [Update(UsingCustomMethod = true)]
        public void 归还保存配件借用单(BorrowBill borrowBill) {
            new BorrowBillsAch(this).归还保存配件借用单(borrowBill);
        }

        [Update(UsingCustomMethod = true)]
        public void 归还审核配件借用单(BorrowBill borrowBill) {
            new BorrowBillsAch(this).归还审核配件借用单(borrowBill);
        }
    }
}
