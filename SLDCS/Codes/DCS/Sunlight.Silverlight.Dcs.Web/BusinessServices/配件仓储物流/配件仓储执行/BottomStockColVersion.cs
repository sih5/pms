﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class BottomStockColVersionAch : DcsSerivceAchieveBase {
        public BottomStockColVersionAch(DcsDomainService domainService)
            : base(domainService) {
        }
        public void 作废合集版本号(BottomStockColVersion bottomStockColVersion) {
            //作废时，根据选择的企业+合集版本号，作废该企业所有合集版本号和子集版本号数据
            var userInfo = Utils.GetCurrentUserInfo();
            var dbBorrowBill = this.ObjectContext.BottomStockColVersions.Where(r => r.Id == bottomStockColVersion.Id   && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbBorrowBill == null)
                throw new ValidationException("所选数据已作废");
            bottomStockColVersion.Status = (int)DcsBaseDataStatus.作废;
            bottomStockColVersion.EndTime = DateTime.Now;
            UpdateToDatabase(bottomStockColVersion);
            //作废相同合集版本号合集数据
            var versions = this.ObjectContext.BottomStockColVersions.Where(r => r.Id != bottomStockColVersion.Id && r.ColVersionCode == bottomStockColVersion.ColVersionCode && r.CompanyId == bottomStockColVersion.CompanyId && r.Status == (int)DcsBaseDataStatus.有效).ToArray();
            foreach(var item in versions) {
                item.Status = (int)DcsBaseDataStatus.作废;
                item.EndTime = DateTime.Now;
                UpdateToDatabase(item);
            }
            //作废相同合集版本号子集数据
            var subVersion = this.ObjectContext.BottomStockSubVersions.Where(r => r.ColVersionCode == bottomStockColVersion.ColVersionCode && r.CompanyId == bottomStockColVersion.CompanyId && r.Status == (int)DcsBaseDataStatus.有效).ToArray();
            foreach(var item in subVersion) {
                item.Status = (int)DcsBaseDataStatus.作废;
                item.EndTime = DateTime.Now;
                UpdateToDatabase(item);
             }
            //作废相同合集版本号的强制储备单
            var orders = ObjectContext.ForceReserveBills.Where(t => t.ColVersionCode == bottomStockColVersion.ColVersionCode && t.CompanyId == bottomStockColVersion.CompanyId && t.Status == (int)DCSForceReserveBillStatus.生效).ToArray();
            foreach(var item in orders) {
                item.Status = (int)DCSForceReserveBillStatus.作废;
                item.AbandonTime = DateTime.Now;
                item.AbandonerId = userInfo.Id;
                item.AbandonerName = userInfo.Name;
                UpdateToDatabase(item);
            }
            ObjectContext.SaveChanges();
        }
        public void 作废子集版本号(BottomStockSubVersion bottomStockSubVersion) {
            //根据选择的企业+子集版本号，作废整个企业+子集版本号数据及企业+合集的合集版本号数据
            var userInfo = Utils.GetCurrentUserInfo();
            var dbBorrowBill = this.ObjectContext.BottomStockSubVersions.Where(r => r.Id == bottomStockSubVersion.Id    && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbBorrowBill == null)
                throw new ValidationException("所选数据已作废");
            bottomStockSubVersion.Status = (int)DcsBaseDataStatus.作废;
            bottomStockSubVersion.EndTime = DateTime.Now;
            UpdateToDatabase(bottomStockSubVersion);
            //作废相同子集版本号子集数据
            var versions = this.ObjectContext.BottomStockSubVersions.Where(r => r.Id != bottomStockSubVersion.Id && r.ColVersionCode == bottomStockSubVersion.ColVersionCode && r.CompanyId == bottomStockSubVersion.CompanyId && r.Status == (int)DcsBaseDataStatus.有效).ToArray();
            foreach(var item in versions) {
                item.Status = (int)DcsBaseDataStatus.作废;
                item.EndTime = DateTime.Now;
                UpdateToDatabase(item);
            }
            //作废相同合集版本号合集数据
            var subVersion = this.ObjectContext.BottomStockColVersions.Where(r => r.ColVersionCode == bottomStockSubVersion.ColVersionCode && r.CompanyId == bottomStockSubVersion.CompanyId && r.Status == (int)DcsBaseDataStatus.有效).ToArray();
            foreach(var item in subVersion) {
                item.Status = (int)DcsBaseDataStatus.作废;
                item.EndTime = DateTime.Now;
                UpdateToDatabase(item);
            }
            //作废相同合集版本号的强制储备单
            var orders = ObjectContext.ForceReserveBills.Where(t => t.ColVersionCode == bottomStockSubVersion.ColVersionCode && t.CompanyId == bottomStockSubVersion.CompanyId && t.Status == (int)DCSForceReserveBillStatus.生效).ToArray();
            foreach(var item in orders) {
                item.Status = (int)DCSForceReserveBillStatus.作废;
                item.AbandonTime = DateTime.Now;
                item.AbandonerId = userInfo.Id;
                item.AbandonerName = userInfo.Name;
                UpdateToDatabase(item);
            }
            ObjectContext.SaveChanges();

        }
    }
    partial class DcsDomainService {
        [Update(UsingCustomMethod = true)]
        public void 作废合集版本号(BottomStockColVersion bottomStockColVersion) {
            new BottomStockColVersionAch(this).作废合集版本号(bottomStockColVersion);
        }
        [Update(UsingCustomMethod = true)]
        public void 作废子集版本号(BottomStockSubVersion bottomStockSubVersion) {
            new BottomStockColVersionAch(this).作废子集版本号(bottomStockSubVersion);
        }
    }
}
