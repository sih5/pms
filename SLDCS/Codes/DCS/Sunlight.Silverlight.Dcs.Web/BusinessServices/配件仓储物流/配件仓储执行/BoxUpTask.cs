﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel.DomainServices.Server;
using System.ComponentModel.DataAnnotations;
using Sunlight.Silverlight.Web;
using System.Transactions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using System.Data.Objects;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class BoxUpTaskAch : DcsSerivceAchieveBase {
        public BoxUpTaskAch(DcsDomainService domainService)
            : base(domainService) {
        }

        public IEnumerable<VirtualBoxUpTask> 查询配件装箱任务(string code, int? warehouseId, int? partsSalesCategoryId, int? status, string sourceCode,
            string partsOutboundPlanCode, string counterpartCompanyCode, string counterpartCompanyName, string sparePartCode, string sparePartName,
            DateTime? bCreateTime, DateTime? eCreateTime, DateTime? bModifyTime, DateTime? eModifyTime) {
            string SQL = @"select bt.Id,bt.Code,bt.WarehouseId,bt.WarehouseCode,bt.WarehouseName,bt.CounterpartCompanyId,bt.CounterpartCompanyCode, bt.CounterpartCompanyName,
                            bt.Status, bt.IsExistShippingOrder,bt.ContainerNumber, bt.CreatorId,bt.CreatorName,bt.CreateTime,bt.ModifierId,bt.ModifierName,bt.ModifyTime,bt.BoxUpFinishTime,
                            bt.Remark,bt.RowVersion,psc.id as PartsSalesCategoryId,psc.name as PartsSalesCategoryName,bt.OrderTypeName
                            from boxuptask bt 
                            left join boxuptaskdetail btd on btd.boxuptaskid=bt.id 
                            left join PartsSalesCategory psc on psc.id = bt.partssalescategoryid
                            left join warehouseoperator wo on wo.warehouseid=bt.warehouseid 
                            where 1=1 ";
            SQL = SQL + " and wo.operatorid =" + Utils.GetCurrentUserInfo().Id;

            if(!string.IsNullOrEmpty(code)) {
                SQL = SQL + " and bt.Code like '%" + code + "%'";
            }
            if(warehouseId.HasValue) {
                SQL = SQL + " and bt.warehouseId =" + warehouseId;
            }
            if(partsSalesCategoryId.HasValue) {
                SQL = SQL + " and bt.partsSalesCategoryId =" + partsSalesCategoryId + "";
            }
            if(status.HasValue) {
                SQL = SQL + " and bt.status=" + status.Value;
            }

            if(!string.IsNullOrEmpty(sourceCode)) {
                SQL = SQL + " and btd.SourceCode like '%" + sourceCode + "%'";
            }
            if(!string.IsNullOrEmpty(sparePartCode)) {
                SQL = SQL + " and btd.sparePartCode like '%" + sparePartCode + "%'";
            }
            if(!string.IsNullOrEmpty(sparePartName)) {
                SQL = SQL + " and btd.sparePartName like '%" + sparePartName + "%'";
            }
            if(!string.IsNullOrEmpty(partsOutboundPlanCode)) {
                SQL = SQL + " and btd.PartsOutboundPlanCode like '%" + partsOutboundPlanCode + "%'";
            }
            if(!string.IsNullOrEmpty(counterpartCompanyCode)) {
                SQL = SQL + " and bt.CounterpartCompanyCode like '%" + counterpartCompanyCode + "%'";
            }
            if(!string.IsNullOrEmpty(counterpartCompanyName)) {
                SQL = SQL + " and bt.CounterpartCompanyName like '%" + counterpartCompanyName + "%'";
            }
            if(bCreateTime.HasValue) {
                SQL = SQL + " and bt.CreateTime>= to_date('" + bCreateTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }
            if(eCreateTime.HasValue) {
                SQL = SQL + " and bt.CreateTime<= to_date('" + eCreateTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }
            if(bModifyTime.HasValue) {
                SQL = SQL + " and bt.BoxUpFinishTime>= to_date('" + bModifyTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }
            if(eModifyTime.HasValue) {
                SQL = SQL + " and bt.BoxUpFinishTime<= to_date('" + eModifyTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }

            SQL = SQL + " group by bt.Id,bt.Code,bt.WarehouseId,bt.WarehouseCode,bt.WarehouseName,bt.CounterpartCompanyId,bt.CounterpartCompanyCode, bt.CounterpartCompanyName, bt.Status, bt.IsExistShippingOrder,bt.ContainerNumber, bt.CreatorId,bt.CreatorName,bt.CreateTime,bt.ModifierId,bt.ModifierName,bt.ModifyTime,bt.BoxUpFinishTime, bt.Remark,bt.RowVersion,psc.id ,psc.name,bt.OrderTypeName order by bt.createtime desc";

            var search = ObjectContext.ExecuteStoreQuery<VirtualBoxUpTask>(SQL).ToList();
            return search;
        }


        public IEnumerable<VirtualPickingTaskForBoxUp> getPickingTaskForBoxUp(string code, int? status, string counterpartCompanyCode, string counterpartCompanyName,
            DateTime? bCreateTime, DateTime? eCreateTime, DateTime? bPickingFinishTime, DateTime? ePickingFinishTime) {
            string SQL = @"select pt.Id,pt.Code,pc.name as PartsSalesCategoryName,pt.WarehouseId,pt.WarehouseCode,pt.WarehouseName,pt.CounterpartCompanyId,pt.CounterpartCompanyCode,pt.CounterpartCompanyName,pt.CreatorName,pt.CreateTime,pt.PickingFinishTime 
                            ,(select b.ReceivingAddress from pickingtaskdetail a inner join partsoutboundplan b on a.partsoutboundplanid = b.id where a.pickingtaskid = pt.id and rownum <=1 ) as ReceivingAddress,(select b.ReceivingWarehouseId from pickingtaskdetail a inner join partsoutboundplan b on a.partsoutboundplanid = b.id where a.pickingtaskid = pt.id and rownum <= 1) as ReceivingWarehouseId,pt.ShippingMethod,pt.OrderTypeId,pt.OrderTypeName from pickingtask pt 
                            inner join partssalescategory pc on pc.id = pt.partssalescategoryid
                            inner join warehouseoperator wo on wo.warehouseid = pt.warehouseid where pt.status = 3 and exists (select 1 from pickingtaskdetail tmp where nvl(tmp.pickingqty , 0)>0 and tmp.pickingtaskid = pt.id) ";
            SQL = SQL + " and wo.operatorid =" + Utils.GetCurrentUserInfo().Id;

            if(!string.IsNullOrEmpty(code)) {
                SQL = SQL + " and pt.Code like '%" + code + "%'";
            }

            if(status.HasValue) {
                SQL = SQL + " and pt.status=" + status.Value;
            }

            if(!string.IsNullOrEmpty(counterpartCompanyCode)) {
                SQL = SQL + " and pt.CounterpartCompanyCode like '%" + counterpartCompanyCode + "%'";
            }
            if(!string.IsNullOrEmpty(counterpartCompanyName)) {
                SQL = SQL + " and pt.CounterpartCompanyName like '%" + counterpartCompanyName + "%'";
            }
            if(bCreateTime.HasValue) {
                SQL = SQL + " and pt.CreateTime>= to_date('" + bCreateTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }
            if(eCreateTime.HasValue) {
                SQL = SQL + " and pt.CreateTime<= to_date('" + eCreateTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }
            if(bPickingFinishTime.HasValue) {
                SQL = SQL + " and pt.PickingFinishTime>= to_date('" + bPickingFinishTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }
            if(ePickingFinishTime.HasValue) {
                SQL = SQL + " and pt.PickingFinishTime<= to_date('" + ePickingFinishTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }

            SQL = SQL + " order by pt.code desc";

            var search = ObjectContext.ExecuteStoreQuery<VirtualPickingTaskForBoxUp>(SQL).ToList();
            return search;
        }

        public VirtualBoxUpTask getBoxUpTaskById(int id) {
            string sql = "select bt.Id,bt.Code,bt.PartsSalesCategoryId,psc.name as PartsSalesCategoryName,bt.WarehouseId,bt.WarehouseCode,bt.WarehouseName,bt.CounterpartCompanyId,bt.CounterpartCompanyCode,bt.CounterpartCompanyName,bt.Status,bt.IsExistShippingOrder,bt.ContainerNumber,bt.CreatorId,bt.CreatorName,bt.CreateTime,bt.ModifierId,bt.ModifierName,bt.ModifyTime,bt.BoxUpFinishTime,bt.Remark  from boxuptask bt left join PartsSalesCategory psc on psc.id=bt.PartsSalesCategoryId";
            sql += " where bt.id =" + id;
            var search = ObjectContext.ExecuteStoreQuery<VirtualBoxUpTask>(sql).SingleOrDefault();
            return search;
        }

        public IEnumerable<VirtualBoxUpTaskDetail> getBoxUpTaskDetailsByTaskId(int id) {
            string SQL = @"select btd.Id,btd.BoxUpTaskId,btd.BoxUpTaskCode,btd.PartsOutboundPlanId,btd.PartsOutboundPlanCode,btd.PickingTaskId,btd.PickingTaskCode,btd.SourceCode,btd.SparePartId,btd.SparePartCode,
                            btd.SparePartName,sp.MeasureUnit,btd.SihCode,btd.PlanQty,nvl(btd.BoxUpQty,0) as BoxUpQty,(btd.PlanQty - nvl(btd.BoxUpQty,0)) as NowBoxUpQuantity,btd.PickingTaskDetailId,btd.ContainerNumber from boxuptask  bt
                            inner join boxuptaskdetail btd on bt.id=btd.boxuptaskid
                            inner join sparepart sp on sp.id=btd.sparepartid
                             inner join PickingTaskDetail c on c.id = btd.PickingTaskDetailId
                            where bt.id = " + id + "  order by c.warehouseareacode asc";

            var search = ObjectContext.ExecuteStoreQuery<VirtualBoxUpTaskDetail>(SQL).ToList();
            return search;
        }

        internal IEnumerable<PickingTask> GetPickingTasksWithLock(IEnumerable<PickingTask> filter) {
            if (filter == null)
                return Enumerable.Empty<PickingTask>();

            //考虑到filter是本地过滤数据的可能性，使用Id关联代替对象关联
            var query = filter as IQueryable<PickingTask>;
            var pickingTaskIds = query == null ? filter.Select(v => v.Id) : query.Select(v => v.Id);

            var objectQuery = ObjectContext.PickingTasks.Where(v => pickingTaskIds.Contains(v.Id)) as ObjectQuery<PickingTask>;
            if (objectQuery == null)
                return Enumerable.Empty<PickingTask>();

            var sql = string.Format("select * from ({0}) for update", objectQuery.ToTraceString());
#if SqlServer
            var paramList = objectQuery.Parameters;
#else
            var paramList = objectQuery.Parameters.Select(v => new Devart.Data.Oracle.OracleParameter(v.Name, v.Value) as object).ToArray();
#endif
            return ObjectContext.ExecuteStoreQuery<PickingTask>(sql, "PickingTasks", MergeOption.AppendOnly, paramList);
        }

        internal void CreateBoxUpTask(VirtualPickingTaskForBoxUp[] virtualPickingTasks) {
            if(virtualPickingTasks != null && !virtualPickingTasks.Any()) {
                throw new ValidationException(ErrorStrings.BoxUpTask_Validation1);
            }
            using (var transaction = new TransactionScope(TransactionScopeOption.RequiresNew)) {
                var pickingTaskIds = virtualPickingTasks.Select(r => r.Id).Distinct().ToList();
                var pickingTasksOrig = GetPickingTasksWithLock(ObjectContext.PickingTasks.Where(r => pickingTaskIds.Contains(r.Id))).ToList();
                var pickingTaskDetailsOrig = ObjectContext.PickingTaskDetails.Where(r => pickingTaskIds.Contains(r.PickingTaskId)).Distinct().ToList();

                foreach (var pick in pickingTasksOrig) {
                    if (pick.Status != (int)DcsPickingTaskStatus.拣货完成) {
                        throw new ValidationException(ErrorStrings.BoxUpTask_Validation2);
                    }
                }
                if (pickingTaskDetailsOrig.Sum(r => r.PickingQty ?? 0) <= 0)
                    throw new ValidationException(ErrorStrings.BoxUpTask_Validation3);

                var pickingTaskFirst = pickingTasksOrig.First();
                //生成装箱任务单
                var user = Utils.GetCurrentUserInfo();
                var now = DateTime.Now;
                var boxUpTask = new BoxUpTask();
                boxUpTask.Code = CodeGenerator.Generate("BoxUpTask", user.EnterpriseCode);
                boxUpTask.PartsSalesCategoryId = pickingTaskFirst.PartsSalesCategoryId;
                boxUpTask.WarehouseId = pickingTaskFirst.WarehouseId;
                boxUpTask.WarehouseCode = pickingTaskFirst.WarehouseCode;
                boxUpTask.WarehouseName = pickingTaskFirst.WarehouseName;
                boxUpTask.CounterpartCompanyId = pickingTaskFirst.CounterpartCompanyId;
                boxUpTask.CounterpartCompanyName = pickingTaskFirst.CounterpartCompanyName;
                boxUpTask.CounterpartCompanyCode = pickingTaskFirst.CounterpartCompanyCode;
                boxUpTask.Status = (int)DcsBoxUpTaskStatus.新建;
                boxUpTask.IsExistShippingOrder = false;
                boxUpTask.CreatorId = user.Id;
                boxUpTask.CreatorName = user.Name;
                boxUpTask.CreateTime = now;
                boxUpTask.OrderTypeId = pickingTaskFirst.OrderTypeId;
                boxUpTask.OrderTypeName = pickingTaskFirst.OrderTypeName;

                var sparePartIds = pickingTaskDetailsOrig.Select(r => r.SparePartId).Distinct().ToList();
                var spareParts = ObjectContext.SpareParts.Where(r => sparePartIds.Contains(r.Id)).ToList();

                //生成装箱任务单清单
                foreach (var pickingTaskDetail in pickingTaskDetailsOrig) {
                    var pickingQty = pickingTaskDetail.PickingQty ?? 0;
                    if (pickingQty > 0) {
                        var boxUpTaskDetail = new BoxUpTaskDetail();
                        var pickingTaskOrig = pickingTasksOrig.Single(r => r.Id == pickingTaskDetail.PickingTaskId);
                        var sparePart = spareParts.Single(r => r.Id == pickingTaskDetail.SparePartId);

                        boxUpTaskDetail.BoxUpTaskCode = boxUpTask.Code;
                        boxUpTaskDetail.PartsOutboundPlanId = pickingTaskDetail.PartsOutboundPlanId;
                        boxUpTaskDetail.PartsOutboundPlanCode = pickingTaskDetail.PartsOutboundPlanCode;
                        boxUpTaskDetail.PickingTaskId = pickingTaskOrig.Id;
                        boxUpTaskDetail.PickingTaskCode = pickingTaskOrig.Code;
                        boxUpTaskDetail.SourceCode = pickingTaskDetail.SourceCode;
                        boxUpTaskDetail.SparePartId = sparePart.Id;
                        boxUpTaskDetail.SparePartCode = sparePart.Code;
                        boxUpTaskDetail.SparePartName = sparePart.Name;
                        boxUpTaskDetail.SihCode = sparePart.ReferenceCode;
                        boxUpTaskDetail.PlanQty = pickingQty;
                        boxUpTaskDetail.PickingTaskDetailId = pickingTaskDetail.Id;

                        boxUpTask.BoxUpTaskDetails.Add(boxUpTaskDetail);
                    }
                }

                InsertToDatabase(boxUpTask);

                //修改拣货任务单状态为 已生成装箱任务
                foreach (var pickingTask in pickingTasksOrig) {
                    pickingTask.Status = (int)DcsPickingTaskStatus.已生成装箱任务;
                    pickingTask.ModifyTime = DateTime.Now;
                    pickingTask.ModifierName = user.Name;
                    pickingTask.ModifierId = user.Id;
                    UpdateToDatabase(pickingTask);
                }
                
                ObjectContext.SaveChanges();
                transaction.Complete();
            }
        }

        public void doBoxUp(VirtualBoxUpTaskDetail[] boxUpTaskDetails, string containerNumber, string remark, bool isBoxUpFinish) {
            using(var transaction = new TransactionScope()) {
                var boxUpTaskId = boxUpTaskDetails.Select(r => r.BoxUpTaskId).Distinct().First();
                var boxUpTaskOrig = ObjectContext.BoxUpTasks.Where(r => r.Id == boxUpTaskId).First();
                if(boxUpTaskOrig.Status == (int)DcsBoxUpTaskStatus.作废 || boxUpTaskOrig.Status == (int)DcsBoxUpTaskStatus.装箱完成 || boxUpTaskOrig.Status == (int)DcsBoxUpTaskStatus.终止) {
                    throw new ValidationException(ErrorStrings.BoxUpTask_Validation4);
                }
                var boxUpTaskDetailOrigs = ObjectContext.BoxUpTaskDetails.Where(r => r.BoxUpTaskId == boxUpTaskId).ToList();
                var user = Utils.GetCurrentUserInfo();

                foreach(var boxUpTaskDetail in boxUpTaskDetails) {
                    var boxUpTaskDetailOrig = boxUpTaskDetailOrigs.Single(r => r.Id == boxUpTaskDetail.Id);
                    var nowBoxUpQuantity = boxUpTaskDetail.NowBoxUpQuantity;
                    var planQuantity = boxUpTaskDetailOrig.PlanQty;
                    var boxUpQuantity = boxUpTaskDetailOrig.BoxUpQty ?? 0;

                    if(nowBoxUpQuantity + boxUpQuantity > planQuantity) {
                        throw new ValidationException(ErrorStrings.BoxUpTask_Validation5);
                    }

                    if(nowBoxUpQuantity != 0) {
                        //如果数据库中清单箱号为空，或者箱号相同，累加已装箱数量
                        if(string.IsNullOrEmpty(boxUpTaskDetailOrig.ContainerNumber) || boxUpTaskDetailOrig.ContainerNumber.Equals(containerNumber)) {
                            //更新装箱单清单
                            boxUpTaskDetailOrig.BoxUpQty = boxUpQuantity + nowBoxUpQuantity;
                            boxUpTaskDetailOrig.ContainerNumber = containerNumber;
                            UpdateToDatabase(boxUpTaskDetailOrig);
                        }
                            //如果数据库箱号与本次装箱箱号不一致,拆分原清单
                        else {
                            //更新装箱单清单
                            boxUpTaskDetailOrig.PlanQty = boxUpQuantity;
                            UpdateToDatabase(boxUpTaskDetailOrig);

                            //拆分,计划量 = 原清单计划量 - 原清单已装箱量，已装箱量 = 本次装箱量
                            var newBoxUpTaskDetail = new BoxUpTaskDetail();
                            newBoxUpTaskDetail.BoxUpTaskId = boxUpTaskOrig.Id;
                            newBoxUpTaskDetail.BoxUpTaskCode = boxUpTaskOrig.Code;
                            newBoxUpTaskDetail.PartsOutboundPlanId = boxUpTaskDetail.PartsOutboundPlanId;
                            newBoxUpTaskDetail.PartsOutboundPlanCode = boxUpTaskDetail.PartsOutboundPlanCode;
                            newBoxUpTaskDetail.PickingTaskId = boxUpTaskDetail.PickingTaskId;
                            newBoxUpTaskDetail.PickingTaskCode = boxUpTaskDetail.PickingTaskCode;
                            newBoxUpTaskDetail.SourceCode = boxUpTaskDetail.SourceCode;
                            newBoxUpTaskDetail.SparePartId = boxUpTaskDetail.SparePartId;
                            newBoxUpTaskDetail.SparePartCode = boxUpTaskDetail.SparePartCode;
                            newBoxUpTaskDetail.SparePartName = boxUpTaskDetail.SparePartName;
                            newBoxUpTaskDetail.SihCode = boxUpTaskDetail.SihCode;
                            newBoxUpTaskDetail.PlanQty = planQuantity - boxUpQuantity;
                            newBoxUpTaskDetail.PickingTaskDetailId = boxUpTaskDetail.PickingTaskDetailId;
                            newBoxUpTaskDetail.ContainerNumber = containerNumber;
                            newBoxUpTaskDetail.BoxUpQty = nowBoxUpQuantity;
                            InsertToDatabase(newBoxUpTaskDetail);
                        }

                        //新增出库实绩
                        PartsOutboundPerformance partsOutboundPerformance = new PartsOutboundPerformance();
                        partsOutboundPerformance.Code = boxUpTaskDetail.BoxUpTaskCode;
                        partsOutboundPerformance.OutType = (int)DcsPartsOutboundPer_OutType.装箱;
                        partsOutboundPerformance.Qty = nowBoxUpQuantity;
                        partsOutboundPerformance.OperaterId = user.Id;
                        partsOutboundPerformance.OperaterName = user.Name;
                        partsOutboundPerformance.OperaterEndTime = DateTime.Now;
                        partsOutboundPerformance.Equipment = (int)DcsPartsOutboundPer_EquipmentType.PC;
                        partsOutboundPerformance.ContainerNumber = containerNumber;
                        partsOutboundPerformance.SparePartId = boxUpTaskDetail.SparePartId;
                        partsOutboundPerformance.SpareCode = boxUpTaskDetail.SparePartCode;
                        partsOutboundPerformance.SpareName = boxUpTaskDetail.SparePartName;
                        InsertToDatabase(partsOutboundPerformance);
                    }
                }

                ObjectContext.SaveChanges();
                //重新查询清单数据
                boxUpTaskDetailOrigs = ObjectContext.BoxUpTaskDetails.Where(r => r.BoxUpTaskId == boxUpTaskId).ToList();

                //如果是点击装箱完成按钮，未装箱的部分拆分成另一张装箱单
                var now = DateTime.Now;
                if(isBoxUpFinish) {
                    //如果全部未装箱，则直接改为停用状态
                    var boxupSum = boxUpTaskDetailOrigs.Sum(r => r.BoxUpQty);
                    if((boxupSum ?? 0) == 0) {
                        //更新主单状态
                        boxUpTaskOrig.Status = (int)DcsBoxUpTaskStatus.停用;
                        boxUpTaskOrig.ModifierId = user.Id;
                        boxUpTaskOrig.ModifierName = user.Name;
                        boxUpTaskOrig.ModifyTime = now;
                        boxUpTaskOrig.Remark = remark;
                        boxUpTaskOrig.ContainerNumber = containerNumber;
                        UpdateToDatabase(boxUpTaskOrig);
                    } else {
                        //更新主单状态
                        boxUpTaskOrig.Status = (int)DcsBoxUpTaskStatus.装箱完成;
                        boxUpTaskOrig.Remark = remark;
                        boxUpTaskOrig.ContainerNumber = containerNumber;
                        boxUpTaskOrig.ModifierId = user.Id;
                        boxUpTaskOrig.ModifierName = user.Name;
                        boxUpTaskOrig.ModifyTime = now;
                        boxUpTaskOrig.BoxUpFinishTime = now;
                        UpdateToDatabase(boxUpTaskOrig);

                        var unFinish = boxUpTaskDetailOrigs.Where(r => r.BoxUpQty != r.PlanQty).ToArray();
                        if(unFinish != null && unFinish.Count() > 0) {
                            //创建拆分之后产生的主单,状态=停用
                            var boxUpTask = new BoxUpTask();
                            boxUpTask.Code = CodeGenerator.Generate("BoxUpTask", user.EnterpriseCode);
                            boxUpTask.PartsSalesCategoryId = boxUpTaskOrig.PartsSalesCategoryId;
                            boxUpTask.WarehouseId = boxUpTaskOrig.WarehouseId;
                            boxUpTask.WarehouseCode = boxUpTaskOrig.WarehouseCode;
                            boxUpTask.WarehouseName = boxUpTaskOrig.WarehouseName;
                            boxUpTask.CounterpartCompanyId = boxUpTaskOrig.CounterpartCompanyId;
                            boxUpTask.CounterpartCompanyName = boxUpTaskOrig.CounterpartCompanyName;
                            boxUpTask.CounterpartCompanyCode = boxUpTaskOrig.CounterpartCompanyCode;
                            boxUpTask.Status = (int)DcsBoxUpTaskStatus.停用;
                            boxUpTask.IsExistShippingOrder = false;
                            boxUpTask.CreatorId = user.Id;
                            boxUpTask.CreatorName = user.Name;
                            boxUpTask.CreateTime = now;

                            foreach(var item in unFinish) {
                                var plan = item.PlanQty;
                                var boxup = item.BoxUpQty ?? 0;
                                var newPlan = plan - boxup;

                                //创建拆分之后的清单
                                var newBoxUpDetail = new BoxUpTaskDetail();
                                newBoxUpDetail.BoxUpTaskId = boxUpTask.Id;
                                newBoxUpDetail.BoxUpTaskCode = boxUpTask.Code;
                                newBoxUpDetail.PartsOutboundPlanId = item.PartsOutboundPlanId;
                                newBoxUpDetail.PartsOutboundPlanCode = item.PartsOutboundPlanCode;
                                newBoxUpDetail.PickingTaskId = item.PickingTaskId;
                                newBoxUpDetail.PickingTaskCode = item.PickingTaskCode;
                                newBoxUpDetail.PickingTaskDetailId = item.PickingTaskDetailId;
                                newBoxUpDetail.SourceCode = item.SourceCode;
                                newBoxUpDetail.SparePartId = item.SparePartId;
                                newBoxUpDetail.SparePartCode = item.SparePartCode;
                                newBoxUpDetail.SparePartName = item.SparePartName;
                                newBoxUpDetail.SihCode = item.SihCode;
                                newBoxUpDetail.PlanQty = newPlan;
                                boxUpTask.BoxUpTaskDetails.Add(newBoxUpDetail);

                                if(boxup == 0) {
                                    //删除原清单
                                    DeleteFromDatabase(item);
                                } else {
                                    //更新原清单
                                    item.PlanQty = boxup;
                                    UpdateToDatabase(item);
                                }
                            }
                            InsertToDatabase(boxUpTask);
                        }
                    }
                } else {
                    //更新主单状态
                    bool flag = true;
                    foreach(var boxUpTaskDetailOrig1 in boxUpTaskDetailOrigs) {
                        var boxupqty = boxUpTaskDetailOrig1.BoxUpQty ?? 0;
                        var planqty = boxUpTaskDetailOrig1.PlanQty;
                        if(planqty != boxupqty) {
                            flag = false;
                        }
                    }
                    if(flag) {
                        boxUpTaskOrig.Status = (int)DcsBoxUpTaskStatus.装箱完成;
                        boxUpTaskOrig.BoxUpFinishTime = DateTime.Now;
                    } else
                        boxUpTaskOrig.Status = (int)DcsBoxUpTaskStatus.部分装箱;
                    boxUpTaskOrig.Remark = remark;
                    boxUpTaskOrig.ContainerNumber = containerNumber;
                    boxUpTaskOrig.ModifierId = user.Id;
                    boxUpTaskOrig.ModifierName = user.Name;
                    boxUpTaskOrig.ModifyTime = DateTime.Now;

                    UpdateToDatabase(boxUpTaskOrig);
                }

                ObjectContext.SaveChanges();
                //更新出库计划单状态
                var partsOutboundPlanIds = boxUpTaskDetails.Select(r => r.PartsOutboundPlanId).Distinct().ToArray();
                var partsOutboundPlans = ObjectContext.PartsOutboundPlans.Where(r => partsOutboundPlanIds.Contains(r.Id)).ToArray();
                foreach(var partsOutboundPlan in partsOutboundPlans) {
                    new PartsOutboundPlanAch(this.DomainService).更新出库计划单状态(partsOutboundPlan);
                }
                ObjectContext.SaveChanges();
                transaction.Complete();
            }
        }

        public void abandonBoxUpTask(VirtualBoxUpTask boxUpTask) {
            var boxUpTaskOrig = ObjectContext.BoxUpTasks.Where(r => r.Id == boxUpTask.Id).SingleOrDefault();
            if(boxUpTaskOrig.Status != (int)DcsBoxUpTaskStatus.新建) {
                throw new ValidationException(ErrorStrings.BoxUpTask_Validation6);
            }
            var user = Utils.GetCurrentUserInfo();
            //作废装箱单 
            boxUpTaskOrig.Status = (int)DcsBoxUpTaskStatus.作废;
            boxUpTaskOrig.ModifierId = user.Id;
            boxUpTaskOrig.ModifierName = user.Name;
            boxUpTaskOrig.ModifyTime = DateTime.Now;
            UpdateToDatabase(boxUpTaskOrig);

            //将拣货任务单状态恢复为拣货完成
            var boxUpTaskDetails = ObjectContext.BoxUpTaskDetails.Where(r => r.BoxUpTaskId == boxUpTaskOrig.Id).ToArray();
            var pickingTaskIds = boxUpTaskDetails.Select(r => r.PickingTaskId).ToArray();
            var pickingTasks = ObjectContext.PickingTasks.Where(r => pickingTaskIds.Contains(r.Id)).ToArray();
            foreach(var pickingTask in pickingTasks) {
                if(pickingTask.Status == (int)DcsPickingTaskStatus.已生成装箱任务) {
                    pickingTask.Status = (int)DcsPickingTaskStatus.拣货完成;
                    pickingTask.ModifierId = user.Id;
                    pickingTask.ModifierName = user.Name;
                    pickingTask.ModifyTime = DateTime.Now;
                    UpdateToDatabase(pickingTask);
                }
            }
            ObjectContext.SaveChanges();
        }

        public void stopBoxUpTask(VirtualBoxUpTask boxUpTask) {
            var boxUpTaskOrig = ObjectContext.BoxUpTasks.Where(r => r.Id == boxUpTask.Id).SingleOrDefault();

            if(boxUpTaskOrig.Status != (int)DcsBoxUpTaskStatus.停用) {
                throw new ValidationException(ErrorStrings.BoxUpTask_Validation7);
            }
            var user = Utils.GetCurrentUserInfo();

            //终止装箱单 
            boxUpTaskOrig.Status = (int)DcsBoxUpTaskStatus.终止;
            boxUpTaskOrig.ModifierId = user.Id;
            boxUpTaskOrig.ModifierName = user.Name;
            boxUpTaskOrig.ModifyTime = DateTime.Now;
            UpdateToDatabase(boxUpTaskOrig);

            var boxUpTaskDetails = ObjectContext.BoxUpTaskDetails.Where(r => r.BoxUpTaskId == boxUpTaskOrig.Id);
            if(boxUpTaskDetails == null || boxUpTaskDetails.Count() == 0) {
                throw new ValidationException(ErrorStrings.BoxUpTask_Validation8);
            }
            var PartsOutboundPlanId = boxUpTaskDetails.First().PartsOutboundPlanId;
            var PartsOutboundPlan = ObjectContext.PartsOutboundPlans.Where(r => r.Id == PartsOutboundPlanId).FirstOrDefault();
            var PickingTaskIds = boxUpTaskDetails.Select(r => r.PickingTaskId).Distinct().ToArray();
            var PickingTaskDetails = ObjectContext.PickingTaskDetails.Where(d => PickingTaskIds.Contains(d.PickingTaskId)).ToArray();

            //新增移库单
            PartsShiftOrder partsShiftOrder = new PartsShiftOrder();
            partsShiftOrder.Code = CodeGenerator.Generate("PartsShiftOrder", PartsOutboundPlan.StorageCompanyCode);
            partsShiftOrder.BranchId = PartsOutboundPlan.BranchId;
            partsShiftOrder.WarehouseId = PartsOutboundPlan.WarehouseId;
            partsShiftOrder.WarehouseCode = PartsOutboundPlan.WarehouseCode;
            partsShiftOrder.WarehouseName = PartsOutboundPlan.WarehouseName;
            partsShiftOrder.StorageCompanyId = PartsOutboundPlan.StorageCompanyId;
            partsShiftOrder.StorageCompanyCode = PartsOutboundPlan.StorageCompanyCode;
            partsShiftOrder.StorageCompanyName = PartsOutboundPlan.StorageCompanyName;
            partsShiftOrder.StorageCompanyType = PartsOutboundPlan.StorageCompanyType;
            partsShiftOrder.CreatorId = user.Id;
            partsShiftOrder.CreatorName = user.Name;
            partsShiftOrder.CreateTime = DateTime.Now;
            partsShiftOrder.Status = (int)DcsPartsShiftOrderStatus.生效;
            partsShiftOrder.Type = (int)DcsPartsShiftOrderType.问题区移库;
            partsShiftOrder.QuestionType = (int)DcsPartsShiftOrderQuestionType.装箱差异;

            //查询问题区库位
            var warehouseAreaCategories = ObjectContext.WarehouseAreaCategories.Where(r => r.Category == (int)DcsAreaType.问题区).ToArray();
            var warehouseAreaCategorieIds = warehouseAreaCategories.Select(r => r.Id).ToList();
            var warehouseArea = ObjectContext.WarehouseAreas.Where(r => warehouseAreaCategorieIds.Contains(r.AreaCategoryId ?? 0) && r.WarehouseId == PartsOutboundPlan.WarehouseId && r.AreaKind == (int)DcsAreaKind.库位 && r.Status == (int)DcsBaseDataStatus.有效).FirstOrDefault();

            if(warehouseArea == null) {
                throw new ValidationException(ErrorStrings.BoxUpTask_Validation9);
            }
            //查询待发区,有且只有一个，库位为“X1-01-01-01”
            var originalWarehouseArea = ObjectContext.WarehouseAreas.Where(r => r.WarehouseId == partsShiftOrder.WarehouseId && r.Code.Equals("X1-01-01-01")).SingleOrDefault();
            if(originalWarehouseArea == null) {
                throw new ValidationException(ErrorStrings.BoxUpTask_Validation10);
            }

            //新增移库清单,清单合并
            foreach(var item in boxUpTaskDetails) {
                var partsShiftOrderDetail = partsShiftOrder.PartsShiftOrderDetails.FirstOrDefault(r => r.SparePartId == item.SparePartId);
                if (partsShiftOrderDetail == null) {
                    partsShiftOrderDetail = new PartsShiftOrderDetail();
                    partsShiftOrderDetail.SparePartId = item.SparePartId;
                    partsShiftOrderDetail.SparePartCode = item.SparePartCode;
                    partsShiftOrderDetail.SparePartName = item.SparePartName;
                    //源库位为待发区库位，有且只有一个
                    partsShiftOrderDetail.OriginalWarehouseAreaId = originalWarehouseArea.Id;
                    partsShiftOrderDetail.OriginalWarehouseAreaCode = originalWarehouseArea.Code;
                    partsShiftOrderDetail.OriginalWarehouseAreaCategory = (int)DcsAreaType.待发区;
                    partsShiftOrderDetail.DestWarehouseAreaId = warehouseArea.Id;
                    partsShiftOrderDetail.DestWarehouseAreaCode = warehouseArea.Code;
                    partsShiftOrderDetail.DestWarehouseAreaCategory = (int)DcsAreaType.问题区;
                    partsShiftOrderDetail.Quantity = (int)item.PlanQty;
                    partsShiftOrder.PartsShiftOrderDetails.Add(partsShiftOrderDetail);
                } else {
                    partsShiftOrderDetail.Quantity += (int)item.PlanQty;
                }
            }
            InsertToDatabase(partsShiftOrder);

            //移库
            foreach(var detail in partsShiftOrder.PartsShiftOrderDetails) {
                var originalPartsStock = new PartsStockAch(this.DomainService).GetPartsStocksWithLock(ObjectContext.PartsStocks.Where(r => r.WarehouseAreaId == detail.OriginalWarehouseAreaId && r.PartId == detail.SparePartId)).FirstOrDefault();
                //问题区库存
                var destPartsStock = new PartsStockAch(this.DomainService).GetPartsStocksWithLock(ObjectContext.PartsStocks.Where(r => r.WarehouseAreaId == warehouseArea.Id && r.PartId == detail.SparePartId)).FirstOrDefault();
                //如果问题区库存不存在，创建一条库存记录
                if(destPartsStock == null) {
                    var ps = new PartsStock();
                    ps.WarehouseId = partsShiftOrder.WarehouseId;
                    ps.StorageCompanyId = partsShiftOrder.StorageCompanyId;
                    ps.StorageCompanyType = partsShiftOrder.StorageCompanyType;
                    ps.BranchId = partsShiftOrder.BranchId;
                    ps.WarehouseAreaId = detail.DestWarehouseAreaId;
                    ps.WarehouseAreaCategory = new WarehouseAreaCategory {
                        Category = (int)DcsAreaType.问题区
                    };
                    ps.PartId = detail.SparePartId;
                    ps.Quantity = detail.Quantity;
                    ps.CreatorId = user.Id;
                    ps.CreatorName = user.Name;
                    ps.CreateTime = DateTime.Now;

                    InsertToDatabase(ps);
                } else {
                    destPartsStock.Quantity += detail.Quantity;
                    destPartsStock.ModifierId = user.Id;
                    destPartsStock.ModifierName = user.Name;
                    destPartsStock.ModifyTime = DateTime.Now;
                    UpdateToDatabase(destPartsStock);
                }
                //更新待发区库存的锁定量，数量
                originalPartsStock.LockedQty -= detail.Quantity;
                originalPartsStock.Quantity -= detail.Quantity;
                originalPartsStock.ModifierId = user.Id;
                originalPartsStock.ModifierName = user.Name;
                originalPartsStock.ModifyTime = DateTime.Now;
                if(originalPartsStock.Quantity < 0 || originalPartsStock.LockedQty < 0) {
                    throw new ValidationException(ErrorStrings.BoxUpTask_Validation11);
                }
                UpdateToDatabase(originalPartsStock);
            }

            ObjectContext.SaveChanges();
        }

        public void insertBoxUpTask(VirtualBoxUpTask boxUpTask) {
        }

        public void updateBoxUpTask(VirtualBoxUpTask boxUpTask) {
        }
        public void insertBoxUpTaskDetail(VirtualBoxUpTaskDetail boxUpTaskDetail) {
        }

        public void updateBoxUpTaskDetail(VirtualBoxUpTaskDetail boxUpTaskDetail) {
        }

    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:33
        //原分布类的函数全部转移到Ach                                                               
        public IEnumerable<VirtualBoxUpTask> 查询配件装箱任务(string code, int? warehouseId, int? partsSalesCategoryId, int? status, string sourceCode,
            string partsOutboundPlanCode, string counterpartCompanyCode, string counterpartCompanyName, string sparePartCode, string sparePartName,
            DateTime? bCreateTime, DateTime? eCreateTime, DateTime? bModifyTime, DateTime? eModifyTime) {
            return new BoxUpTaskAch(this).查询配件装箱任务(code, warehouseId, partsSalesCategoryId, status, sourceCode, partsOutboundPlanCode, counterpartCompanyCode, counterpartCompanyName, sparePartCode, sparePartName,
            bCreateTime, eCreateTime, bModifyTime, eModifyTime);
        }

        public IEnumerable<VirtualPickingTaskForBoxUp> getPickingTaskForBoxUp(string code, int? status, string counterpartCompanyCode, string counterpartCompanyName,
            DateTime? bCreateTime, DateTime? eCreateTime, DateTime? bPickingFinishTime, DateTime? ePickingFinishTime) {
            return new BoxUpTaskAch(this).getPickingTaskForBoxUp(code, status, counterpartCompanyCode, counterpartCompanyName, bCreateTime, eCreateTime, bPickingFinishTime, ePickingFinishTime);
        }
        public VirtualBoxUpTask getBoxUpTaskById(int id) {
            return new BoxUpTaskAch(this).getBoxUpTaskById(id);
        }
        public IEnumerable<VirtualBoxUpTaskDetail> getBoxUpTaskDetailsByTaskId(int id) {
            return new BoxUpTaskAch(this).getBoxUpTaskDetailsByTaskId(id);
        }

        [Invoke]
        public void CreateBoxUpTask(VirtualPickingTaskForBoxUp[] virtualPickingTasks) {
            new BoxUpTaskAch(this).CreateBoxUpTask(virtualPickingTasks);
        }
        [Invoke]
        public void doBoxUp(VirtualBoxUpTaskDetail[] boxUpTaskDetails, string containerNumber, string remark, bool isBoxUpFinish) {
            new BoxUpTaskAch(this).doBoxUp(boxUpTaskDetails, containerNumber, remark, isBoxUpFinish);
        }
        [Update(UsingCustomMethod = true)]
        public void abandonBoxUpTask(VirtualBoxUpTask boxUpTask) {
            new BoxUpTaskAch(this).abandonBoxUpTask(boxUpTask);
        }

        [Update(UsingCustomMethod = true)]
        public void stopBoxUpTask(VirtualBoxUpTask boxUpTask) {
            new BoxUpTaskAch(this).stopBoxUpTask(boxUpTask);
        }

        public void insertBoxUpTask(VirtualBoxUpTask boxUpTask) {
            new BoxUpTaskAch(this).insertBoxUpTask(boxUpTask);
        }


        public void updateBoxUpTask(VirtualBoxUpTask boxUpTask) {
            new BoxUpTaskAch(this).updateBoxUpTask(boxUpTask);
        }
        public void insertBoxUpTaskDetail(VirtualBoxUpTaskDetail boxUpTaskDetail) {
            new BoxUpTaskAch(this).insertBoxUpTaskDetail(boxUpTaskDetail);
        }

        public void updateBoxUpTaskDetail(VirtualBoxUpTaskDetail boxUpTaskDetail) {
            new BoxUpTaskAch(this).updateBoxUpTaskDetail(boxUpTaskDetail);
        }
    }
}
