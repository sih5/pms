﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class CompanyTransferOrderAch : DcsSerivceAchieveBase {
        public CompanyTransferOrderAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 审批服务站间配件调拨单(CompanyTransferOrder companyTransferOrder) {
            UpdateCompanyTransferOrder(companyTransferOrder);//此方法校验了状态必须为新建,并将清单也加到主单的复杂属性中
            var dbCompanyTransferOrder = ObjectContext.CompanyTransferOrders.Where(r => r.Id == companyTransferOrder.Id && r.Status == (int)DcsPartsTransferOrderStatus.新建).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            //校验调拨类型
            //if(dbCompanyTransferOrder != null && dbCompanyTransferOrder.Type != (int)DcsCompanyTransferOrderType.一二级网点调拨) {
            //    throw new ValidationException(ErrorStrings.CompanyTransferOrder_Validation7);
            //}
            var companyTransferOrderDetails = companyTransferOrder.CompanyTransferOrderDetails.ToArray();
            var partIds = companyTransferOrderDetails.Select(r => r.PartsId).ToArray();
            //获取调出经销商的清单中相关配件的库存
            var dealerPartsStrocksFrom = ObjectContext.DealerPartsStocks.Where(r => r.DealerId == companyTransferOrder.OriginalCompanyId && r.SalesCategoryId == companyTransferOrder.PartsSalesCategoryId && partIds.Contains(r.SparePartId)).ToArray();
            //校验调出经销商的清单中相关配件的库存是否满足确认数量
            foreach(var companyTransferOrderDetail in companyTransferOrderDetails) {
                var checkPartsStock = dealerPartsStrocksFrom.SingleOrDefault(r => r.SparePartId == companyTransferOrderDetail.PartsId);
                if(checkPartsStock == null || companyTransferOrderDetail.ConfirmedAmount > checkPartsStock.Quantity) {
                    throw new ValidationException(string.Format(ErrorStrings.CompanyTransferOrder_Validation4, companyTransferOrderDetail.PartsCode));
                }
            }
            var nowTime = DateTime.Now;
            var userInfo = Utils.GetCurrentUserInfo();
            companyTransferOrder.ModifierId = userInfo.Id;
            companyTransferOrder.ModifierName = userInfo.Name;
            companyTransferOrder.ModifyTime = nowTime;
            companyTransferOrder.ApproverId = userInfo.Id;
            companyTransferOrder.ApproverName = userInfo.Name;
            companyTransferOrder.ApproveTime = nowTime;
            //修改状态
            companyTransferOrder.Status = (int)DcsPartsTransferOrderStatus.已审批;
            var partsSalesCategory = ObjectContext.PartsSalesCategories.Where(r => r.Id == companyTransferOrder.PartsSalesCategoryId).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            //修改调出经销商对应库存数量
            foreach(var companyTransferOrderDetail in companyTransferOrderDetails) {
                var dealerPartsStock = dealerPartsStrocksFrom.SingleOrDefault(r => r.SparePartId == companyTransferOrderDetail.PartsId);
                var dbPartDeleaveInformation = ObjectContext.PartDeleaveInformations.SingleOrDefault(c => c.OldPartId == companyTransferOrderDetail.PartsId && c.PartsSalesCategoryId == companyTransferOrder.PartsSalesCategoryId && c.BranchId == partsSalesCategory.BranchId && c.Status == (int)DcsBaseDataStatus.有效);
                if(dealerPartsStock != null) {
                    if(dbPartDeleaveInformation != null) {
                        if(dealerPartsStock.Quantity < companyTransferOrderDetail.ConfirmedAmount * dbPartDeleaveInformation.DeleaveAmount) {
                            throw new ValidationException(string.Format(ErrorStrings.CompanyTransferOrder_Validation11, dealerPartsStock.SparePartCode));
                        }
                        dealerPartsStock.Quantity = (int)(dealerPartsStock.Quantity - companyTransferOrderDetail.ConfirmedAmount * dbPartDeleaveInformation.DeleaveAmount);
                    }else {
                        if(dealerPartsStock.Quantity < companyTransferOrderDetail.ConfirmedAmount) {
                            throw new ValidationException(string.Format(ErrorStrings.CompanyTransferOrder_Validation11, dealerPartsStock.SparePartCode));
                        }
                        dealerPartsStock.Quantity = (int)(dealerPartsStock.Quantity - companyTransferOrderDetail.ConfirmedAmount);
                    }
                }
            }
            //获取调入经销商的相关配件库存      
            var list = new List<DealerPartsStock>();
            var dealerPartsStrocksTo = ObjectContext.DealerPartsStocks.Where(r => r.DealerId == companyTransferOrder.DestCompanyId && r.SalesCategoryId == companyTransferOrder.PartsSalesCategoryId && partIds.Contains(r.SparePartId)).ToArray();
            //对调入经销商的相关配件库存修改
            foreach(var companyTransferOrderDetail in companyTransferOrderDetails) {
                var dealerPartsStock = dealerPartsStrocksTo.SingleOrDefault(r => r.SparePartId == companyTransferOrderDetail.PartsId);
                var dbPartDeleaveInformation = ObjectContext.PartDeleaveInformations.SingleOrDefault(c => c.OldPartId == companyTransferOrderDetail.PartsId && c.PartsSalesCategoryId == companyTransferOrder.PartsSalesCategoryId && c.BranchId == partsSalesCategory.BranchId && c.Status == (int)DcsBaseDataStatus.有效);
                if(dealerPartsStock != null) {
                    if(dbPartDeleaveInformation != null) {
                        dealerPartsStock.Quantity = (int)(dealerPartsStock.Quantity + companyTransferOrderDetail.ConfirmedAmount * dbPartDeleaveInformation.DeleaveAmount);
                    }else {
                        dealerPartsStock.Quantity = (int)(dealerPartsStock.Quantity + companyTransferOrderDetail.ConfirmedAmount);
                    }
                    dealerPartsStock.ModifierId = userInfo.Id;
                    dealerPartsStock.ModifierName = userInfo.Name;
                    dealerPartsStock.ModifyTime = nowTime;
                    UpdateToDatabase(dealerPartsStock);
                }else {
                    dealerPartsStock = new DealerPartsStock();
                    dealerPartsStock.DealerId = companyTransferOrder.DestCompanyId;
                    dealerPartsStock.DealerCode = companyTransferOrder.DestCompanyCode;
                    dealerPartsStock.DealerName = companyTransferOrder.DestCompanyName;
                    dealerPartsStock.SalesCategoryId = companyTransferOrder.PartsSalesCategoryId;
                    dealerPartsStock.SparePartId = companyTransferOrderDetail.PartsId;
                    dealerPartsStock.SparePartCode = companyTransferOrderDetail.PartsCode;
                    if(companyTransferOrderDetail.ConfirmedAmount == null) {
                        throw new ValidationException(ErrorStrings.CompanyTransferOrder_Validation5);
                    }
                    dealerPartsStock.Quantity = dbPartDeleaveInformation == null ? companyTransferOrderDetail.ConfirmedAmount.Value : companyTransferOrderDetail.ConfirmedAmount.Value * dbPartDeleaveInformation.DeleaveAmount;
                    list.Add(dealerPartsStock);
                }
            }
            if(list.Count > 0) {
                if(partsSalesCategory == null) {
                    throw new ValidationException(ErrorStrings.PartsSalesCategory_Validation4);
                }
                foreach(var item in list) {
                    item.BranchId = partsSalesCategory.BranchId;
                    item.SalesCategoryName = partsSalesCategory.Name;
                    item.CreatorId = userInfo.Id;
                    item.CreatorName = userInfo.Name;
                    item.CreateTime = nowTime;
                    ObjectContext.DealerPartsStocks.AddObject(item);
                    DomainService.生成经销商配件库存(item);
                }
            }
        }


        public void 作废企业间配件调拨单(CompanyTransferOrder companyTransferOrder) {
            var dbCompanyTransferOrder = ObjectContext.CompanyTransferOrders.Where(r => r.Id == companyTransferOrder.Id).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbCompanyTransferOrder == null) {
                throw new ValidationException(ErrorStrings.CompanyTransferOrder_Validation8);
            }
            if(dbCompanyTransferOrder.Status != (int)DcsPartsTransferOrderStatus.新建) {
                throw new ValidationException(ErrorStrings.CompanyTransferOrder_Validation9);
            }
            companyTransferOrder.Status = (int)DcsPartsTransferOrderStatus.作废;
            var userInfo = Utils.GetCurrentUserInfo();
            companyTransferOrder.AbandonerId = userInfo.Id;
            companyTransferOrder.AbandonerName = userInfo.Name;
            companyTransferOrder.AbandonTime = DateTime.Now;
            //因为只需要作废主单和清单没关系，所以没有必要把清单加到主单的复杂属性中
            UpdateToDatabase(companyTransferOrder);
        }


        public void 审核服务站兼代理库调拨(CompanyTransferOrder companyTransferOrder) {
            var nowTime = DateTime.Now;
            var userInfo = Utils.GetCurrentUserInfo();
            companyTransferOrder.ModifierId = userInfo.Id;
            companyTransferOrder.ModifierName = userInfo.Name;
            companyTransferOrder.ModifyTime = nowTime;
            companyTransferOrder.Status = (int)DcsPartsTransferOrderStatus.已审批;
            //校验是否存在
            var dbCompanyTransferOrder = ObjectContext.CompanyTransferOrders.Where(r => r.Id == companyTransferOrder.Id && r.Status != (int)DcsPartsTransferOrderStatus.作废).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbCompanyTransferOrder == null) {
                throw new ValidationException(ErrorStrings.CompanyTransferOrder_Validation2);
            }
            //校验状态
            if(dbCompanyTransferOrder.Status != (int)DcsPartsTransferOrderStatus.新建) {
                throw new ValidationException(ErrorStrings.CompanyTransferOrder_Validation2);
            }
            //校验调拨类型
            if(dbCompanyTransferOrder != null && dbCompanyTransferOrder.Type != (int)DcsCompanyTransferOrderType.服务站兼代理库调拨) {
                throw new ValidationException(ErrorStrings.CompanyTransferOrder_Validation6);
            }
            var companyTransferOrderDetails = companyTransferOrder.CompanyTransferOrderDetails.ToArray();
            var partIds = companyTransferOrderDetails.Select(r => r.PartsId).ToArray();
            #region 老逻辑
            #endregion
            var dealerPartsStrocks = ObjectContext.DealerPartsStocks.Where(r => r.DealerId == companyTransferOrder.OriginalCompanyId && r.SalesCategoryId == companyTransferOrder.PartsSalesCategoryId && partIds.Contains(r.SparePartId) && r.SubDealerId == -1).ToArray();
            foreach(var item in companyTransferOrderDetails) {
                var tempDealerPartsStrocks = dealerPartsStrocks.Where(r => r.SparePartId == item.PartsId).ToArray();
                if(tempDealerPartsStrocks.Length > 1) {
                    throw new ValidationException(ErrorStrings.CompanyTransferOrder_Validation12);
                }
                if(tempDealerPartsStrocks.Length == 0) {
                    throw new ValidationException(ErrorStrings.CompanyTransferOrder_Validation13);
                }
                if(tempDealerPartsStrocks[0].Quantity < item.ConfirmedAmount) {
                    throw new ValidationException(tempDealerPartsStrocks[0].SparePartCode + ErrorStrings.CompanyTransferOrder_Validation14);
                }
                tempDealerPartsStrocks[0].Quantity = tempDealerPartsStrocks[0].Quantity - item.ConfirmedAmount ?? 0;
                UpdateToDatabase(tempDealerPartsStrocks[0]);
            }
            var warehouse = ObjectContext.Warehouses.SingleOrDefault(r => r.Id == companyTransferOrder.DestWarehouseId && r.Status == (int)DcsBaseDataStatus.有效);
            if(warehouse == null) {
                throw new ValidationException(ErrorStrings.CompanyTransferOrder_Validation15);
            }
            var partsSalesCategory = ObjectContext.PartsSalesCategories.SingleOrDefault(r => r.Id == companyTransferOrder.PartsSalesCategoryId && r.Status == (int)DcsBaseDataStatus.有效);
            if(partsSalesCategory == null) {
                throw new ValidationException(ErrorStrings.CompanyTransferOrder_Validation16);
            }
            var partsInboundPlan = new PartsInboundPlan();
            //1）配件入库计划.配件入库计划编号根据编号规则自动生成
            partsInboundPlan.WarehouseId = warehouse.Id;    //2）配件入库计划.配件仓库Id=企业间配件调拨单.目标仓库Id
            partsInboundPlan.WarehouseCode = warehouse.Code; //3）配件入库计划.配件仓库编号=仓库.编号（仓库.Id=企业间配件调拨单.目标仓库Id）
            partsInboundPlan.WarehouseName = warehouse.Name;    //4）配件入库计划.配件仓库名称=仓库.名称（仓库.Id=企业间配件调拨单.目标仓库Id）
            partsInboundPlan.StorageCompanyId = companyTransferOrder.DestCompanyId;   //5）配件入库计划.仓储企业Id=企业间配件调拨单.目标企业Id
            partsInboundPlan.StorageCompanyCode = companyTransferOrder.DestCompanyCode;   //6）配件入库计划.仓储企业编号=企业间配件调拨单.目标企业编号
            partsInboundPlan.StorageCompanyName = companyTransferOrder.DestCompanyName;   //7）配件入库计划.仓储企业名称=企业间配件调拨单.目标企业名称
            partsInboundPlan.StorageCompanyType = (int)DcsCompanyType.服务站兼代理库;    //8）配件入库计划.仓储企业类型=服务站兼代理库
            partsInboundPlan.BranchId = partsSalesCategory.BranchId;    //9）配件入库计划.营销分公司Id=配件销售类型.营销分公司Id(配件销售类型.Id=企业间配件调拨单.配件销售类型Id)
            partsInboundPlan.BranchCode = partsSalesCategory.BranchCode;  //10）配件入库计划.营销分公司编号=配件销售类型.分公司编号
            partsInboundPlan.BranchName = partsSalesCategory.BranchName;    //11）配件入库计划.营销分公司名称=配件销售类型.名称
            partsInboundPlan.CounterpartCompanyId = companyTransferOrder.OriginalCompanyId;    //12）配件入库计划.对方单位Id=企业间配件调拨单.源企业Id
            partsInboundPlan.CounterpartCompanyCode = companyTransferOrder.OriginalCompanyCode;    //13）配件入库计划.对方单位编号=企业间配件调拨单.源企业编号
            partsInboundPlan.CounterpartCompanyName = companyTransferOrder.OriginalCompanyName;    //14)配件入库计划.对方单位名称=企业间配件调拨单.源企业名称
            partsInboundPlan.SourceId = companyTransferOrder.Id;    //15）配件入库计划.源单据Id=企业间配件调拨单.Id
            partsInboundPlan.SourceCode = companyTransferOrder.Code;    //16）配件入库计划.源单据编号=企业间配件调拨单.调拨单编号
            partsInboundPlan.InboundType = (int)DcsPartsInboundType.配件调拨;    //17）配件入库计划.入库类型=配件调拨
            partsInboundPlan.OriginalRequirementBillId = companyTransferOrder.Id;    //19）配件入库计划.原始需求单据Id=企业间配件调拨单.Id
            partsInboundPlan.OriginalRequirementBillCode = companyTransferOrder.Code;//38）配件入库计划.原始需求单据编号=企业间配件调拨单.调拨单编号
            partsInboundPlan.OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.企业间配件调拨;    //21）配件入库计划.原始需求单据类型=企业间配件调拨
            partsInboundPlan.Status = (int)DcsPartsInboundPlanStatus.检验完成;    //22）配件入库计划.状态=入库完成
            partsInboundPlan.Remark = companyTransferOrder.Remark;   //23）配件入库计划.备注=企业间配件调拨单.备注
            partsInboundPlan.PartsSalesCategoryId = partsSalesCategory.Id;//37）配件入库计划.配件销售类型Id=企业间配件调拨单.配件销售类型Id
            partsInboundPlan.CreatorId = userInfo.Id;
            partsInboundPlan.CreatorName = userInfo.Name;
            partsInboundPlan.CreateTime = DateTime.Now;
            foreach(var item in companyTransferOrderDetails) {
                var partsInboundPlanDetail = new PartsInboundPlanDetail();
                partsInboundPlan.PartsInboundPlanDetails.Add(partsInboundPlanDetail);
                partsInboundPlanDetail.SparePartId = item.PartsId;//27）配件入库计划清单.配件Id=企业间配件调拨清单.配件Id
                partsInboundPlanDetail.SparePartCode = item.PartsCode;//28）配件入库计划清单.配件编号=企业间配件调拨清单.配件编号
                partsInboundPlanDetail.SparePartName = item.PartsName;//29）配件入库计划清单.配件名称=企业间配件调拨清单.配件名称
                partsInboundPlanDetail.PlannedAmount = item.ConfirmedAmount ?? 0;//30）配件入库计划清单.计划量=企业间配件调拨清单.确认量
                partsInboundPlanDetail.InspectedQuantity = item.ConfirmedAmount;//31）配件入库计划清单.检验量=企业间配件调拨清单.确认量
                partsInboundPlanDetail.Price = item.Price;//32）配件入库计划清单.价格=企业间配件调拨清单.价格
                partsInboundPlanDetail.Remark = item.Remark;//33）配件入库计划清单.备注=企业间配件调拨清单.备注
                this.InsertToDatabase(partsInboundPlanDetail);
            }
            new PartsInboundPlanAch(this.DomainService).InsertPartsInboundPlanValidate(partsInboundPlan);
            var partsInboundCheckBill = new PartsInboundCheckBill();
            partsInboundCheckBill.PartsInboundPlan = partsInboundPlan;//2）配件入库检验单.配件入库计划Id=配件入库计划Id
            //1）配件入库检验单.配件入库检验单编号根据编号规则自动生成
            partsInboundCheckBill.WarehouseId = warehouse.Id;//3）配件入库检验单.配件仓库Id=企业间配件调拨单.目标仓库Id
            partsInboundCheckBill.WarehouseCode = warehouse.Code;//4）配件入库检验单.配件仓库编号=仓库.编号（仓库.Id=企业间配件调拨单.目标仓库Id）
            partsInboundCheckBill.WarehouseName = warehouse.Name;//5）配件入库检验单.配件仓库名称=仓库.名称（仓库.Id=企业间配件调拨单.目标仓库Id）
            partsInboundCheckBill.StorageCompanyId = companyTransferOrder.DestCompanyId;//6）配件入库检验单.仓储企业Id=企业间配件调拨单.目标企业Id
            partsInboundCheckBill.StorageCompanyCode = companyTransferOrder.DestCompanyCode;//7）配件入库检验单.仓储企业编号=企业间配件调拨单.目标企业编号
            partsInboundCheckBill.StorageCompanyName = companyTransferOrder.DestCompanyName;//8）配件入库检验单.仓储企业名称=企业间配件调拨单.目标企业名称
            partsInboundCheckBill.StorageCompanyType = (int)DcsCompanyType.服务站兼代理库;//9）配件入库检验单.仓储企业类型=服务站兼代理库
            partsInboundCheckBill.BranchId = partsSalesCategory.BranchId;//10）配件入库检验单.营销分公司Id=配件销售类型.营销分公司Id(配件销售类型.Id=企业间配件调拨单.配件销售类型Id)
            partsInboundCheckBill.BranchCode = partsSalesCategory.BranchCode;//11）配件入库检验单.营销分公司编号=配件销售类型.分公司编号
            partsInboundCheckBill.BranchName = partsSalesCategory.BranchName;//12）配件入库检验单.营销分公司名称=配件销售类型.分公司名称
            partsInboundCheckBill.CounterpartCompanyId = companyTransferOrder.OriginalCompanyId;//13）配件入库检验单.对方单位Id=企业间配件调拨单.目标企业Id
            partsInboundCheckBill.CounterpartCompanyCode = companyTransferOrder.OriginalCompanyCode;//14）配件入库检验单.对方单位编号=企业间配件调拨单.目标企业编号
            partsInboundCheckBill.CounterpartCompanyName = companyTransferOrder.OriginalCompanyName;//15)配件入库检验单.对方单位名称=企业间配件调拨单.目标企业名称
            partsInboundCheckBill.InboundType = (int)DcsPartsInboundType.配件调拨; //18）配件入库检验单.入库类型=配件调拨
            partsInboundCheckBill.OriginalRequirementBillId = companyTransferOrder.Id;//20）配件入库检验单.原始需求单据Id=企业间配件调拨单.Id
            partsInboundCheckBill.OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.企业间配件调拨; //21）配件入库检验单.原始需求单据类型=企业间配件调拨
            partsInboundCheckBill.OriginalRequirementBillCode = companyTransferOrder.Code;//    配件入库检验单.原始需求单据编号=企业间配件调拨单.调拨单编号
            partsInboundCheckBill.SettlementStatus = (int)DcsPartsSettlementStatus.不结算;//22）配件入库检验单.结算状态=不结算
            partsInboundCheckBill.Remark = companyTransferOrder.Remark;//23）配件入库检验单.备注=企业间配件调拨单.备注
            partsInboundCheckBill.PartsSalesCategoryId = companyTransferOrder.PartsSalesCategoryId;
            partsInboundCheckBill.CreatorId = userInfo.Id;
            partsInboundCheckBill.CreatorName = userInfo.Name;
            partsInboundCheckBill.CreateTime = DateTime.Now;
            var plannedPrices = ObjectContext.PartsPlannedPrices.Where(r => r.PartsSalesCategoryId == companyTransferOrder.PartsSalesCategoryId && r.OwnerCompanyId == partsSalesCategory.BranchId && partIds.Contains(r.SparePartId)).ToArray();
            //随机取一个库位
            var warehouseArea = (from a in ObjectContext.Warehouses.Where(r => r.Status == (int)DcsBaseDataStatus.有效)
                                 join b in ObjectContext.WarehouseAreas.Where(r => r.AreaKind == (int)DcsAreaKind.库位 && r.TopLevelWarehouseAreaId != null && r.Status == (int)DcsBaseDataStatus.有效) on a.Id equals b.WarehouseId
                                 join c in ObjectContext.WarehouseAreaCategories.Where(r => r.Category == (int)DcsAreaType.检验区) on b.AreaCategoryId equals c.Id
                                 where a.Id == warehouse.Id
                                 select b).OrderBy(r => r.Id).FirstOrDefault();
            if(warehouseArea == null) {
                throw new ValidationException(ErrorStrings.CompanyTransferOrder_Validation17);
            }
            var partsStocks = new PartsStockAch(this.DomainService).GetPartsStocksWithLock((from a in ObjectContext.PartsStocks
                               where a.WarehouseId == warehouse.Id && partIds.Contains(a.PartId) && a.WarehouseAreaId == warehouseArea.Id
                               select a)).ToArray();
            var partDeleaveInformations = ObjectContext.PartDeleaveInformations.Where(r => r.BranchId == partsSalesCategory.BranchId && r.PartsSalesCategoryId == companyTransferOrder.PartsSalesCategoryId && partIds.Contains(r.OldPartId) && r.Status == (int)DcsBaseDataStatus.有效).ToArray();
            foreach(var item in companyTransferOrderDetails) {
                var partsInboundCheckBillDetail = new PartsInboundCheckBillDetail();
                partsInboundCheckBill.PartsInboundCheckBillDetails.Add(partsInboundCheckBillDetail);//27）配件入库检验单清单.配件入库检验单Id=配件入库检验单Id
                partsInboundCheckBillDetail.SparePartId = item.PartsId;//28）配件入库检验单清单.配件Id=企业间配件调拨清单.配件Id
                partsInboundCheckBillDetail.SparePartCode = item.PartsCode;//29）配件入库检验单清单.配件编号=企业间配件调拨清单.配件编号
                partsInboundCheckBillDetail.SparePartName = item.PartsName;//30）配件入库检验单清单.配件名称=企业间配件调拨清单.配件名称
                partsInboundCheckBillDetail.InspectedQuantity = item.ConfirmedAmount ?? 0;//31）配件入库检验单清单.检验量=企业间配件调拨清单.确认量
                partsInboundCheckBillDetail.SettlementPrice = item.Price;//33）配件入库检验单清单.结算价=企业间配件调拨清单.价格
                partsInboundCheckBillDetail.WarehouseAreaCode = warehouseArea.Code;
                var tempPrices = plannedPrices.Where(r => r.SparePartId == item.PartsId).ToArray();
                if(tempPrices.Length != 1) {
                    throw new ValidationException(ErrorStrings.CompanyTransferOrder_Validation18);
                }
                partsInboundCheckBillDetail.CostPrice = tempPrices[0].PlannedPrice;//34）配件入库检验单清单.成本价=配件计划价.计划价//（配件计划价.隶属企业Id=配件销售类型.营销分公司Id(配件销售类型.Id=企业间配件调拨单.配件销售类型Id),配件Id=企业间配件调拨清单.配件Id）
                partsInboundCheckBillDetail.WarehouseAreaId = warehouseArea.Id;//40）配件入库检验单清单.库位id=该仓库随机检验区库位.id
                partsInboundCheckBillDetail.Remark = item.Remark; //35）配件入库检验单清单.备注=企业间配件调拨清单.备注
                this.InsertToDatabase(partsInboundCheckBillDetail);
                var unitDeleaveQuantity = 1;
                var tempDeleaveInformation = partDeleaveInformations.FirstOrDefault(r => r.OldPartId == item.PartsId);
                if(tempDeleaveInformation != null) {
                    unitDeleaveQuantity = tempDeleaveInformation.DeleaveAmount;
                }
                var tempPartsStock = partsStocks.FirstOrDefault(r => r.PartId == item.PartsId);
                if(tempPartsStock == null) {
                    tempPartsStock = new PartsStock();
                    tempPartsStock.WarehouseAreaId = warehouseArea.Id;
                    tempPartsStock.WarehouseId = warehouse.Id;
                    tempPartsStock.WarehouseAreaCategoryId = warehouseArea.AreaCategoryId;
                    tempPartsStock.StorageCompanyId = companyTransferOrder.DestCompanyId;
                    tempPartsStock.StorageCompanyType = (int)DcsCompanyType.服务站兼代理库;
                    tempPartsStock.BranchId = partsSalesCategory.BranchId;
                    tempPartsStock.Quantity = partsInboundCheckBillDetail.InspectedQuantity * unitDeleaveQuantity;
                    tempPartsStock.CreatorId = userInfo.Id;
                    tempPartsStock.CreatorName = userInfo.Name;
                    tempPartsStock.CreateTime = DateTime.Now;
                    tempPartsStock.PartId = item.PartsId;
                    InsertToDatabase(tempPartsStock);
                }else {
                    tempPartsStock.Quantity = tempPartsStock.Quantity + partsInboundCheckBillDetail.InspectedQuantity * unitDeleaveQuantity;
                    tempPartsStock.ModifierId = userInfo.Id;
                    tempPartsStock.ModifierName = userInfo.Name;
                    tempPartsStock.ModifyTime = DateTime.Now;
                    UpdateToDatabase(tempPartsStock);
                }

            }
            new PartsInboundCheckBillAch(this.DomainService).InsertPartsInboundCheckBillValidate(partsInboundCheckBill);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:11:45
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 审批服务站间配件调拨单(CompanyTransferOrder companyTransferOrder) {
            new CompanyTransferOrderAch(this).审批服务站间配件调拨单(companyTransferOrder);
        }

        [Update(UsingCustomMethod = true)]
        public void 作废企业间配件调拨单(CompanyTransferOrder companyTransferOrder) {
            new CompanyTransferOrderAch(this).作废企业间配件调拨单(companyTransferOrder);
        }

        [Update(UsingCustomMethod = true)]
        public void 审核服务站兼代理库调拨(CompanyTransferOrder companyTransferOrder) {
            new CompanyTransferOrderAch(this).审核服务站兼代理库调拨(companyTransferOrder);
        }
    }
}
