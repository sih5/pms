﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        [Update(UsingCustomMethod = true)]
        public void 上传服务站盘点单附件(DealerPartsInventoryBill dealerPartsInventoryBill) {
            UpdateToDatabase(dealerPartsInventoryBill);
        }

        [Update(UsingCustomMethod = true)]
        public void 盘点单作废(DealerPartsInventoryBill dealerPartsInventoryBill) {
            var dbDealerPartsInventoryBill = ObjectContext.DealerPartsInventoryBills.Where(r => r.Status == (int)DcsDealerPartsInventoryBillStatus.新建 && r.Id == dealerPartsInventoryBill.Id).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbDealerPartsInventoryBill == null)
                throw new ValidationException(ErrorStrings.DealerPartsInventoryBill_Validation2);
            var userinfo = Utils.GetCurrentUserInfo();
            UpdateToDatabase(dealerPartsInventoryBill);
            dealerPartsInventoryBill.Status = (int)DcsDealerPartsInventoryBillStatus.作废;
            dealerPartsInventoryBill.AbandonerId = userinfo.Id;
            dealerPartsInventoryBill.AbandonerName = userinfo.Name;
            dealerPartsInventoryBill.AbandonTime = System.DateTime.Now;
            UpdateDealerPartsInventoryBillValidate(dealerPartsInventoryBill);
        }

        [Update(UsingCustomMethod = true)]
        public void 驳回服务站配件盘点单(DealerPartsInventoryBill dealerPartsInventoryBill) {
            var dbDealerPartsInventoryBill = ObjectContext.DealerPartsInventoryBills.Where(r => (r.Status == (int)DcsDealerPartsInventoryBillStatus.新建 || 
                r.Status == (int)DcsDealerPartsInventoryBillStatus.初审通过 || r.Status == (int)DcsDealerPartsInventoryBillStatus.审核通过 ||
                r.Status == (int)DcsDealerPartsInventoryBillStatus.审批通过 || r.Status == (int)DcsDealerPartsInventoryBillStatus.高级审核通过) 
                && r.Id == dealerPartsInventoryBill.Id).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbDealerPartsInventoryBill == null)
                throw new ValidationException(ErrorStrings.CompanyTransferOrder_Validation19);
            var userinfo = Utils.GetCurrentUserInfo();
            UpdateToDatabase(dealerPartsInventoryBill);
            dealerPartsInventoryBill.Status = (int)DcsDealerPartsInventoryBillStatus.新建;
            dealerPartsInventoryBill.ApproverId = userinfo.Id;
            dealerPartsInventoryBill.ApproverName = userinfo.Name;
            dealerPartsInventoryBill.ApproveTime = System.DateTime.Now;
            dealerPartsInventoryBill.RejecterId = userinfo.Id;
            dealerPartsInventoryBill.RejecterName = userinfo.Name;
            dealerPartsInventoryBill.RejectTime = DateTime.Now;
            UpdateDealerPartsInventoryBillValidate(dealerPartsInventoryBill);
        }

        [Update(UsingCustomMethod = true)]
        public void 初审服务站配件盘点单(DealerPartsInventoryBill dealerPartsInventoryBill) {
            var dbDealerPartsInventoryBill = ObjectContext.DealerPartsInventoryBills.Where(r => r.Status == (int)DcsDealerPartsInventoryBillStatus.新建 && r.Id == dealerPartsInventoryBill.Id).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbDealerPartsInventoryBill == null)
                throw new ValidationException(ErrorStrings.CompanyTransferOrder_Validation20);
            var userinfo = Utils.GetCurrentUserInfo();
            UpdateToDatabase(dealerPartsInventoryBill);
            var amount = dealerPartsInventoryBill.DealerPartsInventoryDetails.Sum(r => r.DealerPrice * r.StorageDifference);

            var inventoryConfig = ObjectContext.MultiLevelApproveConfigs.FirstOrDefault(r => ((amount >= r.MinApproveFee && amount < r.MaxApproveFee) || amount < 0)
                    && r.Type == (int)DCSMultiLevelApproveConfigType.服务站配件盘点单 && r.ApproverId == userinfo.Id && r.IsFinished && r.Status == (int)DcsBaseDataStatus.有效);
            if(inventoryConfig != null) {
                dealerPartsInventoryBill.Status = (int)DcsDealerPartsInventoryBillStatus.生效;
                FinalSpprovePassedDoBusiness(dealerPartsInventoryBill);
            } else {
                dealerPartsInventoryBill.Status = (int)DcsDealerPartsInventoryBillStatus.初审通过;
            }
            dealerPartsInventoryBill.InitialApproverId = userinfo.Id;
            dealerPartsInventoryBill.InitialApproverName = userinfo.Name;
            dealerPartsInventoryBill.InitialApproveTime = System.DateTime.Now;
            UpdateDealerPartsInventoryBillValidate(dealerPartsInventoryBill);
        }

        [Update(UsingCustomMethod = true)]
        public void 盘点单审核(DealerPartsInventoryBill dealerPartsInventoryBill) {
            var dbDealerPartsInventoryBill = ObjectContext.DealerPartsInventoryBills.Where(r => r.Status == (int)DcsDealerPartsInventoryBillStatus.初审通过 && r.Id == dealerPartsInventoryBill.Id).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbDealerPartsInventoryBill == null)
                throw new ValidationException(ErrorStrings.CompanyTransferOrder_Validation21);
            var userInfo = Utils.GetCurrentUserInfo();
            var amount = dealerPartsInventoryBill.DealerPartsInventoryDetails.Sum(r => r.DealerPrice * r.StorageDifference);

            var inventoryConfig = ObjectContext.MultiLevelApproveConfigs.FirstOrDefault(r => ((amount >= r.MinApproveFee && amount < r.MaxApproveFee) || amount < 0)
                    && r.Type == (int)DCSMultiLevelApproveConfigType.服务站配件盘点单 && r.ApproverId == userInfo.Id && r.IsFinished && r.Status == (int)DcsBaseDataStatus.有效);
            if(inventoryConfig != null) {
                dealerPartsInventoryBill.Status = (int)DcsDealerPartsInventoryBillStatus.生效;
                FinalSpprovePassedDoBusiness(dealerPartsInventoryBill);
            } else {
                dealerPartsInventoryBill.Status = (int)DcsDealerPartsInventoryBillStatus.审核通过;
            }

            dealerPartsInventoryBill.ApproverId = userInfo.Id;
            dealerPartsInventoryBill.ApproverName = userInfo.Name;
            dealerPartsInventoryBill.ApproveTime = DateTime.Now;
            UpdateToDatabase(dealerPartsInventoryBill);

        }

        [Update(UsingCustomMethod = true)]
        public void 终审服务站配件盘点单(DealerPartsInventoryBill dealerPartsInventoryBill) {
            var dbDealerPartsInventoryBill = ObjectContext.DealerPartsInventoryBills.Where(r => r.Status == (int)DcsDealerPartsInventoryBillStatus.审核通过 && r.Id == dealerPartsInventoryBill.Id).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbDealerPartsInventoryBill == null)
                throw new ValidationException(ErrorStrings.CompanyTransferOrder_Validation22);
            var userinfo = Utils.GetCurrentUserInfo();
            UpdateToDatabase(dealerPartsInventoryBill);

            dealerPartsInventoryBill.CheckerId = userinfo.Id;
            dealerPartsInventoryBill.CheckerName = userinfo.Name;
            dealerPartsInventoryBill.CheckTime = System.DateTime.Now;
            var amount = dealerPartsInventoryBill.DealerPartsInventoryDetails.Sum(r => r.DealerPrice * r.StorageDifference);
            var inventoryConfig = ObjectContext.MultiLevelApproveConfigs.FirstOrDefault(r => ((amount >= r.MinApproveFee && amount < r.MaxApproveFee) || amount < 0)
                    && r.Type == (int)DCSMultiLevelApproveConfigType.服务站配件盘点单 && r.ApproverId == userinfo.Id && r.IsFinished && r.Status == (int)DcsBaseDataStatus.有效);
            if(inventoryConfig != null) {
                dealerPartsInventoryBill.Status = (int)DcsDealerPartsInventoryBillStatus.生效;
                FinalSpprovePassedDoBusiness(dealerPartsInventoryBill);
            } else {
                dealerPartsInventoryBill.Status = (int)DcsDealerPartsInventoryBillStatus.审批通过;
            }
            UpdateDealerPartsInventoryBillValidate(dealerPartsInventoryBill);
        }

        [Update(UsingCustomMethod = true)]
        public void 高级审核服务站配件盘点单(DealerPartsInventoryBill dealerPartsInventoryBill) {
            var dbDealerPartsInventoryBill = ObjectContext.DealerPartsInventoryBills.Where(r => r.Status == (int)DcsDealerPartsInventoryBillStatus.审批通过 && r.Id == dealerPartsInventoryBill.Id).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbDealerPartsInventoryBill == null)
                throw new ValidationException(ErrorStrings.CompanyTransferOrder_Validation22);
            var userinfo = Utils.GetCurrentUserInfo();
            UpdateToDatabase(dealerPartsInventoryBill);

            var amount = dealerPartsInventoryBill.DealerPartsInventoryDetails.Sum(r => r.DealerPrice * r.StorageDifference);

            var inventoryConfig = ObjectContext.MultiLevelApproveConfigs.FirstOrDefault(r => ((amount >= r.MinApproveFee && amount < r.MaxApproveFee) || amount < 0)
                    && r.Type == (int)DCSMultiLevelApproveConfigType.服务站配件盘点单 && r.ApproverId == userinfo.Id && r.IsFinished && r.Status == (int)DcsBaseDataStatus.有效);
            if(inventoryConfig != null) {
                dealerPartsInventoryBill.Status = (int)DcsDealerPartsInventoryBillStatus.生效;
                FinalSpprovePassedDoBusiness(dealerPartsInventoryBill);
            } else {
                dealerPartsInventoryBill.Status = (int)DcsDealerPartsInventoryBillStatus.高级审核通过;
            }
            dealerPartsInventoryBill.UpperCheckerId = userinfo.Id;
            dealerPartsInventoryBill.UpperCheckerName = userinfo.Name;
            dealerPartsInventoryBill.UpperCheckTime = System.DateTime.Now;

            //FinalSpprovePassedDoBusiness(dealerPartsInventoryBill);
            UpdateDealerPartsInventoryBillValidate(dealerPartsInventoryBill);
        }

        [Update(UsingCustomMethod = true)]
        public void 高级审批服务站配件盘点单(DealerPartsInventoryBill dealerPartsInventoryBill) {
            var dbDealerPartsInventoryBill = ObjectContext.DealerPartsInventoryBills.Where(r => r.Status == (int)DcsDealerPartsInventoryBillStatus.高级审核通过 && r.Id == dealerPartsInventoryBill.Id).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbDealerPartsInventoryBill == null)
                throw new ValidationException(ErrorStrings.CompanyTransferOrder_Validation22);
            var userinfo = Utils.GetCurrentUserInfo();
            UpdateToDatabase(dealerPartsInventoryBill);
            dealerPartsInventoryBill.Status = (int)DcsDealerPartsInventoryBillStatus.生效;
            dealerPartsInventoryBill.SeniorID = userinfo.Id;
            dealerPartsInventoryBill.SeniorName = userinfo.Name;
            dealerPartsInventoryBill.SeniorTime = System.DateTime.Now;

            FinalSpprovePassedDoBusiness(dealerPartsInventoryBill);
            UpdateDealerPartsInventoryBillValidate(dealerPartsInventoryBill);
        }

        private void FinalSpprovePassedDoBusiness(DealerPartsInventoryBill dealerPartsInventoryBill) {
            var dealerPartsInventoryDetails = dealerPartsInventoryBill.DealerPartsInventoryDetails.ToArray();
            var partsIds = dealerPartsInventoryDetails.Select(r => r.SparePartId).ToArray();
            var dealerPartsStocks = ObjectContext.DealerPartsStocks.Where(r => r.DealerId == dealerPartsInventoryBill.StorageCompanyId && r.SalesCategoryId == dealerPartsInventoryBill.SalesCategoryId && partsIds.Contains(r.SparePartId) && r.SubDealerId == -1).ToArray();
            var salesCategory = ObjectContext.PartsSalesCategories.SingleOrDefault(r => r.Id == dealerPartsInventoryBill.SalesCategoryId && r.Status == (int)DcsBaseDataStatus.有效);
            if(salesCategory == null) {
                throw new ValidationException(ErrorStrings.CompanyTransferOrder_Validation16);
            }
            foreach(var dealerPartsInventoryDetail in dealerPartsInventoryDetails) {
                var dealerPartsStock = dealerPartsStocks.FirstOrDefault(r => r.SparePartId == dealerPartsInventoryDetail.SparePartId);
                //var dbPartDeleaveInformation = ObjectContext.PartDeleaveInformations.SingleOrDefault(c => c.OldPartId == dealerPartsInventoryDetail.SparePartId && c.PartsSalesCategoryId == dealerPartsInventoryBill.SalesCategoryId && c.BranchId == dealerPartsInventoryBill.BranchId && c.Status == (int)DcsBaseDataStatus.有效);
                if(dealerPartsStock == null) {
                    var newDealerPartsStock = new DealerPartsStock {
                        DealerId = dealerPartsInventoryBill.StorageCompanyId,
                        DealerCode = dealerPartsInventoryBill.StorageCompanyCode,
                        DealerName = dealerPartsInventoryBill.StorageCompanyName,
                        BranchId = dealerPartsInventoryBill.BranchId,
                        SalesCategoryId = dealerPartsInventoryBill.SalesCategoryId,
                        SalesCategoryName = salesCategory.Name,
                        SubDealerId = -1,
                        SparePartId = dealerPartsInventoryDetail.SparePartId,
                        SparePartCode = dealerPartsInventoryDetail.SparePartCode,
                        //Quantity = dbPartDeleaveInformation == null ? dealerPartsInventoryDetail.StorageAfterInventory : dealerPartsInventoryDetail.StorageAfterInventory * dbPartDeleaveInformation.DeleaveAmount
                        Quantity = dealerPartsInventoryDetail.StorageAfterInventory
                    };
                    生成经销商配件库存(newDealerPartsStock);
                } else {

                    //if(dbPartDeleaveInformation != null) {
                    //    if(dealerPartsStock.Quantity < -dealerPartsInventoryDetail.StorageDifference * dbPartDeleaveInformation.DeleaveAmount) {
                    //        throw new ValidationException(ErrorStrings.DealerPartsInventoryBill_Validation4);
                    //    }
                    //    dealerPartsStock.Quantity = dealerPartsStock.Quantity + dealerPartsInventoryDetail.StorageDifference * dbPartDeleaveInformation.DeleaveAmount;
                    //} else {
                    if(dealerPartsStock.Quantity < -dealerPartsInventoryDetail.StorageDifference) {
                        throw new ValidationException(ErrorStrings.DealerPartsInventoryBill_Validation4);
                    }
                    dealerPartsStock.Quantity = dealerPartsStock.Quantity + dealerPartsInventoryDetail.StorageDifference;
                    //}
                    UpdateDealerPartsStock(dealerPartsStock);
                }
            }
        }


    }
}
