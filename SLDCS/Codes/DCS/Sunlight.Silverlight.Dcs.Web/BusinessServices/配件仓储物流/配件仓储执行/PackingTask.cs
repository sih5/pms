﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
using System.Configuration;
using System.Data;
using System.Text;
using Devart.Data.Oracle;
using NPOI.SS.Formula.Functions;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using System.Transactions;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class PackingTaskAch : DcsSerivceAchieveBase {
        public PackingTaskAch(DcsDomainService domainService)
            : base(domainService) {
        }
        public IEnumerable<PackingTaskQuery> GetPackingTaskLists(string code, string sourceCode, string partsInboundPlanCode, string partsInboundCheckBillCode,
         int? status, int? warehouseId, DateTime? bCreateTime, DateTime? eCreateTime, DateTime? bModifyTime, DateTime? eModifyTime, string sparePartCode, string sparePartName, int? inboundType, string modifierName, bool? hasDifference) {
          
            var userInfo = Utils.GetCurrentUserInfo();
            if(userInfo.Id==0){
                throw new Exception(ErrorStrings.User_Invalid);
            }
            string SQL = @"select *
  from (select p.ID,
               p.Code,
               p.PartsInboundPlanCode,
               p.PartsInboundCheckBillCode,
               p.SparePartCode,
               p.SparePartName,
               p.WarehouseCode,
               p.WarehouseName,
               p.PlanQty,
               p.PackingQty,
               p.SourceCode,
               p.Status,
               p.CounterpartCompanyCode,
               p.CounterpartCompanyName,
               p.CreatorName,
               p.CreateTime,
               p.PartsInboundPlanId,
               p.PartsInboundCheckBillId,
               p.SparePartId,
               p.BatchNumber,
               p.ModifierName,
               p.ModifyTime,
               (select PackingMaterial
                  from PartsBranchPackingProp s
                  join partsbranch b
                    on s.partsbranchid = b.id
                   and b.status = 1
                 where s.SparePartId = p.sparepartid
                   and PackingType =
                       (select max(PackingType)
                          from PartsBranchPackingProp d
                          join partsbranch w
                            on d.partsbranchid = w.id
                           and w.status = 1
                         where d.SparePartId = p.sparepartid)) as PackingMaterial,
               (select MAX(ReferenceCode)
                  from sparepart
                 where code =
                       (select PackingMaterial
                          from PartsBranchPackingProp s
                          join partsbranch b
                            on s.partsbranchid = b.id
                           and b.status = 1
                         where s.SparePartId = p.sparepartid
                           and PackingType =
                               (select max(PackingType)
                                  from PartsBranchPackingProp d
                                  join partsbranch w
                                    on d.partsbranchid = w.id
                                   and w.status = 1
                                 where d.SparePartId = p.sparepartid))) as ReferenceCode,
               ceil(PlanQty /
                    (select PackingCoefficient
                       from PartsBranchPackingProp s
                       join partsbranch b
                         on s.partsbranchid = b.id
                        and b.status = 1
                      where s.SparePartId = p.sparepartid
                        and PackingType =
                            (select max(PackingType)
                               from PartsBranchPackingProp d
                               join partsbranch w
                                 on d.partsbranchid = w.id
                                and w.status = 1
                              where d.SparePartId = p.sparepartid))) as PackNum,
               pp.inboundtype,
               yy.WarehouseAreaCode,sp.TraceProperty,
                (select count(1) from PartsDifferenceBackBill d inner join PartsDifferenceBackBillDtl dd on d.Id=dd.PartsDifferenceBackBillId where d.type =2 and d.status<>99 and rownum=1 and dd.TaskId=p.Id) as HasDifference 
          from PackingTask p
            join SparePart sp on p.sparepartid=sp.id
          join PartsInboundPlan pp
            on p.partsinboundplanid = pp.id
            left  join (select WarehouseArea.code as WarehouseAreaCode,
                           Warehouse.id as warehouseid,
                           partsstock.partid as partid,
                           row_number() over(partition by Warehouse.code, partsstock.partid order by WarehouseArea.Id) my_rank
                      from WarehouseArea
                     inner join Warehouse
                        on WarehouseArea.WarehouseId = Warehouse.Id
                       and warehouse.storagecompanytype = 1
                     inner join WarehouseAreaCategory
                        on WarehouseArea.AreaCategoryId =
                           WarehouseAreaCategory.Id
                       AND WarehouseAreaCategory.Category = 1                   
                     inner join partsstock
                        on partsstock.warehouseareaid = warehousearea.id) yy
                          on yy.warehouseid =  p.warehouseid
           and yy.partid = p.sparepartid and my_rank = 1
         where 1 = 1 ";
            if(hasDifference == true) {
                SQL = SQL + " and exists (select 1 from PartsDifferenceBackBill d inner join PartsDifferenceBackBillDtl dd on d.Id=dd.PartsDifferenceBackBillId where d.type =2 and d.status<>99 and rownum=1 and dd.TaskId=p.Id)";
            }
            if(hasDifference == false) {
                SQL = SQL + " and not exists (select 1 from PartsDifferenceBackBill d inner join PartsDifferenceBackBillDtl dd on d.Id=dd.PartsDifferenceBackBillId where d.type =2 and d.status<>99 and rownum=1 and dd.TaskId=p.Id)";
            }
            if(!string.IsNullOrEmpty(code)) {
                SQL = SQL + " and LOWER(p.code) like '%" + code.ToLower() + "%'";
            }
            if(!string.IsNullOrEmpty(partsInboundPlanCode)) {
                SQL = SQL + " and LOWER(p.partsInboundPlanCode) like '%" + partsInboundPlanCode.ToLower() + "%'";
            }
            if(!string.IsNullOrEmpty(partsInboundCheckBillCode)) {
                SQL = SQL + " and LOWER(p.partsInboundCheckBillCode) like '%" + partsInboundCheckBillCode.ToLower() + "%'";
            }
            if(!string.IsNullOrEmpty(sparePartCode)) {
                SQL = SQL + " and LOWER(p.SparePartCode) like '%" + sparePartCode.ToLower() + "%'";
            }
            if(!string.IsNullOrEmpty(sparePartName)) {
                SQL = SQL + " and LOWER(p.SparePartName) like '%" + sparePartName.ToLower() + "%'";
            }
            if(status.HasValue) {
                SQL = SQL + " and p.status=" + status.Value;
            }
            if(warehouseId.HasValue) {
                SQL = SQL + " and p.warehouseId=" + warehouseId.Value;
            }
            if(inboundType.HasValue) {
                SQL = SQL + " and pp.inboundType=" + inboundType.Value;
            }
            if(bCreateTime.HasValue) {
                SQL = SQL + " and p.CreateTime>= to_date('" + bCreateTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }
            if(eCreateTime.HasValue) {
                SQL = SQL + " and p.CreateTime<= to_date('" + eCreateTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }

            if(bModifyTime.HasValue) {
                SQL = SQL + " and p.ModifyTime>= to_date('" + bModifyTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }
            if(eModifyTime.HasValue) {
                SQL = SQL + " and p.ModifyTime<= to_date('" + eModifyTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }
            if(!string.IsNullOrEmpty(modifierName)) {
                SQL = SQL + " and LOWER(p.ModifierName) like '%" + modifierName.ToLower() + "%'";
            }
            SQL = SQL + " ) tt";
            if(!string.IsNullOrEmpty(sourceCode)) {
                SQL = SQL + " where tt.ReferenceCode like '%" + sourceCode + "%'";
            }
            var search = ObjectContext.ExecuteStoreQuery<PackingTaskQuery>(SQL).ToList();
            return search;
        }
        public IEnumerable<PackingTask> GetSparePartTrace(int? packingTaskId, string partsInboundCheckBillCode, string sparePartCode) {
            string SQL = @"select (case
                             when sp.traceproperty = 2 then
                              (select ate.tracecode
                                 from AccurateTrace ate
                                where ate.sourcebillid = pk.partsinboundcheckbillid
                                  and ate.partid = pk.sparepartid
                                  and ate.status = 1)
                             else
                              cast('' as varchar2(50))
                           end) as TraceCode,
                          sp. as TracePropertyInt,
                           kv.value as TraceProperty,pk.*
                      from PackingTask pk
                      join sparepart sp
                        on pk.sparepartid = sp.id
                      left join keyvalueitem kv
                        on sp.traceproperty = kv.key
                       and kv.name = 'TraceProperty'
                       where 1=1";
            if(packingTaskId.HasValue) {
                SQL = SQL + " and pk.id=" + packingTaskId;
            }
            if(!string.IsNullOrEmpty(partsInboundCheckBillCode)) {
                SQL = SQL + " and pk.partsInboundCheckBillCode=" + partsInboundCheckBillCode;
            }
            if(!string.IsNullOrEmpty(sparePartCode)) {
                SQL = SQL + " and pk.sparePartCode=" + sparePartCode;
            }
            var search = ObjectContext.ExecuteStoreQuery<PackingTask>(SQL).ToList();
            return search;
        }
        //包装登记
        public void 包装登记(PackingTask packingTask) {
            if(packingTask.ScanNumber<=0) {
                return;
            }
            using(var transaction = new TransactionScope()) {
            var userInfo = Utils.GetCurrentUserInfo();
            if(string.IsNullOrEmpty(userInfo.Name))
                throw new HttpException(401, ErrorStrings.User_Invalid);
            //生成包装任务单清单
            this.insertPackingTaskDetail(packingTask);
            int scanner = packingTask.ScanNumber;
            //D、更新配件检验区锁定库存            
            var partsStocks = (from a in ObjectContext.PartsStocks
                               join b in ObjectContext.WarehouseAreaCategories on a.WarehouseAreaCategoryId equals b.Id
                               where a.PartId == packingTask.SparePartId && b.Category == 3 && a.WarehouseId == packingTask.WarehouseId && a.LockedQty >= scanner
                               select new {
                                   Id = a.Id
                               }).ToArray();
            if(partsStocks.Length == 0)
                throw new ValidationException(ErrorStrings.PackingTask_Validation1);
            int id = partsStocks.FirstOrDefault().Id;
            var parts = new PartsStockAch(this.DomainService).GetPartsStocksWithLock(ObjectContext.PartsStocks.Where(r => r.Id == id)).FirstOrDefault();


            if(parts.LockedQty == null || packingTask.ScanNumber > parts.LockedQty) {
                throw new ValidationException(ErrorStrings.PackingTask_Validation2);
            }
            parts.LockedQty = parts.LockedQty - packingTask.ScanNumber;
            parts.ModifierId = userInfo.Id;
            parts.ModifierName = userInfo.Name;
            parts.ModifyTime = DateTime.Now;
            UpdateToDatabase(parts);

            //F、生成上架任务单
            this.insertPartsShelvesTask(packingTask);
            //生成入库实绩
            this.insertPartsInboundPerformance(packingTask);
            //更新包装任务单
            var oldPacking = ObjectContext.PackingTasks.Where(r => r.Id == packingTask.Id).FirstOrDefault();
            if(null == oldPacking.PackingQty) {
                oldPacking.PackingQty = 0;
            }
            oldPacking.PackingQty = oldPacking.PackingQty + packingTask.ScanNumber;
            if(oldPacking.PackingQty > oldPacking.PlanQty) {
                throw new ValidationException(ErrorStrings.PickingTask_Validation1);
            }
            if(oldPacking.PackingQty == oldPacking.PlanQty) {
                oldPacking.Status = (int)DcsPackingTaskStatus.包装完成;
            } else {
                oldPacking.Status = (int)DcsPackingTaskStatus.部分包装;
            }
            oldPacking.ModifierId = userInfo.Id;
            oldPacking.ModifierName = userInfo.Name;
            oldPacking.ModifyTime = DateTime.Now;
            UpdateToDatabase(oldPacking);
           
            // E、更新入库计划单状态 //调用公共方法
            var partsInboundPlan = ObjectContext.PartsInboundPlans.FirstOrDefault(r => r.Id == packingTask.PartsInboundPlanId);
            if(null == partsInboundPlan) {
                throw new ValidationException(ErrorStrings.PickingTask_Validation2);
            }
            if(packingTask.BoxCode==null) {
                packingTask.BoxCode = "";
             }
            //追溯码相关
            var sihCodes = packingTask.SIHCode.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries).ToArray();
            var traceCodes = packingTask.TraceCode.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries).ToArray();
            var boxCodes = packingTask.BoxCode.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries).ToArray();
            var traces = ObjectContext.AccurateTraces.Where(t => t.Status == (int)DCSAccurateTraceStatus.有效 && t.Type==(int)DCSAccurateTraceType.入库 && t.CompanyId==userInfo.EnterpriseId).ToArray();
           //若存在已终止的入库计划单，则状态改为不考核
            var abandAtrace = ObjectContext.AccurateTraces.Where(t =>t.SourceBillId!=oldPacking.PartsInboundPlanId && t.Status == (int)DCSAccurateTraceStatus.有效 && t.Type == (int)DCSAccurateTraceType.入库 && t.CompanyId == userInfo.EnterpriseId && t.SIHLabelCode == null && ObjectContext.PartsInboundPlans.Any(e => e.Id == t.SourceBillId && e.Status == (int)DcsPartsInboundPlanStatus.终止 && ObjectContext.PartsInboundPlanDetails.Any(y => y.PartsInboundPlanId == e.Id && y.SparePartId == t.PartId))).ToArray();
            //作废的追溯
            var abandTrace = ObjectContext.AccurateTraces.Where(t => t.Status == (int)DCSAccurateTraceStatus.有效 && t.SIHLabelCode != "" && t.CompanyId==userInfo.EnterpriseId && t.Type==(int)DCSAccurateTraceType.入库 && (t.OutQty!=null && t.InQty==t.OutQty) && t.SourceBillId!=oldPacking.PartsInboundPlanId).ToArray();
            if(boxCodes.Count()>0&&sihCodes.Count() > 0 && traceCodes.Count() > 0 && packingTask.TracePropertyInt.HasValue && traces.Count() > 0) {
               
                if(packingTask.TracePropertyInt == (int)DCSTraceProperty.精确追溯) {
                    for(int i = 0; i < sihCodes.Count(); i++) {
                        var abandAccutraces = abandAtrace.Where(t => t.TraceCode == traceCodes[i]).ToArray();
                        foreach(var aband in abandAccutraces) {
                            aband.ModifierId = userInfo.Id;
                            aband.ModifierName = userInfo.Name;
                            aband.ModifyTime = DateTime.Now;
                            aband.Status = (int)DCSAccurateTraceStatus.不考核;
                            UpdateToDatabase(aband);
                        }
                        var sih = traces.Where(t => t.SIHLabelCode == sihCodes[i] && t.TraceCode==traceCodes[i] && t.PackingTaskId ==packingTask.Id ).FirstOrDefault();
                        if(sih!=null) {
                            throw new ValidationException("SIH件码:" + sihCodes[i] + "追溯码："+traceCodes[i]+"已包装完成");
                        }
                        var trace = traces.Where(t => t.TraceCode == traceCodes[i] && t.SIHLabelCode == null && t.SourceBillId == oldPacking.PartsInboundPlanId).FirstOrDefault();
                        if(trace == null  ) {
                            if(oldPacking.CounterpartCompanyCode.Equals("1000003829") ||oldPacking.CounterpartCompanyCode.Equals("1000002367")) {
                                //	当配件供应商为SIH、车桥公司时
                                //新增入库类型的【精确追溯】信息，不需要校验精确码信息，自动绑定“备件编号”+“SIH防伪标签码”和“精确码”
                                var newAccurateTrace = new AccurateTrace {
                                    Type = (int)DCSAccurateTraceType.入库,
                                    CompanyType = (int)DCSAccurateTraceCompanyType.配件中心,
                                    CompanyId=userInfo.EnterpriseId,
                                    SourceBillId = oldPacking.PartsInboundPlanId,
                                    PartId=oldPacking.SparePartId,
                                    TraceCode = traceCodes[i],
                                    SIHLabelCode = sihCodes[i],
                                    Status = (int)DCSAccurateTraceStatus.有效,
                                    TraceProperty=packingTask.TracePropertyInt,
                                    BoxCode = boxCodes[i],
                                  //  InQty=1,
                                    PackingTaskId=packingTask.Id

                                };
                                new AccurateTraceAch(this.DomainService).ValidataAccurateTrace(newAccurateTrace);
                                continue;
                            } else
                                throw new ValidationException("未找到有效的追溯码:" + traceCodes[i]);
                           
                        }
                        trace.SIHLabelCode = sihCodes[i];
                        trace.BoxCode = boxCodes[i];
                       // trace.InQty = 1;
                        trace.ModifierId = userInfo.Id;
                        trace.ModifierName = userInfo.Name;
                        trace.ModifyTime = DateTime.Now;
                        trace.PackingTaskId = packingTask.Id;
                        UpdateToDatabase(trace);
                        var cancel = abandTrace.Where(t => t.TraceCode == traceCodes[i]).FirstOrDefault();
                        if(cancel!=null) {
                            cancel.ModifierId = userInfo.Id;
                            cancel.ModifierName = userInfo.Name;
                            cancel.ModifyTime = DateTime.Now;
                            cancel.Status = (int)DCSAccurateTraceStatus.作废;
                            UpdateToDatabase(cancel);
                        }
                    }
                } else {
                    var abandAccutraces = abandAtrace.Where(t => t.TraceCode == traceCodes[0]).ToArray();
                    foreach(var aband in abandAccutraces) {
                        aband.ModifierId = userInfo.Id;
                        aband.ModifierName = userInfo.Name;
                        aband.ModifyTime = DateTime.Now;
                        aband.Status = (int)DCSAccurateTraceStatus.不考核;
                        UpdateToDatabase(aband);
                    }
                    var trace = traces.Where(t => t.SIHLabelCode == sihCodes[0] && t.BoxCode == boxCodes[0] && t.SourceBillId == oldPacking.PartsInboundPlanId).FirstOrDefault();
                    if(trace != null) {
                        if(trace.SIHLabelCode==null) {
                            trace.SIHLabelCode = sihCodes[0];
                            trace.BoxCode = boxCodes[0];
                        }
                     //   trace.InQty = (trace.InQty==null ? 0 : trace.InQty.Value) + packingTask.ScanNumber;
                        trace.ModifierId = userInfo.Id;
                        trace.ModifierName = userInfo.Name;
                        trace.ModifyTime = DateTime.Now;
                        trace.PackingTaskId = packingTask.Id;
                        UpdateToDatabase(trace);
                        var cancel = abandTrace.Where(t => t.TraceCode == traceCodes[0]).FirstOrDefault();
                        if(cancel != null) {
                            cancel.ModifierId = userInfo.Id;
                            cancel.ModifierName = userInfo.Name;
                            cancel.ModifyTime = DateTime.Now;
                            cancel.Status = (int)DCSAccurateTraceStatus.作废;
                            UpdateToDatabase(cancel);
                        }
                    } else if(oldPacking.CounterpartCompanyCode.Equals("1000003829") || oldPacking.CounterpartCompanyCode.Equals("1000002367")) {
                        //	当配件供应商为SIH、车桥公司时
                        //新增入库类型的【精确追溯】信息，不需要校验精确码信息，自动绑定“备件编号”+“SIH防伪标签码”和“精确码”
                        var newAccurateTrace = new AccurateTrace {
                            Type = (int)DCSAccurateTraceType.入库,
                            CompanyType = (int)DCSAccurateTraceCompanyType.配件中心,
                            CompanyId = userInfo.EnterpriseId,
                            SourceBillId = oldPacking.PartsInboundPlanId,
                            PartId = oldPacking.SparePartId,
                            TraceCode = traceCodes[0],
                            SIHLabelCode = sihCodes[0],
                            Status = (int)DCSAccurateTraceStatus.有效,
                            TraceProperty = packingTask.TracePropertyInt,
                            BoxCode = boxCodes[0],
                          //  InQty = packingTask.ScanNumber,
                            PackingTaskId=packingTask.Id

                        };
                        new AccurateTraceAch(this.DomainService).ValidataAccurateTrace(newAccurateTrace);
                    }
                    
                }
            } else if(boxCodes.Count() > 0 && sihCodes.Count() > 0 && (traceCodes.Count() == 0 || !packingTask.TracePropertyInt.HasValue)) {
               //有SIH标签码，但没有追溯码
                for(int i = 0; i < sihCodes.Count(); i++) {
                    //if(traces.Any(t=>t.SIHLabelCode==sihCodes[i] && t.SourceBillId==packingTask.PartsInboundPlanId)) {
                    //    throw new ValidationException("SIH标签码:" + sihCodes[i]+"已包装完成");
                    //}
                    var oldTrace = traces.Where(t =>t.BoxCode==boxCodes[i]&& t.SIHLabelCode == sihCodes[i] && t.SourceBillId == packingTask.PartsInboundPlanId && t.PackingTaskId==packingTask.Id).FirstOrDefault();
                    if(oldTrace!=null) {
                       // oldTrace.InQty = oldTrace.InQty + packingTask.ScanNumber;
                        oldTrace.ModifierId = userInfo.Id;
                        oldTrace.ModifierName = userInfo.Name;
                        oldTrace.ModifyTime = DateTime.Now;
                        UpdateToDatabase(oldTrace);
                    } else {
                        var newAccurateTrace = new AccurateTrace {
                            Type = (int)DCSAccurateTraceType.入库,
                            CompanyType = (int)DCSAccurateTraceCompanyType.配件中心,
                            CompanyId = userInfo.EnterpriseId,
                            SourceBillId = oldPacking.PartsInboundPlanId,
                            PartId = oldPacking.SparePartId,
                            SIHLabelCode = sihCodes[i],
                            Status = (int)DCSAccurateTraceStatus.有效,
                            TraceProperty = packingTask.TracePropertyInt,
                            BoxCode = boxCodes[i],
                          //  InQty = packingTask.ScanNumber,
                            PackingTaskId = packingTask.Id
                        };
                       
                        new AccurateTraceAch(this.DomainService).ValidataAccurateTrace(newAccurateTrace);
                    }
                }
            }
            ObjectContext.SaveChanges();
            //若包装单状态为已包装，且配件供应商为SIH、车桥公司时，更新原 入库追溯信息为不考核
            if(oldPacking.Status == (int)DcsPackingTaskStatus.包装完成 && (oldPacking.CounterpartCompanyCode.Equals("1000003829") || oldPacking.CounterpartCompanyCode.Equals("1000002367"))) {
                var tracest = ObjectContext.AccurateTraces.Where(t => t.CompanyId == userInfo.EnterpriseId && t.SourceBillId == oldPacking.PartsInboundPlanId && t.SIHLabelCode == null).ToArray();
                if(tracest.Length > 0) {
                    foreach(var item in tracest) {
                        item.Status = (int)DCSAccurateTraceStatus.SIH车桥不考核;
                        item.ModifierId = userInfo.Id;
                        item.ModifierName = userInfo.Name;
                        item.ModifyTime = DateTime.Now;
                        UpdateToDatabase(item);
                    }
                }
            }
            new PartsInboundPlanAch(this.DomainService).更新入库计划状态(partsInboundPlan);
            ObjectContext.SaveChanges();
            transaction.Complete();
            }
        }
        private void insertPackingTaskDetail(PackingTask packingTask) {

            string recomPackingMaterial = packingTask.FirstPackingMaterial;
            int recomQty = packingTask.FirstNumber;
            string phyPackingMaterial = packingTask.FirstPackingMaterialSj;
            int phyQty = packingTask.FirstNumberSj;
            if(packingTask.SecNumberSj > 0) {
                recomPackingMaterial = packingTask.SecPackingMaterial;
                recomQty = packingTask.SecNumber;
                phyPackingMaterial = packingTask.SecPackingMaterialSj;
                phyQty = packingTask.SecNumberSj;
            } else if(packingTask.ThidNumberSj > 0) {
                recomPackingMaterial = packingTask.ThidPackingMaterial;
                recomQty = packingTask.ThidNumber;
                phyPackingMaterial = packingTask.ThidPackingMaterialSj;
                phyQty = packingTask.ThidNumberSj;

            }
            var packingTaskDetail = new PackingTaskDetail {
                PackingTaskId = packingTask.Id,
                RecomPackingMaterial = recomPackingMaterial,
                RecomQty = recomQty,
                PhyPackingMaterial = phyPackingMaterial,
                PhyQty = phyQty
            };
            InsertToDatabase(packingTaskDetail);

        }
        private void insertPartsInboundPerformance(PackingTask packingTask) {

            int containerNum = (int)(Math.Ceiling(Double.Parse(packingTask.ScanNumber.ToString()) / Double.Parse(packingTask.PackingNumber.ToString())));
            var userInfo = Utils.GetCurrentUserInfo();
            var partsInboundPerformance = new PartsInboundPerformance {
                InType = (int)DcsPartsInboundPer_InType.包装,
                BarCode = packingTask.ProductCode,
                Equipment = (int)DcsPartsInboundPer_EquipmentType.PC,
                WarehouseAreaId = packingTask.WarehouseId,
                ContainerNum = containerNum,
                Qty = packingTask.ScanNumber,
                OperaterId = userInfo.Id,
                OperaterName = userInfo.Name,
                OperaterEndTime = DateTime.Now,
                Code = packingTask.Code,
                SparePartId = packingTask.SparePartId,
                SpareCode = packingTask.SparePartCode,
                SpareName = packingTask.SparePartName
            };
            InsertToDatabase(partsInboundPerformance);
        }
        //新增上架任务单
        private void insertPartsShelvesTask(PackingTask packingTask) {
            var userInfo = Utils.GetCurrentUserInfo();
            var partsShelvesTask = new PartsShelvesTask {
                PackingTaskId = packingTask.Id,
                PackingTaskCode = packingTask.Code,
                PartsInboundPlanId = packingTask.PartsInboundPlanId,
                PartsInboundPlanCode = packingTask.PartsInboundPlanCode,
                PartsInboundCheckBillId = packingTask.PartsInboundCheckBillId,
                PartsInboundCheckBillCode = packingTask.PartsInboundCheckBillCode,
                WarehouseId = packingTask.WarehouseId,
                WarehouseCode = packingTask.WarehouseCode,
                WarehouseName = packingTask.WarehouseName,
                SparePartId = packingTask.SparePartId,
                PlannedAmount = packingTask.ScanNumber,
                SourceBillCode = packingTask.SourceCode,
                CreatorId = userInfo.Id,
                CreatorName = userInfo.Name,
                CreateTime = DateTime.Now,
                Status = (int)DcsShelvesTaskStatus.新增,
                BatchNumber = packingTask.BatchNumber

            };
            //获取编号    
            partsShelvesTask.Code = CodeGenerator.Generate("PartsShelvesTask", userInfo.EnterpriseCode);
            InsertToDatabase(partsShelvesTask);
        }

        public IQueryable<PackingTaskDetail> GetPackingTaskDetailLists() {
            return ObjectContext.PackingTaskDetails.OrderBy(r => r.Id);
        }

        public IQueryable<SupplierTraceCode> GetPackingTaskTrace(int? packingTaskId) {
            var returnItem = from a in ObjectContext.SupplierTraceCodes.Where(t => t.Status != (int)DCSSupplierTraceCodeStatus.已作废 && t.Status != (int)DCSSupplierTraceCodeStatus.已审批)
                             join b in ObjectContext.SupplierTraceCodeDetails on a.Id equals b.SupplierTraceCodeId
                             join c in ObjectContext.PartsPurchaseOrders on b.PartsPurchaseOrderId.Value equals c.Id
                             join d in ObjectContext.PartsInboundPlans.Where(t => t.OriginalRequirementBillType == (int)DcsOriginalRequirementBillType.配件采购订单) on c.Id equals d.OriginalRequirementBillId
                             join e in ObjectContext.PackingTasks.Where(t => t.Id == packingTaskId) on new {
                                 d.Id,
                                 b.SparePartId
                             } equals new {
                                 Id=e.PartsInboundPlanId.Value,
                                 e.SparePartId
                             }
                             join g in ObjectContext.AccurateTraces on new {
                                 e.SparePartId,
                                 d.Id
                             } equals new {
                                 SparePartId=g.PartId,
                                 Id=g.SourceBillId.Value
                             }
                             select a;
            return returnItem;
        }
    }
    partial class DcsDomainService {
        public IEnumerable<PackingTaskQuery> GetPackingTaskLists(string code, string sourceCode, string partsInboundPlanCode, string partsInboundCheckBillCode,
        int? status, int? warehouseId, DateTime? bCreateTime, DateTime? eCreateTime, DateTime? bModifyTime, DateTime? eModifyTime, string sparePartCode, string sparePartName, int? inboundType, string modifierName, bool? hasDifference) {
            return new PackingTaskAch(this).GetPackingTaskLists(code, sourceCode, partsInboundPlanCode, partsInboundCheckBillCode, status, warehouseId, bCreateTime, eCreateTime, bModifyTime, eModifyTime, sparePartCode, sparePartName, inboundType, modifierName, hasDifference);
        }
        [Update(UsingCustomMethod = true)]
        public void 包装登记(PackingTask packingTask) {
            new PackingTaskAch(this).包装登记(packingTask);
        }
        public IQueryable<PackingTaskDetail> GetPackingTaskDetailLists() {
            return new PackingTaskAch(this).GetPackingTaskDetailLists();
        }
        public IEnumerable<PackingTask> GetSparePartTrace(int? packingTaskId, string partsInboundCheckBillCode, string sparePartCode) {
            return new PackingTaskAch(this).GetSparePartTrace(packingTaskId, partsInboundCheckBillCode, sparePartCode);
        }
        public IQueryable<SupplierTraceCode> GetPackingTaskTrace(int? packingTaskId) {
            return new PackingTaskAch(this).GetPackingTaskTrace(packingTaskId);
        }
    }
}
