﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsDifferenceBackBillAch : DcsSerivceAchieveBase {
        public PartsDifferenceBackBillAch(DcsDomainService domainService)
            : base(domainService) {
        }

        public void 作废配件差异回退单(PartsDifferenceBackBill input) {
            if(ObjectContext.PartsDifferenceBackBills.Any(r => r.Id == input.Id && r.Status != (int)DCSPartsDifferenceBackBillStatus.新增))
                throw new ValidationException(ErrorStrings.PartsDifferenceBackBill_Validation1);

            var user = Utils.GetCurrentUserInfo();
            input.Status = (int)DCSPartsDifferenceBackBillStatus.作废;
            input.AbandonerId = user.Id;
            input.AbandonerName = user.Name;
            input.AbandonTime = DateTime.Now;
            UpdateToDatabase(input);
            this.UpdatePartsDifferenceBackBillValidate(input);

            if(input.Type != (int)DCSPartsDifferenceBackBillType.入库) {
                var partsInboundCheckBill = ObjectContext.PartsInboundCheckBills.First(o => o.Code == input.PartsInboundCheckBillCode);
                partsInboundCheckBill.IsDifference = false;
            }
            this.ObjectContext.SaveChanges();
        }


        public void 提交配件差异回退单(PartsDifferenceBackBill input) {
            if(ObjectContext.PartsDifferenceBackBills.Any(r => r.Id == input.Id && r.Status != (int)DCSPartsDifferenceBackBillStatus.新增))
                throw new ValidationException(ErrorStrings.PartsDifferenceBackBill_Validation2);

            var user = Utils.GetCurrentUserInfo();
            input.Status = (int)DCSPartsDifferenceBackBillStatus.提交;
            input.SubmitterId = user.Id;
            input.SubmitterName = user.Name;
            input.SubmitTime = DateTime.Now;
            UpdateToDatabase(input);
            this.UpdatePartsDifferenceBackBillValidate(input);
            this.ObjectContext.SaveChanges();
        }

        public void 审核配件差异回退单(int id, int status) {
            var createTime = DateTime.Now.AddDays(1 - DateTime.Now.Day).Date;
            var partsDifferenceBackBill = ObjectContext.PartsDifferenceBackBills.Include("PartsDifferenceBackBillDtls").FirstOrDefault(o => o.Id == id && o.Status == status);
            if(partsDifferenceBackBill == null)
                throw new ValidationException(ErrorStrings.PartsDifferenceBackBill_Validation3);
            // 更新入库计划单状态 //调用公共方法
            var partsInboundPlan = ObjectContext.PartsInboundPlans.Include("PartsInboundPlanDetails").FirstOrDefault(r => r.OriginalRequirementBillCode == partsDifferenceBackBill.SourceCode
                && (r.Status != (int)DcsPartsInboundPlanStatus.取消 && r.Status != (int)DcsPartsInboundPlanStatus.强制检验完成));
            if(null == partsInboundPlan) {
                throw new ValidationException(ErrorStrings.PickingTask_Validation2);
            }
            if(partsDifferenceBackBill.Type != (int)DCSPartsDifferenceBackBillType.入库) {
                if(ObjectContext.PartsInboundCheckBills.Any(o => o.Code == partsDifferenceBackBill.PartsInboundCheckBillCode && o.CreateTime < createTime)) {
                    throw new ValidationException(partsDifferenceBackBill.PartsInboundCheckBillCode + "入库单已跨月或者跨年，不允许回退");
                }
            }

            switch(partsDifferenceBackBill.Status) {
                case (int)DCSPartsDifferenceBackBillStatus.提交:
                    this.InitialApprove(partsDifferenceBackBill);
                    break;
                case (int)DCSPartsDifferenceBackBillStatus.初审通过:
                    this.Audit(partsDifferenceBackBill, partsInboundPlan);
                    break;
                case (int)DCSPartsDifferenceBackBillStatus.审核通过:
                    //审核不在重新修改入库计划单状态
                    this.Approve(partsDifferenceBackBill, partsInboundPlan);
                    ObjectContext.SaveChanges();
                    return;
            }
            ObjectContext.SaveChanges();
            new PartsInboundPlanAch(this.DomainService).更新入库计划状态(partsInboundPlan);
            ObjectContext.SaveChanges();
        }

        private void AccurateTraceSet(PartsDifferenceBackBill input, bool isCancel) {
            var userInfo = Utils.GetCurrentUserInfo();
            //  判断追溯信息
            var tracecodes = ObjectContext.AccurateTraces.Where(t => t.Type == (int)DCSAccurateTraceType.入库 && ObjectContext.PartsInboundPlans.Any(p =>p.InboundType==(int)DcsPartsInboundType.配件采购 && p.Id == t.SourceBillId  && p.SourceCode == input.SourceCode) && t.CompanyId == userInfo.EnterpriseId && t.Status == (int)DCSAccurateTraceStatus.有效 && t.Type == (int)DCSAccurateTraceType.入库 && t.OutQty == null).ToArray();
            foreach(var item in input.PartsDifferenceBackBillDtls) {
                if(item.TraceProperty.HasValue && !string.IsNullOrEmpty(item.TraceCode)) {
                    var newTraces = item.TraceCode.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries).ToArray();
                    if(item.TraceProperty.Value == (int)DCSTraceProperty.精确追溯) {
                        var oldTrace = tracecodes.Where(t => newTraces.Contains(t.TraceCode)).ToArray();
                        foreach(var trace in oldTrace) {
                            if(input.Type == (int)DCSPartsDifferenceBackBillType.上架) {
                                trace.ModifierId = userInfo.Id;
                                trace.InQty = null;
                                trace.ModifierName = userInfo.Name;
                                trace.ModifyTime = DateTime.Now;
                                trace.SIHLabelCode = null;
                                UpdateToDatabase(trace);
                            } else if(isCancel) {
                                trace.Status = (int)DCSAccurateTraceStatus.差异回退不考核;
                            }
                            trace.ModifierId = userInfo.Id;
                            trace.ModifierName = userInfo.Name;
                            trace.ModifyTime = DateTime.Now;
                            UpdateToDatabase(trace);

                        }
                    } else {
                        var oldTrace = tracecodes.Where(t => t.TraceCode == newTraces[0]).FirstOrDefault();
                        if(input.Type == (int)DCSPartsDifferenceBackBillType.上架 && isCancel) {
                                oldTrace.SIHLabelCode = null;
                        }
                        if(input.Type != (int)DCSPartsDifferenceBackBillType.上架 && isCancel) {
                            oldTrace.Status = (int)DCSAccurateTraceStatus.差异回退不考核;
                        }
                        oldTrace.ModifierId = userInfo.Id;
                        oldTrace.ModifierName = userInfo.Name;
                        oldTrace.ModifyTime = DateTime.Now;
                        UpdateToDatabase(oldTrace);
                    }
                }
            }
        }
        public void InitialApprove(PartsDifferenceBackBill input) {
            var user = Utils.GetCurrentUserInfo();
            bool cancel = false;
            switch(input.Type) {
                case (int)DCSPartsDifferenceBackBillType.上架:
                    input.Status = (int)DCSPartsDifferenceBackBillStatus.生效;
                    input.ApproverId = user.Id;
                    input.ApproverName = user.Name;
                    input.ApproveTime = DateTime.Now;
                    var partsShelvesTasks = ObjectContext.PartsShelvesTasks.Where(o => o.SourceBillCode == input.SourceCode).ToList();
                    var packingTasks = ObjectContext.PackingTasks.Where(o => o.SourceCode == input.SourceCode).ToList();

                    var warehouseId = partsShelvesTasks.First().WarehouseId;
                    var partIds = input.PartsDifferenceBackBillDtls.Select(o => o.SparePartId).Distinct().ToArray();
                    var partsStocks = new PartsStockAch(this.DomainService).GetPartsStocksWithLock(ObjectContext.PartsStocks.Where(r => partIds.Contains(r.PartId)
                        && r.WarehouseId == warehouseId && ObjectContext.WarehouseAreaCategories.Any(o => o.Category == (int)DcsAreaType.检验区 && o.Id == r.WarehouseAreaCategoryId))).ToList();

                    foreach(var item in input.PartsDifferenceBackBillDtls) {
                        var partsShelvesTask = partsShelvesTasks.FirstOrDefault(o => o.Id == item.TaskId && (o.PlannedAmount - (o.ShelvesAmount ?? 0)) >= item.DiffQuantity);
                        if(partsShelvesTask == null)
                            throw new ValidationException("上架单状态已发生变更，请驳回修改清单差异量后重新审核");
                        var packingTask = packingTasks.First(o => o.SparePartId == item.SparePartId);
                        var taskDiffQuantity = partsShelvesTask.PlannedAmount.Value - (partsShelvesTask.ShelvesAmount ?? 0);
                        //回退检验区 扣减锁定量操作
                        var partsStock = partsStocks.First(o => o.PartId == item.SparePartId);
                        partsStock.LockedQty = (partsStock.LockedQty ?? 0) + item.DiffQuantity;

                        if(taskDiffQuantity > item.DiffQuantity && partsShelvesTask.Status == (int)DcsPartsShelvesTaskStatus.新增) {
                            partsShelvesTask.PlannedAmount -= item.DiffQuantity;

                            packingTask.Status = (int)DcsPackingTaskStatus.部分包装;
                        } else if(taskDiffQuantity == item.DiffQuantity && partsShelvesTask.Status == (int)DcsPartsShelvesTaskStatus.新增) {
                            partsShelvesTask.Status = (int)DcsPartsShelvesTaskStatus.取消;
                            packingTask.Status = (int)DcsPackingTaskStatus.新增;
                            cancel = true;
                        } else if(taskDiffQuantity == item.DiffQuantity && partsShelvesTask.Status == (int)DcsPartsShelvesTaskStatus.部分上架) {
                            partsShelvesTask.PlannedAmount -= item.DiffQuantity;
                            partsShelvesTask.Status = (int)DcsPartsShelvesTaskStatus.强制上架完成;

                            packingTask.Status = (int)DcsPackingTaskStatus.部分包装;
                        } else if(taskDiffQuantity > item.DiffQuantity && partsShelvesTask.Status == (int)DcsPartsShelvesTaskStatus.部分上架) {
                            partsShelvesTask.PlannedAmount -= item.DiffQuantity;

                            packingTask.Status = (int)DcsPackingTaskStatus.部分包装;
                        }

                        //回退包装单包装量
                        packingTask.PackingQty -= item.DiffQuantity;
                    }
                    this.AccurateTraceSet(input, cancel);
                    break;
                case (int)DCSPartsDifferenceBackBillType.包装:
                case (int)DCSPartsDifferenceBackBillType.入库:
                    input.Status = (int)DCSPartsDifferenceBackBillStatus.初审通过;
                    input.InitialApproverId = user.Id;
                    input.InitialApproverName = user.Name;
                    input.InitialApproveTime = DateTime.Now;
                    break;
            }

        }

        private void Audit(PartsDifferenceBackBill input, PartsInboundPlan partsInboundPlan) {
            var user = Utils.GetCurrentUserInfo();
            switch(input.Type) {
                case (int)DCSPartsDifferenceBackBillType.包装:
                    input.Status = (int)DCSPartsDifferenceBackBillStatus.生效;
                    input.ApproverId = user.Id;
                    input.ApproverName = user.Name;
                    input.ApproveTime = DateTime.Now;
                    var packingTasks = ObjectContext.PackingTasks.Where(o => o.SourceCode == input.SourceCode).ToList();

                    var partsInboundCheckBill = ObjectContext.PartsInboundCheckBills.Include("PartsInboundCheckBillDetails").FirstOrDefault(o => o.Code == input.PartsInboundCheckBillCode);

                    var warehouseId = packingTasks.First().WarehouseId;
                    var partIds = input.PartsDifferenceBackBillDtls.Select(o => o.SparePartId).Distinct().ToArray();
                    var partsStocks = new PartsStockAch(this.DomainService).GetPartsStocksWithLock(ObjectContext.PartsStocks.Where(r => partIds.Contains(r.PartId)
                        && r.WarehouseId == warehouseId && ObjectContext.WarehouseAreaCategories.Any(o => o.Category == (int)DcsAreaType.检验区 && o.Id == r.WarehouseAreaCategoryId))).ToList();
                    var enterprisePartsCosts = new EnterprisePartsCostAch(this.DomainService).GetEnterprisePartsCostsWithLock(ObjectContext.EnterprisePartsCosts.Where(r => r.OwnerCompanyId == partsInboundPlan.StorageCompanyId && partIds.Contains(r.SparePartId))).ToList();

                    if(partsInboundCheckBill.PartsInboundCheckBillDetails.Sum(o => o.InspectedQuantity) == input.PartsDifferenceBackBillDtls.Sum(o => o.DiffQuantity))
                        partsInboundCheckBill.Status = (int)DcsPartsInboundCheckBillStatus.取消;
                    foreach(var item in input.PartsDifferenceBackBillDtls) {
                        var packingTask = packingTasks.FirstOrDefault(o => o.Id == item.TaskId && (o.PlanQty - (o.PackingQty ?? 0)) >= item.DiffQuantity);
                        if(packingTask == null)
                            throw new ValidationException("包装任务单状态已发生变更，请驳回修改清单差异量后重新审核");
                        var taskDiffQuantity = packingTask.PlanQty - (packingTask.PackingQty ?? 0);
                        if(taskDiffQuantity > item.DiffQuantity && packingTask.Status == (int)DcsPackingTaskStatus.新增) {
                            packingTask.PlanQty -= item.DiffQuantity;
                            packingTask.Status = (int)DcsPackingTaskStatus.新增;
                        } else if(taskDiffQuantity == item.DiffQuantity && packingTask.Status == (int)DcsPackingTaskStatus.新增) {
                            packingTask.Status = (int)DcsPackingTaskStatus.取消;
                        } else if(taskDiffQuantity == item.DiffQuantity && packingTask.Status == (int)DcsPackingTaskStatus.部分包装) {
                            packingTask.PlanQty -= item.DiffQuantity;
                            packingTask.Status = (int)DcsPackingTaskStatus.强制包装完成;
                        } else if(taskDiffQuantity > item.DiffQuantity && packingTask.Status == (int)DcsPackingTaskStatus.部分包装) {
                            packingTask.PlanQty -= item.DiffQuantity;
                            packingTask.Status = (int)DcsPackingTaskStatus.部分包装;
                        }

                        if(partsInboundCheckBill.Status != (int)DcsPartsInboundCheckBillStatus.取消) {
                            var partsInboundCheckBillDetail = partsInboundCheckBill.PartsInboundCheckBillDetails.FirstOrDefault(o => o.SparePartId == item.SparePartId);
                            if(partsInboundCheckBillDetail.InspectedQuantity == item.DiffQuantity) {
                                DeleteFromDatabase(partsInboundCheckBillDetail);
                            } else if(partsInboundCheckBillDetail.InspectedQuantity > item.DiffQuantity) {
                                partsInboundCheckBillDetail.InspectedQuantity -= item.DiffQuantity;
                            }
                        }
                        //回退入库计划单，入库检验量
                        var partsInboundPlanDetail = partsInboundPlan.PartsInboundPlanDetails.FirstOrDefault(o => o.SparePartId == item.SparePartId);
                        if((partsInboundPlanDetail.InspectedQuantity ?? 0) < item.DiffQuantity)
                            throw new ValidationException("配件" + partsInboundPlanDetail.SparePartCode + "差异回退量大于收货量");

                        partsInboundPlanDetail.InspectedQuantity = (partsInboundPlanDetail.InspectedQuantity ?? 0) - item.DiffQuantity;
                        //回退检验区 数量与锁定量
                        var partsStock = partsStocks.First(o => o.PartId == item.SparePartId);
                        if(partsStock.LockedQty == null || item.DiffQuantity > partsStock.LockedQty || item.DiffQuantity > partsStock.Quantity) {
                            throw new ValidationException(ErrorStrings.PackingTask_Validation2);
                        }
                        partsStock.LockedQty = (partsStock.LockedQty ?? 0) - item.DiffQuantity;
                        partsStock.Quantity = partsStock.Quantity - item.DiffQuantity;
                        //回退企业库存，重新计算成本金额
                        var enterprisePartsCost = enterprisePartsCosts == null ? null : enterprisePartsCosts.FirstOrDefault(r => r.SparePartId == item.SparePartId);
                        if(partsInboundPlan.InboundType != (int)DcsPartsInboundType.配件调拨) {
                            enterprisePartsCost.Quantity -= item.DiffQuantity;
                            enterprisePartsCost.CostAmount -= item.DiffQuantity * partsInboundPlanDetail.Price;
                            if(enterprisePartsCost.Quantity > 0)
                                enterprisePartsCost.CostPrice = Math.Round(enterprisePartsCost.CostAmount / enterprisePartsCost.Quantity, 2);
                        }
                    }
                    this.AccurateTraceSet(input, false);
                    break;
                case (int)DCSPartsDifferenceBackBillType.入库:
                    input.Status = (int)DCSPartsDifferenceBackBillStatus.审核通过;
                    input.CheckerId = user.Id;
                    input.CheckerName = user.Name;
                    input.CheckTime = DateTime.Now;
                    break;
            }
        }

        private void Approve(PartsDifferenceBackBill input, PartsInboundPlan partsInboundPlan) {
            var user = Utils.GetCurrentUserInfo();
            if(input.Type == (int)DCSPartsDifferenceBackBillType.入库) {
                input.Status = (int)DCSPartsDifferenceBackBillStatus.生效;
                input.ApproverId = user.Id;
                input.ApproverName = user.Name;
                input.ApproveTime = DateTime.Now;
                bool isCancel = false;
                var supplierShippingOrder = ObjectContext.SupplierShippingOrders.Include("SupplierShippingDetails").First(o => o.Id == partsInboundPlan.SourceId);
                var partsPurchaseOrder = ObjectContext.PartsPurchaseOrders.Include("PartsPurchaseOrderDetails").First(o => o.Id == supplierShippingOrder.PartsPurchaseOrderId);

                if(partsInboundPlan.PartsInboundPlanDetails.Sum(o => o.PlannedAmount) == input.PartsDifferenceBackBillDtls.Sum(o => o.DiffQuantity)) {
                    partsInboundPlan.Status = (int)DcsPartsInboundPlanStatus.取消;
                    isCancel = true;
                }
                if(supplierShippingOrder.SupplierShippingDetails.Sum(o => o.Quantity) == input.PartsDifferenceBackBillDtls.Sum(o => o.DiffQuantity))
                    supplierShippingOrder.Status = (int)DcsSupplierShippingOrderStatus.取消;
                foreach(var item in input.PartsDifferenceBackBillDtls) {
                    if(partsInboundPlan.Status != (int)DcsPartsInboundPlanStatus.取消) {
                        var partsInboundPlanDetail = partsInboundPlan.PartsInboundPlanDetails.FirstOrDefault(o => o.SparePartId == item.SparePartId && (o.PlannedAmount - (o.InspectedQuantity ?? 0)) >= item.DiffQuantity);
                        if(partsInboundPlanDetail == null)
                            throw new ValidationException("根据源单据编号" + input.SourceCode + "备件编号" + item.SparePartCode + "未找到对应符合条件的入库计划单清单");

                        partsInboundPlanDetail.PlannedAmount -= item.DiffQuantity;
                    }

                    if(supplierShippingOrder.Status != (int)DcsSupplierShippingOrderStatus.取消) {
                        var supplierShippingOrderDetail = supplierShippingOrder.SupplierShippingDetails.FirstOrDefault(o => o.SparePartId == item.SparePartId);
                        supplierShippingOrderDetail.Quantity -= item.DiffQuantity;
                    }

                    var partsPurchaseOrderDetail = partsPurchaseOrder.PartsPurchaseOrderDetails.First(o => o.SparePartId == item.SparePartId);
                    partsPurchaseOrderDetail.ShippingAmount -= item.DiffQuantity;
                }
                if(partsInboundPlan.PartsInboundPlanDetails.Sum(o => o.PlannedAmount) == partsInboundPlan.PartsInboundPlanDetails.Sum(o => o.InspectedQuantity))
                    partsInboundPlan.Status = (int)DcsPartsInboundPlanStatus.强制检验完成;
                if(!partsPurchaseOrder.PartsPurchaseOrderDetails.Any(o => (o.ShippingAmount ?? 0) != 0))
                    partsPurchaseOrder.Status = (int)DcsPartsPurchaseOrderStatus.确认完毕;
                if(partsPurchaseOrder.PartsPurchaseOrderDetails.Any(o => (o.ShippingAmount ?? 0) > 0))
                    partsPurchaseOrder.Status = (int)DcsPartsPurchaseOrderStatus.部分发运;
                this.AccurateTraceSet(input, isCancel);
            } else {
                throw new ValidationException("单据类型异常，无法审批。");
            }
        }

        public void 驳回配件差异回退单(int id, int status) {
            var user = Utils.GetCurrentUserInfo();
            var input = ObjectContext.PartsDifferenceBackBills.FirstOrDefault(o => o.Id == id && o.Status == status);
            if(input == null)
                throw new ValidationException(ErrorStrings.PartsDifferenceBackBill_Validation3);

            input.Status = (int)DCSPartsDifferenceBackBillStatus.新增;
            input.RejecterId = user.Id;
            input.RejecterName = user.Name;
            input.RejectTime = DateTime.Now;
            input.InitialApproverId = null;
            input.InitialApproverName = null;
            input.InitialApproveTime = null;
            input.CheckerId = null;
            input.CheckerName = null;
            input.CheckTime = null;
            input.ApproverId = null;
            input.ApproverName = null;
            input.ApproveTime = null;
            input.SubmitterId = null;
            input.SubmitterName = null;
            input.SubmitTime = null;
            this.UpdatePartsDifferenceBackBillValidate(input);
            this.ObjectContext.SaveChanges();
        }
    }

    partial class DcsDomainService {
        //原分布类的函数全部转移到Ach                                                               
        [Update(UsingCustomMethod = true)]
        public void 作废配件差异回退单(PartsDifferenceBackBill input) {
            new PartsDifferenceBackBillAch(this).作废配件差异回退单(input);
        }

        [Update(UsingCustomMethod = true)]
        public void 提交配件差异回退单(PartsDifferenceBackBill input) {
            new PartsDifferenceBackBillAch(this).提交配件差异回退单(input);
        }

        [Invoke(HasSideEffects = true)]
        public void 审核配件差异回退单(int id, int status) {
            new PartsDifferenceBackBillAch(this).审核配件差异回退单(id, status);
        }

        [Invoke(HasSideEffects = true)]
        public void 驳回配件差异回退单(int id, int status) {
            new PartsDifferenceBackBillAch(this).驳回配件差异回退单(id, status);
        }
    }
}
