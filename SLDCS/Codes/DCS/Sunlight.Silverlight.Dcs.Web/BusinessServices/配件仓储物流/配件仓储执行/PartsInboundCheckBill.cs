﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.Objects;
using System.Globalization;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using System.Transactions;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsInboundCheckBillAch : DcsSerivceAchieveBase {
        public PartsInboundCheckBillAch(DcsDomainService domainService)
            : base(domainService) {
        }

        public void 生成配件入库检验单(PartsInboundCheckBill partsInboundCheckBill) {
            using(var transaction = new TransactionScope()) {
            var partsInboundCheckBillDetails = partsInboundCheckBill.PartsInboundCheckBillDetails;
            var partsInboundPackingDetails = partsInboundCheckBill.PartsInboundPackingDetails;
            var partIds = partsInboundCheckBillDetails.Select(r => r.SparePartId);
            var warehouseAreaIds = partsInboundCheckBillDetails.Select(r => r.WarehouseAreaId);
            var warehouseAreas = ObjectContext.WarehouseAreas.Where(r => warehouseAreaIds.Contains(r.Id) && r.Status == (int)DcsBaseDataStatus.有效).ToArray();
            var userInfo = Utils.GetCurrentUserInfo();

            var partsInboundPlan = ObjectContext.PartsInboundPlans.SingleOrDefault(r => r.Id == partsInboundCheckBill.PartsInboundPlanId && (r.Status == (int)DcsPartsInboundPlanStatus.新建 || r.Status == (int)DcsPartsInboundPlanStatus.部分检验));
            if(partsInboundPlan == null)
                throw new ValidationException(ErrorStrings.PartsInboundCheckBill_Validation1);
            var partsInboundPlanDetails = ObjectContext.PartsInboundPlanDetails.Where(r => r.PartsInboundPlanId == partsInboundPlan.Id).ToArray();
            var partsStocks = new PartsStockAch(this.DomainService).GetPartsStocksWithLock(ObjectContext.PartsStocks.Where(r => partIds.Contains(r.PartId) && warehouseAreaIds.Contains(r.WarehouseAreaId))).ToList();
            var enterprisePartsCosts = new EnterprisePartsCostAch(this.DomainService).GetEnterprisePartsCostsWithLock(ObjectContext.EnterprisePartsCosts.Where(r => r.OwnerCompanyId == partsInboundPlan.StorageCompanyId && partIds.Contains(r.SparePartId))).ToList();

            var company = ObjectContext.Companies.Single(r => r.Id == partsInboundCheckBill.StorageCompanyId);

            if(partsInboundPlan.OriginalRequirementBillCode != null && partsInboundPlan.OriginalRequirementBillCode.StartsWith("HWPP")) {
                partsInboundCheckBill.PurOrderCode = partsInboundPlan.OriginalRequirementBillCode;
            } else {
                if(partsInboundPlan.OriginalRequirementBillType == (int)DcsOriginalRequirementBillType.配件采购订单) {
                    var partsPurchaseOrder = ObjectContext.PartsPurchaseOrders.FirstOrDefault(r => r.Id == partsInboundPlan.OriginalRequirementBillId);
                    if(partsPurchaseOrder != null && !string.IsNullOrEmpty(partsPurchaseOrder.PurOrderCode))
                        partsInboundCheckBill.PurOrderCode = partsPurchaseOrder.PurOrderCode;
                }

                if(partsInboundPlan.OriginalRequirementBillType == (int)DcsOriginalRequirementBillType.配件调拨单) {
                    var partsTransferOrder = ObjectContext.PartsTransferOrders.FirstOrDefault(r => r.Id == partsInboundPlan.OriginalRequirementBillId);
                    if(partsTransferOrder != null && !string.IsNullOrEmpty(partsTransferOrder.PurOrderCode))
                        partsInboundCheckBill.PurOrderCode = partsTransferOrder.PurOrderCode;
                }
            }

            InsertToDatabase(partsInboundCheckBill);

            var partsAmountDetails = new Dictionary<int, decimal>();
            if(company.Type == (int)DcsCompanyType.分公司) {
                var partsPlannedPrice = (from a in ObjectContext.PartsPlannedPrices.Where(r => r.OwnerCompanyId == partsInboundCheckBill.StorageCompanyId && r.PartsSalesCategoryId == partsInboundCheckBill.PartsSalesCategoryId && partIds.Contains(r.SparePartId)).ToArray()
                                         select a).ToArray();
                var partsPlannedPriceGroups = partsPlannedPrice.GroupBy(r => r.SparePartId).Where(r => r.Count() >= 2).ToArray();
                if(partsPlannedPriceGroups.Any()) {
                    throw new ValidationException(string.Format(ErrorStrings.PartsInboundCheckBillDetail_Much_Prices, partsPlannedPriceGroups.Select(r => r.Key.ToString(CultureInfo.InvariantCulture)).ToArray().Aggregate((a, b) => a += "," + b)));
                }
                partsAmountDetails = partsPlannedPrice.ToDictionary(r => r.SparePartId, r => r.PlannedPrice);
            }
            int id = -3000;
            foreach(var partsInboundCheckBillDetail in partsInboundCheckBillDetails) {
                //更新配件入库计划清单
                var partsInboundPlanDetail = partsInboundPlanDetails.SingleOrDefault(v => v.SparePartId == partsInboundCheckBillDetail.SparePartId && v.Price == partsInboundCheckBillDetail.SettlementPrice && (v.InspectedQuantity ?? 0) != v.PlannedAmount);
                if(partsInboundPlanDetail == null)
                    throw new ValidationException(string.Format(ErrorStrings.PartsInboundCheckBill_Validation6, partsInboundCheckBillDetail.SparePartCode));
                if(!partsInboundPlanDetail.InspectedQuantity.HasValue) {
                    partsInboundPlanDetail.InspectedQuantity = 0;
                }

                if(partsInboundPlanDetail.InspectedQuantity + partsInboundCheckBillDetail.InspectedQuantity > partsInboundPlanDetail.PlannedAmount) {
                    throw new ValidationException(ErrorStrings.PartsInboundCheckBill_Validation8);
                }
                partsInboundPlanDetail.InspectedQuantity += partsInboundCheckBillDetail.InspectedQuantity;

                //重新计算企业配件成本价
                var enterprisePartsCost = enterprisePartsCosts == null ? null : enterprisePartsCosts.FirstOrDefault(r => r.SparePartId == partsInboundCheckBillDetail.SparePartId);
                if(partsInboundCheckBill.InboundType != (int)DcsPartsInboundType.配件调拨) {
                    if(enterprisePartsCost != null) {
                        enterprisePartsCost.Quantity += partsInboundCheckBillDetail.InspectedQuantity;
                        if(partsInboundPlan.InboundType == (int)DcsPartsInboundType.配件采购 || partsInboundPlan.InboundType == (int)DcsPartsInboundType.积压件调剂)
                            enterprisePartsCost.CostAmount += partsInboundCheckBillDetail.InspectedQuantity * partsInboundCheckBillDetail.SettlementPrice;
                        else
                            enterprisePartsCost.CostAmount += partsInboundCheckBillDetail.InspectedQuantity * enterprisePartsCost.CostPrice;
                        enterprisePartsCost.CostPrice = Math.Round(enterprisePartsCost.CostAmount / enterprisePartsCost.Quantity, 2);
                        enterprisePartsCost.ModifierId = userInfo.Id;
                        enterprisePartsCost.ModifierName = userInfo.Name;
                        enterprisePartsCost.ModifyTime = DateTime.Now;

                        UpdateToDatabase(enterprisePartsCost);
                    } else {
                        //if(partsInboundPlan.InboundType != (int)DcsPartsInboundType.配件采购 && partsInboundPlan.InboundType != (int)DcsPartsInboundType.积压件调剂 && partsInboundPlan.InboundType != (int)DcsPartsInboundType.配件外采)
                        //    throw new ValidationException(string.Format(ErrorStrings.PartsInboundCheckBill_Validation9, partsInboundCheckBillDetail.SparePartCode));
                        enterprisePartsCost = new EnterprisePartsCost {
                            OwnerCompanyId = partsInboundCheckBill.StorageCompanyId,
                            PartsSalesCategoryId = partsInboundCheckBill.PartsSalesCategoryId,
                            SparePartId = partsInboundCheckBillDetail.SparePartId,
                            Quantity = partsInboundCheckBillDetail.InspectedQuantity,
                            CostAmount = partsInboundCheckBillDetail.InspectedQuantity * partsInboundCheckBillDetail.SettlementPrice,
                            CostPrice = Math.Round(partsInboundCheckBillDetail.SettlementPrice, 2),
                            CreatorId = userInfo.Id,
                            CreatorName = userInfo.Name,
                            CreateTime = DateTime.Now,
                        };
                        if(partsInboundPlan.InboundType == (int)DcsPartsInboundType.配件采购 || partsInboundPlan.InboundType == (int)DcsPartsInboundType.积压件调剂 || partsInboundPlan.InboundType == (int)DcsPartsInboundType.配件外采)
                            InsertToDatabase(enterprisePartsCost);
                    }
                }

                var partsStock = partsStocks.FirstOrDefault(r => r.PartId == partsInboundCheckBillDetail.SparePartId && r.WarehouseAreaId == partsInboundCheckBillDetail.WarehouseAreaId);
                if(partsStock == null) {
                    //新增配件库存
                    var tempWarehouseArea = warehouseAreas.SingleOrDefault(r => r.Id == partsInboundCheckBillDetail.WarehouseAreaId);
                    if(tempWarehouseArea == null) {
                        throw new ValidationException(ErrorStrings.PartsInboundCheckBill_Validation10);
                    }
                    var ps = new PartsStock {
                        Id = id++,
                        WarehouseId = partsInboundCheckBill.WarehouseId,
                        StorageCompanyId = partsInboundCheckBill.StorageCompanyId,
                        StorageCompanyType = partsInboundCheckBill.StorageCompanyType,
                        BranchId = partsInboundCheckBill.BranchId,
                        WarehouseAreaId = partsInboundCheckBillDetail.WarehouseAreaId,
                        PartId = partsInboundCheckBillDetail.SparePartId,
                        Quantity = partsInboundCheckBillDetail.InspectedQuantity,
                        Remark = partsInboundCheckBillDetail.Remark,
                        WarehouseAreaCategoryId = tempWarehouseArea.AreaCategoryId,
                        CreateTime = DateTime.Now,
                        CreatorId = userInfo.Id,
                        CreatorName = userInfo.Name
                    };
                    //InsertToDatabase(ps);
                    partsStocks.Add(ps);
                } else {
                    //更新配件库存
                    partsStock.Quantity = partsStock.Quantity + partsInboundCheckBillDetail.InspectedQuantity;
                    partsStock.Remark = partsStock.Remark + partsInboundCheckBillDetail.Remark;
                    partsStock.ModifierId = userInfo.Id;
                    partsStock.ModifierName = userInfo.Name;
                    partsStock.ModifyTime = DateTime.Now;
                    if(partsStock.Id > 0)
                        UpdateToDatabase(partsStock);
                }
                if(company.Type == (int)DcsCompanyType.分公司 || company.Type == (int)DcsCompanyType.代理库 || company.Type == (int)DcsCompanyType.服务站兼代理库) {
                    if(partsInboundCheckBill.InboundType == (int)DcsPartsInboundType.配件调拨) {
                        var tmpPartsShippingOrderRefOutBill = (from b in ObjectContext.PartsOutboundBills
                                                               join c in ObjectContext.PartsOutboundBillDetails on b.Id equals c.PartsOutboundBillId
                                                               where b.PartsShippingOrderId == partsInboundPlan.SourceId
                                                               select new {
                                                                   SparePartId = c.SparePartId,
                                                                   CostPrice = c.CostPrice
                                                               }).ToArray();
                        if(tmpPartsShippingOrderRefOutBill == null)
                            throw new ValidationException(ErrorStrings.PartsInboundCheckBill_Validation11);
                        partsInboundCheckBillDetail.CostPrice = tmpPartsShippingOrderRefOutBill.FirstOrDefault(r => r.SparePartId == partsInboundCheckBillDetail.SparePartId).CostPrice;
                    } else if(partsInboundCheckBill.InboundType == (int)DcsPartsInboundType.配件采购) {
                        partsInboundCheckBillDetail.CostPrice = partsInboundPlanDetail.Price;
                    } else {
                        partsInboundCheckBillDetail.CostPrice = enterprisePartsCost.CostPrice;
                    }
                    if(partsInboundCheckBillDetail.CostPrice == 0) {
                        throw new ValidationException(string.Format(ErrorStrings.PartsInboundCheckBill_Validation12,
                            partsInboundCheckBillDetail.SparePartCode));
                    }
                }               
                //if(company.Type == (int)DcsCompanyType.分公司) {
                //    if(partsInboundCheckBill.InboundType == (int)DcsPartsInboundType.配件调拨) {
                //        //var dbPartsShippingOrderRef = ObjectContext.PartsShippingOrderRefs.Include("PartsOutboundBill.PartsOutboundBillDetails").FirstOrDefault(r => r.PartsShippingOrderId == partsInboundPlan.SourceId);
                //        //if (dbPartsShippingOrderRef == null)
                //        //    throw new ValidationException("配件入库计划源单据对应的配件出库单不存在");
                //        //partsInboundCheckBillDetail.CostPrice = dbPartsShippingOrderRef.PartsOutboundBill.PartsOutboundBillDetails.First(r => r.SparePartId == partsInboundCheckBillDetail.SparePartId).CostPrice;
                //        var tmpPartsShippingOrderRefOutBill = (from a in ObjectContext.PartsShippingOrderRefs
                //                                               join b in ObjectContext.PartsOutboundBills on a.PartsOutboundBillId equals b.Id
                //                                               join c in ObjectContext.PartsOutboundBillDetails on b.Id equals c.PartsOutboundBillId
                //                                               where a.PartsShippingOrderId == partsInboundPlan.SourceId
                //                                               select new {
                //                                                   SparePartId = c.SparePartId,
                //                                                   CostPrice = c.CostPrice
                //                                               }).ToArray();
                //        if(tmpPartsShippingOrderRefOutBill == null)
                //            throw new ValidationException("配件入库计划源单据对应的配件出库单不存在");
                //        partsInboundCheckBillDetail.CostPrice = tmpPartsShippingOrderRefOutBill.FirstOrDefault(r => r.SparePartId == partsInboundCheckBillDetail.SparePartId).CostPrice;
                //    } else {
                //        if(!partsAmountDetails.ContainsKey(partsInboundCheckBillDetail.SparePartId)) {
                //            throw new ValidationException(string.Format(ErrorStrings.PartsInboundCheckBill_Validation5, partsInboundCheckBillDetail.SparePartCode));
                //        } else {
                //            partsInboundCheckBillDetail.CostPrice = partsAmountDetails[partsInboundCheckBillDetail.SparePartId];
                //        }
                //    }
                //    if(partsInboundCheckBillDetail.CostPrice == 0) {
                //        throw new ValidationException(string.Format("编号为“{0}”的配件的成本价等于0", partsInboundCheckBillDetail.SparePartCode));
                //    }
                //}
            }
            //批量更新或者新增配件库存
            //var checkPartIds = partsStocks.Select(r => r.PartId);
            //var checkWarehouseAreaIds = partsStocks.Select(r => r.WarehouseAreaId);
            //var checkPartsStocks = ObjectContext.PartsStocks.Where(r => checkPartIds.Contains(r.PartId) && checkWarehouseAreaIds.Contains(r.WarehouseAreaId)).SetMergeOption(MergeOption.NoTracking).ToArray();
            foreach(var item in partsStocks) {
                //数量要大于0
                if(item.Quantity < 0) {
                    throw new ValidationException(ErrorStrings.PartsStock_Validation5);
                }
                if(item.Id < 0) {
                    InsertToDatabase(item);
                }
                //新增修改的唯一性校验
                //switch(item.EntityState) {
                //    case EntityState.Modified:
                //        var checkExists = checkPartsStocks.FirstOrDefault(r => r.Id == item.Id);
                //        if(checkExists == null) {
                //            throw new ValidationException(ErrorStrings.PartsStock_Validation6);
                //        }
                //        var checkOnlyOneForUpdate = checkPartsStocks.SingleOrDefault(r => r.Id != item.Id && r.PartId == item.PartId && r.WarehouseAreaId == item.WarehouseAreaId);
                //        if(checkOnlyOneForUpdate != null) {
                //            throw new ValidationException(ErrorStrings.PartsStock_Validation7);
                //        }
                //        item.ModifierId = userInfo.Id;
                //        item.ModifierName = userInfo.Name;
                //        item.ModifyTime = DateTime.Now;
                //        break;
                //    case EntityState.Added:
                //        var checkOnlyOneForInsert = checkPartsStocks.SingleOrDefault(r => r.PartId == item.PartId && r.WarehouseAreaId == item.WarehouseAreaId);
                //        if(checkOnlyOneForInsert != null) {
                //            throw new ValidationException(ErrorStrings.PartsStock_Validation1);
                //        }
                //        item.CreatorId = userInfo.Id;
                //        item.CreatorName = userInfo.Name;
                //        item.CreateTime = DateTime.Now;
                //        break;
                //}
            }

            bool isAllCheck = partsInboundPlanDetails.All(partsInboundPlanDetail => partsInboundPlanDetail.PlannedAmount == partsInboundPlanDetail.InspectedQuantity);

            //更新入库计划单状态
            if(isAllCheck) {
                partsInboundPlan.Status = (int)DcsPartsInboundPlanStatus.检验完成;
            } else {
                partsInboundPlan.Status = (int)DcsPartsInboundPlanStatus.部分检验;
            }

            foreach(var partsInboundPlanDetail in partsInboundPlanDetails) {
                UpdateToDatabase(partsInboundPlanDetail);
            }
            new PartsInboundPlanAch(this.DomainService).UpdatePartsInboundPlanValidate(partsInboundPlan);

            var batchNumbers = partsInboundPackingDetails.Select(r => r.BatchNumber.ToLower()).ToArray();
            var partsBatchRecords = ObjectContext.PartsBatchRecords.Where(r => batchNumbers.Contains(r.BatchNumber.ToLower())).Select(r => r.BatchNumber.ToLower());

            var partsStockBatchDetails = new List<PartsStockBatchDetail>();//用来装配件库存批次明细的集合用来批量校验

            foreach(var partsInboundPackingDetail in partsInboundPackingDetails) {
                //新增配件库存批次明细
                var partsStock = partsStocks.FirstOrDefault(r => r.PartId == partsInboundPackingDetail.SparePartId && r.WarehouseAreaId == partsInboundPackingDetail.WarehouseAreaId);
                if(partsStock == null)
                    throw new ValidationException(string.Format(ErrorStrings.PartsInboundCheckBill_Validation2, partsInboundPackingDetail.SparePartCode, partsInboundPackingDetail.WarehouseAreaCode));
                var partsStockBatchDetail = new PartsStockBatchDetail {
                    PartsStockId = partsStock.Id,
                    WarehouseId = partsInboundCheckBill.WarehouseId,
                    StorageCompanyId = partsInboundCheckBill.StorageCompanyId,
                    StorageCompanyType = partsInboundCheckBill.StorageCompanyType,
                    PartId = partsInboundPackingDetail.SparePartId,
                    Quantity = partsInboundPackingDetail.Quantity,
                    BatchNumber = partsInboundPackingDetail.BatchNumber,
                    InboundTime = DateTime.Now
                };
                partsStockBatchDetails.Add(partsStockBatchDetail);
                // partsStock.PartsStockBatchDetails.Add(partsStockBatchDetail);
                InsertToDatabase(partsStockBatchDetail);
                // InsertPartsStockBatchDetailValidate(partsStockBatchDetail);
                if(!partsBatchRecords.Contains(partsInboundPackingDetail.BatchNumber.ToLower())) {
                    //新增配件批次记录
                    var partsBatchRecord = new PartsBatchRecord {
                        PartId = partsInboundPackingDetail.SparePartId,
                        Quantity = partsInboundPackingDetail.Quantity,
                        BatchNumber = partsInboundPackingDetail.BatchNumber,
                        Status = (int)DcsBaseDataStatus.有效,
                        OriginalBatchNumber = null
                    };
                    InsertToDatabase(partsBatchRecord);
                    new PartsBatchRecordAch(this.DomainService).InsertPartsBatchRecordValidate(partsBatchRecord); //这个要自动生成编号，框架方法中没有批量生成编号的方法，只能多次读取数据库
                }
            }
            //批量校验批次明细,因为都是 insert  所以不分情况判断
            var batchNumbersForCheck = partsStockBatchDetails.GroupBy(r => new {
                r.BatchNumber
            }).Select(r => r.Key.BatchNumber).ToArray();
            var checkPartsStockBatchDetail = ObjectContext.PartsStockBatchDetails.Where(r => batchNumbersForCheck.Contains(r.BatchNumber)).Select(r => r.BatchNumber).SetMergeOption(MergeOption.NoTracking).ToArray();
            if(checkPartsStockBatchDetail.Length > 0) {
                throw new ValidationException(ErrorStrings.PartsInboundCheckBill_Validation13);
            }
            foreach(var t in partsStockBatchDetails) {
                foreach(var p in partsStocks) {
                    if(t != null && t.PartsStockId == p.Id) {
                        p.PartsStockBatchDetails.Add(t);
                    }
                }
            }

            this.InsertPartsInboundCheckBillValidate(partsInboundCheckBill);

            //更新配件物流批次及清单
            //if(partsInboundCheckBill.InboundType == (int)DcsPartsInboundType.配件调拨 || partsInboundCheckBill.InboundType == (int)DcsPartsInboundType.配件采购) {
            //    var partsLogisticBatch = ObjectContext.PartsLogisticBatches.FirstOrDefault(r => ObjectContext.PartsLogisticBatchBillDetails.Any(v => v.BillId == partsInboundCheckBill.PartsInboundPlanId && v.BillType == (int)DcsPartsLogisticBatchBillDetailBillType.配件入库计划 && v.PartsLogisticBatchId == r.Id));
            //    if(partsLogisticBatch == null)
            //        throw new ValidationException(ErrorStrings.PartsInboundCheckBill_Validation3);
            //    UpdateToDatabase(partsLogisticBatch);
            //    partsLogisticBatch.Status = (int)DcsPartsLogisticBatchStatus.完成;
            //    var partsLogisticBatchItemDetails = ObjectContext.PartsLogisticBatchItemDetails.Where(r => r.PartsLogisticBatchId == partsLogisticBatch.Id);
            //    foreach(var partsLogisticBatchItemDetail in partsLogisticBatchItemDetails) {
            //        var partsInboundCheckBillDetail = partsInboundCheckBillDetails.FirstOrDefault(r => r.SparePartId == partsLogisticBatchItemDetail.SparePartId);
            //        if(partsInboundCheckBillDetail != null)
            //            partsLogisticBatchItemDetail.InboundAmount = partsInboundCheckBillDetail.InspectedQuantity;
            //    }
            //    new PartsLogisticBatchAch(this.DomainService).UpdatePartsLogisticBatchValidate(partsLogisticBatch);
            //}

            if(partsInboundCheckBill.OriginalRequirementBillType == (int)DcsOriginalRequirementBillType.配件销售退货单) {
                var partsSaleReturnBill = this.ObjectContext.PartsSalesReturnBills.Where(r => r.Id == partsInboundCheckBill.OriginalRequirementBillId).FirstOrDefault();
                if(partsSaleReturnBill != null) {
                    if(partsSaleReturnBill.IsBarter == true)
                        partsInboundCheckBill.SettlementStatus = (int)DcsPartsSettlementStatus.已结算;
                    else
                        partsInboundCheckBill.SettlementStatus = (int)DcsPartsSettlementStatus.待结算;
                }

            }
            ObjectContext.SaveChanges();
            transaction.Complete();
            }
        }


        public void 更改不结算状态(PartsInboundCheckBill partsInboundCheckBill) {
            var dbPartsInboundCheckBill = ObjectContext.PartsInboundCheckBills.Where(r => r.Id == partsInboundCheckBill.Id && r.SettlementStatus == (int)DcsPartsSettlementStatus.待结算).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbPartsInboundCheckBill == null)
                throw new ValidationException(ErrorStrings.PartsInboundCheckBill_Validation14);
            partsInboundCheckBill.SettlementStatus = (int)DcsPartsSettlementStatus.不结算;
            UpdateToDatabase(partsInboundCheckBill);
            this.UpdatePartsInboundCheckBillValidate(partsInboundCheckBill);
        }


        public void 更改待结算状态(PartsInboundCheckBill partsInboundCheckBill) {
            var dbPartsInboundCheckBill = ObjectContext.PartsInboundCheckBills.Where(r => r.Id == partsInboundCheckBill.Id && r.SettlementStatus == (int)DcsPartsSettlementStatus.不结算).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbPartsInboundCheckBill == null)
                throw new ValidationException(ErrorStrings.PartsInboundCheckBill_Validation15);
            partsInboundCheckBill.SettlementStatus = (int)DcsPartsSettlementStatus.待结算;
            UpdateToDatabase(partsInboundCheckBill);
            this.UpdatePartsInboundCheckBillValidate(partsInboundCheckBill);
        }

        public void 生成配件收货单(PartsInboundCheckBill partsInboundCheckBill) {
            using(var transaction = new TransactionScope()) {
                try {
                    var partsInboundCheckBillDetails = partsInboundCheckBill.PartsInboundCheckBillDetails;
                    var partIds = partsInboundCheckBillDetails.Select(r => r.SparePartId);
                    var warehouse = ObjectContext.Warehouses.FirstOrDefault(o => o.Id == partsInboundCheckBill.WarehouseId && o.Status == (int)DcsBaseDataStatus.有效);
                    var spareParts = ObjectContext.SpareParts.Where(y => partIds.Contains(y.Id)).ToArray();

                    var userInfo = Utils.GetCurrentUserInfo();
                    if(userInfo.Id == 0) {
                        throw new ValidationException(ErrorStrings.User_Invalid);
                    }
                    var partsInboundPlan = ObjectContext.PartsInboundPlans.SingleOrDefault(r => r.Id == partsInboundCheckBill.PartsInboundPlanId && (r.Status == (int)DcsPartsInboundPlanStatus.新建 || r.Status == (int)DcsPartsInboundPlanStatus.部分检验));
                    if(partsInboundPlan == null)
                        throw new ValidationException(ErrorStrings.PartsInboundCheckBill_Validation1);

                    //if(partsInboundCheckBillDetails.GroupBy(v => v.SparePartId).Any(v => v.Count() > 1))
                    //    throw new ValidationException(ErrorStrings.PartsInboundCheckBill_Validation4);
                    var partsInboundPlanDetails = ObjectContext.PartsInboundPlanDetails.Where(r => r.PartsInboundPlanId == partsInboundPlan.Id).ToArray();

                    var warehouseAreaCategoryIds = ObjectContext.WarehouseAreaCategories.Where(r => r.Category == (int)DcsAreaType.检验区).Select(v => v.Id).ToArray();
                    if(warehouseAreaCategoryIds.Length == 0)
                        return;
                    var warehouseAreas = ObjectContext.WarehouseAreas.Where(r => warehouseAreaCategoryIds.Contains(r.AreaCategoryId.Value) && r.WarehouseId == partsInboundCheckBill.WarehouseId && r.Status == (int)DcsBaseDataStatus.有效 && r.AreaKind == (int)DcsAreaKind.库位).ToArray();
                    var partsStocks = new PartsStockAch(this.DomainService).GetPartsStocksWithLock(ObjectContext.PartsStocks.Where(r => partIds.Contains(r.PartId) && warehouseAreaCategoryIds.Contains(r.WarehouseAreaCategoryId.Value) && r.WarehouseId == partsInboundCheckBill.WarehouseId)).ToList();
                    var enterprisePartsCosts = new EnterprisePartsCostAch(this.DomainService).GetEnterprisePartsCostsWithLock(ObjectContext.EnterprisePartsCosts.Where(r => r.OwnerCompanyId == partsInboundPlan.StorageCompanyId && partIds.Contains(r.SparePartId))).ToList();

                    var company = ObjectContext.Companies.Single(r => r.Id == partsInboundCheckBill.StorageCompanyId);


                    if(partsInboundPlan.OriginalRequirementBillType == (int)DcsOriginalRequirementBillType.配件采购订单) {
                        var partsPurchaseOrder = ObjectContext.PartsPurchaseOrders.FirstOrDefault(r => r.Id == partsInboundPlan.OriginalRequirementBillId);
                        if(partsPurchaseOrder != null && !string.IsNullOrEmpty(partsPurchaseOrder.PurOrderCode))
                            partsInboundCheckBill.PurOrderCode = partsPurchaseOrder.PurOrderCode;
                    }

                    if(partsInboundPlan.OriginalRequirementBillType == (int)DcsOriginalRequirementBillType.配件调拨单) {
                        var partsTransferOrder = ObjectContext.PartsTransferOrders.FirstOrDefault(r => r.Id == partsInboundPlan.OriginalRequirementBillId);
                        if(partsTransferOrder != null && !string.IsNullOrEmpty(partsTransferOrder.PurOrderCode))
                            partsInboundCheckBill.PurOrderCode = partsTransferOrder.PurOrderCode;
                    }

                    if(company.Type == (int)DcsCompanyType.分公司 && partsInboundPlan.InboundType == (int)DcsPartsInboundType.销售退货) {
                        //回写索赔单状态
                        var partsClaimOrderNew = ObjectContext.PartsClaimOrderNews.Where(r => r.Id == partsInboundPlan.SourceId).FirstOrDefault();
                        if(partsClaimOrderNew != null && partsClaimOrderNew.Status == (int)DcsPartsClaimBillStatusNew.区域已发运) {
                            partsClaimOrderNew.Status = (int)DcsPartsClaimBillStatusNew.入库完成;
                            partsClaimOrderNew.ModifierId = userInfo.Id;
                            partsClaimOrderNew.ModifierName = userInfo.Name;
                            partsClaimOrderNew.ModifyTime = DateTime.Now;
                            UpdateToDatabase(partsClaimOrderNew);
                        }
                    }

                    //foreach(PartsInboundCheckBillDetail picbd in partsInboundCheckBillDetails) {
                    //    picbd.InspectedQuantity = picbd.ThisInspectedQuantity.Value;
                    //}

                    //var partsAmountDetails = new Dictionary<int, decimal>();
                    //if (company.Type == (int)DcsCompanyType.分公司) {
                    //    var partsPlannedPrice = (from a in ObjectContext.PartsPlannedPrices.Where(r => r.OwnerCompanyId == partsInboundCheckBill.StorageCompanyId && r.PartsSalesCategoryId == partsInboundCheckBill.PartsSalesCategoryId && partIds.Contains(r.SparePartId)).ToArray()
                    //                             select a).ToArray();
                    //    var partsPlannedPriceGroups = partsPlannedPrice.GroupBy(r => r.SparePartId).Where(r => r.Count() >= 2).ToArray();
                    //    if (partsPlannedPriceGroups.Any()) {
                    //        throw new ValidationException(string.Format("配件Id为如下的{0}配件计划价存在多个", partsPlannedPriceGroups.Select(r => r.Key.ToString(CultureInfo.InvariantCulture)).ToArray().Aggregate((a, b) => a += "," + b)));
                    //    }
                    //    partsAmountDetails = partsPlannedPrice.ToDictionary(r => r.SparePartId, r => r.PlannedPrice);
                    //}

                    foreach(var partsInboundCheckBillDetail in partsInboundCheckBillDetails) {
                        //更新配件入库计划清单
                        var partsInboundPlanDetail = partsInboundPlanDetails.SingleOrDefault(v => v.SparePartId == partsInboundCheckBillDetail.SparePartId && v.Price == partsInboundCheckBillDetail.SettlementPrice && (v.InspectedQuantity ?? 0) != v.PlannedAmount);
                        if(partsInboundPlanDetail == null)
                            throw new ValidationException(string.Format(ErrorStrings.PartsInboundCheckBill_Validation6, partsInboundCheckBillDetail.SparePartCode));
                        if(!partsInboundPlanDetail.InspectedQuantity.HasValue) {
                            partsInboundPlanDetail.InspectedQuantity = 0;
                        }
                        partsInboundCheckBillDetail.InspectedQuantity = (partsInboundCheckBillDetail.ThisInspectedQuantity ?? 0);
                        partsInboundCheckBillDetail.OriginalPrice = partsInboundPlanDetail.OriginalPrice;

                        if(partsInboundPlanDetail.InspectedQuantity + (partsInboundCheckBillDetail.ThisInspectedQuantity ?? 0) > partsInboundPlanDetail.PlannedAmount) {
                            throw new ValidationException(ErrorStrings.PartsInboundCheckBill_Validation8);
                        }
                        partsInboundPlanDetail.InspectedQuantity += partsInboundCheckBillDetail.ThisInspectedQuantity ?? 0;
                        //    var partsStock = partsStocks.FirstOrDefault(r => r.PartId == partsInboundCheckBillDetail.SparePartId);

                        //重新计算企业配件成本价
                        var enterprisePartsCost = enterprisePartsCosts == null ? null : enterprisePartsCosts.FirstOrDefault(r => r.SparePartId == partsInboundCheckBillDetail.SparePartId);
                        if(partsInboundCheckBill.InboundType != (int)DcsPartsInboundType.配件调拨) {
                            if(enterprisePartsCost != null) {
                                enterprisePartsCost.Quantity += (partsInboundCheckBillDetail.ThisInspectedQuantity ?? 0);
                                if(partsInboundPlan.InboundType == (int)DcsPartsInboundType.配件采购)
                                    enterprisePartsCost.CostAmount += (partsInboundCheckBillDetail.ThisInspectedQuantity ?? 0) * partsInboundCheckBillDetail.SettlementPrice;
                                else
                                    enterprisePartsCost.CostAmount += (partsInboundCheckBillDetail.ThisInspectedQuantity ?? 0) * enterprisePartsCost.CostPrice;
                                enterprisePartsCost.CostPrice = Math.Round(enterprisePartsCost.CostAmount / enterprisePartsCost.Quantity, 2);
                                UpdateToDatabase(enterprisePartsCost);
                            } else {
                                //if(partsInboundPlan.InboundType != (int)DcsPartsInboundType.配件采购)
                                //    throw new ValidationException(string.Format(ErrorStrings.PartsInboundCheckBill_Validation16, partsInboundCheckBillDetail.SparePartCode));
                                enterprisePartsCost = new EnterprisePartsCost {
                                    OwnerCompanyId = partsInboundCheckBill.StorageCompanyId,
                                    PartsSalesCategoryId = partsInboundCheckBill.PartsSalesCategoryId,
                                    SparePartId = partsInboundCheckBillDetail.SparePartId,
                                    Quantity = (partsInboundCheckBillDetail.ThisInspectedQuantity ?? 0),
                                    CostAmount = (partsInboundCheckBillDetail.ThisInspectedQuantity ?? 0) * partsInboundCheckBillDetail.SettlementPrice,
                                    CostPrice = Math.Round(partsInboundCheckBillDetail.SettlementPrice, 2),
                                    CreatorId = userInfo.Id,
                                    CreatorName = userInfo.Name,
                                    CreateTime = DateTime.Now,
                                };
                                if(partsInboundPlan.InboundType == (int)DcsPartsInboundType.配件采购)
                                    InsertToDatabase(enterprisePartsCost);
                            }
                        }
                        //if(partsStock == null) {
                        //    //新增配件库存
                        //    if(warehouseAreas.Length == 0) {
                        //        throw new ValidationException(ErrorStrings.PartsInboundCheckBill_Validation17);
                        //    }
                        //    var ps = new PartsStock {
                        //        WarehouseId = partsInboundCheckBill.WarehouseId,
                        //        StorageCompanyId = partsInboundCheckBill.StorageCompanyId,
                        //        StorageCompanyType = partsInboundCheckBill.StorageCompanyType,
                        //        BranchId = partsInboundCheckBill.BranchId,
                        //        WarehouseAreaId = warehouseAreas[0].Id,
                        //        PartId = partsInboundCheckBillDetail.SparePartId,
                        //        Quantity = (partsInboundCheckBillDetail.ThisInspectedQuantity ?? 0),
                        //        //Remark = partsInboundCheckBillDetail.Remark,
                        //        WarehouseAreaCategoryId = warehouseAreas[0].AreaCategoryId,
                        //        CreatorName = userInfo.Name,
                        //        CreateTime = DateTime.Now,
                        //        CreatorId = userInfo.Id
                        //    };
                        //    if((partsInboundCheckBillDetail.IsPacking ?? false) == true)
                        //        ps.LockedQty = (partsInboundCheckBillDetail.ThisInspectedQuantity ?? 0);
                        //    InsertToDatabase(ps);
                        //    partsStocks.Add(ps);
                        //    partsInboundCheckBillDetail.WarehouseAreaId = warehouseAreas[0].Id;
                        //    partsInboundCheckBillDetail.WarehouseAreaCode = warehouseAreas[0].Code;
                        //} else {
                        //    //更新配件库存
                        //    partsStock.Quantity = partsStock.Quantity + (partsInboundCheckBillDetail.ThisInspectedQuantity ?? 0);
                        //    if((partsInboundCheckBillDetail.IsPacking ?? false) == true)
                        //        partsStock.LockedQty = (partsStock.LockedQty.HasValue ? partsStock.LockedQty.Value : 0) + (partsInboundCheckBillDetail.ThisInspectedQuantity ?? 0);
                        //    //partsStock.Remark = partsStock.Remark + partsInboundCheckBillDetail.Remark;
                        //    partsStock.ModifyTime = DateTime.Now;
                        //    partsStock.ModifierId = userInfo.Id;
                        //    partsStock.ModifierName = userInfo.Name;
                        //    UpdateToDatabase(partsStock);

                        //    partsInboundCheckBillDetail.WarehouseAreaId = partsStock.WarehouseAreaId;
                        //    partsInboundCheckBillDetail.WarehouseAreaCode = warehouseAreas[0].Code;
                        //}

                        if(company.Type == (int)DcsCompanyType.分公司 || company.Type == (int)DcsCompanyType.代理库 || company.Type == (int)DcsCompanyType.服务站兼代理库) {
                            if(partsInboundCheckBill.InboundType == (int)DcsPartsInboundType.配件调拨) {
                                var tmpPartsShippingOrderRefOutBill = (from b in ObjectContext.PartsOutboundBills
                                                                       join c in ObjectContext.PartsOutboundBillDetails on b.Id equals c.PartsOutboundBillId
                                                                       where b.PartsShippingOrderId == partsInboundPlan.SourceId
                                                                       select new {
                                                                           SparePartId = c.SparePartId,
                                                                           CostPrice = c.CostPrice
                                                                       }).ToArray();
                                if(tmpPartsShippingOrderRefOutBill == null)
                                    throw new ValidationException(ErrorStrings.PartsInboundCheckBill_Validation18);
                                partsInboundCheckBillDetail.CostPrice = tmpPartsShippingOrderRefOutBill.FirstOrDefault(r => r.SparePartId == partsInboundCheckBillDetail.SparePartId).CostPrice;
                            } else if(partsInboundCheckBill.InboundType == (int)DcsPartsInboundType.配件采购) {
                                partsInboundCheckBillDetail.CostPrice = partsInboundPlanDetail.Price;
                            } else {
                                partsInboundCheckBillDetail.CostPrice = enterprisePartsCost.CostPrice;
                            }
                            if(partsInboundCheckBillDetail.CostPrice == 0) {
                                throw new ValidationException(string.Format(ErrorStrings.PartsInboundCheckBill_Validation12,
                                    partsInboundCheckBillDetail.SparePartCode));
                            }
                        }
                    }
                    var sparepartsId = partsInboundCheckBillDetails.Select(t => t.SparePartId).Distinct().ToArray();
                    foreach(var spId in sparepartsId) {
                        var partsStock = partsStocks.FirstOrDefault(r => r.PartId == spId);
                        var qty = partsInboundCheckBillDetails.Where(t => t.SparePartId == spId).Sum(t => t.ThisInspectedQuantity);
                        var detail = partsInboundCheckBillDetails.Where(t => t.SparePartId == spId).ToArray();
                        var lockQty = partsInboundCheckBillDetails.Where(t => t.IsPacking == true && t.SparePartId == spId).Sum(t => t.ThisInspectedQuantity);
                        if(partsStock == null) {
                            //新增配件库存
                            if(warehouseAreas.Length == 0) {
                                throw new ValidationException(ErrorStrings.PartsInboundCheckBill_Validation17);
                            }
                            var ps = new PartsStock {
                                WarehouseId = partsInboundCheckBill.WarehouseId,
                                StorageCompanyId = partsInboundCheckBill.StorageCompanyId,
                                StorageCompanyType = partsInboundCheckBill.StorageCompanyType,
                                BranchId = partsInboundCheckBill.BranchId,
                                WarehouseAreaId = warehouseAreas[0].Id,
                                PartId = spId,
                                Quantity = qty.Value,
                                //Remark = partsInboundCheckBillDetail.Remark,
                                WarehouseAreaCategoryId = warehouseAreas[0].AreaCategoryId,
                                CreatorName = userInfo.Name,
                                CreateTime = DateTime.Now,
                                CreatorId = userInfo.Id
                            };
                            ps.LockedQty = lockQty.Value;
                            InsertToDatabase(ps);
                            partsStocks.Add(ps);
                            foreach(var item in detail) {
                                item.WarehouseAreaId = warehouseAreas[0].Id;
                                item.WarehouseAreaCode = warehouseAreas[0].Code;
                            }
                        } else {
                            //更新配件库存
                            partsStock.Quantity = partsStock.Quantity + qty.Value;
                            partsStock.LockedQty = (partsStock.LockedQty.HasValue ? partsStock.LockedQty.Value : 0) + (lockQty.Value);
                            partsStock.ModifyTime = DateTime.Now;
                            partsStock.ModifierId = userInfo.Id;
                            partsStock.ModifierName = userInfo.Name;
                            UpdateToDatabase(partsStock);
                            foreach(var item in detail) {
                                item.WarehouseAreaId = partsStock.WarehouseAreaId;
                                item.WarehouseAreaCode = warehouseAreas[0].Code;
                            }
                        }
                    }
                    InsertToDatabase(partsInboundCheckBill);
                    //批量更新或者新增配件库存
                    var checkPartIds = partsStocks.Select(r => r.PartId);
                    var checkWarehouseAreaIds = partsStocks.Select(r => r.WarehouseAreaId);
                    var checkPartsStocks = ObjectContext.PartsStocks.Where(r => checkPartIds.Contains(r.PartId) && checkWarehouseAreaIds.Contains(r.WarehouseAreaId)).SetMergeOption(MergeOption.NoTracking).ToArray();
                    foreach(var item in partsStocks) {
                        //数量要大于0
                        if(item.Quantity < 0) {
                            throw new ValidationException(ErrorStrings.PartsStock_Validation5);
                        }
                        //新增修改的唯一性校验
                        switch(item.EntityState) {
                            case EntityState.Modified:
                                var checkExists = checkPartsStocks.FirstOrDefault(r => r.Id == item.Id);
                                if(checkExists == null) {
                                    throw new ValidationException(ErrorStrings.PartsStock_Validation6);
                                }
                                var checkOnlyOneForUpdate = checkPartsStocks.SingleOrDefault(r => r.Id != item.Id && r.PartId == item.PartId && r.WarehouseAreaId == item.WarehouseAreaId);
                                if(checkOnlyOneForUpdate != null) {
                                    throw new ValidationException(ErrorStrings.PartsStock_Validation7);
                                }
                                item.ModifierId = userInfo.Id;
                                item.ModifierName = userInfo.Name;
                                item.ModifyTime = DateTime.Now;
                                break;
                            case EntityState.Added:
                                var checkOnlyOneForInsert = checkPartsStocks.SingleOrDefault(r => r.PartId == item.PartId && r.WarehouseAreaId == item.WarehouseAreaId);
                                if(checkOnlyOneForInsert != null) {
                                    throw new ValidationException(ErrorStrings.PartsStock_Validation1);
                                }
                                item.CreatorId = userInfo.Id;
                                item.CreatorName = userInfo.Name;
                                item.CreateTime = DateTime.Now;
                                break;
                        }
                    }

                    //bool isAllCheck = partsInboundPlanDetails.All(partsInboundPlanDetail => partsInboundPlanDetail.PlannedAmount == partsInboundPlanDetail.InspectedQuantity);

                    ////更新入库计划单状态
                    //if(isAllCheck) {
                    //    partsInboundPlan.Status = (int)DcsPartsInboundPlanStatus.检验完成;
                    //} else {
                    //    partsInboundPlan.Status = (int)DcsPartsInboundPlanStatus.部分检验;
                    //}

                    foreach(var partsInboundPlanDetail in partsInboundPlanDetails) {
                        UpdateToDatabase(partsInboundPlanDetail);
                    }
                    new PartsInboundPlanAch(this.DomainService).更新入库计划状态(partsInboundPlan);

                    this.InsertPartsInboundCheckBillValidate(partsInboundCheckBill);

                    if(partsInboundCheckBill.OriginalRequirementBillType == (int)DcsOriginalRequirementBillType.配件销售退货单) {
                        var partsSaleReturnBill = this.ObjectContext.PartsSalesReturnBills.Where(r => r.Id == partsInboundCheckBill.OriginalRequirementBillId).FirstOrDefault();
                        if(partsSaleReturnBill != null) {
                            if(partsSaleReturnBill.IsBarter == true)
                                partsInboundCheckBill.SettlementStatus = (int)DcsPartsSettlementStatus.已结算;
                            else
                                partsInboundCheckBill.SettlementStatus = (int)DcsPartsSettlementStatus.待结算;
                        }
                    }

                    var batchNumber = CodeGenerator.Generate("PartsBatchRecord", partsInboundPlan.WarehouseCode);
                    foreach(var partsInboundCheckBillDetail in partsInboundCheckBillDetails) {
                        partsInboundCheckBillDetail.BatchNumber = batchNumber;
                        var sparePart = spareParts.Where(t => t.Id == partsInboundCheckBillDetail.SparePartId).First();
                        if (company.Type == (int)DcsCompanyType.分公司 && (partsInboundCheckBillDetail.IsPacking ?? false) == true && warehouse.Name != "马钢库" && sparePart.PartType != (int)DcsSparePartPartType.包材)
                        {
                            //2020-01-07 追溯码，开放重庆库的 包装单
                            //if(company.Type == (int)DcsCompanyType.分公司 && (partsInboundCheckBillDetail.IsPacking ?? false) == true && warehouse.Name != "重庆库" && warehouse.Name != "马钢库") {
                            var partsPacking = new PackingTask {
                                Code = CodeGenerator.Generate("PartsPacking", userInfo.EnterpriseCode),
                                PartsInboundPlanId = partsInboundPlan.Id,
                                PartsInboundPlanCode = partsInboundPlan.Code,
                                PartsInboundCheckBillId = partsInboundCheckBill.Id,
                                PartsInboundCheckBillCode = partsInboundCheckBill.Code,
                                SparePartId = partsInboundCheckBillDetail.SparePartId,
                                SparePartCode = partsInboundCheckBillDetail.SparePartCode,
                                SparePartName = partsInboundCheckBillDetail.SparePartName,
                                PlanQty = partsInboundCheckBillDetail.ThisInspectedQuantity.Value,
                                SourceCode = partsInboundPlan.OriginalRequirementBillCode,
                                Status = (int)DcsPackingTaskStatus.新增,
                                CounterpartCompanyCode = partsInboundCheckBill.CounterpartCompanyCode,
                                CounterpartCompanyName = partsInboundCheckBill.CounterpartCompanyName,
                                ExpectedPlaceDate = partsInboundPlan.PlanDeliveryTime,
                                CreatorId = userInfo.Id,
                                CreatorName = userInfo.Name,
                                CreateTime = DateTime.Now,
                                WarehouseId = partsInboundCheckBill.WarehouseId,
                                WarehouseCode = partsInboundCheckBill.WarehouseCode,
                                WarehouseName = partsInboundCheckBill.WarehouseName,
                            };
                            if(partsInboundCheckBill.InboundType == (int)DcsPartsInboundType.配件采购 || partsInboundCheckBill.InboundType == (int)DcsPartsInboundType.配件调拨) {
                                partsPacking.BatchNumber = batchNumber;
                            }
                            InsertToDatabase(partsPacking);
                        } else if(company.Type == (int)DcsCompanyType.分公司) {
                            var partsShelvesTask = new PartsShelvesTask {
                                Code = CodeGenerator.Generate("PartsShelvesTask", userInfo.EnterpriseCode),
                                PartsInboundPlanId = partsInboundPlan.Id,
                                PartsInboundPlanCode = partsInboundPlan.Code,
                                PartsInboundCheckBillId = partsInboundCheckBill.Id,
                                PartsInboundCheckBillCode = partsInboundCheckBill.Code,
                                WarehouseId = partsInboundCheckBill.WarehouseId,
                                WarehouseCode = partsInboundCheckBill.WarehouseCode,
                                WarehouseName = partsInboundCheckBill.WarehouseName,
                                SparePartId = partsInboundCheckBillDetail.SparePartId,
                                //SparePartCode = partsInboundCheckBillDetail.SparePartCode,
                                //SparePartName = partsInboundCheckBillDetail.SparePartName,
                                PlannedAmount = partsInboundCheckBillDetail.ThisInspectedQuantity,
                                SourceBillCode = partsInboundPlan.OriginalRequirementBillCode,
                                Status = (int)DcsShelvesTaskStatus.新增,
                                CreatorId = userInfo.Id,
                                CreatorName = userInfo.Name,
                                CreateTime = DateTime.Now,
                            };
                            if(partsInboundCheckBill.InboundType == (int)DcsPartsInboundType.配件采购 || partsInboundCheckBill.InboundType == (int)DcsPartsInboundType.配件调拨) {
                                partsShelvesTask.BatchNumber = batchNumber;
                            }
                            InsertToDatabase(partsShelvesTask);
                        } else {
                            var partsShelvesTask = new PartsShelvesTask {
                                Code = CodeGenerator.Generate("PartsShelvesTask", userInfo.EnterpriseCode),
                                PartsInboundPlanId = partsInboundPlan.Id,
                                PartsInboundPlanCode = partsInboundPlan.Code,
                                PartsInboundCheckBillId = partsInboundCheckBill.Id,
                                PartsInboundCheckBillCode = partsInboundCheckBill.Code,
                                WarehouseId = partsInboundCheckBill.WarehouseId,
                                WarehouseCode = partsInboundCheckBill.WarehouseCode,
                                WarehouseName = partsInboundCheckBill.WarehouseName,
                                SparePartId = partsInboundCheckBillDetail.SparePartId,
                                //SparePartCode = partsInboundCheckBillDetail.SparePartCode,
                                //SparePartName = partsInboundCheckBillDetail.SparePartName,
                                PlannedAmount = partsInboundCheckBillDetail.ThisInspectedQuantity,
                                SourceBillCode = partsInboundPlan.OriginalRequirementBillCode,
                                Status = (int)DcsShelvesTaskStatus.新增,
                                CreatorId = userInfo.Id,
                                CreatorName = userInfo.Name,
                                CreateTime = DateTime.Now,
                            };
                            if(partsInboundCheckBill.InboundType == (int)DcsPartsInboundType.配件采购 || partsInboundCheckBill.InboundType == (int)DcsPartsInboundType.配件调拨) {
                                partsShelvesTask.BatchNumber = batchNumber;
                            }
                            InsertToDatabase(partsShelvesTask);
                        }

                        var partsInboundPlanDetail = partsInboundPlanDetails.FirstOrDefault(o => o.PartsInboundPlanId == partsInboundCheckBill.PartsInboundPlanId && partsInboundCheckBillDetail.SparePartId == o.SparePartId);
                        partsInboundPlanDetail.BatchNumber = batchNumber;
                        UpdateToDatabase(partsInboundPlanDetail);
                    }
                    //更新供应商发运单
                    var supplierShippingOrder = ObjectContext.SupplierShippingOrders.Where(t => t.Code == partsInboundPlan.SourceCode && t.Id == partsInboundPlan.SourceId).FirstOrDefault();
                    if(supplierShippingOrder != null && supplierShippingOrder.LogisticArrivalDate == null) {
                        supplierShippingOrder.LogisticArrivalDate = DateTime.Now;
                        supplierShippingOrder.ArriveMethod = (int)DCSSupplierShippingOrderArriveMethod.默认到货;
                        supplierShippingOrder.ComfirmStatus = (int)DCSSupplierShippingOrderComfirmStatus.入库确认;                       
                    }
                    if (supplierShippingOrder.InboundTime == null)
                    {
                        supplierShippingOrder.InboundTime = DateTime.Now;
                    }
                    ObjectContext.SaveChanges();
                    //收货管理，当入库类型=销售退货、调拨时，
                    //如果选择是否包装=否，默认将原本出库的追溯信息保留，原本的入库追溯信息变更为在库，可再次出库（采用原来的SIH标签码）
                    if(partsInboundCheckBill.InboundType == (int)DcsPartsInboundType.销售退货 || partsInboundCheckBill.InboundType == (int)DcsPartsInboundType.配件调拨) {
                        var accurateTraces = ObjectContext.AccurateTraces.Where(t => t.SourceBillId == partsInboundCheckBill.PartsInboundPlanId && t.Type == (int)DCSAccurateTraceType.入库).ToArray();
                        if(accurateTraces.Count() > 0) {
                            var traceCodes = accurateTraces.Select(r => r.TraceCode).ToArray();
                            var outAccurateTraces = ObjectContext.AccurateTraces.Where(t => t.Type == (int)DCSAccurateTraceType.出库 && traceCodes.Contains(t.TraceCode) && t.Status == (int)DCSAccurateTraceStatus.有效).ToArray();
                            foreach(var partsInboundCheckBillDetail in partsInboundCheckBillDetails) {
                                var inboundQty = partsInboundCheckBillDetail.InspectedQuantity;
                                var upQty = 0;
                                if(!partsInboundCheckBillDetail.IsPacking.Value) {
                                    var inAccurateTraces = accurateTraces.Where(t => t.PartId == partsInboundCheckBillDetail.SparePartId).ToArray();
                                    foreach(var item in inAccurateTraces) {
                                        if(inboundQty == upQty)
                                            continue;
                                        var outTrace = outAccurateTraces.FirstOrDefault(t => t.TraceCode == item.TraceCode && t.PartId == item.PartId);
                                        if(outTrace != null) {
                                            //item.SIHLabelCode = outTrace.SIHLabelCode;
                                            //item.BoxCode = outTrace.BoxCode;
                                            //item.InQty = partsInboundCheckBillDetail.InspectedQuantity;
                                            //item.ModifierId = userInfo.Id;
                                            //item.ModifierName = userInfo.Name;
                                            //item.ModifyTime = DateTime.Now;
                                            //UpdateToDatabase(item);
                                            var newAccuratrace = new AccurateTrace {
                                                Type = (int)DCSAccurateTraceType.入库,
                                                CompanyId = item.CompanyId,
                                                CompanyType = item.CompanyType,
                                                SourceBillId = partsInboundCheckBill.Id,
                                                PartId = item.PartId,
                                                TraceCode = item.TraceCode,
                                                SIHLabelCode = outTrace.SIHLabelCode,
                                                Status = (int)DCSAccurateTraceStatus.有效,
                                                CreatorId = userInfo.Id,
                                                CreateTime = DateTime.Now,
                                                CreatorName = userInfo.Name,
                                                TraceProperty = item.TraceProperty,
                                                BoxCode = outTrace.BoxCode,
                                            };
                                            InsertToDatabase(newAccuratrace);
                                        }
                                        upQty++;
                                    }
                                }

                            }

                        }
                    }
                } catch(OptimisticConcurrencyException) {
                    throw new ValidationException("收货确认失败，请重新查询");
                }
                ObjectContext.SaveChanges();
                transaction.Complete();
            }
        }
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:11:45
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 生成配件入库检验单(PartsInboundCheckBill partsInboundCheckBill) {
            new PartsInboundCheckBillAch(this).生成配件入库检验单(partsInboundCheckBill);
        }


        [Update(UsingCustomMethod = true)]
        public void 更改不结算状态(PartsInboundCheckBill partsInboundCheckBill) {
            new PartsInboundCheckBillAch(this).更改不结算状态(partsInboundCheckBill);
        }


        [Update(UsingCustomMethod = true)]
        public void 更改待结算状态(PartsInboundCheckBill partsInboundCheckBill) {
            new PartsInboundCheckBillAch(this).更改待结算状态(partsInboundCheckBill);
        }

        [Update(UsingCustomMethod = true)]
        public void 生成配件收货单(PartsInboundCheckBill partsInboundCheckBill) {
            new PartsInboundCheckBillAch(this).生成配件收货单(partsInboundCheckBill);
        }
    }
}
