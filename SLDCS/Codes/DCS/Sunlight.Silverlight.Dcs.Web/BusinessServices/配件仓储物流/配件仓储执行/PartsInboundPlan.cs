﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsInboundPlanAch : DcsSerivceAchieveBase {
        public PartsInboundPlanAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 终止配件入库计划(PartsInboundPlan partsInboundPlan) {
            var dbpartsInboundPlan = ObjectContext.PartsInboundPlans.Where(r => r.Id == partsInboundPlan.Id && (r.Status == (int)DcsPartsInboundPlanStatus.新建 || r.Status == (int)DcsPartsInboundPlanStatus.部分检验)).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbpartsInboundPlan == null)
                throw new ValidationException(ErrorStrings.PartsInboundPlan_Validation1);
            UpdateToDatabase(partsInboundPlan);
            partsInboundPlan.Status = (int)DcsPartsInboundPlanStatus.终止;
            this.UpdatePartsInboundPlanValidate(partsInboundPlan);
            if(partsInboundPlan.OriginalRequirementBillType == (int)DcsOriginalRequirementBillType.配件销售退货单) {
                var company = ObjectContext.Companies.Single(r => r.Id == partsInboundPlan.CounterpartCompanyId);
                var partsSalesReturnBill = ObjectContext.PartsSalesReturnBills.Single(o => o.Id == partsInboundPlan.OriginalRequirementBillId);
                var partIds = partsSalesReturnBill.PartsSalesReturnBillDetails.Select(o => o.SparePartId).Distinct();
                if(company.Type == (int)DcsCompanyType.代理库 || company.Type == (int)DcsCompanyType.服务站兼代理库) {
                    var warehouseAreaCategoryIds = ObjectContext.WarehouseAreaCategories.Where(r => r.Category == (int)DcsAreaType.保管区).Select(v => v.Id).ToArray();
                    if(warehouseAreaCategoryIds.Length == 0)
                        return;

                    var partsStocks = new PartsStockAch(this.DomainService).GetPartsStocksWithLock(ObjectContext.PartsStocks.Where(r => partIds.Contains(r.PartId) && warehouseAreaCategoryIds.Contains(r.WarehouseAreaCategoryId.Value) && r.WarehouseId == partsSalesReturnBill.ReturnWarehouseId)).ToList();
                    var enterprisePartsCosts = new EnterprisePartsCostAch(this.DomainService).GetEnterprisePartsCostsWithLock(ObjectContext.EnterprisePartsCosts.Where(r => r.OwnerCompanyId == partsInboundPlan.CounterpartCompanyId && partIds.Contains(r.SparePartId))).ToList();
                    foreach(var partsSalesReturnBillDetail in partsSalesReturnBill.PartsSalesReturnBillDetails) {                       
                        var partsInboundPlanDetail = partsInboundPlan.PartsInboundPlanDetails.FirstOrDefault(o => o.SparePartId == partsSalesReturnBillDetail.SparePartId);
                        if(partsInboundPlanDetail == null||(partsInboundPlanDetail.PlannedAmount - (partsInboundPlanDetail.InspectedQuantity ?? 0)) == 0)
                            continue;
                        //审批数量等于 退货数量 - 待检验入库量
                        partsSalesReturnBillDetail.ApproveQuantity = partsSalesReturnBillDetail.ReturnedQuantity - partsInboundPlanDetail.PlannedAmount + (partsInboundPlanDetail.InspectedQuantity ?? 0);
                        partsSalesReturnBillDetail.ReturnedQuantity = partsSalesReturnBillDetail.ReturnedQuantity - partsInboundPlanDetail.PlannedAmount + (partsInboundPlanDetail.InspectedQuantity ?? 0);
                        UpdateToDatabase(partsSalesReturnBillDetail);
                        //更新目标仓库，库存数量
                        var partsStock = partsStocks.FirstOrDefault(r => r.PartId == partsSalesReturnBillDetail.SparePartId);
                        if(partsStock == null)
                            throw new ValidationException(string.Format(ErrorStrings.PartsInboundPlan_Validation2, partsSalesReturnBillDetail.SparePartCode));
                        else {
                            partsStock.Quantity += partsInboundPlanDetail.PlannedAmount - (partsInboundPlanDetail.InspectedQuantity ?? 0);
                            new PartsStockAch(this.DomainService).UpdatePartsStockValidate(partsStock);
                            UpdateToDatabase(partsStock);
                        }
                        //更新目标单位，企业库存，重新计算成本金额
                        var enterprisePartsCost = enterprisePartsCosts.FirstOrDefault(r => r.SparePartId == partsSalesReturnBillDetail.SparePartId);
                        if(enterprisePartsCost == null)
                            throw new ValidationException(string.Format(ErrorStrings.PartsInboundPlan_Validation3, partsSalesReturnBillDetail.SparePartCode));
                        else {
                            enterprisePartsCost.Quantity += partsInboundPlanDetail.PlannedAmount - (partsInboundPlanDetail.InspectedQuantity ?? 0);
                            enterprisePartsCost.CostAmount += (partsInboundPlanDetail.PlannedAmount - (partsInboundPlanDetail.InspectedQuantity ?? 0)) * enterprisePartsCost.CostPrice;
                            UpdateToDatabase(enterprisePartsCost);
                        }
                    }
                } else if(company.Type == (int)DcsCompanyType.服务站) {
                    var dealerPartsStocks = ObjectContext.DealerPartsStocks.Where(r => partIds.Contains(r.SparePartId) && r.DealerId == partsInboundPlan.CounterpartCompanyId && r.SalesCategoryId == partsInboundPlan.PartsSalesCategoryId).ToArray();
                    foreach(var partsSalesReturnBillDetail in partsSalesReturnBill.PartsSalesReturnBillDetails) {
                        var partsInboundPlanDetail = partsInboundPlan.PartsInboundPlanDetails.FirstOrDefault(o => o.SparePartId == partsSalesReturnBillDetail.SparePartId);
                        if(partsInboundPlanDetail == null||(partsInboundPlanDetail.PlannedAmount - (partsInboundPlanDetail.InspectedQuantity ?? 0)) == 0)
                            continue;
                        //审批数量等于 退货数量 - 待检验入库量
                        partsSalesReturnBillDetail.ApproveQuantity = partsSalesReturnBillDetail.ReturnedQuantity - partsInboundPlanDetail.PlannedAmount + (partsInboundPlanDetail.InspectedQuantity ?? 0);
                        partsSalesReturnBillDetail.ReturnedQuantity = partsSalesReturnBillDetail.ReturnedQuantity - partsInboundPlanDetail.PlannedAmount + (partsInboundPlanDetail.InspectedQuantity ?? 0);
                        UpdateToDatabase(partsSalesReturnBillDetail);

                        //更新目标仓库，库存数量
                        var dealerPartsStock = dealerPartsStocks.FirstOrDefault(r => r.SparePartId == partsSalesReturnBillDetail.SparePartId);
                        if(dealerPartsStock == null)
                            throw new ValidationException(string.Format(ErrorStrings.PartsInboundPlan_Validation4, partsSalesReturnBillDetail.SparePartCode));
                        else {
                            dealerPartsStock.Quantity += partsInboundPlanDetail.PlannedAmount - (partsInboundPlanDetail.InspectedQuantity ?? 0);
                            new DealerPartsStockAch(this.DomainService).UpdateDealerPartsStockValidate(dealerPartsStock);
                            UpdateToDatabase(dealerPartsStock);
                        }
                    }
                }
            }
        }

        public void 更新入库计划状态(PartsInboundPlan partsInboundPlan) {
            // 计算检验状态
            // billPart 部分检验的检验入库清单   billComplete检验完成的检验入库清单
            var partsInboundPlanDetails = ObjectContext.PartsInboundPlanDetails.Where(r => r.PartsInboundPlanId == partsInboundPlan.Id).ToArray();
            var plannedAmount = partsInboundPlanDetails.Sum(o => o.PlannedAmount);
            var inspectedQuantity = partsInboundPlanDetails.Sum(o => o.InspectedQuantity);

            // packingPart 部分包装的包装任务单   packingComplete包装完成的包装任务单
            var packingTasks = ObjectContext.PackingTasks.Where(r => r.PartsInboundPlanId == partsInboundPlan.Id).ToArray();
            var planQty = packingTasks.Sum(o => o.PlanQty);
            var packingQty = packingTasks.Sum(o => o.PackingQty);

            var partsShelvesTasks = ObjectContext.PartsShelvesTasks.Where(r => r.PartsInboundPlanId == partsInboundPlan.Id).ToArray();
            var plannedAmountShelves = partsShelvesTasks.Sum(o => o.PlannedAmount);
            var shelvesAmount = partsShelvesTasks.Sum(o => o.ShelvesAmount);

            if(plannedAmount == inspectedQuantity && plannedAmount == (shelvesAmount ?? 0))
                partsInboundPlan.Status = (int)DcsPartsInboundPlanStatus.上架完成;
            else if(plannedAmount == inspectedQuantity && plannedAmount > (shelvesAmount ?? 0) && (shelvesAmount ?? 0) > 0 && planQty == (packingQty ?? 0))
                partsInboundPlan.Status = (int)DcsPartsInboundPlanStatus.部分上架;
            else if(plannedAmount == inspectedQuantity && plannedAmount == (packingQty ?? 0))
                partsInboundPlan.Status = (int)DcsPartsInboundPlanStatus.包装完成;
            else if(plannedAmount == inspectedQuantity && plannedAmount > (packingQty ?? 0) && (packingQty ?? 0) > 0)
                partsInboundPlan.Status = (int)DcsPartsInboundPlanStatus.部分包装;
            else if(plannedAmount > (inspectedQuantity ?? 0) && (inspectedQuantity ?? 0) > 0)
                partsInboundPlan.Status = (int)DcsPartsInboundPlanStatus.部分检验;
            else if(plannedAmount == (inspectedQuantity ?? 0))
                partsInboundPlan.Status = (int)DcsPartsInboundPlanStatus.检验完成;
            else if(inspectedQuantity == 0)
                partsInboundPlan.Status = (int)DcsPartsInboundPlanStatus.新建;
            new PartsInboundPlanAch(this.DomainService).UpdatePartsInboundPlanValidate(partsInboundPlan);
            ObjectContext.SaveChanges();
        }

        public void 批量收货(int[] ids) {
            var partsInboundPlans = ObjectContext.PartsInboundPlans.Where(o => ids.Contains(o.Id));

            foreach(var partsInboundPlan in partsInboundPlans) {
                var partsInboundCheckBill = new PartsInboundCheckBill();
                partsInboundCheckBill.WarehouseName = partsInboundPlan.WarehouseName;
                partsInboundCheckBill.InboundType = partsInboundPlan.InboundType;
                partsInboundCheckBill.WarehouseCode = partsInboundPlan.WarehouseCode;
                partsInboundCheckBill.Code = GlobalVar.ASSIGNED_BY_SERVER;
                partsInboundCheckBill.PartsInboundPlanId = partsInboundPlan.Id;
                partsInboundCheckBill.WarehouseId = partsInboundPlan.WarehouseId;
                partsInboundCheckBill.StorageCompanyId = partsInboundPlan.StorageCompanyId;
                partsInboundCheckBill.StorageCompanyCode = partsInboundPlan.StorageCompanyCode;
                partsInboundCheckBill.StorageCompanyName = partsInboundPlan.StorageCompanyName;
                partsInboundCheckBill.StorageCompanyType = partsInboundPlan.StorageCompanyType;
                partsInboundCheckBill.BranchId = partsInboundPlan.BranchId;
                partsInboundCheckBill.BranchCode = partsInboundPlan.BranchCode;
                partsInboundCheckBill.BranchName = partsInboundPlan.BranchName;
                partsInboundCheckBill.CounterpartCompanyId = partsInboundPlan.CounterpartCompanyId;
                partsInboundCheckBill.CounterpartCompanyCode = partsInboundPlan.CounterpartCompanyCode;
                partsInboundCheckBill.CounterpartCompanyName = partsInboundPlan.CounterpartCompanyName;
                partsInboundCheckBill.CustomerAccountId = partsInboundPlan.CustomerAccountId;
                partsInboundCheckBill.OriginalRequirementBillId = partsInboundPlan.OriginalRequirementBillId;
                partsInboundCheckBill.OriginalRequirementBillCode = partsInboundPlan.OriginalRequirementBillCode;
                partsInboundCheckBill.OriginalRequirementBillType = partsInboundPlan.OriginalRequirementBillType;
                partsInboundCheckBill.Status = (int)DcsPartsInboundCheckBillStatus.新建;
                partsInboundCheckBill.SettlementStatus = (int)DcsPartsSettlementStatus.待结算;
                partsInboundCheckBill.GPMSPurOrderCode = partsInboundPlan.GPMSPurOrderCode;
                partsInboundCheckBill.PartsSalesCategoryId = partsInboundPlan.PartsSalesCategoryId;
                partsInboundCheckBill.PartsSalesCategory = partsInboundPlan.PartsSalesCategory;
                partsInboundCheckBill.SAPPurchasePlanCode = partsInboundPlan.SAPPurchasePlanCode;//SAP采购计划单号
                partsInboundCheckBill.ERPSourceOrderCode = partsInboundPlan.ERPSourceOrderCode;

                foreach(var entityDetail in partsInboundPlan.PartsInboundPlanDetails.Where(p => p.PlannedAmount - (p.InspectedQuantity ?? 0) > 0)) {
                    partsInboundCheckBill.PartsInboundCheckBillDetails.Add(new PartsInboundCheckBillDetail {
                        PartsInboundCheckBill = partsInboundCheckBill,
                        SparePartId = entityDetail.SparePartId,
                        SparePartCode = entityDetail.SparePartCode,
                        SparePartName = entityDetail.SparePartName,
                        SettlementPrice = entityDetail.Price,
                        POCode = entityDetail.POCode,
                        WarehouseAreaId = default(int),
                        WarehouseAreaCode = "-",
                        InspectedQuantity = (entityDetail.InspectedQuantity.HasValue ? entityDetail.InspectedQuantity.Value : default(int)),
                        IsPacking = true,
                        ThisInspectedQuantity = entityDetail.PlannedAmount - (entityDetail.InspectedQuantity.HasValue ? entityDetail.InspectedQuantity.Value : default(int)),
                    });
                };
                new PartsInboundCheckBillAch(this.DomainService).InsertPartsInboundCheckBillValidate(partsInboundCheckBill);
                new PartsInboundCheckBillAch(this.DomainService).生成配件收货单(partsInboundCheckBill);
                ObjectContext.SaveChanges();
            }
        }
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:11:45
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 终止配件入库计划(PartsInboundPlan partsInboundPlan) {
            new PartsInboundPlanAch(this).终止配件入库计划(partsInboundPlan);
        }

        [Update(UsingCustomMethod = true)]
        public void 更新入库计划状态(PartsInboundPlan partsInboundPlan) {
            new PartsInboundPlanAch(this).更新入库计划状态(partsInboundPlan);
        }

        [Invoke(HasSideEffects = true)]
        public void 批量收货(int[] ids) {
            new PartsInboundPlanAch(this).批量收货(ids);
        }
    }
}
