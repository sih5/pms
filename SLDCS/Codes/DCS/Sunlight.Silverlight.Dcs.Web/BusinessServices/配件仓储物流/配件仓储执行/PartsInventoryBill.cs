﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
using System.Collections.Generic;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        //总部盘点比中心库盘点少一步操作：高级审核
        #region SIH配件盘点
        [Update(UsingCustomMethod = true)]
        public void 总部终审驳回配件盘点单(PartsInventoryBill partsInventoryBill) {
            var dbpartsInventoryBill = ObjectContext.PartsInventoryBills.Where(r => r.Status == (int)DcsPartsInventoryBillStatus.审核通过 && r.Id == partsInventoryBill.Id).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbpartsInventoryBill == null)
                throw new ValidationException(ErrorStrings.CredenceApplication_Approve_Auti);
            var warehouseAreaIds = partsInventoryBill.PartsInventoryDetails.Select(r => r.WarehouseAreaId);
            //判断库位是否有效
            var warehouseAreaStatus = ObjectContext.WarehouseAreas.Where(r => warehouseAreaIds.Contains(r.Id) && r.Status == (int)DcsBaseDataStatus.作废).ToArray();
            if(warehouseAreaStatus.Count() > 0) {
                throw new ValidationException(string.Format(ErrorStrings.PartsInventoryBill_Validation36, warehouseAreaStatus.First().Code));
            }
            var userinfo = Utils.GetCurrentUserInfo();
            if(dbpartsInventoryBill == null)
                throw new ValidationException("只能驳回“审核通过”的配件盘点单");
            UpdateToDatabase(partsInventoryBill);
			//驳回时更新盘点后库存为0；删除追溯码临时表
            this.rejectBill(partsInventoryBill);
            partsInventoryBill.Status = (int)DcsPartsInventoryBillStatus.新建;
            partsInventoryBill.ApprovalId = userinfo.Id;
            partsInventoryBill.ApproverName = userinfo.Name;
            partsInventoryBill.ApproveTime = System.DateTime.Now;
            partsInventoryBill.RejecterId = userinfo.Id;
            partsInventoryBill.RejecterName = userinfo.Name;
            partsInventoryBill.RejectTime = DateTime.Now;
            UpdatePartsInventoryBillValidate(partsInventoryBill);
        }
        [Update(UsingCustomMethod = true)]
        public void 总部审批配件盘点单(PartsInventoryBill partsInventoryBill) {
            var dbpartsInventoryBill = ObjectContext.PartsInventoryBills.Where(r => r.Status == (int)DcsPartsInventoryBillStatus.审核通过 && r.Id == partsInventoryBill.Id).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbpartsInventoryBill == null)
                throw new ValidationException("只能审批“审核通过”状态下的单据");
            var warehouseAreaIds = partsInventoryBill.PartsInventoryDetails.Select(r => r.WarehouseAreaId);
            //判断库位是否有效
            var warehouseAreaStatus = ObjectContext.WarehouseAreas.Where(r => warehouseAreaIds.Contains(r.Id) && r.Status == (int)DcsBaseDataStatus.作废).ToArray();
            if(warehouseAreaStatus.Count() > 0) {
                throw new ValidationException(string.Format(ErrorStrings.PartsInventoryBill_Validation36, warehouseAreaStatus.First().Code));
            }
            var userinfo = Utils.GetCurrentUserInfo();
            UpdateToDatabase(partsInventoryBill);
            partsInventoryBill.Status = (int)DcsPartsInventoryBillStatus.审批通过;
            partsInventoryBill.ApproverId = userinfo.Id;
            partsInventoryBill.ApproverName = userinfo.Name;
            partsInventoryBill.ApproveTime = System.DateTime.Now;
            UpdatePartsInventoryBillValidate(partsInventoryBill);
        }
        [Update(UsingCustomMethod = true)]
        public void 总部初审配件盘点单(PartsInventoryBill partsInventoryBill) {
            var dbpartsInventoryBill = ObjectContext.PartsInventoryBills.Include("PartsInventoryDetails").Where(r => r.Status == (int)DcsPartsInventoryBillStatus.已结果录入 && r.Id == partsInventoryBill.Id).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbpartsInventoryBill == null)
                throw new ValidationException(ErrorStrings.PartsInventoryBill_Validation2);
			if(dbpartsInventoryBill.PartsInventoryDetails.Any(t=>t.CostPrice==null)){
				throw new ValidationException("存在还未结果录入的配件，请先结果录入");
			}
            var warehouseAreaIds = partsInventoryBill.PartsInventoryDetails.Select(r => r.WarehouseAreaId);
            //判断库位是否有效
            var warehouseAreaStatus = ObjectContext.WarehouseAreas.Where(r => warehouseAreaIds.Contains(r.Id) && r.Status == (int)DcsBaseDataStatus.作废).ToArray();
            if(warehouseAreaStatus.Count() > 0) {
                throw new ValidationException(string.Format(ErrorStrings.PartsInventoryBill_Validation36, warehouseAreaStatus.First().Code));
            }
            var userinfo = Utils.GetCurrentUserInfo();
            UpdateToDatabase(partsInventoryBill);

            partsInventoryBill.Status = (int)DcsPartsInventoryBillStatus.初审通过;

            partsInventoryBill.InitialApproverId = userinfo.Id;
            partsInventoryBill.InitialApproverName = userinfo.Name;
            partsInventoryBill.InitialApproveTime = System.DateTime.Now;
            UpdatePartsInventoryBillValidate(partsInventoryBill);
        }

        [Update(UsingCustomMethod = true)]
        public void 总部审核配件盘点单(PartsInventoryBill partsInventoryBill) {
            var dbpartsInventoryBill = ObjectContext.PartsInventoryBills.Where(r => r.Status == (int)DcsPartsInventoryBillStatus.初审通过 && r.Id == partsInventoryBill.Id).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbpartsInventoryBill == null)
                throw new ValidationException(ErrorStrings.PartsPurchasePricingChange_Approve_Init);
            var warehouseAreaIds = partsInventoryBill.PartsInventoryDetails.Select(r => r.WarehouseAreaId);
            //判断库位是否有效
            var warehouseAreaStatus = ObjectContext.WarehouseAreas.Where(r => warehouseAreaIds.Contains(r.Id) && r.Status == (int)DcsBaseDataStatus.作废).ToArray();
            if(warehouseAreaStatus.Count() > 0) {
                throw new ValidationException(string.Format(ErrorStrings.PartsInventoryBill_Validation36, warehouseAreaStatus.First().Code));
            }
            var userinfo = Utils.GetCurrentUserInfo();
            UpdateToDatabase(partsInventoryBill);

            partsInventoryBill.Status = (int)DcsPartsInventoryBillStatus.审核通过;

            partsInventoryBill.CheckerId = userinfo.Id;
            partsInventoryBill.CheckerName = userinfo.Name;
            partsInventoryBill.CheckTime = System.DateTime.Now;
            UpdatePartsInventoryBillValidate(partsInventoryBill);
        }
        [Update(UsingCustomMethod = true)]
        public void 总部库存覆盖驳回配件盘点单(PartsInventoryBill partsInventoryBill) {
            var dbpartsInventoryBill = ObjectContext.PartsInventoryBills.Where(r => (r.Status == (int)DcsPartsInventoryBillStatus.审批通过) && r.Id == partsInventoryBill.Id).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbpartsInventoryBill == null)
                throw new ValidationException(ErrorStrings.PartsInventoryBill_Validation14);
            var userInfo = Utils.GetCurrentUserInfo();
            UpdateToDatabase(partsInventoryBill);
            partsInventoryBill.Status = (int)DcsPartsInventoryBillStatus.新建;
            partsInventoryBill.RejecterId = userInfo.Id;
            partsInventoryBill.RejecterName = userInfo.Name;
            partsInventoryBill.RejectTime = DateTime.Now;
            //驳回时更新盘点后库存为0；删除追溯码临时表
            this.rejectBill(partsInventoryBill);
            UpdatePartsInventoryBillValidate(partsInventoryBill);
        }
        #endregion

        #region 中心库盘点

        [Update(UsingCustomMethod = true)]
        public void 高级审核配件盘点单(PartsInventoryBill partsInventoryBill) {
            var dbpartsInventoryBill = ObjectContext.PartsInventoryBills.Where(r => r.Status == (int)DcsPartsInventoryBillStatus.审批通过 && r.Id == partsInventoryBill.Id).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbpartsInventoryBill == null)
                throw new ValidationException(ErrorStrings.PartsInventoryBill_Validation11);
            var userinfo = Utils.GetCurrentUserInfo();
            UpdateToDatabase(partsInventoryBill);
            if(partsInventoryBill.ApprovalId == 1) {
                var warehouseAreaIds = partsInventoryBill.PartsInventoryDetails.Select(r => r.WarehouseAreaId);
                //判断库位是否有效
                var warehouseAreaStatus = ObjectContext.WarehouseAreas.Where(r => warehouseAreaIds.Contains(r.Id) && r.Status == (int)DcsBaseDataStatus.作废).ToArray();
                if(warehouseAreaStatus.Count() > 0) {
                    throw new ValidationException(string.Format(ErrorStrings.PartsInventoryBill_Validation36, warehouseAreaStatus.First().Code));
                }
                partsInventoryBill.Status = (int)DcsPartsInventoryBillStatus.高级审核通过;
            } else {
                partsInventoryBill.Status = (int)DcsPartsInventoryBillStatus.新建;
            }
            partsInventoryBill.AdvancedAuditID = userinfo.Id;
            partsInventoryBill.AdvancedAuditName = userinfo.Name;
            partsInventoryBill.AdvancedAuditTime = System.DateTime.Now;
            UpdatePartsInventoryBillValidate(partsInventoryBill);
        }

        [Update(UsingCustomMethod = true)]
        public void 初审配件盘点单(PartsInventoryBill partsInventoryBill) {
            var dbpartsInventoryBill = ObjectContext.PartsInventoryBills.Where(r => r.Status == (int)DcsPartsInventoryBillStatus.已结果录入 && r.Id == partsInventoryBill.Id).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbpartsInventoryBill == null)
                throw new ValidationException(ErrorStrings.PartsInventoryBill_Validation2);

            var userinfo = Utils.GetCurrentUserInfo();
            UpdateToDatabase(partsInventoryBill);
            if(partsInventoryBill.ApprovalId == 1) {
                var warehouseAreaIds = partsInventoryBill.PartsInventoryDetails.Select(r => r.WarehouseAreaId);
                //判断库位是否有效
                var warehouseAreaStatus = ObjectContext.WarehouseAreas.Where(r => warehouseAreaIds.Contains(r.Id) && r.Status == (int)DcsBaseDataStatus.作废).ToArray();
                if(warehouseAreaStatus.Count() > 0) {
                    throw new ValidationException(string.Format(ErrorStrings.PartsInventoryBill_Validation36, warehouseAreaStatus.First().Code));
                }

                var inventoryConfig = ObjectContext.MultiLevelApproveConfigs.FirstOrDefault(r => ((partsInventoryBill.AmountDifference >= r.MinApproveFee && partsInventoryBill.AmountDifference < r.MaxApproveFee) || partsInventoryBill.AmountDifference < 0)
                    && r.Type == (int)DCSMultiLevelApproveConfigType.中心库配件盘点单 && r.ApproverId == userinfo.Id && r.IsFinished && r.Status == (int)DcsBaseDataStatus.有效);
                if(inventoryConfig == null) {
                    partsInventoryBill.Status = (int)DcsPartsInventoryBillStatus.初审通过;
                } else {
                    partsInventoryBill.Status = (int)DcsPartsInventoryBillStatus.高级审核通过;
                }
            } else {
                partsInventoryBill.Status = (int)DcsPartsInventoryBillStatus.新建;
            }
            partsInventoryBill.InitialApproverId = userinfo.Id;
            partsInventoryBill.InitialApproverName = userinfo.Name;
            partsInventoryBill.InitialApproveTime = System.DateTime.Now;
            UpdatePartsInventoryBillValidate(partsInventoryBill);
        }
        [Update(UsingCustomMethod = true)]
        public void 代理库审批配件盘点单(PartsInventoryBill partsInventoryBill) {
            var dbpartsInventoryBill = ObjectContext.PartsInventoryBills.Where(r => r.Status == (int)DcsPartsInventoryBillStatus.初审通过 && r.Id == partsInventoryBill.Id).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbpartsInventoryBill == null)
                throw new ValidationException(ErrorStrings.PartsInventoryBill_Validation10);
            var userinfo = Utils.GetCurrentUserInfo();
            UpdateToDatabase(partsInventoryBill);
            if(partsInventoryBill.ApprovalId == 1) {
                var warehouseAreaIds = partsInventoryBill.PartsInventoryDetails.Select(r => r.WarehouseAreaId);
                //判断库位是否有效
                var warehouseAreaStatus = ObjectContext.WarehouseAreas.Where(r => warehouseAreaIds.Contains(r.Id) && r.Status == (int)DcsBaseDataStatus.作废).ToArray();
                if(warehouseAreaStatus.Count() > 0) {
                    throw new ValidationException(string.Format(ErrorStrings.PartsInventoryBill_Validation36, warehouseAreaStatus.First().Code));
                }
                var inventoryConfig = ObjectContext.MultiLevelApproveConfigs.FirstOrDefault(r => ((partsInventoryBill.AmountDifference >= r.MinApproveFee && partsInventoryBill.AmountDifference < r.MaxApproveFee) || partsInventoryBill.AmountDifference < 0)
                    && r.Type == (int)DCSMultiLevelApproveConfigType.中心库配件盘点单 && r.ApproverId == userinfo.Id && r.IsFinished && r.Status == (int)DcsBaseDataStatus.有效);
                if(inventoryConfig == null) {
                    partsInventoryBill.Status = (int)DcsPartsInventoryBillStatus.审核通过;
                } else {
                    partsInventoryBill.Status = (int)DcsPartsInventoryBillStatus.高级审核通过;
                }
            } else {
                partsInventoryBill.Status = (int)DcsPartsInventoryBillStatus.新建;
            }
            partsInventoryBill.CheckerId = userinfo.Id;
            partsInventoryBill.CheckerName = userinfo.Name;
            partsInventoryBill.CheckTime = System.DateTime.Now;

            UpdatePartsInventoryBillValidate(partsInventoryBill);
        }


        [Update(UsingCustomMethod = true)]
        public void 终审配件盘点单(PartsInventoryBill partsInventoryBill) {
            var dbpartsInventoryBill = ObjectContext.PartsInventoryBills.Where(r => r.Status == (int)DcsPartsInventoryBillStatus.审核通过 && r.Id == partsInventoryBill.Id).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbpartsInventoryBill == null)
                throw new ValidationException(ErrorStrings.PartsInventoryBill_Validation11);
            var userinfo = Utils.GetCurrentUserInfo();
            UpdateToDatabase(partsInventoryBill);
            if(partsInventoryBill.ApprovalId == 1) {
                var warehouseAreaIds = partsInventoryBill.PartsInventoryDetails.Select(r => r.WarehouseAreaId);
                //判断库位是否有效
                var warehouseAreaStatus = ObjectContext.WarehouseAreas.Where(r => warehouseAreaIds.Contains(r.Id) && r.Status == (int)DcsBaseDataStatus.作废).ToArray();
                if(warehouseAreaStatus.Count() > 0) {
                    throw new ValidationException(string.Format(ErrorStrings.PartsInventoryBill_Validation36, warehouseAreaStatus.First().Code));
                }
                var inventoryConfig = ObjectContext.MultiLevelApproveConfigs.FirstOrDefault(r => ((partsInventoryBill.AmountDifference >= r.MinApproveFee && partsInventoryBill.AmountDifference < r.MaxApproveFee) || partsInventoryBill.AmountDifference < 0)
                 && r.Type == (int)DCSMultiLevelApproveConfigType.中心库配件盘点单 && r.ApproverId == userinfo.Id && r.IsFinished && r.Status == (int)DcsBaseDataStatus.有效);

                if(inventoryConfig == null) {
                    partsInventoryBill.Status = (int)DcsPartsInventoryBillStatus.审批通过;
                } else {
                    partsInventoryBill.Status = (int)DcsPartsInventoryBillStatus.高级审核通过;
                }
            } else {
                partsInventoryBill.Status = (int)DcsPartsInventoryBillStatus.新建;
            }
            partsInventoryBill.ApproverId = userinfo.Id;
            partsInventoryBill.ApproverName = userinfo.Name;
            partsInventoryBill.ApproveTime = System.DateTime.Now;
            UpdatePartsInventoryBillValidate(partsInventoryBill);
        }

        #endregion

        [Update(UsingCustomMethod = true)]
        public void 审批配件盘点单(PartsInventoryBill partsInventoryBill) {
            var dbpartsInventoryBill = ObjectContext.PartsInventoryBills.Where(r => r.Status == (int)DcsPartsInventoryBillStatus.初审通过 && r.Id == partsInventoryBill.Id).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbpartsInventoryBill == null)
                throw new ValidationException(ErrorStrings.PartsInventoryBill_Validation10);
            var warehouseAreaIds = partsInventoryBill.PartsInventoryDetails.Select(r => r.WarehouseAreaId);
            //判断库位是否有效
            var warehouseAreaStatus = ObjectContext.WarehouseAreas.Where(r => warehouseAreaIds.Contains(r.Id) && r.Status == (int)DcsBaseDataStatus.作废).ToArray();
            if(warehouseAreaStatus.Count() > 0) {
                throw new ValidationException(string.Format(ErrorStrings.PartsInventoryBill_Validation36, warehouseAreaStatus.First().Code));
            }
            var userinfo = Utils.GetCurrentUserInfo();
            UpdateToDatabase(partsInventoryBill);
            partsInventoryBill.Status = (int)DcsPartsInventoryBillStatus.审核通过;
            partsInventoryBill.CheckerId = userinfo.Id;
            partsInventoryBill.CheckerName = userinfo.Name;
            partsInventoryBill.CheckTime = System.DateTime.Now;
            UpdatePartsInventoryBillValidate(partsInventoryBill);
        }

        //SIH总部在用 中心库在用
        [Update(UsingCustomMethod = true)]
        public void 结果录入配件盘点单(PartsInventoryBill partsInventoryBill) {
            var dbpartsInventoryBill = ObjectContext.PartsInventoryBills.Where(r => r.Status == (int)DcsPartsInventoryBillStatus.新建 && r.Id == partsInventoryBill.Id).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbpartsInventoryBill == null)
                throw new ValidationException(ErrorStrings.PartsInventoryBill_Validation3);
            var warehouseAreaIds = partsInventoryBill.PartsInventoryDetails.Select(r => r.WarehouseAreaId);
            //判断库位是否有效
            var warehouseAreaStatus = ObjectContext.WarehouseAreas.Where(r => warehouseAreaIds.Contains(r.Id) && r.Status == (int)DcsBaseDataStatus.作废).ToArray();
            if(warehouseAreaStatus.Count() > 0) {
                throw new ValidationException(string.Format(ErrorStrings.PartsInventoryBill_Validation36, warehouseAreaStatus.First().Code));
            }
            var userinfo = Utils.GetCurrentUserInfo();
            UpdateToDatabase(partsInventoryBill);
            partsInventoryBill.Status = (int)DcsPartsInventoryBillStatus.已结果录入;
            partsInventoryBill.ResultInputOperatorId = userinfo.Id;
            partsInventoryBill.ResultInputOperatorName = userinfo.Name;
            partsInventoryBill.ResultInputTime = System.DateTime.Now;
            //获取当前仓库的销售组织.配件销售类型Id
            var salesunitquery = from a in ObjectContext.SalesUnits.Where(r => r.Status == (int)DcsMasterDataStatus.有效)
                                 join b in ObjectContext.SalesUnitAffiWarehouses on a.Id equals b.SalesUnitId
                                 join c in ObjectContext.Warehouses.Where(r => r.Id == partsInventoryBill.WarehouseId) on b.WarehouseId equals c.Id
                                 select a;
            var salesUnit = salesunitquery.FirstOrDefault();
            if(salesUnit == null) {
                throw new ValidationException(ErrorStrings.PartsInventoryBill_Validation12);
            }
            if(salesUnit.PartsSalesCategoryId <= 0) {
                throw new ValidationException(ErrorStrings.PartsInventoryBill_Validation13);
            }

            var partsIds = partsInventoryBill.PartsInventoryDetails.Select(s => s.SparePartId).ToArray();

            ////获取清单中配件的计划价
            //var partsPlannedPrice = ObjectContext.PartsPlannedPrices.Where(t => t.PartsSalesCategoryId == salesUnit.PartsSalesCategoryId && partsIds.Contains(t.SparePartId));
            decimal amountDifference = 0;
            if(partsInventoryBill.StorageCompanyType == (int)DcsCompanyType.服务站兼代理库) {
                //服务站兼代理库时，价格还是获取销售价格
                var salesPrices = ObjectContext.PartsSalesPrices.Where(t => t.Status == (int)DcsBaseDataStatus.有效 && partsIds.Contains(t.SparePartId)).ToArray();
                foreach(var pibdetail in partsInventoryBill.PartsInventoryDetails) {
                    var costPrice = salesPrices.FirstOrDefault(r => r.SparePartId == pibdetail.SparePartId);
                    decimal price = 0;
                    if(costPrice != null) {
                        price = costPrice.SalesPrice;
                    } else {
                        price = 0;
                    }
                    pibdetail.CostPrice = price;
                    pibdetail.AmountDifference = price * pibdetail.StorageDifference;
                    amountDifference = amountDifference + price * pibdetail.StorageDifference;
                }
            } else {
                //获取清单中配件的成本价
                var enterprisePartsCosts = ObjectContext.EnterprisePartsCosts.Where(r => r.PartsSalesCategoryId == salesUnit.PartsSalesCategoryId && partsIds.Contains(r.SparePartId) && r.OwnerCompanyId == userinfo.EnterpriseId).ToList();
                //获取清单中配件首选供应商的采购价
                var partsSupplierRelation = ObjectContext.PartsSupplierRelations.Where(r => r.PartsSalesCategoryId == salesUnit.PartsSalesCategoryId && partsIds.Contains(r.PartId) && r.IsPrimary == true && r.Status == (int)DcsBaseDataStatus.有效).ToList();
                var partsPurchasePricings = ObjectContext.PartsPurchasePricings.Where(r => r.Status == (int)DcsPartsPurchasePricingStatus.生效 && r.PartsSalesCategoryId == salesUnit.PartsSalesCategoryId && partsIds.Contains(r.PartId) && DateTime.Now >= r.ValidFrom && DateTime.Now <= r.ValidTo).ToList();
                foreach(var pibdetail in partsInventoryBill.PartsInventoryDetails) {
                    var costPrice = enterprisePartsCosts.FirstOrDefault(r => r.SparePartId == pibdetail.SparePartId);
                    decimal price = 0;
                    if(costPrice != null) {
                        price = costPrice.CostPrice;
                    } else {
                        var primarySupplier = partsSupplierRelation.FirstOrDefault(r => r.PartId == pibdetail.SparePartId);
                        if(primarySupplier != null) {
                            var purchasePricing = partsPurchasePricings.FirstOrDefault(r => r.PartId == pibdetail.SparePartId && r.PartsSupplierId == primarySupplier.SupplierId);
                            price = purchasePricing == null ? 0 : purchasePricing.PurchasePrice;
                        }
                    }
                    pibdetail.CostPrice = price;
                    pibdetail.AmountDifference = price * pibdetail.StorageDifference;
                    amountDifference = amountDifference + price * pibdetail.StorageDifference;
                }
            }
            partsInventoryBill.AmountDifference = amountDifference;
            UpdatePartsInventoryBillValidate(partsInventoryBill);
        }

        //SIH总部在用 中心库在用
        [Update(UsingCustomMethod = true)]
        public void 驳回配件盘点单(PartsInventoryBill partsInventoryBill) {
            var dbpartsInventoryBill = ObjectContext.PartsInventoryBills.Where(r => (r.Status == (int)DcsPartsInventoryBillStatus.已结果录入 || r.Status == (int)DcsPartsInventoryBillStatus.初审通过 || r.Status == (int)DcsPartsInventoryBillStatus.审核通过 || r.Status == (int)DcsPartsInventoryBillStatus.审批通过) && r.Id == partsInventoryBill.Id).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbpartsInventoryBill == null)
                throw new ValidationException(ErrorStrings.PartsInventoryBill_Validation16);
            UpdateToDatabase(partsInventoryBill);
            var userinfo = Utils.GetCurrentUserInfo();
            if(partsInventoryBill.Status == (int)DcsPartsInventoryBillStatus.已结果录入) {
                partsInventoryBill.CheckerId = userinfo.Id;
                partsInventoryBill.CheckerName = userinfo.Name;
                partsInventoryBill.CheckTime = System.DateTime.Now;
            } else if(partsInventoryBill.Status == (int)DcsPartsInventoryBillStatus.初审通过) {
                partsInventoryBill.InitialApproverId = userinfo.Id;
                partsInventoryBill.InitialApproverName = userinfo.Name;
                partsInventoryBill.InitialApproveTime = System.DateTime.Now;
            } else if(partsInventoryBill.Status == (int)DcsPartsInventoryBillStatus.审核通过) {
                partsInventoryBill.ApprovalId = userinfo.Id;
                partsInventoryBill.ApproverName = userinfo.Name;
                partsInventoryBill.ApproveTime = System.DateTime.Now;
            } else if(partsInventoryBill.Status == (int)DcsPartsInventoryBillStatus.审批通过) {

                partsInventoryBill.AdvancedAuditID = userinfo.Id;
                partsInventoryBill.AdvancedAuditName = userinfo.Name;
                partsInventoryBill.AdvancedAuditTime = System.DateTime.Now;
            }
			//驳回时更新盘点后库存为0；删除追溯码临时表
            this.rejectBill(partsInventoryBill);
            partsInventoryBill.Status = (int)DcsPartsInventoryBillStatus.新建;
            partsInventoryBill.RejecterId = userinfo.Id;
            partsInventoryBill.RejecterName = userinfo.Name;
            partsInventoryBill.RejectTime = DateTime.Now;
            UpdatePartsInventoryBillValidate(partsInventoryBill);
        }
        //驳回时更新盘点后库存为0；删除追溯码临时表
        private void rejectBill(PartsInventoryBill partsInventoryBill) {
            //查询盘点清单
            var userinfo = Utils.GetCurrentUserInfo();
            var details = ObjectContext.PartsInventoryDetails.Where(t => t.PartsInventoryBillId == partsInventoryBill.Id).ToArray();
            var traceTempTypes = ObjectContext.TraceTempTypes.Where(t => t.Type == (int)DCSTraceTempType.盘点 && t.SourceBillId == partsInventoryBill.Id && t.Status == (int)DcsBaseDataStatus.有效).ToArray();
            foreach(var item in details) {
                item.StorageAfterInventory = 0;
                item.CostPrice = null;
                item.StorageAfterInventory = 0;
                UpdateToDatabase(item);
            }

            foreach(var item in traceTempTypes) {
                item.Status = (int)DcsBaseDataStatus.作废;
                item.ModifyTime = DateTime.Now;
                item.ModifierId = userinfo.Id;
                item.ModifierName = userinfo.Name;
                UpdateToDatabase(item);
            }
            ObjectContext.SaveChanges();
        }
        //SIH总部在用 中心库在用
        [Update(UsingCustomMethod = true)]
        public void 库存覆盖配件盘点单(PartsInventoryBill partsInventoryBill) {
            var dbpartsInventoryBill = ObjectContext.PartsInventoryBills.Where(r => (r.Status == (int)DcsPartsInventoryBillStatus.高级审核通过 || r.Status == (int)DcsPartsInventoryBillStatus.审批通过) && r.Id == partsInventoryBill.Id).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbpartsInventoryBill == null)
                throw new ValidationException(ErrorStrings.PartsInventoryBill_Validation17);
            if(partsInventoryBill.PartsInventoryDetails.Any(s => s.Ifcover == null)) {
                throw new ValidationException(ErrorStrings.PartsInventoryBill_IsCover);
            }
            var userinfo = Utils.GetCurrentUserInfo();
            UpdateToDatabase(partsInventoryBill);
            partsInventoryBill.InventoryRecordOperatorId = userinfo.Id;
            partsInventoryBill.InventoryRecordOperatorName = userinfo.Name;
            partsInventoryBill.InventoryRecordTime = System.DateTime.Now;
            partsInventoryBill.Status = (int)DcsPartsInventoryBillStatus.已库存覆盖;
            UpdatePartsInventoryBillValidate(partsInventoryBill);
            var partsInventoryDetails = partsInventoryBill.PartsInventoryDetails.Where(r => r.Ifcover == true).ToArray();
            var partsIds = partsInventoryDetails.Select(r => r.SparePartId).ToArray().Distinct();
            if(partsInventoryDetails.Any()) {
                var warehouseAreaIds = partsInventoryDetails.Select(r => r.WarehouseAreaId).ToArray();
                //判断库位是否有效
                var warehouseAreaStatus = ObjectContext.WarehouseAreas.Where(r => warehouseAreaIds.Contains(r.Id)).ToArray();
                if(warehouseAreaStatus.Any(o => o.Status == (int)DcsBaseDataStatus.作废)) {
                    var warehouseAreaCodes = warehouseAreaStatus.Where(o => o.Status == (int)DcsBaseDataStatus.作废).Select(o => o.Code).ToList();
                    throw new ValidationException(string.Format(ErrorStrings.PartsInventoryBill_Validation36, string.Join(",", warehouseAreaCodes)));
                }
                var partsStocks = new PartsStockAch(this).GetPartsStocksWithLock(ObjectContext.PartsStocks.Where(r => r.WarehouseId == partsInventoryBill.WarehouseId && partsIds.Contains(r.PartId) && warehouseAreaIds.Contains(r.WarehouseAreaId))).ToList();
                var partsStockIds = partsStocks.Select(r => r.Id).ToArray();
                var partsBranches = ObjectContext.PartsBranches.Where(r => partsIds.Contains(r.PartId) && r.Status == (int)DcsBaseDataStatus.有效 && ObjectContext.SalesUnits.Any(v => v.PartsSalesCategoryId == r.PartsSalesCategoryId && v.Status == (int)DcsMasterDataStatus.有效 && ObjectContext.SalesUnitAffiWarehouses.Any(s => s.WarehouseId == partsInventoryBill.WarehouseId && s.SalesUnitId == v.Id))).ToArray();
                var batchNumbers = partsInventoryDetails.Where(r => r.NewBatchNumber != null).Select(r => r.CurrentBatchNumber.ToLower()).ToArray();
                var partsStockBatchDetails = ObjectContext.PartsStockBatchDetails.Where(r => r.WarehouseId == partsInventoryBill.WarehouseId && r.StorageCompanyId == partsInventoryBill.StorageCompanyId && partsStockIds.Contains(r.PartsStockId) && partsIds.Contains(r.PartId) && batchNumbers.Contains(r.BatchNumber.ToLower()));
                var partsPlannedPrices = ObjectContext.EnterprisePartsCosts.Where(r => r.OwnerCompanyId == partsInventoryBill.StorageCompanyId && partsIds.Contains(r.SparePartId) && ObjectContext.SalesUnits.Any(v => v.PartsSalesCategoryId == r.PartsSalesCategoryId && v.Status == (int)DcsMasterDataStatus.有效 && ObjectContext.SalesUnitAffiWarehouses.Any(s => s.WarehouseId == partsInventoryBill.WarehouseId && s.SalesUnitId == v.Id))).ToList();
                partsInventoryBill.AmountDifference = 0;
                string storageDifferenceError = "";
                foreach(var partsInventoryDetail in partsInventoryDetails) {
                    var partsStock = partsStocks.FirstOrDefault(r => r.PartId == partsInventoryDetail.SparePartId && r.WarehouseAreaId == partsInventoryDetail.WarehouseAreaId);
                    //if(partsStock == null){
                    //    throw new ValidationException(string.Format(ErrorStrings.PartsInventoryBill_Validation7, partsInventoryDetail.SparePartCode));
                    if(partsStock != null && partsStock.Quantity + partsInventoryDetail.StorageDifference < 0 && partsStock.StorageCompanyType != (int)DcsCompanyType.分公司) {//配件公司盘点管理当审批时出现报错：库存为负数，不能审批。这种情况允许审批通过，并且将盘点后库存更新为0.
                        storageDifferenceError += partsInventoryDetail.SparePartCode + ";";
                    }
                }
                if(!string.IsNullOrEmpty(storageDifferenceError)) {
                    throw new ValidationException(string.Format(ErrorStrings.PartsInventoryBill_Validation8, storageDifferenceError));
                }

                List<EnterprisePartsCost> needInsertEnterpriseCost = new List<EnterprisePartsCost>();
                foreach(var partsInventoryDetail in partsInventoryDetails) {
                    var partsStock = partsStocks.FirstOrDefault(r => r.PartId == partsInventoryDetail.SparePartId && r.WarehouseAreaId == partsInventoryDetail.WarehouseAreaId);
                    if(partsStock == null) {
                        var warehouseArea = warehouseAreaStatus.First(o => o.Id == partsInventoryDetail.WarehouseAreaId);
                        partsStock = new PartsStock();
                        partsStock.WarehouseAreaId = partsInventoryDetail.WarehouseAreaId;
                        partsStock.WarehouseId = partsInventoryBill.WarehouseId;
                        partsStock.WarehouseAreaCategoryId = warehouseArea.AreaCategoryId;
                        partsStock.StorageCompanyId = partsInventoryBill.StorageCompanyId;
                        partsStock.StorageCompanyType = partsInventoryBill.StorageCompanyType;
                        partsStock.BranchId = partsInventoryBill.BranchId;
                        partsStock.Quantity = partsInventoryDetail.StorageDifference;
                        partsStock.CreatorId = userinfo.Id;
                        partsStock.CreatorName = userinfo.Name;
                        partsStock.CreateTime = DateTime.Now;
                        partsStock.PartId = partsInventoryDetail.SparePartId;
                        InsertToDatabase(partsStock);
                        partsStocks.Add(partsStock);
                    } else {
                        if(partsStock.Quantity + partsInventoryDetail.StorageDifference < 0) {
                            //配件公司盘点管理当审批时出现报错：库存为负数，不能审批。这种情况允许审批通过，并且将盘点后库存更新为0.
                            if(partsStock.StorageCompanyType != (int)DcsCompanyType.分公司) {
                                partsStock.Quantity = 0;
                            } else {
                                throw new ValidationException(string.Format(ErrorStrings.PartsInventoryBill_Validation8, partsInventoryDetail.SparePartCode));
                            }
                        } else {
                            partsStock.Quantity += partsInventoryDetail.StorageDifference;
                        }
                        new PartsStockAch(this).UpdatePartsStockValidate(partsStock);
                    }

                    var partsBranchCheck = partsBranches.Count(r => r.PartId == partsInventoryDetail.SparePartId);
                    if(partsBranchCheck == 0 || partsBranchCheck > 1) {
                        throw new ValidationException(ErrorStrings.PartsInventoryBill_Validation18);
                    }
                    var partsBranch = partsBranches.FirstOrDefault(r => r.PartId == partsInventoryDetail.SparePartId);
                    if(partsBranch != null && partsBranch.PartsWarhouseManageGranularity != null && partsBranch.PartsWarhouseManageGranularity != (int)DcsPartsBranchPartsWarhouseManageGranularity.无批次管理) {
                        var partsStockBatchDetail = partsStockBatchDetails.FirstOrDefault(r => r.PartId == partsInventoryDetail.Id && r.PartsStockId == partsStock.Id && r.BatchNumber.ToLower() == partsInventoryDetail.CurrentBatchNumber.ToLower());
                        if(partsStockBatchDetail == null) {
                            var newPartsStockBatchDetail = new PartsStockBatchDetail {//配件库存批次明细
                                PartsStockId = partsStock.Id,
                                WarehouseId = partsInventoryBill.WarehouseId,
                                StorageCompanyId = partsInventoryBill.StorageCompanyId,
                                StorageCompanyType = partsInventoryBill.StorageCompanyType,
                                PartId = partsInventoryDetail.SparePartId,
                                BatchNumber = partsInventoryDetail.NewBatchNumber,
                                Quantity = partsStock.Quantity,
                                InboundTime = System.DateTime.Now
                            };
                            InsertToDatabase(newPartsStockBatchDetail);
                            new PartsStockBatchDetailAch(this).InsertPartsStockBatchDetailValidate(newPartsStockBatchDetail);
                        } else {
                            partsStockBatchDetail.Quantity = partsStock.Quantity;
                            new PartsStockBatchDetailAch(this).UpdatePartsStockBatchDetailValidate(partsStockBatchDetail);
                        }
                    }
                    // 库存覆盖：更新企业库存的逻辑，如果配件不存在企业库存：
                    //1、如果仓储企业类型= 分公司， 查询首选供应商的当前采购价作为成本价
                    //2、如果仓储企业类型= 中心库，查询配件对应的 partssalesprice,取Partssalesprice.CenterPrice 作为成本价（查询 销售组织与仓库关系（盘点单.仓库ID = 销售组织与仓库关系.仓库id），关联销售组织（销售组织与仓库关系.销售组织ID = 销售组织.id）,关联配件销售价（配件销售价.配件销售类型ID = 销售组织.配件销售类型ID 并且 配件销售价.配件ID = 盘点单清单.配件iD））
                    //3、如果仓储企业类型= 服务站兼中心库，查询配件对应的 partssalesprice,取Partssalesprice.SalesPrice 作为成本价
                    if(partsInventoryBill.StorageCompanyType == (int)DcsCompanyType.分公司 || partsInventoryBill.StorageCompanyType == (int)DcsCompanyType.代理库 || partsInventoryBill.StorageCompanyType == (int)DcsCompanyType.服务站兼代理库) {
                        decimal price = 0;
                        var partsPlannedPriceCheck = partsPlannedPrices.Count(r => r.SparePartId == partsInventoryDetail.SparePartId);
                        if(partsPlannedPriceCheck > 1) {
                            throw new ValidationException(partsInventoryDetail.SparePartCode + ErrorStrings.PartsInventoryBill_Validation19);
                        } else if(partsPlannedPriceCheck == 1 && partsPlannedPrices.FirstOrDefault(r => r.SparePartId == partsInventoryDetail.SparePartId).CostPrice > 0) {
                            var partsPlannedPrice = partsPlannedPrices.FirstOrDefault(r => r.SparePartId == partsInventoryDetail.SparePartId);
                            price = partsPlannedPrice.CostPrice;
                            //增加逻辑：变更盘点单对应的企业配件成本。 相同的配件ID汇总库存差异 字段，更新企业配件成本.数量 = 企业配件成本.数量+库存差异数量，
                            //企业配件成本.成本金额= 企业配件成本.成本金额 +（库存差异*企业配件成本.成本价）

                            partsPlannedPrice.Quantity += partsInventoryDetail.StorageDifference;
                            partsPlannedPrice.CostAmount += (partsInventoryDetail.StorageDifference * partsPlannedPrice.CostPrice);
                            partsPlannedPrice.ModifierId = userinfo.Id;
                            partsPlannedPrice.ModifierName = userinfo.Name;

                            partsPlannedPrice.ModifyTime = DateTime.Now;
                            if(partsPlannedPrice.Quantity < 0)
                                throw new ValidationException(string.Format(ErrorStrings.PartsInventoryBill_Validation20, partsInventoryDetail.SparePartCode));
                            UpdateToDatabase(partsPlannedPrice);

                        }
                        var enterprisePartsCost = new EnterprisePartsCost();
                        if(partsPlannedPriceCheck == 0) {
                            if(partsInventoryDetail.StorageDifference < 0) {
                                throw new ValidationException(string.Format(ErrorStrings.PartsInventoryBill_Validation20, partsInventoryDetail.SparePartCode));
                            }
                            //查询销售类型Id
                            var salesUnits = ObjectContext.SalesUnits.Where(r => r.OwnerCompanyId == partsInventoryBill.StorageCompanyId && r.Status == (int)DcsMasterDataStatus.有效 && ObjectContext.SalesUnitAffiWarehouses.Any(e => e.WarehouseId == partsInventoryBill.WarehouseId && r.Id == e.SalesUnitId)).ToArray();
                            if(salesUnits.Count() == 0 || salesUnits.Count() > 1)
                                throw new ValidationException(string.Format(ErrorStrings.PartsInventoryBill_Validation21, partsInventoryBill.WarehouseCode));
                            else {
                                enterprisePartsCost.PartsSalesCategoryId = salesUnits.First().PartsSalesCategoryId;
                            }
                            //  enterprisePartsCost.Quantity = partsInventoryDetail.StorageDifference;
                            enterprisePartsCost.OwnerCompanyId = partsInventoryBill.StorageCompanyId;
                            enterprisePartsCost.SparePartId = partsInventoryDetail.SparePartId;
                            enterprisePartsCost.CreatorId = userinfo.Id;
                            enterprisePartsCost.CreatorName = userinfo.Name;
                            enterprisePartsCost.CreateTime = DateTime.Now;
                        } else {
                            enterprisePartsCost = partsPlannedPrices.FirstOrDefault(r => r.SparePartId == partsInventoryDetail.SparePartId);
                            enterprisePartsCost.ModifierId = userinfo.Id;
                            enterprisePartsCost.ModifierName = userinfo.Name;
                            enterprisePartsCost.ModifyTime = DateTime.Now;
                        }
                        if(price == 0 && partsInventoryBill.StorageCompanyType == (int)DcsCompanyType.分公司) {
                            //不存在企业配件成本的查询首选供应商的当前采购价
                            var partsPurchasePricings = ObjectContext.PartsPurchasePricings.Where(r => r.ValidFrom <= DateTime.Now && r.ValidTo >= DateTime.Now && r.PartId == partsInventoryDetail.SparePartId && r.Status != (int)DcsPartsPurchasePricingStatus.作废 && ObjectContext.PartsSupplierRelations.Any(p => r.PartsSupplierId == p.SupplierId && r.PartId == p.PartId && p.IsPrimary == true && ObjectContext.SalesUnits.Any(v => v.PartsSalesCategoryId == r.PartsSalesCategoryId && v.Status == (int)DcsMasterDataStatus.有效 && ObjectContext.SalesUnitAffiWarehouses.Any(s => s.WarehouseId == partsInventoryBill.WarehouseId && s.SalesUnitId == v.Id)))).ToArray();
                            if(partsPurchasePricings.Count() == 0 || partsPurchasePricings.Count() > 1) {
                                throw new ValidationException(partsInventoryDetail.SparePartCode + ErrorStrings.PartsInventoryBill_Validation22);
                            } else {
                                if(partsPurchasePricings.First().PurchasePrice > 0) {
                                    price = partsPurchasePricings.First().PurchasePrice;
                                    enterprisePartsCost.Quantity += partsInventoryDetail.StorageDifference;
                                    enterprisePartsCost.CostAmount += (partsInventoryDetail.StorageDifference * price);
                                    enterprisePartsCost.CostPrice = price;
                                    if(enterprisePartsCost.Quantity < 0) {
                                        throw new ValidationException(string.Format(ErrorStrings.PartsInventoryBill_Validation20, partsInventoryDetail.SparePartCode));
                                    }
                                } else {
                                    throw new ValidationException(partsInventoryDetail.SparePartCode + ErrorStrings.PartsInventoryBill_Validation23);
                                }
                            }
                        }
                        if(price == 0 && (partsInventoryBill.StorageCompanyType == (int)DcsCompanyType.代理库 || partsInventoryBill.StorageCompanyType == (int)DcsCompanyType.服务站兼代理库)) {
                            //2、如果仓储企业类型= 中心库，查询配件对应的 partssalesprice,取Partssalesprice.CenterPrice 作为成本价（查询 销售组织与仓库关系（盘点单.仓库ID = 销售组织与仓库关系.仓库id），关联销售组织（销售组织与仓库关系.销售组织ID = 销售组织.id）,关联配件销售价（配件销售价.配件销售类型ID = 销售组织.配件销售类型ID 并且 配件销售价.配件ID = 盘点单清单.配件iD））
                            //3、如果仓储企业类型= 服务站兼中心库，查询配件对应的 partssalesprice,取Partssalesprice.SalesPrice 作为成本价
                            var partssalespriceList = ObjectContext.PartsSalesPrices.Where(r => r.SparePartId == partsInventoryDetail.SparePartId && ObjectContext.SalesUnits.Any(a => a.PartsSalesCategoryId == r.PartsSalesCategoryId && ObjectContext.SalesUnitAffiWarehouses.Any(c => c.SalesUnitId == a.Id && c.WarehouseId == partsInventoryBill.WarehouseId))).ToArray();
                            if(partssalespriceList.Count() == 0 || partssalespriceList.Count() > 1) {
                                throw new ValidationException(partsInventoryDetail.SparePartCode + ErrorStrings.PartsInventoryBill_Validation24);
                            } else {

                                if(partsInventoryBill.StorageCompanyType == (int)DcsCompanyType.代理库) {
                                    price = partssalespriceList.First().CenterPrice.Value;
                                    if(price == 0) {
                                        throw new ValidationException(partsInventoryDetail.SparePartCode + ErrorStrings.PartsInventoryBill_Validation25);
                                    }
                                } else {
                                    price = partssalespriceList.First().SalesPrice;
                                    if(price == 0) {
                                        throw new ValidationException(partsInventoryDetail.SparePartCode + ErrorStrings.PartsInventoryBill_Validation26);
                                    }
                                }
                                enterprisePartsCost.Quantity += partsInventoryDetail.StorageDifference;
                                enterprisePartsCost.CostAmount += (partsInventoryDetail.StorageDifference * price);
                                enterprisePartsCost.CostPrice = price;
                                if(enterprisePartsCost.Quantity < 0) {
                                    throw new ValidationException(string.Format(ErrorStrings.PartsInventoryBill_Validation20, partsInventoryDetail.SparePartCode));
                                }
                            }
                        }
                        partsInventoryDetail.CostPrice = price;//企业配件成本价
                        partsInventoryDetail.AmountDifference = partsInventoryDetail.CostPrice * partsInventoryDetail.StorageDifference;
                        partsInventoryBill.AmountDifference += partsInventoryDetail.AmountDifference;

                        if(partsPlannedPriceCheck == 0) {
                            //InsertToDatabase(enterprisePartsCost);
                            var insertEnterpriseCost = needInsertEnterpriseCost.FirstOrDefault(r => r.SparePartId == enterprisePartsCost.SparePartId);
                            if(insertEnterpriseCost != null) {
                                insertEnterpriseCost.Quantity += partsInventoryDetail.StorageDifference;
                                insertEnterpriseCost.CostAmount += (partsInventoryDetail.StorageDifference * insertEnterpriseCost.CostPrice);
                                insertEnterpriseCost.ModifierId = userinfo.Id;
                                insertEnterpriseCost.ModifierName = userinfo.Name;
                                insertEnterpriseCost.ModifyTime = DateTime.Now;
                                if(insertEnterpriseCost.Quantity < 0)
                                    throw new ValidationException(string.Format(ErrorStrings.PartsInventoryBill_Validation20, partsInventoryDetail.SparePartCode));
                            } else {
                                needInsertEnterpriseCost.Add(enterprisePartsCost);
                            }
                        } else {
                            UpdateToDatabase(enterprisePartsCost);
                        }
                        //partsInventoryDetail.CostPrice = partsPlannedPrice.PlannedPrice;//配件计划价
                        //   InsertToDatabase(partsWarehousePendingCost);
                    }
                }
                foreach(var enterpriseCost in needInsertEnterpriseCost) {
                    InsertToDatabase(enterpriseCost);
                }

                var traceTempTypes = ObjectContext.TraceTempTypes.Where(t => t.SourceBillId == partsInventoryBill.Id && t.Type == (int)DCSTraceTempType.盘点
                    && t.Status == (int)DcsBaseDataStatus.有效).ToArray();
                var boxCodes = traceTempTypes.Select(o => o.BoxCode).ToList();
                var sihCodes = traceTempTypes.Select(o => o.SIHLabelCode).ToList();

                var accurateTraces = ObjectContext.AccurateTraces.Where(t => t.Type == (int)DCSAccurateTraceType.入库 && t.Status == (int)DCSAccurateTraceStatus.有效 && t.CompanyId == partsInventoryBill.StorageCompanyId
                    && (boxCodes.Contains(t.BoxCode) || sihCodes.Contains(t.SIHLabelCode)) && ObjectContext.PartsInboundPlans.Any(y => y.Id == t.SourceBillId && y.WarehouseId == partsInventoryBill.WarehouseId)).ToArray();
                foreach(var detail in partsInventoryDetails) {
                    var detailTraceTempTypes = traceTempTypes.Where(o => o.SourceBillDetailId == detail.Id);
                    foreach(var traceTempType in detailTraceTempTypes) {
                        var partsStock = partsStocks.FirstOrDefault(r => r.PartId == traceTempType.PartId && r.WarehouseAreaId == traceTempType.WarehouseAreaId);
                        var updateAccurateTraces = string.IsNullOrEmpty(traceTempType.SIHLabelCode) ?
                        accurateTraces.Where(o => o.BoxCode == traceTempType.BoxCode && o.WarehouseAreaId == detail.WarehouseAreaId).ToList() :
                        accurateTraces.Where(o => o.SIHLabelCode == traceTempType.SIHLabelCode && o.WarehouseAreaId == detail.WarehouseAreaId).ToList();

                        //if(traceTempType.Quantity != updateAccurateTraces.Sum(o => o.InQty)) {

                        //}
                        //foreach(var accurateTrace in updateAccurateTraces) {
                        //    if(accurateTrace.quan != detail.CurrentStorage) {
                        //        accurateTrace.InQty = accurateTrace.InQty + (traceTempType.Quantity - traceTempType.InboundQty);
                        //        if((accurateTrace.InQty ?? 0) == 0)
                        //            accurateTrace.Status = (int)DCSAccurateTraceStatus.盘点不考核;
                        //        accurateTrace.ModifierId = userinfo.Id;
                        //        accurateTrace.ModifierName = userinfo.Name;
                        //        accurateTrace.ModifyTime = DateTime.Now;
                        //        UpdateToDatabase(accurateTrace);
                        //    }
                        //    if(accurateTrace == null && detail.StorageDifference >= 0) {
                        //        var inTrace = new AccurateTrace() {
                        //            Type = (int)DCSAccurateTraceType.入库,
                        //            CompanyType = (int)DCSAccurateTraceCompanyType.配件中心,
                        //            CompanyId = partsInventoryBill.StorageCompanyId,
                        //            SourceBillId = partsInventoryBill.Id,
                        //            PartId = detail.SparePartId,
                        //            SIHLabelCode = traceTempType.SIHLabelCode,
                        //            Status = (int)DCSAccurateTraceStatus.有效,
                        //            CreatorId = userinfo.Id,
                        //            CreatorName = userinfo.Name,
                        //            CreateTime = DateTime.Now,
                        //            TraceProperty = traceTempType.TraceProperty,
                        //            BoxCode = traceTempType.BoxCode,
                        //            InQty = (traceTempType.Quantity - traceTempType.InboundQty),
                        //            PartsStockId = partsStock.Id
                        //        };
                        //        InsertToDatabase(inTrace);
                        //    }
                        //} 
                    }
                }
                ObjectContext.SaveChanges();
            }
        }

        //SIH总部在用 中心库在用
        [Update(UsingCustomMethod = true)]
        public void 作废配件盘点单(PartsInventoryBill partsInventoryBill) {
            var dbpartsInventoryBill = ObjectContext.PartsInventoryBills.Where(r => (r.Status == (int)DcsPartsInventoryBillStatus.新建 || r.Status == (int)DcsPartsInventoryBillStatus.审批通过 || r.Status == (int)DcsPartsInventoryBillStatus.高级审核通过 || r.Status == (int)DcsPartsInventoryBillStatus.已结果录入) && r.Id == partsInventoryBill.Id).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbpartsInventoryBill == null)
                throw new ValidationException(ErrorStrings.PartsInventoryBill_Validation6);
            var userinfo = Utils.GetCurrentUserInfo();
            UpdateToDatabase(partsInventoryBill);
            partsInventoryBill.Status = (int)DcsPartsInventoryBillStatus.作废;
            partsInventoryBill.AbandonerId = userinfo.Id;
            partsInventoryBill.AbandonerName = userinfo.Name;
            partsInventoryBill.AbandonTime = System.DateTime.Now;
            UpdatePartsInventoryBillValidate(partsInventoryBill);
        }
        [Invoke(HasSideEffects = true)]
        public void 批量初审盘点单(int[] ids) {
            var bills = ObjectContext.PartsInventoryBills.Where(t => ids.Contains(t.Id)).ToArray();
            if(bills.Count() == 0) {
                throw new ValidationException("未找到有效的盘点单");
            }
            foreach(var item in bills) {
                this.总部初审配件盘点单(item);
            }
			ObjectContext.SaveChanges();
        }
		[Invoke(HasSideEffects = true)]
        public void 批量审核盘点单(int[] ids) {
            var bills = ObjectContext.PartsInventoryBills.Where(t => ids.Contains(t.Id)).ToArray();
            if(bills.Count() == 0) {
                throw new ValidationException("未找到有效的盘点单");
            }
            foreach(var item in bills) {
                this.总部审核配件盘点单(item);
            }
			ObjectContext.SaveChanges();
        }
		[Invoke(HasSideEffects = true)]
        public void 批量审批盘点单(int[] ids) {
            var bills = ObjectContext.PartsInventoryBills.Where(t => ids.Contains(t.Id)).ToArray();
            if(bills.Count() == 0) {
                throw new ValidationException("未找到有效的盘点单");
            }
            foreach(var item in bills) {
                this.总部审批配件盘点单(item);
            }
			ObjectContext.SaveChanges();
        }
    }
}
