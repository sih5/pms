﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsOutboundBillAch : DcsSerivceAchieveBase {
        public PartsOutboundBillAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 生成配件出库单(PartsOutboundBill partsOutboundBill) {
            var partsOutboundBillDetails = partsOutboundBill.PartsOutboundBillDetails.ToArray();
            var partsOutboundPlan = ObjectContext.PartsOutboundPlans.SingleOrDefault(r => r.Id == partsOutboundBill.PartsOutboundPlanId);
            PartsOutboundPlanDetail[] partsOutboundPlanDetails;
            if(partsOutboundPlan == null) {
                if(partsOutboundBill.PartsOutboundPlan == null) {
                    throw new ValidationException(ErrorStrings.PartsOutboundBill_Validation3);
                } else {
                    partsOutboundPlan = partsOutboundBill.PartsOutboundPlan;
                }
                partsOutboundPlanDetails = partsOutboundBill.PartsOutboundPlan.PartsOutboundPlanDetails.ToArray();
            } else {
                partsOutboundPlanDetails = ObjectContext.PartsOutboundPlanDetails.Where(r => r.PartsOutboundPlanId == partsOutboundBill.PartsOutboundPlanId).ToArray();
            }

            var batchNumbers = partsOutboundBillDetails.Where(v => !string.IsNullOrEmpty(v.BatchNumber)).Select(v => v.BatchNumber.ToLower()).ToArray();
            //配件销售退货单，验证配件出库批次与退货批次一致
            if(partsOutboundBill.OriginalRequirementBillType == (int)DcsOriginalRequirementBillType.配件销售退货单 && batchNumbers.Length > 0) {
                var partsSalesReturnBill = ObjectContext.PartsSalesReturnBills.SingleOrDefault(r => r.Id == partsOutboundBill.OriginalRequirementBillId);
                if(partsSalesReturnBill == null)
                    throw new ValidationException(ErrorStrings.PartsOutboundBill_Validation10);
                var returnBatchNumbers = ObjectContext.PartsSalesReturnBillDetails.Where(r => r.PartsSalesReturnBillId == partsSalesReturnBill.Id && r.BatchNumber != null).Select(v => v.BatchNumber.ToLower()).ToArray();
                if(batchNumbers.Any(v => !returnBatchNumbers.Contains(v)))
                    throw new ValidationException(ErrorStrings.PartsOutboundBill_Validation7);
            }

            //获取配件出库计划清单

            //获取配件库存
            var partsIds = partsOutboundBillDetails.Select(r => r.SparePartId).Distinct().ToArray();
            var warehouseAreaIds = partsOutboundBillDetails.Select(r => r.WarehouseAreaId).Distinct().ToArray();
            var partsStocks = new PartsStockAch(this.DomainService).GetPartsStocksWithLock(ObjectContext.PartsStocks.Where(r => r.WarehouseId == partsOutboundBill.WarehouseId && partsIds.Contains(r.PartId) && warehouseAreaIds.Contains(r.WarehouseAreaId))).ToArray();
            //获取配件库存批次明细
            var partsStockBatchDetails = new PartsStockBatchDetail[0];
            if(batchNumbers.Length > 0) {
                partsStockBatchDetails = ObjectContext.PartsStockBatchDetails.Where(r => batchNumbers.Contains(r.BatchNumber.ToLower())).ToArray();
            }
            //获取配件锁定库存
            var partsLockedStocks = new PartsLockedStockAch(this.DomainService).GetPartsLockedStocksWithLock(ObjectContext.PartsLockedStocks.Where(r => r.WarehouseId == partsOutboundBill.WarehouseId && partsIds.Contains(r.PartId))).ToArray();

            var partsAmountDetails = new Dictionary<int, decimal>();
            if(partsOutboundPlan.StorageCompanyType == (int)DcsCompanyType.分公司) {
                partsAmountDetails = (from a in ObjectContext.PartsPlannedPrices.Where(r => r.OwnerCompanyId == partsOutboundBill.StorageCompanyId && r.PartsSalesCategoryId == partsOutboundBill.PartsSalesCategoryId && partsIds.Contains(r.SparePartId)).ToArray()
                                      select a).ToDictionary(r => r.SparePartId, r => r.PlannedPrice);
            }

            foreach(var partsOutboundBillDetail in partsOutboundBillDetails) {
                //更新配件出库计划清单
                var partsOutboundPlanDetail = partsOutboundPlanDetails.FirstOrDefault(r => r.SparePartId == partsOutboundBillDetail.SparePartId);
                if(partsOutboundPlanDetail == null)
                    throw new ValidationException(string.Format(ErrorStrings.PartsOutboundBill_Validation4, partsOutboundBillDetail.SparePartCode));
                partsOutboundPlanDetail.OutboundFulfillment = (partsOutboundPlanDetail.OutboundFulfillment ?? 0) + partsOutboundBillDetail.OutboundAmount;
                if(partsOutboundPlanDetail.OutboundFulfillment.Value > partsOutboundPlanDetail.PlannedAmount)
                    throw new ValidationException(string.Format(ErrorStrings.PartsOutboundBill_Validation9, partsOutboundBillDetail.SparePartCode));
                //减少锁定库存
                var partsLockedStock = partsLockedStocks.FirstOrDefault(r => r.PartId == partsOutboundBillDetail.SparePartId);
                if(partsLockedStock == null)
                    throw new ValidationException(string.Format(ErrorStrings.PartsOutboundBill_Validation5, partsOutboundBillDetail.SparePartCode));
                partsLockedStock.LockedQuantity -= partsOutboundBillDetail.OutboundAmount;
                new PartsLockedStockAch(this.DomainService).UpdatePartsLockedStockValidate(partsLockedStock);
                //减少配件库存
                var partsStock = partsStocks.FirstOrDefault(r => r.WarehouseAreaId == partsOutboundBillDetail.WarehouseAreaId && r.PartId == partsOutboundBillDetail.SparePartId && r.WarehouseAreaId == partsOutboundBillDetail.WarehouseAreaId);
                if(partsStock == null)
                    throw new ValidationException(string.Format(ErrorStrings.PartsOutboundBill_Validation6, partsOutboundBillDetail.SparePartCode, partsOutboundBillDetail.WarehouseAreaCode));
                if(partsOutboundBillDetail.OutboundAmount > partsStock.Quantity)
                    throw new ValidationException(string.Format(ErrorStrings.PartsOutboundBill_Validation11, partsOutboundBillDetail.SparePartCode, partsOutboundBillDetail.WarehouseAreaCode));

                partsStock.Quantity -= partsOutboundBillDetail.OutboundAmount;
                new PartsStockAch(this.DomainService).UpdatePartsStockValidate(partsStock);
                //配件库存批次明细
                if(!string.IsNullOrEmpty(partsOutboundBillDetail.BatchNumber)) {
                    var partsStockBatchDetail = partsStockBatchDetails.FirstOrDefault(r => r.BatchNumber.ToLower() == partsOutboundBillDetail.BatchNumber.ToLower() && r.Quantity == partsOutboundBillDetail.OutboundAmount);
                    if(partsStockBatchDetail == null)
                        throw new ValidationException(string.Format(ErrorStrings.PartsOutboundBill_Validation2, partsOutboundBillDetail.BatchNumber));
                    DeleteFromDatabase(partsStockBatchDetail);
                }
                if(partsOutboundPlan.StorageCompanyType == (int)DcsCompanyType.分公司) {
                    //填充配件出库单清单
                    if(!partsAmountDetails.ContainsKey(partsOutboundBillDetail.SparePartId)) {
                        throw new ValidationException(string.Format(ErrorStrings.PartsOutboundBill_Validation8, partsOutboundBillDetail.SparePartCode));
                    } else {
                        partsOutboundBillDetail.CostPrice = partsAmountDetails[partsOutboundBillDetail.SparePartId];
                    }
                    if(partsOutboundBillDetail.CostPrice == 0) {
                        throw new ValidationException(string.Format(ErrorStrings.PartsOutboundBill_Validation12, partsOutboundBillDetail.SparePartCode));
                    }
                }
            }

            //出库单生成时，系统增加判断：
            //	上游的销售订单的“是否换货”字段为“是”的，出库单“结算状态”被自动赋值为“已结算”；
            //	“是否换货”字段为“否”的，默认为“待结算”。
            if(partsOutboundBill.OriginalRequirementBillType == (int)DcsOriginalRequirementBillType.配件销售退货单) {
                var partsSalesReturnBill = this.ObjectContext.PartsSalesReturnBills.Where(r => r.Id == partsOutboundBill.OriginalRequirementBillId).FirstOrDefault();
                if(partsSalesReturnBill != null)
                    if(partsSalesReturnBill.IsBarter == true)
                        partsOutboundBill.SettlementStatus = (int)DcsPartsSettlementStatus.已结算;
            }
            this.InsertPartsOutboundBillValidate(partsOutboundBill);

            //更新配件出库计划
            partsOutboundPlan.Status = partsOutboundPlanDetails.Any(v => !v.OutboundFulfillment.HasValue || v.OutboundFulfillment.Value < v.PlannedAmount) ? (int)DcsPartsOutboundPlanStatus.部分出库 : (int)DcsPartsOutboundPlanStatus.出库完成;
            new PartsOutboundPlanAch(this.DomainService).UpdatePartsOutboundPlanValidate(partsOutboundPlan);

            var userInfo = Utils.GetCurrentUserInfo();
            if(partsOutboundPlan.CustomerAccountId != null) {
                switch(partsOutboundBill.OutboundType) {
                    case (int)DcsPartsOutboundType.配件销售:
                        new CustomerAccountAch(this.DomainService).销售出库过账(partsOutboundPlan.CustomerAccountId.Value, (decimal)partsOutboundBillDetails.Sum(r => r.SettlementPrice * r.OutboundAmount));
                        break;
                    case (int)DcsPartsOutboundType.采购退货:
                        if(partsOutboundBill.OriginalRequirementBillType == (int)DcsOriginalRequirementBillType.配件采购退货单) {
                            var supplierAccount = ObjectContext.SupplierAccounts.SingleOrDefault(r => r.Id == partsOutboundBill.CustomerAccountId && r.Status == (int)DcsMasterDataStatus.有效);
                            if(supplierAccount == null) {
                                throw new ValidationException(ErrorStrings.PartsOutboundBill_Validation13);
                            }
                            supplierAccount.EstimatedDueAmount = supplierAccount.EstimatedDueAmount + (decimal)partsOutboundBillDetails.Sum(r => r.SettlementPrice * r.OutboundAmount);
                            supplierAccount.ModifierId = userInfo.Id;
                            supplierAccount.ModifierName = userInfo.Name;
                            supplierAccount.ModifyTime = DateTime.Now;
                            UpdateToDatabase(supplierAccount);
                        }
                        break;

                }
            }

            //if(partsOutboundBill.OutboundType == (int)DcsPartsOutboundType.配件销售 || partsOutboundBill.OutboundType == (int)DcsPartsOutboundType.采购退货 || partsOutboundBill.OutboundType == (int)DcsPartsOutboundType.配件调拨 || partsOutboundBill.OutboundType == (int)DcsPartsOutboundType.积压件调剂 || partsOutboundBill.OutboundType == (int)DcsPartsOutboundType.质量件索赔) {
            //    //新增配件物流批次
            //    var partsLogisticBatch = ObjectContext.PartsLogisticBatches.FirstOrDefault(r => r.SourceType == (int)DcsPartsLogisticBatchSourceType.配件出库计划 && r.SourceId == partsOutboundPlan.Id);
            //    if(partsLogisticBatch == null) {
            //        partsLogisticBatch = new PartsLogisticBatch {
            //            SourceId = partsOutboundPlan.Id,
            //            SourceType = (int)DcsPartsLogisticBatchSourceType.配件出库计划,
            //            CompanyId = partsOutboundBill.StorageCompanyId,
            //            Status = (int)DcsPartsLogisticBatchStatus.新增,
            //            ShippingStatus = (int)DcsPartsLogisticBatchShippingStatus.待运
            //        };
            //        InsertToDatabase(partsLogisticBatch);
            //    }
            //    var partsLogisticBatchBillDetail = new PartsLogisticBatchBillDetail {
            //        BillType = (int)DcsPartsLogisticBatchBillDetailBillType.配件出库单
            //    };
            //    partsOutboundBill.PartsLogisticBatchBillDetails.Add(partsLogisticBatchBillDetail);
            //    partsLogisticBatch.PartsLogisticBatchBillDetails.Add(partsLogisticBatchBillDetail);
            //    var sumBatchAmounts = partsOutboundBillDetails.GroupBy(r => new {
            //        r.SparePartId,
            //        r.BatchNumber,
            //    }).Select(r => new {
            //        sumAmount = r.Sum(v => v.OutboundAmount),
            //        sparePartId = r.Key.SparePartId,
            //        batchNumber = r.Key.BatchNumber,
            //        sparePartCode = r.First().SparePartCode,
            //        sparePartName = r.First().SparePartName
            //    }).ToArray();
            //    foreach(var sumBatchAmount in sumBatchAmounts) {
            //        partsLogisticBatch.PartsLogisticBatchItemDetails.Add(new PartsLogisticBatchItemDetail {
            //            CounterpartCompanyId = partsOutboundBill.CounterpartCompanyId,
            //            ReceivingCompanyId = partsOutboundBill.CounterpartCompanyId,
            //            ShippingCompanyId = partsOutboundBill.StorageCompanyId,
            //            SparePartId = sumBatchAmount.sparePartId,
            //            SparePartCode = sumBatchAmount.sparePartCode,
            //            SparePartName = sumBatchAmount.sparePartName,
            //            BatchNumber = sumBatchAmount.batchNumber,
            //            OutboundAmount = sumBatchAmount.sumAmount,
            //            InboundAmount = 0
            //        });
            //    }

            //    new PartsLogisticBatchAch(this.DomainService).InsertPartsLogisticBatchValidate(partsLogisticBatch);
            //}
        }

        public void 生成负数配件出库单(PartsOutboundBill partsOutboundBill) {
            //查出库计划单
            var dbPartsOutboundPlan = ObjectContext.PartsOutboundPlans.SingleOrDefault(r => r.Id == partsOutboundBill.PartsOutboundPlanId && (r.Status == (int)DcsPartsOutboundPlanStatus.出库完成 || r.Status == (int)DcsPartsOutboundPlanStatus.终止));
            //校验
            if(dbPartsOutboundPlan == null) {
                throw new ValidationException(ErrorStrings.PartsOutboundPlan_Validation3);
            }
            //查出库计划清单
            var partsOutboundPlanDetails = ObjectContext.PartsOutboundPlanDetails.Where(r => r.PartsOutboundPlanId == dbPartsOutboundPlan.Id).ToArray();
            //获取出库清单
            var partsOutboundBillDetails = partsOutboundBill.PartsOutboundBillDetails.ToArray();
            //获取出库单清单中的所有配件的Id
            var partIds = partsOutboundBillDetails.Select(r => r.SparePartId).ToArray();
            //获取出库单清单上所有配件的库位
            var warehouseAreaIds = partsOutboundBillDetails.Select(r => r.WarehouseAreaId).ToArray();

            //出库清单数量变为负数，倒冲出库计划清单，根据配件Id匹配
            foreach(var item in partsOutboundBillDetails) {
                item.OutboundAmount = (-1) * item.OutboundAmount;
                var partsOutboundPlanDetail = partsOutboundPlanDetails.SingleOrDefault(r => r.SparePartId == item.SparePartId);
                if(partsOutboundPlanDetail != null) {
                    partsOutboundPlanDetail.OutboundFulfillment = partsOutboundPlanDetail.OutboundFulfillment - item.OutboundAmount;
                }
            }
            //标记出库计划单为修改
            //UpdatePartsOutboundPlan(dbPartsOutboundPlan);

            //获取出库计划单清单中所有配件的库存信息(条件：配件Id，库位Id)
            var partsStocks = ObjectContext.PartsStocks.Where(r => partIds.Contains(r.PartId) && warehouseAreaIds.Contains(r.WarehouseAreaId)).ToArray();


            foreach(var item in partsOutboundBillDetails) {
                var tempPart = partsStocks.SingleOrDefault(r => item.SparePartId == r.PartId);
                //校验出库清单上配件是否有库位
                if(tempPart == null) {
                    throw new ValidationException(string.Format(ErrorStrings.PartsStock_Validation3, item.SparePartName));
                }
                //校验出库清单上配件是否数量充足
                if(tempPart.Quantity < item.OutboundAmount) {
                    throw new ValidationException(string.Format(ErrorStrings.PartsStock_Validation4, item.SparePartName));
                }
                //减少对应的库存
                tempPart.Quantity = tempPart.Quantity - item.OutboundAmount;
                new PartsStockAch(this.DomainService).UpdatePartsStockValidate(tempPart);
            }
            var tempCode = partsOutboundBill.Remark.Trim();
            var tempPartsOutboundBill = ObjectContext.PartsOutboundBills.Single(r => r.Code == tempCode);
            var tempId = tempPartsOutboundBill.Id;
            //var partsLogisticBatche = ObjectContext.PartsLogisticBatches.SingleOrDefault(r => ObjectContext.PartsLogisticBatchBillDetails.Any(v => v.PartsLogisticBatchId == r.Id && v.Id == tempId));
            //if(partsLogisticBatche == null) {
            //    throw new ValidationException(ErrorStrings.partsLogisticBatche_Validation1);
            //}
            //if(partsLogisticBatche.Status == (int)DcsPartsLogisticBatchStatus.新增) {
            //    //获取对应物流批次配件清单
            //    var partsLogisticBatchItemDetails = ObjectContext.PartsLogisticBatchItemDetails.Where(r => partsLogisticBatche.Id == r.PartsLogisticBatchId).ToArray();
            //    //更新批次清单配件清单上配件的数量根据Id和批次号匹配
            //    foreach(var item in partsLogisticBatchItemDetails) {
            //        var tempPart = partsOutboundBillDetails.SingleOrDefault(r => r.SparePartId == item.SparePartId && r.BatchNumber == item.BatchNumber);
            //        if(tempPart != null) {
            //            item.OutReturnAmount = item.OutReturnAmount + tempPart.OutboundAmount;
            //        }
            //    }
            //}
            //var partDetails = partsOutboundBillDetails.Select(r => new EnterprisePartsCostAch.PartsDetail {
            //    SparePartId = r.SparePartId,
            //    Quantity = r.OutboundAmount,
            //    Price = r.CostPrice,
            //}).ToArray();
            //var saleUnit = ObjectContext.SalesUnits.SingleOrDefault(r => ObjectContext.SalesUnitAffiWarehouses.Any(v => r.Id == v.SalesUnitId && v.WarehouseId == partsOutboundBill.WarehouseId));
            //if(saleUnit == null) {
            //    throw new ValidationException(ErrorStrings.SalesUnit_Validation2);
            //}
            //var tempCompany = ObjectContext.Companies.Single(r => r.Id == partsOutboundBill.StorageCompanyId);
            //if(tempCompany.Type == (int)DcsCompanyType.分公司) {
            //    var result = new EnterprisePartsCostAch(this.DomainService).提交出库事务(partsOutboundBill.StorageCompanyId, saleUnit.PartsSalesCategoryId, partsOutboundBill.OutboundType, partDetails);
            //    foreach(var item in partsOutboundBillDetails) {
            //        if(result.ContainsKey(item.SparePartId)) {
            //            item.CostPrice = result[item.SparePartId];
            //        }
            //    }
            //}
            InsertPartsOutboundBill(partsOutboundBill);


        }


        public void 审核服务站兼代理库调拨2(PartsOutboundBill partsOutboundBill) {
            //校验是否存在
            var companyTransferOrder = ObjectContext.CompanyTransferOrders.SingleOrDefault(r => r.Id == partsOutboundBill.OriginalRequirementBillId && r.Status != (int)DcsPartsTransferOrderStatus.作废);
            if(companyTransferOrder == null) {
                throw new ValidationException(ErrorStrings.CompanyTransferOrder_Validation2);
            }
            //校验状态
            if(companyTransferOrder.Status != (int)DcsPartsTransferOrderStatus.新建) {
                throw new ValidationException(ErrorStrings.CompanyTransferOrder_Validation2);
            }
            //校验调拨类型
            if(companyTransferOrder != null && companyTransferOrder.Type != (int)DcsCompanyTransferOrderType.服务站兼代理库调拨) {
                throw new ValidationException(ErrorStrings.CompanyTransferOrder_Validation6);
            }
            //获取清单中配件的可用库存
            var companyTransferOrderDetails = companyTransferOrder.CompanyTransferOrderDetails.ToArray();
            var partIds = companyTransferOrderDetails.Select(r => r.PartsId).ToArray();
            var warehousePartsStocks = DomainService.查询仓库库存(companyTransferOrder.OriginalCompanyId, companyTransferOrder.OriginalWarehouseId, partIds).ToList();
            if(warehousePartsStocks.Count <= 0) {
                throw new ValidationException(ErrorStrings.WarehousePartsStock_Validation1);
            }
            //校验确认量是否大于可用库存
            foreach(var companyTransferOrderDetail in companyTransferOrderDetails) {
                var checkPartsStock = warehousePartsStocks.SingleOrDefault(r => r.SparePartId == companyTransferOrderDetail.PartsId);

                var partsOutboundBillDetail = partsOutboundBill.PartsOutboundBillDetails.Where(r => r.SparePartId == companyTransferOrderDetail.PartsId).ToArray();
                if(partsOutboundBillDetail.Length == 0) {
                    throw new ValidationException(string.Format(ErrorStrings.PartsOutboundBill_Validation14, companyTransferOrderDetail.PartsCode));
                }
                companyTransferOrderDetail.ConfirmedAmount = partsOutboundBillDetail.Sum(r => r.OutboundAmount);
                if(checkPartsStock == null || companyTransferOrderDetail.ConfirmedAmount > checkPartsStock.UsableQuantity) {
                    throw new ValidationException(string.Format(ErrorStrings.CompanyTransferOrder_Validation10, companyTransferOrderDetail.PartsCode));
                }

            }

            var nowTime = DateTime.Now;
            var userInfo = Utils.GetCurrentUserInfo();
            companyTransferOrder.ModifierId = userInfo.Id;
            companyTransferOrder.ModifierName = userInfo.Name;
            companyTransferOrder.ModifyTime = nowTime;
            companyTransferOrder.ApproverId = userInfo.Id;
            companyTransferOrder.ApproverName = userInfo.Name;
            companyTransferOrder.ApproveTime = nowTime;

            //修改主单状态
            companyTransferOrder.Status = (int)DcsPartsTransferOrderStatus.已审批;
            //标记单据为修改操作
            UpdateToDatabase(companyTransferOrder);

            //获取销售类型
            var partsSalesCategory = ObjectContext.PartsSalesCategories.Where(r => r.Id == companyTransferOrder.PartsSalesCategoryId).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(partsSalesCategory == null) {
                throw new ValidationException(ErrorStrings.PartsSalesCategory_Validation4);
            }
            var warehouse = ObjectContext.Warehouses.Where(r => r.Id == companyTransferOrder.OriginalWarehouseId).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(warehouse == null) {
                throw new ValidationException(ErrorStrings.Warehouse_Validation5);
            }
            //新增配件出库计划单
            var partsOutboundPlan = new PartsOutboundPlan();
            partsOutboundPlan.Code = CodeGenerator.Generate("PartsOutboundPlan", partsOutboundPlan.StorageCompanyCode);
            partsOutboundPlan.CreatorId = userInfo.Id;
            partsOutboundPlan.CreatorName = userInfo.Name;
            partsOutboundPlan.CreateTime = nowTime;
            partsOutboundPlan.WarehouseId = warehouse.Id;
            partsOutboundPlan.WarehouseCode = warehouse.Code;
            partsOutboundPlan.WarehouseName = warehouse.Name;
            partsOutboundPlan.StorageCompanyId = companyTransferOrder.OriginalCompanyId;
            partsOutboundPlan.StorageCompanyCode = companyTransferOrder.OriginalCompanyCode;
            partsOutboundPlan.StorageCompanyName = companyTransferOrder.OriginalCompanyName;
            partsOutboundPlan.StorageCompanyType = (int)DcsCompanyType.服务站兼代理库;
            partsOutboundPlan.BranchId = partsSalesCategory.BranchId;
            partsOutboundPlan.BranchCode = partsSalesCategory.BranchCode;
            partsOutboundPlan.BranchName = partsSalesCategory.BranchName;
            partsOutboundPlan.CounterpartCompanyId = companyTransferOrder.DestCompanyId;
            partsOutboundPlan.CounterpartCompanyCode = companyTransferOrder.DestCompanyCode;
            partsOutboundPlan.CounterpartCompanyName = companyTransferOrder.DestCompanyName;
            partsOutboundPlan.SourceId = companyTransferOrder.Id;
            partsOutboundPlan.SourceCode = companyTransferOrder.Code;
            partsOutboundPlan.OutboundType = (int)DcsPartsOutboundType.配件调拨;
            partsOutboundPlan.CustomerAccountId = null;
            partsOutboundPlan.OriginalRequirementBillId = companyTransferOrder.Id;
            partsOutboundPlan.OriginalRequirementBillCode = companyTransferOrder.Code;
            partsOutboundPlan.OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.企业间配件调拨;
            partsOutboundPlan.Status = (int)DcsPartsOutboundPlanStatus.出库完成;
            partsOutboundPlan.Remark = companyTransferOrder.Remark;
            partsOutboundPlan.ReceivingWarehouseId = null;
            partsOutboundPlan.ReceivingWarehouseCode = null;
            partsOutboundPlan.ReceivingWarehouseName = null;
            partsOutboundPlan.ReceivingCompanyId = companyTransferOrder.DestCompanyId;
            partsOutboundPlan.ReceivingCompanyCode = companyTransferOrder.DestCompanyCode;
            partsOutboundPlan.ReceivingCompanyName = companyTransferOrder.DestCompanyName;
            partsOutboundPlan.PartsSalesCategoryId = companyTransferOrder.PartsSalesCategoryId;
            InsertToDatabase(partsOutboundPlan);

            //生成出库计划单清单
            foreach(var partsOutboundBillDetail in partsOutboundBill.PartsOutboundBillDetails) {
                var partsOutboundPlanDetail = new PartsOutboundPlanDetail();
                partsOutboundPlanDetail.SparePartId = partsOutboundBillDetail.SparePartId;
                partsOutboundPlanDetail.SparePartCode = partsOutboundBillDetail.SparePartCode;
                partsOutboundPlanDetail.SparePartName = partsOutboundBillDetail.SparePartName;
                partsOutboundPlanDetail.PlannedAmount = partsOutboundBillDetail.OutboundAmount;
                partsOutboundPlanDetail.OutboundFulfillment = partsOutboundBillDetail.OutboundAmount;
                partsOutboundPlanDetail.Price = partsOutboundBillDetail.SettlementPrice;
                partsOutboundPlanDetail.Remark = partsOutboundBillDetail.Remark;
                partsOutboundPlan.PartsOutboundPlanDetails.Add(partsOutboundPlanDetail);
                InsertToDatabase(partsOutboundPlanDetail);
            }

            //调用配件出库单新增方法，出库计划单Id=生成的出库计划单.Id
            partsOutboundBill.PartsOutboundPlan = partsOutboundPlan;
            InsertPartsOutboundBill(partsOutboundBill);

            var dealerPartsStrocksTo = ObjectContext.DealerPartsStocks.Where(r => r.DealerId == companyTransferOrder.DestCompanyId && r.SalesCategoryId == companyTransferOrder.PartsSalesCategoryId && partIds.Contains(r.SparePartId)).ToArray();
            var partDeleaveInformations = ObjectContext.PartDeleaveInformations.Where(r => r.BranchId == partsOutboundBill.BranchId && r.PartsSalesCategoryId == companyTransferOrder.PartsSalesCategoryId && partIds.Contains(r.OldPartId) && r.Status == (int)DcsBaseDataStatus.有效).ToArray();
            //对调入经销商的相关配件库存修改
            foreach(var companyTransferOrderDetail in companyTransferOrderDetails) {
                //加拆散量
                var tempPartDeleaveInformations = partDeleaveInformations.Where(r => r.OldPartId == companyTransferOrderDetail.PartsId).ToArray();
                if(tempPartDeleaveInformations.Length > 1) {
                    throw new ValidationException(companyTransferOrderDetail.PartsCode + ErrorStrings.PartsOutboundBill_Validation15);
                }
                var unitDeleaveQuantity = 1;
                var newPartId = companyTransferOrderDetail.PartsId;
                if(tempPartDeleaveInformations.Length == 1) {
                    unitDeleaveQuantity = tempPartDeleaveInformations[0].DeleaveAmount;
                }
                var dealerPartsStock = dealerPartsStrocksTo.SingleOrDefault(r => r.SparePartId == newPartId);
                if(dealerPartsStock != null) {

                    dealerPartsStock.Quantity += ((companyTransferOrderDetail.ConfirmedAmount ?? 0) * unitDeleaveQuantity);
                    dealerPartsStock.ModifierId = userInfo.Id;
                    dealerPartsStock.ModifierName = userInfo.Name;
                    dealerPartsStock.ModifyTime = nowTime;
                    UpdateToDatabase(dealerPartsStock);
                } else {
                    dealerPartsStock = new DealerPartsStock();
                    dealerPartsStock.DealerId = companyTransferOrder.DestCompanyId;
                    dealerPartsStock.DealerCode = companyTransferOrder.DestCompanyCode;
                    dealerPartsStock.DealerName = companyTransferOrder.DestCompanyName;
                    dealerPartsStock.BranchId = partsSalesCategory.BranchId;
                    dealerPartsStock.SubDealerId = -1;
                    dealerPartsStock.SalesCategoryId = companyTransferOrder.PartsSalesCategoryId;
                    dealerPartsStock.SalesCategoryName = partsSalesCategory.Name;
                    dealerPartsStock.SparePartId = companyTransferOrderDetail.PartsId;
                    dealerPartsStock.SparePartCode = companyTransferOrderDetail.PartsCode;
                    dealerPartsStock.Quantity = ((companyTransferOrderDetail.ConfirmedAmount ?? 0) * unitDeleaveQuantity);
                    dealerPartsStock.CreatorId = userInfo.Id;
                    dealerPartsStock.CreatorName = userInfo.Name;
                    dealerPartsStock.CreateTime = nowTime;

                    InsertToDatabase(dealerPartsStock);
                }
            }

            //减新配件库存
            var partsOutboundBillDetailWarehouseAreaIds = partsOutboundBill.PartsOutboundBillDetails.Select(r => r.WarehouseAreaId).ToArray();
            var partsOutboundBillDetailSparePartIds = partsOutboundBill.PartsOutboundBillDetails.Select(r => r.SparePartId).ToArray();
            var partsStocks = new PartsStockAch(this.DomainService).GetPartsStocksWithLock(ObjectContext.PartsStocks.Where(r => r.WarehouseId == partsOutboundBill.WarehouseId && partsOutboundBillDetailWarehouseAreaIds.Contains(r.WarehouseAreaId) && partsOutboundBillDetailSparePartIds.Contains(r.PartId))).ToArray();
            foreach(var partsStock in partsStocks) {
                var stock = partsStock;
                var sumOutboundAmount = partsOutboundBill.PartsOutboundBillDetails.Where(r => r.SparePartId == stock.PartId && r.WarehouseAreaId == stock.WarehouseAreaId).Sum(r => r.OutboundAmount);
                //校验出库清单上配件是否数量充足
                if(partsStock.Quantity < sumOutboundAmount) {
                    var sparePart = this.ObjectContext.SpareParts.SingleOrDefault(r => r.Id == stock.PartId);
                    throw new ValidationException(string.Format(ErrorStrings.PartsStock_Validation4, sparePart.Name));
                }
                partsStock.Quantity -= sumOutboundAmount;
                //减少对应的库存
                new PartsStockAch(this.DomainService).UpdatePartsStockValidate(partsStock);
            }
        }

    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:11:45
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 生成配件出库单(PartsOutboundBill partsOutboundBill) {
            new PartsOutboundBillAch(this).生成配件出库单(partsOutboundBill);
        }

        public void 生成负数配件出库单(PartsOutboundBill partsOutboundBill) {
            new PartsOutboundBillAch(this).生成负数配件出库单(partsOutboundBill);
        }


        [Update(UsingCustomMethod = true)]
        public void 审核服务站兼代理库调拨2(PartsOutboundBill partsOutboundBill) {
            new PartsOutboundBillAch(this).审核服务站兼代理库调拨2(partsOutboundBill);
        }
    }
}