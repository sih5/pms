﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsOutboundPlanAch : DcsSerivceAchieveBase {
        public PartsOutboundPlanAch(DcsDomainService domainService)
            : base(domainService) {
        }

        public void 生成配件出库计划只校验保管区库存(PartsOutboundPlan partsOutboundPlan) {
            var partsOutboundPlanDetails = partsOutboundPlan.PartsOutboundPlanDetails.ToArray();
            var partsOutboundPlanDetailPartIds = partsOutboundPlanDetails.Select(r => r.SparePartId);
            var partsLockedStocks = new PartsLockedStockAch(this.DomainService).GetPartsLockedStocksWithLock(ObjectContext.PartsLockedStocks.Where(r => r.WarehouseId == partsOutboundPlan.WarehouseId && partsOutboundPlanDetailPartIds.Contains(r.PartId))).ToArray();
            var partsStocks = new PartsStockAch(this.DomainService).GetPartsStocksWithLock(ObjectContext.PartsStocks.Where(r => r.WarehouseId == partsOutboundPlan.WarehouseId && partsOutboundPlanDetailPartIds.Contains(r.PartId)
                && ObjectContext.WarehouseAreaCategories.Any(x => x.Category == (int)DcsAreaType.保管区 && x.Id == r.WarehouseAreaCategoryId))).ToArray();
            var wmsCongelationStockViews = ObjectContext.WmsCongelationStockViews.Where(r => r.PartsSalesCategoryId == partsOutboundPlan.PartsSalesCategoryId && r.WarehouseId == partsOutboundPlan.WarehouseId && partsOutboundPlanDetailPartIds.Contains(r.SparePartId)).ToArray();
            List<PartsLockedStock> plss = new List<PartsLockedStock>();
            foreach(var partsOutboundPlanDetail in partsOutboundPlanDetails) {
                var partsLockedStock = partsLockedStocks.FirstOrDefault(r => r.PartId == partsOutboundPlanDetail.SparePartId);
                var addPartsLockedStock = plss.FirstOrDefault(o => o.PartId == partsOutboundPlanDetail.SparePartId);
                if(addPartsLockedStock != null)
                    partsLockedStock = addPartsLockedStock;
                var partsStocksAmount = partsStocks.Where(r => r.PartId == partsOutboundPlanDetail.SparePartId).Sum(v => v.Quantity);
                var congelationStockQtyAmount = wmsCongelationStockViews.Where(r => r.SparePartId == partsOutboundPlanDetail.SparePartId).Sum(v => v.CongelationStockQty);
                var disabledStockAmount = wmsCongelationStockViews.Where(r => r.SparePartId == partsOutboundPlanDetail.SparePartId).Sum(v => v.DisabledStock);
                if(partsLockedStock == null) {
                    if(partsStocksAmount - partsOutboundPlanDetail.PlannedAmount - congelationStockQtyAmount - disabledStockAmount < 0)
                        throw new ValidationException(string.Format(ErrorStrings.PartsOutboundPlan_Validation3, partsOutboundPlanDetail.SparePartCode));
                    var newpartsLockedStock = new PartsLockedStock {
                        WarehouseId = partsOutboundPlan.WarehouseId,
                        PartId = partsOutboundPlanDetail.SparePartId,
                        LockedQuantity = partsOutboundPlanDetail.PlannedAmount,
                        StorageCompanyId = partsOutboundPlan.StorageCompanyId,
                        BranchId = partsOutboundPlan.BranchId
                    };
                    plss.Add(newpartsLockedStock);
                    InsertToDatabase(newpartsLockedStock);
                    new PartsLockedStockAch(this.DomainService).InsertPartsLockedStockValidate(newpartsLockedStock);
                } else {
                    if(partsStocksAmount - partsLockedStock.LockedQuantity - partsOutboundPlanDetail.PlannedAmount - congelationStockQtyAmount - disabledStockAmount < 0)
                        throw new ValidationException(string.Format(ErrorStrings.PartsOutboundPlan_Validation3, partsOutboundPlanDetail.SparePartCode));
                    partsLockedStock.LockedQuantity += partsOutboundPlanDetail.PlannedAmount;
                    new PartsLockedStockAch(this.DomainService).UpdatePartsLockedStockValidate(partsLockedStock);
                }
            }
            //因服务端调用。
            InsertToDatabase(partsOutboundPlan);
            this.InsertPartsOutboundPlanValidate(partsOutboundPlan);
        }

        public void 生成配件出库计划(PartsOutboundPlan partsOutboundPlan) {
            var partsOutboundPlanDetails = partsOutboundPlan.PartsOutboundPlanDetails.ToArray();
            var partsOutboundPlanDetailPartIds = partsOutboundPlanDetails.Select(r => r.SparePartId);
            var partsLockedStocks = new PartsLockedStockAch(this.DomainService).GetPartsLockedStocksWithLock(ObjectContext.PartsLockedStocks.Where(r => r.WarehouseId == partsOutboundPlan.WarehouseId && partsOutboundPlanDetailPartIds.Contains(r.PartId))).ToArray();
            var partsStocks = new PartsStockAch(this.DomainService).GetPartsStocksWithLock(ObjectContext.PartsStocks.Where(r => r.WarehouseId == partsOutboundPlan.WarehouseId && partsOutboundPlanDetailPartIds.Contains(r.PartId))).ToArray();
            var wmsCongelationStockViews = ObjectContext.WmsCongelationStockViews.Where(r => r.PartsSalesCategoryId == partsOutboundPlan.PartsSalesCategoryId && r.WarehouseId == partsOutboundPlan.WarehouseId && partsOutboundPlanDetailPartIds.Contains(r.SparePartId)).ToArray();
            var ids=partsOutboundPlanDetailPartIds.Distinct();
            foreach(var id in ids) {
                var partsLockedStock = partsLockedStocks.FirstOrDefault(r => r.PartId == id);
                var plannedAmount = partsOutboundPlanDetails.Where(r => r.SparePartId == id).Sum(t=>t.PlannedAmount);
                var sparePartCode = partsOutboundPlanDetails.Where(y => y.SparePartId == id).Select(r => r.SparePartCode).First();
                var partsStocksAmount = partsStocks.Where(r => r.PartId == id).Sum(v => v.Quantity);
                var congelationStockQtyAmount = wmsCongelationStockViews.Where(r => r.SparePartId == id).Sum(v => v.CongelationStockQty);
                var disabledStockAmount = wmsCongelationStockViews.Where(r => r.SparePartId == id).Sum(v => v.DisabledStock);
                if(partsLockedStock == null) {
                    if(partsStocksAmount - plannedAmount - congelationStockQtyAmount - disabledStockAmount < 0)
                        throw new ValidationException(string.Format(ErrorStrings.PartsOutboundPlan_Validation3, sparePartCode));
                    var newpartsLockedStock = new PartsLockedStock {
                        WarehouseId = partsOutboundPlan.WarehouseId,
                        PartId = id,
                        LockedQuantity = plannedAmount,
                        StorageCompanyId = partsOutboundPlan.StorageCompanyId,
                        BranchId = partsOutboundPlan.BranchId
                    };
                    InsertToDatabase(newpartsLockedStock);
                } else {
                    if(partsStocksAmount - partsLockedStock.LockedQuantity - plannedAmount - congelationStockQtyAmount - disabledStockAmount < 0)
                        throw new ValidationException(string.Format(ErrorStrings.PartsOutboundPlan_Validation3, sparePartCode));
                    partsLockedStock.LockedQuantity += plannedAmount;
                    UpdateToDatabase(partsLockedStock);
                }
             }
            //foreach(var partsOutboundPlanDetail in partsOutboundPlanDetails) {
            //    var partsLockedStock = partsLockedStocks.FirstOrDefault(r => r.PartId == partsOutboundPlanDetail.SparePartId);
            //    var addPartsLockedStock = plss.FirstOrDefault(o => o.PartId == partsOutboundPlanDetail.SparePartId);
            //    if(addPartsLockedStock != null)
            //        partsLockedStock = addPartsLockedStock;
            //    var partsStocksAmount = partsStocks.Where(r => r.PartId == partsOutboundPlanDetail.SparePartId).Sum(v => v.Quantity);
            //    var congelationStockQtyAmount = wmsCongelationStockViews.Where(r => r.SparePartId == partsOutboundPlanDetail.SparePartId).Sum(v => v.CongelationStockQty);
            //    var disabledStockAmount = wmsCongelationStockViews.Where(r => r.SparePartId == partsOutboundPlanDetail.SparePartId).Sum(v => v.DisabledStock);
            //    if(partsLockedStock == null) {
            //        if(partsStocksAmount - partsOutboundPlanDetail.PlannedAmount - congelationStockQtyAmount - disabledStockAmount < 0)
            //            throw new ValidationException(string.Format(ErrorStrings.PartsOutboundPlan_Validation3, partsOutboundPlanDetail.SparePartCode));
            //        var newpartsLockedStock = new PartsLockedStock {
            //            WarehouseId = partsOutboundPlan.WarehouseId,
            //            PartId = partsOutboundPlanDetail.SparePartId,
            //            LockedQuantity = partsOutboundPlanDetail.PlannedAmount,
            //            StorageCompanyId = partsOutboundPlan.StorageCompanyId,
            //            BranchId = partsOutboundPlan.BranchId
            //        };
            //        plss.Add(newpartsLockedStock);
            //        InsertToDatabase(newpartsLockedStock);
            //    } else {
            //        if(partsStocksAmount - partsLockedStock.LockedQuantity - partsOutboundPlanDetail.PlannedAmount - congelationStockQtyAmount - disabledStockAmount < 0)
            //            throw new ValidationException(string.Format(ErrorStrings.PartsOutboundPlan_Validation3, partsOutboundPlanDetail.SparePartCode));
            //        partsLockedStock.LockedQuantity += partsOutboundPlanDetail.PlannedAmount;
            //        UpdateToDatabase(partsLockedStock);
            //    }
            //}
            //因服务端调用。
            InsertToDatabase(partsOutboundPlan);
            this.InsertPartsOutboundPlanValidate(partsOutboundPlan);
        }


        public void 终止配件出库计划(PartsOutboundPlan partsOutboundPlan) {
            var userInfo = Utils.GetCurrentUserInfo();
            var dbpartsOutboundPlan = ObjectContext.PartsOutboundPlans.Where(r => r.Id == partsOutboundPlan.Id && r.Status != (int)DcsPartsOutboundPlanStatus.终止 && r.Status != (int)DcsPartsOutboundPlanStatus.出库完成).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbpartsOutboundPlan == null)
                throw new ValidationException(ErrorStrings.PartsOutboundPlan_Validation2);
            UpdateToDatabase(partsOutboundPlan);
            partsOutboundPlan.Status = (int)DcsPartsOutboundPlanStatus.终止;
            partsOutboundPlan.StopTime = DateTime.Now;
            partsOutboundPlan.StoperId = userInfo.Id;
            partsOutboundPlan.Stoper = userInfo.Name;

            this.UpdatePartsOutboundPlanValidate(partsOutboundPlan);
            var partsOutboundPlanDetails = partsOutboundPlan.PartsOutboundPlanDetails;
            var partsOutboundPlanDetailPartIds = partsOutboundPlanDetails.Select(r => r.SparePartId);
            var partsLockedStocks = new PartsLockedStockAch(this.DomainService).GetPartsLockedStocksWithLock(ObjectContext.PartsLockedStocks.Where(r => r.WarehouseId == partsOutboundPlan.WarehouseId && partsOutboundPlanDetailPartIds.Contains(r.PartId))).ToArray();
            var pickingTaskDetails = ObjectContext.PickingTaskDetails.Where(o => o.PartsOutboundPlanId == partsOutboundPlan.Id && ObjectContext.PickingTasks.Any(r => r.Status != (int)DcsPickingTaskStatus.作废 && r.Id == o.PickingTaskId));
            var spIds = partsOutboundPlanDetails.Select(t => t.SparePartId).Distinct();
            foreach(var spid in spIds) {
                //查询合计出库计划清单对于的分配量
                var sumPlanQty = pickingTaskDetails.Where(o => o.SparePartId == spid).Sum(o => o.PlanQty);
                var partsLockedStock = partsLockedStocks.FirstOrDefault(r => r.PartId == spid);
                var detail = partsOutboundPlanDetails.Where(r => r.SparePartId == spid).First();
                var planAmount = partsOutboundPlanDetails.Where(t=>t.SparePartId==spid).Select(t => t.PlannedAmount).Sum();
                if(partsLockedStock == null) {
                    throw new ValidationException(string.Format(ErrorStrings.PartsOutboundPlan_Validation1, detail.SparePartCode));
                }
                partsLockedStock.LockedQuantity -= planAmount - (sumPlanQty ?? 0);
                UpdateToDatabase(partsLockedStock);
            }
            var anyPick = ObjectContext.PickingTasks.Where(t => t.PickingTaskDetails.Any(p => p.PartsOutboundPlanId == partsOutboundPlan.Id && (p.PickingQty ?? 0) != 0) && t.Status == (int)DcsPickingTaskStatus.拣货完成).ToArray();
            if(anyPick.Count() > 0) {
                if(partsOutboundPlan.StorageCompanyType == (int)DcsCompanyType.分公司) {

                    var pickingTasks = ObjectContext.PickingTasks.Where(o => o.PickingTaskDetails.Any(p => p.PartsOutboundPlanId == partsOutboundPlan.Id) && o.Status != (int)DcsPickingTaskStatus.已生成装箱任务 && o.Status != (int)DcsPickingTaskStatus.作废);
                    if(pickingTasks.Count() > 0) {
                        throw new ValidationException(ErrorStrings.PartsOutboundPlan_Validation5);
                    }
                } else {
                    var pickingTasks = ObjectContext.PickingTasks.Where(o => o.PickingTaskDetails.Any(p => p.PartsOutboundPlanId == partsOutboundPlan.Id) && (o.IsExistShippingOrder ?? false) == false && o.Status != (int)DcsPickingTaskStatus.作废);
                    if(pickingTasks.Count() > 0) {
                        throw new ValidationException(ErrorStrings.PartsOutboundPlan_Validation5);
                    }
                }
            }
            var boxUpTasks = ObjectContext.BoxUpTasks.Where(o => o.BoxUpTaskDetails.Any(p => p.PartsOutboundPlanId == partsOutboundPlan.Id) && !o.IsExistShippingOrder.Value && o.Status != (int)DcsBoxUpTaskStatus.作废 && o.Status != (int)DcsBoxUpTaskStatus.终止);
            if(boxUpTasks.Count() > 0) {
                throw new ValidationException(ErrorStrings.PartsOutboundPlan_Validation6);
            }
            var partsShippingOrders = ObjectContext.PartsShippingOrders.Where(o => o.PartsShippingOrderDetails.Any(p => p.PartsOutboundPlanId == partsOutboundPlan.Id) && o.Status == (int)DcsPartsShippingOrderStatus.新建);
            if(partsShippingOrders.Count() > 0) {
                throw new ValidationException(ErrorStrings.PartsOutboundPlan_Validation7);
            }
            if(partsOutboundPlan.OutboundType != (int)DcsPartsOutboundType.配件零售 && partsOutboundPlan.OutboundType != (int)DcsPartsOutboundType.配件调拨 && partsOutboundPlan.OutboundType != (int)DcsPartsOutboundType.内部领出 && partsOutboundPlan.OutboundType != (int)DcsPartsOutboundType.采购退货) {
                if(!partsOutboundPlan.CustomerAccountId.HasValue) {
                    throw new ValidationException(ErrorStrings.PartsOutboundPlan_Validation4);
                }
                var sumAmount = partsOutboundPlanDetails.Sum(r => (r.PlannedAmount - (r.OutboundFulfillment.HasValue ? r.OutboundFulfillment.Value : 0)) * r.Price);
                new CustomerAccountAch(this.DomainService).出库计划取消记录流水(partsOutboundPlan, sumAmount);
            }
            if(partsOutboundPlan.IsTurn.HasValue && partsOutboundPlan.IsTurn.Value) {
                var returnOrder = ObjectContext.PartsSalesReturnBills.Where(t => t.Code == partsOutboundPlan.SourceCode).FirstOrDefault();
                if(returnOrder != null && !string.IsNullOrEmpty(returnOrder.ReturnReason)) {
                    //查询原始销售订单
                    var salesCodetr = returnOrder.ReturnReason.Split(new string[] { "转单" }, StringSplitOptions.RemoveEmptyEntries);
                    var salesCode = salesCodetr[0];
                    if(!string.IsNullOrEmpty(salesCode)) {
                        var salesOrder = ObjectContext.PartsSalesOrders.Where(t => t.Code == salesCode).FirstOrDefault();
                        if(salesOrder != null) {
                           
                            var partsOutBoudPlandDetail = (from p in
                                                               (
                                                                from b in partsOutboundPlanDetails
                                                                select new {
                                                                    sparePartId = b.SparePartId,
                                                                    quantity = b.PlannedAmount,
                                                                    outboundFulfillment=b.OutboundFulfillment
                                                                })
                                                           group p by new {
                                                               p.sparePartId
                                                           }
                                                               into tempTable
                                                               select new {
                                                                   SparePartId = tempTable.Key.sparePartId,
                                                                   SumQuantity = tempTable.Sum(v => v.quantity-(v.outboundFulfillment .HasValue?v.outboundFulfillment.Value:0))
                                                               }).ToArray();
                            var salesDetail = ObjectContext.PartsSalesOrderDetails.Where(t => t.PartsSalesOrderId == salesOrder.Id).ToArray();
                            var detail = (from a in salesDetail
                                         join b in partsOutBoudPlandDetail on a.SparePartId equals b.SparePartId
                                         select new {
                                             a.OrderPrice,
                                             b.SumQuantity
                                         }).ToArray();
                            var sumAmount = detail.Sum(r => r.SumQuantity * r.OrderPrice);
                            new CustomerAccountAch(this.DomainService).转中心库销售出库终止记录流水(salesOrder, sumAmount);
                        } else {
                            throw new ValidationException("未找到有效的 销售订单");
                        }
                    } else {
                        throw new ValidationException("未找到有效的 销售订单");
                    }
                } else {
                    throw new ValidationException("未找到有效的 销售退货订单");
                }
            }
        }

        public void 批量生成配件出库计划(PartsOutboundPlan[] partsOutboundPlans) {
            foreach(var partsOutboundPlan in partsOutboundPlans) {
                //因服务端调用。
                InsertToDatabase(partsOutboundPlan);
                this.InsertPartsOutboundPlanValidate(partsOutboundPlan);
            }
            var partsOutBoundPlanStructPartsSalesCategoryAndWarehouseId = (from p in
                                                                               (from a in partsOutboundPlans
                                                                                from b in a.PartsOutboundPlanDetails
                                                                                select new {
                                                                                    warehouseId = a.WarehouseId,
                                                                                    sparePartId = b.SparePartId,
                                                                                    partsSalesCategoryId = a.PartsSalesCategoryId,
                                                                                    sparePartCode = b.SparePartCode,
                                                                                    storageCompanyId = a.StorageCompanyId,
                                                                                    branchId = a.BranchId,
                                                                                    quantity = b.PlannedAmount
                                                                                })
                                                                           group p by new {
                                                                               p.warehouseId,
                                                                               p.sparePartId,
                                                                               p.sparePartCode,
                                                                               p.storageCompanyId,
                                                                               p.branchId,
                                                                               p.partsSalesCategoryId
                                                                           }
                                                                               into tempTable
                                                                               select new {
                                                                                   WarehouseId = tempTable.Key.warehouseId,
                                                                                   SparePartId = tempTable.Key.sparePartId,
                                                                                   SparePartCode = tempTable.Key.sparePartCode,
                                                                                   StorageCompanyId = tempTable.Key.storageCompanyId,
                                                                                   BranchId = tempTable.Key.branchId,
                                                                                   PartsSalesCategoryId = tempTable.Key.partsSalesCategoryId,
                                                                                   SumQuantity = tempTable.Sum(v => v.quantity)
                                                                               }).ToArray();
            var partsOutBoundPlanStructWarehouseId = (from p in
                                                          (from a in partsOutboundPlans
                                                           from b in a.PartsOutboundPlanDetails
                                                           select new {
                                                               warehouseId = a.WarehouseId,
                                                               sparePartId = b.SparePartId,
                                                               sparePartCode = b.SparePartCode,
                                                               storageCompanyId = a.StorageCompanyId,
                                                               branchId = a.BranchId,
                                                               quantity = b.PlannedAmount
                                                           })
                                                      group p by new {
                                                          p.warehouseId,
                                                          p.sparePartId,
                                                          p.sparePartCode,
                                                          p.storageCompanyId,
                                                          p.branchId,
                                                      }
                                                          into tempTable
                                                          select new {
                                                              WarehouseId = tempTable.Key.warehouseId,
                                                              SparePartId = tempTable.Key.sparePartId,
                                                              SparePartCode = tempTable.Key.sparePartCode,
                                                              StorageCompanyId = tempTable.Key.storageCompanyId,
                                                              BranchId = tempTable.Key.branchId,
                                                              SumQuantity = tempTable.Sum(v => v.quantity)
                                                          }).ToArray();


            var groupByWarehouseId = from a in partsOutBoundPlanStructPartsSalesCategoryAndWarehouseId
                                     group a by new {
                                         a.WarehouseId,
                                         a.StorageCompanyId,
                                         a.BranchId
                                     };
            foreach(var tempGroupByWarehouseId in groupByWarehouseId) {
                var sparepartIds = tempGroupByWarehouseId.Select(r => r.SparePartId).ToArray();
                var warehouseId = tempGroupByWarehouseId.Key.WarehouseId;
                var partsLockedStocks = new PartsLockedStockAch(this.DomainService).GetPartsLockedStocksWithLock(ObjectContext.PartsLockedStocks.Where(r => r.WarehouseId == warehouseId && sparepartIds.Contains(r.PartId))).ToArray();
                var partsStocks = ObjectContext.PartsStocks.Where(r => r.WarehouseId == warehouseId && sparepartIds.Contains(r.PartId)).ToArray();
                var groupByPartsSalesCategory = from a in tempGroupByWarehouseId
                                                group a by new {
                                                    a.PartsSalesCategoryId
                                                };
                foreach(var tempGroupByPartSalesCategory in groupByPartsSalesCategory) {
                    var partsSalesCategoryId = tempGroupByPartSalesCategory.Key.PartsSalesCategoryId;
                    var wmsCongelationStockViews = ObjectContext.WmsCongelationStockViews.Where(r => r.PartsSalesCategoryId == partsSalesCategoryId && r.WarehouseId == warehouseId && sparepartIds.Contains(r.SparePartId)).ToArray();

                    foreach(var item in partsOutBoundPlanStructWarehouseId) {
                        var partsLockedStock = partsLockedStocks.FirstOrDefault(r => r.PartId == item.SparePartId && r.WarehouseId == item.WarehouseId);
                        var partsStocksAmount = partsStocks.Where(r => r.PartId == item.SparePartId && r.WarehouseId == item.WarehouseId).Sum(v => v.Quantity);
                        var congelationStockQtyAmount = wmsCongelationStockViews.Where(r => r.SparePartId == item.SparePartId).Sum(v => v.CongelationStockQty);
                        var disabledStockAmount = wmsCongelationStockViews.Where(r => r.SparePartId == item.SparePartId).Sum(v => v.DisabledStock);

                        if(partsLockedStock == null) {
                            if(partsStocksAmount - item.SumQuantity - congelationStockQtyAmount - disabledStockAmount < 0)
                                throw new ValidationException(string.Format(ErrorStrings.PartsOutboundPlan_Validation3, item.SparePartCode));
                            var newpartsLockedStock = new PartsLockedStock {
                                WarehouseId = item.WarehouseId,
                                PartId = item.SparePartId,
                                LockedQuantity = item.SumQuantity,
                                StorageCompanyId = item.StorageCompanyId,
                                BranchId = item.BranchId
                            };
                            InsertToDatabase(newpartsLockedStock);
                            new PartsLockedStockAch(this.DomainService).InsertPartsLockedStockValidate(newpartsLockedStock);
                        } else {
                            if(partsStocksAmount - partsLockedStock.LockedQuantity - item.SumQuantity - congelationStockQtyAmount - disabledStockAmount < 0)
                                throw new ValidationException(string.Format(ErrorStrings.PartsOutboundPlan_Validation3, item.SparePartCode));
                            partsLockedStock.LockedQuantity += item.SumQuantity;
                            new PartsLockedStockAch(this.DomainService).UpdatePartsLockedStockValidate(partsLockedStock);
                        }
                    }
                }
            }






        }

        public void 生成捡货单(int[] ids) {
            var userInfo = Utils.GetCurrentUserInfo();
            if(userInfo.EnterpriseId==0) {
                throw new ValidationException(ErrorStrings.User_Invalid);
            }
            //所有的出库计划单
            var search = ObjectContext.PartsOutboundPlans.Where(r => ids.Contains(r.Id)).ToArray();
            var planid = search.Select(r => r.Id).ToList();
            //查询所有的计划单的清单配件id
            var detailSpid = ObjectContext.PartsOutboundPlanDetails.Where(t => planid.Contains(t.PartsOutboundPlanId)).Select(r => r.SparePartId).ToArray();
            //所有的保管区的库存信息
            var queryStocks = (from a in ObjectContext.PartsStocks
                               join b in ObjectContext.WarehouseAreaCategories on a.WarehouseAreaCategoryId equals b.Id
                               where b.Category == 1 && (a.Quantity > a.LockedQty || (a.LockedQty == null && a.Quantity > 0)) && detailSpid.Contains(a.PartId)
                               select new {
                                   Id = a.Id
                               }).ToArray();
            if(0 == queryStocks.Count()) {
                throw new ValidationException(ErrorStrings.PartsOutboundPlan_Validation8);
            }
            var stocksId = queryStocks.Select(r => r.Id).ToArray();
            var partsStocks = ObjectContext.PartsStocks.Include("WarehouseArea").Where(r => stocksId.Contains(r.Id)).ToArray();
            //  var partsStocks = ObjectContext.PartsStocks.Include("WarehouseArea").Where(r => queryStocks.Any(s => s.Id == r.Id)).ToArray();
            PickingTask pick = new PickingTask();
            for(int i = 0; i < search.Count(); i++) {
                //主单
                if(i == 0) {
                    pick.CounterpartCompanyCode = search[i].CounterpartCompanyCode;
                    pick.CounterpartCompanyId = search[i].CounterpartCompanyId;
                    pick.CounterpartCompanyName = search[i].CounterpartCompanyName;
                    pick.PartsSalesCategoryId = search[i].PartsSalesCategoryId;
                    pick.WarehouseId = search[i].WarehouseId;
                    pick.WarehouseCode = search[i].WarehouseCode;
                    pick.WarehouseName = search[i].WarehouseName;
                    pick.OrderTypeId = search[i].PartsSalesOrderTypeId;
                    pick.OrderTypeName = search[i].PartsSalesOrderTypeName;
                    pick.ShippingMethod = search[i].ShippingMethod;
                    pick.Code = CodeGenerator.Generate("PickingTask", userInfo.EnterpriseCode);
                    pick.CreateTime = DateTime.Now;
                    pick.CreatorId = userInfo.Id;
                    pick.CreatorName = userInfo.Name;
                    pick.Status = (int)DcsPickingTaskStatus.新建;
                    pick.BranchId = userInfo.EnterpriseId;
                }
                //清单
                //根据出库计划单查询清单
                //最终判断是否都已生成拣货单
                bool isOver = false;
                int partsOutboundPlanId = search[i].Id;
                var partsOutboundPlanDetail = ObjectContext.PartsOutboundPlanDetails.Include("SparePart").Where(r => r.PartsOutboundPlanId == partsOutboundPlanId).ToArray();
                if(partsOutboundPlanDetail.Count() == 0) {
                    throw new ValidationException(ErrorStrings.PartsOutboundPlan_Validation9);
                }

                for(int j = 0; j < partsOutboundPlanDetail.Count(); j++) {
                    //查找配件的库存信息
                    int spartsId = partsOutboundPlanDetail[j].SparePartId;
                    int warehouseId = search[i].WarehouseId;
                    var partsstock = (from a in partsStocks.Where(t => t.WarehouseArea.Status == (int)DcsBaseDataStatus.有效)
                                      where a.PartId == spartsId && a.WarehouseId == warehouseId
                                      orderby a.PartId, a.Quantity 
                                      select a
                                     ).ToArray();
                    if(0 == partsstock.Count()) {
                        throw new ValidationException(string.Format(ErrorStrings.PartsOutboundPlan_Validation10, partsOutboundPlanDetail[j].SparePartCode));
                    }
                    //应该拣货数量
                    if(null == partsOutboundPlanDetail[j].DownloadQty) {
                        partsOutboundPlanDetail[j].DownloadQty = 0;
                    }
                    int qty = Convert.ToInt32(partsOutboundPlanDetail[j].PlannedAmount) - Convert.ToInt32(partsOutboundPlanDetail[j].DownloadQty);
                    //初始需要拣的数量
                    int initialQty = qty;
                    //最终拣货量
                    int pickQty = 0;
                    for(int k = 0; k < partsstock.Count(); k++) {
                        if(qty == 0) {
                            continue;
                        }
                        var spareId = partsstock[k].Id;
                        var partsStockUp = new PartsStockAch(this.DomainService).GetPartsStocksWithLock((from a in ObjectContext.PartsStocks
                                                                                                         where a.Id == spareId
                                                                                                         select a)).FirstOrDefault();
                        PickingTaskDetail detail = new PickingTaskDetail {
                            PartsOutboundPlanId = search[i].Id,
                            PartsOutboundPlanCode = search[i].Code,
                            SourceCode = search[i].SourceCode,
                            SparePartId = partsOutboundPlanDetail[j].SparePartId,
                            SparePartCode = partsOutboundPlanDetail[j].SparePartCode,
                            SparePartName = partsOutboundPlanDetail[j].SparePartName,
                            WarehouseAreaId = partsStockUp.WarehouseAreaId,
                            MeasureUnit = partsOutboundPlanDetail[j].SparePart.MeasureUnit,
                            WarehouseAreaCode = partsStockUp.WarehouseArea.Code,
                            Price = partsOutboundPlanDetail[j].Price
                        };
                        if(null == partsStockUp.LockedQty) {
                            partsStockUp.LockedQty = 0;
                        }
                        if(qty <= (partsStockUp.Quantity - partsStockUp.LockedQty)) {
                            detail.PlanQty = qty;
                            //插入拣货任务单清单
                            //  InsertToDatabase(detail);
                            pick.PickingTaskDetails.Add(detail);

                            pickQty = pickQty + qty;
                            //更新 库存，更加锁定量
                            partsStockUp.LockedQty = Convert.ToInt32(partsStockUp.LockedQty) + qty;
                            partsStockUp.ModifierId = userInfo.Id;
                            partsStockUp.ModifierName = userInfo.Name;
                            partsStockUp.ModifyTime = DateTime.Now;
                            UpdateToDatabase(partsStockUp);
                            qty = 0;
                        } else {
                            //本次下载量
                            int downAmount = Convert.ToInt32(partsStockUp.Quantity) - Convert.ToInt32(partsStockUp.LockedQty);
                            detail.PlanQty = downAmount;
                            //插入拣货任务单清单
                            // InsertToDatabase(detail);
                            pick.PickingTaskDetails.Add(detail);
                            //更新出库计划单的拣货量
                            if(null == partsOutboundPlanDetail[j].DownloadQty) {
                                partsOutboundPlanDetail[j].DownloadQty = 0;
                            }
                            pickQty = pickQty + downAmount;
                            //更新 库存，更加锁定量
                            partsStockUp.LockedQty = Convert.ToInt32(partsStockUp.LockedQty) + downAmount;
                            partsStockUp.ModifierId = userInfo.Id;
                            partsStockUp.ModifierName = userInfo.Name;
                            partsStockUp.ModifyTime = DateTime.Now;
                            UpdateToDatabase(partsStockUp);
                            //剩下应该拣货的
                            qty = qty - downAmount;

                        }
                    }
                    //更新出库计划单的拣货量
                    partsOutboundPlanDetail[j].DownloadQty = partsOutboundPlanDetail[j].DownloadQty + pickQty;
                    UpdateToDatabase(partsOutboundPlanDetail[j]);
                    if(initialQty > pickQty) {
                        //未全部生成装拣货任务单
                        isOver = true;
                    }
                }
                //更新出库计划单的状态
                if(isOver) {
                    search[i].Status = (int)DcsPartsOutboundPlanStatus.部分分配;
                } else {
                    search[i].Status = (int)DcsPartsOutboundPlanStatus.分配完成
                        ;
                }
                InsertToDatabase(pick);
                search[i].ModifierId = userInfo.Id;
                search[i].ModifierName = userInfo.Name;
                search[i].ModifyTime = DateTime.Now;
                UpdateToDatabase(search[i]);

            }
            ObjectContext.SaveChanges();
        }

        public void 更新出库计划单状态(PartsOutboundPlan partsOutboundPlan) {
            // 计算检验状态
            var partsOutboundPlanDetails = ObjectContext.PartsOutboundPlanDetails.Where(r => r.PartsOutboundPlanId == partsOutboundPlan.Id).ToArray();
            var billPlanAmount = partsOutboundPlanDetails.Sum(o => o.PlannedAmount);
            var billOutCompleteAmount = partsOutboundPlanDetails.Sum(o => o.OutboundFulfillment ?? 0);

            var pickingTaskDetails = ObjectContext.PickingTaskDetails.Where(r => r.PartsOutboundPlanId == partsOutboundPlan.Id && ObjectContext.PickingTasks.Any(o => o.Id == r.PickingTaskId && o.Status != (int)DcsPickingTaskStatus.作废)).ToArray();
            var pickingAmount = pickingTaskDetails.Sum(o => o.PickingQty ?? 0);

            var boxUpTaskDetails = ObjectContext.BoxUpTaskDetails.Where(r => r.PartsOutboundPlanId == partsOutboundPlan.Id && ObjectContext.BoxUpTasks.Any(o => o.Id == r.BoxUpTaskId && (o.Status != (int)DcsBoxUpTaskStatus.终止 || o.Status != (int)DcsBoxUpTaskStatus.作废))).ToArray();
            var boxUpAmount = boxUpTaskDetails.Sum(o => o.BoxUpQty ?? 0);

            var shippingDetails = ObjectContext.PartsShippingOrderDetails.Where(r => r.PartsOutboundPlanId == partsOutboundPlan.Id && ObjectContext.PartsShippingOrders.Any(o => o.Id == r.PartsShippingOrderId && o.Status != (int)DcsPartsShippingOrderStatus.作废)).ToArray();
            var shippingAmountSum = shippingDetails.Sum(o => o.ShippingAmount);

            if(billOutCompleteAmount == billPlanAmount && billOutCompleteAmount > 0)
                partsOutboundPlan.Status = (int)DcsPartsOutboundPlanStatus.出库完成;
            else if(billOutCompleteAmount < billPlanAmount && billOutCompleteAmount > 0)
                partsOutboundPlan.Status = (int)DcsPartsOutboundPlanStatus.部分出库;
            else if(shippingAmountSum == billPlanAmount && shippingAmountSum > 0)
                partsOutboundPlan.Status = (int)DcsPartsOutboundPlanStatus.发运完成;
            else if(shippingAmountSum < billPlanAmount && shippingAmountSum > 0)
                partsOutboundPlan.Status = (int)DcsPartsOutboundPlanStatus.部分发运;
            else if(boxUpAmount < billPlanAmount && boxUpAmount > 0)
                partsOutboundPlan.Status = (int)DcsPartsOutboundPlanStatus.部分装箱;
            else if(boxUpAmount == billPlanAmount && boxUpAmount > 0)
                partsOutboundPlan.Status = (int)DcsPartsOutboundPlanStatus.装箱完成;
            else if(pickingAmount == billPlanAmount && pickingAmount > 0)
                partsOutboundPlan.Status = (int)DcsPartsOutboundPlanStatus.拣货完成;
            else if(pickingAmount < billPlanAmount && pickingAmount > 0)
                partsOutboundPlan.Status = (int)DcsPartsOutboundPlanStatus.部分拣货;

            new PartsOutboundPlanAch(this.DomainService).UpdatePartsOutboundPlanValidate(partsOutboundPlan);
            ObjectContext.SaveChanges();
        }
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:11:45
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 生成配件出库计划(PartsOutboundPlan partsOutboundPlan) {
            new PartsOutboundPlanAch(this).生成配件出库计划(partsOutboundPlan);
        }

        [Update(UsingCustomMethod = true)]
        public void 生成配件出库计划只校验保管区库存(PartsOutboundPlan partsOutboundPlan) {
            new PartsOutboundPlanAch(this).生成配件出库计划只校验保管区库存(partsOutboundPlan);
        }

        [Update(UsingCustomMethod = true)]
        public void 终止配件出库计划(PartsOutboundPlan partsOutboundPlan) {
            new PartsOutboundPlanAch(this).终止配件出库计划(partsOutboundPlan);
        }

        public void 批量生成配件出库计划(PartsOutboundPlan[] partsOutboundPlans) {
            new PartsOutboundPlanAch(this).批量生成配件出库计划(partsOutboundPlans);
        }
        [Invoke(HasSideEffects = true)]
        public void 生成捡货单(int[] ids) {
            new PartsOutboundPlanAch(this).生成捡货单(ids);
        }
    }

}
