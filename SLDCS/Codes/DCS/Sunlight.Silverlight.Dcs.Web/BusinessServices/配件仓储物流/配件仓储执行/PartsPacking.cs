﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Sunlight.Silverlight.Dcs.Web.Resources;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsPackingAch : DcsSerivceAchieveBase {
        public PartsPackingAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 生成配件包装单(PartsPacking partsPacking) {
            var virtualPartsStock =DomainService. 查询待包装配件库存().FirstOrDefault(r => r.PartsStockId == partsPacking.PartsStockId);
            if(virtualPartsStock == null)
                //未查询到待包装配件库存
                throw new ValidationException(string.Format(ErrorStrings.PartsPacking_Validation1, partsPacking.SparePartCode, partsPacking.WarehouseAreaCode));
            if(virtualPartsStock.Quantity < partsPacking.Quantity)
                //本次包装数量大于待包装数量
                throw new ValidationException(string.Format(ErrorStrings.PartsPacking_Validation2, partsPacking.SparePartCode, partsPacking.WarehouseAreaCode));
            var partsBatchRecord = new PartsBatchRecord {
                PartId = partsPacking.SparePartId,
                Quantity = partsPacking.Quantity,
                Status = (int)DcsBaseDataStatus.有效,
            };
            InsertToDatabase(partsBatchRecord);
            new PartsBatchRecordAch(this.DomainService).InsertPartsBatchRecordValidate(partsBatchRecord);
            partsPacking.BatchNumber = partsBatchRecord.BatchNumber;
            InsertToDatabase(partsPacking);
            this.InsertPartsPackingValidate(partsPacking);
            //配件库存批次明细
            var partsStockBatchDetail = new PartsStockBatchDetail {
                PartsStockId = partsPacking.PartsStockId,
                WarehouseId = partsPacking.WarehouseId,
                StorageCompanyId = partsPacking.StorageCompanyId,
                StorageCompanyType = partsPacking.StorageCompanyType,
                PartId = partsPacking.SparePartId,
                BatchNumber = partsPacking.BatchNumber,
                Quantity = partsPacking.Quantity,
                InboundTime = DateTime.Now
            };
            InsertToDatabase(partsStockBatchDetail);
            new PartsStockBatchDetailAch(this.DomainService).InsertPartsStockBatchDetailValidate(partsStockBatchDetail);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:11:45
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 生成配件包装单(PartsPacking partsPacking) {
            new PartsPackingAch(this).生成配件包装单(partsPacking);
        }
    }
}
