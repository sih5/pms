﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Dcs.Web.Resources;
using System.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsShelvesTaskAch : DcsSerivceAchieveBase {
        public PartsShelvesTaskAch(DcsDomainService domainService)
            : base(domainService) {
        }
        public IEnumerable<PartsInboundPerformanceQuery> 查询上架任务单上架详情() {
            var ss = from a in ObjectContext.PartsInboundPerformances.Where(t => t.InType == (int)DcsPartsInboundPer_InType.上架)
                     join b in ObjectContext.WarehouseAreas on a.WarehouseAreaId equals b.Id
                     join c in ObjectContext.WarehouseAreas on b.ParentId equals c.Id
                     select new PartsInboundPerformanceQuery {
                         Id = a.Id,
                         Code=a.Code,
                         WarehouseAreaCode = b.Code,
                         Equipment = a.Equipment,
                         OperaterEndTime = a.OperaterEndTime,
                         OperaterName = a.OperaterName,
                         ParentCode = c.Code,
                         Qty = a.Qty,
                         SpareCode = a.SpareCode,
                         SpareName = a.SpareName,
                         SparePartId = a.SparePartId
                     };
            return ss;
        }
        public IEnumerable<VirtualPartsShelvesTask> 查询配件上架任务(string code, string sourceBillCode, string partsInboundPlanCode, string partsInboundCheckBillCode, int? inboundType, string status,
            int? warehouseId, string sparePartCode, string sparePartName, bool? noWarehouseArea, string warehouseAreaCode, string warehouseArea, bool? hasDifference, DateTime? bCreateTime, DateTime? eCreateTime, DateTime? bShelvesFinishTime, DateTime? eShelvesFinishTime) {
            string SQL = @" select Id,Code,PackingTaskCode,PartsInboundPlanCode,PartsInboundCheckBillCode,WarehouseId,WarehouseCode,WarehouseName,SparePartCode,SparePartName,BatchNumber, PlannedAmount,
                            ShelvesAmount,SourceBillCode,Status,CreatorName,CreateTime,ModifierName,ModifyTime,ShelvesFinishTime,Remark, NoWarehouseArea,WarehouseAreaCode,WarehouseArea,inboundtype,HasDifference,SIHCodes from (select pst.id                        as Id,
                                    pst.code                      as Code,
                                    pst.packingTaskCode           as PackingTaskCode,
                                    pst.partsInboundPlanCode      as PartsInboundPlanCode,
                                    pst.PartsInboundCheckBillCode as PartsInboundCheckBillCode,
                                    pst.WarehouseId               as WarehouseId,
                                    pst.WarehouseCode             as WarehouseCode,
                                    pst.WarehouseName             as WarehouseName,
                                    sp.Code                       as SparePartCode,
                                    sp.Name                       as SparePartName,
                                    pst.BatchNumber               as BatchNumber,
                                    pst.PlannedAmount             as PlannedAmount,
                                    pst.ShelvesAmount             as ShelvesAmount,
                                    pst.SourceBillCode            as SourceBillCode,
                                    pst.Status                    as Status,
                                    pst.CreatorName               as CreatorName,
                                    pst.CreateTime                as CreateTime,
                                    pst.ModifierName              as ModifierName,
                                    pst.ModifyTime                as ModifyTime,
                                    pst.ShelvesFinishTime,
                                    pst.Remark, decode(yy.WarehouseAreaCode,null,1,0)  as noWarehouseArea,
                                    yy.WarehouseAreaCode as WarehouseAreaCode,
                                    yy.warehousearea  as WarehouseArea,
                                    pp.inboundtype,pst.SIHCodes,
                                    (select count(1) from PartsDifferenceBackBill d inner join PartsDifferenceBackBillDtl dd on d.Id=dd.PartsDifferenceBackBillId where d.type =1 and d.status<>99 and rownum=1 and dd.TaskId=pst.Id) as HasDifference
                                from partsshelvestask pst
                                inner join sparepart sp
                                on sp.id = pst.sparepartId
                                inner join partsinboundplan pip
                                on pip.id = pst.partsinboundplanid
                                inner join WarehouseOperator wo on wo.warehouseid = pst.warehouseid and wo.operatorid = {0}
                                join PartsInboundPlan pp on pst.partsinboundplanid = pp.id
                                left join (select WarehouseArea.code as WarehouseAreaCode,
                                        partsstock.warehouseid       as warehouseid,
                                        sd.code            as warehousearea,
                                        partsstock.partid as partid,
                                        row_number() over(partition by  partsstock.warehouseid,partsstock.partid order by  WarehouseArea.Id) my_rank
                                  from WarehouseArea                              
                                 inner join WarehouseAreaCategory
                                    on WarehouseArea.AreaCategoryId =
                                       WarehouseAreaCategory.Id
                                   AND WarehouseAreaCategory.Category = 1
                                 inner join warehouseareamanager
                                    on warehouseareamanager.warehouseareaid =
                                       warehousearea.ParentId
                                   and warehouseareamanager.managerid = {0}
                                 inner join WarehouseArea sd
                                    on sd.id = warehousearea.ParentId
                                 inner join partsstock
                                    on partsstock.warehouseareaid = warehousearea.id         
                                 ) yy
                        on yy.warehouseid = pst.warehouseid and yy.partid = sp.id and my_rank=1 
                                where 1=1 {1})temp where 1=1 
                                ";
            var serachSQL = new StringBuilder();
            if(!string.IsNullOrEmpty(code)) {
                serachSQL.Append(" and pst.code like '%" + code + "%'");
            }
            if(!string.IsNullOrEmpty(sourceBillCode)) {
                serachSQL.Append(" and pst.sourceBillCode like '%" + sourceBillCode + "%'");
            }
            if(!string.IsNullOrEmpty(partsInboundPlanCode)) {
                serachSQL.Append(" and pst.partsInboundPlanCode like '%" + partsInboundPlanCode + "%'");
            }
            if(!string.IsNullOrEmpty(partsInboundCheckBillCode)) {
                serachSQL.Append(" and pst.partsInboundCheckBillCode like '%" + partsInboundCheckBillCode + "%'");
            }
            if(inboundType.HasValue) {
                serachSQL.Append(" and pip.inboundtype=" + inboundType.Value);
            }
            if(!string.IsNullOrEmpty(status)) {
                serachSQL.Append(" and pst.status in (" + status + ")");
            }
            if(warehouseId.HasValue) {
                serachSQL.Append(" and pst.warehouseId=" + warehouseId.Value);
            }
            if(!string.IsNullOrEmpty(sparePartCode)) {
                serachSQL.Append(" and sp.code like '%" + sparePartCode + "%'");
            }
            if(!string.IsNullOrEmpty(sparePartName)) {
                serachSQL.Append(" and sp.name like '%" + sparePartName + "%'");
            }
            if(!string.IsNullOrEmpty(warehouseAreaCode)) {
                SQL = SQL + " and WarehouseAreaCode like '%" + warehouseAreaCode + "%'";
            }
            if(noWarehouseArea != null) {
                if((bool)noWarehouseArea)
                    SQL = SQL + " and noWarehouseArea = 1";
                else
                    SQL = SQL + " and noWarehouseArea = 0";
            }
            if(bCreateTime.HasValue) {
                serachSQL.Append(" and pst.CreateTime>= to_date('" + bCreateTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')");
            }
            if(eCreateTime.HasValue) {
                serachSQL.Append(" and pst.CreateTime<= to_date('" + eCreateTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')");
            }
            if(bShelvesFinishTime.HasValue) {
                serachSQL.Append(" and pst.ShelvesFinishTime>= to_date('" + bShelvesFinishTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')");
            }
            if(eShelvesFinishTime.HasValue) {
                serachSQL.Append(" and pst.ShelvesFinishTime<= to_date('" + eShelvesFinishTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')");
            }
            if(!string.IsNullOrEmpty(warehouseArea)) {
                serachSQL = serachSQL.Append(" and yy.warehouseArea like '%" + warehouseArea + "%'");
            }
            var user = Utils.GetCurrentUserInfo();
            SQL = string.Format(SQL, user.Id,  serachSQL);
            if(hasDifference == true) {
                SQL +=" and temp.HasDifference=1 ";
            }
            if(hasDifference == false) {
                SQL +=" and temp.HasDifference=0 ";
            }
            SQL += " order by  temp.CreateTime";
            var search = ObjectContext.ExecuteStoreQuery<VirtualPartsShelvesTask>(SQL).ToList();
            return search;
        }

        public IEnumerable<VirtualPartsShelvesTaskDetail> GetVirtualPartsShelvesTaskDetailById(int[] ids) {
            string SQL = @"select pst.id as Id, pst.code as Code,
                                    pst.packingTaskId as PackingTaskId,
                                    pst.packingTaskCode as PackingTaskCode,pst.partsInboundPlanId as PartsInboundPlanId,
                                    pst.partsInboundPlanCode as PartsInboundPlanCode,pst.PartsInboundCheckBillId as PartsInboundCheckBillId,
                                    pst.PartsInboundCheckBillCode as PartsInboundCheckBillCode,
                                    pst.SparePartId as SparePartId,
                                    sp.Code as SparePartCode,
                                    sp.Name as SparePartName,
                                    sp.MeasureUnit as MeasureUnit,
                                    pst.BatchNumber as BatchNumber,
                                    pst.PlannedAmount as PlannedAmount,
                                    nvl(pst.ShelvesAmount,0) as ShelvesAmount,
                                    pst.PlannedAmount - nvl(pst.ShelvesAmount,0) as NowShelvesAmount,
                                    pst.SourceBillCode as SourceBillCode,
                                    pst.WarehouseId,pst.WarehouseCode,pst.WarehouseName,pst.Remark,pst.SIHCodes,sp.TraceProperty
                                    from partsshelvestask pst
                                    inner join sparepart sp on sp.id = pst.sparepartId
                                    where 1=1 
                                ";
            if(ids != null && ids.Length > 0) {
                SQL = SQL + " and pst.id in (";
                string statusStr = "";
                for(var i = 0; i < ids.Length; i++) {
                    statusStr = statusStr + ids[i] + ",";
                }
                statusStr = statusStr.Substring(0, statusStr.Length - 1);
                SQL = SQL + statusStr + ")";
            }
            SQL = SQL + " order by pst.code desc";

            var search = ObjectContext.ExecuteStoreQuery<VirtualPartsShelvesTaskDetail>(SQL).ToList();
            return search;

        }


        public void DoPartsShelves(VirtualPartsShelvesTaskDetail[] virtualPartsShelvesTaskDetails) {
            var userInfo = Utils.GetCurrentUserInfo();
            if(string.IsNullOrEmpty(userInfo.Name))
                throw new HttpException(401, ErrorStrings.User_Invalid);
            var warehouseIds = virtualPartsShelvesTaskDetails.Select(r => r.WarehouseId).Distinct();
            if(warehouseIds.Count() > 1) {
                throw new ValidationException(ErrorStrings.PartsShelvesTask_Validation1);
            }
            //上架任务单
            var ids = virtualPartsShelvesTaskDetails.Select(r => r.Id);
            var partsShelvesTaskOrigs = ObjectContext.PartsShelvesTasks.Where(r => ids.Contains(r.Id)).ToArray();
            var nowSum = virtualPartsShelvesTaskDetails.Sum(r => r.NowShelvesAmount);


            if(nowSum > 0) {
                var details = virtualPartsShelvesTaskDetails.Where(r => r.NowShelvesAmount > 0).ToList();
                var zeroDetails = virtualPartsShelvesTaskDetails.Where(r => r.NowShelvesAmount == 0).ToList();
                var warehouseId = warehouseIds.FirstOrDefault();
                var partIds = details.Select(r => r.SparePartId).Distinct().ToArray();
                var spareparts = ObjectContext.SpareParts.Where(r => partIds.Contains(r.Id)).ToArray();
                var partsInboundPlanIds = details.Select(r => r.PartsInboundPlanId).Distinct();
                var partsInboundCheckBillIds = details.Select(r => r.PartsInboundCheckBillId).Distinct();
                var partsInboundCheckBillDetails = ObjectContext.PartsInboundCheckBillDetails.Where(r => partsInboundCheckBillIds.Contains(r.PartsInboundCheckBillId)).ToList();
                var partsInboundPlans = ObjectContext.PartsInboundPlans.Where(r => partsInboundPlanIds.Contains(r.Id)).ToList();
                var accurateTraces = ObjectContext.AccurateTraces.Where(t =>partIds.Contains(t.PartId.Value)&& t.CompanyId == userInfo.EnterpriseId && t.Type == (int)DCSAccurateTraceType.入库 && t.Status == (int)DCSAccurateTraceStatus.有效 && t.SIHLabelCode!=null).ToArray();

                var warehouseAreaIds = partsInboundCheckBillDetails.Select(r => r.WarehouseAreaId).Distinct();
                var targetWarehouseAreaIds = details.Select(r => r.WarehouseAreaId).Distinct();
                var partsStocks = new PartsStockAch(this.DomainService).GetPartsStocksWithLock
                (ObjectContext.PartsStocks.Where(r => partIds.Contains(r.PartId) && warehouseAreaIds.Contains(r.WarehouseAreaId) && r.WarehouseId == warehouseId)).ToList();
                var targetPartsStocks = new PartsStockAch(this.DomainService).GetPartsStocksWithLock
                 (ObjectContext.PartsStocks.Where(r => partIds.Contains(r.PartId) && targetWarehouseAreaIds.Contains(r.WarehouseAreaId) && r.WarehouseId == warehouseId)).ToList();
                var type = ObjectContext.Companies.SingleOrDefault(r => r.Id == userInfo.EnterpriseId).Type;
                foreach(var detail in details) {
                    //校验上架任务是否存在
                    var PartsShelvesTaskOrig = partsShelvesTaskOrigs.FirstOrDefault(r => r.Id == detail.Id);
                    if(PartsShelvesTaskOrig == null) {
                        throw new ValidationException(ErrorStrings.PartsShelvesTask_Validation2);
                    }

                    var nowShelvesAmount = detail.NowShelvesAmount;
                    var plannedAmount = PartsShelvesTaskOrig.PlannedAmount;
                    var shelvesAmount = PartsShelvesTaskOrig.ShelvesAmount == null ? 0 : PartsShelvesTaskOrig.ShelvesAmount;

                    if(nowShelvesAmount + shelvesAmount > plannedAmount) {
                        throw new ValidationException(string.Format(ErrorStrings.PartsShelvesTask_Validation3, detail.SparePartCode));
                    }
                    if(detail.WarehouseAreaId == null || "".Equals(detail.WarehouseAreaId)) {
                        throw new ValidationException(string.Format(ErrorStrings.PartsShelvesTask_Validation4, detail.SparePartCode));
                    }
                    //修改上架任务单
                    var nowTime = DateTime.Now;

                    if(PartsShelvesTaskOrig.BatchNumber == null) {
                        PartsShelvesTaskOrig.BatchNumber = detail.BatchNumber;
                    }
                    PartsShelvesTaskOrig.Remark = detail.Remark;
                    PartsShelvesTaskOrig.ModifierId = userInfo.Id;
                    PartsShelvesTaskOrig.ModifierName = userInfo.Name;
                    PartsShelvesTaskOrig.ModifyTime = nowTime;
                    PartsShelvesTaskOrig.ShelvesAmount = shelvesAmount + nowShelvesAmount;
                    if(PartsShelvesTaskOrig.PlannedAmount == PartsShelvesTaskOrig.ShelvesAmount) {
                        PartsShelvesTaskOrig.Status = (int)DcsPartsShelvesTaskStatus.上架完成;
                        PartsShelvesTaskOrig.ShelvesFinishTime = nowTime;
                    } else {
                        PartsShelvesTaskOrig.Status = (int)DcsPartsShelvesTaskStatus.部分上架;
                    }

                    //添加上架实绩
                    var sparepart = spareparts.FirstOrDefault(r => r.Id == PartsShelvesTaskOrig.SparePartId);
                    var performance = new PartsInboundPerformance();
                    performance.Code = PartsShelvesTaskOrig.Code;
                    performance.InType = (int)DcsPartsInboundPer_InType.上架;
                    performance.BarCode = PartsShelvesTaskOrig.BatchNumber;
                    performance.WarehouseAreaId = detail.WarehouseAreaId;
                    performance.Qty = nowShelvesAmount;
                    performance.OperaterId = userInfo.Id;
                    performance.OperaterName = userInfo.Name;
                    performance.OperaterEndTime = nowTime;
                    performance.Equipment = (int)DcsPartsInboundPer_EquipmentType.PC;
                    performance.SparePartId = PartsShelvesTaskOrig.SparePartId;
                    if(sparepart != null) {
                        performance.SpareCode = sparepart.Code;
                        performance.SpareName = sparepart.Name;
                    }
                    InsertToDatabase(performance);

                   

                    //修改库存数量
                    var partsInboundCheckBillDetail = partsInboundCheckBillDetails.Where(r => r.SparePartId == detail.SparePartId).FirstOrDefault();
                    if(partsInboundCheckBillDetail == null)
                        throw new ValidationException(string.Format(ErrorStrings.PartsShelvesTask_Validation5, detail.SparePartCode));
                    var partsStock = partsStocks.FirstOrDefault(r => r.PartId == detail.SparePartId && r.WarehouseAreaId == partsInboundCheckBillDetail.WarehouseAreaId);
                    if(partsStock != null) {
                        if(partsStock.Quantity - nowShelvesAmount < 0) {
                            throw new ValidationException(string.Format(ErrorStrings.PartsShelvesTask_Validation6, detail.SparePartCode));
                        }
                        partsStock.Quantity -= nowShelvesAmount;
                        partsStock.ModifierId = userInfo.Id;
                        partsStock.ModifierName = userInfo.Name;
                        partsStock.ModifyTime = nowTime;
                        UpdateToDatabase(partsStock);
                    } else
                        throw new ValidationException(string.Format(ErrorStrings.PartsShelvesTask_Validation7, detail.SparePartCode));
                    //增加上架之后的库位库存数量
                    var targetPartsStock = targetPartsStocks.FirstOrDefault(r => r.PartId == detail.SparePartId && r.WarehouseAreaId == detail.WarehouseAreaId);
                    if(targetPartsStock != null) {
                        targetPartsStock.Quantity += nowShelvesAmount;
                        targetPartsStock.ModifierId = userInfo.Id;
                        targetPartsStock.ModifierName = userInfo.Name;
                        targetPartsStock.ModifyTime = nowTime;
                        UpdateToDatabase(targetPartsStock);
                        //添加配件库存批次明细
                        if(PartsShelvesTaskOrig.BatchNumber != null) {
                            var partsStockBatchDetail = new PartsStockBatchDetail();
                            partsStockBatchDetail.PartsStockId = targetPartsStock.Id;
                            partsStockBatchDetail.WarehouseId = PartsShelvesTaskOrig.WarehouseId.Value;
                            partsStockBatchDetail.StorageCompanyId = userInfo.EnterpriseId;
                            partsStockBatchDetail.StorageCompanyType = type;
                            partsStockBatchDetail.PartId = detail.SparePartId;
                            partsStockBatchDetail.BatchNumber = PartsShelvesTaskOrig.BatchNumber;
                            partsStockBatchDetail.Quantity = nowShelvesAmount;
                            partsStockBatchDetail.InboundTime = nowTime;
                            InsertToDatabase(partsStockBatchDetail);
                        }
                    } else
                        throw new ValidationException(string.Format(ErrorStrings.PartsShelvesTask_Validation8, detail.SparePartCode));

                    if(!string.IsNullOrEmpty(detail.CurrSIHCodes)) {
                        var sihCodes = detail.CurrSIHCodes.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries).ToArray();
                        if(detail.TraceProperty.HasValue && detail.TraceProperty == (int)DCSTraceProperty.精确追溯) {
                            if(sihCodes.Any(t=>t.Contains("X"))) {
                                var codes = accurateTraces.Where(t => t.PackingTaskId == detail.PackingTaskId && sihCodes.Contains(t.BoxCode) && t.PartsStockId == null && t.PartId == detail.SparePartId).ToArray();
                                if(codes.Count()!=detail.NowShelvesAmount) {
                                    throw new ValidationException("箱标" + detail.CurrSIHCodes+"对应的未上架数量不等于本次上架数量");
                                }
                                var upqty = codes.Count();
                                if (codes.Count() >= detail.NowShelvesAmount)
                                {
                                    upqty = detail.NowShelvesAmount;
                                }
                                int i = 0;
                                foreach(var item in codes) {
                                    if (i >= upqty)
                                    {
                                        continue;
                                    }
                                    item.PartsStockId = targetPartsStock.Id;
                                    item.WarehouseAreaId = targetPartsStock.WarehouseAreaId;
                                    item.WarehouseAreaCode = detail.WarehouseAreaCode;
                                    item.InQty = 1;
                                    item.ModifierId = userInfo.Id;
                                    item.ModifierName = userInfo.Name;
                                    item.ModifyTime = DateTime.Now;
                                    UpdateToDatabase(item);
                                    i++;
                                }
                            } else {
                                foreach(var item in sihCodes) {
                                    var codes = accurateTraces.Where(t => t.PackingTaskId == detail.PackingTaskId && t.SIHLabelCode == item && t.PartsStockId == null && t.PartId == detail.SparePartId).FirstOrDefault();
                                    if(codes == null) {
                                        throw new ValidationException("标签码" + detail.CurrSIHCodes + "已上架，请重新选择");
                                    } else {
                                        codes.PartsStockId = targetPartsStock.Id;
                                        codes.WarehouseAreaId = targetPartsStock.WarehouseAreaId;
                                        codes.WarehouseAreaCode = detail.WarehouseAreaCode;
                                        codes.InQty = 1;
                                        codes.ModifierId = userInfo.Id;
                                        codes.ModifierName = userInfo.Name;
                                        codes.ModifyTime = DateTime.Now;
                                        UpdateToDatabase(codes);
                                    }
                                }
                            }
                            if(string.IsNullOrEmpty(PartsShelvesTaskOrig.SIHCodes)) {
                                PartsShelvesTaskOrig.SIHCodes = detail.CurrSIHCodes;
                            } else {
                                PartsShelvesTaskOrig.SIHCodes = PartsShelvesTaskOrig.SIHCodes + ";" + detail.CurrSIHCodes;
                            }
                        } else {
                            var codes = accurateTraces.Where(t => t.PackingTaskId == detail.PackingTaskId && t.SIHLabelCode == sihCodes[0] && t.PartId == detail.SparePartId).OrderByDescending(y => y.CreateTime).ToArray();
                            if(sihCodes[0].Contains("X")) {
                                codes = accurateTraces.Where(t => t.PackingTaskId == detail.PackingTaskId && t.BoxCode == sihCodes[0] && t.PartId == detail.SparePartId).OrderByDescending(y => y.CreateTime).ToArray();
                            }
                            if(codes.Any(t=>t.PartsStockId==targetPartsStock.Id)) {
                                var sihCode = codes.First(t => t.PartsStockId == targetPartsStock.Id);
                                sihCode.InQty = sihCode.InQty+detail.NowShelvesAmount;
                                sihCode.ModifierId = userInfo.Id;
                                sihCode.ModifierName = userInfo.Name;
                                sihCode.ModifyTime = DateTime.Now;
                                UpdateToDatabase(sihCode);
                            } else {
                                if(codes.Any(t=>t.PartsStockId==null)) {
                                    var sihCode = codes.First(t => t.PartsStockId ==null);
                                    sihCode.InQty = detail.NowShelvesAmount;
                                    sihCode.PartsStockId = targetPartsStock.Id;
                                    sihCode.WarehouseAreaId = targetPartsStock.WarehouseAreaId;
                                    sihCode.WarehouseAreaCode = detail.WarehouseAreaCode;
                                    sihCode.ModifierId = userInfo.Id;
                                    sihCode.ModifierName = userInfo.Name;
                                    sihCode.ModifyTime = DateTime.Now;
                                    UpdateToDatabase(sihCode);
                                } else if(codes.All(t=>t.PartsStockId!=null)) {
                                    var sihCode = codes.First();
                                    var newSihCode = new AccurateTrace {
                                        Type = sihCode.Type,
                                        CompanyType = sihCode.CompanyType,
                                        CompanyId = sihCode.CompanyId,
                                        SourceBillId = sihCode.SourceBillId,
                                        PartId = sihCode.PartId,
                                        TraceCode = sihCode.TraceCode,
                                        SIHLabelCode = sihCode.SIHLabelCode,
                                        Status = sihCode.Status,
                                        CreatorId = userInfo.Id,
                                        CreateTime = DateTime.Now,
                                        CreatorName = userInfo.Name,
                                        TraceProperty = sihCode.TraceProperty,
                                        BoxCode = sihCode.BoxCode,
                                        InQty = detail.NowShelvesAmount,
                                        PackingTaskId = sihCode.PackingTaskId,
                                        PartsStockId = targetPartsStock.Id,
                                        WarehouseAreaId = targetPartsStock.WarehouseAreaId,
                                        WarehouseAreaCode = detail.WarehouseAreaCode
                                    };
                                    InsertToDatabase(newSihCode);
                              }
                            }
                            if(string.IsNullOrEmpty(PartsShelvesTaskOrig.SIHCodes)) {
                                PartsShelvesTaskOrig.SIHCodes = detail.CurrSIHCodes;
                            }
                            
                        }
                    }
                    UpdateToDatabase(PartsShelvesTaskOrig);
                }
                //上架数量为0 ，允许填写备注保存
                foreach(var zero in zeroDetails) {
                    var PartsShelvesTaskOrig = partsShelvesTaskOrigs.FirstOrDefault(r => r.Id == zero.Id);
                    if(!string.IsNullOrEmpty(zero.Remark) && PartsShelvesTaskOrig.Status == (int)DcsPartsShelvesTaskStatus.新增) {
                        PartsShelvesTaskOrig.Remark = zero.Remark;
                        PartsShelvesTaskOrig.ModifierId = userInfo.Id;
                        PartsShelvesTaskOrig.ModifierName = userInfo.Name;
                        PartsShelvesTaskOrig.ModifyTime = DateTime.Now;
                        UpdateToDatabase(PartsShelvesTaskOrig);
                    }
                }
                ObjectContext.SaveChanges();

                //修改入库计划单状态
                foreach(var partsInboundPlan in partsInboundPlans) {
                    new PartsInboundPlanAch(this.DomainService).更新入库计划状态(partsInboundPlan);
                }
            } else {
                //上架数量为0 ，允许填写备注保存
                foreach(var detail in virtualPartsShelvesTaskDetails) {
                    var PartsShelvesTaskOrig = partsShelvesTaskOrigs.FirstOrDefault(r => r.Id == detail.Id);
                    if(!string.IsNullOrEmpty(detail.Remark) && (PartsShelvesTaskOrig.Status == (int)DcsPartsShelvesTaskStatus.新增 || PartsShelvesTaskOrig.Status == (int)DcsPartsShelvesTaskStatus.部分上架)) {
                        PartsShelvesTaskOrig.Remark = detail.Remark;
                        PartsShelvesTaskOrig.ModifierId = userInfo.Id;
                        PartsShelvesTaskOrig.ModifierName = userInfo.Name;
                        PartsShelvesTaskOrig.ModifyTime = DateTime.Now;
                        UpdateToDatabase(PartsShelvesTaskOrig);
                    }
                }
                ObjectContext.SaveChanges();
            }
        }

        internal void insertPartsShelvesTaskDetail(VirtualPartsShelvesTaskDetail virtualPartsShelvesTaskDetail) {
        }

        public void updatePartsShelvesTaskDetail(VirtualPartsShelvesTaskDetail virtualPartsShelvesTaskDetail) {
        }

    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:33
        //原分布类的函数全部转移到Ach                                                               
        public IEnumerable<VirtualPartsShelvesTask> 查询配件上架任务(string code, string sourceBillCode, string partsInboundPlanCode, string partsInboundCheckBillCode, int? inboundType, string status,
            int? warehouseId, string sparePartCode, string sparePartName, bool? noWarehouseArea, string warehouseAreaCode, string warehouseArea, bool? hasDifference, DateTime? bCreateTime, DateTime? eCreateTime, DateTime? bShelvesFinishTime, DateTime? eShelvesFinishTime) {
            return new PartsShelvesTaskAch(this).查询配件上架任务(code, sourceBillCode, partsInboundPlanCode, partsInboundCheckBillCode, inboundType, status,
             warehouseId, sparePartCode, sparePartName, noWarehouseArea, warehouseAreaCode, warehouseArea, hasDifference, bCreateTime, eCreateTime, bShelvesFinishTime, eShelvesFinishTime);
        }

        public void insertPartsShelvesTaskDetail(VirtualPartsShelvesTaskDetail virtualPartsShelvesTaskDetail) {
            new PartsShelvesTaskAch(this).insertPartsShelvesTaskDetail(virtualPartsShelvesTaskDetail);
        }

        public void updatePartsShelvesTaskDetail(VirtualPartsShelvesTaskDetail virtualPartsShelvesTaskDetail) {
            new PartsShelvesTaskAch(this).updatePartsShelvesTaskDetail(virtualPartsShelvesTaskDetail);
        }

        public IEnumerable<VirtualPartsShelvesTaskDetail> GetVirtualPartsShelvesTaskDetailById(int[] ids) {
            return new PartsShelvesTaskAch(this).GetVirtualPartsShelvesTaskDetailById(ids);
        }

        [Invoke]
        public void DoPartsShelves(VirtualPartsShelvesTaskDetail[] virtualPartsShelvesTaskDetails) {
            new PartsShelvesTaskAch(this).DoPartsShelves(virtualPartsShelvesTaskDetails);
        }
        public IEnumerable<PartsInboundPerformanceQuery> 查询上架任务单上架详情() {
            return new PartsShelvesTaskAch(this).查询上架任务单上架详情();
        }
    }
}
