﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsShiftOrderAch : DcsSerivceAchieveBase {
        public PartsShiftOrderAch(DcsDomainService domainService)
            : base(domainService) {
        }

        public PartsShiftOrder GetPartsShiftOrderWithDetailsById(int id) {
            var result = ObjectContext.PartsShiftOrders.Include("PartsShiftOrderDetails").First(r => r.Id == id);
            var detailSp = (from a in ObjectContext.PartsShiftOrderDetails.Where(t=>t.PartsShiftOrderId==id)
                           join b in ObjectContext.SpareParts on a.SparePartId equals b.Id
                           select b).ToArray();
            var details = result.PartsShiftOrderDetails.ToArray();
            foreach(var item in details) {
                item.TraceProperty = detailSp.Where(t => t.Id == item.SparePartId).First().TraceProperty;
            }
            return result;
        }

        public void 作废配件移库单(PartsShiftOrder partsShiftOrder) {
            var userInfo = Utils.GetCurrentUserInfo();
            var dbpartsShiftOrder = ObjectContext.PartsShiftOrders.Where(r => r.Status == (int)DcsPartsShiftOrderStatus.新建 && r.Id == partsShiftOrder.Id).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbpartsShiftOrder == null)
                throw new ValidationException(ErrorStrings.PartsShiftOrder_Validation7);
            partsShiftOrder.Status = (int)DcsPartsShiftOrderStatus.作废;

            //var partsShiftOrderDetails = partsShiftOrder.PartsShiftOrderDetails.ToArray();
            //var originalWarehouseAreaIds = partsShiftOrderDetails.Select(r => r.OriginalWarehouseAreaId).ToArray();
            //var sparePartIds = partsShiftOrderDetails.Select(r => r.SparePartId);
            ////减少库存锁定量
            //var partsStocks = new PartsStockAch(this.DomainService).GetPartsStocksWithLock(ObjectContext.PartsStocks.Where(r => r.WarehouseId == partsShiftOrder.WarehouseId && sparePartIds.Contains(r.PartId) && originalWarehouseAreaIds.Contains(r.WarehouseAreaId))).ToList();
            //foreach (var stock in partsStocks) {
            //    var partsShiftOrderDetail = partsShiftOrderDetails.SingleOrDefault(r => r.SparePartId == stock.PartId && r.OriginalWarehouseAreaId == stock.WarehouseAreaId);
            //    if(partsShiftOrderDetail == null)
            //        throw new ValidationException(string.Format(ErrorStrings.PartsShiftOrder_Validation8,partsShiftOrderDetail.SparePartCode));
            //    stock.LockedQty = (stock.LockedQty ?? 0) - partsShiftOrderDetail.Quantity;
            //    if(stock.LockedQty < 0) 
            //        throw new ValidationException(string.Format(ErrorStrings.PartsShiftOrder_Validation8,partsShiftOrderDetail.SparePartCode));
            //    stock.ModifyTime = DateTime.Now;
            //    stock.ModifierId = userInfo.Id;
            //    stock.ModifierName = userInfo.Name;
            //    UpdateToDatabase(stock);
            //}
            UpdatePartsShiftOrderValidate(partsShiftOrder);
        }

        public void 驳回配件移库单(PartsShiftOrder partsShiftOrder) { 
            var dbpartsShiftOrder = ObjectContext.PartsShiftOrders.Where(r => (r.Status == (int)DcsPartsShiftOrderStatus.提交 || r.Status == (int)DcsPartsShiftOrderStatus.初审通过) && r.Id == partsShiftOrder.Id).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbpartsShiftOrder == null)
                throw new ValidationException(ErrorStrings.PartsShiftOrder_Validation9);
            var userinfo = Utils.GetCurrentUserInfo();
            if (dbpartsShiftOrder.Status == (int)DcsPartsShiftOrderStatus.新建) { 
                partsShiftOrder.InitialApproverId = userinfo.Id;
                partsShiftOrder.InitialApproverName = userinfo.Name;
                partsShiftOrder.InitialApproveTime = DateTime.Now;
            }else if (dbpartsShiftOrder.Status == (int)DcsPartsShiftOrderStatus.初审通过){
                partsShiftOrder.ApproverId = userinfo.Id;
                partsShiftOrder.ApproverName = userinfo.Name;
                partsShiftOrder.ApproveTime = DateTime.Now;
                //驳回到新建时，解锁库存保管区之间移库时
                //var partsShiftOrderDetails = partsShiftOrder.PartsShiftOrderDetails.ToArray();
                //if(partsShiftOrderDetails.All(t => t.OriginalWarehouseAreaCategory == (int)DcsAreaType.保管区 && t.DestWarehouseAreaCategory == (int)DcsAreaType.保管区)) {
                //    var originalWarehouseAreaIds = partsShiftOrderDetails.Select(r => r.OriginalWarehouseAreaId).ToArray();
                //    var sparePartIds = partsShiftOrderDetails.Select(r => r.SparePartId);
                //    //减少库存锁定量
                //    var partsStocks = new PartsStockAch(this.DomainService).GetPartsStocksWithLock(ObjectContext.PartsStocks.Where(r => r.WarehouseId == partsShiftOrder.WarehouseId && sparePartIds.Contains(r.PartId) && originalWarehouseAreaIds.Contains(r.WarehouseAreaId))).ToList();
                //    foreach(var stock in partsStocks) {
                //        var partsShiftOrderDetail = partsShiftOrderDetails.SingleOrDefault(r => r.SparePartId == stock.PartId && r.OriginalWarehouseAreaId == stock.WarehouseAreaId);
                //        if(partsShiftOrderDetail == null)
                //            throw new ValidationException(string.Format(ErrorStrings.PartsShiftOrder_Validation8, partsShiftOrderDetail.SparePartCode));
                //        stock.LockedQty = (stock.LockedQty ?? 0) - partsShiftOrderDetail.Quantity;
                //        if(stock.LockedQty < 0)
                //            throw new ValidationException(string.Format(ErrorStrings.PartsShiftOrder_Validation8, partsShiftOrderDetail.SparePartCode));
                //        stock.ModifyTime = DateTime.Now;
                //        stock.ModifierId = userinfo.Id;
                //        stock.ModifierName = userinfo.Name;
                //        UpdateToDatabase(stock);
                //        partsShiftOrder.ShiftStatus = null;
                //    }
                //}
            }
            partsShiftOrder.Status = (int)DcsPartsShiftOrderStatus.新建;
            UpdatePartsShiftOrderValidate(partsShiftOrder);
        }
        public void 提交配件移库单(PartsShiftOrder partsShiftOrder) {
            var userInfo = Utils.GetCurrentUserInfo();
            var dbpartsShiftOrder = ObjectContext.PartsShiftOrders.Where(r => r.Status == (int)DcsPartsShiftOrderStatus.新建 && r.Id == partsShiftOrder.Id).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbpartsShiftOrder == null)
                throw new ValidationException(ErrorStrings.PartsShiftOrder_Validation20);
            partsShiftOrder.Status = (int)DcsPartsShiftOrderStatus.提交;
            partsShiftOrder.SubmitterId = userInfo.Id;
            partsShiftOrder.SubmitterName = userInfo.Name;
            partsShiftOrder.SubmitTime = DateTime.Now;
            UpdatePartsShiftOrderValidate(partsShiftOrder);
        }
        public void 终止配件移库单(PartsShiftOrder partsShiftOrder) {
            var userInfo = Utils.GetCurrentUserInfo();
            var dbpartsShiftOrder = ObjectContext.PartsShiftOrders.Include("PartsShiftOrderDetails").Where(r => r.Status == (int)DcsPartsShiftOrderStatus.生效 && r.ShiftStatus != (int)DCSPartsShiftOrderShiftStatus.移库完成 && r.Id == partsShiftOrder.Id).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbpartsShiftOrder == null)
                throw new ValidationException("当前状态不允许终止");
            partsShiftOrder.Status = (int)DcsPartsShiftOrderStatus.终止;
            partsShiftOrder.CloserId = userInfo.Id;
            partsShiftOrder.CloserName = userInfo.Name;
            partsShiftOrder.ModifyTime = DateTime.Now;
            UpdatePartsShiftOrderValidate(partsShiftOrder);
            if(dbpartsShiftOrder.ShiftStatus == (int)DCSPartsShiftOrderShiftStatus.部分移库 || dbpartsShiftOrder.ShiftStatus == (int)DCSPartsShiftOrderShiftStatus.待移库) {               
                var partsShiftOrderDetails = partsShiftOrder.PartsShiftOrderDetails.ToArray();
                var originalWarehouseAreaIds = partsShiftOrderDetails.Select(r => r.OriginalWarehouseAreaId).ToArray();
                var sparePartIds = partsShiftOrderDetails.Where(t=>t.DownShelfQty>0 && t.DownShelfQty!=t.UpShelfQty).Select(r => r.SparePartId);
                if(sparePartIds.Count() == 0) {
                    return;
                }
                var partsShiftSIHDetails = ObjectContext.PartsShiftSIHDetails.Where(t => t.PartsShiftOrderId == partsShiftOrder.Id).ToArray();
                var accutraces = ObjectContext.AccurateTraces.Where(t => t.CompanyId == userInfo.EnterpriseId && t.Type == (int)DCSAccurateTraceType.入库 && t.Status == (int)DCSAccurateTraceStatus.有效).ToArray();
                //增加库存
                var partsStocks = new PartsStockAch(this.DomainService).GetPartsStocksWithLock(ObjectContext.PartsStocks.Where(r => r.WarehouseId == partsShiftOrder.WarehouseId && sparePartIds.Contains(r.PartId) && originalWarehouseAreaIds.Contains(r.WarehouseAreaId))).ToList();

                foreach(var stock in partsStocks) {
                    var partsShiftOrderDetail = partsShiftOrderDetails.SingleOrDefault(r => r.SparePartId == stock.PartId && r.OriginalWarehouseAreaId == stock.WarehouseAreaId);
                    if(partsShiftOrderDetail == null)
                        throw new ValidationException(string.Format(ErrorStrings.PartsShiftOrder_Validation8, partsShiftOrderDetail.SparePartCode));
                    stock.LockedQty = stock.LockedQty - ((partsShiftOrderDetail.DownShelfQty ?? 0) - (partsShiftOrderDetail.UpShelfQty ?? 0));
                    if(stock.LockedQty < 0) {
                        throw new ValidationException(string.Format(ErrorStrings.PartsShiftOrder_Validation2, partsShiftOrderDetail.SparePartCode, partsShiftOrderDetail.OriginalWarehouseAreaCode));
                    }
                    stock.ModifyTime = DateTime.Now;
                    stock.ModifierId = userInfo.Id;
                    stock.ModifierName = userInfo.Name;
                    UpdateToDatabase(stock);
                    if(((partsShiftOrderDetail.DownShelfQty ?? 0) - (partsShiftOrderDetail.UpShelfQty ?? 0)) > 0) {
                        var downs = partsShiftSIHDetails.Where(t => t.PartsShiftOrderDetailId == partsShiftOrderDetail.Id && t.Type == (int)DCSPartsShiftSIHDetailType.下架).ToArray();
                        var ups = partsShiftSIHDetails.Where(t => t.PartsShiftOrderDetailId == partsShiftOrderDetail.Id && t.Type == (int)DCSPartsShiftSIHDetailType.上架).ToArray();
                        foreach(var down in downs) {
                            var upSih = ups.Where(t => t.SIHCode == down.SIHCode).FirstOrDefault();
                            var accu = accutraces.Where(t => t.WarehouseAreaId == partsShiftOrderDetail.OriginalWarehouseAreaId && t.PartId == partsShiftOrderDetail.SparePartId).FirstOrDefault();
                            if(accu == null) {
                                throw new ValidationException(partsShiftOrderDetail.SparePartCode + "未找到有效的标签码");
                            }
                            var qty = 0;
                            if(upSih == null) {
                                qty = down.Quantity.Value;
                            } else {
                                qty = (down.Quantity.Value - upSih.Quantity.Value);
                            }
                            accu.OutQty = accu.OutQty - qty;
                            accu.ModifyTime = DateTime.Now;
                            accu.ModifierId = userInfo.Id;
                            accu.ModifierName = userInfo.Name;
                            UpdateToDatabase(accu);
                        }
                    }
                   
                }
            }
           
        }
        public void 初审配件移库单(PartsShiftOrder partsShiftOrder) {
            var dbpartsShiftOrder = ObjectContext.PartsShiftOrders.Where(r => r.Status == (int)DcsPartsShiftOrderStatus.提交 && r.Id == partsShiftOrder.Id).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbpartsShiftOrder == null)
                throw new ValidationException(ErrorStrings.PartsShiftOrder_Validation10);
            //如果移库类型是正常移库，初审后直接生效
            if (partsShiftOrder.Type == (int)DcsPartsShiftOrderType.正常移库)
            {
                partsShiftOrder.Status = (int)DcsPartsShiftOrderStatus.生效;
                partsShiftOrder.ShiftStatus = (int)DCSPartsShiftOrderShiftStatus.待移库;
                //finalApprovePassedDOBusiness(partsShiftOrder);
            }
            else if(partsShiftOrder.Type == (int)DcsPartsShiftOrderType.问题区移库){ 
                partsShiftOrder.Status = (int)DcsPartsShiftOrderStatus.初审通过;
            }
            var userinfo = Utils.GetCurrentUserInfo();
            var partsShiftOrderDetails = partsShiftOrder.PartsShiftOrderDetails.ToArray();
            if(partsShiftOrderDetails.All(t => t.OriginalWarehouseAreaCategory == (int)DcsAreaType.保管区 && t.DestWarehouseAreaCategory == (int)DcsAreaType.保管区)) {
                var originalWarehouseAreaIds = partsShiftOrderDetails.Select(r => r.OriginalWarehouseAreaId).ToArray();
                var sparePartIds = partsShiftOrderDetails.Select(r => r.SparePartId);
                var partsStocks = new PartsStockAch(this.DomainService).GetPartsStocksWithLock(ObjectContext.PartsStocks.Where(r => r.WarehouseId == partsShiftOrder.WarehouseId && sparePartIds.Contains(r.PartId) && originalWarehouseAreaIds.Contains(r.WarehouseAreaId))).ToList();
                //校验源库位库存，增加锁定量
                foreach(var detail in partsShiftOrderDetails) {
                    var stock = partsStocks.FirstOrDefault(r => r.PartId == detail.SparePartId && r.WarehouseAreaId == detail.OriginalWarehouseAreaId);
                    if(stock == null)
                        throw new ValidationException(string.Format(ErrorStrings.PartsShiftOrder_Validation14, detail.OriginalWarehouseAreaCode));
                    if(stock.Quantity - (stock.LockedQty ?? 0) < detail.Quantity) {
                        throw new ValidationException(string.Format(ErrorStrings.PartsShiftOrder_Validation15, detail.SparePartCode));
                    }
                   // stock.LockedQty = stock.LockedQty == null ? detail.Quantity : stock.LockedQty + detail.Quantity;
                    stock.ModifierId = userinfo.Id;
                    stock.ModifierName = userinfo.Name;
                    stock.ModifyTime = DateTime.Now;
                    UpdateToDatabase(stock);
                    partsShiftOrder.ShiftStatus = (int)DCSPartsShiftOrderShiftStatus.待移库;
                }
            }                                   
            partsShiftOrder.InitialApproverId = userinfo.Id;
            partsShiftOrder.InitialApproverName = userinfo.Name;
            partsShiftOrder.InitialApproveTime = DateTime.Now;
           
            UpdatePartsShiftOrderValidate(partsShiftOrder);
        }

        public void 终审配件移库单(PartsShiftOrder partsShiftOrder) { 
            var dbpartsShiftOrder = ObjectContext.PartsShiftOrders.Where(r => r.Status == (int)DcsPartsShiftOrderStatus.初审通过 && r.Id == partsShiftOrder.Id).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbpartsShiftOrder == null)
                throw new ValidationException(ErrorStrings.PartsShiftOrder_Validation11);
            partsShiftOrder.Status = (int)DcsPartsShiftOrderStatus.生效;
            var userinfo = Utils.GetCurrentUserInfo();
            partsShiftOrder.ApproverId = userinfo.Id;
            partsShiftOrder.ApproverName = userinfo.Name;
            partsShiftOrder.ApproveTime = DateTime.Now;
            var partsShiftOrderDetails = partsShiftOrder.PartsShiftOrderDetails.ToArray();
            if(!partsShiftOrderDetails.All(t => t.OriginalWarehouseAreaCategory == (int)DcsAreaType.保管区 && t.DestWarehouseAreaCategory == (int)DcsAreaType.保管区)) {
                var originalWarehouseAreaIds = partsShiftOrderDetails.Select(r => r.OriginalWarehouseAreaId).ToArray();
                var sparePartIds = partsShiftOrderDetails.Select(r => r.SparePartId);
                var partsStocks = new PartsStockAch(this.DomainService).GetPartsStocksWithLock(ObjectContext.PartsStocks.Where(r => r.WarehouseId == partsShiftOrder.WarehouseId && sparePartIds.Contains(r.PartId) && originalWarehouseAreaIds.Contains(r.WarehouseAreaId))).ToList();
                //校验源库位库存，增加锁定量
                foreach(var detail in partsShiftOrderDetails) {
                    var stock = partsStocks.FirstOrDefault(r => r.PartId == detail.SparePartId && r.WarehouseAreaId == detail.OriginalWarehouseAreaId);
                    if(stock == null)
                        throw new ValidationException(string.Format(ErrorStrings.PartsShiftOrder_Validation14, detail.OriginalWarehouseAreaCode));
                    if(stock.Quantity - (stock.LockedQty ?? 0) < detail.Quantity) {
                        throw new ValidationException(string.Format(ErrorStrings.PartsShiftOrder_Validation15, detail.SparePartCode));
                    }
                   // stock.LockedQty = stock.LockedQty == null ? detail.Quantity : stock.LockedQty + detail.Quantity;
                    stock.ModifierId = userinfo.Id;
                    stock.ModifierName = userinfo.Name;
                    stock.ModifyTime = DateTime.Now;
                    UpdateToDatabase(stock);
                    partsShiftOrder.ShiftStatus = (int)DCSPartsShiftOrderShiftStatus.待移库;
                }
            } 
        //    finalApprovePassedDOBusiness(partsShiftOrder);
            UpdatePartsShiftOrderValidate(partsShiftOrder);
        }

        public void 生成配件移库单并且执行移库(PartsShiftOrder partsShiftOrder) {
            finalApprovePassedDOBusiness(partsShiftOrder);
        }

        private void finalApprovePassedDOBusiness(PartsShiftOrder partsShiftOrder) { 
            var userInfo = Utils.GetCurrentUserInfo();
            var partsShiftOrderDetails = partsShiftOrder.PartsShiftOrderDetails.ToArray();
            var originalWarehouseAreaIds = partsShiftOrderDetails.Select(r => r.OriginalWarehouseAreaId).ToArray();
            var warehouseAreaIds = partsShiftOrderDetails.Select(r => r.DestWarehouseAreaId).Union(originalWarehouseAreaIds);
            var warehouses = ObjectContext.WarehouseAreas.Where(r => warehouseAreaIds.Any(v => v == r.Id)).ToArray();
            if(warehouses.Any(r => r.Status == (int)DcsBaseDataStatus.作废)) {
                throw new ValidationException(ErrorStrings.PartsShiftOrderDetails_Validation1);
            }
            var sparePartIds = partsShiftOrderDetails.Select(r => r.SparePartId);
            var company = ObjectContext.Companies.FirstOrDefault(r => r.Id == partsShiftOrder.StorageCompanyId);
            if(company == null)
                throw new ValidationException(ErrorStrings.PartsShiftOrder_Validation1);
            var partsStocks = new PartsStockAch(this.DomainService).GetPartsStocksWithLock(ObjectContext.PartsStocks.Where(r => r.WarehouseId == partsShiftOrder.WarehouseId && sparePartIds.Contains(r.PartId) && warehouseAreaIds.Contains(r.WarehouseAreaId))).ToList();
            //汇总源库位配件
            var originalPartsShiftOrderDetails = partsShiftOrderDetails.GroupBy(v => new {
                v.SparePartId,
                v.OriginalWarehouseAreaId
            }).Select(v => new {
                v.Key.SparePartId,
                v.Key.OriginalWarehouseAreaId,
                v.First().SparePartCode,
                v.First().OriginalWarehouseAreaCode,
                Quantity = v.Sum(detail => detail.Quantity),
                detailsWithBatchNumber = v.Where(detail => !string.IsNullOrEmpty(detail.BatchNumber)).ToArray(),
            }).ToArray();
            #region 校验是否有未完成的盘点单
            var originalWarehouseArea = originalPartsShiftOrderDetails.Select(r => r.OriginalWarehouseAreaId).ToArray();
            var checkInventoryTemp = (from a in ObjectContext.PartsInventoryBills.Where(r => r.WarehouseId == partsShiftOrder.WarehouseId && r.Status == (int)DcsPartsInventoryBillStatus.审核通过)
                                      from b in a.PartsInventoryDetails
                                      where originalWarehouseArea.Any(v => v == b.WarehouseAreaId)
                                      select b).ToArray();
            var check = checkInventoryTemp.Where(r => originalPartsShiftOrderDetails.Any(v => v.OriginalWarehouseAreaId == r.WarehouseAreaId && v.SparePartId == r.SparePartId)).ToArray();
            if(check.Length > 0) {
                var errorsparePartString = string.Join(",", check.Select(r => r.SparePartCode).ToArray());
                throw new ValidationException(string.Format(ErrorStrings.PartsShiftOrder_Validation12,errorsparePartString));
            }
            #endregion
            //汇总目标库位配件
            var destPartsShiftOrderDetails = partsShiftOrderDetails.GroupBy(v => new {
                v.SparePartId,
                v.DestWarehouseAreaId
            }).Select(v => new {
                v.Key.SparePartId,
                v.Key.DestWarehouseAreaId,
                Quantity = v.Sum(detail => detail.Quantity),
                detailsWithBatchNumber = v.Where(detail => !string.IsNullOrEmpty(detail.BatchNumber)).ToArray(),
            }).ToArray();
            //获取配件库存批次明细
            var partsShiftOrderDetailsWithBatchNumber = partsShiftOrderDetails.Where(r => !string.IsNullOrEmpty(r.BatchNumber)).ToArray();
            var batchNumbers = partsShiftOrderDetailsWithBatchNumber.Select(r => r.BatchNumber.ToLower());
            var partsStockBatchDetails = ObjectContext.PartsStockBatchDetails.Where(r => batchNumbers.Contains(r.BatchNumber.ToLower())).ToArray();
            //新增配件库位变更履历（这个新增不用校验）移库才记履历
            if(partsShiftOrder.Type == (int)DcsPartsShiftOrderType.正常移库 || partsShiftOrder.Type == (int)DcsPartsShiftOrderType.问题区移库) {
                var warehouseAreaHistories = new List<WarehouseAreaHistory>();
                foreach(var item in partsShiftOrderDetails) {
                    var t = ObjectContext.WarehouseAreaCategories.FirstOrDefault(r => r.Category == item.DestWarehouseAreaCategory);
                    if(t == null) {
                        throw new ValidationException(ErrorStrings.PartsShiftOrder_Validation13);
                    }
                    var warehouseAreaHistory = new WarehouseAreaHistory();
                    warehouseAreaHistory.WarehouseId = partsShiftOrder.WarehouseId;
                    warehouseAreaHistory.StorageCompanyId = partsShiftOrder.StorageCompanyId;
                    warehouseAreaHistory.StorageCompanyType = partsShiftOrder.StorageCompanyType;
                    warehouseAreaHistory.BranchId = partsShiftOrder.BranchId;
                    warehouseAreaHistory.WarehouseAreaId = item.OriginalWarehouseAreaId;
                    warehouseAreaHistory.WarehouseAreaCategoryId = item.OriginalWarehouseAreaCategory;
                    warehouseAreaHistory.DestWarehouseAreaId = item.DestWarehouseAreaId;
                    warehouseAreaHistory.DestWarehouseAreaCategoryId = t.Id;
                    warehouseAreaHistory.PartId = item.SparePartId;
                    warehouseAreaHistory.Quantity = item.Quantity;
                    warehouseAreaHistories.Add(warehouseAreaHistory);
                }
                foreach(var item in warehouseAreaHistories) {
                    item.CreatorId = userInfo.Id;
                    item.CreatorName = userInfo.Name;
                    item.CreateTime = DateTime.Now;
                    ObjectContext.WarehouseAreaHistories.AddObject(item);
                    InsertToDatabase(item);
                }
            }
            //校验并调整源库位库存
            foreach(var detail in originalPartsShiftOrderDetails) {
                var partsStock = partsStocks.SingleOrDefault(v => v.WarehouseAreaId == detail.OriginalWarehouseAreaId && v.PartId == detail.SparePartId);
                if(partsStock == null)
                    throw new ValidationException(string.Format(ErrorStrings.PartsShiftOrder_Validation4, detail.SparePartCode, detail.OriginalWarehouseAreaCode));
                if(partsStock.Quantity < detail.Quantity)
                    throw new ValidationException(string.Format(ErrorStrings.PartsShiftOrder_Validation2, detail.SparePartCode, detail.OriginalWarehouseAreaCode));
                partsStock.Quantity -= detail.Quantity;
                if (partsShiftOrder.Type != (int)DcsPartsShiftOrderType.上架) { 
                    partsStock.LockedQty = (partsStock.LockedQty ?? 0) - detail.Quantity;
                    if(partsStock.LockedQty < 0) 
                    throw new ValidationException(string.Format(ErrorStrings.PartsShiftOrder_Validation8, detail.SparePartCode ));
                }
                partsStock.ModifyTime = DateTime.Now;
                partsStock.ModifierId = userInfo.Id;
                partsStock.ModifierName = userInfo.Name;
                UpdateToDatabase(partsStock);
                foreach(var detailWithBatchNumber in detail.detailsWithBatchNumber) {
                    var partsStockBatchDetail = partsStockBatchDetails.SingleOrDefault(v => String.Compare(v.BatchNumber, detailWithBatchNumber.BatchNumber, StringComparison.OrdinalIgnoreCase) == 0 && v.Quantity == detailWithBatchNumber.Quantity);
                    if(partsStockBatchDetail == null)
                        throw new ValidationException(string.Format(ErrorStrings.PartsShiftOrder_Validation6, detailWithBatchNumber.BatchNumber));
                    if(partsStock.Id != partsStockBatchDetail.PartsStockId)
                        throw new ValidationException(string.Format(ErrorStrings.PartsShiftOrder_Validation5, detailWithBatchNumber.BatchNumber));
                }
            }
            //调整目标库位库存
            foreach(var detail in destPartsShiftOrderDetails) {
                var partsStock = partsStocks.SingleOrDefault(v => v.WarehouseAreaId == detail.DestWarehouseAreaId && v.PartId == detail.SparePartId);
                if(partsStock == null) {
                    var tempWarehouseArea = warehouses.Single(r => r.Id == detail.DestWarehouseAreaId);
                    partsStock = new PartsStock {
                        WarehouseId = partsShiftOrder.WarehouseId,
                        WarehouseAreaCategoryId = tempWarehouseArea.AreaCategoryId,
                        StorageCompanyId = partsShiftOrder.StorageCompanyId,
                        StorageCompanyType = partsShiftOrder.StorageCompanyType,
                        BranchId = partsShiftOrder.BranchId,
                        WarehouseAreaId = detail.DestWarehouseAreaId,
                        PartId = detail.SparePartId,
                        Quantity = detail.Quantity
                    };
                    InsertToDatabase(partsStock);
                    partsStocks.Add(partsStock);                }else {
                    partsStock.Quantity += detail.Quantity;
                    UpdateToDatabase(partsStock);
                }
                foreach(var detailWithBatchNumber in detail.detailsWithBatchNumber) {
                    var partsStockBatchDetail = partsStockBatchDetails.Single(v => String.Compare(v.BatchNumber, detailWithBatchNumber.BatchNumber, StringComparison.OrdinalIgnoreCase) == 0 && v.Quantity == detailWithBatchNumber.Quantity);
                    partsStockBatchDetail.PartsStockId = partsStock.Id;
                    partsStock.PartsStockBatchDetails.Add(partsStockBatchDetail);
                }
            }
            //改成批量校验
            var checkPartIds = partsStocks.Select(r => r.PartId);
            var checkWarehouseAreaIds = partsStocks.Select(r => r.WarehouseAreaId);
            var checkPartsStocks = ObjectContext.PartsStocks.Where(r => checkPartIds.Contains(r.PartId) && checkWarehouseAreaIds.Contains(r.WarehouseAreaId)).SetMergeOption(MergeOption.NoTracking).ToArray();
            foreach(var item in partsStocks) {
                //数量要大于0;2019-08-16允许目标库区的库存小于0                
                //if(item.Quantity < 0) {
                //    throw new ValidationException(ErrorStrings.PartsStock_Validation5);
                //}
                //新增修改的唯一性校验
                switch(item.EntityState) {
                    case EntityState.Modified:
                        var checkExists = checkPartsStocks.FirstOrDefault(r => r.Id == item.Id);
                        if(checkExists == null) {
                            throw new ValidationException(ErrorStrings.PartsStock_Validation6);
                        }
                        var checkOnlyOneForUpdate = checkPartsStocks.SingleOrDefault(r => r.Id != item.Id && r.PartId == item.PartId && r.WarehouseAreaId == item.WarehouseAreaId);
                        if(checkOnlyOneForUpdate != null) {
                            throw new ValidationException(ErrorStrings.PartsStock_Validation7);
                        }
                        item.ModifierId = userInfo.Id;
                        item.ModifierName = userInfo.Name;
                        item.ModifyTime = DateTime.Now;
                        break;
                    case EntityState.Added:
                        var checkOnlyOneForInsert = checkPartsStocks.SingleOrDefault(r => r.PartId == item.PartId && r.WarehouseAreaId == item.WarehouseAreaId);
                        if(checkOnlyOneForInsert != null) {
                            throw new ValidationException(ErrorStrings.PartsStock_Validation1);
                        }
                        item.CreatorId = userInfo.Id;
                        item.CreatorName = userInfo.Name;
                        item.CreateTime = DateTime.Now;
                        break;
                }
            }            partsShiftOrder.Status = (int)DcsPartsShiftOrderStatus.生效;
            this.UpdatePartsShiftOrderValidate(partsShiftOrder);
        }

        public void 生成配件移库单(PartsShiftOrder partsShiftOrder) {
            var userInfo = Utils.GetCurrentUserInfo();
            var partsShiftOrderDetails = partsShiftOrder.PartsShiftOrderDetails.ToArray();
            var originalWarehouseAreaIds = partsShiftOrderDetails.Select(r => r.OriginalWarehouseAreaId).ToArray();
            var warehouseAreaIds = partsShiftOrderDetails.Select(r => r.DestWarehouseAreaId).ToArray().Union(originalWarehouseAreaIds).ToArray();
            var warehouses = ObjectContext.WarehouseAreas.Where(r => warehouseAreaIds.Contains(r.Id)).ToArray();
            if(warehouses.Any(r => r.Status == (int)DcsBaseDataStatus.作废)) {
                throw new ValidationException(ErrorStrings.PartsShiftOrderDetails_Validation1);
            }
            var sparePartIds = partsShiftOrderDetails.Select(r => r.SparePartId);
            var company = ObjectContext.Companies.FirstOrDefault(r => r.Id == partsShiftOrder.StorageCompanyId);
            if(company == null)
                throw new ValidationException(ErrorStrings.PartsShiftOrder_Validation1);
             //汇总源库位配件
            var originalPartsShiftOrderDetails = partsShiftOrderDetails.GroupBy(v => new {
                v.SparePartId,
                v.OriginalWarehouseAreaId
            }).Select(v => new {
                v.Key.SparePartId,
                v.Key.OriginalWarehouseAreaId,
                v.First().SparePartCode,
                v.First().OriginalWarehouseAreaCode,
                Quantity = v.Sum(detail => detail.Quantity),
                detailsWithBatchNumber = v.Where(detail => !string.IsNullOrEmpty(detail.BatchNumber)).ToArray(),
            }).ToArray();            #region 校验是否有未完成的盘点单
            var originalWarehouseArea = originalPartsShiftOrderDetails.Select(r => r.OriginalWarehouseAreaId).ToArray();
            var checkInventoryTemp = (from a in ObjectContext.PartsInventoryBills.Where(r => r.WarehouseId == partsShiftOrder.WarehouseId && r.Status == (int)DcsPartsInventoryBillStatus.审核通过)
                                      from b in a.PartsInventoryDetails
                                      where originalWarehouseArea.Contains(b.WarehouseAreaId)
                                      select b).ToArray();
            var check = checkInventoryTemp.Where(r => originalPartsShiftOrderDetails.Any(v => v.OriginalWarehouseAreaId == r.WarehouseAreaId && v.SparePartId == r.SparePartId)).ToArray();
            if(check.Length > 0) {
                var errorsparePartString = string.Join(",", check.Select(r => r.SparePartCode).ToArray());
                throw new ValidationException(string.Format(ErrorStrings.PartsShiftOrder_Validation12,errorsparePartString ));
            }
            #endregion            //增加锁定量时移到初审时            //var partsStocks = new PartsStockAch(this.DomainService).GetPartsStocksWithLock(ObjectContext.PartsStocks.Where(r => r.WarehouseId == partsShiftOrder.WarehouseId && sparePartIds.Contains(r.PartId) && originalWarehouseAreaIds.Contains(r.WarehouseAreaId))).ToList();
            ////校验源库位库存，增加锁定量
            //foreach (var detail in partsShiftOrderDetails) {
            //    var stock = partsStocks.FirstOrDefault(r => r.PartId == detail.SparePartId && r.WarehouseAreaId == detail.OriginalWarehouseAreaId);
            //    if(stock == null)
            //        throw new ValidationException(string.Format(ErrorStrings.PartsShiftOrder_Validation14,detail.OriginalWarehouseAreaCode ));
            //    if (stock.Quantity - (stock.LockedQty ?? 0) < detail.Quantity) { 
            //        throw new ValidationException(string.Format(ErrorStrings.PartsShiftOrder_Validation15,detail.SparePartCode));
            //    }
            //    stock.LockedQty = stock.LockedQty == null ? detail.Quantity : stock.LockedQty + detail.Quantity;
            //    stock.ModifierId = userInfo.Id;
            //    stock.ModifierName = userInfo.Name;
            //    stock.ModifyTime = DateTime.Now;
            //    UpdateToDatabase(stock);
            //}

            //如果库位类型是问题区
            var original = partsShiftOrderDetails.Count(r => r.OriginalWarehouseAreaCategory == (int)DcsAreaType.问题区);
            var dest = partsShiftOrderDetails.Count(r => r.DestWarehouseAreaCategory == (int)DcsAreaType.问题区);

            if (original + dest > 0) {
                partsShiftOrder.Type = (int)DcsPartsShiftOrderType.问题区移库;
            }

            partsShiftOrder.StorageCompanyCode = company.Code;
            partsShiftOrder.StorageCompanyName = company.Name;
            partsShiftOrder.Status = (int)DcsPartsShiftOrderStatus.新建;
            InsertToDatabase(partsShiftOrder);
            this.InsertPartsShiftOrderValidate(partsShiftOrder);
        }

        public void 上传移库单附件(PartsShiftOrder partsShiftOrder)
        {
            var userInfo = Utils.GetCurrentUserInfo();
            partsShiftOrder.ModifierId = userInfo.Id;
            partsShiftOrder.ModifierName = userInfo.Name;
            partsShiftOrder.ModifyTime = DateTime.Now;
            UpdateToDatabase(partsShiftOrder);
        }
        public void 上架配件移库单(PartsShiftOrder partsShiftOrders) {
            var userInfo = Utils.GetCurrentUserInfo();
            var dbpartsShiftOrder = ObjectContext.PartsShiftOrders.Include("PartsShiftOrderDetails").Where(r => (r.ShiftStatus == (int)DCSPartsShiftOrderShiftStatus.部分移库 || r.ShiftStatus == (int)DCSPartsShiftOrderShiftStatus.待移库) && r.Status == (int)DcsPartsShiftOrderStatus.生效 && r.Id == partsShiftOrders.Id).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbpartsShiftOrder == null)
                throw new ValidationException(ErrorStrings.PartsShiftOrder_Data_Validation1);
            var partsShiftOrderDetails = partsShiftOrders.PartsShiftOrderDetails.ToArray();
            var oldPartsShiftOrderDetail = dbpartsShiftOrder.PartsShiftOrderDetails.ToArray();
            var sparePartIds = partsShiftOrderDetails.Select(t => t.SparePartId).ToArray();
            var codes = ObjectContext.AccurateTraces.Where(t => sparePartIds.Contains(t.PartId.Value) && t.CompanyId == userInfo.EnterpriseId && t.Status == (int)DCSAccurateTraceStatus.有效 ).ToArray();
            var originalWarehouseAreaIds = partsShiftOrderDetails.Select(r => r.OriginalWarehouseAreaId).ToArray();
            var warehouseAreaIds = partsShiftOrderDetails.Select(r => r.DestWarehouseAreaId).Union(originalWarehouseAreaIds);
            var warehouses = ObjectContext.WarehouseAreas.Where(r => warehouseAreaIds.Any(v => v == r.Id)).ToArray();
            var oldPartsShiftSIHDetails = ObjectContext.PartsShiftSIHDetails.Where(t => t.PartsShiftOrderId == partsShiftOrders.Id && t.Type == (int)DCSPartsShiftSIHDetailType.上架).ToArray();
            var partsStocks = new PartsStockAch(this.DomainService).GetPartsStocksWithLock(ObjectContext.PartsStocks.Where(r => r.WarehouseId == dbpartsShiftOrder.WarehouseId && sparePartIds.Contains(r.PartId) && warehouseAreaIds.Contains(r.WarehouseAreaId))).ToList();
            //源库位库存信息
            //汇总目标库位配件
            var destPartsShiftOrderDetails = partsShiftOrderDetails.GroupBy(v => new {
                v.SparePartId,
                v.DestWarehouseAreaId
            }).Select(v => new {
                v.Key.SparePartId,
                v.Key.DestWarehouseAreaId,
                Quantity = v.Sum(detail => detail.CourrentUpShelfQty),
                detailsWithBatchNumber = v.Where(detail => !string.IsNullOrEmpty(detail.BatchNumber)).ToArray(),
            }).ToArray();
            //汇总源库位配件
            var originalPartsShiftOrderDetails = partsShiftOrderDetails.GroupBy(v => new
            {
                v.SparePartId,
                v.OriginalWarehouseAreaId
            }).Select(v => new
            {
                v.Key.SparePartId,
                v.Key.OriginalWarehouseAreaId,
                v.First().SparePartCode,
                v.First().OriginalWarehouseAreaCode,
                Quantity = v.Sum(detail => detail.CourrentUpShelfQty),
                detailsWithBatchNumber = v.Where(detail => !string.IsNullOrEmpty(detail.BatchNumber)).ToArray(),
            }).ToArray();
            //获取配件库存批次明细
            var partsShiftOrderDetailsWithBatchNumber = partsShiftOrderDetails.Where(r => !string.IsNullOrEmpty(r.BatchNumber)).ToArray();
            var batchNumbers = partsShiftOrderDetailsWithBatchNumber.Select(r => r.BatchNumber.ToLower());
            var partsStockBatchDetails = ObjectContext.PartsStockBatchDetails.Where(r => batchNumbers.Contains(r.BatchNumber.ToLower())).ToArray();
            var oldAccutrace = ObjectContext.AccurateTraces.Where(t => t.CompanyId == userInfo.EnterpriseId && t.Status == (int)DCSAccurateTraceStatus.有效 && t.Type == (int)DCSAccurateTraceType.入库).ToArray();
            var partsShiftSIHDetailDown = ObjectContext.PartsShiftSIHDetails.Where(t => t.PartsShiftOrderId == dbpartsShiftOrder.Id).ToArray(); ;

            foreach(var item in partsShiftOrderDetails) {
                if(item.CourrentUpShelfQty.HasValue && item.CourrentUpShelfQty.Value <= 0) {
                    continue;
                }
                if (string.IsNullOrEmpty(item.CourrentSIHCode))
                {
                    if(item.CourrentUpShelfQty.HasValue && item.CourrentUpShelfQty.Value>0) {
                        var oldDetail1 = oldPartsShiftOrderDetail.Where(t => t.Id == item.Id).First();
                        if(oldDetail1.UpShelfQty == null) {
                            oldDetail1.UpShelfQty = 0;
                        }
                        item.UpShelfQty = oldDetail1.UpShelfQty + item.CourrentUpShelfQty;
                        UpdateToDatabase(item);
                    }
                    continue;
                }
                var courrentCodes = item.CourrentSIHCode.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries).ToArray();
                List<AccurateTrace> traceCodes = new List<AccurateTrace>();
                var downs = partsShiftSIHDetailDown.Where(t => t.PartsShiftOrderDetailId == item.Id).ToArray();
                foreach (var allcode in courrentCodes)
                {
                    var insetCode = codes.Where(t => allcode == t.SIHLabelCode && t.PartId == item.SparePartId && downs.Any(u => u.SIHCode == t.SIHLabelCode) && (t.WarehouseAreaId == item.OriginalWarehouseAreaId || t.WarehouseAreaId == null)).ToArray();
                    if(allcode.EndsWith("X")) {
                        insetCode = codes.Where(t => allcode == t.BoxCode && t.PartId == item.SparePartId && downs.Any(u => u.SIHCode == t.SIHLabelCode) && (t.WarehouseAreaId == item.OriginalWarehouseAreaId || t.WarehouseAreaId == null)).ToArray();
                    }
                    if(insetCode.Count() == 0) {
                        throw new ValidationException("未找到有效的SIH标签码" + allcode);
                    }
                    foreach(var insertCode in insetCode) {
                        traceCodes.Add(insertCode);
                    }
                } 
                if (traceCodes.Count() == 0 || (traceCodes.Count() < item.CourrentUpShelfQty && item.TraceProperty == (int)DCSTraceProperty.精确追溯) )
                {
                    throw new ValidationException(ErrorStrings.PartsShiftOrder_Data_Validation2 + item.CourrentSIHCode);
                }
                var oldDetail = oldPartsShiftOrderDetail.Where(t => t.Id == item.Id).First();
                if(oldDetail.UpShelfQty==null){
                    oldDetail.UpShelfQty=0;
                }
                //if((item.TraceProperty == (int)DCSTraceProperty.批次追溯 || item.TraceProperty == null) && (!string.IsNullOrEmpty(oldDetail.SIHCode) && oldDetail.SIHCode != courrentCodes[0])) {
                //    throw new ValidationException(item.SparePartCode + ErrorStrings.PartsShiftOrder_Data_Validation3);
                //}
                if(oldDetail.DownShelfQty < oldDetail.UpShelfQty + item.CourrentUpShelfQty) {
                    throw new ValidationException(string.Format( ErrorStrings.PartsShiftOrder_Data_Validation4,item.SparePartCode));
                }                
                item.SIHCode = oldDetail.SIHCode;
                foreach(var code in courrentCodes) {
                    if(!oldDetail.DownSIHCode.Contains(code)) {
                        throw new ValidationException(string.Format( ErrorStrings.PartsShiftOrder_Data_Validation5,code));
                    }
                    if(string.IsNullOrEmpty(oldDetail.SIHCode)) {
                        item.SIHCode = code;
                    } else if(!oldDetail.SIHCode.Contains(code))
                        item.SIHCode = item.SIHCode + ";" + code;
                    else if(oldDetail.SIHCode.Contains(code) && item.TraceProperty == (int)DCSTraceProperty.精确追溯)
                        throw new ValidationException(string.Format("{0}已上架", code));
                }
                var upQty = item.CourrentUpShelfQty;
                int i = 0;
                foreach(var sihCode in traceCodes) {
                    //判断是否为下架标签
                    if (!downs.Any(t => t.SIHCode == sihCode.SIHLabelCode))
                    {
                        throw new ValidationException(sihCode + "不是已下架标签，请重新扫描");
                    }
                    var downSih = downs.Where(t => t.SIHCode == sihCode.SIHLabelCode).First();
                    var upQtyPc = downSih.Quantity; ;
                    if(downSih.Quantity > item.CourrentUpShelfQty) {
                        upQtyPc = item.CourrentUpShelfQty;
                    } 
                    var oldT = oldAccutrace.Where(t => t.PartId == item.SparePartId && t.SIHLabelCode == sihCode.SIHLabelCode && t.WarehouseAreaId == item.DestWarehouseAreaId ).FirstOrDefault();
                    if(item.TraceProperty.HasValue && item.TraceProperty == (int)DCSTraceProperty.精确追溯) {
                        if(i>=upQty.Value){
                            continue;
                        }
                        if(oldT!=null) {
                            oldT.Status = (int)DCSAccurateTraceStatus.作废;
                            oldT.ModifierId = userInfo.Id;
                            oldT.ModifierName = userInfo.Name;
                            oldT.ModifyTime = DateTime.Now;
                            UpdateToDatabase(oldT);
                        }
                        var newAcc = new AccurateTrace {
                            Type = sihCode.Type,
                            CompanyId = sihCode.CompanyId,
                            CompanyType = sihCode.CompanyType,
                            SourceBillId = sihCode.SourceBillId,
                            PartId = sihCode.PartId,
                            TraceCode = sihCode.TraceCode,
                            SIHLabelCode = sihCode.SIHLabelCode,
                            Status = (int)DCSAccurateTraceStatus.有效,
                            CreateTime = DateTime.Now,
                            CreatorId = userInfo.Id,
                            CreatorName = userInfo.Name,
                            TraceProperty = sihCode.TraceProperty,
                            BoxCode = sihCode.BoxCode,
                            InQty = 1,
                            PackingTaskId = sihCode.PackingTaskId,
                            WarehouseAreaId = item.DestWarehouseAreaId,
                            WarehouseAreaCode = item.DeWarehouseAreaCode
                        };
                        InsertToDatabase(newAcc);
                        var partsShiftSIHDetail = new PartsShiftSIHDetail {
                            PartsShiftOrderId = partsShiftOrders.Id,
                            PartsShiftOrderDetailId = item.Id,
                            Type = (int)DCSPartsShiftSIHDetailType.上架,
                            SIHCode = sihCode.SIHLabelCode,
                            Quantity = 1,
                        };
                        InsertToDatabase(partsShiftSIHDetail);
                        i++;
                    } else {
                        if(oldT != null) {
                            oldT.InQty = oldT.InQty + upQtyPc;
                            oldT.ModifierId = userInfo.Id;
                            oldT.ModifierName = userInfo.Name;
                            oldT.ModifyTime = DateTime.Now;
                            UpdateToDatabase(oldT);
                        } else {
                            var newAcc = new AccurateTrace {
                                Type = sihCode.Type,
                                CompanyId = sihCode.CompanyId,
                                CompanyType = sihCode.CompanyType,
                                SourceBillId = sihCode.SourceBillId,
                                PartId = sihCode.PartId,
                                TraceCode = sihCode.TraceCode,
                                SIHLabelCode = sihCode.SIHLabelCode,
                                Status = (int)DCSAccurateTraceStatus.有效,
                                CreateTime = DateTime.Now,
                                CreatorId = userInfo.Id,
                                CreatorName = userInfo.Name,
                                TraceProperty = sihCode.TraceProperty,
                                BoxCode = sihCode.BoxCode,
                                InQty = upQtyPc,
                                PackingTaskId = sihCode.PackingTaskId,
                                WarehouseAreaId = item.DestWarehouseAreaId,
                                WarehouseAreaCode = item.DeWarehouseAreaCode
                            };
                            InsertToDatabase(newAcc);
                        }                       
                        var old = oldPartsShiftSIHDetails.Where(t => t.PartsShiftOrderDetailId == item.Id && t.SIHCode == sihCode.SIHLabelCode).FirstOrDefault();
                        if(old == null) {
                            var partsShiftSIHDetail = new PartsShiftSIHDetail {
                                PartsShiftOrderId = partsShiftOrders.Id,
                                PartsShiftOrderDetailId = item.Id,
                                Type = (int)DCSPartsShiftSIHDetailType.上架,
                                SIHCode = sihCode.SIHLabelCode,
                                Quantity = upQtyPc,
                            };
                            InsertToDatabase(partsShiftSIHDetail);
                        } else {
                            old.Quantity = old.Quantity + item.CourrentUpShelfQty;
                            UpdateToDatabase(old);
                        }
                    }
                }
                item.UpShelfQty = oldDetail.UpShelfQty + item.CourrentUpShelfQty;
                UpdateToDatabase(item);

            }
            this.UpdatePartsShiftOrderValidate(partsShiftOrders);
            //   校验并调整源库位库存
            foreach (var detail in originalPartsShiftOrderDetails)
            {
                if (detail.Quantity == 0)
                {
                    continue;
                }               
                var partsStock = partsStocks.SingleOrDefault(v => v.WarehouseAreaId == detail.OriginalWarehouseAreaId && v.PartId == detail.SparePartId);
                if (partsStock == null)
                    throw new ValidationException(string.Format(ErrorStrings.PartsShiftOrder_Validation4, detail.SparePartCode, detail.OriginalWarehouseAreaCode));               
                partsStock.LockedQty = partsStock.LockedQty - detail.Quantity;
                partsStock.Quantity = partsStock.Quantity - detail.Quantity.Value;
                if (partsStock.LockedQty < 0 || partsStock.Quantity < 0)
                    throw new ValidationException(string.Format(ErrorStrings.PartsShiftOrder_Validation2, detail.SparePartCode, detail.OriginalWarehouseAreaCode)); 
                partsStock.ModifyTime = DateTime.Now;
                partsStock.ModifierId = userInfo.Id;
                partsStock.ModifierName = userInfo.Name;
                UpdateToDatabase(partsStock);
            }
           
            //调整目标库位库存
            foreach(var detail in destPartsShiftOrderDetails) {
                if(detail.Quantity.Value==0) {
                    continue;
                }
               
                var partsStock = partsStocks.SingleOrDefault(v => v.WarehouseAreaId == detail.DestWarehouseAreaId && v.PartId == detail.SparePartId);
                if(partsStock == null) {
                    var tempWarehouseArea = warehouses.Single(r => r.Id == detail.DestWarehouseAreaId);
                    partsStock = new PartsStock {
                        WarehouseId = dbpartsShiftOrder.WarehouseId,
                        WarehouseAreaCategoryId = tempWarehouseArea.AreaCategoryId,
                        StorageCompanyId = dbpartsShiftOrder.StorageCompanyId,
                        StorageCompanyType = dbpartsShiftOrder.StorageCompanyType,
                        BranchId = dbpartsShiftOrder.BranchId,
                        WarehouseAreaId = detail.DestWarehouseAreaId,
                        PartId = detail.SparePartId,
                        Quantity = detail.Quantity.Value,
                        CreateTime=DateTime.Now,
                        CreatorId=userInfo.Id,
                        CreatorName=userInfo.Name
                    };
                    InsertToDatabase(partsStock);
                    partsStocks.Add(partsStock);
                } else {
                    partsStock.Quantity += detail.Quantity.Value;
                    UpdateToDatabase(partsStock);
                }
                foreach(var detailWithBatchNumber in detail.detailsWithBatchNumber) {
                    var partsStockBatchDetail = partsStockBatchDetails.Single(v => String.Compare(v.BatchNumber, detailWithBatchNumber.BatchNumber, StringComparison.OrdinalIgnoreCase) == 0 && v.Quantity == detailWithBatchNumber.Quantity);
                    partsStockBatchDetail.PartsStockId = partsStock.Id;
                    partsStock.PartsStockBatchDetails.Add(partsStockBatchDetail);
                }
            }
            var checkPartIds = partsStocks.Select(r => r.PartId);
            var checkWarehouseAreaIds = partsStocks.Select(r => r.WarehouseAreaId);
            var checkPartsStocks = ObjectContext.PartsStocks.Where(r => checkPartIds.Contains(r.PartId) && checkWarehouseAreaIds.Contains(r.WarehouseAreaId)).SetMergeOption(MergeOption.NoTracking).ToArray();
            foreach(var item in partsStocks) {
                //数量要大于0;2019-08-16允许目标库区的库存小于0                
                //新增修改的唯一性校验
                switch(item.EntityState) {
                    case EntityState.Modified:
                        var checkExists = checkPartsStocks.FirstOrDefault(r => r.Id == item.Id);
                        if(checkExists == null) {
                            throw new ValidationException(ErrorStrings.PartsStock_Validation6);
                        }
                        var checkOnlyOneForUpdate = checkPartsStocks.SingleOrDefault(r => r.Id != item.Id && r.PartId == item.PartId && r.WarehouseAreaId == item.WarehouseAreaId);
                        if(checkOnlyOneForUpdate != null) {
                            throw new ValidationException(ErrorStrings.PartsStock_Validation7);
                        }
                        item.ModifierId = userInfo.Id;
                        item.ModifierName = userInfo.Name;
                        item.ModifyTime = DateTime.Now;
                        break;
                    case EntityState.Added:
                        var checkOnlyOneForInsert = checkPartsStocks.SingleOrDefault(r => r.PartId == item.PartId && r.WarehouseAreaId == item.WarehouseAreaId);
                        if(checkOnlyOneForInsert != null) {
                            throw new ValidationException(ErrorStrings.PartsStock_Validation1);
                        }
                        item.CreatorId = userInfo.Id;
                        item.CreatorName = userInfo.Name;
                        item.CreateTime = DateTime.Now;
                        break;
                }
            }
            ObjectContext.SaveChanges();
            if(partsShiftOrders.PartsShiftOrderDetails.All(t => t.UpShelfQty == t.DownShelfQty && t.UpShelfQty == t.Quantity)) {
                partsShiftOrders.ShiftStatus = (int)DCSPartsShiftOrderShiftStatus.移库完成;
               //若移库清单中存在源库位库存数量=0，则删除配件的仓库库存信息
                var partsstocks = (from a in ObjectContext.PartsStocks.Include("WarehouseAreaCategory").Where(t => t.StorageCompanyId == userInfo.EnterpriseId && t.WarehouseId == partsShiftOrders.WarehouseId && (t.Quantity == 0 || t.Quantity == null))
                                   join b in ObjectContext.PartsShiftOrderDetails.Where(t => t.PartsShiftOrderId == partsShiftOrders.Id) on new {
                                       a.PartId,
                                       a.WarehouseAreaId
                                   } equals new {
                                       PartId = b.SparePartId,
                                       WarehouseAreaId = b.OriginalWarehouseAreaId
                                   }
                                   select a).ToArray();
                var spareIds = partsstocks.Select(t => t.PartId).ToArray();
                var allStock = ObjectContext.PartsStocks.Include("WarehouseAreaCategory").Where(r => spareIds.Contains(r.PartId) && r.WarehouseId==partsShiftOrders.WarehouseId).ToList();
                foreach(var partsStock in partsstocks) {
                    if(partsStock.Quantity == 0) {
                        var stock = allStock.Where(r => r.PartId == partsStock.PartId && r.WarehouseAreaCategory.Category == partsStock.WarehouseAreaCategory.Category).ToArray();
                        if(stock!=null &&  stock.Count() > 1) {
                            var partsStockHistory = new PartsStockHistory {
                                PartsStockId = partsStock.Id,
                                WarehouseId = partsStock.WarehouseId,
                                StorageCompanyId = partsStock.StorageCompanyId,
                                StorageCompanyType = partsStock.StorageCompanyType,
                                BranchId = partsStock.BranchId,
                                WarehouseAreaId = partsStock.WarehouseAreaId,
                                WarehouseAreaCategoryId = partsStock.WarehouseAreaCategoryId,
                                PartId = partsStock.PartId,
                                Quantity = partsStock.Quantity,
                                Remark = partsStock.Remark,
                                CreatorId = partsStock.CreatorId,
                                CreatorName = partsStock.CreatorName,
                                CreateTime = partsStock.CreateTime,
                                ModifierId = partsStock.ModifierId,
                                ModifierName = partsStock.ModifierName,
                                ModifyTime = partsStock.ModifyTime
                            };
                            InsertToDatabase(partsStockHistory);
                            DeleteFromDatabase(partsStock);
                            allStock.Remove(partsStock);
                        }
                       
                    } 
                }
            } else partsShiftOrders.ShiftStatus = (int)DCSPartsShiftOrderShiftStatus.部分移库;
            this.UpdatePartsShiftOrderValidate(partsShiftOrders);
            ObjectContext.SaveChanges();
        }
        public void 下架配件移库单(PartsShiftOrder partsShiftOrders) {
            var userInfo = Utils.GetCurrentUserInfo();
            var dbpartsShiftOrder = ObjectContext.PartsShiftOrders.Include("PartsShiftOrderDetails").Where(r => (r.ShiftStatus == (int)DCSPartsShiftOrderShiftStatus.部分移库 || r.ShiftStatus == (int)DCSPartsShiftOrderShiftStatus.待移库) && r.Status == (int)DcsPartsShiftOrderStatus.生效 && r.Id == partsShiftOrders.Id).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbpartsShiftOrder == null)
                throw new ValidationException(ErrorStrings.PartsShiftOrder_Data_Validation6);
            var partsShiftOrderDetails = partsShiftOrders.PartsShiftOrderDetails.ToArray();
            var oldPartsShiftOrderDetail = dbpartsShiftOrder.PartsShiftOrderDetails.ToArray();
            var sparePartIds = partsShiftOrderDetails.Select(t => t.SparePartId).ToArray();
            var codes = ObjectContext.AccurateTraces.Where(t => sparePartIds.Contains(t.PartId.Value) && t.CompanyId == userInfo.EnterpriseId && t.Status == (int)DCSAccurateTraceStatus.有效   && t.InQty>0 &&(t.InQty != t.OutQty || t.OutQty == null)).ToArray();
            var originalWarehouseAreaIds = partsShiftOrderDetails.Select(r => r.OriginalWarehouseAreaId).ToArray();
            var warehouseAreaIds = partsShiftOrderDetails.Select(r => r.DestWarehouseAreaId).Union(originalWarehouseAreaIds);
            var partsStocks = new PartsStockAch(this.DomainService).GetPartsStocksWithLock(ObjectContext.PartsStocks.Where(r => r.WarehouseId == dbpartsShiftOrder.WarehouseId && sparePartIds.Contains(r.PartId) && warehouseAreaIds.Contains(r.WarehouseAreaId))).ToList();
            var oldPartsShiftSIHDetails = ObjectContext.PartsShiftSIHDetails.Where(t => t.PartsShiftOrderId == partsShiftOrders.Id && t.Type == (int)DCSPartsShiftSIHDetailType.下架).ToArray();
            foreach(var item in partsShiftOrderDetails) {
                if(item.CourrentDownShelfQty.HasValue && item.CourrentDownShelfQty.Value<=0) {
                    continue;
                }
                if(string.IsNullOrEmpty(item.CourrentDownSIHCode)) {
                    if(item.CourrentDownShelfQty.HasValue && item.CourrentDownShelfQty.Value > 0) {
                        var oldDetail1 = oldPartsShiftOrderDetail.SingleOrDefault(t => t.Id == item.Id);
                        if(oldDetail1.DownShelfQty == null) {
                            oldDetail1.DownShelfQty = 0;
                        }
                        item.DownShelfQty = oldDetail1.DownShelfQty + item.CourrentDownShelfQty;
                        UpdateToDatabase(item);
                    }
                    continue;
                }
                var courrentCodes = item.CourrentDownSIHCode.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries).ToArray();
                List<AccurateTrace> traceCodes = new List<AccurateTrace>();
                bool isBox = false;
                foreach (var allcode in courrentCodes)
                {
                    var insetCode = codes.Where(t => allcode == t.SIHLabelCode && t.PartId == item.SparePartId && (t.TraceProperty == (int)DCSTraceProperty.精确追溯 || ((t.TraceProperty == (int)DCSTraceProperty.批次追溯 || t.TraceProperty == null) && t.InQty >= item.CourrentDownShelfQty)) && (t.WarehouseAreaId == item.OriginalWarehouseAreaId || t.WarehouseAreaId == null)).ToArray();
                    if(allcode.EndsWith("X")) {
                        insetCode = codes.Where(t => allcode == t.BoxCode && t.PartId == item.SparePartId && (t.TraceProperty == (int)DCSTraceProperty.精确追溯 || ((t.TraceProperty == (int)DCSTraceProperty.批次追溯 || t.TraceProperty == null) && t.InQty >= item.CourrentDownShelfQty)) && (t.WarehouseAreaId == item.OriginalWarehouseAreaId || t.WarehouseAreaId == null)).ToArray();
                        isBox = true;
                    }
                    if(insetCode.Count() == 0) {
                        throw new ValidationException("未找到有效的SIH标签码" + allcode);
                    }
                    foreach(var insertCode in insetCode) {
                        if(insertCode.OutQty == null) {
                            insertCode.OutQty = 0;
                        }
                        traceCodes.Add(insertCode);
                    }
                }
                if (traceCodes.Count() == 0 || (traceCodes.Count() < item.CourrentDownShelfQty && item.TraceProperty == (int)DCSTraceProperty.精确追溯) )
                {
                    throw new ValidationException(ErrorStrings.PartsShiftOrder_Data_Validation2 + item.CourrentDownSIHCode);
                }
                
                var oldDetail = oldPartsShiftOrderDetail.SingleOrDefault(t => t.Id == item.Id);
                if(oldDetail.DownShelfQty == null) {
                    oldDetail.DownShelfQty = 0;
                }

                if((item.TraceProperty == (int)DCSTraceProperty.批次追溯 || item.TraceProperty == null) && traceCodes.First().InQty - (traceCodes.First().OutQty ) < item.CourrentDownShelfQty) {
                    throw new ValidationException(string.Format(ErrorStrings.PartsShiftOrder_Data_Validation7, item.CourrentDownSIHCode));
                }
                if(isBox) {
                    if((item.TraceProperty == (int)DCSTraceProperty.批次追溯 || item.TraceProperty == null) && traceCodes.Sum(t => t.InQty - t.OutQty) != item.CourrentDownShelfQty) {
                        throw new ValidationException("SIH标签码" + item.CourrentDownSIHCode + "未出库数量不等于本次下架数量");
                    }
                } else {
                    if((item.TraceProperty == (int)DCSTraceProperty.批次追溯 || item.TraceProperty == null) && (traceCodes.First().InQty - traceCodes.First().OutQty) < item.CourrentDownShelfQty) {
                        throw new ValidationException("SIH标签码" + item.CourrentDownSIHCode + "未出库数量小于本次下架数量");
                    }
                }
               
                if(oldDetail.Quantity < oldDetail.DownShelfQty + item.CourrentDownShelfQty) {
                    throw new ValidationException(string.Format(ErrorStrings.PartsShiftOrder_Data_Validation9, item.SparePartCode));
                }
                item.DownSIHCode = oldDetail.DownSIHCode;
                foreach(var code in courrentCodes) {
                    if(string.IsNullOrEmpty(oldDetail.DownSIHCode)) {
                        item.DownSIHCode = code;
                    } else if(!oldDetail.DownSIHCode.Contains(code))
                        item.DownSIHCode = item.DownSIHCode + ";" + code;
                    else if(oldDetail.DownSIHCode.Contains(code) && item.TraceProperty == (int)DCSTraceProperty.精确追溯) {
                        throw new ValidationException(string.Format("{0}已下架", code));
                    }
                }
                var upQty = item.CourrentDownShelfQty;
                int i = 0;
                foreach(var sihCode in traceCodes) {
                    if(item.TraceProperty.HasValue && item.TraceProperty == (int)DCSTraceProperty.精确追溯) {
                        if (i >= upQty)
                        {
                            continue;
                        }
                        sihCode.OutQty = 1;
                        sihCode.ModifierId = userInfo.Id;
                        sihCode.ModifierName = userInfo.Name;
                        sihCode.ModifyTime = DateTime.Now;
                        UpdateToDatabase(sihCode);
                        var partsShiftSIHDetail = new PartsShiftSIHDetail {
                            PartsShiftOrderId = partsShiftOrders.Id,
                            PartsShiftOrderDetailId = item.Id,
                            Type = (int)DCSPartsShiftSIHDetailType.下架,
                            SIHCode = sihCode.SIHLabelCode,
                            Quantity = 1,
                        };
                        InsertToDatabase(partsShiftSIHDetail);
                        i++;
                    } else {
                        sihCode.OutQty = (sihCode.OutQty )+ item.CourrentDownShelfQty;
                        sihCode.ModifierId = userInfo.Id;
                        sihCode.ModifierName = userInfo.Name;
                        sihCode.ModifyTime = DateTime.Now;
                        UpdateToDatabase(sihCode);

                        var old = oldPartsShiftSIHDetails.Where(t=>t.PartsShiftOrderDetailId==item.Id&& t.SIHCode==sihCode.SIHLabelCode).FirstOrDefault();
                        if(old==null) {
                            var partsShiftSIHDetail = new PartsShiftSIHDetail {
                                PartsShiftOrderId = partsShiftOrders.Id,
                                PartsShiftOrderDetailId = item.Id,
                                Type = (int)DCSPartsShiftSIHDetailType.下架,
                                SIHCode = sihCode.SIHLabelCode,
                                Quantity = item.CourrentDownShelfQty,
                            };
                            InsertToDatabase(partsShiftSIHDetail);
                        } else {
                            old.Quantity = old.Quantity + item.CourrentDownShelfQty;
                            UpdateToDatabase(old);
                        }
                    }
                }
                item.DownShelfQty = oldDetail.DownShelfQty + item.CourrentDownShelfQty;
                UpdateToDatabase(item);             
            }
            this.UpdatePartsShiftOrderValidate(partsShiftOrders);
            //汇总源库位配件
            var originalPartsShiftOrderDetails = partsShiftOrderDetails.GroupBy(v => new {
                v.SparePartId,
                v.OriginalWarehouseAreaId
            }).Select(v => new {
                v.Key.SparePartId,
                v.Key.OriginalWarehouseAreaId,
                v.First().SparePartCode,
                v.First().OriginalWarehouseAreaCode,
                Quantity = v.Sum(detail => detail.CourrentDownShelfQty),
                detailsWithBatchNumber = v.Where(detail => !string.IsNullOrEmpty(detail.BatchNumber)).ToArray(),
            }).ToArray();
            #region 校验是否有未完成的盘点单
            var originalWarehouseArea = originalPartsShiftOrderDetails.Select(r => r.OriginalWarehouseAreaId).ToArray();
            var checkInventoryTemp = (from a in ObjectContext.PartsInventoryBills.Where(r => r.WarehouseId == dbpartsShiftOrder.WarehouseId && r.Status == (int)DcsPartsInventoryBillStatus.审核通过)
                                      from b in a.PartsInventoryDetails
                                      where originalWarehouseArea.Any(v => v == b.WarehouseAreaId)
                                      select b).ToArray();
            var check = checkInventoryTemp.Where(r => originalPartsShiftOrderDetails.Any(v => v.OriginalWarehouseAreaId == r.WarehouseAreaId && v.SparePartId == r.SparePartId)).ToArray();

            if(check.Length > 0) {
                var errorsparePartString = string.Join(",", check.Select(r => r.SparePartCode).ToArray());
                throw new ValidationException(string.Format(ErrorStrings.PartsShiftOrder_Validation12, errorsparePartString));
            }
            #endregion
            //获取配件库存批次明细
            var partsShiftOrderDetailsWithBatchNumber = partsShiftOrderDetails.Where(r => !string.IsNullOrEmpty(r.BatchNumber)).ToArray();
            var batchNumbers = partsShiftOrderDetailsWithBatchNumber.Select(r => r.BatchNumber.ToLower());
            var partsStockBatchDetails = ObjectContext.PartsStockBatchDetails.Where(r => batchNumbers.Contains(r.BatchNumber.ToLower())).ToArray();
            //新增配件库位变更履历（这个新增不用校验）移库才记履历
            if(dbpartsShiftOrder.Type == (int)DcsPartsShiftOrderType.正常移库 || dbpartsShiftOrder.Type == (int)DcsPartsShiftOrderType.问题区移库) {
                var warehouseAreaHistories = new List<WarehouseAreaHistory>();
                foreach(var item in partsShiftOrderDetails) {
                    if(item.CourrentDownShelfQty.Value==0) {
                        continue;
                    }
                    var t = ObjectContext.WarehouseAreaCategories.FirstOrDefault(r => r.Category == item.DestWarehouseAreaCategory);
                    if(t == null) {
                        throw new ValidationException(ErrorStrings.PartsShiftOrder_Validation13);
                    }
                    var warehouseAreaHistory = new WarehouseAreaHistory();
                    warehouseAreaHistory.WarehouseId = dbpartsShiftOrder.WarehouseId;
                    warehouseAreaHistory.StorageCompanyId = dbpartsShiftOrder.StorageCompanyId;
                    warehouseAreaHistory.StorageCompanyType = dbpartsShiftOrder.StorageCompanyType;
                    warehouseAreaHistory.BranchId = dbpartsShiftOrder.BranchId;
                    warehouseAreaHistory.WarehouseAreaId = item.OriginalWarehouseAreaId;
                    warehouseAreaHistory.WarehouseAreaCategoryId = item.OriginalWarehouseAreaCategory;
                    warehouseAreaHistory.DestWarehouseAreaId = item.DestWarehouseAreaId;
                    warehouseAreaHistory.DestWarehouseAreaCategoryId = t.Id;
                    warehouseAreaHistory.PartId = item.SparePartId;
                    warehouseAreaHistory.Quantity = item.CourrentDownShelfQty.Value;
                    warehouseAreaHistories.Add(warehouseAreaHistory);
                }
                foreach(var item in warehouseAreaHistories) {
                    item.CreatorId = userInfo.Id;
                    item.CreatorName = userInfo.Name;
                    item.CreateTime = DateTime.Now;
                    ObjectContext.WarehouseAreaHistories.AddObject(item);
                    InsertToDatabase(item);
                }
            }

         //   校验并调整源库位库存
            foreach(var detail in originalPartsShiftOrderDetails) {
                if(detail.Quantity==0) {
                    continue;
                }
                var partsStock = partsStocks.SingleOrDefault(v => v.WarehouseAreaId == detail.OriginalWarehouseAreaId && v.PartId == detail.SparePartId);
                if(partsStock == null)
                    throw new ValidationException(string.Format(ErrorStrings.PartsShiftOrder_Validation4, detail.SparePartCode, detail.OriginalWarehouseAreaCode));
                if(partsStock.LockedQty==null) {
                    partsStock.LockedQty = 0;
                }
                if((partsStock.Quantity - partsStock.LockedQty) < detail.Quantity)
                        throw new ValidationException(string.Format(ErrorStrings.PartsShiftOrder_Validation2, detail.SparePartCode, detail.OriginalWarehouseAreaCode));                                                                          
                partsStock.LockedQty = partsStock.LockedQty + detail.Quantity.Value;
                partsStock.ModifyTime = DateTime.Now;
                partsStock.ModifierId = userInfo.Id;
                partsStock.ModifierName = userInfo.Name;
                UpdateToDatabase(partsStock);

                foreach(var detailWithBatchNumber in detail.detailsWithBatchNumber) {
                    var partsStockBatchDetail = partsStockBatchDetails.SingleOrDefault(v => String.Compare(v.BatchNumber, detailWithBatchNumber.BatchNumber, StringComparison.OrdinalIgnoreCase) == 0 && v.Quantity == detailWithBatchNumber.Quantity);
                    if(partsStockBatchDetail == null)
                        throw new ValidationException(string.Format(ErrorStrings.PartsShiftOrder_Validation6, detailWithBatchNumber.BatchNumber));
                    if(partsStock.Id != partsStockBatchDetail.PartsStockId)
                        throw new ValidationException(string.Format(ErrorStrings.PartsShiftOrder_Validation5, detailWithBatchNumber.BatchNumber));
                }
            }
            ObjectContext.SaveChanges();
        }
        public void 修改移库单(PartsShiftOrder partsShiftOrder) {
            CheckEntityState(partsShiftOrder);
            //因为服务端需要调用
            UpdateToDatabase(partsShiftOrder);
            this.UpdatePartsShiftOrderValidate(partsShiftOrder);
        }
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:11:45
        //原分布类的函数全部转移到Ach                                                               
        [Update(UsingCustomMethod = true)]
        public void 作废配件移库单(PartsShiftOrder partsShiftOrder) {
            new PartsShiftOrderAch(this).作废配件移库单(partsShiftOrder);
        }
        [Update(UsingCustomMethod = true)]
        public void 驳回配件移库单(PartsShiftOrder partsShiftOrder) {
            new PartsShiftOrderAch(this).驳回配件移库单(partsShiftOrder);
        }
        [Update(UsingCustomMethod = true)]
        public void 初审配件移库单(PartsShiftOrder partsShiftOrder) {
            new PartsShiftOrderAch(this).初审配件移库单(partsShiftOrder);
        }
        [Update(UsingCustomMethod = true)]
        public void 终审配件移库单(PartsShiftOrder partsShiftOrder) {
            new PartsShiftOrderAch(this).终审配件移库单(partsShiftOrder);
        }
        [Update(UsingCustomMethod = true)]
        public void 生成配件移库单(PartsShiftOrder partsShiftOrder) {
            new PartsShiftOrderAch(this).生成配件移库单(partsShiftOrder);
        }
        public PartsShiftOrder GetPartsShiftOrderWithDetailsById(int id) {
            return new PartsShiftOrderAch(this).GetPartsShiftOrderWithDetailsById(id);
        }
        [Update(UsingCustomMethod = true)]
        public void 生成配件移库单并且执行移库(PartsShiftOrder partsShiftOrder) {
            new PartsShiftOrderAch(this).生成配件移库单并且执行移库(partsShiftOrder);
        }
        [Update(UsingCustomMethod = true)]
        public void 上传移库单附件(PartsShiftOrder partsShiftOrder) {
            new PartsShiftOrderAch(this).上传移库单附件(partsShiftOrder);
        }
         [Update(UsingCustomMethod = true)]
        public void 上架配件移库单(PartsShiftOrder partsShiftOrder) {
            new PartsShiftOrderAch(this).上架配件移库单( partsShiftOrder);
        }
       [Update(UsingCustomMethod = true)]
         public void 下架配件移库单(PartsShiftOrder partsShiftOrder) {
             new PartsShiftOrderAch(this).下架配件移库单(partsShiftOrder);
        }
       [Update(UsingCustomMethod = true)]
       public void 提交配件移库单(PartsShiftOrder partsShiftOrder) {
           new PartsShiftOrderAch(this).提交配件移库单(partsShiftOrder);
       }
        [Update(UsingCustomMethod = true)]
       public void 终止配件移库单(PartsShiftOrder partsShiftOrder) {
           new PartsShiftOrderAch(this).终止配件移库单(partsShiftOrder);
       }
        [Update(UsingCustomMethod = true)]
        public void 修改移库单(PartsShiftOrder partsShiftOrder) {
            new PartsShiftOrderAch(this).修改移库单(partsShiftOrder);
        }
    }
}
