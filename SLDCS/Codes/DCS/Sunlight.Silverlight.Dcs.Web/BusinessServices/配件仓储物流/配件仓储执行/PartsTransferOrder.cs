﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsTransferOrderAch : DcsSerivceAchieveBase {
        public PartsTransferOrderAch(DcsDomainService domainService)
            : base(domainService) {
        }

        public void 驳回配件调拨单(PartsTransferOrder partsTransferOrder) {
            var dbpartsTransferOrder = ObjectContext.PartsTransferOrders.Where(r => (r.Status == (int)DcsPartsTransferOrderStatus.初审通过 || r.Status == (int)DcsPartsTransferOrderStatus.新建) && r.Id == partsTransferOrder.Id).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbpartsTransferOrder == null)
                throw new ValidationException(ErrorStrings.PartsTransferOrder_Validation8);
            var userinfo = Utils.GetCurrentUserInfo();
            UpdateToDatabase(partsTransferOrder);
            partsTransferOrder.Status = (int)DcsPartsInventoryBillStatus.新建;
            partsTransferOrder.ApproverId = userinfo.Id;
            partsTransferOrder.ApproverName = userinfo.Name;
            partsTransferOrder.ApproveTime = System.DateTime.Now;
            partsTransferOrder.ModifierId = userinfo.Id;
            partsTransferOrder.ModifierName = userinfo.Name;
            partsTransferOrder.ModifyTime = DateTime.Now;
            UpdateToDatabase(partsTransferOrder);
        }

        public void 初审配件调拨单(PartsTransferOrder partsTransferOrder) {
            var dbpartsTransferOrder = ObjectContext.PartsTransferOrders.Where(r => (r.Status == (int)DcsPartsTransferOrderStatus.新建) && r.Id == partsTransferOrder.Id).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbpartsTransferOrder == null)
                throw new ValidationException(ErrorStrings.PartsTransferOrder_Validation9);
            //校验保底库存
            this.ValidateBottomStock(partsTransferOrder);

            var userinfo = Utils.GetCurrentUserInfo();
            UpdateToDatabase(partsTransferOrder);
            partsTransferOrder.Status = (int)DcsPartsTransferOrderStatus.初审通过;
            partsTransferOrder.InitialApproverId = userinfo.Id;
            partsTransferOrder.InitialApproverName = userinfo.Name;
            partsTransferOrder.InitialApproveTime = System.DateTime.Now;
            partsTransferOrder.ModifierId = userinfo.Id;
            partsTransferOrder.ModifierName = userinfo.Name;
            partsTransferOrder.ModifyTime = DateTime.Now;

            UpdateToDatabase(partsTransferOrder);
        }

        public void 审批配件调拨单(PartsTransferOrder partsTransferOrder) {
            var originalWarehouse = ObjectContext.Warehouses.Single(r => r.Id == partsTransferOrder.OriginalWarehouseId);
            var destWarehouse = ObjectContext.Warehouses.Single(r => r.Id == partsTransferOrder.DestWarehouseId);
            //是否自动完成出入库标识
            bool isAuto = false;
            if((originalWarehouse.StorageCenter.HasValue) && (originalWarehouse.StorageCenter == destWarehouse.StorageCenter)) {
                isAuto = true;
            }

            var dbPartsTransferOrder = ObjectContext.PartsTransferOrders.Where(r => r.Id == partsTransferOrder.Id && r.Status == (int)DcsPartsTransferOrderStatus.初审通过).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbPartsTransferOrder == null)
                throw new ValidationException(ErrorStrings.PartsTransferOrder_Validation10);

            var partsTransferDetails = partsTransferOrder.PartsTransferOrderDetails;
            if(partsTransferDetails.All(r => r.ConfirmedAmount == null || r.ConfirmedAmount == 0))
                throw new ValidationException(ErrorStrings.PartsTransferOrder_Validation2);
            var company = ObjectContext.Companies.SingleOrDefault(r => r.Id == partsTransferOrder.StorageCompanyId && r.Status != (int)DcsMasterDataStatus.作废);
            if(company == null)
                throw new ValidationException(ErrorStrings.PartsTransferOrder_Validation4);
            var branch = ObjectContext.Branches.SingleOrDefault(r => ObjectContext.Warehouses.Any(v => v.Id == partsTransferOrder.OriginalWarehouseId && v.BranchId == r.Id && v.Status != (int)DcsBaseDataStatus.作废) && r.Status != (int)DcsMasterDataStatus.作废);
            if(branch == null)
                throw new ValidationException(ErrorStrings.PartsTransferOrder_Validation5);
            var saleUnit = ObjectContext.SalesUnits.FirstOrDefault(r => ObjectContext.SalesUnitAffiWarehouses.Any(v => v.WarehouseId == partsTransferOrder.OriginalWarehouseId && r.Id == v.SalesUnitId) && r.Status != (int)DcsMasterDataStatus.作废);
            if(saleUnit == null)
                throw new ValidationException(ErrorStrings.PartsTransferOrder_Validation6);
            //校验保底库存
            this.ValidateBottomStock(partsTransferOrder);

            var partsOutboundPlan = new PartsOutboundPlan {
                WarehouseId = partsTransferOrder.OriginalWarehouseId,
                WarehouseCode = partsTransferOrder.OriginalWarehouseCode,
                WarehouseName = partsTransferOrder.OriginalWarehouseName,
                StorageCompanyId = partsTransferOrder.StorageCompanyId,
                StorageCompanyCode = company.Code,
                StorageCompanyName = company.Name,
                StorageCompanyType = partsTransferOrder.StorageCompanyType,
                BranchId = branch.Id,
                BranchCode = branch.Code,
                BranchName = branch.Name,
                CounterpartCompanyId = partsTransferOrder.StorageCompanyId,
                CounterpartCompanyCode = company.Code,
                CounterpartCompanyName = company.Name,
                ReceivingCompanyId = partsTransferOrder.StorageCompanyId,
                ReceivingCompanyCode = company.Code,
                ReceivingCompanyName = company.Name,
                SourceId = partsTransferOrder.Id,
                SourceCode = partsTransferOrder.Code,
                OutboundType = (int)DcsPartsOutboundType.配件调拨,
                OriginalRequirementBillId = partsTransferOrder.Id,
                OriginalRequirementBillCode = partsTransferOrder.Code,
                OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.配件调拨单,
                Status = isAuto ? (int)DcsPartsOutboundPlanStatus.出库完成 : (int)DcsPartsOutboundPlanStatus.新建,
                Remark = partsTransferOrder.Remark,
                ReceivingWarehouseId = partsTransferOrder.DestWarehouseId,
                ReceivingWarehouseCode = partsTransferOrder.DestWarehouseCode,
                ReceivingWarehouseName = partsTransferOrder.DestWarehouseName,
                ReceivingAddress=destWarehouse.Address,
                ShippingMethod = partsTransferOrder.ShippingMethod,
                PartsSalesCategoryId = saleUnit.PartsSalesCategoryId,
                GPMSPurOrderCode = partsTransferOrder.GPMSPurOrderCode,
                SAPPurchasePlanCode = partsTransferOrder.SAPPurchasePlanCode
            };
            var partsTransferDetailPartIds = partsTransferDetails.Select(r => r.SparePartId).ToArray();
            var salesUnit = ObjectContext.SalesUnits.FirstOrDefault(r => ObjectContext.SalesUnitAffiWarehouses.Any(v => v.SalesUnitId == r.Id && v.WarehouseId == partsTransferOrder.OriginalWarehouseId));
            if(salesUnit == null) {
                throw new ValidationException(ErrorStrings.PartsTransferOrder_Validation11);

            }

            foreach(var partsTransferDetail in partsTransferDetails) {
                if(partsTransferDetail.ConfirmedAmount.HasValue && partsTransferDetail.ConfirmedAmount.Value > 0) {
                    var partsOutboundPlanDetail = new PartsOutboundPlanDetail {
                        PartsOutboundPlanId = partsOutboundPlan.Id,
                        SparePartId = partsTransferDetail.SparePartId,
                        SparePartCode = partsTransferDetail.SparePartCode,
                        SparePartName = partsTransferDetail.SparePartName,
                        PlannedAmount = partsTransferDetail.ConfirmedAmount.Value,
                        OutboundFulfillment = isAuto ? partsTransferDetail.ConfirmedAmount : 0,
                        Price = partsTransferDetail.Price,
                        Remark = partsTransferDetail.Remark
                    };
                    partsOutboundPlan.PartsOutboundPlanDetails.Add(partsOutboundPlanDetail);
                }
            }
            if(!string.IsNullOrEmpty(partsTransferOrder.PurOrderCode)) {
                var partsPurchasePlan = ObjectContext.PartsPurchasePlan_HW.FirstOrDefault(r => r.Code == partsTransferOrder.PurOrderCode);
                if(partsPurchasePlan != null) {
                    var partsPurchasePlanDetails = ObjectContext.PartsPurchasePlanDetail_HW.Where(r => r.PartsPurchasePlanId == partsPurchasePlan.Id).ToArray();
                    foreach(var partsPurchasePlanDetail in partsPurchasePlanDetails) {
                        foreach(var partsTransferDetail in partsTransferDetails) {
                            if(partsPurchasePlanDetail.PartId == partsTransferDetail.SparePartId) {
                                partsPurchasePlanDetail.UntFulfilledQty += (partsTransferDetail.PlannedAmount - Convert.ToInt32(partsTransferDetail.ConfirmedAmount));
                                partsPurchasePlanDetail.EndAmount = (partsPurchasePlanDetail.EndAmount ?? 0) + (partsTransferDetail.PlannedAmount - Convert.ToInt32(partsTransferDetail.ConfirmedAmount));
                                UpdateToDatabase(partsPurchasePlanDetail);
                            }
                        }
                    }
                }
            }


            if(isAuto) {
                InsertToDatabase(partsOutboundPlan);
                new PartsOutboundPlanAch(this.DomainService).InsertPartsOutboundPlanValidate(partsOutboundPlan);
            } else {
                DomainService.生成配件出库计划(partsOutboundPlan);
            }


            //var dbSalesUnitAffiWarehouses = ObjectContext.SalesUnitAffiWarehouses.Include("SalesUnit").Where(r => r.WarehouseId == partsTransferOrder.OriginalWarehouseId || r.WarehouseId == partsTransferOrder.DestWarehouseId).ToList();
            //var originalPartsSalesCategoryId = dbSalesUnitAffiWarehouses.First(r => r.WarehouseId == partsTransferOrder.OriginalWarehouseId).SalesUnit.PartsSalesCategoryId;
            //var destPartsSalesCategoryId = dbSalesUnitAffiWarehouses.First(r => r.WarehouseId == partsTransferOrder.DestWarehouseId).SalesUnit.PartsSalesCategoryId;
            //var partIds = partsTransferOrder.PartsTransferOrderDetails.Select(r => r.SparePartId).ToArray();
            //var dbPartsPlannedPrices = ObjectContext.PartsPlannedPrices.Where(r => partIds.Contains(r.SparePartId) && (r.PartsSalesCategoryId == originalPartsSalesCategoryId || r.PartsSalesCategoryId == destPartsSalesCategoryId)).ToList();
            //foreach(var detail in partsTransferOrder.PartsTransferOrderDetails) {
            //    var dbPartsPlannedPrice1 = dbPartsPlannedPrices.First(r => r.SparePartId == detail.SparePartId && r.PartsSalesCategoryId == originalPartsSalesCategoryId);
            //    var dbPartsPlannedPrice2 = dbPartsPlannedPrices.First(r => r.SparePartId == detail.SparePartId && r.PartsSalesCategoryId == destPartsSalesCategoryId);
            //    if(dbPartsPlannedPrice1.PlannedPrice != dbPartsPlannedPrice2.PlannedPrice)
            //        throw new ValidationException("调拨出库入库仓库配件计划价不同无法生成调拨单");
            //}

            //步骤3 自动完成出库发运与入库
            if(isAuto) {
                var keyvaluePartsShippingMethod = ObjectContext.KeyValueItems.Where(r => r.Name == "PartsShipping_Method").ToList();
                var shippingMethodKey = keyvaluePartsShippingMethod.Any(r => r.Value == "客户自行运输") ? keyvaluePartsShippingMethod.First(r => r.Value == "客户自行运输").Key : default(int);
                #region 生成出库单及清单，生成发运单及清单
                var outpartIds = partsOutboundPlan.PartsOutboundPlanDetails.Select(r => r.SparePartId).ToArray();

                var newPartsOutboundBill = new PartsOutboundBill();
                newPartsOutboundBill.PartsOutboundPlanId = partsOutboundPlan.Id;
                newPartsOutboundBill.WarehouseId = partsOutboundPlan.WarehouseId;
                newPartsOutboundBill.WarehouseCode = partsOutboundPlan.WarehouseCode;
                newPartsOutboundBill.WarehouseName = partsOutboundPlan.WarehouseName;
                newPartsOutboundBill.StorageCompanyId = partsOutboundPlan.StorageCompanyId;
                newPartsOutboundBill.StorageCompanyCode = partsOutboundPlan.StorageCompanyCode;
                newPartsOutboundBill.StorageCompanyName = partsOutboundPlan.StorageCompanyName;
                newPartsOutboundBill.StorageCompanyType = partsOutboundPlan.StorageCompanyType;
                newPartsOutboundBill.BranchId = partsOutboundPlan.BranchId;
                newPartsOutboundBill.BranchCode = partsOutboundPlan.BranchCode;
                newPartsOutboundBill.BranchName = partsOutboundPlan.BranchName;
                newPartsOutboundBill.PartsSalesCategoryId = partsOutboundPlan.PartsSalesCategoryId;
                newPartsOutboundBill.PartsSalesOrderTypeId = partsOutboundPlan.PartsSalesOrderTypeId;
                newPartsOutboundBill.PartsSalesOrderTypeName = partsOutboundPlan.PartsSalesOrderTypeName;
                newPartsOutboundBill.CounterpartCompanyId = partsOutboundPlan.CounterpartCompanyId;
                newPartsOutboundBill.CounterpartCompanyCode = partsOutboundPlan.CounterpartCompanyCode;
                newPartsOutboundBill.CounterpartCompanyName = partsOutboundPlan.CounterpartCompanyName;
                newPartsOutboundBill.ReceivingCompanyId = partsOutboundPlan.ReceivingCompanyId;
                newPartsOutboundBill.ReceivingCompanyCode = partsOutboundPlan.ReceivingCompanyCode;
                newPartsOutboundBill.ReceivingCompanyName = partsOutboundPlan.ReceivingCompanyName;
                newPartsOutboundBill.ReceivingWarehouseId = partsOutboundPlan.ReceivingWarehouseId;
                newPartsOutboundBill.ReceivingWarehouseCode = partsOutboundPlan.ReceivingWarehouseCode;
                newPartsOutboundBill.ReceivingWarehouseName = partsOutboundPlan.ReceivingWarehouseName;
                newPartsOutboundBill.OutboundType = partsOutboundPlan.OutboundType;
                newPartsOutboundBill.ShippingMethod = partsOutboundPlan.ShippingMethod;
                newPartsOutboundBill.CustomerAccountId = partsOutboundPlan.CustomerAccountId;
                newPartsOutboundBill.OriginalRequirementBillId = partsOutboundPlan.OriginalRequirementBillId;
                newPartsOutboundBill.OriginalRequirementBillType = partsOutboundPlan.OriginalRequirementBillType;
                newPartsOutboundBill.OriginalRequirementBillCode = partsOutboundPlan.OriginalRequirementBillCode;
                newPartsOutboundBill.SettlementStatus = (int)DcsPartsSettlementStatus.待结算;
                newPartsOutboundBill.Remark = partsOutboundPlan.Remark;
                newPartsOutboundBill.GPMSPurOrderCode = partsOutboundPlan.GPMSPurOrderCode;
                newPartsOutboundBill.SAPPurchasePlanCode = partsOutboundPlan.SAPPurchasePlanCode;
                partsOutboundPlan.PartsOutboundBills.Add(newPartsOutboundBill);
                InsertToDatabase(newPartsOutboundBill);
                new PartsOutboundBillAch(this.DomainService).InsertPartsOutboundBillValidate(newPartsOutboundBill);

                var newPartsShippingOrder = new PartsShippingOrder();
                newPartsShippingOrder.Type = (int)DcsPartsShippingOrderType.调拨;
                newPartsShippingOrder.PartsSalesCategoryId = saleUnit.PartsSalesCategoryId;
                newPartsShippingOrder.BranchId = newPartsOutboundBill.BranchId;
                newPartsShippingOrder.ShippingCompanyId = newPartsOutboundBill.StorageCompanyId;
                newPartsShippingOrder.ShippingCompanyCode = newPartsOutboundBill.StorageCompanyCode;
                newPartsShippingOrder.ShippingCompanyName = newPartsOutboundBill.StorageCompanyName;
                newPartsShippingOrder.SettlementCompanyId = newPartsOutboundBill.StorageCompanyId;
                newPartsShippingOrder.SettlementCompanyCode = newPartsOutboundBill.StorageCompanyCode;
                newPartsShippingOrder.SettlementCompanyName = newPartsOutboundBill.StorageCompanyName;
                newPartsShippingOrder.ReceivingCompanyId = newPartsOutboundBill.ReceivingCompanyId;
                newPartsShippingOrder.ReceivingCompanyCode = newPartsOutboundBill.ReceivingCompanyCode;
                newPartsShippingOrder.ReceivingCompanyName = newPartsOutboundBill.ReceivingCompanyName;
                newPartsShippingOrder.InvoiceReceiveSaleCateId = newPartsOutboundBill.PartsSalesOrderTypeId;
                newPartsShippingOrder.InvoiceReceiveSaleCateName = newPartsOutboundBill.PartsSalesOrderTypeName;
                newPartsShippingOrder.WarehouseId = newPartsOutboundBill.WarehouseId;
                newPartsShippingOrder.WarehouseCode = newPartsOutboundBill.WarehouseCode;
                newPartsShippingOrder.WarehouseName = newPartsOutboundBill.WarehouseName;
                newPartsShippingOrder.ReceivingWarehouseId = newPartsOutboundBill.ReceivingWarehouseId;
                newPartsShippingOrder.ReceivingWarehouseCode = newPartsOutboundBill.ReceivingWarehouseCode;
                newPartsShippingOrder.ReceivingWarehouseName = newPartsOutboundBill.ReceivingWarehouseName;
                newPartsShippingOrder.OriginalRequirementBillId = newPartsOutboundBill.OriginalRequirementBillId;
                newPartsShippingOrder.OriginalRequirementBillType = newPartsOutboundBill.OriginalRequirementBillType;
                newPartsShippingOrder.OriginalRequirementBillCode = newPartsOutboundBill.OriginalRequirementBillCode;
                newPartsShippingOrder.ShippingMethod = shippingMethodKey;
                newPartsShippingOrder.GPMSPurOrderCode = newPartsOutboundBill.GPMSPurOrderCode;
                newPartsShippingOrder.SAPPurchasePlanCode = newPartsOutboundBill.SAPPurchasePlanCode;
                newPartsShippingOrder.Status = (int)DcsPartsShippingOrderStatus.回执确认;
                newPartsShippingOrder.Remark = newPartsOutboundBill.Remark;
                DomainService.InsertPartsShippingOrderValidate(newPartsShippingOrder);
                newPartsOutboundBill.PartsShippingOrderId = newPartsShippingOrder.Id;

                //生成入库计划单
                var warehouse = ObjectContext.Warehouses.FirstOrDefault(r => r.Id == partsTransferOrder.DestWarehouseId);
                var saleUnit2 = ObjectContext.SalesUnits.FirstOrDefault(r => ObjectContext.SalesUnitAffiWarehouses.Any(v => v.WarehouseId == partsTransferOrder.DestWarehouseId && r.Id == v.SalesUnitId) && r.Status != (int)DcsMasterDataStatus.作废);
                var newPartsInboundPlan = new PartsInboundPlan();
                newPartsInboundPlan.WarehouseId = newPartsShippingOrder.ReceivingWarehouseId.Value;
                newPartsInboundPlan.WarehouseCode = newPartsShippingOrder.ReceivingWarehouseCode;
                newPartsInboundPlan.WarehouseName = newPartsShippingOrder.ReceivingWarehouseName;
                newPartsInboundPlan.StorageCompanyId = warehouse.StorageCompanyId;
                newPartsInboundPlan.StorageCompanyCode = company.Code;
                newPartsInboundPlan.StorageCompanyName = company.Name;
                newPartsInboundPlan.StorageCompanyType = company.Type;
                newPartsInboundPlan.BranchId = newPartsShippingOrder.BranchId;
                newPartsInboundPlan.BranchCode = newPartsOutboundBill.BranchCode;
                newPartsInboundPlan.BranchName = newPartsOutboundBill.BranchName;
                newPartsInboundPlan.PartsSalesCategoryId = saleUnit2.PartsSalesCategoryId;
                newPartsInboundPlan.CounterpartCompanyId = newPartsShippingOrder.SettlementCompanyId;
                newPartsInboundPlan.CounterpartCompanyCode = newPartsShippingOrder.SettlementCompanyCode;
                newPartsInboundPlan.CounterpartCompanyName = newPartsShippingOrder.SettlementCompanyName;
                newPartsInboundPlan.SourceId = newPartsShippingOrder.Id;
                newPartsInboundPlan.SourceCode = newPartsShippingOrder.Code;
                newPartsInboundPlan.InboundType = (int)DcsPartsInboundType.配件调拨;
                newPartsInboundPlan.OriginalRequirementBillId = newPartsShippingOrder.OriginalRequirementBillId.Value;
                newPartsInboundPlan.OriginalRequirementBillType = newPartsShippingOrder.OriginalRequirementBillType.Value;
                newPartsInboundPlan.OriginalRequirementBillCode = newPartsShippingOrder.OriginalRequirementBillCode;
                newPartsInboundPlan.Status = (int)DcsPartsInboundPlanStatus.上架完成;
                newPartsInboundPlan.Remark = newPartsShippingOrder.Remark;
                newPartsInboundPlan.GPMSPurOrderCode = newPartsShippingOrder.GPMSPurOrderCode;
                newPartsInboundPlan.SAPPurchasePlanCode = newPartsShippingOrder.SAPPurchasePlanCode;
                newPartsShippingOrder.PartsInboundPlans.Add(newPartsInboundPlan);

                var newPartsInboundCheckBill = new PartsInboundCheckBill();
                newPartsInboundCheckBill.PartsInboundPlanId = newPartsInboundPlan.Id;
                newPartsInboundCheckBill.WarehouseId = newPartsInboundPlan.WarehouseId;
                newPartsInboundCheckBill.WarehouseCode = newPartsInboundPlan.WarehouseCode;
                newPartsInboundCheckBill.WarehouseName = newPartsInboundPlan.WarehouseName;
                newPartsInboundCheckBill.StorageCompanyId = newPartsInboundPlan.StorageCompanyId;
                newPartsInboundCheckBill.StorageCompanyCode = newPartsInboundPlan.StorageCompanyCode;
                newPartsInboundCheckBill.StorageCompanyName = newPartsInboundPlan.StorageCompanyName;
                newPartsInboundCheckBill.StorageCompanyType = newPartsInboundPlan.StorageCompanyType;
                newPartsInboundCheckBill.PartsSalesCategoryId = newPartsInboundPlan.PartsSalesCategoryId;
                newPartsInboundCheckBill.BranchId = newPartsInboundPlan.BranchId;
                newPartsInboundCheckBill.BranchCode = newPartsInboundPlan.BranchCode;
                newPartsInboundCheckBill.BranchName = newPartsInboundPlan.BranchName;
                newPartsInboundCheckBill.CounterpartCompanyId = newPartsInboundPlan.CounterpartCompanyId;
                newPartsInboundCheckBill.CounterpartCompanyCode = newPartsInboundPlan.CounterpartCompanyCode;
                newPartsInboundCheckBill.CounterpartCompanyName = newPartsInboundPlan.CounterpartCompanyName;
                newPartsInboundCheckBill.InboundType = (int)DcsPartsInboundType.配件调拨;
                newPartsInboundCheckBill.OriginalRequirementBillId = newPartsInboundPlan.OriginalRequirementBillId;
                newPartsInboundCheckBill.OriginalRequirementBillType = newPartsInboundPlan.OriginalRequirementBillType;
                newPartsInboundCheckBill.OriginalRequirementBillCode = newPartsInboundPlan.OriginalRequirementBillCode;
                newPartsInboundCheckBill.Status = (int)DcsPartsInboundCheckBillStatus.上架完成;
                newPartsInboundCheckBill.SettlementStatus = (int)DcsPartsSettlementStatus.待结算;
                newPartsInboundCheckBill.GPMSPurOrderCode = newPartsInboundPlan.GPMSPurOrderCode;
                newPartsInboundCheckBill.PurOrderCode = partsTransferOrder.PurOrderCode;
                newPartsInboundCheckBill.SAPPurchasePlanCode = partsTransferOrder.SAPPurchasePlanCode;
                newPartsInboundPlan.PartsInboundCheckBills.Add(newPartsInboundCheckBill);
                InsertToDatabase(newPartsInboundCheckBill);
                new PartsInboundCheckBillAch(this.DomainService).InsertPartsInboundCheckBillValidate(newPartsInboundCheckBill);

                foreach(var detail in partsTransferOrder.PartsTransferOrderDetails.Where(r => r.ConfirmedAmount.HasValue && r.ConfirmedAmount > 0).ToList()) {
                    var newPartsShippingOrderDetail = new PartsShippingOrderDetail();
                    newPartsShippingOrderDetail.PartsShippingOrderId = newPartsShippingOrder.Id;
                    newPartsShippingOrderDetail.SparePartId = detail.SparePartId;
                    newPartsShippingOrderDetail.SparePartCode = detail.SparePartCode;
                    newPartsShippingOrderDetail.SparePartName = detail.SparePartName;
                    newPartsShippingOrderDetail.ShippingAmount = detail.ConfirmedAmount.Value;
                    newPartsShippingOrderDetail.ConfirmedAmount = detail.ConfirmedAmount;
                    newPartsShippingOrderDetail.SettlementPrice = detail.Price;
                    newPartsShippingOrderDetail.POCode = detail.POCode;
                    newPartsShippingOrderDetail.PartsOutboundPlanId = partsOutboundPlan.Id;
                    newPartsShippingOrderDetail.PartsOutboundPlanCode = partsOutboundPlan.Code;
                    newPartsShippingOrder.PartsShippingOrderDetails.Add(newPartsShippingOrderDetail);

                    var newPartsInboundPlanDetail = new PartsInboundPlanDetail();
                    newPartsInboundPlanDetail.PartsInboundPlanId = newPartsInboundPlan.Id;
                    newPartsInboundPlanDetail.SparePartId = detail.SparePartId;
                    newPartsInboundPlanDetail.SparePartCode = detail.SparePartCode;
                    newPartsInboundPlanDetail.SparePartName = detail.SparePartName;
                    newPartsInboundPlanDetail.PlannedAmount = detail.ConfirmedAmount.HasValue ? detail.ConfirmedAmount.Value : default(int);
                    newPartsInboundPlanDetail.InspectedQuantity = detail.ConfirmedAmount;
                    newPartsInboundPlanDetail.Price = detail.Price;
                    newPartsInboundPlanDetail.Remark = detail.Remark;
                    newPartsInboundPlanDetail.POCode = detail.POCode;
                    newPartsInboundPlanDetail.OriginalPlanNumber = detail.OriginalPlanNumber;
                    newPartsInboundPlan.PartsInboundPlanDetails.Add(newPartsInboundPlanDetail);
                }
                InsertToDatabase(newPartsShippingOrder);
                InsertToDatabase(newPartsInboundPlan);
                new PartsInboundPlanAch(this.DomainService).InsertPartsInboundPlanValidate(newPartsInboundPlan);

                var dbPartsStocks = ObjectContext.PartsStocks.Include("WarehouseArea").Where(r => r.WarehouseId == newPartsOutboundBill.WarehouseId && outpartIds.Contains(r.PartId) && r.WarehouseAreaCategory.Category == (int)DcsAreaType.保管区 && r.WarehouseArea.Status == (int)DcsBaseDataStatus.有效).ToList();
                var inPartsStocks = ObjectContext.PartsStocks.Include("WarehouseArea").Where(r => r.WarehouseId == newPartsInboundPlan.WarehouseId && outpartIds.Contains(r.PartId) && r.WarehouseAreaCategory.Category == (int)DcsAreaType.保管区 && r.WarehouseArea.Status == (int)DcsBaseDataStatus.有效).ToList();
                //计划价更改为配件成本价
                var db2PartsPlannedPrices = ObjectContext.EnterprisePartsCosts.Where(r => r.OwnerCompanyId == newPartsOutboundBill.StorageCompanyId && r.PartsSalesCategoryId == newPartsOutboundBill.PartsSalesCategoryId && outpartIds.Contains(r.SparePartId)).ToList();
                var inPartsPlannedPrices = ObjectContext.EnterprisePartsCosts.Where(r => r.OwnerCompanyId == newPartsInboundCheckBill.StorageCompanyId && r.PartsSalesCategoryId == newPartsInboundCheckBill.PartsSalesCategoryId && outpartIds.Contains(r.SparePartId)).ToList();
                foreach(var detail in partsOutboundPlan.PartsOutboundPlanDetails) {
                    var partsStock = dbPartsStocks.Where(r => r.PartId == detail.SparePartId).OrderByDescending(r => r.Quantity);// 根据 配件调拨单清单  SparePartId
                    if(!partsStock.Any())
                        throw new ValidationException(string.Format(ErrorStrings.PartsTransferOrder_Validation12, detail.SparePartCode));
                    if(partsStock.Sum(r => r.Quantity) < detail.OutboundFulfillment)
                        throw new ValidationException(string.Format(ErrorStrings.PartsTransferOrder_Validation13, detail.SparePartCode));
                    var partsPlannedPrice = db2PartsPlannedPrices.FirstOrDefault(r => r.SparePartId == detail.SparePartId);
                    var costPrice = inPartsPlannedPrices.FirstOrDefault(r => r.SparePartId == detail.SparePartId);
                    foreach(var stock in partsStock.ToList()) {
                        //入库出库对应库存记录及库位
                        var inPartsStock = inPartsStocks.FirstOrDefault(o => o.PartId == detail.SparePartId && o.WarehouseArea.Code == stock.WarehouseArea.Code);
                        var inWarehouseArea = ObjectContext.WarehouseAreas.FirstOrDefault(p => p.Code == stock.WarehouseArea.Code && p.WarehouseId == newPartsInboundPlan.WarehouseId && p.Status == (int)DcsBaseDataStatus.有效);
                        if (inWarehouseArea == null)
                        {
                            throw new ValidationException(string.Format("未找到{0}的入库库位编号{1}", detail.SparePartCode,stock.WarehouseArea.Code));
                        }
                        //出库单清单与入库单清单新增，并自动扣减出库仓库保管区库存，新增目标仓库保管区库存数量
                        var newPartsOutboundBillDetail = new PartsOutboundBillDetail();
                        var newPartsInboundCheckBillDetail = new PartsInboundCheckBillDetail();
                        if(stock.Quantity >= detail.OutboundFulfillment) {
                            stock.Quantity -= detail.OutboundFulfillment.Value;
                            UpdateToDatabase(stock);  //更新库存
                            new PartsStockAch(this.DomainService).UpdatePartsStockValidate(stock);
                            newPartsOutboundBillDetail.PartsOutboundBillId = newPartsOutboundBill.Id;//出库单清单
                            newPartsOutboundBillDetail.SparePartId = detail.SparePartId;
                            newPartsOutboundBillDetail.SparePartCode = detail.SparePartCode;
                            newPartsOutboundBillDetail.SparePartName = detail.SparePartName;
                            newPartsOutboundBillDetail.OutboundAmount = detail.OutboundFulfillment.Value;
                            newPartsOutboundBillDetail.WarehouseAreaId = stock.WarehouseAreaId;
                            newPartsOutboundBillDetail.WarehouseAreaCode = stock.WarehouseArea.Code;
                            newPartsOutboundBillDetail.SettlementPrice = detail.Price;
                            newPartsOutboundBillDetail.CostPrice = partsPlannedPrice != null ? partsPlannedPrice.CostPrice : default(decimal); //配件成本价赋值给出库清单
                            newPartsOutboundBill.PartsOutboundBillDetails.Add(newPartsOutboundBillDetail);

                            if(inPartsStock == null) {
                                inPartsStock = new PartsStock();
                                inPartsStock.WarehouseId = newPartsInboundCheckBill.WarehouseId;
                                inPartsStock.StorageCompanyId = newPartsInboundCheckBill.StorageCompanyId;
                                inPartsStock.StorageCompanyType = newPartsInboundCheckBill.StorageCompanyType;
                                inPartsStock.BranchId = newPartsInboundCheckBill.BranchId;
                                inPartsStock.WarehouseAreaId = inWarehouseArea.Id;
                                inPartsStock.WarehouseAreaCategoryId = inWarehouseArea.AreaCategoryId;
                                inPartsStock.PartId = detail.SparePartId;
                                inPartsStock.Quantity = detail.OutboundFulfillment.Value;
                                InsertToDatabase(inPartsStock);
                                new PartsStockAch(this.DomainService).InsertPartsStockValidate(inPartsStock);
                            } else {
                                //更新收货出库库存
                                inPartsStock.Quantity += detail.OutboundFulfillment.Value;
                                UpdateToDatabase(inPartsStock);
                                new PartsStockAch(this.DomainService).UpdatePartsStockValidate(inPartsStock);
                            }

                            newPartsInboundCheckBillDetail.PartsInboundCheckBillId = newPartsInboundCheckBill.Id;
                            newPartsInboundCheckBillDetail.SparePartId = detail.SparePartId;
                            newPartsInboundCheckBillDetail.SparePartCode = detail.SparePartCode;
                            newPartsInboundCheckBillDetail.SparePartName = detail.SparePartName;
                            newPartsInboundCheckBillDetail.WarehouseAreaId = stock.WarehouseAreaId;
                            newPartsInboundCheckBillDetail.WarehouseAreaCode = stock.WarehouseArea.Code;
                            newPartsInboundCheckBillDetail.InspectedQuantity = detail.OutboundFulfillment.Value;
                            newPartsInboundCheckBillDetail.SettlementPrice = detail.Price;
                            newPartsInboundCheckBillDetail.POCode = "";
                            newPartsInboundCheckBillDetail.CostPrice = costPrice != null ? costPrice.CostPrice : default(decimal);
                            newPartsInboundCheckBill.PartsInboundCheckBillDetails.Add(newPartsInboundCheckBillDetail);
                            break;
                        }
                        if(inPartsStock == null) {
                            inPartsStock = new PartsStock();
                            inPartsStock.WarehouseId = newPartsInboundCheckBill.WarehouseId;
                            inPartsStock.StorageCompanyId = newPartsInboundCheckBill.StorageCompanyId;
                            inPartsStock.StorageCompanyType = newPartsInboundCheckBill.StorageCompanyType;
                            inPartsStock.BranchId = newPartsInboundCheckBill.BranchId;
                            inPartsStock.WarehouseAreaId = inWarehouseArea.Id;
                            inPartsStock.WarehouseAreaCategoryId = inWarehouseArea.AreaCategoryId;
                            inPartsStock.PartId = detail.SparePartId;
                            inPartsStock.Quantity = stock.Quantity;
                            InsertToDatabase(inPartsStock);
                            new PartsStockAch(this.DomainService).InsertPartsStockValidate(inPartsStock);
                        } else {
                            //更新收货出库库存
                            inPartsStock.Quantity += stock.Quantity;
                            UpdateToDatabase(inPartsStock);
                            new PartsStockAch(this.DomainService).UpdatePartsStockValidate(inPartsStock);
                        }
                        newPartsInboundCheckBillDetail.PartsInboundCheckBillId = newPartsInboundCheckBill.Id;
                        newPartsInboundCheckBillDetail.SparePartId = detail.SparePartId;
                        newPartsInboundCheckBillDetail.SparePartCode = detail.SparePartCode;
                        newPartsInboundCheckBillDetail.SparePartName = detail.SparePartName;
                        newPartsInboundCheckBillDetail.WarehouseAreaId = stock.WarehouseAreaId;
                        newPartsInboundCheckBillDetail.WarehouseAreaCode = stock.WarehouseArea.Code;
                        newPartsInboundCheckBillDetail.InspectedQuantity = stock.Quantity;
                        newPartsInboundCheckBillDetail.SettlementPrice = detail.Price;
                        newPartsInboundCheckBillDetail.POCode = "";
                        newPartsInboundCheckBillDetail.CostPrice = costPrice != null ? costPrice.CostPrice : default(decimal);
                        newPartsInboundCheckBill.PartsInboundCheckBillDetails.Add(newPartsInboundCheckBillDetail);
                        
                        new PartsStockAch(this.DomainService).UpdatePartsStockValidate(stock);
                        newPartsOutboundBillDetail.PartsOutboundBillId = newPartsOutboundBill.Id;
                        newPartsOutboundBillDetail.SparePartId = detail.SparePartId;
                        newPartsOutboundBillDetail.SparePartCode = detail.SparePartCode;
                        newPartsOutboundBillDetail.SparePartName = detail.SparePartName;
                        newPartsOutboundBillDetail.OutboundAmount = stock.Quantity;
                        newPartsOutboundBillDetail.WarehouseAreaId = stock.WarehouseAreaId;
                        newPartsOutboundBillDetail.WarehouseAreaCode = stock.WarehouseArea.Code;
                        newPartsOutboundBillDetail.SettlementPrice = detail.Price;
                        newPartsOutboundBillDetail.CostPrice = partsPlannedPrice != null ? partsPlannedPrice.CostPrice : default(decimal);
                        newPartsOutboundBill.PartsOutboundBillDetails.Add(newPartsOutboundBillDetail);
                        detail.OutboundFulfillment -= stock.Quantity;
                        stock.Quantity = 0;
                        UpdateToDatabase(stock); //更新库存
                    }
                }
                partsOutboundPlan.PartsOutboundBills.Add(newPartsOutboundBill);
                InsertToDatabase(partsOutboundPlan);
                newPartsInboundPlan.PartsInboundCheckBills.Add(newPartsInboundCheckBill);
                InsertToDatabase(newPartsInboundPlan);
                #endregion

                #region 调拨单赋值，并更新到数据库
                //计划价更改为成本价
                var partsPlannedPrices = ObjectContext.EnterprisePartsCosts.Where(r => r.OwnerCompanyId == newPartsOutboundBill.StorageCompanyId && r.PartsSalesCategoryId == salesUnit.PartsSalesCategoryId && partsTransferDetailPartIds.Contains(r.SparePartId)).ToList();
                foreach(var partsTransferDetail in partsTransferDetails) {
                    var partsPlannedPrice = partsPlannedPrices.FirstOrDefault(r => r.SparePartId == partsTransferDetail.SparePartId);
                    if(partsPlannedPrice != null) {
                        partsTransferDetail.PlannPrice = partsPlannedPrice.CostPrice; //配件调拨单清单 计划价赋值
                    }
                }
                var userinfo = Utils.GetCurrentUserInfo();
                partsTransferOrder.TotalPlanAmount = partsTransferDetails.Sum(r => r.PlannedAmount * r.PlannPrice);  //调拨单主单根据调拨单清单进行赋值
                partsTransferOrder.Status = (int)DcsPartsTransferOrderStatus.已审批;
                partsTransferOrder.ApproverId = userinfo.Id;
                partsTransferOrder.ApproverName = userinfo.Name;
                partsTransferOrder.ApproveTime = System.DateTime.Now;
                partsTransferOrder.ModifierId = userinfo.Id;
                partsTransferOrder.ModifierName = userinfo.Name;
                partsTransferOrder.ModifyTime = DateTime.Now;
                UpdateToDatabase(partsTransferOrder);
                //this.UpdatePartsTransferOrderValidate(partsTransferOrder);
                #endregion

                #region 新增配件物流批次，单据清单
                var newPartsLogisticBatch = new PartsLogisticBatch();
                newPartsLogisticBatch.CompanyId = newPartsShippingOrder.BranchId;
                newPartsLogisticBatch.Status = (int)DcsPartsLogisticBatchStatus.完成;
                newPartsLogisticBatch.ShippingStatus = (int)DcsPartsLogisticBatchShippingStatus.发运;
                newPartsLogisticBatch.SourceId = partsOutboundPlan.Id;
                newPartsLogisticBatch.SourceType = (int)DcsPartsLogisticBatchSourceType.配件出库计划;
                partsOutboundPlan.PartsLogisticBatches.Add(newPartsLogisticBatch);
                InsertToDatabase(newPartsLogisticBatch);
                new PartsLogisticBatchAch(this.DomainService).InsertPartsLogisticBatchValidate(newPartsLogisticBatch);
                var partsLogisticBatchBillDetail = new PartsLogisticBatchBillDetail {
                    PartsLogisticBatchId = newPartsLogisticBatch.Id,
                    BillId = newPartsShippingOrder.Id,
                    BillType = (int)DcsPartsLogisticBatchBillDetailBillType.配件发运单,
                    CreatorId = newPartsShippingOrder.CreatorId,
                    CreatorName = newPartsShippingOrder.CreatorName,
                    CreateTime = newPartsShippingOrder.CreateTime
                };
                newPartsShippingOrder.PartsLogisticBatchBillDetails.Add(partsLogisticBatchBillDetail);
                var partsLogisticBatchBillDetail1 = new PartsLogisticBatchBillDetail {
                    PartsLogisticBatchId = newPartsLogisticBatch.Id,
                    BillId = newPartsOutboundBill.Id,
                    BillType = (int)DcsPartsLogisticBatchBillDetailBillType.配件出库单,
                    CreatorId = newPartsOutboundBill.CreatorId,
                    CreatorName = newPartsOutboundBill.CreatorName,
                    CreateTime = newPartsOutboundBill.CreateTime
                };
                newPartsOutboundBill.PartsLogisticBatchBillDetails.Add(partsLogisticBatchBillDetail1);
                newPartsLogisticBatch.PartsLogisticBatchBillDetails.Add(partsLogisticBatchBillDetail);
                newPartsLogisticBatch.PartsLogisticBatchBillDetails.Add(partsLogisticBatchBillDetail1);
                #endregion

                var partsLogisticBatchBillDetail2 = new PartsLogisticBatchBillDetail {
                    PartsLogisticBatchId = newPartsLogisticBatch.Id,
                    BillId = newPartsInboundPlan.Id,
                    BillType = (int)DcsPartsLogisticBatchBillDetailBillType.配件入库计划,
                    CreatorId = newPartsInboundPlan.CreatorId,
                    CreatorName = newPartsInboundPlan.CreatorName,
                    CreateTime = newPartsInboundPlan.CreateTime
                };
                newPartsInboundPlan.PartsLogisticBatchBillDetails.Add(partsLogisticBatchBillDetail2);
                newPartsLogisticBatch.PartsLogisticBatchBillDetails.Add(partsLogisticBatchBillDetail2);
            } else {
                #region 调拨单赋值，并更新到数据库
                //计划价改为 成本价
                var partsPlannedPrices = ObjectContext.EnterprisePartsCosts.Where(r => r.OwnerCompanyId == partsOutboundPlan.StorageCompanyId && partsTransferDetailPartIds.Contains(r.SparePartId) && r.PartsSalesCategoryId == salesUnit.PartsSalesCategoryId).ToArray();

                //var partsPlannedPrices = ObjectContext.PartsPlannedPrices.Where(r => partsTransferDetailPartIds.Contains(r.SparePartId) && r.PartsSalesCategoryId == salesUnit.PartsSalesCategoryId).ToArray();
                foreach(var partsTransferDetail in partsTransferDetails) {
                    var partsPlannedPrice = partsPlannedPrices.FirstOrDefault(r => r.SparePartId == partsTransferDetail.SparePartId);
                    if(partsPlannedPrice != null) {
                        partsTransferDetail.PlannPrice = partsPlannedPrice.CostPrice; //配件调拨单清单 计划价赋值
                    }
                }
                var userinfo = Utils.GetCurrentUserInfo();
                partsTransferOrder.TotalPlanAmount = partsTransferDetails.Sum(r => r.PlannedAmount * r.PlannPrice);  //调拨单主单根据调拨单清单进行赋值
                partsTransferOrder.Status = (int)DcsPartsTransferOrderStatus.已审批;
                partsTransferOrder.ApproverId = userinfo.Id;
                partsTransferOrder.ApproverName = userinfo.Name;
                partsTransferOrder.ApproveTime = System.DateTime.Now;
                partsTransferOrder.ModifierId = userinfo.Id;
                partsTransferOrder.ModifierName = userinfo.Name;
                partsTransferOrder.ModifyTime = DateTime.Now;
                UpdateToDatabase(partsTransferOrder);
                //this.UpdatePartsTransferOrderValidate(partsTransferOrder);
                #endregion
            }
        }

        public void 作废配件调拨单(PartsTransferOrder partsTransferOrder) {
            var dbPartsTransferOrder = ObjectContext.PartsTransferOrders.Where(r => r.Id == partsTransferOrder.Id && r.Status == (int)DcsPartsTransferOrderStatus.新建).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbPartsTransferOrder == null)
                throw new ValidationException(ErrorStrings.PartsTransferOrder_Validation3);
            UpdateToDatabase(partsTransferOrder);
            var userinfo = Utils.GetCurrentUserInfo();
            partsTransferOrder.AbandonerId = userinfo.Id;
            partsTransferOrder.AbandonerName = userinfo.Name;
            partsTransferOrder.AbandonTime = System.DateTime.Now;
            partsTransferOrder.Status = (int)DcsPartsTransferOrderStatus.作废;
            this.UpdatePartsTransferOrderValidate(partsTransferOrder);
            if(!string.IsNullOrEmpty(partsTransferOrder.PurOrderCode)) {
                var partsPurchasePlan = ObjectContext.PartsPurchasePlan_HW.FirstOrDefault(r => r.Code == partsTransferOrder.PurOrderCode);
                if(partsPurchasePlan != null) {
                    var partsPurchasePlanDetails = ObjectContext.PartsPurchasePlanDetail_HW.Where(r => r.PartsPurchasePlanId == partsPurchasePlan.Id).ToArray();
                    foreach(var partsPurchasePlanDetail in partsPurchasePlanDetails) {
                        foreach(var partsTransferDetail in partsTransferOrder.PartsTransferOrderDetails) {
                            if(partsPurchasePlanDetail.PartId == partsTransferDetail.SparePartId) {
                                partsPurchasePlanDetail.UntFulfilledQty += (partsTransferDetail.PlannedAmount - Convert.ToInt32(partsTransferDetail.ConfirmedAmount));
                                partsPurchasePlanDetail.EndAmount = (partsPurchasePlanDetail.EndAmount ?? 0) + (partsTransferDetail.PlannedAmount - Convert.ToInt32(partsTransferDetail.ConfirmedAmount));
                                UpdateToDatabase(partsPurchasePlanDetail);
                            }
                        }
                    }
                }
            }
        }

        public void 调拨单更新采购计划清单(PartsTransferOrder partsTransferOrder) {
            if(string.IsNullOrEmpty(partsTransferOrder.PurOrderCode))
                return;
            foreach(var detail in partsTransferOrder.PartsTransferOrderDetails) {
                var partsPurchasePlanDetail_HW = (from a in ObjectContext.PartsPurchasePlanDetail_HW.Where(r => detail.SparePartId == r.PartId)
                                                  join b in ObjectContext.PartsPurchasePlan_HW.Where(r => r.Code == partsTransferOrder.PurOrderCode) on a.PartsPurchasePlanId equals b.Id
                                                  select a).FirstOrDefault();
                if(partsPurchasePlanDetail_HW == null)
                    throw new ValidationException(string.Format(ErrorStrings.PartsTransferOrder_Validation14, detail.SparePartCode));
                if(detail.PlannedAmount > (partsPurchasePlanDetail_HW.UntFulfilledQty.Value - (partsPurchasePlanDetail_HW.EndAmount ?? 0)))
                    throw new ValidationException(string.Format(ErrorStrings.PartsTransferOrder_Validation15, detail.SparePartCode));
                partsPurchasePlanDetail_HW.UntFulfilledQty -= detail.PlannedAmount;
                UpdateToDatabase(partsPurchasePlanDetail_HW);
            }
            var partsPurchasePlan_HW = ObjectContext.PartsPurchasePlan_HW.Where(r => r.Code == partsTransferOrder.PurOrderCode).FirstOrDefault();
            if(partsPurchasePlan_HW != null)
                new PartsPurchasePlan_HWAch(this.DomainService).UpdatePartsPurchasePlan_HW(partsPurchasePlan_HW);
        }



    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:11:45
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 审批配件调拨单(PartsTransferOrder partsTransferOrder) {
            new PartsTransferOrderAch(this).审批配件调拨单(partsTransferOrder);
        }

        [Update(UsingCustomMethod = true)]
        public void 调拨单更新采购计划清单(PartsTransferOrder partsTransferOrder) {
            new PartsTransferOrderAch(this).调拨单更新采购计划清单(partsTransferOrder);
        }

        public void 作废配件调拨单(PartsTransferOrder partsTransferOrder) {
            new PartsTransferOrderAch(this).作废配件调拨单(partsTransferOrder);
        }

        [Update(UsingCustomMethod = true)]
        public void 驳回配件调拨单(PartsTransferOrder partsTransferOrder) {
            new PartsTransferOrderAch(this).驳回配件调拨单(partsTransferOrder);
        }
        [Update(UsingCustomMethod = true)]
        public void 初审配件调拨单(PartsTransferOrder partsTransferOrder) {
            new PartsTransferOrderAch(this).初审配件调拨单(partsTransferOrder);
        }
    }
}
