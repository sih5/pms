﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
using System.Configuration;
using System.Data;
using System.Text;
using Devart.Data.Oracle;
using NPOI.SS.Formula.Functions;
using System.Transactions;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class PickingTaskAch : DcsSerivceAchieveBase {
        public PickingTaskAch(DcsDomainService domainService)
            : base(domainService) {
        }

        public IEnumerable<PickingTaskQuery> getPickingTaskLists(string code, int? warehouseId, int? partsSalesCategoryId, string status, string sourceCode,
            string partsOutboundPlanCode, string orderTypeName, string counterpartCompanyName, string sparePartCode, string sparePartName,
           DateTime? bCreateTime, DateTime? eCreateTime, DateTime? bPickingfinishtime, DateTime? ePickingfinishtime, int? orderTypeId) {
            var userInfo = Utils.GetCurrentUserInfo();
            string SQL = @"SELECT P.ID AS ID,P.CODE AS CODE,P.WAREHOUSECODE AS WAREHOUSECODE," +
                         "P.WAREHOUSENAME AS WAREHOUSENAME," +
                          "P.COUNTERPARTCOMPANYCODE AS COUNTERPARTCOMPANYCODE,P.COUNTERPARTCOMPANYNAME AS COUNTERPARTCOMPANYNAME,P.STATUS AS STATUS,P.CREATORID AS CREATORID,P.CREATORNAME AS CREATORNAME,P.CREATETIME AS CREATETIME," +
                          "P.MODIFIERID AS MODIFIERID,P.MODIFIERNAME AS MODIFIERNAME,P.MODIFYTIME AS MODIFYTIME,P.PICKINGFINISHTIME AS PICKINGFINISHTIME,P.ORDERTYPEID AS ORDERTYPEID," +
                          "P.ORDERTYPENAME AS ORDERTYPENAME,P.SHIPPINGMETHOD AS SHIPPINGMETHOD,P.ISEXISTSHIPPINGORDER AS ISEXISTSHIPPINGORDER,p.Remark," +
                          "c.name as PartsSalesCategoryName,p.ResponsiblePersonName as ResponsiblePersonName,p.warehouseId FROM PICKINGTASK P left join PartsSalesCategory C on p.partssalescategoryid=c.id " +
                          " where  p.BranchId=" + userInfo.EnterpriseId + "  and  exists( select 1 from WarehouseAreaManager m inner join WarehouseArea w on m.warehouseareaid = w.toplevelwarehouseareaid inner join PickingTaskDetail pd on w.id = pd.warehouseareaid  and w.status=1 where pd.pickingtaskid=p.id and  m.ManagerId = " + userInfo.Id + ")";
            SQL = this.setValue(code, warehouseId, partsSalesCategoryId, null, sourceCode, partsOutboundPlanCode, orderTypeName, counterpartCompanyName, sparePartCode, sparePartName, bCreateTime, eCreateTime, bPickingfinishtime, ePickingfinishtime, orderTypeId, SQL, status);
            SQL = SQL + " order by p.CREATETIME ";
            var search = ObjectContext.ExecuteStoreQuery<PickingTaskQuery>(SQL).ToList();
            return search;
        }
        private string setValue(string code, int? warehouseId, int? partsSalesCategoryId, int? status, string sourceCode,
                                string partsOutboundPlanCode, string orderTypeName, string counterpartCompanyName, string sparePartCode, string sparePartName,
                                DateTime? bCreateTime, DateTime? eCreateTime, DateTime? bPickingfinishtime, DateTime? ePickingfinishtime, int? orderTypeId, string SQL, string statusStr) {
            if(!string.IsNullOrEmpty(code)) {
                SQL = SQL + " and p.code like '%" + code + "%'";
            }
            if(warehouseId.HasValue) {
                SQL = SQL + " and p.warehouseId=" + warehouseId.Value;
            }
            if(partsSalesCategoryId.HasValue) {
                SQL = SQL + " and p.PartsSalesCategoryId=" + partsSalesCategoryId.Value;
            }
            if(status.HasValue) {
                SQL = SQL + " and p.status=" + status.Value;
            }
            if(!string.IsNullOrEmpty(sourceCode)) {
                SQL = SQL + " and  exists (select 1 from PickingTaskDetail s where sourceCode  ='" + sourceCode + "'  and s.pickingtaskid=p.id )";
            }
            if(!string.IsNullOrEmpty(partsOutboundPlanCode)) {
                SQL = SQL + " and exists(select 1 from PickingTaskDetail s where PartsOutboundPlanCode = '" + partsOutboundPlanCode + "'  and s.pickingtaskid=p.id )";

            }

            if(!string.IsNullOrEmpty(orderTypeName)) {
                SQL = SQL + " and p.OrderTypeName like '%" + orderTypeName + "%'";
            }
            if(!string.IsNullOrEmpty(counterpartCompanyName)) {
                SQL = SQL + " and p.CounterpartCompanyName like '%" + counterpartCompanyName + "%'";
            }
            if(!string.IsNullOrEmpty(sparePartCode)) {
                SQL = SQL + " and exists(select 1 from PickingTaskDetail s where sparePartCode  ='" + sparePartCode + "' and s.pickingtaskid=p.id )";

            }
            if(!string.IsNullOrEmpty(sparePartName)) {
                SQL = SQL + " and exists(select 1 from PickingTaskDetail s where sparePartName like '%" + sparePartName + "%'  and s.pickingtaskid=p.id )";

            }

            if(bCreateTime.HasValue) {
                SQL = SQL + " and p.CreateTime>= to_date('" + bCreateTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }
            if(eCreateTime.HasValue) {
                SQL = SQL + " and p.CreateTime<= to_date('" + eCreateTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }

            if(bPickingfinishtime.HasValue) {
                SQL = SQL + " and p.Pickingfinishtime>= to_date('" + bPickingfinishtime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }
            if(ePickingfinishtime.HasValue) {
                SQL = SQL + " and p.Pickingfinishtime<= to_date('" + ePickingfinishtime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }
            if(orderTypeId.HasValue) {
                SQL = SQL + " and p.OrderTypeId=" + orderTypeId.Value;
            }
            if(!string.IsNullOrEmpty(statusStr)) {
                SQL = SQL + " and p.status in (" + statusStr + ")";
            }
            return SQL;
        }
        public IEnumerable<PickingTaskQuery> getPickingTaskListsAll(string code, int? warehouseId, int? partsSalesCategoryId, int? status, string sourceCode,
          string partsOutboundPlanCode, string orderTypeName, string counterpartCompanyName,
         DateTime? bCreateTime, DateTime? eCreateTime, DateTime? bPickingfinishtime, DateTime? ePickingfinishtime) {
            var userInfo = Utils.GetCurrentUserInfo();
            string SQL = @"SELECT P.ID AS ID,P.CODE AS CODE,P.WAREHOUSECODE AS WAREHOUSECODE," +
                         "P.WAREHOUSENAME AS WAREHOUSENAME," +
                          "P.COUNTERPARTCOMPANYCODE AS COUNTERPARTCOMPANYCODE,P.COUNTERPARTCOMPANYNAME AS COUNTERPARTCOMPANYNAME,P.STATUS AS STATUS,P.CREATORID AS CREATORID,P.CREATORNAME AS CREATORNAME,P.CREATETIME AS CREATETIME," +
                          "P.MODIFIERID AS MODIFIERID,P.MODIFIERNAME AS MODIFIERNAME,P.MODIFYTIME AS MODIFYTIME,P.PICKINGFINISHTIME AS PICKINGFINISHTIME,P.ORDERTYPEID AS ORDERTYPEID," +
                          "P.ORDERTYPENAME AS ORDERTYPENAME,P.SHIPPINGMETHOD AS SHIPPINGMETHOD,P.ISEXISTSHIPPINGORDER AS ISEXISTSHIPPINGORDER," +
                          "c.name as PartsSalesCategoryName,p.ResponsiblePersonName as ResponsiblePersonName FROM PICKINGTASK P left join PartsSalesCategory C on p.partssalescategoryid=c.id  where p.BranchId=" + userInfo.EnterpriseId;
            SQL = this.setValue(code, warehouseId, partsSalesCategoryId, status, sourceCode, partsOutboundPlanCode, orderTypeName, counterpartCompanyName, null, null, bCreateTime, eCreateTime, bPickingfinishtime, ePickingfinishtime, null, SQL, null);
            var search = ObjectContext.ExecuteStoreQuery<PickingTaskQuery>(SQL).ToList();
            return search;
        }
        public PickingTask GetPickingTaskWithsDetails(int id) {
            var userInfo = Utils.GetCurrentUserInfo();
            var pickingTask = ObjectContext.PickingTasks.SingleOrDefault(e => e.Id == id);
            if(pickingTask != null) {
                var details = ObjectContext.PickingTaskDetails.Include("SparePart").Where(ex => ex.PickingTaskId == id && ObjectContext.WarehouseAreas.Any(area => area.WarehouseId == pickingTask.WarehouseId && area.Status == (int)DcsBaseDataStatus.有效
                    && ObjectContext.WarehouseAreaManagers.Any(manager => manager.WarehouseAreaId == area.ParentId && manager.ManagerId == userInfo.Id))).ToArray();
            }
            return pickingTask;
        }
        public void 拣货(VirtualPickingTaskDetail[] pickingDetails, int id, string remark) {
            using(var transaction = new TransactionScope()) {
                //查询所有的拣货清单信息
                var oldPickingList = ObjectContext.PickingTaskDetails.Where(r => r.PickingTaskId == id).ToArray();
                foreach(var item in oldPickingList) {
                    var newPick = pickingDetails.Where(r => r.Id == item.Id).FirstOrDefault();
                    if(null != newPick) {
                        item.CourrentPicking = newPick.CourrentPicking == null ? 0 : newPick.CourrentPicking;
                    }
                }
                //判断已拣货量是否等于计划量，若等于，则为拣货完成
                Boolean isPickingOver = false;
                foreach(var item in oldPickingList) {
                    if(null == item.CourrentPicking) {
                        item.CourrentPicking = 0;
                    }
                    if(null == item.PickingQty) {
                        item.PickingQty = 0;
                    }
                    if(item.PlanQty > (item.CourrentPicking + item.PickingQty)) {
                        isPickingOver = true;
                        break;
                    }
                }
                var storageCompanyId = 0;
                var storageCompanyType = 0;
                if(isPickingOver) {
                    //更新主单
                    var userInfo = Utils.GetCurrentUserInfo();
                    var pickingTask = ObjectContext.PickingTasks.Where(r => r.Id == id && (r.Status == (int)DcsPickingTaskStatus.部分拣货 || r.Status == (int)DcsPickingTaskStatus.新建)).FirstOrDefault();
                    if(pickingTask == null) {
                        throw new ValidationException(ErrorStrings.PickingTask_Validation3);
                    }
                    pickingTask.Status = (int)DcsPickingTaskStatus.部分拣货;
                    pickingTask.ModifierId = userInfo.Id;
                    pickingTask.ModifierName = userInfo.Name;
                    pickingTask.ModifyTime = DateTime.Now;
                    pickingTask.Remark = remark;
                    UpdateToDatabase(pickingTask);
                    //先查询是否已有物流批次单，若有则新增明细，没有则新增
                    var partsLogisticBatch = ObjectContext.PartsLogisticBatches.Where(r => r.CompanyId == userInfo.EnterpriseId && r.SourceId == pickingTask.Id && r.Status == (int)DcsPartsLogisticBatchSourceType.配件出库计划).FirstOrDefault();
                    PartsLogisticBatch batch = new PartsLogisticBatch() {
                        CompanyId = userInfo.EnterpriseId,
                        Status = (int)DcsPartsLogisticBatchStatus.新增,
                        ShippingStatus = (int)DcsPartsLogisticBatchShippingStatus.待运,
                        SourceId = pickingTask.Id,
                        SourceType = (int)DcsPartsLogisticBatchSourceType.配件出库计划,
                        CreateTime = DateTime.Now,
                        CreatorId = userInfo.Id,
                        CreatorName = userInfo.Name
                    };
                    Boolean isInsert = false;
                    //入库
                    var tracOld = ObjectContext.AccurateTraces.Where(t => t.Status == (int)DCSAccurateTraceStatus.有效 && t.CompanyId == userInfo.EnterpriseId && t.Type == (int)DCSAccurateTraceType.入库 && t.SIHLabelCode != null && (t.OutQty == null || t.InQty != t.OutQty) && t.InQty>0).ToArray();
                    //出库
                    var tracOldOut = ObjectContext.AccurateTraces.Where(t => t.Status == (int)DCSAccurateTraceStatus.有效 && t.CompanyId == userInfo.EnterpriseId && t.Type == (int)DCSAccurateTraceType.出库 && t.SIHLabelCode != null).ToArray();
                    foreach(var item in pickingDetails) {
                        if(null == item.CourrentPicking) {
                            item.CourrentPicking = 0;
                        }
                        if(item.CourrentPicking > 0) {
                            //新增配件出库实绩
                            PartsOutboundPerformance formance = new PartsOutboundPerformance() {
                                OutType = (int)DcsPartsOutboundPer_OutType.拣货,
                                WarehouseAreaId = item.WarehouseAreaId,
                                Qty = item.CourrentPicking,
                                OperaterId = userInfo.Id,
                                OperaterName = userInfo.Name,
                                Equipment = (int)DcsPartsOutboundPer_EquipmentType.PC,
                                OperaterEndTime = DateTime.Now,
                                Code = pickingTask.Code,
                                SparePartId = item.SparePartId,
                                SpareCode = item.SparePartCode,
                                SpareName = item.SparePartName
                            };
                            InsertToDatabase(formance);

                            //根据货位ID+配件ID查询库存信息
                            //更新库存
                            var partsStock = new PartsStockAch(this.DomainService).GetPartsStocksWithLock((from a in ObjectContext.PartsStocks
                                                                                                           where a.WarehouseId == pickingTask.WarehouseId && a.PartId == item.SparePartId && a.WarehouseAreaId == item.WarehouseAreaId
                                                                                                           select a)).FirstOrDefault();
                            if(null == partsStock) {
                                throw new ValidationException(item.SparePartCode + ErrorStrings.PartsOutboundPlan_Validation8);
                            }
                            if(null == partsStock.LockedQty) {
                                partsStock.LockedQty = 0;
                            }
                            if(Convert.ToInt32(item.CourrentPicking) > partsStock.Quantity || item.CourrentPicking > partsStock.LockedQty) {
                                throw new ValidationException(item.SparePartCode + ErrorStrings.PickingTask_Validation4);
                            }
                            if(storageCompanyId == 0) {
                                storageCompanyId = partsStock.StorageCompanyId;

                            }
                            partsStock.Quantity = partsStock.Quantity - Convert.ToInt32(item.CourrentPicking);
                            partsStock.LockedQty = partsStock.LockedQty - Convert.ToInt32(item.CourrentPicking);
                            partsStock.ModifierId = userInfo.Id;
                            partsStock.ModifierName = userInfo.Name;
                            partsStock.ModifyTime = DateTime.Now;
                            UpdateToDatabase(partsStock);
                            //更新配件锁定库存
                            this.updatePartsLockedStock(item.SparePartId, pickingTask.WarehouseId.Value, item.CourrentPicking.Value);
                            if(null != item.BatchNumber && !"".Equals(item.BatchNumber)) {
                                var warehouse = ObjectContext.Warehouses.Where(r => r.Id == pickingTask.WarehouseId).FirstOrDefault();


                                PartsLogisticBatchItemDetail detail = new PartsLogisticBatchItemDetail() {
                                    CounterpartCompanyId = Convert.ToInt32(pickingTask.CounterpartCompanyId),
                                    ReceivingCompanyId = Convert.ToInt32(pickingTask.CounterpartCompanyId),
                                    ShippingCompanyId = warehouse.StorageCompanyId,
                                    SparePartId = item.SparePartId,
                                    SparePartCode = item.SparePartCode,
                                    SparePartName = item.SparePartName,
                                    BatchNumber = item.BatchNumber,
                                    OutboundAmount = Convert.ToInt32(item.CourrentPicking),
                                    InboundAmount = 0,
                                    OutReturnAmount = 0

                                };
                                if(null == partsLogisticBatch) {
                                    isInsert = true;
                                    batch.PartsLogisticBatchItemDetails.Add(detail);
                                } else {
                                    detail.PartsLogisticBatchId = partsLogisticBatch.Id;
                                    InsertToDatabase(detail);

                                }
                            }
                            storageCompanyType = partsStock.StorageCompanyType;
                            if(partsStock.StorageCompanyType == (int)DcsCompanyType.代理库) {
                                storageCompanyType = (int)DCSAccurateTraceCompanyType.中心库;
                            }
                            //增加出库追溯清单
                            //    item.TraceProperty.HasValue &&
                            if(!string.IsNullOrEmpty(item.TraceCode)) {
                                var traceCode = item.TraceCode.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries).Distinct().ToArray();
                                if(traceCode.Count() > 0) {
                                    if(!item.TraceProperty.HasValue) {
                                        item.TraceProperty = 0;
                                    }
                                    var picking = 0;
                                    foreach(var code in traceCode) {
                                        var olds = tracOld.Where(t => t.SIHLabelCode == code && t.PartId == item.SparePartId && t.CompanyId == userInfo.EnterpriseId && t.Status == (int)DCSAccurateTraceStatus.有效 && t.Type == (int)DCSAccurateTraceType.入库).ToArray();
                                        if(code.EndsWith("X"))
                                            olds = tracOld.Where(t => t.BoxCode == code && t.PartId == item.SparePartId && t.CompanyId == userInfo.EnterpriseId && t.Status == (int)DCSAccurateTraceStatus.有效 && t.Type == (int)DCSAccurateTraceType.入库).ToArray();
                                        if(olds != null && olds.Count() > 0) {
                                            foreach(var old in olds) {
                                                //判断入库的数量是否已全部出库
                                                //if((old.InQty ?? 0) == (old.OutQty ?? 0) && (old.InQty ?? 0) > 0) {
                                                //    throw new ValidationException("标签码:" + code + "已全部出库");
                                                //}
                                                if(picking == item.CourrentPicking.Value) {
                                                    continue;
                                                }
                                                var pickQty = 0;
                                                var inQty = 0;
                                                if(item.TraceProperty.Value == (int)DCSTraceProperty.精确追溯) {
                                                    pickQty = 1;
                                                    inQty = 1;
                                                    picking = picking + 1;
                                                } else {
                                                    if((old.InQty ?? 0) - (old.OutQty ?? 0) >= item.CourrentPicking.Value) {
                                                        pickQty = item.CourrentPicking.Value;
                                                        inQty = old.InQty.Value;
                                                        picking = picking + item.CourrentPicking.Value;
                                                    } else {
                                                        pickQty = (old.InQty ?? 0) - (old.OutQty ?? 0);
                                                        inQty = old.InQty.Value;
                                                        picking = picking + ((old.InQty ?? 0) - (old.OutQty ?? 0));
                                                    }
                                                }
                                                if(old.TraceProperty.HasValue && old.TraceProperty.Value == (int)DCSTraceProperty.精确追溯 && !string.IsNullOrEmpty(old.TraceCode)) {
                                                    var outTraces = tracOldOut.Where(t => t.SourceBillId == item.PartsOutboundPlanId && t.TraceCode == old.TraceCode).FirstOrDefault();
                                                    if(outTraces != null) {
                                                        outTraces.OutQty = outTraces.OutQty + pickQty;
                                                        outTraces.ModifierId = userInfo.Id;
                                                        outTraces.ModifierName = userInfo.Name;
                                                        outTraces.ModifyTime = DateTime.Now;
                                                    } else {
                                                        var newAccurateTrace = new AccurateTrace {
                                                            Type = (int)DCSAccurateTraceType.出库,
                                                            CompanyId = storageCompanyId,
                                                            CompanyType = storageCompanyType,
                                                            SourceBillId = item.PartsOutboundPlanId,
                                                            PartId = item.SparePartId,
                                                            TraceCode = old.TraceCode,
                                                            SIHLabelCode = old.SIHLabelCode,
                                                            Status = (int)DCSAccurateTraceStatus.有效,
                                                            TraceProperty = item.TraceProperty,
                                                            BoxCode = old.BoxCode,
                                                            InQty = inQty,
                                                            OutQty = pickQty
                                                        };
                                                        new AccurateTraceAch(this.DomainService).ValidataAccurateTrace(newAccurateTrace);
                                                    }

                                                } else if(!old.TraceProperty.HasValue || string.IsNullOrEmpty(old.TraceCode) || old.TraceProperty.Value == (int)DCSTraceProperty.批次追溯) {
                                                    var outTraces = tracOldOut.Where(t => t.SourceBillId == item.PartsOutboundPlanId && t.SIHLabelCode == old.SIHLabelCode).FirstOrDefault();
                                                    if(outTraces != null) {
                                                        outTraces.OutQty = outTraces.OutQty + pickQty;
                                                        outTraces.ModifierId = userInfo.Id;
                                                        outTraces.ModifierName = userInfo.Name;
                                                        outTraces.ModifyTime = DateTime.Now;
                                                    } else {
                                                        var newAccurateTrace = new AccurateTrace {
                                                            Type = (int)DCSAccurateTraceType.出库,
                                                            CompanyId = storageCompanyId,
                                                            CompanyType = storageCompanyType,
                                                            SourceBillId = item.PartsOutboundPlanId,
                                                            PartId = item.SparePartId,
                                                            TraceCode = old.TraceCode,
                                                            SIHLabelCode = old.SIHLabelCode,
                                                            Status = (int)DCSAccurateTraceStatus.有效,
                                                            TraceProperty = item.TraceProperty,
                                                            BoxCode = old.BoxCode,
                                                            InQty = inQty,
                                                            OutQty = pickQty
                                                        };
                                                        new AccurateTraceAch(this.DomainService).ValidataAccurateTrace(newAccurateTrace);
                                                    }

                                                }
                                                //作废之前的出库追溯信息
                                                if(item.TraceProperty.Value == (int)DCSTraceProperty.精确追溯) {
                                                    var outTraceLs = tracOldOut.Where(t => t.SourceBillId != item.PartsOutboundPlanId && t.TraceCode == old.TraceCode && t.CompanyId == userInfo.EnterpriseId && t.Status == (int)DCSAccurateTraceStatus.有效).ToArray();
                                                    foreach(var outTrace in outTraceLs) {
                                                        outTrace.ModifierId = userInfo.Id;
                                                        outTrace.ModifierName = userInfo.Name;
                                                        outTrace.ModifyTime = DateTime.Now;
                                                        outTrace.Status = (int)DCSAccurateTraceStatus.作废;
                                                        UpdateToDatabase(outTrace);
                                                    }
                                                }
                                                //增加入库追溯信息中的出库数量
                                                old.OutQty = (old.OutQty ?? 0) + pickQty;
                                                old.ModifierId = userInfo.Id;
                                                old.ModifierName = userInfo.Name;
                                                old.ModifyTime = DateTime.Now;
                                                UpdateToDatabase(old);
                                            }
                                        }
                                    }
                                }
                            }

                        }
                        //更新已拣货量
                        if(null == item.PickingQty) {
                            item.PickingQty = 0;
                        }
                        var pickDetail = ObjectContext.PickingTaskDetails.Where(r => r.Id == item.Id).FirstOrDefault();
                        if(null == pickDetail.PickingQty) {
                            pickDetail.PickingQty = 0;
                        }
                        if(pickDetail.PlanQty < (pickDetail.PickingQty + item.CourrentPicking)) {
                            throw new ValidationException(ErrorStrings.PickingTask_Validation5);
                        }
                        pickDetail.PickingQty += item.CourrentPicking;
                        pickDetail.ShortPickingReason = item.ShortPickingReason;
                        //拣货保存时候校验，本次拣货量+已拣货量=计划量的不能选择短拣原因。
                        if(pickDetail.PickingQty == pickDetail.PlanQty) {
                            pickDetail.ShortPickingReason = null;
                        }
                        UpdateToDatabase(pickDetail);
                        ObjectContext.SaveChanges();
                    }
                    if(isInsert) {
                        InsertToDatabase(batch);
                    }
                    //库区信息
                    var warehouseArea = ObjectContext.WarehouseAreas.Where(r => r.Code == "X1-01-01-01" && r.Status == (int)DcsBaseDataStatus.有效 && r.AreaKind == (int)DcsAreaKind.库位 && r.WarehouseId == pickingTask.WarehouseId).FirstOrDefault();
                    if(null == warehouseArea) {
                        throw new ValidationException(string.Format(ErrorStrings.PickingTask_Validation6, pickingTask.WarehouseCode));
                    }
                    var warehouseAreaCategory = ObjectContext.WarehouseAreaCategories.Where(r => r.Category == (int)DcsAreaType.待发区).FirstOrDefault();
                    //更新待发区库存
                    var spId = pickingDetails.Select(r => r.SparePartId).Distinct().ToArray();
                    var partsStockDfs = new PartsStockAch(this.DomainService).GetPartsStocksWithLock((from a in ObjectContext.PartsStocks
                                                                                                      join b in ObjectContext.WarehouseAreas on a.WarehouseAreaId equals b.Id
                                                                                                      where a.WarehouseId == pickingTask.WarehouseId && spId.Contains(a.PartId) && b.Code == "X1-01-01-01" && b.Status == (int)DcsBaseDataStatus.有效
                                                                                                       && b.AreaKind == (int)DcsAreaKind.库位
                                                                                                      select a)).ToList();
                    foreach(var pid in spId) {
                        //需要更新的数量
                        var sumQty = pickingDetails.Where(r => r.SparePartId == pid).Sum(s => s.CourrentPicking);
                        if(sumQty.Value > 0) {
                            //先查询是否已经有待发区库存，不存在则新增
                            var oldStock = partsStockDfs.Where(r => r.PartId == pid).FirstOrDefault();
                            if(null != oldStock) {
                                if(null == oldStock.LockedQty) {
                                    oldStock.LockedQty = 0;
                                }
                                oldStock.Quantity = oldStock.Quantity + sumQty.Value;
                                oldStock.LockedQty = oldStock.LockedQty + sumQty.Value;
                                oldStock.ModifierId = userInfo.Id;
                                oldStock.ModifierName = userInfo.Name;
                                oldStock.ModifyTime = DateTime.Now;
                                UpdateToDatabase(oldStock);
                            } else {
                                var newPartsStock = new PartsStock() {
                                    WarehouseId = Convert.ToInt32(pickingTask.WarehouseId),
                                    StorageCompanyId = storageCompanyId,
                                    StorageCompanyType = storageCompanyType,
                                    BranchId = pickingTask.BranchId.Value,
                                    WarehouseAreaId = warehouseArea.Id,
                                    WarehouseAreaCategoryId = warehouseAreaCategory.Id,
                                    PartId = pid,
                                    Quantity = sumQty.Value,
                                    LockedQty = sumQty.Value,
                                    CreateTime = DateTime.Now,
                                    CreatorId = userInfo.Id,
                                    CreatorName = userInfo.Name

                                };
                                InsertToDatabase(newPartsStock);
                            }
                        }
                    }
                    ObjectContext.SaveChanges();
                    //更新出库计划单
                    var outPlanIds = pickingDetails.Select(r => r.PartsOutboundPlanId).Distinct().ToArray();
                    foreach(var item in outPlanIds) {
                        var outplan = ObjectContext.PartsOutboundPlans.Where(r => r.Id == item).FirstOrDefault();
                        new PartsOutboundPlanAch(this.DomainService).更新出库计划单状态(outplan);
                    }
                } else {
                    this.完成拣货(pickingDetails, id, remark);
                }
                transaction.Complete();
            }
        }
        public void 完成拣货(VirtualPickingTaskDetail[] pickingDetails, int id, string remark) {
            using(var transaction = new TransactionScope()) {
                var oldPickingList = ObjectContext.PickingTaskDetails.Where(r => r.PickingTaskId == id).ToArray();
                foreach(var item in oldPickingList) {
                    var newPick = pickingDetails.Where(r => r.Id == item.Id).FirstOrDefault();
                    if(null != newPick) {
                        item.CourrentPicking = newPick.CourrentPicking == null ? 0 : newPick.CourrentPicking;
                        item.ShortPickingReason = newPick.ShortPickingReason;
                    }
                }
                //判断是否所有的拣货量小于计划量的都填写了短拣原因
                foreach(var item in oldPickingList) {
                    if(null == item.CourrentPicking) {
                        item.CourrentPicking = 0;
                    }
                    if(null == item.PickingQty) {
                        item.PickingQty = 0;
                    }
                    if(item.PlanQty > (item.CourrentPicking + item.PickingQty) && item.ShortPickingReason == null) {
                        throw new ValidationException(item.WarehouseAreaCode + ErrorStrings.PickingTask_Validation7);
                    }
                }
                //判断是否存在，当前检验量+ 已检验量 大于0的数据，如果不存在则当前检验单一个配件都没有拣货
                var pickingQtyIsNull = oldPickingList.Any(o => (o.CourrentPicking + o.PickingQty) > 0);

                //更新主单
                var userInfo = Utils.GetCurrentUserInfo();
                var pickingTask = ObjectContext.PickingTasks.Where(r => r.Id == id && (r.Status == (int)DcsPickingTaskStatus.部分拣货 || r.Status == (int)DcsPickingTaskStatus.新建)).FirstOrDefault();
                if(pickingTask == null) {
                    throw new ValidationException(ErrorStrings.PickingTask_Validation8);
                }
                pickingTask.Status = pickingQtyIsNull ? (int)DcsPickingTaskStatus.拣货完成 : (int)DcsPickingTaskStatus.已生成装箱任务;
                pickingTask.IsExistShippingOrder = pickingQtyIsNull ? false : true;
                pickingTask.ModifierId = userInfo.Id;
                pickingTask.ModifierName = userInfo.Name;
                pickingTask.ModifyTime = DateTime.Now;
                pickingTask.PickingFinishTime = DateTime.Now;
                pickingTask.Remark = remark;
                UpdateToDatabase(pickingTask);
                var storageCompanyId = 0;
                var storageCompanyType = 0;
                //先查询是否已有物流批次单，若有则新增明细，没有则新增
                var partsLogisticBatch = ObjectContext.PartsLogisticBatches.Where(r => r.CompanyId == userInfo.EnterpriseId && r.SourceId == pickingTask.Id && r.Status == (int)DcsPartsLogisticBatchSourceType.配件出库计划).FirstOrDefault();
                PartsLogisticBatch batch = new PartsLogisticBatch() {
                    CompanyId = userInfo.EnterpriseId,
                    Status = (int)DcsPartsLogisticBatchStatus.新增,
                    ShippingStatus = (int)DcsPartsLogisticBatchShippingStatus.待运,
                    SourceId = pickingTask.Id,
                    SourceType = (int)DcsPartsLogisticBatchSourceType.配件出库计划,
                    CreateTime = DateTime.Now,
                    CreatorId = userInfo.Id,
                    CreatorName = userInfo.Name
                };
                Boolean isInsert = false;
                //入库
                var tracOld = ObjectContext.AccurateTraces.Where(t => t.Status == (int)DCSAccurateTraceStatus.有效 && t.CompanyId == userInfo.EnterpriseId && t.Type == (int)DCSAccurateTraceType.入库 && t.SIHLabelCode != null && (t.OutQty == null || t.InQty != t.OutQty)).ToArray();
                //出库
                var tracOldOut = ObjectContext.AccurateTraces.Where(t => t.Status == (int)DCSAccurateTraceStatus.有效 && t.CompanyId == userInfo.EnterpriseId && t.Type == (int)DCSAccurateTraceType.出库 && t.SIHLabelCode != null).ToArray();
                foreach(var item in oldPickingList.Where(r => r.PlanQty != r.PickingQty)) {
                    if(item.CourrentPicking > 0) {
                        //新增配件出库实绩
                        PartsOutboundPerformance formance = new PartsOutboundPerformance() {
                            OutType = (int)DcsPartsOutboundPer_OutType.拣货,
                            WarehouseAreaId = item.WarehouseAreaId,
                            Qty = item.CourrentPicking,
                            OperaterId = userInfo.Id,
                            OperaterName = userInfo.Name,
                            Equipment = (int)DcsPartsOutboundPer_EquipmentType.PC,
                            OperaterEndTime = DateTime.Now,
                            Code = pickingTask.Code,
                            SparePartId = item.SparePartId,
                            SpareCode = item.SparePartCode,
                            SpareName = item.SparePartName
                        };
                        InsertToDatabase(formance);
                    }
                    //根据货位ID+配件ID+库位ID 查询库存信息
                    //更新库存
                    var partsStock = new PartsStockAch(this.DomainService).GetPartsStocksWithLock((from a in ObjectContext.PartsStocks
                                                                                                   where a.WarehouseId == pickingTask.WarehouseId && a.PartId == item.SparePartId && a.WarehouseAreaId == item.WarehouseAreaId
                                                                                                   select a)).FirstOrDefault();
                    if(null == partsStock) {
                        throw new ValidationException(item.SparePartCode + ErrorStrings.PartsOutboundPlan_Validation8);
                    }
                    if(null == partsStock.LockedQty) {
                        partsStock.LockedQty = 0;
                    }
                    int unlock = (Convert.ToInt32(item.PlanQty) - Convert.ToInt32(item.PickingQty));
                    if(Convert.ToInt32(item.CourrentPicking) > partsStock.Quantity || unlock > partsStock.LockedQty) {
                        throw new ValidationException(item.SparePartCode + ErrorStrings.PickingTask_Validation4);
                    }
                    if(storageCompanyId == 0) {
                        storageCompanyId = partsStock.StorageCompanyId;
                        storageCompanyType = partsStock.StorageCompanyType;
                    }
                    partsStock.Quantity = partsStock.Quantity - Convert.ToInt32(item.CourrentPicking);
                    partsStock.LockedQty = partsStock.LockedQty - Convert.ToInt32(unlock);
                    partsStock.ModifierId = userInfo.Id;
                    partsStock.ModifierName = userInfo.Name;
                    partsStock.ModifyTime = DateTime.Now;
                    UpdateToDatabase(partsStock);
                    //更新配件锁定库存
                    this.updatePartsLockedStock(item.SparePartId, pickingTask.WarehouseId.Value, unlock);

                    if(item.CourrentPicking > 0) {
                        if(null != item.BatchNumber && !"".Equals(item.BatchNumber)) {
                            var warehouse = ObjectContext.Warehouses.Where(r => r.Id == pickingTask.WarehouseId).FirstOrDefault();
                            PartsLogisticBatchItemDetail detail = new PartsLogisticBatchItemDetail() {
                                CounterpartCompanyId = Convert.ToInt32(pickingTask.CounterpartCompanyId),
                                ReceivingCompanyId = Convert.ToInt32(pickingTask.CounterpartCompanyId),
                                ShippingCompanyId = warehouse.StorageCompanyId,
                                SparePartId = item.SparePartId,
                                SparePartCode = item.SparePartCode,
                                SparePartName = item.SparePartName,
                                BatchNumber = item.BatchNumber,
                                OutboundAmount = Convert.ToInt32(item.CourrentPicking),
                                InboundAmount = 0,
                                OutReturnAmount = 0

                            };
                            if(null == partsLogisticBatch) {
                                isInsert = true;
                                batch.PartsLogisticBatchItemDetails.Add(detail);
                            } else {
                                detail.PartsLogisticBatchId = partsLogisticBatch.Id;
                                InsertToDatabase(detail);
                            }
                        }
                        storageCompanyType = partsStock.StorageCompanyType;
                        if(partsStock.StorageCompanyType == (int)DcsCompanyType.代理库) {
                            storageCompanyType = (int)DCSAccurateTraceCompanyType.中心库;
                        }
                        //增加出库追溯清单
                        //配件中心只允许一次出库，中心库暂时不做限制
                        var newDetai = pickingDetails.Where(t => t.SparePartId == item.SparePartId && t.Id == item.Id).First();
                        if(!string.IsNullOrEmpty(newDetai.TraceCode)) {
                            if(!newDetai.TraceProperty.HasValue) {
                                newDetai.TraceProperty = 0;
                            }
                            var traceCode = newDetai.TraceCode.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries).Distinct().ToArray();
                            if(traceCode.Count() > 0) {
                                var picking = 0;
                                foreach(var code in traceCode) {
                                    var olds = tracOld.Where(t => t.SIHLabelCode == code && t.PartId == newDetai.SparePartId).ToArray();
                                    if(code.EndsWith("X"))
                                        olds = tracOld.Where(t => t.BoxCode == code && t.PartId == newDetai.SparePartId).ToArray();
                                    if(olds != null && olds.Count() > 0) {
                                        foreach(var old in olds) {
                                            //判断入库的数量是否已全部出库
                                            //if((old.InQty ?? 0) == (old.OutQty ?? 0) && (old.InQty ?? 0) > 0) {
                                            //    throw new ValidationException("标签码:" + code + "已全部出库");
                                            //}
                                            if(picking == item.CourrentPicking.Value) {
                                                continue;
                                            }
                                            var pickQty = 0;
                                            var inQty = 0;
                                            if(newDetai.TraceProperty.Value == (int)DCSTraceProperty.精确追溯) {
                                                pickQty = 1;
                                                inQty = 1;
                                                picking = picking + 1;
                                            } else {
                                                if((old.InQty ?? 0) - (old.OutQty ?? 0) >= item.CourrentPicking.Value) {
                                                    pickQty = item.CourrentPicking.Value;
                                                    inQty = old.InQty.Value;
                                                    picking = picking + item.CourrentPicking.Value;
                                                } else {
                                                    pickQty = (old.InQty ?? 0) - (old.OutQty ?? 0);
                                                    inQty = old.InQty.Value;
                                                    picking = picking + ((old.InQty ?? 0) - (old.OutQty ?? 0));
                                                }
                                            }
                                            if(old.TraceProperty.HasValue && old.TraceProperty.Value == (int)DCSTraceProperty.精确追溯 && !string.IsNullOrEmpty(old.TraceCode)) {
                                                var outTraces = tracOldOut.Where(t => t.SourceBillId == item.PartsOutboundPlanId && t.TraceCode == old.TraceCode).FirstOrDefault();
                                                if(outTraces != null) {
                                                    outTraces.OutQty = outTraces.OutQty + pickQty;
                                                    outTraces.ModifierId = userInfo.Id;
                                                    outTraces.ModifierName = userInfo.Name;
                                                    outTraces.ModifyTime = DateTime.Now;
                                                } else {
                                                    var newAccurateTrace = new AccurateTrace {
                                                        Type = (int)DCSAccurateTraceType.出库,
                                                        CompanyId = storageCompanyId,
                                                        CompanyType = storageCompanyType,
                                                        SourceBillId = newDetai.PartsOutboundPlanId,
                                                        PartId = newDetai.SparePartId,
                                                        TraceCode = old.TraceCode,
                                                        SIHLabelCode = old.SIHLabelCode,
                                                        Status = (int)DCSAccurateTraceStatus.有效,
                                                        TraceProperty = newDetai.TraceProperty,
                                                        BoxCode = old.BoxCode,
                                                        InQty = inQty,
                                                        OutQty = pickQty
                                                    };
                                                    new AccurateTraceAch(this.DomainService).ValidataAccurateTrace(newAccurateTrace);
                                                }


                                            } else if(!old.TraceProperty.HasValue || string.IsNullOrEmpty(old.TraceCode) || old.TraceProperty.Value == (int)DCSTraceProperty.批次追溯) {
                                                var outTraces = tracOldOut.Where(t => t.SourceBillId == item.PartsOutboundPlanId && t.SIHLabelCode == old.SIHLabelCode).FirstOrDefault();
                                                if(outTraces != null) {
                                                    outTraces.OutQty = outTraces.OutQty + pickQty;
                                                    outTraces.ModifierId = userInfo.Id;
                                                    outTraces.ModifierName = userInfo.Name;
                                                    outTraces.ModifyTime = DateTime.Now;
                                                } else {
                                                    var newAccurateTrace = new AccurateTrace {
                                                        Type = (int)DCSAccurateTraceType.出库,
                                                        CompanyId = storageCompanyId,
                                                        CompanyType = storageCompanyType,
                                                        SourceBillId = newDetai.PartsOutboundPlanId,
                                                        PartId = newDetai.SparePartId,
                                                        TraceCode = old.TraceCode,
                                                        SIHLabelCode = old.SIHLabelCode,
                                                        Status = (int)DCSAccurateTraceStatus.有效,
                                                        TraceProperty = newDetai.TraceProperty,
                                                        BoxCode = old.BoxCode,
                                                        InQty = inQty,
                                                        OutQty = pickQty
                                                    };
                                                    new AccurateTraceAch(this.DomainService).ValidataAccurateTrace(newAccurateTrace);
                                                }
                                            }
                                            //作废之前的出库追溯信息
                                            if(newDetai.TraceProperty.Value == (int)DCSTraceProperty.精确追溯) {
                                                var outTraceLs = tracOldOut.Where(t => t.SourceBillId != newDetai.PartsOutboundPlanId && t.TraceCode == old.TraceCode && t.CompanyId == userInfo.EnterpriseId && t.Status == (int)DCSAccurateTraceStatus.有效).ToArray();
                                                foreach(var outTrace in outTraceLs) {
                                                    outTrace.ModifierId = userInfo.Id;
                                                    outTrace.ModifierName = userInfo.Name;
                                                    outTrace.ModifyTime = DateTime.Now;
                                                    outTrace.Status = (int)DCSAccurateTraceStatus.作废;
                                                    UpdateToDatabase(outTrace);
                                                }
                                            }
                                            //增加入库追溯信息中的出库数量
                                            old.OutQty = (old.OutQty ?? 0) + pickQty;
                                            old.ModifierId = userInfo.Id;
                                            old.ModifierName = userInfo.Name;
                                            old.ModifyTime = DateTime.Now;
                                            UpdateToDatabase(old);
                                        }
                                    }
                                }
                            }
                        }


                    }  //更新已拣货量
                    if(null == item.PickingQty) {
                        item.PickingQty = 0;
                    }
                    var pickDetail = ObjectContext.PickingTaskDetails.Where(r => r.Id == item.Id).FirstOrDefault();
                    if(null == pickDetail.PickingQty) {
                        pickDetail.PickingQty = 0;
                    }
                    if(pickDetail.PlanQty < (pickDetail.PickingQty + item.CourrentPicking)) {
                        throw new ValidationException(ErrorStrings.PickingTask_Validation5);
                    }
                    pickDetail.PickingQty += item.CourrentPicking;
                    pickDetail.ShortPickingReason = item.ShortPickingReason;
                    //拣货保存时候校验，本次拣货量+已拣货量=计划量的不能选择短拣原因。
                    if(pickDetail.PickingQty == pickDetail.PlanQty) {
                        pickDetail.ShortPickingReason = null;
                    }
                    UpdateToDatabase(pickDetail);
                    ObjectContext.SaveChanges();
                }
                if(isInsert) {
                    InsertToDatabase(batch);
                }
                //库区信息
                var warehouseArea = ObjectContext.WarehouseAreas.Where(r => r.Code == "X1-01-01-01" && r.Status == (int)DcsBaseDataStatus.有效 && r.AreaKind == (int)DcsAreaKind.库位 && r.WarehouseId == pickingTask.WarehouseId).FirstOrDefault();
                if(null == warehouseArea) {
                    throw new ValidationException(string.Format(ErrorStrings.PickingTask_Validation6, pickingTask.WarehouseCode));
                }
                //库区用途
                var warehouseAreaCategory = ObjectContext.WarehouseAreaCategories.Where(r => r.Category == (int)DcsAreaType.待发区).FirstOrDefault();
                //更新待发区库存
                var spId = pickingDetails.Select(r => r.SparePartId).Distinct().ToArray();
                var partsStockDfs = new PartsStockAch(this.DomainService).GetPartsStocksWithLock((from a in ObjectContext.PartsStocks
                                                                                                  join b in ObjectContext.WarehouseAreas on a.WarehouseAreaId equals b.Id
                                                                                                  where a.WarehouseId == pickingTask.WarehouseId && spId.Contains(a.PartId) && b.Code == "X1-01-01-01" && b.Status == (int)DcsBaseDataStatus.有效
                                                                                                   && b.AreaKind == (int)DcsAreaKind.库位
                                                                                                  select a)).ToList();
                foreach(var pid in spId) {
                    //需要更新的数量
                    var sumQty = pickingDetails.Where(r => r.SparePartId == pid).Sum(s => s.CourrentPicking);
                    if(sumQty.Value > 0) {
                        //先查询是否已经有待发区库存，不存在则新增
                        var oldStock = partsStockDfs.Where(r => r.PartId == pid).FirstOrDefault();
                        if(null != oldStock) {
                            if(null == oldStock.LockedQty) {
                                oldStock.LockedQty = 0;
                            }
                            oldStock.Quantity = oldStock.Quantity + sumQty.Value;
                            oldStock.LockedQty = oldStock.LockedQty + sumQty.Value;
                            oldStock.ModifierId = userInfo.Id;
                            oldStock.ModifierName = userInfo.Name;
                            oldStock.ModifyTime = DateTime.Now;
                            UpdateToDatabase(oldStock);
                        } else {
                            var newPartsStock = new PartsStock() {
                                WarehouseId = Convert.ToInt32(pickingTask.WarehouseId),
                                StorageCompanyId = storageCompanyId,
                                StorageCompanyType = storageCompanyType,
                                BranchId = pickingTask.BranchId.Value,
                                WarehouseAreaId = warehouseArea.Id,
                                WarehouseAreaCategoryId = warehouseAreaCategory.Id,
                                PartId = pid,
                                Quantity = sumQty.Value,
                                LockedQty = sumQty.Value,
                                CreateTime = DateTime.Now,
                                CreatorId = userInfo.Id,
                                CreatorName = userInfo.Name

                            };
                            InsertToDatabase(newPartsStock);
                        }
                    }
                }
                ObjectContext.SaveChanges();
                //更新出库计划单
                var outPlanIds = pickingDetails.Select(r => r.PartsOutboundPlanId).Distinct().ToArray();
                foreach(var item in outPlanIds) {
                    var outplan = ObjectContext.PartsOutboundPlans.Where(r => r.Id == item).FirstOrDefault();
                    new PartsOutboundPlanAch(this.DomainService).更新出库计划单状态(outplan);
                }
                transaction.Complete();
            }
        }
        public void abandonPickingTask(PickingTaskQuery pick) {
            var userInfo = Utils.GetCurrentUserInfo();
            //作废主单
            var pickingTask = ObjectContext.PickingTasks.Where(r => r.Id == pick.Id).FirstOrDefault();
            pickingTask.Status = (int)DcsPickingTaskStatus.作废;
            pickingTask.ModifierId = userInfo.Id;
            pickingTask.ModifierName = userInfo.Name;
            pickingTask.ModifyTime = DateTime.Now;
            UpdateToDatabase(pickingTask);
            //查询清单
            var detalList = ObjectContext.PickingTaskDetails.Where(r => r.PickingTaskId == pickingTask.Id).ToArray();
            var partsInBoundIds = detalList.Select(r => r.PartsOutboundPlanId).Distinct().ToArray();

            foreach(var plan in partsInBoundIds) {

                var pickDetailList = detalList.Where(r => r.PartsOutboundPlanId == plan.Value).ToArray();
                foreach(var item in pickDetailList) {
                    //出库清单
                    var planDetail = ObjectContext.PartsOutboundPlanDetails.Where(r => r.SparePartId == item.SparePartId && r.PartsOutboundPlanId == plan.Value).FirstOrDefault();
                    var stock = new PartsStockAch(this.DomainService).GetPartsStocksWithLock(ObjectContext.PartsStocks.Where(r => r.PartId == item.SparePartId && r.WarehouseId == pickingTask.WarehouseId && r.WarehouseAreaId == item.WarehouseAreaId)).FirstOrDefault();
                    if(null == stock || stock.LockedQty < item.PlanQty) {
                        throw new ValidationException(ErrorStrings.PartsOutboundPlan_Validation8);
                    }
                    //更新库存减少锁定量
                    stock.LockedQty = Convert.ToInt32(stock.LockedQty) - item.PlanQty;
                    stock.ModifierId = userInfo.Id;
                    stock.ModifierName = userInfo.Name;
                    stock.ModifyTime = DateTime.Now;
                    UpdateToDatabase(stock);
                    //更新出库明细下载量
                    planDetail.DownloadQty = planDetail.DownloadQty - item.PlanQty;
                    UpdateToDatabase(planDetail);
                }
                //更新入库计划单
                var partsOutBound = ObjectContext.PartsOutboundPlans.Where(r => r.Id == plan.Value).SingleOrDefault();
                partsOutBound.Status = (int)DcsPartsOutboundPlanStatus.新建;
                partsOutBound.ModifierId = userInfo.Id;
                partsOutBound.ModifierName = userInfo.Name;
                partsOutBound.ModifyTime = DateTime.Now;
                UpdateToDatabase(partsOutBound);
            }
            ObjectContext.SaveChanges();
        }
        //更新配件锁定库存
        private void updatePartsLockedStock(int partId, int warehouseId, int amount) {

            var partsLockedStock = ObjectContext.PartsLockedStocks.Where(r => r.PartId == partId && r.WarehouseId == warehouseId).FirstOrDefault();
            partsLockedStock.LockedQuantity = partsLockedStock.LockedQuantity - amount;
            var userInfo = Utils.GetCurrentUserInfo();
            partsLockedStock.ModifierId = userInfo.Id;
            partsLockedStock.ModifierName = userInfo.Name;
            partsLockedStock.ModifyTime = DateTime.Now;
            UpdateToDatabase(partsLockedStock);

        }
        public void 设置责任人(int[] ids, int personnelId) {
            var userInfo = Utils.GetCurrentUserInfo();
            //查询所有的拣货单
            var pickingLists = ObjectContext.PickingTasks.Where(r => ids.Contains(r.Id)).ToArray();
            //责任人信息
            var personnel = ObjectContext.Personnels.Where(r => r.Id == personnelId).SingleOrDefault();
            foreach(var item in pickingLists) {
                item.ModifierId = userInfo.Id;
                item.ModifierName = userInfo.Name;
                item.ModifyTime = DateTime.Now;
                item.ResponsiblePersonId = personnel.Id;
                item.ResponsiblePersonName = personnel.Name;
                UpdateToDatabase(item);
            }
            ObjectContext.SaveChanges();
        }
    }
    partial class DcsDomainService {
        public IEnumerable<PickingTaskQuery> getPickingTaskLists(string code, int? warehouseId, int? partsSalesCategoryId, string status, string sourceCode, string partsOutboundPlanCode, string orderTypeName, string counterpartCompanyName, string sparePartCode, string sparePartName,
           DateTime? bCreateTime, DateTime? eCreateTime, DateTime? bPickingfinishtime, DateTime? ePickingfinishtime, int? orderTypeId) {
            return new PickingTaskAch(this).getPickingTaskLists(code, warehouseId, partsSalesCategoryId, status, sourceCode, partsOutboundPlanCode, orderTypeName, counterpartCompanyName, sparePartCode, sparePartName, bCreateTime, eCreateTime, bPickingfinishtime, ePickingfinishtime, orderTypeId);
        }
        public IEnumerable<PickingTaskQuery> getPickingTaskListsAll(string code, int? warehouseId, int? partsSalesCategoryId, int? status, string sourceCode, string partsOutboundPlanCode, string orderTypeName, string counterpartCompanyName,
          DateTime? bCreateTime, DateTime? eCreateTime, DateTime? bPickingfinishtime, DateTime? ePickingfinishtime) {
            return new PickingTaskAch(this).getPickingTaskListsAll(code, warehouseId, partsSalesCategoryId, status, sourceCode, partsOutboundPlanCode, orderTypeName, counterpartCompanyName, bCreateTime, eCreateTime, bPickingfinishtime, ePickingfinishtime);
        }
        public PickingTask GetPickingTaskWithsDetails(int id) {
            return new PickingTaskAch(this).GetPickingTaskWithsDetails(id);
        }
        [Invoke]
        public void 拣货(VirtualPickingTaskDetail[] pickingDetails, int id, string remark) {
            new PickingTaskAch(this).拣货(pickingDetails, id, remark);
        }
        [Invoke]
        public void 完成拣货(VirtualPickingTaskDetail[] pickingDetails, int id, string remark) {
            new PickingTaskAch(this).完成拣货(pickingDetails, id, remark);
        }
        [Update(UsingCustomMethod = true)]
        public void abandonPickingTask(PickingTaskQuery pick) {
            new PickingTaskAch(this).abandonPickingTask(pick);
        }
        [Invoke(HasSideEffects = true)]
        public void 设置责任人(int[] ids, int personnelId) {
            new PickingTaskAch(this).设置责任人(ids, personnelId);
        }
    }
}
