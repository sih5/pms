﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
using System.Configuration;
using System.Data;
using System.Text;
using Devart.Data.Oracle;
using NPOI.SS.Formula.Functions;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class PickingTaskDetailAch : DcsSerivceAchieveBase {

        public PickingTaskDetailAch(DcsDomainService domainService)
            : base(domainService) {
        }

        public IQueryable<PickingTaskDetail> GetPickingTaskDetailLists() {
            return ObjectContext.PickingTaskDetails.OrderBy(r => new {
                r.WarehouseAreaCode,
                r.SparePartCode
            }); 
        }

        public void InsertPickingTaskDetail(PickingTaskDetail detail) {
        }
        public void UpdatePickingTaskDetail(PickingTaskDetail detail) {
        }
        public void UpdatePickingTaskDetail(VirtualPickingTaskDetail detail) {
        }
        public IEnumerable<VirtualPickingTaskDetail> GetPickingTaskDetailListsById(int id) {         

            IEnumerable<VirtualPickingTaskDetail> result = ObjectContext.PickingTaskDetails.Include("SparePart").Where(r => r.PickingTaskId == id).ToList().Select(p =>
               new VirtualPickingTaskDetail {
                   Id = p.Id,
                   PickingTaskId = p.PickingTaskId,
                   SparePartId = p.SparePartId,
                   SparePartCode = p.SparePartCode,
                   SparePartName = p.SparePartName,
                   PartsOutboundPlanCode = p.PartsOutboundPlanCode,
                   MeasureUnit = p.MeasureUnit,
                   PlanQty = p.PlanQty,
                   PickingQty = p.PickingQty,
                   CourrentPicking = p.PlanQty - (p.PickingQty ?? 0),
                   ReferenceCode = p.SparePart.ReferenceCode,
                   WarehouseAreaId = p.WarehouseAreaId,
                   WarehouseAreaCode=p.WarehouseAreaCode,
                   SourceCode = p.SourceCode,
                   ShortPickingReason = p.ShortPickingReason,
                   PartsOutboundPlanId=p.PartsOutboundPlanId.Value
               }

              ).ToList();

            return result;
        }

        public IEnumerable<VirtualPickingTaskDetail> GetPickingTaskDetailListsByIdAndWhouseArea(int id, int[] topLevelWarehouseAreaIds) {
            var user = Utils.GetCurrentUserInfo();
            var res = from p in ObjectContext.PickingTaskDetails.Where(r => r.PickingTaskId == id && (r.PlanQty!=r.PickingQty || r.PickingQty ==null) )
                      join b in ObjectContext.WarehouseAreas.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on p.WarehouseAreaId equals b.Id
                      join c in ObjectContext.WarehouseAreaManagers.Where(r => r.ManagerId == user.Id) on b.TopLevelWarehouseAreaId equals c.WarehouseAreaId
                      join d in ObjectContext.SpareParts on p.SparePartId equals d.Id
                      join e in ObjectContext.Warehouses on b.WarehouseId equals e.Id
                      select new VirtualPickingTaskDetail {
                          Id = p.Id,
                          PickingTaskId = p.PickingTaskId,
                          SparePartId = p.SparePartId,
                          SparePartCode = p.SparePartCode,
                          SparePartName = p.SparePartName,
                          PartsOutboundPlanCode = p.PartsOutboundPlanCode,
                          MeasureUnit = p.MeasureUnit,
                          PlanQty = p.PlanQty,
                          PickingQty = p.PickingQty,
                          CourrentPicking = p.PlanQty - (p.PickingQty ?? 0),
                          ReferenceCode = p.SparePart.ReferenceCode,
                          WarehouseAreaId = p.WarehouseAreaId,
                          WarehouseAreaCode = p.WarehouseAreaCode,
                          SourceCode = p.SourceCode,
                          ShortPickingReason = p.ShortPickingReason,
                          PartsOutboundPlanId = p.PartsOutboundPlanId.Value,
                          TopLevelWarehouseAreaId=b.TopLevelWarehouseAreaId.Value,
                          TraceProperty =d.TraceProperty,
                          CompanyType=e.StorageCompanyType
                      };
            if(topLevelWarehouseAreaIds!=null && topLevelWarehouseAreaIds.Count()>0){
                res=res.Where(r=> topLevelWarehouseAreaIds.Contains(r.TopLevelWarehouseAreaId));
            }
            return res.Distinct().OrderBy(r => new {
                r.WarehouseAreaCode,
                r.SparePartCode
            }); 
        }
        public IEnumerable<VirtualWarehouseArea> GetTopLevelWarehouseAreaByPickingId(int id) {
            var user = Utils.GetCurrentUserInfo();
            var res = from p in ObjectContext.PickingTaskDetails.Where(r => r.PickingTaskId == id && (r.PlanQty != r.PickingQty || r.PickingQty == null))
                      join b in ObjectContext.WarehouseAreas.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on p.WarehouseAreaId equals b.Id
                      join c in ObjectContext.WarehouseAreaManagers.Where(r => r.ManagerId == user.Id) on b.TopLevelWarehouseAreaId equals c.WarehouseAreaId
                      join d in ObjectContext.WarehouseAreas on b.TopLevelWarehouseAreaId equals d.Id
                      select new VirtualWarehouseArea {
                          Id=d.Id,
                          Code=d.Code
                      };
            return res.Distinct().OrderBy(r=>r.Code);
        }
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:30
        //原分布类的函数全部转移到Ach                                                               

        public IQueryable<PickingTaskDetail> GetPickingTaskDetailLists() {
            return new PickingTaskDetailAch(this).GetPickingTaskDetailLists();
        }

        public void InsertPickingTaskDetail(PickingTaskDetail detail) {
            new PickingTaskDetailAch(this).InsertPickingTaskDetail(detail);
        }

        public void UpdatePickingTaskDetail(PickingTaskDetail detail) {
            new PickingTaskDetailAch(this).UpdatePickingTaskDetail(detail);
        }
        public IEnumerable<VirtualPickingTaskDetail> GetPickingTaskDetailListsById(int id) {
            return new PickingTaskDetailAch(this).GetPickingTaskDetailListsById(id);
        }
        public void UpdatePickingTaskDetail(VirtualPickingTaskDetail detail) {
            new PickingTaskDetailAch(this).UpdatePickingTaskDetail(detail);
        }

        public IEnumerable<VirtualPickingTaskDetail> GetPickingTaskDetailListsByIdAndWhouseArea(int id, int[] topLevelWarehouseAreaIds) {
            return new PickingTaskDetailAch(this).GetPickingTaskDetailListsByIdAndWhouseArea(id, topLevelWarehouseAreaIds);
        }
        public IEnumerable<VirtualWarehouseArea> GetTopLevelWarehouseAreaByPickingId(int id) {
            return new PickingTaskDetailAch(this).GetTopLevelWarehouseAreaByPickingId(id);
        }
    }
}
