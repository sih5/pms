﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel.DomainServices.Server;
using System.ComponentModel.DataAnnotations;
using Sunlight.Silverlight.Web;
using System.Transactions;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class PostSaleClaimsTmpAch : DcsSerivceAchieveBase {
        public PostSaleClaimsTmpAch(DcsDomainService domainService)
            : base(domainService) {
        }
        public IEnumerable<VirtualPostSaleClaimsTmp> getPostSaleClaims(string claimBillCode,string sparePartCode,string sparePartName,string dealerCode,string dealerName,int? type,DateTime? bCreateTime,DateTime? eCreateTime) {
            string sql = @"select a.id,a.ClaimBill_Code as ClaimBillCode,b.id as dealerid,a.Dealer_Code as dealercode,b.name as dealername,a.CreateTime,nvl(a.IsClaim,0) as IsClaim,a.SparePart_Code as sparepartcode,a.SparePart_Name as sparepartname,case when a.type =1 then a.amount else -a.amount end as amount,a.status,a.type,a.transfertime,a.SalesPrice,a.RetailGuidePrice,a.GroupCode  from postsaleclaims_tmp a left join dealer b on a.dealer_code = b.code and b.status <> 99 where 1=1 ";
            if (!string.IsNullOrEmpty(claimBillCode)) {
                sql += " and a.ClaimBill_Code like '%" + claimBillCode + "%'";
            }
            if (!string.IsNullOrEmpty(sparePartCode)) {
                sql += " and a.SparePart_Code like '%" + sparePartCode + "%'";
            }
            if (!string.IsNullOrEmpty(sparePartName)) {
                sql += " and a.SparePart_Name like '%" + sparePartName + "%'";
            }
            if (!string.IsNullOrEmpty(dealerCode)) {
                sql += " and a.Dealer_Code like '%" + dealerCode + "%'";
            }
            if (!string.IsNullOrEmpty(dealerName)) {
                sql += " and b.name like '%" + dealerName + "%'";
            }
            if (type.HasValue) {
                sql += " and a.type = " + type + "";
            }
            if (type.HasValue) {
                sql += " and a.type = " + type + "";
            }
            if(bCreateTime.HasValue) {
                sql += " and a.CreateTime>= to_date('" + bCreateTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }
            if(eCreateTime.HasValue) {
                sql += " and a.CreateTime<= to_date('" + eCreateTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }
            var user = Utils.GetCurrentUserInfo();
            var company = ObjectContext.Companies.Single(r => r.Id == user.EnterpriseId);
            if (company.Type != (int)DcsCompanyType.分公司) { 
                sql += " and b.id ="+user.EnterpriseId;
            }
            var search = ObjectContext.ExecuteStoreQuery<VirtualPostSaleClaimsTmp>(sql).ToList();
            return search;
        }
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:33
        //原分布类的函数全部转移到Ach                                                               
        public IEnumerable<VirtualPostSaleClaimsTmp> getPostSaleClaims(string claimBillCode,string sparePartCode,string sparePartName,string dealerCode,string dealerName,int? type,DateTime? bCreateTime,DateTime? eCreateTime) {
            return new PostSaleClaimsTmpAch(this).getPostSaleClaims(claimBillCode,sparePartCode,sparePartName,dealerCode,dealerName,type,bCreateTime,eCreateTime);
        }
    }
}
