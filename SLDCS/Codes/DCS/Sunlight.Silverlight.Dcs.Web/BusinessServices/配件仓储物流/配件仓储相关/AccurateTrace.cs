﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
   partial class AccurateTraceAch : DcsSerivceAchieveBase {
       public AccurateTraceAch(DcsDomainService domainService)
            : base(domainService) {
        }
       public void ValidataAccurateTrace(AccurateTrace accurateTrace) {
           var userInfo = Utils.GetCurrentUserInfo();
           accurateTrace.CreatorId = userInfo.Id;
           accurateTrace.CreatorName = userInfo.Name;
           accurateTrace.CreateTime = DateTime.Now;
           accurateTrace.RowVersion = DateTime.Now;
           InsertToDatabase(accurateTrace);
       }
       public void UpdateTraceCode(AccurateTrace accurateTrace) {
           var userInfo = Utils.GetCurrentUserInfo();

           var oldTrace=ObjectContext.AccurateTraces.Where(r=>r.Id==accurateTrace.Id).First();
            //验证新追溯码是否已存在
           var validata = ObjectContext.AccurateTraces.Where(t => t.TraceCode == accurateTrace.TraceCode && t.Type == (int)DCSAccurateTraceType.入库 && t.Status == (int)DCSAccurateTraceStatus.有效 && t.Id!=accurateTrace.Id).ToArray();
           if(validata.Count()>0) {
               throw new ValidationException("追溯码已存在：" + accurateTrace.TraceCode);
           }
           oldTrace.TraceCode = accurateTrace.TraceCode;
           oldTrace.ModifierId = userInfo.Id;
           oldTrace.ModifierName = userInfo.Name;
           oldTrace.ModifyTime = DateTime.Now;
           UpdateToDatabase(oldTrace);
           ObjectContext.SaveChanges();
       }
    
    }
   partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:11:45
       //原分布类的函数全部转移到Ach                                                               

       [Update(UsingCustomMethod = true)]
       public void ValidataAccurateTrace(AccurateTrace accurateTrace) {
           new AccurateTraceAch(this).ValidataAccurateTrace(accurateTrace);
       }
      [Invoke]
       public void 修改追溯码(AccurateTrace accurateTrace) {
           new AccurateTraceAch(this).UpdateTraceCode(accurateTrace);
       }
   }
}
