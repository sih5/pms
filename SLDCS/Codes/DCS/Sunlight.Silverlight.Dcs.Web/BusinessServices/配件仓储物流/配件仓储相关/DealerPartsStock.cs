﻿using System.Linq;
using System.ServiceModel.DomainServices.Server;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class DealerPartsStockAch : DcsSerivceAchieveBase {
        public DealerPartsStockAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 生成经销商配件库存(DealerPartsStock dealerPartsStock) {
            var dbpartDeleaveInformation = ObjectContext.PartDeleaveInformations.SingleOrDefault(c => c.OldPartId == dealerPartsStock.SparePartId && c.PartsSalesCategoryId == dealerPartsStock.SalesCategoryId && c.Status == (int)DcsBaseDataStatus.有效);
            if(dbpartDeleaveInformation != null) {
                dealerPartsStock.Quantity = dealerPartsStock.Quantity * dbpartDeleaveInformation.DeleaveAmount;
                dealerPartsStock.SparePartCode = dbpartDeleaveInformation.DeleavePartCode;
            }
            InsertDealerPartsStock(dealerPartsStock);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:11:45
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 生成经销商配件库存(DealerPartsStock dealerPartsStock) {
            new DealerPartsStockAch(this).生成经销商配件库存(dealerPartsStock);
        }
    }
}
