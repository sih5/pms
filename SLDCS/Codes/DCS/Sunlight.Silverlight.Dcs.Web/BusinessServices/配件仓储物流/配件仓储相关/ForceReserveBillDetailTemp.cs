﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class ForceReserveBillDetailTempAch : DcsSerivceAchieveBase {
        public ForceReserveBillDetailTempAch(DcsDomainService domainService)
            : base(domainService) {
        }
        public IEnumerable<ForceReserveBillDetailQuery> 查询配件中心库属性(string companyCode, string companyName, string sparePartCode, string sparePartName) {
            string SQL = @"select c.sparepartid,
                           sp.code as SparePartCode,
                           sp.name as SparePartName,
                           c.centerid as companyId,
                           cp.code as companyCode,
                           cp.name as companyName,
                           ps.centerprice,
                           c.newtype as CenterPartProperty,
                           ceil(nvl(sd.StandardQty, 1) * 0.25) as SuggestForceReserveQty,
                            rownum as id
                      from CentralABCClassification c
                      left join PartsSalesPrice ps
                        on c.sparepartid = ps.sparepartid
                       and ps.status = 1
                      join sparepart sp
                        on c.sparepartid = sp.id
                      join company cp
                        on c.centerid = cp.id
                      left join SalesOrderDailyAvg sd
                        on sd.week = to_char(sysdate, 'iw')
                       and sd.centerid = c.id
                       and sd.sparepartid = c.sparepartid
                     where c.newtype in (1, 2)";
            if(!string.IsNullOrEmpty(companyName)) {
                SQL += " and cp.name like '%" + companyName + "%'";
            }
            if(!string.IsNullOrEmpty(companyCode)) {
                SQL += " and Lower(cp.code) like '%" + companyCode.ToLower() + "%'";
            }
            if(!string.IsNullOrEmpty(sparePartCode)) {
                SQL += " and sp.code like '%" + sparePartCode + "%'";
            }
            if(!string.IsNullOrEmpty(sparePartName)) {
                SQL += " and sp.name like '%" + sparePartName + "%'";
            }
            SQL += " order by c.centerid";
            var search = ObjectContext.ExecuteStoreQuery<ForceReserveBillDetailQuery>(SQL).ToList();
            return search;
        }
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:33
        //原分布类的函数全部转移到Ach                                                               
        public IEnumerable<ForceReserveBillDetailQuery> 查询配件中心库属性(string companyCode, string companyName, string sparePartCode, string sparePartName) {
            return new ForceReserveBillDetailTempAch(this).查询配件中心库属性(companyCode, companyName, sparePartCode, sparePartName);
        }
    }
}
