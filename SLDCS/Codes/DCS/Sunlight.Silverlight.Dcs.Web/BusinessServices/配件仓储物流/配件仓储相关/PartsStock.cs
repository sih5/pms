﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Dcs.Web.Resources;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsStockAch : DcsSerivceAchieveBase {
        public PartsStockAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 生成配件库存(PartsStock partsStock) {
            InsertPartsStock(partsStock);
            var warehouseAreaHistory = new WarehouseAreaHistory();
            warehouseAreaHistory.WarehouseId = partsStock.WarehouseId;
            warehouseAreaHistory.StorageCompanyId = partsStock.StorageCompanyId;
            warehouseAreaHistory.StorageCompanyType = partsStock.StorageCompanyType;
            warehouseAreaHistory.BranchId = partsStock.BranchId;
            warehouseAreaHistory.WarehouseAreaId = partsStock.WarehouseAreaId;
            warehouseAreaHistory.WarehouseAreaCategoryId = partsStock.WarehouseAreaCategoryId;
            warehouseAreaHistory.Quantity = partsStock.Quantity;
            ObjectContext.WarehouseAreaHistories.AddObject(warehouseAreaHistory);
            DomainService.InsertWarehouseAreaHistory(warehouseAreaHistory);

        }


        public void 删除空配件库存(int[] ids) {
            var partsStocks = ObjectContext.PartsStocks.Where(r => ids.Contains(r.Id));
            foreach(var partsStock in partsStocks) {
                if(partsStock.Quantity == 0) {
                    var partsStockHistory = new PartsStockHistory {
                        PartsStockId = partsStock.Id,
                        WarehouseId = partsStock.WarehouseId,
                        StorageCompanyId = partsStock.StorageCompanyId,
                        StorageCompanyType = partsStock.StorageCompanyType,
                        BranchId = partsStock.BranchId,
                        WarehouseAreaId = partsStock.WarehouseAreaId,
                        WarehouseAreaCategoryId = partsStock.WarehouseAreaCategoryId,
                        PartId = partsStock.PartId,
                        Quantity = partsStock.Quantity,
                        Remark = partsStock.Remark,
                        CreatorId = partsStock.CreatorId,
                        CreatorName = partsStock.CreatorName,
                        CreateTime = partsStock.CreateTime,
                        ModifierId = partsStock.ModifierId,
                        ModifierName = partsStock.ModifierName,
                        ModifyTime = partsStock.ModifyTime
                    };
                    InsertToDatabase(partsStockHistory);
                    DeleteFromDatabase(partsStock);
                }else {
                    throw new ValidationException(ErrorStrings.PartsStock_Validation9);
                }
            }
            ObjectContext.SaveChanges();
        }


        public void 删除配件库存(decimal[] PartIdPointWarehouseAreaIds) {
            var partsStocks = ObjectContext.PartsStocks.Include("WarehouseAreaCategory").Where(v => PartIdPointWarehouseAreaIds.Any(r => (r - v.PartId) * 10000000000 == v.WarehouseAreaId));                             
          //  只能删除配件存在多条库位的数据，当一个配件只存在一条同类型的库位时，该节点不允许删除库位关系。
            var spareId = partsStocks.Select(r => r.PartId).Distinct().ToArray();
            var allStock = ObjectContext.PartsStocks.Include("WarehouseAreaCategory").Where(r => spareId.Contains(r.PartId));
            foreach(var partsStock in partsStocks) {
                if(partsStock.Quantity == 0) {
                    var stock = allStock.Where(r => r.PartId == partsStock.PartId  && r.WarehouseAreaCategory.Category == partsStock.WarehouseAreaCategory.Category).ToArray();
                    if (stock.Count()==1)
                    {
                        throw new ValidationException(ErrorStrings.PartsStock_Validation10);
                    }
                    var partsStockHistory = new PartsStockHistory {
                        PartsStockId = partsStock.Id,
                        WarehouseId = partsStock.WarehouseId,
                        StorageCompanyId = partsStock.StorageCompanyId,
                        StorageCompanyType = partsStock.StorageCompanyType,
                        BranchId = partsStock.BranchId,
                        WarehouseAreaId = partsStock.WarehouseAreaId,
                        WarehouseAreaCategoryId = partsStock.WarehouseAreaCategoryId,
                        PartId = partsStock.PartId,
                        Quantity = partsStock.Quantity,
                        Remark = partsStock.Remark,
                        CreatorId = partsStock.CreatorId,
                        CreatorName = partsStock.CreatorName,
                        CreateTime = partsStock.CreateTime,
                        ModifierId = partsStock.ModifierId,
                        ModifierName = partsStock.ModifierName,
                        ModifyTime = partsStock.ModifyTime
                    };
                    InsertToDatabase(partsStockHistory);
                    DeleteFromDatabase(partsStock);
                }else {
                    throw new ValidationException(ErrorStrings.PartsStock_Validation9);
                }
            }
            ObjectContext.SaveChanges();
        }

        public void DeletePartsStockByIds(int[] ids) {
            var partsStocks = this.ObjectContext.PartsStocks.Where(r => ids.Contains(r.Id)).ToArray();
            foreach(var partsStock in partsStocks) {
                if(partsStock.Quantity == 0) {
                    var partsStockHistory = new PartsStockHistory {
                        PartsStockId = partsStock.Id,
                        WarehouseId = partsStock.WarehouseId,
                        StorageCompanyId = partsStock.StorageCompanyId,
                        StorageCompanyType = partsStock.StorageCompanyType,
                        BranchId = partsStock.BranchId,
                        WarehouseAreaId = partsStock.WarehouseAreaId,
                        WarehouseAreaCategoryId = partsStock.WarehouseAreaCategoryId,
                        PartId = partsStock.PartId,
                        Quantity = partsStock.Quantity,
                        Remark = partsStock.Remark,
                        CreatorId = partsStock.CreatorId,
                        CreatorName = partsStock.CreatorName,
                        CreateTime = partsStock.CreateTime,
                        ModifierId = partsStock.ModifierId,
                        ModifierName = partsStock.ModifierName,
                        ModifyTime = partsStock.ModifyTime
                    };
                    InsertToDatabase(partsStockHistory);
                    DeleteFromDatabase(partsStock);
                }else {
                    throw new ValidationException(ErrorStrings.PartsStocks_Delete_Error);
                }
            }
            ObjectContext.SaveChanges();
        }


        public void 全量库存生成() {
//            string sql = string.Format(@"INSERT INTO Retailer_PartsStockYX_SYNC 
//                                            select S_Retailer_PartsStockYX_SYNC.NEXTVAL,s.Id,'PartsStock',sysdate  from PartsStock  s 
//                                                inner join Warehouse w on s.warehouseid=w.Id 
//                                                inner join SalesUnitAffiWarehouse u on u.warehouseid=w.Id 
//                                                inner join SalesUnit b on u.SalesUnitId=b.Id
//                                                inner join PartsSalesCategory p on p.Id=b.partssalescategoryid where  p.Code='999999' Or p.Name='999991'
//                                       ");
//            ObjectContext.ExecuteStoreCommand(sql);
            var fullStockTask = new FullStockTask();
            var userInfo = Utils.GetCurrentUserInfo();
            var datetimenow = DateTime.Now;
            fullStockTask.SyncTime = datetimenow.Year + "-" + (datetimenow.Month < 10 ? "0" + datetimenow.Month.ToString() : datetimenow.Month.ToString()) + "-" + (datetimenow.Day < 10 ? "0" + datetimenow.Day.ToString() : datetimenow.Day.ToString());
            fullStockTask.CreatorId = userInfo.Id;
            fullStockTask.CreatorName = userInfo.Name;
            fullStockTask.CreateTime =datetimenow ;
            InsertToDatabase(fullStockTask);
            this.ObjectContext.SaveChanges();
        }
    
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:11:45
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 生成配件库存(PartsStock partsStock) {
            new PartsStockAch(this).生成配件库存(partsStock);
        }
        

        [Invoke(HasSideEffects = true)]
        public void 删除空配件库存(int[] ids) {
            new PartsStockAch(this).删除空配件库存(ids);
        }


       [Invoke(HasSideEffects = true)]
        public void 删除配件库存(decimal[] PartIdPointWarehouseAreaIds) {
            new PartsStockAch(this).删除配件库存(PartIdPointWarehouseAreaIds);
        }

        [Invoke]
        public void DeletePartsStockByIds(int[] ids) {
            new PartsStockAch(this).DeletePartsStockByIds(ids);
        }
        

        [Invoke]
        public void 全量库存生成() {
            new PartsStockAch(this).全量库存生成();
        }
    }
}

