﻿using System;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
   partial class TraceTempTypeAch : DcsSerivceAchieveBase {
       public TraceTempTypeAch(DcsDomainService domainService)
            : base(domainService) {
        }
       public void ValidataraceTempType(TraceTempType traceTempType) {
           var userInfo = Utils.GetCurrentUserInfo();
           traceTempType.CreatorId = userInfo.Id;
           traceTempType.CreatorName = userInfo.Name;
           traceTempType.CreateTime = DateTime.Now;
           traceTempType.RowVersion = DateTime.Now;
           InsertToDatabase(traceTempType);
       }
    }
   partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:11:45
       //原分布类的函数全部转移到Ach                                                               

       [Update(UsingCustomMethod = true)]
       public void ValidataraceTempType(TraceTempType traceTempType) {
           new TraceTempTypeAch(this).ValidataraceTempType(traceTempType);
       }
   }
}

