﻿using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class WarehouseAch : DcsSerivceAchieveBase {
        public WarehouseAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 作废仓库(Warehouse warehouse) {
            var dbwarehouse = ObjectContext.Warehouses.Where(r => r.Id == warehouse.Id && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbwarehouse == null)
                throw new ValidationException(ErrorStrings.Warehouse_Validation2);
            if(ObjectContext.PartsStocks.Any(r => r.WarehouseId == warehouse.Id && r.Quantity > 0))
                throw new ValidationException(ErrorStrings.Warehouse_Validation3);
            UpdateToDatabase(warehouse);
            warehouse.Status = (int)DcsBaseDataStatus.作废;
            this.UpdateWarehouseValidate(warehouse);
            var dbwarehouseAreas = ObjectContext.WarehouseAreas.Where(r => r.WarehouseId == warehouse.Id);
            foreach(var warehouseArea in dbwarehouseAreas) {
                UpdateToDatabase(warehouseArea);
                warehouseArea.Status = (int)DcsBaseDataStatus.作废;
                new WarehouseAreaAch(this.DomainService).UpdateWarehouseAreaValidate(warehouseArea);
            }
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:11:45
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 作废仓库(Warehouse warehouse) {
            new WarehouseAch(this).作废仓库(warehouse);
        }
    }
}
