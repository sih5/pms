﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class WarehouseAreaAch : DcsSerivceAchieveBase {
        public WarehouseAreaAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 作废库区库位(WarehouseArea warehouseArea) {
            var childWarehouseAreas = ObjectContext.GetChildWarehouseAreas(warehouseArea.Id).ToArray();
            var warehouseAreaIds = childWarehouseAreas.Select(v => v.Id);
            if(ObjectContext.PartsStocks.Any(r => warehouseAreaIds.Contains(r.WarehouseAreaId) && r.Quantity > 0))
                throw new ValidationException(ErrorStrings.WarehouseArea_Validation3);
            foreach(var childWarehouseArea in childWarehouseAreas) {
                UpdateToDatabase(childWarehouseArea);
                childWarehouseArea.Status = (int)DcsBaseDataStatus.作废;
              //  this.UpdateWarehouseAreaValidate(childWarehouseArea);
                var userInfo = Utils.GetCurrentUserInfo();
                warehouseArea.ModifierId = userInfo.Id;
                warehouseArea.ModifierName = userInfo.Name;
                warehouseArea.ModifyTime = DateTime.Now;
            }
        }
        /// <summary>
        /// 仅供客户端使用
        /// </summary>
        /// <param name="warehouseArea"></param>

        public void 修改库区库位(WarehouseArea warehouseArea) {
            CheckEntityState(warehouseArea);
            var originalWarehouseArea = ChangeSet.GetOriginal(warehouseArea);
            if(originalWarehouseArea == null || originalWarehouseArea.AreaKind == warehouseArea.AreaKind)
                return;
            if(warehouseArea.AreaKind == (int)DcsAreaKind.库区) {
                if(ObjectContext.PartsStocks.Any(r => r.WarehouseAreaId == warehouseArea.Id && r.Quantity > 0))
                    throw new ValidationException(ErrorStrings.WarehouseArea_Validation4);
            }else if(warehouseArea.AreaKind == (int)DcsAreaKind.库位) {
                var dbParent = ObjectContext.WarehouseAreas.SingleOrDefault(r => r.Id == warehouseArea.ParentId);
                if(dbParent == null || dbParent.AreaKind == (int)DcsAreaKind.仓库)
                    throw new ValidationException(ErrorStrings.WarehouseArea_Validation5);
                if(ObjectContext.WarehouseAreas.Any(r => r.ParentId == warehouseArea.Id))
                    throw new ValidationException(ErrorStrings.WarehouseArea_Validation6);
            }
        }


        public void 新增库区库位(WarehouseArea warehouseArea, WarehouseAreaManager[] managers, WarehouseAreaCategory areaCategory, WarehouseArea parentWarehouseArea) {
            if(warehouseArea.AreaKind == (int)DcsAreaKind.库位 && !warehouseArea.TopLevelWarehouseAreaId.HasValue) {
                throw new ValidationException(ErrorStrings.PartsStock_Validation11);
            }
            this.InsertWarehouseAreaValidate(warehouseArea);
            var dbwarehouseArea = this.ObjectContext.WarehouseAreas.FirstOrDefault(r => r.WarehouseId == warehouseArea.WarehouseId && r.Code.ToLower() == warehouseArea.Code.ToLower());
            if(dbwarehouseArea != null) {
                dbwarehouseArea.Status = (int)DcsBaseDataStatus.有效;
                dbwarehouseArea.ParentId = warehouseArea.ParentId;
                dbwarehouseArea.TopLevelWarehouseAreaId = warehouseArea.TopLevelWarehouseAreaId;
                dbwarehouseArea.WarehouseAreaCategory = areaCategory;
                //if (warehouseArea.AreaKind == (int)DcsAreaKind.库位)
                //{
                //    dbwarehouseArea.ParentWarehouseArea = parentWarehouseArea;
                //}
                dbwarehouseArea.AreaKind = warehouseArea.AreaKind;
                dbwarehouseArea.Remark = warehouseArea.Remark;
                var oldManagers = ObjectContext.WarehouseAreaManagers.Where(r => r.WarehouseAreaId == dbwarehouseArea.Id).ToArray();
                foreach(var manager in oldManagers) {
                    DeleteFromDatabase(manager);
                    DomainService.DeleteWarehouseAreaManager(manager);
                }
                foreach(var manager in managers) {
                    //if (manager.WarehouseArea.AreaKind != (int)DcsAreaKind.库区 || manager.WarehouseArea.ParentWarehouseArea.AreaKind != (int)DcsAreaKind.仓库)
                    //    throw new ValidationException(ErrorStrings.WarehouseAreaManager_Validation2);
                    manager.WarehouseArea = dbwarehouseArea;
                }
                UpdateToDatabase(dbwarehouseArea);
                this.UpdateWarehouseAreaValidate(dbwarehouseArea);
            }else {
                foreach(var manager in managers) {
                    //if (manager.WarehouseArea.AreaKind != (int)DcsAreaKind.库区 || manager.WarehouseArea.ParentWarehouseArea.AreaKind != (int)DcsAreaKind.仓库)
                    //    throw new ValidationException(ErrorStrings.WarehouseAreaManager_Validation2);
                    warehouseArea.WarehouseAreaManagers.Add(manager);
                }
                warehouseArea.WarehouseAreaCategory = areaCategory;
                warehouseArea.AreaCategoryId = warehouseArea.WarehouseAreaCategory.Category;
                //if (warehouseArea.AreaKind == (int)DcsAreaKind.库位)
                //{
                //    warehouseArea.ParentWarehouseArea = parentWarehouseArea;
                //}
                InsertToDatabase(warehouseArea);
                this.InsertWarehouseAreaValidate(warehouseArea);
            }
            ObjectContext.SaveChanges();
        }

        public void 作废库位(int[] ids) {
            foreach(var id in ids){        
            var warehouseArea = ObjectContext.WarehouseAreas.SingleOrDefault(r => r.Id == id);
            if(warehouseArea == null) {
                throw new ValidationException(ErrorStrings.PartsStock_Validation12);
            }
            if(ObjectContext.PartsStocks.Any(r => warehouseArea.Id == r.WarehouseAreaId && r.Quantity > 0))
                throw new ValidationException(ErrorStrings.WarehouseArea_Validation3);
            UpdateToDatabase(warehouseArea);
            warehouseArea.Status = (int)DcsBaseDataStatus.作废;
            var userInfo = Utils.GetCurrentUserInfo();
            warehouseArea.ModifierId = userInfo.Id;
            warehouseArea.ModifierName = userInfo.Name;
            warehouseArea.ModifyTime = DateTime.Now;
            ObjectContext.SaveChanges();
            }
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:11:45
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 作废库区库位(WarehouseArea warehouseArea) {
            new WarehouseAreaAch(this).作废库区库位(warehouseArea);
        }
        /// <summary>
        /// 仅供客户端使用
        /// </summary>
        /// <param name="warehouseArea"></param>
        [Update(UsingCustomMethod = true)]
        public void 修改库区库位(WarehouseArea warehouseArea) {
            new WarehouseAreaAch(this).修改库区库位(warehouseArea);
        }

        [Invoke]
        public void 新增库区库位(WarehouseArea warehouseArea, WarehouseAreaManager[] managers, WarehouseAreaCategory areaCategory, WarehouseArea parentWarehouseArea) {
            new WarehouseAreaAch(this).新增库区库位(warehouseArea,managers,areaCategory,parentWarehouseArea);
        }
        
        [Invoke]
        public void 作废库位(int[] id) {
            new WarehouseAreaAch(this).作废库位(id);
        }
    }
}
