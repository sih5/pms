﻿using System.ComponentModel.DataAnnotations;
using System.ServiceModel.DomainServices.Server;
using Sunlight.Silverlight.Dcs.Web.Resources;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class WarehouseAreaManagerAch : DcsSerivceAchieveBase {
        public WarehouseAreaManagerAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 生成库区负责人(WarehouseAreaManager warehouseAreaManager) {
            if(warehouseAreaManager.WarehouseArea.AreaKind != (int)DcsAreaKind.库区 || warehouseAreaManager.WarehouseArea.ParentWarehouseArea.AreaKind != (int)DcsAreaKind.仓库)
                throw new ValidationException(ErrorStrings.WarehouseAreaManager_Validation2);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:11:45
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 生成库区负责人(WarehouseAreaManager warehouseAreaManager) {
            new WarehouseAreaManagerAch(this).生成库区负责人(warehouseAreaManager);
        }
    }
}
