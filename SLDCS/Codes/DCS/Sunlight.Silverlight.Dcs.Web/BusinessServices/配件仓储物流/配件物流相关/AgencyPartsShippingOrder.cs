﻿using System;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Quartz.Xml;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class AgencyPartsShippingOrderAch : DcsSerivceAchieveBase {
        public void 发货确认代理库(AgencyPartsShippingOrder agencyPartsShippingOrder) {
            var dbAgencyPartsShippingOrder = ObjectContext.AgencyPartsShippingOrders.Where(r => r.Id == agencyPartsShippingOrder.Id).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbAgencyPartsShippingOrder == null)
                throw new ValidationException(ErrorStrings.PartsShippingOrder_Validation26);
            if(dbAgencyPartsShippingOrder.Status != (int)DcsPartsShippingOrderStatus.新建)
                throw new ValidationException(ErrorStrings.PartsShippingOrder_Validation1);

            var logisticCompany = ObjectContext.LogisticCompanies.FirstOrDefault(v => v.Name == agencyPartsShippingOrder.LogisticCompanyName);
            if(logisticCompany != null) {
                agencyPartsShippingOrder.LogisticCompanyId = logisticCompany.Id;
                agencyPartsShippingOrder.LogisticCompanyCode = logisticCompany.Code;
            }

            var userInfo = Utils.GetCurrentUserInfo();
            agencyPartsShippingOrder.Status = (int)DcsPartsShippingOrderStatus.已发货;
            agencyPartsShippingOrder.ModifierId = userInfo.Id;
            agencyPartsShippingOrder.ModifierName = userInfo.Name;
            agencyPartsShippingOrder.ModifyTime = DateTime.Now;
            UpdateAgencyPartsShippingOrderValidate(agencyPartsShippingOrder);
            UpdateToDatabase(agencyPartsShippingOrder);
        }

        public void GPS定位器设备号登记代理库(AgencyPartsShippingOrder agencyPartsShippingOrder) {
            var dbAgencyPartsShippingOrder = ObjectContext.PartsShippingOrders.Where(r => r.Id == agencyPartsShippingOrder.Id).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbAgencyPartsShippingOrder == null)
                throw new ValidationException(ErrorStrings.PartsShippingOrder_Validation26);
            var userInfo = Utils.GetCurrentUserInfo();
            agencyPartsShippingOrder.ModifierId = userInfo.Id;
            agencyPartsShippingOrder.ModifierName = userInfo.Name;
            agencyPartsShippingOrder.ModifyTime = DateTime.Now;
            UpdateAgencyPartsShippingOrderValidate(agencyPartsShippingOrder);
            UpdateToDatabase(agencyPartsShippingOrder);
        }

        public void 送达确认代理库(AgencyPartsShippingOrder agencyPartsShippingOrder) {
            var dbAgencyPartsShippingOrder = ObjectContext.AgencyPartsShippingOrders.Where(r => r.Id == agencyPartsShippingOrder.Id).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbAgencyPartsShippingOrder == null)
                throw new ValidationException(ErrorStrings.PartsShippingOrder_Validation26);
            if(dbAgencyPartsShippingOrder.Status != (int)DcsPartsShippingOrderStatus.已发货)
                throw new ValidationException(ErrorStrings.PartsShippingOrder_Validation27);

            var userInfo = Utils.GetCurrentUserInfo();
            agencyPartsShippingOrder.Status = agencyPartsShippingOrder.ArrivalMode == (int)DcsArrivalMode.货运站 ? (int)DcsPartsShippingOrderStatus.待提货 : (int)DcsPartsShippingOrderStatus.待收货;
            agencyPartsShippingOrder.ModifierId = userInfo.Id;
            agencyPartsShippingOrder.ModifierName = userInfo.Name;
            agencyPartsShippingOrder.ModifyTime = DateTime.Now;
            UpdateAgencyPartsShippingOrderValidate(agencyPartsShippingOrder);
            UpdateToDatabase(agencyPartsShippingOrder);
        }

        public void 超期或电商运单流水反馈代理库(AgencyPartsShippingOrder agencyPartsShippingOrder) {
            var dbAgencyPartsShippingOrder = ObjectContext.AgencyPartsShippingOrders.Where(r => r.Id == agencyPartsShippingOrder.Id).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbAgencyPartsShippingOrder == null)
                throw new ValidationException(ErrorStrings.PartsShippingOrder_Validation26);
            if(dbAgencyPartsShippingOrder.Status != (int)DcsPartsShippingOrderStatus.新建 && dbAgencyPartsShippingOrder.Status != (int)DcsPartsShippingOrderStatus.已发货 && dbAgencyPartsShippingOrder.Status != (int)DcsPartsShippingOrderStatus.待提货 && dbAgencyPartsShippingOrder.Status != (int)DcsPartsShippingOrderStatus.待收货)
                throw new ValidationException(ErrorStrings.PartsShippingOrder_Validation28);

            var userInfo = Utils.GetCurrentUserInfo();
            if(!string.IsNullOrEmpty(dbAgencyPartsShippingOrder.FlowFeedback)) {
                agencyPartsShippingOrder.FlowFeedback = dbAgencyPartsShippingOrder.FlowFeedback + agencyPartsShippingOrder.FlowFeedback;
            }
            agencyPartsShippingOrder.FlowFeedback = string.Format("{0},{1};\n", agencyPartsShippingOrder.FlowFeedback, DateTime.Now.ToString());
            agencyPartsShippingOrder.ModifierId = userInfo.Id;
            agencyPartsShippingOrder.ModifierName = userInfo.Name;
            agencyPartsShippingOrder.ModifyTime = DateTime.Now;
            UpdateAgencyPartsShippingOrderValidate(agencyPartsShippingOrder);
            UpdateToDatabase(agencyPartsShippingOrder);
        }

        public void 配件发运单代理库发货(int[] ids) {
            var agencyPartsShippingOrders = ObjectContext.AgencyPartsShippingOrders.Where(r => ids.Contains(r.Id)).ToArray();
            foreach(var agencyPartsShippingOrder in agencyPartsShippingOrders) {
                if(agencyPartsShippingOrder.Status != (int)DcsPartsShippingOrderStatus.新建)
                    throw new ValidationException(ErrorStrings.AgencyPartsShippingOrder_Validation1);
                agencyPartsShippingOrder.ShippingDate = DateTime.Now;
                agencyPartsShippingOrder.ShippingTime = DateTime.Now;
                agencyPartsShippingOrder.Status = (int)DcsPartsShippingOrderStatus.已发货;
                UpdateAgencyPartsShippingOrderValidate(agencyPartsShippingOrder);
                UpdateToDatabase(agencyPartsShippingOrder);
            }
            ObjectContext.SaveChanges();
        }

        public void 配件发运单代理库送达(int[] ids) {
            var agencyPartsShippingOrders = ObjectContext.AgencyPartsShippingOrders.Where(r => ids.Contains(r.Id)).ToArray();
            foreach(var agencyPartsShippingOrder in agencyPartsShippingOrders) {
                if(agencyPartsShippingOrder.Status != (int)DcsPartsShippingOrderStatus.已发货)
                    throw new ValidationException(ErrorStrings.AgencyPartsShippingOrder_Validation2);
                agencyPartsShippingOrder.LogisticArrivalDate = DateTime.Now;
                agencyPartsShippingOrder.Status = (int)DcsPartsShippingOrderStatus.待收货;
                UpdateAgencyPartsShippingOrderValidate(agencyPartsShippingOrder);
                UpdateToDatabase(agencyPartsShippingOrder);
            }
            ObjectContext.SaveChanges();
        }

        public void 配件发运单代理库收货确认(int[] ids) {
            var userInfo = Utils.GetCurrentUserInfo();
            var agencyPartsShippingOrders = ObjectContext.AgencyPartsShippingOrders.Where(r => ids.Contains(r.Id)).ToArray();
            foreach(var agencyPartsShippingOrder in agencyPartsShippingOrders) {
                if(agencyPartsShippingOrder.Status != (int)DcsPartsShippingOrderStatus.新建 && agencyPartsShippingOrder.Status != (int)DcsPartsShippingOrderStatus.待收货 && agencyPartsShippingOrder.Status != (int)DcsPartsShippingOrderStatus.已发货)
                    throw new ValidationException(ErrorStrings.AgencyPartsShippingOrder_Validation3);
                agencyPartsShippingOrder.ConsigneeId = userInfo.Id;
                agencyPartsShippingOrder.ConsigneeName = userInfo.Name;
                agencyPartsShippingOrder.ConfirmedReceptionTime = DateTime.Now;
                agencyPartsShippingOrder.Status = (int)DcsPartsShippingOrderStatus.收货确认;
                UpdateToDatabase(agencyPartsShippingOrder);
            }
            ObjectContext.SaveChanges();
        }

        public IQueryable<AgencyPartsShippingOrder> GetAgencyPartsShippingOrdersByIds(int[] ids) {
            if(ids != null && ids.Length > 0)
                return ObjectContext.AgencyPartsShippingOrders.Where(t => ids.Contains(t.Id)).OrderBy(e => e.Id);
            return null;
        }
    }

    partial class DcsDomainService {

        public void 配件发运单代理库发货(int[] ids) {
            new AgencyPartsShippingOrderAch(this).配件发运单代理库发货(ids);
        }
        public void 配件发运单代理库送达(int[] ids) {
            new AgencyPartsShippingOrderAch(this).配件发运单代理库送达(ids);
        }
        public void 配件发运单代理库收货确认(int[] ids) {
            new AgencyPartsShippingOrderAch(this).配件发运单代理库收货确认(ids);
        }

        [Update(UsingCustomMethod = true)]
        public void 发货确认代理库(AgencyPartsShippingOrder agencyPartsShippingOrder) {
            new AgencyPartsShippingOrderAch(this).发货确认代理库(agencyPartsShippingOrder);
        }
        [Update(UsingCustomMethod = true)]
        public void 送达确认代理库(AgencyPartsShippingOrder agencyPartsShippingOrder) {
            new AgencyPartsShippingOrderAch(this).送达确认代理库(agencyPartsShippingOrder);
        }

        [Update(UsingCustomMethod = true)]
        public void GPS定位器设备号登记代理库(AgencyPartsShippingOrder agencyPartsShippingOrder) {
            new AgencyPartsShippingOrderAch(this).GPS定位器设备号登记代理库(agencyPartsShippingOrder);
        }
        [Update(UsingCustomMethod = true)]
        public void 超期或电商运单流水反馈代理库(AgencyPartsShippingOrder agencyPartsShippingOrder) {
            new AgencyPartsShippingOrderAch(this).超期或电商运单流水反馈代理库(agencyPartsShippingOrder);
        }
    }
}