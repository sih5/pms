﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class HauDistanceInforAch : DcsSerivceAchieveBase {
        public HauDistanceInforAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 作废运距信息(HauDistanceInfor hauDistanceInfor) {
            var dbHauDistanceInfor = ObjectContext.HauDistanceInfors.Where(r => r.Id == hauDistanceInfor.Id && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbHauDistanceInfor == null)
                throw new ValidationException(ErrorStrings.HauDistanceInfor_Validation3);
            var userinfo = Utils.GetCurrentUserInfo();
            UpdateToDatabase(hauDistanceInfor);
            hauDistanceInfor.Status = (int)DcsBaseDataStatus.作废;
            hauDistanceInfor.AbandonerId = userinfo.Id;
            hauDistanceInfor.AbandonerName = userinfo.Name;
            hauDistanceInfor.AbandonTime = DateTime.Now;
            this.UpdateHauDistanceInforValidate(hauDistanceInfor);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:11:45
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 作废运距信息(HauDistanceInfor hauDistanceInfor) {
            new HauDistanceInforAch(this).作废运距信息(hauDistanceInfor);
        }
    }
}
