﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using System.Transactions;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        [Update(UsingCustomMethod = true)]
        public void 生成配件发运单(PartsShippingOrder partsShippingOrder) {
            var simpleSparePartInfos = partsShippingOrder.PartsShippingOrderDetails.Select(r => new {
                r.SparePartId,
                r.ShippingAmount
            }).ToList();
            var sparePartsIds = partsShippingOrder.PartsShippingOrderDetails.Select(r => r.SparePartId).ToArray();
            var spareparts = ObjectContext.SpareParts.Where(r => sparePartsIds.Contains(r.Id) && r.Status != (int)DcsMasterDataStatus.作废).ToArray();
            var totalWeight = (from a in simpleSparePartInfos
                               from b in spareparts
                               where a.SparePartId == b.Id
                               select new {
                                   weightOfpart = a.ShippingAmount * b.Weight,
                                   volumeOfPart = a.ShippingAmount * b.Volume,
                               }).ToArray();
            //计算总重量
            partsShippingOrder.Weight = totalWeight.Sum(r => r.weightOfpart);
            partsShippingOrder.Volume = totalWeight.Sum(r => r.volumeOfPart);
            var userInfo = Utils.GetCurrentUserInfo();

            var pickingTaskIds = partsShippingOrder.PartsShippingOrderDetails.Where(o => o.TaskType == 1).Select(r => r.TaskId).ToArray();
            var boxUpTaskIds = partsShippingOrder.PartsShippingOrderDetails.Where(o => o.TaskType == 2).Select(r => r.TaskId).ToArray();
            var pickingTasks = ObjectContext.PickingTasks.Where(r => pickingTaskIds.Contains(r.Id)).ToArray();
            var boxUpTasks = ObjectContext.BoxUpTasks.Where(r => boxUpTaskIds.Contains(r.Id)).ToArray();
            var partsOutboundPlanIds = partsShippingOrder.PartsShippingOrderDetails.Select(r => r.PartsOutboundPlanId).ToArray();
            var partsOutboundPlans = ObjectContext.PartsOutboundPlans.Where(o => partsOutboundPlanIds.Contains(o.Id)).ToArray();
            BoxUpTask boxUpTask = null;
            PickingTask pickingTask = null;
            foreach(var partsShippingOrderDetail in partsShippingOrder.PartsShippingOrderDetails.OrderBy(o => o.TaskId)) {
                if(partsShippingOrderDetail.TaskType == 1) {
                    if(pickingTask == null || pickingTask.Id != partsShippingOrderDetail.TaskId) {
                        pickingTask = pickingTasks.FirstOrDefault(o => o.Id == partsShippingOrderDetail.TaskId);
                        if(pickingTask.IsExistShippingOrder.HasValue && pickingTask.IsExistShippingOrder.Value)
                            throw new ValidationException(ErrorStrings.PartsShippingOrder_Validation29);
                        pickingTask.IsExistShippingOrder = true;
                        pickingTask.ModifierId = userInfo.Id;
                        pickingTask.ModifierName = userInfo.Name;
                        pickingTask.ModifyTime = DateTime.Now;
                        UpdateToDatabase(pickingTask);
                    }
                } else {
                    if(boxUpTask == null || boxUpTask.Id != partsShippingOrderDetail.TaskId) {
                        boxUpTask = boxUpTasks.FirstOrDefault(r => r.Id == partsShippingOrderDetail.TaskId);
                        if(boxUpTask.IsExistShippingOrder.HasValue && boxUpTask.IsExistShippingOrder.Value)
                            throw new ValidationException(ErrorStrings.PartsShippingOrder_Validation30);
                        boxUpTask.IsExistShippingOrder = true;
                        boxUpTask.ModifierId = userInfo.Id;
                        boxUpTask.ModifierName = userInfo.Name;
                        boxUpTask.ModifyTime = DateTime.Now;
                        UpdateToDatabase(boxUpTask);
                    }
                }
            }

            ObjectContext.SaveChanges();
            foreach(var item in partsOutboundPlans) {
                new PartsOutboundPlanAch(this).更新出库计划单状态(item);
            }
        }

        [Update(UsingCustomMethod = true)]
        public void 修改配件发运单(PartsShippingOrder partsShippingOrder) {
            //校验状态是否为新建
            CheckEntityState(partsShippingOrder);
            var dbpartsShippingOrder = ObjectContext.PartsShippingOrders.Where(r => r.Id == partsShippingOrder.Id && r.Status == (int)DcsPartsShippingOrderStatus.新建).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbpartsShippingOrder == null)
                throw new ValidationException(ErrorStrings.PartsShippingOrder_Validation1);
            //var partsShippingOrderRefs = ChangeSet.GetAssociatedChanges(partsShippingOrder, r => r.PartsShippingOrderRefs).Cast<PartsShippingOrderRef>().ToArray();
            //var insertShippingOrderRefs = partsShippingOrderRefs.Where(r => ChangeSet.GetChangeOperation(r) == ChangeOperation.Insert && partsShippingOrderRefs.Any(v => ChangeSet.GetChangeOperation(v) == ChangeOperation.Delete && v.PartsOutboundBillId == r.PartsOutboundBillId)).ToArray();
            //var deleteShippingOrderRefs = partsShippingOrderRefs.Where(r => ChangeSet.GetChangeOperation(r) == ChangeOperation.Delete && partsShippingOrderRefs.Any(v => ChangeSet.GetChangeOperation(v) == ChangeOperation.Insert && v.PartsOutboundBillId == r.PartsOutboundBillId)).ToArray();
            //var pendingpartsShippingOrderRefs = partsShippingOrderRefs.Except(insertShippingOrderRefs).Except(deleteShippingOrderRefs).ToArray();

            //查询配件物流批次单据清单 
            //var partsOutboundBillIds = pendingpartsShippingOrderRefs.Select(r => r.PartsOutboundBillId);
            //var partsLogisticBatchBillDetails = ObjectContext.PartsLogisticBatchBillDetails.Where(r => partsOutboundBillIds.Contains(r.BillId) && r.BillType == (int)DcsPartsLogisticBatchBillDetailBillType.配件出库单).ToArray();
            //查询配件物流批次单据清单和配件物流批次单共同的ID
            //var partsLogisticBatchIds = partsLogisticBatchBillDetails.Select(r => r.PartsLogisticBatchId);

            //var partsLogisticBatchIds = partsShippingOrder.PartsShippingOrderDetails.Select(r => r.TaskId);
            ////查询配件物流批次单
            //var partsLogisticBatches = ObjectContext.PartsLogisticBatches.Where(r => partsLogisticBatchIds.Contains(r.SourceId)).ToArray();

            //foreach(var partsShippingOrderRef in pendingpartsShippingOrderRefs) {
            //    var partsLogisticBatch = partsLogisticBatches.FirstOrDefault(r => partsLogisticBatchBillDetails.Any(v => v.BillId == partsShippingOrderRef.PartsOutboundBillId && v.BillType == (int)DcsPartsLogisticBatchBillDetailBillType.配件出库单 && v.PartsLogisticBatchId == r.Id));
            //    if(partsLogisticBatch == null)
            //        throw new ValidationException(ErrorStrings.PartsShippingOrder_Validation11);
            //    switch(ChangeSet.GetChangeOperation(partsShippingOrderRef)) {
            //        case ChangeOperation.Delete:
            //            partsLogisticBatch.ShippingStatus = (int)DcsPartsLogisticBatchShippingStatus.待运;
            //            new PartsLogisticBatchAch(this).UpdatePartsLogisticBatchValidate(partsLogisticBatch);
            //            break;
            //        case ChangeOperation.Insert:
            //            if(partsLogisticBatch.ShippingStatus != (int)DcsPartsLogisticBatchShippingStatus.待运)
            //                throw new ValidationException(ErrorStrings.PartsShippingOrder_Validation2);
            //            partsLogisticBatch.ShippingStatus = (int)DcsPartsLogisticBatchShippingStatus.发运;
            //            new PartsLogisticBatchAch(this).UpdatePartsLogisticBatchValidate(partsLogisticBatch);
            //            break;
            //    }

            //}


        }

        [Update(UsingCustomMethod = true)]
        public void 收货确认配件发运单(PartsShippingOrder partsShippingOrder) {
			 using(var transaction = new TransactionScope()) {
            //校验状态是否为新建 已发货 待提货 待收货
            var dbpartsShippingOrder = ObjectContext.PartsShippingOrders.Include("PartsInboundPlans").Where(r => r.Id == partsShippingOrder.Id && (r.Status == (int)DcsPartsShippingOrderStatus.新建 || r.Status == (int)DcsPartsShippingOrderStatus.已发货 || r.Status == (int)DcsPartsShippingOrderStatus.待提货 || r.Status == (int)DcsPartsShippingOrderStatus.待收货)).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbpartsShippingOrder == null)
                throw new ValidationException(ErrorStrings.PartsShippingOrder_Validation1);
            UpdateToDatabase(partsShippingOrder);
            partsShippingOrder.Status = (int)DcsPartsShippingOrderStatus.收货确认;
            var userInfo = Utils.GetCurrentUserInfo();
            partsShippingOrder.ConsigneeId = userInfo.Id;
            partsShippingOrder.ConsigneeName = userInfo.Name;
            partsShippingOrder.ConfirmedReceptionTime = DateTime.Now;
            if(partsShippingOrder.IsTransportLosses.HasValue && partsShippingOrder.IsTransportLosses.Value) {
                partsShippingOrder.TransportLossesDisposeStatus = (int)DcsPartsShippingOrderTransportLossesDisposeStatus.未处理;
            }
            UpdatePartsShippingOrderValidate(partsShippingOrder);
            var partsOutboundBills = ObjectContext.PartsOutboundBills.Where(r => r.PartsShippingOrderId == partsShippingOrder.Id).ToList();
            if(partsOutboundBills.Count == 0) {
                throw new ValidationException(ErrorStrings.PartsShippingOrder_Validation19);
            }
            var partsOutboundBillIds = partsOutboundBills.Select(r => r.Id);
            var partsSalesCategoryId = partsOutboundBills[0].PartsSalesCategory.Id;
            var partsSalesCategoryName = partsOutboundBills[0].PartsSalesCategory.Name;
            if(partsShippingOrder.ReceivingWarehouseId.HasValue && partsShippingOrder.PartsShippingOrderDetails.Sum(r => r.ConfirmedAmount) > 0) {
                //判断仓库
                var warehouse = ObjectContext.Warehouses.FirstOrDefault(r => r.Id == partsShippingOrder.ReceivingWarehouseId && r.Status == (int)DcsBaseDataStatus.有效);
                if(warehouse == null)
                    throw new ValidationException(ErrorStrings.PartsShippingOrder_Validation16);
                //查询企业
                var company = ObjectContext.Companies.FirstOrDefault(v => v.Id == warehouse.StorageCompanyId && v.Status == (int)DcsMasterDataStatus.有效);
                if(company == null)
                    throw new ValidationException(ErrorStrings.PartsShippingOrder_Validation4);
                //查询营销分公司
                var branch = ObjectContext.Branches.SingleOrDefault(r => r.Id == partsShippingOrder.BranchId && r.Status == (int)DcsMasterDataStatus.有效);
                if(branch == null)
                    throw new ValidationException(ErrorStrings.PartsShippingOrder_Validation5);
                if(!partsShippingOrder.OriginalRequirementBillId.HasValue)
                    throw new ValidationException(ErrorStrings.PartsShippingOrder_Validation9);
                if(!partsShippingOrder.OriginalRequirementBillType.HasValue)
                    throw new ValidationException(ErrorStrings.PartsShippingOrder_Validation10);

                var partsIds = partsShippingOrder.PartsShippingOrderDetails.Select(r => r.SparePartId).ToArray();

                var partsAmountDetails = new Dictionary<int, decimal>();
                if(company.Type == (int)DcsCompanyType.分公司 || company.Type == (int)DcsCompanyType.代理库 || company.Type == (int)DcsCompanyType.服务站兼代理库) {
                    partsAmountDetails = (from a in ObjectContext.EnterprisePartsCosts.Where(r => r.OwnerCompanyId == company.Id && r.PartsSalesCategoryId == partsShippingOrder.PartsSalesCategoryId && partsIds.Contains(r.SparePartId)).ToArray()
                                          select a).ToDictionary(r => r.SparePartId, r => r.CostPrice);
                }			
			    var partsOutboundPlanIds = partsShippingOrder.PartsShippingOrderDetails.Select(o => o.PartsOutboundPlanId).Distinct();
                var partsOutboundPlans = ObjectContext.PartsOutboundPlans.Where(r => partsOutboundPlanIds.Contains(r.Id));
				var outTraces = ObjectContext.AccurateTraces.Where(t => partsOutboundPlanIds.Contains(t.SourceBillId) && t.Type == (int)DCSAccurateTraceType.出库 && t.Status == (int)DCSAccurateTraceStatus.有效).ToArray();
                        var oldInTrace = ObjectContext.AccurateTraces.Where(t => t.CompanyId == company.Id && t.Type == (int)DCSAccurateTraceType.入库 && t.Status == (int)DCSAccurateTraceStatus.有效 && t.InQty==t.OutQty && t.OutQty>0).ToArray();
                if(!(partsShippingOrder.PartsSalesOrderTypeName == "三包垫件" && (company.Type != (int)DcsCompanyType.代理库 || company.Type != (int)DcsCompanyType.服务站兼代理库))) {
                    foreach(var partsOutboundPlan in partsOutboundPlans) {
                        var partsInboundPlan = new PartsInboundPlan {
                            WarehouseId = partsShippingOrder.ReceivingWarehouseId.Value,
                            WarehouseCode = partsShippingOrder.ReceivingWarehouseCode,
                            WarehouseName = partsShippingOrder.ReceivingWarehouseName,
                            StorageCompanyId = company.Id,
                            StorageCompanyCode = company.Code,
                            StorageCompanyName = company.Name,
                            StorageCompanyType = company.Type,
                            BranchId = partsShippingOrder.BranchId,
                            BranchCode = branch.Code,
                            BranchName = branch.Name,
                            CounterpartCompanyId = partsShippingOrder.SettlementCompanyId,
                            CounterpartCompanyCode = partsShippingOrder.SettlementCompanyCode,
                            CounterpartCompanyName = partsShippingOrder.SettlementCompanyName,
                            SourceId = partsShippingOrder.Id,
                            SourceCode = partsShippingOrder.Code,
                            Status = (int)DcsPartsInboundPlanStatus.新建,
                            OriginalRequirementBillId = partsOutboundPlan.OriginalRequirementBillId,
                            OriginalRequirementBillCode = partsOutboundPlan.OriginalRequirementBillCode,
                            OriginalRequirementBillType = partsOutboundPlan.OriginalRequirementBillType,
                            Remark = partsShippingOrder.Remark,
                            SAPPurchasePlanCode = partsShippingOrder.SAPPurchasePlanCode,
                            GPMSPurOrderCode = partsShippingOrder.GPMSPurOrderCode

                        };
						var companyType=(int)DCSAccurateTraceCompanyType.中心库;
						if(company.Type==(int)DcsCompanyType.分公司){
							companyType=(int)DCSAccurateTraceCompanyType.配件中心;
						}else if(company.Type==(int)DcsCompanyType.服务站){
							companyType=(int)DCSAccurateTraceCompanyType.服务站;
						}						
            
                        SalesUnit saleUnit;
                        Dictionary<int, string> POCodeRef = new Dictionary<int, string>();
                        switch(partsShippingOrder.Type) {
                            case (int)DcsPartsShippingOrderType.销售:
                                partsInboundPlan.InboundType = (int)DcsPartsInboundType.配件采购;
                                partsInboundPlan.PartsSalesCategoryId = partsSalesCategoryId;
                                break;
                            case (int)DcsPartsShippingOrderType.退货:
                                partsInboundPlan.InboundType = (int)DcsPartsInboundType.销售退货;
                                partsInboundPlan.PartsSalesCategoryId = partsSalesCategoryId;
                                saleUnit = ObjectContext.SalesUnits.FirstOrDefault(r => r.Status == (int)DcsBaseDataStatus.有效 && r.PartsSalesCategoryId == partsInboundPlan.PartsSalesCategoryId && r.OwnerCompanyId == partsInboundPlan.StorageCompanyId);
                                if(saleUnit == null) {
                                    throw new ValidationException(ErrorStrings.PartsShippingOrder_Validation17);
                                }
                                var customerAccount = ObjectContext.CustomerAccounts.FirstOrDefault(r => r.AccountGroupId == saleUnit.AccountGroupId && r.CustomerCompanyId == partsShippingOrder.ShippingCompanyId);
                                if(customerAccount == null) {
                                    throw new ValidationException(ErrorStrings.PartsShippingOrder_Validation31);
                                }
                                partsInboundPlan.CustomerAccountId = customerAccount.Id;
                                break;
                            case (int)DcsPartsShippingOrderType.调拨:
                                partsInboundPlan.InboundType = (int)DcsPartsInboundType.配件调拨;
                                saleUnit = ObjectContext.SalesUnits.FirstOrDefault(r => ObjectContext.SalesUnitAffiWarehouses.Any(v => v.SalesUnitId == r.Id && v.WarehouseId == partsShippingOrder.ReceivingWarehouseId) && r.Status == (int)DcsBaseDataStatus.有效);
                                if(saleUnit == null)
                                    throw new ValidationException(ErrorStrings.PartsShippingOrder_Validation17);
                                partsInboundPlan.PartsSalesCategoryId = saleUnit.PartsSalesCategoryId;
                                if(partsShippingOrder.OriginalRequirementBillType == (int)DcsOriginalRequirementBillType.配件调拨单) {
                                    var _POCodeRef = (from a in ObjectContext.PartsShippingOrderDetails
                                                      join b in ObjectContext.PartsShippingOrders on a.PartsShippingOrderId equals b.Id
                                                      join c in ObjectContext.PartsTransferOrders on b.OriginalRequirementBillId equals c.Id
                                                      join d in ObjectContext.PartsTransferOrderDetails on new {
                                                          a.SparePartId,
                                                          c.Id
                                                      } equals new {
                                                          d.SparePartId,
                                                          Id = d.PartsTransferOrderId
                                                      }
                                                      where b.Id == partsShippingOrder.Id
                                                      select new {
                                                          d.SparePartId,
                                                          d.POCode
                                                      }).ToList();
                                    _POCodeRef.ForEach(i => {
                                        if(!POCodeRef.ContainsKey(i.SparePartId))
                                            POCodeRef.Add(i.SparePartId, i.POCode);
                                    });
                                }
                                break;
                            case (int)DcsPartsShippingOrderType.质量件索赔:
                                partsInboundPlan.InboundType = (int)DcsPartsInboundType.质量件索赔;
                                partsInboundPlan.PartsSalesCategoryId = partsSalesCategoryId;

                                saleUnit = ObjectContext.SalesUnits.FirstOrDefault(r => r.Status == (int)DcsBaseDataStatus.有效 && r.PartsSalesCategoryId == partsInboundPlan.PartsSalesCategoryId && r.OwnerCompanyId == partsInboundPlan.StorageCompanyId);
                                if(saleUnit == null) {
                                    throw new ValidationException(ErrorStrings.PartsShippingOrder_Validation17);
                                }
                                customerAccount = ObjectContext.CustomerAccounts.FirstOrDefault(r => r.AccountGroupId == saleUnit.AccountGroupId && r.CustomerCompanyId == partsShippingOrder.ShippingCompanyId);
                                if(customerAccount == null) {
                                    throw new ValidationException(ErrorStrings.PartsShippingOrder_Validation31);
                                }
                                partsInboundPlan.CustomerAccountId = customerAccount.Id;

                                break;

                            case (int)DcsPartsShippingOrderType.调剂:
                                partsInboundPlan.InboundType = (int)DcsPartsInboundType.积压件调剂;
                                partsInboundPlan.PartsSalesCategoryId = partsSalesCategoryId;
                                break;
                            default:
                                throw new ValidationException(ErrorStrings.PartsShippingOrder_Validation3);
                        }
                        //查询入库计划单清单
                        foreach(var partsShippingOrderDetail in partsShippingOrder.PartsShippingOrderDetails.Where(o => o.PartsOutboundPlanId == partsOutboundPlan.Id)) {
                            var partsOutboundPlanDetail = partsOutboundPlan.PartsOutboundPlanDetails.FirstOrDefault(o => o.SparePartId == partsShippingOrderDetail.SparePartId && o.PartsOutboundPlanId == partsShippingOrderDetail.PartsOutboundPlanId && o.Price == partsShippingOrderDetail.SettlementPrice);
                            if(!partsShippingOrderDetail.ConfirmedAmount.HasValue)
                                throw new ValidationException(string.Format(ErrorStrings.PartsShippingOrder_Validation8, partsShippingOrderDetail.SparePartCode));
                            if(partsShippingOrderDetail.ConfirmedAmount > 0) {
                                var partsInboundPlanDetail = partsInboundPlan.PartsInboundPlanDetails.FirstOrDefault(o => o.PartsInboundPlanId == partsInboundPlan.Id && o.SparePartId == partsShippingOrderDetail.SparePartId && o.Price == partsShippingOrderDetail.SettlementPrice);
                                if(partsInboundPlanDetail != null) {
                                    partsInboundPlanDetail.PlannedAmount += partsShippingOrderDetail.ConfirmedAmount.Value;
                                } else {
                                    partsInboundPlanDetail = new PartsInboundPlanDetail {
                                        PartsInboundPlanId = partsInboundPlan.Id,
                                        SparePartId = partsShippingOrderDetail.SparePartId,
                                        SparePartCode = partsShippingOrderDetail.SparePartCode,
                                        SparePartName = partsShippingOrderDetail.SparePartName,
                                        PlannedAmount = partsShippingOrderDetail.ConfirmedAmount.Value,
                                        Price = partsShippingOrderDetail.SettlementPrice,
                                        Remark = partsShippingOrderDetail.Remark,
                                        POCode = POCodeRef.Keys.Contains(partsShippingOrderDetail.SparePartId) ? POCodeRef[partsShippingOrderDetail.SparePartId] : null,
                                        OriginalPrice = partsOutboundPlanDetail.OriginalPrice
                                    };
                                    if(partsShippingOrder.Type == (int)DcsPartsShippingOrderType.调拨) {
                                        if(company.Type == (int)DcsCompanyType.分公司 || company.Type == (int)DcsCompanyType.代理库 || company.Type == (int)DcsCompanyType.服务站兼代理库) {
                                            //填充配件出库单清单
                                            if(partsAmountDetails.ContainsKey(partsShippingOrderDetail.SparePartId)) {
                                                partsInboundPlanDetail.Price = partsAmountDetails[partsShippingOrderDetail.SparePartId];
                                            }
                                        }
                                    }
                                    partsInboundPlan.PartsInboundPlanDetails.Add(partsInboundPlanDetail);
                                }
                            }
                        }                      
                        InsertToDatabase(partsInboundPlan);
                        new PartsInboundPlanAch(this).InsertPartsInboundPlanValidate(partsInboundPlan);
						ObjectContext.SaveChanges();
						//新增收货单位入库追溯                   
                        //中心库的零售订单不需要新增 收货单位的入库【精确码追溯】数据
                        //若收货企业已存在此精确码的【精确码追溯】有效入库信息，则作废
                        //当发运为配件公司的采购退货时，需将配件公司原入库【精确码追溯】的状态改为“不考核”
                        if(outTraces.Any()) {
							 var outTrace=outTraces.Where(t=>t.SourceBillId==partsOutboundPlan.Id).ToArray();
							 if(outTrace.Any()){
                            foreach(var outDetail in outTrace) {
                                if(!string.IsNullOrWhiteSpace(outDetail.TraceCode)) {
                                    var UnKh = oldInTrace.Where(t => t.TraceCode==outDetail.TraceCode && t.InQty == t.OutQty && t.InQty > 0).FirstOrDefault();
                                    if(UnKh != null) {
                                        UnKh.Status = (int)DCSAccurateTraceStatus.作废;
                                        UnKh.ModifierId = userInfo.Id;
                                        UnKh.ModifierName = userInfo.Name;
                                        UnKh.ModifyTime = DateTime.Now;
                                        UpdateToDatabase(UnKh);
                                    }
                                }
                                var inBound = new AccurateTrace {
                                    Type = (int)DCSAccurateTraceType.入库,
                                    CompanyType = companyType,
                                    CompanyId = company.Id,
                                    SourceBillId = partsInboundPlan.Id,
                                    PartId = outDetail.PartId,
                                    TraceCode = outDetail.TraceCode,
                                    //SIHLabelCode=sIHLabelCode,
                                    Status = (int)DCSAccurateTraceStatus.有效,
                                    TraceProperty = outDetail.TraceProperty,
                                };
                                if(company.Type == (int)DcsCompanyType.代理库 || company.Type == (int)DcsCompanyType.服务站) {
                                    inBound.SIHLabelCode = outDetail.SIHLabelCode;
                                    inBound.BoxCode = outDetail.BoxCode;
                                    if(outDetail.TraceProperty.HasValue &&outDetail.TraceProperty == (int)DCSTraceProperty.精确追溯) {
                                        inBound.InQty = 1;
                                    } else {
                                        inBound.InQty = outDetail.OutQty;
                                    }
                                }
                                new AccurateTraceAch(this).ValidataAccurateTrace(inBound);
                            }
							 }
                        }
                        //查询配件物流批次

                        var partsLogisticBatches = ObjectContext.PartsLogisticBatches.Where(v => ObjectContext.PartsLogisticBatchBillDetails.Any(r => partsOutboundBillIds.Contains(r.BillId) && r.PartsLogisticBatchId == v.Id && r.BillType == (int)DcsPartsLogisticBatchBillDetailBillType.配件出库单)).ToArray();
                        foreach(var partsLogisticBatch in partsLogisticBatches) {
                            UpdateToDatabase(partsLogisticBatch);
                            var partsLogisticBatchBillDetail = new PartsLogisticBatchBillDetail {
                                BillType = (int)DcsPartsLogisticBatchBillDetailBillType.配件入库计划
                            };
                            partsLogisticBatch.PartsLogisticBatchBillDetails.Add(partsLogisticBatchBillDetail);
                            partsInboundPlan.PartsLogisticBatchBillDetails.Add(partsLogisticBatchBillDetail);
                            new PartsLogisticBatchAch(this).InsertPartsLogisticBatchValidate(partsLogisticBatch);
                        }
                    }
                }
            }

            //查询企业
            var receivingCompany = ObjectContext.Companies.FirstOrDefault(v => v.Id == partsShippingOrder.ReceivingCompanyId && v.Status == (int)DcsMasterDataStatus.有效);
            if(receivingCompany == null)
                throw new ValidationException(ErrorStrings.PartsShippingOrder_ReceivingCompany_NotExist);
            if(!partsShippingOrder.ReceivingWarehouseId.HasValue && partsShippingOrder.PartsShippingOrderDetails.Sum(r => r.ConfirmedAmount) > 0
                && !(partsShippingOrder.PartsSalesOrderTypeName == "三包垫件" && receivingCompany.Type == (int)DcsCompanyType.服务站)) {
                var partIds = partsShippingOrder.PartsShippingOrderDetails.Select(r => r.SparePartId).Distinct().ToList();
                var dealerPartsStocks = ObjectContext.DealerPartsStocks.Where(r => r.SubDealerId == -1 && r.DealerId == partsShippingOrder.ReceivingCompanyId && partIds.Contains(r.SparePartId)).ToArray();
                var saleunitdealerPartsStock = dealerPartsStocks.FirstOrDefault();
                var partsOutboundPlanIds = partsShippingOrder.PartsShippingOrderDetails.Select(o => o.PartsOutboundPlanId).Distinct();
                var newGroupPartsShippingOrderDetails = from psod in partsShippingOrder.PartsShippingOrderDetails
                                                        group psod by new {
                                                            psod.SparePartId,
                                                            psod.SparePartCode
                                                        } into g
                                                        select new {
                                                            SparePartId = g.Key.SparePartId,
                                                            SparePartCode = g.Key.SparePartCode,
                                                            ConfirmedAmount = g.Sum(o => o.ConfirmedAmount)
                                                        };

                foreach(var partsShippingOrderDetail in newGroupPartsShippingOrderDetails) {
                    if(!partsShippingOrderDetail.ConfirmedAmount.HasValue)
                        throw new ValidationException(string.Format(ErrorStrings.PartsShippingOrder_Validation8, partsShippingOrderDetail.SparePartCode));
                    var dealerPartsStock = dealerPartsStocks.FirstOrDefault(r => r.SparePartId == partsShippingOrderDetail.SparePartId);
                    var dbpartsOutboundBill = (from partsout in ObjectContext.PartsOutboundBills
                                               where partsout.PartsShippingOrderId == partsShippingOrder.Id
                                               select partsout).FirstOrDefault();
                    var dbPartDeleaveInformation = ObjectContext.PartDeleaveInformations.SingleOrDefault(c => c.OldPartId == partsShippingOrderDetail.SparePartId && c.PartsSalesCategoryId == dbpartsOutboundBill.PartsSalesCategoryId && c.Status == (int)DcsBaseDataStatus.有效);
                    if(dealerPartsStock != null) {
                        UpdateToDatabase(dealerPartsStock);
                        if(dbPartDeleaveInformation != null) {
                            dealerPartsStock.Quantity += partsShippingOrderDetail.ConfirmedAmount.Value * dbPartDeleaveInformation.DeleaveAmount;
                        } else {
                            dealerPartsStock.Quantity += partsShippingOrderDetail.ConfirmedAmount.Value;
                        }
                        new DealerPartsStockAch(this).UpdateDealerPartsStockValidate(dealerPartsStock);
                    } else {
                        var newdealerPartsStock = new DealerPartsStock {
                            DealerId = partsShippingOrder.ReceivingCompanyId,
                            DealerCode = partsShippingOrder.ReceivingCompanyCode,
                            DealerName = partsShippingOrder.ReceivingCompanyName,
                            BranchId = partsShippingOrder.BranchId,
                            SalesCategoryId = partsSalesCategoryId,
                            SalesCategoryName = partsSalesCategoryName,
                            SparePartId = partsShippingOrderDetail.SparePartId,
                            SparePartCode = partsShippingOrderDetail.SparePartCode,
                            Quantity = dbPartDeleaveInformation == null ? partsShippingOrderDetail.ConfirmedAmount.Value : dbPartDeleaveInformation.DeleaveAmount * partsShippingOrderDetail.ConfirmedAmount.Value,
                            SubDealerId = -1
                        };
                        InsertToDatabase(newdealerPartsStock);
                        new DealerPartsStockAch(this).InsertDealerPartsStockValidate(newdealerPartsStock);
                    }
                }
				 //新增收货单位入库追溯
                    var outTrace = ObjectContext.AccurateTraces.Where(t => partsOutboundPlanIds.Contains(t.SourceBillId) && t.Type == (int)DCSAccurateTraceType.出库 && t.Status == (int)DCSAccurateTraceStatus.有效).ToArray();
                    var oldInTrace = ObjectContext.AccurateTraces.Where(t => t.CompanyId == receivingCompany.Id && t.Type == (int)DCSAccurateTraceType.入库 && t.Status == (int)DCSAccurateTraceStatus.有效&& t.OutQty>0 && t.InQty==t.OutQty).ToArray();
                    //中心库的零售订单不需要新增 收货单位的入库【精确码追溯】数据
                    //若收货企业已存在此精确码的【精确码追溯】有效入库信息，则作废
                    if(outTrace.Any()) {
                        foreach(var outDetail in outTrace) {
                            var UnKh = oldInTrace.Where(t => t.TraceCode.Equals(outDetail.TraceCode)).FirstOrDefault();
                            if(UnKh != null) {
                                UnKh.Status = (int)DCSAccurateTraceStatus.作废;
                                UnKh.ModifierId = userInfo.Id;
                                UnKh.ModifierName = userInfo.Name;
                                UnKh.ModifyTime = DateTime.Now;
                                UpdateToDatabase(UnKh);
                            }
                            var inBound = new AccurateTrace {
                                Type = (int)DCSAccurateTraceType.入库,
                                CompanyType = (int)DCSAccurateTraceCompanyType.服务站,
                                CompanyId = receivingCompany.Id,
                                SourceBillId = partsShippingOrder.Id,
                                PartId = outDetail.PartId,
                                TraceCode = outDetail.TraceCode,
                                SIHLabelCode = outDetail.SIHLabelCode,
                                Status = (int)DCSAccurateTraceStatus.有效,
                                TraceProperty = outDetail.TraceProperty,
                                InQty = outDetail.OutQty,
								BoxCode=outDetail.BoxCode
                            };
                            new AccurateTraceAch(this).ValidataAccurateTrace(inBound);
                        }
                    }
                }
            if(partsShippingOrder.Type == (int)DcsPartsShippingOrderType.质量件索赔) {

                //更新配件索赔单（新）（配件索赔单（新）.配件索赔单编号=配件发运单.原始需求单据编号） 状态=生效，修改人=当前登陆人，修改时间=当前登陆时间，
                var partsClaimOrderNew = ObjectContext.PartsClaimOrderNews.FirstOrDefault(r => r.Code == partsShippingOrder.OriginalRequirementBillCode);
                if(partsClaimOrderNew == null) {
                    throw new ValidationException(ErrorStrings.PartsShippingOrder_Validation32);
                }
                UpdateToDatabase(partsClaimOrderNew);
                partsClaimOrderNew.Status = (int)DcsPartsClaimBillStatusNew.入库完成;
                new PartsClaimOrderNewAch(this).UpdatePartsClaimOrderNewValidate(partsClaimOrderNew);
            }

            //var partSalesCategory = this.ObjectContext.PartsSalesCategories.Where(r => r.Name == "欧曼电商").FirstOrDefault();
            //if(partSalesCategory != null && partSalesCategory.Id == partsShippingOrder.PartsSalesCategoryId) {
            //查找运费处理单  更新其状态和出库单ID和Code
            //查询销售订单获取随车行处理单Id  根据随车行处理单ID查询运费处理单
            var partsSalesOrder = ObjectContext.PartsSalesOrders.Where(r => r.Code == partsShippingOrder.OriginalRequirementBillCode).FirstOrDefault();
            //var partsOutboundBills = ObjectContext.PartsOutboundBills.Where(o => o.PartsShippingOrderId == partsShippingOrder.Id).ToArray();
            if(partsSalesOrder != null && partsSalesOrder.VehiclePartsHandleOrderId.HasValue) {
                var ecommerceFreightDisposal = this.ObjectContext.EcommerceFreightDisposals.Where(r => r.VehiclePartsHandleOrderId == partsSalesOrder.VehiclePartsHandleOrderId.Value && r.ERPSourceOrderCode == partsSalesOrder.ERPSourceOrderCode).FirstOrDefault();
                if(ecommerceFreightDisposal != null) {
                    if(ecommerceFreightDisposal.Status == default(int))
                        ecommerceFreightDisposal.Status = (int)DcsFreightDisposalStatus.待结算;
                    if(ecommerceFreightDisposal.SalesSettlementStatus == null)
                        ecommerceFreightDisposal.SalesSettlementStatus = (int)DcsFreightDisposalStatus.待结算;

                    if(ecommerceFreightDisposal.PartsOutboundBillId == null)
                        ecommerceFreightDisposal.PartsOutboundBillId = partsOutboundBills.Min(r => r.Id);

                    //查询出库单
                    var PartsOutboundBill = this.ObjectContext.PartsOutboundBills.FirstOrDefault(r => r.Id == ecommerceFreightDisposal.PartsOutboundBillId);
                    if(PartsOutboundBill != null && string.IsNullOrEmpty(ecommerceFreightDisposal.PartsOutboundBillCode))
                        ecommerceFreightDisposal.PartsOutboundBillCode = PartsOutboundBill.Code;

                    UpdateToDatabase(ecommerceFreightDisposal);
                }

            }
            //}
			 ObjectContext.SaveChanges();
			 transaction.Complete();
        }
        }

        [Update(UsingCustomMethod = true)]
        public void 回执确认配件发运单(PartsShippingOrder partsShippingOrder) {
            var dbpartsShippingOrder = ObjectContext.PartsShippingOrders.Where(r => r.Id == partsShippingOrder.Id && r.Status == (int)DcsPartsShippingOrderStatus.收货确认).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbpartsShippingOrder == null)
                throw new ValidationException(ErrorStrings.PartsShippingOrder_Validation6);
            UpdateToDatabase(partsShippingOrder);
            partsShippingOrder.Status = (int)DcsPartsShippingOrderStatus.回执确认;
            var userInfo = Utils.GetCurrentUserInfo();
            partsShippingOrder.ReceiptConfirmorId = userInfo.Id;
            partsShippingOrder.ReceiptConfirmorName = userInfo.Name;
            partsShippingOrder.ReceiptConfirmTime = DateTime.Now;
            UpdatePartsShippingOrderValidate(partsShippingOrder);

        }

        [Update(UsingCustomMethod = true)]
        public void 作废配件发运单(PartsShippingOrder partsShippingOrder) {
            var userInfo = Utils.GetCurrentUserInfo();
            var dbpartsShippingOrder = ObjectContext.PartsShippingOrders.Where(r => r.Id == partsShippingOrder.Id && r.Status == (int)DcsPartsShippingOrderStatus.新建).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbpartsShippingOrder == null)
                throw new ValidationException(ErrorStrings.PartsShippingOrder_Validation1);
            //查询配件物流批次
            //var partsOutboundBillIds = partsShippingOrder.PartsShippingOrderRefs.Select(r => r.PartsOutboundBillId);
            //var partsLogisticBatches = ObjectContext.PartsLogisticBatches.Where(v => ObjectContext.PartsLogisticBatchBillDetails.Any(r => partsOutboundBillIds.Contains(r.BillId) && r.PartsLogisticBatchId == v.Id && r.BillType == (int)DcsPartsLogisticBatchBillDetailBillType.配件出库单)).ToArray();
            var partsLogisticBatchIds = partsShippingOrder.PartsShippingOrderDetails.Select(r => r.TaskId);
            var partsLogisticBatches = ObjectContext.PartsLogisticBatches.Where(r => partsLogisticBatchIds.Contains(r.SourceId) && r.SourceType == (int)DcsPartsLogisticBatchSourceType.配件出库计划).ToArray();
            UpdateToDatabase(partsShippingOrder);
            partsShippingOrder.Status = (int)DcsPartsShippingOrderStatus.作废;
            UpdatePartsShippingOrderValidate(partsShippingOrder);
            foreach(var partsLogisticBatch in partsLogisticBatches) {
                partsLogisticBatch.ShippingStatus = (int)DcsPartsLogisticBatchShippingStatus.待运;
                new PartsLogisticBatchAch(this).UpdatePartsLogisticBatchValidate(partsLogisticBatch);
            }


            var pickingTaskIds = partsShippingOrder.PartsShippingOrderDetails.Where(o => o.TaskType == 1).Select(r => r.TaskId).ToArray();
            var boxUpTaskIds = partsShippingOrder.PartsShippingOrderDetails.Where(o => o.TaskType == 2).Select(r => r.TaskId).ToArray();
            var pickingTasks = ObjectContext.PickingTasks.Where(r => pickingTaskIds.Contains(r.Id)).ToArray();
            var boxUpTasks = ObjectContext.BoxUpTasks.Where(r => boxUpTaskIds.Contains(r.Id)).ToArray();
            var partsOutboundPlanIds = partsShippingOrder.PartsShippingOrderDetails.Select(r => r.PartsOutboundPlanId).ToArray();
            var partsOutboundPlans = ObjectContext.PartsOutboundPlans.Where(o => partsOutboundPlanIds.Contains(o.Id)).ToArray();
            BoxUpTask boxUpTask = null;
            PickingTask pickingTask = null;
            foreach(var partsShippingOrderDetail in partsShippingOrder.PartsShippingOrderDetails) {
                if(partsShippingOrderDetail.TaskType == 1) {
                    if(pickingTask == null || pickingTask.Id != partsShippingOrderDetail.TaskId) {
                        pickingTask = pickingTasks.FirstOrDefault(o => o.Id == partsShippingOrderDetail.TaskId);
                        pickingTask.IsExistShippingOrder = false;
                        pickingTask.ModifierId = userInfo.Id;
                        pickingTask.ModifierName = userInfo.Name;
                        pickingTask.ModifyTime = DateTime.Now;
                        UpdateToDatabase(pickingTask);
                    }
                } else {
                    if(boxUpTask == null || boxUpTask.Id != partsShippingOrderDetail.TaskId) {
                        boxUpTask = boxUpTasks.FirstOrDefault(r => r.Id == partsShippingOrderDetail.TaskId);
                        boxUpTask.IsExistShippingOrder = false;
                        boxUpTask.ModifierId = userInfo.Id;
                        boxUpTask.ModifierName = userInfo.Name;
                        boxUpTask.ModifyTime = DateTime.Now;
                        UpdateToDatabase(boxUpTask);
                    }
                }
            }

            this.ObjectContext.SaveChanges();
            foreach(var item in partsOutboundPlans) {
                new PartsOutboundPlanAch(this).更新出库计划单状态(item);
            }
            //var partsShippingOrderRefs = ObjectContext.PartsShippingOrderRefs.Where(r => r.PartsShippingOrderId == partsShippingOrder.Id).ToArray();
            //foreach(var p in partsShippingOrderRefs) {
            //    DeleteFromDatabase(p);
            //}

        }

        [Update(UsingCustomMethod = true)]
        public void 路损处理(PartsShippingOrder partsShippingOrder) {
            var i = 0;
            var dbpartsShippingOrder = ObjectContext.PartsShippingOrders.Where(r => r.Id == partsShippingOrder.Id && r.Status == (int)DcsPartsShippingOrderStatus.收货确认 && r.IsTransportLosses == true && r.TransportLossesDisposeStatus == (int)DcsPartsShippingOrderTransportLossesDisposeStatus.未处理).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbpartsShippingOrder == null)
                throw new ValidationException(ErrorStrings.PartsShippingOrder_Validation12);
            var partsShippingOrderDetails = partsShippingOrder.PartsShippingOrderDetails.ToArray();
            var partsOutboundPlanIds = partsShippingOrderDetails.Where(r => r.TransportLossesDisposeMethod.HasValue && r.InTransitDamageLossAmount.Value > 0).Select(o => o.PartsOutboundPlanId);
            //查询出库计划
            var partsOutboundPlans = ObjectContext.PartsOutboundPlans.Where(o => partsOutboundPlanIds.Contains(o.Id));
            //查询校验出库单
            var partsOutboundBill = ObjectContext.PartsOutboundBills.FirstOrDefault(r => r.PartsShippingOrderId == dbpartsShippingOrder.Id);
            if(partsOutboundBill == null)
                throw new ValidationException(ErrorStrings.PartsShippingOrder_Validation11);
            var partsSalesOrders = ObjectContext.PartsSalesOrders.Where(r => ObjectContext.PartsOutboundPlans.Any(v => v.OriginalRequirementBillId == r.Id && partsOutboundPlanIds.Contains(v.Id)));
            if(partsSalesOrders.Count() == 0)
                throw new ValidationException(ErrorStrings.PartsShippingOrder_Validation14);

            var branch = ObjectContext.Branches.SingleOrDefault(r => r.Id == partsShippingOrder.BranchId);
            if(branch == null)
                throw new ValidationException(ErrorStrings.PartsShippingOrder_Validation5);

            var salesUnitIds = partsSalesOrders.Select(o => o.SalesUnitId);
            var saleUnits = ObjectContext.SalesUnits.Where(r => salesUnitIds.Contains(r.Id) && r.Status != (int)DcsMasterDataStatus.作废);
            if(saleUnits.Count() == 0)
                throw new ValidationException(ErrorStrings.PartsShippingOrder_Validation15);
            #region 销售退回
            if(partsShippingOrderDetails.Any(r => r.TransportLossesDisposeMethod == (int)DcsPartsShippingOrderDetailTransportLossesDispose.销售退回 && r.InTransitDamageLossAmount.Value > 0)) {
                var partsOutboundPlanReturnId = partsShippingOrderDetails.Where(r => r.TransportLossesDisposeMethod == (int)DcsPartsShippingOrderDetailTransportLossesDispose.销售退回 && r.InTransitDamageLossAmount.Value > 0).Select(o => o.PartsOutboundPlanId);
                foreach(var partsOutboundPlan in partsOutboundPlans.Where(o => partsOutboundPlanReturnId.Contains(o.Id))) {
                    var partsSalesOrder = partsSalesOrders.FirstOrDefault(o => o.Id == partsOutboundPlan.OriginalRequirementBillId);
                    //判断清单在出库清单中是否存在
                    var partsOutboundBillforcheck = ObjectContext.PartsOutboundBills.FirstOrDefault(r => r.PartsShippingOrderId == partsShippingOrder.Id && r.PartsOutboundPlanId == partsOutboundPlan.Id);
                    //配件销售退货单赋值
                    var partsSalesReturnBill = new PartsSalesReturnBill();
                    partsSalesReturnBill.Code = CodeGenerator.Generate("PartsSalesReturnBill", partsSalesOrder.SalesUnitOwnerCompanyCode);
                    partsSalesReturnBill.ReturnCompanyId = partsShippingOrder.ReceivingCompanyId;
                    partsSalesReturnBill.ReturnCompanyCode = partsShippingOrder.ReceivingCompanyCode;
                    partsSalesReturnBill.ReturnCompanyName = partsShippingOrder.ReceivingCompanyName;
                    partsSalesReturnBill.InvoiceReceiveCompanyId = partsSalesOrder.InvoiceReceiveCompanyId;
                    partsSalesReturnBill.InvoiceReceiveCompanyCode = partsSalesOrder.InvoiceReceiveCompanyCode;
                    partsSalesReturnBill.InvoiceReceiveCompanyName = partsSalesOrder.InvoiceReceiveCompanyName;
                    partsSalesReturnBill.SubmitCompanyId = partsShippingOrder.ReceivingCompanyId;
                    partsSalesReturnBill.SubmitCompanyCode = partsShippingOrder.ReceivingCompanyCode;
                    partsSalesReturnBill.SubmitCompanyName = partsShippingOrder.ReceivingCompanyName;
                    partsSalesReturnBill.SalesUnitId = partsSalesOrder.SalesUnitId;
                    partsSalesReturnBill.SalesUnitName = partsSalesOrder.SalesUnitName;
                    partsSalesReturnBill.SalesUnitOwnerCompanyId = partsSalesOrder.SalesUnitOwnerCompanyId;
                    partsSalesReturnBill.SalesUnitOwnerCompanyCode = partsSalesOrder.SalesUnitOwnerCompanyCode;
                    partsSalesReturnBill.SalesUnitOwnerCompanyName = partsSalesOrder.SalesUnitOwnerCompanyName;
                    partsSalesReturnBill.CustomerAccountId = partsSalesOrder.CustomerAccountId;
                    partsSalesReturnBill.BranchId = partsShippingOrder.BranchId;
                    partsSalesReturnBill.BranchCode = branch.Code;
                    partsSalesReturnBill.BranchName = branch.Name;
                    partsSalesReturnBill.InvoiceRequirement = (int)DcsPartsSalesReturnBillInvoiceRequirement.出库合并开票;
                    partsSalesReturnBill.WarehouseId = partsOutboundBillforcheck.WarehouseId;
                    partsSalesReturnBill.WarehouseCode = partsOutboundBillforcheck.WarehouseCode;
                    partsSalesReturnBill.WarehouseName = partsOutboundBillforcheck.WarehouseName;
                    partsSalesReturnBill.Status = (int)DcsPartsSalesReturnBillStatus.审批通过;
                    partsSalesReturnBill.ReturnType = (int)DcsPartsSalesReturnBillReturnType.特殊退货;
                    //配件入库计划赋值
                    var saleBackPartsInboundPlan = new PartsInboundPlan();
                    saleBackPartsInboundPlan.WarehouseId = partsOutboundBillforcheck.WarehouseId;
                    saleBackPartsInboundPlan.WarehouseCode = partsOutboundBillforcheck.WarehouseCode;
                    saleBackPartsInboundPlan.WarehouseName = partsOutboundBillforcheck.WarehouseName;
                    saleBackPartsInboundPlan.StorageCompanyId = partsShippingOrder.ShippingCompanyId;
                    saleBackPartsInboundPlan.StorageCompanyCode = partsShippingOrder.ShippingCompanyCode;
                    saleBackPartsInboundPlan.StorageCompanyName = partsShippingOrder.ShippingCompanyName;
                    saleBackPartsInboundPlan.StorageCompanyType = partsOutboundBillforcheck.StorageCompanyType;
                    saleBackPartsInboundPlan.BranchId = partsShippingOrder.BranchId;
                    saleBackPartsInboundPlan.BranchCode = branch.Code;
                    saleBackPartsInboundPlan.BranchName = branch.Name;
                    saleBackPartsInboundPlan.PartsSalesCategoryId = partsOutboundBillforcheck.PartsSalesCategoryId;
                    saleBackPartsInboundPlan.CounterpartCompanyId = partsShippingOrder.ReceivingCompanyId;
                    saleBackPartsInboundPlan.CounterpartCompanyCode = partsShippingOrder.ReceivingCompanyCode;
                    saleBackPartsInboundPlan.CounterpartCompanyName = partsShippingOrder.ReceivingCompanyName;
                    saleBackPartsInboundPlan.InboundType = (int)DcsPartsInboundType.销售退货;
                    saleBackPartsInboundPlan.CustomerAccountId = partsSalesOrder.CustomerAccountId;
                    //  saleBackPartsInboundPlan.OriginalRequirementBillId = partsSalesReturnBill.Id;
                    saleBackPartsInboundPlan.OriginalRequirementBillCode = partsSalesReturnBill.Code;
                    saleBackPartsInboundPlan.OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.配件销售退货单;
                    saleBackPartsInboundPlan.Status = (int)DcsPartsInboundPlanStatus.新建;
                    //销售退货，入库计划清单赋值
                    foreach(var partsShippingOrderDetail in partsShippingOrderDetails.Where(r => r.TransportLossesDisposeMethod == (int)DcsPartsShippingOrderDetailTransportLossesDispose.销售退回 && r.InTransitDamageLossAmount.Value > 0 && r.PartsOutboundPlanId == partsOutboundPlan.Id)) {
                        var partsOutboundBillDetailforcheck = partsOutboundBillforcheck.PartsOutboundBillDetails.FirstOrDefault(r => r.SparePartId == partsShippingOrderDetail.SparePartId);
                        if(partsOutboundBillDetailforcheck == null)
                            throw new ValidationException(ErrorStrings.PartsShippingOrder_Validation18);
                        var partsOutboundPlanDetail = partsOutboundPlan.PartsOutboundPlanDetails.FirstOrDefault(o => o.SparePartId == partsShippingOrderDetail.SparePartId);
                        //销售退货清单赋值
                        var partsSalesReturnBillDetail = new PartsSalesReturnBillDetail {
                            PartsSalesReturnBillId = partsShippingOrderDetail.Id,
                            SparePartId = partsShippingOrderDetail.SparePartId,
                            SparePartCode = partsShippingOrderDetail.SparePartCode,
                            SparePartName = partsShippingOrderDetail.SparePartName,
                            ReturnedQuantity = partsShippingOrderDetail.InTransitDamageLossAmount.Value,
                            ApproveQuantity = partsShippingOrderDetail.InTransitDamageLossAmount,
                            OriginalOrderPrice = partsShippingOrderDetail.SettlementPrice,
                            ReturnPrice = partsShippingOrderDetail.SettlementPrice,
                            BatchNumber = partsOutboundBillDetailforcheck.BatchNumber != null ? partsOutboundBillDetailforcheck.BatchNumber : string.Empty,
                            OriginalPrice = partsOutboundPlanDetail.OriginalPrice,
                            PartsSalesOrderId = partsSalesOrder.Id,
                            OriginalRequirementBillId = partsSalesOrder.Id,
                            OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.配件销售订单,
                            PartsSalesOrderCode = partsSalesOrder.Code
                        };
                        partsSalesReturnBill.PartsSalesReturnBillDetails.Add(partsSalesReturnBillDetail);
                        var partsInboundPlanDetail = new PartsInboundPlanDetail {
                            PartsInboundPlanId = saleBackPartsInboundPlan.Id,
                            SparePartId = partsShippingOrderDetail.SparePartId,
                            SparePartCode = partsShippingOrderDetail.SparePartCode,
                            SparePartName = partsShippingOrderDetail.SparePartName,
                            PlannedAmount = partsShippingOrderDetail.InTransitDamageLossAmount.Value,
                            InspectedQuantity = 0,
                            Price = partsShippingOrderDetail.SettlementPrice,
                            OriginalPrice = partsOutboundPlanDetail.OriginalPrice
                        };
                        saleBackPartsInboundPlan.PartsInboundPlanDetails.Add(partsInboundPlanDetail);
                    }
                    partsSalesReturnBill.TotalAmount = partsSalesReturnBill.PartsSalesReturnBillDetails.Sum(r => r.ReturnPrice * r.ApproveQuantity.Value);
                    new PartsInboundPlanAch(this).InsertPartsInboundPlanValidate(saleBackPartsInboundPlan);
                    saleBackPartsInboundPlan.PartsSalesReturnBill = partsSalesReturnBill;
                    //给入库计划的原始需求单据id赋值
                    saleBackPartsInboundPlan.PartsSalesReturnBillOriginal = partsSalesReturnBill;
                    saleBackPartsInboundPlan.SourceCode = partsSalesReturnBill.Code;
                    InsertToDatabase(partsSalesReturnBill);
                    new PartsSalesReturnBillAch(this).InsertPartsSalesReturnBillValidate(partsSalesReturnBill);
                    partsSalesReturnBill.PartsInboundPlans.Add(saleBackPartsInboundPlan);
                }
            }
            #endregion
            #region 正常收货
            if(partsShippingOrderDetails.Any(r => r.TransportLossesDisposeMethod == (int)DcsPartsShippingOrderDetailTransportLossesDispose.正常收货)) {
                if(partsShippingOrder.ReceivingWarehouseId.HasValue) {
                    var warehouse = ObjectContext.Warehouses.SingleOrDefault(r => r.Id == partsShippingOrder.ReceivingWarehouseId.Value && r.Status != (int)DcsBaseDataStatus.作废);
                    if(warehouse == null)
                        throw new ValidationException(ErrorStrings.PartsShippingOrder_Validation16);
                    var company = ObjectContext.Companies.SingleOrDefault(r => r.Id == warehouse.StorageCompanyId && r.Status != (int)DcsMasterDataStatus.作废);
                    if(company == null)
                        throw new ValidationException(ErrorStrings.PartsShippingOrder_Validation4);
                    var partsAmountDetails = new Dictionary<int, decimal>();

                    var partsOutboundPlanNormalId = partsShippingOrderDetails.Where(r => r.TransportLossesDisposeMethod == (int)DcsPartsShippingOrderDetailTransportLossesDispose.正常收货).Select(o => o.PartsOutboundPlanId);
                    foreach(var partsOutboundPlan in partsOutboundPlans.Where(o => partsOutboundPlanNormalId.Contains(o.Id))) {
                        var partsSalesOrder = partsSalesOrders.FirstOrDefault(o => o.Id == partsOutboundPlan.OriginalRequirementBillId);
                        var saleUnit = saleUnits.FirstOrDefault(o => o.Id == partsSalesOrder.SalesUnitId);

                        var outPlanShippingOrderDetails = partsShippingOrderDetails.Where(r => r.TransportLossesDisposeMethod == (int)DcsPartsShippingOrderDetailTransportLossesDispose.正常收货 && r.PartsOutboundPlanId == partsOutboundPlan.Id);
                        var partsIds = outPlanShippingOrderDetails.Select(o => o.SparePartId);
                        if(company.Type == (int)DcsCompanyType.分公司 || company.Type == (int)DcsCompanyType.代理库 || company.Type == (int)DcsCompanyType.服务站兼代理库) {
                            partsAmountDetails = (from a in ObjectContext.EnterprisePartsCosts.Where(r => r.OwnerCompanyId == company.Id && r.PartsSalesCategoryId == partsShippingOrder.PartsSalesCategoryId && partsIds.Contains(r.SparePartId)).ToArray()
                                                  select a).ToDictionary(r => r.SparePartId, r => r.CostPrice);
                        }
                        var normalPartsInboundPlan = new PartsInboundPlan {
                            Id = i++,
                            WarehouseId = partsShippingOrder.ReceivingWarehouseId.Value,
                            WarehouseCode = partsShippingOrder.ReceivingWarehouseCode,
                            WarehouseName = partsShippingOrder.ReceivingWarehouseName,
                            StorageCompanyId = warehouse.StorageCompanyId,
                            StorageCompanyCode = company.Code,
                            StorageCompanyName = company.Name,
                            StorageCompanyType = company.Type,
                            BranchId = partsShippingOrder.BranchId,
                            BranchCode = branch.Code,
                            BranchName = branch.Name,
                            CounterpartCompanyId = partsShippingOrder.SettlementCompanyId,
                            CounterpartCompanyCode = partsShippingOrder.SettlementCompanyCode,
                            CounterpartCompanyName = partsShippingOrder.SettlementCompanyName,
                            OriginalRequirementBillId = partsShippingOrder.OriginalRequirementBillId.Value,
                            OriginalRequirementBillCode = partsShippingOrder.OriginalRequirementBillCode,
                            OriginalRequirementBillType = partsShippingOrder.OriginalRequirementBillType.Value,
                            Status = (int)DcsPartsInboundPlanStatus.新建,
                            SourceId = partsShippingOrder.Id,
                            SourceCode = partsShippingOrder.Code
                        };
                        switch(partsShippingOrder.Type) {
                            case (int)DcsPartsShippingOrderType.销售:
                                normalPartsInboundPlan.InboundType = (int)DcsPartsInboundType.配件采购;
                                normalPartsInboundPlan.PartsSalesCategoryId = partsOutboundBill.PartsSalesCategoryId;
                                break;
                            case (int)DcsPartsShippingOrderType.退货:
                                normalPartsInboundPlan.InboundType = (int)DcsPartsInboundType.销售退货;
                                normalPartsInboundPlan.PartsSalesCategoryId = partsOutboundBill.PartsSalesCategoryId;
                                break;
                            case (int)DcsPartsShippingOrderType.调拨:
                                normalPartsInboundPlan.InboundType = (int)DcsPartsInboundType.配件调拨;
                                normalPartsInboundPlan.PartsSalesCategoryId = saleUnit.PartsSalesCategoryId;
                                break;
                            default:
                                normalPartsInboundPlan.PartsSalesCategoryId = partsOutboundBill.PartsSalesCategoryId;
                                break;
                        };
                        foreach(var partsShippingOrderDetail in outPlanShippingOrderDetails) {
                            var partsOutboundPlanDetail = partsOutboundPlan.PartsOutboundPlanDetails.FirstOrDefault(o => o.SparePartId == partsShippingOrderDetail.SparePartId);
                            var partsInboundPlanDetail = new PartsInboundPlanDetail {
                                SparePartId = partsShippingOrderDetail.SparePartId,
                                SparePartCode = partsShippingOrderDetail.SparePartCode,
                                SparePartName = partsShippingOrderDetail.SparePartName,
                                PlannedAmount = partsShippingOrderDetail.InTransitDamageLossAmount.Value,
                                InspectedQuantity = 0,
                                Price = partsShippingOrderDetail.SettlementPrice,
                                OriginalPrice = partsOutboundPlanDetail.OriginalPrice
                            };
                            if(partsShippingOrder.Type == (int)DcsPartsShippingOrderType.调拨) {
                                if(company.Type == (int)DcsCompanyType.分公司 || company.Type == (int)DcsCompanyType.代理库 || company.Type == (int)DcsCompanyType.服务站兼代理库) {
                                    //填充配件出库单清单
                                    if(partsAmountDetails.ContainsKey(partsShippingOrderDetail.SparePartId)) {
                                        partsInboundPlanDetail.Price = partsAmountDetails[partsShippingOrderDetail.SparePartId];
                                    }
                                }
                            }
                            normalPartsInboundPlan.PartsInboundPlanDetails.Add(partsInboundPlanDetail);
                        }
                        InsertToDatabase(normalPartsInboundPlan);
                        new PartsInboundPlanAch(this).InsertPartsInboundPlanValidate(normalPartsInboundPlan);
                    }
                } else {
                    var partsSalesCategory = ObjectContext.PartsSalesCategories.FirstOrDefault(r => r.Id == partsShippingOrder.PartsSalesCategoryId && r.Status != (int)DcsBaseDataStatus.作废);
                    if(partsSalesCategory == null)
                        throw new ValidationException(ErrorStrings.PartsShippingOrder_Validation17);
                    var sparepartIds = partsShippingOrderDetails.Select(r => r.SparePartId).ToArray();
                    var dealerPartsStocks = ObjectContext.DealerPartsStocks.Where(r => r.SubDealerId == -1 && r.DealerId == partsShippingOrder.ReceivingCompanyId && r.SalesCategoryId == partsSalesCategory.Id && sparepartIds.Contains(r.SparePartId)).ToArray();
                    foreach(var partsShippingOrderDetail in partsShippingOrderDetails) {
                        var dealerPartsStock = dealerPartsStocks.FirstOrDefault(r => r.SparePartId == partsShippingOrderDetail.SparePartId);
                        if(dealerPartsStock != null) {
                            dealerPartsStock.Quantity += partsShippingOrderDetail.InTransitDamageLossAmount.Value;
                        } else {
                            var newealerPartsStock = new DealerPartsStock {
                                DealerId = partsShippingOrder.ReceivingCompanyId,
                                DealerCode = partsShippingOrder.ReceivingCompanyCode,
                                DealerName = partsShippingOrder.ReceivingCompanyName,
                                BranchId = partsShippingOrder.BranchId,
                                SalesCategoryId = partsOutboundBill.PartsSalesCategoryId,
                                SalesCategoryName = partsSalesCategory.Name,
                                SparePartId = partsShippingOrderDetail.SparePartId,
                                SparePartCode = partsShippingOrderDetail.SparePartCode,
                                Quantity = partsShippingOrderDetail.InTransitDamageLossAmount.Value,
                                SubDealerId = -1
                            };
                            InsertToDatabase(newealerPartsStock);
                            new DealerPartsStockAch(this).InsertDealerPartsStockValidate(newealerPartsStock);
                        }
                    }

                }
            }
            #endregion
            UpdateToDatabase(partsShippingOrder);
            partsShippingOrder.TransportLossesDisposeStatus = (int)DcsPartsShippingOrderTransportLossesDisposeStatus.处理完毕;
        }

        [Update(UsingCustomMethod = true)]
        public void 发运确认(PartsShippingOrder partsShippingOrder) {
			 using(var transaction = new TransactionScope()) {
            var dbpartsShippingOrder = ObjectContext.PartsShippingOrders.Where(r => r.Id == partsShippingOrder.Id && r.Status == (int)DcsPartsShippingOrderStatus.新建).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbpartsShippingOrder == null)
                throw new ValidationException(ErrorStrings.PartsShippingOrder_Validation1);
            var partsOutboundPlanIds = partsShippingOrder.PartsShippingOrderDetails.Select(r => r.PartsOutboundPlanId).Distinct();
            var partsOutboundPlans = ObjectContext.PartsOutboundPlans.Where(o => partsOutboundPlanIds.Contains(o.Id)).ToArray();

            var shippingCompany = ObjectContext.Companies.FirstOrDefault(o => o.Id == dbpartsShippingOrder.ShippingCompanyId && o.Status == (int)DcsMasterDataStatus.有效);
            if(shippingCompany.Type == (int)DcsCompanyType.分公司 && partsOutboundPlans.First().OrderTypeName == "正常订单") {
                var pickingTask = ObjectContext.PickingTasks.FirstOrDefault(o => o.WarehouseId == partsShippingOrder.WarehouseId && o.CounterpartCompanyId == partsShippingOrder.ReceivingCompanyId && (o.Status == (int)DcsPickingTaskStatus.新建 || o.Status == (int)DcsPickingTaskStatus.部分拣货));
                if(pickingTask != null) {
                    throw new ValidationException(ErrorStrings.PartsShippingOrder_Validation33);
                }
                var boxUpTask = ObjectContext.BoxUpTasks.FirstOrDefault(o => o.WarehouseId == partsShippingOrder.WarehouseId && o.CounterpartCompanyId == partsShippingOrder.ReceivingCompanyId && (o.Status == (int)DcsBoxUpTaskStatus.新建 || o.Status == (int)DcsBoxUpTaskStatus.部分装箱));
                if(boxUpTask != null) {
                    throw new ValidationException(ErrorStrings.PartsShippingOrder_Validation34);
                }
            }
			var isReceive=false;
            partsShippingOrder.ShippingDate = DateTime.Now;
            partsShippingOrder.Status = (int)DcsPartsShippingOrderStatus.已发货;
            var userInfo = Utils.GetCurrentUserInfo();
            //partsShippingOrder.ReceiptConfirmorId = userInfo.Id;
            //partsShippingOrder.ReceiptConfirmorName = userInfo.Name;
            //partsShippingOrder.ReceiptConfirmTime = DateTime.Now;
            UpdatePartsShippingOrderValidate(partsShippingOrder);
            //更新配件物流批次发运状态
            var partsLogisticBatchIds = partsShippingOrder.PartsShippingOrderDetails.Select(r => r.TaskId);
            var partsLogisticBatches = ObjectContext.PartsLogisticBatches.Where(r => partsLogisticBatchIds.Contains(r.SourceId) && r.SourceType == (int)DcsPartsLogisticBatchSourceType.配件出库计划).ToArray();
            foreach(var partsLogisticBatch in partsLogisticBatches) {
                partsLogisticBatch.ShippingStatus = (int)DcsPartsLogisticBatchShippingStatus.发运;
                UpdateToDatabase(partsLogisticBatch);
            }
            //var partsOutboundPlanDetailsAll =partsOutboundPlans.PartsOutboundPlanDetails.Where(o => partsOutboundPlanIds.Contains(o.PartsOutboundPlanId)).ToArray();
            int number = 0;
            var now = DateTime.Now;
			var outTraces=ObjectContext.AccurateTraces.Where(t=>partsOutboundPlanIds.Contains(t.SourceBillId)&& t.CompanyId==partsShippingOrder.ShippingCompanyId&& t.Type==(int)DCSAccurateTraceType.出库&& t.Status==(int)DCSAccurateTraceStatus.有效).ToArray();
			foreach(var partsOutboundPlanId in partsOutboundPlanIds) {
                var partsOutboundPlan = partsOutboundPlans.FirstOrDefault(o => o.Id == partsOutboundPlanId);
                var partsOutboundPlanDetails = partsOutboundPlan.PartsOutboundPlanDetails.Where(o => o.PartsOutboundPlanId == partsOutboundPlanId);
                var partsOutboundBill = new PartsOutboundBill();
                partsOutboundBill.Code = CodeGenerator.Generate("PartsOutboundBill", partsOutboundPlan.WarehouseCode);
                partsOutboundBill.WarehouseName = partsOutboundPlan.WarehouseName;
                partsOutboundBill.OutboundType = partsOutboundPlan.OutboundType;
                partsOutboundBill.WarehouseCode = partsOutboundPlan.WarehouseCode;
                partsOutboundBill.PartsOutboundPlanId = partsOutboundPlan.Id;
                partsOutboundBill.WarehouseId = partsOutboundPlan.WarehouseId;
                partsOutboundBill.StorageCompanyId = partsOutboundPlan.StorageCompanyId;
                partsOutboundBill.StorageCompanyCode = partsOutboundPlan.StorageCompanyCode;
                partsOutboundBill.StorageCompanyName = partsOutboundPlan.StorageCompanyName;
                partsOutboundBill.StorageCompanyType = partsOutboundPlan.StorageCompanyType;
                partsOutboundBill.BranchId = partsOutboundPlan.BranchId;
                partsOutboundBill.BranchCode = partsOutboundPlan.BranchCode;
                partsOutboundBill.BranchName = partsOutboundPlan.BranchName;
                partsOutboundBill.GPMSPurOrderCode = partsOutboundPlan.GPMSPurOrderCode;
                partsOutboundBill.CounterpartCompanyId = partsOutboundPlan.CounterpartCompanyId;
                partsOutboundBill.CounterpartCompanyCode = partsOutboundPlan.CounterpartCompanyCode;
                partsOutboundBill.CounterpartCompanyName = partsOutboundPlan.CounterpartCompanyName;
                partsOutboundBill.CustomerAccountId = partsOutboundPlan.CustomerAccountId;
                partsOutboundBill.OriginalRequirementBillId = partsOutboundPlan.OriginalRequirementBillId;
                partsOutboundBill.OriginalRequirementBillType = partsOutboundPlan.OriginalRequirementBillType;
                partsOutboundBill.OriginalRequirementBillCode = partsOutboundPlan.OriginalRequirementBillCode;
                partsOutboundBill.ReceivingCompanyId = partsOutboundPlan.ReceivingCompanyId;
                partsOutboundBill.ReceivingCompanyCode = partsOutboundPlan.ReceivingCompanyCode;
                partsOutboundBill.ReceivingCompanyName = partsOutboundPlan.ReceivingCompanyName;
                partsOutboundBill.ReceivingWarehouseId = partsOutboundPlan.ReceivingWarehouseId;
                partsOutboundBill.ReceivingWarehouseCode = partsOutboundPlan.ReceivingWarehouseCode;
                partsOutboundBill.ReceivingWarehouseName = partsOutboundPlan.ReceivingWarehouseName;
                partsOutboundBill.SettlementStatus = (int)DcsPartsSettlementStatus.待结算;
                partsOutboundBill.ShippingMethod = partsOutboundPlan.ShippingMethod;
                partsOutboundBill.PartsSalesCategoryId = partsOutboundPlan.PartsSalesCategoryId;
                partsOutboundBill.PartsSalesOrderTypeId = partsOutboundPlan.PartsSalesOrderTypeId;
                partsOutboundBill.PartsSalesOrderTypeName = partsOutboundPlan.PartsSalesOrderTypeName;
                partsOutboundBill.PartsSalesOrderTypeId = partsOutboundPlan.PartsSalesOrderTypeId;
                partsOutboundBill.PartsSalesOrderTypeName = partsOutboundPlan.PartsSalesOrderTypeName;
                partsOutboundBill.OrderApproveComment = partsOutboundPlan.OrderApproveComment;
                partsOutboundBill.Remark = partsOutboundPlan.Remark;
                partsOutboundBill.SAPPurchasePlanCode = partsOutboundPlan.SAPPurchasePlanCode;
                partsOutboundBill.ERPSourceOrderCode = partsOutboundPlan.ERPSourceOrderCode;
                partsOutboundBill.PartsShippingOrderId = partsShippingOrder.Id;
                //var virtualPartsStocks = new VirtualPartsStockAch(this).查询配件库位库存(partsOutboundPlan.Id, null, null, userInfo.Id, null, null, null).Where(r => r.WarehouseAreaCategory == (int)DcsAreaType.待发区 && r.UsableQuantity > 0);

                var partsOutboundPlanDetailPartIds = partsOutboundPlanDetails.Where(r => r.PlannedAmount != (r.OutboundFulfillment ?? 0)).Select(r => r.SparePartId).ToArray();
                if(partsOutboundPlanDetailPartIds.Length == 0)
                    return;
                var result = ObjectContext.PartsStocks.Where(r => r.Quantity > 0 && partsOutboundPlanDetailPartIds.Contains(r.PartId) && r.WarehouseId == partsOutboundPlan.WarehouseId);
                var virtualPartsStocks = (from r in result
                                          join partsStockBatchDetail in ObjectContext.PartsStockBatchDetails on new {r.Id,r.WarehouseId} equals new{Id=partsStockBatchDetail.PartsStockId,partsStockBatchDetail.WarehouseId} into details
                                          from v in details.DefaultIfEmpty()
                                          join sp in ObjectContext.SpareParts on r.PartId equals sp.Id
                                          join wh in ObjectContext.Warehouses on r.WarehouseId equals wh.Id
                                          join wa in ObjectContext.WarehouseAreas on r.WarehouseAreaId equals wa.Id
                                          join wac in ObjectContext.WarehouseAreaCategories on r.WarehouseAreaCategoryId equals wac.Id
                                          where wac.Category == (int)DcsAreaType.待发区
                                          select new VirtualPartsStock {
                                              PartsStockId = r.Id,
                                              WarehouseId = r.WarehouseId,
                                              WarehouseCode = wh.Code,
                                              WarehouseName = wh.Name,
                                              StorageCompanyId = r.StorageCompanyId,
                                              StorageCompanyType = r.StorageCompanyType,
                                              BranchId = r.BranchId,
                                              SparePartId = sp.Id,
                                              SparePartCode = sp.Code,
                                              SparePartName = sp.Name,
                                              MeasureUnit = sp.MeasureUnit,
                                              WarehouseAreaId = r.WarehouseAreaId,
                                              WarehouseAreaCode = wa.Code,
                                              Quantity = r.Quantity,
                                              UsableQuantity = (int?)v.Quantity ?? r.Quantity,//此处不能改为v.Quantity 因join 可能导致v.Quantity为Null 所以强制转换有效
                                              BatchNumber = v.BatchNumber ?? "",
                                              InboundTime = v.InboundTime,

                                          }).ToArray();

                //根据发运单清单新增配件出库单清单
                foreach(var partsShippingOrderDetail in partsShippingOrder.PartsShippingOrderDetails.Where(o => (o.PartsOutboundPlanId ?? 0) == partsOutboundPlan.Id).ToArray()) {
                    var data = virtualPartsStocks.Where(r => r.SparePartId == partsShippingOrderDetail.SparePartId);
                    if(!data.Any())
                        throw new ValidationException(string.Format(ErrorStrings.PartsShippingOrder_Validation35, partsShippingOrderDetail.SparePartCode));
                    var partsOutboundPlanDetail = partsOutboundPlanDetails.FirstOrDefault(r => r.SparePartId == partsShippingOrderDetail.SparePartId && r.Price == partsShippingOrderDetail.SettlementPrice && r.PlannedAmount != (r.OutboundFulfillment ?? 0));
                    foreach(var virtualPartsStock in data) {
                        var partsOutboundBillDetail = new PartsOutboundBillDetail();
                        partsOutboundBillDetail.SparePartId = virtualPartsStock.SparePartId;
                        partsOutboundBillDetail.SparePartCode = partsShippingOrderDetail.SparePartCode;
                        partsOutboundBillDetail.SparePartName = partsShippingOrderDetail.SparePartName;
                        partsOutboundBillDetail.WarehouseAreaCode = virtualPartsStock.WarehouseAreaCode;
                        //partsOutboundBillDetail.WarehouseAreaCategory = virtualPartsStock.WarehouseAreaCategory;
                        //partsOutboundBillDetail.UsableQuantity = virtualPartsStock.UsableQuantity;
                        partsOutboundBillDetail.WarehouseAreaId = virtualPartsStock.WarehouseAreaId;
                        partsOutboundBillDetail.BatchNumber = virtualPartsStock.BatchNumber;
                        partsOutboundBillDetail.SettlementPrice = partsOutboundPlanDetail.Price;
                        partsOutboundBillDetail.OriginalPrice = partsOutboundPlanDetail.OriginalPrice;

                        //剩余出库数量
                        var currentOutboundAmount = partsOutboundPlanDetail.PlannedAmount - (partsOutboundPlanDetail.OutboundFulfillment ?? 0);
                        if(currentOutboundAmount == 0) {
                            break;
                        } else {
                            partsOutboundBillDetail.OutboundAmount = currentOutboundAmount > partsShippingOrderDetail.ShippingAmount ? partsShippingOrderDetail.ShippingAmount : currentOutboundAmount;
                            partsOutboundPlanDetail.OutboundFulfillment = (partsOutboundPlanDetail.OutboundFulfillment ?? 0) + partsOutboundBillDetail.OutboundAmount;
                        }
                        partsOutboundBill.PartsOutboundBillDetails.Add(partsOutboundBillDetail);
                    }
                }
                InsertToDatabase(partsOutboundBill);
				var partsIds = partsOutboundBill.PartsOutboundBillDetails.Select(r => r.SparePartId).ToArray();
                //转单逻辑
                if(partsOutboundPlan.IsTurn.HasValue && partsOutboundPlan.IsTurn.Value) {
					partsShippingOrder.Status = (int)DcsPartsShippingOrderStatus.收货确认;
					isReceive=true;
					int id = 0;
                   //查询原始销售订单
                     var partsSalesReturnBill=ObjectContext.PartsSalesReturnBills.Where(t=>t.Id==partsOutboundPlan.SourceId&& t.Code== partsOutboundPlan.SourceCode).FirstOrDefault();
					 var partsSalesOrderCOde=  partsSalesReturnBill.ReturnReason.Substring(2,partsSalesReturnBill.ReturnReason.Length-2);
					 var partsSalesOrder=ObjectContext.PartsSalesOrders.Include("PartsSalesOrderProcesses").Where(r=>r.Code==partsSalesOrderCOde).FirstOrDefault();
					 if(partsSalesOrder==null){
						throw new ValidationException("退货单"+partsSalesReturnBill.Code+"未找到对应的销售订单");
					 }
					  var warehouse = ObjectContext.Warehouses.SingleOrDefault(r => r.Id == partsSalesOrder.WarehouseId);
                      if(warehouse == null)
                     throw new ValidationException(string.Format("Id为{0}仓库不存在", partsSalesOrder.WarehouseId));
				     var orderPartsSalesOrderProcess =partsSalesOrder.PartsSalesOrderProcesses.First();
					 var salesUnit = this.ObjectContext.SalesUnits.FirstOrDefault(r => r.Id == partsSalesOrder.SalesUnitId && r.Status != (int)DcsMasterDataStatus.作废);
                     if(salesUnit == null)
						throw new ValidationException(string.Format("Id为{0}销售组织不存在", partsSalesOrder.SalesUnitId));
					var userInfoCompany = this.ObjectContext.Companies.SingleOrDefault(r => r.Id == salesUnit.OwnerCompanyId);
					if(userInfoCompany == null)
						throw new ValidationException("销售订单所属销售组织隶属企业不存在");
					 var warehouseArea = this.ObjectContext.WarehouseAreas.FirstOrDefault(r => r.Status == (int)DcsBaseDataStatus.有效 && r.WarehouseId == partsSalesOrder.WarehouseId && r.AreaKind == (int)DcsAreaKind.库位 && this.ObjectContext.WarehouseAreaCategories.Any(w => w.Id == r.AreaCategoryId && w.Category == (int)DcsAreaType.检验区));
					if(warehouseArea == null)
					    throw new ValidationException(string.Format("仓库{0}，未找到对应的检验区库存！", warehouse.Code));
					 
					#region 新增总部入库检验单
                    var partsInboundCheckBill = new PartsInboundCheckBill {
                        WarehouseId = partsSalesOrder.WarehouseId ?? 0,
                        WarehouseCode = warehouse.Code,
                        WarehouseName = warehouse.Name,
                        StorageCompanyId = partsSalesOrder.SalesUnitOwnerCompanyId,
                        StorageCompanyCode = partsSalesOrder.SalesUnitOwnerCompanyCode,
                        StorageCompanyName = partsSalesOrder.SalesUnitOwnerCompanyName,
                        StorageCompanyType = (int)DcsCompanyType.分公司,
                        BranchId = partsSalesOrder.BranchId,
                        BranchCode = partsSalesOrder.BranchCode,
                        BranchName = partsSalesOrder.BranchName,
                        CounterpartCompanyId = partsOutboundPlan.StorageCompanyId,
                        CounterpartCompanyCode = partsOutboundPlan.StorageCompanyCode,
                        CounterpartCompanyName = partsOutboundPlan.StorageCompanyName,
                        InboundType = (int)DcsPartsInboundType.销售退货,
                        CustomerAccountId = partsSalesReturnBill.CustomerAccountId,
                        OriginalRequirementBillId = partsSalesReturnBill.Id,
                        OriginalRequirementBillCode = partsSalesReturnBill.Code,
                        OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.配件销售退货单,
                        Status = (int)DcsPartsInboundCheckBillStatus.上架完成,
                        SettlementStatus = (int)DcsPartsSettlementStatus.待结算,
                        PartsSalesCategoryId = partsSalesOrder.SalesCategoryId,
                        Remark = partsSalesOrder.Remark
                    };
                     #endregion

                    #region 总部出库计划单
                    string receivingWarehouseCode = null;
                    if(partsSalesOrder.ReceivingWarehouseId.HasValue) {
                        var receivingWarehouse = ObjectContext.Warehouses.SingleOrDefault(r => r.Id == partsSalesOrder.ReceivingWarehouseId.Value && r.Status == (int)DcsBaseDataStatus.有效);
                        if(receivingWarehouse != null) {
                            receivingWarehouseCode = receivingWarehouse.Code;
                        }
                    }
                    var zbPartsOutboundPlan = new PartsOutboundPlan {
                        Id = id++,
                        WarehouseId = partsSalesOrder.WarehouseId ?? 0,
                        WarehouseName = warehouse.Name,
                        WarehouseCode = warehouse.Code,
                        StorageCompanyId = userInfoCompany.Id,
                        CompanyAddressId = partsSalesOrder.CompanyAddressId,
                        StorageCompanyCode = userInfoCompany.Code,
                        StorageCompanyName = userInfoCompany.Name,
                        StorageCompanyType = userInfoCompany.Type,
                        BranchId = partsSalesOrder.BranchId,
                        BranchCode = partsSalesOrder.BranchCode,
                        BranchName = partsSalesOrder.BranchName,
                        CounterpartCompanyId = partsSalesOrder.InvoiceReceiveCompanyId,
                        CounterpartCompanyCode = partsSalesOrder.InvoiceReceiveCompanyCode,
                        CounterpartCompanyName = partsSalesOrder.InvoiceReceiveCompanyName,
                        SourceId = partsSalesOrder.Id,
                        SourceCode = partsSalesOrder.Code,
                        OutboundType = (int)DcsPartsOutboundType.配件销售,
                        CustomerAccountId = partsSalesOrder.CustomerAccountId,
                        OriginalRequirementBillId = partsSalesOrder.Id,
                        OriginalRequirementBillCode = partsSalesOrder.Code,
                        OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.配件销售订单,
                        ReceivingCompanyId = partsSalesOrder.ReceivingCompanyId,
                        ReceivingCompanyCode = partsSalesOrder.ReceivingCompanyCode,
                        ReceivingCompanyName = partsSalesOrder.ReceivingCompanyName,
                        Status = (int)DcsPartsOutboundPlanStatus.出库完成,
                        ShippingMethod = partsSalesOrder.ShippingMethod,
                        IfWmsInterface = warehouse.WmsInterface,
                        PartsSalesCategoryId = salesUnit.PartsSalesCategoryId,
                        PartsSalesOrderTypeId = partsSalesOrder.PartsSalesOrderTypeId,
                        PartsSalesOrderTypeName = partsSalesOrder.PartsSalesOrderTypeName,
                        OrderApproveComment = orderPartsSalesOrderProcess.ApprovalComment,
                        SAPPurchasePlanCode = partsSalesOrder.OverseasDemandSheetNo,
                        Remark = partsSalesOrder.Remark,
                        ERPSourceOrderCode = partsSalesOrder.ERPSourceOrderCode,
                        ReceivingAddress = partsSalesOrder.ReceivingAddress,
                        ReceivingWarehouseId = partsSalesOrder.ReceivingWarehouseId,
                        ReceivingWarehouseCode = receivingWarehouseCode,
                        ReceivingWarehouseName = partsSalesOrder.ReceivingWarehouseName
                    };
                    #endregion

                    #region 总部出库单
                    var zbPartsOutboundBill = new PartsOutboundBill {
                        Id = id++,
                        PartsOutboundPlanId = zbPartsOutboundPlan.Id,
                        WarehouseId = zbPartsOutboundPlan.WarehouseId,
                        WarehouseCode = zbPartsOutboundPlan.WarehouseCode,
                        WarehouseName = zbPartsOutboundPlan.WarehouseName,
                        StorageCompanyId = zbPartsOutboundPlan.StorageCompanyId,
                        StorageCompanyCode = zbPartsOutboundPlan.StorageCompanyCode,
                        StorageCompanyName = zbPartsOutboundPlan.StorageCompanyName,
                        StorageCompanyType = zbPartsOutboundPlan.StorageCompanyType,
                        BranchId = zbPartsOutboundPlan.BranchId,
                        BranchCode = zbPartsOutboundPlan.BranchCode,
                        BranchName = zbPartsOutboundPlan.BranchName,
                        PartsSalesCategoryId = zbPartsOutboundPlan.PartsSalesCategoryId,
                        PartsSalesOrderTypeId = zbPartsOutboundPlan.PartsSalesOrderTypeId,
                        PartsSalesOrderTypeName = zbPartsOutboundPlan.PartsSalesOrderTypeName,
                        CounterpartCompanyId = zbPartsOutboundPlan.CounterpartCompanyId,
                        CounterpartCompanyCode = zbPartsOutboundPlan.CounterpartCompanyCode,
                        CounterpartCompanyName = zbPartsOutboundPlan.CounterpartCompanyName,
                        ReceivingCompanyId = zbPartsOutboundPlan.ReceivingCompanyId,
                        ReceivingCompanyCode = zbPartsOutboundPlan.ReceivingCompanyCode,
                        ReceivingCompanyName = zbPartsOutboundPlan.ReceivingCompanyName,
                        ReceivingWarehouseId = zbPartsOutboundPlan.ReceivingWarehouseId,
                        ReceivingWarehouseCode = zbPartsOutboundPlan.ReceivingWarehouseCode,
                        ReceivingWarehouseName = zbPartsOutboundPlan.ReceivingWarehouseName,
                        OutboundType = zbPartsOutboundPlan.OutboundType,
                        CustomerAccountId = zbPartsOutboundPlan.CustomerAccountId,
                        OriginalRequirementBillId = zbPartsOutboundPlan.OriginalRequirementBillId,
                        OriginalRequirementBillCode = zbPartsOutboundPlan.OriginalRequirementBillCode,
                        OriginalRequirementBillType = zbPartsOutboundPlan.OriginalRequirementBillType,
                        SettlementStatus = (int)DcsPartsSettlementStatus.待结算,
                        Remark = partsSalesOrder.Remark
                    };
                    #endregion

                    #region 总部发运单
                    var newPartsShippingOrder = new PartsShippingOrder();
                    newPartsShippingOrder.Type = (int)DcsPartsShippingOrderType.销售;
                    newPartsShippingOrder.PartsSalesCategoryId = zbPartsOutboundPlan.PartsSalesCategoryId;
                    newPartsShippingOrder.BranchId = zbPartsOutboundPlan.BranchId;
                    newPartsShippingOrder.ShippingCompanyId = zbPartsOutboundBill.StorageCompanyId;
                    newPartsShippingOrder.ShippingCompanyCode = zbPartsOutboundBill.StorageCompanyCode;
                    newPartsShippingOrder.ShippingCompanyName = zbPartsOutboundBill.StorageCompanyName;
                    newPartsShippingOrder.SettlementCompanyId = zbPartsOutboundBill.StorageCompanyId;
                    newPartsShippingOrder.SettlementCompanyCode = zbPartsOutboundBill.StorageCompanyCode;
                    newPartsShippingOrder.SettlementCompanyName = zbPartsOutboundBill.StorageCompanyName;
                    newPartsShippingOrder.ReceivingCompanyId = zbPartsOutboundBill.ReceivingCompanyId;
                    newPartsShippingOrder.ReceivingCompanyCode = zbPartsOutboundBill.ReceivingCompanyCode;
                    newPartsShippingOrder.ReceivingCompanyName = zbPartsOutboundBill.ReceivingCompanyName;
                    newPartsShippingOrder.InvoiceReceiveSaleCateId = zbPartsOutboundBill.PartsSalesOrderTypeId;
                    newPartsShippingOrder.InvoiceReceiveSaleCateName = zbPartsOutboundBill.PartsSalesOrderTypeName;
                    newPartsShippingOrder.WarehouseId = zbPartsOutboundBill.WarehouseId;
                    newPartsShippingOrder.WarehouseCode = zbPartsOutboundBill.WarehouseCode;
                    newPartsShippingOrder.WarehouseName = zbPartsOutboundBill.WarehouseName;
                    newPartsShippingOrder.ReceivingWarehouseId = zbPartsOutboundBill.ReceivingWarehouseId;
                    newPartsShippingOrder.ReceivingWarehouseCode = zbPartsOutboundBill.ReceivingWarehouseCode;
                    newPartsShippingOrder.ReceivingWarehouseName = zbPartsOutboundBill.ReceivingWarehouseName;
                    newPartsShippingOrder.OriginalRequirementBillId = zbPartsOutboundBill.OriginalRequirementBillId;
                    newPartsShippingOrder.OriginalRequirementBillType = zbPartsOutboundBill.OriginalRequirementBillType;
                    newPartsShippingOrder.OriginalRequirementBillCode = zbPartsOutboundBill.OriginalRequirementBillCode;
                    newPartsShippingOrder.GPMSPurOrderCode = zbPartsOutboundBill.GPMSPurOrderCode;
                    newPartsShippingOrder.SAPPurchasePlanCode = zbPartsOutboundBill.SAPPurchasePlanCode;
                    newPartsShippingOrder.Status = (int)DcsPartsShippingOrderStatus.已发货;
                    newPartsShippingOrder.Remark = partsSalesOrder.Remark;
					newPartsShippingOrder.ReceivingAddress = partsSalesOrder.ReceivingAddress;
					newPartsShippingOrder.PartsSalesOrderTypeName=zbPartsOutboundBill.PartsSalesOrderTypeName;
                    newPartsShippingOrder.PartsSalesOrderTypeId=zbPartsOutboundBill.PartsSalesOrderTypeId;
                    #endregion

                    zbPartsOutboundBill.PartsShippingOrderId = newPartsShippingOrder.Id;

                    #region 配件发运单关联单
                    var newPartsShippingOrderRef = new PartsShippingOrderRef {
                        PartsOutboundBillId = zbPartsOutboundBill.Id,
                        PartsShippingOrderId = newPartsShippingOrder.Id
                    };
                    #endregion

                    #region 总部入库计划单
                    var partsInboundPlan = new PartsInboundPlan {
                        WarehouseId = partsSalesOrder.WarehouseId ?? 0,
                        WarehouseCode = warehouse.Code,
                        WarehouseName = warehouse.Name,
                        StorageCompanyId = partsSalesOrder.SalesUnitOwnerCompanyId,
                        StorageCompanyCode = partsSalesOrder.SalesUnitOwnerCompanyCode,
                        StorageCompanyName = partsSalesOrder.SalesUnitOwnerCompanyName,
                        StorageCompanyType = (int)DcsCompanyType.分公司,
                        BranchId = partsSalesOrder.BranchId,
                        BranchCode = partsSalesOrder.BranchCode,
                        BranchName = partsSalesOrder.BranchName,
                        PartsSalesCategoryId = partsSalesOrder.SalesCategoryId,
                        CounterpartCompanyId = partsOutboundPlan.StorageCompanyId ,
                        CounterpartCompanyCode = partsOutboundPlan.StorageCompanyCode,
                        CounterpartCompanyName = partsOutboundPlan.StorageCompanyName,
                        //SourceId = partsSalesReturnBill.Id,
                        SourceCode = partsSalesReturnBill.Code,
                        InboundType = (int)DcsPartsInboundType.销售退货,
                        CustomerAccountId = partsSalesReturnBill.CustomerAccountId,
                        OriginalRequirementBillId = partsSalesReturnBill.Id,
                        OriginalRequirementBillCode = partsSalesReturnBill.Code,
                        OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.配件销售退货单,
                        Status = (int)DcsPartsInboundPlanStatus.上架完成,
                        IfWmsInterface = false,
                        ArrivalDate = DateTime.Now,
                        PackingFinishTime = DateTime.Now,
                        ShelvesFinishTime = DateTime.Now,
                        ReceivingFinishTime = DateTime.Now,
                        Remark = partsSalesOrder.Remark
                    };
                   #endregion
				   foreach(var item in partsOutboundBill.PartsOutboundBillDetails) {
                       //添加配件入库计划单清单
                       var partsInboundPlanDetail = new PartsInboundPlanDetail {
                           SparePartId = item.SparePartId,
                           SparePartCode = item.SparePartCode,
                           SparePartName = item.SparePartName,
                           PlannedAmount = item.OutboundAmount,
                           InspectedQuantity = item.OutboundAmount,
                           Price = item.SettlementPrice,
                           OriginalPrice = item.SettlementPrice
                       };
                       partsInboundPlan.PartsInboundPlanDetails.Add(partsInboundPlanDetail);
                       //新增入库检验单清单
                       partsInboundCheckBill.PartsInboundCheckBillDetails.Add(new PartsInboundCheckBillDetail {
                           SparePartId = item.SparePartId,
                           SparePartCode = item.SparePartCode,
                           SparePartName = item.SparePartName,
                           WarehouseAreaId = warehouseArea.Id,
                           WarehouseAreaCode = warehouseArea.Code,
                           InspectedQuantity = item.OutboundAmount,
                           SettlementPrice = item.SettlementPrice,
                           OriginalPrice = item.SettlementPrice
                       });                   
                     //总部出库计划单清单
						var PartsOutboundPlanDetail = zbPartsOutboundPlan.PartsOutboundPlanDetails.FirstOrDefault(r => r.SparePartId == item.SparePartId && r.Price == item.OriginalPrice.Value);
						if (PartsOutboundPlanDetail == null) {
							zbPartsOutboundPlan.PartsOutboundPlanDetails.Add(new PartsOutboundPlanDetail {
							  SparePartId = item.SparePartId,
                           SparePartCode = item.SparePartCode,
                           SparePartName = item.SparePartName,
                           PlannedAmount = item.OutboundAmount,
                           Price = item.OriginalPrice.Value,
                           OutboundFulfillment = item.OutboundAmount,
                           OriginalPrice = item.OriginalPrice
							});
						} else {
							PartsOutboundPlanDetail.OutboundFulfillment += item.OutboundAmount;
							PartsOutboundPlanDetail.PlannedAmount += item.OutboundAmount;
						}					   
					   //总部出库单清单
						var PartsOutboundBillDetail = zbPartsOutboundBill.PartsOutboundBillDetails.FirstOrDefault(r => r.SparePartId == item.SparePartId && r.SettlementPrice == item.OriginalPrice.Value);
						if (PartsOutboundBillDetail == null) {
							zbPartsOutboundBill.PartsOutboundBillDetails.Add(new PartsOutboundBillDetail {
							   SparePartId = item.SparePartId,
							   SparePartCode = item.SparePartCode,
							   SparePartName = item.SparePartName,
							   SettlementPrice = item.OriginalPrice.Value,
							   OutboundAmount = item.OutboundAmount,
							   OriginalPrice = item.OriginalPrice
							});
						} else {
							PartsOutboundBillDetail.OutboundAmount += item.OutboundAmount;
						}
					   //总部配件发运单清单
						var PartsShippingOrderDetail = newPartsShippingOrder.PartsShippingOrderDetails.FirstOrDefault(r => r.SparePartId == item.SparePartId && r.SettlementPrice == item.OriginalPrice.Value);
						if (PartsShippingOrderDetail == null) {
							newPartsShippingOrder.PartsShippingOrderDetails.Add(new PartsShippingOrderDetail {
							   PartsShippingOrderId = newPartsShippingOrder.Id,
							   SparePartId = item.SparePartId,
							   SparePartCode = item.SparePartCode,
							   SparePartName = item.SparePartName,
							   ShippingAmount = item.OutboundAmount,
							   ConfirmedAmount = item.OutboundAmount,
							   SettlementPrice = item.OriginalPrice.Value,
							   PartsOutboundPlanId = zbPartsOutboundPlan.Id
							});
						} else {
							PartsShippingOrderDetail.ShippingAmount += item.OutboundAmount;
							PartsShippingOrderDetail.ConfirmedAmount +=item.OutboundAmount;
						}
                  }
                //   var enterprisePartsCostss = new EnterprisePartsCostAch(this).GetEnterprisePartsCostsWithLock(this.ObjectContext.EnterprisePartsCosts.Where(r => r.OwnerCompanyId == partsOutboundPlan.StorageCompanyId && partsIds.Contains(r.SparePartId))).ToList();
                   //中心库成本价
                   var partsAmountCenters = new Dictionary<int, decimal>();
                   partsAmountCenters = (from a in ObjectContext.EnterprisePartsCosts.Where(r => r.OwnerCompanyId == partsOutboundBill.StorageCompanyId && r.PartsSalesCategoryId == partsOutboundBill.PartsSalesCategoryId && partsIds.Contains(r.SparePartId)).ToArray()
                                         select a).ToDictionary(r => r.SparePartId, r => r.CostPrice);
                   //总部成本价
                   var partsAmountDetail = new Dictionary<int, decimal>();
                   partsAmountDetail = (from a in ObjectContext.EnterprisePartsCosts.Where(r => r.OwnerCompanyId == zbPartsOutboundBill.StorageCompanyId && r.PartsSalesCategoryId == zbPartsOutboundBill.PartsSalesCategoryId && partsIds.Contains(r.SparePartId)).ToArray()
                                         select a).ToDictionary(r => r.SparePartId, r => r.CostPrice);
                   foreach(var partsOutboundBillDetail in partsOutboundBill.PartsOutboundBillDetails) {
                      
                       ////减少企业成本库存
                       //var enterprisePartsCost = enterprisePartsCostss == null ? null : enterprisePartsCostss.FirstOrDefault(r => r.SparePartId == partsOutboundBillDetail.SparePartId);
                       //if(enterprisePartsCost != null) {
                       //    enterprisePartsCost.Quantity -= partsOutboundBillDetail.OutboundAmount;
                       //    enterprisePartsCost.CostAmount -= partsOutboundBillDetail.OutboundAmount * enterprisePartsCost.CostPrice;
                       //    UpdateToDatabase(enterprisePartsCost);
                       //} else {
                       //    throw new ValidationException(string.Format(ErrorStrings.PartsOutboundBill_Validation8, partsOutboundBillDetail.SparePartCode));
                       //}

                       //填充配件出库单清单
                       if(!partsAmountCenters.ContainsKey(partsOutboundBillDetail.SparePartId)) {
                           throw new ValidationException(string.Format(ErrorStrings.PartsOutboundBill_Validation8, partsOutboundBillDetail.SparePartCode));
                       } else {
                           partsOutboundBillDetail.CostPrice = partsAmountCenters[partsOutboundBillDetail.SparePartId];
                       }
                       if(partsOutboundBillDetail.CostPrice == 0) {
                           throw new ValidationException(string.Format("编号为“{0}”的配件的成本价等于0", partsOutboundBillDetail.SparePartCode));
                       }
                   }

                   foreach(var partsInboundCheckBillDetail in partsInboundCheckBill.PartsInboundCheckBillDetails) {
                       //填充配件出库单清单
                       if(!partsAmountDetail.ContainsKey(partsInboundCheckBillDetail.SparePartId)) {
                           throw new ValidationException(string.Format(ErrorStrings.PartsOutboundBill_Validation8, partsInboundCheckBillDetail.SparePartCode));
                       } else {
                           partsInboundCheckBillDetail.CostPrice = partsAmountDetail[partsInboundCheckBillDetail.SparePartId];
                       }
                       if(partsInboundCheckBillDetail.CostPrice == 0) {
                           throw new ValidationException(string.Format("编号为“{0}”的配件的成本价等于0", partsInboundCheckBillDetail.SparePartCode));
                       }
                   }

                   foreach(var partsOutboundBillDetail in zbPartsOutboundBill.PartsOutboundBillDetails) {
                       //填充配件出库单清单
                       if(!partsAmountDetail.ContainsKey(partsOutboundBillDetail.SparePartId)) {
                           throw new ValidationException(string.Format(ErrorStrings.PartsOutboundBill_Validation8, partsOutboundBillDetail.SparePartCode));
                       } else {
                           partsOutboundBillDetail.CostPrice = partsAmountDetail[partsOutboundBillDetail.SparePartId];
                       }
                       if(partsOutboundBillDetail.CostPrice == 0) {
                           throw new ValidationException(string.Format("编号为“{0}”的配件的成本价等于0", partsOutboundBillDetail.SparePartCode));
                       }
                   }
				   
				  
                   partsInboundPlan.PartsSalesReturnBill = partsSalesReturnBill;

                   partsOutboundPlan.PartsOutboundBills.Add(partsOutboundBill);
                   partsInboundPlan.PartsInboundCheckBills.Add(partsInboundCheckBill);
                   newPartsShippingOrder.PartsShippingOrderRefs.Add(newPartsShippingOrderRef);
                   zbPartsOutboundBill.PartsShippingOrderRefs.Add(newPartsShippingOrderRef);

                   InsertToDatabase(partsInboundCheckBill);
                   new PartsInboundCheckBillAch(this).InsertPartsInboundCheckBillValidate(partsInboundCheckBill);
                   InsertToDatabase(zbPartsOutboundPlan);
                   new PartsOutboundPlanAch(this).InsertPartsOutboundPlanValidate(zbPartsOutboundPlan);
                   InsertToDatabase(zbPartsOutboundBill);
                   new PartsOutboundBillAch(this).InsertPartsOutboundBillValidate(zbPartsOutboundBill);
                   foreach(var partsShippingOrderDetail in newPartsShippingOrder.PartsShippingOrderDetails)
                       partsShippingOrderDetail.PartsOutboundPlanCode = zbPartsOutboundPlan.Code;
                   InsertToDatabase(newPartsShippingOrder);
                   this.InsertPartsShippingOrderValidate(newPartsShippingOrder);
                   InsertToDatabase(partsInboundPlan);
                   new PartsInboundPlanAch(this).InsertPartsInboundPlanValidate(partsInboundPlan);
				   ObjectContext.SaveChanges();
				   //增加总部的入库追溯信息
				   var  outs= outTraces.Where(t=>t.SourceBillId==partsOutboundPlanId).ToArray();
				    foreach(var item in outs){
						var inTrace=new AccurateTrace(){
							Type=(int)DCSAccurateTraceType.入库,
							CompanyType=(int)DCSAccurateTraceCompanyType.配件中心,
							CompanyId=partsInboundCheckBill.StorageCompanyId,
							SourceBillId=partsInboundPlan.Id,
							PartId=item.PartId,
							TraceCode=item.TraceCode,
							SIHLabelCode=item.SIHLabelCode,
							Status=(int)DCSAccurateTraceStatus.有效,
							CreatorId=userInfo.Id,
							CreatorName=userInfo.Name,
							CreateTime=DateTime.Now,
							TraceProperty=item.TraceProperty,
							BoxCode=item.BoxCode,
							InQty=item.OutQty,
							OutQty=item.OutQty
						};
						InsertToDatabase(inTrace);
					}
					//增加总部的出库追溯信息
					 foreach(var item in outs){
						var  outTrace=new AccurateTrace(){
							Type=(int)DCSAccurateTraceType.出库,
							CompanyType=(int)DCSAccurateTraceCompanyType.配件中心,
							CompanyId=partsInboundCheckBill.StorageCompanyId,
							SourceBillId=zbPartsOutboundPlan.Id,
							PartId=item.PartId,
							TraceCode=item.TraceCode,
							SIHLabelCode=item.SIHLabelCode,
							Status=(int)DCSAccurateTraceStatus.有效,
							CreatorId=userInfo.Id,
							CreatorName=userInfo.Name,
							CreateTime=DateTime.Now,
							TraceProperty=item.TraceProperty,
							BoxCode=item.BoxCode,
							InQty=item.OutQty,
							OutQty=item.OutQty
						};
						InsertToDatabase(outTrace);
					}
				}
                var batchNumbers = partsOutboundBill.PartsOutboundBillDetails.Where(v => !string.IsNullOrEmpty(v.BatchNumber)).Select(v => v.BatchNumber.ToLower()).ToArray();
               // var partsIds = partsOutboundBill.PartsOutboundBillDetails.Select(r => r.SparePartId).Distinct().ToArray();
                var warehouseAreaIds = partsOutboundBill.PartsOutboundBillDetails.Select(r => r.WarehouseAreaId).Distinct().ToArray();
                var partsStocks = new PartsStockAch(this).GetPartsStocksWithLock(ObjectContext.PartsStocks.Where(r => r.WarehouseId == partsOutboundBill.WarehouseId && partsIds.Contains(r.PartId) && warehouseAreaIds.Contains(r.WarehouseAreaId)).ToArray()).ToArray();
                var enterprisePartsCosts = new EnterprisePartsCostAch(this).GetEnterprisePartsCostsWithLock(ObjectContext.EnterprisePartsCosts.Where(r => r.OwnerCompanyId == partsOutboundPlan.StorageCompanyId && partsIds.Contains(r.SparePartId)).ToList()).ToList();

                //获取配件信息，并获取总成件所有的关键代码和总成件领用关系
                var spareParts = ObjectContext.SpareParts.Where(o => partsIds.Contains(o.Id)).ToList();
                var assemblyKeyCodes = spareParts.Where(o => o.PartType == (int)DcsSparePartPartType.总成件_领用).Select(o => o.AssemblyKeyCode).ToList();
                var assemblyPartRequisitionLinks = ObjectContext.AssemblyPartRequisitionLinks.Where(o => assemblyKeyCodes.Contains(o.KeyCode) && o.Status == 1).ToList();
                //锁定配件为总成件，对应的配件库存信息
                IEnumerable<PartsStock> assemblyPartsStocks = null;
                IEnumerable<EnterprisePartsCost> assemblyEnterprisePartsCosts = null;
                if(assemblyPartRequisitionLinks.Count() > 0 && shippingCompany.Type == (int)DcsCompanyType.分公司) {
                    var assemblyPartsIds = assemblyPartRequisitionLinks.Select(o => o.PartId).ToList();
                    assemblyPartsStocks = new PartsStockAch(this).GetPartsStocksWithLock(ObjectContext.PartsStocks.Where(r => r.WarehouseId == partsOutboundBill.WarehouseId && assemblyPartsIds.Contains(r.PartId) && ObjectContext.WarehouseAreaCategories.Any(t => t.Id == r.WarehouseAreaCategoryId && t.Category == (int)DcsAreaType.保管区)).ToArray()).ToArray();
                    assemblyEnterprisePartsCosts = new EnterprisePartsCostAch(this).GetEnterprisePartsCostsWithLock(ObjectContext.EnterprisePartsCosts.Where(r => r.OwnerCompanyId == partsOutboundPlan.StorageCompanyId && assemblyPartsIds.Contains(r.SparePartId)).ToList()).ToList();
                }

                var partsStockBatchDetails = new PartsStockBatchDetail[0];
                if(batchNumbers.Length > 0) {
                    partsStockBatchDetails = ObjectContext.PartsStockBatchDetails.Where(r => batchNumbers.Contains(r.BatchNumber.ToLower())).ToArray();
                }

                var partsAmountDetails = new Dictionary<int, decimal>();
                if(partsOutboundPlan.StorageCompanyType == (int)DcsCompanyType.分公司 || partsOutboundPlan.StorageCompanyType == (int)DcsCompanyType.代理库 || partsOutboundPlan.StorageCompanyType == (int)DcsCompanyType.服务站兼代理库) {
                    partsAmountDetails = (from a in ObjectContext.EnterprisePartsCosts.Where(r => r.OwnerCompanyId == partsOutboundBill.StorageCompanyId && r.PartsSalesCategoryId == partsOutboundBill.PartsSalesCategoryId && partsIds.Contains(r.SparePartId)).ToArray()
                                          select a).ToDictionary(r => r.SparePartId, r => r.CostPrice);
                }

                PartsOutboundPlan newPartsOutboundPlan = null;
                PartsOutboundBill newPartsOutboundBill = null;
                foreach(var partsOutboundBillDetail in partsOutboundBill.PartsOutboundBillDetails) {
                    var partsOutboundPlanDetail = partsOutboundPlanDetails.FirstOrDefault(r => r.SparePartId == partsOutboundBillDetail.SparePartId);
                    if(partsOutboundPlanDetail == null)
                        throw new ValidationException(string.Format(ErrorStrings.PartsOutboundBill_Validation4, partsOutboundBillDetail.SparePartCode));
                    if(partsOutboundPlanDetail.OutboundFulfillment.Value > partsOutboundPlanDetail.PlannedAmount)
                        throw new ValidationException(string.Format(ErrorStrings.PartsOutboundBill_Validation9, partsOutboundBillDetail.SparePartCode));

                    var sparePart = spareParts.Where(o => o.PartType == (int)DcsSparePartPartType.总成件_领用).FirstOrDefault(o => o.Id == partsOutboundBillDetail.SparePartId);
                    if(shippingCompany.Type == (int)DcsCompanyType.分公司 && sparePart != null && partsShippingOrder.Type == (int)DcsPartsShippingOrderType.销售) {
                        foreach(var assemblyPartRequisitionLink in assemblyPartRequisitionLinks.Where(o => o.KeyCode == sparePart.AssemblyKeyCode).ToList()) {
                            var assemblyPartsStock = assemblyPartsStocks.Where(o => o.PartId == assemblyPartRequisitionLink.PartId).ToList();
                            var assemblyEnterprisePartsCost = assemblyEnterprisePartsCosts.FirstOrDefault(o => o.SparePartId == assemblyPartRequisitionLink.PartId);
                            var qty = (assemblyPartRequisitionLink.Qty ?? 0) * partsOutboundBillDetail.OutboundAmount;
                            if(qty > assemblyPartsStock.Sum(o => o.Quantity - (o.LockedQty ?? 0))) {
                                throw new ValidationException(string.Format(ErrorStrings.AssemblyPartRequisitionLink_Validation6, partsOutboundBillDetail.SparePartCode, assemblyPartRequisitionLink.PartCode));
                            } else {
                                if(newPartsOutboundPlan == null) {
                                    var internalAllocationBill = ObjectContext.InternalAllocationBills.FirstOrDefault(o => o.SourceCode == partsOutboundPlan.SourceCode);
                                    if(null==internalAllocationBill){
                                        throw new ValidationException(string.Format(ErrorStrings.InternalAllocationBill_SparePart_NotFound, partsOutboundBillDetail.SparePartCode));
                                    }
                                    newPartsOutboundPlan = new PartsOutboundPlan {
                                        WarehouseId = partsOutboundPlan.WarehouseId,
                                        WarehouseName = partsOutboundPlan.WarehouseName,
                                        WarehouseCode = partsOutboundPlan.WarehouseCode,
                                        StorageCompanyId = partsOutboundPlan.StorageCompanyId,
                                        CompanyAddressId = partsOutboundPlan.CompanyAddressId,
                                        StorageCompanyCode = partsOutboundPlan.StorageCompanyCode,
                                        StorageCompanyName = partsOutboundPlan.StorageCompanyName,
                                        StorageCompanyType = partsOutboundPlan.StorageCompanyType,
                                        BranchId = partsOutboundPlan.BranchId,
                                        BranchCode = partsOutboundPlan.BranchCode,
                                        BranchName = partsOutboundPlan.BranchName,
                                        CounterpartCompanyId = 361,
                                        CounterpartCompanyCode = "XSFWZ",
                                        CounterpartCompanyName = "销售服务组",
                                        SourceId = internalAllocationBill.Id,
                                        SourceCode = internalAllocationBill.Code,
                                        OutboundType = (int)DcsPartsOutboundType.内部领出,
                                        CustomerAccountId = partsOutboundPlan.CustomerAccountId,
                                        OriginalRequirementBillId = internalAllocationBill.Id,
                                        OriginalRequirementBillCode = internalAllocationBill.Code,
                                        OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.内部领出单,
                                        ReceivingCompanyId = partsOutboundPlan.ReceivingCompanyId,
                                        ReceivingCompanyCode = partsOutboundPlan.ReceivingCompanyCode,
                                        ReceivingCompanyName = partsOutboundPlan.ReceivingCompanyName,
                                        Status = (int)DcsPartsOutboundPlanStatus.出库完成,
                                        ShippingMethod = partsOutboundPlan.ShippingMethod,
                                        IfWmsInterface = partsOutboundPlan.IfWmsInterface,
                                        PartsSalesCategoryId = partsOutboundPlan.PartsSalesCategoryId,
                                        PartsSalesOrderTypeId = partsOutboundPlan.PartsSalesOrderTypeId,
                                        PartsSalesOrderTypeName = partsOutboundPlan.PartsSalesOrderTypeName,
                                        OrderApproveComment = partsOutboundPlan.OrderApproveComment,
                                        SAPPurchasePlanCode = partsOutboundPlan.SAPPurchasePlanCode,
                                        Remark = partsOutboundPlan.Remark,
                                        ERPSourceOrderCode = partsOutboundPlan.ERPSourceOrderCode,
                                        ReceivingAddress = partsOutboundPlan.ReceivingAddress
                                    };
                                    InsertToDatabase(newPartsOutboundPlan);
                                    new PartsOutboundPlanAch(this).InsertPartsOutboundPlanValidate(newPartsOutboundPlan);

                                    newPartsOutboundBill = new PartsOutboundBill();
                                    newPartsOutboundBill.Code = CodeGenerator.Generate("PartsOutboundBill", newPartsOutboundPlan.WarehouseCode);
                                    newPartsOutboundBill.WarehouseName = newPartsOutboundPlan.WarehouseName;
                                    newPartsOutboundBill.OutboundType = newPartsOutboundPlan.OutboundType;
                                    newPartsOutboundBill.WarehouseCode = newPartsOutboundPlan.WarehouseCode;
                                    newPartsOutboundBill.PartsOutboundPlanId = newPartsOutboundPlan.Id;
                                    newPartsOutboundBill.WarehouseId = newPartsOutboundPlan.WarehouseId;
                                    newPartsOutboundBill.StorageCompanyId = newPartsOutboundPlan.StorageCompanyId;
                                    newPartsOutboundBill.StorageCompanyCode = newPartsOutboundPlan.StorageCompanyCode;
                                    newPartsOutboundBill.StorageCompanyName = newPartsOutboundPlan.StorageCompanyName;
                                    newPartsOutboundBill.StorageCompanyType = newPartsOutboundPlan.StorageCompanyType;
                                    newPartsOutboundBill.BranchId = newPartsOutboundPlan.BranchId;
                                    newPartsOutboundBill.BranchCode = newPartsOutboundPlan.BranchCode;
                                    newPartsOutboundBill.BranchName = newPartsOutboundPlan.BranchName;
                                    newPartsOutboundBill.GPMSPurOrderCode = newPartsOutboundPlan.GPMSPurOrderCode;
                                    newPartsOutboundBill.CounterpartCompanyId = newPartsOutboundPlan.CounterpartCompanyId;
                                    newPartsOutboundBill.CounterpartCompanyCode = newPartsOutboundPlan.CounterpartCompanyCode;
                                    newPartsOutboundBill.CounterpartCompanyName = newPartsOutboundPlan.CounterpartCompanyName;
                                    newPartsOutboundBill.CustomerAccountId = newPartsOutboundPlan.CustomerAccountId;
                                    newPartsOutboundBill.OriginalRequirementBillId = newPartsOutboundPlan.OriginalRequirementBillId;
                                    newPartsOutboundBill.OriginalRequirementBillType = newPartsOutboundPlan.OriginalRequirementBillType;
                                    newPartsOutboundBill.OriginalRequirementBillCode = newPartsOutboundPlan.OriginalRequirementBillCode;
                                    newPartsOutboundBill.ReceivingCompanyId = newPartsOutboundPlan.ReceivingCompanyId;
                                    newPartsOutboundBill.ReceivingCompanyCode = newPartsOutboundPlan.ReceivingCompanyCode;
                                    newPartsOutboundBill.ReceivingCompanyName = newPartsOutboundPlan.ReceivingCompanyName;
                                    newPartsOutboundBill.ReceivingWarehouseId = newPartsOutboundPlan.ReceivingWarehouseId;
                                    newPartsOutboundBill.ReceivingWarehouseCode = newPartsOutboundPlan.ReceivingWarehouseCode;
                                    newPartsOutboundBill.ReceivingWarehouseName = newPartsOutboundPlan.ReceivingWarehouseName;
                                    newPartsOutboundBill.SettlementStatus = (int)DcsPartsSettlementStatus.待结算;
                                    newPartsOutboundBill.ShippingMethod = newPartsOutboundPlan.ShippingMethod;
                                    newPartsOutboundBill.PartsSalesCategoryId = newPartsOutboundPlan.PartsSalesCategoryId;
                                    newPartsOutboundBill.PartsSalesOrderTypeId = newPartsOutboundPlan.PartsSalesOrderTypeId;
                                    newPartsOutboundBill.PartsSalesOrderTypeName = newPartsOutboundPlan.PartsSalesOrderTypeName;
                                    newPartsOutboundBill.PartsSalesOrderTypeId = newPartsOutboundPlan.PartsSalesOrderTypeId;
                                    newPartsOutboundBill.PartsSalesOrderTypeName = newPartsOutboundPlan.PartsSalesOrderTypeName;
                                    newPartsOutboundBill.OrderApproveComment = newPartsOutboundPlan.OrderApproveComment;
                                    newPartsOutboundBill.Remark = newPartsOutboundPlan.Remark;
                                    newPartsOutboundBill.SAPPurchasePlanCode = newPartsOutboundPlan.SAPPurchasePlanCode;
                                    newPartsOutboundBill.ERPSourceOrderCode = newPartsOutboundPlan.ERPSourceOrderCode;
                                    InsertToDatabase(newPartsOutboundBill);
                                    new PartsOutboundBillAch(this).InsertPartsOutboundBillValidate(newPartsOutboundBill);
                                }

                                var newPartsOutboundPlanDetail = new PartsOutboundPlanDetail {
                                    PartsOutboundPlan = newPartsOutboundPlan,
                                    SparePartId = assemblyPartRequisitionLink.PartId,
                                    SparePartCode = assemblyPartRequisitionLink.PartCode,
                                    SparePartName = assemblyPartRequisitionLink.PartName,
                                    PlannedAmount = qty,
                                    Price = assemblyEnterprisePartsCost.CostPrice
                                };
                                newPartsOutboundPlan.PartsOutboundPlanDetails.Add(newPartsOutboundPlanDetail);

                                if(assemblyEnterprisePartsCost.Quantity < qty) {
                                    throw new ValidationException(string.Format(ErrorStrings.EnterprisePartsCost_NotFull, assemblyPartRequisitionLink.PartCode));
                                }
                                assemblyEnterprisePartsCost.Quantity = assemblyEnterprisePartsCost.Quantity - qty;
                                assemblyEnterprisePartsCost.CostAmount -= qty * assemblyEnterprisePartsCost.CostPrice;
                                UpdateToDatabase(assemblyEnterprisePartsCost);

                                foreach(var newAssemblypartsStock in assemblyPartsStock) {
                                    if(newAssemblypartsStock.Quantity - (newAssemblypartsStock.LockedQty ?? 0) > 0) {
                                        var newPartsOutboundBillDetail = new PartsOutboundBillDetail();
                                        newPartsOutboundBillDetail.PartsOutboundBill = newPartsOutboundBill;
                                        newPartsOutboundBillDetail.SparePartId = assemblyPartRequisitionLink.PartId;
                                        newPartsOutboundBillDetail.SparePartCode = assemblyPartRequisitionLink.PartCode;
                                        newPartsOutboundBillDetail.SparePartName = assemblyPartRequisitionLink.PartName;
                                        newPartsOutboundBillDetail.WarehouseAreaCode = newAssemblypartsStock.WarehouseArea.Code;
                                        newPartsOutboundBillDetail.WarehouseAreaId = newAssemblypartsStock.WarehouseAreaId;
                                        newPartsOutboundBillDetail.SettlementPrice = assemblyEnterprisePartsCost.CostPrice;
                                        newPartsOutboundBillDetail.CostPrice = assemblyEnterprisePartsCost.CostPrice;

                                        //剩余出库数量
                                        var currentOutboundAmount = newPartsOutboundPlanDetail.PlannedAmount - (newPartsOutboundPlanDetail.OutboundFulfillment ?? 0);
                                        if(currentOutboundAmount == 0) {
                                            break;
                                        } else {
                                            var availableStock = newAssemblypartsStock.Quantity - (newAssemblypartsStock.LockedQty ?? 0);
                                            if(currentOutboundAmount > availableStock) {
                                                newAssemblypartsStock.Quantity = 0;
                                                newAssemblypartsStock.ModifierId = userInfo.Id;
                                                newAssemblypartsStock.ModifierName = userInfo.Name;
                                                newAssemblypartsStock.ModifyTime = DateTime.Now;
                                                UpdateToDatabase(newAssemblypartsStock);

                                                newPartsOutboundBillDetail.OutboundAmount = availableStock;
                                            } else {
                                                newAssemblypartsStock.Quantity = newAssemblypartsStock.Quantity - currentOutboundAmount;
                                                newAssemblypartsStock.ModifierId = userInfo.Id;
                                                newAssemblypartsStock.ModifierName = userInfo.Name;
                                                newAssemblypartsStock.ModifyTime = DateTime.Now;
                                                UpdateToDatabase(newAssemblypartsStock);

                                                newPartsOutboundBillDetail.OutboundAmount = currentOutboundAmount;
                                            }
                                            newPartsOutboundPlanDetail.OutboundFulfillment = (newPartsOutboundPlanDetail.OutboundFulfillment ?? 0) + newPartsOutboundBillDetail.OutboundAmount;
                                        }
                                        newPartsOutboundBill.PartsOutboundBillDetails.Add(newPartsOutboundBillDetail);
                                    }
                                }
                            }
                        }
                    }

                    //减少待发区数量
                    var partsStock = partsStocks.FirstOrDefault(r => r.WarehouseAreaId == partsOutboundBillDetail.WarehouseAreaId && r.PartId == partsOutboundBillDetail.SparePartId);
                    if(partsStock == null)
                        throw new ValidationException(string.Format(ErrorStrings.PartsOutboundBill_Validation6, partsOutboundBillDetail.SparePartCode, partsOutboundBillDetail.WarehouseAreaCode));
                    if(partsOutboundBillDetail.OutboundAmount > partsStock.Quantity)
                        throw new ValidationException(string.Format(ErrorStrings.PartsOutboundBill_Validation11, partsOutboundBillDetail.SparePartCode, partsOutboundBillDetail.WarehouseAreaCode));

                    partsStock.Quantity -= partsOutboundBillDetail.OutboundAmount;
                    partsStock.LockedQty -= partsOutboundBillDetail.OutboundAmount;
                    if(partsStock.LockedQty < 0) {
                        throw new ValidationException(string.Format(ErrorStrings.PartsShippingOrder_Validation36, partsOutboundBillDetail.SparePartCode, partsOutboundBillDetail.WarehouseAreaCode));
                    }
                    partsStock.ModifierId = userInfo.Id;
                    partsStock.ModifierName = userInfo.Name;
                    partsStock.ModifyTime = DateTime.Now;
                    if(partsStock.Quantity < 0)
                        throw new ValidationException(ErrorStrings.PartsStock_Validation2);
                    UpdateToDatabase(partsStock);
                    //减少企业成本库存
                    var enterprisePartsCost = enterprisePartsCosts.FirstOrDefault(r => r.SparePartId == partsOutboundBillDetail.SparePartId);
                    if(partsOutboundBill.OutboundType != (int)DcsPartsOutboundType.配件调拨) {
                        if(enterprisePartsCost != null) {
                            enterprisePartsCost.Quantity -= partsOutboundBillDetail.OutboundAmount;
                            enterprisePartsCost.CostAmount -= partsOutboundBillDetail.OutboundAmount * enterprisePartsCost.CostPrice;
                            UpdateToDatabase(enterprisePartsCost);
                        } else {
                            throw new ValidationException(string.Format(ErrorStrings.PartsOutboundBill_Validation8, partsOutboundBillDetail.SparePartCode));
                        }
                    }
                    //配件库存批次明细
                    if(!string.IsNullOrEmpty(partsOutboundBillDetail.BatchNumber)) {
                        var partsStockBatchDetail = partsStockBatchDetails.FirstOrDefault(r => r.BatchNumber.ToLower() == partsOutboundBillDetail.BatchNumber.ToLower() && r.Quantity == partsOutboundBillDetail.OutboundAmount);
                        if(partsStockBatchDetail == null)
                            throw new ValidationException(string.Format(ErrorStrings.PartsOutboundBill_Validation2, partsOutboundBillDetail.BatchNumber));
                        DeleteFromDatabase(partsStockBatchDetail);
                    }
                    if(partsOutboundPlan.StorageCompanyType == (int)DcsCompanyType.分公司 || partsOutboundPlan.StorageCompanyType == (int)DcsCompanyType.代理库 || partsOutboundPlan.StorageCompanyType == (int)DcsCompanyType.服务站兼代理库) {
                        //填充配件出库单清单
                        if(!partsAmountDetails.ContainsKey(partsOutboundBillDetail.SparePartId)) {
                            throw new ValidationException(string.Format(ErrorStrings.PartsOutboundBill_Validation8, partsOutboundBillDetail.SparePartCode));
                        } else {
                            partsOutboundBillDetail.CostPrice = partsAmountDetails[partsOutboundBillDetail.SparePartId];

                            if(partsOutboundPlan.OutboundType == (int)DcsPartsOutboundType.内部领出)
                                partsOutboundBillDetail.SettlementPrice = partsAmountDetails[partsOutboundBillDetail.SparePartId];
                        }
                        if(partsOutboundBillDetail.CostPrice == 0) {
                            throw new ValidationException(string.Format(ErrorStrings.PartsOutboundBill_Validation12, partsOutboundBillDetail.SparePartCode));
                        }
                    }
                }

                //出库单生成时，系统增加判断：
                //	上游的销售订单的“是否换货”字段为“是”的，出库单“结算状态”被自动赋值为“已结算”；
                //	“是否换货”字段为“否”的，默认为“待结算”。
                if(partsOutboundBill.OriginalRequirementBillType == (int)DcsOriginalRequirementBillType.配件销售退货单) {
                    var partsSalesReturnBills = this.ObjectContext.PartsSalesReturnBills.Where(r => r.Id == partsOutboundBill.OriginalRequirementBillId).FirstOrDefault();
                    if(partsSalesReturnBills != null&&partsSalesReturnBills.IsBarter == true)
                            partsOutboundBill.SettlementStatus = (int)DcsPartsSettlementStatus.已结算;
                }
                new PartsOutboundBillAch(this).InsertPartsOutboundBillValidate(partsOutboundBill);

                new PartsOutboundPlanAch(this).更新出库计划单状态(partsOutboundPlan);

                if(partsOutboundBill.OutboundType == (int)DcsPartsOutboundType.配件销售) {
                    new CustomerAccountAch(this).销售出库过账记录流水(partsOutboundPlan, (decimal)partsOutboundBill.PartsOutboundBillDetails.Sum(r => r.SettlementPrice * r.OutboundAmount), now.AddSeconds(number));
                    number++;
                }
				//发运确认时，当发运为配件公司的采购退货时，需将配件公司原入库【精确码追溯】的状态改为“不考核”
				//根据出库计划单查询追溯信息中的追溯码对应的入库信息：
				if(partsOutboundPlan.StorageCompanyType==(int)DcsCompanyType.分公司&&partsOutboundPlan.OutboundType==(int)DcsPartsOutboundType.采购退货){
					var unKh= (from a in ObjectContext.AccurateTraces.Where(t=> t.Type==(int)DCSAccurateTraceType.入库 && t.Status==(int)DCSAccurateTraceStatus.有效&& t.InQty==(t.OutQty??0) && t.InQty>0)
				          join b in  ObjectContext.AccurateTraces.Where(t=>t.SourceBillId==partsOutboundPlanId ) on new {a.TraceCode,a.CompanyId} equals new {b.TraceCode,b.CompanyId}
						  select a).ToArray();
					if(unKh.Any()){
						foreach(var  item  in unKh){
							item.Status=(int)DCSAccurateTraceStatus.采购退货不考核;
							item.ModifierId=userInfo.Id;
							item.ModifierName=userInfo.Name;
							item.ModifyTime=DateTime.Now;
							UpdateToDatabase(item);
						}
					}
				}
				

            }
            var company = ObjectContext.Companies.SingleOrDefault(r => r.Id == partsShippingOrder.ReceivingCompanyId);
            if(company!=null&&(company.Type == (int)DcsCompanyType.集团企业 || company.Type == (int)DcsCompanyType.出口客户)) {
                var companyId = company.Id;
                if(company.Type == (int)DcsCompanyType.出口客户) {
                    var exportCustomerInfo = ObjectContext.ExportCustomerInfoes.Where(t => t.Id == company.Id).First();
                    companyId = exportCustomerInfo.InvoiceCompanyId.Value;
                }
                //新增收货单位入库追溯
                var outTrace = ObjectContext.AccurateTraces.Where(t => partsOutboundPlanIds.Contains(t.SourceBillId) && t.Type == (int)DCSAccurateTraceType.出库 && t.Status == (int)DCSAccurateTraceStatus.有效).ToArray();
                var oldInTrace = ObjectContext.AccurateTraces.Where(t => t.CompanyId == companyId && t.Type == (int)DCSAccurateTraceType.入库 && t.Status == (int)DCSAccurateTraceStatus.有效).ToArray();
                //若收货企业已存在此精确码的【精确码追溯】有效入库信息，则作废

                if(outTrace.Any()) {
                    foreach(var outDetail in outTrace) {                        
                        var inBound = new AccurateTrace {
                            Type = (int)DCSAccurateTraceType.入库,
                            CompanyType = (int)DCSAccurateTraceCompanyType.服务站,
                            CompanyId = companyId,
                            SourceBillId = partsShippingOrder.Id,
                            PartId = outDetail.PartId,
                            TraceCode = outDetail.TraceCode,
                            SIHLabelCode = outDetail.SIHLabelCode,
                            Status = (int)DCSAccurateTraceStatus.有效,
                            TraceProperty = outDetail.TraceProperty,
                            BoxCode = outDetail.BoxCode,
                        };
                        if(outDetail.TraceProperty == (int)DCSTraceProperty.精确追溯) {
                            inBound.InQty = 1;
                        } else {
                            inBound.InQty = outDetail.OutQty;
                        }
                        new AccurateTraceAch(this).ValidataAccurateTrace(inBound);
                    }
                }
            }
            //当收货企业=集团客户时，任何发运单在做发运后，状态=收货确认
            if(company != null&&company.Type == (int)DcsCompanyType.集团企业) {
                partsShippingOrder.Status = (int)DcsPartsShippingOrderStatus.收货确认;
             }
            ObjectContext.SaveChanges();
           
            if(company != null && company.Type == (int)DcsCompanyType.分公司 && !isReceive) {
                foreach(var item in partsShippingOrder.PartsShippingOrderDetails) {
                    item.ConfirmedAmount = item.ShippingAmount;
                }
                this.收货确认配件发运单(partsShippingOrder);
            }
			  transaction.Complete();
              }
        }

    }
}
