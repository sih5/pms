﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using System.Linq;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DealerPartsTransferOrderAch : DcsSerivceAchieveBase {
        public void 更新服务站配件调拨单(DealerPartsTransferOrder dealerPartsTransferOrder) {
            var dbDealerPartsTransferOrder = ObjectContext.DealerPartsTransferOrders.Where(r => r.Id == dealerPartsTransferOrder.Id && r.Status == (int)DcsDealerPartsTransferOrderStatus.新增).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbDealerPartsTransferOrder == null) {
                throw new ValidationException(ErrorStrings.DealerPartsTransferOrder_Validation1);
            }
        }
        public void 提交服务站配件调拨单(DealerPartsTransferOrder dealerPartsTransferOrder) {
            var dbDealerPartsTransferOrder = ObjectContext.DealerPartsTransferOrders.Where(r => r.Id == dealerPartsTransferOrder.Id && r.Status == (int)DcsDealerPartsTransferOrderStatus.新增).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbDealerPartsTransferOrder == null) {
                throw new ValidationException(ErrorStrings.DealerPartsTransferOrder_Validation2);
            }
            var userInfo = Utils.GetCurrentUserInfo();
            dealerPartsTransferOrder.Status = (int)DcsDealerPartsTransferOrderStatus.提交;
            dealerPartsTransferOrder.SubmitterId = userInfo.EnterpriseId;
            dealerPartsTransferOrder.SubmitterName = userInfo.EnterpriseName;
            dealerPartsTransferOrder.SubmitTime = DateTime.Now;
        }

        public void 作废服务站配件调拨单(DealerPartsTransferOrder dealerPartsTransferOrder) {
            var dbDealerPartsTransferOrder = ObjectContext.DealerPartsTransferOrders.Where(r => r.Id == dealerPartsTransferOrder.Id && r.Status == (int)DcsDealerPartsTransferOrderStatus.新增).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbDealerPartsTransferOrder == null) {
                throw new ValidationException(ErrorStrings.DealerPartsTransferOrder_Validation3);
            }
            var userInfo = Utils.GetCurrentUserInfo();
            dealerPartsTransferOrder.Status = (int)DcsDealerPartsTransferOrderStatus.作废;
            dealerPartsTransferOrder.AbandonerId = userInfo.EnterpriseId;
            dealerPartsTransferOrder.SubmitterName = userInfo.EnterpriseName;
            dealerPartsTransferOrder.AbandonTime = DateTime.Now;
        }

        public void 审核服务站配件调拨单(DealerPartsTransferOrder dealerPartsTransferOrder) {
            var dbDealerPartsTransferOrder = ObjectContext.DealerPartsTransferOrders.Where(r => r.Id == dealerPartsTransferOrder.Id && r.Status == (int)DcsDealerPartsTransferOrderStatus.提交).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbDealerPartsTransferOrder == null) {
                throw new ValidationException(ErrorStrings.DealerPartsTransferOrder_Validation4);
            }
            var partIds = dealerPartsTransferOrder.DealerPartsTransOrderDetails.Select(r => r.SparePartId).ToArray();
            var partDeleaveInformationNos = this.ObjectContext.PartDeleaveInformations.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.BranchId == dealerPartsTransferOrder.BranchId && r.PartsSalesCategoryId == dealerPartsTransferOrder.NoWarrantyBrandId && partIds.Contains(r.OldPartId)).ToArray();
            var dealerPartsStockNos = this.ObjectContext.DealerPartsStocks.Where(r => partIds.Contains(r.SparePartId) && r.SalesCategoryId == dealerPartsTransferOrder.NoWarrantyBrandId && r.BranchId == dealerPartsTransferOrder.BranchId && r.DealerId == dealerPartsTransferOrder.DealerId);
            foreach(var dealerPartsTransOrderDetail in dealerPartsTransferOrder.DealerPartsTransOrderDetails) {
                var dealerPartsStock = dealerPartsStockNos.FirstOrDefault(r => r.SparePartId == dealerPartsTransOrderDetail.SparePartId);
                var partDeleaveInformation = partDeleaveInformationNos.FirstOrDefault(r => r.OldPartId == dealerPartsTransOrderDetail.SparePartId);
                var amount = dealerPartsTransOrderDetail.Amount * (partDeleaveInformation == null ? 1 : partDeleaveInformation.DeleaveAmount);
                if(dealerPartsStock == null || dealerPartsStock.Quantity < amount)
                    throw new ValidationException(string.Format(ErrorStrings.DealerPartsTransferOrder_Validation5, dealerPartsTransOrderDetail.SparePartName));
                dealerPartsStock.Quantity = dealerPartsStock.Quantity - amount;
                new DealerPartsStockAch(this.DomainService).UpdateDealerPartsStock(dealerPartsStock);
            }
            var partDeleaveInformations = this.ObjectContext.PartDeleaveInformations.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.BranchId == dealerPartsTransferOrder.BranchId && r.PartsSalesCategoryId == dealerPartsTransferOrder.WarrantyBrandId && partIds.Contains(r.OldPartId)).ToArray();
            var dealerPartsStocks = this.ObjectContext.DealerPartsStocks.Where(r => partIds.Contains(r.SparePartId) && r.SalesCategoryId == dealerPartsTransferOrder.WarrantyBrandId && r.BranchId == dealerPartsTransferOrder.BranchId && r.DealerId == dealerPartsTransferOrder.DealerId);
            foreach(var dealerPartsTransOrderDetail in dealerPartsTransferOrder.DealerPartsTransOrderDetails) {
                var dealerPartsStock = dealerPartsStocks.FirstOrDefault(r => r.SparePartId == dealerPartsTransOrderDetail.SparePartId);
                var partDeleaveInformation = partDeleaveInformations.FirstOrDefault(r => r.OldPartId == dealerPartsTransOrderDetail.SparePartId);
                var amount = dealerPartsTransOrderDetail.Amount * (partDeleaveInformation == null ? 1 : partDeleaveInformation.DeleaveAmount);
                if(dealerPartsStock == null) {
                    var newdealerPartsStock = new DealerPartsStock();
                    newdealerPartsStock.BranchId = dealerPartsTransferOrder.BranchId;
                    newdealerPartsStock.DealerId = dealerPartsTransferOrder.DealerId;
                    newdealerPartsStock.DealerCode = dealerPartsTransferOrder.DealerCode;
                    newdealerPartsStock.DealerName = dealerPartsTransferOrder.DealerName;
                    newdealerPartsStock.SalesCategoryId = dealerPartsTransferOrder.WarrantyBrandId;
                    newdealerPartsStock.SalesCategoryName = dealerPartsTransferOrder.WarrantyBrandName;
                    newdealerPartsStock.SparePartId = dealerPartsTransOrderDetail.SparePartId;
                    newdealerPartsStock.SparePartCode = dealerPartsTransOrderDetail.SparePartCode;
                    newdealerPartsStock.Quantity = amount;
                    new DealerPartsStockAch(this.DomainService).InsertDealerPartsStock(newdealerPartsStock);
                } else {
                    dealerPartsStock.Quantity = dealerPartsStock.Quantity + amount;
                    new DealerPartsStockAch(this.DomainService).UpdateDealerPartsStock(dealerPartsStock);
                }
            }
            var userInfo = Utils.GetCurrentUserInfo();
            dealerPartsTransferOrder.Status = (int)DcsDealerPartsTransferOrderStatus.已审核;
            dealerPartsTransferOrder.CheckerId = userInfo.Id;
            dealerPartsTransferOrder.CheckTime = DateTime.Now;
        }

        public void 驳回服务站配件调拨单(DealerPartsTransferOrder dealerPartsTransferOrder) {
            var dbDealerPartsTransferOrder = ObjectContext.DealerPartsTransferOrders.Where(r => r.Id == dealerPartsTransferOrder.Id && r.Status == (int)DcsDealerPartsTransferOrderStatus.提交).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbDealerPartsTransferOrder == null) {
                throw new ValidationException(ErrorStrings.DealerPartsTransferOrder_Validation6);
            }
            var userInfo = Utils.GetCurrentUserInfo();
            dealerPartsTransferOrder.Status = (int)DcsDealerPartsTransferOrderStatus.作废;
            dealerPartsTransferOrder.RejecterId = userInfo.Id;
            dealerPartsTransferOrder.RejecterName = userInfo.Name;
            dealerPartsTransferOrder.RejectTime = DateTime.Now;
        }

    }

    partial class DcsDomainService {
        [Update(UsingCustomMethod = true)]
        public void 更新服务站配件调拨单(DealerPartsTransferOrder dealerPartsTransferOrder) {
            new DealerPartsTransferOrderAch(this).更新服务站配件调拨单(dealerPartsTransferOrder);
        }
        [Update(UsingCustomMethod = true)]
        public void 审核服务站配件调拨单(DealerPartsTransferOrder dealerPartsTransferOrder) {
            new DealerPartsTransferOrderAch(this).审核服务站配件调拨单(dealerPartsTransferOrder);
        }
        [Update(UsingCustomMethod = true)]
        public void 作废服务站配件调拨单(DealerPartsTransferOrder dealerPartsTransferOrder) {
            new DealerPartsTransferOrderAch(this).作废服务站配件调拨单(dealerPartsTransferOrder);
        }
        [Update(UsingCustomMethod = true)]
        public void 提交服务站配件调拨单(DealerPartsTransferOrder dealerPartsTransferOrder) {
            new DealerPartsTransferOrderAch(this).提交服务站配件调拨单(dealerPartsTransferOrder);
        }
        [Update(UsingCustomMethod = true)]
        public void 驳回服务站配件调拨单(DealerPartsTransferOrder dealerPartsTransferOrder) {
            new DealerPartsTransferOrderAch(this).驳回服务站配件调拨单(dealerPartsTransferOrder);
        }
    }
}
