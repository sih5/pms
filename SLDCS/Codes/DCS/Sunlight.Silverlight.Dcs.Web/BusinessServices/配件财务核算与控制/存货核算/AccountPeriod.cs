﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
using System.ServiceModel.DomainServices.Server;

namespace Sunlight.Silverlight.Dcs.Web
{
    partial class AccountPeriodAch : DcsSerivceAchieveBase
    {
        public AccountPeriodAch(DcsDomainService domainService)
            : base(domainService) {
        }

        public IQueryable<AccountPeriod> GetAccountPeriodQuerys()
        {
            return ObjectContext.AccountPeriods.OrderBy(r => r.Id);

        }
        public void InsertAccountPeriod(AccountPeriod accountPeriod)
        {
            //先判断账期年，账期月是否存在
            var oldPeriod = ObjectContext.AccountPeriods.Where(r => r.Month == accountPeriod.Month && r.Year == accountPeriod.Year).SingleOrDefault();
            if(null!=oldPeriod){
                throw new ValidationException(ErrorStrings.AccountPeriod_Validation1);
            }
            var userInfo = Utils.GetCurrentUserInfo();
            accountPeriod.CreateTime = DateTime.Now;
            accountPeriod.CreatorId = userInfo.Id;
            accountPeriod.CreatorName = userInfo.Name;
            InsertToDatabase(accountPeriod);
        }
        public void 开启(AccountPeriod accountPeriod)
        {
            var userInfo = Utils.GetCurrentUserInfo();
            accountPeriod.ModifyTime = DateTime.Now;
            accountPeriod.ModifierId = userInfo.Id;
            accountPeriod.ModifierName = userInfo.Name;
            accountPeriod.Status = (int)DcsAccountPeriodStatus.有效;
            UpdateToDatabase(accountPeriod);
        }
        public void 关闭(AccountPeriod accountPeriod)
        {
            var userInfo = Utils.GetCurrentUserInfo();
            accountPeriod.ModifyTime = DateTime.Now;
            accountPeriod.ModifierId = userInfo.Id;
            accountPeriod.ModifierName = userInfo.Name;
            accountPeriod.Status = (int)DcsAccountPeriodStatus.已关闭;
            UpdateToDatabase(accountPeriod);
        }
        public void 新增账期(AccountPeriod accountPeriod)
        {
            this.InsertAccountPeriod(accountPeriod);
        }
        public void 验证过账日期是否有效(DateTime? invoiceDate)
        {
            string year = invoiceDate.Value.Year.ToString();
            string month = invoiceDate.Value.Month.ToString();
            var accountPeriod = ObjectContext.AccountPeriods.Where(r => r.Month == month && r.Year == year).SingleOrDefault();
            if (null == accountPeriod)
            {
                throw new ValidationException(ErrorStrings.AccountPeriod_Validation2);
            }
            else if ((int)DcsAccountPeriodStatus.已关闭 == accountPeriod.Status)
            {
                throw new ValidationException(ErrorStrings.AccountPeriod_Validation3);
            }
        }
    }
    partial class DcsDomainService
    {// 已经使用工具进行处理 2017/4/29 12:12:33
        //原分布类的函数全部转移到Ach                                                               
        public IQueryable<AccountPeriod> GetAccountPeriodQuerys()
        {
            return new AccountPeriodAch(this).GetAccountPeriodQuerys();
        }
        public void InsertAccountPeriod(AccountPeriod accountPeriod)
        {
            new AccountPeriodAch(this).InsertAccountPeriod(accountPeriod);
        }
        [Update(UsingCustomMethod = true)]
        public void 开启(AccountPeriod accountPeriod)
        {
            new AccountPeriodAch(this).开启(accountPeriod);
        }
        [Update(UsingCustomMethod = true)]
        public void 关闭(AccountPeriod accountPeriod)
        {
            new AccountPeriodAch(this).关闭(accountPeriod);
        }
        public void 新增账期(AccountPeriod accountPeriod)
        {
            new AccountPeriodAch(this).新增账期(accountPeriod);
        }
        public void 验证过账日期是否有效(DateTime? invoiceDate)
        {
            new AccountPeriodAch(this).验证过账日期是否有效(invoiceDate);
        }
    }
}
