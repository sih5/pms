﻿using System.ServiceModel.DomainServices.Server;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class FinancialSnapshotSetAch : DcsSerivceAchieveBase {
        public FinancialSnapshotSetAch(DcsDomainService domainService)
            : base(domainService) {
        }
        

        public void 作废财务快照(FinancialSnapshotSet financialSnapshotSet) {
            financialSnapshotSet.Status = (int)DcsSnapshoStatus.作废;
            UpdateFinancialSnapshotSet(financialSnapshotSet);
            this.UpdateFinancialSnapshotSetValidate(financialSnapshotSet);
        }
        //public void 生效财务快照(FinancialSnapshotSet financialSnapshotSet) {   
        //    var dbPartsHistoryStock = from dbFinancialSnapshotSet in ObjectContext.FinancialSnapshotSets
        //                              from dbSalesUnit in ObjectContext.SalesUnits// on financialSnapshotSet.PartsSalesCategoryId equals salesUnit.PartsSalesCategoryId
        //                              from dbSalesUnitAffiWarehouse in ObjectContext.SalesUnitAffiWarehouses// on salesUnit.Id equals salesUnitAffiWarehouse.SalesUnitId
        //                              from dbPartsStock in ObjectContext.PartsStocks //on salesUnitAffiWarehouse.WarehouseId equals partsStock.WarehouseId 
        //                              from dbPartsPlannedPrice in ObjectContext.PartsPlannedPrices //on partsStock.PartId equals e.SparePartId   
        //                              where dbFinancialSnapshotSet.PartsSalesCategoryId==dbSalesUnit.PartsSalesCategoryId && dbFinancialSnapshotSet.Status == (int)DcsSnapshoStatus.新建 && dbFinancialSnapshotSet.SnapshotTime >= DateTime.Today
        //                              where dbSalesUnit.Id==dbSalesUnitAffiWarehouse.SalesUnitId
        //                              where dbSalesUnitAffiWarehouse.WarehouseId == dbPartsStock.WarehouseId
        //                              where dbPartsStock.PartId==dbPartsPlannedPrice.SparePartId
        //                              where dbFinancialSnapshotSet.PartsSalesCategoryId==dbPartsPlannedPrice.PartsSalesCategoryId
        //                              select new PartsHistoryStock {
        //                              PartId=dbPartsStock.PartId,
        //                              WarehouseId=dbPartsStock.WarehouseId,
        //                              StorageCompanyId=dbPartsStock.StorageCompanyId,
        //                              StorageCompanyType=dbPartsStock.StorageCompanyType,
        //                              BranchId=dbPartsStock.BranchId,
        //                              WarehouseAreaId=dbPartsStock.WarehouseAreaId,
        //                              WarehouseAreaCategoryId=dbPartsStock.WarehouseAreaCategoryId,
        //                              Quantity=dbPartsStock.Quantity,
        //                              PlannedPrice=dbPartsPlannedPrice.PlannedPrice,
        //                              PlannedPriceAmount=dbPartsStock.Quantity*dbPartsPlannedPrice.PlannedPrice,
        //                              Remark=dbPartsStock.Remark,
        //                              SnapshoTime=dbFinancialSnapshotSet.SnapshotTime
        //                              };
        //    InsertToDatabase(dbPartsHistoryStock);
        //    InsertPartsHistoryStockVaild(dbPartsHistoryStock);
        //}
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:11:15
        //原分布类的函数全部转移到Ach                                                               
        
        [Update(UsingCustomMethod = true)]
        public void 作废财务快照(FinancialSnapshotSet financialSnapshotSet) {
            new FinancialSnapshotSetAch(this).作废财务快照(financialSnapshotSet);
        }
    }
}
