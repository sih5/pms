﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
using System.ServiceModel.DomainServices.Server;

namespace Sunlight.Silverlight.Dcs.Web
{
    partial class FundMonthlySettleBillAch : DcsSerivceAchieveBase
    {
        public FundMonthlySettleBillAch(DcsDomainService domainService)
            : base(domainService) {
        }

        public IQueryable<FundMonthlySettleBill> GetFundMonthlySettleBillQuerys()
        {
            return ObjectContext.FundMonthlySettleBills.OrderBy(r => r.Id);
        }

        public void InsertFundMonthlySettleBill(FundMonthlySettleBill fundMonthlySettleBill)
        {
            //先判断年，月是否存在
            //var oldPeriod = ObjectContext.FundMonthlySettleBills.Where(r => r.MonthlySettleYear == fundMonthlySettleBill.MonthlySettleYear && r.MonthlySettleMonth == fundMonthlySettleBill.MonthlySettleMonth && r.Status == (int)DcsBaseDataStatus.有效).SingleOrDefault();
            //if(null!=oldPeriod){
            //    throw new ValidationException("已存在相同的账期");
            //}
            //var userInfo = Utils.GetCurrentUserInfo();
            //fundMonthlySettleBill.CreateTime = DateTime.Now;
            //fundMonthlySettleBill.CreatorId = userInfo.Id;
            //fundMonthlySettleBill.CreatorName = userInfo.Name;
            //fundMonthlySettleBill.Status = (int)DcsBaseDataStatus.有效;
            //InsertToDatabase(fundMonthlySettleBill);
        }
        public void 作废资金月结(FundMonthlySettleBill fundMonthlySettleBill)
        {
            var userInfo = Utils.GetCurrentUserInfo();
            fundMonthlySettleBill.ModifyTime = DateTime.Now;
            fundMonthlySettleBill.ModifierId = userInfo.Id;
            fundMonthlySettleBill.ModifierName = userInfo.Name;
            fundMonthlySettleBill.Status = (int)DcsBaseDataStatus.作废;
            UpdateToDatabase(fundMonthlySettleBill);
            //删除明细
            //var details = ObjectContext.FundMonthlySettleBillDetails.Where(r => r.FundMonthlySettleBillId == fundMonthlySettleBill.Id);
            //foreach (var detail in details) {
            //    DeleteFromDatabase(detail);
            //}
        }

        public void 新增资金月结(FundMonthlySettleBill fundMonthlySettleBill) {
            var oldPeriod = ObjectContext.FundMonthlySettleBills.Where(r => r.MonthlySettleYear == fundMonthlySettleBill.MonthlySettleYear && r.MonthlySettleMonth == fundMonthlySettleBill.MonthlySettleMonth && r.Status == (int)DcsBaseDataStatus.有效).SingleOrDefault();
            if (null != oldPeriod) {
                throw new ValidationException(ErrorStrings.FundMonthlySettleBill_Validation1);
            }
            DateTime bdate = new DateTime(int.Parse(fundMonthlySettleBill.MonthlySettleYear),int.Parse(fundMonthlySettleBill.MonthlySettleMonth),1,0,0,0);
            var lastMonth = bdate.AddMonths(-1);
            string y = lastMonth.Year.ToString();
            string m = lastMonth.Month.ToString();
            var last = ObjectContext.FundMonthlySettleBills.Where(r => r.MonthlySettleYear == y && r.MonthlySettleMonth == m && r.Status == 1).FirstOrDefault();
            if (last == null) { 
                throw new ValidationException(string.Format(ErrorStrings.FundMonthlySettleBill_Validation2,lastMonth.Year.ToString(),lastMonth.Month.ToString()));
            }
            
            var userInfo = Utils.GetCurrentUserInfo();
            fundMonthlySettleBill.CreateTime = DateTime.Now;
            fundMonthlySettleBill.CreatorId = userInfo.Id;
            fundMonthlySettleBill.CreatorName = userInfo.Name;
            fundMonthlySettleBill.Status = (int)DcsBaseDataStatus.有效;
            
            
            //查询清单
            var details = this.getFundMonthlySettleBillDetailsByDate(fundMonthlySettleBill.MonthlySettleYear, fundMonthlySettleBill.MonthlySettleMonth, userInfo.EnterpriseId);
            if (!details.Any())
                throw new ValidationException(ErrorStrings.FundMonthlySettleBill_Validation3);
            foreach (var detail in details) {
                var fundMonthlySettleBillDetail = new FundMonthlySettleBillDetail();
                fundMonthlySettleBillDetail.AccountGroupId = detail.AccountGroupId;
                fundMonthlySettleBillDetail.AccountGroupName = detail.AccountGroupName;
                fundMonthlySettleBillDetail.CustomerAccountId = detail.CustomerAccountId;
                fundMonthlySettleBillDetail.CustomerCompanyId = detail.CustomerCompanyId;
                fundMonthlySettleBillDetail.CustomerCompanyCode = detail.CustomerCompanyCode;
                fundMonthlySettleBillDetail.CustomerCompanyName = detail.CustomerCompanyName;
                fundMonthlySettleBillDetail.MonthlySettleYear = fundMonthlySettleBill.MonthlySettleYear;
                fundMonthlySettleBillDetail.MonthlySettleMonth = fundMonthlySettleBill.MonthlySettleMonth;
                fundMonthlySettleBillDetail.InitialAmount = detail.InitialAmount;
                fundMonthlySettleBillDetail.ChangeAmount = detail.ChangeAmount;
                fundMonthlySettleBillDetail.FinalAmount = detail.FinalAmount;

                fundMonthlySettleBill.FundMonthlySettleBillDetails.Add(fundMonthlySettleBillDetail);
            }
                InsertToDatabase(fundMonthlySettleBill);

        }

        public IEnumerable<FundMonthlySettleBillDetail> getFundMonthlySettleBillDetailsByDate(string year, string month,int enterpriseId) {
            string sql = @" select rownum as id,0 as FundMonthlySettleBillId,d.name as AccountGroupName,m.id as CustomerAccountId,m.AccountGroupId,m.customercompanyid,c.code as CustomerCompanyCode,c.name as CustomerCompanyName,
nvl(a.FinalAmount,0) as InitialAmount ,b.amount as ChangeAmount,(nvl(a.FinalAmount,0) +nvl(b.amount,0)) as FinalAmount ,{5} as MonthlySettleYear,{6} as MonthlySettleMonth
                            from customeraccount m
                            left join (select a.FinalAmount,a.customeraccountid from FundMonthlySettleBillDetail a inner join FundMonthlySettleBill b on a.fundmonthlysettlebillid = b.id where b.status=1 {7} )a on a.customeraccountid = m.id
                            left join(
                            select b.id,sum(a.changeamount) amount from customeraccounthisdetail a 
                            inner join customeraccount b on a.customeraccountid = b.id
                            inner join accountgroup c on c.id = b.accountgroupid
                             where serialtype = 3  and c.salescompanyid = {0} and a.processdate >=to_date('{1}','yyyy-mm-dd hh24:mi:ss')
                             and a.invoicedate>=to_date('{2}','yyyy-mm-dd hh24:mi:ss') and a.invoicedate <to_date('{3}','yyyy-mm-dd hh24:mi:ss') 
                             group by b.id) b on m.id = b.id
                             inner join company c on c.id = m.customercompanyid
                             inner join accountgroup d on d.id = m.accountgroupid
                             where m.status=1 and d.salescompanyid ={4}
                              order by m.accountbalance ";
            DateTime bdate = new DateTime(int.Parse(year),int.Parse(month),1,0,0,0);
            DateTime edate = bdate.AddMonths(1);
            var lastMonth = bdate.AddMonths(-1);
            string param = " and a.monthlysettlemonth = "+ lastMonth.Month +" and a.monthlysettleyear = "+ lastMonth.Year +" ";
            sql = string.Format(sql, enterpriseId, bdate, bdate, edate, enterpriseId,year,month,param);
            var search = ObjectContext.ExecuteStoreQuery<FundMonthlySettleBillDetail>(sql).ToList();
            return search;
        }
    }
    partial class DcsDomainService
    {// 已经使用工具进行处理 2017/4/29 12:12:33
        //原分布类的函数全部转移到Ach                                                               
        public IQueryable<FundMonthlySettleBill> GetFundMonthlySettleBillQuerys()
        {
            return new FundMonthlySettleBillAch(this).GetFundMonthlySettleBillQuerys();
        }
        public void InsertFundMonthlySettleBill(FundMonthlySettleBill fundMonthlySettleBill)
        {
            new FundMonthlySettleBillAch(this).InsertFundMonthlySettleBill(fundMonthlySettleBill);
        }
        [Update(UsingCustomMethod = true)]
        public void 作废资金月结(FundMonthlySettleBill fundMonthlySettleBill)
        {
            new FundMonthlySettleBillAch(this).作废资金月结(fundMonthlySettleBill);
        }

         public void 新增资金月结(FundMonthlySettleBill fundMonthlySettleBill)
        {
            new FundMonthlySettleBillAch(this).新增资金月结(fundMonthlySettleBill);
        }

    }
}
