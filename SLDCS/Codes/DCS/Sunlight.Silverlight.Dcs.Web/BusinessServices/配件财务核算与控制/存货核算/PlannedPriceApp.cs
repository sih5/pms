﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class PlannedPriceAppAch : DcsSerivceAchieveBase {
        public PlannedPriceAppAch(DcsDomainService domainService)
            : base(domainService) {
        }
        

        public void 作废计划价申请单(PlannedPriceApp plannedPriceApp) {
            var dbPlannedPriceApp = ObjectContext.PlannedPriceApps.Where(r => r.Id == plannedPriceApp.Id && r.Status == (int)DcsPlannedPriceAppStatus.新增).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbPlannedPriceApp == null)
                throw new ValidationException(ErrorStrings.PlannedPriceApp_Validation2);
            UpdateToDatabase(plannedPriceApp);
            plannedPriceApp.Status = (int)DcsPlannedPriceAppStatus.作废;
            var userInfo = Utils.GetCurrentUserInfo();
            plannedPriceApp.ModifierId = userInfo.Id;
            plannedPriceApp.ModifierName = userInfo.Name;
            plannedPriceApp.ModifyTime = DateTime.Now;
        }

        public void 审核计划价申请单(int plannedPriceAppId) {
            var dbPlannedPriceApp = ObjectContext.PlannedPriceApps.SingleOrDefault(r => r.Id == plannedPriceAppId && r.Status == (int)DcsPlannedPriceAppStatus.新增);
            if(dbPlannedPriceApp == null) {
                throw new ValidationException(ErrorStrings.PlannedPriceApp_Validation1);
            }
            foreach (var detial in dbPlannedPriceApp.PlannedPriceAppDetails)
            {
                if (detial.RequestedPrice <= 0)
                {
                    throw new ValidationException(ErrorStrings.PlannedPriceApp_Price);
                }
            }
            dbPlannedPriceApp.Status = (int)DcsPlannedPriceAppStatus.已审核;
            var userInfo = Utils.GetCurrentUserInfo();
            dbPlannedPriceApp.ModifierId = userInfo.Id;
            dbPlannedPriceApp.ModifierName = userInfo.Name;
            dbPlannedPriceApp.ModifyTime = DateTime.Now;
            ObjectContext.SaveChanges();
            var checkTime = DateTime.Now.Date.AddDays(1);
            if(dbPlannedPriceApp.PlannedExecutionTime < checkTime) {
                var text = string.Format("begin ExecutePlannedPriceApp2({0}); end;", plannedPriceAppId);
                ObjectContext.ExecuteStoreCommand(text);
            }
        }

        public void 批量审核计划价申请单(int[] plannedPriceAppIds) {
            var dbPlannedPriceApps = ObjectContext.PlannedPriceApps.Where(r => r.Status == (int)DcsPlannedPriceAppStatus.新增 && plannedPriceAppIds.Contains(r.Id)).ToArray();
            if(dbPlannedPriceApps == null|| dbPlannedPriceApps.Count()==0) {
                throw new ValidationException(ErrorStrings.PlannedPriceApp_Validation1);
            }
            foreach(var dbPlannedPriceApp in dbPlannedPriceApps) {
                dbPlannedPriceApp.Status = (int)DcsPlannedPriceAppStatus.已审核;
                var userInfo = Utils.GetCurrentUserInfo();
                dbPlannedPriceApp.ModifierId = userInfo.Id;
                dbPlannedPriceApp.ModifierName = userInfo.Name;
                dbPlannedPriceApp.ModifyTime = DateTime.Now;
            }
            ObjectContext.SaveChanges();
            var checkTime = DateTime.Now.Date.AddDays(1);
            foreach(var dbPlannedPriceApp in dbPlannedPriceApps) {
                if(dbPlannedPriceApp.PlannedExecutionTime < checkTime) {
                    var text = string.Format("begin ExecutePlannedPriceApp({0}); end;", dbPlannedPriceApp.Id);
                    ObjectContext.ExecuteStoreCommand(text);
                }
            }
        }

        public void 同步生成计划价申请单(PlannedPriceApp plannedPriceApp) {
            var salesCenterstrategies = this.ObjectContext.SalesCenterstrategies.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.PartsSalesCategoryId != plannedPriceApp.PartsSalesCategoryId && r.IsAutoSyncPlanPrice != null && (bool)r.IsAutoSyncPlanPrice).ToArray();
            foreach(var salesCenterstrateg in salesCenterstrategies) {
                var partsSalesCategorie = this.ObjectContext.PartsSalesCategories.SingleOrDefault(r => r.Id == salesCenterstrateg.PartsSalesCategoryId && r.Status == (int)DcsBaseDataStatus.有效 && r.BranchId == plannedPriceApp.OwnerCompanyId);
                if(partsSalesCategorie == null)
                    continue;
                var newPlannedPriceApp = new PlannedPriceApp();
                newPlannedPriceApp.OwnerCompanyId = plannedPriceApp.OwnerCompanyId;
                newPlannedPriceApp.OwnerCompanyCode = plannedPriceApp.OwnerCompanyCode;
                newPlannedPriceApp.OwnerCompanyName = plannedPriceApp.OwnerCompanyName;
                newPlannedPriceApp.PartsSalesCategoryId = partsSalesCategorie.Id;
                newPlannedPriceApp.PartsSalesCategoryName = partsSalesCategorie.Name;
                newPlannedPriceApp.Code = GlobalVar.ASSIGNED_BY_SERVER;
                newPlannedPriceApp.Status = plannedPriceApp.Status;
                newPlannedPriceApp.RecordStatus = plannedPriceApp.RecordStatus;
                newPlannedPriceApp.PlannedExecutionTime = plannedPriceApp.PlannedExecutionTime;
                newPlannedPriceApp.Remark = plannedPriceApp.Remark;
                //var ids = plannedPriceApp.PlannedPriceAppDetails.Select(r => r.SparePartId).ToArray();
                var partsPlannedPriceWithStockQuantity = DomainService.获取配件变动前价格_计划价申请单清单(newPlannedPriceApp.PartsSalesCategoryId, plannedPriceApp.Id).ToArray();
                foreach(var plannedPriceAppDetail in plannedPriceApp.PlannedPriceAppDetails) {
                    var partsPlannedPriceWithStock = partsPlannedPriceWithStockQuantity.SingleOrDefault(r => r.SparePartId == plannedPriceAppDetail.SparePartId);
                    var newPlannedPriceAppDetail = new PlannedPriceAppDetail();
                    newPlannedPriceAppDetail.SparePartId = plannedPriceAppDetail.SparePartId;
                    newPlannedPriceAppDetail.SparePartCode = plannedPriceAppDetail.SparePartCode;
                    newPlannedPriceAppDetail.SparePartName = plannedPriceAppDetail.SparePartName;
                    newPlannedPriceAppDetail.RequestedPrice = plannedPriceAppDetail.RequestedPrice;
                    if(partsPlannedPriceWithStock != null) {
                        newPlannedPriceAppDetail.PriceBeforeChange = partsPlannedPriceWithStock.PlannedPrice;
                        newPlannedPriceAppDetail.Quantity = partsPlannedPriceWithStock.TotalQuantity;
                        newPlannedPriceAppDetail.AmountDifference = partsPlannedPriceWithStock.TotalQuantity * (newPlannedPriceAppDetail.RequestedPrice - partsPlannedPriceWithStock.PlannedPrice);
                    }
                    newPlannedPriceApp.PlannedPriceAppDetails.Add(newPlannedPriceAppDetail);
                }
                newPlannedPriceApp.AmountBeforeChange = newPlannedPriceApp.PlannedPriceAppDetails.Sum(r => r.Quantity * r.PriceBeforeChange);
                newPlannedPriceApp.AmountAfterChange = newPlannedPriceApp.PlannedPriceAppDetails.Sum(r => r.Quantity * r.RequestedPrice);
                newPlannedPriceApp.AmountDifference = newPlannedPriceApp.AmountAfterChange - newPlannedPriceApp.AmountBeforeChange;
                InsertToDatabase(newPlannedPriceApp);
                this.InsertPlannedPriceAppValidate(newPlannedPriceApp);
            }
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:11:15
        //原分布类的函数全部转移到Ach                                                               
        
        [Update(UsingCustomMethod = true)]
        public void 作废计划价申请单(PlannedPriceApp plannedPriceApp) {
            new PlannedPriceAppAch(this).作废计划价申请单(plannedPriceApp);
        }
        
        [Invoke]
        public void 审核计划价申请单(int plannedPriceAppId) {
            new PlannedPriceAppAch(this).审核计划价申请单(plannedPriceAppId);
        }
        
        [Invoke]
        public void 批量审核计划价申请单(int[] plannedPriceAppIds) {
            new PlannedPriceAppAch(this).批量审核计划价申请单(plannedPriceAppIds);
        }
        
        [Update(UsingCustomMethod = true)]
        public void 同步生成计划价申请单(PlannedPriceApp plannedPriceApp) {
            new PlannedPriceAppAch(this).同步生成计划价申请单(plannedPriceApp);
        }
    }
}