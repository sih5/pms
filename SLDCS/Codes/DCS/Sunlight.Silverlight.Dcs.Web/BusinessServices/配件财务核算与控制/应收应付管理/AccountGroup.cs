﻿using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class AccountGroupAch : DcsSerivceAchieveBase {
        public AccountGroupAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 生成账户组(AccountGroup accountGroup) {
            var dbAccountGroup = ObjectContext.AccountGroups.Where(r => r.Name.ToLower() == accountGroup.Name.ToLower() && r.Status != (int)DcsMasterDataStatus.作废).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbAccountGroup != null)
                throw new ValidationException(string.Format(ErrorStrings.AccountGroup_Validation1, accountGroup.Name));
            accountGroup.Status = (int)DcsMasterDataStatus.有效;
        }
        public void 修改账户组(AccountGroup accountGroup) {
            CheckEntityState(accountGroup);
            var dbAccountGroup = ObjectContext.AccountGroups.Where(r => r.Name.ToLower() == accountGroup.Name.ToLower() && r.Status == (int)DcsMasterDataStatus.有效 && r.Id != accountGroup.Id).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbAccountGroup != null)
                throw new ValidationException(string.Format(ErrorStrings.AccountGroup_Validation1, accountGroup.Name));
        }
        public void 作废账户组(AccountGroup accountGroup) {
            var dbAccountGroup = ObjectContext.AccountGroups.Where(r => r.Status != (int)DcsMasterDataStatus.有效 && r.Id == accountGroup.Id).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbAccountGroup != null)
                throw new ValidationException(ErrorStrings.AccountGroup_Validation2);
            var userInfo = Utils.GetCurrentUserInfo();
            UpdateToDatabase(accountGroup);
            accountGroup.Status = (int)DcsMasterDataStatus.作废;
            accountGroup.AbandonerId = userInfo.Id;
            accountGroup.AbandonerName = userInfo.Name;
            accountGroup.AbandonTime = System.DateTime.Now;
            this.UpdateAccountGroupValidate(accountGroup);
        }

        public void 停用账户组(AccountGroup accountGroup) {
            var dbAccountGroup = ObjectContext.AccountGroups.Where(r => r.Id == accountGroup.Id && r.Status == (int)DcsMasterDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbAccountGroup == null)
                throw new ValidationException(ErrorStrings.Agency_Validation1);
            UpdateToDatabase(accountGroup);
            accountGroup.Status = (int)DcsMasterDataStatus.停用;
            this.UpdateAccountGroupValidate(accountGroup);
        }

        public void 恢复账户组(AccountGroup accountGroup) {
            var dbAccountGroup = ObjectContext.AccountGroups.Where(r => r.Id == accountGroup.Id && r.Status == (int)DcsMasterDataStatus.停用).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbAccountGroup == null)
                throw new ValidationException(ErrorStrings.Agency_Validation1);
            UpdateToDatabase(accountGroup);
            accountGroup.Status = (int)DcsMasterDataStatus.有效;
            this.UpdateAccountGroupValidate(accountGroup);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:11:15
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 生成账户组(AccountGroup accountGroup) {
            new AccountGroupAch(this).生成账户组(accountGroup);
        }

        public void 修改账户组(AccountGroup accountGroup) {
            new AccountGroupAch(this).修改账户组(accountGroup);
        }

        public void 作废账户组(AccountGroup accountGroup) {
            new AccountGroupAch(this).作废账户组(accountGroup);
        }
        
        [Update(UsingCustomMethod = true)]
        public void 停用账户组(AccountGroup accountGroup) {
            new AccountGroupAch(this).停用账户组(accountGroup);
        }
        
        [Update(UsingCustomMethod = true)]
        public void 恢复账户组(AccountGroup accountGroup) {
            new AccountGroupAch(this).恢复账户组(accountGroup);
        }
    }
}
