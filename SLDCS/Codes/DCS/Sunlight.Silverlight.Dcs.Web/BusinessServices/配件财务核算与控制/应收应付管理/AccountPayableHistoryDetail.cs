﻿using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using Microsoft.Data.Extensions;using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class AccountPayableHistoryDetailAch : DcsSerivceAchieveBase {
        public AccountPayableHistoryDetailAch(DcsDomainService domainService)
            : base(domainService) {
        }

        public void checkInsertAccountPayableHistoryDetail(AccountPayableHistoryDetail accountPayableHistoryDetail) {
            var lastAccountPayableHistoryDetail = ObjectContext.AccountPayableHistoryDetails.Where(r => r.SupplierAccountId == accountPayableHistoryDetail.SupplierAccountId).OrderByDescending(r => r.Id).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(lastAccountPayableHistoryDetail != null) {
                if(lastAccountPayableHistoryDetail.AfterChangeAmount.HasValue && accountPayableHistoryDetail.BeforeChangeAmount != lastAccountPayableHistoryDetail.AfterChangeAmount) {
                    throw new ValidationException(ErrorStrings.AccountPayableHistoryDetail_Validation1);
                }
            }
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:11:15
        //原分布类的函数全部转移到Ach                                                               

        public void checkInsertAccountPayableHistoryDetail(AccountPayableHistoryDetail accountPayableHistoryDetail) {
            new AccountPayableHistoryDetailAch(this).checkInsertAccountPayableHistoryDetail(accountPayableHistoryDetail);
        }
    }
}
