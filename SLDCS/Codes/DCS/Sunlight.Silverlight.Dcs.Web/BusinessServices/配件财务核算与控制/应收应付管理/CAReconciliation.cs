﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;


namespace Sunlight.Silverlight.Dcs.Web {
    partial class CAReconciliationAch : DcsSerivceAchieveBase {
         public void 提交企业往来账对账函(CAReconciliation cAReconciliation) {
            var dbCredenceApplication = ObjectContext.CAReconciliations.Where(r => r.Id == cAReconciliation.Id && r.Status == (int)DcsReconciliationStatus.新增).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbCredenceApplication == null)
                throw new ValidationException(ErrorStrings.CAReconciliation_Validation2);
            cAReconciliation.Status = (int)DcsReconciliationStatus.提交;
            var userInfo = Utils.GetCurrentUserInfo();
            cAReconciliation.SubmitterId = userInfo.Id;
            cAReconciliation.SubmitterName = userInfo.Name;
            cAReconciliation.SubmitTime = DateTime.Now;
        }
        public void 确认企业往来账对账函(CAReconciliation cAReconciliation) {
            var dbCredenceApplication = ObjectContext.CAReconciliations.Where(r => r.Id == cAReconciliation.Id && r.Status == (int)DcsReconciliationStatus.提交).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbCredenceApplication == null)
                throw new ValidationException(ErrorStrings.CAReconciliation_Validation3);
            cAReconciliation.Status = (int)DcsReconciliationStatus.生效;
            var userInfo = Utils.GetCurrentUserInfo();
            cAReconciliation.ConfirmorId = userInfo.Id;
            cAReconciliation.ConfirmorName = userInfo.Name;
            cAReconciliation.ConfirmorTime = DateTime.Now;
        }
        public void 作废企业往来账对账函(CAReconciliation cAReconciliation) {
            var dbCredenceApplication = ObjectContext.CAReconciliations.Where(r => r.Id == cAReconciliation.Id && r.Status == (int)DcsReconciliationStatus.生效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbCredenceApplication == null)
                throw new ValidationException(ErrorStrings.CAReconciliation_Validation4);
            cAReconciliation.Status = (int)DcsReconciliationStatus.作废;
            var userInfo = Utils.GetCurrentUserInfo();
            cAReconciliation.AbandonerId = userInfo.Id;
            cAReconciliation.AbandonerName = userInfo.Name;
            cAReconciliation.AbandonerTime = DateTime.Now;
        }
    }

    partial class DcsDomainService {
        
        public void 提交企业往来账对账函(CAReconciliation cAReconciliation) {
            new CAReconciliationAch(this).提交企业往来账对账函(cAReconciliation);
        }
        public void 确认企业往来账对账函(CAReconciliation cAReconciliation) {
            new CAReconciliationAch(this).确认企业往来账对账函(cAReconciliation);
        }
        public void 作废企业往来账对账函(CAReconciliation cAReconciliation) {
            new CAReconciliationAch(this).作废企业往来账对账函(cAReconciliation);
        }
    }
}
