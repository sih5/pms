﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class CredenceApplicationAch : DcsSerivceAchieveBase {
        public CredenceApplicationAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 修改信用申请单(CredenceApplication credenceApplication) {
            CheckEntityState(credenceApplication);
            var dbCredenceApplication = ObjectContext.CredenceApplications.Where(r => r.Id == credenceApplication.Id && r.Status == (int)DcsCredenceApplicationStatus.新增).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbCredenceApplication == null)
                throw new ValidationException(ErrorStrings.CredenceApplication_Validation1);
        }

        internal void 高级审批通过(CredenceApplication virtualCredenceApplication) {
            var customerAccount = new CustomerAccountAch(this.DomainService).GetCustomerAccountsWithLock(ObjectContext.CustomerAccounts.Where(r => r.AccountGroupId == virtualCredenceApplication.AccountGroupId && r.CustomerCompanyId == virtualCredenceApplication.CustomerCompanyId && r.Status != (int)DcsMasterDataStatus.作废)).ToArray();
            if(!customerAccount.Any() || customerAccount.Count() > 1)
                throw new ValidationException(ErrorStrings.CredenceApplication_Validation4);
            if(null != virtualCredenceApplication.CreditType && virtualCredenceApplication.CreditType == (int)DcsCredit_Type.正常授信) {
                customerAccount.First().CustomerCredenceAmount = virtualCredenceApplication.CredenceLimit;
            } else {
                customerAccount.First().TempCreditTotalFee = virtualCredenceApplication.CredenceLimit;
            }
            UpdateToDatabase(customerAccount.First());
            new CustomerAccountAch(this.DomainService).UpdateCustomerAccountValidate(customerAccount.First());
            var userInfo = Utils.GetCurrentUserInfo();
            this.UpdateCredenceApplicationValidate(virtualCredenceApplication);
            失效其它信用申请单(virtualCredenceApplication);
        }

        internal void 失效其它信用申请单(CredenceApplication credenceApplication) {
            var credenceApplications = ObjectContext.CredenceApplications.Where(r => r.Status == (int)DcsCredenceApplicationStatus.有效 && r.SalesCompanyId == credenceApplication.SalesCompanyId && r.CustomerCompanyId == credenceApplication.CustomerCompanyId && r.AccountGroupId == credenceApplication.AccountGroupId && r.CreditType == credenceApplication.CreditType).ToArray();
            foreach(var c in credenceApplications) {
                c.Status = (int)DcsCredenceApplicationStatus.失效;
                UpdateToDatabase(c);
                this.UpdateCredenceApplicationValidate(c);
            }
        }


        public void 撤销信用申请单(CredenceApplication credenceApplication) {
            var originalCredenceApplication = ChangeSet.GetOriginal(credenceApplication);
            if(originalCredenceApplication.Status != (int)DcsCredenceApplicationStatus.有效 && originalCredenceApplication.Status != (int)DcsCredenceApplicationStatus.审批通过)
                throw new ValidationException(ErrorStrings.CredenceApplication_Validation5);
            UpdateToDatabase(credenceApplication);
            credenceApplication.Status = (int)DcsCredenceApplicationStatus.失效;
            if(credenceApplication.Status == (int)DcsCredenceApplicationStatus.有效) {
                if(credenceApplication.ApplyCondition == (int)DcsCredenceApplicationApplyCondition.自由支配使用) {
                    var customerAccount = ObjectContext.CustomerAccounts.FirstOrDefault(r => r.AccountGroupId == credenceApplication.AccountGroupId && r.CustomerCompanyId == credenceApplication.CustomerCompanyId);
                    if(customerAccount == null)
                        throw new ValidationException(ErrorStrings.CredenceApplication_Validation4);
                    customerAccount.CustomerCredenceAmount -= credenceApplication.CredenceLimit;
                }
            }
            this.UpdateCredenceApplicationValidate(credenceApplication);
        }


        public void 作废信用申请单(CredenceApplication credenceApplication) {
            var dbCredenceApplication = ObjectContext.CredenceApplications.Where(r => r.Id == credenceApplication.Id && r.Status == (int)DcsCredenceApplicationStatus.新增).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbCredenceApplication == null)
                throw new ValidationException(ErrorStrings.CredenceApplication_Validation6);
            UpdateToDatabase(credenceApplication);
            credenceApplication.Status = (int)DcsCredenceApplicationStatus.作废;
            this.UpdateCredenceApplicationValidate(credenceApplication);
        }


        public void 作废虚拟信用申请单据(VirtualCredenceApplication virtualCredenceApplication) {
            var dbCredenceApplication = ObjectContext.CredenceApplications.Where(r => r.Id == virtualCredenceApplication.Id && r.Status == (int)DcsCredenceApplicationStatus.新增).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbCredenceApplication == null)
                throw new ValidationException(ErrorStrings.CredenceApplication_Validation6);
            UpdateToDatabase(dbCredenceApplication);
            dbCredenceApplication.Status = (int)DcsCredenceApplicationStatus.作废;
            this.UpdateCredenceApplicationValidate(dbCredenceApplication);
        }
        public void 提交虚拟信用申请单据(VirtualCredenceApplication virtualCredenceApplication) {
            var dbCredenceApplication = ObjectContext.CredenceApplications.Where(r => r.Id == virtualCredenceApplication.Id && r.Status == (int)DcsCredenceApplicationStatus.新增).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbCredenceApplication == null)
                throw new ValidationException(ErrorStrings.CredenceApplication_Validation8);
            UpdateToDatabase(dbCredenceApplication);
            dbCredenceApplication.Status = (int)DcsCredenceApplicationStatus.已提交;
            this.UpdateCredenceApplicationValidate(dbCredenceApplication);
        }
        public void 驳回信用申请单(CredenceApplication credenceApplication) {
            var dbCredenceApplication = ObjectContext.CredenceApplications.Where(r => r.Id == credenceApplication.Id && (r.Status == (int)DcsCredenceApplicationStatus.新增||r.Status == (int)DcsCredenceApplicationStatus.已提交 || r.Status == (int)DcsCredenceApplicationStatus.初审通过 || r.Status == (int)DcsCredenceApplicationStatus.审核通过 || r.Status == (int)DcsCredenceApplicationStatus.审批通过 || r.Status == (int)DcsCredenceApplicationStatus.高级审核通过)).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbCredenceApplication == null)
                throw new ValidationException(ErrorStrings.CredenceApplication_Reject_Status);
            UpdateToDatabase(credenceApplication);
            var userInfo = Utils.GetCurrentUserInfo();
                credenceApplication.InitialApproverId = null;
                credenceApplication.InitialApproverName = null;
                credenceApplication.InitialApproveTime = null;
                credenceApplication.CheckerId = null;
                credenceApplication.CheckerName = null;
                credenceApplication.CheckTime = null;
                credenceApplication.ApproverId = null;
                credenceApplication.ApproverName = null;
                credenceApplication.ApproveTime = null;
                credenceApplication.UpperCheckerId = null;
                credenceApplication.UpperCheckerName = null;
                credenceApplication.UpperCheckTime = null;
                credenceApplication.UpperApproverId = null;
                credenceApplication.UpperApproverName = null;
                credenceApplication.UpperApproveTime = null;
                credenceApplication.Status = (int)DcsCredenceApplicationStatus.新增;
                this.UpdateCredenceApplicationValidate(credenceApplication);
        }
        public void 初审信用申请单(CredenceApplication credenceApplication) {
            var dbCredenceApplication = ObjectContext.CredenceApplications.Where(r => r.Id == credenceApplication.Id && r.Status == (int)DcsCredenceApplicationStatus.已提交 && r.CreditType == (int)DcsCredit_Type.临时授信).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbCredenceApplication == null)
                throw new ValidationException(ErrorStrings.CredenceApplication_Validation10);
            UpdateToDatabase(credenceApplication);
            var userInfo = Utils.GetCurrentUserInfo();
            var amount = credenceApplication.CredenceLimit;
            var inventoryConfig = ObjectContext.MultiLevelApproveConfigs.FirstOrDefault(r => amount >= r.MinApproveFee && amount < r.MaxApproveFee
                   && r.Type == (int)DCSMultiLevelApproveConfigType.信用申请单 && r.ApproverId == userInfo.Id && r.IsFinished && r.Status == (int)DcsBaseDataStatus.有效);
            if(inventoryConfig != null) {
                credenceApplication.Status = (int)DcsCredenceApplicationStatus.有效;
                高级审批通过(credenceApplication);
            } else {
                credenceApplication.Status = (int)DcsCredenceApplicationStatus.初审通过;
            }

            credenceApplication.InitialApproverId = userInfo.Id;
            credenceApplication.InitialApproverName = userInfo.Name;
            credenceApplication.InitialApproveTime = DateTime.Now;
            this.UpdateCredenceApplicationValidate(credenceApplication);
        }
        public void 审核信用申请单(CredenceApplication credenceApplication) {
            var dbCredenceApplication = ObjectContext.CredenceApplications.Where(r => r.Id == credenceApplication.Id && ((r.Status == (int)DcsCredenceApplicationStatus.初审通过 && r.CreditType == (int)DcsCredit_Type.临时授信) || (r.Status == (int)DcsCredenceApplicationStatus.已提交 && r.CreditType == (int)DcsCredit_Type.正常授信))).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbCredenceApplication == null)
                throw new ValidationException(ErrorStrings.CredenceApplication_Reject_Approve);
            UpdateToDatabase(credenceApplication);
            var userInfo = Utils.GetCurrentUserInfo();
            var amount = credenceApplication.CredenceLimit;
            var inventoryConfig = ObjectContext.MultiLevelApproveConfigs.FirstOrDefault(r => amount >= r.MinApproveFee && amount < r.MaxApproveFee
                    && r.Type == (int)DCSMultiLevelApproveConfigType.信用申请单 && r.ApproverId == userInfo.Id && r.IsFinished && r.Status == (int)DcsBaseDataStatus.有效);
            if(inventoryConfig != null && credenceApplication.CreditType == (int)DcsCredit_Type.临时授信) {
                credenceApplication.Status = (int)DcsCredenceApplicationStatus.有效;
                高级审批通过(credenceApplication);
            } else {
                credenceApplication.Status = (int)DcsCredenceApplicationStatus.审核通过;
            }
            credenceApplication.CheckerId = userInfo.Id;
            credenceApplication.CheckerName = userInfo.Name;
            credenceApplication.CheckTime = DateTime.Now;
            this.UpdateCredenceApplicationValidate(credenceApplication);
        }
        public void 审批信用申请单(CredenceApplication credenceApplication) {
            var userInfo = Utils.GetCurrentUserInfo();
            var company = ObjectContext.Companies.Single(r => r.Id == userInfo.EnterpriseId);
            var dbCredenceApplication = ObjectContext.CredenceApplications.Where(r => r.Id == credenceApplication.Id && (r.Status == (int)DcsCredenceApplicationStatus.审核通过 || r.Status == (int)DcsCredenceApplicationStatus.新增)).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if((company.Type == (int)DcsCompanyType.代理库 || company.Type == (int)DcsCompanyType.服务站兼代理库) && dbCredenceApplication == null)
                throw new ValidationException(ErrorStrings.CredenceApplication_Validation2);
            if(company.Type == (int)DcsCompanyType.分公司 && dbCredenceApplication == null)
                throw new ValidationException(ErrorStrings.CredenceApplication_Approve_Auti);
            if((company.Type == (int)DcsCompanyType.代理库 || company.Type == (int)DcsCompanyType.服务站兼代理库) || credenceApplication.CreditType == (int)DcsCredit_Type.正常授信) {
                高级审批通过(credenceApplication);
                credenceApplication.Status = (int)DcsCredenceApplicationStatus.有效;
            } else {
                var amount = credenceApplication.CredenceLimit;
                var inventoryConfig = ObjectContext.MultiLevelApproveConfigs.FirstOrDefault(r => amount >= r.MinApproveFee && amount < r.MaxApproveFee
                     && r.Type == (int)DCSMultiLevelApproveConfigType.信用申请单 && r.ApproverId == userInfo.Id && r.IsFinished && r.Status == (int)DcsBaseDataStatus.有效);
                if(inventoryConfig != null) {
                    credenceApplication.Status = (int)DcsCredenceApplicationStatus.有效;
                    高级审批通过(credenceApplication);
                } else {
                    credenceApplication.Status = (int)DcsCredenceApplicationStatus.审批通过;
                }
            }
            credenceApplication.ApproverId = userInfo.Id;
            credenceApplication.ApproverName = userInfo.Name;
            credenceApplication.ApproveTime = DateTime.Now;
            UpdateToDatabase(credenceApplication);
            this.UpdateCredenceApplicationValidate(credenceApplication);
        }
        public void 高级审核信用申请单(CredenceApplication credenceApplication) {
            var dbCredenceApplication = ObjectContext.CredenceApplications.Where(r => r.Id == credenceApplication.Id && (r.Status == (int)DcsCredenceApplicationStatus.审批通过)).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbCredenceApplication == null)
                throw new ValidationException(ErrorStrings.CredenceApplication_Approve_Autiew);
            UpdateToDatabase(credenceApplication);
            var userInfo = Utils.GetCurrentUserInfo();
            var amount = credenceApplication.CredenceLimit;
            var inventoryConfig = ObjectContext.MultiLevelApproveConfigs.FirstOrDefault(r => amount >= r.MinApproveFee && amount < r.MaxApproveFee
                    && r.Type == (int)DCSMultiLevelApproveConfigType.信用申请单 && r.ApproverId == userInfo.Id && r.IsFinished && r.Status == (int)DcsBaseDataStatus.有效);
            if(inventoryConfig != null) {
                credenceApplication.Status = (int)DcsCredenceApplicationStatus.有效;
                高级审批通过(credenceApplication);
            } else {
                credenceApplication.Status = (int)DcsCredenceApplicationStatus.高级审核通过;
            }

            credenceApplication.UpperCheckerId = userInfo.Id;
            credenceApplication.UpperCheckerName = userInfo.Name;
            credenceApplication.UpperCheckTime = DateTime.Now;
            this.UpdateCredenceApplicationValidate(credenceApplication);
        }
        public void 高级审批信用申请单(CredenceApplication credenceApplication) {
            var dbCredenceApplication = ObjectContext.CredenceApplications.Where(r => r.Id == credenceApplication.Id && (r.Status == (int)DcsCredenceApplicationStatus.高级审核通过)).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbCredenceApplication == null)
                throw new ValidationException(ErrorStrings.CredenceApplication_Approve_HighApprove);
            UpdateToDatabase(credenceApplication);
            高级审批通过(credenceApplication);
            var userInfo = Utils.GetCurrentUserInfo();
            credenceApplication.Status = (int)DcsCredenceApplicationStatus.有效;
            credenceApplication.UpperApproverId = userInfo.Id;
            credenceApplication.UpperApproverName = userInfo.Name;
            credenceApplication.UpperApproveTime = DateTime.Now;
            this.UpdateCredenceApplicationValidate(credenceApplication);
        }
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:11:15
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 修改信用申请单(CredenceApplication credenceApplication) {
            new CredenceApplicationAch(this).修改信用申请单(credenceApplication);
        }


        [Update(UsingCustomMethod = true)]
        public void 撤销信用申请单(CredenceApplication credenceApplication) {
            new CredenceApplicationAch(this).撤销信用申请单(credenceApplication);
        }


        [Update(UsingCustomMethod = true)]
        public void 作废信用申请单(CredenceApplication credenceApplication) {
            new CredenceApplicationAch(this).作废信用申请单(credenceApplication);
        }

        [Update(UsingCustomMethod = true)]
        public void 作废虚拟信用申请单据(VirtualCredenceApplication virtualCredenceApplication) {
            new CredenceApplicationAch(this).作废虚拟信用申请单据(virtualCredenceApplication);
        }
        [Update(UsingCustomMethod = true)]
        public void 驳回信用申请单(CredenceApplication credenceApplication) {
            new CredenceApplicationAch(this).驳回信用申请单(credenceApplication);
        }
        [Update(UsingCustomMethod = true)]
        public void 初审信用申请单(CredenceApplication credenceApplication) {
            new CredenceApplicationAch(this).初审信用申请单(credenceApplication);
        }
        [Update(UsingCustomMethod = true)]
        public void 提交虚拟信用申请单据(VirtualCredenceApplication virtualCredenceApplication) {
            new CredenceApplicationAch(this).提交虚拟信用申请单据(virtualCredenceApplication);
        }

        [Update(UsingCustomMethod = true)]
        public void 审核信用申请单(CredenceApplication credenceApplication) {
            new CredenceApplicationAch(this).审核信用申请单(credenceApplication);
        }
        [Update(UsingCustomMethod = true)]
        public void 审批信用申请单(CredenceApplication credenceApplication) {
            new CredenceApplicationAch(this).审批信用申请单(credenceApplication);
        }
        [Update(UsingCustomMethod = true)]
        public void 高级审核信用申请单(CredenceApplication credenceApplication) {
            new CredenceApplicationAch(this).高级审核信用申请单(credenceApplication);
        }
        [Update(UsingCustomMethod = true)]
        public void 高级审批信用申请单(CredenceApplication credenceApplication) {
            new CredenceApplicationAch(this).高级审批信用申请单(credenceApplication);
        }
    }
}
