﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class CustomerAccountAch : DcsSerivceAchieveBase {
        public CustomerAccountAch(DcsDomainService domainService)
            : base(domainService) {
        }


        internal void 订单审批过账(int customerAccountId, decimal amount) {
            var dbCustomerAccount = GetCustomerAccountsWithLock(ObjectContext.CustomerAccounts.Where(r => r.Id == customerAccountId && r.Status != (int)DcsMasterDataStatus.作废)).ToArray();
            if(!dbCustomerAccount.Any() || dbCustomerAccount.Count() > 1)
                throw new ValidationException(ErrorStrings.CustomerAccount_Validation3);
            dbCustomerAccount.First().PendingAmount += amount;
            //UpdateToDatabase(dbCustomerAccount.First());
            this.UpdateCustomerAccountValidate(dbCustomerAccount.First());
            this.ObjectContext.SaveChanges();
        }

        internal void 订单审批过账记录流水(PartsSalesOrder partsSalesOrder, decimal amount) {
            var dbCustomerAccount = GetCustomerAccountsWithLock(ObjectContext.CustomerAccounts.Where(r => r.Id == partsSalesOrder.CustomerAccountId && r.Status != (int)DcsMasterDataStatus.作废)).ToArray();
            if(!dbCustomerAccount.Any() || dbCustomerAccount.Count() > 1)
                throw new ValidationException(ErrorStrings.CustomerAccount_Validation3);

            var userInfo = Utils.GetCurrentUserInfo();
            var customerAccountHisDetail = new CustomerAccountHisDetail();
            customerAccountHisDetail.CustomerAccountId = dbCustomerAccount.First().Id;
            customerAccountHisDetail.ChangeAmount = 0 + amount;
            customerAccountHisDetail.ProcessDate = DateTime.Now;
            customerAccountHisDetail.CreatorName = userInfo.Name;
            customerAccountHisDetail.BusinessType = (int)DcsAccountPaymentBusinessType.销售出库计划;
            customerAccountHisDetail.BeforeChangeAmount = dbCustomerAccount.First().PendingAmount;
            customerAccountHisDetail.Debit = amount;
            customerAccountHisDetail.InvoiceDate = DateTime.Now;
            customerAccountHisDetail.SourceId = partsSalesOrder.Id;
            customerAccountHisDetail.SourceCode = partsSalesOrder.Code;
            customerAccountHisDetail.AfterChangeAmount = dbCustomerAccount.First().PendingAmount + amount;
            customerAccountHisDetail.Summary = "销售直供订单审核锁定";
            customerAccountHisDetail.SerialType = (int)DcsSerialType.审批待发;
            DomainService.checkInsertCustomerAccountHisDetail(customerAccountHisDetail);
            InsertToDatabase(customerAccountHisDetail);

            dbCustomerAccount.First().PendingAmount += amount;
            //UpdateToDatabase(dbCustomerAccount.First());
            this.UpdateCustomerAccountValidate(dbCustomerAccount.First());
            this.ObjectContext.SaveChanges();
        }

        internal void 销售出库过账(int customerAccountId, decimal amount) {
            var dbCustomerAccount = GetCustomerAccountsWithLock(ObjectContext.CustomerAccounts.Where(r => r.Id == customerAccountId && r.Status != (int)DcsMasterDataStatus.作废)).ToArray();
            if(!dbCustomerAccount.Any() || dbCustomerAccount.Count() > 1)
                throw new ValidationException(ErrorStrings.CustomerAccount_Validation3);
            dbCustomerAccount.First().PendingAmount -= amount;
            dbCustomerAccount.First().ShippedProductValue += amount;
            //UpdateToDatabase(dbCustomerAccount.First());
            this.UpdateCustomerAccountValidate(dbCustomerAccount.First());
            this.ObjectContext.SaveChanges();
        }

        internal void 销售出库过账记录流水(PartsOutboundPlan partsOutboundPlan, decimal amount,DateTime now) {
            var dbCustomerAccount = GetCustomerAccountsWithLock(ObjectContext.CustomerAccounts.Where(r => r.Id == partsOutboundPlan.CustomerAccountId && r.Status != (int)DcsMasterDataStatus.作废)).ToArray();
            if(!dbCustomerAccount.Any() || dbCustomerAccount.Count() > 1)
                throw new ValidationException(ErrorStrings.CustomerAccount_Validation3);

            var userInfo = Utils.GetCurrentUserInfo();
            //审批待发流水
            var customerAccountHisDetail = new CustomerAccountHisDetail();
            customerAccountHisDetail.CustomerAccountId = dbCustomerAccount.First().Id;
            customerAccountHisDetail.ChangeAmount = 0 - amount;
            customerAccountHisDetail.ProcessDate = now;
            customerAccountHisDetail.CreatorName = userInfo.Name;
            customerAccountHisDetail.BusinessType = (int)DcsAccountPaymentBusinessType.销售出库;
            customerAccountHisDetail.BeforeChangeAmount = dbCustomerAccount.First().PendingAmount;
            customerAccountHisDetail.Debit = amount;
            customerAccountHisDetail.InvoiceDate = DateTime.Now;
            customerAccountHisDetail.SourceId = partsOutboundPlan.Id;
            customerAccountHisDetail.SourceCode = partsOutboundPlan.Code;
            customerAccountHisDetail.AfterChangeAmount = dbCustomerAccount.First().PendingAmount - amount;
            customerAccountHisDetail.Summary = "发运确认解锁审批待发金额";
            customerAccountHisDetail.SerialType = (int)DcsSerialType.审批待发;
            DomainService.checkInsertCustomerAccountHisDetail(customerAccountHisDetail);
            InsertToDatabase(customerAccountHisDetail);
            //发出商品流水
            var customerAccountHisDetailShippe = new CustomerAccountHisDetail();
            customerAccountHisDetailShippe.CustomerAccountId = dbCustomerAccount.First().Id;
            customerAccountHisDetailShippe.ChangeAmount = 0 + amount;
            customerAccountHisDetailShippe.ProcessDate = now;
            customerAccountHisDetailShippe.CreatorName = userInfo.Name;
            customerAccountHisDetailShippe.BusinessType = (int)DcsAccountPaymentBusinessType.销售出库;
            customerAccountHisDetailShippe.BeforeChangeAmount = dbCustomerAccount.First().ShippedProductValue;
            customerAccountHisDetailShippe.Debit = amount;
            customerAccountHisDetailShippe.InvoiceDate = DateTime.Now;
            customerAccountHisDetailShippe.SourceId = partsOutboundPlan.Id;
            customerAccountHisDetailShippe.SourceCode = partsOutboundPlan.Code;
            customerAccountHisDetailShippe.AfterChangeAmount = dbCustomerAccount.First().ShippedProductValue + amount;
            customerAccountHisDetailShippe.Summary = "发运确认增加发出商品金额";
            customerAccountHisDetailShippe.SerialType = (int)DcsSerialType.发出商品;
            DomainService.checkInsertCustomerAccountHisDetail(customerAccountHisDetailShippe);
            InsertToDatabase(customerAccountHisDetailShippe);

            dbCustomerAccount.First().PendingAmount -= amount;
            dbCustomerAccount.First().ShippedProductValue += amount;
            //UpdateToDatabase(dbCustomerAccount.First());
            this.UpdateCustomerAccountValidate(dbCustomerAccount.First());
            this.ObjectContext.SaveChanges();
        }

        internal void 审核直供销售订单锁定客户账户(int customerAccountId, decimal amount) {
            var dbCustomerAccount = GetCustomerAccountsWithLock(ObjectContext.CustomerAccounts.Where(r => r.Id == customerAccountId && r.Status != (int)DcsMasterDataStatus.作废)).ToArray();
            if(!dbCustomerAccount.Any() || dbCustomerAccount.Count() > 1)
                throw new ValidationException(ErrorStrings.CustomerAccount_Validation3);
            dbCustomerAccount.First().PendingAmount += amount;
            UpdateToDatabase(dbCustomerAccount.First());
            this.UpdateCustomerAccountValidate(dbCustomerAccount.First());
        }

        internal void 终止直供销售订单解锁客户账户(int customerAccountId, decimal amount) {
            var dbCustomerAccount = GetCustomerAccountsWithLock(ObjectContext.CustomerAccounts.Where(r => r.Id == customerAccountId && r.Status != (int)DcsMasterDataStatus.作废)).ToArray();
            if(!dbCustomerAccount.Any() || dbCustomerAccount.Count() > 1)
                throw new ValidationException(ErrorStrings.CustomerAccount_Validation3);
            dbCustomerAccount.First().PendingAmount -= amount;
            //UpdateToDatabase(dbCustomerAccount.First());
            this.UpdateCustomerAccountValidate(dbCustomerAccount.First());
            this.ObjectContext.SaveChanges();
        }

        internal void 终止直供销售订单解锁客户账户记录流水(PartsSalesOrder partsSalesOrder, decimal amount) {
            var dbCustomerAccount = GetCustomerAccountsWithLock(ObjectContext.CustomerAccounts.Where(r => r.Id == partsSalesOrder.CustomerAccountId && r.Status != (int)DcsMasterDataStatus.作废)).ToArray();
            if(!dbCustomerAccount.Any() || dbCustomerAccount.Count() > 1)
                throw new ValidationException(ErrorStrings.CustomerAccount_Validation3);


            var userInfo = Utils.GetCurrentUserInfo();
            var customerAccountHisDetail = new CustomerAccountHisDetail();
            customerAccountHisDetail.CustomerAccountId = dbCustomerAccount.First().Id;
            customerAccountHisDetail.ChangeAmount = 0 - amount;
            customerAccountHisDetail.ProcessDate = DateTime.Now;
            customerAccountHisDetail.CreatorName = userInfo.Name;
            customerAccountHisDetail.BusinessType = (int)DcsAccountPaymentBusinessType.直供订单预扣款;
            customerAccountHisDetail.BeforeChangeAmount = dbCustomerAccount.First().PendingAmount;
            customerAccountHisDetail.Debit = amount;
            customerAccountHisDetail.InvoiceDate = DateTime.Now;
            customerAccountHisDetail.SourceId = partsSalesOrder.Id;
            customerAccountHisDetail.SourceCode = partsSalesOrder.Code;
            customerAccountHisDetail.AfterChangeAmount = dbCustomerAccount.First().PendingAmount - amount;
            customerAccountHisDetail.Summary = "取消采购订单解锁审批待发金额";
            customerAccountHisDetail.SerialType = (int)DcsSerialType.审批待发;
            DomainService.checkInsertCustomerAccountHisDetail(customerAccountHisDetail);
            InsertToDatabase(customerAccountHisDetail);

            dbCustomerAccount.First().PendingAmount -= amount;
            //UpdateToDatabase(dbCustomerAccount.First());
            this.UpdateCustomerAccountValidate(dbCustomerAccount.First());
            this.ObjectContext.SaveChanges();
        }

        internal void 出库计划取消(int customerAccountId, decimal amount) {
            var dbCustomerAccount = GetCustomerAccountsWithLock(ObjectContext.CustomerAccounts.Where(r => r.Id == customerAccountId && r.Status != (int)DcsMasterDataStatus.作废)).ToArray();
            if(!dbCustomerAccount.Any() || dbCustomerAccount.Count() > 1)
                throw new ValidationException(ErrorStrings.CustomerAccount_Validation3);
            dbCustomerAccount.First().PendingAmount -= amount;
            //UpdateToDatabase(dbCustomerAccount.First());
            this.UpdateCustomerAccountValidate(dbCustomerAccount.First());
            this.ObjectContext.SaveChanges();
        }

        internal void 出库计划取消记录流水(PartsOutboundPlan partsOutboundPlan, decimal amount) {
            var dbCustomerAccount = GetCustomerAccountsWithLock(ObjectContext.CustomerAccounts.Where(r => r.Id == partsOutboundPlan.CustomerAccountId && r.Status != (int)DcsMasterDataStatus.作废)).ToArray();
            if(!dbCustomerAccount.Any() || dbCustomerAccount.Count() > 1)
                throw new ValidationException(ErrorStrings.CustomerAccount_Validation3);

            var userInfo = Utils.GetCurrentUserInfo();
            var customerAccountHisDetail = new CustomerAccountHisDetail();
            customerAccountHisDetail.CustomerAccountId = dbCustomerAccount.First().Id;
            customerAccountHisDetail.ChangeAmount = 0 - amount;
            customerAccountHisDetail.ProcessDate = DateTime.Now;
            customerAccountHisDetail.CreatorName = userInfo.Name;
            customerAccountHisDetail.BusinessType = (int)DcsAccountPaymentBusinessType.终止销售出库计划;
            customerAccountHisDetail.BeforeChangeAmount = dbCustomerAccount.First().PendingAmount;
            customerAccountHisDetail.Debit = amount;
            customerAccountHisDetail.InvoiceDate = DateTime.Now;
            customerAccountHisDetail.SourceId = partsOutboundPlan.Id;
            customerAccountHisDetail.SourceCode = partsOutboundPlan.Code;
            customerAccountHisDetail.AfterChangeAmount = dbCustomerAccount.First().PendingAmount - amount;
            customerAccountHisDetail.Summary = "终止出库计划解锁";
            customerAccountHisDetail.SerialType = (int)DcsSerialType.审批待发;
            DomainService.checkInsertCustomerAccountHisDetail(customerAccountHisDetail);
            InsertToDatabase(customerAccountHisDetail);

            dbCustomerAccount.First().PendingAmount -= amount;
            //UpdateToDatabase(dbCustomerAccount.First());
            this.UpdateCustomerAccountValidate(dbCustomerAccount.First());
            this.ObjectContext.SaveChanges();
        }
        //转中心库销售出库终止
        internal void 转中心库销售出库终止记录流水(PartsSalesOrder partsSalesOrder, decimal amount) {
            var dbCustomerAccount = GetCustomerAccountsWithLock(ObjectContext.CustomerAccounts.Where(r => r.Id == partsSalesOrder.CustomerAccountId && r.Status != (int)DcsMasterDataStatus.作废)).ToArray();
            if(!dbCustomerAccount.Any() || dbCustomerAccount.Count() > 1)
                throw new ValidationException(ErrorStrings.CustomerAccount_Validation3);

            var userInfo = Utils.GetCurrentUserInfo();
            var customerAccountHisDetail = new CustomerAccountHisDetail();
            customerAccountHisDetail.CustomerAccountId = dbCustomerAccount.First().Id;
            customerAccountHisDetail.ChangeAmount = 0 - amount;
            customerAccountHisDetail.ProcessDate = DateTime.Now;
            customerAccountHisDetail.CreatorName = userInfo.Name;
            customerAccountHisDetail.BusinessType = (int)DcsAccountPaymentBusinessType.终止销售出库计划;
            customerAccountHisDetail.BeforeChangeAmount = dbCustomerAccount.First().PendingAmount;
            customerAccountHisDetail.Debit = amount;
            customerAccountHisDetail.InvoiceDate = DateTime.Now;
            customerAccountHisDetail.SourceId = partsSalesOrder.Id;
            customerAccountHisDetail.SourceCode = partsSalesOrder.Code;
            customerAccountHisDetail.AfterChangeAmount = dbCustomerAccount.First().PendingAmount - amount;
            customerAccountHisDetail.Summary = "转中心库终止出库计划解锁";
            customerAccountHisDetail.SerialType = (int)DcsSerialType.审批待发;
            DomainService.checkInsertCustomerAccountHisDetail(customerAccountHisDetail);
            InsertToDatabase(customerAccountHisDetail);

            dbCustomerAccount.First().PendingAmount -= amount;
            //UpdateToDatabase(dbCustomerAccount.First());
            this.UpdateCustomerAccountValidate(dbCustomerAccount.First());
            this.ObjectContext.SaveChanges();
        }
        public void 销售结算过账(PartsSalesSettlement partsSalesSettlement) {
            var dbCustomerAccount = GetCustomerAccountsWithLock(ObjectContext.CustomerAccounts.Where(r => r.Id == partsSalesSettlement.CustomerAccountId && r.Status != (int)DcsMasterDataStatus.作废)).ToArray();
            if(!dbCustomerAccount.Any() || dbCustomerAccount.Count() > 1)
                throw new ValidationException(ErrorStrings.CustomerAccount_Validation3);

            var prtsSalesSettlementRefs = ObjectContext.PartsSalesSettlementRefs.Where(r => r.PartsSalesSettlementId == partsSalesSettlement.Id && r.SourceType == (int)DcsPartsSalesSettlementRefSourceType.配件出库单).ToArray();
            if(partsSalesSettlement.SettlementPath == (int)DcsPartsPurchaseSettleBillSettlementPath.正常结算) {
                dbCustomerAccount.First().AccountBalance -= partsSalesSettlement.TotalSettlementAmount;

                if(prtsSalesSettlementRefs.Any())
                    dbCustomerAccount.First().ShippedProductValue -= prtsSalesSettlementRefs.Sum(r => r.SettlementAmount);
            }
            if(partsSalesSettlement.SettlementPath == (int)DcsPartsPurchaseSettleBillSettlementPath.反冲结算) {
                //传值过来是负所以为减 负负得正
                dbCustomerAccount.First().AccountBalance -= partsSalesSettlement.TotalSettlementAmount;
                prtsSalesSettlementRefs = ObjectContext.PartsSalesSettlementRefs.Where(r => r.PartsSalesSettlementId == partsSalesSettlement.OffsettedSettlementBillId && r.SourceType == (int)DcsPartsSalesSettlementRefSourceType.配件出库单).ToArray();
                if(prtsSalesSettlementRefs.Any())
                    dbCustomerAccount.First().ShippedProductValue += prtsSalesSettlementRefs.Sum(r => r.SettlementAmount);
            }
            UpdateToDatabase(dbCustomerAccount.First());
            this.UpdateCustomerAccountValidate(dbCustomerAccount.First());
            var userInfo = Utils.GetCurrentUserInfo();
            var customerAccountHisDetail = new CustomerAccountHisDetail();
            customerAccountHisDetail.CustomerAccountId = dbCustomerAccount.First().Id;
            customerAccountHisDetail.ChangeAmount = 0 - partsSalesSettlement.TotalSettlementAmount;
            customerAccountHisDetail.ProcessDate = DateTime.Now;
            customerAccountHisDetail.CreatorName = userInfo.Name;
            if(partsSalesSettlement.SettlementPath == (int)DcsPartsPurchaseSettleBillSettlementPath.正常结算) {
                customerAccountHisDetail.BusinessType = (int)DcsAccountPaymentBusinessType.销售结算;
                customerAccountHisDetail.BeforeChangeAmount = dbCustomerAccount.First().AccountBalance + partsSalesSettlement.TotalSettlementAmount;
                customerAccountHisDetail.Debit = partsSalesSettlement.TotalSettlementAmount;
            }
            if(partsSalesSettlement.SettlementPath == (int)DcsPartsPurchaseSettleBillSettlementPath.反冲结算) {
                customerAccountHisDetail.BusinessType = (int)DcsAccountPaymentBusinessType.销售反结算;
                customerAccountHisDetail.BeforeChangeAmount = dbCustomerAccount.First().AccountBalance + partsSalesSettlement.TotalSettlementAmount;
                customerAccountHisDetail.Debit = partsSalesSettlement.TotalSettlementAmount;//贷方
            }
            customerAccountHisDetail.InvoiceDate = partsSalesSettlement.InvoiceDate;
            customerAccountHisDetail.SourceId = partsSalesSettlement.Id;
            customerAccountHisDetail.SourceCode = partsSalesSettlement.Code;
            customerAccountHisDetail.AfterChangeAmount = dbCustomerAccount.First().AccountBalance;
            customerAccountHisDetail.Summary = partsSalesSettlement.RebateMethod.HasValue ? Enum.GetName(typeof(DcsPartsSalesSettlementRebateMethod), partsSalesSettlement.RebateMethod.Value) : "" + (partsSalesSettlement.RebateAmount.HasValue ? partsSalesSettlement.RebateAmount.Value : default(decimal?));
            customerAccountHisDetail.SerialType = (int)DcsSerialType.账户金额;
            DomainService.checkInsertCustomerAccountHisDetail(customerAccountHisDetail);
            InsertToDatabase(customerAccountHisDetail);

            //发出商品流水
            var customerAccountHisDetailShippe = new CustomerAccountHisDetail();
            customerAccountHisDetailShippe.CustomerAccountId = dbCustomerAccount.First().Id;
            customerAccountHisDetailShippe.ProcessDate = DateTime.Now;
            customerAccountHisDetailShippe.CreatorName = userInfo.Name;

            customerAccountHisDetailShippe.Debit = partsSalesSettlement.TotalSettlementAmount;
            customerAccountHisDetailShippe.InvoiceDate = partsSalesSettlement.InvoiceDate;
            customerAccountHisDetailShippe.SourceId = partsSalesSettlement.Id;
            customerAccountHisDetailShippe.SourceCode = partsSalesSettlement.Code;
            if(partsSalesSettlement.SettlementPath == (int)DcsPartsPurchaseSettleBillSettlementPath.正常结算) {
                customerAccountHisDetail.BusinessType = (int)DcsAccountPaymentBusinessType.销售结算;
                customerAccountHisDetailShippe.ChangeAmount = 0 - partsSalesSettlement.TotalSettlementAmount;

                customerAccountHisDetailShippe.BeforeChangeAmount = dbCustomerAccount.First().ShippedProductValue + prtsSalesSettlementRefs.Sum(r => r.SettlementAmount);
                customerAccountHisDetailShippe.Summary = "销售结算扣减发出商品金额";
            }
            if(partsSalesSettlement.SettlementPath == (int)DcsPartsPurchaseSettleBillSettlementPath.反冲结算) {
                customerAccountHisDetail.BusinessType = (int)DcsAccountPaymentBusinessType.销售反结算;
                customerAccountHisDetailShippe.ChangeAmount = 0 + partsSalesSettlement.TotalSettlementAmount;
                customerAccountHisDetailShippe.BeforeChangeAmount = dbCustomerAccount.First().ShippedProductValue - prtsSalesSettlementRefs.Sum(r => r.SettlementAmount);
                customerAccountHisDetailShippe.Summary = "反结算增加发出商品金额";
            }
            customerAccountHisDetailShippe.AfterChangeAmount = dbCustomerAccount.First().ShippedProductValue;
            customerAccountHisDetailShippe.SerialType = (int)DcsSerialType.发出商品;
            DomainService.checkInsertCustomerAccountHisDetail(customerAccountHisDetailShippe);
            InsertToDatabase(customerAccountHisDetailShippe);
        }

        // 反结算专用
        // 根据销售退货结算单发票金额增加客户流水账，若无发票，则不过账
        public void 销售退货结算过账(PartsSalesRtnSettlement partsSalesRtnSettlement) {
            var dbCustomerAccount = GetCustomerAccountsWithLock(ObjectContext.CustomerAccounts.Where(r => r.Id == partsSalesRtnSettlement.CustomerAccountId && r.Status != (int)DcsMasterDataStatus.作废)).ToArray();
            if(!dbCustomerAccount.Any() || dbCustomerAccount.Count() > 1)
                throw new ValidationException(ErrorStrings.CustomerAccount_Validation3);
            var salesRtnSettleInvoiceRels = ObjectContext.SalesRtnSettleInvoiceRels.Where(r => r.PartsSalesRtnSettlementId == partsSalesRtnSettlement.Id);
            var invoiceIds = salesRtnSettleInvoiceRels.Select(r => r.InvoiceId);
            var userInfo = Utils.GetCurrentUserInfo();
            var invoiceInformations = ObjectContext.InvoiceInformations.Where(r => invoiceIds.Contains(r.Id)).ToArray();
            foreach(var inv in invoiceInformations) {
                dbCustomerAccount.First().AccountBalance -= Decimal.Parse(inv.InvoiceAmount.ToString());
                var customerAccountHisDetail = new CustomerAccountHisDetail {
                    CustomerAccountId = dbCustomerAccount.First().Id,
                    ChangeAmount = inv.InvoiceAmount,
                    ProcessDate = DateTime.Now,
                    BusinessType = (int)DcsAccountPaymentBusinessType.销售退货入库,
                    SourceId = inv.Id,
                    SourceCode = inv.Code,
                    AfterChangeAmount = dbCustomerAccount.First().AccountBalance,
                    BeforeChangeAmount = dbCustomerAccount.First().AccountBalance + inv.InvoiceAmount,
                    Credit = inv.InvoiceAmount,
                    CreatorName = userInfo.Name,
                    SerialType = (int)DcsSerialType.账户金额
                };
                DomainService.checkInsertCustomerAccountHisDetail(customerAccountHisDetail);
                InsertToDatabase(customerAccountHisDetail);
            }
            UpdateToDatabase(dbCustomerAccount.First());
            this.UpdateCustomerAccountValidate(dbCustomerAccount.First());
        }

        public void 销售退货结算根据结算金额过账(PartsSalesRtnSettlement partsSalesRtnSettlement) {
            var dbCustomerAccount = GetCustomerAccountsWithLock(ObjectContext.CustomerAccounts.Where(r => r.Id == partsSalesRtnSettlement.CustomerAccountId && r.Status != (int)DcsMasterDataStatus.作废)).ToArray();
            if(!dbCustomerAccount.Any() || dbCustomerAccount.Count() > 1)
                throw new ValidationException(ErrorStrings.CustomerAccount_Validation3);
            dbCustomerAccount.First().AccountBalance += partsSalesRtnSettlement.TotalSettlementAmount;
            UpdateToDatabase(dbCustomerAccount.First());
            this.UpdateCustomerAccountValidate(dbCustomerAccount.First());
            var userInfo = Utils.GetCurrentUserInfo();
            var customerAccountHisDetail = new CustomerAccountHisDetail {
                CustomerAccountId = dbCustomerAccount.First().Id,
                ChangeAmount = partsSalesRtnSettlement.TotalSettlementAmount,
                ProcessDate = DateTime.Now,
                BusinessType = (int)DcsAccountPaymentBusinessType.销售退货入库,
                SourceId = partsSalesRtnSettlement.Id,
                SourceCode = partsSalesRtnSettlement.Code,
                Summary = partsSalesRtnSettlement.Remark,
                AfterChangeAmount = dbCustomerAccount.First().AccountBalance,
                BeforeChangeAmount = dbCustomerAccount.First().AccountBalance - partsSalesRtnSettlement.TotalSettlementAmount,
                Credit = partsSalesRtnSettlement.TotalSettlementAmount,
                CreatorName = userInfo.Name,
                InvoiceDate = partsSalesRtnSettlement.InvoiceDate,
                SerialType = (int)DcsSerialType.账户金额
            };
            DomainService.checkInsertCustomerAccountHisDetail(customerAccountHisDetail);
            InsertToDatabase(customerAccountHisDetail);
        }

        internal void 销售订单审批过账(int customerAccountId, decimal amount) {
            var dbCustomerAccount = GetCustomerAccountsWithLock(ObjectContext.CustomerAccounts.Where(r => r.Id == customerAccountId && r.Status != (int)DcsMasterDataStatus.作废)).ToArray();
            if(!dbCustomerAccount.Any() || dbCustomerAccount.Count() > 1)
                throw new ValidationException(ErrorStrings.CustomerAccount_Validation3);
            dbCustomerAccount.First().PendingAmount += amount;
            //UpdateToDatabase(dbCustomerAccount.First());
            this.UpdateCustomerAccountValidate(dbCustomerAccount.First());
        }

        internal void 销售订单审批过账记录流水(PartsSalesOrder partsSalesOrder, decimal amount) {
            var dbCustomerAccount = GetCustomerAccountsWithLock(ObjectContext.CustomerAccounts.Where(r => r.Id == partsSalesOrder.CustomerAccountId && r.Status != (int)DcsMasterDataStatus.作废)).ToArray();
            if(!dbCustomerAccount.Any() || dbCustomerAccount.Count() > 1)
                throw new ValidationException(ErrorStrings.CustomerAccount_Validation3);

            var userInfo = Utils.GetCurrentUserInfo();
            var customerAccountHisDetail = new CustomerAccountHisDetail();
            customerAccountHisDetail.CustomerAccountId = dbCustomerAccount.First().Id;
            customerAccountHisDetail.ChangeAmount = 0 + amount;
            customerAccountHisDetail.ProcessDate = DateTime.Now;
            customerAccountHisDetail.CreatorName = userInfo.Name;
            customerAccountHisDetail.BusinessType = (int)DcsAccountPaymentBusinessType.销售出库计划;
            customerAccountHisDetail.BeforeChangeAmount = dbCustomerAccount.First().PendingAmount;
            customerAccountHisDetail.Debit = amount;
            customerAccountHisDetail.InvoiceDate = DateTime.Now;
            customerAccountHisDetail.SourceId = partsSalesOrder.Id;
            customerAccountHisDetail.SourceCode = partsSalesOrder.Code;
            customerAccountHisDetail.AfterChangeAmount = dbCustomerAccount.First().PendingAmount + amount;
            customerAccountHisDetail.Summary = "销售订单审核锁定";
            customerAccountHisDetail.SerialType = (int)DcsSerialType.审批待发;
            DomainService.checkInsertCustomerAccountHisDetail(customerAccountHisDetail);
            InsertToDatabase(customerAccountHisDetail);

            dbCustomerAccount.First().PendingAmount += amount;
            //UpdateToDatabase(dbCustomerAccount.First());
            this.UpdateCustomerAccountValidate(dbCustomerAccount.First());
            this.ObjectContext.SaveChanges();
        }

        internal void 销售订单审批过账记录流水2(PartsSalesOrder partsSalesOrder, decimal amount) {
            var dbCustomerAccount = GetCustomerAccountsWithLock(ObjectContext.CustomerAccounts.Where(r => r.Id == partsSalesOrder.CustomerAccountId && r.Status != (int)DcsMasterDataStatus.作废)).ToArray();
            if (!dbCustomerAccount.Any() || dbCustomerAccount.Count() > 1)
                throw new ValidationException(ErrorStrings.CustomerAccount_Validation3);

            var userInfo = Utils.GetCurrentUserInfo();
            var customerAccountHisDetail = new CustomerAccountHisDetail();
            customerAccountHisDetail.CustomerAccountId = dbCustomerAccount.First().Id;
            customerAccountHisDetail.ChangeAmount = 0 + amount;
            customerAccountHisDetail.ProcessDate = DateTime.Now;
            customerAccountHisDetail.CreatorName = userInfo.Name;
            customerAccountHisDetail.BusinessType = (int)DcsAccountPaymentBusinessType.销售出库;
            customerAccountHisDetail.BeforeChangeAmount = dbCustomerAccount.First().ShippedProductValue;
            customerAccountHisDetail.Debit = amount;
            customerAccountHisDetail.InvoiceDate = DateTime.Now;
            customerAccountHisDetail.SourceId = partsSalesOrder.Id;
            customerAccountHisDetail.SourceCode = partsSalesOrder.Code;
            customerAccountHisDetail.AfterChangeAmount = dbCustomerAccount.First().ShippedProductValue + amount;
            customerAccountHisDetail.Summary = "转中心库审核增加发出商品金额";
            customerAccountHisDetail.SerialType = (int)DcsSerialType.发出商品;
            DomainService.checkInsertCustomerAccountHisDetail(customerAccountHisDetail);
            InsertToDatabase(customerAccountHisDetail);

            dbCustomerAccount.First().ShippedProductValue += amount;
            //UpdateToDatabase(dbCustomerAccount.First());
            this.UpdateCustomerAccountValidate(dbCustomerAccount.First());
            this.ObjectContext.SaveChanges();
        }

        /// <summary>
        /// 获取指定的配件锁定库存，并加数据库行级锁（待当前会话结束后释放）
        /// </summary>
        /// <param name="filter">CustomerAccount的查询语句，或查询结果集</param>
        /// <returns></returns>
        internal IEnumerable<CustomerAccount> GetCustomerAccountsWithLock(IEnumerable<CustomerAccount> filter) {
            if(filter == null)
                return Enumerable.Empty<CustomerAccount>();

            //考虑到filter是本地过滤数据的可能性，使用Id关联代替对象关联
            var query = filter as IQueryable<CustomerAccount>;
            var customerAccountIds = query == null ? filter.Select(v => v.Id) : query.Select(v => v.Id);

            var objectQuery = ObjectContext.CustomerAccounts.Where(v => customerAccountIds.Contains(v.Id)) as ObjectQuery<CustomerAccount>;
            if(objectQuery == null)
                return Enumerable.Empty<CustomerAccount>();

            var sql = string.Format("select * from ({0}) for update", objectQuery.ToTraceString());
#if SqlServer
            var paramList = objectQuery.Parameters;
#else
            var paramList = objectQuery.Parameters.Select(v => new Devart.Data.Oracle.OracleParameter(v.Name, v.Value) as object).ToArray();
#endif
            return ObjectContext.ExecuteStoreQuery<CustomerAccount>(sql, "CustomerAccounts", MergeOption.AppendOnly, paramList);
        }

    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:11:15
        //原分布类的函数全部转移到Ach                                                               

        public void 销售结算过账(PartsSalesSettlement partsSalesSettlement) {
            new CustomerAccountAch(this).销售结算过账(partsSalesSettlement);
        }
        public void 销售退货结算过账(PartsSalesRtnSettlement partsSalesRtnSettlement) {
            new CustomerAccountAch(this).销售退货结算过账(partsSalesRtnSettlement);
        }

        public void 销售退货结算根据结算金额过账(PartsSalesRtnSettlement partsSalesRtnSettlement) {
            new CustomerAccountAch(this).销售退货结算根据结算金额过账(partsSalesRtnSettlement);
        }
    }
}
