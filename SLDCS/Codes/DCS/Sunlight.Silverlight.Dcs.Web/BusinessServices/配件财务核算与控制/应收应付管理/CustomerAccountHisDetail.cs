﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class CustomerAccountHisDetailAch : DcsSerivceAchieveBase {
        public CustomerAccountHisDetailAch(DcsDomainService domainService)
            : base(domainService) {
        }

        public void checkInsertCustomerAccountHisDetail(CustomerAccountHisDetail customerAccountHisDetail) {
            if(customerAccountHisDetail.SerialType.HasValue) {
                var lastCustomerAccountHisDetail = ObjectContext.CustomerAccountHisDetails.Where(r => r.CustomerAccountId == customerAccountHisDetail.CustomerAccountId && r.SerialType == customerAccountHisDetail.SerialType).OrderByDescending(r => r.Id).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
                if(lastCustomerAccountHisDetail != null) {
                    if(lastCustomerAccountHisDetail.AfterChangeAmount.HasValue && customerAccountHisDetail.BeforeChangeAmount != lastCustomerAccountHisDetail.AfterChangeAmount) {
                        throw new ValidationException(ErrorStrings.CustomerAccountHisDetail_Validation1);
                    }
                }
            } else {
                throw new ValidationException(ErrorStrings.CustomerAccountHisDetail_Validation1);
            }
        }
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:11:15
        //原分布类的函数全部转移到Ach                                                               

        public void checkInsertCustomerAccountHisDetail(CustomerAccountHisDetail customerAccountHisDetail) {
            new CustomerAccountHisDetailAch(this).checkInsertCustomerAccountHisDetail(customerAccountHisDetail);
        }
    }
}
