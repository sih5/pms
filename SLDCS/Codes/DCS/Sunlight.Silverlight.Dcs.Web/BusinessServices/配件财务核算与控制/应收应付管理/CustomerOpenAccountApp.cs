﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class CustomerOpenAccountAppAch : DcsSerivceAchieveBase {
        public CustomerOpenAccountAppAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 审核客户开户申请单(CustomerOpenAccountApp customerOpenAccountApp) {
            var dbCustomerOpenAccountApp = ObjectContext.CustomerOpenAccountApps.Where(r => r.Id == customerOpenAccountApp.Id && r.Status == (int)DcsWorkflowOfSimpleApprovalStatus.新建).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbCustomerOpenAccountApp == null)
                throw new ValidationException(ErrorStrings.CustomerOpenAccountApp_Validation1);
            var dbCustomerAccount = ObjectContext.CustomerAccounts.Where(r => r.AccountGroupId == customerOpenAccountApp.AccountGroupId && r.CustomerCompanyId == customerOpenAccountApp.CustomerCompanyId && r.Status != (int)DcsMasterDataStatus.作废).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbCustomerAccount != null)
                throw new ValidationException(ErrorStrings.CustomerOpenAccountApp_Validation2);
            var customerAccount = new CustomerAccount {
                AccountGroupId = customerOpenAccountApp.AccountGroupId,
                CustomerCompanyId = customerOpenAccountApp.CustomerCompanyId,
                AccountBalance = customerOpenAccountApp.AccountInitialDeposit,
                ShippedProductValue = 0,
                PendingAmount = 0,
                CustomerCredenceAmount = 0,
                Status = (int)DcsMasterDataStatus.有效,
            };
            var userInfo = Utils.GetCurrentUserInfo();
            var customerAccountHisDetail = new CustomerAccountHisDetail {
                ChangeAmount = customerOpenAccountApp.AccountInitialDeposit,
                ProcessDate = DateTime.Now,
                BusinessType = (int)DcsAccountPaymentBusinessType.开户,
                SourceId = customerOpenAccountApp.Id,
                SourceCode = customerOpenAccountApp.Code,
                Summary = customerOpenAccountApp.Remark,
                AfterChangeAmount = customerOpenAccountApp.AccountInitialDeposit,
                BeforeChangeAmount = 0,
                Credit = 0,
                CreatorName = userInfo.Name,
                SerialType = (int)DcsSerialType.账户金额
            };
            customerAccount.CustomerAccountHisDetails.Add(customerAccountHisDetail);
            DomainService.checkInsertCustomerAccountHisDetail(customerAccountHisDetail);
            InsertToDatabase(customerAccountHisDetail);
            InsertToDatabase(customerAccount);
            new CustomerAccountAch(this.DomainService).InsertCustomerAccountValidate(customerAccount);
            UpdateToDatabase(customerOpenAccountApp);
            customerOpenAccountApp.Status = (int)DcsWorkflowOfSimpleApprovalStatus.已审核;
            this.UpdateCustomerOpenAccountAppValidate(customerOpenAccountApp);
        }

        public void 审核客户开户申请单单据(int id) {
            var dbCustomerOpenAccountApp = ObjectContext.CustomerOpenAccountApps.Where(r => r.Id == id && r.Status == (int)DcsWorkflowOfSimpleApprovalStatus.新建).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbCustomerOpenAccountApp == null)
                throw new ValidationException(ErrorStrings.CustomerOpenAccountApp_Validation1);
            var dbCustomerAccount = ObjectContext.CustomerAccounts.Where(r => r.AccountGroupId == dbCustomerOpenAccountApp.AccountGroupId && r.CustomerCompanyId == dbCustomerOpenAccountApp.CustomerCompanyId && r.Status != (int)DcsMasterDataStatus.作废).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbCustomerAccount != null)
                throw new ValidationException(ErrorStrings.CustomerOpenAccountApp_Validation2);
            var customerAccount = new CustomerAccount {
                AccountGroupId = dbCustomerOpenAccountApp.AccountGroupId,
                CustomerCompanyId = dbCustomerOpenAccountApp.CustomerCompanyId,
                AccountBalance = dbCustomerOpenAccountApp.AccountInitialDeposit,
                ShippedProductValue = 0,
                PendingAmount = 0,
                CustomerCredenceAmount = 0,
                Status = (int)DcsMasterDataStatus.有效
            };
            var userInfo = Utils.GetCurrentUserInfo();
            var customerAccountHisDetail = new CustomerAccountHisDetail {
                ChangeAmount = dbCustomerOpenAccountApp.AccountInitialDeposit,
                ProcessDate = DateTime.Now,
                BusinessType = (int)DcsAccountPaymentBusinessType.开户,
                SourceId = dbCustomerOpenAccountApp.Id,
                SourceCode = dbCustomerOpenAccountApp.Code,
                Summary = dbCustomerOpenAccountApp.Remark,
                AfterChangeAmount = dbCustomerOpenAccountApp.AccountInitialDeposit,
                BeforeChangeAmount = 0,
                Credit = 0,
                CreatorName = userInfo.Name,
                SerialType = (int)DcsSerialType.账户金额
            };
            customerAccount.CustomerAccountHisDetails.Add(customerAccountHisDetail);
            DomainService.checkInsertCustomerAccountHisDetail(customerAccountHisDetail);
            InsertToDatabase(customerAccountHisDetail);
            InsertToDatabase(customerAccount);
            new CustomerAccountAch(this.DomainService).InsertCustomerAccountValidate(customerAccount);
            UpdateToDatabase(dbCustomerOpenAccountApp);
            dbCustomerOpenAccountApp.Status = (int)DcsWorkflowOfSimpleApprovalStatus.已审核;
            this.UpdateCustomerOpenAccountAppValidate(dbCustomerOpenAccountApp);
            this.ObjectContext.SaveChanges();
        }

        public void 作废客户开户申请单(CustomerOpenAccountApp customerOpenAccountApp) {
            var dbCustomerOpenAccountApp = ObjectContext.CustomerOpenAccountApps.Where(r => r.Id == customerOpenAccountApp.Id && r.Status == (int)DcsWorkflowOfSimpleApprovalStatus.新建).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbCustomerOpenAccountApp == null)
                throw new ValidationException(ErrorStrings.CustomerOpenAccountApp_Validation3);
            var userInfo = Utils.GetCurrentUserInfo();
            UpdateToDatabase(customerOpenAccountApp);
            customerOpenAccountApp.Status = (int)DcsMasterDataStatus.作废;
            customerOpenAccountApp.AbandonerId = userInfo.Id;
            customerOpenAccountApp.AbandonerName = userInfo.Name;
            customerOpenAccountApp.AbandonTime = DateTime.Now;
            this.UpdateCustomerOpenAccountAppValidate(customerOpenAccountApp);
        }

        public void 作废客户开户申请单单据(int id) {
            var dbCustomerOpenAccountApp = ObjectContext.CustomerOpenAccountApps.Where(r => r.Id == id && r.Status == (int)DcsWorkflowOfSimpleApprovalStatus.新建).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbCustomerOpenAccountApp == null)
                throw new ValidationException(ErrorStrings.CustomerOpenAccountApp_Validation3);
            var userInfo = Utils.GetCurrentUserInfo();
            UpdateToDatabase(dbCustomerOpenAccountApp);
            dbCustomerOpenAccountApp.Status = (int)DcsMasterDataStatus.作废;
            dbCustomerOpenAccountApp.AbandonerId = userInfo.Id;
            dbCustomerOpenAccountApp.AbandonerName = userInfo.Name;
            dbCustomerOpenAccountApp.AbandonTime = DateTime.Now;
            this.UpdateCustomerOpenAccountAppValidate(dbCustomerOpenAccountApp);
            this.ObjectContext.SaveChanges();
        }

        public void 新增同时开户(CustomerOpenAccountApp customerOpenAccountApp) {
            var dbCustomerAccount = ObjectContext.CustomerAccounts.Where(r => r.AccountGroupId == customerOpenAccountApp.AccountGroupId && r.CustomerCompanyId == customerOpenAccountApp.CustomerCompanyId && r.Status != (int)DcsMasterDataStatus.作废).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbCustomerAccount != null)
                throw new ValidationException(ErrorStrings.CustomerOpenAccountApp_Validation2);
            var customerAccount = new CustomerAccount {
                AccountGroupId = customerOpenAccountApp.AccountGroupId,
                CustomerCompanyId = customerOpenAccountApp.CustomerCompanyId,
                AccountBalance = customerOpenAccountApp.AccountInitialDeposit,
                ShippedProductValue = 0,
                PendingAmount = 0,
                CustomerCredenceAmount = 0,
                Status = (int)DcsMasterDataStatus.有效
            };
            InsertToDatabase(customerAccount);
            new CustomerAccountAch(this.DomainService).InsertCustomerAccountValidate(customerAccount);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:11:15
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 审核客户开户申请单(CustomerOpenAccountApp customerOpenAccountApp) {
            new CustomerOpenAccountAppAch(this).审核客户开户申请单(customerOpenAccountApp);
        }

        [Invoke]
        public void 审核客户开户申请单单据(int id) {
            new CustomerOpenAccountAppAch(this).审核客户开户申请单单据(id);
        }

        [Update(UsingCustomMethod = true)]
        public void 作废客户开户申请单(CustomerOpenAccountApp customerOpenAccountApp) {
            new CustomerOpenAccountAppAch(this).作废客户开户申请单(customerOpenAccountApp);
        }

        [Invoke]
        public void 作废客户开户申请单单据(int id) {
            new CustomerOpenAccountAppAch(this).作废客户开户申请单单据(id);
        }

        [Update(UsingCustomMethod = true)]
        public void 新增同时开户(CustomerOpenAccountApp customerOpenAccountApp) {
            new CustomerOpenAccountAppAch(this).新增同时开户(customerOpenAccountApp);
        }
    }
}
