﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class CustomerTransferBillAch : DcsSerivceAchieveBase {
        public CustomerTransferBillAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 审核客户转账单(CustomerTransferBill customerTransferBill, DateTime now) {
            var dbCustomerTransferBill = ObjectContext.CustomerTransferBills.Where(r => r.Id == customerTransferBill.Id && r.Status == (int)DcsWorkflowOfSimpleApprovalStatus.新建).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbCustomerTransferBill == null)
                throw new ValidationException(ErrorStrings.CustomerTransferBill_Validation1);
            //查询转入转出的客户类型
            var outCompany = ObjectContext.Companies.Where(r => r.Id == customerTransferBill.OutboundCustomerCompanyId).FirstOrDefault();
            var inCompany = ObjectContext.Companies.Where(r => r.Id == customerTransferBill.InboundCustomerCompanyId).FirstOrDefault();
            //如果转出或者转入的客户类型为 供应商不校验是否存在客户账户，并且不变更对应的客户账户金额。
            //当且仅当客户类型不为供应商的时候，才去变更客户账户，并生成账户明细。
            var userInfo = Utils.GetCurrentUserInfo();

            if((int)DcsCompanyType.配件供应商 != outCompany.Type) {
                var dbCustomerAccountOut = new CustomerAccountAch(this.DomainService).GetCustomerAccountsWithLock(ObjectContext.CustomerAccounts.Where(r => r.AccountGroupId == customerTransferBill.OutboundAccountGroupId && r.CustomerCompanyId == customerTransferBill.OutboundCustomerCompanyId && r.Status != (int)DcsMasterDataStatus.作废)).ToArray();
                if(!dbCustomerAccountOut.Any() || dbCustomerAccountOut.Count() > 1)
                    throw new ValidationException(ErrorStrings.CustomerTransferBill_Validation2);
                //获取分公司策略，转账控制
                //查询分公司策略（客户转账单.转出账户组id=销售组织.账户组id，销售组织.营销分公司id=分公司策略.分公司id）
                var branchstrategy = ObjectContext.Branchstrategies.FirstOrDefault(r => ObjectContext.SalesUnits.Any(s => s.BranchId == r.BranchId && s.AccountGroupId == customerTransferBill.OutboundAccountGroupId));
                if(branchstrategy == null)
                    throw new ValidationException(ErrorStrings.CustomerTransferBill_Validation5);
                dbCustomerAccountOut.First().AccountBalance -= customerTransferBill.Amount;
                if((int)DcsCompanyType.服务站 == outCompany.Type || (int)DcsCompanyType.服务站兼代理库 == outCompany.Type) {
                    if(dbCustomerAccountOut.First().AccountBalance < 0) {
                        if(!branchstrategy.AccountControl.HasValue || (branchstrategy.AccountControl.HasValue && !branchstrategy.AccountControl.Value)) {
                            throw new ValidationException(ErrorStrings.CustomerTransferBill_Validation4);
                        }
                    }
                }
                var customerAccountHisDetailOut = new CustomerAccountHisDetail {
                    CustomerAccountId = dbCustomerAccountOut.First().Id,
                    ChangeAmount = -1 * customerTransferBill.Amount,
                    Credit = -1 * customerTransferBill.Amount,
                    BeforeChangeAmount = dbCustomerAccountOut.First().AccountBalance + customerTransferBill.Amount,
                    AfterChangeAmount = dbCustomerAccountOut.First().AccountBalance,
                    ProcessDate = now,
                    BusinessType = (int)DcsAccountPaymentBusinessType.客户转账,
                    SourceId = customerTransferBill.Id,
                    SourceCode = customerTransferBill.Code,
                    Summary = customerTransferBill.Summary,
                    CreatorName = userInfo.Name,
                    InvoiceDate = customerTransferBill.InvoiceDate,
                    SerialType = (int)DcsSerialType.账户金额
                };
                dbCustomerAccountOut.First().CustomerAccountHisDetails.Add(customerAccountHisDetailOut);
                DomainService.checkInsertCustomerAccountHisDetail(customerAccountHisDetailOut);
                InsertToDatabase(customerAccountHisDetailOut);
                new CustomerAccountAch(this.DomainService).UpdateCustomerAccountValidate(dbCustomerAccountOut.First());
            }
            if((int)DcsCompanyType.配件供应商 != inCompany.Type) {
                var dbCustomerAccountIn = new CustomerAccountAch(this.DomainService).GetCustomerAccountsWithLock(ObjectContext.CustomerAccounts.Where(r => r.AccountGroupId == customerTransferBill.InboundAccountGroupId && r.CustomerCompanyId == customerTransferBill.InboundCustomerCompanyId && r.Status != (int)DcsMasterDataStatus.作废)).ToArray();
                if(!dbCustomerAccountIn.Any() || dbCustomerAccountIn.Count() > 1)
                    throw new ValidationException(ErrorStrings.CustomerTransferBill_Validation3);
                dbCustomerAccountIn.First().AccountBalance += customerTransferBill.Amount;
                var customerAccountHisDetailIn = new CustomerAccountHisDetail {
                    CustomerAccountId = dbCustomerAccountIn.First().Id,
                    ChangeAmount = customerTransferBill.Amount,
                    Credit = customerTransferBill.Amount,
                    BeforeChangeAmount = dbCustomerAccountIn.First().AccountBalance - customerTransferBill.Amount,
                    AfterChangeAmount = dbCustomerAccountIn.First().AccountBalance,
                    ProcessDate = now,
                    BusinessType = (int)DcsAccountPaymentBusinessType.客户转账,
                    SourceId = customerTransferBill.Id,
                    SourceCode = customerTransferBill.Code,
                    Summary = customerTransferBill.Summary,
                    CreatorName = userInfo.Name,
                    InvoiceDate = customerTransferBill.InvoiceDate,
                    SerialType = (int)DcsSerialType.账户金额
                };
                dbCustomerAccountIn.First().CustomerAccountHisDetails.Add(customerAccountHisDetailIn);
                DomainService.checkInsertCustomerAccountHisDetail(customerAccountHisDetailIn);
                InsertToDatabase(customerAccountHisDetailIn);
                new CustomerAccountAch(this.DomainService).UpdateCustomerAccountValidate(dbCustomerAccountIn.First());
            }
            //如果类型=客户三方转款
            //如果转出=服务站或者服务站兼中心库， 并且转入=中心库：增加服务站在中心库账户组的余额，生成往来账
            //如果转出=中心库，转入=服务站或者服务站兼中心库，扣减服务站在中心库账户组的余额，生成往来账
            if(customerTransferBill.BusinessType == (int)DcsCustomerTransferBill_BusinessType.客户三方转款) {
                if((outCompany.Type == (int)DcsCompanyType.服务站 || outCompany.Type == (int)DcsCompanyType.服务站兼代理库) && inCompany.Type == (int)DcsCompanyType.代理库) {
                    //如果转出=服务站或者服务站兼中心库， 并且转入=中心库：增加服务站在中心库账户组的余额，生成往来账
                   //销售组织查询账户组信息
                    var salesUnit = ObjectContext.SalesUnits.Where(r => r.OwnerCompanyId == customerTransferBill.InboundCustomerCompanyId).FirstOrDefault();
                    if(null == salesUnit) {
                        throw new ValidationException(string.Format(ErrorStrings.CustomerTransferBill_Validation6,inCompany.Name));
                    }
                    //客户账户
                    var dbCustomerAccountIn = new CustomerAccountAch(this.DomainService).GetCustomerAccountsWithLock(ObjectContext.CustomerAccounts.Where(r => r.AccountGroupId == salesUnit.AccountGroupId && r.CustomerCompanyId == customerTransferBill.OutboundCustomerCompanyId && r.Status != (int)DcsMasterDataStatus.作废)).ToArray();
                    if(!dbCustomerAccountIn.Any() || dbCustomerAccountIn.Count() > 1)
                        throw new ValidationException(ErrorStrings.CustomerTransferBill_Validation3);
                    dbCustomerAccountIn.First().AccountBalance += customerTransferBill.Amount;
                    var customerAccountHisDetailIn = new CustomerAccountHisDetail {
                        CustomerAccountId = dbCustomerAccountIn.First().Id,
                        ChangeAmount = customerTransferBill.Amount,
                        Credit = customerTransferBill.Amount,
                        BeforeChangeAmount = dbCustomerAccountIn.First().AccountBalance - customerTransferBill.Amount,
                        AfterChangeAmount = dbCustomerAccountIn.First().AccountBalance,
                        ProcessDate = now,
                        BusinessType = (int)DcsAccountPaymentBusinessType.客户转账,
                        SourceId = customerTransferBill.Id,
                        SourceCode = customerTransferBill.Code,
                        Summary = customerTransferBill.Summary,
                        CreatorName = userInfo.Name,
                        InvoiceDate = customerTransferBill.InvoiceDate,
                        SerialType = (int)DcsSerialType.账户金额
                    };
                    dbCustomerAccountIn.First().CustomerAccountHisDetails.Add(customerAccountHisDetailIn);
                    DomainService.checkInsertCustomerAccountHisDetail(customerAccountHisDetailIn);
                    InsertToDatabase(customerAccountHisDetailIn);
                    new CustomerAccountAch(this.DomainService).UpdateCustomerAccountValidate(dbCustomerAccountIn.First());
                } else if((inCompany.Type == (int)DcsCompanyType.服务站 || inCompany.Type == (int)DcsCompanyType.服务站兼代理库) && outCompany.Type == (int)DcsCompanyType.代理库) {
                    //如果转出=中心库，转入=服务站或者服务站兼中心库，扣减服务站在中心库账户组的余额，生成往来账
                    //销售组织查询账户组信息
                    var salesUnit = ObjectContext.SalesUnits.Where(r => r.OwnerCompanyId == customerTransferBill.OutboundCustomerCompanyId).FirstOrDefault();
                    if(null == salesUnit) {
                        throw new ValidationException(string.Format(ErrorStrings.CustomerTransferBill_Validation6,inCompany.Name));
                    }
                    var dbCustomerAccountOut = new CustomerAccountAch(this.DomainService).GetCustomerAccountsWithLock(ObjectContext.CustomerAccounts.Where(r => r.AccountGroupId == salesUnit.AccountGroupId && r.CustomerCompanyId == customerTransferBill.InboundCustomerCompanyId && r.Status != (int)DcsMasterDataStatus.作废)).ToArray();
                    if(!dbCustomerAccountOut.Any() || dbCustomerAccountOut.Count() > 1)
                        throw new ValidationException(ErrorStrings.CustomerTransferBill_Validation2);

                    dbCustomerAccountOut.First().AccountBalance -= customerTransferBill.Amount;
                    var customerAccountHisDetailOut = new CustomerAccountHisDetail {
                        CustomerAccountId = dbCustomerAccountOut.First().Id,
                        ChangeAmount = -1 * customerTransferBill.Amount,
                        Credit = -1 * customerTransferBill.Amount,
                        BeforeChangeAmount = dbCustomerAccountOut.First().AccountBalance + customerTransferBill.Amount,
                        AfterChangeAmount = dbCustomerAccountOut.First().AccountBalance,
                        ProcessDate = now,
                        BusinessType = (int)DcsAccountPaymentBusinessType.客户转账,
                        SourceId = customerTransferBill.Id,
                        SourceCode = customerTransferBill.Code,
                        Summary = customerTransferBill.Summary,
                        CreatorName = userInfo.Name,
                        InvoiceDate = customerTransferBill.InvoiceDate,
                        SerialType = (int)DcsSerialType.账户金额
                    };
                    dbCustomerAccountOut.First().CustomerAccountHisDetails.Add(customerAccountHisDetailOut);
                    DomainService.checkInsertCustomerAccountHisDetail(customerAccountHisDetailOut);
                    InsertToDatabase(customerAccountHisDetailOut);
                    new CustomerAccountAch(this.DomainService).UpdateCustomerAccountValidate(dbCustomerAccountOut.First());
                }
                
            }
            UpdateToDatabase(customerTransferBill);
            customerTransferBill.Status = (int)DcsWorkflowOfSimpleApprovalStatus.已审核;
            this.UpdateCustomerTransferBillValidate(customerTransferBill);
            ObjectContext.SaveChanges();
            //  InsertCustomerAccountHisDetailForCustomerTransferBill(customerTransferBill, dbCustomerAccountIn.Id, dbCustomerAccountOut.Id);
        }


        public void InsertCustomerAccountHisDetailForCustomerTransferBill(CustomerTransferBill customerTransferBill, int CustomerAccountInId, int CustomerAccountOutId) {
            var userInfo = Utils.GetCurrentUserInfo();
            var customerAccountHisDetailIn = new CustomerAccountHisDetail {
                CustomerAccountId = CustomerAccountInId,
                ChangeAmount = customerTransferBill.Amount,
                ProcessDate = DateTime.Now,
                BusinessType = (int)DcsAccountPaymentBusinessType.企业间转账,
                SourceId = customerTransferBill.Id,
                SourceCode = customerTransferBill.Code,
                Summary = customerTransferBill.Summary,
                CreatorName = userInfo.Name,
                SerialType = (int)DcsSerialType.账户金额
            };
            DomainService.checkInsertCustomerAccountHisDetail(customerAccountHisDetailIn);
            InsertToDatabase(customerAccountHisDetailIn);
            var customerAccountHisDetailOut = new CustomerAccountHisDetail {
                CustomerAccountId = CustomerAccountOutId,
                ChangeAmount = -customerTransferBill.Amount,
                ProcessDate = DateTime.Now,
                BusinessType = (int)DcsAccountPaymentBusinessType.企业间转账,
                SourceId = customerTransferBill.Id,
                SourceCode = customerTransferBill.Code,
                Summary = customerTransferBill.Summary,
                CreatorName = userInfo.Name,
                SerialType = (int)DcsSerialType.账户金额
            };
            DomainService.checkInsertCustomerAccountHisDetail(customerAccountHisDetailOut);
            InsertToDatabase(customerAccountHisDetailOut);
        }
        public void 批量审核客户转账单(int[] ids) {
            int index = 0;
            var now = DateTime.Now;
            foreach(var id in ids) {
                var dbCustomerTransferBill = ObjectContext.CustomerTransferBills.Where(r => r.Id == id).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
                this.审核客户转账单(dbCustomerTransferBill, now.AddSeconds(index));
                index++;
            }
            ObjectContext.SaveChanges();
        }

    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:11:15
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 审核客户转账单(CustomerTransferBill customerTransferBill, DateTime now) {
            new CustomerTransferBillAch(this).审核客户转账单(customerTransferBill, now);
        }


        [Invoke]
        public void InsertCustomerAccountHisDetailForCustomerTransferBill(CustomerTransferBill customerTransferBill, int CustomerAccountInId, int CustomerAccountOutId) {
            new CustomerTransferBillAch(this).InsertCustomerAccountHisDetailForCustomerTransferBill(customerTransferBill, CustomerAccountInId, CustomerAccountOutId);
        }
        [Invoke]
        public void 批量审核客户转账单(int[] ids) {
            new CustomerTransferBillAch(this).批量审核客户转账单(ids);
        }
    }
}
