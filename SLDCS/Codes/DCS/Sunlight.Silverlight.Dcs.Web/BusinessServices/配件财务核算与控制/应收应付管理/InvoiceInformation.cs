﻿using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Server;using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class InvoiceInformationAch : DcsSerivceAchieveBase {
        public InvoiceInformationAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 删除发票信息(InvoiceInformation invoiceInformation) {
            var purchaseSettleInvoiceRels = ObjectContext.PurchaseSettleInvoiceRels.Where(r => r.InvoiceId == invoiceInformation.Id).ToArray();
            foreach(var purchaseSettleInvoiceRel in purchaseSettleInvoiceRels) {
                DeleteFromDatabase(purchaseSettleInvoiceRel);
            }
            DeleteFromDatabase(invoiceInformation);
        }

        public void 新增采购发票(InvoiceInformation invoiceInformation, int partsPurchaseSettleBillId) {
            InsertInvoiceInformation(invoiceInformation);
            PurchaseSettleInvoiceRel psir = new PurchaseSettleInvoiceRel();
            psir.PartsPurchaseSettleBillId = partsPurchaseSettleBillId;
            psir.InvoiceId = invoiceInformation.Id;
            DomainService.InsertPurchaseSettleInvoiceRel(psir);
        }

        public void 删除采购发票(InvoiceInformation invoiceInformation, int partsPurchaseSettleBillId) {
            DeleteInvoiceInformation(invoiceInformation);
            PurchaseSettleInvoiceRel psir = ObjectContext.PurchaseSettleInvoiceRels.FirstOrDefault(r => r.InvoiceId == invoiceInformation.Id && r.PartsPurchaseSettleBillId == partsPurchaseSettleBillId);
            if(psir == null) {
                throw new ValidationException(ErrorStrings.InvoiceInformation_Validation1);
            }
            DomainService.DeletePurchaseSettleInvoiceRel(psir);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:11:15
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 删除发票信息(InvoiceInformation invoiceInformation) {
            new InvoiceInformationAch(this).删除发票信息(invoiceInformation);
        }
        
        [Update(UsingCustomMethod = true)]
        public void 新增采购发票(InvoiceInformation invoiceInformation, int partsPurchaseSettleBillId) {
            new InvoiceInformationAch(this).新增采购发票(invoiceInformation,partsPurchaseSettleBillId);
        }
        
        [Update(UsingCustomMethod = true)]
        public void 删除采购发票(InvoiceInformation invoiceInformation, int partsPurchaseSettleBillId) {
            new InvoiceInformationAch(this).删除采购发票(invoiceInformation,partsPurchaseSettleBillId);
        }
    }
}
