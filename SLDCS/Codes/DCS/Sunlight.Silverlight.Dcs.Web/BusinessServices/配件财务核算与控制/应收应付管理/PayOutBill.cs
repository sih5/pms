﻿
using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class PayOutBillAch : DcsSerivceAchieveBase {
        public PayOutBillAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 审批付款单(PayOutBill payOutBill) {
            var payOutBill1 = ObjectContext.PayOutBills.Where(r => r.Id == payOutBill.Id).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(payOutBill1 == null) {
                throw new ValidationException(ErrorStrings.Common_Validation2);
            }
            if(payOutBill1.Status != (int)DcsWorkflowOfSimpleApprovalStatus.新建) {
                throw new ValidationException(ErrorStrings.PayOutBill_Validation6);
            }
            var dbsupplierAccount = ObjectContext.SupplierAccounts.Where(r => r.BuyerCompanyId == payOutBill.BuyerCompanyId && r.PartsSalesCategoryId == payOutBill.PartsSalesCategoryId && r.SupplierCompanyId == payOutBill.SupplierCompanyId).ToArray();
            if(dbsupplierAccount.Length == 0)
                throw new ValidationException(ErrorStrings.PayOutBill_Validation1);
            if(dbsupplierAccount.Length > 1)
                throw new ValidationException(ErrorStrings.PayOutBill_Validation2);
            var dbsupplierAccount1 = dbsupplierAccount.First();
            if(dbsupplierAccount.Length == 1) {
                UpdateToDatabase(dbsupplierAccount1);
                dbsupplierAccount1.DueAmount = dbsupplierAccount1.DueAmount + payOutBill.Amount;
                new SupplierAccountAch(this.DomainService).UpdateSupplierAccountValidate(dbsupplierAccount1);
            }
            //新增应付账款变更明细
            var dbAccountPayableHistoryDetail = new AccountPayableHistoryDetail {
                SupplierAccountId = dbsupplierAccount1.Id,
                ChangeAmount = payOutBill.Amount,
                ProcessDate = DateTime.Now,
                BusinessType = (int)DcsAccountPayOutBusinessType.付款,
                SourceId = payOutBill.Id,
                SourceCode = payOutBill.Code,
                Summary = null,
                AfterChangeAmount = dbsupplierAccount1.DueAmount,
                BeforeChangeAmount = dbsupplierAccount1.DueAmount - payOutBill.Amount,
                Debit = payOutBill.Amount
            };
            InsertToDatabase(dbAccountPayableHistoryDetail);
            new AccountPayableHistoryDetailAch(this.DomainService).InsertAccountPayableHistoryDetailValidate(dbAccountPayableHistoryDetail);
            UpdateToDatabase(payOutBill);
            payOutBill.Status = (int)DcsWorkflowOfSimpleApprovalStatus.已审核;
            this.UpdatePayOutBillValidate(payOutBill);
        }

        public void 作废付款单(PayOutBill payOutBill) {
            var dbPayOutBill = ObjectContext.PayOutBills.Where(r => r.Id == payOutBill.Id).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbPayOutBill == null) {
                throw new ValidationException(ErrorStrings.PayOutBill_Validation3);
            }
            if(dbPayOutBill.Status != (int)DcsWorkflowOfSimpleApprovalStatus.新建) {
                throw new ValidationException(ErrorStrings.PayOutBill_Validation4);
            }
            UpdateToDatabase(payOutBill);
            payOutBill.Status = (int)DcsWorkflowOfSimpleApprovalStatus.作废;
            this.UpdatePayOutBillValidate(payOutBill);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:11:15
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 审批付款单(PayOutBill payOutBill) {
            new PayOutBillAch(this).审批付款单(payOutBill);
        }
        
        [Update(UsingCustomMethod = true)]
        public void 作废付款单(PayOutBill payOutBill) {
            new PayOutBillAch(this).作废付款单(payOutBill);
        }
    }
}
