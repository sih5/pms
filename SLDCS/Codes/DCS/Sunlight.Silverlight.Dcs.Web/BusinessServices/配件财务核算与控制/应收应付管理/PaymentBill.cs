﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class PaymentBillAch : DcsSerivceAchieveBase {
        public PaymentBillAch(DcsDomainService domainService)
            : base(domainService) {
        }

        private int index = 0;

        public void 审核来款单(PaymentBill paymentBill) {
            var dbPaymentBill = ObjectContext.PaymentBills.Where(r => r.Id == paymentBill.Id && r.Status == (int)DcsPaymentBillStatus.新建).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbPaymentBill == null)
                throw new ValidationException(ErrorStrings.PaymentBill_Validation3);
            if(paymentBill.AccountGroupId.HasValue) {
                var dbPaymentBeneficiaryLists = ObjectContext.PaymentBeneficiaryLists.Where(r => r.PaymentBillId == paymentBill.Id).ToArray();
                var accountGroupIds = dbPaymentBeneficiaryLists.Select(r => r.AccountGroupId).ToArray();
                var customerCompanyIds = dbPaymentBeneficiaryLists.Select(r => r.CustomerCompanyId).ToArray();
                var dbCustomerAccounts = new CustomerAccountAch(this.DomainService).GetCustomerAccountsWithLock(ObjectContext.CustomerAccounts.Where(r => customerCompanyIds.Contains(r.CustomerCompanyId) && accountGroupIds.Contains(r.AccountGroupId) && r.Status != (int)DcsMasterDataStatus.作废)).ToArray();
                foreach(var paymentBeneficiaryList in dbPaymentBeneficiaryLists) {
                    var customerAccount = dbCustomerAccounts.FirstOrDefault(r => r.CustomerCompanyId == paymentBeneficiaryList.CustomerCompanyId && r.AccountGroupId == paymentBeneficiaryList.AccountGroupId);
                    if(customerAccount == null)
                        throw new ValidationException(ErrorStrings.PaymentBill_Validation2);
                    var userInfo = Utils.GetCurrentUserInfo();
                    var beforeChangeAmount = customerAccount.AccountBalance;
                    customerAccount.AccountBalance += paymentBeneficiaryList.Amount;
                    var afterChangeAmount = customerAccount.AccountBalance;
                    new CustomerAccountAch(this.DomainService).UpdateCustomerAccountValidate(customerAccount);
                    UpdateToDatabase(paymentBeneficiaryList);
                    paymentBeneficiaryList.Status = (int)DcsWorkflowOfSimpleApprovalStatus.已审核;
                    new PaymentBeneficiaryListAch(this.DomainService).UpdatePaymentBeneficiaryListValidate(paymentBeneficiaryList);
                    var customerAccountHisDetail = new CustomerAccountHisDetail();
                    customerAccountHisDetail.CustomerAccountId = customerAccount.Id;
                    customerAccountHisDetail.ChangeAmount = paymentBeneficiaryList.Amount;
                    customerAccountHisDetail.ProcessDate = DateTime.Now.AddSeconds(this.index);
                    customerAccountHisDetail.BusinessType = (int)DcsAccountPaymentBusinessType.来款;
                    customerAccountHisDetail.SourceCode = paymentBeneficiaryList.Code;
                    customerAccountHisDetail.Summary = paymentBeneficiaryList.Summary;
                    customerAccountHisDetail.Credit = paymentBeneficiaryList.Amount;
                    customerAccountHisDetail.BeforeChangeAmount = beforeChangeAmount;
                    customerAccountHisDetail.AfterChangeAmount = afterChangeAmount;
                    customerAccountHisDetail.CreatorName = userInfo.Name;
                    customerAccountHisDetail.SerialType = (int)DcsSerialType.账户金额;
                    paymentBeneficiaryList.CustomerAccountHisDetails.Add(customerAccountHisDetail);
                    DomainService.checkInsertCustomerAccountHisDetail(customerAccountHisDetail);
                    InsertToDatabase(customerAccountHisDetail);
                }
                paymentBill.Status = (int)DcsPaymentBillStatus.已分割;
            } else {
                paymentBill.Status = (int)DcsPaymentBillStatus.已审核;
            }
            UpdateToDatabase(paymentBill);
            this.UpdatePaymentBillValidate(paymentBill);
        }


        public void 审核来款单据(int id, DateTime now) {
            var userInfo = Utils.GetCurrentUserInfo();
            var dbPaymentBill = ObjectContext.PaymentBills.Where(r => r.Id == id && r.Status == (int)DcsPaymentBillStatus.新建).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbPaymentBill == null)
                throw new ValidationException(ErrorStrings.PaymentBill_Validation3);
            if(ObjectContext.Companies.Any(o => o.Id == userInfo.EnterpriseId && o.Type == (int)DcsCompanyType.分公司) && !dbPaymentBill.BankAccountId.HasValue)
                throw new ValidationException(ErrorStrings.PaymentBill_Validation5);
            if(dbPaymentBill.AccountGroupId.HasValue) {
                var dbPaymentBeneficiaryLists = ObjectContext.PaymentBeneficiaryLists.Where(r => r.PaymentBillId == id).ToArray();
                var accountGroupIds = dbPaymentBeneficiaryLists.Select(r => r.AccountGroupId).ToArray();
                var customerCompanyIds = dbPaymentBeneficiaryLists.Select(r => r.CustomerCompanyId).ToArray();
                var dbCustomerAccounts = new CustomerAccountAch(this.DomainService).GetCustomerAccountsWithLock(ObjectContext.CustomerAccounts.Where(r => customerCompanyIds.Contains(r.CustomerCompanyId) && accountGroupIds.Contains(r.AccountGroupId) && r.Status != (int)DcsMasterDataStatus.作废)).ToArray();
                foreach(var paymentBeneficiaryList in dbPaymentBeneficiaryLists) {
                    var customerAccount = dbCustomerAccounts.FirstOrDefault(r => r.CustomerCompanyId == paymentBeneficiaryList.CustomerCompanyId && r.AccountGroupId == paymentBeneficiaryList.AccountGroupId);
                    if(customerAccount == null)
                        throw new ValidationException(ErrorStrings.PaymentBill_Validation2);

                    var beforeChangeAmount = customerAccount.AccountBalance;
                    customerAccount.AccountBalance += paymentBeneficiaryList.Amount;
                    var afterChangeAmount = customerAccount.AccountBalance;
                    new CustomerAccountAch(this.DomainService).UpdateCustomerAccountValidate(customerAccount);
                    UpdateToDatabase(paymentBeneficiaryList);
                    paymentBeneficiaryList.Status = (int)DcsWorkflowOfSimpleApprovalStatus.已审核;
                    new PaymentBeneficiaryListAch(this.DomainService).UpdatePaymentBeneficiaryListValidate(paymentBeneficiaryList);
                    var customerAccountHisDetail = new CustomerAccountHisDetail();
                    customerAccountHisDetail.CustomerAccountId = customerAccount.Id;
                    customerAccountHisDetail.ChangeAmount = paymentBeneficiaryList.Amount;
                    customerAccountHisDetail.ProcessDate = now;
                    customerAccountHisDetail.BusinessType = (int)DcsAccountPaymentBusinessType.来款;
                    customerAccountHisDetail.SourceCode = paymentBeneficiaryList.Code;
                    customerAccountHisDetail.Summary = paymentBeneficiaryList.Summary;
                    customerAccountHisDetail.Credit = paymentBeneficiaryList.Amount;
                    customerAccountHisDetail.BeforeChangeAmount = beforeChangeAmount;
                    customerAccountHisDetail.AfterChangeAmount = afterChangeAmount;
                    customerAccountHisDetail.CreatorName = userInfo.Name;
                    customerAccountHisDetail.InvoiceDate = dbPaymentBill.InvoiceDate;
                    customerAccountHisDetail.SerialType = (int)DcsSerialType.账户金额;
                    paymentBeneficiaryList.CustomerAccountHisDetails.Add(customerAccountHisDetail);
                    DomainService.checkInsertCustomerAccountHisDetail(customerAccountHisDetail);
                    InsertToDatabase(customerAccountHisDetail);
                }
                dbPaymentBill.Status = (int)DcsPaymentBillStatus.已分割;
            } else {
                dbPaymentBill.Status = (int)DcsPaymentBillStatus.已审核;
            }
            UpdateToDatabase(dbPaymentBill);
            this.UpdatePaymentBillValidate(dbPaymentBill);
            this.ObjectContext.SaveChanges();
        }

        public void 作废来款单据(int id) {
            var dbPaymentBill = ObjectContext.PaymentBills.Where(r => r.Id == id && (r.Status == (int)DcsPaymentBillStatus.新建 || r.Status == (int)DcsPaymentBillStatus.已审核)).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbPaymentBill == null)
                throw new ValidationException(ErrorStrings.PaymentBill_Validation4);
            dbPaymentBill.Status = (int)DcsPaymentBillStatus.作废;
            UpdateToDatabase(dbPaymentBill);
            this.UpdatePaymentBillValidate(dbPaymentBill);
            this.ObjectContext.SaveChanges();
        }


        public void 分割来款单(PaymentBill paymentBill) {
            var dbPaymentBill = ObjectContext.PaymentBills.Where(r => r.Id == paymentBill.Id && r.Status == (int)DcsPaymentBillStatus.已审核).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbPaymentBill == null)
                throw new ValidationException(ErrorStrings.PaymentBill_Validation1);
            var dbPaymentBeneficiaryLists = paymentBill.PaymentBeneficiaryLists.ToArray();
            var accountGroupIds = dbPaymentBeneficiaryLists.Select(r => r.AccountGroupId).ToArray();
            var customerCompanyIds = dbPaymentBeneficiaryLists.Select(r => r.CustomerCompanyId).ToArray();
            var dbCustomerAccounts = new CustomerAccountAch(this.DomainService).GetCustomerAccountsWithLock(ObjectContext.CustomerAccounts.Where(r => customerCompanyIds.Contains(r.CustomerCompanyId) && accountGroupIds.Contains(r.AccountGroupId) && r.Status != (int)DcsMasterDataStatus.作废)).ToArray();
            foreach(var paymentBeneficiaryList in dbPaymentBeneficiaryLists) {
                var customerAccount = dbCustomerAccounts.FirstOrDefault(r => r.CustomerCompanyId == paymentBeneficiaryList.CustomerCompanyId && r.AccountGroupId == paymentBeneficiaryList.AccountGroupId);
                if(customerAccount == null)
                    throw new ValidationException(ErrorStrings.PaymentBill_Validation2);
                var userInfo = Utils.GetCurrentUserInfo();
                customerAccount.AccountBalance += paymentBeneficiaryList.Amount;
                new CustomerAccountAch(this.DomainService).UpdateCustomerAccountValidate(customerAccount);
                InsertToDatabase(paymentBeneficiaryList);
                paymentBeneficiaryList.Status = (int)DcsWorkflowOfSimpleApprovalStatus.已审核;
                new PaymentBeneficiaryListAch(this.DomainService).InsertPaymentBeneficiaryListValidate(paymentBeneficiaryList);
                var customerAccountHisDetail = new CustomerAccountHisDetail();
                customerAccountHisDetail.CustomerAccountId = customerAccount.Id;
                customerAccountHisDetail.ChangeAmount = paymentBeneficiaryList.Amount;
                customerAccountHisDetail.ProcessDate = DateTime.Now;
                customerAccountHisDetail.BusinessType = (int)DcsAccountPaymentBusinessType.来款;
                customerAccountHisDetail.SourceCode = paymentBeneficiaryList.Code;
                customerAccountHisDetail.Summary = paymentBeneficiaryList.Summary;
                customerAccountHisDetail.AfterChangeAmount = customerAccount.AccountBalance;
                customerAccountHisDetail.BeforeChangeAmount = customerAccount.AccountBalance - paymentBeneficiaryList.Amount;
                customerAccountHisDetail.Credit = paymentBeneficiaryList.Amount;
                customerAccountHisDetail.CreatorName = userInfo.Name;
                customerAccountHisDetail.SerialType = (int)DcsSerialType.账户金额;
                paymentBeneficiaryList.CustomerAccountHisDetails.Add(customerAccountHisDetail);
                DomainService.checkInsertCustomerAccountHisDetail(customerAccountHisDetail);
                InsertToDatabase(customerAccountHisDetail);
            }
            UpdateToDatabase(paymentBill);
            paymentBill.Status = (int)DcsPaymentBillStatus.已分割;
            this.UpdatePaymentBillValidate(paymentBill);
        }
        public void 批量审核来款单据(int[] ids) {
            this.index = 0;
            var now = DateTime.Now;
            foreach(var id in ids) {
                this.审核来款单据(id, now.AddSeconds(this.index));
                this.index++;
            }
        }
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:11:15
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 审核来款单(PaymentBill paymentBill) {
            new PaymentBillAch(this).审核来款单(paymentBill);
        }


        [Invoke]
        public void 审核来款单据(int id, DateTime now) {
            new PaymentBillAch(this).审核来款单据(id, now);
        }

        [Invoke]
        public void 作废来款单据(int id) {
            new PaymentBillAch(this).作废来款单据(id);
        }


        [Update(UsingCustomMethod = true)]
        public void 分割来款单(PaymentBill paymentBill) {
            new PaymentBillAch(this).分割来款单(paymentBill);
        }
        [Invoke]
        public void 批量审核来款单据(int[] ids) {
            new PaymentBillAch(this).批量审核来款单据(ids);
        }
    }
}
