﻿using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using System.Transactions;using Sunlight.Silverlight.Dcs.Web.Resources;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class SAPInvoiceInfoAch : DcsSerivceAchieveBase {
        public SAPInvoiceInfoAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public int 手工设置发票状态(SAPInvoiceInfo[] entities) {
            if(entities == null || entities.Length == 0) {
                return 0;
            }
            var i = entities.Length;
            //var groups = entities.GroupBy(r => r.BusinessType);
            //foreach(var tempgroup in groups) {
            //    var ids = tempgroup.Select(r => r.Id).ToArray();
            //    var key = (int)tempgroup.Key;
            //    switch(key) {
            //        case (int)DcsSAPInvoiceType.采购发票:
            //            设置采购发票状态(ids);
            //            break;
            //        case (int)DcsSAPInvoiceType.销售发票:
            //            设置销售发票状态(ids);
            //            break;
            //        case (int)DcsSAPInvoiceType.采购退货发票:
            //            设置销售发票状态(ids);
            //            break;
            //    }
            //}
            //ObjectContext.SaveChanges();
            SetSapInvoiceStatus(entities, "YX");
            return i;
        }

        public void SetSapInvoiceStatus(SAPInvoiceInfo[] infoes, string sysCode) {
            if(string.IsNullOrWhiteSpace(sysCode)) {
                throw new ValidationException(ErrorStrings.SAPInvoiceInfo_Validation1);
            }
            if(sysCode != "YX" && sysCode != "FD") {
                throw new ValidationException(ErrorStrings.SAPInvoiceInfo_Validation2);
            }
            var groups = infoes.GroupBy(r => r.BusinessType);
            using(TransactionScope scope = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions {
                IsolationLevel = IsolationLevel.Serializable
            })) {
                foreach(var tempGroup in groups) {
                    var tempBussinessType = tempGroup.Key;
                    var ids = tempGroup.Select(r => r.Id).ToArray();
                    var sqlOfQuery = CreateQuerySqlOfSapInvoice(tempBussinessType, sysCode, ids);
                    var dbEntities = ObjectContext.ExecuteStoreQuery<SAPInvoiceInfo>(sqlOfQuery);
                    var entities = dbEntities.ToArray();
                    if(entities.Length == 0) {
                        throw new ValidationException(ErrorStrings.SAPInvoiceInfo_Validation3);
                    }
                    if(entities.Any(r => r.Status == (int)DcsPMSHandleStatus.待处理 || r.Status == (int)DcsPMSHandleStatus.处理成功)) {
                        throw new ValidationException(ErrorStrings.SAPInvoiceInfo_Validation4);
                    }
                    var sqlOfUpdate = CreateUpdateSqlOfSapInvoice(tempBussinessType, sysCode, ids);
                    ObjectContext.ExecuteStoreCommand(sqlOfUpdate);
                }
                scope.Complete();
            }
        }
        internal string CreateQuerySqlOfSapInvoice(int bussinessType, string sysCode, int[] ids) {
            if(string.IsNullOrWhiteSpace(sysCode)) {
                throw new ValidationException(ErrorStrings.SAPInvoiceInfo_Validation1);
            }
            if(sysCode != "YX") {
                throw new ValidationException(ErrorStrings.SAPInvoiceInfo_Validation2);
            }
            var stringOfIds = string.Join(",", ids);
            var stringOfBussinessType = bussinessType.ToString();
            string stringOfTableName;
            string sql;
            switch(bussinessType) {
                case 1: //采购发票（这里指回传的采购发票）
                    stringOfTableName = string.Format("SAP_{0}_PURINVOICEPASSBACK", sysCode);
                    sql = string.Format(@"SELECT ID, BUKRS AS COMPANYCODE, {0}AS BUSINESSTYPE, JSDNUM AS CODE, '0' AS INVOICENUMBER, 0 AS INVOICEAMOUNT, 0 AS INVOICETAX, RECEIVETIME AS CREATETIME, HANDLESTATUS AS STATUS,HANDLEMESSAGE AS MESSAGE FROM {1} WHERE ID IN({2}) FOR UPDATE", stringOfBussinessType, stringOfTableName, stringOfIds);
                    break;
                case 2: //销售发票(这里指回传的销售和销售退货的发票）
                    stringOfTableName = string.Format("SAP_{0}_SALESBILLINFO", sysCode);
                    sql = string.Format(@"SELECT ID, BUKRS AS COMPANYCODE, {0}AS BUSINESSTYPE, CODE, INVOICENUMBER, INVOICEAMOUNT,INVOICETAX, CREATETIME, STATUS,MESSAGE FROM {1} WHERE ID IN({2}) FOR UPDATE", stringOfBussinessType, stringOfTableName, stringOfIds);
                    break;
                case 3: //采购退货发票(这里指回传的采购退货发票）
                    stringOfTableName = string.Format("SAP_{0}_INVOICEVERACCOUNTINFO", sysCode);
                    sql = string.Format(@"SELECT ID, BUKRS AS COMPANYCODE, {0}AS BUSINESSTYPE, CODE, INVOICENUMBER, INVOICEAMOUNT,INVOICETAX, CREATETIME, STATUS,MESSAGE FROM {1} WHERE ID IN({2}) FOR UPDATE", stringOfBussinessType, stringOfTableName, stringOfIds);
                    break;
                default:
                    throw new ValidationException(ErrorStrings.SAPInvoiceInfo_Validation5);
            }

            return sql;
        }
        internal string CreateUpdateSqlOfSapInvoice(int bussinessType, string sysCode, int[] ids) {
            if(string.IsNullOrWhiteSpace(sysCode)) {
                throw new ValidationException(ErrorStrings.SAPInvoiceInfo_Validation1);
            }
            if(sysCode != "YX") {
                throw new ValidationException(ErrorStrings.SAPInvoiceInfo_Validation2);
            }
            var stringOfIds = string.Join(",", ids);
            var stringOfBussinessType = bussinessType.ToString();
            string stringOfTableName;
            string sql;
            switch(bussinessType) {
                case 1://采购发票
                    stringOfTableName = string.Format("SAP_{0}_PURINVOICEPASSBACK", sysCode);
                    sql = string.Format(@"UPDATE {0} SET HANDLESTATUS=1,HANDLETIME=SYSDATE,HANDLEMESSAGE='' WHERE ID IN({1})", stringOfTableName, stringOfIds);
                    break;
                case 2://销售发票
                    stringOfTableName = string.Format("SAP_{0}_SALESBILLINFO", sysCode);
                    sql = string.Format(@"UPDATE {0} SET STATUS=1,MODIFYTIME=SYSDATE,MESSAGE='' WHERE ID IN({1})", stringOfTableName, stringOfIds);
                    break;
                case 3://回传的采购发票
                    stringOfTableName = string.Format("SAP_{0}_INVOICEVERACCOUNTINFO", sysCode);
                    sql = string.Format(@"UPDATE {0} SET STATUS=1,MODIFYTIME=SYSDATE,MESSAGE='' WHERE ID IN({1})", stringOfTableName, stringOfIds);
                    break;
                default:
                    throw new ValidationException(ErrorStrings.SAPInvoiceInfo_Validation5);
            }
            return sql;
        }

    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:11:15
        //原分布类的函数全部转移到Ach                                                               

        [Invoke]
        public int 手工设置发票状态(SAPInvoiceInfo[] entities) {
            return new SAPInvoiceInfoAch(this).手工设置发票状态(entities);
        }

        public void SetSapInvoiceStatus(SAPInvoiceInfo[] infoes, string sysCode) {
            new SAPInvoiceInfoAch(this).SetSapInvoiceStatus(infoes,sysCode);
        }
    }
}
