﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class SIHCreditInfoAch : DcsSerivceAchieveBase {
        public SIHCreditInfoAch(DcsDomainService domainService)
            : base(domainService) {
        }

        public void 作废SIH授信服务商申请单(SIHCreditInfo input) {
            if(ObjectContext.SIHCreditInfoes.Any(r => r.Id == input.Id && r.Status != (int)DCSSIHCreditInfoStatus.新增 && r.Status != (int)DCSSIHCreditInfoStatus.提交))
                throw new ValidationException(ErrorStrings.SIHCreditInfo_Validation4);

            var user = Utils.GetCurrentUserInfo();
            input.Status = (int)DCSSIHCreditInfoStatus.作废;
            input.AbandonerId = user.Id;
            input.AbandonerName = user.Name;
            input.AbandonTime = DateTime.Now;
            UpdateToDatabase(input);
            this.UpdateSIHCreditInfoValidate(input);
            this.ObjectContext.SaveChanges();
        }

        public void 批量提交SIH授信服务商申请单(int[] ids) {
            var sihCreditInfos = ObjectContext.SIHCreditInfoes.Where(r => ids.Contains(r.Id)).ToList();
            var errorMessage = "";
            foreach(var sihCreditInfo in sihCreditInfos) {
                if(sihCreditInfo.Status != (int)DCSSIHCreditInfoStatus.新增) {
                    errorMessage = errorMessage + sihCreditInfo.Code + "、";
                } else {
                    sihCreditInfo.Status = (int)DCSSIHCreditInfoStatus.提交;
                    UpdateToDatabase(sihCreditInfo);
                    this.UpdateSIHCreditInfoValidate(sihCreditInfo);
                }
            }
            if(!"".Equals(errorMessage)) {
                throw new ValidationException(errorMessage + "单据不是新增状态，不允许提交");
            }
            ObjectContext.SaveChanges();
        }

        public void 批量审核SIH授信服务商申请单(int[] ids, int status, bool isApprover) {
            var sihCreditInfos = ObjectContext.SIHCreditInfoes.Where(r => ids.Contains(r.Id)).ToList();
            var user = Utils.GetCurrentUserInfo();
            var marketDptPersonnelRelation = ObjectContext.MarketDptPersonnelRelations.FirstOrDefault(o => o.PersonnelId == user.Id && o.Status == (int)DcsBaseDataStatus.有效);
            switch(status) {
                case (int)DCSSIHCreditInfoStatus.提交:
                    if(sihCreditInfos.Any(o => o.Status != (int)DCSSIHCreditInfoStatus.提交))
                        throw new ValidationException(string.Format("{0}失败，存在状态不等于提交的单据(请重新查询后，在{0})",
                            marketDptPersonnelRelation == null ? "初审" : "审核"));
                    sihCreditInfos.ForEach(o => {
                        if(isApprover) {
                            if(marketDptPersonnelRelation != null && o.Type == (int)DCSSIHCreditInfoType.配件中心) {
                                o.Status = (int)DCSSIHCreditInfoStatus.初审通过;
                                o.InitialApproverId = user.Id;
                                o.InitialApproverName = user.Name;
                                o.InitialApproveTime = DateTime.Now;
                            } else if(marketDptPersonnelRelation != null && o.Type == (int)DCSSIHCreditInfoType.分销中心) {
                                o.Status = (int)DCSSIHCreditInfoStatus.审核通过;
                                o.CheckerId = user.Id;
                                o.CheckerName = user.Name;
                                o.CheckTime = DateTime.Now;
                            } else if(marketDptPersonnelRelation == null ) {
                                throw new ValidationException("审核失败，不存在分销中心与人员关系，请先维护");
                            }
                        } else
                            驳回初始化审核信息(o, user);
                    });
                    break;
                case (int)DCSSIHCreditInfoStatus.初审通过:
                    if(sihCreditInfos.Any(o => o.Status != (int)DCSSIHCreditInfoStatus.初审通过))
                        throw new ValidationException("审核失败，存在状态不等于初审通过的单据(请重新查询后，在审核)");
                    sihCreditInfos.ForEach(o => {
                        if(isApprover) {
                            o.Status = (int)DCSSIHCreditInfoStatus.审核通过;
                            o.CheckerId = user.Id;
                            o.CheckerName = user.Name;
                            o.CheckTime = DateTime.Now;
                        } else
                            驳回初始化审核信息(o, user);
                    });
                    break;
                case (int)DCSSIHCreditInfoStatus.审核通过:
                    if(sihCreditInfos.Any(o => o.Status != (int)DCSSIHCreditInfoStatus.审核通过))
                        throw new ValidationException("审批失败，存在状态不等于审核通过的单据(请重新查询后，在审批)");
                    sihCreditInfos.ForEach(o => {
                        if(isApprover) {
                            if(marketDptPersonnelRelation == null && o.Type == (int)DCSSIHCreditInfoType.配件中心) {
                                o.Status = (int)DCSSIHCreditInfoStatus.审批通过;
                                o.ApproverId = user.Id;
                                o.ApproverName = user.Name;
                                o.ApproveTime = DateTime.Now;
                            } else if(marketDptPersonnelRelation != null && o.Type == (int)DCSSIHCreditInfoType.分销中心) {
                                o.Status = (int)DCSSIHCreditInfoStatus.生效;
                                o.UpperApproverId = user.Id;
                                o.UpperApproverName = user.Name;
                                o.UpperCheckTime = DateTime.Now;
                                高级审核SIH授信服务商申请单(o, true);
                            } else if(marketDptPersonnelRelation == null && o.Type == (int)DCSSIHCreditInfoType.分销中心) {
                                throw new ValidationException("审批失败，不存在分销中心与人员关系，请先维护");
                            }
                        } else
                            驳回初始化审核信息(o, user);
                    });
                    break;
                case (int)DCSSIHCreditInfoStatus.审批通过:
                    if(sihCreditInfos.Any(o => o.Status != (int)DCSSIHCreditInfoStatus.审批通过))
                        throw new ValidationException("高级审核失败，存在状态不等于审批通过的单据(请重新查询后，在高级审核)");
                    sihCreditInfos.ForEach(o => {
                        if(isApprover) {
                            o.Status = (int)DCSSIHCreditInfoStatus.生效;
                            o.UpperApproverId = user.Id;
                            o.UpperApproverName = user.Name;
                            o.UpperCheckTime = DateTime.Now;
                            高级审核SIH授信服务商申请单(o, false);
                        } else
                            驳回初始化审核信息(o, user);
                    });
                    break;
                default:
                    throw new ValidationException("单据状态错误，请重新查询数据。");
            }
            ObjectContext.SaveChanges();
        }

        private void 高级审核SIH授信服务商申请单(SIHCreditInfo input, bool isDistribution) {
            var user = Utils.GetCurrentUserInfo();
            var details = input.SIHCreditInfoDetails.Where(o => o.ValidationTime.Value.Date <= DateTime.Now.Date);
            var companyIds = details.Select(o => o.CompanyId).ToList();
            var dealerIds = details.Select(o => o.DealerId).ToList();
            var customerAccounts = ObjectContext.CustomerAccounts.Where(r => r.Status == (int)DcsMasterDataStatus.有效
                && (dealerIds.Contains(r.CustomerCompanyId) || companyIds.Contains(r.CustomerCompanyId))).ToList();
            var sihCreditInfoDetails = ObjectContext.SIHCreditInfoDetails.Where(o => dealerIds.Contains(o.DealerId)).ToList();
            var accountGroup = ObjectContext.AccountGroups.FirstOrDefault(o => o.SalesCompanyId == user.EnterpriseId
                && o.Status == (int)DcsMasterDataStatus.有效);
            foreach(var item in details) {
                var customerAccount = customerAccounts.FirstOrDefault(o => o.CustomerCompanyId == item.DealerId && o.AccountGroupId == item.AccountGroupId);
                if(customerAccount == null)
                    throw new ValidationException("未找到对应的客户账户，企业编号" + item.DealerCode + "账户组编号" + item.AccountGroupCode);
                if(isDistribution) {
                    if((customerAccount.SIHCreditZB ?? 0) < (item.ASIHCredit ?? 0))
                        throw new ValidationException("本次SIH授信额度（服务商）不能大于配件公司授信的额度");
                } else {
                    customerAccount.SIHCreditZB = item.ASIHCredit;
                    customerAccount.ValidationTime = item.ValidationTime;
                    customerAccount.ExpireTime = item.ExpireTime;
                }
                    
                customerAccount.SIHCredit = item.ASIHCredit;

                new CustomerAccountAch(this.DomainService).UpdateCustomerAccountValidate(customerAccount);

                sihCreditInfoDetails.Where(o => o.DealerId == item.DealerId && o.AccountGroupId == item.AccountGroupId
                     && o.Status == (int)DCSSIHCreditInfoDetailStatus.生效 && o.Id != item.Id).ToList()
                     .ForEach(o => {
                         o.Status = (int)DCSSIHCreditInfoDetailStatus.已失效;
                     });

                item.Status = (int)DCSSIHCreditInfoDetailStatus.生效;

                var customerAccountCompany = customerAccounts.FirstOrDefault(o => o.CustomerCompanyId == item.CompanyId && o.AccountGroupId == accountGroup.Id);
                if(customerAccountCompany == null)
                    throw new ValidationException("未找到对应的客户账户，企业编号" + item.CompanyCode + "账户组编号" + item.AccountGroupCode);
                customerAccountCompany.SIHCredit = (customerAccountCompany.SIHCredit ?? 0) - item.BSIHCredit + item.ASIHCredit;
                new CustomerAccountAch(this.DomainService).UpdateCustomerAccountValidate(customerAccountCompany);
            }
            ObjectContext.SaveChanges();
        }

        private void 驳回初始化审核信息(SIHCreditInfo input, Sunlight.Silverlight.Web.Utils.UserInfo user) {
            input.RejecterId = user.Id;
            input.RejecterName = user.Name;
            input.RejectTime = DateTime.Now;
            input.UpperApproverId = null;
            input.UpperApproverName = null;
            input.UpperCheckTime = null;
            input.InitialApproverId = null;
            input.InitialApproverName = null;
            input.InitialApproveTime = null;
            input.CheckerId = null;
            input.CheckerName = null;
            input.CheckTime = null;
            input.ApproverId = null;
            input.ApproverName = null;
            input.ApproveTime = null;
            input.UpperApproverId = null;
            input.UpperApproverName = null;
            input.UpperCheckTime = null;
            input.Status = (int)DCSSIHCreditInfoStatus.新增;
        }
    }

    partial class DcsDomainService {
        [Update(UsingCustomMethod = true)]
        public void 作废SIH授信服务商申请单(SIHCreditInfo input) {
            new SIHCreditInfoAch(this).作废SIH授信服务商申请单(input);
        }

        [Invoke(HasSideEffects = true)]
        public void 批量提交SIH授信服务商申请单(int[] ids) {
            new SIHCreditInfoAch(this).批量提交SIH授信服务商申请单(ids);
        }

        [Invoke(HasSideEffects = true)]
        public void 批量审核SIH授信服务商申请单(int[] ids, int status, bool isApprover) {
            new SIHCreditInfoAch(this).批量审核SIH授信服务商申请单(ids, status, isApprover);
        }
    }
}
