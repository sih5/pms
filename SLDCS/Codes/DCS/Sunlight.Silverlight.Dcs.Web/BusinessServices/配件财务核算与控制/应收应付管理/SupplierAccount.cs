﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using Sunlight.Silverlight.Dcs.Web.Resources;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class SupplierAccountAch : DcsSerivceAchieveBase {
        public SupplierAccountAch(DcsDomainService domainService)
            : base(domainService) {
        }

        /// <summary>
        /// 仅供服务端使用
        /// </summary>
        internal void 开户(int buyerCompanyId, int supplierCompanyId, decimal initAmount) {
            var supplierAccount = new SupplierAccount {
                BuyerCompanyId = buyerCompanyId,
                SupplierCompanyId = supplierCompanyId,
                DueAmount = initAmount,
                EstimatedDueAmount = 0,
                PaymentLimit = 0,
                Status = (int)DcsMasterDataStatus.有效
            };
            InsertToDatabase(supplierAccount);
            this.InsertSupplierAccountValidate(supplierAccount);
        }
        /// <summary>
        /// 仅供服务端使用
        /// </summary>
        internal void 付款过账(int buyerCompanyId, int supplierCompanyId, decimal amount, int payOutBillId, string payOutBillCode) {
            var dbpayOutBill = ObjectContext.PayOutBills.SingleOrDefault(r => r.Id == payOutBillId);
            if(dbpayOutBill == null) {
                throw new ValidationException(ErrorStrings.SupplierAccount_Validation3);
            }
            var dbSupplierAccount = ObjectContext.SupplierAccounts.FirstOrDefault(r => r.BuyerCompanyId == buyerCompanyId && r.SupplierCompanyId == supplierCompanyId && r.Status == (int)DcsMasterDataStatus.有效 && r.PartsSalesCategoryId == dbpayOutBill.PartsSalesCategoryId);
            if(dbSupplierAccount == null)
                throw new ValidationException(ErrorStrings.SupplierAccount_Validation2);
            dbSupplierAccount.DueAmount -= amount;
            this.UpdateSupplierAccountValidate(dbSupplierAccount);
            var accountPayableHistoryDetail = new AccountPayableHistoryDetail {
                SupplierAccountId = dbSupplierAccount.Id,
                ChangeAmount = amount,
                ProcessDate = System.DateTime.Now,
                BusinessType = (int)DcsAccountPayOutBusinessType.付款,
                SourceId = payOutBillId,
                SourceCode = payOutBillCode,
                AfterChangeAmount = dbSupplierAccount.DueAmount,
                BeforeChangeAmount = dbSupplierAccount.DueAmount + amount,
                Debit = amount
            };
            InsertToDatabase(accountPayableHistoryDetail);
            new AccountPayableHistoryDetailAch(this.DomainService).InsertAccountPayableHistoryDetailValidate(accountPayableHistoryDetail);
        }
        /// <summary>
        /// 仅供服务端使用
        /// </summary>
        internal void 采购退货结算单过账(PartsPurchaseRtnSettleBill partsPurchaseRtnSettleBill) {
            if(partsPurchaseRtnSettleBill.Status != (int)DcsPartsPurchaseSettleStatus.已审批)
                CheckEntityState(partsPurchaseRtnSettleBill);
            var dbSupplierAccount = ObjectContext.SupplierAccounts.SingleOrDefault(r => r.BuyerCompanyId == partsPurchaseRtnSettleBill.BranchId && r.SupplierCompanyId == partsPurchaseRtnSettleBill.PartsSupplierId && r.Status == (int)DcsMasterDataStatus.有效 && r.PartsSalesCategoryId == partsPurchaseRtnSettleBill.PartsSalesCategoryId);
            if(dbSupplierAccount == null)
                throw new ValidationException(ErrorStrings.SupplierAccount_Validation2);
            dbSupplierAccount.DueAmount -= partsPurchaseRtnSettleBill.TotalSettlementAmount - partsPurchaseRtnSettleBill.InvoiceAmountDifference;
            this.UpdateSupplierAccountValidate(dbSupplierAccount);
            var accountPayableHistoryDetail = new AccountPayableHistoryDetail {
                SupplierAccountId = dbSupplierAccount.Id,
                ChangeAmount = partsPurchaseRtnSettleBill.TotalSettlementAmount - partsPurchaseRtnSettleBill.InvoiceAmountDifference,
                ProcessDate = System.DateTime.Now,
                BusinessType = (int)DcsAccountPayOutBusinessType.采购退货结算,
                SourceId = partsPurchaseRtnSettleBill.Id,
                SourceCode = partsPurchaseRtnSettleBill.Code,
                AfterChangeAmount = dbSupplierAccount.DueAmount,
                BeforeChangeAmount = dbSupplierAccount.DueAmount + partsPurchaseRtnSettleBill.TotalSettlementAmount - partsPurchaseRtnSettleBill.InvoiceAmountDifference,
                Credit = partsPurchaseRtnSettleBill.TotalSettlementAmount - partsPurchaseRtnSettleBill.InvoiceAmountDifference
            };
            if(partsPurchaseRtnSettleBill.EntityState == EntityState.Added)
                partsPurchaseRtnSettleBill.AccountPayableHistoryDetails.Add(accountPayableHistoryDetail);
            InsertToDatabase(accountPayableHistoryDetail);
            new AccountPayableHistoryDetailAch(this.DomainService).InsertAccountPayableHistoryDetailValidate(accountPayableHistoryDetail);
        }
        /// <summary>
        /// 仅供服务端使用
        /// </summary>
        internal void 反结算采购退货结算单过账(PartsPurchaseRtnSettleBill partsPurchaseRtnSettleBill) {
            var invoiceInformations = ObjectContext.InvoiceInformations.Where(r => r.SourceId == partsPurchaseRtnSettleBill.OffsettedSettlementBillId && r.SourceType == (int)DcsInvoiceInformationSourceType.配件采购退货结算单 && r.Status != (int)DcsInvoiceInformationStatus.作废).ToArray();
            foreach(var invoiceInformation in invoiceInformations) {
                invoiceInformation.Status = (int)DcsInvoiceInformationStatus.作废;
                new InvoiceInformationAch(this.DomainService).UpdateInvoiceInformationValidate(invoiceInformation);
            }
            if(!invoiceInformations.Any())
                return;
            decimal invoiceAmountAll = Convert.ToDecimal(invoiceInformations.Sum(v => v.InvoiceAmount));
            if(partsPurchaseRtnSettleBill.Status != (int)DcsPartsPurchaseSettleStatus.已审批)
                CheckEntityState(partsPurchaseRtnSettleBill);
            var dbSupplierAccount = ObjectContext.SupplierAccounts.SingleOrDefault(r => r.BuyerCompanyId == partsPurchaseRtnSettleBill.BranchId && r.SupplierCompanyId == partsPurchaseRtnSettleBill.PartsSupplierId && r.Status == (int)DcsMasterDataStatus.有效 && r.PartsSalesCategoryId == partsPurchaseRtnSettleBill.PartsSalesCategoryId);
            if(dbSupplierAccount == null)
                throw new ValidationException(ErrorStrings.SupplierAccount_Validation2);
            dbSupplierAccount.DueAmount += invoiceAmountAll;
            this.UpdateSupplierAccountValidate(dbSupplierAccount);
            var accountPayableHistoryDetail = new AccountPayableHistoryDetail {
                SupplierAccountId = dbSupplierAccount.Id,
                ChangeAmount = invoiceAmountAll,
                ProcessDate = System.DateTime.Now,
                BusinessType = (int)DcsAccountPayOutBusinessType.采购退货结算,
                SourceId = partsPurchaseRtnSettleBill.Id,
                SourceCode = partsPurchaseRtnSettleBill.Code,
                AfterChangeAmount = dbSupplierAccount.DueAmount,
                BeforeChangeAmount = dbSupplierAccount.DueAmount - invoiceAmountAll,
                Credit = invoiceAmountAll
            };
            if(partsPurchaseRtnSettleBill.EntityState == EntityState.Added)
                partsPurchaseRtnSettleBill.AccountPayableHistoryDetails.Add(accountPayableHistoryDetail);
            InsertToDatabase(accountPayableHistoryDetail);
            new AccountPayableHistoryDetailAch(this.DomainService).InsertAccountPayableHistoryDetailValidate(accountPayableHistoryDetail);
        }
        /// <summary>
        /// 仅供服务端使用
        /// </summary>
        internal void 采购结算单过账(PartsPurchaseSettleBill partsPurchaseSettleBill) {
            CheckEntityState(partsPurchaseSettleBill);
            var dbSupplierAccount = ObjectContext.SupplierAccounts.SingleOrDefault(r => r.BuyerCompanyId == partsPurchaseSettleBill.BranchId && r.SupplierCompanyId == partsPurchaseSettleBill.PartsSupplierId && r.Status == (int)DcsMasterDataStatus.有效 && r.PartsSalesCategoryId == partsPurchaseSettleBill.PartsSalesCategoryId);
            if(dbSupplierAccount == null)
                throw new ValidationException(ErrorStrings.SupplierAccount_Validation2);
            dbSupplierAccount.DueAmount += partsPurchaseSettleBill.TotalSettlementAmount - partsPurchaseSettleBill.InvoiceAmountDifference;
            this.UpdateSupplierAccountValidate(dbSupplierAccount);
            var accountPayableHistoryDetail = new AccountPayableHistoryDetail {
                SupplierAccountId = dbSupplierAccount.Id,
                ChangeAmount = partsPurchaseSettleBill.TotalSettlementAmount - partsPurchaseSettleBill.InvoiceAmountDifference,
                ProcessDate = System.DateTime.Now,
                BusinessType = (int)DcsAccountPayOutBusinessType.采购结算,
                SourceId = partsPurchaseSettleBill.Id,
                SourceCode = partsPurchaseSettleBill.Code,
                AfterChangeAmount = dbSupplierAccount.DueAmount,
                BeforeChangeAmount = dbSupplierAccount.DueAmount - (partsPurchaseSettleBill.TotalSettlementAmount - partsPurchaseSettleBill.InvoiceAmountDifference),
                Debit = partsPurchaseSettleBill.TotalSettlementAmount - partsPurchaseSettleBill.InvoiceAmountDifference
            };
            if(partsPurchaseSettleBill.EntityState == EntityState.Added)
                partsPurchaseSettleBill.AccountPayableHistoryDetails.Add(accountPayableHistoryDetail);
            InsertToDatabase(accountPayableHistoryDetail);
            new AccountPayableHistoryDetailAch(this.DomainService).InsertAccountPayableHistoryDetailValidate(accountPayableHistoryDetail);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:11:15
        //原分布类的函数全部转移到Ach                                                               
    //使用工具生成后没有任何共有方法 NoPublic
    }
}
