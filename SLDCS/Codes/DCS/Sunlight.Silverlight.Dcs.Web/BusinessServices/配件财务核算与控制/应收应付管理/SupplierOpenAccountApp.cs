﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class SupplierOpenAccountAppAch : DcsSerivceAchieveBase {
        public SupplierOpenAccountAppAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 审核供应商开户申请单(SupplierOpenAccountApp supplierOpenAccountApp) {
            var supplierOpenAccountApp1 = ObjectContext.SupplierOpenAccountApps.Where(r => r.Id == supplierOpenAccountApp.Id).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(supplierOpenAccountApp1.Status != (int)DcsWorkflowOfSimpleApprovalStatus.新建) {
                throw new ValidationException(ErrorStrings.SupplierOpenAccountApp_Validation5);
            }
            var partsSalesCategory = ObjectContext.PartsSalesCategories.Where(r => r.Id == supplierOpenAccountApp1.PartsSalesCategoryId).SingleOrDefault();
            var dbSupplierAccount = ObjectContext.SupplierAccounts.Where(r => r.BuyerCompanyId == supplierOpenAccountApp.BuyerCompanyId && r.PartsSalesCategoryId == supplierOpenAccountApp.PartsSalesCategoryId && r.SupplierCompanyId == supplierOpenAccountApp.SupplierCompanyId).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbSupplierAccount == null) {
                var supplierAccount = new SupplierAccount {
                    BuyerCompanyId = supplierOpenAccountApp.BuyerCompanyId,
                    PartsSalesCategoryId = supplierOpenAccountApp.PartsSalesCategoryId,
                    PartsSalesCategoryName = partsSalesCategory.Name,
                    SupplierCompanyId = supplierOpenAccountApp.SupplierCompanyId,
                    DueAmount = supplierOpenAccountApp.AccountInitialDeposit,
                    EstimatedDueAmount = 0,
                    PaymentLimit = 0,
                    Status = (int)DcsMasterDataStatus.有效
                };
                InsertToDatabase(supplierAccount);
                new SupplierAccountAch(this.DomainService).InsertSupplierAccountValidate(supplierAccount);
                //var userInfo = Utils.GetCurrentUserInfo();
                //var customerAccountHisDetail = new CustomerAccountHisDetail {
                //    ChangeAmount = supplierOpenAccountApp.AccountInitialDeposit,
                //    ProcessDate = DateTime.Now,
                //    BusinessType = (int)DcsAccountPaymentBusinessType.开户,
                //    SourceId = supplierOpenAccountApp.Id,
                //    SourceCode = supplierOpenAccountApp.Code,
                //    Summary = supplierOpenAccountApp.Remark,
                //    AfterChangeAmount = supplierOpenAccountApp.AccountInitialDeposit,
                //    BeforeChangeAmount = 0,
                //    Credit = supplierOpenAccountApp.AccountInitialDeposit,
                //    CreatorName = userInfo.Name
                //};
                //supplierAccount.CustomerAccountHisDetails.Add(customerAccountHisDetail);
                //checkInsertCustomerAccountHisDetail(customerAccountHisDetail);
                //InsertToDatabase(customerAccountHisDetail);
                var accountPayableHistoryDetail = new AccountPayableHistoryDetail {
                    ChangeAmount = supplierOpenAccountApp.AccountInitialDeposit,
                    ProcessDate = DateTime.Now,
                    BusinessType = (int)DcsAccountPaymentBusinessType.开户,
                    SourceId = supplierOpenAccountApp.Id,
                    SourceCode = supplierOpenAccountApp.Code,
                    Summary = "开户",
                    AfterChangeAmount = supplierOpenAccountApp.AccountInitialDeposit,
                    BeforeChangeAmount = 0,
                    Credit = supplierOpenAccountApp.AccountInitialDeposit
                };
                supplierAccount.AccountPayableHistoryDetails.Add(accountPayableHistoryDetail);
                DomainService.checkInsertAccountPayableHistoryDetail(accountPayableHistoryDetail);
                InsertToDatabase(accountPayableHistoryDetail);
                UpdateToDatabase(supplierOpenAccountApp);
                supplierOpenAccountApp.Status = (int)DcsWorkflowOfSimpleApprovalStatus.已审核;
                this.UpdateSupplierOpenAccountAppValidate(supplierOpenAccountApp);
            }else {
                throw new ValidationException(ErrorStrings.SupplierOpenAccountApp_Validation1);
            }
        }

        public void 作废供应商开户申请单(SupplierOpenAccountApp supplierOpenAccountApp) {
            var dbSupplierOpenAccountApp = ObjectContext.SupplierOpenAccountApps.Where(r => r.Id == supplierOpenAccountApp.Id).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbSupplierOpenAccountApp == null) {
                throw new ValidationException(ErrorStrings.SupplierOpenAccountApp_Validation1);
            }
            if(dbSupplierOpenAccountApp.Status != (int)DcsWorkflowOfSimpleApprovalStatus.新建) {
                throw new ValidationException(ErrorStrings.SupplierOpenAccountApp_Validation3);
            }
            var userInfo = Utils.GetCurrentUserInfo();
            UpdateToDatabase(supplierOpenAccountApp);
            supplierOpenAccountApp.Status = (int)DcsWorkflowOfSimpleApprovalStatus.作废;
            supplierOpenAccountApp.AbandonerId = userInfo.Id;
            supplierOpenAccountApp.AbandonerName = userInfo.Name;
            supplierOpenAccountApp.AbandonTime = DateTime.Now;
            this.UpdateSupplierOpenAccountAppValidate(supplierOpenAccountApp);
        }

    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:11:15
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 审核供应商开户申请单(SupplierOpenAccountApp supplierOpenAccountApp) {
            new SupplierOpenAccountAppAch(this).审核供应商开户申请单(supplierOpenAccountApp);
        }
        
        [Update(UsingCustomMethod = true)]
        public void 作废供应商开户申请单(SupplierOpenAccountApp supplierOpenAccountApp) {
            new SupplierOpenAccountAppAch(this).作废供应商开户申请单(supplierOpenAccountApp);
        }
    }
}
