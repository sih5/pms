﻿
using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class InternalAcquisitionBillAch : DcsSerivceAchieveBase {
        public InternalAcquisitionBillAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 审批内部领入单(InternalAcquisitionBill internalAcquisitionBill) {
            var dbinternalAcquisitionBill = ObjectContext.InternalAcquisitionBills.Where(r => r.Id == internalAcquisitionBill.Id && r.Status == (int)DcsWorkflowOfSimpleApprovalStatus.新建).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbinternalAcquisitionBill == null)
                throw new ValidationException(ErrorStrings.InternalAcquisitionBill_Validation1);
            var warehouse = ObjectContext.Warehouses.SingleOrDefault(r => r.Id == internalAcquisitionBill.WarehouseId && r.Status == (int)DcsBaseDataStatus.有效);
            if(warehouse == null)
                throw new ValidationException(string.Format(ErrorStrings.InternalAcquisitionBill_Validation3, internalAcquisitionBill.Code));
            var branch = ObjectContext.Branches.SingleOrDefault(r => r.Id == internalAcquisitionBill.BranchId && r.Status != (int)DcsMasterDataStatus.作废);
            if(branch == null)
                throw new ValidationException(ErrorStrings.InternalAcquisitionBill_Validation4);
            var departmentInformation = ObjectContext.DepartmentInformations.SingleOrDefault(r => r.Id == internalAcquisitionBill.DepartmentId && r.Status == (int)DcsBaseDataStatus.有效);
            if(departmentInformation == null)
                throw new ValidationException(string.Format(ErrorStrings.InternalAcquisitionBill_Validation5, internalAcquisitionBill.Code));
            var salesUnit = ObjectContext.SalesUnits.FirstOrDefault(r => r.Status != (int)DcsMasterDataStatus.作废 && ObjectContext.SalesUnitAffiWarehouses.Any(v => v.SalesUnitId == r.Id && v.WarehouseId == internalAcquisitionBill.WarehouseId));
            if(salesUnit == null)
                throw new ValidationException(ErrorStrings.InternalAcquisitionBill_Validation7);
            var internalAcquisitionDetails = internalAcquisitionBill.InternalAcquisitionDetails;
            var userInfo = Utils.GetCurrentUserInfo();
            UpdateToDatabase(internalAcquisitionBill);
            internalAcquisitionBill.Status = (int)DcsWorkflowOfSimpleApprovalStatus.已审核;
            internalAcquisitionBill.ApproverId = userInfo.Id;
            internalAcquisitionBill.ApproverName = userInfo.Name;
            internalAcquisitionBill.ApproveTime = DateTime.Now;
            this.UpdateInternalAcquisitionBillValidate(internalAcquisitionBill);
            var branchstrateies = this.ObjectContext.Branchstrategies.Where(r => r.BranchId == internalAcquisitionBill.BranchId).ToArray();
            if(branchstrateies.Length > 1 || branchstrateies.Length == 0) {
                throw new ValidationException(ErrorStrings.InternalAcquisitionBill_Validation8);
            }
            var branchstratey = branchstrateies[0];
            var flag = (branchstratey.InternalUsePriceStrategy == null || branchstratey.InternalUsePriceStrategy == (int)DcsInternalUsePrice.计划价) ? 0 : 1;
            var partsInboundPlan = new PartsInboundPlan {
                WarehouseCode = warehouse.Code,
                WarehouseName = internalAcquisitionBill.WarehouseName,
                WarehouseId = internalAcquisitionBill.WarehouseId,
                StorageCompanyId = internalAcquisitionBill.BranchId,
                StorageCompanyCode = branch.Code,
                StorageCompanyName = internalAcquisitionBill.BranchName,
                StorageCompanyType = (int)DcsCompanyType.分公司,
                BranchId = internalAcquisitionBill.BranchId,
                BranchCode = branch.Code,
                BranchName = internalAcquisitionBill.BranchName,
                CounterpartCompanyId = internalAcquisitionBill.DepartmentId,
                CounterpartCompanyCode = departmentInformation.Code,
                CounterpartCompanyName = internalAcquisitionBill.DepartmentName,
                SourceId = internalAcquisitionBill.Id,
                SourceCode = internalAcquisitionBill.Code,
                InboundType = (int)DcsPartsInboundType.内部领入,
                CustomerAccountId = null,
                OriginalRequirementBillId = internalAcquisitionBill.Id,
                OriginalRequirementBillCode = internalAcquisitionBill.Code,
                OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.内部领入单,
                Status = (int)DcsPartsInboundPlanStatus.新建,
                Remark = internalAcquisitionBill.Remark,
                PartsSalesCategoryId = salesUnit.PartsSalesCategoryId,
                IfWmsInterface = warehouse.WmsInterface,
                RequestedDeliveryTime = internalAcquisitionBill.RequestedDeliveryTime,
                PartsPurchaseOrderTypeId = internalAcquisitionBill.PartsPurchaseOrderTypeId
            };
            if(flag == 0) {
                foreach(var internalAcquisitionDetail in internalAcquisitionDetails) {
                    var partsInboundPlanDetail = new PartsInboundPlanDetail {
                        SparePartId = internalAcquisitionDetail.SparePartId,
                        SparePartCode = internalAcquisitionDetail.SparePartCode,
                        SparePartName = internalAcquisitionDetail.SparePartName,
                        PlannedAmount = internalAcquisitionDetail.Quantity,
                        Price = internalAcquisitionDetail.UnitPrice,
                        Remark = internalAcquisitionDetail.Remark,
                    };
                    partsInboundPlan.PartsInboundPlanDetails.Add(partsInboundPlanDetail);
                }
            }else {
                foreach(var internalAcquisitionDetail in internalAcquisitionDetails) {
                    var partsInboundPlanDetail = new PartsInboundPlanDetail {
                        SparePartId = internalAcquisitionDetail.SparePartId,
                        SparePartCode = internalAcquisitionDetail.SparePartCode,
                        SparePartName = internalAcquisitionDetail.SparePartName,
                        PlannedAmount = internalAcquisitionDetail.Quantity,
                        Price = internalAcquisitionDetail.SalesPrice.Value,
                        Remark = internalAcquisitionDetail.Remark,
                    };
                    partsInboundPlan.PartsInboundPlanDetails.Add(partsInboundPlanDetail);
                }
            }
            InsertToDatabase(partsInboundPlan);
            new PartsInboundPlanAch(this.DomainService).InsertPartsInboundPlanValidate(partsInboundPlan);
        }

        public void 作废内部领入单(InternalAcquisitionBill internalAcquisitionBill) {
            var dbinternalAcquisitionBill = ObjectContext.InternalAcquisitionBills.Where(r => r.Id == internalAcquisitionBill.Id && r.Status == (int)DcsWorkflowOfSimpleApprovalStatus.新建).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbinternalAcquisitionBill == null)
                throw new ValidationException(ErrorStrings.InternalAcquisitionBill_Validation6);
            var userInfo = Utils.GetCurrentUserInfo();
            UpdateToDatabase(internalAcquisitionBill);
            internalAcquisitionBill.Status = (int)DcsWorkflowOfSimpleApprovalStatus.作废;
            internalAcquisitionBill.AbandonerId = userInfo.Id;
            internalAcquisitionBill.AbandonerName = userInfo.Name;
            internalAcquisitionBill.AbandonTime = DateTime.Now;
            this.UpdateInternalAcquisitionBillValidate(internalAcquisitionBill);
        }

        public void 保存校验内部领入单(InternalAcquisitionBill internalAcquisitionBill) {
            var departmentBrandRelation = ObjectContext.DepartmentBrandRelations.Where(r => r.DepartmentId == internalAcquisitionBill.DepartmentId);
            var departmentBrandRelationarray = departmentBrandRelation.ToArray();
            if(departmentBrandRelationarray.Length > 0) {
                var brandlist = departmentBrandRelationarray.Select(r => r.PartsSalesCategoryId);
                var salesUnit = from a in ObjectContext.SalesUnits.Where(s => brandlist.Contains(s.PartsSalesCategoryId))
                                join b in ObjectContext.SalesUnitAffiWarehouses.Where(r => r.WarehouseId == internalAcquisitionBill.WarehouseId) on a.Id equals b.SalesUnitId
                                select a;
                var salesunitarray = salesUnit.ToArray();
                if(salesunitarray.Length < 1) {
                    throw new ValidationException(ErrorStrings.InternalAcquisitionBill_Validation9);
                }
            }
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:11:28
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 审批内部领入单(InternalAcquisitionBill internalAcquisitionBill) {
            new InternalAcquisitionBillAch(this).审批内部领入单(internalAcquisitionBill);
        }
        
        [Update(UsingCustomMethod = true)]
        public void 作废内部领入单(InternalAcquisitionBill internalAcquisitionBill) {
            new InternalAcquisitionBillAch(this).作废内部领入单(internalAcquisitionBill);
        }
        
        [Update(UsingCustomMethod = true)]
        public void 保存校验内部领入单(InternalAcquisitionBill internalAcquisitionBill) {
            new InternalAcquisitionBillAch(this).保存校验内部领入单(internalAcquisitionBill);
        }
    }
}
