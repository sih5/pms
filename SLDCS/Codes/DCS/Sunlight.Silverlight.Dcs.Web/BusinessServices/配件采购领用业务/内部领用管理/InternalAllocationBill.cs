﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class InternalAllocationBillAch : DcsSerivceAchieveBase {
        public InternalAllocationBillAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 审批内部领出单(InternalAllocationBill internalAllocationBill) {
            var dbinternalAllocationBill = ObjectContext.InternalAllocationBills.Where(r => r.Id == internalAllocationBill.Id && r.Status == (int)DcsInternalAllocationBillStatus.已初审).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbinternalAllocationBill == null)
                throw new ValidationException(ErrorStrings.InternalAllocationBill_Validation1);
            var warehouse = ObjectContext.Warehouses.FirstOrDefault(r => r.Id == internalAllocationBill.WarehouseId && r.Status == (int)DcsBaseDataStatus.有效);
            if(warehouse == null)
                throw new ValidationException(string.Format(ErrorStrings.InternalAllocationBill_Validation5, internalAllocationBill.Code));
            var branch = ObjectContext.Branches.FirstOrDefault(r => r.Id == internalAllocationBill.BranchId && r.Status != (int)DcsMasterDataStatus.作废);
            if(branch == null)
                throw new ValidationException(ErrorStrings.InternalAllocationBill_Validation6);
            var departmentInformation = ObjectContext.DepartmentInformations.FirstOrDefault(r => r.Id == internalAllocationBill.DepartmentId && r.Status == (int)DcsBaseDataStatus.有效);
            if(departmentInformation == null)
                throw new ValidationException(string.Format(ErrorStrings.InternalAllocationBill_Validation7, internalAllocationBill.Code));
            var salesUnit = ObjectContext.SalesUnits.FirstOrDefault(r => r.Status != (int)DcsMasterDataStatus.作废 && ObjectContext.SalesUnitAffiWarehouses.Any(v => v.SalesUnitId == r.Id && v.WarehouseId == internalAllocationBill.WarehouseId));
            if(salesUnit == null)
                throw new ValidationException(ErrorStrings.InternalAllocationBill_Validation9);
            var internalAllocationDetails = internalAllocationBill.InternalAllocationDetails;
            var userInfo = Utils.GetCurrentUserInfo();
            UpdateToDatabase(internalAllocationBill);
            internalAllocationBill.Status = (int)DcsInternalAllocationBillStatus.已审核;
            internalAllocationBill.ApproverId = userInfo.Id;
            internalAllocationBill.ApproverName = userInfo.Name;
            internalAllocationBill.ApproveTime = DateTime.Now;
            this.UpdateInternalAllocationBillValidate(internalAllocationBill);

            var branchstrateies = this.ObjectContext.Branchstrategies.Where(r => r.BranchId == internalAllocationBill.BranchId).ToArray();
            if(branchstrateies.Length > 1 || branchstrateies.Length == 0) {
                throw new ValidationException(ErrorStrings.InternalAcquisitionBill_Validation8);
            }
            var branchstratey = branchstrateies[0];
            var flag = (branchstratey.InternalUsePriceStrategy == null || branchstratey.InternalUsePriceStrategy == (int)DcsInternalUsePrice.计划价) ? 0 : 1;

            //检验库存
            this.ValidateBottomStock(internalAllocationBill);

            var partsOutboundPlan = new PartsOutboundPlan {
                WarehouseCode = warehouse.Code,
                WarehouseName = internalAllocationBill.WarehouseName,
                WarehouseId = internalAllocationBill.WarehouseId,
                StorageCompanyId = internalAllocationBill.BranchId,
                StorageCompanyCode = branch.Code,
                StorageCompanyName = internalAllocationBill.BranchName,
                StorageCompanyType = (int)DcsCompanyType.分公司,
                BranchId = internalAllocationBill.BranchId,
                BranchCode = branch.Code,
                BranchName = internalAllocationBill.BranchName,
                CounterpartCompanyId = internalAllocationBill.DepartmentId,
                CounterpartCompanyCode = departmentInformation.Code,
                CounterpartCompanyName = departmentInformation.Name,
                SourceId = internalAllocationBill.Id,
                SourceCode = internalAllocationBill.Code,
                OutboundType = (int)DcsPartsOutboundType.内部领出,
                CustomerAccountId = null,
                OriginalRequirementBillId = internalAllocationBill.Id,
                OriginalRequirementBillCode = internalAllocationBill.Code,
                OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.内部领出单,
                Status = (int)DcsPartsOutboundPlanStatus.新建,
                ReceivingCompanyId = internalAllocationBill.DepartmentId,
                ReceivingCompanyCode = departmentInformation.Code,
                ReceivingCompanyName = departmentInformation.Name,
                Remark = internalAllocationBill.Remark,
                PartsSalesCategoryId = salesUnit.PartsSalesCategoryId,
                IfWmsInterface = warehouse.WmsInterface
            };
            if(flag == 0) {
                foreach(var internalAllocationDetail in internalAllocationDetails) {
                    var partsOutboundPlanDetail = new PartsOutboundPlanDetail {
                        SparePartId = internalAllocationDetail.SparePartId,
                        SparePartCode = internalAllocationDetail.SparePartCode,
                        SparePartName = internalAllocationDetail.SparePartName,
                        PlannedAmount = internalAllocationDetail.Quantity,
                        Price = internalAllocationDetail.UnitPrice,
                        Remark = internalAllocationDetail.Remark,
                    };
                    partsOutboundPlan.PartsOutboundPlanDetails.Add(partsOutboundPlanDetail);
                }
            } else {
                foreach(var internalAllocationDetail in internalAllocationDetails) {
                    var partsOutboundPlanDetail = new PartsOutboundPlanDetail {
                        SparePartId = internalAllocationDetail.SparePartId,
                        SparePartCode = internalAllocationDetail.SparePartCode,
                        SparePartName = internalAllocationDetail.SparePartName,
                        PlannedAmount = internalAllocationDetail.Quantity,
                        Price = internalAllocationDetail.SalesPrice.Value,
                        Remark = internalAllocationDetail.Remark,
                    };
                    partsOutboundPlan.PartsOutboundPlanDetails.Add(partsOutboundPlanDetail);
                }
            }
            DomainService.生成配件出库计划(partsOutboundPlan);
        }

        private void ValidateBottomStock(InternalAllocationBill partsTransferOrder) {
            var user = Utils.GetCurrentUserInfo();
            var company = ObjectContext.Companies.Single(r => r.Id == user.EnterpriseId);
            if(company.Type == (int)DcsCompanyType.分公司) {
                var partIds = partsTransferOrder.InternalAllocationDetails.Select(r => r.SparePartId).ToArray();
                //可用库存
                var partsStocks = new WarehousePartsStockAch(this.DomainService).配件调拨查询仓库库存(user.EnterpriseId, partsTransferOrder.WarehouseId, partIds).ToArray();

                var internalAllocationDetails = ObjectContext.InternalAllocationDetails.Where(o => o.InternalAllocationBill.Id != partsTransferOrder.Id && o.InternalAllocationBill.Status != (int)DcsInternalAllocationBillStatus.作废 && o.InternalAllocationBill.Status != (int)DcsInternalAllocationBillStatus.已审核 && partIds.Contains(o.SparePartId) && o.InternalAllocationBill.WarehouseId == partsTransferOrder.WarehouseId);

                foreach(var detail in partsTransferOrder.InternalAllocationDetails) {
                    var partsStock = partsStocks.FirstOrDefault(r => r.SparePartId == detail.SparePartId);
                    var quantitySum = internalAllocationDetails.Where(o => o.SparePartId == detail.SparePartId).Sum(o => o.Quantity);
                    if(partsStock == null)
                        throw new ValidationException(string.Format(ErrorStrings.InternalAllocationBill_Validation10,detail.SparePartCode));

                    if(partsStock.UsableQuantity - quantitySum - detail.Quantity < 0) {
                        throw new ValidationException(string.Format(ErrorStrings.InternalAllocationBill_Validation11,detail.SparePartCode));
                    }
                }
            }
        }
        public void 作废内部领出单(InternalAllocationBill internalAllocationBill) {
            var dbinternalAllocationBill = ObjectContext.InternalAllocationBills.Where(r => r.Id == internalAllocationBill.Id && r.Status == (int)DcsInternalAllocationBillStatus.新建).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbinternalAllocationBill == null)
                throw new ValidationException(ErrorStrings.InternalAllocationBill_Validation8);
            var userInfo = Utils.GetCurrentUserInfo();
            UpdateToDatabase(internalAllocationBill);
            internalAllocationBill.Status = (int)DcsInternalAllocationBillStatus.作废;
            internalAllocationBill.AbandonerId = userInfo.Id;
            internalAllocationBill.AbandonerName = userInfo.Name;
            internalAllocationBill.AbandonTime = DateTime.Now;
            this.UpdateInternalAllocationBillValidate(internalAllocationBill);
        }


        public void 生成内部领出单(InternalAllocationBill internalAllocationBill) {
            this.保存校验内部领出单(internalAllocationBill);
            //检验库存
            this.ValidateBottomStock(internalAllocationBill);
            var internalAllocationDetails = internalAllocationBill.InternalAllocationDetails.ToArray();
            var sparePartIds = internalAllocationDetails.Select(r => r.SparePartId).ToArray();
            //var warehousePartsStocks =new WarehousePartsStockAch(this.DomainService).GetWarehousePartsStockSimple(internalAllocationBill.WarehouseId, sparePartIds).ToArray();
            //foreach(var internalAllocationDetail in internalAllocationDetails) {
            //    var warehousePartsStock = warehousePartsStocks.FirstOrDefault(r => r.SparePartId == internalAllocationDetail.SparePartId);
            //    if(internalAllocationDetail.Quantity > warehousePartsStock.UsableQuantity)
            //        throw new ValidationException(string.Format(ErrorStrings.InternalAllocationBill_Validation3, internalAllocationDetail.SparePartCode, internalAllocationBill.WarehouseName));
            //}
        }


        public void 修改内部领出单(InternalAllocationBill internalAllocationBill) {
            this.保存校验内部领出单(internalAllocationBill);
            //检验库存
            this.ValidateBottomStock(internalAllocationBill);
            var internalAllocationDetails = internalAllocationBill.InternalAllocationDetails.ToArray();
            var sparePartIds = internalAllocationDetails.Select(r => r.SparePartId).ToArray();
            //var warehousePartsStocks = new WarehousePartsStockAch(this.DomainService).GetWarehousePartsStockSimple(internalAllocationBill.WarehouseId, sparePartIds).ToArray();
            //foreach(var internalAllocationDetail in internalAllocationDetails) {
            //    var warehousePartsStock = warehousePartsStocks.FirstOrDefault(r => r.SparePartId == internalAllocationDetail.SparePartId);
            //    if(internalAllocationDetail.Quantity > warehousePartsStock.UsableQuantity)
            //        throw new ValidationException(string.Format(ErrorStrings.InternalAllocationBill_Validation3, internalAllocationDetail.SparePartCode, internalAllocationBill.WarehouseName));
            //}
        }


        public void 保存校验内部领出单(InternalAllocationBill internalAllocationBill) {
            var departmentBrandRelation = ObjectContext.DepartmentBrandRelations.Where(r => r.DepartmentId == internalAllocationBill.DepartmentId);
            var departmentBrandRelationarray = departmentBrandRelation.ToArray();
            if(departmentBrandRelationarray.Length > 0) {
                var brandlist = departmentBrandRelationarray.Select(r => r.PartsSalesCategoryId);
                var salesUnit = from a in ObjectContext.SalesUnits.Where(s => brandlist.Contains(s.PartsSalesCategoryId))
                                join b in ObjectContext.SalesUnitAffiWarehouses.Where(r => r.WarehouseId == internalAllocationBill.WarehouseId) on a.Id equals b.SalesUnitId
                                select a;
                var salesunitarray = salesUnit.ToArray();
                if(salesunitarray.Length < 1) {
                    throw new ValidationException(ErrorStrings.InternalAllocationBill_Validation12);
                }
            }
        }
        public void 驳回内部领出单(InternalAllocationBill internalAllocationBill) {
            var dbinternalAllocationBill = ObjectContext.InternalAllocationBills.Where(r => r.Id == internalAllocationBill.Id && (r.Status == (int)DcsInternalAllocationBillStatus.新建 || r.Status == (int)DcsInternalAllocationBillStatus.已初审)).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbinternalAllocationBill == null)
                throw new ValidationException(ErrorStrings.InternalAllocationBill_Validation13);
            var userInfo = Utils.GetCurrentUserInfo();
            UpdateToDatabase(internalAllocationBill);
            internalAllocationBill.Status = (int)DcsInternalAllocationBillStatus.新建;
            //internalAllocationBill.AbandonerId = userInfo.Id;
            //internalAllocationBill.AbandonerName = userInfo.Name;
            //internalAllocationBill.AbandonTime = DateTime.Now;
            this.UpdateInternalAllocationBillValidate(internalAllocationBill);
        }
        public void 初审内部领出单(InternalAllocationBill internalAllocationBill) {
            var dbinternalAllocationBill = ObjectContext.InternalAllocationBills.Where(r => r.Id == internalAllocationBill.Id && r.Status == (int)DcsInternalAllocationBillStatus.新建).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbinternalAllocationBill == null)
                throw new ValidationException(ErrorStrings.InternalAllocationBill_Validation14);
            //检验库存
            this.ValidateBottomStock(internalAllocationBill);
            var userInfo = Utils.GetCurrentUserInfo();
            UpdateToDatabase(internalAllocationBill);
            internalAllocationBill.Status = (int)DcsInternalAllocationBillStatus.已初审;
            internalAllocationBill.CheckerId = userInfo.Id;
            internalAllocationBill.CheckerName = userInfo.Name;
            internalAllocationBill.CheckTime = DateTime.Now;
            this.UpdateInternalAllocationBillValidate(internalAllocationBill);
        }

    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:11:28
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 审批内部领出单(InternalAllocationBill internalAllocationBill) {
            new InternalAllocationBillAch(this).审批内部领出单(internalAllocationBill);
        }


        [Update(UsingCustomMethod = true)]
        public void 作废内部领出单(InternalAllocationBill internalAllocationBill) {
            new InternalAllocationBillAch(this).作废内部领出单(internalAllocationBill);
        }


        [Update(UsingCustomMethod = true)]
        public void 生成内部领出单(InternalAllocationBill internalAllocationBill) {
            new InternalAllocationBillAch(this).生成内部领出单(internalAllocationBill);
        }


        [Update(UsingCustomMethod = true)]
        public void 修改内部领出单(InternalAllocationBill internalAllocationBill) {
            new InternalAllocationBillAch(this).修改内部领出单(internalAllocationBill);
        }


        [Update(UsingCustomMethod = true)]
        public void 保存校验内部领出单(InternalAllocationBill internalAllocationBill) {
            new InternalAllocationBillAch(this).保存校验内部领出单(internalAllocationBill);
        }
        [Update(UsingCustomMethod = true)]
        public void 驳回内部领出单(InternalAllocationBill internalAllocationBill) {
            new InternalAllocationBillAch(this).驳回内部领出单(internalAllocationBill);
        }
        [Update(UsingCustomMethod = true)]
        public void 初审内部领出单(InternalAllocationBill internalAllocationBill) {
            new InternalAllocationBillAch(this).初审内部领出单(internalAllocationBill);
        }
    }
}
