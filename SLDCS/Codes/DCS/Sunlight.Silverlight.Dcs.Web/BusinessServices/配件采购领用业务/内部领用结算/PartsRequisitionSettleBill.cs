﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsRequisitionSettleBillAch : DcsSerivceAchieveBase {
        public PartsRequisitionSettleBillAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 生成配件领用结算单(PartsRequisitionSettleBill partsRequisitionSettleBill) {
            if (null != partsRequisitionSettleBill.InvoiceDate.Value)
            {
                //当过帐日期不为空时，验证过帐日期是否有效
                this.DomainService.验证过账日期是否有效(partsRequisitionSettleBill.InvoiceDate);
            }
            var partsRequisitionSettleRefs = partsRequisitionSettleBill.PartsRequisitionSettleRefs.ToArray();
            var partsOutboundBillIds = partsRequisitionSettleBill.PartsRequisitionSettleRefs.Where(r => r.SourceType == (int)DcsPartsRequisitionSettleRefSourceType.配件出库单).Select(r => r.SourceId);
            var partsInboundCheckBillIds = partsRequisitionSettleBill.PartsRequisitionSettleRefs.Where(r => r.SourceType == (int)DcsPartsRequisitionSettleRefSourceType.配件入库检验单).Select(r => r.SourceId);
            var partsOutboundBills = ObjectContext.PartsOutboundBills.Where(r => partsOutboundBillIds.Contains(r.Id)).ToArray();
            var partsInboundCheckBills = ObjectContext.PartsInboundCheckBills.Where(r => partsInboundCheckBillIds.Contains(r.Id)).ToArray();
            foreach(var partsRequisitionSettleRef in partsRequisitionSettleRefs) {
                if(partsRequisitionSettleRef.SourceType == (int)DcsPartsRequisitionSettleRefSourceType.配件出库单) {
                    var pendingSettlementPartsOutboundBill = partsOutboundBills.SingleOrDefault(r => r.Id == partsRequisitionSettleRef.SourceId && r.SettlementStatus == (int)DcsPartsSettlementStatus.待结算);
                    if(pendingSettlementPartsOutboundBill == null)
                        throw new ValidationException(string.Format(ErrorStrings.PartsRequisitionSettleBill_Validation1, partsRequisitionSettleRef.SourceCode));
                    pendingSettlementPartsOutboundBill.SettlementStatus = (int)DcsPartsSettlementStatus.已结算;
                    new PartsOutboundBillAch(this.DomainService).UpdatePartsOutboundBillValidate(pendingSettlementPartsOutboundBill);
                }
                if(partsRequisitionSettleRef.SourceType == (int)DcsPartsRequisitionSettleRefSourceType.配件入库检验单) {
                    var pendingSettlementPartsOutboundBill = partsInboundCheckBills.SingleOrDefault(r => r.Id == partsRequisitionSettleRef.SourceId && r.SettlementStatus == (int)DcsPartsSettlementStatus.待结算);
                    if(pendingSettlementPartsOutboundBill == null)
                        throw new ValidationException(string.Format(ErrorStrings.PartsRequisitionSettleBill_Validation2, partsRequisitionSettleRef.SourceCode));
                    pendingSettlementPartsOutboundBill.SettlementStatus = (int)DcsPartsSettlementStatus.已结算;
                    new PartsInboundCheckBillAch(this.DomainService).UpdatePartsInboundCheckBillValidate(pendingSettlementPartsOutboundBill);
                }
            }
            InsertToDatabase(partsRequisitionSettleBill);
            partsRequisitionSettleBill.Status = (int)DcsPartsRequisitionSettleBillStatus.新建;
            this.InsertPartsRequisitionSettleBillValidate(partsRequisitionSettleBill);
        }

        public void 修改配件领用结算单(PartsRequisitionSettleBill partsRequisitionSettleBill) {
            if (null != partsRequisitionSettleBill.InvoiceDate.Value)
            {
                //当过帐日期不为空时，验证过帐日期是否有效
                this.DomainService.验证过账日期是否有效(partsRequisitionSettleBill.InvoiceDate);
            }
            var dbpartsRequisitionSettleBill = ObjectContext.PartsRequisitionSettleBills.Where(r => r.Id == partsRequisitionSettleBill.Id && r.Status == (int)DcsPartsRequisitionSettleBillStatus.新建).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbpartsRequisitionSettleBill == null)
                throw new ValidationException(ErrorStrings.PartsRequisitionSettleBill_Validation3);
            CheckEntityState(partsRequisitionSettleBill);
            var partsRequisitionSettleRefs = ChangeSet.GetAssociatedChanges(partsRequisitionSettleBill, r => r.PartsRequisitionSettleRefs).Cast<PartsRequisitionSettleRef>().ToArray();
            var insertPartsRequisitionSettleRefs = partsRequisitionSettleRefs.Where(r => ChangeSet.GetChangeOperation(r) == ChangeOperation.Insert && partsRequisitionSettleRefs.Any(v => ChangeSet.GetChangeOperation(v) == ChangeOperation.Delete && v.SourceId == r.SourceId)).ToArray();
            var deletePartsRequisitionSettleRefs = partsRequisitionSettleRefs.Where(r => ChangeSet.GetChangeOperation(r) == ChangeOperation.Delete && partsRequisitionSettleRefs.Any(v => ChangeSet.GetChangeOperation(v) == ChangeOperation.Insert && v.SourceId == r.SourceId)).ToArray();
            var pendingPartsRequisitionSettleRefs = partsRequisitionSettleRefs.Except(insertPartsRequisitionSettleRefs).Except(deletePartsRequisitionSettleRefs).ToArray();
            var partsOutboundBillIds = pendingPartsRequisitionSettleRefs.Where(r => r.SourceType == (int)DcsPartsRequisitionSettleRefSourceType.配件出库单).Select(r => r.SourceId);
            var partsInboundCheckBillIds = pendingPartsRequisitionSettleRefs.Where(r => r.SourceType == (int)DcsPartsRequisitionSettleRefSourceType.配件入库检验单).Select(r => r.SourceId);
            var partsOutboundBills = ObjectContext.PartsOutboundBills.Where(r => partsOutboundBillIds.Contains(r.Id)).ToArray();
            var partsInboundCheckBills = ObjectContext.PartsInboundCheckBills.Where(r => partsInboundCheckBillIds.Contains(r.Id)).ToArray();
            foreach(var partsRequisitionSettleRef in pendingPartsRequisitionSettleRefs) {
                switch(ChangeSet.GetChangeOperation(partsRequisitionSettleRef)) {
                    case ChangeOperation.Insert:
                        if(partsRequisitionSettleRef.SourceType == (int)DcsPartsRequisitionSettleRefSourceType.配件出库单) {
                            var pendingSettlementPartsOutboundBill = partsOutboundBills.SingleOrDefault(r => r.Id == partsRequisitionSettleRef.SourceId && r.SettlementStatus == (int)DcsPartsSettlementStatus.待结算);
                            if(pendingSettlementPartsOutboundBill == null)
                                throw new ValidationException(string.Format(ErrorStrings.PartsRequisitionSettleBill_Validation1, partsRequisitionSettleRef.SourceCode));
                            pendingSettlementPartsOutboundBill.SettlementStatus = (int)DcsPartsSettlementStatus.已结算;
                            new PartsOutboundBillAch(this.DomainService).UpdatePartsOutboundBillValidate(pendingSettlementPartsOutboundBill);
                        }
                        if(partsRequisitionSettleRef.SourceType == (int)DcsPartsRequisitionSettleRefSourceType.配件入库检验单) {
                            var pendingSettlementPartsOutboundBill = partsInboundCheckBills.SingleOrDefault(r => r.Id == partsRequisitionSettleRef.SourceId && r.SettlementStatus == (int)DcsPartsSettlementStatus.待结算);
                            if(pendingSettlementPartsOutboundBill == null)
                                throw new ValidationException(string.Format(ErrorStrings.PartsRequisitionSettleBill_Validation2, partsRequisitionSettleRef.SourceCode));
                            pendingSettlementPartsOutboundBill.SettlementStatus = (int)DcsPartsSettlementStatus.已结算;
                            new PartsInboundCheckBillAch(this.DomainService).UpdatePartsInboundCheckBillValidate(pendingSettlementPartsOutboundBill);
                        }
                        break;
                    case ChangeOperation.Delete:
                        if(partsRequisitionSettleRef.SourceType == (int)DcsPartsRequisitionSettleRefSourceType.配件出库单) {
                            var pendingSettlementPartsOutboundBill = partsOutboundBills.SingleOrDefault(r => r.Id == partsRequisitionSettleRef.SourceId);
                            if(pendingSettlementPartsOutboundBill == null)
                                throw new ValidationException(string.Format(ErrorStrings.PartsRequisitionSettleBill_Validation4, partsRequisitionSettleRef.SourceCode));
                            pendingSettlementPartsOutboundBill.SettlementStatus = (int)DcsPartsSettlementStatus.待结算;
                            new PartsOutboundBillAch(this.DomainService).UpdatePartsOutboundBillValidate(pendingSettlementPartsOutboundBill);
                        }
                        if(partsRequisitionSettleRef.SourceType == (int)DcsPartsRequisitionSettleRefSourceType.配件入库检验单) {
                            var pendingSettlementPartsOutboundBill = partsInboundCheckBills.SingleOrDefault(r => r.Id == partsRequisitionSettleRef.SourceId);
                            if(pendingSettlementPartsOutboundBill == null)
                                throw new ValidationException(string.Format(ErrorStrings.PartsRequisitionSettleBill_Validation5, partsRequisitionSettleRef.SourceCode));
                            pendingSettlementPartsOutboundBill.SettlementStatus = (int)DcsPartsSettlementStatus.待结算;
                            new PartsInboundCheckBillAch(this.DomainService).UpdatePartsInboundCheckBillValidate(pendingSettlementPartsOutboundBill);
                        }
                        break;
                }
            }
        }

        public void 作废配件领用结算单(PartsRequisitionSettleBillExport partsRequisitionSettleBillExport)
        {
            PartsRequisitionSettleBill partsRequisitionSettleBill = ObjectContext.PartsRequisitionSettleBills.Where(r => r.Id == partsRequisitionSettleBillExport.Id).SetMergeOption(MergeOption.NoTracking).First();
            var dbpartsRequisitionSettleBill = ObjectContext.PartsRequisitionSettleBills.Where(r => r.Id == partsRequisitionSettleBill.Id && r.Status == (int)DcsPartsRequisitionSettleBillStatus.新建).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbpartsRequisitionSettleBill == null)
                throw new ValidationException(ErrorStrings.PartsRequisitionSettleBill_Validation6);
            var partsRequisitionSettleRefs = partsRequisitionSettleBill.PartsRequisitionSettleRefs.ToArray();
            var partsOutboundBillIds = partsRequisitionSettleBill.PartsRequisitionSettleRefs.Where(r => r.SourceType == (int)DcsPartsRequisitionSettleRefSourceType.配件出库单).Select(r => r.SourceId);
            var partsInboundCheckBillIds = partsRequisitionSettleBill.PartsRequisitionSettleRefs.Where(r => r.SourceType == (int)DcsPartsRequisitionSettleRefSourceType.配件入库检验单).Select(r => r.SourceId);
            var partsOutboundBills = ObjectContext.PartsOutboundBills.Where(r => partsOutboundBillIds.Contains(r.Id)).ToArray();
            var partsInboundCheckBills = ObjectContext.PartsInboundCheckBills.Where(r => partsInboundCheckBillIds.Contains(r.Id)).ToArray();
            foreach(var partsRequisitionSettleRef in partsRequisitionSettleRefs) {
                if(partsRequisitionSettleRef.SourceType == (int)DcsPartsRequisitionSettleRefSourceType.配件出库单) {
                    var pendingSettlementPartsOutboundBill = partsOutboundBills.SingleOrDefault(r => r.Id == partsRequisitionSettleRef.SourceId);
                    if(pendingSettlementPartsOutboundBill == null)
                        throw new ValidationException(string.Format(ErrorStrings.PartsRequisitionSettleBill_Validation4, partsRequisitionSettleRef.SourceCode));
                    pendingSettlementPartsOutboundBill.SettlementStatus = (int)DcsPartsSettlementStatus.待结算;
                    new PartsOutboundBillAch(this.DomainService).UpdatePartsOutboundBillValidate(pendingSettlementPartsOutboundBill);
                }
                if(partsRequisitionSettleRef.SourceType == (int)DcsPartsRequisitionSettleRefSourceType.配件入库检验单) {
                    var pendingSettlementPartsOutboundBill = partsInboundCheckBills.SingleOrDefault(r => r.Id == partsRequisitionSettleRef.SourceId);
                    if(pendingSettlementPartsOutboundBill == null)
                        throw new ValidationException(string.Format(ErrorStrings.PartsRequisitionSettleBill_Validation5, partsRequisitionSettleRef.SourceCode));
                    pendingSettlementPartsOutboundBill.SettlementStatus = (int)DcsPartsSettlementStatus.待结算;
                    new PartsInboundCheckBillAch(this.DomainService).UpdatePartsInboundCheckBillValidate(pendingSettlementPartsOutboundBill);
                }
            }
            UpdateToDatabase(partsRequisitionSettleBill);
            partsRequisitionSettleBill.Status = (int)DcsPartsRequisitionSettleBillStatus.作废;
            var userInfo = Utils.GetCurrentUserInfo();
            partsRequisitionSettleBill.AbandonerId = userInfo.Id;
            partsRequisitionSettleBill.AbandonerName = userInfo.Name;
            partsRequisitionSettleBill.AbandonTime = DateTime.Now;
            this.UpdatePartsRequisitionSettleBillValidate(partsRequisitionSettleBill);
        }

        public void 审批配件领用结算单(PartsRequisitionSettleBillExport partsRequisitionSettleBillExport)
        {
            PartsRequisitionSettleBill partsRequisitionSettleBill = ObjectContext.PartsRequisitionSettleBills.Where(r => r.Id == partsRequisitionSettleBillExport.Id).SetMergeOption(MergeOption.NoTracking).First();
            var dbpartsRequisitionSettleBill = ObjectContext.PartsRequisitionSettleBills.Where(r => r.Id == partsRequisitionSettleBill.Id && r.Status == (int)DcsPartsRequisitionSettleBillStatus.新建).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbpartsRequisitionSettleBill == null)
                throw new ValidationException(ErrorStrings.PartsRequisitionSettleBill_Validation7);
            UpdateToDatabase(partsRequisitionSettleBill);
            partsRequisitionSettleBill.Status = (int)DcsPartsRequisitionSettleBillStatus.已审批;
            var userInfo = Utils.GetCurrentUserInfo();
            partsRequisitionSettleBill.ApproverId = userInfo.Id;
            partsRequisitionSettleBill.ApproverName = userInfo.Name;
            partsRequisitionSettleBill.ApproveTime = DateTime.Now;
            this.UpdatePartsRequisitionSettleBillValidate(partsRequisitionSettleBill);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:11:28
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 生成配件领用结算单(PartsRequisitionSettleBill partsRequisitionSettleBill) {
            new PartsRequisitionSettleBillAch(this).生成配件领用结算单(partsRequisitionSettleBill);
        }
        
        [Update(UsingCustomMethod = true)]
        public void 修改配件领用结算单(PartsRequisitionSettleBill partsRequisitionSettleBill) {
            new PartsRequisitionSettleBillAch(this).修改配件领用结算单(partsRequisitionSettleBill);
        }
        
        [Update(UsingCustomMethod = true)]
        public void 作废配件领用结算单(PartsRequisitionSettleBillExport partsRequisitionSettleBillExport)
        {
            new PartsRequisitionSettleBillAch(this).作废配件领用结算单(partsRequisitionSettleBillExport);
        }
        
        [Update(UsingCustomMethod = true)]
        public void 审批配件领用结算单(PartsRequisitionSettleBillExport partsRequisitionSettleBillExport)
        {
            new PartsRequisitionSettleBillAch(this).审批配件领用结算单(partsRequisitionSettleBillExport);
        }
    }
}
