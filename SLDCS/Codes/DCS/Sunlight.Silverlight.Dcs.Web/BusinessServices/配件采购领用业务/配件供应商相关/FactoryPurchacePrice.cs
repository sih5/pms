﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Data.Objects.DataClasses;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.ServiceModel.DomainServices.Server;
using System.Configuration;
using Microsoft.Data.Extensions;
using NPOI.SS.Formula.Functions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
using Devart.Data.Oracle;
using System.Data;
using System.Transactions;
using Sunlight.Silverlight.Dcs.Web.Entities;
using System.Text;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class FactoryPurchacePriceAch : DcsSerivceAchieveBase {
        public FactoryPurchacePriceAch(DcsDomainService domainService)
            : base(domainService) {
        }

        public IEnumerable<VirtualFactoryPurchacePrice> 查询SAP采购价(string supplierCode, string supplierName, string sparePartCode, string sparePartName,string hyCode, DateTime? bCreateTime, DateTime? eCreateTime, bool? isGenerate) {
            string sql = "select a.id,a.suppliercode,b.id as supplierId,b.name as suppliername,a.sapsparecode as sparepartcode,c.id as SparePartId,c.name as sparepartname,a.contractprice,a.validfrom,a.validto,a.usercode,a.createttime as createtime,nvl(a.IsGenerate,0) as IsGenerate,a.HYCode,(nvl(a.IsTemp,0) + 1) as IsTemp, (case when a.source ='OA' then 1 else a.Rate end) as Rate,d.IsOrderable,( case when a.sapsparecode like 'W%' or a.sapsparecode like 'w%' then c.ReferenceCode else a.sapsparecode end) as SupplierPartCode,a.LimitQty,a.Source from FactoryPurchacePrice_tmp a left join partssupplier b on a.suppliercode = b.code and b.status =1 left join sparepart c on c.code = a.sapsparecode left join PartsBranch d on c.id = d.partid and d.status = 1 where 1=1 ";
            if (!string.IsNullOrEmpty(supplierCode)) {
                sql += " and a.suppliercode = '" + supplierCode + "'";
            }
            if (!string.IsNullOrEmpty(supplierName)) {
                sql += " and b.name = '" + supplierName + "'";
            }
            if (!string.IsNullOrEmpty(sparePartCode)) { 
                    var spareparts = sparePartCode.Split(','); 
                    if (spareparts.Length == 1) { 
                        var sparepartcode = spareparts[0]; 
                        sql += " and a.sapsparecode = '" + sparePartCode + "'";
                    } else {
                        for (int i = 0; i < spareparts.Length; i++) {
                            spareparts[i] = "'" + spareparts[i] + "'";
                        }
                        string codes = string.Join(",", spareparts); 
                        sql += " and a.sapsparecode in (" + codes + ")";
                    }
                } 
            if (!string.IsNullOrEmpty(sparePartName)) {
                sql += " and c.name like '%" + sparePartName + "%'";
            }
             if (!string.IsNullOrEmpty(hyCode)) {
                sql += " and a.hycode like '%" + hyCode + "%'";
            }
            if (bCreateTime.HasValue) {
                sql += " and a.CreatetTime>= to_date('" + bCreateTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }
            if (eCreateTime.HasValue) {
                sql += " and a.CreatetTime<= to_date('" + eCreateTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }
            if (isGenerate.HasValue) {
                if ((bool)isGenerate)
                    sql += " and a.IsGenerate = 1";
                else
                    sql += " and (a.IsGenerate = 0 or a.IsGenerate is null)";
            }
            sql += " order by a.sapsparecode ";
            var search = ObjectContext.ExecuteStoreQuery<VirtualFactoryPurchacePrice>(sql).ToList();
            return search;
        }
        public IEnumerable<FactoryPurchacePriceForHand> 查询采购价待处理(string supplierCode, string supplierName, string sparePartCode, string sparePartName, int? status, bool? isOrderable) {
            string sql = @"select * from (select 1     as status,
                               sp.id as sparepartId,       
                               sp.code as SparePartCode,
                               sp.name as SparePartName,
                               null as SupplierId,
                               null    as SupplierCode,
                               null    as SupplierName,
                               0       as IsPrimary,
                               null    as ContractPrice,
                               null    as PriceType,
                               0       as rate,
                               null    as validfrom,
                               null    as validto,
                               null    as source,
                               null    as Id,pb.ReferenceCode as HYCode,
                               ps.name as PartsSupplierNamePri,ps.code as PartsSupplierCodePri,pb.IsOrderable
                          from sparepart sp left join partsbranch pb on sp.id=pb.partid
                          left join PartsSupplierRelation psr on psr.PartId=sp.id and psr.IsPrimary=1 and psr.status=1
                          left join  PartsSupplier ps on psr.SupplierId=ps.id
                         where (not exists (select 1
                                              from PartsPurchasePricing pp
                                              left join PartsSupplierRelation psr
                                                on psr.partid = pp.partid
                                               and psr.supplierid = pp.PartsSupplierId
                                               and psr.status <> 99
                                               and psr.partssalescategoryid = pp.partssalescategoryid
                                             where pp.partid = sp.id
                                               and pp.status = 2
                                               and pp.validfrom < sysdate
                                               and pp.validto > sysdate
                                               and psr.isprimary = 1) or not exists
                                (select 1
                                   from PartsPurchasePricing pp
                                  where pp.partid = sp.id
                                    and pp.status = 2))
                           and not exists (select *
                                  from FactoryPurchacePrice_tmp ft
                                 where ft.sapsparecode = sp.code
                                   and ft.validfrom < sysdate
                                   and ft.validto > sysdate
                                   and nvl(ft.IsGenerate, 0) = 0)
                           and sp.parttype = 2  and pb.IsOrderable=1 {0}
                        union all
                        select status,
                               sparepartId,
                               SparePartCode,
                               SparePartName ,
                               SupplierId,
                               SupplierCode,
                               SupplierName,
                               IsPrimary,
                               contractprice,
                               IsTemp,
                               Rate,
                               validfrom,
                               validto,
                               source,
                               Id,HYCode,
                               PartsSupplierNamePri,PartsSupplierCodePri,IsOrderable
                          from (select 2 as status,
                                       c.id as sparepartId,
                                       c.code as SparePartCode,
                                       c.name as SparePartName,
                                       b.id as SupplierId,
                                       a.suppliercode as SupplierCode,
                                       b.name as SupplierName,
                                       psr.IsPrimary,
                                       a.contractprice,
                                       (nvl(a.IsTemp, 0) + 1) as IsTemp,
                                       (case
                                         when a.source = 'OA' then
                                          1
                                         else
                                          a.Rate
                                       end) as Rate,
                                       a.validfrom,
                                       a.validto,
                                       a.source,
                                       row_number() over(partition by a.sapsparecode order by a.IsTemp asc, a.contractprice asc,a.source) as ydy,
                                       a.id as Id,pb.ReferenceCode as HYCode,
                                        ps.name as PartsSupplierNamePri,ps.code as PartsSupplierCodePri,pb.IsOrderable
                                  from FactoryPurchacePrice_tmp a   
                                  left join partssupplier b
                                    on a.suppliercode = b.code
                                   and b.status = 1
                                  join sparepart c
                                    on c.code = a.sapsparecode left join partsbranch pb on c.id=pb.partid 
                                  join PartsBranch pb on c.id =pb.PartId
                                  left join PartsSupplierRelation psr
                                    on psr.partid = c.id
                                   and psr.supplierid = b.id
                                   and psr.status <> 99 
                                    left join PartsSupplierRelation psrt on psrt.PartId=c.id and psrt.IsPrimary=1 and psrt.status=1
                                   left join  PartsSupplier ps on psrt.SupplierId=ps.id
                                 where nvl(a.isgenerate, 0) = 0
                                   and a.validfrom < sysdate
                                   and a.validto > sysdate
                                   and (not exists
                                        (select 1
                                           from PartsPurchasePricing pp
                                           left join PartsSupplierRelation psr
                                             on psr.partid = pp.partid
                                            and psr.supplierid = pp.PartsSupplierId
                                            and psr.status <> 99
                                            and psr.partssalescategoryid = pp.partssalescategoryid
                                          where pp.partid = c.id
                                            and pp.status = 2
                                            and pp.validfrom < sysdate
                                            and pp.validto > sysdate
                                            and psr.isprimary = 1) or not exists
                                        (select 1
                                           from PartsPurchasePricing pp
                                          where pp.partid = c.id
                                            and pp.status = 2))
                              {1})
                         where ydy = 1
                        union
                        select status,
                               sparepartId,
                               SparePartCode,
                               SparePartName,
                               SupplierId,
                               SupplierCode,
                               SupplierName,
                               IsPrimary,
                               contractprice,
                               IsTemp,
                               Rate,
                               validfrom,
                               validto,
                               source,
                               Id,HYCode,
                               PartsSupplierNamePri,PartsSupplierCodePri,IsOrderable
                          from (select 3 as status,
                                       c.id as sparepartId,
                                       c.code as  SparePartCode,
                                       c.name as SparePartName,
                                       b.id as SupplierId,
                                       a.suppliercode as SupplierCode,
                                       b.name as SupplierName,
                                       psr.IsPrimary,
                                       a.contractprice,
                                       (nvl(a.IsTemp, 0) + 1) as IsTemp,
                                       (case
                                         when a.source = 'OA' then
                                          1
                                         else
                                          a.Rate
                                       end) as Rate,
                                       a.validfrom,
                                       a.validto,
                                       a.source,
                                       row_number() over(partition by a.sapsparecode order by a.contractprice asc,a.source) as ydy,
                                       a.id as Id,c.ReferenceCode as HYCode,
                                        ps.name as PartsSupplierNamePri,ps.code as PartsSupplierCodePri,pb.IsOrderable
                                  from FactoryPurchacePrice_tmp a
                                  left join partssupplier b
                                    on a.suppliercode = b.code
                                   and b.status = 1
                                  join sparepart c
                                    on c.code = a.sapsparecode left join partsbranch pb on c.id=pb.partid
                                   join PartsBranch pb on c.id =pb.PartId
                                  left join PartsSupplierRelation psr
                                    on psr.partid = c.id
                                   and psr.supplierid = b.id
                                   and psr.status <> 99
                                    left join PartsSupplierRelation psrt on psrt.PartId=c.id and psrt.IsPrimary=1 and psrt.status=1
                                   left join  PartsSupplier ps on psrt.SupplierId=ps.id
                                 where nvl(a.isgenerate, 0) = 0
                                   and a.validfrom < sysdate
                                   and a.validto > sysdate
                                   and (nvl(a.IsTemp, 0) + 1) = 1
                                   and exists
                                 (select 1
                                          from PartsPurchasePricing pp
                                          left join PartsSupplierRelation psr
                                            on psr.partid = pp.partid
                                           and psr.supplierid = pp.PartsSupplierId
                                           and psr.status <> 99
                                           and psr.partssalescategoryid = pp.partssalescategoryid
                                         where pp.partid = c.id
                                           and pp.status = 2
                                           and pp.pricetype = 2
                                           and pp.validfrom < sysdate
                                           and pp.validto > sysdate
                                           and psr.isprimary = 1) {1})
                         where ydy = 1
                        union
                        select status,
                               sparepartId,
                               SparePartCode,
                               SparePartName,
                               SupplierId,
                               SupplierCode,
                               SupplierName,
                               IsPrimary,
                               contractprice,
                               IsTemp,
                               Rate,
                               validfrom,
                               validto,
                               source,
                               Id,HYCode,
                                PartsSupplierNamePri,PartsSupplierCodePri,IsOrderable
                          from (select 4 as status,
                                       c.id as sparepartId,
                                        c.code as SparePartCode,
                                       c.name as SparePartName,
                                       b.id as SupplierId,
                                       a.suppliercode as SupplierCode,
                                       b.name as SupplierName,
                                       psr.IsPrimary,
                                       a.contractprice,
                                       (nvl(a.IsTemp, 0) + 1) as IsTemp,
                                       (case
                                         when a.source = 'OA' then
                                          1
                                         else
                                          a.Rate
                                       end) as Rate,
                                       a.validfrom,
                                       a.validto,
                                       a.source,
                                       row_number() over(partition by a.sapsparecode order by a.contractprice asc,a.source) as ydy,
                                       a.id as Id,c.ReferenceCode as HYCode,
                                      ps.name as PartsSupplierNamePri,ps.code as PartsSupplierCodePri,pb.IsOrderable
                                  from FactoryPurchacePrice_tmp a
                                  left join partssupplier b
                                    on a.suppliercode = b.code
                                   and b.status = 1
                                  join sparepart c
                                    on c.code = a.sapsparecode  left join partsbranch pb on c.id=pb.partid
                                  join PartsBranch pb on c.id= pb.PartId
                                  left join PartsSupplierRelation psr
                                    on psr.partid = c.id
                                   and psr.supplierid = b.id
                                   and psr.status <> 99
                                    left join PartsSupplierRelation psrt on psrt.PartId=c.id and psrt.IsPrimary=1 and psrt.status=1
                                   left join  PartsSupplier ps on psrt.SupplierId=ps.id
                                 where nvl(a.isgenerate, 0) = 0
                                   and a.validfrom < sysdate
                                   and a.validto > sysdate
                                   and (nvl(a.IsTemp, 0) + 1) = 1
                                   and exists
                                 (select 1
                                          from PartsPurchasePricing pp
                                          left join PartsSupplierRelation psr
                                            on psr.partid = pp.partid
                                           and psr.supplierid = pp.PartsSupplierId
                                           and psr.status <> 99
                                           and psr.partssalescategoryid = pp.partssalescategoryid
                                         where pp.partid = c.id
                                           and pp.status = 2
                                           and pp.pricetype = 1
                                           and a.contractprice < pp.PurchasePrice
                                           and pp.validfrom < sysdate
                                           and pp.validto > sysdate
                                           and psr.isprimary = 1){1})
                         where ydy = 1
                        ) pp where 1=1 {2}";
            var search0 = new StringBuilder();
            var search1 = new StringBuilder();
            var search2 = new StringBuilder();
            if(!string.IsNullOrEmpty(supplierCode)) {
                search0.Append(" and 1<>1 ");
                search1.Append(" and b.code = '" + supplierCode + "'");                              
            }
            if(!string.IsNullOrEmpty(supplierName)) {
                search0.Append(" and 1<>1");
                search1.Append(" and b.name = '" + supplierName + "'");  
            }
            if(!string.IsNullOrEmpty(sparePartCode)) {
                var spareparts = sparePartCode.Split(',');
                if(spareparts.Length == 1) {
                    var sparepartcode = spareparts[0];
                    search0.Append(" and sp.code = '" + sparePartCode + "'");
                    search1.Append(" and c.code = '" + sparePartCode + "'");    
                } else {
                    for(int i = 0; i < spareparts.Length; i++) {
                        spareparts[i] = "'" + spareparts[i] + "'";
                    }
                    string codes = string.Join(",", spareparts);
                    search0.Append(" and sp.code in (" + codes + ")");
                    search1.Append("  and c.code in (" + codes + ")");  
                }
            }
            if(!string.IsNullOrEmpty(sparePartName)) {
                search0.Append(" and sp.name = '" + sparePartName + "'");
                  search1.Append(" and c.name like '%" + sparePartName + "%'");    
            }
            if(status.HasValue) {
                search2.Append(" and pp.status="+status);
            }
            if(isOrderable.HasValue&& isOrderable.Value) {
                search0.Append(" and pb.IsOrderable=1 ");
                search1.Append(" and pb.IsOrderable=1 ");
            } if(isOrderable.HasValue && !isOrderable.Value) {
                search0.Append(" and pb.IsOrderable=0 ");
                search1.Append(" and pb.IsOrderable=0 ");
            }
            sql = string.Format(sql, search0, search1, search2);
            var search = ObjectContext.ExecuteStoreQuery<FactoryPurchacePriceForHand>(sql).ToList();
            return search;
        }
        public void 生成采购价变更申请(VirtualFactoryPurchacePrice[] virtualFactoryPurchacePrices) {
            if (virtualFactoryPurchacePrices.Any()) {
                var user = Utils.GetCurrentUserInfo();
                var partsSalesCategory = ObjectContext.PartsSalesCategories.FirstOrDefault();
                if (partsSalesCategory == null)
                    throw new ValidationException(ErrorStrings.FactoryPurchacePrice_Validation1);
                foreach (var validate in virtualFactoryPurchacePrices) { 
                    if (validate.SparePartId == default(int))
                        throw new ValidationException(string.Format(ErrorStrings.FactoryPurchacePrice_Validation2,validate.SparePartCode ));
                    if (validate.SupplierId == default(int))
                        throw new ValidationException(string.Format(ErrorStrings.FactoryPurchacePrice_Validation3,validate.SupplierCode));
                }
                //校验配件和供应商 不能重复
                if(virtualFactoryPurchacePrices.GroupBy(x => new {
                    x.SparePartId,
                    x.SupplierId
                }).Where(x => x.Count() > 1).Any(sum => sum.Any())) {
                    throw new ValidationException(ErrorStrings.FactoryPurchacePrice_Validation4);
                };

                var partsPurchasePricingChange = new PartsPurchasePricingChange();
                partsPurchasePricingChange.BranchId = user.EnterpriseId;
                partsPurchasePricingChange.PartsSalesCategoryId = partsSalesCategory.Id;
                partsPurchasePricingChange.PartsSalesCategoryName = partsSalesCategory.Name;
                partsPurchasePricingChange.Status = (int)DcsPartsPurchasePricingChangeStatus.新增;

                foreach (var item in virtualFactoryPurchacePrices) {
                    var partsPurchasePricingDetail = new PartsPurchasePricingDetail();
                    partsPurchasePricingDetail.PartId = item.SparePartId;
                    partsPurchasePricingDetail.PartCode = item.SparePartCode;
                    partsPurchasePricingDetail.PartName = item.SparePartName;
                    partsPurchasePricingDetail.SupplierId = item.SupplierId;
                    partsPurchasePricingDetail.SupplierCode = item.SupplierCode;
                    partsPurchasePricingDetail.SupplierName = item.SupplierName;
                    partsPurchasePricingDetail.PriceType = item.IsTemp ?? (int)DcsPurchasePriceType.正式价格;
                    partsPurchasePricingDetail.ValidFrom = item.ValidFrom ?? DateTime.Now;
                    partsPurchasePricingDetail.ValidTo = item.ValidTo ?? DateTime.Now;
                    partsPurchasePricingDetail.ValidTo = new DateTime(partsPurchasePricingDetail.ValidTo.Year, partsPurchasePricingDetail.ValidTo.Month, partsPurchasePricingDetail.ValidTo.Day, 23, 59, 59);
                    partsPurchasePricingDetail.Price = item.ContractPrice ?? 0m;
                    if (partsPurchasePricingDetail.Price <= 0m) { 
                        throw new ValidationException(string.Format(ErrorStrings.FactoryPurchacePrice_Validation5,item.SparePartCode));
                    }
                    if (partsPurchasePricingDetail.ValidFrom >= partsPurchasePricingDetail.ValidTo) { 
                        throw new ValidationException(string.Format(ErrorStrings.FactoryPurchacePrice_Validation6,item.SparePartCode));
                    }
                    partsPurchasePricingDetail.SupplierPartCode = item.SupplierPartCode;
                    partsPurchasePricingDetail.IfPurchasable = true;
                    partsPurchasePricingDetail.DataSource = item.Source == "SAP" ? (int)DcsPurchasePricingDataSource.SAP : (int)DcsPurchasePricingDataSource.OA;
                    partsPurchasePricingChange.PartsPurchasePricingDetails.Add(partsPurchasePricingDetail);
                }

                var partIds = partsPurchasePricingChange.PartsPurchasePricingDetails.Select(r => r.PartId).Distinct().ToArray();
                var supplierIds = partsPurchasePricingChange.PartsPurchasePricingDetails.Select(r => r.SupplierId).Distinct().ToArray();
                var allPartsSupplierRelations = ObjectContext.PartsSupplierRelations.Where(r => r.PartsSalesCategoryId == partsPurchasePricingChange.PartsSalesCategoryId && partIds.Contains(r.PartId) && r.Status != (int)DcsBaseDataStatus.作废).ToArray();
                var partsSupplierRelations = allPartsSupplierRelations.Where(r => supplierIds.Contains(r.SupplierId));
                var allPartsPurchasePricings = ObjectContext.PartsPurchasePricings.Where(r => r.Status != (int)DcsBaseDataStatus.作废 && partIds.Contains(r.PartId) && r.PartsSalesCategoryId == partsPurchasePricingChange.PartsSalesCategoryId).Include("PartsSupplier").ToArray();

                foreach (var detail in partsPurchasePricingChange.PartsPurchasePricingDetails) {
                    //赋值是否首选，如果这个配件这个供应商不存在价格，是否首选= 是  否则 是否首选 = 否
                    var thisPrice = allPartsPurchasePricings.Where(r => detail.PartId == r.PartId && r.PartsSupplierId == detail.SupplierId && DateTime.Now >= r.ValidFrom && DateTime.Now <= r.ValidTo).FirstOrDefault();
                    if (thisPrice == null) {
                        detail.IsPrimary = true;
                    }else{
                        detail.IsPrimary = false;
                    }
                    //配件参考价取首选供应商的价格
                    var primarySupplier = allPartsSupplierRelations.Where(r => r.PartId == detail.PartId && r.IsPrimary == true).FirstOrDefault();
                    if (primarySupplier != null) {
                        var primaryPurchasePrise = allPartsPurchasePricings.FirstOrDefault(r => r.PartId == detail.PartId && r.PartsSupplierId == primarySupplier.SupplierId && DateTime.Now >= r.ValidFrom && DateTime.Now <= r.ValidTo);
                        detail.ReferencePrice = primaryPurchasePrise == null ?  0m : primaryPurchasePrise.PurchasePrice;
                        //原首选供应商
                        detail.OldPriSupplierId = primarySupplier.SupplierId;
                        detail.OldPriSupplierCode = primarySupplier.PartsSupplier.Code;
                        detail.OldPriSupplierName = primarySupplier.PartsSupplier.Name;
                        if(primaryPurchasePrise != null){
                            detail.OldPriPrice = primaryPurchasePrise.PurchasePrice;
                            detail.OldPriPriceType = primaryPurchasePrise.PriceType;
                        }
                    } else { 
                        detail.ReferencePrice = 0m;
                    }

                    //其他供应商的价格
                    var partsPurchasePricings = allPartsPurchasePricings.Where(r => detail.PartId == r.PartId && r.PartsSupplierId != detail.SupplierId && DateTime.Now >= r.ValidFrom && DateTime.Now <= r.ValidTo);
                    if (partsPurchasePricings != null && partsPurchasePricings.Any()) {
                        var partsPurchasePricing = partsPurchasePricings.FirstOrDefault(r => r.PurchasePrice == partsPurchasePricings.Min(d => d.PurchasePrice));
                        //如果本身的价格比最低价低，不赋值最低采购价，最低价供应商，最低价是否首选 三个字段
                        if ((decimal)detail.Price >= partsPurchasePricing.PurchasePrice) {
                            detail.MinPurchasePrice = partsPurchasePricing.PurchasePrice;
                            detail.MinPriceSupplierId = partsPurchasePricing.PartsSupplierId;
                            detail.MinPriceSupplier = partsPurchasePricing.PartsSupplier.Name;

                            var partsSupplierRelation = partsSupplierRelations.Where(r => r.PartId == detail.PartId && r.SupplierId == detail.SupplierId).FirstOrDefault();
                            if (partsSupplierRelation != null) {
                                detail.IsPrimaryMinPrice = partsSupplierRelation.IsPrimary ?? false;
                                detail.SupplierPartCode = partsSupplierRelation.SupplierPartCode;
                            }
                        }
                    }
                }
                //同一个配件，最多只能一个首选..
                foreach (var partId in partIds) {
                    var details = partsPurchasePricingChange.PartsPurchasePricingDetails.Where(r => r.IsPrimary == true && r.PartId == partId).ToArray();
                    if (details != null && details.Count() > 1) {
                        var detail = details.FirstOrDefault();
                        partsPurchasePricingChange.PartsPurchasePricingDetails.Where(r => r.PartId == partId).ForEach(d => d.IsPrimary = false);
                        var primary = partsPurchasePricingChange.PartsPurchasePricingDetails.FirstOrDefault(r => r.PartId == partId && r.SupplierId == detail.SupplierId);
                        primary.IsPrimary = true;
                    }
                }
                partsPurchasePricingChange.BranchId = user.EnterpriseId;
                partsPurchasePricingChange.CreatorId = user.Id;
                partsPurchasePricingChange.CreatorName = user.Name;
                partsPurchasePricingChange.CreateTime = DateTime.Now;
                partsPurchasePricingChange.Code = CodeGenerator.Generate("PartsPurchasePricingChange_SAP");
                InsertToDatabase(partsPurchasePricingChange);

                //更新SAP工厂采购价,是否已生成 = 是
                var ids = virtualFactoryPurchacePrices.Select(r => r.Id);
                var datas = ObjectContext.FactoryPurchacePrice_Tmp.Where(r => ids.Contains(r.Id));
                foreach (var data in datas) {
                    if (data.IsGenerate != true) {
                        data.IsGenerate = true;
                        UpdateToDatabase(data);
                    }
                }
                ObjectContext.SaveChanges();
            }
        }
        public void 待处理生成采购价变更申请(FactoryPurchacePriceForHand[] virtualFactoryPurchacePrices) {
            if(virtualFactoryPurchacePrices.Any()) {
                var user = Utils.GetCurrentUserInfo();
                var partsSalesCategory = ObjectContext.PartsSalesCategories.FirstOrDefault();
                if(partsSalesCategory == null)
                    throw new ValidationException(ErrorStrings.FactoryPurchacePrice_Validation1);
                foreach(var validate in virtualFactoryPurchacePrices) {
                    if(validate.SparePartId == default(int))
                        throw new ValidationException(string.Format(ErrorStrings.FactoryPurchacePrice_Validation2, validate.SparePartCode));
                    if(validate.SupplierId == default(int))
                        throw new ValidationException(string.Format(ErrorStrings.FactoryPurchacePrice_Validation3, validate.SupplierCode));
                }
                //校验配件和供应商 不能重复
                if(virtualFactoryPurchacePrices.GroupBy(x => new {
                    x.SparePartId,
                    x.SupplierId
                }).Where(x => x.Count() > 1).Any(sum => sum.Any())) {
                    throw new ValidationException(ErrorStrings.FactoryPurchacePrice_Validation4);
                };

                var partsPurchasePricingChange = new PartsPurchasePricingChange();
                partsPurchasePricingChange.BranchId = user.EnterpriseId;
                partsPurchasePricingChange.PartsSalesCategoryId = partsSalesCategory.Id;
                partsPurchasePricingChange.PartsSalesCategoryName = partsSalesCategory.Name;
                partsPurchasePricingChange.Status = (int)DcsPartsPurchasePricingChangeStatus.新增;

                foreach(var item in virtualFactoryPurchacePrices) {
                    var partsPurchasePricingDetail = new PartsPurchasePricingDetail();
                    partsPurchasePricingDetail.PartId = item.SparePartId;
                    partsPurchasePricingDetail.PartCode = item.SparePartCode;
                    partsPurchasePricingDetail.PartName = item.SparePartName;
                    partsPurchasePricingDetail.SupplierId = item.SupplierId;
                    partsPurchasePricingDetail.SupplierCode = item.SupplierCode;
                    partsPurchasePricingDetail.SupplierName = item.SupplierName;
                    partsPurchasePricingDetail.PriceType = item.PriceType;
                    partsPurchasePricingDetail.ValidFrom = item.ValidFrom ?? DateTime.Now;
                    partsPurchasePricingDetail.ValidTo = item.ValidTo ?? DateTime.Now;
                    partsPurchasePricingDetail.ValidTo = new DateTime(partsPurchasePricingDetail.ValidTo.Year, partsPurchasePricingDetail.ValidTo.Month, partsPurchasePricingDetail.ValidTo.Day, 23, 59, 59);
                    partsPurchasePricingDetail.Price = item.ContractPrice ?? 0m;
                    if(partsPurchasePricingDetail.Price <= 0m) {
                        throw new ValidationException(string.Format(ErrorStrings.FactoryPurchacePrice_Validation5, item.SparePartCode));
                    }
                    if(partsPurchasePricingDetail.ValidFrom >= partsPurchasePricingDetail.ValidTo) {
                        throw new ValidationException(string.Format(ErrorStrings.FactoryPurchacePrice_Validation6, item.SparePartCode));
                    }
                    partsPurchasePricingDetail.SupplierPartCode = partsPurchasePricingDetail.PartCode.ToLower().Contains("w") ==true? item.HYCode:partsPurchasePricingDetail.PartCode;
                    partsPurchasePricingDetail.IfPurchasable = true;
                    partsPurchasePricingDetail.DataSource = item.Source == "SAP" ? (int)DcsPurchasePricingDataSource.SAP : (int)DcsPurchasePricingDataSource.OA;
                    partsPurchasePricingChange.PartsPurchasePricingDetails.Add(partsPurchasePricingDetail);
                }

                var partIds = partsPurchasePricingChange.PartsPurchasePricingDetails.Select(r => r.PartId).Distinct().ToArray();
                var supplierIds = partsPurchasePricingChange.PartsPurchasePricingDetails.Select(r => r.SupplierId).Distinct().ToArray();
                var allPartsSupplierRelations = ObjectContext.PartsSupplierRelations.Where(r => r.PartsSalesCategoryId == partsPurchasePricingChange.PartsSalesCategoryId && partIds.Contains(r.PartId) && r.Status != (int)DcsBaseDataStatus.作废).ToArray();
                var partsSupplierRelations = allPartsSupplierRelations.Where(r => supplierIds.Contains(r.SupplierId));
                var allPartsPurchasePricings = ObjectContext.PartsPurchasePricings.Where(r => r.Status != (int)DcsBaseDataStatus.作废 && partIds.Contains(r.PartId) && r.PartsSalesCategoryId == partsPurchasePricingChange.PartsSalesCategoryId).Include("PartsSupplier").ToArray();

                foreach(var detail in partsPurchasePricingChange.PartsPurchasePricingDetails) {
                    //赋值是否首选，如果这个配件这个供应商不存在价格，是否首选= 是  否则 是否首选 = 否
                    var thisPrice = allPartsPurchasePricings.Where(r => detail.PartId == r.PartId && r.PartsSupplierId == detail.SupplierId && DateTime.Now >= r.ValidFrom && DateTime.Now <= r.ValidTo).FirstOrDefault();
                    if(thisPrice == null) {
                        detail.IsPrimary = true;
                    } else {
                        detail.IsPrimary = false;
                    }
                    //配件参考价取首选供应商的价格
                    var primarySupplier = allPartsSupplierRelations.Where(r => r.PartId == detail.PartId && r.IsPrimary == true).FirstOrDefault();
                    if(primarySupplier != null) {
                        var primaryPurchasePrise = allPartsPurchasePricings.FirstOrDefault(r => r.PartId == detail.PartId && r.PartsSupplierId == primarySupplier.SupplierId && DateTime.Now >= r.ValidFrom && DateTime.Now <= r.ValidTo);
                        detail.ReferencePrice = primaryPurchasePrise == null ? 0m : primaryPurchasePrise.PurchasePrice;
                        //原首选供应商
                        detail.OldPriSupplierId = primarySupplier.SupplierId;
                        detail.OldPriSupplierCode = primarySupplier.PartsSupplier.Code;
                        detail.OldPriSupplierName = primarySupplier.PartsSupplier.Name;
                        if(primaryPurchasePrise != null) {
                            detail.OldPriPrice = primaryPurchasePrise.PurchasePrice;
                            detail.OldPriPriceType = primaryPurchasePrise.PriceType;
                        }
                    } else {
                        detail.ReferencePrice = 0m;
                    }

                    //其他供应商的价格
                    var partsPurchasePricings = allPartsPurchasePricings.Where(r => detail.PartId == r.PartId && r.PartsSupplierId != detail.SupplierId && DateTime.Now >= r.ValidFrom && DateTime.Now <= r.ValidTo);
                    if(partsPurchasePricings != null && partsPurchasePricings.Any()) {
                        var partsPurchasePricing = partsPurchasePricings.FirstOrDefault(r => r.PurchasePrice == partsPurchasePricings.Min(d => d.PurchasePrice));
                        //如果本身的价格比最低价低，不赋值最低采购价，最低价供应商，最低价是否首选 三个字段
                        if((decimal)detail.Price >= partsPurchasePricing.PurchasePrice) {
                            detail.MinPurchasePrice = partsPurchasePricing.PurchasePrice;
                            detail.MinPriceSupplierId = partsPurchasePricing.PartsSupplierId;
                            detail.MinPriceSupplier = partsPurchasePricing.PartsSupplier.Name;

                            var partsSupplierRelation = partsSupplierRelations.Where(r => r.PartId == detail.PartId && r.SupplierId == detail.SupplierId).FirstOrDefault();
                            if(partsSupplierRelation != null) {
                                detail.IsPrimaryMinPrice = partsSupplierRelation.IsPrimary ?? false;
                                detail.SupplierPartCode = partsSupplierRelation.SupplierPartCode;
                            }
                        }
                    }
                }
                //同一个配件，最多只能一个首选..
                foreach(var partId in partIds) {
                    var details = partsPurchasePricingChange.PartsPurchasePricingDetails.Where(r => r.IsPrimary == true && r.PartId == partId).ToArray();
                    if(details != null && details.Count() > 1) {
                        var detail = details.FirstOrDefault();
                        partsPurchasePricingChange.PartsPurchasePricingDetails.Where(r => r.PartId == partId).ForEach(d => d.IsPrimary = false);
                        var primary = partsPurchasePricingChange.PartsPurchasePricingDetails.FirstOrDefault(r => r.PartId == partId && r.SupplierId == detail.SupplierId);
                        primary.IsPrimary = true;
                    }
                }
                partsPurchasePricingChange.BranchId = user.EnterpriseId;
                partsPurchasePricingChange.CreatorId = user.Id;
                partsPurchasePricingChange.CreatorName = user.Name;
                partsPurchasePricingChange.CreateTime = DateTime.Now;
                partsPurchasePricingChange.Code = CodeGenerator.Generate("PartsPurchasePricingChange_SAP");
                InsertToDatabase(partsPurchasePricingChange);

                //更新SAP工厂采购价,是否已生成 = 是
                var ids = virtualFactoryPurchacePrices.Select(r => r.Id);
                var datas = ObjectContext.FactoryPurchacePrice_Tmp.Where(r => ids.Contains(r.Id));
                foreach(var data in datas) {
                    if(data.IsGenerate != true) {
                        data.IsGenerate = true;
                        UpdateToDatabase(data);
                    }
                }
                ObjectContext.SaveChanges();
            }
        }

    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:11:30
        //原分布类的函数全部转移到Ach                                                               

        [Query(HasSideEffects = true)]
        public IEnumerable<VirtualFactoryPurchacePrice> 查询SAP采购价(string supplierCode, string supplierName, string sparePartCode, string sparePartName,string hyCode, DateTime? bCreateTime, DateTime? eCreateTime, bool? isGenerate) {
            return new FactoryPurchacePriceAch(this).查询SAP采购价(supplierCode, supplierName, sparePartCode, sparePartName,hyCode, bCreateTime, eCreateTime, isGenerate);
        }

        [Invoke(HasSideEffects = true)]
         public void 生成采购价变更申请(VirtualFactoryPurchacePrice[] virtualFactoryPurchacePrices) {
            new FactoryPurchacePriceAch(this).生成采购价变更申请(virtualFactoryPurchacePrices);
        }
         [Query(HasSideEffects = true)]
        public IEnumerable<FactoryPurchacePriceForHand> 查询采购价待处理(string supplierCode, string supplierName, string sparePartCode, string sparePartName, int? status, bool? isOrderable) {
            return new FactoryPurchacePriceAch(this).查询采购价待处理(supplierCode, supplierName, sparePartCode, sparePartName, status, isOrderable);

        }
        [Invoke(HasSideEffects = true)]
        public void 待处理生成采购价变更申请(FactoryPurchacePriceForHand[] virtualFactoryPurchacePrices) {
            new FactoryPurchacePriceAch(this).待处理生成采购价变更申请(virtualFactoryPurchacePrices);
        }
    }
}
