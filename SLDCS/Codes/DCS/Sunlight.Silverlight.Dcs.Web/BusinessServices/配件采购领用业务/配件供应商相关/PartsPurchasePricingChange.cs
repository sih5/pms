﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Data.Objects.DataClasses;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.ServiceModel.DomainServices.Server;
using System.Configuration;
using Microsoft.Data.Extensions;
using NPOI.SS.Formula.Functions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
using Devart.Data.Oracle;
using System.Data;
using System.Transactions;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsPurchasePricingChangeAch : DcsSerivceAchieveBase {
        public PartsPurchasePricingChangeAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 生成配件采购价格变更申请单(PartsPurchasePricingChange partsPurchasePricingChange) {
            //var branchstrategy = ObjectContext.Branchstrategies.FirstOrDefault(r => r.BranchId == partsPurchasePricingChange.BranchId && r.Status != (int)DcsBaseDataStatus.作废);
            //if(branchstrategy == null)
            //    throw new ValidationException(ErrorStrings.PartsPurchasePricingChange_Validation5);

            var salesCenterstrategy = this.ObjectContext.SalesCenterstrategies.SingleOrDefault(r => r.PartsSalesCategoryId == partsPurchasePricingChange.PartsSalesCategoryId && r.Status == (int)DcsBaseDataStatus.有效);
            if(salesCenterstrategy == null)
                throw new ValidationException(ErrorStrings.SupplierShippingOrder_Validation31);
            if(salesCenterstrategy.PartsPurchasePricingStrategy == (int)DcsBranchstrategyPartsPurchasePricingStrategy.先定价) {
                foreach(var partsPurchasePricingDetail in partsPurchasePricingChange.PartsPurchasePricingDetails) {
                    if(partsPurchasePricingDetail.ValidFrom < System.DateTime.Now.Date)
                        throw new ValidationException(string.Format(ErrorStrings.PartsPurchasePricingChange_Validation6, partsPurchasePricingDetail.PartCode));
                }
            }
        }


        public void 生成配件采购价格变更申请单营销(PartsPurchasePricingChange partsPurchasePricingChange, List<PartsPurchasePricingDetail> partsPurchasePricingDetails) {
            var partsSalesCategory = partsPurchasePricingDetails.GroupBy(r => r.PartsSalesCategoryName);
            foreach(var item in partsSalesCategory) {
                var details = partsPurchasePricingDetails.Where(r => r.PartsSalesCategoryName == item.Key);
                var partsPurchasePricingChangeCopy = new PartsPurchasePricingChange();
                partsPurchasePricingChangeCopy.Code = partsPurchasePricingChange.Code;
                partsPurchasePricingChangeCopy.BranchId = partsPurchasePricingChange.BranchId;
                partsPurchasePricingChangeCopy.PartsSalesCategoryId = details.FirstOrDefault().PartsSalesCategoryId;
                partsPurchasePricingChangeCopy.PartsSalesCategoryName = item.Key;
                partsPurchasePricingChangeCopy.Status = partsPurchasePricingChange.Status;
                partsPurchasePricingChangeCopy.Tax = partsPurchasePricingChange.Tax;
                partsPurchasePricingChangeCopy.Remark = partsPurchasePricingChange.Remark;
                partsPurchasePricingChangeCopy.PartsPurchasePricingDetails = new EntityCollection<PartsPurchasePricingDetail>();
                foreach(var detail in details) {
                    var partsPurchasePricingDetailCopy = new PartsPurchasePricingDetail();
                    partsPurchasePricingDetailCopy.PartId = detail.PartId;
                    partsPurchasePricingDetailCopy.PartCode = detail.PartCode;
                    partsPurchasePricingDetailCopy.PartName = detail.PartName;
                    partsPurchasePricingDetailCopy.SupplierId = detail.SupplierId;
                    partsPurchasePricingDetailCopy.SupplierCode = detail.SupplierCode;
                    partsPurchasePricingDetailCopy.SupplierName = detail.SupplierName;
                    partsPurchasePricingDetailCopy.PriceType = detail.PriceType;
                    partsPurchasePricingDetailCopy.Price = detail.Price;
                    partsPurchasePricingDetailCopy.ReferencePrice = detail.ReferencePrice;
                    partsPurchasePricingDetailCopy.ValidFrom = detail.ValidFrom;
                    partsPurchasePricingDetailCopy.ValidTo = detail.ValidTo;
                    partsPurchasePricingDetailCopy.Remark = detail.Remark;
                    partsPurchasePricingChangeCopy.PartsPurchasePricingDetails.Add(partsPurchasePricingDetailCopy);
                }
                InsertPartsPurchasePricingChange(partsPurchasePricingChangeCopy);
                this.ObjectContext.SaveChanges();
            }
        }

        public void 提交配件采购价格变更申请单(PartsPurchasePricingChange partsPurchasePricingChange) {
            var dbPartsPurchasePricingChange = ObjectContext.PartsPurchasePricingChanges.Where(v => v.Id == partsPurchasePricingChange.Id && v.Status == (int)DcsPartsPurchasePricingChangeStatus.新增).Include("PartsPurchasePricingDetails").SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if (dbPartsPurchasePricingChange == null)
                throw new ValidationException(ErrorStrings.PartsPurchasePricingChange_Validation9);
            foreach (var detail in dbPartsPurchasePricingChange.PartsPurchasePricingDetails) { 
                if(string.IsNullOrEmpty(detail.SupplierPartCode))
                    throw new ValidationException(string.Format(ErrorStrings.PartsPurchasePricingChange_Validation10,detail.PartCode));
                if(detail.Price <= 0m)
                    throw new ValidationException(string.Format(ErrorStrings.PartsPurchasePricingChange_Validation11,detail.PartCode));
            }
            var partsPurchasePricingChangeDetails = ObjectContext.PartsPurchasePricingDetails.Where(r => r.ParentId == partsPurchasePricingChange.Id).ToArray();
            var partIds = partsPurchasePricingChangeDetails.Select(r => r.PartId).ToArray();
            var supplierIds = partsPurchasePricingChangeDetails.Select(r => r.SupplierId).ToArray();
            var partsSupplierRelationsForParts = ObjectContext.PartsSupplierRelations.Where(r => partIds.Contains(r.PartId) && r.Status != (int)DcsBaseDataStatus.作废 && r.PartsSalesCategoryId == partsPurchasePricingChange.PartsSalesCategoryId).ToArray();
            var partsSupplierRelationsNew = partsSupplierRelationsForParts.Where(r => supplierIds.Contains(r.SupplierId)).ToArray();
            var partsPurchasePricings = ObjectContext.PartsPurchasePricings.Where(r => r.PartsSalesCategoryId == partsPurchasePricingChange.PartsSalesCategoryId && partIds.Contains(r.PartId) && r.Status != (int)DcsBaseDataStatus.作废).ToArray();

            foreach (var detail in partsPurchasePricingChangeDetails) {
                //失效时间<当前时间，是补采购价，不需要校验首选供应商和更改首选供应商
                if(detail.ValidTo < DateTime.Now) {
                    continue;
                }
                //配件所有的供应商
                var relations = partsSupplierRelationsForParts.Where(r => r.PartId == detail.PartId && r.PartsSalesCategoryId == partsPurchasePricingChange.PartsSalesCategoryId).ToList();
                //配件采购价格
                var partsPurchasePricing = partsPurchasePricings.Where(r => r.PartId == detail.PartId && r.PartsSalesCategoryId == partsPurchasePricingChange.PartsSalesCategoryId).FirstOrDefault();
                //配件供应商关系
                var partsSupplierRelation = partsSupplierRelationsNew.Where(r => r.PartId == detail.PartId && r.SupplierId == detail.SupplierId && r.PartsSalesCategoryId == partsPurchasePricingChange.PartsSalesCategoryId).FirstOrDefault();
               
                if (partsPurchasePricing != null) {
                    if ((detail.IsPrimary ?? false) == false && relations.Any()) {
                        var count = relations.Count(r => (r.IsPrimary ?? false) == true);
                        var primaryCount = partsPurchasePricingChangeDetails.Count(r => r.PartId == detail.PartId && r.IsPrimary == true);
                        if (count == 0)
                            throw new ValidationException(string.Format(ErrorStrings.PartsPurchasePricingChange_Validation12, detail.PartCode));
                        if (count == 1 && partsSupplierRelation != null && partsSupplierRelation.IsPrimary == true && primaryCount == 0)
                            throw new ValidationException(string.Format(ErrorStrings.PartsPurchasePricingChange_Validation12, detail.PartCode));
                    }
                }
            }
            var userInfo = Utils.GetCurrentUserInfo();
            //暂时，提交时，状态变成为初审通过
            partsPurchasePricingChange.Status = (int)DcsPartsPurchasePricingChangeStatus.初审通过;
            partsPurchasePricingChange.InitialApproverId = userInfo.Id;
            partsPurchasePricingChange.InitialApproverName = userInfo.Name;
            partsPurchasePricingChange.InitialApproveTime = DateTime.Now;
          //  partsPurchasePricingChange.Status = (int)DcsPartsPurchasePricingChangeStatus.提交;
          
            partsPurchasePricingChange.ModifierId = userInfo.Id;
            partsPurchasePricingChange.ModifierName = userInfo.Name;
            partsPurchasePricingChange.ModifyTime = DateTime.Now;
            UpdateToDatabase(partsPurchasePricingChange);
        }

        public void 初审配件采购价格变更申请单(PartsPurchasePricingChange partsPurchasePricingChange) {
            //审核意见和审核状态
            var approvalId = partsPurchasePricingChange.ApprovalId;

            var dbPartsPurchasePricingChange = ObjectContext.PartsPurchasePricingChanges.Where(v => v.Id == partsPurchasePricingChange.Id && v.Status == (int)DcsPartsPurchasePricingChangeStatus.提交).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbPartsPurchasePricingChange == null)
                throw new ValidationException(ErrorStrings.PartsPurchasePricingChange_Validation2);

            var partsPurchasePricingChangeDetails = ObjectContext.PartsPurchasePricingDetails.Where(r => r.ParentId == partsPurchasePricingChange.Id).ToArray();
            var partIds = partsPurchasePricingChangeDetails.Select(r => r.PartId).ToArray();
            var supplierIds = partsPurchasePricingChangeDetails.Select(r => r.SupplierId).ToArray();
            var partsSupplierRelationsForParts = ObjectContext.PartsSupplierRelations.Where(r => partIds.Contains(r.PartId) && r.Status != (int)DcsBaseDataStatus.作废 && r.PartsSalesCategoryId == partsPurchasePricingChange.PartsSalesCategoryId).ToArray();
            var partsSupplierRelationsNew = partsSupplierRelationsForParts.Where(r => supplierIds.Contains(r.SupplierId)).ToArray();
            var partsPurchasePricings = ObjectContext.PartsPurchasePricings.Where(r => r.PartsSalesCategoryId == partsPurchasePricingChange.PartsSalesCategoryId && partIds.Contains(r.PartId) && r.Status != (int)DcsBaseDataStatus.作废).ToArray();

            foreach (var detail in partsPurchasePricingChangeDetails) {
                //失效时间<当前时间，是补采购价，不需要校验首选供应商和更改首选供应商
                if(detail.ValidTo < DateTime.Now) {
                    continue;
                }
                //配件所有的供应商
                var relations = partsSupplierRelationsForParts.Where(r => r.PartId == detail.PartId && r.PartsSalesCategoryId == partsPurchasePricingChange.PartsSalesCategoryId).ToList();
                //配件采购价格
                var partsPurchasePricing = partsPurchasePricings.Where(r => r.PartId == detail.PartId && r.PartsSalesCategoryId == partsPurchasePricingChange.PartsSalesCategoryId).FirstOrDefault();
                //配件供应商关系
                var partsSupplierRelation = partsSupplierRelationsNew.Where(r => r.PartId == detail.PartId && r.SupplierId == detail.SupplierId && r.PartsSalesCategoryId == partsPurchasePricingChange.PartsSalesCategoryId).FirstOrDefault();
                
                if (partsPurchasePricing != null) {
                    if ((detail.IsPrimary ?? false) == false && relations.Any()) {
                        var count = relations.Count(r => (r.IsPrimary ?? false) == true);
                        var primaryCount = partsPurchasePricingChangeDetails.Count(r => r.PartId == detail.PartId && r.IsPrimary == true);
                        if (count == 0)
                            throw new ValidationException(string.Format(ErrorStrings.PartsPurchasePricingChange_Validation12, detail.PartCode));
                        if (count == 1 && partsSupplierRelation != null && partsSupplierRelation.IsPrimary == true && primaryCount == 0)
                            throw new ValidationException(string.Format(ErrorStrings.PartsPurchasePricingChange_Validation12, detail.PartCode));
                    }
                }
            }

            if(approvalId == 1) {
                partsPurchasePricingChange.Status = (int)DcsPartsPurchasePricingChangeStatus.初审通过;
            } else {
                partsPurchasePricingChange.Status = (int)DcsPartsPurchasePricingChangeStatus.新增;
            }

            var userInfo = Utils.GetCurrentUserInfo();
            partsPurchasePricingChange.InitialApproverId = userInfo.Id;
            partsPurchasePricingChange.InitialApproverName = userInfo.Name;
            partsPurchasePricingChange.InitialApproveTime = DateTime.Now;
        }

         public void 审核配件采购价格变更申请单New(PartsPurchasePricingChange partsPurchasePricingChange) {
            //审核意见和审核状态
            var approvalId = partsPurchasePricingChange.ApprovalId;

            var dbPartsPurchasePricingChange = ObjectContext.PartsPurchasePricingChanges.Where(v => v.Id == partsPurchasePricingChange.Id && v.Status == (int)DcsPartsPurchasePricingChangeStatus.初审通过).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbPartsPurchasePricingChange == null)
                throw new ValidationException(ErrorStrings.PartsPurchasePricingChange_Approve_Init);

            var partsPurchasePricingChangeDetails = ObjectContext.PartsPurchasePricingDetails.Where(r => r.ParentId == partsPurchasePricingChange.Id).ToArray();
            var partIds = partsPurchasePricingChangeDetails.Select(r => r.PartId).ToArray();
            var supplierIds = partsPurchasePricingChangeDetails.Select(r => r.SupplierId).ToArray();
            var partsSupplierRelationsForParts = ObjectContext.PartsSupplierRelations.Where(r => partIds.Contains(r.PartId) && r.Status != (int)DcsBaseDataStatus.作废 && r.PartsSalesCategoryId == partsPurchasePricingChange.PartsSalesCategoryId).ToArray();
            var partsSupplierRelationsNew = partsSupplierRelationsForParts.Where(r => supplierIds.Contains(r.SupplierId)).ToArray();
            var partsPurchasePricings = ObjectContext.PartsPurchasePricings.Where(r => r.PartsSalesCategoryId == partsPurchasePricingChange.PartsSalesCategoryId && partIds.Contains(r.PartId) && r.Status != (int)DcsBaseDataStatus.作废).ToArray();

            foreach (var detail in partsPurchasePricingChangeDetails) {
                //失效时间<当前时间，是补采购价，不需要校验首选供应商和更改首选供应商
                if(detail.ValidTo < DateTime.Now) {
                    continue;
                }
                //配件所有的供应商
                var relations = partsSupplierRelationsForParts.Where(r => r.PartId == detail.PartId && r.PartsSalesCategoryId == partsPurchasePricingChange.PartsSalesCategoryId).ToList();
                //配件采购价格
                var partsPurchasePricing = partsPurchasePricings.Where(r => r.PartId == detail.PartId && r.PartsSalesCategoryId == partsPurchasePricingChange.PartsSalesCategoryId).FirstOrDefault();
                //配件供应商关系
                var partsSupplierRelation = partsSupplierRelationsNew.Where(r => r.PartId == detail.PartId && r.SupplierId == detail.SupplierId && r.PartsSalesCategoryId == partsPurchasePricingChange.PartsSalesCategoryId).FirstOrDefault();
                
                if (partsPurchasePricing != null) {
                    if ((detail.IsPrimary ?? false) == false && relations.Any()) {
                        var count = relations.Count(r => (r.IsPrimary ?? false) == true);
                        var primaryCount = partsPurchasePricingChangeDetails.Count(r => r.PartId == detail.PartId && r.IsPrimary == true);
                        if (count == 0)
                            throw new ValidationException(string.Format(ErrorStrings.PartsPurchasePricingChange_Validation12, detail.PartCode));
                        if (count == 1 && partsSupplierRelation != null && partsSupplierRelation.IsPrimary == true && primaryCount == 0)
                            throw new ValidationException(string.Format(ErrorStrings.PartsPurchasePricingChange_Validation12, detail.PartCode));
                    }
                }
            }

            if(approvalId == 1) {
                partsPurchasePricingChange.Status = (int)DcsPartsPurchasePricingChangeStatus.审核通过;
            } else {
                partsPurchasePricingChange.Status = (int)DcsPartsPurchasePricingChangeStatus.新增;
            }

            var userInfo = Utils.GetCurrentUserInfo();
            partsPurchasePricingChange.CheckerId = userInfo.Id;
            partsPurchasePricingChange.CheckTime = DateTime.Now;
            partsPurchasePricingChange.CheckerName = userInfo.Name;
        }

        public void 驳回配件采购价格变更申请单(PartsPurchasePricingChange partsPurchasePricingChange) {
            var dbPartsPurchasePricingChange = ObjectContext.PartsPurchasePricingChanges.Where(v => v.Id == partsPurchasePricingChange.Id && (v.Status == (int)DcsPartsPurchasePricingChangeStatus.提交 ||v.Status == (int)DcsPartsPurchasePricingChangeStatus.初审通过 ||v.Status == (int)DcsPartsPurchasePricingChangeStatus.审核通过 )).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if (dbPartsPurchasePricingChange == null)
                throw new ValidationException(ErrorStrings.PartsPurchasePricingChange_Validation13);
            var userInfo = Utils.GetCurrentUserInfo();
            if (dbPartsPurchasePricingChange.Status == (int)DcsPartsPurchasePricingChangeStatus.新增) { 
                partsPurchasePricingChange.InitialApproverId = userInfo.Id;
                partsPurchasePricingChange.InitialApproverName = userInfo.Name;
                partsPurchasePricingChange.InitialApproveTime = DateTime.Now;
            }else if(dbPartsPurchasePricingChange.Status == (int)DcsPartsPurchasePricingChangeStatus.审核通过){
                partsPurchasePricingChange.ApproverId = userInfo.Id;
                partsPurchasePricingChange.ApproveTime = DateTime.Now;
                partsPurchasePricingChange.ApproverName = userInfo.Name;
            }else if(dbPartsPurchasePricingChange.Status == (int)DcsPartsPurchasePricingChangeStatus.初审通过){
                partsPurchasePricingChange.CheckerId = userInfo.Id;
                partsPurchasePricingChange.CheckTime = DateTime.Now;
                partsPurchasePricingChange.CheckerName = userInfo.Name;
            }
            partsPurchasePricingChange.Status = (int)DcsPartsPurchasePricingChangeStatus.新增;
        }

        public void 批量初审配件采购价格变更申请(int[] partsPurchasePricingChanges) {
            var dbPartsPurchasePricingChanges = ObjectContext.PartsPurchasePricingChanges.Where(r => partsPurchasePricingChanges.Contains(r.Id) && r.Status == (int)DcsPartsPurchasePricingChangeStatus.新增).SetMergeOption(MergeOption.NoTracking).ToArray();
            foreach(var partsPurchasePricingChange in dbPartsPurchasePricingChanges) {
                partsPurchasePricingChange.Status = (int)DcsPartsPurchasePricingChangeStatus.初审通过;
                var userInfo = Utils.GetCurrentUserInfo();
                partsPurchasePricingChange.ApproverId = userInfo.Id;
                partsPurchasePricingChange.ApproveTime = DateTime.Now;
                partsPurchasePricingChange.ApproverName = userInfo.Name;
                UpdateToDatabase(partsPurchasePricingChange);
            }
            ObjectContext.SaveChanges();
        }


        public void 批量终审配件采购价格变更申请(int[] partsPurchasePricingChanges) {
            var dbPartsPurchasePricingChanges = ObjectContext.PartsPurchasePricingChanges.Where(r => partsPurchasePricingChanges.Contains(r.Id) && (r.Status == (int)DcsPartsPurchasePricingChangeStatus.新增 || r.Status == (int)DcsPartsPurchasePricingChangeStatus.初审通过)).SetMergeOption(MergeOption.NoTracking).ToArray();
            foreach(var partsPurchasePricingChange in dbPartsPurchasePricingChanges) {
                审核配件采购价格变更申请单(partsPurchasePricingChange);
            }
            this.ObjectContext.SaveChanges();
        }

        internal void 审核配件采购价格变更申请单(PartsPurchasePricingChange partsPurchasePricingChange) {
            var dbPartsPurchasePricingChange = ObjectContext.PartsPurchasePricingChanges.Where(v => v.Id == partsPurchasePricingChange.Id &&  v.Status == (int)DcsPartsPurchasePricingChangeStatus.审核通过).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbPartsPurchasePricingChange == null)
                throw new ValidationException(ErrorStrings.PartsPurchasePricingChange_Validation14);
            var userInfo = Utils.GetCurrentUserInfo();
            UpdateToDatabase(partsPurchasePricingChange);
            partsPurchasePricingChange.Status = (int)DcsPartsPurchasePricingChangeStatus.生效;
            partsPurchasePricingChange.ApproverId = userInfo.Id;
            partsPurchasePricingChange.ApproveTime = DateTime.Now;
            partsPurchasePricingChange.ApproverName = userInfo.Name;

            if(partsPurchasePricingChange.ApprovalId == 1) {
                partsPurchasePricingChange.Status = (int)DcsPartsPurchasePricingChangeStatus.生效;
            } else {
                partsPurchasePricingChange.Status = (int)DcsPartsPurchasePricingChangeStatus.新增;
            }

            //驳回
            this.UpdatePartsPurchasePricingChangeValidate(partsPurchasePricingChange);
            if(partsPurchasePricingChange.ApprovalId == 0) {
                UpdateToDatabase(partsPurchasePricingChange);
                return;
            }
            using(var transaction = new TransactionScope()) {

                var partsPurchasePricingDetails = partsPurchasePricingChange.PartsPurchasePricingDetails;
                foreach(var partsPurchasePricingDetail in partsPurchasePricingDetails) {
                    var tempTime = new DateTime(partsPurchasePricingDetail.ValidFrom.Year, partsPurchasePricingDetail.ValidFrom.Month, partsPurchasePricingDetail.ValidFrom.Day, 0, 0, 0);
                    partsPurchasePricingDetail.ValidFrom = tempTime;
                    tempTime = new DateTime(partsPurchasePricingDetail.ValidTo.Year, partsPurchasePricingDetail.ValidTo.Month, partsPurchasePricingDetail.ValidTo.Day, 23, 59, 59);
                    partsPurchasePricingDetail.ValidTo = tempTime;
                }
                //--------------------------------------------------------------------------------------------------------------------------------------------------------------
                var partIds = partsPurchasePricingDetails.Select(r => r.PartId).ToArray();
                var supplierIds = partsPurchasePricingDetails.Select(r => r.SupplierId).ToArray();
                var partsSupplierRelations = ObjectContext.PartsSupplierRelations.Where(r => partIds.Contains(r.PartId) && supplierIds.Contains(r.SupplierId) && r.BranchId == partsPurchasePricingChange.BranchId && r.Status == (int)DcsBaseDataStatus.有效).ToArray();
                var partsPurchasePricings = ObjectContext.PartsPurchasePricings.Where(r => r.PartsSalesCategoryId == partsPurchasePricingChange.PartsSalesCategoryId && partIds.Contains(r.PartId) && r.Status != (int)DcsBaseDataStatus.作废).ToArray();
                foreach(var partsPurchasePricingDetail in partsPurchasePricingDetails) {
                    if(partsPurchasePricingDetail.ValidTo < DateTime.Now) {
                        if(partsPurchasePricings.Any(t=>t.PartId == partsPurchasePricingDetail.PartId && t.PartsSupplierId == partsPurchasePricingDetail.SupplierId && t.PriceType == partsPurchasePricingDetail.PriceType && t.PurchasePrice == partsPurchasePricingDetail.Price && t.ValidFrom == partsPurchasePricingDetail.ValidFrom && t.ValidTo == partsPurchasePricingDetail.ValidTo)) {
                            continue;
                        }
                        var newPartsPurchasePricing = new PartsPurchasePricing {
                            PriceType = partsPurchasePricingDetail.PriceType,
                            BranchId = partsPurchasePricingChange.BranchId,
                            PartId = partsPurchasePricingDetail.PartId,
                            PartsSupplierId = partsPurchasePricingDetail.SupplierId,
                            ValidFrom = partsPurchasePricingDetail.ValidFrom,
                            ValidTo = partsPurchasePricingDetail.ValidTo,
                            PurchasePrice = partsPurchasePricingDetail.Price,
                            PartsSalesCategoryId = partsPurchasePricingChange.PartsSalesCategoryId,
                            PartsSalesCategoryName = partsPurchasePricingChange.PartsSalesCategoryName,
                            Status = (int)DcsPartsPurchasePricingStatus.生效,
                            CreatorId = dbPartsPurchasePricingChange.CreatorId,
                            CreatorName = dbPartsPurchasePricingChange.CreatorName,
                            CreateTime = DateTime.Now,
                            LimitQty = partsPurchasePricingDetail.LimitQty,
                            DataSource = partsPurchasePricingDetail.DataSource
                        };
                        InsertToDatabase(newPartsPurchasePricing);
                        //新增采购价格变动情况记录
                        decimal oldPrice = partsPurchasePricingDetail.ReferencePrice.Value;
                        if(decimal.Zero.Equals(oldPrice)){
                                    //查询上次最新的采购价格变更申请记录的价格
                            var his = ObjectContext.PartsPurchasePricings.Where(t => ObjectContext.PartsSupplierRelations.Any(y => y.PartId == t.PartId && y.IsPrimary == true && y.SupplierId == t.PartsSupplierId) && t.PartId == newPartsPurchasePricing.PartId && t.ValidTo <= DateTime.Now).OrderByDescending(t => t.ValidTo).FirstOrDefault();
                            if(his!=null){
                                oldPrice = his.PurchasePrice;
                            }
                        }
                        var purchasePricingChangeHis = new PurchasePricingChangeHi() {
                            PartsSalesCategoryId = partsPurchasePricingChange.PartsSalesCategoryId,
                            SparePartId = partsPurchasePricingDetail.PartId,
                            SparePartCode = partsPurchasePricingDetail.PartCode,
                            SparePartName = partsPurchasePricingDetail.PartName,
                            OldPurchasePrice = oldPrice,
                            PurchasePrice = partsPurchasePricingDetail.Price,
                            Status = (int)DcsMasterDataStatus.有效,
                            CreatorId = dbPartsPurchasePricingChange.CreatorId,
                            CreatorName = dbPartsPurchasePricingChange.CreatorName,
                            CreateTime = DateTime.Now,
                            IsPrimary = partsPurchasePricingDetail.IsPrimary,
                            ValidFrom = partsPurchasePricingDetail.ValidFrom,
                            ValidTo = partsPurchasePricingDetail.ValidTo
                        };
                        if(oldPrice == decimal.Zero) {
                            purchasePricingChangeHis.ChangeRatio = 0;
                        } else {
                            purchasePricingChangeHis.ChangeRatio = Convert.ToDouble(purchasePricingChangeHis.PurchasePrice / oldPrice - 1);
                        }
                        InsertToDatabase(purchasePricingChangeHis);
                    } else {
                    var partsSupplierRelation = partsSupplierRelations.FirstOrDefault(r => partsPurchasePricingDetail.PartId == r.PartId && partsPurchasePricingDetail.SupplierId == r.SupplierId);
                    if(partsSupplierRelation == null) {
                        var psSupplierRelation = new PartsSupplierRelation {
                            BranchId = userInfo.EnterpriseId,
                            PartId = partsPurchasePricingDetail.PartId,
                            SupplierId = partsPurchasePricingDetail.SupplierId,
                            Status = (int)DcsBaseDataStatus.有效,
                            PartsSalesCategoryId = partsPurchasePricingChange.PartsSalesCategoryId,
                            PartsSalesCategoryName = partsPurchasePricingChange.PartsSalesCategoryName,
                            SupplierPartCode = partsPurchasePricingDetail.SupplierPartCode,
                            IsPrimary = partsPurchasePricingDetail.IsPrimary
                        };
                        psSupplierRelation.CreatorId = userInfo.Id;
                        psSupplierRelation.CreateTime = DateTime.Now;
                        psSupplierRelation.CreatorName = userInfo.Name;
                        InsertToDatabase(psSupplierRelation);
                    } else {
                        if (partsSupplierRelation.SupplierPartCode != partsPurchasePricingDetail.SupplierPartCode ||partsSupplierRelation.IsPrimary != partsPurchasePricingDetail.IsPrimary) {
                            partsSupplierRelation.IsPrimary = partsPurchasePricingDetail.IsPrimary;
                            partsSupplierRelation.SupplierPartCode = partsPurchasePricingDetail.SupplierPartCode;
                            partsSupplierRelation.ModifierId = userInfo.Id;
                            partsSupplierRelation.ModifierName = userInfo.Name;
                            partsSupplierRelation.ModifyTime = DateTime.Now;
                            UpdateToDatabase(partsSupplierRelation);
                        }
                    }
                    var detail = partsPurchasePricingDetail;
                    var overlappartsPurchasePricings = partsPurchasePricings.Where(r => r.PartsSupplierId == detail.SupplierId && r.PartId == detail.PartId && (r.ValidFrom <= detail.ValidTo && r.ValidTo >= detail.ValidFrom)).ToArray();
                    if(overlappartsPurchasePricings.Length == 0) {
                        if(!partsPurchasePricings.Any(t => t.PartId == partsPurchasePricingDetail.PartId && t.PartsSupplierId == partsPurchasePricingDetail.SupplierId && t.PriceType == partsPurchasePricingDetail.PriceType && t.PurchasePrice == partsPurchasePricingDetail.Price && t.ValidFrom == partsPurchasePricingDetail.ValidFrom && t.ValidTo == partsPurchasePricingDetail.ValidTo)) {
                            var newPartsPurchasePricing = new PartsPurchasePricing {
                                PriceType = partsPurchasePricingDetail.PriceType,
                                BranchId = partsPurchasePricingChange.BranchId,
                                PartId = partsPurchasePricingDetail.PartId,
                                PartsSupplierId = partsPurchasePricingDetail.SupplierId,
                                ValidFrom = partsPurchasePricingDetail.ValidFrom,
                                ValidTo = partsPurchasePricingDetail.ValidTo,
                                PurchasePrice = partsPurchasePricingDetail.Price,
                                PartsSalesCategoryId = partsPurchasePricingChange.PartsSalesCategoryId,
                                PartsSalesCategoryName = partsPurchasePricingChange.PartsSalesCategoryName,
                                LimitQty = partsPurchasePricingDetail.LimitQty,
                                Status = (int)DcsPartsPurchasePricingStatus.生效,
                                DataSource = partsPurchasePricingDetail.DataSource
                            };
                            InsertToDatabase(newPartsPurchasePricing);
                            new PartsPurchasePricingAch(this.DomainService).InsertPartsPurchasePricingValidate(newPartsPurchasePricing);
                            newPartsPurchasePricing.CreatorId = dbPartsPurchasePricingChange.CreatorId;
                            newPartsPurchasePricing.CreatorName = dbPartsPurchasePricingChange.CreatorName;
                        }
                       
                    } else {
                        foreach(var partsPurchasePricing in overlappartsPurchasePricings) {
                            if(partsPurchasePricing.ValidFrom >= partsPurchasePricingDetail.ValidFrom && partsPurchasePricing.ValidTo <= partsPurchasePricingDetail.ValidTo) {
                                DeleteFromDatabase(partsPurchasePricing);
                            } else if(partsPurchasePricing.ValidFrom < partsPurchasePricingDetail.ValidFrom && partsPurchasePricing.ValidTo > partsPurchasePricingDetail.ValidTo) {
                                var cutOldPartsPurchasePricingBefore = new PartsPurchasePricing {
                                    PriceType = partsPurchasePricing.PriceType,
                                    BranchId = partsPurchasePricing.BranchId,
                                    PartId = partsPurchasePricing.PartId,
                                    PartsSupplierId = partsPurchasePricing.PartsSupplierId,
                                    ValidFrom = partsPurchasePricing.ValidFrom,
                                    ValidTo = partsPurchasePricingDetail.ValidFrom.AddSeconds(-1),
                                    PurchasePrice = partsPurchasePricing.PurchasePrice,
                                    DataSource = partsPurchasePricing.DataSource,
                                    Status = (int)DcsPartsPurchasePricingStatus.生效,
                                    PartsSalesCategoryId = partsPurchasePricing.PartsSalesCategoryId,
                                    PartsSalesCategoryName = partsPurchasePricing.PartsSalesCategoryName,
                                    Remark = ""//string.Format("被编号为“{0}”申请单截断", partsPurchasePricingChange.Code)
                                };
                                InsertToDatabase(cutOldPartsPurchasePricingBefore);
                                new PartsPurchasePricingAch(this.DomainService).InsertPartsPurchasePricingValidate(cutOldPartsPurchasePricingBefore);
                                cutOldPartsPurchasePricingBefore.CreatorId = partsPurchasePricing.CreatorId;
                                cutOldPartsPurchasePricingBefore.CreatorName = partsPurchasePricing.CreatorName;
                                cutOldPartsPurchasePricingBefore.CreateTime = partsPurchasePricing.CreateTime;
                                cutOldPartsPurchasePricingBefore.ModifierId = userInfo.Id;
                                cutOldPartsPurchasePricingBefore.ModifierName = userInfo.Name;
                                cutOldPartsPurchasePricingBefore.ModifyTime = DateTime.Now;
                                var cutOldPartsPurchasePricingBehind = new PartsPurchasePricing {
                                    PriceType = partsPurchasePricingDetail.PriceType,
                                    BranchId = partsPurchasePricingChange.BranchId,
                                    PartId = partsPurchasePricingDetail.PartId,
                                    PartsSupplierId = partsPurchasePricingDetail.SupplierId,
                                    ValidFrom = partsPurchasePricingDetail.ValidTo.AddSeconds(1),
                                    ValidTo = partsPurchasePricing.ValidTo,
                                    PurchasePrice = partsPurchasePricing.PurchasePrice,
                                    DataSource = partsPurchasePricing.DataSource,
                                    Status = (int)DcsPartsPurchasePricingStatus.生效,
                                    PartsSalesCategoryId = partsPurchasePricing.PartsSalesCategoryId,
                                    PartsSalesCategoryName = partsPurchasePricing.PartsSalesCategoryName,
                                    Remark = ""//string.Format("被编号为“{0}”申请单截断", partsPurchasePricingChange.Code)
                                };
                                InsertToDatabase(cutOldPartsPurchasePricingBehind);
                                new PartsPurchasePricingAch(this.DomainService).InsertPartsPurchasePricingValidate(cutOldPartsPurchasePricingBehind);
                                cutOldPartsPurchasePricingBehind.CreatorId = partsPurchasePricing.CreatorId;
                                cutOldPartsPurchasePricingBehind.CreatorName = partsPurchasePricing.CreatorName;
                                cutOldPartsPurchasePricingBehind.CreateTime = partsPurchasePricing.CreateTime;
                                cutOldPartsPurchasePricingBehind.ModifierId = userInfo.Id;
                                cutOldPartsPurchasePricingBehind.ModifierName = userInfo.Name;
                                cutOldPartsPurchasePricingBehind.ModifyTime = DateTime.Now;
                                DeleteFromDatabase(partsPurchasePricing);
                            } else if(partsPurchasePricing.ValidFrom >= partsPurchasePricingDetail.ValidFrom && partsPurchasePricing.ValidTo > partsPurchasePricingDetail.ValidTo) {
                                partsPurchasePricing.ValidFrom = partsPurchasePricingDetail.ValidTo.AddSeconds(1);
                                partsPurchasePricing.Remark = string.Format(ErrorStrings.PartsPurchasePricingChange_Validation15, partsPurchasePricingChange.Code);
                                partsPurchasePricing.ModifierId = userInfo.Id;
                                partsPurchasePricing.ModifierName = userInfo.Name;
                                partsPurchasePricing.ModifyTime = DateTime.Now;
                                //new PartsPurchasePricingAch(this.DomainService).UpdatePartsPurchasePricingValidate(partsPurchasePricing);
                                UpdateToDatabase(partsPurchasePricing);
                            } else if(partsPurchasePricing.ValidFrom < partsPurchasePricingDetail.ValidFrom && partsPurchasePricing.ValidTo <= partsPurchasePricingDetail.ValidTo) {
                                partsPurchasePricing.ValidTo = partsPurchasePricingDetail.ValidFrom.AddSeconds(-1);
                                partsPurchasePricing.Remark = string.Format(ErrorStrings.PartsPurchasePricingChange_Validation16, partsPurchasePricingChange.Code);
                                partsPurchasePricing.ModifierId = userInfo.Id;
                                partsPurchasePricing.ModifierName = userInfo.Name;
                                partsPurchasePricing.ModifyTime = DateTime.Now;
                                //new PartsPurchasePricingAch(this.DomainService).UpdatePartsPurchasePricingValidate(partsPurchasePricing);
                                UpdateToDatabase(partsPurchasePricing);
                            } else {
                                throw new ValidationException(string.Format(ErrorStrings.PartsPurchasePricingChange_Validation8, partsPurchasePricingDetail.PartCode));
                            }
                        }
                            var newPartsPurchasePricing = new PartsPurchasePricing {
                                PriceType = partsPurchasePricingDetail.PriceType,
                                BranchId = partsPurchasePricingChange.BranchId,
                                PartId = partsPurchasePricingDetail.PartId,
                                PartsSupplierId = partsPurchasePricingDetail.SupplierId,
                                ValidFrom = partsPurchasePricingDetail.ValidFrom,
                                ValidTo = partsPurchasePricingDetail.ValidTo,
                                PurchasePrice = partsPurchasePricingDetail.Price,
                                PartsSalesCategoryId = partsPurchasePricingChange.PartsSalesCategoryId,
                                PartsSalesCategoryName = partsPurchasePricingChange.PartsSalesCategoryName,
                                Status = (int)DcsPartsPurchasePricingStatus.生效,
                                CreatorId = dbPartsPurchasePricingChange.CreatorId,
                                CreatorName = dbPartsPurchasePricingChange.CreatorName,
                                CreateTime = DateTime.Now,
                                LimitQty = partsPurchasePricingDetail.LimitQty,
                                DataSource = partsPurchasePricingDetail.DataSource
                            };
                            InsertToDatabase(newPartsPurchasePricing);
                    }
                    //新增采购价格变动情况记录
                    decimal oldPrice = partsPurchasePricingDetail.ReferencePrice.Value;
                    if(decimal.Zero.Equals(oldPrice)) {
                        //查询上次最新的采购价格变更申请记录的价格
                        var his = ObjectContext.PartsPurchasePricings.Where(t => ObjectContext.PartsSupplierRelations.Any(y => y.PartId == t.PartId && y.IsPrimary == true && y.SupplierId == t.PartsSupplierId) && t.PartId == partsPurchasePricingDetail.PartId && t.ValidTo <= DateTime.Now).OrderByDescending(t => t.ValidTo).FirstOrDefault();
                        if(his != null) {
                            oldPrice = his.PurchasePrice;
                        }
                    }
                    var purchasePricingChangeHis = new PurchasePricingChangeHi() {
                        PartsSalesCategoryId = partsPurchasePricingChange.PartsSalesCategoryId,
                        SparePartId = partsPurchasePricingDetail.PartId,
                        SparePartCode = partsPurchasePricingDetail.PartCode,
                        SparePartName = partsPurchasePricingDetail.PartName,
                        OldPurchasePrice = oldPrice,
                        PurchasePrice = partsPurchasePricingDetail.Price,
                        Status = (int)DcsMasterDataStatus.有效,
                        CreatorId = dbPartsPurchasePricingChange.CreatorId,
                        CreatorName = dbPartsPurchasePricingChange.CreatorName,
                        CreateTime = DateTime.Now,
                        IsPrimary = partsPurchasePricingDetail.IsPrimary,
                        ValidFrom = partsPurchasePricingDetail.ValidFrom,
                        ValidTo = partsPurchasePricingDetail.ValidTo
                    };
                    if(oldPrice == decimal.Zero) {
                        purchasePricingChangeHis.ChangeRatio = 0;
                    } else {
                        purchasePricingChangeHis.ChangeRatio = Convert.ToDouble(purchasePricingChangeHis.PurchasePrice / oldPrice - 1);
                    }
                    InsertToDatabase(purchasePricingChangeHis);
                    }
                }
                //更新partsbranch
                var branches = ObjectContext.PartsBranches.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && partIds.Contains(r.PartId)).ToArray();
                foreach (var partId in partIds.Distinct()) {
                    var branch = branches.FirstOrDefault(r => r.PartId == partId);
                    //如果清单中有相同配件，并且是否可采购全为否，更新为否，其他的为是
                    var details = partsPurchasePricingDetails.Where(r => r.PartId == partId && r.ValidTo > DateTime.Now);
                    if (details.Any()) { 
                        if (details.Any(r => r.IfPurchasable == true)) {
                            if (branch.IsOrderable == false) {
                                branch.IsOrderable = true;
                                branch.ModifierId = userInfo.Id;
                                branch.ModifierName = userInfo.Name;
                                branch.ModifyTime = DateTime.Now;
                                UpdateToDatabase(branch);
                            }
                        } else{
                            if (branch.IsOrderable == true) {
                                branch.IsOrderable = false;
                                branch.ModifierId = userInfo.Id;
                                branch.ModifierName = userInfo.Name;
                                branch.ModifyTime = DateTime.Now;
                                UpdateToDatabase(branch);
                            }
                        }
                    }
                }

                this.ObjectContext.SaveChanges();

                //采购价变更申请终审通过： 增加逻辑1、 变更申请单清单中，不存在采购价的（查询配件采购价，配件ID = 配件采购价.配件ID ， 如果查询不到数据的），这批清单，更新配件与供应商关系.是否首选供应商= 是。
                //1 之外的清单，再根据清单.是否首选，更新配件与供应商关系.是否首选= 清单.是否首选。
                //更新配件与供应商.是否首选= 否之前， 要判断本次 如果本次更新后 配件不存在首选供应商，给予报错：***配件更新后无首选供应商，请确认。
                var partsSupplierRelationsForParts = ObjectContext.PartsSupplierRelations.Where(r => partIds.Contains(r.PartId) && r.Status != (int)DcsBaseDataStatus.作废 && r.PartsSalesCategoryId == partsPurchasePricingChange.PartsSalesCategoryId).ToArray();
                //重新查询配件与供应商关系
                //var partsSupplierRelationsNew = ObjectContext.PartsSupplierRelations.Where(r => partIds.Contains(r.PartId) && supplierIds.Contains(r.SupplierId) && r.BranchId == partsPurchasePricingChange.BranchId && r.Status == (int)DcsBaseDataStatus.有效).ToArray();
                var partsSupplierRelationsNew = partsSupplierRelationsForParts.Where(r => supplierIds.Contains(r.SupplierId)).ToArray();
                foreach(var detail in partsPurchasePricingDetails) {
                    //失效时间<当前时间，是补采购价，不需要校验首选供应商和更改首选供应商
                    if(detail.ValidTo < DateTime.Now) {
                        continue;
                    }
                    //配件所有的供应商
                    var relations = partsSupplierRelationsForParts.Where(r => r.PartId == detail.PartId && r.PartsSalesCategoryId == partsPurchasePricingChange.PartsSalesCategoryId).ToArray();
                    //配件采购价格
                    var partsPurchasePricing = partsPurchasePricings.Where(r => r.PartId == detail.PartId && r.PartsSalesCategoryId == partsPurchasePricingChange.PartsSalesCategoryId).FirstOrDefault();
                    //配件供应商关系
                    var partsSupplierRelation = partsSupplierRelationsNew.Where(r => r.PartId == detail.PartId && r.SupplierId == detail.SupplierId && r.PartsSalesCategoryId == partsPurchasePricingChange.PartsSalesCategoryId).FirstOrDefault();                  
                    if(partsPurchasePricing != null) {
                        //如果是首选供应商，将其他供应商都更改为非首选
                        if ((detail.IsPrimary ?? false) == true) {
                            var par = relations.Where(r => r.IsPrimary == true && r.SupplierId != detail.SupplierId).FirstOrDefault();
                            if (par != null) {
                                par.IsPrimary = false;
                                par.ModifierId = userInfo.Id;
                                par.ModifierName = userInfo.Name;
                                par.ModifyTime = DateTime.Now;
                                UpdateToDatabase(par);
                            }
                        }
                        else { 
                            var count = relations.Count(r => (r.IsPrimary ?? false) == true);
                            if (count == 0 )
                                throw new ValidationException(string.Format(ErrorStrings.PartsPurchasePricingChange_Validation12, detail.PartCode));
                            if (count == 1 && partsSupplierRelation.IsPrimary == true)
                                throw new ValidationException(string.Format(ErrorStrings.PartsPurchasePricingChange_Validation12, detail.PartCode));
                        }
                        partsSupplierRelation.IsPrimary = detail.IsPrimary ?? false;
                        partsSupplierRelation.ModifierId = userInfo.Id;
                        partsSupplierRelation.ModifierName = userInfo.Name;
                        partsSupplierRelation.ModifyTime = DateTime.Now;
                        UpdateToDatabase(partsSupplierRelation);
                    }
                        //不存在采购价
                    else {
                        foreach(var item in relations) {
                            if ((item.IsPrimary == null || item.IsPrimary == true) && item.SupplierId == detail.SupplierId) { 
                                item.IsPrimary = false;
                                item.ModifierId = userInfo.Id;
                                item.ModifierName = userInfo.Name;
                                item.ModifyTime = DateTime.Now;
                                UpdateToDatabase(item);
                            }
                        }
                        if (partsPurchasePricingDetails.Count(r => r.PartId == detail.PartId) > 1) {
                            var minPirce = partsPurchasePricingDetails.Where(x => x.PartId == detail.PartId).Min(y => y.Price);
                            var t = partsPurchasePricingDetails.First(r =>  r.PartId == detail.PartId && r.Price == minPirce);
                            var min = partsSupplierRelationsNew.FirstOrDefault(r => r.PartId == detail.PartId && r.SupplierId == t.SupplierId);
                            if (min != null) {
                                min.IsPrimary = true;
                                min.ModifierId = userInfo.Id;
                                min.ModifierName = userInfo.Name;
                                min.ModifyTime = DateTime.Now;
                                UpdateToDatabase(min);
                            }
                        } else {
                            partsSupplierRelation.IsPrimary = true;
                            partsSupplierRelation.ModifierId = userInfo.Id;
                            partsSupplierRelation.ModifierName = userInfo.Name;
                            partsSupplierRelation.ModifyTime = DateTime.Now;
                            UpdateToDatabase(partsSupplierRelation);
                        }
                    }
                }
                this.ObjectContext.SaveChanges();

                //var partsSupplierRelationsForPartsNew = ObjectContext.PartsSupplierRelations.Where(r => partIds.Contains(r.PartId) && r.Status != (int)DcsBaseDataStatus.作废 && r.PartsSalesCategoryId == partsPurchasePricingChange.PartsSalesCategoryId).ToArray();
                //foreach(var detail in partsPurchasePricingDetails) {
                //    //配件所有的供应商
                //    var relations = partsSupplierRelationsForPartsNew.Where(r => r.PartId == detail.PartId && r.PartsSalesCategoryId == partsPurchasePricingChange.PartsSalesCategoryId).ToArray();
                //    //判断配件是否还存在首选供应商,一个配件只有一个首选供应商
                //    var count = relations.Count(r => (r.IsPrimary ?? false) == true);
                //    if(count > 1)
                //        throw new ValidationException(string.Format("{0}配件更新后有多个首选供应商，请确认", detail.PartCode));
                //    if(count == 0)
                //        throw new ValidationException(string.Format("{0}配件更新后无首选供应商，请确认", detail.PartCode));
                //}
                transaction.Complete();
            }
        }



        public void 作废配件采购价格变更申请单(PartsPurchasePricingChange partsPurchasePricingChange) {
            var dbpartsBranch = ObjectContext.PartsPurchasePricingChanges.Where(v => v.Id == partsPurchasePricingChange.Id && (v.Status == (int)DcsPartsPurchasePricingChangeStatus.新增 || v.Status == (int)DcsPartsPurchasePricingChangeStatus.初审通过)).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbpartsBranch == null)
                throw new ValidationException(ErrorStrings.PartsPurchasePricingChange_Validation17);
            UpdateToDatabase(partsPurchasePricingChange);
            partsPurchasePricingChange.Status = (int)DcsPartsPurchasePricingChangeStatus.作废;
            var userInfo = Utils.GetCurrentUserInfo();
            partsPurchasePricingChange.AbandonerId = userInfo.Id;
            partsPurchasePricingChange.AbandonTime = DateTime.Now;
            partsPurchasePricingChange.AbandonerName = userInfo.Name;
            this.UpdatePartsPurchasePricingChangeValidate(partsPurchasePricingChange);

        }


        public void 生成特殊协议采购价申请单(PartsPurchasePricingChange partsPurchasePricingChange, List<PartsPurchasePricingDetail> partsPurchasePricingDetails) {
            OracleConnection yxDCSoraCon = new OracleConnection();
            try {
                string yxDCSConnStr = ConfigurationManager.ConnectionStrings["YXDcsServiceConn"].ConnectionString;
                yxDCSoraCon.ConnectionString = yxDCSConnStr;

                int yxDCSNextValue = 0;
                if(yxDCSoraCon.State == ConnectionState.Closed)
                    yxDCSoraCon.Open();

                OracleCommand yxDCSCommand = new OracleCommand("select s_PartsPurchasePricingChange.nextval as nextval from dual", yxDCSoraCon);
                var yxDCSReader = yxDCSCommand.ExecuteReader();
                if(yxDCSReader.Read())
                    yxDCSNextValue = Convert.ToInt32(yxDCSReader["nextval"]);
                yxDCSReader.Close();
                if(yxDCSNextValue == 0)
                    return;
                int yxDCSBranchId = 0;
                yxDCSCommand = new OracleCommand("select id from company where code='1101'", yxDCSoraCon);
                var yxDCSBranchReader = yxDCSCommand.ExecuteReader();
                if(yxDCSBranchReader.Read())
                    yxDCSBranchId = Convert.ToInt32(yxDCSBranchReader["id"]);
                yxDCSBranchReader.Close();
                if(yxDCSBranchId == 0)
                    return;
                partsPurchasePricingChange.Id = yxDCSNextValue;
                partsPurchasePricingChange.BranchId = yxDCSBranchId;
                partsPurchasePricingChange.Status = (int)DcsPartsPurchasePricingChangeStatus.新增;
                partsPurchasePricingChange.CreateTime = DateTime.Now;
                //生成配件采购价格变更申请单-code
                string codeSql = @"select regexp_replace('PPPC1101' || TO_CHAR(sysdate,'yyyymmdd')||'{SERIAL}','{SERIAL}',lpad(TO_CHAR(GetSerial((SELECT id FROM YXDCS.CodeTemplate WHERE name='PartsPurchasePricingChange' AND IsActived=1), TRUNC(sysdate, 'DD'), '1101')),4,'0')) as code from dual";
                string code = "";
                OracleCommand codecommand = new OracleCommand(codeSql, yxDCSoraCon);
                var codereader = codecommand.ExecuteReader();
                if(codereader.Read()) {
                    code = codereader["code"].ToString();
                }
                codereader.Close();
                partsPurchasePricingChange.Code = code;
                OracleDataAdapter oraDap = new OracleDataAdapter("select Id,Code,Name from Company where code='FT001078'", yxDCSoraCon);
                OracleDataTable companyTable = new OracleDataTable();
                oraDap.Fill(companyTable);

                if(companyTable == null || companyTable.Rows.Count == 0)
                    return;
                // 配件销售类型Id 和 配件销售类型名称
                oraDap = new OracleDataAdapter("select Id,Code,Name from PartsSalesCategory where name='随车行' and BranchId=" + yxDCSBranchId, yxDCSoraCon);
                OracleDataTable partsSalesCategoryTable = new OracleDataTable();
                oraDap.Fill(partsSalesCategoryTable);
                if(partsSalesCategoryTable == null || partsSalesCategoryTable.Rows.Count == 0)
                    return;
                partsPurchasePricingChange.PartsSalesCategoryId = Convert.ToInt32(partsSalesCategoryTable.Rows[0][0]);
                partsPurchasePricingChange.PartsSalesCategoryName = partsSalesCategoryTable.Rows[0][2].ToString();

                //配件采购价格变更申请清单-满足条件的配件信息列表
                OracleDataTable sparePartsTable = new OracleDataTable();
                string codesStr = " '";
                foreach(var partsPurchasePricingDetail in partsPurchasePricingDetails) {
                    codesStr += partsPurchasePricingDetail.PartCode.ToString() + "','";
                }
                codesStr = codesStr.Substring(0, codesStr.Length - 2);
                oraDap = new OracleDataAdapter("select Id,Code,Name from SparePart where code in(" + codesStr + ")", yxDCSoraCon);
                oraDap.Fill(sparePartsTable);
                if(sparePartsTable == null || sparePartsTable.Rows.Count == 0) {
                    return;
                }

                foreach(var partsPurchasePricingDetail in partsPurchasePricingDetails) {
                    DataRow[] datarows = sparePartsTable.Select("Code = '" + partsPurchasePricingDetail.PartCode + "'");
                    if(datarows.Length > 0) {
                        partsPurchasePricingDetail.ParentId = yxDCSNextValue;
                        partsPurchasePricingDetail.PartId = Convert.ToInt32(datarows[0][0]);
                        partsPurchasePricingDetail.PartName = datarows[0][2].ToString();
                        partsPurchasePricingDetail.SupplierId = Convert.ToInt32(companyTable.Rows[0][0]);
                        partsPurchasePricingDetail.SupplierCode = companyTable.Rows[0][1].ToString();
                        partsPurchasePricingDetail.SupplierName = companyTable.Rows[0][2].ToString();
                        partsPurchasePricingDetail.ValidFrom = DateTime.Now;
                        partsPurchasePricingDetail.ValidTo = DateTime.Now.AddYears(1);
                    }
                }

                //插入清单与主单信息
                string sqlInsertdetail = new PartsSalesOrderAch(this.DomainService).GetMyInsertSql("PartsPurchasePricingDetail", "Id", new[] { "ParentId", "PartId", "PartCode", "PartName", "SupplierId", "SupplierCode", "SupplierName", "Price", "ValidFrom", "ValidTo" }, "s_PartsPurchasePricingDetail.nextval");
                foreach(var item in partsPurchasePricingDetails) {
                    var detailcommand = new OracleCommand(sqlInsertdetail, yxDCSoraCon);
                    detailcommand.Parameters.Add(new OracleParameter("ParentId", item.ParentId));
                    detailcommand.Parameters.Add(new OracleParameter("PartId", item.PartId));
                    detailcommand.Parameters.Add(new OracleParameter("PartCode", item.PartCode));
                    detailcommand.Parameters.Add(new OracleParameter("PartName", item.PartName));
                    detailcommand.Parameters.Add(new OracleParameter("SupplierId", item.SupplierId));
                    detailcommand.Parameters.Add(new OracleParameter("SupplierCode", item.SupplierCode));
                    detailcommand.Parameters.Add(new OracleParameter("SupplierName", item.SupplierName));
                    detailcommand.Parameters.Add(new OracleParameter("Price", item.Price));
                    detailcommand.Parameters.Add(new OracleParameter("ValidFrom", item.ValidFrom));
                    detailcommand.Parameters.Add(new OracleParameter("ValidTo", item.ValidTo));
                    if(yxDCSoraCon.State == ConnectionState.Closed)
                        yxDCSoraCon.Open();
                    detailcommand.ExecuteNonQuery();
                }

                string sqlInsertM = new PartsSalesOrderAch(this.DomainService).GetMyInsertSql("PartsPurchasePricingChange", null, new[] { "Id", "Code", "BranchId", "PartsSalesCategoryId", "PartsSalesCategoryName", "CreateTime", "Status" }, null);
                var mcommand = new OracleCommand(sqlInsertM, yxDCSoraCon);
                mcommand.Parameters.Add(new OracleParameter("Id", yxDCSNextValue));
                mcommand.Parameters.Add(new OracleParameter("Code", partsPurchasePricingChange.Code));
                mcommand.Parameters.Add(new OracleParameter("BranchId", partsPurchasePricingChange.BranchId));
                mcommand.Parameters.Add(new OracleParameter("PartsSalesCategoryId", partsPurchasePricingChange.PartsSalesCategoryId));
                mcommand.Parameters.Add(new OracleParameter("PartsSalesCategoryName", partsPurchasePricingChange.PartsSalesCategoryName));
                mcommand.Parameters.Add(new OracleParameter("CreateTime", partsPurchasePricingChange.CreateTime));
                mcommand.Parameters.Add(new OracleParameter("Status", partsPurchasePricingChange.Status));
                if(yxDCSoraCon.State == ConnectionState.Closed)
                    yxDCSoraCon.Open();
                mcommand.ExecuteNonQuery();

            } finally {
                if(yxDCSoraCon.State != ConnectionState.Closed)
                    yxDCSoraCon.Close();
            }
        }

    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:11:30
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 生成配件采购价格变更申请单(PartsPurchasePricingChange partsPurchasePricingChange) {
            new PartsPurchasePricingChangeAch(this).生成配件采购价格变更申请单(partsPurchasePricingChange);
        }


        [Invoke]
        public void 生成配件采购价格变更申请单营销(PartsPurchasePricingChange partsPurchasePricingChange, List<PartsPurchasePricingDetail> partsPurchasePricingDetails) {
            new PartsPurchasePricingChangeAch(this).生成配件采购价格变更申请单营销(partsPurchasePricingChange, partsPurchasePricingDetails);
        }


        [Update(UsingCustomMethod = true)]
        public void 初审配件采购价格变更申请单(PartsPurchasePricingChange partsPurchasePricingChange) {
            new PartsPurchasePricingChangeAch(this).初审配件采购价格变更申请单(partsPurchasePricingChange);
        }

        [Invoke(HasSideEffects = true)]
        public void 批量初审配件采购价格变更申请(int[] partsPurchasePricingChanges) {
            new PartsPurchasePricingChangeAch(this).批量初审配件采购价格变更申请(partsPurchasePricingChanges);
        }


        [Invoke(HasSideEffects = true)]
        public void 批量终审配件采购价格变更申请(int[] partsPurchasePricingChanges) {
            new PartsPurchasePricingChangeAch(this).批量终审配件采购价格变更申请(partsPurchasePricingChanges);
        }

        [Update(UsingCustomMethod = true)]
        public void 作废配件采购价格变更申请单(PartsPurchasePricingChange partsPurchasePricingChange) {
            new PartsPurchasePricingChangeAch(this).作废配件采购价格变更申请单(partsPurchasePricingChange);
        }


        [Invoke]
        public void 生成特殊协议采购价申请单(PartsPurchasePricingChange partsPurchasePricingChange, List<PartsPurchasePricingDetail> partsPurchasePricingDetails) {
            new PartsPurchasePricingChangeAch(this).生成特殊协议采购价申请单(partsPurchasePricingChange, partsPurchasePricingDetails);
        }

        [Update(UsingCustomMethod = true)]
        public void 审核配件采购价格变更申请单(PartsPurchasePricingChange partsPurchasePricingChange) {
            new PartsPurchasePricingChangeAch(this).审核配件采购价格变更申请单(partsPurchasePricingChange);
        }

        [Update(UsingCustomMethod = true)]
        public void 审核配件采购价格变更申请单New(PartsPurchasePricingChange partsPurchasePricingChange) {
            new PartsPurchasePricingChangeAch(this).审核配件采购价格变更申请单New(partsPurchasePricingChange);
        }

        [Update(UsingCustomMethod = true)]
        public void 驳回配件采购价格变更申请单(PartsPurchasePricingChange partsPurchasePricingChange) {
            new PartsPurchasePricingChangeAch(this).驳回配件采购价格变更申请单(partsPurchasePricingChange);
        }
        [Update(UsingCustomMethod = true)]
        public void 提交配件采购价格变更申请单(PartsPurchasePricingChange partsPurchasePricingChange) {
            new PartsPurchasePricingChangeAch(this).提交配件采购价格变更申请单(partsPurchasePricingChange);
        }
    }
}
