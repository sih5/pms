﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsSupplierAch : DcsSerivceAchieveBase {
        public PartsSupplierAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 停用配件供应商基本信息(PartsSupplier partsSupplier) {
            var dbsparePart = ObjectContext.PartsSuppliers.Where(r => r.Id == partsSupplier.Id && r.Status == (int)DcsMasterDataStatus.有效).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbsparePart == null)
                throw new ValidationException(ErrorStrings.PartsSupplier_Validation1);
            UpdateToDatabase(partsSupplier);
            partsSupplier.Status = (int)DcsMasterDataStatus.停用;
            this.UpdatePartsSupplierValidate(partsSupplier);
        }

        public void 恢复配件供应商基本信息(PartsSupplier partsSupplier) {
            var dbsparePart = ObjectContext.PartsSuppliers.Where(r => r.Id == partsSupplier.Id && r.Status == (int)DcsMasterDataStatus.停用).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbsparePart == null)
                throw new ValidationException(ErrorStrings.PartsSupplier_Validation2);
            UpdateToDatabase(partsSupplier);
            partsSupplier.Status = (int)DcsMasterDataStatus.有效;
            this.UpdatePartsSupplierValidate(partsSupplier);
        }

        public void 作废配件供应商基本信息(PartsSupplier partsSupplier) {
            var dbpartsSupplier = ObjectContext.PartsSuppliers.Where(r => r.Id == partsSupplier.Id && r.Status == (int)DcsMasterDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbpartsSupplier == null)
                throw new ValidationException(ErrorStrings.PartsSupplier_Validation4);
            UpdateToDatabase(partsSupplier);
            partsSupplier.Status = (int)DcsMasterDataStatus.作废;
            var userInfo = Utils.GetCurrentUserInfo();
            partsSupplier.AbandonerId = userInfo.Id;
            partsSupplier.AbandonerName = userInfo.Name;
            partsSupplier.AbandonTime = DateTime.Now;
            this.UpdatePartsSupplierValidate(partsSupplier);
            var company = partsSupplier.Company;
            UpdateToDatabase(company);
            company.Status = (int)DcsMasterDataStatus.作废;
            company.ModifierId = userInfo.Id;
            company.ModifierName = userInfo.Name;
            company.ModifyTime = DateTime.Now;
            new CompanyAch(this.DomainService).UpdateCompanyValidate(company);
        }

        public void 修改配件供应商基本信息(PartsSupplier partsSupplier) {
            var userInfo = Utils.GetCurrentUserInfo();
            CheckEntityState(partsSupplier);
            var dbpartsSupplier = ObjectContext.PartsSuppliers.Where(r => r.Id == partsSupplier.Id && r.Status == (int)DcsMasterDataStatus.有效).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbpartsSupplier == null)
                throw new ValidationException(ErrorStrings.PartsSupplier_Validation3);

            partsSupplier.ModifierId = userInfo.Id;
            partsSupplier.ModifierName = userInfo.Name;
            partsSupplier.ModifyTime = DateTime.Now;
            UpdateToDatabase(partsSupplier);
            if(partsSupplier.Name != dbpartsSupplier.Name) {
                //修改供应商名称
                var companyInvoiceInfos = this.ObjectContext.CompanyInvoiceInfoes.Where(r => r.CompanyId == partsSupplier.Id).ToArray();
                foreach(var companyInvoiceInfo in companyInvoiceInfos) {
                    companyInvoiceInfo.CompanyName = partsSupplier.Name;
                    companyInvoiceInfo.ModifyTime = DateTime.Now;
                    companyInvoiceInfo.ModifierId = userInfo.Id;
                    companyInvoiceInfo.ModifierName = userInfo.Name;
                    UpdateToDatabase(companyInvoiceInfo);
                }
                var supplierPlanArrears = this.ObjectContext.SupplierPlanArrears.Where(r => r.SupplierId == partsSupplier.Id).ToArray();
                foreach(var supplierPlanArrear in supplierPlanArrears) {
                    supplierPlanArrear.SupplierCompanyName = partsSupplier.Name;
                    supplierPlanArrear.ModifyTime = DateTime.Now;
                    supplierPlanArrear.ModifierId = userInfo.Id;
                    supplierPlanArrear.ModifierName = userInfo.Name;
                    UpdateToDatabase(supplierPlanArrear);
                }
            }
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:11:30
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 停用配件供应商基本信息(PartsSupplier partsSupplier) {
            new PartsSupplierAch(this).停用配件供应商基本信息(partsSupplier);
        }
        
        [Update(UsingCustomMethod = true)]
        public void 恢复配件供应商基本信息(PartsSupplier partsSupplier) {
            new PartsSupplierAch(this).恢复配件供应商基本信息(partsSupplier);
        }
        
        [Update(UsingCustomMethod = true)]
        public void 作废配件供应商基本信息(PartsSupplier partsSupplier) {
            new PartsSupplierAch(this).作废配件供应商基本信息(partsSupplier);
        }

        [Update(UsingCustomMethod = true)]
        public void 修改配件供应商基本信息(PartsSupplier partsSupplier) {
            new PartsSupplierAch(this).修改配件供应商基本信息(partsSupplier);
        }
    }
}
