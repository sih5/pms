﻿using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsSupplierRelationAch : DcsSerivceAchieveBase {
        public PartsSupplierRelationAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 作废配件与供应商关系(PartsSupplierRelation partsSupplierRelation) {
            var dbpartsSupplierRelation = ObjectContext.PartsSupplierRelations.Where(r => r.Id == partsSupplierRelation.Id && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbpartsSupplierRelation == null)
                throw new ValidationException(ErrorStrings.PartsSupplierRelation_Validation3);
            UpdateToDatabase(partsSupplierRelation);
            partsSupplierRelation.Status = (int)DcsBaseDataStatus.作废;
            this.UpdatePartsSupplierRelationValidate(partsSupplierRelation);
            var partsSupplierRelationHistory = new PartsSupplierRelationHistory {
                PartsSupplierRelationId = partsSupplierRelation.Id,
                BranchId = partsSupplierRelation.BranchId,
                PartsSalesCategoryId = partsSupplierRelation.PartsSalesCategoryId,
                PartsSalesCategoryName = partsSupplierRelation.PartsSalesCategoryName,
                PartId = partsSupplierRelation.PartId,
                SupplierId = partsSupplierRelation.SupplierId,
                SupplierPartCode = partsSupplierRelation.SupplierPartCode,
                SupplierPartName = partsSupplierRelation.SupplierPartName,
                IsPrimary = partsSupplierRelation.IsPrimary,
                PurchasePercentage = partsSupplierRelation.PurchasePercentage,
                EconomicalBatch = partsSupplierRelation.EconomicalBatch,
                MinBatch = partsSupplierRelation.MinBatch,
                Status = partsSupplierRelation.Status,
                MonthlyArrivalPeriod = partsSupplierRelation.MonthlyArrivalPeriod,
                EmergencyArrivalPeriod = partsSupplierRelation.EmergencyArrivalPeriod,
                ArrivalReplenishmentCycle = partsSupplierRelation.ArrivalReplenishmentCycle,
                Remark = partsSupplierRelation.Remark
            };
            partsSupplierRelation.PartsSupplierRelationHistories.Add(partsSupplierRelationHistory);
            DomainService.InsertPartsSupplierRelationHistory(partsSupplierRelationHistory);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:11:30
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 作废配件与供应商关系(PartsSupplierRelation partsSupplierRelation) {
            new PartsSupplierRelationAch(this).作废配件与供应商关系(partsSupplierRelation);
        }
    }
}
