﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsOuterPurchaseChangeAch : DcsSerivceAchieveBase {
        public PartsOuterPurchaseChangeAch(DcsDomainService domainService)
            : base(domainService) {
        }
        #region 确认-初审-审核-审批-高级审核-高级审批
        //确认 
        public void 确认配件外采申请单(PartsOuterPurchaseChange partsOuterPurchaseChange) {
            var dbPartsOuterPurchaseChange = ObjectContext.PartsOuterPurchaseChanges.Where(v => v.Id == partsOuterPurchaseChange.Id && v.Status == (int)DcsPartsOuterPurchaseChangeStatus.提交).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbPartsOuterPurchaseChange == null)
                throw new ValidationException(ErrorStrings.PartsOuterPurchaseChange_Validation6);
            var userinfo = Utils.GetCurrentUserInfo();
            if(partsOuterPurchaseChange.ApprovalId == 1) {
                var inventoryConfig = ObjectContext.MultiLevelApproveConfigs.FirstOrDefault(r => ((partsOuterPurchaseChange.Amount >= r.MinApproveFee && partsOuterPurchaseChange.Amount < r.MaxApproveFee) || partsOuterPurchaseChange.Amount < 0)
                    && r.Type == (int)DCSMultiLevelApproveConfigType.配件外采管理 && r.ApproverId == userinfo.Id && r.IsFinished && r.Status == (int)DcsBaseDataStatus.有效);
                if(inventoryConfig != null) {
                    finalApproveBusiness(partsOuterPurchaseChange);
                } else {
                    partsOuterPurchaseChange.Status = (int)DcsPartsOuterPurchaseChangeStatus.确认通过;
                }
            } else {
                partsOuterPurchaseChange.Status = (int)DcsPartsOuterPurchaseChangeStatus.新建;
                partsOuterPurchaseChange.RejecterId = userinfo.Id;
                partsOuterPurchaseChange.RejecterName = userinfo.Name;
                partsOuterPurchaseChange.RejectTime = DateTime.Now;
                partsOuterPurchaseChange.RejectComment = partsOuterPurchaseChange.ApproveComment;
            }
            partsOuterPurchaseChange.ConfirmID = userinfo.Id;
            partsOuterPurchaseChange.ConfirmName = userinfo.Name;
            partsOuterPurchaseChange.CONFIRMTIME = DateTime.Now;
            UpdateToDatabase(partsOuterPurchaseChange);
            this.UpdatePartsOuterPurchaseChangeValidate(partsOuterPurchaseChange);
        }
        //高级审核
        public void 高级审核配件外采申请单(PartsOuterPurchaseChange partsOuterPurchaseChange) {
            var dbPartsOuterPurchaseChange = ObjectContext.PartsOuterPurchaseChanges.Where(v => v.Id == partsOuterPurchaseChange.Id && v.Status == (int)DcsPartsOuterPurchaseChangeStatus.审批通过).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbPartsOuterPurchaseChange == null)
                throw new ValidationException(ErrorStrings.PartsOuterPurchaseChange_Validation6);
            var userinfo = Utils.GetCurrentUserInfo();
            if(partsOuterPurchaseChange.ApprovalId == 1) {
                var inventoryConfig = ObjectContext.MultiLevelApproveConfigs.FirstOrDefault(r => ((partsOuterPurchaseChange.Amount >= r.MinApproveFee && partsOuterPurchaseChange.Amount < r.MaxApproveFee) || partsOuterPurchaseChange.Amount < 0)
                    && r.Type == (int)DCSMultiLevelApproveConfigType.配件外采管理 && r.ApproverId == userinfo.Id && r.IsFinished && r.Status == (int)DcsBaseDataStatus.有效);
                if(inventoryConfig != null) {
                    finalApproveBusiness(partsOuterPurchaseChange);
                } else {
                    partsOuterPurchaseChange.Status = (int)DcsPartsOuterPurchaseChangeStatus.高级审核通过;
                }
            } else {
                partsOuterPurchaseChange.Status = (int)DcsPartsOuterPurchaseChangeStatus.新建;
                partsOuterPurchaseChange.RejecterId = userinfo.Id;
                partsOuterPurchaseChange.RejecterName = userinfo.Name;
                partsOuterPurchaseChange.RejectTime = DateTime.Now;
                partsOuterPurchaseChange.RejectComment = partsOuterPurchaseChange.ApproveComment;
            }
            partsOuterPurchaseChange.AdvancedAuditID = userinfo.Id;
            partsOuterPurchaseChange.AdvancedAuditName = userinfo.Name;
            partsOuterPurchaseChange.AdvancedAuditTime = DateTime.Now;
            UpdateToDatabase(partsOuterPurchaseChange);
            this.UpdatePartsOuterPurchaseChangeValidate(partsOuterPurchaseChange);
        }
        //高级审批
        public void 高级审批配件外采申请单(PartsOuterPurchaseChange partsOuterPurchaseChange) {
            var userinfo = Utils.GetCurrentUserInfo();
            var dbPartsOuterPurchaseChange = ObjectContext.PartsOuterPurchaseChanges.Where(v => v.Id == partsOuterPurchaseChange.Id && v.Status == (int)DcsPartsOuterPurchaseChangeStatus.高级审核通过).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbPartsOuterPurchaseChange == null)
                throw new ValidationException(ErrorStrings.PartsOuterPurchaseChange_Validation16);
            partsOuterPurchaseChange.SeniorApproveID = userinfo.Id;
            partsOuterPurchaseChange.SeniorApproveName = userinfo.Name;
            partsOuterPurchaseChange.SeniorApproveTime = DateTime.Now;
            finalApproveBusiness(partsOuterPurchaseChange);
        }
        #endregion

        public void 提交配件外采申请单(PartsOuterPurchaseChange partsOuterPurchaseChange) {
            var dbPartsOuterPurchaseChange = ObjectContext.PartsOuterPurchaseChanges.Where(v => v.Id == partsOuterPurchaseChange.Id && v.Status == (int)DcsPartsOuterPurchaseChangeStatus.新建).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbPartsOuterPurchaseChange == null)
                throw new ValidationException(ErrorStrings.PartsOuterPurchaseChange_Validation1);
            var userinfo = Utils.GetCurrentUserInfo();
            UpdateToDatabase(partsOuterPurchaseChange);
            partsOuterPurchaseChange.Status = (int)DcsPartsOuterPurchaseChangeStatus.提交;
            partsOuterPurchaseChange.SubmitterId = userinfo.Id;
            partsOuterPurchaseChange.SubmitterName = userinfo.Name;
            partsOuterPurchaseChange.SubmitTime = DateTime.Now;
            this.UpdatePartsOuterPurchaseChangeValidate(partsOuterPurchaseChange);
        }
        //原初审 已修改
        public void 初审配件外采申请单(PartsOuterPurchaseChange partsOuterPurchaseChange) {
            var dbPartsOuterPurchaseChange = ObjectContext.PartsOuterPurchaseChanges.Where(v => v.Id == partsOuterPurchaseChange.Id && v.Status == (int)DcsPartsOuterPurchaseChangeStatus.确认通过).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbPartsOuterPurchaseChange == null)
                throw new ValidationException(ErrorStrings.PartsOuterPurchaseChange_Validation6);
            var userinfo = Utils.GetCurrentUserInfo();
            if(partsOuterPurchaseChange.ApprovalId == 1) {
                var inventoryConfig = ObjectContext.MultiLevelApproveConfigs.FirstOrDefault(r => ((partsOuterPurchaseChange.Amount >= r.MinApproveFee && partsOuterPurchaseChange.Amount < r.MaxApproveFee) || partsOuterPurchaseChange.Amount < 0)
                    && r.Type == (int)DCSMultiLevelApproveConfigType.配件外采管理 && r.ApproverId == userinfo.Id && r.IsFinished && r.Status == (int)DcsBaseDataStatus.有效);
                if(inventoryConfig != null) {
                    finalApproveBusiness(partsOuterPurchaseChange);
                } else {
                    partsOuterPurchaseChange.Status = (int)DcsPartsOuterPurchaseChangeStatus.初审通过;
                }
            } else {
                partsOuterPurchaseChange.Status = (int)DcsPartsOuterPurchaseChangeStatus.新建;
                partsOuterPurchaseChange.RejecterId = userinfo.Id;
                partsOuterPurchaseChange.RejecterName = userinfo.Name;
                partsOuterPurchaseChange.RejectTime = DateTime.Now;
                partsOuterPurchaseChange.RejectComment = partsOuterPurchaseChange.ApproveComment;
            }
            partsOuterPurchaseChange.InitialApproverId = userinfo.Id;
            partsOuterPurchaseChange.InitialApproverName = userinfo.Name;
            partsOuterPurchaseChange.InitialApproveTime = DateTime.Now;
            UpdateToDatabase(partsOuterPurchaseChange);
            this.UpdatePartsOuterPurchaseChangeValidate(partsOuterPurchaseChange);
        }
        //原审核 已修改
        public void 审批配件外采申请单(PartsOuterPurchaseChange partsOuterPurchaseChange) {
            var dbPartsOuterPurchaseChange = ObjectContext.PartsOuterPurchaseChanges.Where(v => v.Id == partsOuterPurchaseChange.Id && v.Status == (int)DcsPartsOuterPurchaseChangeStatus.初审通过).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbPartsOuterPurchaseChange == null)
                throw new ValidationException(ErrorStrings.PartsOuterPurchaseChange_Validation7);
            var userinfo = Utils.GetCurrentUserInfo();
            if(partsOuterPurchaseChange.ApprovalId == 1) {
                var inventoryConfig = ObjectContext.MultiLevelApproveConfigs.FirstOrDefault(r => ((partsOuterPurchaseChange.Amount >= r.MinApproveFee && partsOuterPurchaseChange.Amount < r.MaxApproveFee) || partsOuterPurchaseChange.Amount < 0)
                   && r.Type == (int)DCSMultiLevelApproveConfigType.配件外采管理 && r.ApproverId == userinfo.Id && r.IsFinished && r.Status == (int)DcsBaseDataStatus.有效);
                if(inventoryConfig != null) {
                    finalApproveBusiness(partsOuterPurchaseChange);
                } else {
                    partsOuterPurchaseChange.Status = (int)DcsPartsOuterPurchaseChangeStatus.审核通过;
                }
            } else {
                partsOuterPurchaseChange.Status = (int)DcsPartsOuterPurchaseChangeStatus.新建;
                partsOuterPurchaseChange.RejecterId = userinfo.Id;
                partsOuterPurchaseChange.RejecterName = userinfo.Name;
                partsOuterPurchaseChange.RejectTime = DateTime.Now;
                partsOuterPurchaseChange.RejectComment = partsOuterPurchaseChange.ApproveComment;
            }
            partsOuterPurchaseChange.CheckerId = userinfo.Id;
            partsOuterPurchaseChange.CheckerName = userinfo.Name;
            partsOuterPurchaseChange.CheckTime = DateTime.Now;
            UpdateToDatabase(partsOuterPurchaseChange);
            this.UpdatePartsOuterPurchaseChangeValidate(partsOuterPurchaseChange);
        }

        private void finalApproveBusiness(PartsOuterPurchaseChange partsOuterPurchaseChange) {
            var userinfo = Utils.GetCurrentUserInfo();
            if(partsOuterPurchaseChange.ApprovalId == 1) {
                partsOuterPurchaseChange.Status = (int)DcsPartsOuterPurchaseChangeStatus.生效;
            } else {
                partsOuterPurchaseChange.Status = (int)DcsPartsOuterPurchaseChangeStatus.新建;
                partsOuterPurchaseChange.RejecterId = userinfo.Id;
                partsOuterPurchaseChange.RejecterName = userinfo.Name;
                partsOuterPurchaseChange.RejectTime = DateTime.Now;
                partsOuterPurchaseChange.RejectComment = partsOuterPurchaseChange.ApproveComment;
            }
            UpdateToDatabase(partsOuterPurchaseChange);
            //partsOuterPurchaseChange.ApproverId = userinfo.Id;
            //partsOuterPurchaseChange.ApproverName = userinfo.Name;
            //partsOuterPurchaseChange.ApproveTime = DateTime.Now;
            this.UpdatePartsOuterPurchaseChangeValidate(partsOuterPurchaseChange);
            //驳回，不执行后面的业务操作
            if(partsOuterPurchaseChange.ApprovalId == 0)
                return;

            var company = ObjectContext.Companies.SingleOrDefault(r => r.Id == partsOuterPurchaseChange.CustomerCompanyId && r.Status != (int)DcsMasterDataStatus.作废);
            if(company == null)
                throw new ValidationException(ErrorStrings.PartsOuterPurchaseChange_Validation3);
            if((company.Type == (int)DcsCompanyType.代理库 || company.Type == (int)DcsCompanyType.服务站兼代理库) && (!partsOuterPurchaseChange.ReceivingWarehouseId.HasValue)) {
                var warehouse = ObjectContext.Warehouses.FirstOrDefault(r => r.Status != (int)DcsBaseDataStatus.作废 && ObjectContext.SalesUnitAffiWarehouses.Any(v => v.WarehouseId == r.Id && ObjectContext.SalesUnits.Any(k => k.Id == v.SalesUnitId && k.OwnerCompanyId == partsOuterPurchaseChange.CustomerCompanyId && k.PartsSalesCategoryId == partsOuterPurchaseChange.PartsSalesCategoryrId && k.Status != (int)DcsMasterDataStatus.作废)));
                if(warehouse == null)
                    throw new ValidationException(ErrorStrings.PartsOuterPurchaseChange_Validation4);
                var branch = ObjectContext.Branches.SingleOrDefault(r => r.Id == partsOuterPurchaseChange.BranchId);
                if(branch == null) {
                    throw new ValidationException(String.Format("不存在分公司Id为{0}的分公司", partsOuterPurchaseChange.BranchId));
                }
                var partsInboundPlan = new PartsInboundPlan();
                partsInboundPlan.WarehouseId = warehouse.Id;
                partsInboundPlan.WarehouseCode = warehouse.Code;
                partsInboundPlan.WarehouseName = warehouse.Name;
                partsInboundPlan.StorageCompanyId = partsOuterPurchaseChange.CustomerCompanyId;
                partsInboundPlan.StorageCompanyCode = company.Code;
                partsInboundPlan.StorageCompanyName = company.Name;
                partsInboundPlan.StorageCompanyType = company.Type;
                partsInboundPlan.BranchId = partsOuterPurchaseChange.BranchId;
                partsInboundPlan.BranchCode = branch.Code;
                partsInboundPlan.BranchName = branch.Name;
                partsInboundPlan.PartsSalesCategoryId = partsOuterPurchaseChange.PartsSalesCategoryrId;
                partsInboundPlan.CounterpartCompanyId = -1;
                partsInboundPlan.CounterpartCompanyCode = "外采";
                partsInboundPlan.CounterpartCompanyName = "外采";
                partsInboundPlan.SourceId = partsOuterPurchaseChange.Id;
                partsInboundPlan.SourceCode = partsOuterPurchaseChange.Code;
                partsInboundPlan.InboundType = (int)DcsPartsInboundType.配件外采;
                partsInboundPlan.OriginalRequirementBillId = partsOuterPurchaseChange.Id;
                partsInboundPlan.OriginalRequirementBillCode = partsOuterPurchaseChange.Code;
                partsInboundPlan.OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.配件外采申请单;
                partsInboundPlan.Status = (int)DcsPartsInboundPlanStatus.新建;
                partsInboundPlan.IfWmsInterface = warehouse.WmsInterface;
                partsInboundPlan.Remark = partsOuterPurchaseChange.Remark;
                foreach(var partsOuterPurchaselist in partsOuterPurchaseChange.PartsOuterPurchaselists) {
                    var partsInboundPlanDetail = new PartsInboundPlanDetail {
                        PartsInboundPlanId = partsInboundPlan.Id,
                        SparePartId = partsOuterPurchaselist.PartsId,
                        SparePartCode = partsOuterPurchaselist.PartsCode,
                        SparePartName = partsOuterPurchaselist.PartsName,
                        PlannedAmount = partsOuterPurchaselist.Quantity,
                        Price = partsOuterPurchaselist.TradePrice
                    };
                    partsInboundPlan.PartsInboundPlanDetails.Add(partsInboundPlanDetail);
                }
                InsertToDatabase(partsInboundPlan);
                new PartsInboundPlanAch(this.DomainService).InsertPartsInboundPlanValidate(partsInboundPlan);
            }
            //外采收货仓库不为空时生成入库计划
            if((company.Type == (int)DcsCompanyType.代理库 || company.Type == (int)DcsCompanyType.服务站兼代理库) && partsOuterPurchaseChange.ReceivingWarehouseId.HasValue && partsOuterPurchaseChange.ReceivingWarehouseId.Value != default(int)) {
                var warehouse = ObjectContext.Warehouses.FirstOrDefault(r => r.Status == (int)DcsBaseDataStatus.有效 && r.Id == partsOuterPurchaseChange.ReceivingWarehouseId);
                if(warehouse == null) {
                    throw new ValidationException(ErrorStrings.PartsOuterPurchaseChange_Validation4);
                }
                var branch = ObjectContext.Branches.SingleOrDefault(r => r.Id == partsOuterPurchaseChange.BranchId);
                if(branch == null) {
                    throw new ValidationException(String.Format("不存在名为{0}的分公司", partsOuterPurchaseChange.BranchName));
                }
                //入库计划
                var partsInboundPlan = new PartsInboundPlan();
                partsInboundPlan.WarehouseId = warehouse.Id;
                partsInboundPlan.WarehouseCode = warehouse.Code;
                partsInboundPlan.WarehouseName = warehouse.Name;
                partsInboundPlan.StorageCompanyId = partsOuterPurchaseChange.CustomerCompanyId;
                partsInboundPlan.StorageCompanyCode = company.Code;
                partsInboundPlan.StorageCompanyName = company.Name;
                partsInboundPlan.StorageCompanyType = company.Type;
                partsInboundPlan.BranchId = partsOuterPurchaseChange.BranchId;
                partsInboundPlan.BranchCode = branch.Code;
                partsInboundPlan.BranchName = branch.Name;
                partsInboundPlan.PartsSalesCategoryId = partsOuterPurchaseChange.PartsSalesCategoryrId;
                partsInboundPlan.CounterpartCompanyId = -1;
                partsInboundPlan.CounterpartCompanyCode = "外采";
                partsInboundPlan.CounterpartCompanyName = "外采";
                partsInboundPlan.SourceId = partsOuterPurchaseChange.Id;
                partsInboundPlan.SourceCode = partsOuterPurchaseChange.Code;
                partsInboundPlan.InboundType = (int)DcsPartsInboundType.配件外采;
                partsInboundPlan.OriginalRequirementBillId = partsOuterPurchaseChange.Id;
                partsInboundPlan.OriginalRequirementBillCode = partsOuterPurchaseChange.Code;
                partsInboundPlan.OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.配件外采申请单;
                partsInboundPlan.Status = (int)DcsPartsInboundPlanStatus.新建;
                partsInboundPlan.IfWmsInterface = warehouse.WmsInterface;
                partsInboundPlan.Remark = partsOuterPurchaseChange.Remark;
                //入库计划清单
                foreach(var partsOuterPurchaselist in partsOuterPurchaseChange.PartsOuterPurchaselists) {
                    var partsInboundPlanDetail = new PartsInboundPlanDetail {
                        PartsInboundPlanId = partsInboundPlan.Id,
                        SparePartId = partsOuterPurchaselist.PartsId,
                        SparePartCode = partsOuterPurchaselist.PartsCode,
                        SparePartName = partsOuterPurchaselist.PartsName,
                        PlannedAmount = partsOuterPurchaselist.Quantity,
                        Price = partsOuterPurchaselist.TradePrice
                    };
                    partsInboundPlan.PartsInboundPlanDetails.Add(partsInboundPlanDetail);
                }
                InsertToDatabase(partsInboundPlan);
                new PartsInboundPlanAch(this.DomainService).InsertPartsInboundPlanValidate(partsInboundPlan);
            }
            var userInfo = Utils.GetCurrentUserInfo();
            if(company.Type == (int)DcsCompanyType.服务站) {
                var spareCodes = partsOuterPurchaseChange.PartsOuterPurchaselists.Select(r => r.PartsCode).ToArray();
                var dealerPartsStocks = ObjectContext.DealerPartsStocks.Where(r => r.SubDealerId == -1 && r.DealerId == company.Id && r.SalesCategoryId == partsOuterPurchaseChange.PartsSalesCategoryrId && spareCodes.Contains(r.SparePartCode)).ToArray();
                foreach(var partsOuterPurchaselist in partsOuterPurchaseChange.PartsOuterPurchaselists) {
                    var dealerPartsStock = dealerPartsStocks.FirstOrDefault(r => r.SparePartId == partsOuterPurchaselist.PartsId);
                    var dbPartDeleaveInformation = ObjectContext.PartDeleaveInformations.SingleOrDefault(c => c.OldPartId == partsOuterPurchaselist.PartsId && c.BranchId == partsOuterPurchaseChange.BranchId && c.PartsSalesCategoryId == partsOuterPurchaseChange.PartsSalesCategoryrId && c.Status == (int)DcsBaseDataStatus.有效);
                    int quantity;
                    if(dbPartDeleaveInformation != null) {
                        quantity = partsOuterPurchaselist.Quantity * dbPartDeleaveInformation.DeleaveAmount;
                    } else {
                        quantity = partsOuterPurchaselist.Quantity;
                    }
                    if(dealerPartsStock != null) {
                        dealerPartsStock.Quantity += quantity;
                        dealerPartsStock.ModifierId = userInfo.Id;
                        dealerPartsStock.ModifierName = userInfo.Name;
                        dealerPartsStock.ModifyTime = DateTime.Now;
                        UpdateToDatabase(dealerPartsStock);
                    } else {
                        var newdealerPartsStock = new DealerPartsStock {
                            DealerId = company.Id,
                            DealerCode = company.Code,
                            DealerName = company.Name,
                            BranchId = partsOuterPurchaseChange.BranchId,
                            SalesCategoryId = partsOuterPurchaseChange.PartsSalesCategoryrId,
                            SalesCategoryName = partsOuterPurchaseChange.PartsSalesCategoryName,
                            SparePartId = partsOuterPurchaselist.PartsId,
                            SparePartCode = partsOuterPurchaselist.PartsCode,
                            Quantity = quantity,
                            SubDealerId = -1
                        };
                        InsertToDatabase(newdealerPartsStock);
                        new DealerPartsStockAch(this.DomainService).InsertDealerPartsStockValidate(newdealerPartsStock);
                    }
                }
            }
        }
        //原审批
        public void 终审配件外采申请单(PartsOuterPurchaseChange partsOuterPurchaseChange) {
            //var userinfo = Utils.GetCurrentUserInfo();
            //var dbPartsOuterPurchaseChange = ObjectContext.PartsOuterPurchaseChanges.Where(v => v.Id == partsOuterPurchaseChange.Id && v.Status == (int)DcsPartsOuterPurchaseChangeStatus.审核通过).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            //if(dbPartsOuterPurchaseChange == null)
            //    throw new ValidationException(ErrorStrings.PartsOuterPurchaseChange_Validation8);
            //partsOuterPurchaseChange.ApproverId = userinfo.Id;
            //partsOuterPurchaseChange.ApproverName = userinfo.Name;
            //partsOuterPurchaseChange.ApproveTime = DateTime.Now;
            //finalApproveBusiness(partsOuterPurchaseChange);


            var dbPartsOuterPurchaseChange = ObjectContext.PartsOuterPurchaseChanges.Where(v => v.Id == partsOuterPurchaseChange.Id && v.Status == (int)DcsPartsOuterPurchaseChangeStatus.审核通过).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbPartsOuterPurchaseChange == null)
                throw new ValidationException(ErrorStrings.PartsOuterPurchaseChange_Validation7);
            var userinfo = Utils.GetCurrentUserInfo();
            if(partsOuterPurchaseChange.ApprovalId == 1) {
                var inventoryConfig = ObjectContext.MultiLevelApproveConfigs.FirstOrDefault(r => ((partsOuterPurchaseChange.Amount >= r.MinApproveFee && partsOuterPurchaseChange.Amount < r.MaxApproveFee) || partsOuterPurchaseChange.Amount < 0)
                   && r.Type == (int)DCSMultiLevelApproveConfigType.配件外采管理 && r.ApproverId == userinfo.Id && r.IsFinished && r.Status == (int)DcsBaseDataStatus.有效);
                if(inventoryConfig != null) {
                    finalApproveBusiness(partsOuterPurchaseChange);
                } else {
                    partsOuterPurchaseChange.Status = (int)DcsPartsOuterPurchaseChangeStatus.审批通过;
                }
            } else {
                partsOuterPurchaseChange.Status = (int)DcsPartsOuterPurchaseChangeStatus.新建;
                partsOuterPurchaseChange.RejecterId = userinfo.Id;
                partsOuterPurchaseChange.RejecterName = userinfo.Name;
                partsOuterPurchaseChange.RejectTime = DateTime.Now;
                partsOuterPurchaseChange.RejectComment = partsOuterPurchaseChange.ApproveComment;
            }
            partsOuterPurchaseChange.ApproverId = userinfo.Id;
            partsOuterPurchaseChange.ApproverName = userinfo.Name;
            partsOuterPurchaseChange.ApproveTime = DateTime.Now;
            UpdateToDatabase(partsOuterPurchaseChange);
            this.UpdatePartsOuterPurchaseChangeValidate(partsOuterPurchaseChange);

        }

        public void 作废配件外采申请单(PartsOuterPurchaseChange partsOuterPurchaseChange) {
            var dbPartsOuterPurchaseChange = ObjectContext.PartsOuterPurchaseChanges.Where(v => v.Id == partsOuterPurchaseChange.Id && (v.Status == (int)DcsPartsOuterPurchaseChangeStatus.提交 || v.Status == (int)DcsPartsOuterPurchaseChangeStatus.新建)).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbPartsOuterPurchaseChange == null)
                throw new ValidationException(ErrorStrings.PartsOuterPurchaseChange_Validation5);
            var userinfo = Utils.GetCurrentUserInfo();
            UpdateToDatabase(partsOuterPurchaseChange);
            partsOuterPurchaseChange.Status = (int)DcsPartsOuterPurchaseChangeStatus.作废;
            partsOuterPurchaseChange.AbandonerId = userinfo.Id;
            partsOuterPurchaseChange.AbandonerName = userinfo.Name;
            partsOuterPurchaseChange.AbandonTime = DateTime.Now;
            this.UpdatePartsOuterPurchaseChangeValidate(partsOuterPurchaseChange);
        }


        public void 生成配件外采申请单(PartsOuterPurchaseChange partsOuterPurchaseChange) {
            InsertPartsOuterPurchaseChange(partsOuterPurchaseChange);
            var partsOuterPurchaselists = partsOuterPurchaseChange.PartsOuterPurchaselists;
            var partIds = partsOuterPurchaselists.Select(r => r.PartsId).ToArray();
            var partsList = partsOuterPurchaselists.Select(r => new {
                r.PartsId,
                r.PartsCode,
                r.Quantity
            }).ToArray();
            var dealerPartsStocks = ObjectContext.DealerPartsStocks.Where(r => partIds.Any(v => r.SparePartId == v) && r.DealerId == partsOuterPurchaseChange.CustomerCompanyId && r.SalesCategoryId == partsOuterPurchaseChange.PartsSalesCategoryrId && r.SubDealerId == -1).ToArray();
            var checkdealerPartsStocks = dealerPartsStocks.GroupBy(r => r.SparePartId).Where(r => r.Count() > 1).ToArray();
            if(checkdealerPartsStocks.Length > 1) {
                throw new ValidationException(ErrorStrings.PartsOuterPurchaseChange_Validation9);
            }
            if(dealerPartsStocks.Length > 0) {
                foreach(var dealerPartsStock in dealerPartsStocks) {
                    var tempQuantity = partsList.Where(r => r.PartsId == dealerPartsStock.SparePartId).Select(r => r.Quantity).FirstOrDefault();
                    if(tempQuantity < dealerPartsStock.Quantity)
                        throw new ValidationException(string.Format(ErrorStrings.PartsOuterPurchaselist_Validation1, dealerPartsStock.SparePartCode));
                }
            }
            if(partsOuterPurchaseChange.SourceId != null) {
                //var repairOrder = ObjectContext.RepairOrders.SingleOrDefault(r => r.Id == partsOuterPurchaseChange.SourceId);
                //if(repairOrder != null) {
                //    if(repairOrder.IsOutPurOrSale.HasValue && repairOrder.IsOutPurOrSale.Value) {
                //        throw new ValidationException("维修缺件已经生成外采或紧急销售订单，不允许再次提报！");
                //    }else {
                //        repairOrder.IsOutPurOrSale = true;
                //        UpdateToDatabase(repairOrder);
                //    }
                //}
            }
        }


        public void 维修单生成外采申请单(PartsOuterPurchaseChange partsOuterPurchaseChange) {
            //这个方法作用是生成外采申请单以后，回头修改维修单上的一个属性
            var tempId = partsOuterPurchaseChange.SourceId;
            //只有维修单能紧急销售订单
            //var repairOrder = ObjectContext.RepairOrders.SingleOrDefault(r => r.Id == tempId);
            //if(repairOrder != null) {
            //    if(repairOrder.IsOutPurOrSale.HasValue && repairOrder.IsOutPurOrSale.Value) {
            //        throw new ValidationException("维修缺件已经生成外采或紧急销售订单，不允许再次提报！");
            //    }else {
            //        repairOrder.IsOutPurOrSale = true;
            //    }
            //}
            //UpdateToDatabase(repairOrder);
        }

        public void 驳回配件外采申请单(PartsOuterPurchaseChange partsOuterPurchaseChange) {
            var dbPartsOuterPurchaseChange = ObjectContext.PartsOuterPurchaseChanges.Where(v => v.Id == partsOuterPurchaseChange.Id && v.Status == (int)DcsPartsOuterPurchaseChangeStatus.提交).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbPartsOuterPurchaseChange == null)
                throw new ValidationException(ErrorStrings.PartsOuterPurchaseChange_Validation2);
            var userinfo = Utils.GetCurrentUserInfo();
            partsOuterPurchaseChange.Status = (int)DcsPartsOuterPurchaseChangeStatus.新建;
            partsOuterPurchaseChange.RejecterId = userinfo.Id;
            partsOuterPurchaseChange.RejecterName = userinfo.Name;
            partsOuterPurchaseChange.RejectTime = DateTime.Now;
        }

        public void 终端企业审批配件外出(PartsOuterPurchaseChange partsOuterPurchaseChange) {
            var dbPartsOuterPurchaseChange = ObjectContext.PartsOuterPurchaseChanges.Where(v => v.Id == partsOuterPurchaseChange.Id && v.Status == (int)DcsPartsOuterPurchaseChangeStatus.提交).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbPartsOuterPurchaseChange == null)
                throw new ValidationException(ErrorStrings.PartsOuterPurchaseChange_Validation2);
            var partsSalesCategory = ObjectContext.PartsSalesCategories.SingleOrDefault(r => r.Id == dbPartsOuterPurchaseChange.PartsSalesCategoryrId);
            if(partsSalesCategory == null) {
                throw new ValidationException(String.Format(ErrorStrings.PartsOuterPurchaseChange_Validation10, dbPartsOuterPurchaseChange.PartsSalesCategoryName));
            }
            if(!partsSalesCategory.IsNotWarranty) {
                throw new ValidationException(ErrorStrings.PartsOuterPurchaseChange_Validation11);
            }
            partsOuterPurchaseChange.Status = (int)DcsPartsOuterPurchaseChangeStatus.生效;
            var company = ObjectContext.Companies.SingleOrDefault(r => r.Id == partsOuterPurchaseChange.CustomerCompanyId);
            if(company == null) {
                throw new ValidationException(String.Format(ErrorStrings.PartsOuterPurchaseChange_Validation12, partsOuterPurchaseChange.CustomerCompanyNace));
            }
            if((company.Type == (int)DcsCompanyType.代理库 || company.Type == (int)DcsCompanyType.服务站兼代理库) && (!partsOuterPurchaseChange.ReceivingWarehouseId.HasValue)) {
                var salesUnit = ObjectContext.SalesUnits.FirstOrDefault(r => r.OwnerCompanyId == partsOuterPurchaseChange.CustomerCompanyId && r.PartsSalesCategoryId == partsOuterPurchaseChange.PartsSalesCategoryrId);
                if(salesUnit == null) {
                    throw new ValidationException(String.Format(ErrorStrings.PartsOuterPurchaseChange_Validation13, partsOuterPurchaseChange.PartsSalesCategoryName));
                }
                var salesUnitAffiWarehouse = ObjectContext.SalesUnitAffiWarehouses.FirstOrDefault(r => r.SalesUnitId == salesUnit.Id);
                if(salesUnitAffiWarehouse == null) {
                    throw new ValidationException(String.Format(ErrorStrings.PartsOuterPurchaseChange_Validation14, salesUnit.Name));
                }
                var branch = ObjectContext.Branches.SingleOrDefault(r => r.Id == partsOuterPurchaseChange.BranchId);
                if(branch == null) {
                    throw new ValidationException(String.Format(ErrorStrings.PartsOuterPurchaseChange_Validation15, partsOuterPurchaseChange.BranchName));
                }
                var partsInboundPlan = new PartsInboundPlan {
                    WarehouseId = salesUnitAffiWarehouse.WarehouseId,
                    WarehouseCode = salesUnitAffiWarehouse.Warehouse.Code,
                    WarehouseName = salesUnitAffiWarehouse.Warehouse.Name,
                    IfWmsInterface = salesUnitAffiWarehouse.Warehouse.WmsInterface,
                    StorageCompanyId = partsOuterPurchaseChange.CustomerCompanyId,
                    StorageCompanyCode = partsOuterPurchaseChange.CustomerCompanyCode,
                    StorageCompanyName = partsOuterPurchaseChange.CustomerCompanyNace,
                    StorageCompanyType = company.Type,
                    BranchId = partsOuterPurchaseChange.BranchId,
                    BranchName = branch.Name,
                    BranchCode = branch.Code,
                    CounterpartCompanyId = 0,
                    CounterpartCompanyCode = "",
                    CounterpartCompanyName = "",
                    SourceId = partsOuterPurchaseChange.Id,
                    SourceCode = partsOuterPurchaseChange.Code,
                    PartsSalesCategoryId = partsOuterPurchaseChange.PartsSalesCategoryrId,
                    InboundType = (int)DcsPartsInboundType.配件外采,
                    CustomerAccountId = null,
                    OriginalRequirementBillId = partsOuterPurchaseChange.SourceId ?? 0,
                    OriginalRequirementBillCode = partsOuterPurchaseChange.SourceCode,
                    OriginalRequirementBillType = partsOuterPurchaseChange.SourceCategoryr ?? 0,
                    Status = (int)DcsPartsInboundPlanStatus.新建,
                    Remark = partsOuterPurchaseChange.Remark
                };
                foreach(var partsOuterPurchaselist in partsOuterPurchaseChange.PartsOuterPurchaselists) {
                    var partsInboundPlanDetail = new PartsInboundPlanDetail {
                        PartsInboundPlanId = partsInboundPlan.Id,
                        SparePartId = partsOuterPurchaselist.PartsId,
                        SparePartCode = partsOuterPurchaselist.PartsCode,
                        SparePartName = partsOuterPurchaselist.PartsName,
                        PlannedAmount = partsOuterPurchaselist.Quantity,
                        Price = partsOuterPurchaselist.TradePrice
                    };
                    partsInboundPlan.PartsInboundPlanDetails.Add(partsInboundPlanDetail);
                }
                InsertToDatabase(partsInboundPlan);
                new PartsInboundPlanAch(this.DomainService).InsertPartsInboundPlanValidate(partsInboundPlan);
            }
            //外采收货仓库不为空时生成入库计划
            if((company.Type == (int)DcsCompanyType.代理库 || company.Type == (int)DcsCompanyType.服务站兼代理库) && partsOuterPurchaseChange.ReceivingWarehouseId.HasValue && partsOuterPurchaseChange.ReceivingWarehouseId.Value != default(int)) {
                var warehouse = ObjectContext.Warehouses.FirstOrDefault(r => r.Status == (int)DcsBaseDataStatus.有效 && r.Id == partsOuterPurchaseChange.ReceivingWarehouseId);
                if(warehouse == null) {
                    throw new ValidationException(ErrorStrings.PartsOuterPurchaseChange_Validation4);
                }
                var branch = ObjectContext.Branches.SingleOrDefault(r => r.Id == partsOuterPurchaseChange.BranchId);
                if(branch == null) {
                    throw new ValidationException(String.Format(ErrorStrings.PartsOuterPurchaseChange_Validation15, partsOuterPurchaseChange.BranchName));
                }
                var partsInboundPlan = new PartsInboundPlan {
                    WarehouseId = warehouse.Id,
                    WarehouseCode = warehouse.Code,
                    WarehouseName = warehouse.Name,
                    IfWmsInterface = warehouse.WmsInterface,
                    StorageCompanyId = partsOuterPurchaseChange.CustomerCompanyId,
                    StorageCompanyCode = partsOuterPurchaseChange.CustomerCompanyCode,
                    StorageCompanyName = partsOuterPurchaseChange.CustomerCompanyNace,
                    StorageCompanyType = company.Type,
                    BranchId = partsOuterPurchaseChange.BranchId,
                    BranchName = branch.Name,
                    BranchCode = branch.Code,
                    CounterpartCompanyId = 0,
                    CounterpartCompanyCode = "",
                    CounterpartCompanyName = "",
                    SourceId = partsOuterPurchaseChange.Id,
                    SourceCode = partsOuterPurchaseChange.Code,
                    PartsSalesCategoryId = partsOuterPurchaseChange.PartsSalesCategoryrId,
                    InboundType = (int)DcsPartsInboundType.配件外采,
                    CustomerAccountId = null,
                    OriginalRequirementBillId = partsOuterPurchaseChange.SourceId ?? 0,
                    OriginalRequirementBillCode = partsOuterPurchaseChange.SourceCode,
                    OriginalRequirementBillType = partsOuterPurchaseChange.SourceCategoryr ?? 0,
                    Status = (int)DcsPartsInboundPlanStatus.新建,
                    Remark = partsOuterPurchaseChange.Remark
                };
                foreach(var partsOuterPurchaselist in partsOuterPurchaseChange.PartsOuterPurchaselists) {
                    var partsInboundPlanDetail = new PartsInboundPlanDetail {
                        PartsInboundPlanId = partsInboundPlan.Id,
                        SparePartId = partsOuterPurchaselist.PartsId,
                        SparePartCode = partsOuterPurchaselist.PartsCode,
                        SparePartName = partsOuterPurchaselist.PartsName,
                        PlannedAmount = partsOuterPurchaselist.Quantity,
                        Price = partsOuterPurchaselist.TradePrice
                    };
                    partsInboundPlan.PartsInboundPlanDetails.Add(partsInboundPlanDetail);
                }
                InsertToDatabase(partsInboundPlan);
                new PartsInboundPlanAch(this.DomainService).InsertPartsInboundPlanValidate(partsInboundPlan);
            }
            if(company.Type == (int)DcsCompanyType.服务站) {
                var userInfo = Utils.GetCurrentUserInfo();
                var spareCodes = partsOuterPurchaseChange.PartsOuterPurchaselists.Select(r => r.PartsCode).ToArray();
                var dealerPartsStocks = ObjectContext.DealerPartsStocks.Where(r => r.SubDealerId == -1 && r.DealerId == company.Id && r.SalesCategoryId == partsOuterPurchaseChange.PartsSalesCategoryrId && spareCodes.Contains(r.SparePartCode)).ToArray();
                foreach(var partsOuterPurchaselist in partsOuterPurchaseChange.PartsOuterPurchaselists) {
                    var dealerPartsStock = dealerPartsStocks.FirstOrDefault(r => r.SparePartId == partsOuterPurchaselist.PartsId);
                    var dbPartDeleaveInformation = ObjectContext.PartDeleaveInformations.SingleOrDefault(c => c.OldPartId == partsOuterPurchaselist.PartsId && c.BranchId == partsOuterPurchaseChange.BranchId && c.PartsSalesCategoryId == partsOuterPurchaseChange.PartsSalesCategoryrId && c.Status == (int)DcsBaseDataStatus.有效);
                    if(dealerPartsStock != null) {
                        if(dbPartDeleaveInformation != null) {
                            dealerPartsStock.Quantity += partsOuterPurchaselist.Quantity * dbPartDeleaveInformation.DeleaveAmount;
                        } else {
                            dealerPartsStock.Quantity += partsOuterPurchaselist.Quantity;
                        }
                        dealerPartsStock.ModifierId = userInfo.Id;
                        dealerPartsStock.ModifierName = userInfo.Name;
                        dealerPartsStock.ModifyTime = DateTime.Now;
                        UpdateToDatabase(dealerPartsStock);
                    } else {
                        var newdealerPartsStock = new DealerPartsStock {
                            DealerId = company.Id,
                            DealerCode = company.Code,
                            DealerName = company.Name,
                            BranchId = partsOuterPurchaseChange.BranchId,
                            SalesCategoryId = partsOuterPurchaseChange.PartsSalesCategoryrId,
                            SalesCategoryName = partsOuterPurchaseChange.PartsSalesCategoryName,
                            SparePartId = partsOuterPurchaselist.PartsId,
                            SparePartCode = partsOuterPurchaselist.PartsCode,
                            Quantity = dbPartDeleaveInformation != null ? partsOuterPurchaselist.Quantity * dbPartDeleaveInformation.DeleaveAmount : partsOuterPurchaselist.Quantity,
                            SubDealerId = -1
                        };
                        InsertToDatabase(newdealerPartsStock);
                        new DealerPartsStockAch(this.DomainService).InsertDealerPartsStockValidate(newdealerPartsStock);
                    }
                }
            }
        }

    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:11:30
        //原分布类的函数全部转移到Ach                                                               
        [Update(UsingCustomMethod = true)]
        public void 确认配件外采申请单(PartsOuterPurchaseChange partsOuterPurchaseChange) {
            new PartsOuterPurchaseChangeAch(this).确认配件外采申请单(partsOuterPurchaseChange);
        }
        [Update(UsingCustomMethod = true)]
        public void 高级审核配件外采申请单(PartsOuterPurchaseChange partsOuterPurchaseChange) {
            new PartsOuterPurchaseChangeAch(this).高级审核配件外采申请单(partsOuterPurchaseChange);
        }
        [Update(UsingCustomMethod = true)]
        public void 高级审批配件外采申请单(PartsOuterPurchaseChange partsOuterPurchaseChange) {
            new PartsOuterPurchaseChangeAch(this).高级审批配件外采申请单(partsOuterPurchaseChange);
        }

        [Update(UsingCustomMethod = true)]
        public void 提交配件外采申请单(PartsOuterPurchaseChange partsOuterPurchaseChange) {
            new PartsOuterPurchaseChangeAch(this).提交配件外采申请单(partsOuterPurchaseChange);
        }
        [Update(UsingCustomMethod = true)]
        public void 初审配件外采申请单(PartsOuterPurchaseChange partsOuterPurchaseChange) {
            new PartsOuterPurchaseChangeAch(this).初审配件外采申请单(partsOuterPurchaseChange);
        }

        [Update(UsingCustomMethod = true)]
        public void 终审配件外采申请单(PartsOuterPurchaseChange partsOuterPurchaseChange) {
            new PartsOuterPurchaseChangeAch(this).终审配件外采申请单(partsOuterPurchaseChange);
        }

        [Update(UsingCustomMethod = true)]
        public void 审批配件外采申请单(PartsOuterPurchaseChange partsOuterPurchaseChange) {
            new PartsOuterPurchaseChangeAch(this).审批配件外采申请单(partsOuterPurchaseChange);
        }

        [Update(UsingCustomMethod = true)]
        public void 作废配件外采申请单(PartsOuterPurchaseChange partsOuterPurchaseChange) {
            new PartsOuterPurchaseChangeAch(this).作废配件外采申请单(partsOuterPurchaseChange);
        }

        [Update(UsingCustomMethod = true)]
        public void 生成配件外采申请单(PartsOuterPurchaseChange partsOuterPurchaseChange) {
            new PartsOuterPurchaseChangeAch(this).生成配件外采申请单(partsOuterPurchaseChange);
        }

        [Update(UsingCustomMethod = true)]
        public void 维修单生成外采申请单(PartsOuterPurchaseChange partsOuterPurchaseChange) {
            new PartsOuterPurchaseChangeAch(this).维修单生成外采申请单(partsOuterPurchaseChange);
        }

        [Update(UsingCustomMethod = true)]
        public void 驳回配件外采申请单(PartsOuterPurchaseChange partsOuterPurchaseChange) {
            new PartsOuterPurchaseChangeAch(this).驳回配件外采申请单(partsOuterPurchaseChange);
        }

        [Update(UsingCustomMethod = true)]
        public void 终端企业审批配件外出(PartsOuterPurchaseChange partsOuterPurchaseChange) {
            new PartsOuterPurchaseChangeAch(this).终端企业审批配件外出(partsOuterPurchaseChange);
        }
    }
}
