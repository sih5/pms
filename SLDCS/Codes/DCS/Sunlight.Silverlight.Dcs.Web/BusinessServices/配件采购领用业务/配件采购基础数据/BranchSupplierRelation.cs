﻿using Sunlight.Silverlight.Web;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class BranchSupplierRelationAch : DcsSerivceAchieveBase {
        public BranchSupplierRelationAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 作废分公司与供应商关系(BranchSupplierRelation branchSupplierRelation) {
            var dbBranchSupplierRelation = ObjectContext.BranchSupplierRelations.Where(r => r.Id == branchSupplierRelation.Id && r.Status != (int)DcsBaseDataStatus.作废).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbBranchSupplierRelation == null)
                throw new ValidationException(ErrorStrings.BranchSupplierRelation_Validation3);
            var userinfo = Utils.GetCurrentUserInfo();
            UpdateToDatabase(branchSupplierRelation);
            branchSupplierRelation.AbandonerId = userinfo.Id;
            branchSupplierRelation.AbandonerName = userinfo.Name;
            branchSupplierRelation.AbandonTime = System.DateTime.Now;
            branchSupplierRelation.Status = (int)DcsBaseDataStatus.作废;
            this.UpdateBranchSupplierRelationValidate(branchSupplierRelation);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:11:30
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 作废分公司与供应商关系(BranchSupplierRelation branchSupplierRelation) {
            new BranchSupplierRelationAch(this).作废分公司与供应商关系(branchSupplierRelation);
        }
    }
}