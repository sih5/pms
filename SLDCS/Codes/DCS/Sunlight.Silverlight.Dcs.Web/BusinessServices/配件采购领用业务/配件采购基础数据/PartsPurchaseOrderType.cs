﻿using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsPurchaseOrderTypeAch : DcsSerivceAchieveBase {
        public PartsPurchaseOrderTypeAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 作废配件采购订单类型(PartsPurchaseOrderType partsPurchaseOrderType) {
            var dbPartsPurchaseOrderType = ObjectContext.PartsPurchaseOrderTypes.Where(r => r.Id == partsPurchaseOrderType.Id && r.Status != (int)DcsBaseDataStatus.作废).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbPartsPurchaseOrderType == null)
                throw new ValidationException(ErrorStrings.PartsPurchaseOrderType_Validation2);
            var userinfo = Utils.GetCurrentUserInfo();
            UpdateToDatabase(partsPurchaseOrderType);
            partsPurchaseOrderType.AbandonerId = userinfo.Id;
            partsPurchaseOrderType.AbandonerName = userinfo.Name;
            partsPurchaseOrderType.AbandonTime = System.DateTime.Now;
            partsPurchaseOrderType.Status = (int)DcsBaseDataStatus.作废;
            this.UpdatePartsPurchaseOrderTypeValidate(partsPurchaseOrderType);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:11:30
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 作废配件采购订单类型(PartsPurchaseOrderType partsPurchaseOrderType) {
            new PartsPurchaseOrderTypeAch(this).作废配件采购订单类型(partsPurchaseOrderType);
        }
    }
}
