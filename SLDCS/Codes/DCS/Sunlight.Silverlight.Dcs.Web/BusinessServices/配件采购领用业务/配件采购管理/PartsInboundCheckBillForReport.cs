﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsInboundCheckBillForReportAch : DcsSerivceAchieveBase {
        public PartsInboundCheckBillForReportAch(DcsDomainService domainService)
            : base(domainService) {
        }
        public IEnumerable<PartsInboundCheckBillForReport> 采购入库及供应商差价报表查询(DateTime? bCreateTime, DateTime? eCreateTime, DateTime? bPartsPurchaseOrderDate, DateTime? ePartsPurchaseOrderDate, string code, string partsPurchaseOrderCode, string sparePartCode, string supplierPartName, int? settleBillStatus, string settleBillCode) {
            string SQL = @"select pb.id,
                           pc.code,
                           pc.CounterpartCompanyCode as PartsSupplierCode,
                           pc.counterpartcompanyname as PartsSupplierName,
                           pb.sparepartcode,
                           pb.sparepartname,
                           ph.referencecode,
                           pr.SupplierPartCode,
                           ph.PartABC,
                           pb.inspectedquantity,
                           pc.createtime,
                           pb.settlementprice,
                           po.code              as PartsPurchaseOrderCode,
                           po.createtime        as PartsPurchaseOrderDate,
                           pt.name              as PartsPurchaseOrderType,
                           ps.code              as SettleBillCode,
                           ps.status            as SettleBillStatus,
                           yy.PurchasePrice,
                           pb.settlementprice- nvl(yy.PurchasePrice,0) as DifferPurchasePrice,
                           (pb.settlementprice- nvl(yy.PurchasePrice,0)) *pb.inspectedquantity as DifferPurchasePriceAll,
                            pc.remark as PartsInboundCheckBillRemark,
                            po.remark as PartsPurchaseOrderRemark
                          from partsinboundcheckbilldetail pb
                          join partsinboundcheckbill pc
                              on pb.partsinboundcheckbillid = pc.id
                             join PartsPurchaseOrder po
                            on pc.originalrequirementbillid = po.id
                            and pc.originalrequirementbillcode = po.code
                           join  PartsPurchaseOrderDetail pp on pb.sparepartid=pp.sparepartid and po.id =pp.partspurchaseorderid and pp.PriceType=2                         
                           left join PartsSupplierRelation pr
                            on pb.sparepartid = pr.partid
                            and pc.branchid = pr.branchid
                              and pc.partssalescategoryid = pr.partssalescategoryid
                             and pr.status = 1
                            and pc.counterpartcompanyid =pr.SupplierId
                            join partsbranch ph
                           on pb.sparepartid = ph.partid
                            and ph.status = 1
                            and ph.partssalescategoryid = pc.partssalescategoryid
                            and pc.branchid = ph.branchid
                             join PartsPurchaseOrderType pt
                             on po.partspurchaseordertypeid = pt.id
                             left join PartsPurchaseSettleRef pf
                             on pc.id = pf.sourceid
                               and exists (select 1
                              from PartsPurchaseSettleBill ps
                             where pf.partspurchasesettlebillid = ps.id
                               and ps.status <> 99
                               and ps.status <> 4)
                           and pc.code = pf.sourcecode
                            left join PartsPurchaseSettleBill ps
                           on pf.partspurchasesettlebillid = ps.id
                          and ps.status <> 99
                         and ps.status <> 4
                        left join (select *
                                       from (select Extent5.id, Extent5.PartId,
                                                    Extent5.BranchId,
                                                    Extent5.PartsSupplierId,
                                                    Extent5.Validfrom,
                                                    Extent5.Validto,
                                                    Extent5.Purchaseprice,
                                                    row_number() over(partition by Extent5.PartId, Extent5.BranchId, Extent5.PartsSupplierId, Extent5.Purchaseprice order by Extent5.Createtime desc) as ydy
                                               from PartsPurchasePricing Extent5
                                              where Extent5.PriceType = 1
                                                and Extent5.Status = 2
                                                AND Extent5.Status = 2) tt) yy
                            ON pb.sparepartid = yy.PartId
                           AND pc.BranchId = yy.BranchId      
                           AND po.partssupplierid = yy.PartsSupplierId
                           and yy.Validfrom <= po.createtime
                           and yy.Validto >= po.createtime
                           and yy.ydy = 1  where pc.InboundType = 1 and pc.StorageCompanyType = 1 ";
            if(bCreateTime.HasValue && bCreateTime.Value == Convert.ToDateTime("9999/12/31 23:59:59")) {
                bCreateTime = null;
            }
            if(bPartsPurchaseOrderDate.HasValue && bPartsPurchaseOrderDate.Value == Convert.ToDateTime("9999/12/31 23:59:59")) {
                bPartsPurchaseOrderDate = null;
            }
            if(!string.IsNullOrEmpty(code)) {
                SQL = SQL + " and LOWER(pc.code) like '%" + code.ToLower() + "%'";
            }
            if(!string.IsNullOrEmpty(partsPurchaseOrderCode)) {
                SQL = SQL + " and LOWER(po.Code) like '%" + partsPurchaseOrderCode.ToLower() + "%'";
            }
            if(!string.IsNullOrEmpty(sparePartCode)) {
                SQL = SQL + " and LOWER(pb.sparePartCode) like '%" + sparePartCode.ToLower() + "%'";
            }
            if(!string.IsNullOrEmpty(supplierPartName)) {
                SQL = SQL + " and LOWER(pc.counterpartcompanyname) like '%" + supplierPartName.ToLower() + "%'";
            }
            if(settleBillStatus.HasValue) {
                SQL = SQL + " and ps.status=" + settleBillStatus.Value;
            }
            if(!string.IsNullOrEmpty(settleBillCode)) {
                SQL = SQL + " and LOWER(ps.code) like '%" + settleBillCode.ToLower() + "%'";
            }
            if(bCreateTime.HasValue) {
                SQL = SQL + " and pc.CreateTime>= to_date('" + bCreateTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }
            if(eCreateTime.HasValue) {
                SQL = SQL + " and pc.CreateTime<= to_date('" + eCreateTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }
            if(bPartsPurchaseOrderDate.HasValue) {
                SQL = SQL + " and po.CreateTime>= to_date('" + bPartsPurchaseOrderDate.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }
            if(ePartsPurchaseOrderDate.HasValue) {
                SQL = SQL + " and po.CreateTime<= to_date('" + ePartsPurchaseOrderDate.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }
            var search = ObjectContext.ExecuteStoreQuery<PartsInboundCheckBillForReport>(SQL).ToList();
            return search;
        }
        public IEnumerable<PartsInboundCheckBillForReport> 配件采购入库检验单统计(DateTime? bCreateTime, DateTime? eCreateTime, string code, string partsSupplierCode, string partsSupplierName, string sparePartCode, string supplierPartCode, string sparePartName, int? partABC, string settleBillStatus, string invoice,string referenceCode) {
            string SQL = @"select * from (select ck.id, pc.code,
                          pc.CounterpartCompanyCode as PartsSupplierCode,
                          pc.counterpartcompanyname as PartsSupplierName,
                          ck.sparepartcode,
                          pr.SupplierPartCode,
                          sp.referencecode,
                          ck.sparepartname,
                          ph.partabc,
                          ck.settlementprice,
                          ck.inspectedquantity,
                          ck.settlementprice * ck.inspectedquantity as settlementpriceSum,
                          pc.createtime,
                         nvl(pt.status,0) as SettleBillStatus,
                         pc.remark as PartsInboundCheckBillRemark,
                           (select listagg(a.InvoiceNumber, ',') within group(order by b.partspurchasesettlebillid)
                  from InvoiceInformation a
                  join PurchaseSettleInvoiceRel b
                    on a.id = b.InvoiceId
                 where pc.id = pf.sourceid
                   and pf.sourcetype = 5
                   and b.PartsPurchaseSettleBillId = pt.id
                   and pt.status not in(4,99)) as Invoice,
                          ck.CostPrice,
                         ck.CostPrice *ck.InspectedQuantity as CostPriceAll,
                       pt.code as SettleBillCode
                       from partsinboundcheckbilldetail ck
                       join partsinboundcheckbill pc
                       on ck.partsinboundcheckbillid = pc.id
                        left join PartsSupplierRelation pr
                        on pc.counterpartcompanyid = pr.SupplierId
                       and pr.partid = ck.sparepartid
                       join sparepart sp
                       on ck.sparepartid = sp.id
                       join partsbranch ph
                       on ck.sparepartid = ph.partid
                       and ph.status = 1
                       and pc.partssalescategoryid = ph.partssalescategoryid
                       and ph.branchid = pc.branchid
                    left join PartsPurchaseSettleRef pf
            on pc.id = pf.sourceid
           and pf.sourcetype = 5
           and exists (select 1
                  from partspurchasesettlebill pt
                 where pf.partspurchasesettlebillid = pt.id
                   and pt.status not in (4, 99))
          left join partspurchasesettlebill pt
            on pf.partspurchasesettlebillid = pt.id
           and pt.status not in (4, 99)
                       where pc.InboundType = 1
                      and pc.StorageCompanyid= 12730 ";
            if(bCreateTime.HasValue && bCreateTime.Value == Convert.ToDateTime("9999/12/31 23:59:59")) {
                bCreateTime = null;
            }
            if(eCreateTime.HasValue && eCreateTime.Value == Convert.ToDateTime("9999/12/31 23:59:59")) {
                eCreateTime = null;
            }
            if(!string.IsNullOrEmpty(code)) {
                SQL = SQL + " and LOWER(pc.code) like '%" + code.ToLower() + "%'";
            }
            if(!string.IsNullOrEmpty(partsSupplierCode)) {
                SQL = SQL + " and LOWER(pc.CounterpartCompanyCode) like '%" + partsSupplierCode.ToLower() + "%'";
            }
            if(!string.IsNullOrEmpty(partsSupplierName)) {
                SQL = SQL + " and LOWER(pc.counterpartcompanyname) like '%" + partsSupplierName.ToLower() + "%'";
            }
            if(!string.IsNullOrEmpty(sparePartCode)) {
                SQL = SQL + " and LOWER(ck.sparePartCode) like '%" + sparePartCode.ToLower() + "%'";
            }
            if(!string.IsNullOrEmpty(supplierPartCode)) {
                SQL = SQL + " and LOWER(pr.supplierPartCode) like '%" + supplierPartCode.ToLower() + "%'";
            }
            if(partABC.HasValue) {
                SQL = SQL + " and ph.partABC=" + partABC.Value;
            }
            if(!string.IsNullOrEmpty(sparePartName)) {
                SQL = SQL + " and LOWER(ck.sparePartName) like '%" + sparePartName.ToLower() + "%'";
            }
            if(bCreateTime.HasValue) {
                SQL = SQL + " and pc.CreateTime>= to_date('" + bCreateTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }
            if(eCreateTime.HasValue) {
                SQL = SQL + " and pc.CreateTime<= to_date('" + eCreateTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }
            if(!string.IsNullOrEmpty(referenceCode)) {
                SQL = SQL + " and LOWER(sp.referenceCode) like '%" + referenceCode.ToLower() + "%'";
            }
          
            SQL = SQL + " )tt where 1=1";
            if(!string.IsNullOrEmpty(settleBillStatus)) {
                SQL = SQL + " and tt.settleBillStatus in (" + settleBillStatus+")";
            }
            if(!string.IsNullOrEmpty(invoice)) {
                SQL = SQL + " and LOWER(tt.invoice) like '%" + invoice.ToLower() + "%'";
            }
            var search = ObjectContext.ExecuteStoreQuery<PartsInboundCheckBillForReport>(SQL).ToList();
            return search;
        }
        public IEnumerable<PartsInboundCheckBillForReport> 中心库入库单统计(DateTime? bCreateTime, DateTime? eCreateTime, string code, string sparePartCode, string referenceCode, int? warehouseId, int? inboundType, string partsSalesOrderCode) {
            string SQL = @"select ck.id,
                           po.inboundtype,
                           (select mt.Name
                              from MarketingDepartment mt
                              join Agency ag
                                on mt.id = ag.marketingdepartmentid
                             where ag.id = po.storagecompanyid) as MarketingDepartmentName,
                           po.warehousecode,
                           po.warehousename,
                           po.warehouseid,
                           wh.ProvinceName as province,
                           nvl(nvl(ps.code,pr.code),prb.code) as PartsSalesOrderCode,
                           (select min(a.code)
                              from PartsSalesOrderProcess a
                              join PartsSalesOrderProcessDetail b
                                on a.id = b.partssalesorderprocessid
                             where a.OriginalSalesOrderId = ps.id
                               and b.sparepartid = ck.sparepartid) as PartsSalesOrderProcessCode,
                           ps.partssalesordertypename,
                           po.code,
                           sp.referencecode,
                           ck.sparepartcode,
                           ck.sparepartname,
                           pb.partabc,
                           ck.inspectedquantity,
                           ck.settlementprice,
                           ck.settlementprice * ck.inspectedquantity as SettlementPriceSum,
                           ck.originalprice,
                           ck.originalprice * ck.inspectedquantity as originalpriceSum,
                           po.createtime,
                           po.remark as PartsInboundCheckBillRemark,
                           pi.groupname,
                           po.CounterpartCompanyName
                      from partsinboundcheckbilldetail ck
                      join partsinboundcheckbill po
                        on ck.partsinboundcheckbillid = po.id
                      join Company wh
                        on po.storagecompanyid = wh.id
                      left join partssalesorder ps
                        on po.originalrequirementbillid = ps.id
                       and po.originalrequirementbillcode = ps.code
                       and po.inboundtype = 1
                      left join partssalesreturnbill pr
                        on po.originalrequirementbillid = pr.id
                       and po.originalrequirementbillcode = pr.code
                       and po.inboundtype = 2
                      left join PartsRetailReturnBill prb
                        on po.originalrequirementbillid = prb.id
                       and po.originalrequirementbillcode = prb.code
                       and po.inboundtype = 5
                      join sparepart sp
                        on ck.sparepartid = sp.id
                      left join partsbranch pb
                        on ck.sparepartid = pb.partid
                       and pb.status = 1
                       and pb.partssalescategoryid = po.partssalescategoryid
                      left join PartsSalePriceIncreaseRate pi
                        on pb.IncreaseRateGroupId = pi.id
                     where po.StorageCompanyType = 3 ";
            if(bCreateTime.HasValue && bCreateTime.Value == Convert.ToDateTime("9999/12/31 23:59:59")) {
                bCreateTime = null;
            }
            if(eCreateTime.HasValue && eCreateTime.Value == Convert.ToDateTime("9999/12/31 23:59:59")) {
                eCreateTime = null;
            }
            if(!string.IsNullOrEmpty(code)) {
                SQL = SQL + " and LOWER(po.code) like '%" + code.ToLower() + "%'";
            }                      
            if(!string.IsNullOrEmpty(sparePartCode)) {
                SQL = SQL + " and LOWER(ck.sparePartCode) like '%" + sparePartCode.ToLower() + "%'";
            }
            if(!string.IsNullOrEmpty(partsSalesOrderCode)) {
                SQL = SQL + " and LOWER(ps.Code) like '%" + partsSalesOrderCode.ToLower() + "%'";
            }
            if(warehouseId.HasValue) {
                SQL = SQL + " and po.warehouseId=" + warehouseId.Value;
            }
            if(inboundType.HasValue) {
                SQL = SQL + " and po.InboundType = " + inboundType + "";
            }
            if(bCreateTime.HasValue) {
                SQL = SQL + " and po.CreateTime>= to_date('" + bCreateTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }
            if(eCreateTime.HasValue) {
                SQL = SQL + " and po.CreateTime<= to_date('" + eCreateTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }
            if(!string.IsNullOrEmpty(referenceCode)) {
                SQL = SQL + " and LOWER(sp.referenceCode) like '%" + referenceCode.ToLower() + "%'";
            }
            var userInfo = Utils.GetCurrentUserInfo();
            var company = ObjectContext.Companies.Where(r=>r.Id==userInfo.EnterpriseId).FirstOrDefault();
            if(company.Type == (int)DcsCompanyType.代理库) {
                SQL = SQL + " and po.StorageCompanyId= " + userInfo.EnterpriseId + "";
            }
            var search = ObjectContext.ExecuteStoreQuery<PartsInboundCheckBillForReport>(SQL).ToList();
            return search;
        }
        public IEnumerable<DealerPartsInboundBill> 服务站入库单统计(string code, string inboundType, string marketingDepartmentName, string storageCompanyName, string receivingCompanyName, DateTime? bConfirmedReceptionTime, DateTime? eConfirmedReceptionTime) {
            string SQL = @"select *
                              from (select po.code,
                                           '采购收货单' as InboundType,
                                           ps.ShippingAmount as Quantity,
                                           ps.SettlementPrice,
                                           pp.SalesPrice as CostPrice,
                                           pg.RetailGuidePrice as OriginalPrice,
                                           po.receivingcompanyid as dealerId,
                                           ps.id,
                                           pr.groupname,
                                           mt.Name as MarketingDepartmentName,
                                           pl.StorageCompanyName,
                                           pl.ReceivingCompanyCode,
                                           pl.receivingcompanyname,
                                           po.ConfirmedReceptionTime,ps.sparepartcode as PartsCode,ps.sparepartname as PartsName
                                      from PartsShippingOrder po
                                      join partsshippingorderdetail ps
                                        on po.id = ps.partsshippingorderid
                                      join partsoutboundplan pl
                                        on ps.partsoutboundplanid = pl.id
                                      left join PartsSalesPrice pp
                                        on ps.sparepartid = pp.sparepartid
                                       and po.branchid = pp.branchid
                                       and po.partssalescategoryid = pp.partssalescategoryid
                                       and pp.status = 1
                                      left join PartsRetailGuidePrice pg
                                        on pg.sparepartid = ps.sparepartid
                                       and pg.status = 1
                                       and pg.branchid = po.branchid
                                       and pg.partssalescategoryid = po.partssalescategoryid
                                      left join partsbranch pb
                                        on ps.sparepartid = pb.partid
                                       and pb.status = 1
                                       and pb.partssalescategoryid = po.partssalescategoryid
                                      left join PartsSalePriceIncreaseRate pr
                                        on pb.increaserategroupid = pr.id
                                      left join company cp
                                        on pl.ReceivingCompanyId = cp.id
                                      left join DealerServiceInfo ag
                                        on ag.DealerId = cp.id
                                      left join MarketingDepartment mt
                                        on mt.id = ag.marketingdepartmentid
                                     where po.status in(2,3)
                                       and (cp.type = 2 or cp.type = 7)
                                    union all
                                    select pc.code,
                                           '外采单' as InboundType,
                                           pl.Quantity as Quantity,
                                           pl.TradePrice as SettlementPrice,
                                           pp.SalesPrice as CostPrice,
                                           pl.OuterPurchasePrice as OriginalPrice,
                                           pc.CustomerCompanyId as dealerId,
                                           pl.id,
                                           pr.groupname,
                                           mt.Name as MarketingDepartmentName,
                                           ad.AgencyName as StorageCompanyName,
                                           pc.CustomerCompanyCode as ReceivingCompanyCode,
                                           pc.customercompanynace as receivingcompanyname,
                                           pc.ApproveTime as ConfirmedReceptionTime,pl.PartsCode,pl.PartsName
                                      from PartsOuterPurchaselist pl
                                      join PartsOuterPurchaseChange pc
                                        on pl.partsouterpurchasechangeid = pc.id
                                      left join PartsSalesPrice pp
                                        on pl.partsid = pp.sparepartid
                                       and pc.branchid = pp.branchid
                                       and pc.partssalescategoryrid = pp.partssalescategoryid
                                       and pp.status = 1
                                      left join partsbranch pb
                                        on pl.partsid = pb.partid
                                       and pb.status = 1
                                       and pb.partssalescategoryid = pc.partssalescategoryrid
                                      left join PartsSalePriceIncreaseRate pr
                                        on pb.increaserategroupid = pr.id
                                      left join DealerServiceInfo ag
                                        on ag.DealerId = pc.CustomerCompanyId
                                      left join MarketingDepartment mt
                                        on mt.id = ag.marketingdepartmentid
                                      join AgencyDealerRelation ad
                                        on pc.CustomerCompanyId = ad.dealerid
                                       and ad.status = 1
                                     where pc.status = 5
                                    union all
                                    select dr.code,
                                           '零售退货单' as InboundType,
                                           do.Quantity as Quantity,
                                           do.Price as SettlementPrice,
                                           pp.SalesPrice as CostPrice,
                                           do.Price as OriginalPrice,
                                           dr.DealerId as dealerId,
                                           do.id,
                                           pr.groupname,
                                           mt.Name as MarketingDepartmentName,
                                           ad.AgencyName as StorageCompanyName,
                                           dr.DealerCode as ReceivingCompanyCode,
                                           dr.DealerName as receivingcompanyname,
                                           dr.ApproveTime as ConfirmedReceptionTime,do.PartsCode,do.PartsName
                                      from DealerRetailReturnBillDetail do
                                      join DealerPartsSalesReturnBill dr
                                        on do.DealerPartsSalesReturnBillId = dr.id
                                      left join PartsSalesPrice pp
                                        on do.PartsId = pp.sparepartid
                                       and dr.branchid = pp.branchid
                                       and dr.status = 1
                                      left join partsbranch pb
                                        on pb.partid = do.PartsId
                                       and pb.status = 1
                                      left join PartsSalePriceIncreaseRate pr
                                        on pb.increaserategroupid = pr.id
                                      left join DealerServiceInfo ag
                                        on ag.DealerId = dr.DealerId
                                      left join MarketingDepartment mt
                                        on mt.id = ag.marketingdepartmentid
                                      join AgencyDealerRelation ad
                                        on dr.DealerId = ad.dealerid
                                       and ad.status = 1
                                     where dr.status = 2) tt
                             where 1 = 1  ";          
            if(!string.IsNullOrEmpty(code)) {
                SQL = SQL + " and LOWER(tt.code) like '%" + code.ToLower() + "%'";
            }
            if(!string.IsNullOrEmpty(inboundType)) {
                SQL = SQL + " and tt.InboundType like '%" + inboundType + "%'";
            }
            if(!string.IsNullOrEmpty(marketingDepartmentName)) {
                SQL = SQL + " and tt.marketingDepartmentName like '%" + marketingDepartmentName + "%'";
            }
            if(!string.IsNullOrEmpty(storageCompanyName)) {
                SQL = SQL + " and tt.storageCompanyName like '%" + storageCompanyName + "%'";
            }
            if(!string.IsNullOrEmpty(receivingCompanyName)) {
                SQL = SQL + " and tt.receivingCompanyName like '%" + receivingCompanyName + "%'";
            }
            var userInfo = Utils.GetCurrentUserInfo();
            var company = ObjectContext.Companies.Where(r => r.Id == userInfo.EnterpriseId).FirstOrDefault();
            if(company.Type == (int)DcsCompanyType.代理库 ) {
                SQL = SQL + " and exists (select 1 from AgencyDealerRelation ar where ar.AgencyId=" + userInfo.EnterpriseId + " and ar.DealerId=tt.dealerId and ar.status=1) ";
            } else if(company.Type == (int)DcsCompanyType.服务站) {
                SQL = SQL + " and tt.dealerId= " + userInfo.EnterpriseId;
            }
            if(bConfirmedReceptionTime.HasValue && bConfirmedReceptionTime.Value == Convert.ToDateTime("9999/12/31 23:59:59")) {
                bConfirmedReceptionTime = null;
            }
            if(eConfirmedReceptionTime.HasValue && eConfirmedReceptionTime.Value == Convert.ToDateTime("9999/12/31 23:59:59")) {
                eConfirmedReceptionTime = null;
            }
            if(bConfirmedReceptionTime.HasValue) {
                SQL = SQL + " and tt.ConfirmedReceptionTime>= to_date('" + bConfirmedReceptionTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }

            if(eConfirmedReceptionTime.HasValue) {
                SQL = SQL + " and tt.ConfirmedReceptionTime<= to_date('" + eConfirmedReceptionTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }
            var search = ObjectContext.ExecuteStoreQuery<DealerPartsInboundBill>(SQL).ToList();
            return search;
        }
    }
    partial class DcsDomainService {
        public IEnumerable<PartsInboundCheckBillForReport> 采购入库及供应商差价报表查询(DateTime? bCreateTime, DateTime? eCreateTime, DateTime? bPartsPurchaseOrderDate, DateTime? ePartsPurchaseOrderDate, string code, string partsPurchaseOrderCode, string sparePartCode, string supplierPartName, int? settleBillStatus, string settleBillCode) {
            return new PartsInboundCheckBillForReportAch(this).采购入库及供应商差价报表查询(bCreateTime, eCreateTime, bPartsPurchaseOrderDate, ePartsPurchaseOrderDate, code, partsPurchaseOrderCode, sparePartCode, supplierPartName, settleBillStatus, settleBillCode);
        }
        public IEnumerable<PartsInboundCheckBillForReport> 配件采购入库检验单统计(DateTime? bCreateTime, DateTime? eCreateTime, string code, string partsSupplierCode, string partsSupplierName, string sparePartCode, string supplierPartCode, string sparePartName, int? partABC, string settleBillStatus, string invoice, string referenceCode) {
            return new PartsInboundCheckBillForReportAch(this).配件采购入库检验单统计(bCreateTime, eCreateTime,  code,  partsSupplierCode,  partsSupplierName,  sparePartCode,  supplierPartCode,  sparePartName,  partABC,  settleBillStatus,  invoice,  referenceCode);
        }
        public IEnumerable<PartsInboundCheckBillForReport> 中心库入库单统计(DateTime? bCreateTime, DateTime? eCreateTime, string code, string sparePartCode, string referenceCode, int? warehouseId, int? inboundType, string partsSalesOrderCode) {
            return new PartsInboundCheckBillForReportAch(this).中心库入库单统计(bCreateTime, eCreateTime, code, sparePartCode, referenceCode, warehouseId, inboundType, partsSalesOrderCode);

        }
        public IEnumerable<DealerPartsInboundBill> 服务站入库单统计(string code, string inboundType, string marketingDepartmentName, string storageCompanyName, string receivingCompanyName, DateTime? bConfirmedReceptionTime, DateTime? eConfirmedReceptionTime) {
            return new PartsInboundCheckBillForReportAch(this).服务站入库单统计(code, inboundType, marketingDepartmentName, storageCompanyName, receivingCompanyName, bConfirmedReceptionTime, eConfirmedReceptionTime);
        }
    }
}
