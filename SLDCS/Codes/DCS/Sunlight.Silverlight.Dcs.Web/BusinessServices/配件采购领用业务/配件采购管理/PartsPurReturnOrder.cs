﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
using System.Collections.Generic;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsPurReturnOrderAch : DcsSerivceAchieveBase {
        public PartsPurReturnOrderAch(DcsDomainService domainService)
            : base(domainService) {
        }
        //验证退货数量是否小于等于检验数量-已退货数量
        private void 验证退货数量(PartsPurReturnOrder partsPurReturnOrder,string operation) {
            var inboundCheckBillDetails = new PartsInboundCheckBillDetailAch(this.DomainService).按原始需求单据查询入库明细((int)partsPurReturnOrder.PartsPurchaseOrderId, (int)DcsOriginalRequirementBillType.配件采购订单);
            foreach(var item in partsPurReturnOrder.PartsPurReturnOrderDetails){              
                var returnOrder =( from a in ObjectContext.PartsPurReturnOrderDetails.Where(r=>r.SparePartId==item.SparePartId)
                                   join b in ObjectContext.PartsPurReturnOrders.Where(r => r.Status != 99) on a.PartsPurReturnOrderId equals b.Id
                                   join c in ObjectContext.PartsPurchaseOrders.Where(r => r.Code == partsPurReturnOrder.PartsPurchaseOrderCode) on b.PartsPurchaseOrderId equals c.Id 
                                    select a   
                                       ).ToArray();
                var returnQuantity = 0;
                if(returnOrder.Count()>0) {
                    if ("add".Equals(operation)) {
                        foreach (var detail in returnOrder) {
                            returnQuantity += detail.Quantity;
                        }
                    } else {
                        foreach (var detail in returnOrder) {
                            if(item.Id != detail.Id){
                                returnQuantity += detail.Quantity;
                            }
                        }
                    }
                }
                var allInspectedQuantity = inboundCheckBillDetails.Where(x => x.SparePartId == item.SparePartId).Sum(y => y.InspectedQuantity);
                if(allInspectedQuantity <(item.Quantity + returnQuantity)) {
                    throw new ValidationException(string.Format(ErrorStrings.PartsPurReturnOrder_Validation9,item.SparePartCode));
                }
            }
        }

        public IEnumerable<VirtualForPurchaseReturn> 查询已退货数量(string PartsPurchaseOrderCode,int[] partIds,int partsPurReturnOrderId,int supplierId) {
            int id = 0;
            List<VirtualForPurchaseReturn> datas = new List<VirtualForPurchaseReturn>();
            var returnOrder =( from a in ObjectContext.PartsPurReturnOrderDetails.Where(r => partIds.Contains(r.SparePartId))
                                join b in ObjectContext.PartsPurReturnOrders.Where(r => r.Status != 99 && r.Id != partsPurReturnOrderId) on a.PartsPurReturnOrderId equals b.Id
                                join c in ObjectContext.PartsPurchaseOrders.Where(r => r.Code == PartsPurchaseOrderCode) on b.PartsPurchaseOrderId equals c.Id 
                                select a   
                                    ).ToArray();
            foreach (var partId in partIds) {
                var entity = new VirtualForPurchaseReturn {
                    Id = id++,
                    SparePartId = partId,
                    ReturnedQuantity = 0
                };
                datas.Add(entity);
            }
            foreach (var item in returnOrder) {
                var data = datas.FirstOrDefault(r => r.SparePartId == item.SparePartId);
                data.ReturnedQuantity +=  item.Quantity;
            }
            var result = datas.ToList();
            //查询供应商图号
            var partsSupplierRelations = ObjectContext.PartsSupplierRelations.Where(r => r.Status != 99 && partIds.Contains(r.PartId) && supplierId == r.SupplierId).ToArray();
            foreach (var data in datas) {
                var relation = partsSupplierRelations.FirstOrDefault(r => r.PartId == data.SparePartId);
                if (relation != null)
                    data.SupplierPartCode = relation.SupplierPartCode;
            }
            return result;
        }

        public void 生成配件采购退货单(PartsPurReturnOrder partsPurReturnOrder) {
            if(partsPurReturnOrder.OutStatus != (int)DcsReturnOutStatus.未出库)
                throw new ValidationException(ErrorStrings.PartsPurReturnOrder_Validation10);
            this.验证退货数量(partsPurReturnOrder,"add");
            var partsPurReturnOrderDetails = partsPurReturnOrder.PartsPurReturnOrderDetails;
            var sparePartIds = partsPurReturnOrderDetails.Select(r => r.SparePartId);
            var partsSupplierRelations = ObjectContext.PartsSupplierRelations.Where(r => r.SupplierId == partsPurReturnOrder.PartsSupplierId && sparePartIds.Contains(r.PartId)).ToArray();
            var spareParts = ObjectContext.SpareParts.Where(r => sparePartIds.Contains(r.Id) && r.Status == (int)DcsMasterDataStatus.有效).ToArray();
            if(partsPurReturnOrder.ReturnReason == (int)DcsPartsPurReturnOrderReturnReason.质量件退货) {
                //var errorSparePartCode = (from a in partsPurReturnOrderDetails
                //                          join b in ObjectContext.PartsPurchaseOrders on a.PartsPurReturnOrderId equals b.Id
                //                          join c in ObjectContext.PartsPurchaseOrderDetails on new {
                //                              PartsPurchaseOrderId = b.Id,
                //                              a.SparePartId
                //                          }equals new {
                //                              c.PartsPurchaseOrderId,
                //                              c.SparePartId
                //                          }
                //                          where a.Quantity > c.ConfirmedAmount
                //                          select a.SparePartCode
                //           );
                System.Collections.Generic.List<string> errorSparePartCode = new System.Collections.Generic.List<string>();
                foreach(PartsPurReturnOrderDetail partsPurReturnOrderDetail in partsPurReturnOrderDetails){
                    var errorSparePart=from a in ObjectContext.PartsPurchaseOrderDetails where a.PartsPurchaseOrderId==partsPurReturnOrder.PartsPurchaseOrderId && a.SparePartId==partsPurReturnOrderDetail.SparePartId && a.ConfirmedAmount<partsPurReturnOrderDetail.Quantity select a.SparePartCode;
                    if (errorSparePart.Any())
                        errorSparePartCode.Add(errorSparePart.First());
                }
                if(errorSparePartCode.Any()) {
                    throw new ValidationException(string.Format(ErrorStrings.PartsPurReturnOrder_Validation11, string.Join(",", errorSparePartCode.Take(10))));
                }
            }
            foreach(var partsPurReturnOrderDetail in partsPurReturnOrderDetails) {
                //if (partsPurReturnOrder.ReturnReason != (int)DcsPartsPurReturnOrderReturnReason.积压件退货)
                //{
                    var partsSupplierRelation = partsSupplierRelations.FirstOrDefault(r => r.PartId == partsPurReturnOrderDetail.SparePartId);
                    if (partsSupplierRelation == null)
                        throw new ValidationException(string.Format(ErrorStrings.PartsPurReturnOrder_Validation6, partsPurReturnOrderDetail.SparePartCode, partsPurReturnOrder.PartsSupplierCode));
                    partsPurReturnOrderDetail.SupplierPartCode = partsSupplierRelation.SupplierPartCode;
               // }
                var sparePart = spareParts.SingleOrDefault(r => r.Id == partsPurReturnOrderDetail.SparePartId);
                if(sparePart == null)
                    throw new ValidationException(string.Format(ErrorStrings.PartsPurReturnOrder_Validation7, partsPurReturnOrderDetail.SparePartCode));
                partsPurReturnOrderDetail.MeasureUnit = sparePart.MeasureUnit;
            }
            InsertToDatabase(partsPurReturnOrder);
            this.InsertPartsPurReturnOrderValidate(partsPurReturnOrder);
        }


        public void 修改配件采购退货单(PartsPurReturnOrder partsPurReturnOrder)
        {
            this.验证退货数量(partsPurReturnOrder,"update");
            var dbpartsPurReturnOrder = ObjectContext.PartsPurReturnOrders.Where(r => r.Id == partsPurReturnOrder.Id && r.Status == (int)DcsPartsPurReturnOrderStatus.新建).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if (dbpartsPurReturnOrder == null)
                throw new ValidationException(ErrorStrings.PartsPurReturnOrder_Validation12);
            var partsPurReturnOrderDetails = partsPurReturnOrder.PartsPurReturnOrderDetails;
            var sparePartIds = partsPurReturnOrderDetails.Select(r => r.SparePartId);
            var partsSupplierRelations = ObjectContext.PartsSupplierRelations.Where(r => r.SupplierId == partsPurReturnOrder.PartsSupplierId && sparePartIds.Contains(r.PartId)).ToArray();
            var spareParts = ObjectContext.SpareParts.Where(r => sparePartIds.Contains(r.Id) && r.Status == (int)DcsMasterDataStatus.有效).ToArray();
            foreach(var partsPurReturnOrderDetail in partsPurReturnOrderDetails) {
                //if(partsPurReturnOrder.ReturnReason != (int)DcsPartsPurReturnOrderReturnReason.积压件退货) {
                    var partsSupplierRelation = partsSupplierRelations.FirstOrDefault(r => r.PartId == partsPurReturnOrderDetail.SparePartId);
                    if(partsSupplierRelation == null)
                        throw new ValidationException(string.Format(ErrorStrings.PartsPurReturnOrder_Validation6, partsPurReturnOrderDetail.SparePartCode, partsPurReturnOrder.PartsSupplierCode));
                    partsPurReturnOrderDetail.SupplierPartCode = partsSupplierRelation.SupplierPartCode;
               // }
                var sparePart = spareParts.FirstOrDefault(r => r.Id == partsPurReturnOrderDetail.SparePartId);
                if(sparePart == null)
                    throw new ValidationException(string.Format(ErrorStrings.PartsPurReturnOrder_Validation7, partsPurReturnOrderDetail.SparePartCode));
                partsPurReturnOrderDetail.MeasureUnit = sparePart.MeasureUnit;
            }
            UpdateToDatabase(partsPurReturnOrder);
            this.UpdatePartsPurReturnOrderValidate(partsPurReturnOrder);
        }

        public void 审批配件采购退货单(PartsPurReturnOrder partsPurReturnOrder) {
            var dbpartsPurReturnOrder = ObjectContext.PartsPurReturnOrders.Where(r => r.Id == partsPurReturnOrder.Id && r.Status == (int)DcsPartsPurReturnOrderStatus.初审通过).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbpartsPurReturnOrder == null)
                throw new ValidationException(ErrorStrings.PartsPurReturnOrder_Validation13);
            var warehouse = ObjectContext.Warehouses.SingleOrDefault(r => r.Id == partsPurReturnOrder.WarehouseId && r.Status == (int)DcsBaseDataStatus.有效);
            if(warehouse == null)
                throw new ValidationException(ErrorStrings.PartsPurReturnOrder_Validation4);
            var supplierAccount = ObjectContext.SupplierAccounts.FirstOrDefault(r => r.BuyerCompanyId == partsPurReturnOrder.BranchId && r.SupplierCompanyId == partsPurReturnOrder.PartsSupplierId && r.Status == (int)DcsMasterDataStatus.有效);
            if(supplierAccount == null)
                throw new ValidationException(ErrorStrings.PartsPurReturnOrder_Validation5);
            UpdateToDatabase(partsPurReturnOrder);
            var userInfo = Utils.GetCurrentUserInfo();
            partsPurReturnOrder.ApproverId = userInfo.Id;
            partsPurReturnOrder.ApproverName = userInfo.Name;
            partsPurReturnOrder.ApproveTime = DateTime.Now;
            if (partsPurReturnOrder.Status == 66)
            {
                partsPurReturnOrder.Status = (int)DcsPartsPurReturnOrderStatus.终审通过;
                var partsPurReturnOrderDetails = partsPurReturnOrder.PartsPurReturnOrderDetails;
                var partsOutboundPlan = new PartsOutboundPlan
                {
                    WarehouseId = partsPurReturnOrder.WarehouseId,
                    WarehouseCode = warehouse.Code,
                    WarehouseName = partsPurReturnOrder.WarehouseName,
                    StorageCompanyId = partsPurReturnOrder.BranchId,
                    StorageCompanyCode = partsPurReturnOrder.BranchCode,
                    StorageCompanyName = partsPurReturnOrder.BranchName,
                    StorageCompanyType = (int)DcsCompanyType.分公司,
                    BranchId = partsPurReturnOrder.BranchId,
                    BranchCode = partsPurReturnOrder.BranchCode,
                    BranchName = partsPurReturnOrder.BranchName,
                    CounterpartCompanyId = partsPurReturnOrder.PartsSupplierId,
                    CounterpartCompanyCode = partsPurReturnOrder.PartsSupplierCode,
                    CounterpartCompanyName = partsPurReturnOrder.PartsSupplierName,
                    ReceivingCompanyId = partsPurReturnOrder.PartsSupplierId,
                    ReceivingCompanyCode = partsPurReturnOrder.PartsSupplierCode,
                    ReceivingCompanyName = partsPurReturnOrder.PartsSupplierName,
                    SourceId = partsPurReturnOrder.Id,
                    SourceCode = partsPurReturnOrder.Code,
                    OutboundType = (int)DcsPartsOutboundType.采购退货,
                    CustomerAccountId = supplierAccount.Id,
                    OriginalRequirementBillId = partsPurReturnOrder.Id,
                    OriginalRequirementBillCode = partsPurReturnOrder.Code,
                    OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.配件采购退货单,
                    Status = (int)DcsPartsOutboundPlanStatus.新建,
                    Remark = partsPurReturnOrder.Remark,
                    PartsSalesCategoryId = partsPurReturnOrder.PartsSalesCategoryId,
                    IfWmsInterface = warehouse.WmsInterface
                };
                foreach (var partsPurReturnOrderDetail in partsPurReturnOrderDetails)
                {
                    var PartsOutboundPlanDetail = partsOutboundPlan.PartsOutboundPlanDetails.FirstOrDefault(r => r.SparePartId == partsPurReturnOrderDetail.SparePartId && r.Price == partsPurReturnOrderDetail.UnitPrice);
                    if (PartsOutboundPlanDetail == null) {
                        partsOutboundPlan.PartsOutboundPlanDetails.Add(new PartsOutboundPlanDetail {
                            SparePartId = partsPurReturnOrderDetail.SparePartId,
                            SparePartCode = partsPurReturnOrderDetail.SparePartCode,
                            SparePartName = partsPurReturnOrderDetail.SparePartName,
                            PlannedAmount = partsPurReturnOrderDetail.Quantity,
                            Price = partsPurReturnOrderDetail.UnitPrice,
                            Remark = partsPurReturnOrderDetail.Remark
                        });
                    } else {
                        PartsOutboundPlanDetail.PlannedAmount += partsPurReturnOrderDetail.Quantity;
                    }
                }
                DomainService.生成配件出库计划只校验保管区库存(partsOutboundPlan);
            }
            else if (partsPurReturnOrder.Status == 77)
            {
                partsPurReturnOrder.ModifierId = userInfo.Id;
                partsPurReturnOrder.ModifierName = userInfo.Name;
                partsPurReturnOrder.ModifyTime = DateTime.Now;
                partsPurReturnOrder.Status = (int)DcsPartsPurReturnOrderStatus.新建;
            }
            this.UpdatePartsPurReturnOrderValidate(partsPurReturnOrder);
        }

        public void 初审配件采购退货单(PartsPurReturnOrder partsPurReturnOrder)
        {
            var dbpartsPurReturnOrder = ObjectContext.PartsPurReturnOrders.Where(r => r.Id == partsPurReturnOrder.Id && r.Status == (int)DcsPartsPurReturnOrderStatus.新建).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if (dbpartsPurReturnOrder == null)
                throw new ValidationException(ErrorStrings.PartsPurReturnOrder_Validation14);
            UpdateToDatabase(partsPurReturnOrder);
            var userInfo = Utils.GetCurrentUserInfo();
            partsPurReturnOrder.ModifierId = userInfo.Id;
            partsPurReturnOrder.ModifierName = userInfo.Name;
            partsPurReturnOrder.ModifyTime = DateTime.Now;
            partsPurReturnOrder.InitialApproverId = userInfo.Id;
            partsPurReturnOrder.InitialApproverName = userInfo.Name;
            partsPurReturnOrder.InitialApproveTime = DateTime.Now;
            if (partsPurReturnOrder.Status==66)
            {
                partsPurReturnOrder.Status = (int)DcsPartsPurReturnOrderStatus.初审通过;
            }
            else if (partsPurReturnOrder.Status == 77)
            {
                partsPurReturnOrder.Status = (int)DcsPartsPurReturnOrderStatus.新建;
            }
           
            this.UpdatePartsPurReturnOrderValidate(partsPurReturnOrder);
        }

        public void 驳回配件采购退货单(PartsPurReturnOrder partsPurReturnOrder) {
            var dbpartsPurReturnOrder = ObjectContext.PartsPurReturnOrders.Where(r => r.Id == partsPurReturnOrder.Id && (r.Status == (int)DcsPartsPurReturnOrderStatus.新建 || r.Status == (int)DcsPartsPurReturnOrderStatus.初审通过)).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if (dbpartsPurReturnOrder == null)
                throw new ValidationException(ErrorStrings.PartsPurReturnOrder_Validation15);
            UpdateToDatabase(partsPurReturnOrder);
            var userInfo = Utils.GetCurrentUserInfo();
            partsPurReturnOrder.ModifierId = userInfo.Id;
            partsPurReturnOrder.ModifierName = userInfo.Name;
            partsPurReturnOrder.ModifyTime = DateTime.Now;

            if (partsPurReturnOrder.Status == (int)DcsPartsPurReturnOrderStatus.新建) {
                partsPurReturnOrder.InitialApproverId = userInfo.Id;
                partsPurReturnOrder.InitialApproverName = userInfo.Name;
                partsPurReturnOrder.InitialApproveTime = DateTime.Now;
            } else if (partsPurReturnOrder.Status == (int)DcsPartsPurReturnOrderStatus.初审通过) {
                 partsPurReturnOrder.ApproverId = userInfo.Id;
                 partsPurReturnOrder.ApproverName = userInfo.Name;
                 partsPurReturnOrder.ApproveTime = DateTime.Now;
            }
            partsPurReturnOrder.Status = (int)DcsPartsPurReturnOrderStatus.新建;
            this.UpdatePartsPurReturnOrderValidate(partsPurReturnOrder);
        }

        public void 作废配件采购退货单(PartsPurReturnOrder partsPurReturnOrder) {
            var dbpartsPurReturnOrder = ObjectContext.PartsPurReturnOrders.Where(r => r.Id == partsPurReturnOrder.Id && r.Status == (int)DcsPartsPurReturnOrderStatus.新建).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbpartsPurReturnOrder == null)
                throw new ValidationException(ErrorStrings.PartsPurReturnOrder_Validation3);
            UpdateToDatabase(partsPurReturnOrder);
            var userInfo = Utils.GetCurrentUserInfo();
            partsPurReturnOrder.ModifierId = userInfo.Id;
            partsPurReturnOrder.ModifierName = userInfo.Name;
            partsPurReturnOrder.ModifyTime = DateTime.Now;
            partsPurReturnOrder.Status = (int)DcsPartsPurReturnOrderStatus.作废;
            partsPurReturnOrder.AbandonerId = userInfo.Id;
            partsPurReturnOrder.AbandonerName = userInfo.Name;
            partsPurReturnOrder.AbandonTime = DateTime.Now;
            this.UpdatePartsPurReturnOrderValidate(partsPurReturnOrder);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:11:30
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 生成配件采购退货单(PartsPurReturnOrder partsPurReturnOrder) {
            new PartsPurReturnOrderAch(this).生成配件采购退货单(partsPurReturnOrder);
        }
        
        [Update(UsingCustomMethod = true)]
        public void 修改配件采购退货单(PartsPurReturnOrder partsPurReturnOrder) {
            new PartsPurReturnOrderAch(this).修改配件采购退货单(partsPurReturnOrder);
        }
        
        [Update(UsingCustomMethod = true)]
        public void 审批配件采购退货单(PartsPurReturnOrder partsPurReturnOrder) {
            new PartsPurReturnOrderAch(this).审批配件采购退货单(partsPurReturnOrder);
        }

        [Update(UsingCustomMethod = true)]
        public void 初审配件采购退货单(PartsPurReturnOrder partsPurReturnOrder)
        {
            new PartsPurReturnOrderAch(this).初审配件采购退货单(partsPurReturnOrder);
        }
        
        [Update(UsingCustomMethod = true)]
        public void 作废配件采购退货单(PartsPurReturnOrder partsPurReturnOrder) {
            new PartsPurReturnOrderAch(this).作废配件采购退货单(partsPurReturnOrder);
        }

        public IEnumerable<VirtualForPurchaseReturn> 查询已退货数量(string PartsPurchaseOrderCode, int[] partIds,int partsPurReturnOrderId,int supplierId) {
            return new PartsPurReturnOrderAch(this).查询已退货数量(PartsPurchaseOrderCode,partIds,partsPurReturnOrderId,supplierId);
        }
        [Update(UsingCustomMethod = true)]
        public void 驳回配件采购退货单(PartsPurReturnOrder partsPurReturnOrder) {
            new PartsPurReturnOrderAch(this).驳回配件采购退货单(partsPurReturnOrder);
        }
    }
}
