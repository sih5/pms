﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using System.Text;
using FTService;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;



namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsPurchaseOrderAch : DcsSerivceAchieveBase {
        public PartsPurchaseOrderAch(DcsDomainService domainService)
            : base(domainService) {
        }

        public void 生成配件采购订单(PartsPurchaseOrder partsPurchaseOrder) {
            var salesUnitAffiWarehouseID = from salesUnitAffiWarehouse in ObjectContext.SalesUnitAffiWarehouses
                                           join salesUnit in ObjectContext.SalesUnits on salesUnitAffiWarehouse.SalesUnitId equals salesUnit.Id
                                           where salesUnitAffiWarehouse.WarehouseId == partsPurchaseOrder.WarehouseId && salesUnit.PartsSalesCategoryId == partsPurchaseOrder.PartsSalesCategoryId
                                           select salesUnitAffiWarehouse;
            if(!salesUnitAffiWarehouseID.Any() && !partsPurchaseOrder.IfDirectProvision) {
                throw new ValidationException(ErrorStrings.PartsPurchaseOrder_Validation10);
            }
            var dbPartsPurchaseOrderDetails = partsPurchaseOrder.PartsPurchaseOrderDetails;
            var sparePartIds = dbPartsPurchaseOrderDetails.Select(v => v.SparePartId);
            //            2.如果 配件采购订单.分公司编号=2450 且 配件采购订单.供应商编号 =FT010063
            //进行以下校验：查询配件信息（配件信息.id in 配件采购清单.配件id数组）
            //如果 存在多条配件信息 其配件信息.配件参考编号 相同 提示：配件图号 配件A ,配件B，... 对应的零部件图号 xx 相同
            if(partsPurchaseOrder.BranchCode == "2450" && partsPurchaseOrder.PartsSupplierCode == "FT010063") {
                var dbSparePart = ObjectContext.SpareParts.Where(r => sparePartIds.Contains(r.Id) && r.Status == (int)DcsBaseDataStatus.有效);
                var groups = dbSparePart.GroupBy(r =>
                       r.ReferenceCode
                   ).Where(r => r.Count() > 1);
                if(groups.Any()) {
                    var checkReferenceCode = groups.Select(r => r.Key).ToArray();
                    foreach(var referenceCode in checkReferenceCode) {
                        StringBuilder a = new StringBuilder();
                        foreach(var groupItem in groups.SelectMany(r => r).Where(r => r.ReferenceCode == referenceCode)) {
                            a.Append(groupItem.Code);
                            a.Append(',');
                        }
                        if(a.Length > 0)
                            a.Remove(a.Length - 1, 1);
                        throw new ValidationException(string.Format(ErrorStrings.PartsPurchaseOrder_Validation11, a, referenceCode));
                    }
                }
            }
            var dbPartsBrancheIds = ObjectContext.PartsBranches.Where(r => sparePartIds.Contains(r.PartId) && r.IsOrderable == true && r.BranchId == partsPurchaseOrder.BranchId && r.PartsSalesCategoryId == partsPurchaseOrder.PartsSalesCategoryId && r.Status == (int)DcsBaseDataStatus.有效).Select(v => v.PartId).ToArray();
            var partCodes = dbPartsPurchaseOrderDetails.Where(e => !dbPartsBrancheIds.Contains(e.SparePartId)).Select(v => v.SparePartCode).ToArray();
            if(partCodes.Length > 0)
                throw new ValidationException(string.Format(ErrorStrings.PartsPurchaseOrder_Validation1, String.Join("、", partCodes)));
            //更新价格类型
            foreach(var detail in dbPartsPurchaseOrderDetails) {
                var referencepriceRecord = ObjectContext.PartsPurchasePricings.Where(entitiy => entitiy.Status == 2 && entitiy.PartId == detail.SparePartId).OrderBy(entitiy => entitiy.CreateTime).FirstOrDefault();
                if(referencepriceRecord == null || referencepriceRecord.PurchasePrice == 0)
                    continue;
                detail.PurchasePriceBefore = referencepriceRecord.PurchasePrice;
                if(!detail.PriceType.HasValue)
                    detail.PriceType = referencepriceRecord.PriceType;//价格类型
            }
            //更新清单变更前价格
            foreach(var detail in dbPartsPurchaseOrderDetails) {
                var referencepriceRecord = new PartsPurchaseOrderDetailAch(this.DomainService).GetPartsPurchaseOrderDetailsByExecuteSql(partsPurchaseOrder.PartsSalesCategoryId, partsPurchaseOrder.PartsSupplierId, detail.SparePartId, DateTime.Now);
                if(referencepriceRecord == null || referencepriceRecord.ReferencePrice == 0)
                    continue;
                detail.PurchasePriceBefore = referencepriceRecord.ReferencePrice;
                detail.PriceType = referencepriceRecord.PriceType;//价格类型
            }
            //因为服务端需要调用
            InsertToDatabase(partsPurchaseOrder);
            this.InsertPartsPurchaseOrderValidate(partsPurchaseOrder);
        }

        public void 生成配件采购订单1(PartsPurchaseOrder partsPurchaseOrder) {
            var salesUnitAffiWarehouseID = from salesUnitAffiWarehouse in ObjectContext.SalesUnitAffiWarehouses
                                           join salesUnit in ObjectContext.SalesUnits on salesUnitAffiWarehouse.SalesUnitId equals salesUnit.Id
                                           where salesUnitAffiWarehouse.WarehouseId == partsPurchaseOrder.WarehouseId && salesUnit.PartsSalesCategoryId == partsPurchaseOrder.PartsSalesCategoryId
                                           select salesUnitAffiWarehouse;
            if(!salesUnitAffiWarehouseID.Any() && !partsPurchaseOrder.IfDirectProvision) {
                throw new ValidationException(ErrorStrings.PartsPurchaseOrder_Validation10);
            }
            var dbPartsPurchaseOrderDetails = partsPurchaseOrder.PartsPurchaseOrderDetails;
            var sparePartIds = dbPartsPurchaseOrderDetails.Select(v => v.SparePartId);
            //            2.如果 配件采购订单.分公司编号=2450 且 配件采购订单.供应商编号 =FT010063
            //进行以下校验：查询配件信息（配件信息.id in 配件采购清单.配件id数组）
            //如果 存在多条配件信息 其配件信息.配件参考编号 相同 提示：配件图号 配件A ,配件B，... 对应的零部件图号 xx 相同
            if(partsPurchaseOrder.BranchCode == "2450" && partsPurchaseOrder.PartsSupplierCode == "FT010063") {
                var dbSparePart = ObjectContext.SpareParts.Where(r => sparePartIds.Contains(r.Id) && r.Status == (int)DcsBaseDataStatus.有效);
                var groups = dbSparePart.GroupBy(r =>
                       r.ReferenceCode
                   ).Where(r => r.Count() > 1);
                if(groups.Any()) {
                    var checkReferenceCode = groups.Select(r => r.Key).ToArray();
                    foreach(var referenceCode in checkReferenceCode) {
                        StringBuilder a = new StringBuilder();
                        foreach(var groupItem in groups.SelectMany(r => r).Where(r => r.ReferenceCode == referenceCode)) {
                            a.Append(groupItem.Code);
                            a.Append(',');
                        }
                        if(a.Length > 0)
                            a.Remove(a.Length - 1, 1);
                        throw new ValidationException(string.Format(ErrorStrings.PartsPurchaseOrder_Validation11, a, referenceCode));
                    }
                }
            }
            var dbPartsBrancheIds = ObjectContext.PartsBranches.Where(r => sparePartIds.Contains(r.PartId) && r.IsOrderable == true && r.BranchId == partsPurchaseOrder.BranchId && r.PartsSalesCategoryId == partsPurchaseOrder.PartsSalesCategoryId && r.Status == (int)DcsBaseDataStatus.有效).Select(v => v.PartId).ToArray();
            var partCodes = dbPartsPurchaseOrderDetails.Where(e => !dbPartsBrancheIds.Contains(e.SparePartId)).Select(v => v.SparePartCode).ToArray();
            if(partCodes.Length > 0)
                throw new ValidationException(string.Format(ErrorStrings.PartsPurchaseOrder_Validation1, String.Join("、", partCodes)));

            var partsPurchasePlan = ObjectContext.PartsPurchasePlan_HW.Where(i => i.Code == partsPurchaseOrder.PurOrderCode).FirstOrDefault();
            if(partsPurchasePlan == null)
                throw new ValidationException(string.Format(ErrorStrings.PartsPurchaseOrder_Validation12, partsPurchaseOrder.PurOrderCode));
            var partsPurchasePlanDetails = ObjectContext.PartsPurchasePlanDetail_HW.Where(r => r.PartsPurchasePlanId == partsPurchasePlan.Id).ToArray();
            if(!partsPurchasePlanDetails.Any())
                throw new ValidationException(string.Format(ErrorStrings.PartsPurchaseOrder_Validation13, partsPurchaseOrder.PurOrderCode));
            //更新价格类型
            foreach(var detail in dbPartsPurchaseOrderDetails) {
                var referencepriceRecord = ObjectContext.PartsPurchasePricings.Where(entitiy => entitiy.Status == 2 && entitiy.PartId == detail.SparePartId).OrderBy(entitiy => entitiy.CreateTime).FirstOrDefault();
                if(referencepriceRecord == null || referencepriceRecord.PurchasePrice == 0)
                    continue;
                detail.PurchasePriceBefore = referencepriceRecord.PurchasePrice;
                detail.PriceType = referencepriceRecord.PriceType;//价格类型
            }
            //更新清单变更前价格
            foreach(var detail in dbPartsPurchaseOrderDetails) {
                var partsPurchasePlanDetail_HW = partsPurchasePlanDetails.FirstOrDefault(i => i.PartId == detail.SparePartId);
                if(partsPurchasePlanDetail_HW == null)
                    throw new ValidationException(string.Format(ErrorStrings.PartsPurchaseOrder_Validation14, detail.SparePartCode));
                if(detail.OrderAmount > (partsPurchasePlanDetail_HW.UntFulfilledQty.Value - (partsPurchasePlanDetail_HW.EndAmount ?? 0)))
                    throw new ValidationException(string.Format(ErrorStrings.PartsPurchaseOrder_Validation15, detail.SparePartCode));
                partsPurchasePlanDetail_HW.UntFulfilledQty -= detail.OrderAmount;
                var referencepriceRecord = new PartsPurchaseOrderDetailAch(this.DomainService).GetPartsPurchaseOrderDetailsByExecuteSql(partsPurchaseOrder.PartsSalesCategoryId, partsPurchaseOrder.PartsSupplierId, detail.SparePartId, DateTime.Now);
                if(referencepriceRecord == null || referencepriceRecord.ReferencePrice == 0)
                    continue;
                detail.PurchasePriceBefore = referencepriceRecord.ReferencePrice;
                detail.PriceType = referencepriceRecord.PriceType;//价格类型
            }
            var partsPurchasePlan_HW = ObjectContext.PartsPurchasePlan_HW.Where(r => r.Code == partsPurchaseOrder.PurOrderCode).FirstOrDefault();
            if(partsPurchasePlan_HW != null)
                new PartsPurchasePlan_HWAch(this.DomainService).UpdatePartsPurchasePlan_HW(partsPurchasePlan_HW);
            ObjectContext.AddToPartsPurchaseOrders(partsPurchaseOrder);
            //因为服务端需要调用
            InsertToDatabase(partsPurchaseOrder);
            this.InsertPartsPurchaseOrderValidate(partsPurchaseOrder);
        }


        /// <summary>
        /// 仅供客户端使用
        /// </summary>
        public void 修改配件采购订单(PartsPurchaseOrder partsPurchaseOrder) {
            CheckEntityState(partsPurchaseOrder);
            var salesUnitAffiWarehouseID = from salesUnitAffiWarehouse in ObjectContext.SalesUnitAffiWarehouses
                                           join salesUnit in ObjectContext.SalesUnits on salesUnitAffiWarehouse.SalesUnitId equals salesUnit.Id
                                           where salesUnitAffiWarehouse.WarehouseId == partsPurchaseOrder.WarehouseId && salesUnit.PartsSalesCategoryId == partsPurchaseOrder.PartsSalesCategoryId
                                           select salesUnitAffiWarehouse;
            if(!salesUnitAffiWarehouseID.Any() && !partsPurchaseOrder.IfDirectProvision) {
                throw new ValidationException(ErrorStrings.PartsPurchaseOrder_Validation10);
            }
            var dbpartsPurchaseOrder = ObjectContext.PartsPurchaseOrders.Where(r => r.Id == partsPurchaseOrder.Id && (r.Status == (int)DcsPartsPurchaseOrderStatus.新增 || r.Status == (int)DcsPartsPurchaseOrderStatus.提交)).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbpartsPurchaseOrder == null)
                throw new ValidationException(ErrorStrings.PartsPurchaseOrder_Validation2);
            var dbPartsPurchaseOrderDetails = partsPurchaseOrder.PartsPurchaseOrderDetails;
            var sparePartIds = dbPartsPurchaseOrderDetails.Select(v => v.SparePartId);
            if(partsPurchaseOrder.BranchCode == "2450" && partsPurchaseOrder.PartsSupplierCode == "FT010063") {
                var dbSparePart = ObjectContext.SpareParts.Where(r => sparePartIds.Contains(r.Id) && r.Status == (int)DcsBaseDataStatus.有效);
                var groups = dbSparePart.GroupBy(r =>
                       r.ReferenceCode
                   ).Where(r => r.Count() > 1);
                if(groups.Any()) {
                    var checkReferenceCode = groups.Select(r => r.Key).ToArray();
                    foreach(var referenceCode in checkReferenceCode) {
                        StringBuilder a = new StringBuilder();
                        foreach(var groupItem in groups.SelectMany(r => r).Where(r => r.ReferenceCode == referenceCode)) {
                            a.Append(groupItem.Code);
                            a.Append(',');
                        }
                        if(a.Length > 0)
                            a.Remove(a.Length - 1, 1);
                        throw new ValidationException(string.Format(ErrorStrings.PartsPurchaseOrder_Validation11, a, referenceCode));
                    }
                }
            }
            var dbPartsBrancheIds = ObjectContext.PartsBranches.Where(r => sparePartIds.Contains(r.PartId) && r.IsOrderable == true && r.BranchId == partsPurchaseOrder.BranchId && r.PartsSalesCategoryId == partsPurchaseOrder.PartsSalesCategoryId && r.Status == (int)DcsBaseDataStatus.有效).Select(v => v.PartId).ToArray();
            var partCodes = dbPartsPurchaseOrderDetails.Where(e => !dbPartsBrancheIds.Contains(e.SparePartId)).Select(v => v.SparePartCode).ToArray();
            if(partCodes.Length > 0)
                throw new ValidationException(string.Format(ErrorStrings.PartsPurchaseOrder_Validation1, String.Join("、", partCodes)));
            //更新价格类型
            foreach(var detail in dbPartsPurchaseOrderDetails) {
                var referencepriceRecord = ObjectContext.PartsPurchasePricings.Where(entitiy => entitiy.Status == 2 && entitiy.PartId == detail.SparePartId).OrderBy(entitiy => entitiy.CreateTime).FirstOrDefault();
                if(referencepriceRecord == null || referencepriceRecord.PurchasePrice == 0)
                    continue;
                detail.PurchasePriceBefore = referencepriceRecord.PurchasePrice;
                detail.PriceType = referencepriceRecord.PriceType;//价格类型
            }
            //更新清单变更前价格
            foreach(var detail in dbPartsPurchaseOrderDetails) {
                var referencepriceRecord = new PartsPurchaseOrderDetailAch(this.DomainService).GetPartsPurchaseOrderDetailsByExecuteSql(partsPurchaseOrder.PartsSalesCategoryId, partsPurchaseOrder.PartsSupplierId, detail.SparePartId, partsPurchaseOrder.CreateTime);
                if(referencepriceRecord == null || referencepriceRecord.ReferencePrice == 0)
                    continue;
                detail.PurchasePriceBefore = referencepriceRecord.ReferencePrice;
                detail.PriceType = referencepriceRecord.PriceType;//价格类型
            }
        }

        //

        public void 提交配件采购订单(int[] partsPurchaseOrderIds) {
            foreach(var partsPurchaseOrderId in partsPurchaseOrderIds) {
                var dbpartsPurchaseOrder = ObjectContext.PartsPurchaseOrders.Where(r => r.Id == partsPurchaseOrderId && r.Status == (int)DcsPartsPurchaseOrderStatus.新增).FirstOrDefault();
                if(dbpartsPurchaseOrder == null)
                    throw new ValidationException(ErrorStrings.PartsPurchaseOrder_Validation3);
                UpdateToDatabase(dbpartsPurchaseOrder);
                dbpartsPurchaseOrder.Status = (int)DcsPartsPurchaseOrderStatus.提交;
                if(dbpartsPurchaseOrder.OriginalRequirementBillId == null) {
                    dbpartsPurchaseOrder.OriginalRequirementBillId = partsPurchaseOrderId;
                }
                if(dbpartsPurchaseOrder.OriginalRequirementBillType == null) {
                    dbpartsPurchaseOrder.OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.配件采购订单;
                }
                var now = DateTime.Now;
                var userInfo = Utils.GetCurrentUserInfo();
                dbpartsPurchaseOrder.SubmitterId = userInfo.Id;
                dbpartsPurchaseOrder.SubmitterName = userInfo.Name;
                dbpartsPurchaseOrder.SubmitTime = now;
                this.UpdatePartsPurchaseOrderValidate(dbpartsPurchaseOrder);
                var branchSupplierRelations = ObjectContext.BranchSupplierRelations.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.SupplierId == dbpartsPurchaseOrder.PartsSupplierId).OrderBy(e => e.Id);
                foreach(var partsPurchaseOrderDetail in dbpartsPurchaseOrder.PartsPurchaseOrderDetails) {
                    partsPurchaseOrderDetail.PlanDeliveryTime = now;
                    var branchSupplierRelation = branchSupplierRelations.Single();
                    var spareSupplierRelations = ObjectContext.PartsSupplierRelations.FirstOrDefault(o => o.PartsSalesCategoryId == dbpartsPurchaseOrder.PartsSalesCategoryId && o.SupplierId == dbpartsPurchaseOrder.PartsSupplierId && o.Status == (int)DcsBaseDataStatus.有效 && o.PartId == partsPurchaseOrderDetail.SparePartId);
                    if(branchSupplierRelation != null && spareSupplierRelations != null)
                        partsPurchaseOrderDetail.PromisedDeliveryTime = now.AddDays((branchSupplierRelation.ArrivalCycle ?? 0) + (spareSupplierRelations.OrderCycle ?? 0));

                    UpdateToDatabase(partsPurchaseOrderDetail);
                }
                ObjectContext.SaveChanges();
                //增加对采购订单下传IMS系统的方法的调用(不影响提交业务操作)
                var dbbranchstrategy = ObjectContext.Branchstrategies.SingleOrDefault(r => r.BranchId == dbpartsPurchaseOrder.BranchId && r.Status == (int)DcsBaseDataStatus.有效);
                if(dbbranchstrategy == null)
                    throw new ValidationException(ErrorStrings.Branchstrategy_Validation4);
                if(dbbranchstrategy.IsIMS.HasValue && dbbranchstrategy.IsIMS == true) {
                    if(dbpartsPurchaseOrder.PartsSupplierCode == "FT010063") {
                        var service = new WebServiceCalledIMS();
                        service.上传采购单生成xml(partsPurchaseOrderId, dbpartsPurchaseOrder.Code, false, 0);
                    }
                }
            }

        }

        public void 审批配件采购订单(PartsPurchaseOrder partsPurchaseOrder) {
            var dbpartsPurchaseOrder = ObjectContext.PartsPurchaseOrders.Where(r => r.Id == partsPurchaseOrder.Id && (r.Status == (int)DcsPartsPurchaseOrderStatus.提交 || r.Status == (int)DcsPartsPurchaseOrderStatus.部分确认 || r.Status == (int)DcsPartsPurchaseOrderStatus.部分发运)).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbpartsPurchaseOrder == null)
                throw new ValidationException(ErrorStrings.PartsPurchaseOrder_Validation4);
            var dbbranchstrategy = ObjectContext.Branchstrategies.SingleOrDefault(r => r.BranchId == partsPurchaseOrder.BranchId && r.Status == (int)DcsBaseDataStatus.有效);
            if(dbbranchstrategy == null)
                throw new ValidationException(ErrorStrings.Branchstrategy_Validation4);
            if(dbbranchstrategy.IsIMS.HasValue && dbbranchstrategy.IsIMS == true) {
                if(partsPurchaseOrder.PartsSupplierCode == "FT010063") {
                    throw new ValidationException("DPTS采购订单请在IMS系统内处理。");
                }
            }
            var dbpartsPurchaseOrderDetails = ObjectContext.PartsPurchaseOrderDetails.Where(r => r.PartsPurchaseOrderId == dbpartsPurchaseOrder.Id).SetMergeOption(MergeOption.NoTracking).ToArray();
            var partsPurchaseOrderDetails = partsPurchaseOrder.PartsPurchaseOrderDetails.ToArray();
            foreach(var partsPurchaseOrderDetail in partsPurchaseOrderDetails) {
                var dbPartsPurchaseOrderDetail = dbpartsPurchaseOrderDetails.SingleOrDefault(r => r.SparePartId == partsPurchaseOrderDetail.SparePartId);
                if(dbPartsPurchaseOrderDetail != null) {
                    partsPurchaseOrderDetail.ConfirmedAmount += dbPartsPurchaseOrderDetail.ConfirmedAmount;
                    if(partsPurchaseOrder.PartsPurchaseOrderDetails.Any(r => r.OrderAmount < r.ConfirmedAmount)) {
                        throw new ValidationException(ErrorStrings.PartsPurchaseOrder_Validation9);
                    }
                }
            }
            UpdateToDatabase(partsPurchaseOrder);
            if(partsPurchaseOrder.PartsPurchaseOrderDetails.Any(r => r.ConfirmedAmount < r.OrderAmount)) {
                if(dbpartsPurchaseOrder.Status == (int)DcsPartsPurchaseOrderStatus.提交) {
                    partsPurchaseOrder.Status = (int)DcsPartsPurchaseOrderStatus.部分确认;
                }
                if(dbpartsPurchaseOrder.Status == (int)DcsPartsPurchaseOrderStatus.部分发运) {
                    partsPurchaseOrder.Status = (int)DcsPartsPurchaseOrderStatus.部分发运;
                } else if(dbpartsPurchaseOrder.Status == (int)DcsPartsPurchaseOrderStatus.部分确认) {
                    partsPurchaseOrder.Status = (int)DcsPartsPurchaseOrderStatus.部分确认;
                }
            } else {
                if(dbpartsPurchaseOrder.Status == (int)DcsPartsPurchaseOrderStatus.提交) {
                    partsPurchaseOrder.Status = (int)DcsPartsPurchaseOrderStatus.确认完毕;
                }
                if(dbpartsPurchaseOrder.Status == (int)DcsPartsPurchaseOrderStatus.部分发运) {
                    partsPurchaseOrder.Status = (int)DcsPartsPurchaseOrderStatus.部分发运;
                } else if(dbpartsPurchaseOrder.Status == (int)DcsPartsPurchaseOrderStatus.部分确认) {
                    partsPurchaseOrder.Status = (int)DcsPartsPurchaseOrderStatus.确认完毕;
                }
            }
            var userInfo = Utils.GetCurrentUserInfo();
            partsPurchaseOrder.ApproverId = userInfo.Id;
            partsPurchaseOrder.ApproverName = userInfo.Name;
            partsPurchaseOrder.ApproveTime = DateTime.Now;
            if(partsPurchaseOrder.FirstApproveTime == null)
                partsPurchaseOrder.FirstApproveTime = DateTime.Now;
            partsPurchaseOrder.ApproveTime = DateTime.Now;
            this.UpdatePartsPurchaseOrderValidate(partsPurchaseOrder);
        }


        public void 终止配件采购订单(PartsPurchaseOrder partsPurchaseOrder) {           
            var dbpartsPurchaseOrder = ObjectContext.PartsPurchaseOrders.Where(r => r.Id == partsPurchaseOrder.Id && (r.Status == (int)DcsPartsPurchaseOrderStatus.提交 || r.Status == (int)DcsPartsPurchaseOrderStatus.部分确认 || r.Status == (int)DcsPartsPurchaseOrderStatus.部分发运 || r.Status == (int)DcsPartsPurchaseOrderStatus.确认完毕)).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbpartsPurchaseOrder == null)
                throw new ValidationException(ErrorStrings.PartsPurchaseOrder_Validation5);
            var status = dbpartsPurchaseOrder.Status;
            var dbbranchstrategy = ObjectContext.Branchstrategies.SingleOrDefault(r => r.BranchId == partsPurchaseOrder.BranchId && r.Status == (int)DcsBaseDataStatus.有效);
            if(dbbranchstrategy == null)
                throw new ValidationException(ErrorStrings.Branchstrategy_Validation4);
            if(dbbranchstrategy.IsIMS.HasValue && dbbranchstrategy.IsIMS == true) {
                if(partsPurchaseOrder.PartsSupplierCode == "FT010063") {
                    throw new ValidationException("DPTS采购订单请在IMS系统内处理。");
                }
            }
            UpdateToDatabase(partsPurchaseOrder);
            partsPurchaseOrder.Status = (int)DcsPartsPurchaseOrderStatus.终止;
            var userInfo = Utils.GetCurrentUserInfo();
            partsPurchaseOrder.CloserId = userInfo.Id;
            partsPurchaseOrder.CloserName = userInfo.Name;
            partsPurchaseOrder.CloseTime = DateTime.Now;
            this.UpdatePartsPurchaseOrderValidate(partsPurchaseOrder);
            //作废、终止采购单
            this.OffsetPartsPurchasePlanTemp(partsPurchaseOrder.Id, status);
            if(!string.IsNullOrEmpty(partsPurchaseOrder.PurOrderCode)) {
                var partsPurchasePlan = ObjectContext.PartsPurchasePlan_HW.FirstOrDefault(r => r.Code == partsPurchaseOrder.PurOrderCode);
                if(partsPurchasePlan != null) {
                    var partsPurchasePlanDetails = ObjectContext.PartsPurchasePlanDetail_HW.Where(r => r.PartsPurchasePlanId == partsPurchasePlan.Id).ToArray();
                    foreach(var partsPurchasePlanDetail in partsPurchasePlanDetails) {
                        foreach(var partsPurchaseOrderDetail in partsPurchaseOrder.PartsPurchaseOrderDetails) {
                            if(partsPurchasePlanDetail.PartId == partsPurchaseOrderDetail.SparePartId) {
                                partsPurchasePlanDetail.UntFulfilledQty += (partsPurchaseOrderDetail.OrderAmount - Convert.ToInt32((partsPurchaseOrderDetail.ShippingAmount ?? 0)));
                                partsPurchasePlanDetail.EndAmount = (partsPurchasePlanDetail.EndAmount ?? 0) + (partsPurchaseOrderDetail.OrderAmount - Convert.ToInt32((partsPurchaseOrderDetail.ShippingAmount ?? 0)));
                                UpdateToDatabase(partsPurchasePlanDetail);
                            }
                        }
                    }
                }
            }
        }


        public void 终止直供配件采购订单(PartsPurchaseOrder partsPurchaseOrder) {
            var status = partsPurchaseOrder.Status;
            var dbPartsPurchaseOrder = ObjectContext.PartsPurchaseOrders.Where(r => r.Id == partsPurchaseOrder.Id && (r.Status == (int)DcsPartsPurchaseOrderStatus.提交 || r.Status == (int)DcsPartsPurchaseOrderStatus.确认完毕 || r.Status == (int)DcsPartsPurchaseOrderStatus.部分发运) && r.IfDirectProvision).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbPartsPurchaseOrder == null)
                throw new ValidationException(ErrorStrings.PartsPurchaseOrder_Validation7);
            var dbbranchstrategy = ObjectContext.Branchstrategies.SingleOrDefault(r => r.BranchId == partsPurchaseOrder.BranchId && r.Status == (int)DcsBaseDataStatus.有效);
            if(dbbranchstrategy == null)
                throw new ValidationException(ErrorStrings.Branchstrategy_Validation4);          
            partsPurchaseOrder.Status = (int)DcsPartsPurchaseOrderStatus.终止;
            var userInfo = Utils.GetCurrentUserInfo();
            partsPurchaseOrder.CloserId = userInfo.Id;
            partsPurchaseOrder.CloserName = userInfo.Name;
            partsPurchaseOrder.CloseTime = DateTime.Now;
            this.UpdatePartsPurchaseOrderValidate(partsPurchaseOrder);         
            UpdateToDatabase(partsPurchaseOrder);
          
            if(partsPurchaseOrder.IfDirectProvision) {
                var partsPurchaseOrderDetails = partsPurchaseOrder.PartsPurchaseOrderDetails;
                var partsIds = partsPurchaseOrderDetails.Select(r => r.SparePartId).ToArray();
                var partsSalesOrder = ObjectContext.PartsSalesOrders.SingleOrDefault(r => r.Id == partsPurchaseOrder.OriginalRequirementBillId.Value && r.Status != (int)DcsPartsSalesOrderStatus.作废);
                if(partsSalesOrder == null)
                    throw new ValidationException(ErrorStrings.PartsPurchaseOrder_Validation8);
                var partsSalesOrderDetails = ObjectContext.PartsSalesOrderDetails.Where(r => r.PartsSalesOrderId == partsSalesOrder.Id && partsIds.Contains(r.SparePartId)).ToArray();
                if(dbPartsPurchaseOrder.Status == (int)DcsPartsPurchaseOrderStatus.提交) {
                   var tempStruct = from partsPurchaseOrderDetail in partsPurchaseOrderDetails
                                 from partsSalesOrderDetail in partsSalesOrderDetails
                                 let tamount = (partsPurchaseOrderDetail.OrderAmount - (partsPurchaseOrderDetail.ShippingAmount ?? 0))
                                 let pamout = partsSalesOrderDetail.ApproveQuantity ?? 0
                                 where partsPurchaseOrderDetail.SparePartId == partsSalesOrderDetail.SparePartId
                                 select new {
                                     tempamount = tamount > pamout ? pamout : tamount,
                                     tempprice = partsSalesOrderDetail.OrderPrice
                                 };
                   var cancelAmout = tempStruct.Sum(r => r.tempamount * r.tempprice);

                   new CustomerAccountAch(this.DomainService).终止直供销售订单解锁客户账户记录流水(partsSalesOrder, cancelAmout);
                } else {
                   var tempStruct = from partsPurchaseOrderDetail in partsPurchaseOrderDetails
                                 from partsSalesOrderDetail in partsSalesOrderDetails
                                 let tamount = (partsPurchaseOrderDetail.ConfirmedAmount - (partsPurchaseOrderDetail.ShippingAmount ?? 0))
                                 let pamout = partsSalesOrderDetail.ApproveQuantity ?? 0
                                 where partsPurchaseOrderDetail.SparePartId == partsSalesOrderDetail.SparePartId
                                 select new {
                                     tempamount = tamount > pamout ? pamout : tamount,
                                     tempprice = partsSalesOrderDetail.OrderPrice
                                 };
                   var cancelAmout = tempStruct.Sum(r => r.tempamount * r.tempprice);

                   new CustomerAccountAch(this.DomainService).终止直供销售订单解锁客户账户记录流水(partsSalesOrder, cancelAmout);
                }
               
               //若有内部领出单，则更新领出单或者作废领出单
                var internalAllocationBills = ObjectContext.InternalAllocationBills.Include("InternalAllocationDetails").Where(r => r.SourceCode == partsPurchaseOrder.OriginalRequirementBillCode).ToArray();
                if(internalAllocationBills.Count() > 0) {
                    if(status == (int)DcsPartsPurchaseOrderStatus.部分发运) {
                        //获取配件信息，并获取总成件所有的关键代码和总成件领用关系
                        var spareParts = ObjectContext.SpareParts.Where(o => partsIds.Contains(o.Id)).ToList();
                        var assemblyKeyCodes = spareParts.Where(o => o.PartType == (int)DcsSparePartPartType.总成件_领用).Select(o => o.AssemblyKeyCode).ToList();
                        var assemblyPartRequisitionLinks = ObjectContext.AssemblyPartRequisitionLinks.Where(o => assemblyKeyCodes.Contains(o.KeyCode) && o.Status == 1).ToList();
                        foreach(var partsPurchaseOrderDetail in partsPurchaseOrder.PartsPurchaseOrderDetails) {
                            var bill = internalAllocationBills.Where(r => r.ZcId == partsPurchaseOrderDetail.SparePartId).First();
                            var billDetail = bill.InternalAllocationDetails;
                            foreach(var detail in billDetail) {
                                var key = assemblyPartRequisitionLinks.Where(t => t.PartId == detail.SparePartId).FirstOrDefault();
                                detail.Quantity = detail.Quantity - ((partsPurchaseOrderDetail.OrderAmount - partsPurchaseOrderDetail.ShippingAmount.Value) * key.Qty.Value);
                                UpdateToDatabase(detail);
                            }
                            bill.TotalAmount = billDetail.Sum(t => t.UnitPrice * t.Quantity);
                            bill.Remark = bill.Remark + "终止采购订单，修改数量";
                            bill.ModifyTime = DateTime.Now;
                            UpdateToDatabase(bill);
                        }

                    } else {
                        foreach(var item in internalAllocationBills) {
                            item.Status = (int)DcsInternalAllocationBillStatus.作废;
                            item.AbandonerName = "终止采购订单，作废对应的领出单";
                            UpdateToDatabase(item);
                        }
                    }
                   
                    ObjectContext.SaveChanges();
                }
                
            }
        }

        public void 作废配件采购订单(PartsPurchaseOrder partsPurchaseOrder) {
            var dbpartsPurchaseOrder = ObjectContext.PartsPurchaseOrders.Where(r => r.Id == partsPurchaseOrder.Id && r.Status == (int)DcsPartsPurchaseOrderStatus.新增).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbpartsPurchaseOrder == null)
                throw new ValidationException(ErrorStrings.PartsPurchaseOrder_Validation6);
            var status = dbpartsPurchaseOrder.Status;
            UpdateToDatabase(partsPurchaseOrder);
            partsPurchaseOrder.Status = (int)DcsPartsPurchaseOrderStatus.作废;
            var userInfo = Utils.GetCurrentUserInfo();
            partsPurchaseOrder.AbandonerId = userInfo.Id;
            partsPurchaseOrder.AbandonerName = userInfo.Name;
            partsPurchaseOrder.AbandonTime = DateTime.Now;
            this.UpdatePartsPurchaseOrderValidate(partsPurchaseOrder);
            if(partsPurchaseOrder.IfDirectProvision) {
                var partsPurchaseOrderDetails = partsPurchaseOrder.PartsPurchaseOrderDetails;
                var partsIds = partsPurchaseOrderDetails.Select(r => r.SparePartId).ToArray();
                var partsSalesOrder = ObjectContext.PartsSalesOrders.SingleOrDefault(r => r.Id == partsPurchaseOrder.OriginalRequirementBillId.Value && r.Status != (int)DcsPartsSalesOrderStatus.作废);
                if(partsSalesOrder == null)
                    throw new ValidationException(ErrorStrings.PartsPurchaseOrder_Validation8);
                var partsSalesOrderDetails = ObjectContext.PartsSalesOrderDetails.Where(r => r.PartsSalesOrderId == partsSalesOrder.Id && partsIds.Contains(r.SparePartId)).ToArray();

                var cancleAmout = (from partsPurchaseOrderDetail in partsPurchaseOrderDetails
                                   from partsSalesOrderDetail in partsSalesOrderDetails
                                   where partsPurchaseOrderDetail.SparePartId == partsSalesOrderDetail.SparePartId
                                   select (partsPurchaseOrderDetail.OrderAmount - partsPurchaseOrderDetail.ConfirmedAmount) * partsSalesOrderDetail.OrderPrice).Sum();
                new CustomerAccountAch(this.DomainService).终止直供销售订单解锁客户账户记录流水(partsSalesOrder, cancleAmout);
            }
            if(!string.IsNullOrEmpty(partsPurchaseOrder.PurOrderCode)) {
                var partsPurchasePlan = ObjectContext.PartsPurchasePlan_HW.FirstOrDefault(r => r.Code == partsPurchaseOrder.PurOrderCode);
                if(partsPurchasePlan != null) {
                    var partsPurchasePlanDetails = ObjectContext.PartsPurchasePlanDetail_HW.Where(r => r.PartsPurchasePlanId == partsPurchasePlan.Id).ToArray();
                    foreach(var partsPurchasePlanDetail in partsPurchasePlanDetails) {
                        foreach(var partsPurchaseOrderDetail in partsPurchaseOrder.PartsPurchaseOrderDetails) {
                            if(partsPurchasePlanDetail.PartId == partsPurchaseOrderDetail.SparePartId) {
                                partsPurchasePlanDetail.UntFulfilledQty += (partsPurchaseOrderDetail.OrderAmount - Convert.ToInt32((partsPurchaseOrderDetail.ShippingAmount ?? 0)));
                                partsPurchasePlanDetail.EndAmount = (partsPurchasePlanDetail.EndAmount ?? 0) + (partsPurchaseOrderDetail.OrderAmount - Convert.ToInt32((partsPurchaseOrderDetail.ShippingAmount ?? 0)));
                                UpdateToDatabase(partsPurchasePlanDetail);
                            }
                        }
                    }
                }
            }
            //作废、终止采购单
            this.OffsetPartsPurchasePlanTemp(partsPurchaseOrder.Id, status);
        }

        private void OffsetPartsPurchasePlanTemp(int? partsPurchaseOrderId,int? status) {
            var userInfo = Utils.GetCurrentUserInfo();
            if(status != (int)DcsPartsPurchaseOrderStatus.部分发运) {
                var historyTep = ObjectContext.PartsPurchasePlanTemps.Where(t => t.PartsPurchaseOrderId == partsPurchaseOrderId).ToArray();
                foreach(var item in historyTep) {
                    var newPartsPurchasePlanTemp = new PartsPurchasePlanTemp {
                        PartsPurchaseOrderId = item.PartsPurchaseOrderId,
                        PartsPurchaseOrderCode = item.PartsPurchaseOrderCode,
                        WarehouseId = item.WarehouseId,
                        WarehouseName = item.WarehouseName,
                        PackSparePartId = item.PackSparePartId,
                        PackSparePartCode = item.PackSparePartCode,
                        PackSparePartName = item.PackSparePartName,
                        PackPlanQty = -item.PackPlanQty,
                        PackNum = item.PackNum,
                        OrderAmount = -item.OrderAmount,
                        SparePartId = item.SparePartId,
                        SparePartCode = item.SparePartCode,
                        SparePartName = item.SparePartName,
                        Status = (int)DcsBaseDataStatus.有效,
                        CreateTime = DateTime.Now,
                        CreatorId = userInfo.Id,
                        CreatorName = userInfo.Name 
                    };
                    InsertToDatabase(newPartsPurchasePlanTemp);
                }
            } else {
                var historyTep = ObjectContext.PartsPurchasePlanTemps.Where(t => t.PartsPurchaseOrderId == partsPurchaseOrderId).ToArray();
                var detail = ObjectContext.PartsPurchaseOrderDetails.Where(t => t.PartsPurchaseOrderId == partsPurchaseOrderId && (t.ShippingAmount == null || t.OrderAmount != t.ShippingAmount)).ToArray();
                foreach(var pDetail in detail) {
                    var items = historyTep.Where(t=>t.SparePartId==pDetail.SparePartId).ToArray();
                    foreach(var item in items) {
                        if(pDetail.ShippingAmount==null) {
                            pDetail.ShippingAmount = 0;
                        }
                        var newPartsPurchasePlanTemp = new PartsPurchasePlanTemp {
                            PartsPurchaseOrderId = item.PartsPurchaseOrderId,
                            PartsPurchaseOrderCode = item.PartsPurchaseOrderCode,
                            WarehouseId = item.WarehouseId,
                            WarehouseName = item.WarehouseName,
                            PackSparePartId = item.PackSparePartId,
                            PackSparePartCode = item.PackSparePartCode,
                            PackSparePartName = item.PackSparePartName,
                            PackPlanQty = -(pDetail.OrderAmount - pDetail.ShippingAmount),
                            PackNum = item.PackNum,
                            OrderAmount = -Int32.Parse((Math.Ceiling(Double.Parse((pDetail.OrderAmount - pDetail.ShippingAmount).ToString()) / Double.Parse(item.PackNum.ToString()))).ToString()),
                            SparePartId = item.SparePartId,
                            SparePartCode = item.SparePartCode,
                            SparePartName = item.SparePartName,
                            Status = (int)DcsBaseDataStatus.有效,
                            CreateTime = DateTime.Now,
                            CreatorId = userInfo.Id,
                            CreatorName = userInfo.Name
                        };
                        InsertToDatabase(newPartsPurchasePlanTemp);
                    }
                }
            }
            ObjectContext.SaveChanges();
        }
        public void 渠道变更(PartsPurchaseOrder partsPurchaseOrder) {
            var originalPartsPurchaseOrder = ObjectContext.PartsPurchaseOrders.FirstOrDefault(r => r.Id == partsPurchaseOrder.OriginalRequirementBillId && (r.Status == (int)DcsPartsPurchaseOrderStatus.提交 || r.Status == (int)DcsPartsPurchaseOrderStatus.部分发运 || r.Status == (int)DcsPartsPurchaseOrderStatus.部分确认));
            if(originalPartsPurchaseOrder == null) {
                throw new ValidationException(ErrorStrings.PartsPurchaseOrder_Validation16);
            }
            var originalPartsPurchaseOrderDetails = originalPartsPurchaseOrder.PartsPurchaseOrderDetails.ToArray();
            var partsPurchaseOrderDetails = partsPurchaseOrder.PartsPurchaseOrderDetails.ToArray();
            foreach(var partsPurchaseOrderDetail in partsPurchaseOrderDetails) {
                var originalSparePartId = partsPurchaseOrderDetail.OriginalSparePartId;
                var tempOriginalPartPurchaseOrderDetails = originalPartsPurchaseOrderDetails.SingleOrDefault(r => r.SparePartId == originalSparePartId);
                if(tempOriginalPartPurchaseOrderDetails != null) {
                    tempOriginalPartPurchaseOrderDetails.OrderAmount = tempOriginalPartPurchaseOrderDetails.OrderAmount - partsPurchaseOrderDetail.OrderAmount;
                    if(tempOriginalPartPurchaseOrderDetails.OrderAmount < 0) {
                        throw new ValidationException(ErrorStrings.PartsPurchaseOrder_Validation17);
                    }
                    tempOriginalPartPurchaseOrderDetails.Remark = string.Format(ErrorStrings.PartsPurchaseOrder_Validation18,partsPurchaseOrderDetail.OrderAmount);
                    UpdateToDatabase(tempOriginalPartPurchaseOrderDetails);
                }
            }

            if(originalPartsPurchaseOrderDetails.All(r => r.OrderAmount == 0)) {
                originalPartsPurchaseOrder.Status = (int)DcsPartsPurchaseOrderStatus.终止;
                originalPartsPurchaseOrder.TotalAmount = 0;
            } else {
                originalPartsPurchaseOrder.TotalAmount = originalPartsPurchaseOrderDetails.Sum(r => r.OrderAmount * r.UnitPrice);
            }
            UpdateToDatabase(originalPartsPurchaseOrder);
            this.UpdatePartsPurchaseOrderValidate(originalPartsPurchaseOrder);
        }



        public int 销售生成采购订单(Dictionary<int, int> partsIdWithQuantity, int outboundWarehouseId, int partsSalesOrderId, string partsSalesOrderCode, DateTime RequestedDeliveryTime) {
            try {
                var partIds = partsIdWithQuantity.Select(r => r.Key).ToArray();
                var outboundWarehouse = this.ObjectContext.Warehouses.SingleOrDefault(r => r.Id == outboundWarehouseId && r.Status == (int)DcsBaseDataStatus.有效);
                if(outboundWarehouse == null) {
                    throw new ValidationException(ErrorStrings.PartsPurchaseOrder_Validation19);
                }
                var parts = this.ObjectContext.SpareParts.Where(r => partIds.Contains(r.Id) && r.Status == (int)DcsMasterDataStatus.有效);
                var errorPartIds = partIds.Where(r => parts.All(v => v.Id != r)).ToArray();
                if(errorPartIds.Length > 0) {
                    var errorPartsCode = parts.Where(r => errorPartIds.Contains(r.Id)).Select(r => r.Code).ToArray();
                    string codes = string.Join(",", errorPartsCode);
                    throw new ValidationException(string.Format(ErrorStrings.PartsPurchaseOrder_Validation20,codes));
                }
                var arrayOfPartsSalesCategory = (from a in this.ObjectContext.PartsSalesCategories.Where(r => r.Status == (int)DcsBaseDataStatus.有效)
                                                 join b in this.ObjectContext.SalesUnits.Where(r => r.Status == (int)DcsMasterDataStatus.有效) on a.Id equals b.PartsSalesCategoryId
                                                 join c in this.ObjectContext.SalesUnitAffiWarehouses on b.Id equals c.SalesUnitId
                                                 join d in this.ObjectContext.Warehouses.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on c.WarehouseId equals d.Id
                                                 where d.Id == outboundWarehouseId
                                                 select a).ToArray();
                if(arrayOfPartsSalesCategory == null || arrayOfPartsSalesCategory.Length != 1) {
                    throw new ValidationException(ErrorStrings.PartsPurchaseOrder_Validation21);
                }
                var partsSalesCategoryOfOutboundWarehouse = arrayOfPartsSalesCategory[0];
                var partsBranches = this.ObjectContext.PartsBranches.Where(r => partIds.Contains(r.PartId) && r.PartsSalesCategoryId == partsSalesCategoryOfOutboundWarehouse.Id).ToArray();
                errorPartIds = partIds.Where(r => partsBranches.All(v => v.PartId != r)).ToArray();
                if(errorPartIds.Length > 0) {
                    var errorPartsCode = parts.Where(r => errorPartIds.Contains(r.Id)).Select(r => r.Code).ToArray();
                    string codes = string.Join(",", errorPartsCode);
                    throw new ValidationException(string.Format(ErrorStrings.PartsPurchaseOrder_Validation22,codes));
                }
                errorPartIds = partsBranches.Where(r => !r.IsOrderable).Select(r => r.PartId).ToArray();
                if(errorPartIds.Length > 0) {
                    var errorPartsCode = parts.Where(r => errorPartIds.Contains(r.Id)).Select(r => r.Code).ToArray();
                    string codes = string.Join(",", errorPartsCode);
                    throw new ValidationException(string.Format(ErrorStrings.PartsPurchaseOrder_Validation23,codes));
                }

                var partsSupplierRelations = this.ObjectContext.PartsSupplierRelations.Where(r => partIds.Contains(r.PartId) && r.Status == (int)DcsBaseDataStatus.有效 && r.PartsSalesCategoryId == partsSalesCategoryOfOutboundWarehouse.Id && r.IsPrimary == true).ToArray();
                errorPartIds = partIds.Where(r => partsSupplierRelations.All(v => v.PartId != r)).ToArray();
                if(errorPartIds.Length > 0) {
                    var errorPartsCode = parts.Where(r => errorPartIds.Contains(r.Id)).Select(r => r.Code).ToArray();
                    string codes = string.Join(",", errorPartsCode);
                    throw new ValidationException(string.Format(ErrorStrings.PartsPurchaseOrder_Validation24,codes));
                }
                var partsSuppliersOfPrimary = partsSupplierRelations.Select(r => r.PartsSupplier).Distinct().ToArray();
                var primarySupplierIds = partsSuppliersOfPrimary.Select(r => r.Id).ToArray();
                var branchOfOutboundWarehouse = this.ObjectContext.Branches.SingleOrDefault(r => r.Id == outboundWarehouse.BranchId && r.Status == (int)DcsBaseDataStatus.有效);
                if(branchOfOutboundWarehouse == null) {
                    throw new ValidationException(ErrorStrings.PartsPurchaseOrder_Validation25);
                }

                //var branchStrategy = this.ObjectContext.Branchstrategies.SingleOrDefault(r => r.BranchId == outboundWarehouse.BranchId && r.Status == (int)DcsBaseDataStatus.有效);
                //if(branchStrategy == null) {
                //    throw new ValidationException("未找到出库仓库对应的分公司的分公司策略");
                //}

                var salesCenterstrategy = this.ObjectContext.SalesCenterstrategies.SingleOrDefault(r => r.PartsSalesCategoryId == partsSalesCategoryOfOutboundWarehouse.Id && r.Status == (int)DcsBaseDataStatus.有效);
                if(salesCenterstrategy == null)
                    throw new ValidationException(ErrorStrings.PartsPurchaseOrder_Validation26);

                var tempPartsPurchasePricings = this.ObjectContext.PartsPurchasePricings.Where(r => r.ValidFrom <= DateTime.Now && r.ValidTo >= DateTime.Now && r.Status != (int)DcsPartsPurchasePricingStatus.作废 && r.PartsSalesCategoryId == partsSalesCategoryOfOutboundWarehouse.Id && partIds.Contains(r.PartId) && primarySupplierIds.Contains(r.PartsSupplierId)).ToArray();
                var partsPurchasePricings = tempPartsPurchasePricings.Where(r => partsSupplierRelations.Any(v => v.PartId == r.PartId && v.SupplierId == r.PartsSupplierId)).ToArray();
                if(salesCenterstrategy.PartsPurchasePricingStrategy == (int)DcsBranchstrategyPartsPurchasePricingStrategy.先定价) {
                    errorPartIds = partIds.Where(r => partsPurchasePricings.All(v => v.PartId != r)).ToArray();
                    if(errorPartIds.Length > 0) {
                        var errorPartsCode = parts.Where(r => errorPartIds.Contains(r.Id)).Select(r => r.Code).ToArray();
                        string codes = string.Join(",", errorPartsCode);
                        throw new ValidationException(string.Format(ErrorStrings.PartsPurchaseOrder_Validation27,codes));
                    }
                }
                var tempStruct = from a in partsIdWithQuantity
                                 join c in partsSupplierRelations on a.Key equals c.PartId
                                 join k in partsPurchasePricings on new {
                                     c.PartId,
                                     PartsSupplierId = c.SupplierId
                                 } equals new {
                                     k.PartId,
                                     k.PartsSupplierId
                                 } into tempTable
                                 from b in tempTable.DefaultIfEmpty()
                                 select new {
                                     c.PartId,
                                     PartsSupplierId = c.SupplierId,
                                     PurchasePrice = b == null ? 0 : b.PurchasePrice,
                                     Quantity = a.Value,

                                 };
                var tempGroups = tempStruct.GroupBy(r => new {
                    r.PartId,
                    r.PartsSupplierId,
                    r.PurchasePrice,
                }).Select(r => new {
                    r.Key.PartId,
                    r.Key.PartsSupplierId,
                    r.Key.PurchasePrice,
                    tempSumQuantiy = r.Sum(v => v.Quantity),
                }).GroupBy(r => new {
                    r.PartsSupplierId
                });
                var PartsPurchaseOrderType = this.ObjectContext.PartsPurchaseOrderTypes.SingleOrDefault(r => r.PartsSalesCategoryId == partsSalesCategoryOfOutboundWarehouse.Id && r.Name == "急需采购订单");
                if(PartsPurchaseOrderType == null) {
                    throw new ValidationException(ErrorStrings.PartsPurchaseOrder_Validation28);
                }
                int i = 0;
                foreach(var itemOfGroup in tempGroups) {
                    i++;
                    var tempPartSupplier = partsSuppliersOfPrimary.SingleOrDefault(r => r.Id == itemOfGroup.Key.PartsSupplierId);
                    if(tempPartSupplier == null) {
                        throw new ValidationException(ErrorStrings.PartsPurchaseOrder_Validation29);
                    }

                    var partsPurchaseOrder = new PartsPurchaseOrder();
                    partsPurchaseOrder.BranchId = branchOfOutboundWarehouse.Id;
                    partsPurchaseOrder.BranchCode = branchOfOutboundWarehouse.Code;
                    partsPurchaseOrder.BranchName = branchOfOutboundWarehouse.Name;
                    partsPurchaseOrder.WarehouseId = outboundWarehouse.Id;
                    partsPurchaseOrder.WarehouseName = outboundWarehouse.Name;
                    partsPurchaseOrder.PartsSalesCategoryId = partsSalesCategoryOfOutboundWarehouse.Id;
                    partsPurchaseOrder.PartsSalesCategoryName = partsSalesCategoryOfOutboundWarehouse.Name;
                    partsPurchaseOrder.PartsSupplierId = tempPartSupplier.Id;
                    partsPurchaseOrder.PartsSupplierCode = tempPartSupplier.Code;
                    partsPurchaseOrder.PartsSupplierName = tempPartSupplier.Name;
                    partsPurchaseOrder.OriginalRequirementBillId = partsSalesOrderId;
                    partsPurchaseOrder.OriginalRequirementBillCode = partsSalesOrderCode;
                    partsPurchaseOrder.OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.配件销售订单;
                    partsPurchaseOrder.PartsPurchaseOrderTypeId = PartsPurchaseOrderType.Id;
                    partsPurchaseOrder.RequestedDeliveryTime = RequestedDeliveryTime;
                    partsPurchaseOrder.Status = (int)DcsPartsPurchaseOrderStatus.新增;
                    partsPurchaseOrder.InStatus = (int)DcsPurchaseInStatus.未入库;
                    int j = 0;
                    foreach(var item in itemOfGroup) {
                        j++;
                        var tempSparePart = parts.SingleOrDefault(r => r.Id == item.PartId);
                        if(tempSparePart == null) {
                            throw new ValidationException(ErrorStrings.PartsPurchaseOrder_Validation30);
                        }
                        var tempPartsSupplierRelation = partsSupplierRelations.SingleOrDefault(r => r.PartId == tempSparePart.Id && r.SupplierId == tempPartSupplier.Id);
                        if(tempPartsSupplierRelation == null) {
                            throw new ValidationException(ErrorStrings.PartsPurchaseOrder_Validation31);
                        }
                        var partsPurchaseOrderDetail = new PartsPurchaseOrderDetail();
                        partsPurchaseOrderDetail.SerialNumber = j;
                        partsPurchaseOrderDetail.SparePartId = item.PartId;
                        partsPurchaseOrderDetail.SparePartCode = tempSparePart.Code;
                        partsPurchaseOrderDetail.SparePartName = tempSparePart.Name;
                        partsPurchaseOrderDetail.SupplierPartCode = tempPartsSupplierRelation.SupplierPartCode;
                        partsPurchaseOrderDetail.UnitPrice = item.PurchasePrice;
                        partsPurchaseOrderDetail.OrderAmount = item.tempSumQuantiy;
                        partsPurchaseOrderDetail.MeasureUnit = tempSparePart.MeasureUnit;
                        InsertToDatabase(partsPurchaseOrderDetail);
                        partsPurchaseOrder.PartsPurchaseOrderDetails.Add(partsPurchaseOrderDetail);
                    }
                    partsPurchaseOrder.TotalAmount = partsPurchaseOrder.PartsPurchaseOrderDetails.Sum(r => r.UnitPrice * r.OrderAmount);
                    //更新价格类型
                    foreach(var detail in partsPurchaseOrder.PartsPurchaseOrderDetails) {
                        var referencepriceRecord = ObjectContext.PartsPurchasePricings.Where(entitiy => entitiy.Status == 2 && entitiy.PartId == detail.SparePartId).OrderBy(entitiy => entitiy.CreateTime).FirstOrDefault();
                        if(referencepriceRecord == null || referencepriceRecord.PurchasePrice == 0)
                            continue;
                        detail.PurchasePriceBefore = referencepriceRecord.PurchasePrice;
                        detail.PriceType = referencepriceRecord.PriceType;//价格类型
                    }
                    //更新清单变更前价格
                    foreach(var detail in partsPurchaseOrder.PartsPurchaseOrderDetails) {
                        var referencepriceRecord = new PartsPurchaseOrderDetailAch(this.DomainService).GetPartsPurchaseOrderDetailsByExecuteSql(partsPurchaseOrder.PartsSalesCategoryId, partsPurchaseOrder.PartsSupplierId, detail.SparePartId, DateTime.Now);
                        if(referencepriceRecord == null || referencepriceRecord.ReferencePrice == 0)
                            continue;
                        detail.PurchasePriceBefore = referencepriceRecord.ReferencePrice;
                        detail.PriceType = referencepriceRecord.PriceType;//价格类型
                    }
                    InsertToDatabase(partsPurchaseOrder);
                    this.InsertPartsPurchaseOrderValidate(partsPurchaseOrder);
                }
                this.ObjectContext.SaveChanges();
                return i;
            } catch(Exception ex) {

                throw new ValidationException(ex.Message);
            }
        }

        public IQueryable<PartsPurchaseOrder> GetPartsPurchaseOrderByIds(int[] ids) {
            return ObjectContext.PartsPurchaseOrders.Include("PartsPurchaseOrderType").Where(r => ids.Contains(r.Id));
        }

        public void 供应商确认配件采购订单(PartsPurchaseOrder partsPurchaseOrder) {
            var dbpartsPurchaseOrder = ObjectContext.PartsPurchaseOrders.Where(r => r.Id == partsPurchaseOrder.Id && (r.Status == (int)DcsPartsPurchaseOrderStatus.提交)).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbpartsPurchaseOrder == null)
                throw new ValidationException(ErrorStrings.PartsPurchaseOrder_Validation32);
            var dbbranchstrategy = ObjectContext.Branchstrategies.SingleOrDefault(r => r.BranchId == partsPurchaseOrder.BranchId && r.Status == (int)DcsBaseDataStatus.有效);
            if(dbbranchstrategy == null)
                throw new ValidationException(ErrorStrings.Branchstrategy_Validation4);
            if(dbbranchstrategy.IsIMS.HasValue && dbbranchstrategy.IsIMS == true) {
                if(partsPurchaseOrder.PartsSupplierCode == "FT010063") {
                    throw new ValidationException("DPTS采购订单请在IMS系统内处理。");
                }
            }
            var dbpartsPurchaseOrderDetails = ObjectContext.PartsPurchaseOrderDetails.Where(r => r.PartsPurchaseOrderId == dbpartsPurchaseOrder.Id).SetMergeOption(MergeOption.NoTracking).ToArray();
            var partsPurchaseOrderDetails = partsPurchaseOrder.PartsPurchaseOrderDetails.ToArray();
            //校验确认量必须是配件主包装属性.系数的整数倍
            if(partsPurchaseOrder.PartsPurchaseOrderType.Name.IndexOf("急") < 0 && partsPurchaseOrder.PartsPurchaseOrderType.Name != "中心库采购") {
                var partsBranchPackingProps = ObjectContext.PartsBranchPackingProps.Where(o => ObjectContext.PartsBranches.Any(r => r.BranchId == partsPurchaseOrder.BranchId
                        && r.PartsSalesCategoryId == partsPurchaseOrder.PartsSalesCategoryId && r.Status == (int)DcsMasterDataStatus.有效
                        && r.MainPackingType == o.PackingType && r.PartId == o.SparePartId));
                foreach(var partsPurchaseOrderDetail in partsPurchaseOrderDetails) {
                    var dbPartsPurchaseOrderDetail = dbpartsPurchaseOrderDetails.SingleOrDefault(r => r.SparePartId == partsPurchaseOrderDetail.SparePartId);
                    if(dbPartsPurchaseOrderDetail != null) {
                        partsPurchaseOrderDetail.ConfirmedAmount += dbPartsPurchaseOrderDetail.ConfirmedAmount;
                        if(partsPurchaseOrder.PartsPurchaseOrderDetails.Any(r => r.OrderAmount < r.ConfirmedAmount)) {
                            throw new ValidationException(ErrorStrings.PartsPurchaseOrder_Validation9);
                        }

                        var partsBranchPackingProp = partsBranchPackingProps.Where(o => o.SparePartId == partsPurchaseOrderDetail.SparePartId).First();
                        if(partsBranchPackingProp == null)
                            throw new ValidationException(string.Format(ErrorStrings.PartsPurchaseOrder_Validation33, partsPurchaseOrderDetail.SupplierPartCode));
                        if(partsPurchaseOrderDetail.ConfirmedAmount % partsBranchPackingProp.PackingCoefficient > 0) {
                            throw new ValidationException(string.Format(ErrorStrings.PartsPurchaseOrder_Validation34, partsPurchaseOrderDetail.SupplierPartCode, partsBranchPackingProp.PackingCoefficient));
                        }
                    }
                }
            }
            UpdateToDatabase(partsPurchaseOrder);

            if(dbbranchstrategy.IsIMS.HasValue && dbbranchstrategy.IsIMS == true) {
                if(partsPurchaseOrder.PartsSupplierCode == "FT010063") {
                    throw new ValidationException("DPTS采购订单请在IMS系统内处理。");
                }
            }
            if(partsPurchaseOrder.IfDirectProvision) {
                var partsIds = partsPurchaseOrder.PartsPurchaseOrderDetails.Select(r => r.SparePartId).ToArray();
                var partsSalesOrder = ObjectContext.PartsSalesOrders.SingleOrDefault(r => r.Id == partsPurchaseOrder.OriginalRequirementBillId.Value && r.Status != (int)DcsPartsSalesOrderStatus.作废);
                if(partsSalesOrder == null)
                    throw new ValidationException(ErrorStrings.PartsPurchaseOrder_Validation8);
                var partsSalesOrderDetails = ObjectContext.PartsSalesOrderDetails.Where(r => r.PartsSalesOrderId == partsSalesOrder.Id && partsIds.Contains(r.SparePartId)).ToArray();
                var tempStruct = from partsPurchaseOrderDetail in partsPurchaseOrder.PartsPurchaseOrderDetails
                                 from partsSalesOrderDetail in partsSalesOrderDetails
                                 where partsPurchaseOrderDetail.SparePartId == partsSalesOrderDetail.SparePartId
                                 select new {
                                     tempamount = partsPurchaseOrderDetail.OrderAmount - partsPurchaseOrderDetail.ConfirmedAmount,
                                     tempprice = partsSalesOrderDetail.OrderPrice
                                 };
                var cancelAmout = tempStruct.Sum(r => r.tempamount * r.tempprice);

                if(cancelAmout > 0)
                    new CustomerAccountAch(this.DomainService).终止直供销售订单解锁客户账户记录流水(partsSalesOrder, cancelAmout);
            }

            if(partsPurchaseOrderDetails.Any(o => o.ConfirmedAmount > 0)) {
                partsPurchaseOrder.Status = (int)DcsPartsPurchaseOrderStatus.确认完毕;
                var userInfo = Utils.GetCurrentUserInfo();
                partsPurchaseOrder.ApproverId = userInfo.Id;
                partsPurchaseOrder.ApproverName = userInfo.Name;
                partsPurchaseOrder.ApproveTime = DateTime.Now;
                if(partsPurchaseOrder.FirstApproveTime == null)
                    partsPurchaseOrder.FirstApproveTime = DateTime.Now;
                partsPurchaseOrder.ApproveTime = DateTime.Now;
            }
            //供应商将所有品种确认为0，并填写了短供原因,系统自动终止采购订单
            if(partsPurchaseOrderDetails.All(o => o.ConfirmedAmount == 0&& o.ShortSupReason!=null)) {
                 this.终止配件采购订单(partsPurchaseOrder);
            }
            //订货量=确认量时，默认清空短供原因
            foreach(var detail in partsPurchaseOrder.PartsPurchaseOrderDetails.ToArray()) {
                if(detail.ConfirmedAmount == detail.OrderAmount && detail.ConfirmedAmount>0) {
                    detail.ShortSupReason = null;
                }
            }
            this.UpdatePartsPurchaseOrderValidate(partsPurchaseOrder);
        }
        public void 批量供应商确认配件采购订单(int[] ids) {
            foreach(var id in ids) {
                PartsPurchaseOrder partsPurchaseOrder = ObjectContext.PartsPurchaseOrders.Include("PartsPurchaseOrderDetails").Where(r => r.Id == id && (r.Status == (int)DcsPartsPurchaseOrderStatus.提交 || r.Status == (int)DcsPartsPurchaseOrderStatus.部分确认 || r.Status == (int)DcsPartsPurchaseOrderStatus.部分发运)).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
                if(partsPurchaseOrder == null)
                    throw new ValidationException(ErrorStrings.PartsPurchaseOrder_Validation4);
                var dbbranchstrategy = ObjectContext.Branchstrategies.SingleOrDefault(r => r.BranchId == partsPurchaseOrder.BranchId && r.Status == (int)DcsBaseDataStatus.有效);
                if(dbbranchstrategy == null)
                    throw new ValidationException(ErrorStrings.Branchstrategy_Validation4);
                var partsPurchaseOrderDetails = partsPurchaseOrder.PartsPurchaseOrderDetails.ToArray();
                foreach(var partsPurchaseOrderDetail in partsPurchaseOrderDetails) {
                    partsPurchaseOrderDetail.ConfirmedAmount = partsPurchaseOrderDetail.OrderAmount;
                    if(partsPurchaseOrder.PartsPurchaseOrderDetails.Any(r => r.OrderAmount < r.ConfirmedAmount)) {
                        throw new ValidationException(ErrorStrings.PartsPurchaseOrder_Validation9);
                    }
                    UpdateToDatabase(partsPurchaseOrderDetail);
                }
                UpdateToDatabase(partsPurchaseOrder);
                if(partsPurchaseOrder.IfDirectProvision) {
                    var partsIds = partsPurchaseOrderDetails.Select(r => r.SparePartId).ToArray();
                    var partsSalesOrder = ObjectContext.PartsSalesOrders.SingleOrDefault(r => r.Id == partsPurchaseOrder.OriginalRequirementBillId.Value && r.Status != (int)DcsPartsSalesOrderStatus.作废);
                    if(partsSalesOrder == null)
                        throw new ValidationException(ErrorStrings.PartsPurchaseOrder_Validation8);
                    var partsSalesOrderDetails = ObjectContext.PartsSalesOrderDetails.Where(r => r.PartsSalesOrderId == partsSalesOrder.Id && partsIds.Contains(r.SparePartId)).ToArray();
                    var tempStruct = from partsPurchaseOrderDetail in partsPurchaseOrderDetails
                                     from partsSalesOrderDetail in partsSalesOrderDetails
                                     where partsPurchaseOrderDetail.SparePartId == partsSalesOrderDetail.SparePartId
                                     select new {
                                         tempamount = partsPurchaseOrderDetail.OrderAmount - partsPurchaseOrderDetail.ConfirmedAmount,
                                         tempprice = partsSalesOrderDetail.OrderPrice
                                     };
                    var cancelAmout = tempStruct.Sum(r => r.tempamount * r.tempprice);

                    if(cancelAmout > 0)
                        new CustomerAccountAch(this.DomainService).终止直供销售订单解锁客户账户记录流水(partsSalesOrder, cancelAmout);
                }
                //订货量=确认量时，默认清空短供原因
                foreach(var detail in partsPurchaseOrder.PartsPurchaseOrderDetails.ToArray()) {
                    if(detail.ConfirmedAmount == detail.OrderAmount) {
                        detail.ShortSupReason = null;
                    }
                }
                partsPurchaseOrder.Status = (int)DcsPartsPurchaseOrderStatus.确认完毕;
                var userInfo = Utils.GetCurrentUserInfo();
                partsPurchaseOrder.ApproverId = userInfo.Id;
                partsPurchaseOrder.ApproverName = userInfo.Name;
                partsPurchaseOrder.ApproveTime = DateTime.Now;
                if(partsPurchaseOrder.FirstApproveTime == null)
                    partsPurchaseOrder.FirstApproveTime = DateTime.Now;
                partsPurchaseOrder.ApproveTime = DateTime.Now;
                this.UpdatePartsPurchaseOrderValidate(partsPurchaseOrder);
                ObjectContext.SaveChanges();
            }
        }

        public void 重传配件采购订单(int id) {
            var dbpartsPurchaseOrder = ObjectContext.PartsPurchaseOrders.Where(r => r.Id == id && (ObjectContext.PartsPurchaseOrder_Sync.Any(d => d.Objid == r.Code && (d.IOStatus == 1||d.IOStatus == 3)) || !ObjectContext.PartsPurchaseOrder_Sync.Any(d => d.Objid == r.Code))).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbpartsPurchaseOrder == null)
                throw new ValidationException(ErrorStrings.PartsPurchaseOrder_Validation35);
            var sync = ObjectContext.PartsPurchaseOrder_Sync.FirstOrDefault(r => r.Objid == dbpartsPurchaseOrder.Code);
            if (sync != null) {
                if (sync.IOStatus == 1) { 
                    sync.IOStatus = 0;
                    UpdateToDatabase(sync);
                    ObjectContext.SaveChanges();
                }
            }else{
                //新增一条接口数据
                sync = new PartsPurchaseOrder_Sync();
                sync.Objid = dbpartsPurchaseOrder.Code;
                sync.IOStatus = 0;
                sync.CreateTime = DateTime.Now;
                InsertToDatabase(sync);
                ObjectContext.SaveChanges();
            }
        }

    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:11:30
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 生成配件采购订单(PartsPurchaseOrder partsPurchaseOrder) {
            new PartsPurchaseOrderAch(this).生成配件采购订单(partsPurchaseOrder);
        }

        [Update(UsingCustomMethod = true)]
        public void 生成配件采购订单1(PartsPurchaseOrder partsPurchaseOrder) {
            new PartsPurchaseOrderAch(this).生成配件采购订单1(partsPurchaseOrder);
        }
        /// <summary>
        /// 仅供客户端使用
        /// </summary>
        [Update(UsingCustomMethod = true)]
        public void 修改配件采购订单(PartsPurchaseOrder partsPurchaseOrder) {
            new PartsPurchaseOrderAch(this).修改配件采购订单(partsPurchaseOrder);
        }
        //[Update(UsingCustomMethod = true)]
        [Invoke]
        public void 提交配件采购订单(int[] partsPurchaseOrderId) {
            new PartsPurchaseOrderAch(this).提交配件采购订单(partsPurchaseOrderId);
        }
        [Invoke]
        public void 重传配件采购订单(int id) { 
            new PartsPurchaseOrderAch(this).重传配件采购订单(id);
        }

        [Update(UsingCustomMethod = true)]
        public void 审批配件采购订单(PartsPurchaseOrder partsPurchaseOrder) {
            new PartsPurchaseOrderAch(this).审批配件采购订单(partsPurchaseOrder);
        }

        [Update(UsingCustomMethod = true)]
        public void 供应商确认配件采购订单(PartsPurchaseOrder partsPurchaseOrder) {
            new PartsPurchaseOrderAch(this).供应商确认配件采购订单(partsPurchaseOrder);
        }

        [Update(UsingCustomMethod = true)]
        public void 终止配件采购订单(PartsPurchaseOrder partsPurchaseOrder) {
            new PartsPurchaseOrderAch(this).终止配件采购订单(partsPurchaseOrder);
        }


        [Update(UsingCustomMethod = true)]
        public void 终止直供配件采购订单(PartsPurchaseOrder partsPurchaseOrder) {
            new PartsPurchaseOrderAch(this).终止直供配件采购订单(partsPurchaseOrder);
        }


        [Update(UsingCustomMethod = true)]
        public void 作废配件采购订单(PartsPurchaseOrder partsPurchaseOrder) {
            new PartsPurchaseOrderAch(this).作废配件采购订单(partsPurchaseOrder);
        }


        [Update(UsingCustomMethod = true)]
        public void 渠道变更(PartsPurchaseOrder partsPurchaseOrder) {
            new PartsPurchaseOrderAch(this).渠道变更(partsPurchaseOrder);
        }

        [Invoke]
        public void 批量供应商确认配件采购订单(int[] ids) {
            new PartsPurchaseOrderAch(this).批量供应商确认配件采购订单(ids);
        }

        [Invoke(HasSideEffects = true)]
        public int 销售生成采购订单(Dictionary<int, int> partsIdWithQuantity, int outboundWarehouseId, int partsSalesOrderId, string partsSalesOrderCode, DateTime RequestedDeliveryTime) {
            try {
                var partIds = partsIdWithQuantity.Select(r => r.Key).ToArray();
                var outboundWarehouse = this.ObjectContext.Warehouses.SingleOrDefault(r => r.Id == outboundWarehouseId && r.Status == (int)DcsBaseDataStatus.有效);
                if(outboundWarehouse == null) {
                    throw new ValidationException("出库仓库异常,此仓库已经被作废");
                }
                var parts = this.ObjectContext.SpareParts.Where(r => partIds.Contains(r.Id) && r.Status == (int)DcsMasterDataStatus.有效);
                var errorPartIds = partIds.Where(r => parts.All(v => v.Id != r)).ToArray();
                if(errorPartIds.Length > 0) {
                    var errorPartsCode = parts.Where(r => errorPartIds.Contains(r.Id)).Select(r => r.Code).ToArray();
                    string codes = string.Join(",", errorPartsCode);
                    throw new ValidationException("配件" + codes + "找不到配件营销信息");
                }
                var arrayOfPartsSalesCategory = (from a in this.ObjectContext.PartsSalesCategories.Where(r => r.Status == (int)DcsBaseDataStatus.有效)
                                                 join b in this.ObjectContext.SalesUnits.Where(r => r.Status == (int)DcsMasterDataStatus.有效) on a.Id equals b.PartsSalesCategoryId
                                                 join c in this.ObjectContext.SalesUnitAffiWarehouses on b.Id equals c.SalesUnitId
                                                 join d in this.ObjectContext.Warehouses.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on c.WarehouseId equals d.Id
                                                 where d.Id == outboundWarehouseId
                                                 select a).ToArray();
                if(arrayOfPartsSalesCategory == null || arrayOfPartsSalesCategory.Length != 1) {
                    throw new ValidationException("根据出库仓库获取品牌出现异常");
                }
                var partsSalesCategoryOfOutboundWarehouse = arrayOfPartsSalesCategory[0];
                var partsBranches = this.ObjectContext.PartsBranches.Where(r => partIds.Contains(r.PartId) && r.PartsSalesCategoryId == partsSalesCategoryOfOutboundWarehouse.Id).ToArray();
                errorPartIds = partIds.Where(r => partsBranches.All(v => v.PartId != r)).ToArray();
                if(errorPartIds.Length > 0) {
                    var errorPartsCode = parts.Where(r => errorPartIds.Contains(r.Id)).Select(r => r.Code).ToArray();
                    string codes = string.Join(",", errorPartsCode);
                    throw new ValidationException("配件" + codes + "找不到配件营销信息");
                }
                errorPartIds = partsBranches.Where(r => !r.IsOrderable).Select(r => r.PartId).ToArray();
                if(errorPartIds.Length > 0) {
                    var errorPartsCode = parts.Where(r => errorPartIds.Contains(r.Id)).Select(r => r.Code).ToArray();
                    string codes = string.Join(",", errorPartsCode);
                    throw new ValidationException("配件" + codes + "不可采购的无法生成采购订单");
                }

                var partsSupplierRelations = this.ObjectContext.PartsSupplierRelations.Where(r => partIds.Contains(r.PartId) && r.Status == (int)DcsBaseDataStatus.有效 && r.PartsSalesCategoryId == partsSalesCategoryOfOutboundWarehouse.Id && r.IsPrimary == true).ToArray();
                errorPartIds = partIds.Where(r => partsSupplierRelations.All(v => v.PartId != r)).ToArray();
                if(errorPartIds.Length > 0) {
                    var errorPartsCode = parts.Where(r => errorPartIds.Contains(r.Id)).Select(r => r.Code).ToArray();
                    string codes = string.Join(",", errorPartsCode);
                    throw new ValidationException("配件" + codes + "不存在首选供应商");
                }
                var partsSuppliersOfPrimary = partsSupplierRelations.Select(r => r.PartsSupplier).Distinct().ToArray();
                var primarySupplierIds = partsSuppliersOfPrimary.Select(r => r.Id).ToArray();
                var branchOfOutboundWarehouse = this.ObjectContext.Branches.SingleOrDefault(r => r.Id == outboundWarehouse.BranchId && r.Status == (int)DcsBaseDataStatus.有效);
                if(branchOfOutboundWarehouse == null) {
                    throw new ValidationException("未找到出库仓库对应的分公司");
                }

                //var branchStrategy = this.ObjectContext.Branchstrategies.SingleOrDefault(r => r.BranchId == outboundWarehouse.BranchId && r.Status == (int)DcsBaseDataStatus.有效);
                //if(branchStrategy == null) {
                //    throw new ValidationException("未找到出库仓库对应的分公司的分公司策略");
                //}

                var salesCenterstrategy = this.ObjectContext.SalesCenterstrategies.SingleOrDefault(r => r.PartsSalesCategoryId == partsSalesCategoryOfOutboundWarehouse.Id && r.Status == (int)DcsBaseDataStatus.有效);
                if(salesCenterstrategy == null)
                    throw new ValidationException("未找到出库仓库对应的销售中心策略");

                var tempPartsPurchasePricings = this.ObjectContext.PartsPurchasePricings.Where(r => r.ValidFrom <= DateTime.Now && r.ValidTo >= DateTime.Now && r.Status != (int)DcsPartsPurchasePricingStatus.作废 && r.PartsSalesCategoryId == partsSalesCategoryOfOutboundWarehouse.Id && partIds.Contains(r.PartId) && primarySupplierIds.Contains(r.PartsSupplierId)).ToArray();
                var partsPurchasePricings = tempPartsPurchasePricings.Where(r => partsSupplierRelations.Any(v => v.PartId == r.PartId && v.SupplierId == r.PartsSupplierId)).ToArray();
                if(salesCenterstrategy.PartsPurchasePricingStrategy == (int)DcsBranchstrategyPartsPurchasePricingStrategy.先定价) {
                    errorPartIds = partIds.Where(r => partsPurchasePricings.All(v => v.PartId != r)).ToArray();
                    if(errorPartIds.Length > 0) {
                        var errorPartsCode = parts.Where(r => errorPartIds.Contains(r.Id)).Select(r => r.Code).ToArray();
                        string codes = string.Join(",", errorPartsCode);
                        throw new ValidationException("配件" + codes + "不存在配件采购价(销售中心策略为先定价)");
                    }
                }
                var tempStruct = from a in partsIdWithQuantity
                                 join c in partsSupplierRelations on a.Key equals c.PartId
                                 join k in partsPurchasePricings on new {
                                     c.PartId,
                                     PartsSupplierId = c.SupplierId
                                 } equals new {
                                     k.PartId,
                                     k.PartsSupplierId
                                 } into tempTable
                                 from b in tempTable.DefaultIfEmpty()
                                 select new {
                                     c.PartId,
                                     PartsSupplierId = c.SupplierId,
                                     PurchasePrice = b == null ? 0 : b.PurchasePrice,
                                     Quantity = a.Value,

                                 };
                var tempGroups = tempStruct.GroupBy(r => new {
                    r.PartId,
                    r.PartsSupplierId,
                    r.PurchasePrice,
                }).Select(r => new {
                    r.Key.PartId,
                    r.Key.PartsSupplierId,
                    r.Key.PurchasePrice,
                    tempSumQuantiy = r.Sum(v => v.Quantity),
                }).GroupBy(r => new {
                    r.PartsSupplierId
                });
                var PartsPurchaseOrderType = this.ObjectContext.PartsPurchaseOrderTypes.SingleOrDefault(r => r.PartsSalesCategoryId == partsSalesCategoryOfOutboundWarehouse.Id && r.Name == "急需采购订单");
                if(PartsPurchaseOrderType == null) {
                    throw new ValidationException("获取配件采购订单类型时出现异常");
                }
                int i = 0;
                foreach(var itemOfGroup in tempGroups) {
                    i++;
                    var tempPartSupplier = partsSuppliersOfPrimary.SingleOrDefault(r => r.Id == itemOfGroup.Key.PartsSupplierId);
                    if(tempPartSupplier == null) {
                        throw new ValidationException("生成采购订单时获取供应商出现异常");
                    }

                    var partsPurchaseOrder = new PartsPurchaseOrder();
                    partsPurchaseOrder.BranchId = branchOfOutboundWarehouse.Id;
                    partsPurchaseOrder.BranchCode = branchOfOutboundWarehouse.Code;
                    partsPurchaseOrder.BranchName = branchOfOutboundWarehouse.Name;
                    partsPurchaseOrder.WarehouseId = outboundWarehouse.Id;
                    partsPurchaseOrder.WarehouseName = outboundWarehouse.Name;
                    partsPurchaseOrder.PartsSalesCategoryId = partsSalesCategoryOfOutboundWarehouse.Id;
                    partsPurchaseOrder.PartsSalesCategoryName = partsSalesCategoryOfOutboundWarehouse.Name;
                    partsPurchaseOrder.PartsSupplierId = tempPartSupplier.Id;
                    partsPurchaseOrder.PartsSupplierCode = tempPartSupplier.Code;
                    partsPurchaseOrder.PartsSupplierName = tempPartSupplier.Name;
                    partsPurchaseOrder.OriginalRequirementBillId = partsSalesOrderId;
                    partsPurchaseOrder.OriginalRequirementBillCode = partsSalesOrderCode;
                    partsPurchaseOrder.OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.配件销售订单;
                    partsPurchaseOrder.PartsPurchaseOrderTypeId = PartsPurchaseOrderType.Id;
                    partsPurchaseOrder.RequestedDeliveryTime = RequestedDeliveryTime;
                    partsPurchaseOrder.Status = (int)DcsPartsPurchaseOrderStatus.新增;
                    partsPurchaseOrder.InStatus = (int)DcsPurchaseInStatus.未入库;
                    int j = 0;
                    foreach(var item in itemOfGroup) {
                        j++;
                        var tempSparePart = parts.SingleOrDefault(r => r.Id == item.PartId);
                        if(tempSparePart == null) {
                            throw new ValidationException("生成采购清单时获取配件出现异常");
                        }
                        var tempPartsSupplierRelation = partsSupplierRelations.SingleOrDefault(r => r.PartId == tempSparePart.Id && r.SupplierId == tempPartSupplier.Id);
                        if(tempPartsSupplierRelation == null) {
                            throw new ValidationException("生成采购清单时获取配件与供应商关系出现异常");
                        }
                        var partsPurchaseOrderDetail = new PartsPurchaseOrderDetail();
                        partsPurchaseOrderDetail.SerialNumber = j;
                        partsPurchaseOrderDetail.SparePartId = item.PartId;
                        partsPurchaseOrderDetail.SparePartCode = tempSparePart.Code;
                        partsPurchaseOrderDetail.SparePartName = tempSparePart.Name;
                        partsPurchaseOrderDetail.SupplierPartCode = tempPartsSupplierRelation.SupplierPartCode;
                        partsPurchaseOrderDetail.UnitPrice = item.PurchasePrice;
                        partsPurchaseOrderDetail.OrderAmount = item.tempSumQuantiy;
                        partsPurchaseOrderDetail.MeasureUnit = tempSparePart.MeasureUnit;
                        InsertToDatabase(partsPurchaseOrderDetail);
                        partsPurchaseOrder.PartsPurchaseOrderDetails.Add(partsPurchaseOrderDetail);
                    }
                    partsPurchaseOrder.TotalAmount = partsPurchaseOrder.PartsPurchaseOrderDetails.Sum(r => r.UnitPrice * r.OrderAmount);
                    //更新价格类型
                    foreach(var detail in partsPurchaseOrder.PartsPurchaseOrderDetails) {
                        var referencepriceRecord = ObjectContext.PartsPurchasePricings.Where(entitiy => entitiy.Status == 2 && entitiy.PartId == detail.SparePartId).OrderBy(entitiy => entitiy.CreateTime).FirstOrDefault();
                        if(referencepriceRecord == null || referencepriceRecord.PurchasePrice == 0)
                            continue;
                        detail.PurchasePriceBefore = referencepriceRecord.PurchasePrice;
                        detail.PriceType = referencepriceRecord.PriceType;//价格类型
                    }
                    //更新清单变更前价格
                    foreach(var detail in partsPurchaseOrder.PartsPurchaseOrderDetails) {
                        var referencepriceRecord = new PartsPurchaseOrderDetailAch(this).GetPartsPurchaseOrderDetailsByExecuteSql(partsPurchaseOrder.PartsSalesCategoryId, partsPurchaseOrder.PartsSupplierId, detail.SparePartId, DateTime.Now);
                        if(referencepriceRecord == null || referencepriceRecord.ReferencePrice == 0)
                            continue;
                        detail.PurchasePriceBefore = referencepriceRecord.ReferencePrice;
                        detail.PriceType = referencepriceRecord.PriceType;//价格类型
                    }
                    InsertToDatabase(partsPurchaseOrder);
                    new PartsPurchaseOrderAch(this).InsertPartsPurchaseOrderValidate(partsPurchaseOrder);
                }
                this.ObjectContext.SaveChanges();
                return i;
            } catch(Exception ex) {

                throw new ValidationException(ex.Message);
            }
        }//保持原有的public方法，NoChangePublic


        public IQueryable<PartsPurchaseOrder> GetPartsPurchaseOrderByIds(int[] ids) {
            return new PartsPurchaseOrderAch(this).GetPartsPurchaseOrderByIds(ids);
        }
    }
}
