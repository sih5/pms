﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using System.Text;
using FTService;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;



namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsPurchasePlanAch : DcsSerivceAchieveBase {

        public void 生成配件采购计划订单(PartsPurchasePlan partsPurchasePlan) {
            var salesUnitAffiWarehouseID = from salesUnitAffiWarehouse in ObjectContext.SalesUnitAffiWarehouses
                                           join salesUnit in ObjectContext.SalesUnits on salesUnitAffiWarehouse.SalesUnitId equals salesUnit.Id
                                           where salesUnitAffiWarehouse.WarehouseId == partsPurchasePlan.WarehouseId && salesUnit.PartsSalesCategoryId == partsPurchasePlan.PartsSalesCategoryId
                                           select salesUnitAffiWarehouse;
            if(!salesUnitAffiWarehouseID.Any()) {
                throw new ValidationException(ErrorStrings.PartsPurchasePlan_Validation1);
            }
            //配件清单存在未维护最小包装数量的的配件,请先维护
            var dbPartsPurchasePlanDetails = partsPurchasePlan.PartsPurchasePlanDetails;

            PartsPurchaseOrderType partsPurchaseOrderType = ObjectContext.PartsPurchaseOrderTypes.FirstOrDefault(o => o.Id == partsPurchasePlan.PartsPlanTypeId);
            //紧急订单不校验计划量，中心库采购的订单也不校验
            if(partsPurchaseOrderType.Name.IndexOf("急") < 0 && partsPurchaseOrderType.Name != "中心库采购")
                this.validate(dbPartsPurchasePlanDetails.ToList());
            //因为服务端需要调用
            InsertToDatabase(partsPurchasePlan);
            this.InsertPartsPurchasePlanValidate(partsPurchasePlan);
        }
        //配件的计划量必须大于等于配件与首选供应商关系中的最小采购批量，
        //并且是 配件营销信息.最小包装.系数的  数量的整数倍

        private void validate(List<PartsPurchasePlanDetail> details) {
            var sparePartIds = details.Select(v => v.SparePartId);
            //查询配件与首选供应商的关系
            var PartCodes = ObjectContext.SpareParts.Where(e => sparePartIds.Contains(e.Id)).OrderBy(r => r.Id).ToArray();
            var partsSupplierRelations = ObjectContext.PartsSupplierRelations.Where(r => sparePartIds.Contains(r.PartId) && r.IsPrimary == true && r.Status == (int)DcsBaseDataStatus.有效).OrderBy(r => r.Id).ToArray();
            var mainPack = ObjectContext.PartsBranchPackingProps.Where(d => sparePartIds.Contains(d.SparePartId.Value) && ObjectContext.PartsBranches.Any(s => s.PartId == d.SparePartId && s.MainPackingType == d.PackingType && s.Id == d.PartsBranchId && s.Status == (int)DcsMasterDataStatus.有效)).ToArray();
            foreach(var item in details) {
                var relation = partsSupplierRelations.Where(r => r.PartId == item.SparePartId).FirstOrDefault();
                if(relation == null)
                    throw new ValidationException(string.Format(ErrorStrings.PartsPurchasePlan_Validation2, item.SparePartCode));
                else {
                    if(relation.MinBatch == null) {
                        relation.MinBatch = 0;
                    }
                    if(item.PlanAmount < relation.MinBatch.Value) {
                        throw new ValidationException(string.Format(ErrorStrings.PartsPurchasePlan_Validation3, item.SparePartCode, relation.MinBatch.Value));
                    }
                }
                var pack = mainPack.Where(r => r.SparePartId == item.SparePartId).FirstOrDefault();
                if(pack == null)
                    throw new ValidationException(string.Format(ErrorStrings.PartsPurchasePlan_Validation4, item.SparePartCode));
                else {
                    if(pack.PackingCoefficient == null) {
                        pack.PackingCoefficient = 0;
                    } else {
                        if(item.PlanAmount % pack.PackingCoefficient.Value > 0) {
                            throw new ValidationException(string.Format(ErrorStrings.PartsPurchasePlan_Validation5, item.SparePartCode, pack.PackingCoefficient.Value));
                        }
                    }
                }
            }
        }

        private PartsSupplierRelation[] validateForTerminate(List<PartsPurchasePlanDetail> details,bool isCheck) {
            var sparePartIds = details.Select(v => v.SparePartId);
            //查询配件与首选供应商的关系
            var PartCodes = ObjectContext.SpareParts.Where(e => sparePartIds.Contains(e.Id)).OrderBy(r => r.Id).ToArray();
            var partsSupplierRelations = ObjectContext.PartsSupplierRelations.Where(r => sparePartIds.Contains(r.PartId) && r.IsPrimary == true && r.Status == (int)DcsBaseDataStatus.有效).OrderBy(r => r.Id).ToArray();
            if (!isCheck)
                return partsSupplierRelations;
            var mainPack = ObjectContext.PartsBranchPackingProps.Where(d => sparePartIds.Contains(d.SparePartId.Value) && ObjectContext.PartsBranches.Any(s => s.PartId == d.SparePartId && s.MainPackingType == d.PackingType && s.Id == d.PartsBranchId && s.Status == (int)DcsMasterDataStatus.有效)).ToArray();
            foreach(var item in details) {
                var relation = partsSupplierRelations.Where(r => r.PartId == item.SparePartId).FirstOrDefault();
                if(relation == null)
                    throw new ValidationException(string.Format(ErrorStrings.PartsPurchasePlan_Validation2, item.SparePartCode));
                else {
                    if(relation.MinBatch == null) {
                        relation.MinBatch = 0;
                    }
                    if(item.PlanAmount < relation.MinBatch.Value) {
                        throw new ValidationException(string.Format(ErrorStrings.PartsPurchasePlan_Validation3, item.SparePartCode, relation.MinBatch.Value));
                    }
                }

                if(item.PartsPurchasePlan.PartsPurchaseOrderType.Name.IndexOf("急") < 0 && item.PartsPurchasePlan.PartsPurchaseOrderType.Name != "中心库采购") {
                    var pack = mainPack.Where(r => r.SparePartId == item.SparePartId).FirstOrDefault();
                    if(pack == null)
                        throw new ValidationException(string.Format(ErrorStrings.PartsPurchasePlan_Validation4, item.SparePartCode));
                    else {
                        if(pack.PackingCoefficient == null) {
                            pack.PackingCoefficient = 0;
                        } else {
                            if(item.PlanAmount % pack.PackingCoefficient.Value > 0) {
                                throw new ValidationException(string.Format(ErrorStrings.PartsPurchasePlan_Validation5, item.SparePartCode, pack.PackingCoefficient.Value));
                            }
                        }
                    }
                }
            }
            return partsSupplierRelations;
        }
        /// <summary>
        /// 仅供客户端使用
        /// </summary>
        public void 修改配件采购计划订单(PartsPurchasePlan partsPurchasePlan) {
            CheckEntityState(partsPurchasePlan);
            var salesUnitAffiWarehouseID = from salesUnitAffiWarehouse in ObjectContext.SalesUnitAffiWarehouses
                                           join salesUnit in ObjectContext.SalesUnits on salesUnitAffiWarehouse.SalesUnitId equals salesUnit.Id
                                           where salesUnitAffiWarehouse.WarehouseId == partsPurchasePlan.WarehouseId && salesUnit.PartsSalesCategoryId == partsPurchasePlan.PartsSalesCategoryId
                                           select salesUnitAffiWarehouse;
            if(!salesUnitAffiWarehouseID.Any()) {
                throw new ValidationException(ErrorStrings.PartsPurchasePlan_Validation1);
            }
            var dbpartsPurchasePlan = ObjectContext.PartsPurchasePlans.Where(r => r.Id == partsPurchasePlan.Id && (r.Status == (int)DcsPurchasePlanOrderStatus.新增)).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbpartsPurchasePlan == null)
                throw new ValidationException(ErrorStrings.PartsPurchasePlan_Validation6);
            //配件清单存在未维护最小包装数量的的配件,请先维护
            var dbPartsPurchasePlanDetails = partsPurchasePlan.PartsPurchasePlanDetails;
            //校验计划量
            PartsPurchaseOrderType partsPurchaseOrderType = ObjectContext.PartsPurchaseOrderTypes.FirstOrDefault(o => o.Id == partsPurchasePlan.PartsPlanTypeId);
            //紧急订单不校验计划量
            if(partsPurchaseOrderType.Name.IndexOf("急") < 0 && partsPurchaseOrderType.Name != "中心库采购")
                this.validate(dbPartsPurchasePlanDetails.ToList());
            //因为服务端需要调用
            UpdateToDatabase(partsPurchasePlan);
            this.UpdatePartsPurchasePlanValidate(partsPurchasePlan);
        }
        public void 作废配件采购计划订单(PartsPurchasePlan partsPurchasePlan) {
            var dbpartsPurchasePlan = ObjectContext.PartsPurchasePlans.Where(r => r.Id == partsPurchasePlan.Id && r.Status == (int)DcsPurchasePlanOrderStatus.新增).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbpartsPurchasePlan == null)
                throw new ValidationException(ErrorStrings.PartsPurchasePlan_Validation7);
            UpdateToDatabase(partsPurchasePlan);
            partsPurchasePlan.Status = (int)DcsPurchasePlanOrderStatus.作废;
            var userInfo = Utils.GetCurrentUserInfo();
            partsPurchasePlan.AbandonerId = userInfo.Id;
            partsPurchasePlan.AbandonerName = userInfo.Name;
            partsPurchasePlan.AbandonTime = DateTime.Now;
            this.UpdatePartsPurchasePlanValidate(partsPurchasePlan);
        }

        public void 驳回配件采购计划订单(PartsPurchasePlan partsPurchasePlan) {
            var dbpartsPurchasePlan = ObjectContext.PartsPurchasePlans.Where(r => r.Id == partsPurchasePlan.Id && (r.Status == (int)DcsPurchasePlanOrderStatus.提交 || r.Status == (int)DcsPurchasePlanOrderStatus.初审通过 || r.Status == (int)DcsPurchasePlanOrderStatus.审核通过)).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbpartsPurchasePlan == null)
                throw new ValidationException(ErrorStrings.PartsPurchasePlan_Validation8);
            UpdateToDatabase(partsPurchasePlan);
            partsPurchasePlan.Status = (int)DcsPurchasePlanOrderStatus.新增;
            var userInfo = Utils.GetCurrentUserInfo();
            if(partsPurchasePlan.Status == (int)DcsPurchasePlanOrderStatus.提交) {
                partsPurchasePlan.ApproverId = userInfo.Id;
                partsPurchasePlan.ApproverName = userInfo.Name;
                partsPurchasePlan.ApproveTime = DateTime.Now;
            } else {
                partsPurchasePlan.CloserId = userInfo.Id;
                partsPurchasePlan.CloserName = userInfo.Name;
                partsPurchasePlan.CloseTime = DateTime.Now;
            }
            this.UpdatePartsPurchasePlanValidate(partsPurchasePlan);
        }
        public void 提交配件采购计划订单(int partsPurchasePlanId) {
            var userInfo = Utils.GetCurrentUserInfo();
            var dbpartsPurchasePlan = ObjectContext.PartsPurchasePlans.Where(r => r.Id == partsPurchasePlanId && r.Status == (int)DcsPurchasePlanOrderStatus.新增).FirstOrDefault();
            if(dbpartsPurchasePlan == null)
                throw new ValidationException(ErrorStrings.PartsPurchasePlan_Validation9);

            var dbPartsPurchasePlanDetails = dbpartsPurchasePlan.PartsPurchasePlanDetails.ToArray();
            //var dbPartsPurchasePlanDetails = ObjectContext.PartsPurchasePlanDetails.Where(r => r.PurchasePlanId == partsPurchasePlanId).ToArray();
            var sparePartIds = dbPartsPurchasePlanDetails.Select(v => v.SparePartId).ToArray();
            //配件不可采购 提示错误
            var dbPartsBrancheIds = ObjectContext.PartsBranches.Where(r => sparePartIds.Contains(r.PartId) && r.IsOrderable == true && r.BranchId == dbpartsPurchasePlan.BranchId && r.PartsSalesCategoryId == dbpartsPurchasePlan.PartsSalesCategoryId && r.Status == (int)DcsBaseDataStatus.有效).Select(v => v.PartId).ToArray();
            var partCodes = dbPartsPurchasePlanDetails.Where(e => !dbPartsBrancheIds.Contains(e.SparePartId)).Select(v => v.SparePartCode).ToArray();
            if(partCodes.Length > 0) {
                string erro1 = "";
                for(int i = 0; i < partCodes.Length; i++) {
                    if(string.IsNullOrEmpty(erro1))
                        erro1 = "【" + partCodes[i] + "】";
                    else
                        erro1 += "【" + partCodes[i] + "】";
                }
                if(!string.IsNullOrEmpty(erro1))
                    throw new ValidationException(string.Format(ErrorStrings.PartsPurchasePlan_Validation10, erro1));
            }
            ////配件无采购价 提示错误 PartsPurchasePricing
            //var dbPartsPricings = ObjectContext.PartsPurchasePricings.Where(r => sparePartIds.Contains(r.PartId) && r.BranchId == dbpartsPurchasePlan.BranchId && r.PartsSalesCategoryId == dbpartsPurchasePlan.PartsSalesCategoryId && r.Status == (int)DcsPartsPurchasePricingStatus.生效).Select(v => v.PartId).ToArray();
            //var partCodeForPrices = partsPurchaseOrderDetails.Where(e => !dbPartsPricings.Contains(e.SparePartId)).Select(v => v.SparePartCode).ToArray();
            //if (partCodeForPrices.Length > 0)
            //{
            //    string erro1 = "";
            //    for (int i = 0; i < partCodeForPrices.Length; i++)
            //    {
            //        if (string.IsNullOrEmpty(erro1))
            //            erro1 = "【" + partCodeForPrices[i] + "】";
            //        else
            //            erro1 += "【" + partCodeForPrices[i] + "】";
            //    }
            //    if (!string.IsNullOrEmpty(erro1))
            //        throw new ValidationException(string.Format("配件{0}不存在采购价", erro1));
            //}
            //配件无首选供应商 提示错误  PartsSupplierRelation  IsPrimary
            var dbPartsPrimarys = ObjectContext.PartsSupplierRelations.Where(r => sparePartIds.Contains(r.PartId) && r.IsPrimary == true && r.BranchId == dbpartsPurchasePlan.BranchId && r.PartsSalesCategoryId == dbpartsPurchasePlan.PartsSalesCategoryId && r.Status == (int)DcsBaseDataStatus.有效).Select(v => v.PartId).ToArray();
            var partCodeForPrimarys = dbPartsPurchasePlanDetails.Where(e => !dbPartsPrimarys.Contains(e.SparePartId)).Select(v => v.SparePartCode).ToArray();
            if(partCodeForPrimarys.Length > 0) {
                string erro1 = "";
                for(int i = 0; i < partCodeForPrimarys.Length; i++) {
                    if(string.IsNullOrEmpty(erro1))
                        erro1 = "【" + partCodeForPrimarys[i] + "】";
                    else
                        erro1 += "【" + partCodeForPrimarys[i] + "】";
                }
                if(!string.IsNullOrEmpty(erro1))
                    throw new ValidationException(string.Format(ErrorStrings.PartsPurchasePlan_Validation11, erro1));
            }
            //配件无采购价 提示错误 PartsPurchasePricing
            string error = "";
            var dbPartsPrimarysForPirce = ObjectContext.PartsSupplierRelations.Where(r => sparePartIds.Contains(r.PartId) && r.IsPrimary == true && r.BranchId == dbpartsPurchasePlan.BranchId && r.PartsSalesCategoryId == dbpartsPurchasePlan.PartsSalesCategoryId && r.Status == (int)DcsBaseDataStatus.有效).ToArray();
            var supplierIds = dbPartsPrimarysForPirce.Select(r => r.SupplierId).ToArray();
            var dbPartsPricingsAll = ObjectContext.PartsPurchasePricings.Where(r => sparePartIds.Contains(r.PartId) && r.BranchId == dbpartsPurchasePlan.BranchId && r.PartsSalesCategoryId == dbpartsPurchasePlan.PartsSalesCategoryId && r.Status == (int)DcsPartsPurchasePricingStatus.生效 && supplierIds.Contains(r.PartsSupplierId)).ToArray();
            foreach(var item in dbPartsPrimarysForPirce) {
                var dbPartsPricings = dbPartsPricingsAll.Where(r => r.PartId == item.PartId && r.PartsSupplierId == item.SupplierId).ToArray();
                if(dbPartsPricings.Length == 0)
                    error += "【" + item.SparePart.Code + "】";

                if(dbPartsPricings.Length > 0) {
                    var time = DateTime.Now;
                    var dbPartsPurchasePlanDetail = dbPartsPurchasePlanDetails.Where(o => o.SparePartId == item.PartId).FirstOrDefault();
                    var dbPartsPricing = dbPartsPricings.FirstOrDefault(o => o.ValidFrom <= time && o.ValidTo >= time);
                    if(dbPartsPricing.LimitQty.HasValue && dbPartsPricing.LimitQty.Value > 0 && (dbPartsPricing.LimitQty ?? 0) - (dbPartsPricing.UsedQty ?? 0) < dbPartsPurchasePlanDetail.PlanAmount) {
                        throw new ValidationException(string.Format(ErrorStrings.PartsPurchasePlan_Validation12, item.SparePart.Code));
                    } else {
                        dbPartsPricing.UsedQty = (dbPartsPricing.UsedQty ?? 0) + dbPartsPurchasePlanDetail.PlanAmount;
                        dbPartsPricing.ModifierId = userInfo.Id;
                        dbPartsPricing.ModifierName = userInfo.Name;
                        dbPartsPricing.ModifyTime = DateTime.Now;
                        UpdateToDatabase(dbPartsPricing);
                    }
                }
            }
            if(!string.IsNullOrEmpty(error)) {
                throw new ValidationException(string.Format(ErrorStrings.PartsPurchasePlan_Validation13, error));
            }
            //暂时，提交时，将状态改为初审通过
          //  dbpartsPurchasePlan.Status = (int)DcsPurchasePlanOrderStatus.提交;
            dbpartsPurchasePlan.Status = (int)DcsPurchasePlanOrderStatus.初审通过;
            dbpartsPurchasePlan.ApproverId = userInfo.Id;
            dbpartsPurchasePlan.ApproverName = userInfo.Name;
            dbpartsPurchasePlan.ApproveTime = DateTime.Now;

            dbpartsPurchasePlan.SubmitterId = userInfo.Id;
            dbpartsPurchasePlan.SubmitterName = userInfo.Name;
            dbpartsPurchasePlan.SubmitTime = DateTime.Now;
            UpdateToDatabase(dbpartsPurchasePlan);
            this.UpdatePartsPurchasePlanValidate(dbpartsPurchasePlan);
            ObjectContext.SaveChanges();
        }

        /// <summary>
        /// 审核
        /// </summary>
        /// <param name="partsPurchasePlan"></param>
        public void 审核配件采购计划订单(PartsPurchasePlan partsPurchasePlan) {
            var dbpartsPurchasePlan = ObjectContext.PartsPurchasePlans.Where(r => r.Id == partsPurchasePlan.Id && (r.Status == (int)DcsPurchasePlanOrderStatus.初审通过)).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbpartsPurchasePlan == null)
                throw new ValidationException(ErrorStrings.PartsPurchasePlan_Approve_InitApprove);
            var dbbranchstrategy = ObjectContext.Branchstrategies.SingleOrDefault(r => r.BranchId == partsPurchasePlan.BranchId && r.Status == (int)DcsBaseDataStatus.有效);
            if(dbbranchstrategy == null)
                throw new ValidationException(ErrorStrings.SupplierShippingOrder_Validation29);
            if(partsPurchasePlan.Status.Equals(66)) {
                var dbpartsPurchasePlanDetails = ObjectContext.PartsPurchasePlanDetails.Where(r => r.PurchasePlanId == dbpartsPurchasePlan.Id).SetMergeOption(MergeOption.NoTracking).ToArray();
                var partsPurchaseOrderDetails = partsPurchasePlan.PartsPurchasePlanDetails.ToArray();
                var sparePartIds = partsPurchaseOrderDetails.Select(v => v.SparePartId);
                //配件不可采购 提示错误
                var dbPartsBrancheIds = ObjectContext.PartsBranches.Where(r => sparePartIds.Contains(r.PartId) && r.IsOrderable == true && r.BranchId == dbpartsPurchasePlan.BranchId && r.PartsSalesCategoryId == dbpartsPurchasePlan.PartsSalesCategoryId && r.Status == (int)DcsBaseDataStatus.有效).Select(v => v.PartId).ToArray();
                var partCodes = partsPurchaseOrderDetails.Where(e => !dbPartsBrancheIds.Contains(e.SparePartId)).Select(v => v.SparePartCode).ToArray();
                if(partCodes.Length > 0) {
                    string erro1 = "";
                    for(int i = 0; i < partCodes.Length; i++) {
                        if(string.IsNullOrEmpty(erro1))
                            erro1 = "【" + partCodes[i] + "】";
                        else
                            erro1 += "【" + partCodes[i] + "】";
                    }
                    if(!string.IsNullOrEmpty(erro1))
                        throw new ValidationException(string.Format(ErrorStrings.PartsPurchasePlan_Validation10, erro1));
                }
                PartsPurchaseOrderType partsPurchaseOrderType = ObjectContext.PartsPurchaseOrderTypes.FirstOrDefault(o => o.Id == partsPurchasePlan.PartsPlanTypeId);
                //紧急订单不校验计划量
                if(partsPurchaseOrderType.Name.IndexOf("急") < 0 && partsPurchaseOrderType.Name != "中心库采购")
                    this.validate(partsPurchaseOrderDetails.ToList());

                //配件无首选供应商 提示错误  PartsSupplierRelation  IsPrimary
                var dbPartsPrimarys = ObjectContext.PartsSupplierRelations.Where(r => sparePartIds.Contains(r.PartId) && r.IsPrimary == true && r.BranchId == dbpartsPurchasePlan.BranchId && r.PartsSalesCategoryId == dbpartsPurchasePlan.PartsSalesCategoryId && r.Status == (int)DcsBaseDataStatus.有效).Select(v => v.PartId).ToArray();
                var partCodeForPrimarys = partsPurchaseOrderDetails.Where(e => !dbPartsPrimarys.Contains(e.SparePartId)).Select(v => v.SparePartCode).ToArray();
                if(partCodeForPrimarys.Length > 0) {
                    string erro1 = "";
                    for(int i = 0; i < partCodeForPrimarys.Length; i++) {
                        if(string.IsNullOrEmpty(erro1))
                            erro1 = "【" + partCodeForPrimarys[i] + "】";
                        else
                            erro1 += "【" + partCodeForPrimarys[i] + "】";
                    }
                    if(!string.IsNullOrEmpty(erro1))
                        throw new ValidationException(string.Format(ErrorStrings.PartsPurchasePlan_Validation11, erro1));
                }
                //配件无采购价 提示错误 PartsPurchasePricing
                string error = "";
                var dbPartsPrimarysForPirce = ObjectContext.PartsSupplierRelations.Where(r => sparePartIds.Contains(r.PartId) && r.IsPrimary == true && r.BranchId == dbpartsPurchasePlan.BranchId && r.PartsSalesCategoryId == dbpartsPurchasePlan.PartsSalesCategoryId && r.Status == (int)DcsBaseDataStatus.有效).ToArray();
                var supplierIds = dbPartsPrimarysForPirce.Select(r => r.SupplierId).ToArray();
                var dbPartsPricingsAll = ObjectContext.PartsPurchasePricings.Where(r => sparePartIds.Contains(r.PartId) && r.BranchId == dbpartsPurchasePlan.BranchId && r.PartsSalesCategoryId == dbpartsPurchasePlan.PartsSalesCategoryId && r.Status == (int)DcsPartsPurchasePricingStatus.生效 && supplierIds.Contains(r.PartsSupplierId)).ToArray();

                foreach(var item in dbPartsPrimarysForPirce) {
                    //var dbPartsPricings = ObjectContext.PartsPurchasePricings.Where(r => r.PartId == item.PartId && r.BranchId == dbpartsPurchasePlan.BranchId && r.PartsSalesCategoryId == dbpartsPurchasePlan.PartsSalesCategoryId && r.Status == (int)DcsPartsPurchasePricingStatus.生效 && r.PartsSupplierId == item.SupplierId).ToArray();
                    var dbPartsPricings = dbPartsPricingsAll.Where(r => r.PartId == item.PartId && r.PartsSupplierId == item.SupplierId).ToArray();
                    if(dbPartsPricings.Length == 0)
                        error += "【" + item.SparePart.Code + "】";
                }
                if(!string.IsNullOrEmpty(error)) {
                    throw new ValidationException(string.Format(ErrorStrings.PartsPurchasePlan_Validation13, error));
                }
                var userInfo = Utils.GetCurrentUserInfo();
                partsPurchasePlan.CheckerId = userInfo.Id;
                partsPurchasePlan.CheckerName = userInfo.Name;
                partsPurchasePlan.CheckTime = DateTime.Now;
                partsPurchasePlan.Status = (int)DcsPurchasePlanOrderStatus.审核通过;
                UpdateToDatabase(partsPurchasePlan);
                this.UpdatePartsPurchasePlanValidate(partsPurchasePlan);
            } else if(partsPurchasePlan.Status.Equals(77)) {
                var userInfo = Utils.GetCurrentUserInfo();
                partsPurchasePlan.CheckerId = userInfo.Id;
                partsPurchasePlan.CheckerName = userInfo.Name;
                partsPurchasePlan.CheckTime = DateTime.Now;
                partsPurchasePlan.Status = (int)DcsPurchasePlanOrderStatus.新增;
                UpdateToDatabase(partsPurchasePlan);
                this.UpdatePartsPurchasePlanValidate(partsPurchasePlan);
            }
        }

        public void 初审配件采购计划订单(PartsPurchasePlan partsPurchasePlan) {
            var dbpartsPurchasePlan = ObjectContext.PartsPurchasePlans.Where(r => r.Id == partsPurchasePlan.Id && (r.Status == (int)DcsPurchasePlanOrderStatus.提交)).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbpartsPurchasePlan == null)
                throw new ValidationException(ErrorStrings.PartsPurchasePlan_Validation14);
            var dbbranchstrategy = ObjectContext.Branchstrategies.SingleOrDefault(r => r.BranchId == partsPurchasePlan.BranchId && r.Status == (int)DcsBaseDataStatus.有效);
            if(dbbranchstrategy == null)
                throw new ValidationException(ErrorStrings.SupplierShippingOrder_Validation29);
            if(partsPurchasePlan.Status.Equals(66)) {
                var dbpartsPurchasePlanDetails = ObjectContext.PartsPurchasePlanDetails.Where(r => r.PurchasePlanId == dbpartsPurchasePlan.Id).SetMergeOption(MergeOption.NoTracking).ToArray();
                var partsPurchaseOrderDetails = partsPurchasePlan.PartsPurchasePlanDetails.ToArray();
                var sparePartIds = partsPurchaseOrderDetails.Select(v => v.SparePartId);
                //配件不可采购 提示错误
                var dbPartsBrancheIds = ObjectContext.PartsBranches.Where(r => sparePartIds.Contains(r.PartId) && r.IsOrderable == true && r.BranchId == dbpartsPurchasePlan.BranchId && r.PartsSalesCategoryId == dbpartsPurchasePlan.PartsSalesCategoryId && r.Status == (int)DcsBaseDataStatus.有效).Select(v => v.PartId).ToArray();
                var partCodes = partsPurchaseOrderDetails.Where(e => !dbPartsBrancheIds.Contains(e.SparePartId)).Select(v => v.SparePartCode).ToArray();
                if(partCodes.Length > 0) {
                    string erro1 = "";
                    for(int i = 0; i < partCodes.Length; i++) {
                        if(string.IsNullOrEmpty(erro1))
                            erro1 = "【" + partCodes[i] + "】";
                        else
                            erro1 += "【" + partCodes[i] + "】";
                    }
                    if(!string.IsNullOrEmpty(erro1))
                        throw new ValidationException(string.Format(ErrorStrings.PartsPurchasePlan_Validation10, erro1));
                }
                PartsPurchaseOrderType partsPurchaseOrderType = ObjectContext.PartsPurchaseOrderTypes.FirstOrDefault(o => o.Id == partsPurchasePlan.PartsPlanTypeId);
                //紧急订单不校验计划量
                if(partsPurchaseOrderType.Name.IndexOf("急") < 0 && partsPurchaseOrderType.Name != "中心库采购")
                    this.validate(partsPurchaseOrderDetails.ToList());

                //配件无首选供应商 提示错误  PartsSupplierRelation  IsPrimary
                var dbPartsPrimarys = ObjectContext.PartsSupplierRelations.Where(r => sparePartIds.Contains(r.PartId) && r.IsPrimary == true && r.BranchId == dbpartsPurchasePlan.BranchId && r.PartsSalesCategoryId == dbpartsPurchasePlan.PartsSalesCategoryId && r.Status == (int)DcsBaseDataStatus.有效).Select(v => v.PartId).ToArray();
                var partCodeForPrimarys = partsPurchaseOrderDetails.Where(e => !dbPartsPrimarys.Contains(e.SparePartId)).Select(v => v.SparePartCode).ToArray();
                if(partCodeForPrimarys.Length > 0) {
                    string erro1 = "";
                    for(int i = 0; i < partCodeForPrimarys.Length; i++) {
                        if(string.IsNullOrEmpty(erro1))
                            erro1 = "【" + partCodeForPrimarys[i] + "】";
                        else
                            erro1 += "【" + partCodeForPrimarys[i] + "】";
                    }
                    if(!string.IsNullOrEmpty(erro1))
                        throw new ValidationException(string.Format(ErrorStrings.PartsPurchasePlan_Validation11, erro1));
                }
                //配件无采购价 提示错误 PartsPurchasePricing
                string error = "";
                var dbPartsPrimarysForPirce = ObjectContext.PartsSupplierRelations.Where(r => sparePartIds.Contains(r.PartId) && r.IsPrimary == true && r.BranchId == dbpartsPurchasePlan.BranchId && r.PartsSalesCategoryId == dbpartsPurchasePlan.PartsSalesCategoryId && r.Status == (int)DcsBaseDataStatus.有效).ToArray();
                var supplierIds = dbPartsPrimarysForPirce.Select(r => r.SupplierId).ToArray();
                var dbPartsPricingsAll = ObjectContext.PartsPurchasePricings.Where(r => sparePartIds.Contains(r.PartId) && r.BranchId == dbpartsPurchasePlan.BranchId && r.PartsSalesCategoryId == dbpartsPurchasePlan.PartsSalesCategoryId && r.Status == (int)DcsPartsPurchasePricingStatus.生效 && supplierIds.Contains(r.PartsSupplierId)).ToArray();

                foreach(var item in dbPartsPrimarysForPirce) {
                    //var dbPartsPricings = ObjectContext.PartsPurchasePricings.Where(r => r.PartId == item.PartId && r.BranchId == dbpartsPurchasePlan.BranchId && r.PartsSalesCategoryId == dbpartsPurchasePlan.PartsSalesCategoryId && r.Status == (int)DcsPartsPurchasePricingStatus.生效 && r.PartsSupplierId == item.SupplierId).ToArray();
                    var dbPartsPricings = dbPartsPricingsAll.Where(r => r.PartId == item.PartId && r.PartsSupplierId == item.SupplierId).ToArray();
                    if(dbPartsPricings.Length == 0)
                        error += "【" + item.SparePart.Code + "】";
                }
                if(!string.IsNullOrEmpty(error)) {
                    throw new ValidationException(string.Format(ErrorStrings.PartsPurchasePlan_Validation13, error));
                }
                var userInfo = Utils.GetCurrentUserInfo();
                partsPurchasePlan.ApproverId = userInfo.Id;
                partsPurchasePlan.ApproverName = userInfo.Name;
                partsPurchasePlan.ApproveTime = DateTime.Now;
                partsPurchasePlan.Status = (int)DcsPurchasePlanOrderStatus.初审通过;
                //if (partsPurchasePlan.FirstApproveTime == null)
                //    partsPurchasePlan.FirstApproveTime = DateTime.Now;
                partsPurchasePlan.ApproveTime = DateTime.Now;
                UpdateToDatabase(partsPurchasePlan);
                this.UpdatePartsPurchasePlanValidate(partsPurchasePlan);
            } else if(partsPurchasePlan.Status.Equals(77)) {
                var userInfo = Utils.GetCurrentUserInfo();
                partsPurchasePlan.ApproverId = userInfo.Id;
                partsPurchasePlan.ApproverName = userInfo.Name;
                partsPurchasePlan.ApproveTime = DateTime.Now;
                partsPurchasePlan.Status = (int)DcsPurchasePlanOrderStatus.新增;
                //if (partsPurchasePlan.FirstApproveTime == null)
                //    partsPurchasePlan.FirstApproveTime = DateTime.Now;
                partsPurchasePlan.ApproveTime = DateTime.Now;
                UpdateToDatabase(partsPurchasePlan);
                this.UpdatePartsPurchasePlanValidate(partsPurchasePlan);
            }
        }

        public void 终审配件采购计划订单(PartsPurchasePlan partsPurchasePlan) {
            var dbpartsPurchasePlan = ObjectContext.PartsPurchasePlans.Where(r => r.Id == partsPurchasePlan.Id && (r.Status == (int)DcsPurchasePlanOrderStatus.审核通过)).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbpartsPurchasePlan == null)
                throw new ValidationException(ErrorStrings.PartsPurchasePlan_Approve_FinalApprove);
            if(partsPurchasePlan.Status.Equals(66)) {
                var dbbranchstrategy = ObjectContext.Branchstrategies.SingleOrDefault(r => r.BranchId == partsPurchasePlan.BranchId && r.Status == (int)DcsBaseDataStatus.有效);
                if(dbbranchstrategy == null)
                    throw new ValidationException(ErrorStrings.SupplierShippingOrder_Validation29);

                //  var dbpartsPurchasePlanDetails = ObjectContext.PartsPurchasePlanDetails.Where(r => r.PurchasePlanId == dbpartsPurchasePlan.Id).SetMergeOption(MergeOption.NoTracking).ToArray();
                var partsPurchasePlanDetails = partsPurchasePlan.PartsPurchasePlanDetails.ToArray();
                var sparePartIds = partsPurchasePlanDetails.Select(v => v.SparePartId);

                //校验计划量
                PartsPurchaseOrderType partsPurchaseOrderType = ObjectContext.PartsPurchaseOrderTypes.FirstOrDefault(o => o.Id == partsPurchasePlan.PartsPlanTypeId);
                //紧急订单不校验计划量
                bool isCheck = partsPurchaseOrderType.Name.IndexOf("急") < 0 && partsPurchaseOrderType.Name != "中心库采购";
                //校验计划量
                var partsSupplierRelations = this.validateForTerminate(partsPurchasePlan.PartsPurchasePlanDetails.ToList(),isCheck);
                //配件不可采购 提示错误
                var dbPartsBrancheIds = ObjectContext.PartsBranches.Where(r => sparePartIds.Contains(r.PartId) && r.IsOrderable == true && r.BranchId == dbpartsPurchasePlan.BranchId && r.PartsSalesCategoryId == dbpartsPurchasePlan.PartsSalesCategoryId && r.Status == (int)DcsBaseDataStatus.有效).Select(v => v.PartId).ToArray();
                var partCodes = partsPurchasePlanDetails.Where(e => !dbPartsBrancheIds.Contains(e.SparePartId)).Select(v => v.SparePartCode).ToArray();
                if(partCodes.Length > 0) {
                    string erro1 = "";
                    for(int i = 0; i < partCodes.Length; i++) {
                        if(string.IsNullOrEmpty(erro1))
                            erro1 = "【" + partCodes[i] + "】";
                        else
                            erro1 += "【" + partCodes[i] + "】";
                    }
                    if(!string.IsNullOrEmpty(erro1))
                        throw new ValidationException(string.Format(ErrorStrings.PartsPurchasePlan_Validation10, erro1));
                }

                ////配件无采购价 提示错误 PartsPurchasePricing
                //var dbPartsPricings = ObjectContext.PartsPurchasePricings.Where(r => sparePartIds.Contains(r.PartId) && r.BranchId == dbpartsPurchasePlan.BranchId && r.PartsSalesCategoryId == dbpartsPurchasePlan.PartsSalesCategoryId && r.Status == (int)DcsPartsPurchasePricingStatus.生效).Select(v => v.PartId).ToArray();
                //var partCodeForPrices = partsPurchasePlanDetails.Where(e => !dbPartsPricings.Contains(e.SparePartId)).Select(v => v.SparePartCode).ToArray();
                //if (partCodeForPrices.Length > 0)
                //{
                //    string erro1 = "";
                //    for (int i = 0; i < partCodeForPrices.Length; i++)
                //    {
                //        if (string.IsNullOrEmpty(erro1))
                //            erro1 = "【" + partCodeForPrices[i] + "】";
                //        else
                //            erro1 += "【" + partCodeForPrices[i] + "】";
                //    }
                //    if (!string.IsNullOrEmpty(erro1))
                //        throw new ValidationException(string.Format("配件{0}不存在采购价", erro1));
                //}

                //配件无首选供应商 提示错误  PartsSupplierRelation  IsPrimary
                //var dbPartsPrimarys = ObjectContext.PartsSupplierRelations.Where(r => sparePartIds.Contains(r.PartId) && r.IsPrimary == true && r.BranchId == dbpartsPurchasePlan.BranchId && r.PartsSalesCategoryId == dbpartsPurchasePlan.PartsSalesCategoryId && r.Status == (int)DcsBaseDataStatus.有效).Select(v => v.PartId).ToArray();
                //var partCodeForPrimarys = partsPurchasePlanDetails.Where(e => !dbPartsPrimarys.Contains(e.SparePartId)).Select(v => v.SparePartCode).ToArray();
                //if(partCodeForPrimarys.Length > 0) {
                //    string erro1 = "";
                //    for(int i = 0; i < partCodeForPrimarys.Length; i++) {
                //        if(string.IsNullOrEmpty(erro1))
                //            erro1 = "【" + partCodeForPrimarys[i] + "】";
                //        else
                //            erro1 += "【" + partCodeForPrimarys[i] + "】";
                //    }
                //    if(!string.IsNullOrEmpty(erro1))
                //        throw new ValidationException(string.Format("配件{0}无首选供应商", erro1));
                //}
                //配件无采购价 提示错误 PartsPurchasePricing
                string error = "";
                // var priceList = ObjectContext.PartsPurchasePricings.Where(r => sparePartIds.Contains(r.PartId) && r.BranchId == dbpartsPurchasePlan.BranchId && r.PartsSalesCategoryId == dbpartsPurchasePlan.PartsSalesCategoryId && r.Status == (int)DcsPartsPurchasePricingStatus.生效).ToArray();
                var dbPartsPrimarysForPirce = ObjectContext.PartsSupplierRelations.Where(r => sparePartIds.Contains(r.PartId) && r.IsPrimary == true && r.BranchId == dbpartsPurchasePlan.BranchId && r.PartsSalesCategoryId == dbpartsPurchasePlan.PartsSalesCategoryId && r.Status == (int)DcsBaseDataStatus.有效);
                var priceList = (from a in ObjectContext.PartsPurchasePricings.Where(r => sparePartIds.Contains(r.PartId) && r.BranchId == dbpartsPurchasePlan.BranchId && r.PartsSalesCategoryId == dbpartsPurchasePlan.PartsSalesCategoryId && r.Status == (int)DcsPartsPurchasePricingStatus.生效)
                                 join b in dbPartsPrimarysForPirce on new {
                                     a.PartId,
                                     SupplierId = a.PartsSupplierId
                                 } equals new {
                                     b.PartId,
                                     b.SupplierId
                                 }
                                 select a).ToArray();
                foreach(var item in partsPurchasePlanDetails) {
                    var dbPartsPricings = priceList.Where(r => r.PartId == item.SparePartId).ToArray();
                    if(dbPartsPricings.Length == 0)
                        error += "【" + item.SparePartCode + "】";
                }
                if(!string.IsNullOrEmpty(error)) {
                    throw new ValidationException(string.Format(ErrorStrings.PartsPurchasePlan_Validation13, error));
                }
                var parameters = partsPurchasePlanDetails.ToDictionary(aa => aa.SparePartId, aa => aa.PlanAmount);
                var userInfo = Utils.GetCurrentUserInfo();
                partsPurchasePlan.CloserId = userInfo.Id;
                partsPurchasePlan.CloserName = userInfo.Name;
                partsPurchasePlan.CloseTime = DateTime.Now;
                partsPurchasePlan.Status = (int)DcsPurchasePlanOrderStatus.终审通过;
                UpdateToDatabase(partsPurchasePlan);
                this.UpdatePartsPurchasePlanValidate(partsPurchasePlan);
                计划生成采购订单(parameters, dbpartsPurchasePlan.WarehouseId, dbpartsPurchasePlan.Id, dbpartsPurchasePlan.Code, DateTime.Now, partsPurchasePlan.PartsPlanTypeId, partsPurchasePlan.Memo, partsSupplierRelations, partsPurchasePlan.IsTransSap, partsPurchasePlan.IsPack);
            } else if(partsPurchasePlan.Status.Equals(77)) {
                var userInfo = Utils.GetCurrentUserInfo();
                partsPurchasePlan.CloserId = userInfo.Id;
                partsPurchasePlan.CloserName = userInfo.Name;
                partsPurchasePlan.CloseTime = DateTime.Now;
                partsPurchasePlan.Status = (int)DcsPurchasePlanOrderStatus.新增;
                UpdateToDatabase(partsPurchasePlan);
                this.UpdatePartsPurchasePlanValidate(partsPurchasePlan);

            }
        }
        public int 计划生成采购订单(Dictionary<int, int> partsIdWithQuantity, int WarehouseId, int PartsPurchaseId, string PartsPurchaseCode, DateTime RequestedDeliveryTime, int? PartsPlanTypeId, string Memo, PartsSupplierRelation[] partsSupplierRelations,bool? IsTransSap,bool? IsPack) {
            try {
                 var userInfo = Utils.GetCurrentUserInfo();
                var partIds = partsIdWithQuantity.Select(r => r.Key).ToArray();
                var outboundWarehouse = this.ObjectContext.Warehouses.SingleOrDefault(r => r.Id == WarehouseId && r.Status == (int)DcsBaseDataStatus.有效);
                if(outboundWarehouse == null) {
                    throw new ValidationException(ErrorStrings.PartsPurchaseOrder_Validation19);
                }
                var parts = this.ObjectContext.SpareParts.Where(r => partIds.Contains(r.Id) && r.Status == (int)DcsMasterDataStatus.有效).ToArray();
                var errorPartIds = partIds.Where(r => parts.All(v => v.Id != r)).ToArray();
                if(errorPartIds.Length > 0) {
                    var errorPartsCode = parts.Where(r => errorPartIds.Contains(r.Id)).Select(r => r.Code).ToArray();
                    string codes = string.Join(",", errorPartsCode);
                    throw new ValidationException(string.Format(ErrorStrings.PartsPurchasePlan_Validation16,codes));
                }
                var arrayOfPartsSalesCategory = (from a in this.ObjectContext.PartsSalesCategories.Where(r => r.Status == (int)DcsBaseDataStatus.有效)
                                                 join b in this.ObjectContext.SalesUnits.Where(r => r.Status == (int)DcsMasterDataStatus.有效) on a.Id equals b.PartsSalesCategoryId
                                                 join c in this.ObjectContext.SalesUnitAffiWarehouses on b.Id equals c.SalesUnitId
                                                 join d in this.ObjectContext.Warehouses.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on c.WarehouseId equals d.Id
                                                 where d.Id == WarehouseId
                                                 select a).ToArray();
                if(arrayOfPartsSalesCategory == null || arrayOfPartsSalesCategory.Length != 1) {
                    throw new ValidationException(ErrorStrings.PartsPurchasePlan_Validation17);
                }
                var partsSalesCategoryOfOutboundWarehouse = arrayOfPartsSalesCategory[0];
              //  var partsBranches = this.ObjectContext.PartsBranches.Where(r => partIds.Contains(r.PartId) && r.PartsSalesCategoryId == partsSalesCategoryOfOutboundWarehouse.Id && r.Status == (int)DcsMasterDataStatus.有效).ToArray();
                //errorPartIds = partIds.Where(r => partsBranches.All(v => v.PartId != r)).ToArray();
                //if(errorPartIds.Length > 0) {
                //    var errorPartsCode = parts.Where(r => errorPartIds.Contains(r.Id)).Select(r => r.Code).ToArray();
                //    string codes = string.Join(",", errorPartsCode);
                //    throw new ValidationException("配件" + codes + "找不到配件营销信息");
                //}
                //errorPartIds = partsBranches.Where(r => !r.IsOrderable).Select(r => r.PartId).ToArray();
                //if(errorPartIds.Length > 0) {
                //    var errorPartsCode = parts.Where(r => errorPartIds.Contains(r.Id)).Select(r => r.Code).ToArray();
                //    string codes = string.Join(",", errorPartsCode);
                //    throw new ValidationException("配件" + codes + "不可采购的无法生成采购订单");
                //}

                //var partsSupplierRelations = this.ObjectContext.PartsSupplierRelations.Where(r => partIds.Contains(r.PartId) && r.Status == (int)DcsBaseDataStatus.有效 && r.PartsSalesCategoryId == partsSalesCategoryOfOutboundWarehouse.Id && r.IsPrimary == true).ToArray();
                //errorPartIds = partIds.Where(r => partsSupplierRelations.All(v => v.PartId != r)).ToArray();
                //if(errorPartIds.Length > 0) {
                //    var errorPartsCode = parts.Where(r => errorPartIds.Contains(r.Id)).Select(r => r.Code).ToArray();
                //    string codes = string.Join(",", errorPartsCode);
                //    throw new ValidationException("配件" + codes + "不存在首选供应商");
                //}
                var partsSuppliersOfPrimary = partsSupplierRelations.Select(r => r.PartsSupplier).Distinct().ToArray();
                var primarySupplierIds = partsSuppliersOfPrimary.Select(r => r.Id).ToArray();
                var branchOfOutboundWarehouse = this.ObjectContext.Branches.SingleOrDefault(r => r.Id == outboundWarehouse.BranchId && r.Status == (int)DcsBaseDataStatus.有效);
                if(branchOfOutboundWarehouse == null) {
                    throw new ValidationException(ErrorStrings.PartsPurchasePlan_Validation18);
                }

                //var branchStrategy = this.ObjectContext.Branchstrategies.SingleOrDefault(r => r.BranchId == outboundWarehouse.BranchId && r.Status == (int)DcsBaseDataStatus.有效);
                //if(branchStrategy == null) {
                //    throw new ValidationException("未找到出库仓库对应的分公司的分公司策略");
                //}

                var salesCenterstrategy = this.ObjectContext.SalesCenterstrategies.SingleOrDefault(r => r.PartsSalesCategoryId == partsSalesCategoryOfOutboundWarehouse.Id && r.Status == (int)DcsBaseDataStatus.有效);
                if(salesCenterstrategy == null)
                    throw new ValidationException(ErrorStrings.PartsPurchasePlan_Validation19);

                var tempPartsPurchasePricings = this.ObjectContext.PartsPurchasePricings.Where(r => r.ValidFrom <= DateTime.Now && r.ValidTo >= DateTime.Now && r.Status != (int)DcsPartsPurchasePricingStatus.作废 && r.PartsSalesCategoryId == partsSalesCategoryOfOutboundWarehouse.Id && partIds.Contains(r.PartId) && primarySupplierIds.Contains(r.PartsSupplierId)).ToArray();
                var partsPurchasePricings = tempPartsPurchasePricings.Where(r => partsSupplierRelations.Any(v => v.PartId == r.PartId && v.SupplierId == r.PartsSupplierId)).ToArray();
                if(salesCenterstrategy.PartsPurchasePricingStrategy == (int)DcsBranchstrategyPartsPurchasePricingStrategy.先定价) {
                    errorPartIds = partIds.Where(r => partsPurchasePricings.All(v => v.PartId != r)).ToArray();
                    if(errorPartIds.Length > 0) {
                        var errorPartsCode = parts.Where(r => errorPartIds.Contains(r.Id)).Select(r => r.Code).ToArray();
                        string codes = string.Join(",", errorPartsCode);
                        throw new ValidationException(string.Format(ErrorStrings.PartsPurchaseOrder_Validation27,codes));
                    }
                }
                var tempStruct = from a in partsIdWithQuantity
                                 join c in partsSupplierRelations on a.Key equals c.PartId
                                 join k in partsPurchasePricings on new {
                                     c.PartId,
                                     PartsSupplierId = c.SupplierId
                                 } equals new {
                                     k.PartId,
                                     k.PartsSupplierId
                                 } into tempTable
                                 from b in tempTable.DefaultIfEmpty() where b.ValidFrom<DateTime.Now && b.ValidTo>DateTime.Now && c.IsPrimary==true
                                 select new {
                                     c.PartId,
                                     PartsSupplierId = c.SupplierId,
                                     PurchasePrice = b == null ? 0 : b.PurchasePrice,
                                     Quantity = a.Value
                                 };
                var tempGroups = tempStruct.GroupBy(r => new {
                    r.PartId,
                    r.PartsSupplierId,
                    r.PurchasePrice
                }).Select(r => new {
                    r.Key.PartId,
                    r.Key.PartsSupplierId,
                    r.Key.PurchasePrice,
                    tempSumQuantiy = r.Sum(v => v.Quantity),
                }).GroupBy(r => new {
                    r.PartsSupplierId
                });
                var PartsPurchaseOrderType = this.ObjectContext.PartsPurchaseOrderTypes.SingleOrDefault(r => r.PartsSalesCategoryId == partsSalesCategoryOfOutboundWarehouse.Id && r.Id == PartsPlanTypeId);
                if(PartsPurchaseOrderType == null) {
                    throw new ValidationException(ErrorStrings.PartsPurchaseOrder_Validation28);
                }
                int i = 0;
                foreach(var itemOfGroup in tempGroups) {
                    i++;
                    var tempPartSupplier = partsSuppliersOfPrimary.SingleOrDefault(r => r.Id == itemOfGroup.Key.PartsSupplierId);
                    if(tempPartSupplier == null) {
                        throw new ValidationException(ErrorStrings.PartsPurchaseOrder_Validation29);
                    }

                    var partsPurchaseOrder = new PartsPurchaseOrder();
                    partsPurchaseOrder.BranchId = branchOfOutboundWarehouse.Id;
                    partsPurchaseOrder.BranchCode = branchOfOutboundWarehouse.Code;
                    partsPurchaseOrder.BranchName = branchOfOutboundWarehouse.Name;
                    partsPurchaseOrder.WarehouseId = outboundWarehouse.Id;
                    partsPurchaseOrder.WarehouseName = outboundWarehouse.Name;
                    partsPurchaseOrder.PartsSalesCategoryId = partsSalesCategoryOfOutboundWarehouse.Id;
                    partsPurchaseOrder.PartsSalesCategoryName = partsSalesCategoryOfOutboundWarehouse.Name;
                    partsPurchaseOrder.PartsSupplierId = tempPartSupplier.Id;
                    partsPurchaseOrder.PartsSupplierCode = tempPartSupplier.Code;
                    partsPurchaseOrder.PartsSupplierName = tempPartSupplier.Name;
                    partsPurchaseOrder.OriginalRequirementBillId = PartsPurchaseId;
                    partsPurchaseOrder.OriginalRequirementBillCode = PartsPurchaseCode;
                    partsPurchaseOrder.PurOrderId = PartsPurchaseId;
                    partsPurchaseOrder.PurOrderCode = PartsPurchaseCode;
                    partsPurchaseOrder.OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.配件销售订单;
                    partsPurchaseOrder.PartsPurchaseOrderTypeId = PartsPurchaseOrderType.Id;
                    partsPurchaseOrder.RequestedDeliveryTime = RequestedDeliveryTime;
                    partsPurchaseOrder.Status = (int)DcsPurchasePlanOrderStatus.新增;
                    partsPurchaseOrder.InStatus = (int)DcsPurchaseInStatus.未入库;
                    partsPurchaseOrder.ReceivingCompanyId = partsPurchaseOrder.BranchId;
                    partsPurchaseOrder.ReceivingCompanyName = partsPurchaseOrder.BranchName;
                    partsPurchaseOrder.IsTransSap = IsTransSap;
                    if(outboundWarehouse.Contact == null && outboundWarehouse.PhoneNumber == null)
                        partsPurchaseOrder.ReceivingAddress = outboundWarehouse.Address;
                    else
                        partsPurchaseOrder.ReceivingAddress = outboundWarehouse.Address + "【" + outboundWarehouse.Contact + "," + outboundWarehouse.PhoneNumber + "】";

                    partsPurchaseOrder.Remark = Memo;//备注
                    partsPurchaseOrder.IsPack = IsPack;
                    int j = 0;
                    decimal totalfee = 0;
                    var partsid=itemOfGroup.Select(t=>t.PartId).Distinct().ToArray();
                     var partsBranches = this.ObjectContext.PartsBranches.Where(r => partsid.Contains(r.PartId)  && r.Status == (int)DcsMasterDataStatus.有效).ToArray();
                    foreach(var item in itemOfGroup) {
                        j++;
                        var tempSparePart = parts.SingleOrDefault(r => r.Id == item.PartId);
                        if(tempSparePart == null) {
                            throw new ValidationException(ErrorStrings.PartsPurchaseOrder_Validation30);
                        }
                        var tempPartsSupplierRelation = partsSupplierRelations.SingleOrDefault(r => r.PartId == tempSparePart.Id && r.SupplierId == tempPartSupplier.Id);
                        if(tempPartsSupplierRelation == null) {
                            throw new ValidationException(ErrorStrings.PartsPurchaseOrder_Validation31);
                        }
                        var branch=partsBranches.Where(r=>r.PartId==item.PartId).FirstOrDefault();
                        var partsPurchaseOrderDetail = new PartsPurchaseOrderDetail();
                        partsPurchaseOrderDetail.SerialNumber = j;
                        partsPurchaseOrderDetail.SparePartId = item.PartId;
                        partsPurchaseOrderDetail.SparePartCode = tempSparePart.Code;
                        partsPurchaseOrderDetail.SparePartName = tempSparePart.Name;
                        partsPurchaseOrderDetail.SupplierPartCode = tempPartsSupplierRelation.SupplierPartCode;
                        partsPurchaseOrderDetail.UnitPrice = item.PurchasePrice;
                        partsPurchaseOrderDetail.OrderAmount = item.tempSumQuantiy;
                        partsPurchaseOrderDetail.MeasureUnit = tempSparePart.MeasureUnit;
                        partsPurchaseOrderDetail.ABCStrategy = Enum.GetName(typeof(DcsABCStrategyCategory), branch.PartABC==null?0:branch.PartABC);
                        totalfee = totalfee + partsPurchaseOrderDetail.OrderAmount * partsPurchaseOrderDetail.UnitPrice;

                        var price = partsPurchasePricings.Where(r => r.PartId == item.PartId && r.PartsSupplierId == item.PartsSupplierId).FirstOrDefault();
                        if (price != null) {
                            partsPurchaseOrderDetail.PriceType = price.PriceType;
                        }
                        InsertToDatabase(partsPurchaseOrderDetail);
                        partsPurchaseOrder.PartsPurchaseOrderDetails.Add(partsPurchaseOrderDetail);
                    }
                    partsPurchaseOrder.TotalAmount = totalfee;//总金额
                    partsPurchaseOrder.TotalAmount = partsPurchaseOrder.PartsPurchaseOrderDetails.Sum(r => r.UnitPrice * r.OrderAmount);
                    ////更新价格类型
                    //foreach(var detail in partsPurchaseOrder.PartsPurchaseOrderDetails) {
                    //    var referencepriceRecord = ObjectContext.PartsPurchasePricings.Where(entitiy => entitiy.Status == 2 && entitiy.PartId == detail.SparePartId).OrderBy(entitiy => entitiy.CreateTime).FirstOrDefault();
                    //    if(referencepriceRecord == null || referencepriceRecord.PurchasePrice == 0)
                    //        continue;
                    //    detail.PurchasePriceBefore = referencepriceRecord.PurchasePrice;
                    //    detail.PriceType = referencepriceRecord.PriceType;//价格类型
                    //}
                    //更新清单变更前价格
                    //var spId = partsPurchaseOrder.PartsPurchaseOrderDetails.Select(r => r.SparePartId).ToArray();
                    //var changeList = new PartsPurchaseOrderDetailAch(this.DomainService).GetPartsPurchaseOrderDetailsByExecuteSql2(partsPurchaseOrder.PartsSalesCategoryId, partsPurchaseOrder.PartsSupplierId, spId, DateTime.Now).ToArray();
                    //foreach(var detail in partsPurchaseOrder.PartsPurchaseOrderDetails) {
                    //    var referencepriceRecord = changeList.Where(r => r.PartId == detail.SparePartId).SingleOrDefault();
                    //    if(referencepriceRecord == null || referencepriceRecord.ReferencePrice == 0)
                    //        continue;
                    //    detail.PurchasePriceBefore = referencepriceRecord.ReferencePrice;
                    //    detail.PriceType = referencepriceRecord.PriceType;//价格类型
                    //}
                    InsertToDatabase(partsPurchaseOrder);
                    this.InsertPartsPurchaseOrderForPlanValidate(partsPurchaseOrder);
                    ObjectContext.SaveChanges();
                    //	当“是否需包材”为是，新增数据【包材采购计划临时表】
                    if(IsPack.HasValue && IsPack.Value){
                        var spIds = partsPurchaseOrder.PartsPurchaseOrderDetails.Select(d => d.SparePartId).Distinct().ToArray();
                        var partsBranchPackingProps = (from a in ObjectContext.PartsBranchPackingProps
                                                      join b in ObjectContext.PartsBranches.Where(t => t.Status == (int)DcsMasterDataStatus.有效 && spIds.Contains(t.PartId)) on a.PartsBranchId equals b.Id
                                                       join c in ObjectContext.SpareParts.Where(t => t.PartType == (int)DcsSparePartPartType.包材) on a.PackingMaterial equals c.Code
                                                      select new {
                                                        a.PackingMaterial,
                                                        c.Id,
                                                        c.Name,
                                                        b.PartId,
                                                        a.PackingCoefficient
                                                      }).ToArray();
                        var spareParts = ObjectContext.SpareParts.Where(t=>spIds.Contains(t.Id)).ToArray();
                        var warehouses = ObjectContext.Warehouses.Where(t => t.StorageCompanyType == (int)DcsCompanyType.分公司 && t.Status == (int)DcsBaseDataStatus.有效).ToArray();
                        foreach(var spItem in spareParts) {
                            var partsBranchPackingProp = partsBranchPackingProps.Where(t=>t.PartId==spItem.Id);
                            foreach(var item in partsBranchPackingProp) {
                                var warehouse = warehouses.Where(t => t.Name == "双桥总库").First();
                                if(partsPurchaseOrder.IfDirectProvision || (!partsPurchaseOrder.IfDirectProvision && partsPurchaseOrder.WarehouseName.Contains("出口") && item.Name.Contains("木箱"))) {
                                    continue;
                                }
                                var PackPlanQty=partsPurchaseOrder.PartsPurchaseOrderDetails.Where(t=>t.SparePartId==spItem.Id).Sum(t=>t.OrderAmount);
                                var PartsPurchasePlanTemp = new PartsPurchasePlanTemp {
                                    PartsPurchaseOrderId = partsPurchaseOrder.Id,
                                    PartsPurchaseOrderCode = partsPurchaseOrder.Code,
                                    WarehouseId=warehouse.Id,
                                    WarehouseName=warehouse.Name,
                                    PackSparePartId=item.Id,
                                    PackSparePartCode = item.PackingMaterial,
                                    PackSparePartName=item.Name,
                                    PackPlanQty = PackPlanQty,
                                    PackNum=item.PackingCoefficient,
                                    OrderAmount =Int32.Parse((Math.Ceiling(Double.Parse(PackPlanQty.ToString()) / Double.Parse(item.PackingCoefficient.ToString()))).ToString()),
                                    SparePartId=spItem.Id,
                                    SparePartCode=spItem.Code,
                                    SparePartName=spItem.Name,
                                    Status = (int)DcsBaseDataStatus.有效,
                                    CreateTime=DateTime.Now,
                                    CreatorId=userInfo.Id,
                                    CreatorName=userInfo.Name
                                };
                                InsertToDatabase(PartsPurchasePlanTemp);
                            }
                        }
                    }
                }
                this.ObjectContext.SaveChanges();
                return i;
            } catch(Exception ex) {

                throw new ValidationException(ex.Message);
            }
        }

        public IQueryable<PartsPurchasePlan> GetPartsPurchasePlanByIds(int[] ids) {
            return ObjectContext.PartsPurchasePlans.Include("PartsPurchaseOrderType").Where(r => ids.Contains(r.Id));
        }

    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:11:30
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 生成配件采购计划订单(PartsPurchasePlan partsPurchasePlan) {
            new PartsPurchasePlanAch(this).生成配件采购计划订单(partsPurchasePlan);
        }
        /// <summary>
        /// 仅供客户端使用
        /// </summary>
        [Update(UsingCustomMethod = true)]
        public void 修改配件采购计划订单(PartsPurchasePlan partsPurchasePlan) {
            new PartsPurchasePlanAch(this).修改配件采购计划订单(partsPurchasePlan);
        }
        //[Update(UsingCustomMethod = true)]
        [Invoke]
        public void 提交配件采购计划订单(int partsPurchaseOrderId) {
            new PartsPurchasePlanAch(this).提交配件采购计划订单(partsPurchaseOrderId);
        }

        [Update(UsingCustomMethod = true)]
        public void 初审配件采购计划订单(PartsPurchasePlan partsPurchasePlan) {
            new PartsPurchasePlanAch(this).初审配件采购计划订单(partsPurchasePlan);
        }

        [Update(UsingCustomMethod = true)]
        public void 审核配件采购计划订单(PartsPurchasePlan partsPurchasePlan) {
            new PartsPurchasePlanAch(this).审核配件采购计划订单(partsPurchasePlan);
        }

        [Update(UsingCustomMethod = true)]
        public void 终审配件采购计划订单(PartsPurchasePlan partsPurchasePlan) {
            new PartsPurchasePlanAch(this).终审配件采购计划订单(partsPurchasePlan);
        }

        [Update(UsingCustomMethod = true)]
        public void 作废配件采购计划订单(PartsPurchasePlan partsPurchasePlan) {
            new PartsPurchasePlanAch(this).作废配件采购计划订单(partsPurchasePlan);
        }

        [Update(UsingCustomMethod = true)]
        public void 驳回配件采购计划订单(PartsPurchasePlan partsPurchasePlan) {
            new PartsPurchasePlanAch(this).驳回配件采购计划订单(partsPurchasePlan);
        }
        [Invoke(HasSideEffects = true)]
        //保持原有的public方法，NoChangePublic
        public int 计划生成采购订单(Dictionary<int, int> partsIdWithQuantity, int WarehouseId, int PartsPurchaseId, string PartsPurchaseCode, DateTime RequestedDeliveryTime) {
            try {
                var partIds = partsIdWithQuantity.Select(r => r.Key).ToArray();
                var outboundWarehouse = this.ObjectContext.Warehouses.SingleOrDefault(r => r.Id == WarehouseId && r.Status == (int)DcsBaseDataStatus.有效);
                if(outboundWarehouse == null) {
                    throw new ValidationException("出库仓库异常,此仓库已经被作废");
                }
                var parts = this.ObjectContext.SpareParts.Where(r => partIds.Contains(r.Id) && r.Status == (int)DcsMasterDataStatus.有效);
                var errorPartIds = partIds.Where(r => parts.All(v => v.Id != r)).ToArray();
                if(errorPartIds.Length > 0) {
                    var errorPartsCode = parts.Where(r => errorPartIds.Contains(r.Id)).Select(r => r.Code).ToArray();
                    string codes = string.Join(",", errorPartsCode);
                    throw new ValidationException("配件" + codes + "找不到配件营销信息");
                }
                var arrayOfPartsSalesCategory = (from a in this.ObjectContext.PartsSalesCategories.Where(r => r.Status == (int)DcsBaseDataStatus.有效)
                                                 join b in this.ObjectContext.SalesUnits.Where(r => r.Status == (int)DcsMasterDataStatus.有效) on a.Id equals b.PartsSalesCategoryId
                                                 join c in this.ObjectContext.SalesUnitAffiWarehouses on b.Id equals c.SalesUnitId
                                                 join d in this.ObjectContext.Warehouses.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on c.WarehouseId equals d.Id
                                                 where d.Id == WarehouseId
                                                 select a).ToArray();
                if(arrayOfPartsSalesCategory == null || arrayOfPartsSalesCategory.Length != 1) {
                    throw new ValidationException("根据出库仓库获取品牌出现异常");
                }
                var partsSalesCategoryOfOutboundWarehouse = arrayOfPartsSalesCategory[0];
                var partsBranches = this.ObjectContext.PartsBranches.Where(r => partIds.Contains(r.PartId) && r.PartsSalesCategoryId == partsSalesCategoryOfOutboundWarehouse.Id).ToArray();
                errorPartIds = partIds.Where(r => partsBranches.All(v => v.PartId != r)).ToArray();
                if(errorPartIds.Length > 0) {
                    var errorPartsCode = parts.Where(r => errorPartIds.Contains(r.Id)).Select(r => r.Code).ToArray();
                    string codes = string.Join(",", errorPartsCode);
                    throw new ValidationException("配件" + codes + "找不到配件营销信息");
                }
                errorPartIds = partsBranches.Where(r => !r.IsOrderable).Select(r => r.PartId).ToArray();
                if(errorPartIds.Length > 0) {
                    var errorPartsCode = parts.Where(r => errorPartIds.Contains(r.Id)).Select(r => r.Code).ToArray();
                    string codes = string.Join(",", errorPartsCode);
                    throw new ValidationException("配件" + codes + "不可采购的无法生成采购订单");
                }

                var partsSupplierRelations = this.ObjectContext.PartsSupplierRelations.Where(r => partIds.Contains(r.PartId) && r.Status == (int)DcsBaseDataStatus.有效 && r.PartsSalesCategoryId == partsSalesCategoryOfOutboundWarehouse.Id && r.IsPrimary == true).ToArray();
                errorPartIds = partIds.Where(r => partsSupplierRelations.All(v => v.PartId != r)).ToArray();
                if(errorPartIds.Length > 0) {
                    var errorPartsCode = parts.Where(r => errorPartIds.Contains(r.Id)).Select(r => r.Code).ToArray();
                    string codes = string.Join(",", errorPartsCode);
                    throw new ValidationException("配件" + codes + "不存在首选供应商");
                }
                var partsSuppliersOfPrimary = partsSupplierRelations.Select(r => r.PartsSupplier).Distinct().ToArray();
                var primarySupplierIds = partsSuppliersOfPrimary.Select(r => r.Id).ToArray();
                var branchOfOutboundWarehouse = this.ObjectContext.Branches.SingleOrDefault(r => r.Id == outboundWarehouse.BranchId && r.Status == (int)DcsBaseDataStatus.有效);
                if(branchOfOutboundWarehouse == null) {
                    throw new ValidationException("未找到出库仓库对应的分公司");
                }

                //var branchStrategy = this.ObjectContext.Branchstrategies.SingleOrDefault(r => r.BranchId == outboundWarehouse.BranchId && r.Status == (int)DcsBaseDataStatus.有效);
                //if(branchStrategy == null) {
                //    throw new ValidationException("未找到出库仓库对应的分公司的分公司策略");
                //}

                var salesCenterstrategy = this.ObjectContext.SalesCenterstrategies.SingleOrDefault(r => r.PartsSalesCategoryId == partsSalesCategoryOfOutboundWarehouse.Id && r.Status == (int)DcsBaseDataStatus.有效);
                if(salesCenterstrategy == null)
                    throw new ValidationException("未找到出库仓库对应的销售中心策略");

                var tempPartsPurchasePricings = this.ObjectContext.PartsPurchasePricings.Where(r => r.ValidFrom <= DateTime.Now && r.ValidTo >= DateTime.Now && r.Status != (int)DcsPartsPurchasePricingStatus.作废 && r.PartsSalesCategoryId == partsSalesCategoryOfOutboundWarehouse.Id && partIds.Contains(r.PartId) && primarySupplierIds.Contains(r.PartsSupplierId)).ToArray();
                var partsPurchasePricings = tempPartsPurchasePricings.Where(r => partsSupplierRelations.Any(v => v.PartId == r.PartId && v.SupplierId == r.PartsSupplierId)).ToArray();
                if(salesCenterstrategy.PartsPurchasePricingStrategy == (int)DcsBranchstrategyPartsPurchasePricingStrategy.先定价) {
                    errorPartIds = partIds.Where(r => partsPurchasePricings.All(v => v.PartId != r)).ToArray();
                    if(errorPartIds.Length > 0) {
                        var errorPartsCode = parts.Where(r => errorPartIds.Contains(r.Id)).Select(r => r.Code).ToArray();
                        string codes = string.Join(",", errorPartsCode);
                        throw new ValidationException("配件" + codes + "不存在配件采购价(销售中心策略为先定价)");
                    }
                }
                var tempStruct = from a in partsIdWithQuantity
                                 join c in partsSupplierRelations on a.Key equals c.PartId
                                 join k in partsPurchasePricings on new {
                                     c.PartId,
                                     PartsSupplierId = c.SupplierId
                                 } equals new {
                                     k.PartId,
                                     k.PartsSupplierId
                                 } into tempTable
                                 from b in tempTable.DefaultIfEmpty()
                                 select new {
                                     c.PartId,
                                     PartsSupplierId = c.SupplierId,
                                     PurchasePrice = b == null ? 0 : b.PurchasePrice,
                                     Quantity = a.Value,

                                 };
                var tempGroups = tempStruct.GroupBy(r => new {
                    r.PartId,
                    r.PartsSupplierId,
                    r.PurchasePrice,
                }).Select(r => new {
                    r.Key.PartId,
                    r.Key.PartsSupplierId,
                    r.Key.PurchasePrice,
                    tempSumQuantiy = r.Sum(v => v.Quantity),
                }).GroupBy(r => new {
                    r.PartsSupplierId
                });
                var PartsPurchaseOrderType = this.ObjectContext.PartsPurchaseOrderTypes.SingleOrDefault(r => r.PartsSalesCategoryId == partsSalesCategoryOfOutboundWarehouse.Id && r.Name == "急需采购订单");
                if(PartsPurchaseOrderType == null) {
                    throw new ValidationException("获取配件采购订单类型时出现异常");
                }
                int i = 0;
                foreach(var itemOfGroup in tempGroups) {
                    i++;
                    var tempPartSupplier = partsSuppliersOfPrimary.SingleOrDefault(r => r.Id == itemOfGroup.Key.PartsSupplierId);
                    if(tempPartSupplier == null) {
                        throw new ValidationException("生成采购订单时获取供应商出现异常");
                    }

                    var partsPurchasePlan = new PartsPurchasePlan();
                    partsPurchasePlan.BranchId = branchOfOutboundWarehouse.Id;
                    partsPurchasePlan.BranchCode = branchOfOutboundWarehouse.Code;
                    partsPurchasePlan.BranchName = branchOfOutboundWarehouse.Name;
                    partsPurchasePlan.WarehouseId = outboundWarehouse.Id;
                    partsPurchasePlan.WarehouseName = outboundWarehouse.Name;
                    partsPurchasePlan.PartsSalesCategoryId = partsSalesCategoryOfOutboundWarehouse.Id;
                    partsPurchasePlan.PartsSalesCategoryName = partsSalesCategoryOfOutboundWarehouse.Name;
                    //partsPurchasePlan.PartsSupplierId = tempPartSupplier.Id;
                    //partsPurchasePlan.PartsSupplierCode = tempPartSupplier.Code;
                    //partsPurchasePlan.PartsSupplierName = tempPartSupplier.Name;
                    //partsPurchasePlan.OriginalRequirementBillId = partsSalesOrderId;
                    //partsPurchasePlan.OriginalRequirementBillCode = partsSalesOrderCode;
                    //partsPurchasePlan.OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.配件销售订单;
                    //partsPurchasePlan.PartsPurchaseOrderTypeId = PartsPurchaseOrderType.Id;
                    //partsPurchasePlan.RequestedDeliveryTime = RequestedDeliveryTime;
                    partsPurchasePlan.Status = (int)DcsPurchasePlanOrderStatus.新增;
                    //partsPurchasePlan.InStatus = (int)DcsPurchaseInStatus.未入库;
                    int j = 0;
                    foreach(var item in itemOfGroup) {
                        j++;
                        var tempSparePart = parts.SingleOrDefault(r => r.Id == item.PartId);
                        if(tempSparePart == null) {
                            throw new ValidationException("生成采购清单时获取配件出现异常");
                        }
                        var tempPartsSupplierRelation = partsSupplierRelations.SingleOrDefault(r => r.PartId == tempSparePart.Id && r.SupplierId == tempPartSupplier.Id);
                        if(tempPartsSupplierRelation == null) {
                            throw new ValidationException("生成采购清单时获取配件与供应商关系出现异常");
                        }
                        var partsPurchasePlanDetail = new PartsPurchasePlanDetail();
                        partsPurchasePlanDetail.SerialNumber = j;
                        partsPurchasePlanDetail.SparePartId = item.PartId;
                        partsPurchasePlanDetail.SparePartCode = tempSparePart.Code;
                        partsPurchasePlanDetail.SparePartName = tempSparePart.Name;
                        partsPurchasePlanDetail.SupplierPartCode = tempPartsSupplierRelation.SupplierPartCode;
                        //partsPurchasePlanDetail.UnitPrice = item.PurchasePrice;
                        //partsPurchasePlanDetail.OrderAmount = item.tempSumQuantiy;
                        partsPurchasePlanDetail.MeasureUnit = tempSparePart.MeasureUnit;
                        InsertToDatabase(partsPurchasePlanDetail);
                        partsPurchasePlan.PartsPurchasePlanDetails.Add(partsPurchasePlanDetail);
                    }
                    // partsPurchasePlan.TotalAmount = partsPurchasePlan.PartsPurchasePlanDetails.Sum(r => r.UnitPrice * r.OrderAmount);
                    ////更新清单变更前价格
                    //foreach (var detail in partsPurchasePlan.PartsPurchasePlanDetails)
                    //{
                    //    var referencepriceRecord = new PartsPurchasePlanDetailAch(this).GetPartsPurchaseOrderDetailsByExecuteSql(partsPurchasePlan.PartsSalesCategoryId, partsPurchasePlan.PartsSupplierId, detail.SparePartId, DateTime.Now);
                    //    if (referencepriceRecord == null || referencepriceRecord.ReferencePrice == 0)
                    //        continue;
                    //    detail.PurchasePriceBefore = referencepriceRecord.ReferencePrice;
                    //    detail.PriceType = referencepriceRecord.PriceType;//价格类型
                    //}
                    InsertToDatabase(partsPurchasePlan);
                    new PartsPurchasePlanAch(this).InsertPartsPurchasePlanValidate(partsPurchasePlan);
                }
                this.ObjectContext.SaveChanges();
                return i;
            } catch(Exception ex) {

                throw new ValidationException(ex.Message);
            }
        }

        public IQueryable<PartsPurchasePlan> GetPartsPurchasePlanByIds(int[] ids) {
            return new PartsPurchasePlanAch(this).GetPartsPurchasePlanByIds(ids);
        }
    }
}
