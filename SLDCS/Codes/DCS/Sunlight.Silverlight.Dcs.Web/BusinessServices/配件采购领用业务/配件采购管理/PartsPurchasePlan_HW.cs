﻿using System;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsPurchasePlan_HWAch : DcsSerivceAchieveBase {
        public PartsPurchasePlan_HWAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 数据校验(PartsPurchasePlan_HW partsPurchasePlan_HW) {
            if(partsPurchasePlan_HW.PartsPurchasePlanDetail_HWs == null)
                return;
            var partsPurchasePlan_HWStatus = DcsPurchasePlanStatus.可分解;
            decimal? totalAmount = 0;
            var partsSalesCategoryHW = ObjectContext.PartsSalesCategories.FirstOrDefault(i => i.Name == "海外轻卡" && i.Status == (int)DcsBaseDataStatus.有效);
            var partsSalesCategoryId = partsSalesCategoryHW == null ? 0 : partsSalesCategoryHW.Id;
            //var suplierCodes = partsPurchasePlan_HW.PartsPurchasePlanDetail_HWs.Select(r => r.SuplierCode).ToList();
            //var branchSupplierRelations = ObjectContext.BranchSupplierRelations.Where(i => suplierCodes.Contains(i.BusinessCode) && i.PartsSalesCategoryId == partsSalesCategoryId && i.Status == (int)DcsBaseDataStatus.有效).ToList();
            //var supplierIds = branchSupplierRelations.Select(r => r.SupplierId).ToList();
            //var partsSuppliers = ObjectContext.PartsSuppliers.Where(i => supplierIds.Contains(i.Id) && i.Status == (int)DcsBaseDataStatus.有效).ToList();
            var overseasSuplierCodes = partsPurchasePlan_HW.PartsPurchasePlanDetail_HWs.Select(r => r.OverseasSuplierCode).ToList();
            var branchSupplierRelationOverseas = ObjectContext.BranchSupplierRelations.Where(i => overseasSuplierCodes.Contains(i.BusinessCode) && i.PartsSalesCategoryId == partsSalesCategoryId && i.Status == (int)DcsBaseDataStatus.有效).ToList();
            var supplierIds = branchSupplierRelationOverseas.Select(r => r.SupplierId).ToList();
            var partsSuppliers = ObjectContext.PartsSuppliers.Where(i => supplierIds.Contains(i.Id) && i.Status == (int)DcsBaseDataStatus.有效).ToList();
           
            var partCodes = partsPurchasePlan_HW.PartsPurchasePlanDetail_HWs.Select(r => r.PartCode).ToList();
            var partIds = partsPurchasePlan_HW.PartsPurchasePlanDetail_HWs.Select(r => r.PartId).ToList();
            var _spareParts = ObjectContext.SpareParts.Where(r => partCodes.Contains(r.Code) && partIds.Contains(r.Id) && r.Status == (int)DcsBaseDataStatus.有效).ToList();

            var partsSupplierRelations = ObjectContext.PartsSupplierRelations.Where(r => partIds.Contains(r.PartId) && supplierIds.Contains(r.SupplierId) && r.PartsSalesCategoryId == partsPurchasePlan_HW.PartsSalesCategoryId && r.Status == (int)DcsBaseDataStatus.有效).ToList();

            var partsPurchasePricings = ObjectContext.PartsPurchasePricings.Where(r => partIds.Contains(r.PartId) && r.PartsSalesCategoryId == partsPurchasePlan_HW.PartsSalesCategoryId && supplierIds.Contains(r.PartsSupplierId) && r.Status == (int)DcsPartsPurchasePricingStatus.生效 && r.ValidFrom < DateTime.Now && r.ValidTo > DateTime.Now).ToList();
            var partsBranchs = ObjectContext.PartsBranches.Where(r => partIds.Contains(r.PartId) && r.PartsSalesCategoryId == partsPurchasePlan_HW.PartsSalesCategoryId && r.IsOrderable && r.Status == (int)DcsBaseDataStatus.有效).ToList();

            var partsPlannedPrices = ObjectContext.PartsPlannedPrices.Where(r => r.PartsSalesCategoryId == partsSalesCategoryId && partIds.Contains(r.SparePartId));

            foreach(var partsPurchasePlanDetail_HW in partsPurchasePlan_HW.PartsPurchasePlanDetail_HWs) {
                if((partsPurchasePlanDetail_HW.UntFulfilledQty - (partsPurchasePlanDetail_HW.EndAmount ?? 0))>0) {
                    string errorStr = "";
                    if(string.IsNullOrEmpty(partsPurchasePlanDetail_HW.SuplierCode))
                        partsPurchasePlanDetail_HW.SuplierCode = partsPurchasePlanDetail_HW.OverseasSuplierCode;
                    var BranchSupplierRelation = branchSupplierRelationOverseas.Where(i => i.BusinessCode == partsPurchasePlanDetail_HW.SuplierCode && i.PartsSalesCategoryId == partsSalesCategoryId && i.Status == (int)DcsBaseDataStatus.有效).FirstOrDefault();
                    if(BranchSupplierRelation != null) {
                        var PartsSupplier = partsSuppliers.Where(i => i.Id == BranchSupplierRelation.SupplierId && i.Status == (int)DcsBaseDataStatus.有效).FirstOrDefault();
                        if(PartsSupplier != null) {
                            partsPurchasePlanDetail_HW.PartsSupplierCode = PartsSupplier.Code;
                            partsPurchasePlanDetail_HW.PartsSupplierId = PartsSupplier.Id;
                            partsPurchasePlanDetail_HW.PartsSupplierName = PartsSupplier.Name;
                        }
                    }
                    var branchSupplierRelationOversea = branchSupplierRelationOverseas.Where(i => i.BusinessCode == partsPurchasePlanDetail_HW.OverseasSuplierCode && i.PartsSalesCategoryId == partsSalesCategoryId && i.Status == (int)DcsBaseDataStatus.有效).FirstOrDefault();
                    var _sparePart = _spareParts.Where(r => r.Code == partsPurchasePlanDetail_HW.PartCode && r.Id == partsPurchasePlanDetail_HW.PartId && r.Status == (int)DcsBaseDataStatus.有效).SingleOrDefault();
                    if(_sparePart == null)
                        errorStr += "配件图号不存在;";
                    if(branchSupplierRelationOversea == null) {
                        errorStr += "分公司与供应商关不存在;";
                    } else {
                        var partsSupplierRelation = partsSupplierRelations.Where(r => r.PartId == partsPurchasePlanDetail_HW.PartId && r.SupplierId == branchSupplierRelationOversea.SupplierId && r.PartsSalesCategoryId == partsPurchasePlan_HW.PartsSalesCategoryId && r.Status == (int)DcsBaseDataStatus.有效).FirstOrDefault();
                        if(partsSupplierRelation == null)
                            errorStr += "配件与供应商关系不存在;";
                        var partsPlannedPrice = partsPlannedPrices.Where(r => r.SparePartId == partsPurchasePlanDetail_HW.PartId).FirstOrDefault();
                        if(partsPlannedPrice == null)
                            errorStr += "配件不存在计划价;";
                        var partsSupplierId = branchSupplierRelationOversea.SupplierId;
                        var partsPurchasePricing = partsPurchasePricings.Where(r => r.PartId == partsPurchasePlanDetail_HW.PartId && r.PartsSalesCategoryId == partsPurchasePlan_HW.PartsSalesCategoryId && r.PartsSupplierId == partsSupplierId && r.Status == (int)DcsPartsPurchasePricingStatus.生效 && r.ValidFrom < DateTime.Now && r.ValidTo > DateTime.Now).FirstOrDefault();
                        if(partsPurchasePricing == null)
                            errorStr += "配件不存在采购价;";
                        else {
                            partsPurchasePlanDetail_HW.UnitPrice = partsPurchasePricing.PurchasePrice;
                            totalAmount += partsPurchasePlanDetail_HW.UnitPrice * partsPurchasePlanDetail_HW.PartsPurchasePlanQty;
                        }
                        var partsBranch = partsBranchs.Where(r => r.PartId == partsPurchasePlanDetail_HW.PartId && r.PartsSalesCategoryId == partsPurchasePlan_HW.PartsSalesCategoryId && r.IsOrderable && r.Status == (int)DcsBaseDataStatus.有效).SingleOrDefault();
                        if(partsBranch == null)
                            errorStr += "配件不可采购;";
                    }
                    partsPurchasePlanDetail_HW.FaultReason = errorStr;
                    if(!string.IsNullOrEmpty(partsPurchasePlanDetail_HW.FaultReason)) {
                        partsPurchasePlan_HWStatus = DcsPurchasePlanStatus.数据异常;
                    }
                    UpdateToDatabase(partsPurchasePlanDetail_HW);
                }
            }
            var userInfo = Utils.GetCurrentUserInfo();
            partsPurchasePlan_HW.TotalAmount = totalAmount;
            partsPurchasePlan_HW.SAPPurchasePlanCode = partsPurchasePlan_HW.Aboardcode;
            partsPurchasePlan_HW.ModifierId = userInfo.Id;
            partsPurchasePlan_HW.ModifierName = userInfo.Name;
            partsPurchasePlan_HW.ModifyTime = DateTime.Now;
            partsPurchasePlan_HW.Status = (int)partsPurchasePlan_HWStatus;
            UpdateToDatabase(partsPurchasePlan_HW);
        }


        public void 申请终止采购计划(PartsPurchasePlan_HW partsPurchasePlan_HW, string StopReason) {
            partsPurchasePlan_HW.StopReason = StopReason;
            var userInfo = Utils.GetCurrentUserInfo();
            partsPurchasePlan_HW.StoporId = userInfo.Id;
            partsPurchasePlan_HW.StoporName = userInfo.Name;
            partsPurchasePlan_HW.StopTime = DateTime.Now;
            partsPurchasePlan_HW.Status = (int)DcsPurchasePlanStatus.申请终止;
            UpdateToDatabase(partsPurchasePlan_HW);
        }


        public void 更新采购计划状态(PartsPurchasePlan_HW partsPurchasePlan_HW, bool OperationFromPurOrder) {
            partsPurchasePlan_HW = ObjectContext.PartsPurchasePlan_HW.Where(i => i.Id == partsPurchasePlan_HW.Id).FirstOrDefault();
            if(partsPurchasePlan_HW == null)
                return;
            foreach(PartsPurchasePlanDetail_HW i in partsPurchasePlan_HW.PartsPurchasePlanDetail_HWs) {
                i.TransferQty = 0;
                i.PurchaseQty = 0;
                i.TransferConfimQty = 0;
                i.PurchaseConfimQty = 0;
                i.UntFulfilledQty = i.PartsPurchasePlanQty;
            }
            var partsTransferOrders = ObjectContext.PartsTransferOrders.Where(i => i.PurOrderCode == partsPurchasePlan_HW.Code && i.Status != (int)DcsBaseDataStatus.作废).ToList();
            foreach(var partsTransferOrder in partsTransferOrders) {
                foreach(var partsPurchasePlanDetail_HW in partsPurchasePlan_HW.PartsPurchasePlanDetail_HWs) {
                    var detail = partsTransferOrder.PartsTransferOrderDetails.Where(i => i.SparePartId == partsPurchasePlanDetail_HW.PartId).SingleOrDefault();
                    if(detail == null)
                        continue;
                    partsPurchasePlanDetail_HW.TransferQty += detail.PlannedAmount;
                    partsPurchasePlanDetail_HW.TransferConfimQty += (detail.ConfirmedAmount ?? 0);
                    partsPurchasePlanDetail_HW.UntFulfilledQty -= detail.PlannedAmount;
                    if(detail.ConfirmedAmount > 0) {
                        partsPurchasePlanDetail_HW.UntFulfilledQty += (detail.PlannedAmount - detail.ConfirmedAmount);
                    }
                    UpdateToDatabase(partsPurchasePlanDetail_HW);
                }
            }
            var partsPurchaseOrders = ObjectContext.PartsPurchaseOrders.Where(i => i.PurOrderCode == partsPurchasePlan_HW.Code && i.Status != (int)DcsBaseDataStatus.作废).ToList();
            foreach(var partsPurchaseOrder in partsPurchaseOrders) {
                foreach(var partsPurchasePlanDetail_HW in partsPurchasePlan_HW.PartsPurchasePlanDetail_HWs) {
                    var detail = partsPurchaseOrder.PartsPurchaseOrderDetails.Where(i => i.SparePartId == partsPurchasePlanDetail_HW.PartId).SingleOrDefault();
                    if(detail == null)
                        continue;
                    partsPurchasePlanDetail_HW.PurchaseQty += detail.OrderAmount;
                    partsPurchasePlanDetail_HW.PurchaseConfimQty += detail.ConfirmedAmount;
                    partsPurchasePlanDetail_HW.UntFulfilledQty -= detail.OrderAmount;
                    if(detail.ConfirmedAmount > 0) {
                        partsPurchasePlanDetail_HW.UntFulfilledQty += (detail.OrderAmount - detail.ConfirmedAmount);
                    }
                    UpdateToDatabase(partsPurchasePlanDetail_HW);
                }
            }
            ////////////////会造成未满足数量不准确，导致分解单据时，生成采购订单或者调拨单的最大数量不一致的问题
            //var partsInboundCheckBills = ObjectContext.PartsInboundCheckBills.Where(i => i.PurOrderCode == partsPurchasePlan_HW.Code && i.Status != (int)DcsBaseDataStatus.作废).ToList();
            //foreach(var partsInboundCheckBill in partsInboundCheckBills) {
            //    var partsInboundPlan = ObjectContext.PartsInboundPlans.Where(i => i.Id == partsInboundCheckBill.PartsInboundPlanId).FirstOrDefault();
            //    if(partsInboundPlan == null)
            //        continue;
            //    foreach(var partsPurchasePlanDetail_HW in partsPurchasePlan_HW.PartsPurchasePlanDetail_HWs) {
            //        var detail = partsInboundCheckBill.PartsInboundCheckBillDetails.Where(i => i.SparePartId == partsPurchasePlanDetail_HW.PartId).SingleOrDefault();
            //        var detailPIP = partsInboundPlan.PartsInboundPlanDetails.Where(i => i.SparePartId == partsPurchasePlanDetail_HW.PartId).SingleOrDefault();
            //        if(detail == null || detailPIP == null || detail.InspectedQuantity < detailPIP.PlannedAmount)
            //            continue;
            //        partsPurchasePlanDetail_HW.UntFulfilledQty += (detailPIP.PlannedAmount - detail.InspectedQuantity);
            //        UpdateToDatabase(partsPurchasePlanDetail_HW);
            //    }
            //}
            var Status = DcsPurchasePlanStatus.分解完成;
            if(partsPurchasePlan_HW.PartsPurchasePlanDetail_HWs.Any(i => !string.IsNullOrEmpty(i.FaultReason)))
                Status = DcsPurchasePlanStatus.数据异常;
            else
                foreach(var partsPurchasePlanDetail_HW in partsPurchasePlan_HW.PartsPurchasePlanDetail_HWs) {
                    if(partsPurchasePlanDetail_HW.PartsPurchasePlanQty > partsPurchasePlanDetail_HW.TransferQty + partsPurchasePlanDetail_HW.PurchaseQty) {
                        Status = DcsPurchasePlanStatus.部分分解;
                        break;
                    }
                }
            partsPurchasePlan_HW.Status = (int)Status;
            if(OperationFromPurOrder) {
                var userInfo = Utils.GetCurrentUserInfo();
                partsPurchasePlan_HW.ModifierId = userInfo.Id;
                partsPurchasePlan_HW.ModifierName = userInfo.Name;
                partsPurchasePlan_HW.ModifyTime = DateTime.Now;
            }
            UpdateToDatabase(partsPurchasePlan_HW);
        }

    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:11:30
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 数据校验(PartsPurchasePlan_HW partsPurchasePlan_HW) {
            new PartsPurchasePlan_HWAch(this).数据校验(partsPurchasePlan_HW);
        }


        [Update(UsingCustomMethod = true)]
        public void 申请终止采购计划(PartsPurchasePlan_HW partsPurchasePlan_HW, string StopReason) {
            new PartsPurchasePlan_HWAch(this).申请终止采购计划(partsPurchasePlan_HW, StopReason);
        }


        [Update(UsingCustomMethod = true)]
        public void 更新采购计划状态(PartsPurchasePlan_HW partsPurchasePlan_HW, bool OperationFromPurOrder = false) {
            new PartsPurchasePlan_HWAch(this).更新采购计划状态(partsPurchasePlan_HW, OperationFromPurOrder);
        }
    }
}