﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class StorehouseCensusAch : DcsSerivceAchieveBase {
        public StorehouseCensusAch(DcsDomainService domainService)
            : base(domainService) {
        }
        public IEnumerable<StorehouseCensus> 库龄报表统计(string sparePartCode, string sparePartName, string referenceCode) {
            string SQL = @"select pp.sparepartcode,
       Rownum,
       pp.referencecode,
       pp.sparepartname,
       pp.PartABC,
       pp.CostAmount,
       pp.costprice,
       pp.quantity,
       pp.QuantityOne,
       QuantityOne * pp.costprice as QuantityOneAmount,
       pp.QuantityTwo,
       QuantityTwo * pp.costprice as QuantityTwoAmount,
       pp.QuantityThree,
       QuantityThree * pp.costprice as QuantityThreeAmount,
       pp.QuantityFour,
       QuantityFour * pp.costprice as QuantityFourAmount,
       pp.QuantityFive,
       QuantityFive * pp.costprice as QuantityFiveAmount,
      ( case when (pp.quantity - (QuantityOne + QuantityTwo + QuantityThree +
       QuantityFour + QuantityFive) <0) then 0 else pp.quantity - (QuantityOne + QuantityTwo + QuantityThree +
       QuantityFour + QuantityFive) end )as QuantityMore,
        ( case when (pp.quantity - (QuantityOne + QuantityTwo + QuantityThree +
       QuantityFour + QuantityFive) <0) then 0 else (pp.quantity - (QuantityOne + QuantityTwo + QuantityThree +
       QuantityFour + QuantityFive)) * pp.costprice end )as QuantityMoreAmount,
       pp.sparepartid,
       pp.StorageCompanyId
  from (select tt.quantity,
              (case 
                 when tt.quantity < QuantityOne  then
                  tt.quantity
                 else
                  QuantityOne
               end) as QuantityOne,
               (case
                 when tt.quantity >= QuantityOne + QuantityTwo and
                      (QuantityThree > 0 or QuantityFour > 0 or
                      QuantityFive > 0 or QuantityMore > 0) then
                  QuantityTwo
                 when tt.quantity <= tt.QuantityOne or
                      (QuantityTwo = 0 and QuantityThree = 0 and
                      QuantityFour = 0 and QuantityFive = 0 and
                      QuantityMore = 0) then
                  0
                 when tt.QuantityOne < tt.quantity and
                      (tt.QuantityOne + tt.QuantityTwo > tt.quantity or
                      (QuantityThree = 0 and QuantityFour = 0 and
                      QuantityFive = 0 and QuantityMore = 0)) then
                  tt.quantity - tt.QuantityOne
               end) as QuantityTwo,
               
               (case
                 when (tt.quantity >=
                      QuantityOne + QuantityTwo + QuantityThree) and
                      (QuantityFour > 0 or QuantityFive > 0 or
                      QuantityMore > 0) then
                  QuantityThree
                 when tt.QuantityOne + tt.QuantityTwo >= tt.quantity or
                      (QuantityThree = 0 and QuantityFour = 0 and
                      QuantityFive = 0 and QuantityMore = 0) then
                  0
                 when tt.QuantityOne + tt.QuantityTwo < tt.quantity and
                      (tt.QuantityOne + tt.QuantityTwo + tt.QuantityThree >
                      tt.quantity or (QuantityFour = 0 and QuantityFive = 0 and
                      QuantityMore = 0)) then
                  tt.quantity - (tt.QuantityOne + tt.QuantityTwo)
               end) as QuantityThree,
               (case
                 when (tt.quantity >= QuantityOne + QuantityTwo +
                      QuantityThree + QuantityFour) and
                      (QuantityFive > 0 or QuantityMore > 0) then
                  QuantityFour
                 when QuantityOne + QuantityTwo + QuantityThree >=
                      tt.quantity or (QuantityFour = 0 and QuantityFive = 0 and
                      QuantityMore = 0) then
                  0
                 when QuantityOne + QuantityTwo + QuantityThree < tt.quantity and
                      (QuantityOne + QuantityTwo + QuantityThree +
                      QuantityFour > tt.quantity or
                      (QuantityFive = 0 and QuantityMore = 0)) then
                  tt.quantity - (QuantityOne + QuantityTwo + QuantityThree)
               end) as QuantityFour,
               
               (case
                 when (tt.quantity >=
                      QuantityOne + QuantityTwo + QuantityThree +
                      QuantityFour + QuantityFive) and QuantityMore > 0 then
                  QuantityFive
                 when QuantityOne + QuantityTwo + QuantityThree +
                      QuantityFour >= tt.quantity or
                      (QuantityFive = 0 and QuantityMore = 0) then
                  0
                 when QuantityOne + QuantityTwo + QuantityThree +
                      QuantityFour < tt.quantity and
                      (QuantityOne + QuantityTwo + QuantityThree +
                      QuantityFour + QuantityFive > tt.quantity or
                      (QuantityMore = 0)) then
                  tt.quantity -
                  (QuantityOne + QuantityTwo + QuantityThree + QuantityFour)
               end) as QuantityFive,
               tt.costprice,
               tt.CostAmount,
               tt.referencecode,
               tt.PartABC,
               tt.sparepartcode,
               tt.sparepartname,
               tt.sparepartid,
               tt.StorageCompanyId,
               EnterprisePartsCostId
          from (select ss.quantity,
                       nvl(sum(QuantityOne),0) as QuantityOne,
                       nvl(sum(QuantityTwo),0) as QuantityTwo,
                       nvl(sum(QuantityThree),0) as QuantityThree,
                       nvl(sum(QuantityFour),0) as QuantityFour,
                       nvl(sum(QuantityFive),0) as QuantityFive,
                      nvl( sum(QuantityMore),0) as QuantityMore,                    
                       ss.costprice,
                       ss.CostAmount,
                       ss.referencecode,
                       ss.PartABC,
                       ss.sparepartcode,
                       ss.sparepartname,
                       ss.sparepartid,
                       ss.StorageCompanyId,
                       EnterprisePartsCostId
                  from (select tt.QuantityOne,
                               tt.QuantityTwo,
                               tt.QuantityThree,
                               tt.QuantityFour,
                               tt.QuantityFive,
                               tt.QuantityMore,
                               ps.sparepartid,
                               ps.costprice,
                               ps.quantity,
                               (ps.costprice * ps.quantity) as CostAmount,
                                ps.OwnerCompanyId as StorageCompanyId,
                               sp.referencecode,
                               sp.code as sparepartcode,
                               sp.name as sparepartname,
                               pb.PartABC as PartABC,
                               ps.id as EnterprisePartsCostId
                          from EnterprisePartsCost ps
                          left join (select ww.sparepartid,
                                           ww.StorageCompanyId,
                                           ww.partssalescategoryid,
                                           sum(ww.QuantityOne) as QuantityOne,
                                           sum(ww.QuantityTwo) as QuantityTwo,
                                           sum(ww.QuantityThree) as QuantityThree,
                                           sum(ww.QuantityFour) as QuantityFour,
                                           sum(ww.QuantityFive) as QuantityFive,
                                           sum(ww.QuantityMore) as QuantityMore
                                      from (select pd.sparepartid,
                                                   pc.StorageCompanyId,
                                                   pc.partssalescategoryid,
                                                   (case
                                                     when (sysdate -
                                                          pc.createtime <= 90) then
                                                      inspectedquantity
                                                     else
                                                      0
                                                   end) as QuantityOne,
                                                   (case
                                                     when (90 < sysdate -
                                                          pc.createtime and
                                                          sysdate -
                                                          pc.createtime <= 180) then
                                                      inspectedquantity
                                                     else
                                                      0
                                                   end) as QuantityTwo,
                                                   (case
                                                     when (180 < sysdate -
                                                          pc.createtime and
                                                          sysdate -
                                                          pc.createtime <= 360) then
                                                      inspectedquantity
                                                     else
                                                      0
                                                   end) as QuantityThree,
                                                   (case
                                                     when (360 < sysdate -
                                                          pc.createtime and
                                                          sysdate -
                                                          pc.createtime <= 720) then
                                                      inspectedquantity
                                                     else
                                                      0
                                                   end) as QuantityFour,
                                                   (case
                                                     when (720 < sysdate -
                                                          pc.createtime and
                                                          sysdate -
                                                          pc.createtime <= 1080) then
                                                      inspectedquantity
                                                     else
                                                      0
                                                   end) as QuantityFive,
                                                   (case
                                                     when (sysdate -
                                                          pc.createtime > 1080) then
                                                      inspectedquantity
                                                     else
                                                      0
                                                   end) as QuantityMore
                                              from PartsInboundCheckBillDetail pd
                                              left join PartsInboundCheckBill pc
                                                on pd.partsinboundcheckbillid =
                                                   pc.id
                                             where (pc.InboundType = 1 or
                                                   pc.InboundType = 2)
                                               and pc.StorageCompanyType = 1) ww
                                     group by ww.sparepartid,
                                              ww.StorageCompanyId,
                                              ww.partssalescategoryid) tt
                            on ps.sparepartid = tt.sparepartid
                           and ps.ownercompanyid = tt.storagecompanyid
                           and ps.partssalescategoryid =
                               tt.partssalescategoryid
                          left join company cy
                            on ps.ownercompanyid = cy.id
                          left join sparepart sp
                            on ps.sparepartid = sp.id
                           and sp.status = 1
                          left join partsbranch pb
                            on ps.sparepartid = pb.partid
                           and pb.status = 1                           
                         where cy.type = 1 ";
            if(!string.IsNullOrEmpty(sparePartCode))
            {
                SQL = SQL + " and LOWER(sp.code) like '%" + sparePartCode.ToLower() + "%'";
            }
            if(!string.IsNullOrEmpty(sparePartName)) {
                SQL = SQL + " and LOWER(sp.name) like '%" + sparePartName.ToLower() + "%'";
            }
            if(!string.IsNullOrEmpty(referenceCode)) {
                SQL = SQL + " and LOWER(sp.referenceCode) like '%" + referenceCode.ToLower() + "%'";
            }
            SQL = SQL + ") ss group by ss.sparepartid,  ss.costprice,ss.quantity, ss.CostAmount,ss.StorageCompanyId, ss.referencecode, ss.PartABC,ss.sparepartcode,ss.sparepartname,ss.EnterprisePartsCostId) tt) pp ";
            var search = ObjectContext.ExecuteStoreQuery<StorehouseCensus>(SQL).ToList();
            return search;
        }
        public IEnumerable<StorehouseCensus> 中心库库龄报表统计(string sparePartCode, string sparePartName, string referenceCode,string centerName) {
            string SQL = @"select pp.sparepartcode,
       pp.referencecode,
       pp.sparepartname,
       pp.PartABC,
       pp.CostAmount,
       pp.costprice,
       pp.quantity,
       pp.QuantityOne,
       QuantityOne * pp.costprice as QuantityOneAmount,
       pp.QuantityTwo,
       QuantityTwo * pp.costprice as QuantityTwoAmount,
       pp.QuantityThree,
       QuantityThree * pp.costprice as QuantityThreeAmount,
       pp.QuantityFour,
       QuantityFour * pp.costprice as QuantityFourAmount,
       pp.QuantityFive,
       QuantityFive * pp.costprice as QuantityFiveAmount,
       (case
         when (pp.quantity - (QuantityOne + QuantityTwo + QuantityThree +
              QuantityFour + QuantityFive) < 0) then
          0
         else
          pp.quantity - (QuantityOne + QuantityTwo + QuantityThree +
          QuantityFour + QuantityFive)
       end) as QuantityMore,
       (case
         when (pp.quantity - (QuantityOne + QuantityTwo + QuantityThree +
              QuantityFour + QuantityFive) < 0) then
          0
         else
          (pp.quantity - (QuantityOne + QuantityTwo + QuantityThree +
          QuantityFour + QuantityFive)) * pp.costprice
       end) as QuantityMoreAmount,
       pp.sparepartid,
       pp.StorageCompanyId,
       code as centerName,
       name as centerCode,
         Rownum 
  from (select tt.quantity,
               (case
                 when tt.quantity < QuantityOne then
                  tt.quantity
                 else
                  QuantityOne
               end) as QuantityOne,
               (case
                 when tt.quantity >= QuantityOne + QuantityTwo and
                      (QuantityThree > 0 or QuantityFour > 0 or
                      QuantityFive > 0 or QuantityMore > 0) then
                  QuantityTwo
                 when tt.quantity <= tt.QuantityOne or
                      (QuantityTwo = 0 and QuantityThree = 0 and
                      QuantityFour = 0 and QuantityFive = 0 and
                      QuantityMore = 0) then
                  0
                 when tt.QuantityOne < tt.quantity and
                      (tt.QuantityOne + tt.QuantityTwo > tt.quantity or
                      (QuantityThree = 0 and QuantityFour = 0 and
                      QuantityFive = 0 and QuantityMore = 0)) then
                  tt.quantity - tt.QuantityOne
               end) as QuantityTwo,
               
               (case
                 when (tt.quantity >=
                      QuantityOne + QuantityTwo + QuantityThree) and
                      (QuantityFour > 0 or QuantityFive > 0 or
                      QuantityMore > 0) then
                  QuantityThree
                 when tt.QuantityOne + tt.QuantityTwo >= tt.quantity or
                      (QuantityThree = 0 and QuantityFour = 0 and
                      QuantityFive = 0 and QuantityMore = 0) then
                  0
                 when tt.QuantityOne + tt.QuantityTwo < tt.quantity and
                      (tt.QuantityOne + tt.QuantityTwo + tt.QuantityThree >
                      tt.quantity or (QuantityFour = 0 and QuantityFive = 0 and
                      QuantityMore = 0)) then
                  tt.quantity - (tt.QuantityOne + tt.QuantityTwo)
               end) as QuantityThree,
               (case
                 when (tt.quantity >= QuantityOne + QuantityTwo +
                      QuantityThree + QuantityFour) and
                      (QuantityFive > 0 or QuantityMore > 0) then
                  QuantityFour
                 when QuantityOne + QuantityTwo + QuantityThree >=
                      tt.quantity or (QuantityFour = 0 and QuantityFive = 0 and
                      QuantityMore = 0) then
                  0
                 when QuantityOne + QuantityTwo + QuantityThree < tt.quantity and
                      (QuantityOne + QuantityTwo + QuantityThree +
                      QuantityFour > tt.quantity or
                      (QuantityFive = 0 and QuantityMore = 0)) then
                  tt.quantity - (QuantityOne + QuantityTwo + QuantityThree)
               end) as QuantityFour,
               
               (case
                 when (tt.quantity >=
                      QuantityOne + QuantityTwo + QuantityThree +
                      QuantityFour + QuantityFive) and QuantityMore > 0 then
                  QuantityFive
                 when QuantityOne + QuantityTwo + QuantityThree +
                      QuantityFour >= tt.quantity or
                      (QuantityFive = 0 and QuantityMore = 0) then
                  0
                 when QuantityOne + QuantityTwo + QuantityThree +
                      QuantityFour < tt.quantity and
                      (QuantityOne + QuantityTwo + QuantityThree +
                      QuantityFour + QuantityFive > tt.quantity or
                      (QuantityMore = 0)) then
                  tt.quantity -
                  (QuantityOne + QuantityTwo + QuantityThree + QuantityFour)
               end) as QuantityFive,
               tt.costprice,
               tt.CostAmount,
               tt.referencecode,
               tt.PartABC,
               tt.sparepartcode,
               tt.sparepartname,
               tt.sparepartid,
               tt.StorageCompanyId,
               EnterprisePartsCostId,
               tt.code,
               tt.name
          from (select ss.quantity,
                       nvl(sum(QuantityOne), 0) as QuantityOne,
                       nvl(sum(QuantityTwo), 0) as QuantityTwo,
                       nvl(sum(QuantityThree), 0) as QuantityThree,
                       nvl(sum(QuantityFour), 0) as QuantityFour,
                       nvl(sum(QuantityFive), 0) as QuantityFive,
                       nvl(sum(QuantityMore), 0) as QuantityMore,
                       ss.costprice,
                       ss.CostAmount,
                       ss.referencecode,
                       ss.PartABC,
                       ss.sparepartcode,
                       ss.sparepartname,
                       ss.sparepartid,
                       ss.StorageCompanyId,
                       EnterprisePartsCostId,
                       ss.code,
                       ss.name
                  from (select tt.QuantityOne,
                               tt.QuantityTwo,
                               tt.QuantityThree,
                               tt.QuantityFour,
                               tt.QuantityFive,
                               tt.QuantityMore,
                               ps.SparePartId,
                               round( ps.costprice/1.13,2) as costprice,
                               ps.quantity,
                               (round( ps.costprice/1.13,2) * ps.quantity) as CostAmount,
                               ps.OwnerCompanyId as StorageCompanyId,
                               sp.referencecode,
                               sp.code as sparepartcode,
                               sp.name as sparepartname,
                               pb.PartABC as PartABC,
                               ps.id as EnterprisePartsCostId,
                               cy.name,
                               cy.code
                          from EnterprisePartsCost ps
                          left join (select ww.sparepartid,
                                           ww.StorageCompanyId,
                                           ww.partssalescategoryid,
                                           sum(ww.QuantityOne) as QuantityOne,
                                           sum(ww.QuantityTwo) as QuantityTwo,
                                           sum(ww.QuantityThree) as QuantityThree,
                                           sum(ww.QuantityFour) as QuantityFour,
                                           sum(ww.QuantityFive) as QuantityFive,
                                           sum(ww.QuantityMore) as QuantityMore
                                      from (select pd.sparepartid,
                                                   pc.StorageCompanyId,
                                                   pc.partssalescategoryid,
                                                   (case
                                                     when (sysdate -
                                                          pc.createtime <= 90) then
                                                      inspectedquantity
                                                     else
                                                      0
                                                   end) as QuantityOne,
                                                   (case
                                                     when (90 < sysdate -
                                                          pc.createtime and
                                                          sysdate -
                                                          pc.createtime <= 180) then
                                                      inspectedquantity
                                                     else
                                                      0
                                                   end) as QuantityTwo,
                                                   (case
                                                     when (180 < sysdate -
                                                          pc.createtime and
                                                          sysdate -
                                                          pc.createtime <= 360) then
                                                      inspectedquantity
                                                     else
                                                      0
                                                   end) as QuantityThree,
                                                   (case
                                                     when (360 < sysdate -
                                                          pc.createtime and
                                                          sysdate -
                                                          pc.createtime <= 720) then
                                                      inspectedquantity
                                                     else
                                                      0
                                                   end) as QuantityFour,
                                                   (case
                                                     when (720 < sysdate -
                                                          pc.createtime and
                                                          sysdate -
                                                          pc.createtime <= 1080) then
                                                      inspectedquantity
                                                     else
                                                      0
                                                   end) as QuantityFive,
                                                   (case
                                                     when (sysdate -
                                                          pc.createtime > 1080) then
                                                      inspectedquantity
                                                     else
                                                      0
                                                   end) as QuantityMore
                                              from PartsInboundCheckBillDetail pd
                                              left join PartsInboundCheckBill pc
                                                on pd.partsinboundcheckbillid =
                                                   pc.id
                                             where (pc.InboundType = 1 or
                                                   pc.InboundType = 2)
                                               and pc.StorageCompanyType = 3) ww
                                     group by ww.sparepartid,
                                              ww.StorageCompanyId,
                                              ww.partssalescategoryid) tt
                            on ps.sparepartid = tt.sparepartid
                           and ps.ownercompanyid = tt.storagecompanyid
                           and ps.partssalescategoryid =
                               tt.partssalescategoryid
                          left join company cy
                            on ps.ownercompanyid = cy.id
                          left join sparepart sp
                            on ps.sparepartid = sp.id
                           and sp.status = 1
                          left join partsbranch pb
                            on ps.sparepartid = pb.partid
                           and pb.status = 1
                         where cy.type = 3 ";
            if(!string.IsNullOrEmpty(sparePartCode)) {
                SQL = SQL + " and LOWER(sp.code) like '%" + sparePartCode.ToLower() + "%'";
            }
            if(!string.IsNullOrEmpty(sparePartName)) {
                SQL = SQL + " and LOWER(sp.name) like '%" + sparePartName.ToLower() + "%'";
            }
            if(!string.IsNullOrEmpty(referenceCode)) {
                SQL = SQL + " and LOWER(sp.referenceCode) like '%" + referenceCode.ToLower() + "%'";
            }
            if(!string.IsNullOrEmpty(centerName)){
                SQL = SQL + " and  LOWER(cy.name） like '%" + centerName.ToLower() + "%'";
            }
            var userInfo = Utils.GetCurrentUserInfo();
            var company = ObjectContext.Companies.Where(r => r.Id == userInfo.EnterpriseId).FirstOrDefault();
            if(null==company){
                throw new ValidationException(ErrorStrings.User_Invalid);
            }
            if(company.Type==(int)DcsCompanyType.代理库){
                SQL = SQL + " and  cy.id =" +userInfo.EnterpriseId;
            }
            if(company.Type != (int)DcsCompanyType.代理库 && company.Type != (int)DcsCompanyType.分公司) {
                return null;
            }
            SQL = SQL + ") ss group by ss.sparepartid,  ss.costprice,ss.quantity, ss.CostAmount,ss.StorageCompanyId, ss.referencecode, ss.PartABC,ss.sparepartcode,ss.sparepartname,ss.EnterprisePartsCostId，ss.code,ss.name) tt) pp ";
            var search = ObjectContext.ExecuteStoreQuery<StorehouseCensus>(SQL).ToList();
            return search;
        }
    }
    partial class DcsDomainService {
        public IEnumerable<StorehouseCensus> 库龄报表统计(string sparePartCode, string sparePartName, string referenceCode) {
            return new StorehouseCensusAch(this).库龄报表统计(sparePartCode, sparePartName, referenceCode);
        }
        public IEnumerable<StorehouseCensus> 中心库库龄报表统计(string sparePartCode, string sparePartName, string referenceCode, string centerName) {
        return new StorehouseCensusAch(this).中心库库龄报表统计(sparePartCode, sparePartName, referenceCode,centerName);
        }        
    }
}
