﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class SupplierPlanArrearAch : DcsSerivceAchieveBase {
        public SupplierPlanArrearAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 作废供应商计划欠款(SupplierPlanArrear supplierPlanArrear) {
            var dbSupplierPreApprovedLoan = ObjectContext.SupplierPlanArrears.Where(r => r.Id == supplierPlanArrear.Id && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbSupplierPreApprovedLoan == null) {
                throw new ValidationException("只能作废状态有效的单据");
            }
            supplierPlanArrear.Status = (int)DcsBaseDataStatus.作废;
            var userInfo = Utils.GetCurrentUserInfo();
            supplierPlanArrear.ModifierId = userInfo.Id;
            supplierPlanArrear.ModifierName = userInfo.Name;
            supplierPlanArrear.ModifyTime = DateTime.Now;
            UpdateToDatabase(supplierPlanArrear);
        }

        public void 新增供应商计划欠款(SupplierPlanArrear supplierPlanArrear) {
            var dbSupplierPlanArrear = ObjectContext.SupplierPlanArrears.Where(r => r.PartsSalesCategoryId == supplierPlanArrear.PartsSalesCategoryId && r.SupplierId == supplierPlanArrear.SupplierId && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbSupplierPlanArrear != null) {
                throw new ValidationException("该供应商已存在计划欠款信息，无法保存");
            }
            var userInfo = Utils.GetCurrentUserInfo();
            supplierPlanArrear.CreatorId = userInfo.Id;
            supplierPlanArrear.CreatorName = userInfo.Name;
            supplierPlanArrear.CreateTime = DateTime.Now;
        }
        public void 修改供应商计划欠款(SupplierPlanArrear supplierPlanArrear) {
            var dbSupplierPlanArrear = ObjectContext.SupplierPlanArrears.Where(r => r.Id != supplierPlanArrear.Id && r.PartsSalesCategoryId == supplierPlanArrear.PartsSalesCategoryId && r.SupplierId == supplierPlanArrear.SupplierId && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbSupplierPlanArrear != null) {
                throw new ValidationException("该供应商已存在计划欠款信息，无法保存");
            }
            var dbSupplierPlanArrear1 = ObjectContext.SupplierPlanArrears.Where(r => r.Id == supplierPlanArrear.Id && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbSupplierPlanArrear1 == null) {
                throw new ValidationException("只能操作非作废的单据");
            }
            var userInfo = Utils.GetCurrentUserInfo();
            supplierPlanArrear.ModifierId = userInfo.Id;
            supplierPlanArrear.ModifierName = userInfo.Name;
            supplierPlanArrear.ModifyTime = DateTime.Now;
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:11:30
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 作废供应商计划欠款(SupplierPlanArrear supplierPlanArrear) {
            new SupplierPlanArrearAch(this).作废供应商计划欠款(supplierPlanArrear);
        }
        
        public void 新增供应商计划欠款(SupplierPlanArrear supplierPlanArrear) {
            new SupplierPlanArrearAch(this).新增供应商计划欠款(supplierPlanArrear);
        }

        public void 修改供应商计划欠款(SupplierPlanArrear supplierPlanArrear) {
            new SupplierPlanArrearAch(this).修改供应商计划欠款(supplierPlanArrear);
        }
    }
}
