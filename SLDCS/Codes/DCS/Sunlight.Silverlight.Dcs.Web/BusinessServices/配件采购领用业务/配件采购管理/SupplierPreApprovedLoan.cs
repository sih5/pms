﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class SupplierPreApprovedLoanAch : DcsSerivceAchieveBase {
        public SupplierPreApprovedLoanAch(DcsDomainService domainService)
            : base(domainService) {
        }
        

        public void 作废供应商货款预批单(SupplierPreApprovedLoan supplierPreApprovedLoan)
        {
            var dbsupplierPreApprovedLoan = ObjectContext.SupplierPreApprovedLoans.Where(r => r.Id == supplierPreApprovedLoan.Id && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if (dbsupplierPreApprovedLoan == null)
            {
                throw new ValidationException("只能操作非作废的单据");
            }
            var userInfo = Utils.GetCurrentUserInfo();
            supplierPreApprovedLoan.Status = (int)DcsBaseDataStatus.作废;
            supplierPreApprovedLoan.ModifierId = userInfo.Id;
            supplierPreApprovedLoan.ModifierName = userInfo.Name;
            supplierPreApprovedLoan.ModifyTime = DateTime.Now;
        }

        public void 修改供应商货款预批单(SupplierPreApprovedLoan supplierPreApprovedLoan) {
            var supplierPreAppLoanDetails = supplierPreApprovedLoan.SupplierPreAppLoanDetails;
            var tempGroup = supplierPreAppLoanDetails.GroupBy(r => r.SupplierId);
            if(tempGroup.Any(r => r.Count() > 1)) {
                throw new ValidationException("存在供应商重复的清单");
            }
            var dbsupplierPreApprovedLoan = ObjectContext.SupplierPreApprovedLoans.Where(r => r.Id == supplierPreApprovedLoan.Id && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbsupplierPreApprovedLoan == null) {
                throw new ValidationException("只能操作非作废的单据");
            }
            var userInfo = Utils.GetCurrentUserInfo();
            supplierPreApprovedLoan.ModifierId = userInfo.Id;
            supplierPreApprovedLoan.ModifierName = userInfo.Name;
            supplierPreApprovedLoan.ModifyTime = DateTime.Now;
        }

        public void 新增供应商货款预批单(SupplierPreApprovedLoan supplierPreApprovedLoan) {
            var supplierPreAppLoanDetails = supplierPreApprovedLoan.SupplierPreAppLoanDetails;
            var tempGroup = supplierPreAppLoanDetails.GroupBy(r => r.SupplierId);
            if(tempGroup.Any(r => r.Count() > 1)) {
                throw new ValidationException("存在供应商重复的清单");
            }
            var userInfo = Utils.GetCurrentUserInfo();
            supplierPreApprovedLoan.CreatorId = userInfo.Id;
            supplierPreApprovedLoan.CreatorName = userInfo.Name;
            supplierPreApprovedLoan.CreateTime = DateTime.Now;
            var branchCode = (from a in ObjectContext.Branches.Where(r => r.Status == (int)DcsBaseDataStatus.有效)
                              from b in ObjectContext.PartsSalesCategories.Where(r => r.Status == (int)DcsBaseDataStatus.有效)
                              where a.Id == b.BranchId && b.Id == supplierPreApprovedLoan.PartsSalesCategoryId
                              select a.Code).SingleOrDefault();
            if(branchCode == null) {
                throw new ValidationException("未找到对应分公司编号");
            }
            if(string.IsNullOrWhiteSpace(supplierPreApprovedLoan.PreApprovedCode) || supplierPreApprovedLoan.PreApprovedCode == GlobalVar.ASSIGNED_BY_SERVER) {
                supplierPreApprovedLoan.PreApprovedCode = CodeGenerator.Generate("SupplierPreApprovedLoan", branchCode);
            }
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:11:30
        //原分布类的函数全部转移到Ach                                                               
        
        [Update(UsingCustomMethod = true)]
        public void 作废供应商货款预批单(SupplierPreApprovedLoan supplierPreApprovedLoan)
        {
            new SupplierPreApprovedLoanAch(this).作废供应商货款预批单(supplierPreApprovedLoan);
        }
        
        [Update(UsingCustomMethod = true)]
        public void 修改供应商货款预批单(SupplierPreApprovedLoan supplierPreApprovedLoan) {
            new SupplierPreApprovedLoanAch(this).修改供应商货款预批单(supplierPreApprovedLoan);
        }
        
        [Update(UsingCustomMethod = true)]
        public void 新增供应商货款预批单(SupplierPreApprovedLoan supplierPreApprovedLoan) {
            new SupplierPreApprovedLoanAch(this).新增供应商货款预批单(supplierPreApprovedLoan);
        }
    }
}
