﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
using System.Collections.Generic;
using Sunlight.Silverlight.Dcs.Web.Entities;
using System.Transactions;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class SupplierShippingOrderAch : DcsSerivceAchieveBase {
        public SupplierShippingOrderAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 生成供应商发运单(SupplierShippingOrder supplierShippingOrder) {
              using(var transaction = new TransactionScope()) {
            //int i = 0;
             var partsPurchaseOrder = ObjectContext.PartsPurchaseOrders.Include("PartsPurchaseOrderType").Where(r => r.Id == supplierShippingOrder.PartsPurchaseOrderId).Include("PartsPurchaseOrderDetails").SingleOrDefault();
            if(partsPurchaseOrder == null)
                throw new ValidationException(string.Format(ErrorStrings.SupplierShippingOrder_Validation6, supplierShippingOrder.Code));
            var dbbranchstrategy = ObjectContext.Branchstrategies.SingleOrDefault(r => r.BranchId == partsPurchaseOrder.BranchId && r.Status == (int)DcsBaseDataStatus.有效);
            if(dbbranchstrategy == null)
                throw new ValidationException(ErrorStrings.Branchstrategy_Validation4);
            if(partsPurchaseOrder.Status != (int)DcsPartsPurchaseOrderStatus.确认完毕 && partsPurchaseOrder.Status != (int)DcsPartsPurchaseOrderStatus.部分发运 && partsPurchaseOrder.Status != (int)DcsPartsPurchaseOrderStatus.部分确认)
                throw new ValidationException(string.Format(ErrorStrings.SupplierShippingOrder_Validation1));
            var supplierShippingDetails = supplierShippingOrder.SupplierShippingDetails;
            var partsPurchaseOrderDetails = partsPurchaseOrder.PartsPurchaseOrderDetails.ToArray();
            var spids = supplierShippingDetails.Select(r => r.SparePartId).ToArray();
            var sparets = ObjectContext.SpareParts.Where(r => spids.Contains(r.Id)).ToArray();
            foreach(var supplierShippingDetail in supplierShippingDetails) {
                var dbpartsPurchaseOrderDetail = partsPurchaseOrderDetails.FirstOrDefault(r => r.SparePartId == supplierShippingDetail.SparePartId);
                if(dbpartsPurchaseOrderDetail == null)
                    throw new ValidationException(string.Format(ErrorStrings.SupplierShippingOrder_Validation7, partsPurchaseOrder.Code, supplierShippingDetail.SparePartCode));
                if(dbpartsPurchaseOrderDetail.ShippingAmount == null)
                    dbpartsPurchaseOrderDetail.ShippingAmount = 0;
                if(dbpartsPurchaseOrderDetail.ConfirmedAmount < supplierShippingDetail.Quantity + dbpartsPurchaseOrderDetail.ShippingAmount.Value)
                    throw new ValidationException(string.Format(ErrorStrings.SupplierShippingOrder_Validation4, dbpartsPurchaseOrderDetail.SparePartCode));
                dbpartsPurchaseOrderDetail.ShippingAmount += supplierShippingDetail.Quantity;
                var sp = sparets.Where(r => r.Id == supplierShippingDetail.SparePartId).FirstOrDefault();
                supplierShippingDetail.Volume = sp.Volume * supplierShippingDetail.Quantity;
                supplierShippingDetail.Weight = sp.Weight * supplierShippingDetail.Quantity;
            }
            supplierShippingOrder.TotalVolume = supplierShippingDetails.Sum(r=>r.Volume);
            supplierShippingOrder.TotalWeight = supplierShippingDetails.Sum(r => r.Weight);
            partsPurchaseOrder.Status = partsPurchaseOrderDetails.Any(r => r.ConfirmedAmount != r.ShippingAmount) ? (int)DcsPartsPurchaseOrderStatus.部分发运 : (int)DcsPartsPurchaseOrderStatus.发运完毕;
            new PartsPurchaseOrderAch(this.DomainService).UpdatePartsPurchaseOrderValidate(partsPurchaseOrder);
            var dbWarehouse = ObjectContext.Warehouses.FirstOrDefault(r => r.Id == supplierShippingOrder.ReceivingWarehouseId);
            if(dbWarehouse == null)
                throw new ValidationException(ErrorStrings.PartsShippingOrder_Validation16);
            //if (dbWarehouse.WmsInterface || ((dbWarehouse.Name == "海外乘用车山东CDC仓库" || dbWarehouse.Name == "海外轻卡山东CDC仓库" || dbWarehouse.Name == "海外中重卡山东CDC仓库")
            //    && (supplierShippingOrder.PartsSalesCategoryName == "海外乘用车" || supplierShippingOrder.PartsSalesCategoryName == "海外轻卡" || supplierShippingOrder.PartsSalesCategoryName == "海外中重卡")))
            //{
               supplierShippingOrder.ComfirmStatus = (int)DCSSupplierShippingOrderComfirmStatus.新建;
               if(partsPurchaseOrder.PartsPurchaseOrderType.Name != "正常订单") {
                   if(partsPurchaseOrder.ShippingMethod.HasValue) {
                       supplierShippingOrder.ShippingMethod = partsPurchaseOrder.ShippingMethod.Value;
                   } else
                       supplierShippingOrder.ShippingMethod = 0;
               }
            if(supplierShippingOrder.IfDirectProvision) {
                supplierShippingOrder.Status = (int)DcsSupplierShippingOrderStatus.新建;
                supplierShippingOrder.OriginalRequirementBillId = partsPurchaseOrder.OriginalRequirementBillId.Value;
                supplierShippingOrder.OriginalRequirementBillCode = partsPurchaseOrder.OriginalRequirementBillCode;
                supplierShippingOrder.OriginalRequirementBillType = partsPurchaseOrder.OriginalRequirementBillType.Value;
                ObjectContext.SaveChanges();
                //新建追溯码临时表
                //var oldAccurateTrace = ObjectContext.AccurateTraces.Where(t => t.Status == (int)DcsBaseDataStatus.有效).ToList();
                //var oldTraceTempType = ObjectContext.TraceTempTypes.Where(t => t.Status == (int)DcsBaseDataStatus.有效).ToList();
                //foreach(var detail in supplierShippingDetails) {
                //    var traceCode=detail.TraceCode.Split(';').ToList();
                //    foreach(var code in traceCode){
                //        if(oldAccurateTrace.Any(t => t.TraceCode == code) || oldTraceTempType.Any(t=>t.TraceCode==code)) {
                //            throw new ValidationException("历史数据已存在追溯码："+code);
                //        } else {
                //            var newTraceTempType = new TraceTempType {
                //                Type=(int)DCSTraceTempType.直供,
                //                SourceBillId = supplierShippingOrder.Id,
                //                SourceBillDetailId = detail.Id,
                //                PartId=detail.SparePartId,
                //                TraceCode=code,
                //                Status=(int)DcsBaseDataStatus.有效,
                //                TraceProperty = detail.TraceProperty
                //            };
                //            new TraceTempTypeAch(this.DomainService).ValidataraceTempType(newTraceTempType);
                //        }
                //    }
                //}
            } else {
                supplierShippingOrder.Status = (int)DcsSupplierShippingOrderStatus.收货确认;
                var newPartsInboundPlan = new PartsInboundPlan();
                newPartsInboundPlan.WarehouseId = supplierShippingOrder.ReceivingWarehouseId;
                newPartsInboundPlan.WarehouseCode = dbWarehouse.Code;
                newPartsInboundPlan.WarehouseName = supplierShippingOrder.ReceivingWarehouseName;
                newPartsInboundPlan.StorageCompanyId = supplierShippingOrder.BranchId;
                newPartsInboundPlan.StorageCompanyCode = supplierShippingOrder.BranchCode;
                newPartsInboundPlan.StorageCompanyName = supplierShippingOrder.BranchName;
                newPartsInboundPlan.StorageCompanyType = (int)DcsCompanyType.分公司;
                newPartsInboundPlan.BranchId = supplierShippingOrder.BranchId;
                newPartsInboundPlan.BranchCode = supplierShippingOrder.BranchCode;
                newPartsInboundPlan.BranchName = supplierShippingOrder.BranchName;
                newPartsInboundPlan.PartsSalesCategoryId = supplierShippingOrder.PartsSalesCategoryId;
                newPartsInboundPlan.CounterpartCompanyId = supplierShippingOrder.PartsSupplierId;
                newPartsInboundPlan.CounterpartCompanyCode = supplierShippingOrder.PartsSupplierCode;
                newPartsInboundPlan.CounterpartCompanyName = supplierShippingOrder.PartsSupplierName;
                newPartsInboundPlan.SourceId = supplierShippingOrder.Id;
                newPartsInboundPlan.SourceCode = supplierShippingOrder.Code;
                newPartsInboundPlan.InboundType = (int)DcsPartsInboundType.配件采购;
                newPartsInboundPlan.OriginalRequirementBillId = supplierShippingOrder.PartsPurchaseOrderId;
                newPartsInboundPlan.OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.配件采购订单;
                newPartsInboundPlan.OriginalRequirementBillCode = supplierShippingOrder.PartsPurchaseOrderCode;
                newPartsInboundPlan.Status = (int)DcsPartsInboundPlanStatus.新建;
                newPartsInboundPlan.IfWmsInterface = dbWarehouse.WmsInterface;
                newPartsInboundPlan.ArrivalDate = supplierShippingOrder.ArrivalDate;
                newPartsInboundPlan.Remark = supplierShippingOrder.Remark;
                newPartsInboundPlan.GPMSPurOrderCode = supplierShippingOrder.GPMSPurOrderCode;
                newPartsInboundPlan.SAPPurchasePlanCode = supplierShippingOrder.SAPPurchasePlanCode;
                newPartsInboundPlan.PlanDeliveryTime = supplierShippingOrder.PlanDeliveryTime;
                InsertToDatabase(newPartsInboundPlan);
                new PartsInboundPlanAch(this.DomainService).InsertPartsInboundPlanValidate(newPartsInboundPlan);
                foreach(var supplierShippingDetail in supplierShippingDetails) {
                    supplierShippingDetail.ConfirmedAmount = supplierShippingDetail.Quantity;
                    var newPartsInboundPlanDetail = new PartsInboundPlanDetail();
                    newPartsInboundPlanDetail.SparePartId = supplierShippingDetail.SparePartId;
                    newPartsInboundPlanDetail.SparePartCode = supplierShippingDetail.SparePartCode;
                    newPartsInboundPlanDetail.SparePartName = supplierShippingDetail.SparePartName;
                    newPartsInboundPlanDetail.PlannedAmount = supplierShippingDetail.Quantity;
                    newPartsInboundPlanDetail.Price = supplierShippingDetail.UnitPrice;
                    newPartsInboundPlanDetail.Remark = supplierShippingDetail.Remark;
                    newPartsInboundPlanDetail.POCode = supplierShippingDetail.POCode;

                    var dbpartsPurchaseOrderDetail = partsPurchaseOrderDetails.FirstOrDefault(r => r.SparePartId == supplierShippingDetail.SparePartId);
                    newPartsInboundPlanDetail.OriginalPlanNumber = dbpartsPurchaseOrderDetail.OriginalPlanNumber;

                    newPartsInboundPlan.PartsInboundPlanDetails.Add(newPartsInboundPlanDetail);
                    InsertToDatabase(newPartsInboundPlanDetail);
                    ObjectContext.SaveChanges();
                    //新建精确码追溯
                 //   var oldAccurateTrace = ObjectContext.AccurateTraces.Where(t => t.Status == (int)DcsBaseDataStatus.有效).ToList();
                    var oldTraceTempType = ObjectContext.TraceTempTypes.Where(t => t.Status == (int)DcsBaseDataStatus.有效).ToList();
                    if(!string.IsNullOrEmpty( supplierShippingDetail.TraceCode) && supplierShippingDetail.TraceProperty.HasValue) {
                        var traceCode = supplierShippingDetail.TraceCode.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries).ToArray();
                        foreach(var code in traceCode) {
                           // if(oldAccurateTrace.Any(t => t.TraceCode == code) || oldTraceTempType.Any(t => t.TraceCode == code)) {
                             //   throw new ValidationException("历史数据已存在追溯码：" + code);
                           // } else {
                                var newAccurateTrace = new AccurateTrace {
                                    Type = (int)DCSAccurateTraceType.入库,
                                    SourceBillId = newPartsInboundPlan.Id,
                                    PartId = supplierShippingDetail.SparePartId,
                                    TraceCode = code,
                                    CompanyType = (int)DCSAccurateTraceCompanyType.配件中心,
                                    CompanyId = newPartsInboundPlan.StorageCompanyId,
                                    Status = (int)DCSAccurateTraceStatus.有效,
                                    TraceProperty = supplierShippingDetail.TraceProperty
                                };
                                new AccurateTraceAch(this.DomainService).ValidataAccurateTrace(newAccurateTrace);
                          //  }
                        }
                }
                }
            }
            //}
            ////更新配件物流批次

            //var dbPartsLogisticBatch = ObjectContext.PartsLogisticBatches.Where(r => r.SourceId == supplierShippingOrder.PartsPurchaseOrderId && r.SourceType == (int)DcsPartsLogisticBatchSourceType.配件采购订单).ToArray();
            //if (dbPartsLogisticBatch.Length > 1)
            //{
            //    throw new ValidationException("采购订单已存在多个配件物流批次");
            //}
            //if (dbPartsLogisticBatch.Any())
            //{
            //    //新增配件物流批次单据清单
            //    var newPartsLogisticBatchBillDetail = new PartsLogisticBatchBillDetail();
            //    newPartsLogisticBatchBillDetail.Id = i--;
            //    newPartsLogisticBatchBillDetail.BillId = supplierShippingOrder.Id;
            //    newPartsLogisticBatchBillDetail.BillType = (int)DcsPartsLogisticBatchBillDetailBillType.供应商发运单;
            //    dbPartsLogisticBatch.First().PartsLogisticBatchBillDetails.Add(newPartsLogisticBatchBillDetail);
            //    InsertToDatabase(newPartsLogisticBatchBillDetail);

            //    //新增配件物流批次配件清单
            //    foreach (var supplierShippingDetail in supplierShippingDetails)
            //    {
            //        var newPartsLogisticBatchItemDetail = new PartsLogisticBatchItemDetail();
            //        newPartsLogisticBatchItemDetail.CounterpartCompanyId = supplierShippingOrder.BranchId;
            //        newPartsLogisticBatchItemDetail.ReceivingCompanyId = supplierShippingOrder.ReceivingCompanyId.GetValueOrDefault();
            //        newPartsLogisticBatchItemDetail.ShippingCompanyId = supplierShippingOrder.PartsSupplierId;
            //        newPartsLogisticBatchItemDetail.SparePartId = supplierShippingDetail.SparePartId;
            //        newPartsLogisticBatchItemDetail.SparePartCode = supplierShippingDetail.SparePartCode;
            //        newPartsLogisticBatchItemDetail.SparePartName = supplierShippingDetail.SparePartName;
            //        newPartsLogisticBatchItemDetail.OutboundAmount = supplierShippingDetail.ConfirmedAmount.GetValueOrDefault(0);
            //        newPartsLogisticBatchItemDetail.BatchNumber = "";
            //        newPartsLogisticBatchItemDetail.InboundAmount = 0;
            //        dbPartsLogisticBatch.First().PartsLogisticBatchItemDetails.Add(newPartsLogisticBatchItemDetail);
            //        InsertToDatabase(newPartsLogisticBatchItemDetail);
            //    }
            //}
            //else
            //{
            //    //新增物流批次       
            //    var newPartsLogisticBatch = new PartsLogisticBatch();
            //    newPartsLogisticBatch.Id = i--;
            //    newPartsLogisticBatch.ShippingStatus = (int)DcsPartsLogisticBatchShippingStatus.供应商发运;
            //    newPartsLogisticBatch.Status = (int)DcsPartsLogisticBatchStatus.新增;
            //    newPartsLogisticBatch.CompanyId = supplierShippingOrder.PartsSupplierId;
            //    newPartsLogisticBatch.SourceId = supplierShippingOrder.PartsPurchaseOrderId;
            //    newPartsLogisticBatch.SourceType = (int)DcsPartsLogisticBatchSourceType.配件采购订单;
            //    InsertToDatabase(newPartsLogisticBatch);
            //    new PartsLogisticBatchAch(this.DomainService).InsertPartsLogisticBatchValidate(newPartsLogisticBatch);

            //    //新增配件物流批次单据清单
            //    var newPartsLogisticBatchBillDetail = new PartsLogisticBatchBillDetail();
            //    newPartsLogisticBatchBillDetail.Id = i--;
            //    newPartsLogisticBatchBillDetail.PartsLogisticBatchId = newPartsLogisticBatch.Id;
            //    newPartsLogisticBatchBillDetail.BillId = supplierShippingOrder.Id;
            //    newPartsLogisticBatchBillDetail.BillType = (int)DcsPartsLogisticBatchBillDetailBillType.供应商发运单;

            //    newPartsLogisticBatch.PartsLogisticBatchBillDetails.Add(newPartsLogisticBatchBillDetail);
            //    InsertToDatabase(newPartsLogisticBatchBillDetail);

            //    //新增配件物流批次配件清单
            //    foreach (var supplierShippingDetail in supplierShippingDetails)
            //    {
            //        var newPartsLogisticBatchItemDetail = new PartsLogisticBatchItemDetail();
            //        newPartsLogisticBatchItemDetail.Id = i--;
            //        newPartsLogisticBatchItemDetail.PartsLogisticBatchId = newPartsLogisticBatch.Id;
            //        newPartsLogisticBatchItemDetail.CounterpartCompanyId = supplierShippingOrder.BranchId;
            //        newPartsLogisticBatchItemDetail.ReceivingCompanyId = supplierShippingOrder.ReceivingCompanyId.GetValueOrDefault();
            //        newPartsLogisticBatchItemDetail.ShippingCompanyId = supplierShippingOrder.PartsSupplierId;
            //        newPartsLogisticBatchItemDetail.SparePartId = supplierShippingDetail.SparePartId;
            //        newPartsLogisticBatchItemDetail.SparePartCode = supplierShippingDetail.SparePartCode;
            //        newPartsLogisticBatchItemDetail.SparePartName = supplierShippingDetail.SparePartName;
            //        newPartsLogisticBatchItemDetail.OutboundAmount = supplierShippingDetail.ConfirmedAmount.GetValueOrDefault(0);
            //        newPartsLogisticBatchItemDetail.BatchNumber = "";
            //        newPartsLogisticBatchItemDetail.InboundAmount = 0;
            //        newPartsLogisticBatch.PartsLogisticBatchItemDetails.Add(newPartsLogisticBatchItemDetail);
            //        InsertToDatabase(newPartsLogisticBatchItemDetail);
            //    }
            //}
            transaction.Complete();
              }
        }


        public void 修改供应商发运单(SupplierShippingOrder supplierShippingOrder) {
            CheckEntityState(supplierShippingOrder);
            var dbsupplierShippingOrder = ObjectContext.SupplierShippingOrders.Where(r => r.Id == supplierShippingOrder.Id && r.Status != (int)DcsSupplierShippingOrderStatus.新建).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbsupplierShippingOrder != null)
                throw new ValidationException(string.Format(ErrorStrings.SupplierShippingOrder_Validation5));

            var supplierShippingDetails = supplierShippingOrder.SupplierShippingDetails.ToArray();
            var dbsupplierShippingDetails = ObjectContext.SupplierShippingDetails.Where(r => r.SupplierShippingOrderId == supplierShippingOrder.Id).SetMergeOption(MergeOption.NoTracking).ToArray();
            var partsPurchaseOrder = ObjectContext.PartsPurchaseOrders.Where(r => r.Id == supplierShippingOrder.PartsPurchaseOrderId).Include("PartsPurchaseOrderDetails").SingleOrDefault();
            if(partsPurchaseOrder == null)
                throw new ValidationException(string.Format(ErrorStrings.SupplierShippingOrder_Validation6, supplierShippingOrder.Code));
            var partsPurchaseOrderDetails = partsPurchaseOrder.PartsPurchaseOrderDetails.ToArray();
            foreach(var supplierShippingDetail in dbsupplierShippingDetails) {
                var partsPurchaseOrderDetail = partsPurchaseOrderDetails.FirstOrDefault(r => r.SparePartId == supplierShippingDetail.SparePartId);
                if(partsPurchaseOrderDetail == null)
                    throw new ValidationException(string.Format(ErrorStrings.SupplierShippingOrder_Validation7, supplierShippingOrder.Code, supplierShippingDetail.SparePartCode));
                if(partsPurchaseOrderDetail.ShippingAmount == null)
                    partsPurchaseOrderDetail.ShippingAmount = 0;
                partsPurchaseOrderDetail.ShippingAmount -= supplierShippingDetail.Quantity;
            }
            foreach(var supplierShippingDetail in supplierShippingDetails) {
                var partsPurchaseOrderDetail = partsPurchaseOrderDetails.FirstOrDefault(r => r.SparePartId == supplierShippingDetail.SparePartId);
                if(partsPurchaseOrderDetail == null)
                    throw new ValidationException(string.Format(ErrorStrings.SupplierShippingOrder_Validation7, supplierShippingOrder.Code, supplierShippingDetail.SparePartCode));
                if(partsPurchaseOrderDetail.ShippingAmount == null)
                    partsPurchaseOrderDetail.ShippingAmount = 0;
                partsPurchaseOrderDetail.ShippingAmount += supplierShippingDetail.Quantity;
                if(partsPurchaseOrderDetail.ShippingAmount > partsPurchaseOrderDetail.ConfirmedAmount)
                    throw new ValidationException(string.Format(ErrorStrings.SupplierShippingOrder_Validation4, supplierShippingDetail.SparePartCode));
            }
            if(partsPurchaseOrderDetails.Any(r => r.ShippingAmount != 0))
                partsPurchaseOrder.Status = (int)DcsPartsPurchaseOrderStatus.部分发运;
            if(partsPurchaseOrderDetails.All(r => r.ConfirmedAmount == r.OrderAmount && r.ConfirmedAmount == r.ShippingAmount))
                partsPurchaseOrder.Status = (int)DcsPartsPurchaseOrderStatus.发运完毕;
            this.UpdateSupplierShippingOrderValidate(supplierShippingOrder);
        }

        public void 修改供应商发运单2(SupplierShippingOrder supplierShippingOrder) {
            CheckEntityState(supplierShippingOrder);
            var dbsupplierShippingOrder = ObjectContext.SupplierShippingOrders.Where(r => r.Id == supplierShippingOrder.Id && r.Status != (int)DcsSupplierShippingOrderStatus.新建).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbsupplierShippingOrder != null)
                throw new ValidationException(string.Format(ErrorStrings.SupplierShippingOrder_Validation5));
            this.UpdateSupplierShippingOrderValidate(supplierShippingOrder);
        }
        public void 确认供应商发运单(SupplierShippingOrder supplierShippingOrder) {
            var dbsupplierShippingOrder = ObjectContext.SupplierShippingOrders.Where(r => r.Id == supplierShippingOrder.Id && (r.Status == (int)DcsSupplierShippingOrderStatus.部分确认 || r.Status == (int)DcsSupplierShippingOrderStatus.新建)).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbsupplierShippingOrder == null)
                throw new ValidationException(ErrorStrings.SupplierShippingOrder_Validation2);

            //var branchstrategy = ObjectContext.Branchstrategies.FirstOrDefault(r => r.BranchId == supplierShippingOrder.BranchId && r.Status != (int)DcsBaseDataStatus.作废);

            //if(branchstrategy == null) {
            //    throw new ValidationException(ErrorStrings.SupplierShippingOrder_Validation29);
            //}

            var salesCenterstrategy = this.ObjectContext.SalesCenterstrategies.SingleOrDefault(r => r.PartsSalesCategoryId == supplierShippingOrder.PartsSalesCategoryId && r.Status == (int)DcsBaseDataStatus.有效);
            if(salesCenterstrategy == null)
                throw new ValidationException(ErrorStrings.SupplierShippingOrder_Validation31);

            if(salesCenterstrategy.PartsPurchasePricingStrategy == (int)DcsBranchstrategyPartsPurchasePricingStrategy.先定价) {
                var partsCodesCheckUnitPrice = supplierShippingOrder.SupplierShippingDetails.Where(r => r.UnitPrice == 0).Select(v => v.SparePartCode).ToArray();
                if(partsCodesCheckUnitPrice.Length > 0) {
                    throw new ValidationException(string.Format(ErrorStrings.SupplierShippingOrder_Validation27, string.Join("，", partsCodesCheckUnitPrice)));
                }
            }
            var sumQuantity = 0;
            var sumConfirmedAmount = 0;
            foreach(var supplierShippingDetail in supplierShippingOrder.SupplierShippingDetails) {
                sumQuantity += supplierShippingDetail.Quantity;
                sumConfirmedAmount += supplierShippingDetail.ConfirmedAmount ?? 0;
            }
            //更新供应商发运单
            if(sumQuantity == sumConfirmedAmount) {
                supplierShippingOrder.Status = (int)DcsSupplierShippingOrderStatus.收货确认;
            } else if(sumQuantity > sumConfirmedAmount) {
                supplierShippingOrder.Status = (int)DcsSupplierShippingOrderStatus.部分确认;
            }
            var userInfo = Utils.GetCurrentUserInfo();
            supplierShippingOrder.ConfirmorId = userInfo.Id;
            supplierShippingOrder.ConfirmorName = userInfo.Name;
            supplierShippingOrder.ConfirmationTime = DateTime.Now;
            if(supplierShippingOrder.FirstConfirmationTime == null)
                supplierShippingOrder.FirstConfirmationTime = DateTime.Now;
            supplierShippingOrder.ConfirmationTime = DateTime.Now;
            UpdateToDatabase(supplierShippingOrder);
            this.UpdateSupplierShippingOrderValidate(supplierShippingOrder);
            var partsPurchaseOrder = ObjectContext.PartsPurchaseOrders.SingleOrDefault(r => r.Id == supplierShippingOrder.PartsPurchaseOrderId);
            if(partsPurchaseOrder == null) {
                throw new ValidationException(ErrorStrings.SupplierShippingOrder_Validation32);
            }
            //添加配件入库计划单
            var warehouse = ObjectContext.Warehouses.FirstOrDefault(r => r.Id == supplierShippingOrder.ReceivingWarehouseId && r.Status == (int)DcsBaseDataStatus.有效);
            if(warehouse == null)
                throw new ValidationException(string.Format(ErrorStrings.SupplierShippingOrder_Validation8, supplierShippingOrder.Code));
            //var supplierAccount = ObjectContext.SupplierAccounts.FirstOrDefault(r => r.BuyerCompanyId == supplierShippingOrder.BranchId && r.SupplierCompanyId == supplierShippingOrder.PartsSupplierId && r.Status == (int)DcsMasterDataStatus.有效);
            //if (supplierAccount == null)
            //    throw new ValidationException(string.Format(ErrorStrings.SupplierShippingOrder_Validation9, supplierShippingOrder.Code));
            var partsInboundPlan = new PartsInboundPlan {
                WarehouseId = supplierShippingOrder.ReceivingWarehouseId,
                WarehouseCode = warehouse.Code,
                WarehouseName = warehouse.Name,
                StorageCompanyId = supplierShippingOrder.BranchId,
                StorageCompanyCode = supplierShippingOrder.BranchCode,
                StorageCompanyName = supplierShippingOrder.BranchName,
                StorageCompanyType = (int)DcsCompanyType.分公司,
                BranchId = supplierShippingOrder.BranchId,
                BranchCode = supplierShippingOrder.BranchCode,
                BranchName = supplierShippingOrder.BranchName,
                CounterpartCompanyId = supplierShippingOrder.PartsSupplierId,
                CounterpartCompanyCode = supplierShippingOrder.PartsSupplierCode,
                CounterpartCompanyName = supplierShippingOrder.PartsSupplierName,
                SourceId = supplierShippingOrder.Id,
                SourceCode = supplierShippingOrder.Code,
                InboundType = (int)DcsPartsInboundType.配件采购,
                //CustomerAccountId = supplierAccount.Id,
                OriginalRequirementBillId = supplierShippingOrder.PartsPurchaseOrderId,
                OriginalRequirementBillCode = supplierShippingOrder.PartsPurchaseOrderCode,
                OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.配件采购订单,
                Status = (int)DcsPartsInboundPlanStatus.新建,
                PartsSalesCategoryId = supplierShippingOrder.PartsSalesCategoryId,
                IfWmsInterface = warehouse.WmsInterface,
                ArrivalDate = supplierShippingOrder.ArrivalDate,
                GPMSPurOrderCode = supplierShippingOrder.GPMSPurOrderCode,
                Remark = partsPurchaseOrder.PlanSource + "/" + supplierShippingOrder.Remark,
                SAPPurchasePlanCode = supplierShippingOrder.SAPPurchasePlanCode
            };
            var supplierShippingDetails = ChangeSet.GetAssociatedChanges(supplierShippingOrder, v => v.SupplierShippingDetails);

            foreach(SupplierShippingDetail supplierShippingDetail in supplierShippingDetails) {
                var dbSupplierShippingDetail = dbsupplierShippingOrder.SupplierShippingDetails.SingleOrDefault(r => r.Id == supplierShippingDetail.Id);
                if(dbSupplierShippingDetail == null) {
                    throw new ValidationException(ErrorStrings.SupplierShippingOrder_Validation33);
                }
                var currentConfirmedAmount = supplierShippingDetail.ConfirmedAmount - dbSupplierShippingDetail.ConfirmedAmount;
                if(currentConfirmedAmount != null && currentConfirmedAmount != 0) {
                    //添加配件入库计划清单
                    var partsInboundPlanDetail = new PartsInboundPlanDetail {
                        PartsInboundPlanId = partsInboundPlan.Id,
                        SparePartId = supplierShippingDetail.SparePartId,
                        SparePartCode = supplierShippingDetail.SparePartCode,
                        SparePartName = supplierShippingDetail.SparePartName,
                        PlannedAmount = currentConfirmedAmount.Value,
                        Price = supplierShippingDetail.UnitPrice,
                        POCode = supplierShippingDetail.POCode,
                        Remark = supplierShippingDetail.Remark
                    };
                    partsInboundPlan.PartsInboundPlanDetails.Add(partsInboundPlanDetail);
                }
            }
            if(partsInboundPlan.PartsInboundPlanDetails.Any()) {
                //更新配件物流批次
                var partsLogisticBatchBillDetail = ObjectContext.PartsLogisticBatchBillDetails.FirstOrDefault(r => r.BillId == supplierShippingOrder.Id && r.BillType == (int)DcsPartsLogisticBatchBillDetailBillType.供应商发运单);
                if(partsLogisticBatchBillDetail != null) {
                    //添加配件入库计划单的配件物流批次单据清单
                    partsInboundPlan.PartsLogisticBatchBillDetails.Add(new PartsLogisticBatchBillDetail {
                        PartsLogisticBatchId = partsLogisticBatchBillDetail.PartsLogisticBatchId,
                        BillId = partsInboundPlan.Id,
                        BillType = (int)DcsPartsLogisticBatchBillDetailBillType.配件入库计划,
                        CreatorId = partsInboundPlan.CreatorId,
                        CreatorName = partsInboundPlan.CreatorName,
                        CreateTime = partsInboundPlan.CreateTime
                    });
                }

                InsertToDatabase(partsInboundPlan);
                new PartsInboundPlanAch(this.DomainService).InsertPartsInboundPlanValidate(partsInboundPlan);
            }
        }


        public void 确认供应商直供发运单(SupplierShippingOrder supplierShippingOrder) {
          using(var transaction = new TransactionScope()) {   try {
                var dbsupplierShippingOrder = ObjectContext.SupplierShippingOrders.Where(r => r.Id == supplierShippingOrder.Id && r.Status != (int)DcsSupplierShippingOrderStatus.新建 && r.Status != (int)DcsSupplierShippingOrderStatus.部分确认).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
                if(dbsupplierShippingOrder != null)
                    throw new ValidationException(string.Format(ErrorStrings.SupplierShippingOrder_Validation2));
                //foreach(var supplierShippingDetail in supplierShippingOrder.SupplierShippingDetails) {
                //    if(supplierShippingDetail.UnitPrice == 0)               
                //        throw new ValidationException(string.Format(ErrorStrings.SupplierShippingOrder_Validation25, supplierShippingDetail.SparePartCode));
                //}
                var partsPurchaseOrder = ObjectContext.PartsPurchaseOrders.SingleOrDefault(r => r.Id == supplierShippingOrder.PartsPurchaseOrderId && r.Status != (int)DcsPartsPurchaseOrderStatus.作废);
                if(partsPurchaseOrder == null)
                    throw new ValidationException(ErrorStrings.SupplierShippingOrder_Validation26);
                ////配件物流批次单据清单
                //var partsLogisticBatchBillDetail = ObjectContext.PartsLogisticBatchBillDetails.FirstOrDefault(r => r.BillId == supplierShippingOrder.Id && r.BillType == (int)DcsPartsLogisticBatchBillDetailBillType.供应商发运单);
                //if (partsLogisticBatchBillDetail == null)
                //    throw new ValidationException(string.Format(ErrorStrings.SupplierShippingOrder_Validation10, supplierShippingOrder.Code));
                var sumQuantity = 0;
                var sumConfirmedAmount = 0;
                foreach(var supplierShippingDetail in supplierShippingOrder.SupplierShippingDetails) {
                    sumQuantity += supplierShippingDetail.Quantity;
                    sumConfirmedAmount += supplierShippingDetail.ConfirmedAmount ?? 0;
                }
                //更新供应商发运单
                //if(sumConfirmedAmount == 0) {
                //    throw new ValidationException(ErrorStrings.SupplierShippingOrder_Validation34);
                //}
                if(sumQuantity == sumConfirmedAmount) {
                    supplierShippingOrder.Status = (int)DcsSupplierShippingOrderStatus.收货确认;
                } else if(sumQuantity > sumConfirmedAmount) {
                    supplierShippingOrder.Status = (int)DcsSupplierShippingOrderStatus.部分确认;
                }
                if(supplierShippingOrder.LogisticArrivalDate == null) {
                    supplierShippingOrder.LogisticArrivalDate = DateTime.Now;                   
                    supplierShippingOrder.ArriveMethod = (int)DCSSupplierShippingOrderArriveMethod.默认到货;
                    supplierShippingOrder.ComfirmStatus = (int)DCSSupplierShippingOrderComfirmStatus.入库确认;
                }
                if(supplierShippingOrder.InboundTime==null ) {
                    supplierShippingOrder.InboundTime = DateTime.Now;
                 }
                var userInfo = Utils.GetCurrentUserInfo();
                supplierShippingOrder.ConfirmorId = userInfo.Id;
                supplierShippingOrder.ConfirmorName = userInfo.Name;
                supplierShippingOrder.ConfirmationTime = DateTime.Now;
                UpdateToDatabase(supplierShippingOrder);
                this.UpdateSupplierShippingOrderValidate(supplierShippingOrder);
                var company = ObjectContext.Companies.FirstOrDefault(r => r.Id == supplierShippingOrder.ReceivingCompanyId.Value && r.Status != (int)DcsMasterDataStatus.作废);
                if(company == null)
                    throw new ValidationException(string.Format(ErrorStrings.SupplierShippingOrder_Validation16));
                var saleOrder = ObjectContext.PartsSalesOrders.SingleOrDefault(order => order.Id == supplierShippingOrder.OriginalRequirementBillId);
                if(saleOrder == null)
                    throw new ValidationException(ErrorStrings.SupplierShippingOrder_Validation23);
                if(company.Type == (int)DcsCompanyType.代理库 || company.Type == (int)DcsCompanyType.服务站兼代理库) {
                    //添加配件入库计划单
                    if(!supplierShippingOrder.ReceivingCompanyId.HasValue)
                        throw new ValidationException(ErrorStrings.SupplierShippingOrder_Validation21);
                    var receivingCompany = ObjectContext.Companies.SingleOrDefault(v => v.Id == supplierShippingOrder.ReceivingCompanyId.Value && v.Status == (int)DcsMasterDataStatus.有效);
                    if(receivingCompany == null)
                        throw new ValidationException(ErrorStrings.SupplierShippingOrder_Validation22);
                    if(!supplierShippingOrder.DirectRecWarehouseId.HasValue)
                        throw new ValidationException(ErrorStrings.SupplierShippingOrder_Validation35);
                    var directRecWarehouse = ObjectContext.Warehouses.SingleOrDefault(v => v.Id == supplierShippingOrder.DirectRecWarehouseId && v.Status == (int)DcsMasterDataStatus.有效);
                    if(directRecWarehouse == null)
                        throw new ValidationException(ErrorStrings.SupplierShippingOrder_Validation36);

                    var partsInboundPlan = new PartsInboundPlan {
                        WarehouseId = supplierShippingOrder.DirectRecWarehouseId.Value,
                        WarehouseCode = directRecWarehouse.Code,
                        WarehouseName = supplierShippingOrder.DirectRecWarehouseName,
                        StorageCompanyId = receivingCompany.Id,
                        StorageCompanyCode = receivingCompany.Code,
                        StorageCompanyName = receivingCompany.Name,
                        StorageCompanyType = (int)DcsCompanyType.代理库,
                        BranchId = supplierShippingOrder.BranchId,
                        BranchCode = supplierShippingOrder.BranchCode,
                        BranchName = supplierShippingOrder.BranchName,
                        CounterpartCompanyId = supplierShippingOrder.BranchId,
                        CounterpartCompanyCode = supplierShippingOrder.BranchCode,
                        CounterpartCompanyName = supplierShippingOrder.BranchName,
                        SourceId = supplierShippingOrder.Id,
                        SourceCode = supplierShippingOrder.Code,
                        InboundType = (int)DcsPartsInboundType.配件采购,
                        CustomerAccountId = saleOrder.CustomerAccountId,
                        OriginalRequirementBillId = supplierShippingOrder.OriginalRequirementBillId,
                        OriginalRequirementBillCode = supplierShippingOrder.OriginalRequirementBillCode,
                        OriginalRequirementBillType = supplierShippingOrder.OriginalRequirementBillType,
                        Status = (int)DcsPartsInboundPlanStatus.新建,
                        Remark = supplierShippingOrder.Remark,
                        PartsSalesCategoryId = supplierShippingOrder.PartsSalesCategoryId,
                        ArrivalDate = supplierShippingOrder.ArrivalDate,
                        SAPPurchasePlanCode = supplierShippingOrder.SAPPurchasePlanCode
                    };
                    InsertToDatabase(partsInboundPlan);
                    new PartsInboundPlanAch(this.DomainService).InsertPartsInboundPlanValidate(partsInboundPlan);                   
                    var supplierShippingDetails = supplierShippingOrder.SupplierShippingDetails;
                    var sparePartIds = supplierShippingDetails.Select(v => v.SparePartId).ToArray();
                    var saleOrderDetails = ObjectContext.PartsSalesOrderDetails.Where(v => v.PartsSalesOrderId == saleOrder.Id && sparePartIds.Contains(v.SparePartId)).Select(v => new {
                        v.SparePartId,
                        v.OrderPrice
                    }).ToArray();
                    // var oldTraceTempType = ObjectContext.TraceTempTypes.Where(t => t.SourceBillId == supplierShippingOrder.Id && t.Type==(int)DCSTraceTempType.直供).ToList();
                    foreach(var supplierShippingDetail in supplierShippingDetails) {
                        var saleOrderDetail = saleOrderDetails.SingleOrDefault(v => v.SparePartId == supplierShippingDetail.SparePartId);
                        if(saleOrderDetail == null)
                            throw new ValidationException(string.Format(ErrorStrings.SupplierShippingOrder_Validation24, supplierShippingDetail.SparePartCode));
                        //添加配件入库计划清单
                        var partsInboundPlanDetail = new PartsInboundPlanDetail {
                            PartsInboundPlanId = partsInboundPlan.Id,
                            SparePartId = supplierShippingDetail.SparePartId,
                            SparePartCode = supplierShippingDetail.SparePartCode,
                            SparePartName = supplierShippingDetail.SparePartName,
                            PlannedAmount = supplierShippingDetail.ConfirmedAmount ?? 0,
                            Price = saleOrderDetail.OrderPrice,
                            Remark = supplierShippingDetail.Remark,
                            POCode = supplierShippingDetail.POCode
                        };
                        partsInboundPlan.PartsInboundPlanDetails.Add(partsInboundPlanDetail);
                        ObjectContext.SaveChanges();
                        //增加追溯码记录
                       
                        //if(oldTraceTempType.Count()>0){
                        //    var detailTemp = oldTraceTempType.Where(t => t.SourceBillDetailId == supplierShippingDetail.Id).ToArray();
                        //    foreach(var temp in detailTemp){
                        //        var trac = new AccurateTrace {
                        //            Type = (int)DCSAccurateTraceType.入库,
                        //            CompanyType = (int)DCSAccurateTraceCompanyType.中心库,
                        //            CompanyId = receivingCompany.Id,
                        //            PartId = supplierShippingDetail.SparePartId,
                        //            TraceCode=temp.TraceCode,
                        //            Status = (int)DCSAccurateTraceStatus.有效,
                        //            SourceBillId = partsInboundPlan.Id,
                        //            TraceProperty = supplierShippingDetail.TraceProperty
                        //        };
                        //        new AccurateTraceAch(this.DomainService).ValidataAccurateTrace(trac);
                        //        temp.Status = (int)DcsBaseDataStatus.作废;
                        //        temp.ModifyTime = DateTime.Now;
                        //        temp.ModifierId = userInfo.Id;
                        //        temp.ModifierName = userInfo.Name;
                        //        UpdateToDatabase(temp);
                        //    }
                        //}
                    }                                    
                }
               
                //更新经销商库存
                if(company.Type == (int)DcsCompanyType.服务站) {
                    var dbDealerPartsStocks = ObjectContext.DealerPartsStocks.Where(r => r.DealerId == supplierShippingOrder.ReceivingCompanyId && r.SalesCategoryId == supplierShippingOrder.PartsSalesCategoryId && r.SubDealerId == -1).ToArray();
                    var supplierShippingOrderDetails = supplierShippingOrder.SupplierShippingDetails;
                    var sparePartIds = supplierShippingOrderDetails.Select(r => r.SparePartId).ToArray();
                    var dbpartDeleaveInformations = ObjectContext.PartDeleaveInformations.Where(r => sparePartIds.Contains(r.OldPartId) && r.PartsSalesCategoryId == supplierShippingOrder.PartsSalesCategoryId && r.BranchId == supplierShippingOrder.BranchId && r.Status == (int)DcsBaseDataStatus.有效).ToArray();
                    foreach(var item in supplierShippingOrderDetails) {
                        var dbDealerPartsStock = dbDealerPartsStocks.SingleOrDefault(r => r.SparePartId == item.SparePartId);
                        //需要增加配件拆散信息实际上只有有才有拆散件信息
                        var dbpartDeleaveInformation = dbpartDeleaveInformations.FirstOrDefault(c => c.OldPartId == item.SparePartId);
                        if(dbDealerPartsStock != null) {
                            if(dbpartDeleaveInformation != null) {
                                dbDealerPartsStock.Quantity = dbDealerPartsStock.Quantity + (item.ConfirmedAmount ?? 0) * dbpartDeleaveInformation.DeleaveAmount;
                            } else {
                                dbDealerPartsStock.Quantity = dbDealerPartsStock.Quantity + (item.ConfirmedAmount ?? 0);
                            }
                            DomainService.UpdateDealerPartsStock(dbDealerPartsStock);
                        } else {
                            //上面的步骤相当于执行validate中的校验,所以可以直接InsertToDatabase
                            var dps = new DealerPartsStock();
                            dps.DealerId = Convert.ToInt32(supplierShippingOrder.ReceivingCompanyId);
                            dps.DealerCode = company.Code;
                            dps.DealerName = supplierShippingOrder.ReceivingCompanyName;
                            dps.BranchId = supplierShippingOrder.BranchId;
                            dps.SalesCategoryId = supplierShippingOrder.PartsSalesCategoryId;
                            dps.SalesCategoryName = supplierShippingOrder.PartsSalesCategoryName;
                            dps.SparePartId = item.SparePartId;
                            dps.SparePartCode = item.SparePartCode;
                            dps.SubDealerId = -1;
                            var tempPartDeleaveInformation = dbpartDeleaveInformations.FirstOrDefault(r => r.OldPartId == item.SparePartId);
                            if(tempPartDeleaveInformation == null) {
                                dps.Quantity = item.ConfirmedAmount ?? 0;
                            } else {
                                dps.Quantity = (item.ConfirmedAmount ?? 0) * tempPartDeleaveInformation.DeleaveAmount;
                            }
                            dps.CreatorId = userInfo.Id;
                            dps.CreatorName = userInfo.Name;
                            dps.CreateTime = DateTime.Now;
                            InsertToDatabase(dps);
                        }
                    }
                }
            } catch(OptimisticConcurrencyException) {
                throw new ValidationException(ErrorStrings.SupplierShippingOrder_Validation37);
            }
          ObjectContext.SaveChanges();
          transaction.Complete();
          }
        }


        public void 完成供应商直供发运单(SupplierShippingOrder supplierShippingOrder) {
            var dbsupplierShippingOrder = ObjectContext.SupplierShippingOrders.Where(r => r.Id == supplierShippingOrder.Id).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbsupplierShippingOrder == null) {
                throw new ValidationException(ErrorStrings.SupplierShippingOrder_Validation38);
            }
            if(dbsupplierShippingOrder.DirectProvisionFinished != null && (bool)dbsupplierShippingOrder.DirectProvisionFinished) {
                throw new ValidationException(ErrorStrings.SupplierShippingOrder_Validation39);
            }
            var userInfo = Utils.GetCurrentUserInfo();
            var salesCenterStrategy = ObjectContext.SalesCenterstrategies.Where(r => r.PartsSalesCategoryId == supplierShippingOrder.PartsSalesCategoryId && r.Status == (int)DcsBaseDataStatus.有效).FirstOrDefault();
            var supplierShippingDetails = supplierShippingOrder.SupplierShippingDetails;

            if(salesCenterStrategy != null && salesCenterStrategy.PartsPurchasePricingStrategy == (int)DcsBranchstrategyPartsPurchasePricingStrategy.先定价) {
                foreach(var supplierShippingDetail in supplierShippingDetails) {
                    if(supplierShippingDetail.UnitPrice == 0)
                        throw new ValidationException(string.Format(ErrorStrings.SupplierShippingOrder_Validation30, supplierShippingDetail.SparePartCode));
                }
            }

            if(dbsupplierShippingOrder.Status != (int)DcsSupplierShippingOrderStatus.收货确认) {
                if(dbsupplierShippingOrder.Status != (int)DcsSupplierShippingOrderStatus.终止) {
                    throw new ValidationException(string.Format(ErrorStrings.SupplierShippingOrder_Validation17));
                }
                var tempConfirmedAmount = supplierShippingDetails.Select(r => r.ConfirmedAmount.HasValue && r.ConfirmedAmount.Value != 0).ToArray();
                if(tempConfirmedAmount.Any(r => r == false)) {
                    throw new ValidationException(ErrorStrings.SupplierShippingOrder_Validation40);
                }

            }

            //更新供应商发运单
            supplierShippingOrder.DirectProvisionFinished = true;
            UpdateToDatabase(supplierShippingOrder);
            this.UpdateSupplierShippingOrderValidate(supplierShippingOrder);

            //获取分公司虚拟库
            var _warehouse = ObjectContext.Warehouses.Where(r => r.StorageCompanyId == supplierShippingOrder.BranchId && r.Type == (int)DcsWarehouseType.虚拟库 && r.Status == (int)DcsBaseDataStatus.有效);
            var salesUnitAffiWarehouse = from a in _warehouse
                                         join b in ObjectContext.SalesUnitAffiWarehouses on a.Id equals b.WarehouseId
                                         join c in ObjectContext.SalesUnits on b.SalesUnitId equals c.Id
                                         where c.PartsSalesCategoryId == supplierShippingOrder.PartsSalesCategoryId
                                         select a.Id;
            var warehouse = _warehouse.FirstOrDefault(r => salesUnitAffiWarehouse.Contains(r.Id));
            if(warehouse == null)
                throw new ValidationException(string.Format(ErrorStrings.SupplierShippingOrder_Validation15, supplierShippingOrder.BranchCode));
            ////获取供应商账户
            //var supplierAccount = ObjectContext.SupplierAccounts.FirstOrDefault(r => r.BuyerCompanyId == supplierShippingOrder.BranchId && r.SupplierCompanyId == supplierShippingOrder.PartsSupplierId && r.Status == (int)DcsMasterDataStatus.有效);
            //if (supplierAccount == null)
            //    throw new ValidationException(string.Format(ErrorStrings.SupplierShippingOrder_Validation9, supplierShippingOrder.Code));
            //获取销售订单
            var partsSalesOrder = ObjectContext.PartsSalesOrders.SingleOrDefault(r => r.Id == supplierShippingOrder.OriginalRequirementBillId && r.Status != (int)DcsPartsSalesOrderStatus.作废);
            if(partsSalesOrder == null)
                throw new ValidationException(string.Format(ErrorStrings.SupplierShippingOrder_Validation14, supplierShippingOrder.Code));

            //获取销售组织对应的仓库
            var warehouseforPartsShippingOrder = ObjectContext.Warehouses.FirstOrDefault(r => ObjectContext.SalesUnitAffiWarehouses.Any(affi => affi.SalesUnitId == partsSalesOrder.SalesUnitId && r.Id == affi.WarehouseId) && r.Status == (int)DcsBaseDataStatus.有效);
            if(warehouseforPartsShippingOrder == null)
                throw new ValidationException(string.Format(ErrorStrings.SupplierShippingOrder_Validation8, supplierShippingOrder.Code));
            var company = ObjectContext.Companies.SingleOrDefault(r => r.Id == supplierShippingOrder.ReceivingCompanyId && r.Status != (int)DcsMasterDataStatus.作废);
            if(company == null)
                throw new ValidationException(string.Format(ErrorStrings.SupplierShippingOrder_Validation18, supplierShippingOrder.Code));

            //添加配件入库计划单
            var partsInboundPlan = new PartsInboundPlan {
                WarehouseId = warehouse.Id,
                WarehouseCode = warehouse.Code,
                WarehouseName = warehouse.Name,
                StorageCompanyId = supplierShippingOrder.BranchId,
                StorageCompanyCode = supplierShippingOrder.BranchCode,
                StorageCompanyName = supplierShippingOrder.BranchName,
                StorageCompanyType = (int)DcsCompanyType.分公司,
                BranchId = supplierShippingOrder.BranchId,
                BranchCode = supplierShippingOrder.BranchCode,
                BranchName = supplierShippingOrder.BranchName,
                CounterpartCompanyId = supplierShippingOrder.PartsSupplierId,
                CounterpartCompanyCode = supplierShippingOrder.PartsSupplierCode,
                CounterpartCompanyName = supplierShippingOrder.PartsSupplierName,
                SourceId = supplierShippingOrder.Id,
                SourceCode = supplierShippingOrder.Code,
                InboundType = (int)DcsPartsInboundType.配件采购,
                //CustomerAccountId = supplierAccount.Id,
                OriginalRequirementBillId = supplierShippingOrder.PartsPurchaseOrderId,
                OriginalRequirementBillCode = supplierShippingOrder.PartsPurchaseOrderCode,
                OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.配件采购订单,
                Status = (int)DcsPartsInboundPlanStatus.检验完成,
                Remark = supplierShippingOrder.Remark,
                PartsSalesCategoryId = supplierShippingOrder.PartsSalesCategoryId,
                ArrivalDate = supplierShippingOrder.ArrivalDate,
                SAPPurchasePlanCode = supplierShippingOrder.SAPPurchasePlanCode
            };

            //添加配件入库检验单
            var partsInboundCheckBill = new PartsInboundCheckBill {
                PartsInboundPlanId = partsInboundPlan.Id,
                WarehouseId = warehouse.Id,
                WarehouseCode = warehouse.Code,
                WarehouseName = warehouse.Name,
                StorageCompanyId = supplierShippingOrder.BranchId,
                StorageCompanyCode = supplierShippingOrder.BranchCode,
                StorageCompanyName = supplierShippingOrder.BranchName,
                StorageCompanyType = (int)DcsCompanyType.分公司,
                BranchId = supplierShippingOrder.BranchId,
                BranchCode = supplierShippingOrder.BranchCode,
                BranchName = supplierShippingOrder.BranchName,
                CounterpartCompanyId = supplierShippingOrder.PartsSupplierId,
                CounterpartCompanyCode = supplierShippingOrder.PartsSupplierCode,
                CounterpartCompanyName = supplierShippingOrder.PartsSupplierName,
                InboundType = (int)DcsPartsInboundType.配件采购,
                //CustomerAccountId = supplierAccount.Id,
                OriginalRequirementBillId = supplierShippingOrder.PartsPurchaseOrderId,
                OriginalRequirementBillCode = supplierShippingOrder.PartsPurchaseOrderCode,
                OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.配件采购订单,
                Status = (int)DcsPartsInboundCheckBillStatus.上架完成,
                SettlementStatus = (int)DcsPartsSettlementStatus.待结算,
                Remark = supplierShippingOrder.Remark,
                PartsSalesCategoryId = supplierShippingOrder.PartsSalesCategoryId,
                SAPPurchasePlanCode = supplierShippingOrder.SAPPurchasePlanCode
            };
            if(supplierShippingOrder.PartsSupplierCode.Equals("1000003829") || supplierShippingOrder.PartsSupplierCode.Equals("1000002367")) {
                //上汽依维柯红岩车桥有限公司 和上汽依维柯红岩商用车有限公司 供应商生成 入库计划，入库检验单时赋值 平台单号（交货单号）
                partsInboundPlan.ERPSourceOrderCode = supplierShippingOrder.ERPSourceOrderCode;
                partsInboundCheckBill.ERPSourceOrderCode = supplierShippingOrder.ERPSourceOrderCode;
            }
            if(supplierShippingOrder.ReceivingCompanyId == null) {
                throw new ValidationException(ErrorStrings.SupplierShippingOrder_Validation41);
            }
            //添加配件出库计划单
            var partsOutboundPlan = new PartsOutboundPlan {
                WarehouseId = warehouse.Id,
                WarehouseCode = warehouse.Code,
                WarehouseName = warehouse.Name,
                StorageCompanyId = supplierShippingOrder.BranchId,
                StorageCompanyCode = supplierShippingOrder.BranchCode,
                StorageCompanyName = supplierShippingOrder.BranchName,
                StorageCompanyType = (int)DcsCompanyType.分公司,
                BranchId = supplierShippingOrder.BranchId,
                BranchCode = supplierShippingOrder.BranchCode,
                BranchName = supplierShippingOrder.BranchName,
                CounterpartCompanyId = partsSalesOrder.InvoiceReceiveCompanyId,
                CounterpartCompanyCode = partsSalesOrder.InvoiceReceiveCompanyCode,
                CounterpartCompanyName = partsSalesOrder.InvoiceReceiveCompanyName,
                OutboundType = (int)DcsPartsOutboundType.配件销售,
                CustomerAccountId = partsSalesOrder.CustomerAccountId,
                SourceId = supplierShippingOrder.Id,
                SourceCode = supplierShippingOrder.Code,
                OriginalRequirementBillId = supplierShippingOrder.OriginalRequirementBillId,
                OriginalRequirementBillCode = supplierShippingOrder.OriginalRequirementBillCode,
                OriginalRequirementBillType = supplierShippingOrder.OriginalRequirementBillType,
                Status = (int)DcsPartsOutboundPlanStatus.出库完成,
                Remark = supplierShippingOrder.Remark,
                ReceivingCompanyId = supplierShippingOrder.ReceivingCompanyId.Value,
                ReceivingCompanyCode = company.Code,
                ReceivingCompanyName = supplierShippingOrder.ReceivingCompanyName,
                PartsSalesCategoryId = supplierShippingOrder.PartsSalesCategoryId
            };
            if(company.Type == (int)DcsCompanyType.代理库 || company.Type == (int)DcsCompanyType.服务站兼代理库) {
                partsOutboundPlan.ReceivingWarehouseId = warehouseforPartsShippingOrder.Id;
                partsOutboundPlan.ReceivingWarehouseCode = warehouseforPartsShippingOrder.Code;
                partsOutboundPlan.ReceivingWarehouseName = warehouseforPartsShippingOrder.Name;
            }

            //添加配件出库单
            var partsOutboundBill = new PartsOutboundBill {
                PartsOutboundPlanId = partsOutboundPlan.Id,
                WarehouseId = warehouse.Id,
                WarehouseCode = warehouse.Code,
                WarehouseName = warehouse.Name,
                StorageCompanyId = supplierShippingOrder.BranchId,
                StorageCompanyCode = supplierShippingOrder.BranchCode,
                StorageCompanyName = supplierShippingOrder.BranchName,
                StorageCompanyType = (int)DcsCompanyType.分公司,
                BranchId = supplierShippingOrder.BranchId,
                BranchCode = supplierShippingOrder.BranchCode,
                BranchName = supplierShippingOrder.BranchName,
                CounterpartCompanyId = partsSalesOrder.InvoiceReceiveCompanyId,
                CounterpartCompanyCode = partsSalesOrder.InvoiceReceiveCompanyCode,
                CounterpartCompanyName = partsSalesOrder.InvoiceReceiveCompanyName,
                PartsSalesOrderTypeId = partsSalesOrder.PartsSalesOrderTypeId,
                PartsSalesOrderTypeName = partsSalesOrder.PartsSalesOrderTypeName,
                OutboundType = (int)DcsPartsOutboundType.配件销售,
                CustomerAccountId = partsSalesOrder.CustomerAccountId,
                OriginalRequirementBillId = supplierShippingOrder.OriginalRequirementBillId,
                OriginalRequirementBillCode = supplierShippingOrder.OriginalRequirementBillCode,
                OriginalRequirementBillType = supplierShippingOrder.OriginalRequirementBillType,
                SettlementStatus = (int)DcsPartsSettlementStatus.待结算,
                Remark = supplierShippingOrder.Remark,
                ReceivingCompanyId = supplierShippingOrder.ReceivingCompanyId.Value,
                ReceivingCompanyCode = company.Code,
                ReceivingCompanyName = supplierShippingOrder.ReceivingCompanyName,
                PartsSalesCategoryId = supplierShippingOrder.PartsSalesCategoryId
            };
            if(company.Type == (int)DcsCompanyType.代理库 || company.Type == (int)DcsCompanyType.服务站兼代理库) {
                partsOutboundPlan.ReceivingWarehouseId = warehouseforPartsShippingOrder.Id;
                partsOutboundPlan.ReceivingWarehouseCode = warehouseforPartsShippingOrder.Code;
                partsOutboundPlan.ReceivingWarehouseName = warehouseforPartsShippingOrder.Name;
            }

            //获取企业配件成本
            var sparePartIds = supplierShippingDetails.Select(v => v.SparePartId).ToArray();
            var enterprisePartsCosts = ObjectContext.EnterprisePartsCosts.Where(o => o.OwnerCompanyId == supplierShippingOrder.BranchId && o.PartsSalesCategoryId == supplierShippingOrder.PartsSalesCategoryId && sparePartIds.Contains(o.SparePartId));
            //获取销售订单清单数据
            var partsSalesOrderDetails = ObjectContext.PartsSalesOrderDetails.Where(r => r.PartsSalesOrderId == partsSalesOrder.Id).ToArray();

            //获取配件物流批次
            //var partsLogisticBatchBillDetail = ObjectContext.PartsLogisticBatchBillDetails.Where(r => r.BillType == (int)DcsPartsLogisticBatchBillDetailBillType.供应商发运单 && r.BillId == supplierShippingOrder.Id).Select(r => r.PartsLogisticBatchId).ToArray();
            //if (partsLogisticBatchBillDetail == null || !partsLogisticBatchBillDetail.Any())
            //    throw new ValidationException(string.Format(ErrorStrings.SupplierShippingOrder_Validation10, supplierShippingOrder.Code));
            //  var partsLogisticBatchItemDetails = ObjectContext.PartsLogisticBatchItemDetails.Where(r => partsLogisticBatchBillDetail.Contains(r.PartsLogisticBatchId)).ToArray();

            foreach(var supplierShippingDetail in supplierShippingDetails) {
                //获取配件计划价
                var enterprisePartsCost = enterprisePartsCosts.FirstOrDefault(r => r.SparePartId == supplierShippingDetail.SparePartId);
                if(enterprisePartsCost == null) {
                    enterprisePartsCost = new EnterprisePartsCost {
                        OwnerCompanyId = supplierShippingOrder.BranchId,
                        PartsSalesCategoryId = supplierShippingOrder.PartsSalesCategoryId,
                        SparePartId = supplierShippingDetail.SparePartId,
                        Quantity = 0,
                        CostAmount = 0,
                        CostPrice = supplierShippingDetail.UnitPrice,
                        CreatorId = UserInfo.Id,
                        CreatorName = UserInfo.Name,
                        CreateTime = DateTime.Now
                    };
                    InsertToDatabase(enterprisePartsCost);
                }
                //获取配件销售订单清单
                var partsSalesOrderDetail = partsSalesOrderDetails.FirstOrDefault(r => r.SparePartId == supplierShippingDetail.SparePartId);
                if(partsSalesOrderDetail == null)
                    throw new ValidationException(string.Format(ErrorStrings.SupplierShippingOrder_Validation12, supplierShippingOrder.Code, supplierShippingDetail.SparePartCode));
                ////获取配件物流批次配件清单
                //var partsLogisticBatchItemDetail = partsLogisticBatchItemDetails.FirstOrDefault(r => r.SparePartId == supplierShippingDetail.SparePartId);
                //if (partsLogisticBatchItemDetail == null)
                //    throw new ValidationException(string.Format(ErrorStrings.SupplierShippingOrder_Validation13, supplierShippingOrder.Code, supplierShippingDetail.SparePartCode));
                if(supplierShippingDetail.ConfirmedAmount != null && supplierShippingDetail.ConfirmedAmount != 0) {
                    //添加配件入库计划清单
                    var partsInboundPlanDetail = new PartsInboundPlanDetail {
                        PartsInboundPlanId = partsInboundPlan.Id,
                        SparePartId = supplierShippingDetail.SparePartId,
                        SparePartCode = supplierShippingDetail.SparePartCode,
                        SparePartName = supplierShippingDetail.SparePartName,
                        PlannedAmount = supplierShippingDetail.ConfirmedAmount ?? 0,
                        InspectedQuantity = supplierShippingDetail.ConfirmedAmount ?? 0,
                        Price = supplierShippingDetail.UnitPrice,
                        POCode = supplierShippingDetail.POCode,
                        Remark = supplierShippingDetail.Remark,
                        OriginalPrice = partsSalesOrderDetail.OriginalPrice

                    };
                    partsInboundPlan.PartsInboundPlanDetails.Add(partsInboundPlanDetail);
                    //添加配件入库检验清单
                    var partsInboundCheckBillDetail = new PartsInboundCheckBillDetail {
                        PartsInboundCheckBillId = partsInboundCheckBill.Id,
                        SparePartId = supplierShippingDetail.SparePartId,
                        SparePartCode = supplierShippingDetail.SparePartCode,
                        SparePartName = supplierShippingDetail.SparePartName,
                        InspectedQuantity = supplierShippingDetail.ConfirmedAmount ?? 0,
                        SettlementPrice = supplierShippingDetail.UnitPrice,
                        WarehouseAreaId = warehouse.Id,
                        WarehouseAreaCode = warehouse.Code,
                        CostPrice = supplierShippingDetail.UnitPrice,
                        POCode = supplierShippingDetail.POCode,
                        Remark = supplierShippingDetail.Remark,
                        OriginalPrice = partsSalesOrderDetail.OriginalPrice
                    };
                    partsInboundCheckBill.PartsInboundCheckBillDetails.Add(partsInboundCheckBillDetail);
                    //添加配件出库计划清单
                    var partsOutboundPlanDetail = new PartsOutboundPlanDetail {
                        PartsOutboundPlanId = partsOutboundPlan.Id,
                        SparePartId = supplierShippingDetail.SparePartId,
                        SparePartCode = supplierShippingDetail.SparePartCode,
                        SparePartName = supplierShippingDetail.SparePartName,
                        OutboundFulfillment = supplierShippingDetail.ConfirmedAmount ?? 0,
                        PlannedAmount = supplierShippingDetail.ConfirmedAmount ?? 0,
                        Price = partsSalesOrderDetail.OrderPrice,
                        Remark = supplierShippingDetail.Remark,
                        OriginalPrice = partsSalesOrderDetail.OriginalPrice
                    };
                    partsOutboundPlan.PartsOutboundPlanDetails.Add(partsOutboundPlanDetail);
                    //添加配件出库单清单
                    var partsOutboundBillDetail = new PartsOutboundBillDetail {
                        PartsOutboundBillId = partsOutboundBill.Id,
                        SparePartId = partsSalesOrderDetail.SparePartId,
                        SparePartCode = partsSalesOrderDetail.SparePartCode,
                        SparePartName = partsSalesOrderDetail.SparePartName,
                        OutboundAmount = supplierShippingDetail.ConfirmedAmount ?? 0,
                        //  BatchNumber = partsLogisticBatchItemDetail.BatchNumber,
                        SettlementPrice = partsSalesOrderDetail.OrderPrice,
                        CostPrice = supplierShippingDetail.UnitPrice,
                        Remark = supplierShippingDetail.Remark,
                        OriginalPrice = partsSalesOrderDetail.OriginalPrice
                    };
                    partsOutboundBill.PartsOutboundBillDetails.Add(partsOutboundBillDetail);
                }


                ////添加配件批次记录
                //var partsBatchRecord = new PartsBatchRecord {
                //    PartId = partsLogisticBatchItemDetail.SparePartId,
                //    Quantity = partsLogisticBatchItemDetail.OutboundAmount,
                //    BatchNumber = partsLogisticBatchItemDetail.BatchNumber,
                //    Status = (int)DcsBaseDataStatus.有效
                //};
                //InsertToDatabase(partsBatchRecord);
                //InsertPartsBatchRecordValidate(partsBatchRecord);
            }

            //供应商发运管理：点确认的时候，判断供应商发运单.是否直供 = 是，需要查询客户账户，
            //根据供应商发运单.原始单据ID查询销售订单，再根据销售订单.客户账户ID 查询客户账户信息，更新对应客户账户的审批待发金额 = 
            //原审批待发金额- 生成的出库单金额， 对应客户账户的发出商品金额 = 原发出商品金额+出库单金额
            if(supplierShippingOrder.IfDirectProvision) {
                var customeraccount = new CustomerAccountAch(this.DomainService).GetCustomerAccountsWithLock(ObjectContext.CustomerAccounts.Where(r => r.Id == partsSalesOrder.CustomerAccountId && r.Status == (int)DcsMasterDataStatus.有效)).ToArray();
                if(!customeraccount.Any() || customeraccount.Count() > 1) {
                    throw new ValidationException(ErrorStrings.SupplierShippingOrder_Validation42);
                }
                var sumAmount = partsOutboundBill.PartsOutboundBillDetails.Sum(r => r.OutboundAmount * r.SettlementPrice);

               
                var customerAccountHisDetail = new CustomerAccountHisDetail();
                customerAccountHisDetail.CustomerAccountId = customeraccount.First().Id;
                customerAccountHisDetail.ChangeAmount = 0 - sumAmount;
                customerAccountHisDetail.ProcessDate = DateTime.Now;
                customerAccountHisDetail.CreatorName = userInfo.Name;
                customerAccountHisDetail.BusinessType = (int)DcsAccountPaymentBusinessType.销售出库;
                customerAccountHisDetail.BeforeChangeAmount = customeraccount.First().PendingAmount;
                customerAccountHisDetail.Debit = sumAmount;
                customerAccountHisDetail.InvoiceDate = DateTime.Now;
                customerAccountHisDetail.SourceId = partsSalesOrder.Id;
                customerAccountHisDetail.SourceCode = partsSalesOrder.Code;
                customerAccountHisDetail.AfterChangeAmount = customeraccount.First().PendingAmount - sumAmount;
                customerAccountHisDetail.Summary = "供应商发运确认直供订单扣减审批待发金额";
                customerAccountHisDetail.SerialType = (int)DcsSerialType.审批待发;
                DomainService.checkInsertCustomerAccountHisDetail(customerAccountHisDetail);
                InsertToDatabase(customerAccountHisDetail);

                var customerAccountHisDetailShippe = new CustomerAccountHisDetail();
                customerAccountHisDetailShippe.CustomerAccountId = customeraccount.First().Id;
                customerAccountHisDetailShippe.ChangeAmount = 0 + sumAmount;
                customerAccountHisDetailShippe.ProcessDate = DateTime.Now;
                customerAccountHisDetailShippe.CreatorName = userInfo.Name;
                customerAccountHisDetailShippe.BusinessType = (int)DcsAccountPaymentBusinessType.销售出库;
                customerAccountHisDetailShippe.BeforeChangeAmount = customeraccount.First().ShippedProductValue;
                customerAccountHisDetailShippe.Debit = sumAmount;
                customerAccountHisDetailShippe.InvoiceDate = DateTime.Now;
                customerAccountHisDetailShippe.SourceId = partsSalesOrder.Id;
                customerAccountHisDetailShippe.SourceCode = partsSalesOrder.Code;
                customerAccountHisDetailShippe.AfterChangeAmount = customeraccount.First().ShippedProductValue + sumAmount;
                customerAccountHisDetailShippe.Summary = "供应商发运确认直供订单增加发出商品金额";
                customerAccountHisDetailShippe.SerialType = (int)DcsSerialType.发出商品;
                DomainService.checkInsertCustomerAccountHisDetail(customerAccountHisDetailShippe);
                InsertToDatabase(customerAccountHisDetailShippe);

                customeraccount.First().PendingAmount -= sumAmount;
                customeraccount.First().ShippedProductValue += sumAmount;
            }
            var batchNumbers = partsOutboundBill.PartsOutboundBillDetails.Where(v => !string.IsNullOrEmpty(v.BatchNumber)).Select(v => v.BatchNumber.ToLower()).ToArray();

            var partsIds = partsOutboundBill.PartsOutboundBillDetails.Select(r => r.SparePartId).Distinct().ToArray();

               //获取配件信息，并获取总成件所有的关键代码和总成件领用关系
                var spareParts = ObjectContext.SpareParts.Where(o => partsIds.Contains(o.Id)).ToList();
                var assemblyKeyCodes = spareParts.Where(o => o.PartType == (int)DcsSparePartPartType.总成件_领用).Select(o => o.AssemblyKeyCode).ToList();
                var assemblyPartRequisitionLinks = ObjectContext.AssemblyPartRequisitionLinks.Where(o => assemblyKeyCodes.Contains(o.KeyCode) && o.Status == 1).ToList();
                //锁定配件为总成件，对应的配件库存信息
                IEnumerable<PartsStock> assemblyPartsStocks = null;
                IEnumerable<EnterprisePartsCost> assemblyEnterprisePartsCosts = null;
                var house = ObjectContext.Warehouses.Where(r => r.Name=="重庆库" && r.Status==(int)DcsBaseDataStatus.有效).FirstOrDefault();
                if(null == house) {
                    throw new ValidationException(ErrorStrings.Warehouse_Cq);
                }
                if(assemblyPartRequisitionLinks.Count() > 0) {
                    var assemblyPartsIds = assemblyPartRequisitionLinks.Select(o => o.PartId).ToList();
                    assemblyPartsStocks = new PartsStockAch(this.DomainService).GetPartsStocksWithLock(ObjectContext.PartsStocks.Where(r => r.WarehouseId == house.Id && assemblyPartsIds.Contains(r.PartId) && ObjectContext.WarehouseAreaCategories.Any(t => t.Id == r.WarehouseAreaCategoryId && t.Category == (int)DcsAreaType.保管区)).ToArray()).ToArray();
                    assemblyEnterprisePartsCosts = new EnterprisePartsCostAch(this.DomainService).GetEnterprisePartsCostsWithLock(ObjectContext.EnterprisePartsCosts.Where(r => r.OwnerCompanyId == partsOutboundPlan.StorageCompanyId && assemblyPartsIds.Contains(r.SparePartId)).ToList()).ToList();
                }

                var partsStockBatchDetails = new PartsStockBatchDetail[0];
                if(batchNumbers.Length > 0) {
                    partsStockBatchDetails = ObjectContext.PartsStockBatchDetails.Where(r => batchNumbers.Contains(r.BatchNumber.ToLower())).ToArray();
                }

                var partsAmountDetails = new Dictionary<int, decimal>();
                if(partsOutboundPlan.StorageCompanyType == (int)DcsCompanyType.分公司 || partsOutboundPlan.StorageCompanyType == (int)DcsCompanyType.代理库 || partsOutboundPlan.StorageCompanyType == (int)DcsCompanyType.服务站兼代理库) {
                    partsAmountDetails = (from a in ObjectContext.EnterprisePartsCosts.Where(r => r.OwnerCompanyId == partsOutboundBill.StorageCompanyId && r.PartsSalesCategoryId == partsOutboundBill.PartsSalesCategoryId && partsIds.Contains(r.SparePartId)).ToArray()
                                          select a).ToDictionary(r => r.SparePartId, r => r.CostPrice);
                }
           
                foreach(var partsOutboundBillDetail in partsOutboundBill.PartsOutboundBillDetails) {
                    PartsOutboundPlan newPartsOutboundPlan = null;
                    PartsOutboundBill newPartsOutboundBill = null;
                    var partsOutboundPlanDetail = partsOutboundPlan.PartsOutboundPlanDetails.FirstOrDefault(r => r.SparePartId == partsOutboundBillDetail.SparePartId);
                    if(partsOutboundPlanDetail == null)
                        throw new ValidationException(string.Format(ErrorStrings.PartsOutboundBill_Validation4, partsOutboundBillDetail.SparePartCode));
                    if(partsOutboundPlanDetail.OutboundFulfillment.Value > partsOutboundPlanDetail.PlannedAmount)
                        throw new ValidationException(string.Format(ErrorStrings.PartsOutboundBill_Validation9, partsOutboundBillDetail.SparePartCode));

                    var sparePart = spareParts.Where(o => o.PartType == (int)DcsSparePartPartType.总成件_领用).FirstOrDefault(o => o.Id == partsOutboundBillDetail.SparePartId);
                        foreach(var assemblyPartRequisitionLink in assemblyPartRequisitionLinks.Where(o => o.KeyCode == sparePart.AssemblyKeyCode).ToList()) {
                            var assemblyPartsStock = assemblyPartsStocks.Where(o => o.PartId == assemblyPartRequisitionLink.PartId).ToList();
                            var assemblyEnterprisePartsCost = assemblyEnterprisePartsCosts.FirstOrDefault(o => o.SparePartId == assemblyPartRequisitionLink.PartId);
                            var qty = (assemblyPartRequisitionLink.Qty ?? 0) * partsOutboundBillDetail.OutboundAmount;
                            if(qty > assemblyPartsStock.Sum(o => o.Quantity - (o.LockedQty ?? 0))) {
                                throw new ValidationException(string.Format(ErrorStrings.AssemblyPartRequisitionLink_Validation6, partsOutboundBillDetail.SparePartCode, assemblyPartRequisitionLink.PartCode));
                            } else {
                                if(newPartsOutboundPlan == null) {
                                    var internalAllocationBill = ObjectContext.InternalAllocationBills.FirstOrDefault(o => o.SourceCode == supplierShippingOrder.OriginalRequirementBillCode && ObjectContext.InternalAllocationDetails.Any(t => t.InternalAllocationBillId == o.Id && t.SparePartId == assemblyPartRequisitionLink.PartId));
                                    if(null == internalAllocationBill) {
                                        throw new ValidationException(string.Format(ErrorStrings.InternalAllocationBill_SparePart_NotFound, partsOutboundBillDetail.SparePartCode));
                                    }                                  
                                    newPartsOutboundPlan = new PartsOutboundPlan {
                                        WarehouseId = internalAllocationBill.WarehouseId,
                                        WarehouseName = internalAllocationBill.WarehouseName,
                                        WarehouseCode = house.Code,
                                        StorageCompanyId = partsOutboundPlan.StorageCompanyId,
                                        CompanyAddressId = partsOutboundPlan.CompanyAddressId,
                                        StorageCompanyCode = partsOutboundPlan.StorageCompanyCode,
                                        StorageCompanyName = partsOutboundPlan.StorageCompanyName,
                                        StorageCompanyType = partsOutboundPlan.StorageCompanyType,
                                        BranchId = partsOutboundPlan.BranchId,
                                        BranchCode = partsOutboundPlan.BranchCode,
                                        BranchName = partsOutboundPlan.BranchName,
                                        CounterpartCompanyId = 361,
                                        CounterpartCompanyCode = "XSFWZ",
                                        CounterpartCompanyName = "销售服务组",
                                        SourceId = internalAllocationBill.Id,
                                        SourceCode = internalAllocationBill.Code,
                                        OutboundType = (int)DcsPartsOutboundType.内部领出,
                                        CustomerAccountId = partsOutboundPlan.CustomerAccountId,
                                        OriginalRequirementBillId = internalAllocationBill.Id,
                                        OriginalRequirementBillCode = internalAllocationBill.Code,
                                        OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.内部领出单,
                                        ReceivingCompanyId = partsOutboundPlan.ReceivingCompanyId,
                                        ReceivingCompanyCode = partsOutboundPlan.ReceivingCompanyCode,
                                        ReceivingCompanyName = partsOutboundPlan.ReceivingCompanyName,
                                        Status = (int)DcsPartsOutboundPlanStatus.出库完成,
                                        ShippingMethod = partsOutboundPlan.ShippingMethod,
                                        IfWmsInterface = partsOutboundPlan.IfWmsInterface,
                                        PartsSalesCategoryId = partsOutboundPlan.PartsSalesCategoryId,
                                        PartsSalesOrderTypeId = partsOutboundPlan.PartsSalesOrderTypeId,
                                        PartsSalesOrderTypeName = partsOutboundPlan.PartsSalesOrderTypeName,
                                        OrderApproveComment = partsOutboundPlan.OrderApproveComment,
                                        SAPPurchasePlanCode = partsOutboundPlan.SAPPurchasePlanCode,
                                        Remark = partsOutboundPlan.Remark,
                                        ERPSourceOrderCode = partsOutboundPlan.ERPSourceOrderCode,
                                        ReceivingAddress = partsOutboundPlan.ReceivingAddress
                                    };
                                    InsertToDatabase(newPartsOutboundPlan);
                                    new PartsOutboundPlanAch(this.DomainService).InsertPartsOutboundPlanValidate(newPartsOutboundPlan);

                                    newPartsOutboundBill = new PartsOutboundBill();
                                   // newPartsOutboundBill.Code = CodeGenerator.Generate("PartsOutboundBill", newPartsOutboundPlan.WarehouseCode);
                                    newPartsOutboundBill.WarehouseName = newPartsOutboundPlan.WarehouseName;
                                    newPartsOutboundBill.OutboundType = newPartsOutboundPlan.OutboundType;
                                    newPartsOutboundBill.WarehouseCode = newPartsOutboundPlan.WarehouseCode;
                                    newPartsOutboundBill.PartsOutboundPlanId = newPartsOutboundPlan.Id;
                                    newPartsOutboundBill.WarehouseId = newPartsOutboundPlan.WarehouseId;
                                    newPartsOutboundBill.StorageCompanyId = newPartsOutboundPlan.StorageCompanyId;
                                    newPartsOutboundBill.StorageCompanyCode = newPartsOutboundPlan.StorageCompanyCode;
                                    newPartsOutboundBill.StorageCompanyName = newPartsOutboundPlan.StorageCompanyName;
                                    newPartsOutboundBill.StorageCompanyType = newPartsOutboundPlan.StorageCompanyType;
                                    newPartsOutboundBill.BranchId = newPartsOutboundPlan.BranchId;
                                    newPartsOutboundBill.BranchCode = newPartsOutboundPlan.BranchCode;
                                    newPartsOutboundBill.BranchName = newPartsOutboundPlan.BranchName;
                                    newPartsOutboundBill.GPMSPurOrderCode = newPartsOutboundPlan.GPMSPurOrderCode;
                                    newPartsOutboundBill.CounterpartCompanyId = newPartsOutboundPlan.CounterpartCompanyId;
                                    newPartsOutboundBill.CounterpartCompanyCode = newPartsOutboundPlan.CounterpartCompanyCode;
                                    newPartsOutboundBill.CounterpartCompanyName = newPartsOutboundPlan.CounterpartCompanyName;
                                    newPartsOutboundBill.CustomerAccountId = newPartsOutboundPlan.CustomerAccountId;
                                    newPartsOutboundBill.OriginalRequirementBillId = newPartsOutboundPlan.OriginalRequirementBillId;
                                    newPartsOutboundBill.OriginalRequirementBillType = newPartsOutboundPlan.OriginalRequirementBillType;
                                    newPartsOutboundBill.OriginalRequirementBillCode = newPartsOutboundPlan.OriginalRequirementBillCode;
                                    newPartsOutboundBill.ReceivingCompanyId = newPartsOutboundPlan.ReceivingCompanyId;
                                    newPartsOutboundBill.ReceivingCompanyCode = newPartsOutboundPlan.ReceivingCompanyCode;
                                    newPartsOutboundBill.ReceivingCompanyName = newPartsOutboundPlan.ReceivingCompanyName;
                                    newPartsOutboundBill.ReceivingWarehouseId = newPartsOutboundPlan.ReceivingWarehouseId;
                                    newPartsOutboundBill.ReceivingWarehouseCode = newPartsOutboundPlan.ReceivingWarehouseCode;
                                    newPartsOutboundBill.ReceivingWarehouseName = newPartsOutboundPlan.ReceivingWarehouseName;
                                    newPartsOutboundBill.SettlementStatus = (int)DcsPartsSettlementStatus.待结算;
                                    newPartsOutboundBill.ShippingMethod = newPartsOutboundPlan.ShippingMethod;
                                    newPartsOutboundBill.PartsSalesCategoryId = newPartsOutboundPlan.PartsSalesCategoryId;
                                    newPartsOutboundBill.PartsSalesOrderTypeId = newPartsOutboundPlan.PartsSalesOrderTypeId;
                                    newPartsOutboundBill.PartsSalesOrderTypeName = newPartsOutboundPlan.PartsSalesOrderTypeName;
                                    newPartsOutboundBill.PartsSalesOrderTypeId = newPartsOutboundPlan.PartsSalesOrderTypeId;
                                    newPartsOutboundBill.PartsSalesOrderTypeName = newPartsOutboundPlan.PartsSalesOrderTypeName;
                                    newPartsOutboundBill.OrderApproveComment = newPartsOutboundPlan.OrderApproveComment;
                                    newPartsOutboundBill.Remark = newPartsOutboundPlan.Remark;
                                    newPartsOutboundBill.SAPPurchasePlanCode = newPartsOutboundPlan.SAPPurchasePlanCode;
                                    newPartsOutboundBill.ERPSourceOrderCode = newPartsOutboundPlan.ERPSourceOrderCode;
                                    InsertToDatabase(newPartsOutboundBill);
                                    new PartsOutboundBillAch(this.DomainService).InsertPartsOutboundBillValidate(newPartsOutboundBill);
                                }

                                var newPartsOutboundPlanDetail = new PartsOutboundPlanDetail {
                                    PartsOutboundPlan = newPartsOutboundPlan,
                                    SparePartId = assemblyPartRequisitionLink.PartId,
                                    SparePartCode = assemblyPartRequisitionLink.PartCode,
                                    SparePartName = assemblyPartRequisitionLink.PartName,
                                    PlannedAmount = qty,
                                    Price = assemblyEnterprisePartsCost.CostPrice
                                };
                                newPartsOutboundPlan.PartsOutboundPlanDetails.Add(newPartsOutboundPlanDetail);

                                if(assemblyEnterprisePartsCost.Quantity < qty) {
                                    throw new ValidationException(string.Format("配件{0}企业库存不足！", assemblyPartRequisitionLink.PartCode));
                                }
                                assemblyEnterprisePartsCost.Quantity = assemblyEnterprisePartsCost.Quantity - qty;
                                assemblyEnterprisePartsCost.CostAmount -= qty * assemblyEnterprisePartsCost.CostPrice;
                                UpdateToDatabase(assemblyEnterprisePartsCost);

                                foreach(var newAssemblypartsStock in assemblyPartsStock) {
                                    if(newAssemblypartsStock.Quantity - (newAssemblypartsStock.LockedQty ?? 0) > 0) {
                                        var newPartsOutboundBillDetail = new PartsOutboundBillDetail();
                                        newPartsOutboundBillDetail.PartsOutboundBill = newPartsOutboundBill;
                                        newPartsOutboundBillDetail.SparePartId = assemblyPartRequisitionLink.PartId;
                                        newPartsOutboundBillDetail.SparePartCode = assemblyPartRequisitionLink.PartCode;
                                        newPartsOutboundBillDetail.SparePartName = assemblyPartRequisitionLink.PartName;
                                        newPartsOutboundBillDetail.WarehouseAreaCode = newAssemblypartsStock.WarehouseArea.Code;
                                        newPartsOutboundBillDetail.WarehouseAreaId = newAssemblypartsStock.WarehouseAreaId;
                                        newPartsOutboundBillDetail.SettlementPrice = assemblyEnterprisePartsCost.CostPrice;
                                        newPartsOutboundBillDetail.CostPrice = assemblyEnterprisePartsCost.CostPrice;

                                        //剩余出库数量
                                        var currentOutboundAmount = newPartsOutboundPlanDetail.PlannedAmount - (newPartsOutboundPlanDetail.OutboundFulfillment ?? 0);
                                        if(currentOutboundAmount == 0) {
                                            break;
                                        } else {
                                            var availableStock = newAssemblypartsStock.Quantity - (newAssemblypartsStock.LockedQty ?? 0);
                                            if(currentOutboundAmount > availableStock) {
                                                newAssemblypartsStock.Quantity = 0;
                                                newAssemblypartsStock.ModifierId = userInfo.Id;
                                                newAssemblypartsStock.ModifierName = userInfo.Name;
                                                newAssemblypartsStock.ModifyTime = DateTime.Now;
                                                UpdateToDatabase(newAssemblypartsStock);

                                                newPartsOutboundBillDetail.OutboundAmount = availableStock;
                                            } else {
                                                newAssemblypartsStock.Quantity = newAssemblypartsStock.Quantity - currentOutboundAmount;
                                                newAssemblypartsStock.ModifierId = userInfo.Id;
                                                newAssemblypartsStock.ModifierName = userInfo.Name;
                                                newAssemblypartsStock.ModifyTime = DateTime.Now;
                                                UpdateToDatabase(newAssemblypartsStock);

                                                newPartsOutboundBillDetail.OutboundAmount = currentOutboundAmount;
                                            }
                                            newPartsOutboundPlanDetail.OutboundFulfillment = (newPartsOutboundPlanDetail.OutboundFulfillment ?? 0) + newPartsOutboundBillDetail.OutboundAmount;
                                        }
                                        newPartsOutboundBill.PartsOutboundBillDetails.Add(newPartsOutboundBillDetail);
                                    }
                                }
                            }
                        }
                        ObjectContext.SaveChanges();
                }

            InsertToDatabase(partsInboundPlan);
            new PartsInboundPlanAch(this.DomainService).InsertPartsInboundPlanValidate(partsInboundPlan);

            partsInboundPlan.PartsInboundCheckBills.Add(partsInboundCheckBill);
            new PartsInboundCheckBillAch(this.DomainService).InsertPartsInboundCheckBillValidate(partsInboundCheckBill);

            InsertToDatabase(partsOutboundPlan);
            new PartsOutboundPlanAch(this.DomainService).InsertPartsOutboundPlanValidate(partsOutboundPlan);

            partsOutboundPlan.PartsOutboundBills.Add(partsOutboundBill);
            new PartsOutboundBillAch(this.DomainService).InsertPartsOutboundBillValidate(partsOutboundBill);

        }


        public void 作废供应商发运单(SupplierShippingOrder supplierShippingOrder) {
            var dbsupplierShippingDetails = ObjectContext.SupplierShippingDetails.Where(r => r.SupplierShippingOrderId == supplierShippingOrder.Id).ToArray();
            var dbSupplierShippingOrder = ObjectContext.SupplierShippingOrders.Where(r => r.Id == supplierShippingOrder.Id && r.Status != (int)DcsSupplierShippingOrderStatus.新建).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbSupplierShippingOrder != null)
                throw new ValidationException(string.Format(ErrorStrings.SupplierShippingOrder_Validation3));
            var partsPurchaseOrder = ObjectContext.PartsPurchaseOrders.Where(r => r.Id == supplierShippingOrder.PartsPurchaseOrderId).Include("PartsPurchaseOrderDetails").SingleOrDefault();
            if(partsPurchaseOrder == null)
                throw new ValidationException(string.Format(ErrorStrings.SupplierShippingOrder_Validation6, supplierShippingOrder.Code));
            var partsPurchaseOrderDetails = partsPurchaseOrder.PartsPurchaseOrderDetails.ToArray();
            foreach(var supplierShippingDetail in dbsupplierShippingDetails) {
                var partsPurchaseOrderDetail = partsPurchaseOrderDetails.FirstOrDefault(r => r.SparePartId == supplierShippingDetail.SparePartId);
                if(partsPurchaseOrderDetail == null)
                    throw new ValidationException(string.Format(ErrorStrings.SupplierShippingOrder_Validation7, partsPurchaseOrder.Code, supplierShippingDetail.SparePartCode));
                if(partsPurchaseOrderDetail.ShippingAmount == null)
                    partsPurchaseOrderDetail.ShippingAmount = 0;
                partsPurchaseOrderDetail.ShippingAmount -= supplierShippingDetail.Quantity;
            }
            supplierShippingOrder.Status = (int)DcsSupplierShippingOrderStatus.作废;
            var userInfo = Utils.GetCurrentUserInfo();
            supplierShippingOrder.AbandonerId = userInfo.Id;
            supplierShippingOrder.AbandonerName = userInfo.Name;
            supplierShippingOrder.AbandonTime = DateTime.Now;
            UpdateToDatabase(supplierShippingOrder);
            this.UpdateSupplierShippingOrderValidate(supplierShippingOrder);

            partsPurchaseOrder.Status = partsPurchaseOrderDetails.Any(r => r.ShippingAmount > 0) ? (int)DcsPartsPurchaseOrderStatus.部分发运 : (int)DcsPartsPurchaseOrderStatus.确认完毕;
            if(partsPurchaseOrderDetails.Any(r => r.ShippingAmount > 0))
                partsPurchaseOrder.Status = (int)DcsPartsPurchaseOrderStatus.部分发运;
            else {
                if(partsPurchaseOrderDetails.Any(r => r.ShippingAmount == 0 && r.ConfirmedAmount < r.ShippingAmount))
                    partsPurchaseOrder.Status = (int)DcsPartsPurchaseOrderStatus.部分确认;
                if(partsPurchaseOrderDetails.All(r => r.ShippingAmount == r.ConfirmedAmount))
                    partsPurchaseOrder.Status = (int)DcsPartsPurchaseOrderStatus.确认完毕;
            }
            new PartsPurchaseOrderAch(this.DomainService).UpdatePartsPurchaseOrderValidate(partsPurchaseOrder);
        }


        public void 终止供应商发运单(VirtualSupplierShippingOrder supplierShippingOrder) {
            var dbSupplierShippingOrder = ObjectContext.SupplierShippingOrders.Where(r => r.Id == supplierShippingOrder.Id && (r.Status == (int)DcsPartsPurchaseOrderStatus.部分发运 || r.Status == (int)DcsPartsPurchaseOrderStatus.新增)).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbSupplierShippingOrder == null) {
                throw new ValidationException(ErrorStrings.SupplierShippingOrder_Validation43);
            }
            //如果 是否直供 = 是 
            //查询客户账户（id=销售订单.客户账户Id(销售订单.编号=供应商发运单.原始需求单据编号)) 审批待发金额-=sum((供应商发运单清单.数量-确认量）*配件销售订单清单.订货价格)
            //查询配件销售订单，配件采购订单.原始需求单据Id=配件销售订单.Id，获取对应配件的订单价格和客户帐户Id
            if(dbSupplierShippingOrder.IfDirectProvision) {
                //获取配件销售订单
                var partsSalesOrder = ObjectContext.PartsSalesOrders.SingleOrDefault(r => r.Id == dbSupplierShippingOrder.OriginalRequirementBillId && r.Status != (int)DcsPartsSalesOrderStatus.作废);
                if(partsSalesOrder == null)
                    throw new ValidationException(string.Format(ErrorStrings.SupplierShippingOrder_Validation14, dbSupplierShippingOrder.Code));
                //获取销售订单清单数据
                var partsSalesOrderDetails = ObjectContext.PartsSalesOrderDetails.Where(r => r.PartsSalesOrderId == partsSalesOrder.Id).ToArray();

                var customeraccount = new CustomerAccountAch(this.DomainService).GetCustomerAccountsWithLock(ObjectContext.CustomerAccounts.Where(r => r.Id == partsSalesOrder.CustomerAccountId && r.Status == (int)DcsMasterDataStatus.有效)).ToArray();
                if(!customeraccount.Any() || customeraccount.Count() > 1) {
                    throw new ValidationException(ErrorStrings.SupplierShippingOrder_Validation42);
                }

                var amount = (from a in dbSupplierShippingOrder.SupplierShippingDetails
                              join b in partsSalesOrderDetails on a.SparePartId equals b.SparePartId
                              select new {
                                  a,
                                  b
                              }).Sum(r => (r.a.Quantity - (r.a.ConfirmedAmount ?? 0)) * r.b.OrderPrice);

                var userInfo = Utils.GetCurrentUserInfo();
                var customerAccountHisDetail = new CustomerAccountHisDetail();
                customerAccountHisDetail.CustomerAccountId = customeraccount.First().Id;
                customerAccountHisDetail.ChangeAmount = 0 - amount;
                customerAccountHisDetail.ProcessDate = DateTime.Now;
                customerAccountHisDetail.CreatorName = userInfo.Name;
                customerAccountHisDetail.BusinessType = (int)DcsAccountPaymentBusinessType.终止销售出库计划;
                customerAccountHisDetail.BeforeChangeAmount = customeraccount.First().PendingAmount;
                customerAccountHisDetail.Debit = amount;
                customerAccountHisDetail.InvoiceDate = DateTime.Now;
                customerAccountHisDetail.SourceId = partsSalesOrder.Id;
                customerAccountHisDetail.SourceCode = partsSalesOrder.Code;
                customerAccountHisDetail.AfterChangeAmount = customeraccount.First().PendingAmount - amount;
                customerAccountHisDetail.Summary = "终止供应商直供发运单，解锁审批待发！";
                customerAccountHisDetail.SerialType = (int)DcsSerialType.审批待发;
                DomainService.checkInsertCustomerAccountHisDetail(customerAccountHisDetail);
                InsertToDatabase(customerAccountHisDetail);

                customeraccount.First().PendingAmount -= amount;
                UpdateToDatabase(customeraccount.First());
                customeraccount.First().ModifierId = userInfo.Id;
                customeraccount.First().ModifierName = userInfo.Name;
                customeraccount.First().ModifyTime = DateTime.Now;
            }
            dbSupplierShippingOrder.Status = (int)DcsSupplierShippingOrderStatus.终止;
            UpdateToDatabase(dbSupplierShippingOrder);
            this.UpdateSupplierShippingOrderValidate(dbSupplierShippingOrder);
            ObjectContext.SaveChanges();
        }

        public void 汇总生成供应商发运单(PartsPurchaseOrderDetail[] lartsPurchaseOrderDetails, int EnterpriseId, string EnterpriseCode, string EnterpriseName, DateTime? ShippingRequestedDeliveryTime, int? shippingMethod, string Driver, string DeliveryBillNumber, string Phone, string LogisticCompany, string VehicleLicensePlate, DateTime? PlanDeliveryTime, string ShippingRemark) {
          using(var transaction = new TransactionScope()) {
            int x = 0;
            int y = 0;
            int PartsPurchaseOrderTypeId = 0;
            var purchaseOrderCode = lartsPurchaseOrderDetails.First();
            var purchaseOrder = ObjectContext.PartsPurchaseOrders.Where(t => t.Code == purchaseOrderCode.PartsPurchaseOrderCode).First();
            EnterpriseId = purchaseOrder.PartsSupplierId;
            EnterpriseCode = purchaseOrder.PartsSupplierCode;
            EnterpriseName = purchaseOrder.PartsSupplierName;
            //string PartsPurchaseOrderType = "";
            var partsPurchaseOrderCodes = lartsPurchaseOrderDetails.Select(r => r.PartsPurchaseOrderCode).ToArray();
            //var SparePartIds = lartsPurchaseOrderDetails.Select(r => r.SparePartId).ToArray();
            //获取所有采购订单信息
            var PartsPurchaseOrderByCodes = ObjectContext.PartsPurchaseOrders.Include("PartsPurchaseOrderType").Where(r => partsPurchaseOrderCodes.Contains(r.Code)).OrderBy(r => r.Code);
            if(PartsPurchaseOrderByCodes == null) {
                throw new ValidationException(ErrorStrings.SupplierShippingOrder_Validation44);
            }
            //查询所有配件信息
            var spIds = lartsPurchaseOrderDetails.Select(r => r.SparePartId).Distinct().ToArray();
            var sparts = ObjectContext.SpareParts.Where(r => spIds.Contains(r.Id)).ToArray();
            foreach(var partsPurchaseOrder in PartsPurchaseOrderByCodes) {
                //校验发运量必须是配件主包装属性.系数的整数倍
                var partsBranchPackingProps = ObjectContext.PartsBranchPackingProps.Where(o => ObjectContext.PartsBranches.Any(r => r.BranchId == partsPurchaseOrder.BranchId
                        && r.PartsSalesCategoryId == partsPurchaseOrder.PartsSalesCategoryId && r.Status == (int)DcsMasterDataStatus.有效
                        && r.MainPackingType == o.PackingType && r.PartId == o.SparePartId)).ToArray();

                //0.判断采购订单状态是否符合发运
                if(partsPurchaseOrder.Status != (int)DcsPartsPurchaseOrderStatus.确认完毕 && partsPurchaseOrder.Status != (int)DcsPartsPurchaseOrderStatus.部分发运 && partsPurchaseOrder.Status != (int)DcsPartsPurchaseOrderStatus.部分确认)
                    throw new ValidationException(string.Format(ErrorStrings.SupplierShippingOrder_Validation45, partsPurchaseOrder.Code));
                var userInfo = Utils.GetCurrentUserInfo();
                //1.配件发运主单构建
                var supplierShippingOrder = new SupplierShippingOrder();
                supplierShippingOrder.Id = x++;
                supplierShippingOrder.BranchId = partsPurchaseOrder.BranchId;
                supplierShippingOrder.BranchCode = partsPurchaseOrder.BranchCode;
                supplierShippingOrder.BranchName = partsPurchaseOrder.BranchName;
                supplierShippingOrder.ReceivingWarehouseId = partsPurchaseOrder.WarehouseId;
                supplierShippingOrder.ReceivingWarehouseName = partsPurchaseOrder.WarehouseName;
                supplierShippingOrder.PartsPurchaseOrderId = partsPurchaseOrder.Id;
                supplierShippingOrder.PartsPurchaseOrderCode = partsPurchaseOrder.Code;
                supplierShippingOrder.GPMSPurOrderCode = partsPurchaseOrder.GPMSPurOrderCode;
                supplierShippingOrder.IfDirectProvision = partsPurchaseOrder.IfDirectProvision;
                supplierShippingOrder.PartsSupplierId = EnterpriseId;
                supplierShippingOrder.PartsSupplierCode = EnterpriseCode;
                supplierShippingOrder.PartsSupplierName = EnterpriseName;
                supplierShippingOrder.PartsSalesCategoryId = partsPurchaseOrder.PartsSalesCategoryId;
                supplierShippingOrder.PartsSalesCategoryName = partsPurchaseOrder.PartsSalesCategoryName;
                supplierShippingOrder.ReceivingAddress = partsPurchaseOrder.ReceivingAddress;
                supplierShippingOrder.PlanSource = partsPurchaseOrder.PlanSource;
                supplierShippingOrder.DirectRecWarehouseId = partsPurchaseOrder.DirectRecWarehouseId;
                supplierShippingOrder.DirectRecWarehouseName = partsPurchaseOrder.DirectRecWarehouseName;
                supplierShippingOrder.SAPPurchasePlanCode = partsPurchaseOrder.PurOrderCode;//SAP采购计划单号
                supplierShippingOrder.OriginalRequirementBillId = partsPurchaseOrder.Id;//采购订单ID
                supplierShippingOrder.OriginalRequirementBillCode = partsPurchaseOrder.Code;//采购订单编号
                supplierShippingOrder.OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.配件采购订单;
                supplierShippingOrder.RequestedDeliveryTime = ShippingRequestedDeliveryTime ?? partsPurchaseOrder.RequestedDeliveryTime;//要求到达时间
                supplierShippingOrder.ShippingMethod = shippingMethod ?? partsPurchaseOrder.ShippingMethod ?? 0;//发运方式
                supplierShippingOrder.Driver = Driver;//司机
                supplierShippingOrder.DeliveryBillNumber = DeliveryBillNumber;//送货单号
                supplierShippingOrder.Phone = Phone;//电话
                supplierShippingOrder.LogisticCompany = LogisticCompany;//物流公司
                supplierShippingOrder.VehicleLicensePlate = VehicleLicensePlate;//车牌号
                supplierShippingOrder.PlanDeliveryTime = PlanDeliveryTime ?? DateTime.Now;//预计到达时间
                supplierShippingOrder.Remark = ShippingRemark;//备注
                supplierShippingOrder.ComfirmStatus = (int)DCSSupplierShippingOrderComfirmStatus.新建;

                if(supplierShippingOrder.IfDirectProvision) {
                    supplierShippingOrder.Status = (int)DcsSupplierShippingOrderStatus.新建;
                    supplierShippingOrder.OriginalRequirementBillId = partsPurchaseOrder.OriginalRequirementBillId.Value;
                    supplierShippingOrder.OriginalRequirementBillCode = partsPurchaseOrder.OriginalRequirementBillCode;
                    supplierShippingOrder.OriginalRequirementBillType = partsPurchaseOrder.OriginalRequirementBillType.Value;
                    //直供发运单生成的时候，发运方式，根据原始需求单据编号，查询销售订单，取销售订单的发运方式。
                    var salesOrder = ObjectContext.PartsSalesOrders.Where(r => r.Id == supplierShippingOrder.OriginalRequirementBillId && r.Code == supplierShippingOrder.OriginalRequirementBillCode).FirstOrDefault();
                    supplierShippingOrder.ShippingMethod = salesOrder.ShippingMethod;
                } else {
                    supplierShippingOrder.Status = (int)DcsSupplierShippingOrderStatus.收货确认;
                }
                PartsPurchaseOrderTypeId = partsPurchaseOrder.PartsPurchaseOrderTypeId;//订单类型
                //PartsPurchaseOrderType = partsPurchaseOrder.PartsPurchaseOrderType;//订单类型

                if(supplierShippingOrder.IfDirectProvision)//收货单位Id与名称
                {
                    supplierShippingOrder.ReceivingCompanyId = partsPurchaseOrder.ReceivingCompanyId;
                    supplierShippingOrder.ReceivingCompanyName = partsPurchaseOrder.ReceivingCompanyName;
                } else {
                    supplierShippingOrder.ReceivingCompanyId = partsPurchaseOrder.BranchId;
                    supplierShippingOrder.ReceivingCompanyName = partsPurchaseOrder.BranchName;
                }
                if(partsPurchaseOrder.PartsPurchaseOrderType.Name != "正常订单"  ) {
                    if(partsPurchaseOrder.ShippingMethod.HasValue) {
                        supplierShippingOrder.ShippingMethod = partsPurchaseOrder.ShippingMethod.Value;
                    } else
                        supplierShippingOrder.ShippingMethod = 0;
                }

                //2.构建配件发运清单
                var lartsPurchaseOrderDetailSparePartCodes = lartsPurchaseOrderDetails.Where(o => o.PartsPurchaseOrderCode == partsPurchaseOrder.Code).Select(v => v.SparePartCode);
                IEnumerable<PartsPurchaseOrderDetail> PartsPurchaseOrderDetailForShippings = ObjectContext.PartsPurchaseOrderDetails.Where(r => r.PartsPurchaseOrderId == partsPurchaseOrder.Id && lartsPurchaseOrderDetailSparePartCodes.Contains(r.SparePartCode)).ToArray();
                if(PartsPurchaseOrderDetailForShippings == null)
                    throw new ValidationException(ErrorStrings.SupplierShippingOrder_Validation46);
                foreach(var PartsPurchaseOrderDetailForShipping in PartsPurchaseOrderDetailForShippings) {
                    var partsBranchPackingProp = partsBranchPackingProps.Where(o => o.SparePartId == PartsPurchaseOrderDetailForShipping.SparePartId).FirstOrDefault();
                    if(partsBranchPackingProp == null)
                        throw new ValidationException(string.Format(ErrorStrings.PartsPurchaseOrder_Validation33,PartsPurchaseOrderDetailForShipping.SparePartCode));

                    //填充供应商发运清单
                    var shippingDetail = new SupplierShippingDetail {
                        SerialNumber = supplierShippingOrder.SupplierShippingDetails.Any() ? supplierShippingOrder.SupplierShippingDetails.Max(detail => detail.SerialNumber) + 1 : 1,
                        SparePartId = PartsPurchaseOrderDetailForShipping.SparePartId,
                        SparePartCode = PartsPurchaseOrderDetailForShipping.SparePartCode,
                        SparePartName = PartsPurchaseOrderDetailForShipping.SparePartName,
                        SupplierPartCode = PartsPurchaseOrderDetailForShipping.SupplierPartCode,
                        MeasureUnit = PartsPurchaseOrderDetailForShipping.MeasureUnit,
                        POCode = PartsPurchaseOrderDetailForShipping.POCode,
                        UnitPrice = PartsPurchaseOrderDetailForShipping.UnitPrice,
                        ConfirmedAmount = 0//确认量默认0

                    };
                    var detailLarts = lartsPurchaseOrderDetails.Where(r => r.SparePartId == PartsPurchaseOrderDetailForShipping.SparePartId && r.PartsPurchaseOrderCode == supplierShippingOrder.PartsPurchaseOrderCode).FirstOrDefault();
                    shippingDetail.Quantity = detailLarts.UnShippingAmount;
                    shippingDetail.TraceCode = detailLarts.TraceCode;
                    shippingDetail.TraceProperty = detailLarts.TraceProperty;
                    if(!supplierShippingOrder.IfDirectProvision) {
                        shippingDetail.ConfirmedAmount = detailLarts.UnShippingAmount;
                    }
                    var sp = sparts.Where(r => r.Id == shippingDetail.SparePartId).FirstOrDefault();
                    shippingDetail.Volume = sp.Volume * shippingDetail.Quantity;
                    shippingDetail.Weight = sp.Weight * shippingDetail.Quantity;
                    if((partsPurchaseOrder.PartsPurchaseOrderType.Name.IndexOf("急") < 0 && partsPurchaseOrder.PartsPurchaseOrderType.Name != "中心库采购") && shippingDetail.Quantity % partsBranchPackingProp.PackingCoefficient > 0) {
                        throw new ValidationException(string.Format(ErrorStrings.SupplierShippingOrder_Validation47, shippingDetail.SupplierPartCode, partsBranchPackingProp.PackingCoefficient));
                    }
                    //foreach (var lartsPurchaseOrderDetail in lartsPurchaseOrderDetails)
                    //{
                    //    if (lartsPurchaseOrderDetail.SparePartId == PartsPurchaseOrderDetailForShipping.SparePartId && lartsPurchaseOrderDetail.PartsPurchaseOrderCode == supplierShippingOrder.PartsPurchaseOrderCode)
                    //    {
                    //        shippingDetail.Quantity = lartsPurchaseOrderDetail.UnShippingAmount;

                    //        shippingDetail.ConfirmedAmount = lartsPurchaseOrderDetail.UnShippingAmount;

                    //    }
                    //}
                    ////3.修改采购清单发运数量及确认数量
                    //var dbpartsPurchaseOrderDetail = partsPurchaseOrderDetails.FirstOrDefault(r => r.SparePartId == shippingDetail.SparePartId && r.PartsPurchaseOrderId == supplierShippingOrder.PartsPurchaseOrderId);
                    //if (dbpartsPurchaseOrderDetail == null)
                    //    throw new ValidationException(string.Format(ErrorStrings.SupplierShippingOrder_Validation7, partsPurchaseOrder.Code, shippingDetail.SparePartCode));
                    //if (dbpartsPurchaseOrderDetail.ShippingAmount == null)
                    //    dbpartsPurchaseOrderDetail.ShippingAmount = 0;
                    //if (dbpartsPurchaseOrderDetail.OrderAmount < shippingDetail.Quantity + dbpartsPurchaseOrderDetail.ShippingAmount.Value)
                    //    throw new ValidationException(string.Format("{0}编号为“{1}”的配件，本次发运量超过待运数量", partsPurchaseOrder.Code, dbpartsPurchaseOrderDetail.SparePartCode));
                    //dbpartsPurchaseOrderDetail.ShippingAmount += shippingDetail.Quantity;
                    //dbpartsPurchaseOrderDetail.ConfirmedAmount += shippingDetail.Quantity;

                    //*********配件发运清单新增***********
                    InsertToDatabase(shippingDetail);
                    supplierShippingOrder.SupplierShippingDetails.Add(shippingDetail);
                }
                supplierShippingOrder.TotalWeight = supplierShippingOrder.SupplierShippingDetails.Sum(r => r.Weight);
                supplierShippingOrder.TotalVolume = supplierShippingOrder.SupplierShippingDetails.Sum(r => r.Volume);
                var supplierShippingDetails = supplierShippingOrder.SupplierShippingDetails;
                //3.修改采购订单状态
                #region 修改采购清单发运数量及确认数量
                var partsPurchaseOrderDetails = partsPurchaseOrder.PartsPurchaseOrderDetails.ToArray();
                foreach(var supplierShippingDetail in supplierShippingDetails) {
                    var dbpartsPurchaseOrderDetail = partsPurchaseOrderDetails.FirstOrDefault(r => r.SparePartId == supplierShippingDetail.SparePartId && r.PartsPurchaseOrderId == supplierShippingOrder.PartsPurchaseOrderId);
                    if(dbpartsPurchaseOrderDetail == null)
                        throw new ValidationException(string.Format(ErrorStrings.SupplierShippingOrder_Validation7, partsPurchaseOrder.Code, supplierShippingDetail.SparePartCode));
                    if(dbpartsPurchaseOrderDetail.ShippingAmount == null)
                        dbpartsPurchaseOrderDetail.ShippingAmount = 0;
                    if(dbpartsPurchaseOrderDetail.OrderAmount < supplierShippingDetail.Quantity + dbpartsPurchaseOrderDetail.ShippingAmount.Value)
                        throw new ValidationException(string.Format(ErrorStrings.SupplierShippingOrder_Validation48, partsPurchaseOrder.Code, dbpartsPurchaseOrderDetail.SparePartCode));
                    dbpartsPurchaseOrderDetail.ShippingAmount += supplierShippingDetail.Quantity;
                    //dbpartsPurchaseOrderDetail.ConfirmedAmount += supplierShippingDetail.Quantity;
                }
                #endregion
                partsPurchaseOrder.Status = partsPurchaseOrderDetails.Any(r => r.ConfirmedAmount != r.ShippingAmount) ? (int)DcsPartsPurchaseOrderStatus.部分发运 : (int)DcsPartsPurchaseOrderStatus.发运完毕;
                //*********采购订单状态变更保存***********
                new PartsPurchaseOrderAch(this.DomainService).UpdatePartsPurchaseOrderValidate(partsPurchaseOrder);

                //5.保存配件发运单信息
                InsertToDatabase(supplierShippingOrder);
                new SupplierShippingOrderAch(this.DomainService).InsertSupplierShippingOrderValidate(supplierShippingOrder);//*********新增供应商发运单保存***********

                //4.生成入库计划单信息
                var dbWarehouse = ObjectContext.Warehouses.FirstOrDefault(r => r.Id == supplierShippingOrder.ReceivingWarehouseId);
                if(dbWarehouse == null)
                    throw new ValidationException(ErrorStrings.PartsShippingOrder_Validation16);
                //var dbSupplierAccount = ObjectContext.SupplierAccounts.FirstOrDefault(r => r.BuyerCompanyId == supplierShippingOrder.BranchId && r.SupplierCompanyId == supplierShippingOrder.PartsSupplierId);
                //if (dbSupplierAccount == null)
                //    throw new ValidationException("供应商账户不存在");
                if(!supplierShippingOrder.IfDirectProvision) {
                    var newPartsInboundPlan = new PartsInboundPlan();
                    newPartsInboundPlan.Id = y++;
                    newPartsInboundPlan.WarehouseId = supplierShippingOrder.ReceivingWarehouseId;
                    newPartsInboundPlan.WarehouseCode = dbWarehouse.Code;
                    newPartsInboundPlan.WarehouseName = supplierShippingOrder.ReceivingWarehouseName;
                    newPartsInboundPlan.StorageCompanyId = supplierShippingOrder.BranchId;
                    newPartsInboundPlan.StorageCompanyCode = supplierShippingOrder.BranchCode;
                    newPartsInboundPlan.StorageCompanyName = supplierShippingOrder.BranchName;
                    newPartsInboundPlan.StorageCompanyType = (int)DcsCompanyType.分公司;
                    newPartsInboundPlan.BranchId = supplierShippingOrder.BranchId;
                    newPartsInboundPlan.BranchCode = supplierShippingOrder.BranchCode;
                    newPartsInboundPlan.BranchName = supplierShippingOrder.BranchName;
                    newPartsInboundPlan.PartsSalesCategoryId = supplierShippingOrder.PartsSalesCategoryId;
                    newPartsInboundPlan.CounterpartCompanyId = supplierShippingOrder.PartsSupplierId;
                    newPartsInboundPlan.CounterpartCompanyCode = supplierShippingOrder.PartsSupplierCode;
                    newPartsInboundPlan.CounterpartCompanyName = supplierShippingOrder.PartsSupplierName;
                    newPartsInboundPlan.SourceId = supplierShippingOrder.Id;
                    newPartsInboundPlan.SourceCode = supplierShippingOrder.Code;
                    newPartsInboundPlan.InboundType = (int)DcsPartsInboundType.配件采购;
                    //newPartsInboundPlan.CustomerAccountId = dbSupplierAccount.Id;
                    newPartsInboundPlan.OriginalRequirementBillId = supplierShippingOrder.PartsPurchaseOrderId;
                    newPartsInboundPlan.OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.配件采购订单;
                    newPartsInboundPlan.OriginalRequirementBillCode = supplierShippingOrder.PartsPurchaseOrderCode;
                    newPartsInboundPlan.Status = (int)DcsPartsInboundPlanStatus.新建;
                    newPartsInboundPlan.IfWmsInterface = dbWarehouse.WmsInterface;
                    newPartsInboundPlan.ArrivalDate = supplierShippingOrder.ArrivalDate;
                    newPartsInboundPlan.Remark = supplierShippingOrder.Remark;
                    newPartsInboundPlan.GPMSPurOrderCode = supplierShippingOrder.GPMSPurOrderCode;
                    newPartsInboundPlan.SAPPurchasePlanCode = supplierShippingOrder.SAPPurchasePlanCode;
                    newPartsInboundPlan.PartsPurchaseOrderTypeId = PartsPurchaseOrderTypeId;//采购订单类型
                    newPartsInboundPlan.Remark = ShippingRemark;//备注
                    newPartsInboundPlan.PlanDeliveryTime = PlanDeliveryTime;
                    //*********入库计划单新增***********
                    InsertToDatabase(newPartsInboundPlan);
                    new PartsInboundPlanAch(this.DomainService).InsertPartsInboundPlanValidate(newPartsInboundPlan);   
                    foreach(var supplierShippingDetail in supplierShippingDetails) {
                        var newPartsInboundPlanDetail = new PartsInboundPlanDetail();
                        newPartsInboundPlanDetail.SparePartId = supplierShippingDetail.SparePartId;
                        newPartsInboundPlanDetail.SparePartCode = supplierShippingDetail.SparePartCode;
                        newPartsInboundPlanDetail.SparePartName = supplierShippingDetail.SparePartName;
                        newPartsInboundPlanDetail.PlannedAmount = supplierShippingDetail.Quantity;
                        newPartsInboundPlanDetail.Price = supplierShippingDetail.UnitPrice;
                        newPartsInboundPlanDetail.Remark = supplierShippingDetail.Remark;
                        newPartsInboundPlanDetail.POCode = supplierShippingDetail.POCode;

                        var dbpartsPurchaseOrderDetail = partsPurchaseOrderDetails.FirstOrDefault(r => r.SparePartId == supplierShippingDetail.SparePartId);
                        newPartsInboundPlanDetail.OriginalPlanNumber = dbpartsPurchaseOrderDetail.OriginalPlanNumber;

                        newPartsInboundPlan.PartsInboundPlanDetails.Add(newPartsInboundPlanDetail);
                        InsertToDatabase(newPartsInboundPlanDetail);
                        ObjectContext.SaveChanges();
                        //新建精确码追溯
                       // var oldAccurateTrace = ObjectContext.AccurateTraces.Where(t => t.Status == (int)DcsBaseDataStatus.有效).ToList();
                        var oldTraceTempType = ObjectContext.TraceTempTypes.Where(t => t.Status == (int)DcsBaseDataStatus.有效).ToList();
                        if(!string.IsNullOrEmpty(supplierShippingDetail.TraceCode) && supplierShippingDetail.TraceProperty.HasValue) {
                            var traceCode = supplierShippingDetail.TraceCode.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries).ToArray();
                            foreach(var code in traceCode) {
                              //  if(oldAccurateTrace.Any(t => t.TraceCode == code) || oldTraceTempType.Any(t => t.TraceCode == code)) {
                                  //  throw new ValidationException("历史数据已存在追溯码：" + code);
                               // } else {
                                    var newAccurateTrace = new AccurateTrace {
                                        Type = (int)DCSAccurateTraceType.入库,
                                        SourceBillId = newPartsInboundPlan.Id,
                                        PartId = supplierShippingDetail.SparePartId,
                                        TraceCode = code,
                                        CompanyType = (int)DCSAccurateTraceCompanyType.配件中心,
                                        CompanyId = newPartsInboundPlan.StorageCompanyId,
                                        Status = (int)DCSAccurateTraceStatus.有效,
                                        TraceProperty = supplierShippingDetail.TraceProperty
                                    };
                                    new AccurateTraceAch(this.DomainService).ValidataAccurateTrace(newAccurateTrace);
                              //  }
                            }
                        }
                    }
                                  
                    
                } else {
                    //新建追溯码临时表
                    //var oldAccurateTrace = ObjectContext.AccurateTraces.Where(t => t.Status == (int)DcsBaseDataStatus.有效).ToList();
                    //var oldTraceTempType = ObjectContext.TraceTempTypes.Where(t => t.Status == (int)DcsBaseDataStatus.有效).ToList();
                    //foreach(var detail in supplierShippingDetails) {
                    //    var traceCode = detail.TraceCode.Split(';').ToList();
                    //    foreach(var code in traceCode) {
                    //        if(oldAccurateTrace.Any(t => t.TraceCode == code) || oldTraceTempType.Any(t => t.TraceCode == code)) {
                    //            throw new ValidationException("历史数据已存在追溯码：" + code);
                    //        } else {
                    //            var newTraceTempType = new TraceTempType {
                    //                Type = (int)DCSTraceTempType.直供,
                    //                SourceBillId = supplierShippingOrder.Id,
                    //                SourceBillDetailId = detail.Id,
                    //                PartId = detail.SparePartId,
                    //                TraceCode = code,
                    //                Status=(int)DcsBaseDataStatus.有效,
                    //                TraceProperty = detail.TraceProperty
                    //            };
                    //            new TraceTempTypeAch(this.DomainService).ValidataraceTempType(newTraceTempType);
                    //        }
                    //    }
                    //}
                }
            }
            ObjectContext.SaveChanges();//方法提交
            transaction.Complete();
          }
        }
      
        public void 供应商确认供应商直供发运单(SupplierShippingOrder supplierShippingOrder) {
            var oldShip = ObjectContext.SupplierShippingOrders.Where(t => t.Id == supplierShippingOrder.Id).First();
            if(oldShip.ComfirmStatus == (int)DCSSupplierShippingOrderComfirmStatus.确认完成) {
                throw new ValidationException("发运单已确认完，请重新刷新数据" );
            }
            var userInfo = Utils.GetCurrentUserInfo();
            var company = ObjectContext.Companies.Where(t => t.Id == userInfo.EnterpriseId).First();
            if(company.Type==(int)DcsCompanyType.配件供应商) {
                supplierShippingOrder.ArriveMethod = (int)DCSSupplierShippingOrderArriveMethod.供应商确认;
            } else supplierShippingOrder.ArriveMethod = (int)DCSSupplierShippingOrderArriveMethod.供货计划组确认;
            this.UpdateSupplierShippingOrderValidate(supplierShippingOrder);
            supplierShippingOrder.SupplierComfirmId = userInfo.Id;
            supplierShippingOrder.SupplierComfirm = userInfo.Name;
            supplierShippingOrder.SupplierComfirmDate = DateTime.Now;
            supplierShippingOrder.ComfirmStatus = (int)DCSSupplierShippingOrderComfirmStatus.确认完成;
            UpdateToDatabase(supplierShippingOrder);
        }
        public void 修改供应商直供发运单(SupplierShippingOrder supplierShippingOrder) {
            var oldShip = ObjectContext.SupplierShippingOrders.Where(t => t.Id == supplierShippingOrder.Id).First();
            if(oldShip.ModifyStatus.HasValue && oldShip.ModifyStatus == (int)DCSSupplierShippingOrderModifyStatus.审核通过) {
                throw new ValidationException("发运单已审核完毕，不允许修改");
            }
            var userInfo = Utils.GetCurrentUserInfo();
            this.UpdateSupplierShippingOrderValidate(supplierShippingOrder);
            supplierShippingOrder.SupplierModifyId = userInfo.Id;
            supplierShippingOrder.SupplierModify = userInfo.Name;
            supplierShippingOrder.SupplierModifyDate = DateTime.Now;
            supplierShippingOrder.ModifyStatus = (int)DCSSupplierShippingOrderModifyStatus.已修改;
            UpdateToDatabase(supplierShippingOrder);
        }
        public void 审核供应商直供发运单(SupplierShippingOrder supplierShippingOrder) {
            var oldShip = ObjectContext.SupplierShippingOrders.Where(t => t.Id == supplierShippingOrder.Id).First();
            if(oldShip.ModifyStatus.HasValue && oldShip.ModifyStatus == (int)DCSSupplierShippingOrderModifyStatus.审核通过) {
                throw new ValidationException("发运单已审核完毕，不允许审核");
            }
            var userInfo = Utils.GetCurrentUserInfo();
            this.UpdateSupplierShippingOrderValidate(supplierShippingOrder);
            supplierShippingOrder.ApproveId = userInfo.Id;
            supplierShippingOrder.Approver = userInfo.Name;
            supplierShippingOrder.ApproveDate = DateTime.Now;
            supplierShippingOrder.ModifyStatus = (int)DCSSupplierShippingOrderModifyStatus.审核通过;
            UpdateToDatabase(supplierShippingOrder);
        }
        public void 驳回供应商直供发运单(SupplierShippingOrder supplierShippingOrder) {
            var oldShip = ObjectContext.SupplierShippingOrders.Where(t => t.Id == supplierShippingOrder.Id).First();
            if(oldShip.ModifyStatus.HasValue && oldShip.ModifyStatus == (int)DCSSupplierShippingOrderModifyStatus.审核通过) {
                throw new ValidationException("发运单已审核完毕，不允许驳回");
            }
            var userInfo = Utils.GetCurrentUserInfo();
            this.UpdateSupplierShippingOrderValidate(supplierShippingOrder);
            supplierShippingOrder.ApproveId = userInfo.Id;
            supplierShippingOrder.Approver = userInfo.Name;
            supplierShippingOrder.ApproveDate = DateTime.Now;
            supplierShippingOrder.ModifyStatus = null;
            supplierShippingOrder.ModifyArrive = null;
            UpdateToDatabase(supplierShippingOrder);
        }
        public void 修改供应商发运单物流(SupplierShippingOrder supplierShippingOrder) {
            var oldShip = ObjectContext.SupplierShippingOrders.Where(t => t.Id == supplierShippingOrder.Id).First();
            this.UpdateSupplierShippingOrderValidate(oldShip);
            oldShip.LogisticCompany = supplierShippingOrder.LogisticCompany;
            oldShip.DeliveryBillNumber = supplierShippingOrder.DeliveryBillNumber;
            UpdateToDatabase(oldShip);
        }
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:11:30
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 生成供应商发运单(SupplierShippingOrder supplierShippingOrder) {
            new SupplierShippingOrderAch(this).生成供应商发运单(supplierShippingOrder);
        }


        [Update(UsingCustomMethod = true)]
        public void 修改供应商发运单(SupplierShippingOrder supplierShippingOrder) {
            new SupplierShippingOrderAch(this).修改供应商发运单(supplierShippingOrder);
        }


        [Update(UsingCustomMethod = true)]
        public void 确认供应商发运单(SupplierShippingOrder supplierShippingOrder) {
            new SupplierShippingOrderAch(this).确认供应商发运单(supplierShippingOrder);
        }


        [Update(UsingCustomMethod = true)]
        public void 确认供应商直供发运单(SupplierShippingOrder supplierShippingOrder) {
            new SupplierShippingOrderAch(this).确认供应商直供发运单(supplierShippingOrder);
        }


        [Update(UsingCustomMethod = true)]
        public void 完成供应商直供发运单(SupplierShippingOrder supplierShippingOrder) {
            new SupplierShippingOrderAch(this).完成供应商直供发运单(supplierShippingOrder);
        }


        [Update(UsingCustomMethod = true)]
        public void 作废供应商发运单(SupplierShippingOrder supplierShippingOrder) {
            new SupplierShippingOrderAch(this).作废供应商发运单(supplierShippingOrder);
        }


        [Update(UsingCustomMethod = true)]
        public void 终止供应商发运单(VirtualSupplierShippingOrder supplierShippingOrder) {
            new SupplierShippingOrderAch(this).终止供应商发运单(supplierShippingOrder);
        }

        [Invoke(HasSideEffects = true)]
        public void 汇总生成供应商发运单(PartsPurchaseOrderDetail[] lartsPurchaseOrderDetails, int EnterpriseId, string EnterpriseCode, string EnterpriseName, DateTime? ShippingRequestedDeliveryTime, int? shippingMethod, string Driver, string DeliveryBillNumber, string Phone, string LogisticCompany, string VehicleLicensePlate, DateTime? PlanDeliveryTime, string ShippingRemark) {
            new SupplierShippingOrderAch(this).汇总生成供应商发运单(lartsPurchaseOrderDetails, EnterpriseId, EnterpriseCode, EnterpriseName, ShippingRequestedDeliveryTime, shippingMethod, Driver, DeliveryBillNumber, Phone, LogisticCompany, VehicleLicensePlate, PlanDeliveryTime, ShippingRemark);
        }
         [Update(UsingCustomMethod = true)]
        public void 修改供应商发运单2(SupplierShippingOrder supplierShippingOrder) {
            new SupplierShippingOrderAch(this).修改供应商发运单2(supplierShippingOrder);
        }
         [Update(UsingCustomMethod = true)]
        public void 供应商确认供应商直供发运单(SupplierShippingOrder supplierShippingOrder) {
             new SupplierShippingOrderAch(this).供应商确认供应商直供发运单(supplierShippingOrder);
         }
         [Update(UsingCustomMethod = true)]
         public void 修改供应商直供发运单(SupplierShippingOrder supplierShippingOrder) {
             new SupplierShippingOrderAch(this).修改供应商直供发运单(supplierShippingOrder);
         }
         [Update(UsingCustomMethod = true)]
         public void 审核供应商直供发运单(SupplierShippingOrder supplierShippingOrder) {
             new SupplierShippingOrderAch(this).审核供应商直供发运单(supplierShippingOrder);
         }
         [Update(UsingCustomMethod = true)]
         public void 驳回供应商直供发运单(SupplierShippingOrder supplierShippingOrder) {
             new SupplierShippingOrderAch(this).驳回供应商直供发运单(supplierShippingOrder);
         }
         [Update(UsingCustomMethod = true)]
         public void 修改供应商发运单物流(SupplierShippingOrder supplierShippingOrder) {
             new SupplierShippingOrderAch(this).修改供应商发运单物流(supplierShippingOrder);
         }
    }
}
