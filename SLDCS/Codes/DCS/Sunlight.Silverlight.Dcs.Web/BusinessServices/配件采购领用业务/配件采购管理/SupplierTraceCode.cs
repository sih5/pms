﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using System.Text;
using FTService;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class SupplierTraceCodeAch : DcsSerivceAchieveBase {
        public SupplierTraceCodeAch(DcsDomainService domainService)
            : base(domainService) {
        }
        /// <summary>
        /// 新增
        /// </summary>
        /// <param name="supplierTraceCode"></param>
        public void 新增修正单(SupplierTraceCode supplierTraceCode) {
            var userInfo = Utils.GetCurrentUserInfo();
            if(userInfo.Id == 0)
                throw new ValidationException("用户已超时，请重新登录");
            this.InsertSupplierTraceCode(supplierTraceCode);
            //检验新追溯码是否已存在
            var detail = supplierTraceCode.SupplierTraceCodeDetails;
            var oldAccurateTrace = ObjectContext.AccurateTraces.Where(t => t.Status == (int)DCSAccurateTraceStatus.有效 && t.CompanyId == userInfo.EnterpriseId).ToArray();
            var oldTraceTempType = ObjectContext.TraceTempTypes.Where(t => t.Status == (int)DcsBaseDataStatus.有效).ToArray();
            var packingCodes = detail.Select(r => r.PackingCode);
            var pckingTasks = ObjectContext.PackingTasks.Where(t => packingCodes.Contains(t.Code)).ToArray();
            foreach(var item in detail) {
                if(oldAccurateTrace.Any(r => r.TraceCode == item.NewTraceCode) || oldTraceTempType.Any(t => t.TraceCode == item.NewTraceCode)) {
                    throw new ValidationException("追溯码：" + item.NewTraceCode + "已存在");
                }
                //修改包装单状态
                var pack = pckingTasks.Where(t => t.Code == item.PackingCode).FirstOrDefault();
                if(pack == null) {
                    throw new ValidationException("包装任务单：" + item.PackingCode + "不存在");
                }
                if(pack.Status != (int)DcsPackingTaskStatus.新增 && pack.Status != (int)DcsPackingTaskStatus.部分包装) {
                    throw new ValidationException("包装任务单：" + item.PackingCode + "已包装完成");
                }

            }
            ObjectContext.SaveChanges();
        }
        /// <summary>
        /// 提交
        /// </summary>
        /// <param name="ids"></param>
        public void SubmitSupplierTraceCode(int[] ids) {
            var userInfo = Utils.GetCurrentUserInfo();
            if(userInfo.Id == 0)
                throw new ValidationException("用户已超时，请重新登录");
            var supplierTraceCodes = ObjectContext.SupplierTraceCodes.Where(t => ids.Contains(t.Id)).ToArray();
            foreach(var item in supplierTraceCodes) {
                var trace = supplierTraceCodes.Where(t => t.Id == item.Id).FirstOrDefault();
                if(trace.Status != (int)DCSSupplierTraceCodeStatus.新增) {
                    throw new ValidationException("非新增状态不允许提交");
                }
                item.Status = (int)DCSSupplierTraceCodeStatus.提交;
                item.SubmitterId = userInfo.Id;
                item.SubmitterName = userInfo.Name;
                item.SubmitTime = DateTime.Now;
                this.UpdateSupplierTraceCodeValidate(item);
            }
            ObjectContext.SaveChanges();
        }
        /// <summary>
        /// 作废
        /// </summary>
        /// <param name="ids"></param>
        public void AbandonSupplierTraceCode(int[] ids) {
            var userInfo = Utils.GetCurrentUserInfo();
            if(userInfo.Id == 0)
                throw new ValidationException("用户已超时，请重新登录");
            var supplierTraceCodes = ObjectContext.SupplierTraceCodes.Where(t => ids.Contains(t.Id)).ToArray();
            foreach(var item in supplierTraceCodes) {
                var trace = supplierTraceCodes.Where(t => t.Id == item.Id).FirstOrDefault();
                if(trace.Status != (int)DCSSupplierTraceCodeStatus.新增) {
                    throw new ValidationException("非新增状态不允许作废");
                }
                item.Status = (int)DCSSupplierTraceCodeStatus.已作废;
                item.AbandonerId = userInfo.Id;
                item.AbandonerName = userInfo.Name;
                item.AbandonTime = DateTime.Now;
                this.UpdateSupplierTraceCodeValidate(item);
            }
            ObjectContext.SaveChanges();
        }
        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="supplierTraceCode"></param>
        public void 修改修正单(SupplierTraceCode supplierTraceCode) {
            var userInfo = Utils.GetCurrentUserInfo();
            if(userInfo.Id == 0)
                throw new ValidationException("用户已超时，请重新登录");
            var oldTrac = ObjectContext.SupplierTraceCodes.Include("SupplierTraceCodeDetails").Where(t => t.Id == supplierTraceCode.Id).FirstOrDefault();
            if(null == oldTrac) {
                throw new ValidationException("未找到修正单:" + supplierTraceCode.Code);
            }
            if(oldTrac.Status != (int)DCSSupplierTraceCodeStatus.新增) {
                throw new ValidationException("非新增状态不允许修改");
            }
            //检验新追溯码是否已存在
            var details = supplierTraceCode.SupplierTraceCodeDetails;
            var oldAccurateTrace = ObjectContext.AccurateTraces.Where(t => t.Status == (int)DCSAccurateTraceStatus.有效 && t.CompanyId == userInfo.EnterpriseId).ToArray();
            var oldTraceTempType = ObjectContext.TraceTempTypes.Where(t => t.Status == (int)DcsBaseDataStatus.有效).ToArray();
            foreach(var item in details) {
                if(oldAccurateTrace.Any(r => r.TraceCode == item.NewTraceCode) || oldTraceTempType.Any(t => t.TraceCode == item.NewTraceCode)) {
                    throw new ValidationException("追溯码：" + item.NewTraceCode + "已存在");
                }
            }

            foreach(var detail in supplierTraceCode.SupplierTraceCodeDetails) {
                var newDrail = oldTrac.SupplierTraceCodeDetails.Where(r => r.Id == detail.Id).First();
                newDrail.NewTraceCode = detail.NewTraceCode;
                UpdateToDatabase(newDrail);
            }
            oldTrac.ModifierId = userInfo.Id;
            oldTrac.ModifierName = userInfo.Name;
            oldTrac.ModifyTime = DateTime.Now;
            oldTrac.Path = supplierTraceCode.Path;
            UpdateToDatabase(oldTrac);
            ObjectContext.SaveChanges();
        }
        /// <summary>
        /// 审核
        /// </summary>
        /// <param name="supplierTraceCode"></param>
        public void AutidSupplierTraceCodes(SupplierTraceCode supplierTraceCode) {
            var userInfo = Utils.GetCurrentUserInfo();
            if(userInfo.Id == 0)
                throw new ValidationException("用户已超时，请重新登录");
            var oldTrac = ObjectContext.SupplierTraceCodes.Where(t => t.Id == supplierTraceCode.Id).FirstOrDefault();
            if(null == oldTrac) {
                throw new ValidationException("未找到修正单:" + supplierTraceCode.Code);
            }
            if(oldTrac.Status != (int)DCSSupplierTraceCodeStatus.提交) {
                throw new ValidationException("非提交状态不允许审核");
            }

            oldTrac.Status = (int)DCSSupplierTraceCodeStatus.已审核;
            oldTrac.CheckerId = userInfo.Id;
            oldTrac.CheckerName = userInfo.Name;
            oldTrac.CheckTime = DateTime.Now;
            oldTrac.ApproveComment = supplierTraceCode.ApproveComment;
            this.UpdateSupplierTraceCodeValidate(oldTrac);
            ObjectContext.SaveChanges();
        }
        /// <summary>
        /// 驳回
        /// </summary>
        /// <param name="supplierTraceCode"></param>
        public void RejectSupplierTraceCodes(SupplierTraceCode supplierTraceCode) {
            var userInfo = Utils.GetCurrentUserInfo();
            if(userInfo.Id == 0)
                throw new ValidationException("用户已超时，请重新登录");
            var oldTrac = ObjectContext.SupplierTraceCodes.Where(t => t.Id == supplierTraceCode.Id).FirstOrDefault();
            if(null == oldTrac) {
                throw new ValidationException("未找到修正单:" + supplierTraceCode.Code);
            }
            if(oldTrac.Status != (int)DCSSupplierTraceCodeStatus.提交 && oldTrac.Status != (int)DCSSupplierTraceCodeStatus.已审核) {
                throw new ValidationException("该状态不允许驳回");
            }
            oldTrac.Status = (int)DCSSupplierTraceCodeStatus.新增;
            oldTrac.ApproveComment = supplierTraceCode.ApproveComment;
            oldTrac.CheckerId = null;
            oldTrac.CheckerName = null;
            oldTrac.CheckTime = null;
            oldTrac.ApproverId = null;
            oldTrac.ApproverName = null;
            oldTrac.ApproverTime = null;
            this.UpdateSupplierTraceCodeValidate(oldTrac);
            ObjectContext.SaveChanges();
        }
        /// <summary>
        /// 审批
        /// </summary>
        /// <param name="supplierTraceCode"></param>
        public void ApproveSupplierTraceCodes(SupplierTraceCode supplierTraceCode) {
            var userInfo = Utils.GetCurrentUserInfo();
            if(userInfo.Id == 0)
                throw new ValidationException("用户已超时，请重新登录");
            var oldTrac = ObjectContext.SupplierTraceCodes.Include("SupplierTraceCodeDetails").Where(t => t.Id == supplierTraceCode.Id).FirstOrDefault();
            if(null == oldTrac) {
                throw new ValidationException("未找到修正单:" + supplierTraceCode.Code);
            }
            if(oldTrac.Status != (int)DCSSupplierTraceCodeStatus.已审核) {
                throw new ValidationException("非已审核状态不允许审批");
            }
            oldTrac.Status = (int)DCSSupplierTraceCodeStatus.已审批;
            oldTrac.ApproverId = userInfo.Id;
            oldTrac.ApproverName = userInfo.Name;
            oldTrac.ApproverTime = DateTime.Now;
            oldTrac.ApproveComment = supplierTraceCode.ApproveComment;
            this.UpdateSupplierTraceCodeValidate(oldTrac);
            var oldTraceCodes = oldTrac.SupplierTraceCodeDetails.Select(t => t.OldTraceCode).ToArray();
            //修改精确追溯码
            var accurateTrace = ObjectContext.AccurateTraces.Where(t => oldTraceCodes.Contains(t.TraceCode) && t.Status == (int)DCSAccurateTraceStatus.有效 && t.SIHLabelCode == null).ToArray();
            //验证新的追溯码是否已存在
            var oldAccurateTrace = ObjectContext.AccurateTraces.Where(t => t.Status == (int)DCSAccurateTraceStatus.有效 && t.CompanyId == userInfo.EnterpriseId).ToArray();
            foreach(var detail in oldTrac.SupplierTraceCodeDetails) {
                var newTrace = accurateTrace.Where(t => t.TraceCode == detail.OldTraceCode).FirstOrDefault();
                if(null == newTrace) {
                    throw new ValidationException("未找到追溯码：" + detail.OldTraceCode + "相关的追溯信息");
                }
                if(oldAccurateTrace.Any(r => r.TraceCode == detail.NewTraceCode)) {
                    throw new ValidationException("追溯码：" + detail.NewTraceCode + "已存在追溯信息中");
                }
                newTrace.TraceCode = detail.NewTraceCode;
                newTrace.ModifierId = userInfo.Id;
                newTrace.ModifierName = userInfo.Name;
                newTrace.ModifyTime = DateTime.Now;
                UpdateToDatabase(newTrace);
            }
            ObjectContext.SaveChanges();
        }
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:34
        //原分布类的函数全部转移到Ach                                                               
        [Update(UsingCustomMethod = true)]
        public void 新增修正单(SupplierTraceCode supplierTraceCode) {
            new SupplierTraceCodeAch(this).新增修正单(supplierTraceCode);
        }
        [Update(UsingCustomMethod = true)]
        public void 修改修正单(SupplierTraceCode supplierTraceCode) {
            new SupplierTraceCodeAch(this).修改修正单(supplierTraceCode);
        }
        [Invoke(HasSideEffects = true)]
        public void SubmitSupplierTraceCode(int[] ids) {
            new SupplierTraceCodeAch(this).SubmitSupplierTraceCode(ids);
        }
        [Invoke(HasSideEffects = true)]
        public void AbandonSupplierTraceCode(int[] ids) {
            new SupplierTraceCodeAch(this).AbandonSupplierTraceCode(ids);
        }
        [Update(UsingCustomMethod = true)]
        public void AutidSupplierTraceCodes(SupplierTraceCode supplierTraceCode) {
            new SupplierTraceCodeAch(this).AutidSupplierTraceCodes(supplierTraceCode);
        }
        [Update(UsingCustomMethod = true)]
        public void ApproveSupplierTraceCodes(SupplierTraceCode supplierTraceCode) {
            new SupplierTraceCodeAch(this).ApproveSupplierTraceCodes(supplierTraceCode);
        }
        [Update(UsingCustomMethod = true)]
        public void RejectSupplierTraceCodes(SupplierTraceCode supplierTraceCode) {
            new SupplierTraceCodeAch(this).RejectSupplierTraceCodes(supplierTraceCode);
        }
    }
}
