﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using System.Text;
using FTService;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;


namespace Sunlight.Silverlight.Dcs.Web {
    partial class TemPurchaseOrderAch : DcsSerivceAchieveBase {
        public void 提交临时采购订单(int id) {
            var userInfo = Utils.GetCurrentUserInfo();
            var oldTemPurchaseOrder = ObjectContext.TemPurchaseOrders.Where(t => t.Id == id && t.Status == (int)DCSTemPurchaseOrderStatus.新增).Include("TemPurchaseOrderDetails").FirstOrDefault();
            if(oldTemPurchaseOrder == null) {
                throw new ValidationException(ErrorStrings.TemPurchasePlanOrders_Validation2);
            }
            if(oldTemPurchaseOrder.TemPurchaseOrderDetails.Any(t => t.PlanAmount > 5) && oldTemPurchaseOrder.ApproveStatus != (int)DCSTemPurchaseOrderApproveStatus.审批通过) {
                throw new ValidationException(ErrorStrings.TemPurchasePlanOrders_Validation3);
            }
            oldTemPurchaseOrder.Status = (int)DCSTemPurchaseOrderStatus.提交;
            oldTemPurchaseOrder.SubmitterId = userInfo.Id;
            oldTemPurchaseOrder.SubmitterName = userInfo.Name;
            oldTemPurchaseOrder.SubmitTime = DateTime.Now;
            this.UpdateTemPurchaseOrderValidate(oldTemPurchaseOrder);
            UpdateToDatabase(oldTemPurchaseOrder);
            ObjectContext.SaveChanges();
        }
        public void 确认临时采购订单(TemPurchaseOrder temPurchaseOrder) {
            var userInfo = Utils.GetCurrentUserInfo();
            var oldTemPurchaseOrder = ObjectContext.TemPurchaseOrders.Where(t => t.Id == temPurchaseOrder.Id && t.Status == (int)DCSTemPurchaseOrderStatus.提交).Include("TemPurchaseOrderDetails").FirstOrDefault();
            if(oldTemPurchaseOrder == null) {
                throw new ValidationException(ErrorStrings.TemPurchasePlanOrders_Validation2);
            }
            var details = temPurchaseOrder.TemPurchaseOrderDetails.ToArray();
            foreach(var detail in oldTemPurchaseOrder.TemPurchaseOrderDetails) {
                var item = details.Where(t=>t.Id==detail.Id).First();
                detail.ConfirmedAmount = item.ConfirmedAmount;
                detail.ShortSupReason = item.ShortSupReason;
              //  UpdateToDatabase(detail);
            }
            oldTemPurchaseOrder.Status = (int)DCSTemPurchaseOrderStatus.确认完毕;
            oldTemPurchaseOrder.ConfirmerId = userInfo.Id;
            oldTemPurchaseOrder.Confirmer = userInfo.Name;
            oldTemPurchaseOrder.ConfirmeTime = DateTime.Now;
            this.UpdateTemPurchaseOrderValidate(oldTemPurchaseOrder);
            UpdateToDatabase(oldTemPurchaseOrder);
            ObjectContext.SaveChanges();
        }
        public void 终止临时采购订单(TemPurchaseOrder temPurchaseOrder) {
            var dbpartsPurchaseOrder = ObjectContext.TemPurchaseOrders.Where(r => r.Id == temPurchaseOrder.Id && (r.Status == (int)DCSTemPurchaseOrderStatus.提交 || r.Status == (int)DCSTemPurchaseOrderStatus.新增 || r.Status == (int)DCSTemPurchaseOrderStatus.部分发运 || r.Status == (int)DCSTemPurchaseOrderStatus.确认完毕)).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbpartsPurchaseOrder == null)
                throw new ValidationException(ErrorStrings.PartsPurchaseOrder_Validation5);

            UpdateToDatabase(dbpartsPurchaseOrder);
            dbpartsPurchaseOrder.Status = (int)DCSTemPurchaseOrderStatus.终止;
            dbpartsPurchaseOrder.ApproveStatus = (int)DCSTemPurchaseOrderApproveStatus.终止;
            var userInfo = Utils.GetCurrentUserInfo();
            dbpartsPurchaseOrder.StopperId = userInfo.Id;
            dbpartsPurchaseOrder.Stopper = userInfo.Name;
            dbpartsPurchaseOrder.StopTime = DateTime.Now;
            dbpartsPurchaseOrder.StopReason = temPurchaseOrder.StopReason;
            this.UpdateTemPurchaseOrderValidate(dbpartsPurchaseOrder);
        }
        public void 审核临时采购订单(TemPurchaseOrder temPurchaseOrder) {
            var userInfo = Utils.GetCurrentUserInfo();
            var oldTemPurchaseOrder = ObjectContext.TemPurchaseOrders.Where(t => t.Id == temPurchaseOrder.Id && t.ApproveStatus == (int)DCSTemPurchaseOrderApproveStatus.提交).FirstOrDefault();
            if(oldTemPurchaseOrder == null) {
                throw new ValidationException(ErrorStrings.TemPurchasePlanOrders_Validation2);
            }
            oldTemPurchaseOrder.ApproveStatus = (int)DCSTemPurchaseOrderApproveStatus.审核通过;
            oldTemPurchaseOrder.ApproverId = userInfo.Id;
            oldTemPurchaseOrder.ApproverName = userInfo.Name;
            oldTemPurchaseOrder.ApproveTime = DateTime.Now;
            oldTemPurchaseOrder.RejectReason = null;
            this.UpdateTemPurchaseOrderValidate(oldTemPurchaseOrder);
            UpdateToDatabase(oldTemPurchaseOrder);
            ObjectContext.SaveChanges();
        }
        public void 审批临时采购订单(TemPurchaseOrder temPurchaseOrder) {
            var userInfo = Utils.GetCurrentUserInfo();
            var oldTemPurchaseOrder = ObjectContext.TemPurchaseOrders.Where(t => t.Id == temPurchaseOrder.Id && t.ApproveStatus == (int)DCSTemPurchaseOrderApproveStatus.审核通过).FirstOrDefault();
            if(oldTemPurchaseOrder == null) {
                throw new ValidationException(ErrorStrings.TemPurchasePlanOrders_Validation2);
            }
            oldTemPurchaseOrder.ApproveStatus = (int)DCSTemPurchaseOrderApproveStatus.审批通过;
            oldTemPurchaseOrder.CheckerId = userInfo.Id;
            oldTemPurchaseOrder.CheckerName = userInfo.Name;
            oldTemPurchaseOrder.CheckTime = DateTime.Now;
            oldTemPurchaseOrder.RejectReason = null;
            this.UpdateTemPurchaseOrderValidate(oldTemPurchaseOrder);
            UpdateToDatabase(oldTemPurchaseOrder);
            ObjectContext.SaveChanges();
        }
        public void 驳回临时采购订单(TemPurchaseOrder temPurchaseOrder) {
            var userInfo = Utils.GetCurrentUserInfo();
            var oldTemPurchaseOrder = ObjectContext.TemPurchaseOrders.Where(t => t.Id == temPurchaseOrder.Id && (t.ApproveStatus == (int)DCSTemPurchaseOrderApproveStatus.提交 || t.ApproveStatus == (int)DCSTemPurchaseOrderApproveStatus.审核通过)).FirstOrDefault();
            if(oldTemPurchaseOrder == null) {
                throw new ValidationException(ErrorStrings.TemPurchasePlanOrders_Validation2);
            }
            oldTemPurchaseOrder.ApproveStatus = (int)DCSTemPurchaseOrderApproveStatus.新增;
            oldTemPurchaseOrder.RejectReason = temPurchaseOrder.RejectReason;
            this.UpdateTemPurchaseOrderValidate(oldTemPurchaseOrder);
            UpdateToDatabase(oldTemPurchaseOrder);
            ObjectContext.SaveChanges();
        }
        public void 审核提交临时采购订单(int id) {
            var userInfo = Utils.GetCurrentUserInfo();
            var oldTemPurchaseOrder = ObjectContext.TemPurchaseOrders.Where(t => t.Id == id && t.ApproveStatus == (int)DCSTemPurchaseOrderApproveStatus.新增).FirstOrDefault();
            if(oldTemPurchaseOrder == null) {
                throw new ValidationException(ErrorStrings.TemPurchasePlanOrders_Validation2);
            }
            oldTemPurchaseOrder.ApproveStatus = (int)DCSTemPurchaseOrderApproveStatus.提交;
            oldTemPurchaseOrder.SubmitterId = userInfo.Id;
            oldTemPurchaseOrder.SubmitterName = userInfo.Name;
            oldTemPurchaseOrder.SubmitTime = DateTime.Now;
            oldTemPurchaseOrder.RejectReason = null;
            this.UpdateTemPurchaseOrderValidate(oldTemPurchaseOrder);
            UpdateToDatabase(oldTemPurchaseOrder);
            ObjectContext.SaveChanges();
        }
        public void 强制完成临时采购订单(TemPurchaseOrder temPurchaseOrder) {
            var dbpartsPurchaseOrder = ObjectContext.TemPurchaseOrders.Where(r => r.Id == temPurchaseOrder.Id && (r.Status == (int)DCSTemPurchaseOrderStatus.部分发运||(r.Status == (int)DCSTemPurchaseOrderStatus.发运完毕 && r.PartsSalesOrderCode==null))).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbpartsPurchaseOrder == null)
                throw new ValidationException("订单状态是部分发运或者是发运完成且“销售订单号”为空时，才可强制完成");

            UpdateToDatabase(dbpartsPurchaseOrder);
            dbpartsPurchaseOrder.Status = (int)DCSTemPurchaseOrderStatus.强制完成;
            var userInfo = Utils.GetCurrentUserInfo();
            dbpartsPurchaseOrder.ForcederId = userInfo.Id;
            dbpartsPurchaseOrder.Forcedder = userInfo.Name;
            dbpartsPurchaseOrder.ForcedTime = DateTime.Now;
            dbpartsPurchaseOrder.ForcedReason = temPurchaseOrder.ForcedReason;
           //查看是否已有发运单，如有，则作废
            var shipingOrders = ObjectContext.TemSupplierShippingOrders.Where(t=>t.TemPurchaseOrderId==temPurchaseOrder.Id).ToArray();
            foreach(var item in shipingOrders) {
                item.Status = (int)DCSTemSupplierShippingOrderStatus.作废;
                new TemSupplierShippingOrderAch(this.DomainService).UpdateTemSupplierShippingOrderValidate(item);
                UpdateToDatabase(item);
            }
            this.UpdateTemPurchaseOrderValidate(dbpartsPurchaseOrder);
            ObjectContext.SaveChanges();
        }
    }
    partial class DcsDomainService {
        [Invoke]
        public void 提交临时采购订单(int id) {
            new TemPurchaseOrderAch(this).提交临时采购订单(id);
        }
        [Update(UsingCustomMethod = true)]
        public void 确认临时采购订单(TemPurchaseOrder temPurchaseOrder) {
            new TemPurchaseOrderAch(this).确认临时采购订单(temPurchaseOrder);
        }
        [Update(UsingCustomMethod = true)]
        public void 终止临时采购订单(TemPurchaseOrder temPurchaseOrder) {
            new TemPurchaseOrderAch(this).终止临时采购订单(temPurchaseOrder);
        }
        [Update(UsingCustomMethod = true)]
        public void 驳回临时采购订单(TemPurchaseOrder temPurchaseOrder) {
            new TemPurchaseOrderAch(this).驳回临时采购订单(temPurchaseOrder);
        }
        [Update(UsingCustomMethod = true)]
        public void 审核临时采购订单(TemPurchaseOrder temPurchaseOrder) {
            new TemPurchaseOrderAch(this).审核临时采购订单(temPurchaseOrder);
        }
        [Update(UsingCustomMethod = true)]
        public void 审批临时采购订单(TemPurchaseOrder temPurchaseOrder) {
            new TemPurchaseOrderAch(this).审批临时采购订单(temPurchaseOrder);
        }
        [Invoke(HasSideEffects = true)]
        public void 审核提交临时采购订单(int id) {
            new TemPurchaseOrderAch(this).审核提交临时采购订单(id);
        }
        [Update(UsingCustomMethod = true)]
        public void 强制完成临时采购订单(TemPurchaseOrder temPurchaseOrder) {
            new TemPurchaseOrderAch(this).强制完成临时采购订单(temPurchaseOrder);
        }
    }
}
