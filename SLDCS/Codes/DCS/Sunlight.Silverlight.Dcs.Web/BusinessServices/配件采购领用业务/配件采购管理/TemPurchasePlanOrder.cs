﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using System.Text;
using FTService;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;



namespace Sunlight.Silverlight.Dcs.Web {
    partial class TemPurchasePlanOrderAch : DcsSerivceAchieveBase {
        public void 生成临时采购计划单(TemPurchasePlanOrder temPurchasePlanOrder) {
            if(!temPurchasePlanOrder.CustomerType.HasValue) {
                var company = ObjectContext.Companies.Where(t => t.Id == temPurchasePlanOrder.ReceCompanyId).First();
                var delaer = ObjectContext.ChannelCapabilities.Where(t => ObjectContext.DealerServiceInfoes.Any(y => y.Id == temPurchasePlanOrder.ReceCompanyId && y.ChannelCapabilityId == t.Id)).FirstOrDefault();
                if(delaer != null) {
                    if(delaer.Name.Equals("服务站")) {
                        temPurchasePlanOrder.CustomerType = (int)DCSTemPurchasePlanOrderCustomerType.服务站;
                    } else
                        temPurchasePlanOrder.CustomerType = (int)DCSTemPurchasePlanOrderCustomerType.经销商;
                } else if(company.Type == (int)DcsCompanyType.代理库) {
                    temPurchasePlanOrder.CustomerType = (int)DCSTemPurchasePlanOrderCustomerType.中心库;
                } else {
                    temPurchasePlanOrder.CustomerType = (int)DCSTemPurchasePlanOrderCustomerType.服务站;
                }
            }
            InsertToDatabase(temPurchasePlanOrder);
            this.InsertTemPurchasePlanOrderValidate(temPurchasePlanOrder);
        }
        public void 修改临时采购计划单(TemPurchasePlanOrder temPurchasePlanOrder) {
            CheckEntityState(temPurchasePlanOrder);

            var dbpartsPurchasePlan = ObjectContext.TemPurchasePlanOrders.Where(r => r.Id == temPurchasePlanOrder.Id && (r.Status == (int)DCSTemPurchasePlanOrderStatus.新建)).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbpartsPurchasePlan == null)
                throw new ValidationException(ErrorStrings.PartsPurchasePlan_Validation6);
            //因为服务端需要调用
            UpdateToDatabase(temPurchasePlanOrder);
            this.UpdateTemPurchasePlanOrderValidate(temPurchasePlanOrder);
        }
        public void 作废临时采购计划单(int partsPurchasePlanId) {
            var dbpartsPurchasePlan = ObjectContext.TemPurchasePlanOrders.Where(r => r.Id == partsPurchasePlanId && (r.Status == (int)DCSTemPurchasePlanOrderStatus.新建)).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbpartsPurchasePlan == null)
                throw new ValidationException(ErrorStrings.PartsPurchasePlan_Validation7);
            UpdateToDatabase(dbpartsPurchasePlan);
            dbpartsPurchasePlan.Status = (int)DCSTemPurchasePlanOrderStatus.作废;
            var userInfo = Utils.GetCurrentUserInfo();
            dbpartsPurchasePlan.AbandonerId = userInfo.Id;
            dbpartsPurchasePlan.AbandonerName = userInfo.Name;
            dbpartsPurchasePlan.AbandonTime = DateTime.Now;
            this.UpdateTemPurchasePlanOrderValidate(dbpartsPurchasePlan);
            ObjectContext.SaveChanges();
        }
        public void 提交临时采购计划订单(int partsPurchasePlanId) {
            var userInfo = Utils.GetCurrentUserInfo();
            var dbpartsPurchasePlan = ObjectContext.TemPurchasePlanOrders.Where(r => r.Id == partsPurchasePlanId && (r.Status == (int)DCSTemPurchasePlanOrderStatus.新建)).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbpartsPurchasePlan == null)
                throw new ValidationException(ErrorStrings.PartsPurchasePlan_Validation9);
            dbpartsPurchasePlan.SubmitterId = userInfo.Id;
            dbpartsPurchasePlan.SubmitterName = userInfo.Name;
            dbpartsPurchasePlan.SubmitTime = DateTime.Now;
            dbpartsPurchasePlan.Status = (int)DCSTemPurchasePlanOrderStatus.提交;
            if(dbpartsPurchasePlan.CustomerType == (int)DCSTemPurchasePlanOrderCustomerType.中心库) {
                dbpartsPurchasePlan.Status = (int)DCSTemPurchasePlanOrderStatus.审核通过;
            }
            dbpartsPurchasePlan.RejectId = null;
            dbpartsPurchasePlan.Rejector = null;
            dbpartsPurchasePlan.RejectTime = null;
            dbpartsPurchasePlan.RejectReason = null;
            dbpartsPurchasePlan.ApproverId = null;
            dbpartsPurchasePlan.ApproverName = null;
            dbpartsPurchasePlan.ApproveTime = null;
            dbpartsPurchasePlan.CheckerId = null;
            dbpartsPurchasePlan.CheckerName = null;
            dbpartsPurchasePlan.CheckTime = null;
            UpdateToDatabase(dbpartsPurchasePlan);
            this.UpdateTemPurchasePlanOrderValidate(dbpartsPurchasePlan);
            ObjectContext.SaveChanges();
        }
        public void 审核通过临时采购计划单(TemPurchasePlanOrder temPurchasePlanOrder) {
            var userInfo = Utils.GetCurrentUserInfo();
            var dbpartsPurchasePlan = ObjectContext.TemPurchasePlanOrders.Where(r => r.Id == temPurchasePlanOrder.Id && (r.Status == (int)DCSTemPurchasePlanOrderStatus.提交)).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbpartsPurchasePlan == null)
                throw new ValidationException(ErrorStrings.Action_Approve_Submit);
            temPurchasePlanOrder.Status = (int)DCSTemPurchasePlanOrderStatus.审核通过;
            temPurchasePlanOrder.ApproverId = userInfo.Id;
            temPurchasePlanOrder.ApproverName = userInfo.Name;
            temPurchasePlanOrder.ApproveTime = DateTime.Now;
            temPurchasePlanOrder.OrderCompanyCode = userInfo.EnterpriseCode;
            temPurchasePlanOrder.OrderCompanyId = userInfo.EnterpriseId;
            temPurchasePlanOrder.OrderCompanyName = userInfo.EnterpriseName;
            //查询中心库的收货仓库
            var warehouse = ObjectContext.Warehouses.Where(t => t.StorageCompanyId == userInfo.EnterpriseId && t.Status == (int)DcsBaseDataStatus.有效 && t.Type == (int)DcsWarehouseType.总库).FirstOrDefault();
                if(warehouse==null){
                    throw new ValidationException("请先维护中心库"+userInfo.EnterpriseName+"的总库仓库信息");
                }
                temPurchasePlanOrder.WarehouseId = warehouse.Id;
            temPurchasePlanOrder.WarehouseName = warehouse.Name;
             this.UpdateTemPurchasePlanOrderValidate(temPurchasePlanOrder);
            UpdateToDatabase(temPurchasePlanOrder);
            ObjectContext.SaveChanges();
        }
        public void 复核通过临时采购计划单(TemPurchasePlanOrder temPurchasePlanOrder) {
            var userInfo = Utils.GetCurrentUserInfo();
            var dbpartsPurchasePlan = ObjectContext.TemPurchasePlanOrders.Where(r => r.Id == temPurchasePlanOrder.Id && (r.Status == (int)DCSTemPurchasePlanOrderStatus.审核通过)).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbpartsPurchasePlan == null)
                throw new ValidationException(ErrorStrings.TemPurchasePlanOrders_Validation1);
            temPurchasePlanOrder.Status = (int)DCSTemPurchasePlanOrderStatus.复核通过;
            temPurchasePlanOrder.CheckerId = userInfo.Id;
            temPurchasePlanOrder.CheckerName = userInfo.Name;
            temPurchasePlanOrder.CheckTime = DateTime.Now;
            this.UpdateTemPurchasePlanOrderValidate(temPurchasePlanOrder);
            UpdateToDatabase(temPurchasePlanOrder);
            ObjectContext.SaveChanges();
        }
        public void 驳回临时采购计划单(TemPurchasePlanOrder temPurchasePlanOrder) {
            var userInfo = Utils.GetCurrentUserInfo();
            var dbpartsPurchasePlan = ObjectContext.TemPurchasePlanOrders.Where(r => r.Id == temPurchasePlanOrder.Id).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbpartsPurchasePlan.Status != (int)DCSTemPurchasePlanOrderStatus.提交 && dbpartsPurchasePlan.Status != (int)DCSTemPurchasePlanOrderStatus.审核通过 && dbpartsPurchasePlan.Status != (int)DCSTemPurchasePlanOrderStatus.复核通过)
                throw new ValidationException(ErrorStrings.TemPurchasePlanOrders_Validation2);
            temPurchasePlanOrder.Status = (int)DCSTemPurchasePlanOrderStatus.新建;
            if(temPurchasePlanOrder.CustomerType != (int)DCSTemPurchasePlanOrderCustomerType.中心库) {
                temPurchasePlanOrder.OrderCompanyId = null;
                temPurchasePlanOrder.OrderCompanyCode = null;
                temPurchasePlanOrder.OrderCompanyName = null;
                temPurchasePlanOrder.WarehouseId = null;
                temPurchasePlanOrder.WarehouseName = null;
            }
            temPurchasePlanOrder.RejectId = userInfo.Id;
            temPurchasePlanOrder.Rejector = userInfo.Name;
            temPurchasePlanOrder.RejectReason = temPurchasePlanOrder.RejectReason;
            temPurchasePlanOrder.RejectTime = DateTime.Now;
            this.UpdateTemPurchasePlanOrderValidate(temPurchasePlanOrder);
            UpdateToDatabase(temPurchasePlanOrder);
            ObjectContext.SaveChanges();
        }
        public void 确认临时采购计划单(TemPurchasePlanOrder temPurchasePlanOrder) {
            var userInfo = Utils.GetCurrentUserInfo();
            var dbpartsPurchasePlan = ObjectContext.TemPurchasePlanOrders.Where(r => r.Id == temPurchasePlanOrder.Id && (r.Status == (int)DCSTemPurchasePlanOrderStatus.复核通过)).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbpartsPurchasePlan == null)
                throw new ValidationException(ErrorStrings.TemPurchasePlanOrders_Validation2);
            var suplierIds = temPurchasePlanOrder.TemPurchasePlanOrderDetails.Select(t => t.SuplierId).Distinct().ToArray();
            foreach(var suplierId in suplierIds) {
                var details = temPurchasePlanOrder.TemPurchasePlanOrderDetails.Where(t=>t.SuplierId==suplierId).ToArray();
                var temPurchaseOrder = new TemPurchaseOrder {
                    TemPurchasePlanOrderId = temPurchasePlanOrder.Id,
                    TemPurchasePlanOrderCode = temPurchasePlanOrder.Code,
                    WarehouseId = temPurchasePlanOrder.WarehouseId,
                    WarehouseName = temPurchasePlanOrder.WarehouseName,
                    Status = (int)DCSTemPurchaseOrderStatus.新增,
                    ApproveStatus = (int)DCSTemPurchaseOrderApproveStatus.提交,
                    SuplierId = details.First().SuplierId,
                    SuplierCode=details.First().SuplierCode,
                    SuplierName=details.First().SuplierName,
                    OrderType = temPurchasePlanOrder.PlanType,
                    IsTurnSale = temPurchasePlanOrder.IsTurnSale,
                    ReceCompanyCode = temPurchasePlanOrder.ReceCompanyCode,
                    ReceCompanyId = temPurchasePlanOrder.ReceCompanyId,
                    ReceCompanyName = temPurchasePlanOrder.ReceCompanyName,
                    OrderCompanyCode = temPurchasePlanOrder.OrderCompanyCode,
                    OrderCompanyId = temPurchasePlanOrder.OrderCompanyId,
                    OrderCompanyName = temPurchasePlanOrder.OrderCompanyName,
                    ShippingMethod = temPurchasePlanOrder.ShippingMethod,
                    ReceiveStatus = (int)DCSTemPurchaseOrderReceiveStatus.未收货,
                    ReceiveAddress = temPurchasePlanOrder.ReceiveAddress,
                    Linker=temPurchasePlanOrder.Linker,
                    LinkPhone=temPurchasePlanOrder.LinkPhone,
                    Memo = temPurchasePlanOrder.Memo,
                    FreightType = temPurchasePlanOrder.FreightType,
                    CustomerType = temPurchasePlanOrder.CustomerType
                };
                var personnelSupplierRelation = ObjectContext.PersonnelSupplierRelations.Where(t => t.SupplierId == suplierId && t.Status == (int)DcsBaseDataStatus.有效).FirstOrDefault();
                if(personnelSupplierRelation!=null) {
                    temPurchaseOrder.Buyer = personnelSupplierRelation.PersonName;
                    temPurchaseOrder.BuyerId = personnelSupplierRelation.PersonId;
                }
                foreach(var detail in details) {
                    var temPurchaseOrderDetail = new TemPurchaseOrderDetail {
                        SparePartId=detail.SparePartId,
                        SparePartCode = detail.SparePartCode,
                        SparePartName=detail.SparePartName,
                        SupplierPartCode=detail.SupplierPartCode,
                        PlanAmount=detail.PlanAmount,
                        MeasureUnit=detail.MeasureUnit,
                        Remark=detail.Memo
                    };
                    temPurchaseOrder.TemPurchaseOrderDetails.Add(temPurchaseOrderDetail);
                }
                new TemPurchaseOrderAch(this.DomainService).InsertTemPurchaseOrderValidate(temPurchaseOrder);
                InsertToDatabase(temPurchaseOrder);
            }
            temPurchasePlanOrder.Status = (int)DCSTemPurchasePlanOrderStatus.确认通过;
            temPurchasePlanOrder.ConfirmerId = userInfo.Id;
            temPurchasePlanOrder.Confirmer = userInfo.Name;
            temPurchasePlanOrder.ConfirmeTime = DateTime.Now;
            this.UpdateTemPurchasePlanOrderValidate(temPurchasePlanOrder);
            UpdateToDatabase(temPurchasePlanOrder);
            ObjectContext.SaveChanges();
        }

    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:11:30
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 生成临时采购计划单(TemPurchasePlanOrder temPurchasePlanOrder) {
            new TemPurchasePlanOrderAch(this).生成临时采购计划单(temPurchasePlanOrder);
        }
        /// <summary>
        /// 仅供客户端使用
        /// </summary>
        [Update(UsingCustomMethod = true)]
        public void 修改临时采购计划单(TemPurchasePlanOrder temPurchasePlanOrder) {
            new TemPurchasePlanOrderAch(this).修改临时采购计划单(temPurchasePlanOrder);
        }
        /// <summary>
        /// 仅供客户端使用
        /// </summary>
        [Invoke]
        public void 作废临时采购计划单(int id) {
            new TemPurchasePlanOrderAch(this).作废临时采购计划单(id);
        }
        [Invoke]
        public void 提交临时采购计划订单(int id) {
            new TemPurchasePlanOrderAch(this).提交临时采购计划订单(id);
        }
        [Update(UsingCustomMethod = true)]
        public void 审核通过临时采购计划单(TemPurchasePlanOrder temPurchasePlanOrder) {
            new TemPurchasePlanOrderAch(this).审核通过临时采购计划单(temPurchasePlanOrder);
        }
         [Update(UsingCustomMethod = true)]
        public void 复核通过临时采购计划单(TemPurchasePlanOrder temPurchasePlanOrder) {
            new TemPurchasePlanOrderAch(this).复核通过临时采购计划单(temPurchasePlanOrder);
        }
         [Update(UsingCustomMethod = true)]
         public void 驳回临时采购计划单(TemPurchasePlanOrder temPurchasePlanOrder) {
             new TemPurchasePlanOrderAch(this).驳回临时采购计划单(temPurchasePlanOrder);
         }
         [Update(UsingCustomMethod = true)]
         public void 确认临时采购计划单(TemPurchasePlanOrder temPurchasePlanOrder) {
             new TemPurchasePlanOrderAch(this).确认临时采购计划单(temPurchasePlanOrder);
         }
    }
}
