﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
using System.Collections.Generic;
using Sunlight.Silverlight.Dcs.Web.Entities;
using System.Transactions;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class TemSupplierShippingOrderAch : DcsSerivceAchieveBase {

        public void 生成临时供应商发运单(TemSupplierShippingOrder supplierShippingOrder) {
            using(var transaction = new TransactionScope()) {
                var userInfo = Utils.GetCurrentUserInfo();
                var partsPurchaseOrder = ObjectContext.TemPurchaseOrders.Where(r => r.Id == supplierShippingOrder.TemPurchaseOrderId).Include("TemPurchaseOrderDetails").SingleOrDefault();
                if(partsPurchaseOrder == null)
                    throw new ValidationException(string.Format(ErrorStrings.SupplierShippingOrder_Validation6, supplierShippingOrder.Code));
                if(partsPurchaseOrder.Status != (int)DCSTemPurchaseOrderStatus.确认完毕 && partsPurchaseOrder.Status != (int)DCSTemPurchaseOrderStatus.部分发运)
                    throw new ValidationException(string.Format(ErrorStrings.SupplierShippingOrder_Validation1));
                var supplierShippingDetails = supplierShippingOrder.TemShippingOrderDetails;
                var partsPurchaseOrderDetails = partsPurchaseOrder.TemPurchaseOrderDetails.ToArray();
                var spids = supplierShippingDetails.Select(r => r.SparePartId).ToArray();
                var sparets = ObjectContext.SpareParts.Where(r => spids.Contains(r.Id)).ToArray();
                foreach(var supplierShippingDetail in supplierShippingDetails) {
                    var dbpartsPurchaseOrderDetail = partsPurchaseOrderDetails.FirstOrDefault(r => r.SparePartId == supplierShippingDetail.SparePartId);
                    if(dbpartsPurchaseOrderDetail == null)
                        throw new ValidationException(string.Format(ErrorStrings.SupplierShippingOrder_Validation7, partsPurchaseOrder.Code, supplierShippingDetail.SparePartCode));
                    if(dbpartsPurchaseOrderDetail.ShippingAmount == null)
                        dbpartsPurchaseOrderDetail.ShippingAmount = 0;
                    if(dbpartsPurchaseOrderDetail.ConfirmedAmount < supplierShippingDetail.Quantity + dbpartsPurchaseOrderDetail.ShippingAmount.Value)
                        throw new ValidationException(string.Format(ErrorStrings.SupplierShippingOrder_Validation4, dbpartsPurchaseOrderDetail.SparePartCode));
                    dbpartsPurchaseOrderDetail.ShippingAmount += supplierShippingDetail.Quantity;
                    var sp = sparets.Where(r => r.Id == supplierShippingDetail.SparePartId).FirstOrDefault();
                    supplierShippingDetail.Volume = sp.Volume * supplierShippingDetail.Quantity;
                    supplierShippingDetail.Weight = sp.Weight * supplierShippingDetail.Quantity;
                }
                supplierShippingOrder.TotalVolume = supplierShippingDetails.Sum(r => r.Volume);
                supplierShippingOrder.TotalWeight = supplierShippingDetails.Sum(r => r.Weight);
                partsPurchaseOrder.Status = partsPurchaseOrderDetails.Any(r => r.ConfirmedAmount != r.ShippingAmount) ? (int)DCSTemPurchaseOrderStatus.部分发运 : (int)DCSTemPurchaseOrderStatus.发运完毕;
                partsPurchaseOrder.Shipper = userInfo.Name;
                partsPurchaseOrder.ShippingId = userInfo.Id;
                partsPurchaseOrder.ShippTime = supplierShippingOrder.ShippingDate;

                new TemPurchaseOrderAch(this.DomainService).UpdateTemPurchaseOrderValidate(partsPurchaseOrder);
                var dbWarehouse = ObjectContext.Warehouses.FirstOrDefault(r => r.Id == supplierShippingOrder.ReceivingWarehouseId);
                if(dbWarehouse == null && partsPurchaseOrder.CustomerType != (int)DCSTemPurchasePlanOrderCustomerType.出口客户 && partsPurchaseOrder.CustomerType != (int)DCSTemPurchasePlanOrderCustomerType.集团企业)
                    throw new ValidationException(ErrorStrings.PartsShippingOrder_Validation16);
                supplierShippingOrder.OrderCompanyId = partsPurchaseOrder.OrderCompanyId;
                supplierShippingOrder.OrderCompanyCode = partsPurchaseOrder.OrderCompanyCode;
                supplierShippingOrder.OrderCompanyName = partsPurchaseOrder.OrderCompanyName;
                this.InsertTemSupplierShippingOrderValidate(supplierShippingOrder);
                transaction.Complete();
            }
        }
        public void 汇总生成供应商临时发运单(TemPurchaseOrderDetail[] lartsPurchaseOrderDetails, int? shippingMethod, string Driver, string DeliveryBillNumber, string Phone, string LogisticCompany, string VehicleLicensePlate, DateTime? PlanDeliveryTime, string ShippingRemark) {
            using(var transaction = new TransactionScope()) {
                int x = 0;
                var purchaseOrderCode = lartsPurchaseOrderDetails.First();
                var purchaseOrder = ObjectContext.TemPurchaseOrders.Where(t => t.Code == purchaseOrderCode.TemPurchaseOrderCode).First();
                int EnterpriseId = purchaseOrder.SuplierId.Value;
                var EnterpriseCode = purchaseOrder.SuplierCode;
                var EnterpriseName = purchaseOrder.SuplierName;
                var partsPurchaseOrderCodes = lartsPurchaseOrderDetails.Select(r => r.TemPurchaseOrderCode).ToArray();
                //获取所有采购订单信息
                var PartsPurchaseOrderByCodes = ObjectContext.TemPurchaseOrders.Where(r => partsPurchaseOrderCodes.Contains(r.Code)).OrderBy(r => r.Code);
                if(PartsPurchaseOrderByCodes == null) {
                    throw new ValidationException(ErrorStrings.SupplierShippingOrder_Validation44);
                }
                //查询所有配件信息
                var spIds = lartsPurchaseOrderDetails.Select(r => r.SparePartId).Distinct().ToArray();
                var sparts = ObjectContext.SpareParts.Where(r => spIds.Contains(r.Id)).ToArray();
                foreach(var partsPurchaseOrder in PartsPurchaseOrderByCodes) {

                    //0.判断采购订单状态是否符合发运
                    if(partsPurchaseOrder.Status != (int)DCSTemPurchaseOrderStatus.确认完毕 && partsPurchaseOrder.Status != (int)DCSTemPurchaseOrderStatus.部分发运)
                        throw new ValidationException(string.Format(ErrorStrings.SupplierShippingOrder_Validation45, partsPurchaseOrder.Code));
                    var userInfo = Utils.GetCurrentUserInfo();
                    //1.配件发运主单构建
                    var supplierShippingOrder = new TemSupplierShippingOrder();
                    supplierShippingOrder.Id = x++;
                    supplierShippingOrder.ReceivingWarehouseId = partsPurchaseOrder.WarehouseId.Value;
                    supplierShippingOrder.ReceivingWarehouseName = partsPurchaseOrder.WarehouseName;
                    supplierShippingOrder.TemPurchaseOrderId = partsPurchaseOrder.Id;
                    supplierShippingOrder.TemPurchaseOrderCode = partsPurchaseOrder.Code;
                    supplierShippingOrder.PartsSupplierId = EnterpriseId;
                    supplierShippingOrder.PartsSupplierCode = EnterpriseCode;
                    supplierShippingOrder.PartsSupplierName = EnterpriseName;
                    supplierShippingOrder.ReceivingAddress = partsPurchaseOrder.ReceiveAddress;
                    supplierShippingOrder.OriginalRequirementBillId = partsPurchaseOrder.TemPurchasePlanOrderId;//临时采购计划单ID
                    supplierShippingOrder.OriginalRequirementBillCode = partsPurchaseOrder.TemPurchasePlanOrderCode;//临时采购计划编号
                    supplierShippingOrder.ShippingMethod = shippingMethod ?? partsPurchaseOrder.ShippingMethod ?? 0;//发运方式
                    supplierShippingOrder.Driver = Driver;//司机
                    supplierShippingOrder.DeliveryBillNumber = DeliveryBillNumber;//送货单号
                    supplierShippingOrder.Phone = Phone;//电话
                    supplierShippingOrder.LogisticCompany = LogisticCompany;//物流公司
                    supplierShippingOrder.VehicleLicensePlate = VehicleLicensePlate;//车牌号
                    supplierShippingOrder.PlanDeliveryTime = PlanDeliveryTime ?? DateTime.Now;//预计到达时间
                    supplierShippingOrder.Remark = ShippingRemark;//备注
                    supplierShippingOrder.Status = (int)DCSTemSupplierShippingOrderStatus.新建;
                    supplierShippingOrder.ReceivingCompanyId = partsPurchaseOrder.ReceCompanyId;
                    supplierShippingOrder.ReceivingCompanyName = partsPurchaseOrder.ReceCompanyName;
                    supplierShippingOrder.ReceivingCompanyCode = partsPurchaseOrder.ReceCompanyCode;
                    supplierShippingOrder.OrderCompanyId = partsPurchaseOrder.OrderCompanyId;
                    supplierShippingOrder.OrderCompanyCode = partsPurchaseOrder.OrderCompanyCode;
                    supplierShippingOrder.OrderCompanyName = partsPurchaseOrder.OrderCompanyName;
                    //2.构建配件发运清单
                    var lartsPurchaseOrderDetailSparePartCodes = lartsPurchaseOrderDetails.Where(o => o.TemPurchaseOrderCode == partsPurchaseOrder.Code).Select(v => v.SparePartCode);
                    IEnumerable<TemPurchaseOrderDetail> PartsPurchaseOrderDetailForShippings = ObjectContext.TemPurchaseOrderDetails.Where(r => r.TemPurchaseOrderId == partsPurchaseOrder.Id && lartsPurchaseOrderDetailSparePartCodes.Contains(r.SparePartCode)).ToArray();
                    if(PartsPurchaseOrderDetailForShippings == null)
                        throw new ValidationException(ErrorStrings.SupplierShippingOrder_Validation46);
                    foreach(var PartsPurchaseOrderDetailForShipping in PartsPurchaseOrderDetailForShippings) {
                        if(PartsPurchaseOrderDetailForShipping.ShippingAmount == null) {
                            PartsPurchaseOrderDetailForShipping.ShippingAmount = 0;
                        }
                        //填充供应商发运清单
                        var shippingDetail = new TemShippingOrderDetail {
                            SparePartId = PartsPurchaseOrderDetailForShipping.SparePartId.Value,
                            SparePartCode = PartsPurchaseOrderDetailForShipping.SparePartCode,
                            SparePartName = PartsPurchaseOrderDetailForShipping.SparePartName,
                            SupplierPartCode = PartsPurchaseOrderDetailForShipping.SupplierPartCode,
                            MeasureUnit = PartsPurchaseOrderDetailForShipping.MeasureUnit,
                            ConfirmedAmount = 0,//确认量默认0
                            Quantity = PartsPurchaseOrderDetailForShipping.ConfirmedAmount - PartsPurchaseOrderDetailForShipping.ShippingAmount.Value
                        };

                        var sp = sparts.Where(r => r.Id == shippingDetail.SparePartId).FirstOrDefault();
                        shippingDetail.Volume = sp.Volume * shippingDetail.Quantity;
                        shippingDetail.Weight = sp.Weight * shippingDetail.Quantity;

                        //*********配件发运清单新增***********
                        InsertToDatabase(shippingDetail);
                        supplierShippingOrder.TemShippingOrderDetails.Add(shippingDetail);
                    }
                    supplierShippingOrder.TotalWeight = supplierShippingOrder.TemShippingOrderDetails.Sum(r => r.Weight);
                    supplierShippingOrder.TotalVolume = supplierShippingOrder.TemShippingOrderDetails.Sum(r => r.Volume);
                    var supplierShippingDetails = supplierShippingOrder.TemShippingOrderDetails;
                    //3.修改采购订单状态
                    #region 修改采购清单发运数量及确认数量
                    var partsPurchaseOrderDetails = partsPurchaseOrder.TemPurchaseOrderDetails.ToArray();
                    foreach(var supplierShippingDetail in supplierShippingDetails) {
                        var dbpartsPurchaseOrderDetail = partsPurchaseOrderDetails.FirstOrDefault(r => r.SparePartId == supplierShippingDetail.SparePartId && r.TemPurchaseOrderId == supplierShippingOrder.TemPurchaseOrderId);
                        if(dbpartsPurchaseOrderDetail == null)
                            throw new ValidationException(string.Format(ErrorStrings.SupplierShippingOrder_Validation7, partsPurchaseOrder.Code, supplierShippingDetail.SparePartCode));
                        if(dbpartsPurchaseOrderDetail.ShippingAmount == null)
                            dbpartsPurchaseOrderDetail.ShippingAmount = 0;
                        if(dbpartsPurchaseOrderDetail.ConfirmedAmount < supplierShippingDetail.Quantity + dbpartsPurchaseOrderDetail.ShippingAmount.Value)
                            throw new ValidationException(string.Format(ErrorStrings.SupplierShippingOrder_Validation48, partsPurchaseOrder.Code, dbpartsPurchaseOrderDetail.SparePartCode));
                        dbpartsPurchaseOrderDetail.ShippingAmount += supplierShippingDetail.Quantity;
                    }
                    #endregion
                    partsPurchaseOrder.Status = partsPurchaseOrderDetails.Any(r => r.ConfirmedAmount != r.ShippingAmount) ? (int)DCSTemPurchaseOrderStatus.部分发运 : (int)DCSTemPurchaseOrderStatus.发运完毕;
                    //*********采购订单状态变更保存***********
                    new TemPurchaseOrderAch(this.DomainService).UpdateTemPurchaseOrderValidate(partsPurchaseOrder);

                    //5.保存配件发运单信息
                    InsertToDatabase(supplierShippingOrder);
                    new TemSupplierShippingOrderAch(this.DomainService).InsertTemSupplierShippingOrderValidate(supplierShippingOrder);//*********新增供应商发运单保存***********
                }
                ObjectContext.SaveChanges();//方法提交
                transaction.Complete();
            }
        }
        public void 修改临时供应商发运单(TemSupplierShippingOrder supplierShippingOrder) {
            var oldTemSupplierShippingOrder = ObjectContext.TemSupplierShippingOrders.Where(t => t.Id == supplierShippingOrder.Id  && t.Status != (int)DCSTemSupplierShippingOrderStatus.作废).FirstOrDefault();
            if(oldTemSupplierShippingOrder==null) {
                throw new ValidationException("该状态下不允许修改");
            }
            oldTemSupplierShippingOrder.DeliveryBillNumber = supplierShippingOrder.DeliveryBillNumber;
            this.UpdateTemSupplierShippingOrderValidate(oldTemSupplierShippingOrder);
            UpdateToDatabase(oldTemSupplierShippingOrder);
            ObjectContext.SaveChanges();
        }
        public void 收货确认临时供应商发运单(TemSupplierShippingOrder supplierShippingOrder) {
            var userInfo = Utils.GetCurrentUserInfo();
            var oldsupplierShippingOrder = ObjectContext.TemSupplierShippingOrders.Where(t => t.Id == supplierShippingOrder.Id && (t.Status == (int)DCSTemSupplierShippingOrderStatus.新建 || t.Status == (int)DCSTemSupplierShippingOrderStatus.部分收货)).Include("TemShippingOrderDetails").FirstOrDefault();
            if(oldsupplierShippingOrder == null) {
                throw new ValidationException("该状态下不允许确认");
            }
            var temPurchaseOrder = ObjectContext.TemPurchaseOrders.Where(t => t.Id == supplierShippingOrder.TemPurchaseOrderId).FirstOrDefault();
            if(temPurchaseOrder==null) {
                throw new ValidationException("未找到临时订单：" + supplierShippingOrder.TemPurchaseOrderCode);
            }
            var details = supplierShippingOrder.TemShippingOrderDetails.ToArray();
            foreach(var item in oldsupplierShippingOrder.TemShippingOrderDetails){
                var detail = details.Where(t => t.Id == item.Id).FirstOrDefault();
                if(detail!=null) {
                    if(item.ConfirmedAmount==null) {
                        item.ConfirmedAmount = 0;
                    }
                    item.ConfirmedAmount = item.ConfirmedAmount + item.UnConfirmedAmount;
                    UpdateToDatabase(item);
                }
            }
            if(oldsupplierShippingOrder.TemShippingOrderDetails.All(t => t.ConfirmedAmount == t.Quantity)) {
                oldsupplierShippingOrder.Status = (int)DCSTemSupplierShippingOrderStatus.收货完成;
                oldsupplierShippingOrder.ConfirmationTime = DateTime.Now;
                temPurchaseOrder.ReceiveStatus = (int)DCSTemPurchaseOrderReceiveStatus.收货完成;
            } else {
                oldsupplierShippingOrder.Status = (int)DCSTemSupplierShippingOrderStatus.部分收货;
                temPurchaseOrder.ReceiveStatus = (int)DCSTemPurchaseOrderReceiveStatus.部分收货;
            }
            if(oldsupplierShippingOrder.FirstConfirmationTime==null) {
                oldsupplierShippingOrder.FirstConfirmationTime = DateTime.Now;
            }
            oldsupplierShippingOrder.ConfirmationTime = DateTime.Now;
            oldsupplierShippingOrder.ConfirmorId = userInfo.Id;
            oldsupplierShippingOrder.ConfirmorName = userInfo.Name;
            oldsupplierShippingOrder.Path = supplierShippingOrder.Path;
            oldsupplierShippingOrder.ArrivalDate = DateTime.Now;
            UpdateTemSupplierShippingOrderValidate(oldsupplierShippingOrder);
            UpdateToDatabase(oldsupplierShippingOrder);
            ObjectContext.SaveChanges();
        }
        public void 终止临时供应商发运单(int id) {
            var oldTemSupplierShippingOrder = ObjectContext.TemSupplierShippingOrders.Where(t => t.Id == id &&( t.Status != (int)DCSTemSupplierShippingOrderStatus.新建|| t.Status != (int)DCSTemSupplierShippingOrderStatus.部分收货)).FirstOrDefault();
            if(oldTemSupplierShippingOrder == null) {
                throw new ValidationException("该状态下不允许终止");
            }
            oldTemSupplierShippingOrder.Status = (int)DCSTemSupplierShippingOrderStatus.终止;
            this.UpdateTemSupplierShippingOrderValidate(oldTemSupplierShippingOrder);
            UpdateToDatabase(oldTemSupplierShippingOrder);
            ObjectContext.SaveChanges();
        }
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:11:30
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 生成临时供应商发运单(TemSupplierShippingOrder supplierShippingOrder) {
            new TemSupplierShippingOrderAch(this).生成临时供应商发运单(supplierShippingOrder);
        }
        [Invoke(HasSideEffects = true)]
        public void 汇总生成供应商临时发运单(TemPurchaseOrderDetail[] lartsPurchaseOrderDetails, int? shippingMethod, string Driver, string DeliveryBillNumber, string Phone, string LogisticCompany, string VehicleLicensePlate, DateTime? PlanDeliveryTime, string ShippingRemark) {
            new TemSupplierShippingOrderAch(this).汇总生成供应商临时发运单(lartsPurchaseOrderDetails, shippingMethod, Driver, DeliveryBillNumber, Phone, LogisticCompany, VehicleLicensePlate, PlanDeliveryTime, ShippingRemark);

        }
         [Update(UsingCustomMethod = true)]
         public void 修改临时供应商发运单(TemSupplierShippingOrder supplierShippingOrder) {
             new TemSupplierShippingOrderAch(this).修改临时供应商发运单(supplierShippingOrder);
        }
         [Update(UsingCustomMethod = true)]
         public void 收货确认临时供应商发运单(TemSupplierShippingOrder supplierShippingOrder) {
             new TemSupplierShippingOrderAch(this).收货确认临时供应商发运单(supplierShippingOrder);
         }
          [Invoke(HasSideEffects = true)]
         public void 终止临时供应商发运单(int id) {
             new TemSupplierShippingOrderAch(this).终止临时供应商发运单(id);
         }
    }
}

