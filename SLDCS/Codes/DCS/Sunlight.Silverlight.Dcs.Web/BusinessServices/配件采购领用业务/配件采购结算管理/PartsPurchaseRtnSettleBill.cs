﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsPurchaseRtnSettleBillAch : DcsSerivceAchieveBase {
        public PartsPurchaseRtnSettleBillAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 生成配件采购退货结算单(PartsPurchaseRtnSettleBill partsPurchaseRtnSettleBill) {
            if (null != partsPurchaseRtnSettleBill.InvoiceDate.Value)
            {
                //当过帐日期不为空时，验证过帐日期是否有效
                this.DomainService.验证过账日期是否有效(partsPurchaseRtnSettleBill.InvoiceDate);
            }
            var partsPurchaseRtnSettleRefs = partsPurchaseRtnSettleBill.PartsPurchaseRtnSettleRefs;
            var checkSourceCode = partsPurchaseRtnSettleRefs.GroupBy(r => r.SourceCode).Any(r => r.Count() > 1);
            if(checkSourceCode) {
                throw new ValidationException(ErrorStrings.PartsPurchaseRtnSettleBill_Validation11);
            }
            var sourceIds = partsPurchaseRtnSettleRefs.Select(r => r.SourceId);
            var partsOutboundBills = ObjectContext.PartsOutboundBills.Where(r => sourceIds.Contains(r.Id));
            //var partsOutboundBillIds = partsOutboundBills.Select(r => r.Id).ToArray();
            //var listOfOutboundAndInboundDetail =DomainService. 查询待结算出入库明细清单后定价(new int[0], partsOutboundBillIds, partsPurchaseRtnSettleBill.PartsSalesCategoryId.Value, partsPurchaseRtnSettleBill.PartsSupplierId);
            //var allPartsOutboundBillDetails = ObjectContext.PartsOutboundBillDetails.Where(r => partsOutboundBillIds.Contains(r.PartsOutboundBillId)).ToArray();
            foreach(var partsPurchaseRtnSettleRef in partsPurchaseRtnSettleRefs) {
                var partsOutboundBill = partsOutboundBills.SingleOrDefault(r => r.Id == partsPurchaseRtnSettleRef.SourceId && r.SettlementStatus == (int)DcsPartsSettlementStatus.待结算);
                if(partsOutboundBill == null)
                    throw new ValidationException(ErrorStrings.PartsPurchaseRtnSettleBill_Validation1);
                partsOutboundBill.SettlementStatus = (int)DcsPartsSettlementStatus.已结算;
                //var tempPartsOutboundBillDetails = allPartsOutboundBillDetails.Where(r => r.PartsOutboundBillId == partsOutboundBill.Id).ToArray();
                //for(int i = 0; i < tempPartsOutboundBillDetails.Length; i++) {
                //    if(tempPartsOutboundBillDetails[i].SettlementPrice == 0) {
                //var tempItemOfOutboundAndInboundDetail = listOfOutboundAndInboundDetail.FirstOrDefault(r => r.SparePartId == tempPartsOutboundBillDetails[i].SparePartId && r.BillId == tempPartsOutboundBillDetails[i].PartsOutboundBillId && r.BillType == (int)DcsPartsLogisticBatchBillDetailBillType.配件出库单);
                //if(tempItemOfOutboundAndInboundDetail == null) {
                //    throw new ValidationException("回填出库单清单时未找到结算价");
                //}
                //tempPartsOutboundBillDetails[i].SettlementPrice = tempItemOfOutboundAndInboundDetail.SettlementPrice;
                //partsPurchaseRtnSettleRef.SettlementAmount = partsPurchaseRtnSettleRef.SettlementAmount + (tempPartsOutboundBillDetails[i].OutboundAmount * tempPartsOutboundBillDetails[i].SettlementPrice);
                //}
                //}
                UpdateToDatabase(partsOutboundBill);
                new PartsOutboundBillAch(this.DomainService).UpdatePartsOutboundBillValidate(partsOutboundBill);
            }
            //partsPurchaseRtnSettleBill.TotalSettlementAmount = partsPurchaseRtnSettleRefs.Sum(r => r.SettlementAmount);
        }

        public void 修改配件采购退货结算单(PartsPurchaseRtnSettleBill partsPurchaseRtnSettleBill) {
            if (null != partsPurchaseRtnSettleBill.InvoiceDate.Value)
            {
                //当过帐日期不为空时，验证过帐日期是否有效
                this.DomainService.验证过账日期是否有效(partsPurchaseRtnSettleBill.InvoiceDate);
            }
            CheckEntityState(partsPurchaseRtnSettleBill);
            var partsPurchaseRtnSettleRefs = ChangeSet.GetAssociatedChanges(partsPurchaseRtnSettleBill, r => r.PartsPurchaseRtnSettleRefs).Cast<PartsPurchaseRtnSettleRef>().ToArray();
            var insertPartsPurchaseRtnSettleRefs = partsPurchaseRtnSettleRefs.Where(r => ChangeSet.GetChangeOperation(r) == ChangeOperation.Insert && partsPurchaseRtnSettleRefs.Any(v => ChangeSet.GetChangeOperation(v) == ChangeOperation.Delete && v.SourceId == r.SourceId)).ToArray();
            var deletePartsPurchaseRtnSettleRefs = partsPurchaseRtnSettleRefs.Where(r => ChangeSet.GetChangeOperation(r) == ChangeOperation.Delete && partsPurchaseRtnSettleRefs.Any(v => ChangeSet.GetChangeOperation(v) == ChangeOperation.Insert && v.SourceId == r.SourceId)).ToArray();
            //待处理的关联单
            var pendingPartsPurchaseRtnSettleRefs = partsPurchaseRtnSettleRefs.Except(insertPartsPurchaseRtnSettleRefs).Except(deletePartsPurchaseRtnSettleRefs).ToArray();
            var sourceIds = pendingPartsPurchaseRtnSettleRefs.Select(r => r.SourceId);
            var partsOutboundBills = ObjectContext.PartsOutboundBills.Where(r => sourceIds.Contains(r.Id)).ToArray();
            foreach(var partsPurchaseRtnSettleRef in pendingPartsPurchaseRtnSettleRefs) {
                switch(ChangeSet.GetChangeOperation(partsPurchaseRtnSettleRef)) {
                    case ChangeOperation.Insert:
                        var pendingSettlementPartsOutboundBill = partsOutboundBills.SingleOrDefault(r => r.Id == partsPurchaseRtnSettleRef.SourceId && r.SettlementStatus == (int)DcsPartsSettlementStatus.待结算);
                        if(pendingSettlementPartsOutboundBill == null)
                            throw new ValidationException(ErrorStrings.PartsPurchaseRtnSettleBill_Validation1);
                        pendingSettlementPartsOutboundBill.SettlementStatus = (int)DcsPartsSettlementStatus.已结算;
                        new PartsOutboundBillAch(this.DomainService).UpdatePartsOutboundBillValidate(pendingSettlementPartsOutboundBill);
                        break;
                    case ChangeOperation.Delete:
                        var partsOutboundBill = partsOutboundBills.SingleOrDefault(r => r.Id == partsPurchaseRtnSettleRef.SourceId);
                        if(partsOutboundBill == null)
                            throw new ValidationException(string.Format(ErrorStrings.PartsPurchaseRtnSettleBill_Validation7, partsPurchaseRtnSettleRef.SourceCode));
                        partsOutboundBill.SettlementStatus = (int)DcsPartsSettlementStatus.待结算;
                        new PartsOutboundBillAch(this.DomainService).UpdatePartsOutboundBillValidate(partsOutboundBill);
                        break;
                }
            }
        }

        public void 审批配件采购退货结算单(PartsPurchaseRtnSettleBill partsPurchaseRtnSettleBill) {
            var userinfo = Utils.GetCurrentUserInfo();
            var partsSalesCategoryIds = ObjectContext.PersonSalesCenterLinks.Where(r => r.PersonId == userinfo.Id).Select(r => r.PartsSalesCategoryId).ToArray();
            if(!partsSalesCategoryIds.Contains(partsPurchaseRtnSettleBill.PartsSalesCategoryId.Value))
                throw new ValidationException(ErrorStrings.PartsPurchaseRtnSettleBill_Validation8);
            var dbpartsPurchaseRtnSettleBill = ObjectContext.PartsPurchaseRtnSettleBills.Where(r => r.Id == partsPurchaseRtnSettleBill.Id && r.Status == (int)DcsPartsPurchaseSettleStatus.新建).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbpartsPurchaseRtnSettleBill == null)
                throw new ValidationException(ErrorStrings.PartsPurchaseRtnSettleBill_Validation2);
            UpdateToDatabase(partsPurchaseRtnSettleBill);
            partsPurchaseRtnSettleBill.Status = (int)DcsPartsPurchaseSettleStatus.已审批;
            var userInfo = Utils.GetCurrentUserInfo();
            partsPurchaseRtnSettleBill.ApproverId = userInfo.Id;
            partsPurchaseRtnSettleBill.ApproverName = userInfo.Name;
            partsPurchaseRtnSettleBill.ApproveTime = DateTime.Now;
            this.UpdatePartsPurchaseRtnSettleBillValidate(partsPurchaseRtnSettleBill);
        }

        public void 反结算配件采购退货结算单(PartsPurchaseRtnSettleBill partsPurchaseRtnSettleBill) {
            var userinfo = Utils.GetCurrentUserInfo();
            var partsSalesCategoryIds = ObjectContext.PersonSalesCenterLinks.Where(r => r.PersonId == userinfo.Id).Select(r => r.PartsSalesCategoryId).ToArray();
            if(!partsSalesCategoryIds.Contains(partsPurchaseRtnSettleBill.PartsSalesCategoryId.Value))
                throw new ValidationException(ErrorStrings.PartsPurchaseRtnSettleBill_Validation9);
            var dbpartsPurchaseRtnSettleBill = ObjectContext.PartsPurchaseRtnSettleBills.Where(r => r.Id == partsPurchaseRtnSettleBill.Id && r.Status == (int)DcsPartsPurchaseSettleStatus.发票登记).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbpartsPurchaseRtnSettleBill == null)
                throw new ValidationException(ErrorStrings.PartsPurchaseRtnSettleBill_Validation3);
            var partsPurchaseRtnSettleRefs = partsPurchaseRtnSettleBill.PartsPurchaseRtnSettleRefs;
            var sourceIds = partsPurchaseRtnSettleRefs.Select(r => r.SourceId);
            var partsOutboundBills = ObjectContext.PartsOutboundBills.Where(r => sourceIds.Contains(r.Id));
            foreach(var partsPurchaseRtnSettleRef in partsPurchaseRtnSettleRefs) {
                var partsOutboundBill = partsOutboundBills.SingleOrDefault(r => r.Id == partsPurchaseRtnSettleRef.SourceId);
                if(partsOutboundBill == null)
                    throw new ValidationException(string.Format(ErrorStrings.PartsPurchaseRtnSettleBill_Validation7, partsPurchaseRtnSettleRef.SourceCode));
                partsOutboundBill.SettlementStatus = (int)DcsPartsSettlementStatus.待结算;
                new PartsOutboundBillAch(this.DomainService).UpdatePartsOutboundBillValidate(partsOutboundBill);
            }
            var insertPartsPurchaseRtnSettleBill = new PartsPurchaseRtnSettleBill {
                WarehouseId = partsPurchaseRtnSettleBill.WarehouseId,
                WarehouseCode = partsPurchaseRtnSettleBill.WarehouseCode,
                WarehouseName = partsPurchaseRtnSettleBill.WarehouseName,
                BranchId = partsPurchaseRtnSettleBill.BranchId,
                BranchCode = partsPurchaseRtnSettleBill.BranchCode,
                BranchName = partsPurchaseRtnSettleBill.BranchName,
                PartsSupplierId = partsPurchaseRtnSettleBill.PartsSupplierId,
                PartsSupplierCode = partsPurchaseRtnSettleBill.PartsSupplierCode,
                PartsSupplierName = partsPurchaseRtnSettleBill.PartsSupplierName,
                TotalSettlementAmount = -partsPurchaseRtnSettleBill.TotalSettlementAmount,
                OffsettedSettlementBillId = partsPurchaseRtnSettleBill.Id,
                OffsettedSettlementBillCode = partsPurchaseRtnSettleBill.Code,
                TaxRate = partsPurchaseRtnSettleBill.TaxRate,
                Tax = -partsPurchaseRtnSettleBill.Tax,
                InvoiceAmountDifference = 0,
                InvoicePath = partsPurchaseRtnSettleBill.InvoicePath,
                Status = (int)DcsPartsPurchaseSettleStatus.已审批,
                SettlementPath = (int)DcsPartsPurchaseSettleBillSettlementPath.反冲结算,
                PartsSalesCategoryId = partsPurchaseRtnSettleBill.PartsSalesCategoryId ?? 0,
                PartsSalesCategoryName = partsPurchaseRtnSettleBill.PartsSalesCategoryName
            };
            InsertToDatabase(insertPartsPurchaseRtnSettleBill);
            this.InsertPartsPurchaseRtnSettleBillValidate(insertPartsPurchaseRtnSettleBill);
            new SupplierAccountAch(this.DomainService).反结算采购退货结算单过账(insertPartsPurchaseRtnSettleBill);
            var invoiceInformations = ObjectContext.InvoiceInformations.Where(r => r.SourceId == partsPurchaseRtnSettleBill.Id && r.SourceType == (int)DcsInvoiceInformationSourceType.配件采购退货结算单 && r.Status != (int)DcsInvoiceInformationStatus.作废);
            foreach(var invoiceInformation in invoiceInformations) {
                invoiceInformation.Status = (int)DcsInvoiceInformationStatus.作废;
                new InvoiceInformationAch(this.DomainService).UpdateInvoiceInformationValidate(invoiceInformation);
            }
            UpdateToDatabase(partsPurchaseRtnSettleBill);
            partsPurchaseRtnSettleBill.Status = (int)DcsPartsPurchaseSettleStatus.已反冲;
            this.UpdatePartsPurchaseRtnSettleBillValidate(partsPurchaseRtnSettleBill);
        }

        public void 作废配件采购退货结算单(PartsPurchaseRtnSettleBill partsPurchaseRtnSettleBill) {
            var userinfo = Utils.GetCurrentUserInfo();
            var partsSalesCategoryIds = ObjectContext.PersonSalesCenterLinks.Where(r => r.PersonId == userinfo.Id).Select(r => r.PartsSalesCategoryId).ToArray();
            if(!partsSalesCategoryIds.Contains(partsPurchaseRtnSettleBill.PartsSalesCategoryId.Value))
                throw new ValidationException(ErrorStrings.PartsPurchaseRtnSettleBill_Validation10);
            var dbpartsPurchaseRtnSettleBill = ObjectContext.PartsPurchaseRtnSettleBills.Where(r => r.Id == partsPurchaseRtnSettleBill.Id && r.Status == (int)DcsPartsPurchaseSettleStatus.新建).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbpartsPurchaseRtnSettleBill == null)
                throw new ValidationException(ErrorStrings.PartsPurchaseRtnSettleBill_Validation4);
            var partsPurchaseRtnSettleRefs = partsPurchaseRtnSettleBill.PartsPurchaseRtnSettleRefs;
            var sourceIds = partsPurchaseRtnSettleRefs.Select(r => r.SourceId);
            var partsOutboundBills = ObjectContext.PartsOutboundBills.Where(r => sourceIds.Contains(r.Id));
            foreach(var partsPurchaseRtnSettleRef in partsPurchaseRtnSettleRefs) {
                var partsOutboundBill = partsOutboundBills.SingleOrDefault(r => r.Id == partsPurchaseRtnSettleRef.SourceId);
                if(partsOutboundBill == null)
                    throw new ValidationException(string.Format(ErrorStrings.PartsPurchaseRtnSettleBill_Validation7, partsPurchaseRtnSettleRef.SourceCode));
                partsOutboundBill.SettlementStatus = (int)DcsPartsSettlementStatus.待结算;
                new PartsOutboundBillAch(this.DomainService).UpdatePartsOutboundBillValidate(partsOutboundBill);
            }
            UpdateToDatabase(partsPurchaseRtnSettleBill);
            partsPurchaseRtnSettleBill.Status = (int)DcsPartsPurchaseSettleStatus.作废;
            var userInfo = Utils.GetCurrentUserInfo();
            partsPurchaseRtnSettleBill.AbandonerId = userInfo.Id;
            partsPurchaseRtnSettleBill.AbandonerName = userInfo.Name;
            partsPurchaseRtnSettleBill.AbandonTime = DateTime.Now;
            this.UpdatePartsPurchaseRtnSettleBillValidate(partsPurchaseRtnSettleBill);
        }

        public void 配件采购退货结算单发票登记(PartsPurchaseRtnSettleBill partsPurchaseRtnSettleBill, decimal invoiceAmount) {
            var dbpartsPurchaseRtnSettleBill = ObjectContext.PartsPurchaseRtnSettleBills.Where(r => r.Id == partsPurchaseRtnSettleBill.Id && r.Status == (int)DcsPartsPurchaseSettleStatus.已审批).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbpartsPurchaseRtnSettleBill == null)
                throw new ValidationException(ErrorStrings.PartsPurchaseRtnSettleBill_Validation6);
            if(invoiceAmount <= 0)
                throw new ValidationException(ErrorStrings.PartsPurchaseRtnSettleBill_Validation5);
            UpdateToDatabase(partsPurchaseRtnSettleBill);
            partsPurchaseRtnSettleBill.Status = (int)DcsPartsPurchaseSettleStatus.发票已审核;
            partsPurchaseRtnSettleBill.InvoiceAmountDifference = partsPurchaseRtnSettleBill.TotalSettlementAmount - invoiceAmount;
            this.UpdatePartsPurchaseRtnSettleBillValidate(partsPurchaseRtnSettleBill);
            new SupplierAccountAch(this.DomainService).采购退货结算单过账(partsPurchaseRtnSettleBill);
        }

        public void 审批配件采购退货结算单(int partsPurchaseRtnSettleBillId) {
            var partsPurchaseRtnSettleBill = ObjectContext.PartsPurchaseRtnSettleBills.Where(r => r.Id == partsPurchaseRtnSettleBillId).FirstOrDefault();
            var userinfo = Utils.GetCurrentUserInfo();
            var partsSalesCategoryIds = ObjectContext.PersonSalesCenterLinks.Where(r => r.PersonId == userinfo.Id).Select(r => r.PartsSalesCategoryId).ToArray();
            if(!partsSalesCategoryIds.Contains(partsPurchaseRtnSettleBill.PartsSalesCategoryId.Value))
                throw new ValidationException(ErrorStrings.PartsPurchaseRtnSettleBill_Validation8);
            var dbpartsPurchaseRtnSettleBill = ObjectContext.PartsPurchaseRtnSettleBills.Where(r => r.Id == partsPurchaseRtnSettleBill.Id && r.Status == (int)DcsPartsPurchaseSettleStatus.新建).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbpartsPurchaseRtnSettleBill == null)
                throw new ValidationException(ErrorStrings.PartsPurchaseRtnSettleBill_Validation2);
            UpdateToDatabase(partsPurchaseRtnSettleBill);
            partsPurchaseRtnSettleBill.Status = (int)DcsPartsPurchaseSettleStatus.已审批;
            var userInfo = Utils.GetCurrentUserInfo();
            partsPurchaseRtnSettleBill.ApproverId = userInfo.Id;
            partsPurchaseRtnSettleBill.ApproverName = userInfo.Name;
            partsPurchaseRtnSettleBill.ApproveTime = DateTime.Now;
            this.UpdatePartsPurchaseRtnSettleBillValidate(partsPurchaseRtnSettleBill);
            ObjectContext.SaveChanges();
        }

        public void 反结算配件采购退货结算单(int partsPurchaseRtnSettleBillId) {
            var partsPurchaseRtnSettleBill = ObjectContext.PartsPurchaseRtnSettleBills.Where(r => r.Id == partsPurchaseRtnSettleBillId).FirstOrDefault();
            var userinfo = Utils.GetCurrentUserInfo();
            var partsSalesCategoryIds = ObjectContext.PersonSalesCenterLinks.Where(r => r.PersonId == userinfo.Id).Select(r => r.PartsSalesCategoryId).ToArray();
            if(!partsSalesCategoryIds.Contains(partsPurchaseRtnSettleBill.PartsSalesCategoryId.Value))
                throw new ValidationException(ErrorStrings.PartsPurchaseRtnSettleBill_Validation9);
            var dbpartsPurchaseRtnSettleBill = ObjectContext.PartsPurchaseRtnSettleBills.Where(r => r.Id == partsPurchaseRtnSettleBill.Id && r.Status == (int)DcsPartsPurchaseSettleStatus.发票已审核).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbpartsPurchaseRtnSettleBill == null)
                throw new ValidationException(ErrorStrings.PartsPurchaseRtnSettleBill_Validation3);
            var partsPurchaseRtnSettleRefs = partsPurchaseRtnSettleBill.PartsPurchaseRtnSettleRefs;
            var sourceIds = partsPurchaseRtnSettleRefs.Select(r => r.SourceId);
            var partsOutboundBills = ObjectContext.PartsOutboundBills.Where(r => sourceIds.Contains(r.Id));
            foreach(var partsPurchaseRtnSettleRef in partsPurchaseRtnSettleRefs) {
                var partsOutboundBill = partsOutboundBills.SingleOrDefault(r => r.Id == partsPurchaseRtnSettleRef.SourceId);
                if(partsOutboundBill == null)
                    throw new ValidationException(string.Format(ErrorStrings.PartsPurchaseRtnSettleBill_Validation7, partsPurchaseRtnSettleRef.SourceCode));
                partsOutboundBill.SettlementStatus = (int)DcsPartsSettlementStatus.待结算;
                new PartsOutboundBillAch(this.DomainService).UpdatePartsOutboundBillValidate(partsOutboundBill);
            }
            var insertPartsPurchaseRtnSettleBill = new PartsPurchaseRtnSettleBill {
                WarehouseId = partsPurchaseRtnSettleBill.WarehouseId,
                WarehouseCode = partsPurchaseRtnSettleBill.WarehouseCode,
                WarehouseName = partsPurchaseRtnSettleBill.WarehouseName,
                BranchId = partsPurchaseRtnSettleBill.BranchId,
                BranchCode = partsPurchaseRtnSettleBill.BranchCode,
                BranchName = partsPurchaseRtnSettleBill.BranchName,
                PartsSupplierId = partsPurchaseRtnSettleBill.PartsSupplierId,
                PartsSupplierCode = partsPurchaseRtnSettleBill.PartsSupplierCode,
                PartsSupplierName = partsPurchaseRtnSettleBill.PartsSupplierName,
                TotalSettlementAmount = -partsPurchaseRtnSettleBill.TotalSettlementAmount,
                OffsettedSettlementBillId = partsPurchaseRtnSettleBill.Id,
                OffsettedSettlementBillCode = partsPurchaseRtnSettleBill.Code,
                TaxRate = partsPurchaseRtnSettleBill.TaxRate,
                Tax = -partsPurchaseRtnSettleBill.Tax,
                InvoiceAmountDifference = 0,
                InvoicePath = partsPurchaseRtnSettleBill.InvoicePath,
                Status = (int)DcsPartsPurchaseSettleStatus.已审批,
                SettlementPath = (int)DcsPartsPurchaseSettleBillSettlementPath.反冲结算,
                PartsSalesCategoryId = partsPurchaseRtnSettleBill.PartsSalesCategoryId ?? 0,
                PartsSalesCategoryName = partsPurchaseRtnSettleBill.PartsSalesCategoryName
            };
            InsertToDatabase(insertPartsPurchaseRtnSettleBill);
            this.InsertPartsPurchaseRtnSettleBillValidate(insertPartsPurchaseRtnSettleBill);
            new SupplierAccountAch(this.DomainService).反结算采购退货结算单过账(insertPartsPurchaseRtnSettleBill);
            UpdateToDatabase(partsPurchaseRtnSettleBill);
            partsPurchaseRtnSettleBill.Status = (int)DcsPartsPurchaseSettleStatus.已反冲;
            this.UpdatePartsPurchaseRtnSettleBillValidate(partsPurchaseRtnSettleBill);
            ObjectContext.SaveChanges();
        }

        public void 作废配件采购退货结算单(int partsPurchaseRtnSettleBillId) {
            var partsPurchaseRtnSettleBill = ObjectContext.PartsPurchaseRtnSettleBills.Where(r => r.Id == partsPurchaseRtnSettleBillId).FirstOrDefault();
            var userinfo = Utils.GetCurrentUserInfo();
            var partsSalesCategoryIds = ObjectContext.PersonSalesCenterLinks.Where(r => r.PersonId == userinfo.Id).Select(r => r.PartsSalesCategoryId).ToArray();
            if(!partsSalesCategoryIds.Contains(partsPurchaseRtnSettleBill.PartsSalesCategoryId.Value))
                throw new ValidationException(ErrorStrings.PartsPurchaseRtnSettleBill_Validation10);
            var dbpartsPurchaseRtnSettleBill = ObjectContext.PartsPurchaseRtnSettleBills.Where(r => r.Id == partsPurchaseRtnSettleBill.Id && r.Status == (int)DcsPartsPurchaseSettleStatus.新建).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbpartsPurchaseRtnSettleBill == null)
                throw new ValidationException(ErrorStrings.PartsPurchaseRtnSettleBill_Validation4);
            var partsPurchaseRtnSettleRefs = partsPurchaseRtnSettleBill.PartsPurchaseRtnSettleRefs.ToArray();
            var sourceIds = partsPurchaseRtnSettleRefs.Select(r => r.SourceId);
            var partsOutboundBills = ObjectContext.PartsOutboundBills.Where(r => sourceIds.Contains(r.Id));
            foreach(var partsPurchaseRtnSettleRef in partsPurchaseRtnSettleRefs) {
                var partsOutboundBill = partsOutboundBills.SingleOrDefault(r => r.Id == partsPurchaseRtnSettleRef.SourceId);
                if(partsOutboundBill == null)
                    throw new ValidationException(string.Format(ErrorStrings.PartsPurchaseRtnSettleBill_Validation7, partsPurchaseRtnSettleRef.SourceCode));
                partsOutboundBill.SettlementStatus = (int)DcsPartsSettlementStatus.待结算;
                new PartsOutboundBillAch(this.DomainService).UpdatePartsOutboundBillValidate(partsOutboundBill);
            }
            UpdateToDatabase(partsPurchaseRtnSettleBill);
            partsPurchaseRtnSettleBill.Status = (int)DcsPartsPurchaseSettleStatus.作废;
            var userInfo = Utils.GetCurrentUserInfo();
            partsPurchaseRtnSettleBill.AbandonerId = userInfo.Id;
            partsPurchaseRtnSettleBill.AbandonerName = userInfo.Name;
            partsPurchaseRtnSettleBill.AbandonTime = DateTime.Now;
            this.UpdatePartsPurchaseRtnSettleBillValidate(partsPurchaseRtnSettleBill);
            ObjectContext.SaveChanges();
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:11:30
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 生成配件采购退货结算单(PartsPurchaseRtnSettleBill partsPurchaseRtnSettleBill) {
            new PartsPurchaseRtnSettleBillAch(this).生成配件采购退货结算单(partsPurchaseRtnSettleBill);
        }
        
        [Update(UsingCustomMethod = true)]
        public void 修改配件采购退货结算单(PartsPurchaseRtnSettleBill partsPurchaseRtnSettleBill) {
            new PartsPurchaseRtnSettleBillAch(this).修改配件采购退货结算单(partsPurchaseRtnSettleBill);
        }
        
        [Update(UsingCustomMethod = true)]
        public void 审批配件采购退货结算单(PartsPurchaseRtnSettleBill partsPurchaseRtnSettleBill) {
            new PartsPurchaseRtnSettleBillAch(this).审批配件采购退货结算单(partsPurchaseRtnSettleBill);
        }
        
        [Update(UsingCustomMethod = true)]
        public void 反结算配件采购退货结算单(PartsPurchaseRtnSettleBill partsPurchaseRtnSettleBill) {
            new PartsPurchaseRtnSettleBillAch(this).反结算配件采购退货结算单(partsPurchaseRtnSettleBill);
        }
        
        [Update(UsingCustomMethod = true)]
        public void 作废配件采购退货结算单(PartsPurchaseRtnSettleBill partsPurchaseRtnSettleBill) {
            new PartsPurchaseRtnSettleBillAch(this).作废配件采购退货结算单(partsPurchaseRtnSettleBill);
        }
        
        [Update(UsingCustomMethod = true)]
        public void 配件采购退货结算单发票登记(PartsPurchaseRtnSettleBill partsPurchaseRtnSettleBill, decimal invoiceAmount) {
            new PartsPurchaseRtnSettleBillAch(this).配件采购退货结算单发票登记(partsPurchaseRtnSettleBill,invoiceAmount);
        }
        
        [Invoke]
        public void 审批配件采购退货结算单(int partsPurchaseRtnSettleBillId) {
            new PartsPurchaseRtnSettleBillAch(this).审批配件采购退货结算单(partsPurchaseRtnSettleBillId);
        }
        
        [Invoke]
        public void 反结算配件采购退货结算单(int partsPurchaseRtnSettleBillId) {
            new PartsPurchaseRtnSettleBillAch(this).反结算配件采购退货结算单(partsPurchaseRtnSettleBillId);
        }
        
        [Invoke]
        public void 作废配件采购退货结算单(int partsPurchaseRtnSettleBillId) {
            new PartsPurchaseRtnSettleBillAch(this).作废配件采购退货结算单(partsPurchaseRtnSettleBillId);
        }
    }
}