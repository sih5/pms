﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using System.Transactions;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsPurchaseSettleBillAch : DcsSerivceAchieveBase {
        public PartsPurchaseSettleBillAch(DcsDomainService domainService)
            : base(domainService) {
        }

        public void 验证过账日期是否有效(DateTime? invoiceDate)
        {
            string year = invoiceDate.Value.Year.ToString();
            string month = invoiceDate.Value.Month.ToString();
            var accountPeriod = ObjectContext.AccountPeriods.Where(r => r.Month == month && r.Year == year).SingleOrDefault();
            if (null == accountPeriod)
            {
                throw new ValidationException(ErrorStrings.AccountPeriod_Validation2);
            }
            else if ((int)DcsAccountPeriodStatus.已关闭 == accountPeriod.Status)
            {
                throw new ValidationException(ErrorStrings.AccountPeriod_Validation3);
            }
        }

        public void 生成配件采购结算单(PartsPurchaseSettleBill partsPurchaseSettleBill) {
            using(var transaction = new TransactionScope()) {
                if (null != partsPurchaseSettleBill.InvoiceDate.Value)
                {
                    //当过帐日期不为空时，验证过帐日期是否有效
                    this.验证过账日期是否有效(partsPurchaseSettleBill.InvoiceDate);
                }
                var partsPurchaseSettleRefs = partsPurchaseSettleBill.PartsPurchaseSettleRefs;
                var partsOutboundBillIds = partsPurchaseSettleRefs.Where(r => r.SourceType == (int)DcsPartsPurchaseSettleRefSourceType.配件出库单).Select(r => r.SourceId).ToArray();
                var partsInboundCheckBillIds = partsPurchaseSettleRefs.Where(r => r.SourceType == (int)DcsPartsPurchaseSettleRefSourceType.配件入库检验单).Select(r => r.SourceId).ToArray();
                //var branchstrategie = ObjectContext.Branchstrategies.SingleOrDefault(p => p.BranchId == partsPurchaseSettleBill.BranchId && p.Status == (int)DcsBaseDataStatus.有效);
                //if(branchstrategie == null)
                //    throw new ValidationException("营销分公司策略不存在");

                var salesCenterstrategy = this.ObjectContext.SalesCenterstrategies.SingleOrDefault(r => r.PartsSalesCategoryId == partsPurchaseSettleBill.PartsSalesCategoryId && r.Status == (int)DcsBaseDataStatus.有效);
                if(salesCenterstrategy == null)
                    throw new ValidationException(ErrorStrings.SupplierShippingOrder_Validation31);

                var listOfOutboundAndInboundDetail = DomainService.查询待结算出入库明细清单后定价(partsInboundCheckBillIds, partsOutboundBillIds, partsPurchaseSettleBill.PartsSalesCategoryId.Value, partsPurchaseSettleBill.PartsSupplierId);
                var partsOutboundBills = ObjectContext.PartsOutboundBills.Where(r => partsOutboundBillIds.Contains(r.Id)).ToArray();
                var partsOutboundBillDetails = ObjectContext.PartsOutboundBillDetails.Where(r => partsOutboundBillIds.Contains(r.PartsOutboundBillId)).ToArray();
                var partsInboundCheckBills = ObjectContext.PartsInboundCheckBills.Where(r => partsInboundCheckBillIds.Contains(r.Id)).ToArray();
                var partsInboundCheckBillDetails = ObjectContext.PartsInboundCheckBillDetails.Where(r => partsInboundCheckBillIds.Contains(r.PartsInboundCheckBillId)).ToArray();



                foreach(var partsPurchaseSettleRef in partsPurchaseSettleRefs) {
                    switch(partsPurchaseSettleRef.SourceType) {
                        case (int)DcsPartsPurchaseSettleRefSourceType.配件出库单:
                            var partsOutboundBill = partsOutboundBills.SingleOrDefault(r => r.Id == partsPurchaseSettleRef.SourceId && r.SettlementStatus == (int)DcsPartsSettlementStatus.待结算);
                            if(partsOutboundBill == null) {
                                throw new ValidationException(ErrorStrings.PartsPurchaseSettleBill_Validation1);
                            }
                            var partsOutboundBillDetailsById = partsOutboundBillDetails.Where(r => r.PartsOutboundBillId == partsOutboundBill.Id).ToArray();
                            partsOutboundBill.SettlementStatus = (int)DcsPartsSettlementStatus.已结算;
                            if(salesCenterstrategy.PartsPurchasePricingStrategy == (int)DcsBranchstrategyPartsPurchasePricingStrategy.后定价) {
                                for(var i = 0; i < partsOutboundBillDetailsById.Count(); i++) {
                                    int num = i;
                                    var tempItemOfOutboundAndInboundDetail = listOfOutboundAndInboundDetail.FirstOrDefault(r => r.SparePartId == partsOutboundBillDetailsById[num].SparePartId && r.BillId == partsOutboundBillDetailsById[num].PartsOutboundBillId && r.BillType == (int)DcsPartsLogisticBatchBillDetailBillType.配件出库单);
                                    if(tempItemOfOutboundAndInboundDetail == null) {
                                        throw new ValidationException(ErrorStrings.PartsPurchaseSettleBill_Validation13);
                                    }
                                    partsOutboundBillDetailsById[i].SettlementPrice = tempItemOfOutboundAndInboundDetail.SettlementPrice;
                                    UpdateToDatabase(partsOutboundBillDetailsById[i]);
                                }
                                var totalAmount = partsOutboundBillDetailsById.Sum(r => r.SettlementPrice * (-r.OutboundAmount));
                                var updatePartsPurchaseSettleRef = partsPurchaseSettleRefs.Where(r => r.SourceId == partsOutboundBill.Id).FirstOrDefault();
                                if(updatePartsPurchaseSettleRef == null)
                                    throw new ValidationException(ErrorStrings.PartsPurchaseSettleBill_Validation14);
                                updatePartsPurchaseSettleRef.SettlementAmount = totalAmount;
                            }
                            new PartsOutboundBillAch(this.DomainService).UpdatePartsOutboundBillValidate(partsOutboundBill);

                            break;
                        case (int)DcsPartsPurchaseSettleRefSourceType.配件入库检验单:
                            var partsInboundCheckBill = partsInboundCheckBills.SingleOrDefault(r => r.Id == partsPurchaseSettleRef.SourceId && r.SettlementStatus == (int)DcsPartsSettlementStatus.待结算);
                            if(partsInboundCheckBill == null)
                                throw new ValidationException(ErrorStrings.PartsPurchaseSettleBill_Validation2);
                            var partsInboundCheckBillDetailsById = partsInboundCheckBillDetails.Where(r => r.PartsInboundCheckBillId == partsInboundCheckBill.Id).ToArray();
                            partsInboundCheckBill.SettlementStatus = (int)DcsPartsSettlementStatus.已结算;
                            if(salesCenterstrategy.PartsPurchasePricingStrategy == (int)DcsBranchstrategyPartsPurchasePricingStrategy.后定价) {
                                for(var i = 0; i < partsInboundCheckBillDetailsById.Count(); i++) {
                                    int num = i;
                                    var tempItemOfOutboundAndInboundDetail = listOfOutboundAndInboundDetail.FirstOrDefault(r => r.SparePartId == partsInboundCheckBillDetailsById[num].SparePartId && r.BillId == partsInboundCheckBillDetailsById[num].PartsInboundCheckBillId && r.BillType == (int)DcsPartsLogisticBatchBillDetailBillType.配件入库检验单);
                                    if(tempItemOfOutboundAndInboundDetail == null) {
                                        throw new ValidationException(ErrorStrings.PartsPurchaseSettleBill_Validation15);
                                    }
                                    partsInboundCheckBillDetailsById[i].SettlementPrice = tempItemOfOutboundAndInboundDetail.SettlementPrice;

                                    UpdateToDatabase(partsInboundCheckBillDetailsById[i]);
                                }
                                var totalAmount = partsInboundCheckBillDetailsById.Sum(r => r.SettlementPrice * r.InspectedQuantity);
                                var updatePartsPurchaseSettleRef = partsPurchaseSettleRefs.Where(r => r.SourceId == partsInboundCheckBill.Id).FirstOrDefault();
                                if(updatePartsPurchaseSettleRef == null)
                                    throw new ValidationException(ErrorStrings.PartsPurchaseSettleBill_Validation16);
                                updatePartsPurchaseSettleRef.SettlementAmount = totalAmount;
                            }
                            new PartsInboundCheckBillAch(this.DomainService).UpdatePartsInboundCheckBillValidate(partsInboundCheckBill);
                            break;
                    }
                }


                ////处理虚拟积分清单
                var bonusPointsOrders1 = this.ObjectContext.BonusPointsOrders.Where(r => partsOutboundBillIds.Contains(r.PartsOutboundBillId ?? 0));
                var bonusPointsOrders2 = this.ObjectContext.BonusPointsOrders.Where(r => partsInboundCheckBillIds.Contains(r.PartsInboundCheckBillId ?? 0));
                //if(bonusPointsOrders1.Any() || bonusPointsOrders2.Any()) {
                //    var partsPurchaseSettleDetail = new PartsPurchaseSettleDetail();
                //    partsPurchaseSettleDetail.SerialNumber = partsPurchaseSettleBill.PartsPurchaseSettleDetails.Count() + 1;
                //    partsPurchaseSettleDetail.SettlementPrice = bonusPointsOrders1.Sum(r => r.BonusPointsAmount ?? 0) + bonusPointsOrders2.Sum(r => r.BonusPointsAmount ?? 0);
                //    partsPurchaseSettleDetail.SettlementAmount = bonusPointsOrders1.Sum(r => r.BonusPointsAmount ?? 0) + bonusPointsOrders2.Sum(r => r.BonusPointsAmount ?? 0);
                //    partsPurchaseSettleDetail.QuantityToSettle = 1;
                //    var sparePart = this.ObjectContext.SpareParts.FirstOrDefault(r => r.Name == "虚拟积分配件");
                //    if(sparePart != null) {
                //        partsPurchaseSettleDetail.SparePartId = sparePart.Id;
                //        partsPurchaseSettleDetail.SparePartCode = sparePart.Code;
                //        partsPurchaseSettleDetail.SparePartName = sparePart.Name;
                //    } else
                //        throw new ValidationException("未找到虚拟积分配件");
                //    partsPurchaseSettleBill.PartsPurchaseSettleDetails.Add(partsPurchaseSettleDetail);
                //}


                ////处理虚拟运费清单
                var ecommerceFreightDisposals = this.ObjectContext.EcommerceFreightDisposals.Where(r => partsInboundCheckBillIds.Contains(r.PartsInboundCheckBillId ?? 0));
                var returnFreightDisposals = this.ObjectContext.ReturnFreightDisposals.Where(r => partsOutboundBillIds.Contains(r.PartsOutboundBillId ?? 0));
                //if(ecommerceFreightDisposals.Any() || returnFreightDisposals.Any()) {
                //    var partsPurchaseSettleDetail1 = new PartsPurchaseSettleDetail();
                //    partsPurchaseSettleDetail1.SerialNumber = partsPurchaseSettleBill.PartsPurchaseSettleDetails.Count() + 1;
                //    partsPurchaseSettleDetail1.SettlementPrice = ecommerceFreightDisposals.Sum(r => r.FreightCharges ?? 0) - returnFreightDisposals.Sum(r => r.FreightCharges ?? 0);
                //    partsPurchaseSettleDetail1.SettlementAmount = ecommerceFreightDisposals.Sum(r => r.FreightCharges ?? 0) - returnFreightDisposals.Sum(r => r.FreightCharges ?? 0);
                //    partsPurchaseSettleDetail1.QuantityToSettle = 1;
                //    var sparePart = this.ObjectContext.SpareParts.FirstOrDefault(r => r.Name == "虚拟运费配件");
                //    if(sparePart != null) {
                //        partsPurchaseSettleDetail1.SparePartId = sparePart.Id;
                //        partsPurchaseSettleDetail1.SparePartCode = sparePart.Code;
                //        partsPurchaseSettleDetail1.SparePartName = sparePart.Name;
                //    } else
                //        throw new ValidationException("未找到虚拟运费配件");
                //    partsPurchaseSettleBill.PartsPurchaseSettleDetails.Add(partsPurchaseSettleDetail1);
                //}
                //partsPurchaseSettleBill.TotalSettlementAmount = partsPurchaseSettleBill.PartsPurchaseSettleDetails.Sum(r => r.SettlementAmount);
                this.ObjectContext.SaveChanges();

                //处理积分订单 运费处理单 退货运费处理单的结算状态和结算单ID
                foreach(var item in bonusPointsOrders1) {
                    item.PurchaseSettlementStatus = (int)DcsPartsSettlementStatus.已结算;
                    item.PartsPurchaseSettleBillId = partsPurchaseSettleBill.Id;
                    item.PartsPurchaseSettleBillCode = partsPurchaseSettleBill.Code;
                }
                foreach(var item in bonusPointsOrders2) {
                    item.PurchaseSettlementStatus = (int)DcsPartsSettlementStatus.已结算;
                    item.PartsPurchaseSettleBillId = partsPurchaseSettleBill.Id;
                    item.PartsPurchaseSettleBillCode = partsPurchaseSettleBill.Code;
                }
                foreach(var item in ecommerceFreightDisposals) {
                    item.Status = (int)DcsFreightDisposalStatus.已结算;
                    item.PartsPurchaseSettleBillId = partsPurchaseSettleBill.Id;
                    item.PartsPurchaseSettleBillCode = partsPurchaseSettleBill.Code;
                    item.PurchaseSettlementStatus = (int)DcsPartsSettlementStatus.已结算;
                }
                foreach(var item in returnFreightDisposals) {
                    item.Status = (int)DcsFreightDisposalStatus.已结算;
                    item.PartsPurchaseSettleBillId = partsPurchaseSettleBill.Id;
                    item.PartsPurchaseSettleBillCode = partsPurchaseSettleBill.Code;
                    item.PurchaseSettlementStatus = (int)DcsPartsSettlementStatus.已结算;
                }
                transaction.Complete();
            }
        }


        public void 修改配件采购结算单(PartsPurchaseSettleBill partsPurchaseSettleBill) {
            if (null != partsPurchaseSettleBill.InvoiceDate.Value)
            {
                //当过帐日期不为空时，验证过帐日期是否有效
                this.验证过账日期是否有效(partsPurchaseSettleBill.InvoiceDate);
            }
            var partsPurchaseSettleRefs = partsPurchaseSettleBill.PartsPurchaseSettleRefs;
            var partsOutboundBillIds = partsPurchaseSettleRefs.Where(r => r.SourceType == (int)DcsPartsPurchaseSettleRefSourceType.配件出库单).Select(r => r.SourceId).ToArray();
            var partsInboundCheckBillIds = partsPurchaseSettleRefs.Where(r => r.SourceType == (int)DcsPartsPurchaseSettleRefSourceType.配件入库检验单).Select(r => r.SourceId).ToArray();
            var partsOutboundBills = ObjectContext.PartsOutboundBills.Where(r => partsOutboundBillIds.Contains(r.Id)).ToArray();
            var partsOutboundBillDetails = ObjectContext.PartsOutboundBillDetails.Where(r => partsOutboundBillIds.Contains(r.PartsOutboundBillId)).ToArray();
            var partsInboundCheckBills = ObjectContext.PartsInboundCheckBills.Where(r => partsInboundCheckBillIds.Contains(r.Id)).ToArray();
            var partsInboundCheckBillDetails = ObjectContext.PartsInboundCheckBillDetails.Where(r => partsInboundCheckBillIds.Contains(r.PartsInboundCheckBillId)).ToArray();
            //var branchstrategie = ObjectContext.Branchstrategies.SingleOrDefault(p => p.BranchId == partsPurchaseSettleBill.BranchId && p.Status == (int)DcsBaseDataStatus.有效);
            //if(branchstrategie == null)
            //    throw new ValidationException("营销分公司策略不存在");

            //处理虚拟积分清单
            var bonusPointsOrders1 = this.ObjectContext.BonusPointsOrders.Where(r => partsOutboundBillIds.Contains(r.PartsOutboundBillId ?? 0));
            var bonusPointsOrders2 = this.ObjectContext.BonusPointsOrders.Where(r => partsInboundCheckBillIds.Contains(r.PartsInboundCheckBillId ?? 0));
            //if(bonusPointsOrders1.Any() || bonusPointsOrders2.Any()) {
            //    var partsPurchaseSettleDetail = new PartsPurchaseSettleDetail();
            //    partsPurchaseSettleDetail.SerialNumber = partsPurchaseSettleBill.PartsPurchaseSettleDetails.Count() + 1;
            //    if(partsPurchaseSettleBill.PartsPurchaseSettleDetails.Any(r => r.SparePartName == "虚拟积分配件"))
            //        partsPurchaseSettleDetail = partsPurchaseSettleBill.PartsPurchaseSettleDetails.FirstOrDefault(r => r.SparePartName == "虚拟积分配件");
            //    partsPurchaseSettleDetail.SettlementPrice = bonusPointsOrders1.Sum(r => r.BonusPointsAmount ?? 0) + bonusPointsOrders2.Sum(r => r.BonusPointsAmount ?? 0);
            //    partsPurchaseSettleDetail.SettlementAmount = bonusPointsOrders1.Sum(r => r.BonusPointsAmount ?? 0) + bonusPointsOrders2.Sum(r => r.BonusPointsAmount ?? 0);
            //    partsPurchaseSettleDetail.QuantityToSettle = 1;
            //    var sparePart = this.ObjectContext.SpareParts.FirstOrDefault(r => r.Name == "虚拟积分配件");
            //    if(sparePart != null) {
            //        partsPurchaseSettleDetail.SparePartId = sparePart.Id;
            //        partsPurchaseSettleDetail.SparePartCode = sparePart.Code;
            //        partsPurchaseSettleDetail.SparePartName = sparePart.Name;
            //    } else
            //        throw new ValidationException("未找到虚拟积分配件");
            //    partsPurchaseSettleBill.PartsPurchaseSettleDetails.Add(partsPurchaseSettleDetail);
            //}

            //处理虚拟运费清单
            var ecommerceFreightDisposals = this.ObjectContext.EcommerceFreightDisposals.Where(r => partsInboundCheckBillIds.Contains(r.PartsInboundCheckBillId ?? 0));
            var returnFreightDisposals = this.ObjectContext.ReturnFreightDisposals.Where(r => partsOutboundBillIds.Contains(r.PartsOutboundBillId ?? 0));
            //if(ecommerceFreightDisposals.Any() || returnFreightDisposals.Any()) {
            //    var partsPurchaseSettleDetail1 = new PartsPurchaseSettleDetail();
            //    partsPurchaseSettleDetail1.SerialNumber = partsPurchaseSettleBill.PartsPurchaseSettleDetails.Count() + 1;
            //    if(partsPurchaseSettleBill.PartsPurchaseSettleDetails.Any(r => r.SparePartName == "虚拟运费配件"))
            //        partsPurchaseSettleDetail1 = partsPurchaseSettleBill.PartsPurchaseSettleDetails.FirstOrDefault(r => r.SparePartName == "虚拟运费配件");
            //    partsPurchaseSettleDetail1.SettlementPrice = ecommerceFreightDisposals.Sum(r => r.FreightCharges ?? 0) - returnFreightDisposals.Sum(r => r.FreightCharges ?? 0);
            //    partsPurchaseSettleDetail1.SettlementAmount = ecommerceFreightDisposals.Sum(r => r.FreightCharges ?? 0) - returnFreightDisposals.Sum(r => r.FreightCharges ?? 0);
            //    partsPurchaseSettleDetail1.QuantityToSettle = 1;
            //    var sparePart = this.ObjectContext.SpareParts.FirstOrDefault(r => r.Name == "虚拟运费配件");
            //    if(sparePart != null) {
            //        partsPurchaseSettleDetail1.SparePartId = sparePart.Id;
            //        partsPurchaseSettleDetail1.SparePartCode = sparePart.Code;
            //        partsPurchaseSettleDetail1.SparePartName = sparePart.Name;
            //    } else
            //        throw new ValidationException("未找到虚拟运费配件");
            //    partsPurchaseSettleBill.PartsPurchaseSettleDetails.Add(partsPurchaseSettleDetail1);
            //}
            //partsPurchaseSettleBill.TotalSettlementAmount = partsPurchaseSettleBill.PartsPurchaseSettleDetails.Sum(r => r.SettlementAmount);

            //处理积分订单 运费处理单 退货运费处理单的结算状态和结算单ID
            foreach(var item in bonusPointsOrders1) {
                item.PurchaseSettlementStatus = (int)DcsPartsSettlementStatus.已结算;
                item.PartsPurchaseSettleBillId = partsPurchaseSettleBill.Id;
                item.PartsPurchaseSettleBillCode = partsPurchaseSettleBill.Code;
            }
            foreach(var item in bonusPointsOrders2) {
                item.PurchaseSettlementStatus = (int)DcsPartsSettlementStatus.已结算;
                item.PartsPurchaseSettleBillId = partsPurchaseSettleBill.Id;
                item.PartsPurchaseSettleBillCode = partsPurchaseSettleBill.Code;
            }
            foreach(var item in ecommerceFreightDisposals) {
                item.Status = (int)DcsFreightDisposalStatus.已结算;
                item.PartsPurchaseSettleBillId = partsPurchaseSettleBill.Id;
                item.PartsPurchaseSettleBillCode = partsPurchaseSettleBill.Code;
                item.PurchaseSettlementStatus = (int)DcsPartsSettlementStatus.已结算;
            }
            foreach(var item in returnFreightDisposals) {
                item.Status = (int)DcsFreightDisposalStatus.已结算;
                item.PartsPurchaseSettleBillId = partsPurchaseSettleBill.Id;
                item.PartsPurchaseSettleBillCode = partsPurchaseSettleBill.Code;
                item.PurchaseSettlementStatus = (int)DcsPartsSettlementStatus.已结算;
            }


            var salesCenterstrategy = this.ObjectContext.SalesCenterstrategies.SingleOrDefault(r => r.PartsSalesCategoryId == partsPurchaseSettleBill.PartsSalesCategoryId && r.Status == (int)DcsBaseDataStatus.有效);
            if(salesCenterstrategy == null)
                throw new ValidationException(ErrorStrings.SupplierShippingOrder_Validation31);

            var listOfOutboundAndInboundDetails = DomainService.查询待结算出入库明细清单后定价(partsInboundCheckBillIds, partsOutboundBillIds, partsPurchaseSettleBill.PartsSalesCategoryId.Value, partsPurchaseSettleBill.PartsSupplierId);
            foreach(var partsPurchaseSettleRef in partsPurchaseSettleRefs) {
                switch(ChangeSet.GetChangeOperation(partsPurchaseSettleRef)) {
                    case ChangeOperation.Update:
                    case ChangeOperation.Insert:
                        switch(partsPurchaseSettleRef.SourceType) {
                            case (int)DcsPartsPurchaseSettleRefSourceType.配件出库单:
                                var pendingSettlementPartsOutboundBill = partsOutboundBills.SingleOrDefault(r => r.Id == partsPurchaseSettleRef.SourceId && (r.SettlementStatus == (int)DcsPartsSettlementStatus.待结算 || r.SettlementStatus == (int)DcsPartsSettlementStatus.已结算));
                                if(pendingSettlementPartsOutboundBill == null)
                                    throw new ValidationException(ErrorStrings.PartsPurchaseSettleBill_Validation1);
                                pendingSettlementPartsOutboundBill.SettlementStatus = (int)DcsPartsSettlementStatus.已结算;
                                new PartsOutboundBillAch(this.DomainService).UpdatePartsOutboundBillValidate(pendingSettlementPartsOutboundBill);
                                var details = partsOutboundBillDetails.Where(p => p.PartsOutboundBillId == pendingSettlementPartsOutboundBill.Id);
                                if(salesCenterstrategy.PartsPurchasePricingStrategy == (int)DcsBranchstrategyPartsPurchasePricingStrategy.后定价) {
                                    foreach(var detail in details) {
                                        var listOfOutboundAndInboundDetail = listOfOutboundAndInboundDetails.FirstOrDefault(r => r.SparePartId == detail.SparePartId && r.BillId == detail.PartsOutboundBillId && r.BillType == (int)DcsPartsLogisticBatchBillDetailBillType.配件出库单);
                                        if(listOfOutboundAndInboundDetail == null)
                                            throw new ValidationException(ErrorStrings.PartsPurchaseSettleBill_Validation13);
                                        detail.SettlementPrice = listOfOutboundAndInboundDetail.SettlementPrice;
                                        UpdateToDatabase(detail);
                                    }
                                    partsPurchaseSettleRef.SettlementAmount = details.Sum(p => p.SettlementPrice * (-p.OutboundAmount));
                                }
                                break;
                            case (int)DcsPartsPurchaseSettleRefSourceType.配件入库检验单:
                                var pendingSettlementPartsInboundCheckBill = partsInboundCheckBills.SingleOrDefault(r => r.Id == partsPurchaseSettleRef.SourceId && (r.SettlementStatus == (int)DcsPartsSettlementStatus.待结算 || r.SettlementStatus == (int)DcsPartsSettlementStatus.已结算));
                                if(pendingSettlementPartsInboundCheckBill == null)
                                    throw new ValidationException(ErrorStrings.PartsPurchaseSettleBill_Validation2);
                                pendingSettlementPartsInboundCheckBill.SettlementStatus = (int)DcsPartsSettlementStatus.已结算;
                                new PartsInboundCheckBillAch(this.DomainService).UpdatePartsInboundCheckBillValidate(pendingSettlementPartsInboundCheckBill);
                                var detailss = partsInboundCheckBillDetails.Where(p => p.PartsInboundCheckBillId == pendingSettlementPartsInboundCheckBill.Id);
                                if(salesCenterstrategy.PartsPurchasePricingStrategy == (int)DcsBranchstrategyPartsPurchasePricingStrategy.后定价) {
                                    foreach(var detail in detailss) {
                                        var listOfOutboundAndInboundDetail = listOfOutboundAndInboundDetails.FirstOrDefault(r => r.SparePartId == detail.SparePartId && r.BillId == detail.PartsInboundCheckBillId && r.BillType == (int)DcsPartsLogisticBatchBillDetailBillType.配件入库检验单);
                                        if(listOfOutboundAndInboundDetail == null)
                                            throw new ValidationException(ErrorStrings.PartsPurchaseSettleBill_Validation15);
                                        detail.SettlementPrice = listOfOutboundAndInboundDetail.SettlementPrice;
                                        UpdateToDatabase(detail);
                                    }
                                    partsPurchaseSettleRef.SettlementAmount = detailss.Sum(p => p.SettlementPrice * p.InspectedQuantity);
                                }
                                break;
                        }
                        break;
                    //case ChangeOperation.Delete:
                    //    switch(partsPurchaseSettleRef.SourceType) {
                    //        case (int)DcsPartsPurchaseSettleRefSourceType.配件出库单:
                    //            var partsOutboundBill = partsOutboundBills.SingleOrDefault(r => r.Id == partsPurchaseSettleRef.SourceId);
                    //            if(partsOutboundBill == null)
                    //                throw new ValidationException(string.Format(ErrorStrings.PartsPurchaseSettleBill_Validation8, partsPurchaseSettleRef.SourceCode));
                    //            partsOutboundBill.SettlementStatus = (int)DcsPartsSettlementStatus.待结算;
                    //            new PartsOutboundBillAch(this.DomainService).UpdatePartsOutboundBillValidate(partsOutboundBill);
                    //            break;
                    //        case (int)DcsPartsPurchaseSettleRefSourceType.配件入库检验单:
                    //            var partsInboundCheckBill = partsInboundCheckBills.SingleOrDefault(r => r.Id == partsPurchaseSettleRef.SourceId);
                    //            if(partsInboundCheckBill == null)
                    //                throw new ValidationException(string.Format(ErrorStrings.PartsPurchaseSettleBill_Validation9, partsPurchaseSettleRef.SourceCode));
                    //            partsInboundCheckBill.SettlementStatus = (int)DcsPartsSettlementStatus.待结算;
                    //            new PartsInboundCheckBillAch(this.DomainService).UpdatePartsInboundCheckBillValidate(partsInboundCheckBill);
                    //            break;
                    //    }
                    //    break;
                }
            }
            //倒冲修改采购结算单时删除的关联单源单据结算状态
            var dbRefs = ObjectContext.PartsPurchaseSettleRefs.Where(e => e.PartsPurchaseSettleBillId == partsPurchaseSettleBill.Id).ToArray();
            var dbPartsOutboundBillIds = dbRefs.Where(r => r.SourceType == (int)DcsPartsPurchaseSettleRefSourceType.配件出库单).Select(r => r.SourceId).ToArray();
            var dbPartsInboundCheckBillIds = dbRefs.Where(r => r.SourceType == (int)DcsPartsPurchaseSettleRefSourceType.配件入库检验单).Select(r => r.SourceId).ToArray();
            var deletePartsOutboundBillIds = dbPartsOutboundBillIds.Except(partsOutboundBillIds);
            var deletePartsInboundCheckBillIds = dbPartsInboundCheckBillIds.Except(partsInboundCheckBillIds);
            var deletePartsInboundCheckBills = ObjectContext.PartsInboundCheckBills.Where(r => deletePartsInboundCheckBillIds.Contains(r.Id)).ToArray();
            var deletePartsOutboundBills = ObjectContext.PartsOutboundBills.Where(r => deletePartsOutboundBillIds.Contains(r.Id)).ToArray();
            foreach(var deletePartsOutboundBill in deletePartsOutboundBills) {
                deletePartsOutboundBill.SettlementStatus = (int)DcsPartsSettlementStatus.待结算;
                new PartsOutboundBillAch(this.DomainService).UpdatePartsOutboundBillValidate(deletePartsOutboundBill);
            }
            foreach(var deletePartsInboundCheckBill in deletePartsInboundCheckBills) {
                deletePartsInboundCheckBill.SettlementStatus = (int)DcsPartsSettlementStatus.待结算;
                new PartsInboundCheckBillAch(this.DomainService).UpdatePartsInboundCheckBillValidate(deletePartsInboundCheckBill);
            }

            //处理虚拟积分清单
            var bonusPointsOrders3 = this.ObjectContext.BonusPointsOrders.Where(r => deletePartsOutboundBillIds.Contains(r.PartsOutboundBillId ?? 0));
            var bonusPointsOrders4 = this.ObjectContext.BonusPointsOrders.Where(r => deletePartsInboundCheckBillIds.Contains(r.PartsInboundCheckBillId ?? 0));

            //处理虚拟运费清单
            var ecommerceFreightDisposals1 = this.ObjectContext.EcommerceFreightDisposals.Where(r => partsInboundCheckBillIds.Contains(r.PartsInboundCheckBillId ?? 0));
            var returnFreightDisposals1 = this.ObjectContext.ReturnFreightDisposals.Where(r => partsOutboundBillIds.Contains(r.PartsOutboundBillId ?? 0));

            foreach(var item in bonusPointsOrders3) {
                item.PurchaseSettlementStatus = null;
                item.PartsPurchaseSettleBillId = null;
                item.PartsPurchaseSettleBillCode = string.Empty;
            }
            foreach(var item in bonusPointsOrders4) {
                item.PurchaseSettlementStatus = null;
                item.PartsPurchaseSettleBillId = null;
                item.PartsPurchaseSettleBillCode = string.Empty;
            }
            foreach(var item in ecommerceFreightDisposals1) {
                item.Status = (int)DcsFreightDisposalStatus.待结算;
                item.PartsPurchaseSettleBillId = null;
                item.PartsPurchaseSettleBillCode = string.Empty;
                item.PurchaseSettlementStatus = null;
            }
            foreach(var item in returnFreightDisposals1) {
                item.Status = (int)DcsFreightDisposalStatus.待结算;
                item.PartsPurchaseSettleBillId = null;
                item.PartsPurchaseSettleBillCode = string.Empty;
                item.PurchaseSettlementStatus = null;
            }

        }

        public void 审批配件采购结算单(PartsPurchaseSettleBill virtualPartsPurchaseSettleBillWithSumPlannedPrice) {
            var partsPurchaseSettleBill = ObjectContext.PartsPurchaseSettleBills.Where(r => r.Id == virtualPartsPurchaseSettleBillWithSumPlannedPrice.Id).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(partsPurchaseSettleBill == null)
                throw new ValidationException(ErrorStrings.BottomStock_Validation3);
            //var userinfo = Utils.GetCurrentUserInfo();
            //var partsSalesCategoryIds = ObjectContext.PersonSalesCenterLinks.Where(r => r.PersonId == userinfo.Id).Select(r => r.PartsSalesCategoryId).ToArray();
            //if(!partsSalesCategoryIds.Contains(partsPurchaseSettleBill.PartsSalesCategoryId.Value))
            //    throw new ValidationException(ErrorStrings.PartsPurchaseSettleBill_Validation10);
            var dbpartsPurchaseSettleBill = ObjectContext.PartsPurchaseSettleBills.Where(r => r.Id == partsPurchaseSettleBill.Id && r.Status == (int)DcsPartsPurchaseSettleStatus.新建).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbpartsPurchaseSettleBill == null)
                throw new ValidationException(ErrorStrings.PartsPurchaseSettleBill_Validation3);
            UpdateToDatabase(virtualPartsPurchaseSettleBillWithSumPlannedPrice);
            if(virtualPartsPurchaseSettleBillWithSumPlannedPrice.TotalSettlementAmount == 0) {
                virtualPartsPurchaseSettleBillWithSumPlannedPrice.Status = (int)DcsPartsPurchaseSettleStatus.发票已审核;
            } else {
                virtualPartsPurchaseSettleBillWithSumPlannedPrice.Status = (int)DcsPartsPurchaseSettleStatus.已审批;
            }
            var userInfo = Utils.GetCurrentUserInfo();
            virtualPartsPurchaseSettleBillWithSumPlannedPrice.ApproverId = userInfo.Id;
            virtualPartsPurchaseSettleBillWithSumPlannedPrice.ApproverName = userInfo.Name;
            virtualPartsPurchaseSettleBillWithSumPlannedPrice.ApproveTime = DateTime.Now;

            this.UpdatePartsPurchaseSettleBillValidate(virtualPartsPurchaseSettleBillWithSumPlannedPrice);
        }

        public void 审批配件采购结算单(VirtualPartsPurchaseSettleBillWithSumPlannedPrice virtualPartsPurchaseSettleBillWithSumPlannedPrice) {
            var partsPurchaseSettleBill = ObjectContext.PartsPurchaseSettleBills.Where(r => r.Id == virtualPartsPurchaseSettleBillWithSumPlannedPrice.Id).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(partsPurchaseSettleBill == null)
                throw new ValidationException(ErrorStrings.BottomStock_Validation3);
            var userinfo = Utils.GetCurrentUserInfo();
            var partsSalesCategoryIds = ObjectContext.PersonSalesCenterLinks.Where(r => r.PersonId == userinfo.Id).Select(r => r.PartsSalesCategoryId).ToArray();
            if(!partsSalesCategoryIds.Contains(partsPurchaseSettleBill.PartsSalesCategoryId.Value))
                throw new ValidationException(ErrorStrings.PartsPurchaseSettleBill_Validation10);
            var dbpartsPurchaseSettleBill = ObjectContext.PartsPurchaseSettleBills.Where(r => r.Id == partsPurchaseSettleBill.Id && r.Status == (int)DcsPartsPurchaseSettleStatus.新建).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbpartsPurchaseSettleBill == null)
                throw new ValidationException(ErrorStrings.PartsPurchaseSettleBill_Validation3);
            UpdateToDatabase(partsPurchaseSettleBill);
            if(partsPurchaseSettleBill.TotalSettlementAmount == 0) {
                partsPurchaseSettleBill.Status = (int)DcsPartsPurchaseSettleStatus.发票已审核;
            } else {
                partsPurchaseSettleBill.Status = (int)DcsPartsPurchaseSettleStatus.已审批;
            }
            var userInfo = Utils.GetCurrentUserInfo();
            partsPurchaseSettleBill.ApproverId = userInfo.Id;
            partsPurchaseSettleBill.ApproverName = userInfo.Name;
            partsPurchaseSettleBill.ApproveTime = DateTime.Now;
            this.UpdatePartsPurchaseSettleBillValidate(partsPurchaseSettleBill);
        }


        public void 反结算配件采购结算单(VirtualPartsPurchaseSettleBillWithSumPlannedPrice virtualPartsPurchaseSettleBillWithSumPlannedPrice) {
            var partsPurchaseSettleBill = ObjectContext.PartsPurchaseSettleBills.Where(r => r.Id == virtualPartsPurchaseSettleBillWithSumPlannedPrice.Id).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(partsPurchaseSettleBill == null)
                throw new ValidationException(ErrorStrings.BottomStock_Validation3);
            var userinfo = Utils.GetCurrentUserInfo();
            var partsSalesCategoryIds = ObjectContext.PersonSalesCenterLinks.Where(r => r.PersonId == userinfo.Id).Select(r => r.PartsSalesCategoryId).ToArray();
            if(!partsSalesCategoryIds.Contains(partsPurchaseSettleBill.PartsSalesCategoryId.Value))
                throw new ValidationException(ErrorStrings.PartsPurchaseSettleBill_Validation11);
            var dbpartsPurchaseSettleBill = ObjectContext.PartsPurchaseSettleBills.Where(r => r.Id == partsPurchaseSettleBill.Id && r.Status == (int)DcsPartsPurchaseSettleStatus.发票登记).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbpartsPurchaseSettleBill == null)
                throw new ValidationException(ErrorStrings.PartsPurchaseSettleBill_Validation4);
            var partsPurchaseSettleRefs = partsPurchaseSettleBill.PartsPurchaseSettleRefs;
            var partsOutboundBillIds = partsPurchaseSettleRefs.Where(r => r.SourceType == (int)DcsPartsPurchaseSettleRefSourceType.配件出库单).Select(r => r.SourceId);
            var partsInboundCheckBillIds = partsPurchaseSettleRefs.Where(r => r.SourceType == (int)DcsPartsPurchaseSettleRefSourceType.配件入库检验单).Select(r => r.SourceId);
            var partsOutboundBills = ObjectContext.PartsOutboundBills.Where(r => partsOutboundBillIds.Contains(r.Id)).ToArray();
            var partsInboundCheckBills = ObjectContext.PartsInboundCheckBills.Where(r => partsInboundCheckBillIds.Contains(r.Id)).ToArray();
            foreach(var partsPurchaseSettleRef in partsPurchaseSettleRefs) {
                switch(partsPurchaseSettleRef.SourceType) {
                    case (int)DcsPartsPurchaseSettleRefSourceType.配件出库单:
                        var partsOutboundBill = partsOutboundBills.SingleOrDefault(r => r.Id == partsPurchaseSettleRef.SourceId);
                        if(partsOutboundBill == null)
                            throw new ValidationException(string.Format(ErrorStrings.PartsPurchaseSettleBill_Validation8, partsPurchaseSettleRef.SourceCode));
                        partsOutboundBill.SettlementStatus = (int)DcsPartsSettlementStatus.待结算;
                        new PartsOutboundBillAch(this.DomainService).UpdatePartsOutboundBillValidate(partsOutboundBill);
                        break;
                    case (int)DcsPartsPurchaseSettleRefSourceType.配件入库检验单:
                        var partsInboundCheckBill = partsInboundCheckBills.SingleOrDefault(r => r.Id == partsPurchaseSettleRef.SourceId);
                        if(partsInboundCheckBill == null)
                            throw new ValidationException(string.Format(ErrorStrings.PartsPurchaseSettleBill_Validation9, partsPurchaseSettleRef.SourceCode));
                        partsInboundCheckBill.SettlementStatus = (int)DcsPartsSettlementStatus.待结算;
                        new PartsInboundCheckBillAch(this.DomainService).UpdatePartsInboundCheckBillValidate(partsInboundCheckBill);
                        break;
                }
            }
            var insertPartsPurchaseSettleBill = new PartsPurchaseSettleBill {
                WarehouseId = partsPurchaseSettleBill.WarehouseId,
                WarehouseCode = partsPurchaseSettleBill.WarehouseCode,
                WarehouseName = partsPurchaseSettleBill.WarehouseName,
                PartsSalesCategoryId = partsPurchaseSettleBill.PartsSalesCategoryId,
                PartsSalesCategoryName = partsPurchaseSettleBill.PartsSalesCategoryName,
                BranchId = partsPurchaseSettleBill.BranchId,
                BranchCode = partsPurchaseSettleBill.BranchCode,
                BranchName = partsPurchaseSettleBill.BranchName,
                PartsSupplierId = partsPurchaseSettleBill.PartsSupplierId,
                PartsSupplierCode = partsPurchaseSettleBill.PartsSupplierCode,
                PartsSupplierName = partsPurchaseSettleBill.PartsSupplierName,
                TotalSettlementAmount = -partsPurchaseSettleBill.TotalSettlementAmount,
                OffsettedSettlementBillId = partsPurchaseSettleBill.Id,
                OffsettedSettlementBillCode = partsPurchaseSettleBill.Code,
                TaxRate = partsPurchaseSettleBill.TaxRate,
                Tax = -partsPurchaseSettleBill.Tax,
                InvoiceAmountDifference = 0,
                TotalCostAmount = 0,
                CostAmountDifference = 0,
                Status = (int)DcsPartsPurchaseSettleStatus.已审批,
                SettlementPath = (int)DcsPartsPurchaseSettleBillSettlementPath.反冲结算,
            };
            InsertToDatabase(insertPartsPurchaseSettleBill);
            this.InsertPartsPurchaseSettleBillValidate(insertPartsPurchaseSettleBill);
            //采购结算单过账(insertPartsPurchaseSettleBill); //过账操作放在SAP回传发票之时，故此处将过账操作屏蔽
            var invoiceInformations = ObjectContext.InvoiceInformations.Where(r => r.SourceId == partsPurchaseSettleBill.Id && r.SourceType == (int)DcsInvoiceInformationSourceType.配件采购结算单 && r.Status != (int)DcsInvoiceInformationStatus.作废);
            foreach(var invoiceInformation in invoiceInformations) {
                invoiceInformation.Status = (int)DcsInvoiceInformationStatus.作废;
                new InvoiceInformationAch(this.DomainService).UpdateInvoiceInformationValidate(invoiceInformation);
            }
            UpdateToDatabase(partsPurchaseSettleBill);
            partsPurchaseSettleBill.Status = (int)DcsPartsPurchaseSettleStatus.已反冲;
            this.UpdatePartsPurchaseSettleBillValidate(partsPurchaseSettleBill);
        }


        public void 作废配件采购结算单(PartsPurchaseSettleBill partsPurchaseSettleBill) {
            var userinfo = Utils.GetCurrentUserInfo();
            //var partsSalesCategoryIds = ObjectContext.PersonSalesCenterLinks.Where(r => r.PersonId == userinfo.Id).Select(r => r.PartsSalesCategoryId).ToArray();
            //if(!partsSalesCategoryIds.Contains(partsPurchaseSettleBill.PartsSalesCategoryId.Value))
            //    throw new ValidationException(ErrorStrings.PartsPurchaseSettleBill_Validation12);
            var dbpartsPurchaseSettleBill = ObjectContext.PartsPurchaseSettleBills.Where(r => r.Id == partsPurchaseSettleBill.Id && r.Status == (int)DcsPartsPurchaseSettleStatus.新建).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbpartsPurchaseSettleBill == null)
                throw new ValidationException(ErrorStrings.PartsPurchaseSettleBill_Validation17);
            var partsPurchaseSettleRefs = partsPurchaseSettleBill.PartsPurchaseSettleRefs;
            var partsOutboundBillIds = partsPurchaseSettleRefs.Where(r => r.SourceType == (int)DcsPartsPurchaseSettleRefSourceType.配件出库单).Select(r => r.SourceId);
            var partsInboundCheckBillIds = partsPurchaseSettleRefs.Where(r => r.SourceType == (int)DcsPartsPurchaseSettleRefSourceType.配件入库检验单).Select(r => r.SourceId);
            var partsOutboundBills = ObjectContext.PartsOutboundBills.Where(r => partsOutboundBillIds.Contains(r.Id)).ToArray();
            var partsInboundCheckBills = ObjectContext.PartsInboundCheckBills.Where(r => partsInboundCheckBillIds.Contains(r.Id)).ToArray();
            foreach(var partsPurchaseSettleRef in partsPurchaseSettleRefs) {
                switch(partsPurchaseSettleRef.SourceType) {
                    case (int)DcsPartsPurchaseSettleRefSourceType.配件出库单:
                        var partsOutboundBill = partsOutboundBills.SingleOrDefault(r => r.Id == partsPurchaseSettleRef.SourceId);
                        if(partsOutboundBill == null)
                            throw new ValidationException(string.Format(ErrorStrings.PartsPurchaseSettleBill_Validation8, partsPurchaseSettleRef.SourceCode));
                        partsOutboundBill.SettlementStatus = (int)DcsPartsSettlementStatus.待结算;
                        new PartsOutboundBillAch(this.DomainService).UpdatePartsOutboundBillValidate(partsOutboundBill);
                        break;
                    case (int)DcsPartsPurchaseSettleRefSourceType.配件入库检验单:
                        var partsInboundCheckBill = partsInboundCheckBills.SingleOrDefault(r => r.Id == partsPurchaseSettleRef.SourceId);
                        if(partsInboundCheckBill == null)
                            throw new ValidationException(string.Format(ErrorStrings.PartsPurchaseSettleBill_Validation9, partsPurchaseSettleRef.SourceCode));
                        partsInboundCheckBill.SettlementStatus = (int)DcsPartsSettlementStatus.待结算;
                        new PartsInboundCheckBillAch(this.DomainService).UpdatePartsInboundCheckBillValidate(partsInboundCheckBill);
                        break;
                }
            }
            UpdateToDatabase(partsPurchaseSettleBill);
            partsPurchaseSettleBill.Status = (int)DcsPartsPurchaseSettleStatus.作废;
            var userInfo = Utils.GetCurrentUserInfo();
            partsPurchaseSettleBill.AbandonerId = userInfo.Id;
            partsPurchaseSettleBill.AbandonerName = userInfo.Name;
            partsPurchaseSettleBill.AbandonTime = DateTime.Now;
            this.UpdatePartsPurchaseSettleBillValidate(partsPurchaseSettleBill);
        }


        public void 作废虚拟配件采购结算单(VirtualPartsPurchaseSettleBillWithSumPlannedPrice virtualPartsPurchaseSettleBillWithSumPlannedPrice) {
            var partsPurchaseSettleBill = ObjectContext.PartsPurchaseSettleBills.Where(r => r.Id == virtualPartsPurchaseSettleBillWithSumPlannedPrice.Id).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(partsPurchaseSettleBill == null)
                throw new ValidationException(ErrorStrings.BottomStock_Validation3);
            var userinfo = Utils.GetCurrentUserInfo();
            var partsSalesCategoryIds = ObjectContext.PersonSalesCenterLinks.Where(r => r.PersonId == userinfo.Id).Select(r => r.PartsSalesCategoryId).ToArray();
            if(!partsSalesCategoryIds.Contains(partsPurchaseSettleBill.PartsSalesCategoryId.Value))
                throw new ValidationException(ErrorStrings.PartsPurchaseSettleBill_Validation12);
            var dbpartsPurchaseSettleBill = ObjectContext.PartsPurchaseSettleBills.Where(r => r.Id == partsPurchaseSettleBill.Id && r.Status == (int)DcsPartsPurchaseSettleStatus.新建).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbpartsPurchaseSettleBill == null)
                throw new ValidationException(ErrorStrings.PartsPurchaseSettleBill_Validation5);
            var partsPurchaseSettleRefs = partsPurchaseSettleBill.PartsPurchaseSettleRefs;
            var partsOutboundBillIds = partsPurchaseSettleRefs.Where(r => r.SourceType == (int)DcsPartsPurchaseSettleRefSourceType.配件出库单).Select(r => r.SourceId);
            var partsInboundCheckBillIds = partsPurchaseSettleRefs.Where(r => r.SourceType == (int)DcsPartsPurchaseSettleRefSourceType.配件入库检验单).Select(r => r.SourceId);
            var partsOutboundBills = ObjectContext.PartsOutboundBills.Where(r => partsOutboundBillIds.Contains(r.Id)).ToArray();
            var partsInboundCheckBills = ObjectContext.PartsInboundCheckBills.Where(r => partsInboundCheckBillIds.Contains(r.Id)).ToArray();
            foreach(var partsPurchaseSettleRef in partsPurchaseSettleRefs) {
                switch(partsPurchaseSettleRef.SourceType) {
                    case (int)DcsPartsPurchaseSettleRefSourceType.配件出库单:
                        var partsOutboundBill = partsOutboundBills.SingleOrDefault(r => r.Id == partsPurchaseSettleRef.SourceId);
                        if(partsOutboundBill == null)
                            throw new ValidationException(string.Format(ErrorStrings.PartsPurchaseSettleBill_Validation8, partsPurchaseSettleRef.SourceCode));
                        partsOutboundBill.SettlementStatus = (int)DcsPartsSettlementStatus.待结算;
                        new PartsOutboundBillAch(this.DomainService).UpdatePartsOutboundBillValidate(partsOutboundBill);
                        break;
                    case (int)DcsPartsPurchaseSettleRefSourceType.配件入库检验单:
                        var partsInboundCheckBill = partsInboundCheckBills.SingleOrDefault(r => r.Id == partsPurchaseSettleRef.SourceId);
                        if(partsInboundCheckBill == null)
                            throw new ValidationException(string.Format(ErrorStrings.PartsPurchaseSettleBill_Validation9, partsPurchaseSettleRef.SourceCode));
                        partsInboundCheckBill.SettlementStatus = (int)DcsPartsSettlementStatus.待结算;
                        new PartsInboundCheckBillAch(this.DomainService).UpdatePartsInboundCheckBillValidate(partsInboundCheckBill);
                        break;
                }
            }
            UpdateToDatabase(partsPurchaseSettleBill);
            partsPurchaseSettleBill.Status = (int)DcsPartsPurchaseSettleStatus.作废;
            var userInfo = Utils.GetCurrentUserInfo();
            partsPurchaseSettleBill.AbandonerId = userInfo.Id;
            partsPurchaseSettleBill.AbandonerName = userInfo.Name;
            partsPurchaseSettleBill.AbandonTime = DateTime.Now;
            this.UpdatePartsPurchaseSettleBillValidate(partsPurchaseSettleBill);

            //处理虚拟积分清单
            var bonusPointsOrders3 = this.ObjectContext.BonusPointsOrders.Where(r => partsOutboundBillIds.Contains(r.PartsOutboundBillId ?? 0));
            var bonusPointsOrders4 = this.ObjectContext.BonusPointsOrders.Where(r => partsInboundCheckBillIds.Contains(r.PartsInboundCheckBillId ?? 0));

            //处理虚拟运费清单
            var ecommerceFreightDisposals1 = this.ObjectContext.EcommerceFreightDisposals.Where(r => partsInboundCheckBillIds.Contains(r.PartsInboundCheckBillId ?? 0));
            var returnFreightDisposals1 = this.ObjectContext.ReturnFreightDisposals.Where(r => partsOutboundBillIds.Contains(r.PartsOutboundBillId ?? 0));

            foreach(var item in bonusPointsOrders3) {
                item.PurchaseSettlementStatus = null;
                item.PartsPurchaseSettleBillId = null;
                item.PartsPurchaseSettleBillCode = string.Empty;
            }
            foreach(var item in bonusPointsOrders4) {
                item.PurchaseSettlementStatus = null;
                item.PartsPurchaseSettleBillId = null;
                item.PartsPurchaseSettleBillCode = string.Empty;
            }
            foreach(var item in ecommerceFreightDisposals1) {
                item.Status = (int)DcsFreightDisposalStatus.待结算;
                item.PartsPurchaseSettleBillId = null;
                item.PartsPurchaseSettleBillCode = string.Empty;
                item.PurchaseSettlementStatus = null;
            }
            foreach(var item in returnFreightDisposals1) {
                item.Status = (int)DcsFreightDisposalStatus.待结算;
                item.PartsPurchaseSettleBillId = null;
                item.PartsPurchaseSettleBillCode = string.Empty;
                item.PurchaseSettlementStatus = null;
            }
        }

        //
        //public void 配件采购结算单发票登记(PartsPurchaseSettleBill partsPurchaseSettleBill, decimal invoiceAmount, decimal invoiceTax) {
        //    var dbpartsPurchaseSettleBill = ObjectContext.PartsPurchaseSettleBills.Where(r => r.Id == partsPurchaseSettleBill.Id && r.Status == (int)DcsPartsPurchaseSettleStatus.已审批).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
        //    if(dbpartsPurchaseSettleBill == null)
        //        throw new ValidationException(ErrorStrings.PartsPurchaseSettleBill_Validation7);

        //    UpdateToDatabase(partsPurchaseSettleBill);
        //    partsPurchaseSettleBill.Status = (int)DcsPartsPurchaseSettleStatus.发票登记;
        //    partsPurchaseSettleBill.InvoiceFlag = (int)DcsInvoiceFlag.一结算单多发票;

        //    this.UpdatePartsPurchaseSettleBillValidate(partsPurchaseSettleBill);
        //}


        public void 配件采购结算单发票登记(PartsPurchaseSettleBill partsPurchaseSettleBill, InvoiceInformation[] invoiceInformations) {
            if(invoiceInformations == null || invoiceInformations.Length == 0) {
                throw new ValidationException(ErrorStrings.PartsPurchaseSettleBill_Validation18);
            }
            var dbpartsPurchaseSettleBill = ObjectContext.PartsPurchaseSettleBills.Where(r => r.Id == partsPurchaseSettleBill.Id && r.Status == (int)DcsPartsPurchaseSettleStatus.已审批).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbpartsPurchaseSettleBill == null) {
                throw new ValidationException(ErrorStrings.PartsPurchaseSettleBill_Validation19);
            }
            var invoiceNumbers = invoiceInformations.Select(r => r.InvoiceNumber).ToArray();
            var dbInvoiceInformation = ObjectContext.InvoiceInformations.Where(r =>  r.Status != (int)DcsInvoiceInformationStatus.作废).SetMergeOption(MergeOption.NoTracking).ToArray();

            foreach(var item in invoiceInformations) {
                var oldItem = dbInvoiceInformation.Where(t=>t.InvoiceCode==item.InvoiceCode && t.InvoiceNumber==item.InvoiceNumber).FirstOrDefault();
                if(oldItem != null) {
                    throw new ValidationException(string.Format(ErrorStrings.PartsPurchaseSettleBill_Validation20,oldItem.Code+"+"+ oldItem.InvoiceNumber));
                }
            }
           
            partsPurchaseSettleBill.Status = (int)DcsPartsPurchaseSettleStatus.发票登记;
            partsPurchaseSettleBill.InvoiceFlag = (int)DcsInvoiceFlag.一结算单多发票;
            UpdateToDatabase(partsPurchaseSettleBill);
            var userInfo = Utils.GetCurrentUserInfo();
            var companyId = invoiceInformations.First().OwnerCompanyId;
            var compy = ObjectContext.Companies.SingleOrDefault(r => r.Id == companyId);
            foreach(var invoiceInformation in invoiceInformations) {
                invoiceInformation.PartsSalesCategoryId = partsPurchaseSettleBill.PartsSalesCategoryId;
                invoiceInformation.SourceType = (int)DcsInvoiceInformationSourceType.配件采购结算单;
                invoiceInformation.SourceId = partsPurchaseSettleBill.Id;
                invoiceInformation.SourceCode = partsPurchaseSettleBill.Code;
                if(compy == null) {
                    throw new ValidationException(ErrorStrings.PartsPurchaseSettleBill_Validation21);
                }
                if(string.IsNullOrWhiteSpace(invoiceInformation.Code) || invoiceInformation.Code == GlobalVar.ASSIGNED_BY_SERVER) {
                    invoiceInformation.Code = CodeGenerator.Generate("InvoiceInformation", compy.Code);
                }
                invoiceInformation.SourceType = (int)DcsInvoiceInformationSourceType.配件采购结算单;
                //四舍五入,保留两位小数
                if(invoiceInformation.InvoiceAmount.HasValue) {
                    invoiceInformation.InvoiceAmount = Math.Round(invoiceInformation.InvoiceAmount.Value, 2, MidpointRounding.AwayFromZero);
                }
                if(invoiceInformation.InvoiceTax.HasValue) {
                    invoiceInformation.InvoiceTax = Math.Round(invoiceInformation.InvoiceTax.Value, 2, MidpointRounding.AwayFromZero);
                }

                invoiceInformation.CreateTime = DateTime.Now;
                invoiceInformation.CreatorId = userInfo.Id;
                invoiceInformation.CreatorName = userInfo.Name;
                InsertToDatabase(invoiceInformation);
                var newRef = new PurchaseSettleInvoiceRel();
                newRef.PartsPurchaseSettleBill = partsPurchaseSettleBill;
                newRef.InvoiceInformation = invoiceInformation;
                this.InsertToDatabase(newRef);
            }
            this.ObjectContext.SaveChanges();



        }

        public void 多条配件采购结算单发票登记(InvoiceInformation invoiceInformation, int[] partsPurchaseSettleBillIds) {
            //  1、逐条更新配件采购结算单（id in参数采购结算单id数组）
            //     状态=发票登记，发票标记=多结算单单发票
            //同时对发票bo调用
            //发票信息.新增发票

            invoiceInformation.SourceType = (int)DcsInvoiceInformationSourceType.配件采购结算单;
            if(partsPurchaseSettleBillIds == null || partsPurchaseSettleBillIds.Length == 0) {
                throw new ValidationException(ErrorStrings.PartsPurchaseSettleBill_Validation22);
            }
            if(invoiceInformation == null) {
                throw new ValidationException(ErrorStrings.PartsPurchaseSettleBill_Validation23);
            }

            var partsPurchaseSettleBills = this.ObjectContext.PartsPurchaseSettleBills.Where(r => partsPurchaseSettleBillIds.Contains(r.Id)).ToArray();
            if(partsPurchaseSettleBills.Any(r => r.Status != (int)DcsPartsPurchaseSettleStatus.已审批)) {
                throw new ValidationException(ErrorStrings.PartsPurchaseSettleBill_Validation24);
            }
            var dbInvoiceInformation = ObjectContext.InvoiceInformations.Where(r => r.InvoiceNumber == invoiceInformation.InvoiceNumber).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbInvoiceInformation != null) {
                throw new ValidationException(ErrorStrings.PartsPurchaseSettleBill_Validation25);
            }
            var userInfo = Utils.GetCurrentUserInfo();

            var companyId = invoiceInformation.OwnerCompanyId;
            var compy = ObjectContext.Companies.SingleOrDefault(r => r.Id == companyId);
            if(compy == null) {
                throw new ValidationException(ErrorStrings.PartsPurchaseSettleBill_Validation21);
            }
            if(string.IsNullOrWhiteSpace(invoiceInformation.Code) || invoiceInformation.Code == GlobalVar.ASSIGNED_BY_SERVER) {
                invoiceInformation.Code = CodeGenerator.Generate("InvoiceInformation", compy.Code);
            }
            //var tempTaxValue = invoiceInformation.InvoiceTax == null ? 0 : invoiceInformation.InvoiceTax.Value;
            //invoiceInformation.InvoiceTax = Math.Round(tempTaxValue, 2);

            //四舍五入,保留两位小数
            if(invoiceInformation.InvoiceAmount.HasValue) {
                invoiceInformation.InvoiceAmount = Math.Round(invoiceInformation.InvoiceAmount.Value, 2, MidpointRounding.AwayFromZero);
            }
            if(invoiceInformation.InvoiceTax.HasValue) {
                invoiceInformation.InvoiceTax = Math.Round(invoiceInformation.InvoiceTax.Value, 2, MidpointRounding.AwayFromZero);
            }
            invoiceInformation.CreatorId = userInfo.Id;
            invoiceInformation.CreateTime = DateTime.Now;
            invoiceInformation.CreatorName = userInfo.Name;
            this.InsertToDatabase(invoiceInformation);
            foreach(var item in partsPurchaseSettleBills) {
                item.Status = (int)DcsPartsPurchaseSettleStatus.发票登记;
                item.InvoiceFlag = (int)DcsInvoiceFlag.多结算单一发票;
                item.ModifierId = userInfo.Id;
                item.ModifyTime = DateTime.Now;
                item.ModifierName = userInfo.Name;
                var newRef = new PurchaseSettleInvoiceRel();
                newRef.PartsPurchaseSettleBillId = item.Id;
                newRef.InvoiceInformation = invoiceInformation;
                this.InsertToDatabase(newRef);
            }
            this.ObjectContext.SaveChanges();
        }

        public void 审核采购发票(PartsPurchaseSettleBill partsPurchaseSettleBill, bool isPass) {
            var dbPartsPurchaseSettleBill = ObjectContext.PartsPurchaseSettleBills.Where(r => r.Id == partsPurchaseSettleBill.Id).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbPartsPurchaseSettleBill == null || !(dbPartsPurchaseSettleBill.Status == (int)DcsPartsPurchaseSettleStatus.发票驳回 || dbPartsPurchaseSettleBill.Status == (int)DcsPartsPurchaseSettleStatus.发票登记)) {
                throw new ValidationException(ErrorStrings.PartsPurchaseSettleBill_Validation26);
            }
            var userInfo = Utils.GetCurrentUserInfo();
            partsPurchaseSettleBill.ModifierId = userInfo.Id;
            partsPurchaseSettleBill.ModifyTime = DateTime.Now;
            partsPurchaseSettleBill.ModifierName = userInfo.Name;
            partsPurchaseSettleBill.InvoiceApproverId = userInfo.Id;
            partsPurchaseSettleBill.InvoiceApproveTime = DateTime.Now;
            partsPurchaseSettleBill.InvoiceApproverName = userInfo.Name;
            if(isPass) {
                partsPurchaseSettleBill.Status = (int)DcsPartsPurchaseSettleStatus.发票已审核;
                var invoiceInformationsQuery = from a in this.ObjectContext.PurchaseSettleInvoiceRels
                                               from b in this.ObjectContext.InvoiceInformations
                                               where a.InvoiceId == b.Id && a.PartsPurchaseSettleBillId == partsPurchaseSettleBill.Id
                                               select b;
                var invoiceInformations = invoiceInformationsQuery.ToArray();
                foreach(var invoiceInformation in invoiceInformations) {

                    invoiceInformation.Status = (int)DcsInvoiceInformationStatus.生效;
                    invoiceInformation.ApproverId = userInfo.Id;
                    invoiceInformation.ApproveTime = DateTime.Now;
                    invoiceInformation.ApproverName = userInfo.Name;
                    invoiceInformation.ModifierId = userInfo.Id;
                    invoiceInformation.ModifyTime = DateTime.Now;
                    invoiceInformation.ModifierName = userInfo.Name;
                    UpdateToDatabase(invoiceInformation);
                }
                //Sync18TableInsert(partsPurchaseSettleBill);
                //倒冲出库单的采购结算单号 by mk
                if (partsPurchaseSettleBill.IsUnifiedSettle==true) {
                    var dbPartsOutboundBillQuery = from a in this.ObjectContext.PartsPurchaseSettleRefs
                                              from b in this.ObjectContext.PartsInboundCheckBills
                                              from c in this.ObjectContext.PartsOutboundBills
                                              where a.SourceCode == b.Code && b.CPPartsInboundCheckCode == c.CPPartsInboundCheckCode && a.PartsPurchaseSettleBillId== dbPartsPurchaseSettleBill.Id
                                                   select c;
                    var dbPartsOutboundBills = dbPartsOutboundBillQuery.ToArray();
                    foreach (var dbPartsOutboundBill in dbPartsOutboundBills)
                    {
                        dbPartsOutboundBill.PartsPurchaseSettleCode = dbPartsPurchaseSettleBill.Code;
                        UpdateToDatabase(dbPartsOutboundBill);
                    }
                }

            } else {
                partsPurchaseSettleBill.Status = (int)DcsPartsPurchaseSettleStatus.发票驳回;
            }
        }


        public void 审核多条采购结算单发票(PartsPurchaseSettleBill partsPurchaseSettleBill, bool isPass, int invoiceId) {
            //查询发票信息（id=参数 发票Id）
            //如果 是否通过=是
            //更新采购结算单 状态=发票已审核 (发票信息.id=采购结算单发票关联单.发票id,采购结算单发票关联单.采购结算单id=采购结算单.Id)
            //更新 发票信息状态=生效
            //如果是否通过=否
            //更新采购结算单 状态=发票驳回(发票信息.id=采购结算单发票关联单.发票id,采购结算单发票关联单.采购结算单id=采购结算单.Id)
            //新增sap同步队列19 赋值：业务编号Id=发票信息.Id
            //业务单据编号=发票信息.发票号
            //业务单据类型=发票信息
            var invoiceInformation = ObjectContext.InvoiceInformations.SingleOrDefault(r => r.Id == invoiceId);
            if(invoiceInformation == null) {
                throw new ValidationException(ErrorStrings.PartsPurchaseSettleBill_Validation27);
            }
            if(invoiceInformation.Status == (int)DcsInvoiceInformationStatus.生效) {
                throw new ValidationException(ErrorStrings.PartsPurchaseSettleBill_Validation28);
            }
            var partsPurchaseSettleBillsQuery = from a in ObjectContext.PurchaseSettleInvoiceRels
                                                from b in ObjectContext.PartsPurchaseSettleBills
                                                where a.InvoiceId == invoiceId && a.PartsPurchaseSettleBillId == b.Id
                                                select b;
            var partsPurchaseSettleBills = partsPurchaseSettleBillsQuery.ToArray();
            var userInfo = Utils.GetCurrentUserInfo();
            invoiceInformation.ModifierId = userInfo.Id;
            invoiceInformation.ModifyTime = DateTime.Now;
            invoiceInformation.ModifierName = userInfo.Name;
            invoiceInformation.ApproverId = userInfo.Id;
            invoiceInformation.ApproveTime = DateTime.Now;
            invoiceInformation.ApproverName = userInfo.Name;
            if(isPass) {
                invoiceInformation.Status = (int)DcsInvoiceInformationStatus.生效;
                if(partsPurchaseSettleBills.Any(r => r.Status != (int)DcsPartsPurchaseSettleStatus.发票登记 && r.Status != (int)DcsPartsPurchaseSettleStatus.发票驳回)) {
                    throw new ValidationException(ErrorStrings.PartsPurchaseSettleBill_Validation29);
                }
                foreach(var purchaseSettleBill in partsPurchaseSettleBills) {
                    purchaseSettleBill.Status = (int)DcsPartsPurchaseSettleStatus.发票已审核;
                    purchaseSettleBill.ModifierId = userInfo.Id;
                    purchaseSettleBill.ModifyTime = DateTime.Now;
                    purchaseSettleBill.ModifierName = userInfo.Name;
                    purchaseSettleBill.InvoiceApproverId = userInfo.Id;
                    purchaseSettleBill.InvoiceApproveTime = DateTime.Now;
                    purchaseSettleBill.InvoiceApproverName = userInfo.Name;
                    UpdateToDatabase(purchaseSettleBill);

                    //倒冲出库单的采购结算单号 by mk
                    if (purchaseSettleBill.IsUnifiedSettle == true)
                    {
                        var dbPartsOutboundBillQuery = from a in this.ObjectContext.PartsPurchaseSettleRefs
                                                       from b in this.ObjectContext.PartsInboundCheckBills
                                                       from c in this.ObjectContext.PartsOutboundBills
                                                       where a.SourceCode == b.Code && b.CPPartsInboundCheckCode == c.CPPartsInboundCheckCode && a.PartsPurchaseSettleBillId == purchaseSettleBill.Id
                                                       select c;
                        var dbPartsOutboundBills = dbPartsOutboundBillQuery.ToArray();
                        foreach (var dbPartsOutboundBill in dbPartsOutboundBills)
                        {
                            dbPartsOutboundBill.PartsPurchaseSettleCode = purchaseSettleBill.Code;
                            UpdateToDatabase(dbPartsOutboundBill);
                        }
                    }
                }

                var branchCode = partsPurchaseSettleBills.First().BranchCode;
                //Sync19TableInsert(invoiceInformation, branchCode);

            } else {
                if(partsPurchaseSettleBills.Any(r => r.Status != (int)DcsPartsPurchaseSettleStatus.发票登记)) {
                    throw new ValidationException(ErrorStrings.PartsPurchaseSettleBill_Validation30);
                }
                foreach(var purchaseSettleBill in partsPurchaseSettleBills) {
                    purchaseSettleBill.Status = (int)DcsPartsPurchaseSettleStatus.发票驳回;
                    purchaseSettleBill.ModifierId = userInfo.Id;
                    purchaseSettleBill.ModifyTime = DateTime.Now;
                    purchaseSettleBill.ModifierName = userInfo.Name;
                    purchaseSettleBill.InvoiceApproverId = userInfo.Id;
                    purchaseSettleBill.InvoiceApproveTime = DateTime.Now;
                    purchaseSettleBill.InvoiceApproverName = userInfo.Name;
                    UpdateToDatabase(purchaseSettleBill);
                }
            }
            UpdateToDatabase(invoiceInformation);
        }


        public void 配件采购结算单发票审核(PartsPurchaseSettleBill partsPurchaseSettleBill, decimal invoiceAmount, decimal invoiceTax) {
            var dbpartsPurchaseSettleBill = ObjectContext.PartsPurchaseSettleBills.Where(r => r.Id == partsPurchaseSettleBill.Id && (r.Status == (int)DcsPartsPurchaseSettleStatus.发票登记 || r.Status == (int)DcsPartsPurchaseSettleStatus.发票驳回)).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbpartsPurchaseSettleBill == null)
                throw new ValidationException(ErrorStrings.PartsPurchaseSettleBill_Validation26);
            if(invoiceAmount <= 0)
                throw new ValidationException(ErrorStrings.PartsPurchaseSettleBill_Validation6);
            UpdateToDatabase(partsPurchaseSettleBill);
            //0为发票审核通过，1为驳回
            if(partsPurchaseSettleBill.IsPassed == 0) {
                CheckInvoiceCanApprove(partsPurchaseSettleBill.Id, partsPurchaseSettleBill.Code);
                partsPurchaseSettleBill.Status = (int)DcsPartsPurchaseSettleStatus.发票已审核;
                partsPurchaseSettleBill.InvoiceAmountDifference = partsPurchaseSettleBill.TotalSettlementAmount - invoiceAmount;
                partsPurchaseSettleBill.CostAmountDifference = partsPurchaseSettleBill.TotalCostAmount - invoiceAmount;
                this.UpdatePartsPurchaseSettleBillValidate(partsPurchaseSettleBill);
                new SupplierAccountAch(this.DomainService).采购结算单过账(partsPurchaseSettleBill);
                //更新发票状态
                var invoiceInformations = ObjectContext.InvoiceInformations.Where(r => r.SourceCode == partsPurchaseSettleBill.Code).ToArray();
                if(invoiceInformations.Length < 1) {
                    throw new ValidationException(ErrorStrings.PartsPurchaseSettleBill_Validation27);
                }

                foreach(var invoiceInformation in invoiceInformations) {
                    UpdateToDatabase(invoiceInformation);
                    invoiceInformation.Status = (int)DcsInvoiceInformationStatus.生效;
                    new InvoiceInformationAch(this.DomainService).UpdateInvoiceInformationValidate(invoiceInformation);
                }

            } else {
                partsPurchaseSettleBill.Status = (int)DcsPartsPurchaseSettleStatus.发票驳回;
            }
            this.UpdatePartsPurchaseSettleBillValidate(partsPurchaseSettleBill);
        }


        public void CheckInvoiceCanApprove(int partsPurchaseSettleBillid, string partsPurchaseSettleBillCode) {
            var tempEntities1 = ObjectContext.ExecuteStoreQuery<int>("select nowsyncnumber from statusofsync where synctypename='SAP_8_SYNC'");
            var nowsyncnumber = tempEntities1.First();
            var tempEntities2 = this.ObjectContext.ExecuteStoreQuery<int>("SELECT COUNT(BUSSINESSID) FROM SAP_8_SYNC WHERE BUSSINESSID=" + partsPurchaseSettleBillid + " AND BUSSINESSTABLENAME='PARTSPURCHASESETTLEBILL' AND SYNCNUMBER>" + nowsyncnumber);
            var tempCount = tempEntities2.First();
            if(tempCount >= 1) {
                throw new ValidationException(ErrorStrings.PartsPurchaseSettleBill_Validation31);
            }
            if(tempCount == 0) {
                var tempEntity3 = this.ObjectContext.ExecuteStoreQuery<int>(string.Format("select count(*) from errortable_sap_yx where status=1 and bussinesscode='{0}'", partsPurchaseSettleBillCode));
                var unhandledcount = tempEntity3.First();
                if(unhandledcount >= 1) {
                    throw new ValidationException(ErrorStrings.PartsPurchaseSettleBill_Validation32);
                }
            }
        }


        public void 修改采购发票信息及采购结算单(int[] nowPartsPurchaseSettleBillIds, InvoiceInformation invoiceInformation) {
            //查询采购结算单发票关联单（采购结算单发票关联单.发票Id=参数 发票信息.id）
            //如果查询的采购结算单发票关联单的id数组 不存在于参数 采购结算单id数组中
            //更新不存在的采购结算单.状态=已审批（id in 不存在采购结算单发票关联单.采购结算单id）
            //删除对应不存在采购结算单发票关联单
            //如果采购结算单id数组 不存在于采购结算单发票关联单中 ，新增对应的采购结算单发票关联单
            //赋值 采购结算单发票关联单.发票Id=参数 发票信息.Id ,采购结算单id in 参数 采购结算单id数组
            //根据发票信息bo 更新发票信息
            var originalPurchaseSettleInvoiceRels = ObjectContext.PurchaseSettleInvoiceRels.Where(r => r.InvoiceId == invoiceInformation.Id).ToArray();
            int i = 1;
            var nowPurchaseSettleInvoiceRels = nowPartsPurchaseSettleBillIds.Select(r => new PurchaseSettleInvoiceRel {
                InvoiceId = invoiceInformation.Id,
                PartsPurchaseSettleBillId = r,
                Id = i++
            }).ToArray();
            var purchaseSettleInvoiceRelOfAdd = nowPurchaseSettleInvoiceRels.Where(r => !originalPurchaseSettleInvoiceRels.Any(v => v.InvoiceId == r.InvoiceId && v.PartsPurchaseSettleBillId == r.PartsPurchaseSettleBillId)).ToArray();
            foreach(var purchaseSettleInvoiceRel in purchaseSettleInvoiceRelOfAdd) {
                this.InsertToDatabase(purchaseSettleInvoiceRel);
            }

            var purchaseSettleInvoiceRelOfDelete = originalPurchaseSettleInvoiceRels.Where(r => !nowPurchaseSettleInvoiceRels.Any(v => v.InvoiceId == r.InvoiceId && v.PartsPurchaseSettleBillId == r.PartsPurchaseSettleBillId)).ToArray();
            foreach(var purchaseSettleInvoiceRel in purchaseSettleInvoiceRelOfDelete) {
                DeleteFromDatabase(purchaseSettleInvoiceRel);
            }
            var tempPurchaseSettleIdOfAdd = purchaseSettleInvoiceRelOfAdd.Select(r => r.PartsPurchaseSettleBillId).ToArray();
            var tempPurchaseSettleIdOfDelete = purchaseSettleInvoiceRelOfDelete.Select(r => r.PartsPurchaseSettleBillId).ToArray();
            var purchaseSettleIdToRegister = tempPurchaseSettleIdOfAdd.Except(tempPurchaseSettleIdOfDelete);
            var purchaseSettleBillToRegister = ObjectContext.PartsPurchaseSettleBills.Where(r => purchaseSettleIdToRegister.Contains(r.Id)).ToArray();
            var userInfo = Utils.GetCurrentUserInfo();
            // 多结算单一发票修改后剩一张结算单的，需要将结算单结算标识修改为一结算单多发票，因接口传输时一结算单一发票包含在一结算单多发票的情况中
            int settCount = nowPartsPurchaseSettleBillIds.Count();
            if(settCount == 1) {
                int settId = nowPartsPurchaseSettleBillIds[0];
                var purchaseSettleBillToUpdate = ObjectContext.PartsPurchaseSettleBills.Where(r => r.Id == settId).SingleOrDefault();
                purchaseSettleBillToUpdate.InvoiceFlag = (int)DcsInvoiceFlag.一结算单多发票;
                purchaseSettleBillToUpdate.ModifierId = userInfo.Id;
                purchaseSettleBillToUpdate.ModifyTime = DateTime.Now;
                purchaseSettleBillToUpdate.ModifierName = userInfo.Name;
                UpdateToDatabase(purchaseSettleBillToUpdate);
                // 发票需要加上相应源单据信息
                invoiceInformation.SourceId = settId;
                invoiceInformation.SourceCode = purchaseSettleBillToUpdate.Code;
                invoiceInformation.SourceType = (int)DcsInvoiceInformationSourceType.配件采购结算单;
            }
            foreach(var item in purchaseSettleBillToRegister) {
                item.Status = (int)DcsPartsPurchaseSettleStatus.发票登记;
                item.InvoiceFlag = (int)DcsInvoiceFlag.多结算单一发票;
                UpdateToDatabase(item);
                item.ModifierId = userInfo.Id;
                item.ModifyTime = DateTime.Now;
                item.ModifierName = userInfo.Name;
            }
            var purchaseSettleIdToApprove = tempPurchaseSettleIdOfDelete.Except(tempPurchaseSettleIdOfAdd);
            var purchaseSettleBillToApprove = ObjectContext.PartsPurchaseSettleBills.Where(r => purchaseSettleIdToApprove.Contains(r.Id)).ToArray();
            foreach(var item in purchaseSettleBillToApprove) {
                item.Status = (int)DcsPartsPurchaseSettleStatus.已审批;
                item.InvoiceFlag = null;
                UpdateToDatabase(item);
                item.ModifierId = userInfo.Id;
                item.ModifyTime = DateTime.Now;
                item.ModifierName = userInfo.Name;
            }
            UpdateToDatabase(invoiceInformation);
            new InvoiceInformationAch(this.DomainService).UpdateInvoiceInformationValidate(invoiceInformation);
            ObjectContext.SaveChanges();
        }



        public void 发票已审核反结算(VirtualPartsPurchaseSettleBillWithSumPlannedPrice virtualPartsPurchaseSettleBillWithSumPlannedPrice) {
            var partsPurchaseSettleBill = ObjectContext.PartsPurchaseSettleBills.Where(r => r.Id == virtualPartsPurchaseSettleBillWithSumPlannedPrice.Id).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(partsPurchaseSettleBill == null)
                throw new ValidationException(ErrorStrings.BottomStock_Validation3);
            var userinfo = Utils.GetCurrentUserInfo();
            partsPurchaseSettleBill.Status = (int)DcsPartsPurchaseSettleStatus.发票登记;
            partsPurchaseSettleBill.ModifierId = userinfo.Id;
            partsPurchaseSettleBill.ModifyTime = DateTime.Now;
            partsPurchaseSettleBill.ModifierName = userinfo.Name;
            UpdateToDatabase(partsPurchaseSettleBill);
            this.UpdatePartsPurchaseSettleBillValidate(partsPurchaseSettleBill);

            var purchaseSettleInvoiceRel = ObjectContext.PurchaseSettleInvoiceRels.Where(r => r.PartsPurchaseSettleBillId == partsPurchaseSettleBill.Id).Select(r => r.InvoiceId).ToArray();
            if(purchaseSettleInvoiceRel == null)
                throw new ValidationException(ErrorStrings.PartsPurchaseSettleBill_Validation33);
            var invoiceInformations = ObjectContext.InvoiceInformations.Where(r => purchaseSettleInvoiceRel.Contains(r.Id)).ToArray();
            if(invoiceInformations == null)
                throw new ValidationException(ErrorStrings.PartsPurchaseSettleBill_Validation27);
            foreach(var item in invoiceInformations) {
                item.Status = (int)DcsInvoiceInformationStatus.已开票;
                item.ModifierId = userinfo.Id;
                item.ModifyTime = DateTime.Now;
                item.ModifierName = userinfo.Name;
                UpdateToDatabase(item);
                new InvoiceInformationAch(this.DomainService).UpdateInvoiceInformationValidate(item);

            }

        }


    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:11:30
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 生成配件采购结算单(PartsPurchaseSettleBill partsPurchaseSettleBill) {
            new PartsPurchaseSettleBillAch(this).生成配件采购结算单(partsPurchaseSettleBill);
        }


        [Update(UsingCustomMethod = true)]
        public void 修改配件采购结算单(PartsPurchaseSettleBill partsPurchaseSettleBill) {
            new PartsPurchaseSettleBillAch(this).修改配件采购结算单(partsPurchaseSettleBill);
        }

        [Update(UsingCustomMethod = true)]
        public void 审批配件采购结算单(PartsPurchaseSettleBill virtualPartsPurchaseSettleBillWithSumPlannedPrice) {
            new PartsPurchaseSettleBillAch(this).审批配件采购结算单(virtualPartsPurchaseSettleBillWithSumPlannedPrice);
        }

        [Update(UsingCustomMethod = true)]
        public void 审批配件采购结算单(VirtualPartsPurchaseSettleBillWithSumPlannedPrice virtualPartsPurchaseSettleBillWithSumPlannedPrice) {
            new PartsPurchaseSettleBillAch(this).审批配件采购结算单(virtualPartsPurchaseSettleBillWithSumPlannedPrice);
        }


        [Update(UsingCustomMethod = true)]
        public void 反结算配件采购结算单(VirtualPartsPurchaseSettleBillWithSumPlannedPrice virtualPartsPurchaseSettleBillWithSumPlannedPrice) {
            new PartsPurchaseSettleBillAch(this).反结算配件采购结算单(virtualPartsPurchaseSettleBillWithSumPlannedPrice);
        }


        [Update(UsingCustomMethod = true)]
        public void 作废配件采购结算单(PartsPurchaseSettleBill partsPurchaseSettleBill) {
            new PartsPurchaseSettleBillAch(this).作废配件采购结算单(partsPurchaseSettleBill);
        }


        [Update(UsingCustomMethod = true)]
        public void 作废虚拟配件采购结算单(VirtualPartsPurchaseSettleBillWithSumPlannedPrice virtualPartsPurchaseSettleBillWithSumPlannedPrice) {
            new PartsPurchaseSettleBillAch(this).作废虚拟配件采购结算单(virtualPartsPurchaseSettleBillWithSumPlannedPrice);
        }


        [Invoke]
        public void 配件采购结算单发票登记(PartsPurchaseSettleBill partsPurchaseSettleBill, InvoiceInformation[] invoiceInformations) {
            new PartsPurchaseSettleBillAch(this).配件采购结算单发票登记(partsPurchaseSettleBill, invoiceInformations);
        }

        [Invoke]
        public void 多条配件采购结算单发票登记(InvoiceInformation invoiceInformation, int[] partsPurchaseSettleBillIds) {
            new PartsPurchaseSettleBillAch(this).多条配件采购结算单发票登记(invoiceInformation, partsPurchaseSettleBillIds);
        }

        [Update(UsingCustomMethod = true)]
        public void 审核采购发票(PartsPurchaseSettleBill partsPurchaseSettleBill, bool isPass) {
            new PartsPurchaseSettleBillAch(this).审核采购发票(partsPurchaseSettleBill, isPass);
        }


        [Update(UsingCustomMethod = true)]
        public void 审核多条采购结算单发票(PartsPurchaseSettleBill partsPurchaseSettleBill, bool isPass, int invoiceId) {
            new PartsPurchaseSettleBillAch(this).审核多条采购结算单发票(partsPurchaseSettleBill, isPass, invoiceId);
        }


        [Update(UsingCustomMethod = true)]
        public void 配件采购结算单发票审核(PartsPurchaseSettleBill partsPurchaseSettleBill, decimal invoiceAmount, decimal invoiceTax) {
            new PartsPurchaseSettleBillAch(this).配件采购结算单发票审核(partsPurchaseSettleBill, invoiceAmount, invoiceTax);
        }


        public void CheckInvoiceCanApprove(int partsPurchaseSettleBillid, string partsPurchaseSettleBillCode) {
            new PartsPurchaseSettleBillAch(this).CheckInvoiceCanApprove(partsPurchaseSettleBillid, partsPurchaseSettleBillCode);
        }


        [Invoke]
        public void 修改采购发票信息及采购结算单(int[] nowPartsPurchaseSettleBillIds, InvoiceInformation invoiceInformation) {
            new PartsPurchaseSettleBillAch(this).修改采购发票信息及采购结算单(nowPartsPurchaseSettleBillIds, invoiceInformation);
        }

        [Update(UsingCustomMethod = true)]
        public void 发票已审核反结算(VirtualPartsPurchaseSettleBillWithSumPlannedPrice virtualPartsPurchaseSettleBillWithSumPlannedPrice) {
            new PartsPurchaseSettleBillAch(this).发票已审核反结算(virtualPartsPurchaseSettleBillWithSumPlannedPrice);
        }
    }
}