﻿using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class AgencyDealerRelationAch : DcsSerivceAchieveBase {
        public AgencyDealerRelationAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 作废代理库与经销商隶属关系(AgencyDealerRelation agencyDealerRelation) {
            var dbAgencyDealerRelation = ObjectContext.AgencyDealerRelations.Where(r => r.Id == agencyDealerRelation.Id && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(agencyDealerRelation == null)
            throw new ValidationException(ErrorStrings.AgencyDealerRelation_Validation3);
            UpdateToDatabase(agencyDealerRelation);
            agencyDealerRelation.Status = (int)DcsBaseDataStatus.作废;
            this.UpdateAgencyDealerRelationValidate(agencyDealerRelation);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:11:57
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 作废代理库与经销商隶属关系(AgencyDealerRelation agencyDealerRelation) {
            new AgencyDealerRelationAch(this).作废代理库与经销商隶属关系(agencyDealerRelation);
        }
    }
}
