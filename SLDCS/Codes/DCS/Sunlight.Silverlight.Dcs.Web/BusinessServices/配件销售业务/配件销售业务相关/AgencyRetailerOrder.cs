﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.Net;
using System.ServiceModel.DomainServices.Server;
using System.Text;
using System.Transactions;
using System.Web.Configuration;
using FTDCSWebService;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class AgencyRetailerOrderAch : DcsSerivceAchieveBase {
        public void 终止代理库电商订单(AgencyRetailerOrder agencyRetailerOrder) {
            var userInfo = Utils.GetCurrentUserInfo();
            var dbAgencyRetailerOrder = this.ObjectContext.AgencyRetailerOrders.Where(r => r.Id == agencyRetailerOrder.Id && (r.Status == (int)DcsAgencyRetailerOrderStatus.新建 || r.Status == (int)DcsAgencyRetailerOrderStatus.部分确认)).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbAgencyRetailerOrder == null) {
                throw new ValidationException(ErrorStrings.AgencyRetailerOrder_Validation1);
            }

            var partsSalesOrder = this.ObjectContext.PartsSalesOrders.Where(r => r.Id == agencyRetailerOrder.PartsSalesOrderId).FirstOrDefault();
            if(partsSalesOrder == null)
                throw new ValidationException(ErrorStrings.AgencyRetailerOrder_Validation2);

            partsSalesOrder.Status = (int)DcsPartsSalesOrderStatus.终止;

            partsSalesOrder.ModifierId = userInfo.Id;
            partsSalesOrder.ModifierName = userInfo.Name;
            partsSalesOrder.ModifyTime = DateTime.Now;
            UpdateToDatabase(partsSalesOrder);

            agencyRetailerOrder.Status = (int)DcsAgencyRetailerOrderStatus.终止;
            agencyRetailerOrder.ShippingStatus = (int)DcsShippingStatus.发运完毕;
            agencyRetailerOrder.CloserId = userInfo.Id;
            agencyRetailerOrder.CloserName = userInfo.Name;
            agencyRetailerOrder.CloserTime = DateTime.Now;
            UpdateToDatabase(agencyRetailerOrder);
            PMS_SYC_150_SendPMSUpdateOrderStatus_PS(agencyRetailerOrder, partsSalesOrder);
        }

        public void 确认代理库电商订单(AgencyRetailerOrder agencyRetailerOrder, List<AgencyRetailerList> agencyRetailerLists, ObservableCollection<PartsOutboundBillDetail> partsOutboundBillDetails) {
            using(var transaction = new TransactionScope()) {
                var userInfo = Utils.GetCurrentUserInfo();
                var dbAgencyRetailerOrder = ObjectContext.AgencyRetailerOrders.Where(r => r.Id == agencyRetailerOrder.Id && (r.Status == (int)DcsAgencyRetailerOrderStatus.新建 || r.Status == (int)DcsAgencyRetailerOrderStatus.部分确认)).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
                if(dbAgencyRetailerOrder == null) {
                    throw new ValidationException("只能确认新建和部分确认状态的单据");
                }
                //查询仓库信息
                var warehouse = this.ObjectContext.Warehouses.Where(r => r.Id == agencyRetailerOrder.WarehouseId).FirstOrDefault();
                if(warehouse == null)
                    throw new ValidationException("未找到 电商订单上的仓库信息,请确认");

                var company = this.ObjectContext.Companies.Where(r => r.Id == warehouse.StorageCompanyId).FirstOrDefault();
                if(company == null)
                    throw new ValidationException("电商订单仓库信息对应的企业信息不存在,请确认");

                var warehouseJPCDC = this.ObjectContext.Warehouses.Include("Branch").Where(r => r.Name == "精品北京CDC仓库" && r.Status == (int)DcsBaseDataStatus.有效).FirstOrDefault();
                if(warehouseJPCDC == null)
                    throw new ValidationException("未找到 精品北京CDC仓库,请确认");

                var warehouseSCXCDC = this.ObjectContext.Warehouses.Include("Branch").Where(r => r.Name == "随车行北京CDC仓库" && r.Status == (int)DcsBaseDataStatus.有效).FirstOrDefault();
                if(warehouseSCXCDC == null)
                    throw new ValidationException("未找到 随车行北京CDC仓库,请确认");

                var partsSalesOrderType = this.ObjectContext.PartsSalesOrderTypes.Where(r => r.BranchId == agencyRetailerOrder.BranchId && r.PartsSalesCategoryId == agencyRetailerOrder.PartsSalesCategoryId && r.Name == "急需订单").FirstOrDefault();

                var partsSalesOrder = this.ObjectContext.PartsSalesOrders.Where(r => r.Id == agencyRetailerOrder.PartsSalesOrderId).FirstOrDefault();
                if(partsSalesOrder == null)
                    throw new ValidationException("未找到 对应的销售订单,请确认");

                var partsSalesOrderDetails = this.ObjectContext.PartsSalesOrderDetails.Where(r => r.PartsSalesOrderId == partsSalesOrder.Id).ToArray();
                ////查询销售订单清单 虚拟配件信息  
                //var partsSalesDetail = partsSalesOrderDetails.Where(r => r.SparePartCode == "XNPJYF").FirstOrDefault();
                //if(partsSalesDetail == null)
                //    throw new ValidationException("未找到 销售订单上的虚拟配件信息,请确认");

                ////查询虚拟件库存
                //var partsStockXNPJ = this.ObjectContext.PartsStocks.Include("WarehouseArea").Where(r => r.PartId == partsSalesDetail.SparePartId && r.BranchId == partsSalesOrder.BranchId && r.WarehouseId == warehouseSCXCDC.Id).FirstOrDefault();
                //if(partsStockXNPJ == null)
                //    throw new ValidationException("未找到 虚拟配件的库存信息,请确认");

                var branch = this.ObjectContext.Branches.Where(r => r.Id == agencyRetailerOrder.BranchId).FirstOrDefault();
                if(branch == null)
                    throw new ValidationException("未找到 对应的营销分公司,请确认");

                var partsSalesCategory = this.ObjectContext.PartsSalesCategories.Where(r => r.Name == "红岩配件" && r.Status == (int)DcsBaseDataStatus.有效).FirstOrDefault();
                if(partsSalesCategory == null)
                    throw new ValidationException("未找到 红岩配件品牌,请确认");


                var salesUnitAffiWarehouse = this.ObjectContext.SalesUnitAffiWarehouses.Include("SalesUnit").Where(r => r.WarehouseId == agencyRetailerOrder.WarehouseId && r.SalesUnit.PartsSalesCategoryId == partsSalesCategory.Id).FirstOrDefault();
                if(salesUnitAffiWarehouse == null)
                    throw new ValidationException("未找到 对应的销售组织仓库关系,请确认");

                var company1 = this.ObjectContext.Companies.Where(r => r.Id == salesUnitAffiWarehouse.SalesUnit.OwnerCompanyId).FirstOrDefault();
                if(company1 == null)
                    throw new ValidationException("未找到 对应的销售组织隶属企业信息,请确认");

                var sparePartIds = agencyRetailerLists.Select(r => r.SparePartId).ToArray();
                //销售价格
                var partsSalesPrices = this.ObjectContext.PartsSalesPrices.Where(r => ObjectContext.SalesUnits.Any(e => ObjectContext.SalesUnitAffiWarehouses.Any(ex => ex.WarehouseId == warehouseJPCDC.Id && ex.SalesUnitId == e.Id)) && sparePartIds.Contains(r.SparePartId)).ToArray();
                //计划价
                var partsPlannedPrices = this.ObjectContext.PartsPlannedPrices.Where(r => sparePartIds.Contains(r.SparePartId)).ToArray();


                #region 更新配件销售订单审批数量和代理库电商订单确认量
                //计算代理库电商订单确认量
                //更新清单确认量
                var dbAgencyRetailerLists = this.ObjectContext.AgencyRetailerLists.Where(r => r.AgencyRetailerOrderId == agencyRetailerOrder.Id);
                foreach(var agencyRetailerList in agencyRetailerLists.Where(r => r.CurrentOutQuantity > 0)) {
                    agencyRetailerList.ConfirmedAmount = (agencyRetailerList.ConfirmedAmount ?? 0) + agencyRetailerList.CurrentOutQuantity;
                    var dbAgencyRetailerList = dbAgencyRetailerLists.FirstOrDefault(r => r.Id == agencyRetailerList.Id);
                    if(dbAgencyRetailerList != null) {
                        dbAgencyRetailerList.ConfirmedAmount = agencyRetailerList.ConfirmedAmount;
                        UpdateToDatabase(dbAgencyRetailerList);
                    }
                }


                if(partsSalesOrderDetails.Any()) {
                    foreach(var detail in partsSalesOrderDetails) {
                        var agencyRetailerList = agencyRetailerLists.Where(r => r.SparePartId == detail.SparePartId && r.CurrentOutQuantity > 0).FirstOrDefault();
                        if(agencyRetailerList != null) {
                            detail.ApproveQuantity = agencyRetailerList.ConfirmedAmount;
                            UpdateToDatabase(detail);
                        }
                    }
                }
                //判断销售订单清单中 配件编号 = XNPJYF 的清单 审批数量是否 为0，若为0，则更新审批数量=订货数量
                //同时扣减分公司 中该配件的库存：仓库 = 随车行北京CDC，库位 根据 配件编号=XNPJYF 获取对应的 配件
                //Id，然后根据配件Id，销售订单.营销分公司Id，随车行北京CDC仓库的Id获取对应的 库位，扣减数量 = 销售订
                //单清单中该配件的 审批数量
                //if(partsSalesDetail.ApproveQuantity == null || (partsSalesDetail.ApproveQuantity.HasValue && partsSalesDetail.ApproveQuantity.Value == 0)) {
                //    partsSalesDetail.ApproveQuantity = partsSalesDetail.OrderedQuantity;
                //    UpdateToDatabase(partsSalesDetail);

                //    if(partsSalesDetail.ApproveQuantity > partsStockXNPJ.Quantity)
                //        throw new ValidationException("虚拟件库存不足，请确认");

                //    partsStockXNPJ.Quantity -= partsSalesDetail.ApproveQuantity ?? 0;
                //    UpdateToDatabase(partsStockXNPJ);
                //}

                this.ObjectContext.SaveChanges();

                if(agencyRetailerLists.Any(r => (r.ConfirmedAmount ?? 0) < r.OrderedQuantity)) {
                    agencyRetailerOrder.Status = (int)DcsAgencyRetailerOrderStatus.部分确认;
                    agencyRetailerOrder.ShippingStatus = (int)DcsShippingStatus.部分发运;
                    partsSalesOrder.Status = (int)DcsPartsSalesOrderStatus.部分审批;
                } else {
                    agencyRetailerOrder.Status = (int)DcsAgencyRetailerOrderStatus.确认完毕;
                    agencyRetailerOrder.ShippingStatus = (int)DcsShippingStatus.发运完毕;
                    partsSalesOrder.Status = (int)DcsPartsSalesOrderStatus.审批完成;
                }
                #endregion

                #region 扣减代理库库存 增加服务站库存
                //根据出库清单扣减代理库库存
                var partIds = partsOutboundBillDetails.Select(r => r.SparePartId).Distinct().ToArray();
                var partsStocks = this.ObjectContext.PartsStocks.Include("WarehouseAreaCategory").Where(r => r.WarehouseId == agencyRetailerOrder.WarehouseId && partIds.Contains(r.PartId) && r.WarehouseAreaCategory.Category == (int)DcsAreaType.保管区);

                foreach(var partsOutboundBillDetail in partsOutboundBillDetails.Where(r => r.OutboundAmount > 0)) {
                    //减少配件库存
                    var partsStock = partsStocks.FirstOrDefault(r => r.WarehouseAreaId == partsOutboundBillDetail.WarehouseAreaId && r.PartId == partsOutboundBillDetail.SparePartId);

                    if(partsStock == null)
                        throw new ValidationException(string.Format(ErrorStrings.PartsOutboundBill_Validation6, partsOutboundBillDetail.SparePartCode, partsOutboundBillDetail.WarehouseAreaCode));
                    if(partsOutboundBillDetail.OutboundAmount > partsStock.Quantity)
                        throw new ValidationException(string.Format("编号为{0}的配件,在编号为{1}的库位上库存不足", partsOutboundBillDetail.SparePartCode, partsOutboundBillDetail.WarehouseAreaCode));

                    partsStock.Quantity -= partsOutboundBillDetail.OutboundAmount;
                    partsStock.ModifyTime = DateTime.Now;
                    partsStock.ModifierId = userInfo.Id;
                    partsStock.ModifierName = userInfo.Name;
                    UpdateToDatabase(partsStock);
                }
                //根据代理库电商订单增加服务站库存（根据配件销售订单提报单位ID查询经销商库存）
                var dealerPartsStocks = this.ObjectContext.DealerPartsStocks.Where(r => r.DealerId == partsSalesOrder.SubmitCompanyId && r.BranchId == partsSalesOrder.BranchId && r.SalesCategoryId == partsSalesOrder.SalesCategoryId).ToArray();
                foreach(var agencyRetailerList in agencyRetailerLists.Where(r => r.CurrentOutQuantity > 0)) {
                    var dealerPartsStock = dealerPartsStocks.SingleOrDefault(r => r.SparePartId == agencyRetailerList.SparePartId);
                    if(dealerPartsStock != null) {
                        dealerPartsStock.Quantity += agencyRetailerList.CurrentOutQuantity;
                        dealerPartsStock.ModifierId = userInfo.Id;
                        dealerPartsStock.ModifierName = userInfo.Name;
                        dealerPartsStock.ModifyTime = DateTime.Now;
                        UpdateToDatabase(dealerPartsStock);
                    } else {
                        dealerPartsStock = new DealerPartsStock();
                        dealerPartsStock.DealerId = partsSalesOrder.SubmitCompanyId;
                        dealerPartsStock.DealerCode = partsSalesOrder.SubmitCompanyCode;
                        dealerPartsStock.DealerName = partsSalesOrder.SubmitCompanyName;
                        dealerPartsStock.BranchId = partsSalesOrder.BranchId;
                        dealerPartsStock.SubDealerId = -1;
                        dealerPartsStock.SalesCategoryId = partsSalesOrder.SalesCategoryId;
                        dealerPartsStock.SalesCategoryName = partsSalesOrder.SalesCategoryName;
                        dealerPartsStock.SparePartId = agencyRetailerList.SparePartId ?? 0;
                        dealerPartsStock.SparePartCode = agencyRetailerList.SparePartCode;
                        dealerPartsStock.Quantity = agencyRetailerList.CurrentOutQuantity;
                        dealerPartsStock.CreatorId = userInfo.Id;
                        dealerPartsStock.CreatorName = userInfo.Name;
                        dealerPartsStock.CreateTime = DateTime.Now;

                        InsertToDatabase(dealerPartsStock);
                    }
                }
                #endregion

                #region 生成出库计划单-代理库及清单
                var agencyPartsOutboundPlan = new AgencyPartsOutboundPlan();
                if(company != null) {
                    agencyPartsOutboundPlan.StorageCompanyId = company.Id;
                    agencyPartsOutboundPlan.StorageCompanyCode = company.Code;
                    agencyPartsOutboundPlan.StorageCompanyName = company.Name;
                    agencyPartsOutboundPlan.StorageCompanyType = company.Type;
                }
                agencyPartsOutboundPlan.Code = CodeGenerator.Generate("AgencyPartsOutboundPlan", agencyPartsOutboundPlan.StorageCompanyCode);
                agencyPartsOutboundPlan.WarehouseId = agencyRetailerOrder.WarehouseId.HasValue ? agencyRetailerOrder.WarehouseId.Value : 0;
                agencyPartsOutboundPlan.WarehouseCode = agencyRetailerOrder.WarehouseCode;
                agencyPartsOutboundPlan.WarehouseName = agencyRetailerOrder.WarehouseName;
                agencyPartsOutboundPlan.PartsSalesCategoryId = partsSalesCategory.Id;
                agencyPartsOutboundPlan.ERPSourceOrderCode = agencyRetailerOrder.ERPSourceOrderCode;
                if(partsSalesOrderType != null) {
                    agencyPartsOutboundPlan.PartsSalesOrderTypeName = partsSalesOrderType.Name;
                    agencyPartsOutboundPlan.PartsSalesOrderTypeId = partsSalesOrderType.Id;
                }
                agencyPartsOutboundPlan.CounterpartCompanyId = agencyRetailerOrder.CounterpartCompanyId ?? 0;
                agencyPartsOutboundPlan.CounterpartCompanyCode = agencyRetailerOrder.CounterpartCompanyCode;
                agencyPartsOutboundPlan.CounterpartCompanyName = agencyRetailerOrder.CounterpartCompanyName;
                agencyPartsOutboundPlan.ReceivingCompanyId = agencyRetailerOrder.CounterpartCompanyId ?? 0;
                agencyPartsOutboundPlan.ReceivingCompanyCode = agencyRetailerOrder.CounterpartCompanyCode;
                agencyPartsOutboundPlan.ReceivingCompanyName = agencyRetailerOrder.CounterpartCompanyName;
                agencyPartsOutboundPlan.SourceId = agencyRetailerOrder.Id;
                agencyPartsOutboundPlan.SourceCode = agencyRetailerOrder.Code;
                agencyPartsOutboundPlan.OutboundType = (int)DcsPartsOutboundType.配件销售;
                agencyPartsOutboundPlan.ShippingMethod = (int)DcsPartsShippingMethod.公路物流;
                agencyPartsOutboundPlan.OriginalRequirementBillId = partsSalesOrder.Id;
                agencyPartsOutboundPlan.OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.配件销售订单;
                agencyPartsOutboundPlan.OriginalRequirementBillCode = partsSalesOrder.Code;
                agencyPartsOutboundPlan.Status = (int)DcsPartsOutboundPlanStatus.出库完成;
                agencyPartsOutboundPlan.BranchId = branch.Id;
                agencyPartsOutboundPlan.BranchCode = branch.Code;
                agencyPartsOutboundPlan.BranchName = branch.Name;
                agencyPartsOutboundPlan.CreatorId = userInfo.Id;
                agencyPartsOutboundPlan.CreatorName = userInfo.Name;
                agencyPartsOutboundPlan.CreateTime = DateTime.Now;

                InsertToDatabase(agencyPartsOutboundPlan);

                this.ObjectContext.SaveChanges();

                #endregion

                #region 生成出库单-代理库及清单
                var agencyPartsOutboundBill = new AgencyPartsOutboundBill();

                if(company != null) {
                    agencyPartsOutboundBill.StorageCompanyId = company.Id;
                    agencyPartsOutboundBill.StorageCompanyCode = company.Code;
                    agencyPartsOutboundBill.StorageCompanyName = company.Name;
                    agencyPartsOutboundBill.StorageCompanyType = company.Type;
                }
                agencyPartsOutboundBill.Code = CodeGenerator.Generate("AgencyPartsOutboundBill", agencyPartsOutboundBill.StorageCompanyCode);
                agencyPartsOutboundBill.PartsOutboundPlanId = agencyPartsOutboundPlan.Id;
                agencyPartsOutboundBill.WarehouseId = agencyRetailerOrder.WarehouseId.HasValue ? agencyRetailerOrder.WarehouseId.Value : 0;
                agencyPartsOutboundBill.WarehouseCode = agencyRetailerOrder.WarehouseCode;
                agencyPartsOutboundBill.WarehouseName = agencyRetailerOrder.WarehouseName;
                agencyPartsOutboundBill.ERPSourceOrderCode = agencyRetailerOrder.ERPSourceOrderCode;
                agencyPartsOutboundBill.PartsSalesCategoryId = partsSalesCategory.Id;
                if(partsSalesOrderType != null) {
                    agencyPartsOutboundBill.PartsSalesOrderTypeName = partsSalesOrderType.Name;
                    agencyPartsOutboundBill.PartsSalesOrderTypeId = partsSalesOrderType.Id;
                }
                agencyPartsOutboundBill.CounterpartCompanyId = agencyRetailerOrder.CounterpartCompanyId ?? 0;
                agencyPartsOutboundBill.CounterpartCompanyCode = agencyRetailerOrder.CounterpartCompanyCode;
                agencyPartsOutboundBill.CounterpartCompanyName = agencyRetailerOrder.CounterpartCompanyName;
                agencyPartsOutboundBill.ReceivingCompanyId = agencyRetailerOrder.CounterpartCompanyId ?? 0;
                agencyPartsOutboundBill.ReceivingCompanyCode = agencyRetailerOrder.CounterpartCompanyCode;
                agencyPartsOutboundBill.ReceivingCompanyName = agencyRetailerOrder.CounterpartCompanyName;
                agencyPartsOutboundBill.OutboundType = (int)DcsPartsOutboundType.配件销售;
                agencyPartsOutboundBill.ShippingMethod = (int)DcsPartsShippingMethod.公路物流;
                agencyPartsOutboundBill.OriginalRequirementBillId = partsSalesOrder.Id;
                agencyPartsOutboundBill.OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.配件销售订单;
                agencyPartsOutboundBill.OriginalRequirementBillCode = partsSalesOrder.Code;
                agencyPartsOutboundBill.SettlementStatus = (int)DcsPartsSettlementStatus.待结算;
                agencyPartsOutboundBill.BranchId = branch.Id;
                agencyPartsOutboundBill.BranchCode = branch.Code;
                agencyPartsOutboundBill.BranchName = branch.Name;
                agencyPartsOutboundBill.CreatorId = userInfo.Id;
                agencyPartsOutboundBill.CreatorName = userInfo.Name;
                agencyPartsOutboundBill.CreateTime = DateTime.Now;

                InsertToDatabase(agencyPartsOutboundBill);
                this.ObjectContext.SaveChanges();

                #endregion

                #region 生成发运单-代理库
                var agencyPartsShippingOrder = new AgencyPartsShippingOrder();

                agencyPartsShippingOrder.Type = (int)DcsPartsShippingOrderType.销售;
                agencyPartsShippingOrder.BranchId = userInfo.EnterpriseId;
                agencyPartsShippingOrder.PartsSalesCategoryId = agencyRetailerOrder.PartsSalesCategoryId;
                if(company != null) {
                    agencyPartsShippingOrder.ShippingCompanyId = company.Id;
                    agencyPartsShippingOrder.ShippingCompanyCode = company.Code;
                    agencyPartsShippingOrder.ShippingCompanyName = company.Name;

                    agencyPartsShippingOrder.SettlementCompanyId = company.Id;
                    ;
                    agencyPartsShippingOrder.SettlementCompanyCode = company.Code;
                    ;
                    agencyPartsShippingOrder.SettlementCompanyName = company.Name;
                    ;
                }
                agencyPartsShippingOrder.ReceivingCompanyId = agencyRetailerOrder.CounterpartCompanyId ?? 0;
                ;
                agencyPartsShippingOrder.ReceivingCompanyCode = agencyRetailerOrder.CounterpartCompanyCode;
                agencyPartsShippingOrder.ReceivingCompanyName = agencyRetailerOrder.CounterpartCompanyName;
                agencyPartsShippingOrder.ReceivingAddress = partsSalesOrder.ReceivingAddress;
                //agencyPartsShippingOrder.InvoiceReceiveSaleCateId=;
                //agencyPartsShippingOrder.InvoiceReceiveSaleCateName=;
                agencyPartsShippingOrder.WarehouseId = agencyRetailerOrder.WarehouseId;
                agencyPartsShippingOrder.WarehouseCode = agencyRetailerOrder.WarehouseCode;
                agencyPartsShippingOrder.WarehouseName = agencyRetailerOrder.WarehouseName;
                //agencyPartsShippingOrder.ReceivingWarehouseId=;
                //agencyPartsShippingOrder.ReceivingWarehouseCode=;
                //agencyPartsShippingOrder.ReceivingWarehouseName=;
                //agencyPartsShippingOrder.ReceivingAddress=;
                agencyPartsShippingOrder.LogisticCompanyId = agencyRetailerOrder.LogisticCompanyId;
                agencyPartsShippingOrder.LogisticCompanyCode = agencyRetailerOrder.LogisticCompanyCode;
                agencyPartsShippingOrder.LogisticCompanyName = agencyRetailerOrder.LogisticCompanyName;
                agencyPartsShippingOrder.OriginalRequirementBillId = partsSalesOrder.Id;
                agencyPartsShippingOrder.OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.配件销售订单;
                agencyPartsShippingOrder.OriginalRequirementBillCode = partsSalesOrder.Code;

                //agencyPartsShippingOrder.IsTransportLosses=;
                //agencyPartsShippingOrder.TransportLossesDisposeStatus=;
                //agencyPartsShippingOrder.ShippingMethod=;
                //agencyPartsShippingOrder.LogisticArrivalDate=;
                //agencyPartsShippingOrder.RequestedArrivalDate=;
                //agencyPartsShippingOrder.ShippingDate=;
                //agencyPartsShippingOrder.Weight=;
                //agencyPartsShippingOrder.Volume=;
                //agencyPartsShippingOrder.BillingMethod=;
                //agencyPartsShippingOrder.TransportCostRate=;
                //agencyPartsShippingOrder.TransportCost=;
                //agencyPartsShippingOrder.TransportCostSettleStatus=;
                //agencyPartsShippingOrder.InsuranceRate=;
                //agencyPartsShippingOrder.InsuranceFee=;
                //agencyPartsShippingOrder.TransportMileage=;
                //agencyPartsShippingOrder.TransportVehiclePlate=;
                //agencyPartsShippingOrder.TransportDriver=;
                //agencyPartsShippingOrder.TransportDriverPhone=;
                agencyPartsShippingOrder.DeliveryBillNumber = agencyRetailerOrder.DeliveryBillNumber;
                //agencyPartsShippingOrder.InterfaceRecordId=;
                agencyPartsShippingOrder.Status = (int)DcsAgencyShippingOrderStatus.新建;
                //agencyPartsShippingOrder.Remark=;
                //agencyPartsShippingOrder.OrderApproveComment=;
                agencyPartsShippingOrder.Code = CodeGenerator.Generate("AgencyPartsShippingOrder", agencyPartsShippingOrder.ShippingCompanyCode);
                agencyPartsShippingOrder.CreatorId = userInfo.Id;
                ;
                agencyPartsShippingOrder.CreatorName = userInfo.Name;
                agencyPartsShippingOrder.CreateTime = DateTime.Now;
                agencyPartsShippingOrder.BranchId = branch.Id;

                InsertToDatabase(agencyPartsShippingOrder);
                this.ObjectContext.SaveChanges();

                var agencyPartsShippingOrderRef = new AgencyPartsShippingOrderRef();
                agencyPartsShippingOrderRef.PartsShippingOrderId = agencyPartsShippingOrder.Id;
                agencyPartsShippingOrderRef.PartsOutboundBillId = agencyPartsOutboundBill.Id;

                InsertToDatabase(agencyPartsShippingOrderRef);
                this.ObjectContext.SaveChanges();
                #endregion

                #region 生成销售退货单
                var partsSalesReturnBill = new PartsSalesReturnBill();
                partsSalesReturnBill.ReturnCompanyId = agencyRetailerOrder.ShippingCompanyId ?? 0;
                partsSalesReturnBill.ReturnCompanyCode = agencyRetailerOrder.ShippingCompanyCode;
                partsSalesReturnBill.ReturnCompanyName = agencyRetailerOrder.ShippingCompanyName;
                partsSalesReturnBill.InvoiceReceiveCompanyId = agencyRetailerOrder.BranchId;
                partsSalesReturnBill.InvoiceReceiveCompanyCode = branch.Code;
                partsSalesReturnBill.InvoiceReceiveCompanyName = agencyRetailerOrder.BranchName;
                partsSalesReturnBill.SubmitCompanyId = partsSalesReturnBill.ReturnCompanyId;
                partsSalesReturnBill.SubmitCompanyCode = partsSalesReturnBill.ReturnCompanyCode;
                partsSalesReturnBill.SubmitCompanyName = partsSalesReturnBill.ReturnCompanyName;
                //partsSalesReturnBill.TotalAmount = agencyRetailerLists.Sum(r => r.CurrentOutQuantity * r.UnitPrice ?? 0);

                partsSalesReturnBill.SalesUnitOwnerCompanyId = company1.Id;
                partsSalesReturnBill.SalesUnitOwnerCompanyCode = company1.Code;
                partsSalesReturnBill.SalesUnitOwnerCompanyName = company1.Name;


                var salesUnitAffiWarehouse1 = this.ObjectContext.SalesUnitAffiWarehouses.Include("SalesUnit").Where(r => r.WarehouseId == warehouseJPCDC.Id && r.SalesUnit.PartsSalesCategoryId == partsSalesCategory.Id).FirstOrDefault();
                if(salesUnitAffiWarehouse1 == null)
                    throw new ValidationException("未找到 精品北京CDC仓库的销售组织仓库关系,请确认");

                partsSalesReturnBill.SalesUnitId = salesUnitAffiWarehouse1.SalesUnitId;
                partsSalesReturnBill.SalesUnitName = salesUnitAffiWarehouse1.SalesUnit.Name;

                var customerAccount = this.ObjectContext.CustomerAccounts.Where(r => r.AccountGroupId == salesUnitAffiWarehouse1.SalesUnit.AccountGroupId && r.CustomerCompanyId == partsSalesReturnBill.ReturnCompanyId && r.Status == (int)DcsMasterDataStatus.有效).FirstOrDefault();
                if(customerAccount == null)
                    throw new ValidationException("未找到 对应的客户账户, 请确认");
                partsSalesReturnBill.CustomerAccountId = customerAccount.Id;

                partsSalesReturnBill.WarehouseId = warehouseJPCDC.Id;
                partsSalesReturnBill.WarehouseCode = warehouseJPCDC.Code;
                partsSalesReturnBill.WarehouseName = warehouseJPCDC.Name;
                partsSalesReturnBill.Status = (int)DcsPartsSalesReturnBillStatus.审核通过;
                partsSalesReturnBill.ReturnType = (int)DcsPartsSalesReturnBillReturnType.特殊退货;
                partsSalesReturnBill.ShippingMethod = (int)DcsPartsShippingMethod.公路物流;
                partsSalesReturnBill.IsBarter = false;

                partsSalesReturnBill.Code = CodeGenerator.Generate("PartsSalesReturnBill", partsSalesReturnBill.SalesUnitOwnerCompanyCode);
                partsSalesReturnBill.BranchId = branch.Id;
                partsSalesReturnBill.BranchCode = branch.Code;
                partsSalesReturnBill.BranchName = branch.Name;
                partsSalesReturnBill.ApproverId = userInfo.Id;
                partsSalesReturnBill.ApproverName = userInfo.Name;
                partsSalesReturnBill.ApproveTime = DateTime.Now;
                partsSalesReturnBill.CreatorId = userInfo.Id;
                ;
                partsSalesReturnBill.CreatorName = userInfo.Name;
                partsSalesReturnBill.CreateTime = DateTime.Now;

                InsertToDatabase(partsSalesReturnBill);
                this.ObjectContext.SaveChanges();


                #endregion

                #region 生成配件入库计划单（退货）
                var partsInboundPlan = new PartsInboundPlan();
                partsInboundPlan.WarehouseId = warehouseJPCDC.Id;
                partsInboundPlan.WarehouseCode = warehouseJPCDC.Code;
                partsInboundPlan.WarehouseName = warehouseJPCDC.Name;
                partsInboundPlan.StorageCompanyId = warehouseJPCDC.StorageCompanyId;
                var comapny = this.ObjectContext.Companies.Where(r => r.Id == warehouseJPCDC.StorageCompanyId).FirstOrDefault();
                if(comapny != null) {
                    partsInboundPlan.StorageCompanyCode = comapny.Code;
                    partsInboundPlan.StorageCompanyName = comapny.Name;
                    partsInboundPlan.StorageCompanyType = comapny.Type;
                }
                partsInboundPlan.PartsSalesCategoryId = partsSalesCategory.Id;
                partsInboundPlan.PartsSalesCategorieName = partsSalesCategory.Name;
                partsInboundPlan.CounterpartCompanyId = partsSalesReturnBill.ReturnCompanyId;
                partsInboundPlan.CounterpartCompanyCode = partsSalesReturnBill.ReturnCompanyCode;
                partsInboundPlan.CounterpartCompanyName = partsSalesReturnBill.ReturnCompanyName;
                partsInboundPlan.OriginalRequirementBillId = partsSalesReturnBill.Id;
                partsInboundPlan.OriginalRequirementBillCode = partsSalesReturnBill.Code;
                partsInboundPlan.OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.配件销售订单;
                partsInboundPlan.SourceId = partsSalesReturnBill.Id;
                partsInboundPlan.SourceCode = partsSalesReturnBill.Code;
                partsInboundPlan.Status = (int)DcsPartsInboundPlanStatus.检验完成;
                partsInboundPlan.InboundType = (int)DcsPartsInboundType.代发退货;
                partsInboundPlan.CustomerAccountId = partsSalesReturnBill.CustomerAccountId;
                partsInboundPlan.IfWmsInterface = false;

                partsInboundPlan.Code = CodeGenerator.Generate("PartsInboundPlan", partsInboundPlan.StorageCompanyCode);
                partsInboundPlan.BranchId = branch.Id;
                partsInboundPlan.BranchCode = branch.Code;
                partsInboundPlan.BranchName = branch.Name;

                partsInboundPlan.CreatorId = userInfo.Id;
                ;
                partsInboundPlan.CreatorName = userInfo.Name;
                partsInboundPlan.CreateTime = DateTime.Now;

                InsertToDatabase(partsInboundPlan);
                this.ObjectContext.SaveChanges();

                #endregion

                #region 更新客户账户 发出商品金额
                //根据销售订单查询客户账户
                var customerAccount1 = this.ObjectContext.CustomerAccounts.Where(r => r.Id == partsSalesOrder.CustomerAccountId).FirstOrDefault();
                if(customerAccount1 == null)
                    throw new ValidationException("未找到 销售订单对应的客户账户信息, 请确认");
                customerAccount1.ShippedProductValue = customerAccount1.ShippedProductValue + agencyRetailerLists.Sum(r => r.CurrentOutQuantity * r.UnitPrice ?? 0);
                ;
                UpdateToDatabase(customerAccount1);
                #endregion

                #region 生成配件入库单(退货)
                var partsInboundCheckBill = new PartsInboundCheckBill();
                partsInboundCheckBill.PartsInboundPlanId = partsInboundPlan.Id;
                partsInboundCheckBill.PartsSalesCategoryId = partsSalesCategory.Id;
                partsInboundCheckBill.WarehouseId = partsInboundPlan.WarehouseId;
                partsInboundCheckBill.WarehouseCode = partsInboundPlan.WarehouseCode;
                partsInboundCheckBill.WarehouseName = partsInboundPlan.WarehouseName;
                partsInboundCheckBill.StorageCompanyId = partsInboundPlan.StorageCompanyId;
                partsInboundCheckBill.StorageCompanyCode = partsInboundPlan.StorageCompanyCode;
                partsInboundCheckBill.StorageCompanyName = partsInboundPlan.StorageCompanyName;
                partsInboundCheckBill.StorageCompanyType = partsInboundPlan.StorageCompanyType;
                partsInboundCheckBill.CounterpartCompanyId = partsInboundPlan.CounterpartCompanyId;
                partsInboundCheckBill.CounterpartCompanyCode = partsInboundPlan.CounterpartCompanyCode;
                partsInboundCheckBill.CounterpartCompanyName = partsInboundPlan.CounterpartCompanyName;
                partsInboundCheckBill.CustomerAccountId = partsSalesReturnBill.CustomerAccountId;
                partsInboundCheckBill.InboundType = partsInboundPlan.InboundType;
                partsInboundCheckBill.OriginalRequirementBillId = partsInboundPlan.OriginalRequirementBillId;
                partsInboundCheckBill.OriginalRequirementBillCode = partsInboundPlan.OriginalRequirementBillCode;
                partsInboundCheckBill.OriginalRequirementBillType = partsInboundPlan.OriginalRequirementBillType;

                partsInboundCheckBill.Status = (int)DcsPartsInboundCheckBillStatus.上架完成;
                partsInboundCheckBill.SettlementStatus = (int)DcsPartsSettlementStatus.待结算;
                partsInboundCheckBill.Code = CodeGenerator.Generate("PartsInboundCheckBill", partsInboundCheckBill.StorageCompanyCode);

                partsInboundCheckBill.BranchId = branch.Id;
                partsInboundCheckBill.BranchCode = branch.Code;
                partsInboundCheckBill.BranchName = branch.Name;

                partsInboundCheckBill.CreatorId = userInfo.Id;
                ;
                partsInboundCheckBill.CreatorName = userInfo.Name;
                partsInboundCheckBill.CreateTime = DateTime.Now;

                InsertToDatabase(partsInboundCheckBill);
                this.ObjectContext.SaveChanges();

                #endregion

                #region 生成调拨单
                var partsTransferOrder = new PartsTransferOrder();
                partsTransferOrder.OriginalWarehouseId = warehouseJPCDC.Id;
                partsTransferOrder.OriginalWarehouseCode = warehouseJPCDC.Code;
                partsTransferOrder.OriginalWarehouseName = warehouseJPCDC.Name;
                partsTransferOrder.StorageCompanyId = warehouseJPCDC.StorageCompanyId;
                partsTransferOrder.StorageCompanyType = warehouseJPCDC.StorageCompanyType;
                partsTransferOrder.DestWarehouseId = warehouseSCXCDC.Id;
                partsTransferOrder.DestWarehouseCode = warehouseSCXCDC.Code;
                partsTransferOrder.DestWarehouseName = warehouseSCXCDC.Name;
                partsTransferOrder.Status = (int)DcsPartsTransferOrderStatus.已审批;
                partsTransferOrder.Type = (int)DcsPartsTransferOrderType.急需调拨;
                partsTransferOrder.OriginalBillId = agencyRetailerOrder.Id;
                partsTransferOrder.OriginalBillCode = agencyRetailerOrder.Code;
                partsTransferOrder.ShippingMethod = (int)DcsPartsShippingMethod.公路物流;


                var dbBranch = ObjectContext.Branches.SingleOrDefault(r => ObjectContext.Warehouses.Any(v => v.Id == partsTransferOrder.OriginalWarehouseId && v.BranchId == r.Id && v.Status != (int)DcsBaseDataStatus.作废));
                if(dbBranch == null) {
                    throw new ValidationException(ErrorStrings.Branch_Validation4);
                }
                partsTransferOrder.Code = CodeGenerator.Generate("PartsTransferOrder", dbBranch.Code);

                partsTransferOrder.CreatorId = userInfo.Id;
                partsTransferOrder.CreatorName = userInfo.Name;
                partsTransferOrder.CreateTime = DateTime.Now;

                InsertToDatabase(partsTransferOrder);
                this.ObjectContext.SaveChanges();
                #endregion

                #region 生成出库计划单和出库单（调拨）
                //配件出库计划
                var partsOutboundPlan = new PartsOutboundPlan();
                partsOutboundPlan.WarehouseId = partsTransferOrder.OriginalWarehouseId;
                partsOutboundPlan.WarehouseCode = partsTransferOrder.OriginalWarehouseCode;
                partsOutboundPlan.WarehouseName = partsTransferOrder.OriginalWarehouseName;
                partsOutboundPlan.StorageCompanyId = partsTransferOrder.StorageCompanyId;
                partsOutboundPlan.StorageCompanyType = partsTransferOrder.StorageCompanyType;
                partsOutboundPlan.CounterpartCompanyId = partsTransferOrder.StorageCompanyId;
                partsOutboundPlan.ReceivingCompanyId = partsTransferOrder.StorageCompanyId;

                //获取企业信息
                var company2 = this.ObjectContext.Companies.Where(r => r.Id == partsTransferOrder.StorageCompanyId).FirstOrDefault();
                if(company2 != null) {
                    partsOutboundPlan.StorageCompanyCode = company2.Code;
                    partsOutboundPlan.StorageCompanyName = company2.Name;
                    partsOutboundPlan.CounterpartCompanyCode = company2.Code;
                    partsOutboundPlan.CounterpartCompanyName = company2.Name;
                    partsOutboundPlan.ReceivingCompanyCode = company2.Code;
                    partsOutboundPlan.ReceivingCompanyName = company2.Name;
                }

                partsOutboundPlan.SourceId = partsTransferOrder.Id;
                partsOutboundPlan.SourceCode = partsTransferOrder.Code;
                partsOutboundPlan.OutboundType = (int)DcsPartsOutboundType.配件调拨;
                partsOutboundPlan.OriginalRequirementBillId = partsTransferOrder.Id;
                partsOutboundPlan.OriginalRequirementBillCode = partsTransferOrder.Code;
                partsOutboundPlan.OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.配件调拨单;
                partsOutboundPlan.Status = (int)DcsPartsOutboundPlanStatus.出库完成;
                partsOutboundPlan.ReceivingWarehouseId = partsTransferOrder.DestWarehouseId;
                partsOutboundPlan.ReceivingWarehouseCode = partsTransferOrder.DestWarehouseCode;
                partsOutboundPlan.ReceivingWarehouseName = partsTransferOrder.DestWarehouseName;
                partsOutboundPlan.ShippingMethod = partsTransferOrder.ShippingMethod;
                partsOutboundPlan.IfWmsInterface = false;

                //var salesUnitAffiWarehouse1 = this.ObjectContext.SalesUnitAffiWarehouses.Include("SalesUnit").Where(r => r.WarehouseId == partsTransferOrder.OriginalWarehouseId).FirstOrDefault();
                //if(salesUnitAffiWarehouse1 == null)
                //    throw new ValidationException("未找到 对应的销售组织仓库关系,请确认");
                partsOutboundPlan.PartsSalesCategoryId = partsSalesCategory.Id;
                partsOutboundPlan.PartsSalesOrderTypeId = partsSalesOrderType.Id;
                partsOutboundPlan.PartsSalesOrderTypeName = partsSalesOrderType.Name;
                partsOutboundPlan.Code = CodeGenerator.Generate("PartsOutboundPlan", partsOutboundPlan.StorageCompanyCode);


                partsOutboundPlan.BranchId = warehouseJPCDC.BranchId;
                partsOutboundPlan.BranchCode = warehouseJPCDC.Branch.Code;
                partsOutboundPlan.BranchName = warehouseJPCDC.Branch.Name;
                partsOutboundPlan.ERPSourceOrderCode = agencyRetailerOrder.ERPSourceOrderCode;

                partsOutboundPlan.CreatorId = userInfo.Id;
                partsOutboundPlan.CreatorName = userInfo.Name;
                partsOutboundPlan.CreateTime = DateTime.Now;

                InsertToDatabase(partsOutboundPlan);

                this.ObjectContext.SaveChanges();

                //配件出库单
                var partOutboundBill = new PartsOutboundBill();
                partOutboundBill.PartsOutboundPlanId = partsOutboundPlan.Id;
                partOutboundBill.WarehouseId = partsTransferOrder.OriginalWarehouseId;
                partOutboundBill.WarehouseCode = partsTransferOrder.OriginalWarehouseCode;
                partOutboundBill.WarehouseName = partsTransferOrder.OriginalWarehouseName;
                partOutboundBill.StorageCompanyId = partsTransferOrder.StorageCompanyId;
                partOutboundBill.StorageCompanyType = partsTransferOrder.StorageCompanyType;
                partOutboundBill.CounterpartCompanyId = partsTransferOrder.StorageCompanyId;
                partOutboundBill.ReceivingCompanyId = partsTransferOrder.StorageCompanyId;
                partOutboundBill.ERPSourceOrderCode = agencyRetailerOrder.ERPSourceOrderCode;

                if(company2 != null) {
                    partOutboundBill.StorageCompanyCode = company2.Code;
                    partOutboundBill.StorageCompanyName = company2.Name;
                    partOutboundBill.CounterpartCompanyCode = company2.Code;
                    partOutboundBill.CounterpartCompanyName = company2.Name;
                    partOutboundBill.ReceivingCompanyCode = company2.Code;
                    partOutboundBill.ReceivingCompanyName = company2.Name;
                }

                partOutboundBill.OutboundType = (int)DcsPartsOutboundType.配件调拨;
                partOutboundBill.OriginalRequirementBillId = partsTransferOrder.Id;
                partOutboundBill.OriginalRequirementBillCode = partsTransferOrder.Code;
                partOutboundBill.OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.配件调拨单;
                partOutboundBill.SettlementStatus = (int)DcsPartsSettlementStatus.不结算;
                partOutboundBill.ReceivingWarehouseId = partsTransferOrder.DestWarehouseId;
                partOutboundBill.ReceivingWarehouseCode = partsTransferOrder.DestWarehouseCode;
                partOutboundBill.ReceivingWarehouseName = partsTransferOrder.DestWarehouseName;
                partOutboundBill.ShippingMethod = partsTransferOrder.ShippingMethod;
                partOutboundBill.PartsSalesCategoryId = partsSalesCategory.Id;
                partOutboundBill.PartsSalesOrderTypeId = partsSalesOrderType.Id;
                partOutboundBill.PartsSalesOrderTypeName = partsSalesOrderType.Name;
                partOutboundBill.Code = CodeGenerator.Generate("PartsOutboundBill", partOutboundBill.StorageCompanyCode);


                partOutboundBill.BranchId = warehouseJPCDC.BranchId;
                partOutboundBill.BranchCode = warehouseJPCDC.Branch.Code;
                partOutboundBill.BranchName = warehouseJPCDC.Branch.Name;
                partOutboundBill.CreatorId = userInfo.Id;
                partOutboundBill.CreatorName = userInfo.Name;
                partOutboundBill.CreateTime = DateTime.Now;

                InsertToDatabase(partOutboundBill);
                this.ObjectContext.SaveChanges();

                #endregion

                #region 生成入库计划单和入库检验单（调拨）
                var partsInboundPlan1 = new PartsInboundPlan();
                partsInboundPlan1.WarehouseId = partOutboundBill.ReceivingWarehouseId ?? 0;
                partsInboundPlan1.WarehouseCode = partOutboundBill.ReceivingWarehouseCode;
                partsInboundPlan1.WarehouseName = partOutboundBill.ReceivingWarehouseName;
                //查询出库单收货仓库对应仓库企业
                var company3 = this.ObjectContext.Companies.Where(r => r.Id == warehouseSCXCDC.StorageCompanyId).FirstOrDefault();
                if(company3 == null)
                    throw new ValidationException("未找到 对应仓储企业信息,请确认");

                partsInboundPlan1.StorageCompanyId = warehouseSCXCDC.StorageCompanyId;
                if(comapny != null) {
                    partsInboundPlan1.StorageCompanyCode = company3.Code;
                    partsInboundPlan1.StorageCompanyName = company3.Name;
                    partsInboundPlan1.StorageCompanyType = company3.Type;
                }
                partsInboundPlan1.PartsSalesCategoryId = partsSalesOrder.SalesCategoryId;
                partsInboundPlan1.PartsSalesCategorieName = partsSalesOrder.SalesCategoryName;
                partsInboundPlan1.CounterpartCompanyId = partOutboundBill.StorageCompanyId;
                partsInboundPlan1.CounterpartCompanyCode = partOutboundBill.StorageCompanyCode;
                partsInboundPlan1.CounterpartCompanyName = partOutboundBill.StorageCompanyName;
                partsInboundPlan1.OriginalRequirementBillId = partsTransferOrder.Id;
                partsInboundPlan1.OriginalRequirementBillCode = partsTransferOrder.Code;
                partsInboundPlan1.SourceId = partsTransferOrder.Id;
                partsInboundPlan1.SourceCode = partsTransferOrder.Code;
                partsInboundPlan1.OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.配件调拨单;
                partsInboundPlan1.Status = (int)DcsPartsInboundPlanStatus.检验完成;
                partsInboundPlan1.InboundType = (int)DcsPartsInboundType.配件调拨;

                partsInboundPlan1.Code = CodeGenerator.Generate("PartsInboundPlan", partsInboundPlan1.StorageCompanyCode);
                partsInboundPlan1.BranchId = partOutboundBill.BranchId;
                partsInboundPlan1.BranchCode = partOutboundBill.BranchCode;
                partsInboundPlan1.BranchName = partOutboundBill.BranchName;

                partsInboundPlan1.CreatorId = userInfo.Id;
                ;
                partsInboundPlan1.CreatorName = userInfo.Name;
                partsInboundPlan1.CreateTime = DateTime.Now;

                InsertToDatabase(partsInboundPlan1);
                this.ObjectContext.SaveChanges();

                var partsInboundCheckBill1 = new PartsInboundCheckBill();
                partsInboundCheckBill1.PartsInboundPlanId = partsInboundPlan1.Id;
                partsInboundCheckBill1.WarehouseId = partOutboundBill.ReceivingWarehouseId ?? 0;
                partsInboundCheckBill1.WarehouseCode = partOutboundBill.ReceivingWarehouseCode;
                partsInboundCheckBill1.WarehouseName = partOutboundBill.ReceivingWarehouseName;
                partsInboundCheckBill1.StorageCompanyId = company3.Id;
                partsInboundCheckBill1.StorageCompanyCode = company3.Code;
                partsInboundCheckBill1.StorageCompanyName = company3.Name;
                partsInboundCheckBill1.StorageCompanyType = company3.Type;
                partsInboundCheckBill1.PartsSalesCategoryId = partsSalesOrder.SalesCategoryId;
                partsInboundCheckBill1.CounterpartCompanyId = partOutboundBill.StorageCompanyId;
                partsInboundCheckBill1.CounterpartCompanyCode = partOutboundBill.StorageCompanyCode;
                partsInboundCheckBill1.CounterpartCompanyName = partOutboundBill.StorageCompanyName;
                partsInboundCheckBill1.InboundType = (int)DcsPartsInboundType.配件调拨;
                partsInboundCheckBill1.OriginalRequirementBillId = partsTransferOrder.Id;
                partsInboundCheckBill1.OriginalRequirementBillCode = partsTransferOrder.Code;
                partsInboundCheckBill1.Status = (int)DcsPartsInboundCheckBillStatus.上架完成;
                partsInboundCheckBill1.SettlementStatus = (int)DcsPartsSettlementStatus.不结算;
                partsInboundCheckBill1.Code = CodeGenerator.Generate("PartsInboundCheckBill", partsInboundCheckBill.StorageCompanyCode);

                partsInboundCheckBill1.BranchId = partOutboundBill.BranchId;
                partsInboundCheckBill1.BranchCode = partOutboundBill.BranchCode;
                partsInboundCheckBill1.BranchName = partOutboundBill.BranchName;
                partsInboundCheckBill1.CreatorId = userInfo.Id;
                ;
                partsInboundCheckBill1.CreatorName = userInfo.Name;
                partsInboundCheckBill1.CreateTime = DateTime.Now;

                InsertToDatabase(partsInboundCheckBill1);

                #endregion

                #region 生成出库单和出库计划单（分公司）
                //配件出库计划
                var partsOutboundPlan1 = new PartsOutboundPlan();
                partsOutboundPlan1.WarehouseId = warehouseSCXCDC.Id;
                partsOutboundPlan1.WarehouseCode = warehouseSCXCDC.Code;
                partsOutboundPlan1.WarehouseName = warehouseSCXCDC.Name;

                //获取企业信息
                var company4 = this.ObjectContext.Companies.Where(r => r.Id == warehouseSCXCDC.StorageCompanyId).FirstOrDefault();
                if(company4 != null) {
                    partsOutboundPlan1.StorageCompanyId = company4.Id;
                    partsOutboundPlan1.StorageCompanyCode = company4.Code;
                    partsOutboundPlan1.StorageCompanyName = company4.Name;
                    partsOutboundPlan1.StorageCompanyType = company4.Type;
                }
                partsOutboundPlan1.CounterpartCompanyId = agencyRetailerOrder.CounterpartCompanyId ?? 0;
                partsOutboundPlan1.CounterpartCompanyCode = agencyRetailerOrder.CounterpartCompanyCode;
                partsOutboundPlan1.CounterpartCompanyName = agencyRetailerOrder.CounterpartCompanyName;
                partsOutboundPlan1.ReceivingCompanyId = agencyRetailerOrder.CounterpartCompanyId ?? 0;
                partsOutboundPlan1.ReceivingCompanyCode = agencyRetailerOrder.CounterpartCompanyCode;
                partsOutboundPlan1.ReceivingCompanyName = agencyRetailerOrder.CounterpartCompanyName;

                partsOutboundPlan1.SourceId = partsSalesOrder.Id;
                partsOutboundPlan1.SourceCode = partsSalesOrder.Code;
                partsOutboundPlan1.OutboundType = (int)DcsPartsOutboundType.配件销售;
                partsOutboundPlan1.OriginalRequirementBillId = partsSalesOrder.Id;
                partsOutboundPlan1.OriginalRequirementBillCode = partsSalesOrder.Code;
                partsOutboundPlan1.OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.配件销售订单;
                partsOutboundPlan1.Status = (int)DcsPartsOutboundPlanStatus.出库完成;
                partsOutboundPlan1.ShippingMethod = (int)DcsPartsShippingMethod.公路物流;
                partsOutboundPlan1.IfWmsInterface = false;
                partsOutboundPlan1.PartsSalesCategoryId = partsSalesOrder.SalesCategoryId;
                partsOutboundPlan1.PartsSalesOrderTypeId = partsSalesOrderType.Id;
                partsOutboundPlan1.PartsSalesOrderTypeName = partsSalesOrderType.Name;
                partsOutboundPlan1.Code = CodeGenerator.Generate("PartsOutboundPlan", partsOutboundPlan.StorageCompanyCode);
                partsOutboundPlan1.ERPSourceOrderCode = agencyRetailerOrder.ERPSourceOrderCode;

                partsOutboundPlan1.BranchId = branch.Id;
                partsOutboundPlan1.BranchCode = branch.Code;
                partsOutboundPlan1.BranchName = branch.Name;

                partsOutboundPlan1.CreatorId = userInfo.Id;
                partsOutboundPlan1.CreatorName = userInfo.Name;
                partsOutboundPlan1.CreateTime = DateTime.Now;

                InsertToDatabase(partsOutboundPlan1);



                //配件出库单
                var partOutboundBill1 = new PartsOutboundBill();
                partOutboundBill1.WarehouseId = warehouseSCXCDC.Id;
                partOutboundBill1.WarehouseCode = warehouseSCXCDC.Code;
                partOutboundBill1.WarehouseName = warehouseSCXCDC.Name;
                if(company4 != null) {
                    partOutboundBill1.StorageCompanyId = company4.Id;
                    partOutboundBill1.StorageCompanyCode = company4.Code;
                    partOutboundBill1.StorageCompanyName = company4.Name;
                    partOutboundBill1.StorageCompanyType = company4.Type;
                }
                partOutboundBill1.CounterpartCompanyId = agencyRetailerOrder.CounterpartCompanyId ?? 0;
                partOutboundBill1.CounterpartCompanyCode = agencyRetailerOrder.CounterpartCompanyCode;
                partOutboundBill1.CounterpartCompanyName = agencyRetailerOrder.CounterpartCompanyName;
                partOutboundBill1.ReceivingCompanyId = agencyRetailerOrder.CounterpartCompanyId ?? 0;
                partOutboundBill1.ReceivingCompanyCode = agencyRetailerOrder.CounterpartCompanyCode;
                partOutboundBill1.ReceivingCompanyName = agencyRetailerOrder.CounterpartCompanyName;
                partOutboundBill1.PartsSalesCategoryId = partsSalesOrder.SalesCategoryId;
                partOutboundBill1.ERPSourceOrderCode = agencyRetailerOrder.ERPSourceOrderCode;

                var salesUnitAffiWarehouse3 = this.ObjectContext.SalesUnitAffiWarehouses.Include("SalesUnit").Where(r => r.WarehouseId == partOutboundBill1.WarehouseId && r.SalesUnit.PartsSalesCategoryId == partOutboundBill1.PartsSalesCategoryId).FirstOrDefault();
                if(salesUnitAffiWarehouse3 == null)
                    throw new ValidationException("未找到 精品北京CDC仓库的销售组织仓库关系,请确认");

                var customerAccount3 = this.ObjectContext.CustomerAccounts.Where(r => r.AccountGroupId == salesUnitAffiWarehouse3.SalesUnit.AccountGroupId && r.CustomerCompanyId == partOutboundBill1.CounterpartCompanyId && r.Status == (int)DcsMasterDataStatus.有效).FirstOrDefault();
                if(customerAccount3 == null)
                    throw new ValidationException("未找到 对应的客户账户, 请确认");
                partOutboundBill1.CustomerAccountId = customerAccount3.Id;

                partOutboundBill1.OutboundType = (int)DcsPartsOutboundType.配件销售;
                partOutboundBill1.OriginalRequirementBillId = partsSalesOrder.Id;
                partOutboundBill1.OriginalRequirementBillCode = partsSalesOrder.Code;
                partOutboundBill1.OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.配件销售订单;
                partOutboundBill1.SettlementStatus = (int)DcsPartsSettlementStatus.待结算;
                //partOutboundBill1.ReceivingWarehouseId = partsTransferOrder.DestWarehouseId;
                //partOutboundBill1.ReceivingWarehouseCode = partsTransferOrder.DestWarehouseCode;
                //partOutboundBill1.ReceivingWarehouseName = partsTransferOrder.DestWarehouseName;
                partOutboundBill1.ShippingMethod = (int)DcsPartsShippingMethod.公路物流;

                partOutboundBill1.PartsSalesOrderTypeId = partsSalesOrderType.Id;
                partOutboundBill1.PartsSalesOrderTypeName = partsSalesOrderType.Name;
                partOutboundBill1.Code = CodeGenerator.Generate("PartsOutboundBill", partOutboundBill1.StorageCompanyCode);


                partOutboundBill1.BranchId = branch.Id;
                partOutboundBill1.BranchCode = branch.Code;
                partOutboundBill1.BranchName = branch.Name;
                partOutboundBill1.CreatorId = userInfo.Id;
                partOutboundBill1.CreatorName = userInfo.Name;
                partOutboundBill1.CreateTime = DateTime.Now;

                InsertToDatabase(partOutboundBill1);
                this.ObjectContext.SaveChanges();
                #endregion

                #region 清单生成

                foreach(var agencyRetailerList in agencyRetailerLists.Where(r => r.CurrentOutQuantity > 0)) {
                    //配件出库计划清单-代理库
                    var agencyPartsOutboundPlanDetail = new APartsOutboundPlanDetail();
                    agencyPartsOutboundPlanDetail.SparePartId = agencyRetailerList.SparePartId.HasValue ? agencyRetailerList.SparePartId.Value : 0;
                    agencyPartsOutboundPlanDetail.SparePartCode = agencyRetailerList.SparePartCode;
                    agencyPartsOutboundPlanDetail.SparePartName = agencyRetailerList.SparePartName;
                    agencyPartsOutboundPlanDetail.PlannedAmount = agencyRetailerList.CurrentOutQuantity;
                    agencyPartsOutboundPlanDetail.OutboundFulfillment = agencyRetailerList.CurrentOutQuantity;
                    agencyPartsOutboundPlanDetail.Price = agencyRetailerList.UnitPrice.HasValue ? agencyRetailerList.UnitPrice.Value : 0;
                    agencyPartsOutboundPlanDetail.Remark = agencyRetailerList.Remark;

                    agencyPartsOutboundPlan.APartsOutboundPlanDetails.Add(agencyPartsOutboundPlanDetail);

                    //配件发运单清单-代理库
                    var agencyPartsShippingOrderDetail = new APartsShippingOrderDetail();
                    agencyPartsShippingOrderDetail.SparePartId = agencyRetailerList.SparePartId.HasValue ? agencyRetailerList.SparePartId.Value : 0;
                    agencyPartsShippingOrderDetail.SparePartCode = agencyRetailerList.SparePartCode;
                    agencyPartsShippingOrderDetail.SparePartName = agencyRetailerList.SparePartName;
                    agencyPartsShippingOrderDetail.ShippingAmount = agencyRetailerList.CurrentOutQuantity;
                    agencyPartsShippingOrderDetail.ConfirmedAmount = agencyRetailerList.CurrentOutQuantity;
                    agencyPartsShippingOrderDetail.SettlementPrice = agencyRetailerList.UnitPrice ?? 0;
                    agencyPartsShippingOrderDetail.Remark = agencyRetailerList.Remark;

                    agencyPartsShippingOrder.APartsShippingOrderDetails.Add(agencyPartsShippingOrderDetail);

                    //销售退货单清单
                    var partsSalesReturnBillDetail = new PartsSalesReturnBillDetail();
                    partsSalesReturnBillDetail.SparePartId = agencyRetailerList.SparePartId.HasValue ? agencyRetailerList.SparePartId.Value : 0;
                    partsSalesReturnBillDetail.SparePartCode = agencyRetailerList.SparePartCode;
                    partsSalesReturnBillDetail.SparePartName = agencyRetailerList.SparePartName;
                    partsSalesReturnBillDetail.ReturnedQuantity = agencyRetailerList.CurrentOutQuantity;
                    partsSalesReturnBillDetail.ApproveQuantity = agencyRetailerList.CurrentOutQuantity;
                    partsSalesReturnBillDetail.OriginalOrderPrice = agencyRetailerList.UnitPrice ?? 0;

                    //取最新的销售价格
                    var partsSalesPrice = partsSalesPrices.Where(r => r.SparePartId == partsSalesReturnBillDetail.SparePartId && r.PartsSalesCategoryId == agencyRetailerOrder.PartsSalesCategoryId).FirstOrDefault();
                    if(partsSalesPrice != null)
                        partsSalesReturnBillDetail.ReturnPrice = partsSalesPrice.SalesPrice;
                    else
                        throw new ValidationException("未找到对应配件的销售价格");
                    partsSalesReturnBillDetail.Remark = agencyRetailerList.Remark;

                    partsSalesReturnBill.PartsSalesReturnBillDetails.Add(partsSalesReturnBillDetail);


                    //配件入库计划单清单
                    var partsInboundPlanDetail = new PartsInboundPlanDetail();
                    partsInboundPlanDetail.SparePartId = agencyRetailerList.SparePartId.HasValue ? agencyRetailerList.SparePartId.Value : 0;
                    partsInboundPlanDetail.SparePartCode = agencyRetailerList.SparePartCode;
                    partsInboundPlanDetail.SparePartName = agencyRetailerList.SparePartName;
                    partsInboundPlanDetail.PlannedAmount = agencyRetailerList.CurrentOutQuantity;
                    partsInboundPlanDetail.InspectedQuantity = agencyRetailerList.CurrentOutQuantity;
                    partsInboundPlanDetail.Price = agencyRetailerList.UnitPrice ?? 0;

                    if(partsSalesPrice != null)
                        partsInboundPlanDetail.Price = partsSalesPrice.SalesPrice;
                    else
                        throw new ValidationException("未找到对应配件的销售价格");
                    partsInboundPlanDetail.Remark = agencyRetailerList.Remark;

                    partsInboundPlan.PartsInboundPlanDetails.Add(partsInboundPlanDetail);

                    //获取计划价
                    var partsPlanPrice = partsPlannedPrices.FirstOrDefault(r => r.SparePartId == partsSalesReturnBillDetail.SparePartId && r.PartsSalesCategoryId == agencyRetailerOrder.PartsSalesCategoryId);
                    if(partsPlanPrice == null)
                        throw new ValidationException("未找到对应配件的计划价");

                    //配件调拨单清单
                    var partsTransferOrderDetail = new PartsTransferOrderDetail();
                    partsTransferOrderDetail.SparePartId = agencyRetailerList.SparePartId.HasValue ? agencyRetailerList.SparePartId.Value : 0;
                    partsTransferOrderDetail.SparePartCode = agencyRetailerList.SparePartCode;
                    partsTransferOrderDetail.SparePartName = agencyRetailerList.SparePartName;
                    partsTransferOrderDetail.PlannedAmount = agencyRetailerList.CurrentOutQuantity;
                    partsTransferOrderDetail.ConfirmedAmount = agencyRetailerList.CurrentOutQuantity;
                    partsTransferOrderDetail.Price = partsSalesPrice.SalesPrice;
                    partsTransferOrderDetail.PlannPrice = partsPlanPrice.PlannedPrice;
                    partsTransferOrderDetail.Remark = agencyRetailerList.Remark;

                    partsTransferOrder.PartsTransferOrderDetails.Add(partsTransferOrderDetail);


                    //配件出库计划单清单（调拨单）
                    var partsOutboundPlanDetail = new PartsOutboundPlanDetail();
                    partsOutboundPlanDetail.SparePartId = agencyRetailerList.SparePartId.HasValue ? agencyRetailerList.SparePartId.Value : 0;
                    partsOutboundPlanDetail.SparePartCode = agencyRetailerList.SparePartCode;
                    partsOutboundPlanDetail.SparePartName = agencyRetailerList.SparePartName;
                    partsOutboundPlanDetail.PlannedAmount = agencyRetailerList.CurrentOutQuantity;
                    partsOutboundPlanDetail.OutboundFulfillment = agencyRetailerList.CurrentOutQuantity;
                    partsOutboundPlanDetail.Price = agencyRetailerList.UnitPrice.HasValue ? agencyRetailerList.UnitPrice.Value : 0;
                    partsOutboundPlanDetail.Remark = agencyRetailerList.Remark;

                    partsOutboundPlan.PartsOutboundPlanDetails.Add(partsOutboundPlanDetail);

                    //配件出库计划单清单（分公司）
                    var partsOutboundPlanDetail1 = new PartsOutboundPlanDetail();
                    partsOutboundPlanDetail1.SparePartId = agencyRetailerList.SparePartId.HasValue ? agencyRetailerList.SparePartId.Value : 0;
                    partsOutboundPlanDetail1.SparePartCode = agencyRetailerList.SparePartCode;
                    partsOutboundPlanDetail1.SparePartName = agencyRetailerList.SparePartName;
                    partsOutboundPlanDetail1.PlannedAmount = agencyRetailerList.CurrentOutQuantity;
                    partsOutboundPlanDetail1.OutboundFulfillment = agencyRetailerList.CurrentOutQuantity;
                    partsOutboundPlanDetail1.Price = agencyRetailerList.UnitPrice.HasValue ? agencyRetailerList.UnitPrice.Value : 0;
                    partsOutboundPlanDetail1.Remark = agencyRetailerList.Remark;

                    partsOutboundPlan1.PartsOutboundPlanDetails.Add(partsOutboundPlanDetail1);


                    //配件入库计划单清单（调拨）
                    var partsInboundPlanDetail1 = new PartsInboundPlanDetail();
                    partsInboundPlanDetail1.SparePartId = agencyRetailerList.SparePartId.HasValue ? agencyRetailerList.SparePartId.Value : 0;
                    partsInboundPlanDetail1.SparePartCode = agencyRetailerList.SparePartCode;
                    partsInboundPlanDetail1.SparePartName = agencyRetailerList.SparePartName;
                    partsInboundPlanDetail1.PlannedAmount = agencyRetailerList.CurrentOutQuantity;
                    partsInboundPlanDetail1.InspectedQuantity = agencyRetailerList.CurrentOutQuantity;
                    partsInboundPlanDetail1.Price = agencyRetailerList.UnitPrice ?? 0;
                    partsInboundPlanDetail1.Remark = agencyRetailerList.Remark;

                    partsInboundPlan1.PartsInboundPlanDetails.Add(partsInboundPlanDetail1);
                }

                partsSalesReturnBill.TotalAmount = partsSalesReturnBill.PartsSalesReturnBillDetails.Sum(r => r.ReturnPrice * r.ApproveQuantity.Value);

                foreach(var partsOutboundBillDetail in partsOutboundBillDetails.Where(r => r.OutboundAmount > 0)) {
                    //配件出库单清单-代理库
                    var agencyPartsOutboundBillDetail = new APartsOutboundBillDetail();
                    agencyPartsOutboundBillDetail.SparePartId = partsOutboundBillDetail.SparePartId;
                    agencyPartsOutboundBillDetail.SparePartCode = partsOutboundBillDetail.SparePartCode;
                    agencyPartsOutboundBillDetail.SparePartName = partsOutboundBillDetail.SparePartName;
                    agencyPartsOutboundBillDetail.OutboundAmount = partsOutboundBillDetail.OutboundAmount;
                    agencyPartsOutboundBillDetail.WarehouseAreaId = partsOutboundBillDetail.WarehouseAreaId;
                    agencyPartsOutboundBillDetail.WarehouseAreaCode = partsOutboundBillDetail.WarehouseAreaCode;
                    agencyPartsOutboundBillDetail.SettlementPrice = partsOutboundBillDetail.SettlementPrice;
                    //获取计划价
                    var partsPlanPrice = partsPlannedPrices.FirstOrDefault(r => r.SparePartId == partsOutboundBillDetail.SparePartId && r.PartsSalesCategoryId == agencyPartsOutboundBill.PartsSalesCategoryId);
                    if(partsPlanPrice != null)
                        agencyPartsOutboundBillDetail.CostPrice = partsPlanPrice.PlannedPrice;
                    else
                        throw new ValidationException("未找到对应配件的计划价");


                    agencyPartsOutboundBill.APartsOutboundBillDetails.Add(agencyPartsOutboundBillDetail);

                    //配件出库单清单（调拨）
                    var partsOutboundBillDetailNew = new PartsOutboundBillDetail();
                    partsOutboundBillDetailNew.SparePartId = partsOutboundBillDetail.SparePartId;
                    partsOutboundBillDetailNew.SparePartCode = partsOutboundBillDetail.SparePartCode;
                    partsOutboundBillDetailNew.SparePartName = partsOutboundBillDetail.SparePartName;
                    partsOutboundBillDetailNew.OutboundAmount = partsOutboundBillDetail.OutboundAmount;
                    partsOutboundBillDetailNew.WarehouseAreaId = partsOutboundBillDetail.WarehouseAreaId;
                    partsOutboundBillDetailNew.WarehouseAreaCode = partsOutboundBillDetail.WarehouseAreaCode;
                    partsOutboundBillDetailNew.SettlementPrice = partsOutboundBillDetail.SettlementPrice;

                    partsPlanPrice = partsPlannedPrices.FirstOrDefault(r => r.SparePartId == partsOutboundBillDetail.SparePartId && r.PartsSalesCategoryId == partOutboundBill.PartsSalesCategoryId);
                    if(partsPlanPrice != null)
                        partsOutboundBillDetailNew.CostPrice = partsPlanPrice.PlannedPrice;
                    else
                        throw new ValidationException("未找到对应配件的计划价");

                    partOutboundBill.PartsOutboundBillDetails.Add(partsOutboundBillDetailNew);

                    //配件入库单清单
                    var partsInboundCheckBillDetail = new PartsInboundCheckBillDetail();
                    partsInboundCheckBillDetail.SparePartId = partsOutboundBillDetail.SparePartId;
                    partsInboundCheckBillDetail.SparePartCode = partsOutboundBillDetail.SparePartCode;
                    partsInboundCheckBillDetail.SparePartName = partsOutboundBillDetail.SparePartName;
                    partsInboundCheckBillDetail.WarehouseAreaId = partsOutboundBillDetail.WarehouseAreaId ?? 0;
                    partsInboundCheckBillDetail.WarehouseAreaCode = partsOutboundBillDetail.WarehouseAreaCode;
                    partsInboundCheckBillDetail.InspectedQuantity = partsOutboundBillDetail.OutboundAmount;

                    //取最新的销售价格
                    var partsSalesPrice = partsSalesPrices.Where(r => r.SparePartId == partsOutboundBillDetail.SparePartId && r.PartsSalesCategoryId == partsInboundCheckBill.PartsSalesCategoryId).FirstOrDefault();
                    if(partsSalesPrice != null) {
                        partsInboundCheckBillDetail.SettlementPrice = partsSalesPrice.SalesPrice;
                    } else
                        throw new ValidationException("未找到对应配件的销售价格");

                    //取最新的计划价 
                    partsPlanPrice = partsPlannedPrices.FirstOrDefault(r => r.SparePartId == partsOutboundBillDetail.SparePartId && r.PartsSalesCategoryId == partsInboundCheckBill.PartsSalesCategoryId);
                    if(partsPlanPrice != null)
                        partsInboundCheckBillDetail.CostPrice = partsPlanPrice.PlannedPrice;
                    else
                        throw new ValidationException("未找到对应配件的计划价");

                    partsInboundCheckBill.PartsInboundCheckBillDetails.Add(partsInboundCheckBillDetail);

                    //配件入库单清单（调拨）

                    var partsInboundCheckBillDetail1 = new PartsInboundCheckBillDetail();
                    partsInboundCheckBillDetail1.SparePartId = partsOutboundBillDetail.SparePartId;
                    partsInboundCheckBillDetail1.SparePartCode = partsOutboundBillDetail.SparePartCode;
                    partsInboundCheckBillDetail1.SparePartName = partsOutboundBillDetail.SparePartName;
                    partsInboundCheckBillDetail1.WarehouseAreaId = partsOutboundBillDetail.WarehouseAreaId ?? 0;
                    partsInboundCheckBillDetail1.WarehouseAreaCode = partsOutboundBillDetail.WarehouseAreaCode;
                    partsInboundCheckBillDetail1.InspectedQuantity = partsOutboundBillDetail.OutboundAmount;
                    partsInboundCheckBillDetail1.SettlementPrice = partsOutboundBillDetail.SettlementPrice;

                    //取最新的计划价 
                    partsPlanPrice = partsPlannedPrices.FirstOrDefault(r => r.SparePartId == partsOutboundBillDetail.SparePartId && r.PartsSalesCategoryId == partsInboundCheckBill1.PartsSalesCategoryId);
                    if(partsPlanPrice != null)
                        partsInboundCheckBillDetail1.CostPrice = partsPlanPrice.PlannedPrice;
                    else
                        throw new ValidationException("未找到对应配件的计划价");

                    partsInboundCheckBill1.PartsInboundCheckBillDetails.Add(partsInboundCheckBillDetail1);

                    //配件出库单清单(分公司)
                    var partsOutboundBillDetail1 = new PartsOutboundBillDetail();
                    partsOutboundBillDetail1.SparePartId = partsOutboundBillDetail.SparePartId;
                    partsOutboundBillDetail1.SparePartCode = partsOutboundBillDetail.SparePartCode;
                    partsOutboundBillDetail1.SparePartName = partsOutboundBillDetail.SparePartName;
                    partsOutboundBillDetail1.OutboundAmount = partsOutboundBillDetail.OutboundAmount;
                    partsOutboundBillDetail1.WarehouseAreaId = partsOutboundBillDetail.WarehouseAreaId;
                    partsOutboundBillDetail1.WarehouseAreaCode = partsOutboundBillDetail.WarehouseAreaCode;
                    partsOutboundBillDetail1.SettlementPrice = partsOutboundBillDetail.SettlementPrice;

                    partsPlanPrice = partsPlannedPrices.FirstOrDefault(r => r.SparePartId == partsOutboundBillDetail.SparePartId && r.PartsSalesCategoryId == partOutboundBill1.PartsSalesCategoryId);
                    if(partsPlanPrice != null)
                        partsOutboundBillDetail1.CostPrice = partsPlanPrice.PlannedPrice;
                    else
                        throw new ValidationException("未找到对应配件的计划价");


                    partOutboundBill1.PartsOutboundBillDetails.Add(partsOutboundBillDetail1);
                }
                //调拨总金额
                partsTransferOrder.TotalAmount = partsTransferOrder.PartsTransferOrderDetails.Sum(r => r.ConfirmedAmount * r.Price ?? 0);
                //计划价总金额
                partsTransferOrder.TotalPlanAmount = partsTransferOrder.PartsTransferOrderDetails.Sum(r => r.ConfirmedAmount * r.PlannPrice ?? 0);
                #endregion

                agencyRetailerOrder.ConfirmorId = userInfo.Id;
                agencyRetailerOrder.ConfirmorName = userInfo.Name;
                agencyRetailerOrder.ConfirmorTime = DateTime.Now;

                partsSalesOrder.ModifierId = userInfo.Id;
                partsSalesOrder.ModifierName = userInfo.Name;
                partsSalesOrder.ModifyTime = DateTime.Now;
                partsSalesOrder.ApproverId = userInfo.Id;
                partsSalesOrder.ApproverName = userInfo.Name;
                partsSalesOrder.ApproveTime = DateTime.Now;

                UpdateToDatabase(partsSalesOrder);
                UpdateToDatabase(agencyRetailerOrder);

                this.ObjectContext.SaveChanges();

                PMS_SYC_150_SendPMSUpdateOrderStatus_PS(agencyRetailerOrder, partsSalesOrder);

                transaction.Complete();
            }
        }

        public void PMS_SYC_150_SendPMSUpdateOrderStatus_PS(AgencyRetailerOrder agencyRetailerOrder, PartsSalesOrder partsSalesOrder) {
            bool interfaceEnabled;
            Boolean.TryParse(WebConfigurationManager.AppSettings["isInterfaceEnabled150"], out interfaceEnabled);
            if(!interfaceEnabled)
                return;
            var agencyRetailerLists = this.ObjectContext.AgencyRetailerLists.Where(v => v.AgencyRetailerOrderId == agencyRetailerOrder.Id).ToArray();
            //if(partsSalesOrder.SalesCategoryName == "随车行") {
            int status = 0;
            switch(agencyRetailerOrder.Status) {
                case 2:
                    status = 4;
                    break;
                case 3:
                    status = 5;
                    break;
                case 4:
                    status = 6;
                    break;
            }
            StringBuilder sb1 = new StringBuilder();
            sb1.Append("<DATA>");
            sb1.Append("<HEAD>");
            sb1.Append("<BIZTRANSACTIONID>PMS_SYC_150_" + DateTime.Now.ToString("yyyyMMddhhmmssfff") + "</BIZTRANSACTIONID>");
            sb1.Append("<COUNT>1</COUNT>");
            sb1.Append("<CONSUMER></CONSUMER>");
            sb1.Append("<SRVLEVEL>1</SRVLEVEL>");
            sb1.Append("<ACCOUNT></ACCOUNT>");
            sb1.Append("<PASSWORD></PASSWORD>");
            sb1.Append("</HEAD>");
            sb1.Append("<LIST>");
            sb1.Append("<ITEM>");
            sb1.Append("<orderNumber>" + partsSalesOrder.ERPSourceOrderCode + "</orderNumber>");
            sb1.Append("<Code>" + agencyRetailerOrder.Code + "</Code>");
            sb1.Append("<status>" + status + "</status>");
            sb1.Append("<orderPattern>" + "W" + "</orderPattern>");
            sb1.Append("<remark></remark>");
            sb1.Append("<orderList>");
            foreach(var agencyRetailerList in agencyRetailerLists) {
                sb1.Append("<ITEM>");
                sb1.Append("<goodCode>" + agencyRetailerList.SparePartCode + "</goodCode>");
                sb1.Append("<Quantity>" + agencyRetailerList.OrderedQuantity + "</Quantity>");
                sb1.Append("<ApproveQuantity>" + agencyRetailerList.ConfirmedAmount + "</ApproveQuantity>");
                sb1.Append("</ITEM>");
            }
            sb1.Append("</orderList>");
            sb1.Append("</ITEM>");
            sb1.Append("</LIST>");
            sb1.Append("</DATA>");
            var listXml = sb1.ToString();
            var listPartsSalesOrder = listXml;
            var report = "";
            try {
                var retailerUpdateOrderStatus = new PMS_SYC_150_SendPMSUpdateOrderStatusService_pttbindingQSService();

                retailerUpdateOrderStatus.Credentials = new NetworkCredential("PMS", "pms201603111515");
                var error = retailerUpdateOrderStatus.PMS_SYC_150_SendPMSUpdateOrderStatusService(listXml, out report);
                using(TransactionScope scope = new TransactionScope(TransactionScopeOption.RequiresNew)) {
                    //var nccesb = new ESBLogDianShang();
                    //nccesb.InterfaceName = "RetailerOrderStatus";
                    //nccesb.Message = report + "----" + listPartsSalesOrder;
                    //nccesb.SyncNumberBegin = partsSalesOrder.Id;
                    //nccesb.SyncNumberEnd = 0;
                    //nccesb.TheDate = DateTime.Now;
                    //InsertToDatabase(nccesb);
                    this.ObjectContext.SaveChanges();
                    scope.Complete();
                }
            } catch(Exception e) {
                using(TransactionScope scope = new TransactionScope(TransactionScopeOption.RequiresNew)) {
                    //var nccesb = new ESBLogDianShang();
                    //nccesb.InterfaceName = "RetailerOrderStatus";
                    //nccesb.Message = report + e.Message + "----" + listPartsSalesOrder;
                    //nccesb.SyncNumberBegin = partsSalesOrder.Id;
                    //nccesb.SyncNumberEnd = 0;
                    //nccesb.TheDate = DateTime.Now;
                    //InsertToDatabase(nccesb);
                    string s = e.Message;
                    this.ObjectContext.SaveChanges();
                    scope.Complete();
                }
            }
            //}
        }
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:11:57
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 终止代理库电商订单(AgencyRetailerOrder agencyRetailerOrder) {
            new AgencyRetailerOrderAch(this).终止代理库电商订单(agencyRetailerOrder);
        }


        [Invoke]
        public void 确认代理库电商订单(AgencyRetailerOrder agencyRetailerOrder, List<AgencyRetailerList> agencyRetailerLists, ObservableCollection<PartsOutboundBillDetail> partsOutboundBillDetails) {
            new AgencyRetailerOrderAch(this).确认代理库电商订单(agencyRetailerOrder, agencyRetailerLists, partsOutboundBillDetails);
        }
    }
}