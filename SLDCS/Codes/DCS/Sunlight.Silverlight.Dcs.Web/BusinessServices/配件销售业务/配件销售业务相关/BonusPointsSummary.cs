﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using System.Text;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class BonusPointsSummaryAch : DcsSerivceAchieveBase {
        public BonusPointsSummaryAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 新增积分汇总单(BonusPointsSummary bonusPointsSummary) {
            var userInfo = Utils.GetCurrentUserInfo();
            if(string.IsNullOrWhiteSpace(bonusPointsSummary.BonusPointsSummaryCode) || bonusPointsSummary.BonusPointsSummaryCode == GlobalVar.ASSIGNED_BY_SERVER)
                bonusPointsSummary.BonusPointsSummaryCode = CodeGenerator.Generate("BonusPointsSummary", userInfo.EnterpriseCode);
            InsertBonusPointsSummary(bonusPointsSummary);
            var bonusPointsIds = bonusPointsSummary.BonusPointsSummaryLists.Select(r => r.BonusPointsId);
            var bonusPoints = this.ObjectContext.BonusPointsOrders.Where(r => bonusPointsIds.Contains(r.Id));
            if(bonusPoints == null) {
                throw new ValidationException("不存在积分订单");
            }
            foreach(var item in bonusPoints) {
                item.SettlementStatus = (int)DcsBonusPointsSettlementStatus.已汇总;
                UpdateToDatabase(item);
            }
        }

        public void 作废积分汇总单(int[] ids) {
            var userInfo = Utils.GetCurrentUserInfo();
            var bonusPointsSummaries = this.ObjectContext.BonusPointsSummaries.Where(r => ids.Contains(r.Id)).ToArray();
            foreach(var item in bonusPointsSummaries) {
                item.Status = (int)DcsBonusPointsSummaryStatus.作废;
                item.AbandonerId = userInfo.Id;
                item.AbandonerName = userInfo.Name;
                item.AbandonTime = DateTime.Now;
                UpdateToDatabase(item);
                var bonusPointsOrderIds = item.BonusPointsSummaryLists.Select(r => r.BonusPointsId).ToArray();
                var bonusPointsOrders = this.ObjectContext.BonusPointsOrders.Where(r => bonusPointsOrderIds.Contains(r.Id)).ToArray();
                foreach(var detail in bonusPointsOrders) {
                    detail.SettlementStatus = (int)DcsBonusPointsSettlementStatus.未汇总;
                    UpdateToDatabase(detail);
                }
            }
            ObjectContext.SaveChanges();
        }

        public void 审核积分汇总单(int[] ids) {
            var userInfo = Utils.GetCurrentUserInfo();
            var bonusPointsSummaries = this.ObjectContext.BonusPointsSummaries.Where(r => ids.Contains(r.Id)).ToArray();
            foreach(var item in bonusPointsSummaries) {
                item.Status = (int)DcsBonusPointsSummaryStatus.已审批;
                item.ApproverId = userInfo.Id;
                item.ApproverName = userInfo.Name;
                item.ApproveTime = DateTime.Now;
                UpdateToDatabase(item);
                var bonusPointsOrderIds = item.BonusPointsSummaryLists.Select(r => r.BonusPointsId).ToArray();
                var bonusPointsOrders = this.ObjectContext.BonusPointsOrders.Where(r => bonusPointsOrderIds.Contains(r.Id)).ToArray();
                foreach(var detail in bonusPointsOrders) {
                    detail.SettlementStatus = (int)DcsBonusPointsSettlementStatus.已汇总;
                    UpdateToDatabase(detail);
                }
            }
            ObjectContext.SaveChanges();
        }

    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:11:57
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 新增积分汇总单(BonusPointsSummary bonusPointsSummary) {
            new BonusPointsSummaryAch(this).新增积分汇总单(bonusPointsSummary);
        }
        
        [Invoke(HasSideEffects = true)]
        public void 作废积分汇总单(int[] ids) {
            new BonusPointsSummaryAch(this).作废积分汇总单(ids);
        }
        
        [Invoke(HasSideEffects = true)]
        public void 审核积分汇总单(int[] ids) {
            new BonusPointsSummaryAch(this).审核积分汇总单(ids);
        }
    }
}
