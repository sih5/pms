﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Data;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using System.Text;
using Devart.Data.Oracle;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class CrossSalesOrderAch : DcsSerivceAchieveBase {
        public void 新增跨区域销售单(CrossSalesOrder crossSalesOrder) {
            this.InsertCrossSalesOrderValidate(crossSalesOrder);
        }
        public void 修改跨区域销售单(CrossSalesOrder crossSalesOrder) {
            this.UpdateCrossSalesOrderValidate(crossSalesOrder);
        }
        public void 提交跨区域销售单(CrossSalesOrder crossSalesOrder) {
            var dbcrossSalesOrder = ObjectContext.CrossSalesOrders.Where(r => r.Id == crossSalesOrder.Id && r.Status == (int)DCSCrossSalesOrderStatus.新建).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbcrossSalesOrder == null)
                throw new ValidationException("只能提交处于‘新建’状态的订单");
            var userInfo = Utils.GetCurrentUserInfo();
            UpdateToDatabase(crossSalesOrder);
            crossSalesOrder.Status = (int)DCSCrossSalesOrderStatus.提交;
            crossSalesOrder.SubmitterId = userInfo.Id;
            crossSalesOrder.SubmitterName = userInfo.Name;
            crossSalesOrder.SubmitTime = DateTime.Now;
            this.UpdateCrossSalesOrderValidate(crossSalesOrder);

        }
        public void 作废跨区域销售单(CrossSalesOrder crossSalesOrder) {
            var dbcrossSalesOrder = ObjectContext.CrossSalesOrders.Where(r => r.Id == crossSalesOrder.Id && r.Status == (int)DCSCrossSalesOrderStatus.新建).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbcrossSalesOrder == null)
                throw new ValidationException("只能作废处于‘新建’状态的订单");
            var userInfo = Utils.GetCurrentUserInfo();
            UpdateToDatabase(crossSalesOrder);
            crossSalesOrder.Status = (int)DCSCrossSalesOrderStatus.作废;
            crossSalesOrder.AbandonerId = userInfo.Id;
            crossSalesOrder.AbandonerName = userInfo.Name;
            crossSalesOrder.AbandonTime = DateTime.Now;
            this.UpdateCrossSalesOrderValidate(crossSalesOrder);

        }
        public void 终止跨区域销售单(CrossSalesOrder crossSalesOrder) {
            var dbcrossSalesOrder = ObjectContext.CrossSalesOrders.Where(r => r.Id == crossSalesOrder.Id && (r.Status == (int)DCSCrossSalesOrderStatus.提交 || r.Status == (int)DCSCrossSalesOrderStatus.初审通过 || r.Status == (int)DCSCrossSalesOrderStatus.审核通过 || r.Status == (int)DCSCrossSalesOrderStatus.审批通过 || r.Status == (int)DCSCrossSalesOrderStatus.高级审核通过)).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbcrossSalesOrder == null)
                throw new ValidationException("只能终止处于‘提交、初审通过、审核通过、审批通过、高级审核通过’状态的订单");
            var userInfo = Utils.GetCurrentUserInfo();
            UpdateToDatabase(crossSalesOrder);
            crossSalesOrder.Status = (int)DCSCrossSalesOrderStatus.终止;
            crossSalesOrder.StoperId = userInfo.Id;
            crossSalesOrder.Stoper = userInfo.Name;
            crossSalesOrder.StopTime = DateTime.Now;
            this.UpdateCrossSalesOrderValidate(crossSalesOrder);
        }
        public void 驳回跨区域销售单(CrossSalesOrder crossSalesOrder) {
            var dbcrossSalesOrder = ObjectContext.CrossSalesOrders.Where(r => r.Id == crossSalesOrder.Id && (r.Status == (int)DCSCrossSalesOrderStatus.提交 || r.Status == (int)DCSCrossSalesOrderStatus.初审通过 || r.Status == (int)DCSCrossSalesOrderStatus.审核通过 || r.Status == (int)DCSCrossSalesOrderStatus.审批通过 || r.Status == (int)DCSCrossSalesOrderStatus.新建)).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbcrossSalesOrder == null)
                throw new ValidationException("当前状态不能驳回");
            var userInfo = Utils.GetCurrentUserInfo();
            UpdateToDatabase(crossSalesOrder);
            crossSalesOrder.Status = (int)DCSCrossSalesOrderStatus.新建;
            crossSalesOrder.SubmitterId = null;
            crossSalesOrder.SubmitterName = null;
            crossSalesOrder.SubmitTime = null;
            crossSalesOrder.ApproverId = null;
            crossSalesOrder.ApproverName = null;
            crossSalesOrder.ApproveTime = null;
            crossSalesOrder.CheckerId = null;
            crossSalesOrder.CheckerName = null;
            crossSalesOrder.CheckTime = null;
            crossSalesOrder.CloserId = null;
            crossSalesOrder.CloserName = null;
            crossSalesOrder.CloseTime = null;
            this.UpdateCrossSalesOrderValidate(crossSalesOrder);
        }
        public void 初审跨区域销售单(CrossSalesOrder crossSalesOrder) {
            var dbcrossSalesOrder = ObjectContext.CrossSalesOrders.Where(r => r.Id == crossSalesOrder.Id && r.Status == (int)DCSCrossSalesOrderStatus.提交).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbcrossSalesOrder == null)
                throw new ValidationException("只能初审处于‘提交’状态的订单");
            var userInfo = Utils.GetCurrentUserInfo();
            UpdateToDatabase(crossSalesOrder);
            crossSalesOrder.Status = (int)DCSCrossSalesOrderStatus.初审通过;
            crossSalesOrder.ApproverId = userInfo.Id;
            crossSalesOrder.ApproverName = userInfo.Name;
            crossSalesOrder.ApproveTime = DateTime.Now;
            this.UpdateCrossSalesOrderValidate(crossSalesOrder);
        }
        public void 审核跨区域销售单(CrossSalesOrder crossSalesOrder) {
            var dbcrossSalesOrder = ObjectContext.CrossSalesOrders.Where(r => r.Id == crossSalesOrder.Id && r.Status == (int)DCSCrossSalesOrderStatus.初审通过).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbcrossSalesOrder == null)
                throw new ValidationException("只能初审处于‘提交’状态的订单");
            var userInfo = Utils.GetCurrentUserInfo();
            UpdateToDatabase(crossSalesOrder);
            crossSalesOrder.Status = (int)DCSCrossSalesOrderStatus.审核通过;
            crossSalesOrder.CheckerId = userInfo.Id;
            crossSalesOrder.CheckerName = userInfo.Name;
            crossSalesOrder.CheckTime = DateTime.Now;
            this.UpdateCrossSalesOrderValidate(crossSalesOrder);
        }
        public void 审批跨区域销售单(CrossSalesOrder crossSalesOrder) {
            var dbcrossSalesOrder = ObjectContext.CrossSalesOrders.Where(r => r.Id == crossSalesOrder.Id && r.Status == (int)DCSCrossSalesOrderStatus.审核通过).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbcrossSalesOrder == null)
                throw new ValidationException("只能初审处于‘提交’状态的订单");
            var userInfo = Utils.GetCurrentUserInfo();
            UpdateToDatabase(crossSalesOrder);
            crossSalesOrder.Status = (int)DCSCrossSalesOrderStatus.审批通过;
            crossSalesOrder.CloserId = userInfo.Id;
            crossSalesOrder.CloserName = userInfo.Name;
            crossSalesOrder.CloseTime = DateTime.Now;
            this.UpdateCrossSalesOrderValidate(crossSalesOrder);
        }
        public void 高级审核跨区域销售单(CrossSalesOrder crossSalesOrder) {
            var dbcrossSalesOrder = ObjectContext.CrossSalesOrders.Include("CrossSalesOrderDetails").Where(r => r.Id == crossSalesOrder.Id && r.Status == (int)DCSCrossSalesOrderStatus.审批通过).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbcrossSalesOrder == null)
                throw new ValidationException("只能初审处于‘提交’状态的订单");
            var userInfo = Utils.GetCurrentUserInfo();
            UpdateToDatabase(crossSalesOrder);
            crossSalesOrder.Status = (int)DCSCrossSalesOrderStatus.高级审核通过;
            crossSalesOrder.SeniorAuditId = userInfo.Id;
            crossSalesOrder.SeniorAuditName = userInfo.Name;
            crossSalesOrder.SeniorAuditTime = DateTime.Now;
            this.UpdateCrossSalesOrderValidate(crossSalesOrder);
            //是否生成销售单=是 时 判断配件是否有效，有效则生成销售订单
            if(crossSalesOrder.IsAutoSales.HasValue && crossSalesOrder.IsAutoSales.Value && dbcrossSalesOrder.CrossSalesOrderDetails.All(t => t.SparePartId != null && t.CenterPrice != null)) {
                crossSalesOrder.Status = (int)DCSCrossSalesOrderStatus.生效;
                this.生成销售订单(crossSalesOrder, dbcrossSalesOrder.CrossSalesOrderDetails.ToList());
            }
            if((!crossSalesOrder.IsAutoSales.HasValue || (crossSalesOrder.IsAutoSales.HasValue && !crossSalesOrder.IsAutoSales.Value)) && dbcrossSalesOrder.CrossSalesOrderDetails.All(t => t.SparePartId != null )) {
                crossSalesOrder.Status = (int)DCSCrossSalesOrderStatus.生效;
            }
        }
        private void 生成销售订单(CrossSalesOrder dbcrossSalesOrder, List<CrossSalesOrderDetail> crossSalesOrderDetails) {
            var userInfo = Utils.GetCurrentUserInfo();
            //订单类型：紧急订单
            var orderType = ObjectContext.PartsSalesOrderTypes.Where(t => t.Name == "紧急订单").FirstOrDefault();
            if(orderType == null) {
                throw new ValidationException("未找到'紧急订单' 订单类型");
            }
            var company = ObjectContext.Companies.Where(t => t.Type == (int)DcsCompanyType.分公司 && t.Status == (int)DcsMasterDataStatus.有效).First();
            var partsSalesCategory = ObjectContext.PartsSalesCategories.Where(t => t.Status == (int)DcsBaseDataStatus.有效 && t.BranchId == company.Id).First();
            //销售组织
            var salesUnit = ObjectContext.SalesUnits.Where(t => t.OwnerCompanyId == company.Id && t.Status == (int)DcsMasterDataStatus.有效 && t.BranchId == company.Id).FirstOrDefault();
            if(salesUnit == null) {
                throw new ValidationException("未找到" + company.Code + "的销售组织");
            }
            //客户账户
            var customerAccount = ObjectContext.CustomerAccounts.Where(t => t.CustomerCompanyId == dbcrossSalesOrder.ApplyCompnayId && t.Status == (int)DcsMasterDataStatus.有效 && ObjectContext.AccountGroups.Any(p => p.Id == t.AccountGroupId && p.SalesCompanyId == company.Id)).FirstOrDefault();
            if(customerAccount == null) {
                throw new ValidationException("未找到提报单位的账户信息");
            }
            //订货仓库
            var orderWarehouse = ObjectContext.Warehouses.Where(t => t.Name == "双桥总库" && t.Status == (int)DcsBaseDataStatus.有效 && t.Type == (int)DcsWarehouseType.总库).FirstOrDefault();
            if(orderWarehouse == null) {
                throw new ValidationException("未找到订货仓库'双桥总库'");
            }
            //收货仓库
            var receWarehouse = ObjectContext.Warehouses.Where(t => t.StorageCompanyId == dbcrossSalesOrder.ApplyCompnayId && t.Status == (int)DcsBaseDataStatus.有效 && t.Type == (int)DcsWarehouseType.总库).FirstOrDefault();
            if(receWarehouse == null) {
                throw new ValidationException("未找到收货单位的收货仓库");
            }
            var subCompany = ObjectContext.Companies.Where(t => t.Id == dbcrossSalesOrder.ApplyCompnayId).First();
            var spIds = crossSalesOrderDetails.Select(t => t.SparePartId).ToArray();
            var spareparts = ObjectContext.SpareParts.Where(t => spIds.Contains(t.Id)).ToArray();
            //主单
            var partsSalesOrder = new PartsSalesOrder {
                Code = orderType.BusinessCode + CodeGenerator.Generate("PartsSalesOrder", company.Code),
                BranchId = company.Id,
                BranchCode = company.Code,
                BranchName = company.Name,
                SalesCategoryId = partsSalesCategory.Id,
                SalesCategoryName = partsSalesCategory.Name,
                SalesUnitId = salesUnit.Id,
                SalesUnitCode = salesUnit.Code,
                SalesUnitName = salesUnit.Name,
                SalesUnitOwnerCompanyId = company.Id,
                SalesUnitOwnerCompanyCode = company.Code,
                SalesUnitOwnerCompanyName = company.Name,
                SourceBillCode = dbcrossSalesOrder.Code,
                SourceBillId = dbcrossSalesOrder.Id,
                CustomerAccountId = customerAccount.Id,
                IsDebt = false,
                InvoiceReceiveCompanyId = dbcrossSalesOrder.ApplyCompnayId,
                InvoiceReceiveCompanyCode = dbcrossSalesOrder.ApplyCompnayCode,
                InvoiceReceiveCompanyName = dbcrossSalesOrder.ApplyCompnayName,
                InvoiceReceiveCompanyType = 3,
                SubmitCompanyId = dbcrossSalesOrder.ApplyCompnayId,
                SubmitCompanyCode = dbcrossSalesOrder.ApplyCompnayCode,
                SubmitCompanyName = dbcrossSalesOrder.ApplyCompnayName,
                PartsSalesOrderTypeId = orderType.Id,
                PartsSalesOrderTypeName = orderType.Name,
                IfAgencyService = true,
                SalesActivityDiscountRate = 1,
                SalesActivityDiscountAmount = 0,
                RequestedDeliveryTime = DateTime.Now.AddDays(15),
                IfDirectProvision = false,
                CustomerType = (int)DcsCompanyType.代理库,
                Remark = "跨区域销售订单",
                ReceivingCompanyId = dbcrossSalesOrder.ApplyCompnayId,
                ReceivingCompanyCode = dbcrossSalesOrder.ApplyCompnayCode,
                ReceivingCompanyName = dbcrossSalesOrder.ApplyCompnayName,
                ShippingMethod = dbcrossSalesOrder.ShippingMethod,
                ContactPerson = dbcrossSalesOrder.ReceivingName,
                ContactPhone = dbcrossSalesOrder.ReceivingPhone,
                ReceivingAddress = dbcrossSalesOrder.ReceivingAddress,
                ReceivingWarehouseId = receWarehouse.Id,
                ReceivingWarehouseName = receWarehouse.Name,
                WarehouseId = orderWarehouse.Id,
                Status = (int)DcsPartsSalesOrderStatus.提交,
                CreateTime = DateTime.Now,
                CreatorId = userInfo.Id,
                CreatorName = userInfo.Name,
                SubmitterId = userInfo.Id,
                SubmitterName = userInfo.Name,
                SubmitTime = DateTime.Now,
                IsBarter = false,
                Province=subCompany.ProvinceName,
                City = subCompany.CityName
            };
            if(!string.IsNullOrEmpty(partsSalesOrder.Province)) {
                var region = ObjectContext.Regions.Where(t => t.Name == partsSalesOrder.Province && t.Type == 2).FirstOrDefault();
                if(region!=null) {
                    partsSalesOrder.ProvinceID = region.Id;
                }
            }
            //清单
            foreach(var item in crossSalesOrderDetails) {
                var sparepart = spareparts.Where(t => t.Id == item.SparePartId).First();
                var salesDetail = new PartsSalesOrderDetail {
                    SparePartId = item.SparePartId.Value,
                    SparePartName = item.SparePartName,
                    SparePartCode = item.SparePartCode,
                    OrderedQuantity = item.OrderedQuantity,
                    OriginalPrice = item.RetailGuidePrice.Value,
                    CustOrderPriceGradeCoefficient = 1,
                    IfCanNewPart = false,
                    DiscountedPrice = 0,
                    OrderPrice = item.CenterPrice.Value,
                    OrderSum = item.CenterPrice.Value * item.OrderedQuantity,
                    SalesPrice = item.SalesPrice,
                    ABCStrategy = item.ABCStrategy,
                    PriceTypeName = item.PriceTypeName,
                    MeasureUnit = sparepart.MeasureUnit
                };
                partsSalesOrder.PartsSalesOrderDetails.Add(salesDetail);
            }
            partsSalesOrder.TotalAmount = partsSalesOrder.PartsSalesOrderDetails.Sum(t => t.OrderSum);
            InsertToDatabase(partsSalesOrder);
            dbcrossSalesOrder.SalesCode = partsSalesOrder.Code;
			UpdateToDatabase(dbcrossSalesOrder);
            ObjectContext.SaveChanges();
        }
        public void 完善资料(CrossSalesOrder crossSalesOrder) {
            var dbcrossSalesOrder = ObjectContext.CrossSalesOrders.Include("CrossSalesOrderDetails").Where(r => r.Id == crossSalesOrder.Id && r.Status == (int)DCSCrossSalesOrderStatus.高级审核通过).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbcrossSalesOrder == null)
                throw new ValidationException("只能处理处于‘高级审核通过’状态的订单");
            var userInfo = Utils.GetCurrentUserInfo();
            UpdateToDatabase(crossSalesOrder);
            crossSalesOrder.Status = (int)DCSCrossSalesOrderStatus.生效;
            crossSalesOrder.FinisherId = userInfo.Id;
            crossSalesOrder.Finisher = userInfo.Name;
            crossSalesOrder.FinisherTime = DateTime.Now;
            this.UpdateCrossSalesOrderValidate(crossSalesOrder);
            var abandDetails = dbcrossSalesOrder.CrossSalesOrderDetails.ToArray();
            //新增最新的完善信息
            var details = crossSalesOrder.CrossSalesOrderDetails.ToArray();

            foreach(var abandDetail in abandDetails) {
                var crossDetail = new CrossSalesOrderDetail {
                    CrossSalesOrderId = abandDetail.CrossSalesOrderId,
                    SparePartId = abandDetail.SparePartId,
                    SparePartCode = abandDetail.SparePartCode,                    
                    SparePartName=abandDetail.SparePartName,
                    OrderedQuantity = abandDetail.OrderedQuantity,
                    ABCStrategy = abandDetail.ABCStrategy,
                    CenterPrice = abandDetail.CenterPrice,
                    SalesPrice = abandDetail.SalesPrice,
                    RetailGuidePrice = abandDetail.RetailGuidePrice,
                    PriceTypeName = abandDetail.PriceTypeName,
                    Vin = abandDetail.Vin,
                    Explain = abandDetail.Explain,
                    Status = (int)DcsBaseDataStatus.作废
                };                
                crossSalesOrder.CrossSalesOrderDetails.Add(crossDetail);
                InsertToDatabase(crossDetail);
            }
            //生成销售订单
            var detailForSale = crossSalesOrder.CrossSalesOrderDetails.Where(t => t.Status == (int)DcsBaseDataStatus.有效).ToList();
            this.生成销售订单(crossSalesOrder, detailForSale);
            UpdateToDatabase(crossSalesOrder);
        }
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:34
        //原分布类的函数全部转移到Ach     
         [Update(UsingCustomMethod = true)]
        public void 新增跨区域销售单(CrossSalesOrder crossSalesOrder) {
            new CrossSalesOrderAch(this).新增跨区域销售单(crossSalesOrder);
        }
         [Update(UsingCustomMethod = true)]
         public void 修改跨区域销售单(CrossSalesOrder crossSalesOrder) {
             new CrossSalesOrderAch(this).修改跨区域销售单(crossSalesOrder);
         }              
        [Update(UsingCustomMethod = true)]
        public void 提交跨区域销售单(CrossSalesOrder crossSalesOrder) {
            new CrossSalesOrderAch(this).提交跨区域销售单(crossSalesOrder);
        }
        [Update(UsingCustomMethod = true)]
        public void 作废跨区域销售单(CrossSalesOrder crossSalesOrder) {
            new CrossSalesOrderAch(this).作废跨区域销售单(crossSalesOrder);
        }
        [Update(UsingCustomMethod = true)]
        public void 终止跨区域销售单(CrossSalesOrder crossSalesOrder) {
            new CrossSalesOrderAch(this).终止跨区域销售单(crossSalesOrder);
        }
        [Update(UsingCustomMethod = true)]
        public void 驳回跨区域销售单(CrossSalesOrder crossSalesOrder) {
            new CrossSalesOrderAch(this).驳回跨区域销售单(crossSalesOrder);
        }
        [Update(UsingCustomMethod = true)]
        public void 初审跨区域销售单(CrossSalesOrder crossSalesOrder) {
            new CrossSalesOrderAch(this).初审跨区域销售单(crossSalesOrder);
        }
        [Update(UsingCustomMethod = true)]
        public void 审核跨区域销售单(CrossSalesOrder crossSalesOrder) {
            new CrossSalesOrderAch(this).审核跨区域销售单(crossSalesOrder);
        }
        [Update(UsingCustomMethod = true)]
        public void 审批跨区域销售单(CrossSalesOrder crossSalesOrder) {
            new CrossSalesOrderAch(this).审批跨区域销售单(crossSalesOrder);
        }
        [Update(UsingCustomMethod = true)]
        public void 高级审核跨区域销售单(CrossSalesOrder crossSalesOrder) {
            new CrossSalesOrderAch(this).高级审核跨区域销售单(crossSalesOrder);
        }
        [Update(UsingCustomMethod = true)]
        public void 完善资料(CrossSalesOrder crossSalesOrder) {
            new CrossSalesOrderAch(this).完善资料(crossSalesOrder);
        }
    }

}
