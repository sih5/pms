﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class CustomerDirectSpareListsAch : DcsSerivceAchieveBase {
        public CustomerDirectSpareListsAch(DcsDomainService domainService) : base(domainService) {
        }

        public void 作废客户直供配件清单维护(CustomerDirectSpareList customerDirectSpareList) {
            var dbCustomerDirectSpareList = ObjectContext.CustomerDirectSpareLists.Where(r => r.Id == customerDirectSpareList.Id && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbCustomerDirectSpareList == null)
                throw new ValidationException(ErrorStrings.CustomerDirectSpareList_Validation1);
            UpdateToDatabase(dbCustomerDirectSpareList);
            dbCustomerDirectSpareList.Status = (int)DcsBaseDataStatus.作废;
            new CustomerDirectSpareListAch(this.DomainService).UpdateCustomerDirectSpareListValidate(dbCustomerDirectSpareList);
        }
        public void 作废客户直供配件清单维护2(VirtualCustomerDirectSpareList customerDirectSpareList)
        {
            var dbCustomerDirectSpareList = ObjectContext.CustomerDirectSpareLists.Where(r => r.Id == customerDirectSpareList.Id && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if (dbCustomerDirectSpareList == null)
                throw new ValidationException(ErrorStrings.CustomerDirectSpareList_Validation1);
            UpdateToDatabase(dbCustomerDirectSpareList);
            dbCustomerDirectSpareList.Status = (int)DcsBaseDataStatus.作废;
            new CustomerDirectSpareListAch(this.DomainService).UpdateCustomerDirectSpareListValidate(dbCustomerDirectSpareList);
        }
        public void 新增客户直供配件清单维护(CustomerDirectSpareList customerDirectSpareList) {
            var currentCustomerDirectSpareList = this.ObjectContext.CustomerDirectSpareLists.FirstOrDefault(r => r.PartsSalesCategoryId == customerDirectSpareList.PartsSalesCategoryId && r.CustomerId == customerDirectSpareList.CustomerId && r.SparePartId == customerDirectSpareList.SparePartId);
            if(currentCustomerDirectSpareList != null)
                this.DeleteFromDatabase(currentCustomerDirectSpareList);
            this.InsertToDatabase(customerDirectSpareList);
            new CustomerDirectSpareListAch(this.DomainService).InsertCustomerDirectSpareList(customerDirectSpareList);
            this.ObjectContext.SaveChanges();
        }
        public IQueryable<VirtualCustomerDirectSpareList> GetVirtualCustomerDirectSpareList(bool? isPrimarys)
        {
            var result = from a in ObjectContext.CustomerDirectSpareLists
                         join b in ObjectContext.PartsSupplierRelations on new { PartId = a.SparePartId, a.PartsSalesCategoryId, Status = (int)DcsBaseDataStatus .有效} equals new { b.PartId, b.PartsSalesCategoryId ,b.Status} into res
                         from bb in res.DefaultIfEmpty()
                         join c in ObjectContext.PartsSuppliers on new { Id=bb.SupplierId, Status = (int)DcsMasterDataStatus.有效 } equals new { c.Id, Status = c.Status.Value } into cc
                         from ps in cc.DefaultIfEmpty()                        
                         select new VirtualCustomerDirectSpareList
                         {
                             Id = a.Id,
                             PartsSalesCategoryId = a.PartsSalesCategoryId,
                             PartsSalesCategoryName = a.PartsSalesCategoryName,
                             CustomerId = a.CustomerId,
                             CustomerCode = a.CustomerCode,
                             CustomerName = a.CustomerName,
                             SparePartId = a.SparePartId,
                             SparePartCode = a.SparePartCode,
                             SparePartName = a.SparePartName,
                             MeasureUnit = a.MeasureUnit,
                             IfDirectProvision = a.IfDirectProvision,
                             Status = a.Status,
                             CreatorId = a.CreatorId,
                             CreatorName = a.CreatorName,
                             CreateTime = a.CreateTime,
                             ModifierId = a.ModifierId,
                             ModifierName = a.ModifierName,
                             ModifyTime = a.ModifyTime,
                             PartsSupplierCode = ps.Code==null? " ":ps.Code,
                             PartsSupplierName = ps.Name,
                             IsPrimary = bb.IsPrimary
                         };
            if (isPrimarys.HasValue && isPrimarys==true)
            {
                result = result.Where(r => r.IsPrimary == true);
            }
            else if (isPrimarys.HasValue && !isPrimarys==true)
            {
                result = result.Where(r => r.IsPrimary == false);
            }
                return result.OrderBy( r => r.Id);
        }
        public IEnumerable<VirtualCustomerDirectSpareList> 导出直供清单(bool? isPrimarys, int? partsSalesCategoryId, string customerCode, string customerName, string sparePartCode, string sparePartName, int? status) {
            var result = from a in ObjectContext.CustomerDirectSpareLists
                         join b in ObjectContext.PartsSupplierRelations on new {
                             PartId = a.SparePartId,
                             a.PartsSalesCategoryId,
                             Status = (int)DcsBaseDataStatus.有效
                         } equals new {
                             b.PartId,
                             b.PartsSalesCategoryId,
                             b.Status
                         } into res
                         from bb in res.DefaultIfEmpty()
                         join c in ObjectContext.PartsSuppliers on new {
                             Id = bb.SupplierId,
                             Status = (int)DcsMasterDataStatus.有效
                         } equals new {
                             c.Id,
                             Status = c.Status.Value
                         } into cc
                         from ps in cc.DefaultIfEmpty()
                         select new VirtualCustomerDirectSpareList {
                             Id = a.Id,
                             PartsSalesCategoryId = a.PartsSalesCategoryId,
                             PartsSalesCategoryName = a.PartsSalesCategoryName,
                             CustomerId = a.CustomerId,
                             CustomerCode = a.CustomerCode,
                             CustomerName = a.CustomerName,
                             SparePartId = a.SparePartId,
                             SparePartCode = a.SparePartCode,
                             SparePartName = a.SparePartName,
                             MeasureUnit = a.MeasureUnit,
                             IfDirectProvision = a.IfDirectProvision,
                             Status = a.Status,
                             CreatorId = a.CreatorId,
                             CreatorName = a.CreatorName,
                             CreateTime = a.CreateTime,
                             ModifierId = a.ModifierId,
                             ModifierName = a.ModifierName,
                             ModifyTime = a.ModifyTime,
                             PartsSupplierCode = ps.Code == null ? " " : ps.Code,
                             PartsSupplierName = ps.Name,
                             IsPrimary = bb.IsPrimary
                         };
            if(isPrimarys.HasValue && isPrimarys == true) {
                result = result.Where(r => r.IsPrimary == true);
            } else if(isPrimarys.HasValue && !isPrimarys == true) {
                result = result.Where(r => r.IsPrimary == false);
            }
            if(partsSalesCategoryId.HasValue) {
                result = result.Where(r => r.PartsSalesCategoryId == partsSalesCategoryId);
            }
            if(!string.IsNullOrEmpty(customerCode)) {
                result = result.Where(r => r.CustomerCode.Contains(customerCode));
            }
            if(!string.IsNullOrEmpty(customerName)) {
                result = result.Where(r => r.CustomerName.Contains(customerName));
            }
            if(!string.IsNullOrEmpty(sparePartCode)) {
                result = result.Where(r => r.SparePartCode.Contains(sparePartCode));
            }
            if(!string.IsNullOrEmpty(sparePartName)) {
                result = result.Where(r => r.SparePartName.Contains(sparePartName));
            }
            if(status.HasValue) {
                result = result.Where(r => r.Status == status);
            }
            return result.OrderBy(r => r.Id).ToArray();
        }
}

    partial class DcsDomainService {
        [Update(UsingCustomMethod = true)]
        public void 作废客户直供配件清单维护(CustomerDirectSpareList customerDirectSpareList) {
            new CustomerDirectSpareListsAch(this).作废客户直供配件清单维护(customerDirectSpareList);
        }

        [Invoke]
        public void 新增客户直供配件清单维护(CustomerDirectSpareList customerDirectSpareList) {
            new CustomerDirectSpareListsAch(this).新增客户直供配件清单维护(customerDirectSpareList);
        }
        public IQueryable<VirtualCustomerDirectSpareList> GetVirtualCustomerDirectSpareList(bool? isPrimarys)
        {
            return new CustomerDirectSpareListsAch(this).GetVirtualCustomerDirectSpareList(isPrimarys);
        }
        public void 作废客户直供配件清单维护2(VirtualCustomerDirectSpareList customerDirectSpareList)
        {
            new CustomerDirectSpareListsAch(this).作废客户直供配件清单维护2(customerDirectSpareList);
        }
        public IEnumerable<VirtualCustomerDirectSpareList> 导出直供清单(bool? isPrimarys, int? partsSalesCategoryId, string customerCode, string customerName, string sparePartCode, string sparePartName, int? status) {
            return new CustomerDirectSpareListsAch(this).导出直供清单(isPrimarys, partsSalesCategoryId, customerCode, customerName, sparePartCode, sparePartName, status);
        }
    }
}
