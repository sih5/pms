﻿using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class CustomerInformationAch : DcsSerivceAchieveBase {
        public CustomerInformationAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 修改客户信息(CustomerInformation customerInformation) {
            var dbcustomerInformation = ObjectContext.CustomerInformations.Where(r => r.Id == customerInformation.Id && r.Status == (int)DcsMasterDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbcustomerInformation == null)
                throw new ValidationException(ErrorStrings.CustomerInformation_Validation1);
            UpdateToDatabase(customerInformation);
            customerInformation.Status = (int)DcsMasterDataStatus.有效;
            this.UpdateCustomerInformationValidate(customerInformation);
        }

        public void 停用客户信息(CustomerInformation customerInformation) {
            var dbcustomerInformation = ObjectContext.CustomerInformations.Where(r => r.Id == customerInformation.Id && r.Status == (int)DcsMasterDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbcustomerInformation == null)
                throw new ValidationException(ErrorStrings.CustomerInformation_Validation2);
            UpdateToDatabase(customerInformation);
            customerInformation.Status = (int)DcsMasterDataStatus.停用;
            this.UpdateCustomerInformationValidate(customerInformation);
        }

        public void 作废客户信息(CustomerInformation customerInformation) {
            var dbcustomerInformation = ObjectContext.CustomerInformations.Where(r => r.Id == customerInformation.Id && r.Status == (int)DcsMasterDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbcustomerInformation == null)
                throw new ValidationException(ErrorStrings.CustomerInformation_Validation3);
            UpdateToDatabase(customerInformation);
            customerInformation.Status = (int)DcsMasterDataStatus.作废;
            this.UpdateCustomerInformationValidate(customerInformation);
        }

        public void 恢复客户信息(CustomerInformation customerInformation) {
            var dbcustomerInformation = ObjectContext.CustomerInformations.Where(r => r.Id == customerInformation.Id && r.Status == (int)DcsMasterDataStatus.停用).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbcustomerInformation == null)
                throw new ValidationException(ErrorStrings.CustomerInformation_Validation4);
            UpdateToDatabase(customerInformation);
            customerInformation.Status = (int)DcsMasterDataStatus.有效;
            this.UpdateCustomerInformationValidate(customerInformation);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:11:57
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 修改客户信息(CustomerInformation customerInformation) {
            new CustomerInformationAch(this).修改客户信息(customerInformation);
        }
        
        [Update(UsingCustomMethod = true)]
        public void 停用客户信息(CustomerInformation customerInformation) {
            new CustomerInformationAch(this).停用客户信息(customerInformation);
        }
        
        [Update(UsingCustomMethod = true)]
        public void 作废客户信息(CustomerInformation customerInformation) {
            new CustomerInformationAch(this).作废客户信息(customerInformation);
        }
        
        [Update(UsingCustomMethod = true)]
        public void 恢复客户信息(CustomerInformation customerInformation) {
            new CustomerInformationAch(this).恢复客户信息(customerInformation);
        }
    }
}
