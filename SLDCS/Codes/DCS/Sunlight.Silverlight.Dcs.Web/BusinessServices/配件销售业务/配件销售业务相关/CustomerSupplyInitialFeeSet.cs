﻿using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class CustomerSupplyInitialFeeSetsAch : DcsSerivceAchieveBase {
        public CustomerSupplyInitialFeeSetsAch(DcsDomainService domainService)
            : base(domainService) {
        }

        public void 作废客户与供应商起订金额设置(CustomerSupplyInitialFeeSet customerSupplyInitialFeeSet) {
            var dbCustomerSupplyInitialFeeSet = ObjectContext.CustomerSupplyInitialFeeSets.Where(r => r.Id == customerSupplyInitialFeeSet.Id && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbCustomerSupplyInitialFeeSet == null)
                throw new ValidationException(ErrorStrings.CustomerSupplyInitialFeeSet_Validation1);
            UpdateToDatabase(dbCustomerSupplyInitialFeeSet);
            dbCustomerSupplyInitialFeeSet.Status = (int)DcsBaseDataStatus.作废;
            new CustomerSupplyInitialFeeSetAch(this.DomainService).UpdateCustomerSupplyInitialFeeSetValidate(dbCustomerSupplyInitialFeeSet);
        }

        public void 新增客户与供应商起订金额设置(CustomerSupplyInitialFeeSet customerSupplyInitialFeeSet) {
            var currentCustomerSupplyInitialFeeSet = this.ObjectContext.CustomerSupplyInitialFeeSets.FirstOrDefault(r => r.PartsSalesCategoryId == customerSupplyInitialFeeSet.PartsSalesCategoryId && r.CustomerId == customerSupplyInitialFeeSet.CustomerId && r.SupplierId == customerSupplyInitialFeeSet.SupplierId);
            if(currentCustomerSupplyInitialFeeSet != null)
                this.DeleteFromDatabase(currentCustomerSupplyInitialFeeSet);
            this.InsertToDatabase(customerSupplyInitialFeeSet);
            new CustomerSupplyInitialFeeSetAch(this.DomainService).InsertCustomerSupplyInitialFeeSet(customerSupplyInitialFeeSet);
            this.ObjectContext.SaveChanges();
        }
    }

    partial class DcsDomainService {
        [Update(UsingCustomMethod = true)]
        public void 作废客户与供应商起订金额设置(CustomerSupplyInitialFeeSet customerSupplyInitialFeeSet) {
            new CustomerSupplyInitialFeeSetsAch(this).作废客户与供应商起订金额设置(customerSupplyInitialFeeSet);
        }

        [Invoke]
        public void 新增客户与供应商起订金额设置(CustomerSupplyInitialFeeSet customerSupplyInitialFeeSet) {
            new CustomerSupplyInitialFeeSetsAch(this).新增客户与供应商起订金额设置(customerSupplyInitialFeeSet);
        }
    }
}
