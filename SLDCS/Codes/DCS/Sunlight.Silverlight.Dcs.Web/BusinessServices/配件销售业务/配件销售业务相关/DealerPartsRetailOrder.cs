﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class DealerPartsRetailOrderAch : DcsSerivceAchieveBase {
        public DealerPartsRetailOrderAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 作废服务站配件零售订单(DealerPartsRetailOrder dealerPartsRetailOrder) {
            var dbDealerPartsRetailOrder = ObjectContext.DealerPartsRetailOrders.Where(r => r.Id == dealerPartsRetailOrder.Id && r.Status == (int)DcsWorkflowOfSimpleApprovalStatus.新建).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbDealerPartsRetailOrder == null)
                throw new ValidationException(ErrorStrings.DealerPartsRetailOrder_Validation1);
            UpdateToDatabase(dealerPartsRetailOrder);
            dealerPartsRetailOrder.Status = (int)DcsWorkflowOfSimpleApprovalStatus.作废;
            var userInfo = Utils.GetCurrentUserInfo();
            dealerPartsRetailOrder.AbandonerId = userInfo.Id;
            dealerPartsRetailOrder.AbandonerName = userInfo.Name;
            dealerPartsRetailOrder.AbandonTime = DateTime.Now;
            this.UpdateDealerPartsRetailOrderValidate(dealerPartsRetailOrder);
        }

        //验证标签码 
        private void ValidateDealerTrace(DealerPartsRetailOrder dealerPartsRetailOrder) {
            var inTraces = ObjectContext.AccurateTraces.Where(t => t.CompanyId == dealerPartsRetailOrder.DealerId && t.Type == (int)DCSAccurateTraceType.入库 && (t.OutQty == null || t.InQty != t.OutQty) && t.Status == (int)DCSAccurateTraceStatus.有效).ToArray();
            foreach(var item in dealerPartsRetailOrder.DealerRetailOrderDetails) {
                if(item.TraceProperty.HasValue && !string.IsNullOrEmpty(item.SIHLabelCode)) {
                    var traceQty = 0;

                    var traces = item.SIHLabelCode.Split(new Char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                    foreach(var code in traces) {
                        var inTrace = inTraces.Where(t => t.BoxCode == code).ToArray();
                        if(code.EndsWith("J"))
                            inTrace = inTraces.Where(t => t.SIHLabelCode == code).ToArray();
                        if(inTrace.Count() == 0) {
                            throw new ValidationException(code + "标签码不存在,或已出库完成");
                        }
                        foreach(var trace in inTrace) {
                            if(trace.PartId != item.PartsId) {
                                throw new ValidationException(code + "不是配件" + item.PartsCode + "的标签码");
                            }
                            if(item.TraceProperty.Value == (int)DCSTraceProperty.精确追溯) {
                                traceQty++;
                            }
                        }
                    }
                    //if(item.TraceProperty.Value == (int)DCSTraceProperty.精确追溯 && traceQty > 0 && traceQty > item.Quantity) {
                    //    throw new ValidationException(item.PartsCode + "配件的标签码数量大于销售数量数量");
                    //}
                }
            }
        } 
        public void 生成服务站配件零售订单(DealerPartsRetailOrder dealerPartsRetailOrder) {
            var dealerRetailOrderDetails = dealerPartsRetailOrder.DealerRetailOrderDetails.ToArray();
            int tempSubDealerId;
            if(dealerPartsRetailOrder.RetailOrderType == (int)DcsDealerPartsRetailOrderRetailOrderType.二级站) {
                tempSubDealerId = -1;
            }else {
                if(!dealerPartsRetailOrder.SubDealerId.HasValue) {
                    throw new ValidationException("二级站Id为空");
                }
                tempSubDealerId = dealerPartsRetailOrder.SubDealerId.Value;
            }
            //检验标签码
            this.ValidateDealerTrace(dealerPartsRetailOrder);
            var dalerPartsStocks = from a in ObjectContext.DealerPartsStocks.Where(r => r.DealerId == dealerPartsRetailOrder.DealerId && r.SalesCategoryId == dealerPartsRetailOrder.PartsSalesCategoryId && r.SubDealerId == tempSubDealerId)
                                   join b in ObjectContext.PartsRetailGuidePrices.Where(r => r.PartsSalesCategoryId == dealerPartsRetailOrder.PartsSalesCategoryId && r.Status != (int)DcsBaseDataStatus.作废) on new {
                                       sparePartId = a.SparePartId,
                                       partsSalesCategoryId = a.SalesCategoryId
                                   }equals new {
                                       sparePartId = b.SparePartId,
                                       partsSalesCategoryId = b.PartsSalesCategoryId
                                   }into tempTable
                                   from t in tempTable.DefaultIfEmpty()
                                   join c in ObjectContext.PartsSalesCategories.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on a.SalesCategoryId equals c.Id
                                   join d in ObjectContext.SpareParts on t.SparePartId equals d.Id
                                   select a;
            for(var i = 0; i < dealerRetailOrderDetails.Count(); i++) {
                var tempdealerRetailOrderDetailsPartsId = dealerRetailOrderDetails[i].PartsId;
                var dalerPartsStock = dalerPartsStocks.FirstOrDefault(r => r.SparePartId == tempdealerRetailOrderDetailsPartsId);
                if(dalerPartsStock == null)
                    throw new ValidationException(string.Format(ErrorStrings.DealerPartsRetailOrder_Validation4, dealerRetailOrderDetails[i].PartsCode));
                if(dalerPartsStock.Quantity < dealerRetailOrderDetails[i].Quantity)
                    throw new ValidationException(string.Format(ErrorStrings.DealerPartsRetailOrder_Validation5, dealerRetailOrderDetails[i].PartsCode));
            }
            InsertToDatabase(dealerPartsRetailOrder);
            this.InsertDealerPartsRetailOrderValidate(dealerPartsRetailOrder);
        }

        public void 修改服务站配件零售订单(DealerPartsRetailOrder dealerPartsRetailOrder) {
            var dbDealerPartsRetailOrder = ObjectContext.DealerPartsRetailOrders.Where(r => r.Id == dealerPartsRetailOrder.Id && r.Status != (int)DcsWorkflowOfSimpleApprovalStatus.作废).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbDealerPartsRetailOrder == null)
                throw new ValidationException(ErrorStrings.DealerPartsRetailOrder_Validation7);
            var dealerRetailOrderDetails = dealerPartsRetailOrder.DealerRetailOrderDetails.ToArray();
            var partsIds = dealerPartsRetailOrder.DealerRetailOrderDetails.Select(r => r.PartsId);
            int tempSubDealerId;
            if(dealerPartsRetailOrder.RetailOrderType == (int)DcsDealerPartsRetailOrderRetailOrderType.二级站) {
                tempSubDealerId = -1;
            }else {
                if(!dealerPartsRetailOrder.SubDealerId.HasValue) {
                    throw new ValidationException("二级站Id为空");
                }
                tempSubDealerId = dealerPartsRetailOrder.SubDealerId.Value;
            }
            //检验标签码
            this.ValidateDealerTrace(dealerPartsRetailOrder);
            var dealerPartsStocks = ObjectContext.DealerPartsStocks.Where(r => r.DealerId == dealerPartsRetailOrder.DealerId && r.SalesCategoryId == dealerPartsRetailOrder.PartsSalesCategoryId && partsIds.Contains(r.SparePartId) && r.SubDealerId == tempSubDealerId).ToArray();
            var partsRetailGuidePrices = ObjectContext.PartsRetailGuidePrices.Where(r => r.PartsSalesCategoryId == dealerPartsRetailOrder.PartsSalesCategoryId && partsIds.Contains(r.SparePartId) && r.Status != (int)DcsBaseDataStatus.作废).ToArray();
            for(var i = 0; i < dealerRetailOrderDetails.Count(); i++) {
                var dalerPartsStock = dealerPartsStocks.FirstOrDefault(r => r.SparePartId == dealerRetailOrderDetails[i].PartsId);
                if(dalerPartsStock == null)
                    throw new ValidationException(string.Format(ErrorStrings.DealerPartsRetailOrder_Validation4, dealerRetailOrderDetails[i].PartsCode));
                if(dalerPartsStock.Quantity < dealerRetailOrderDetails[i].Quantity)
                    throw new ValidationException(string.Format(ErrorStrings.DealerPartsRetailOrder_Validation5, dealerRetailOrderDetails[i].PartsCode));
                var partsRetailGuidePrice = partsRetailGuidePrices.FirstOrDefault(r => r.SparePartId == dealerRetailOrderDetails[i].PartsId);
                if(partsRetailGuidePrice == null)
                    throw new ValidationException(string.Format(ErrorStrings.DealerPartsRetailOrder_Validation6, dealerRetailOrderDetails[i].PartsCode));
                //dealerRetailOrderDetails[0].Price = partsRetailGuidePrice.RetailGuidePrice;
            }
            UpdateToDatabase(dealerPartsRetailOrder);
            this.UpdateDealerPartsRetailOrderValidate(dealerPartsRetailOrder);
        }

        public void 审批服务站配件零售订单(DealerPartsRetailOrder dealerPartsRetailOrder) {
            var dbDealerPartsRetailOrder = ObjectContext.DealerPartsRetailOrders.Where(r => r.Id == dealerPartsRetailOrder.Id && r.Status == (int)DcsWorkflowOfSimpleApprovalStatus.新建).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbDealerPartsRetailOrder == null)
                throw new ValidationException(ErrorStrings.DealerPartsRetailOrder_Validation3);
            var userInfo = Utils.GetCurrentUserInfo();
            dealerPartsRetailOrder.ApproverId = userInfo.Id;
            dealerPartsRetailOrder.ApproverName = userInfo.Name;
            dealerPartsRetailOrder.ApproveTime = DateTime.Now;
            dealerPartsRetailOrder.Status = (int)DcsWorkflowOfSimpleApprovalStatus.已审核;
            UpdateToDatabase(dealerPartsRetailOrder);

            this.UpdateDealerPartsRetailOrderValidate(dealerPartsRetailOrder);
            var partsIds = dealerPartsRetailOrder.DealerRetailOrderDetails.Select(r => r.PartsId).ToArray();
            var dealerPartsStocks = ObjectContext.DealerPartsStocks.Where(r => r.DealerId == dealerPartsRetailOrder.DealerId && r.SalesCategoryId == dealerPartsRetailOrder.PartsSalesCategoryId && partsIds.Contains(r.SparePartId) && r.SubDealerId == -1);
            foreach(var dealerRetailOrderDetail in dealerPartsRetailOrder.DealerRetailOrderDetails) {
                var dealerPartsStock = dealerPartsStocks.FirstOrDefault(r => r.SparePartId == dealerRetailOrderDetail.PartsId);
                //var dbPartDeleaveInformation = ObjectContext.PartDeleaveInformations.SingleOrDefault(c => c.OldPartId == dealerRetailOrderDetail.PartsId && c.PartsSalesCategoryId == dealerPartsRetailOrder.PartsSalesCategoryId && c.BranchId == dealerPartsRetailOrder.BranchId && c.Status == (int)DcsBaseDataStatus.有效);
                if(dealerPartsStock == null)
                    throw new ValidationException(string.Format(ErrorStrings.DealerPartsRetailOrder_Validation2, dealerRetailOrderDetail.PartsCode));
                //if(dbPartDeleaveInformation != null) {
                //    if(dealerPartsStock.Quantity < dealerRetailOrderDetail.Quantity * dbPartDeleaveInformation.DeleaveAmount) {
                //        throw new ValidationException(ErrorStrings.DealerPartsRetailOrder_Validation9);
                //    }
                //    dealerPartsStock.Quantity -= dealerRetailOrderDetail.Quantity * dbPartDeleaveInformation.DeleaveAmount;
                //}else {
                if(dealerPartsStock.Quantity < dealerRetailOrderDetail.Quantity) {
                    throw new ValidationException(ErrorStrings.DealerPartsRetailOrder_Validation9);
                }
                dealerPartsStock.Quantity -= dealerRetailOrderDetail.Quantity;
                //}
                DomainService.UpdateDealerPartsStock(dealerPartsStock);
            }
            if(dealerPartsRetailOrder.RetailOrderType == (int)DcsDealerPartsRetailOrderRetailOrderType.二级站) {
                var dealerPartsStockSubDealers = ObjectContext.DealerPartsStocks.Where(r => r.DealerId == dealerPartsRetailOrder.DealerId && r.SalesCategoryId == dealerPartsRetailOrder.PartsSalesCategoryId && r.SubDealerId == dealerPartsRetailOrder.SubDealerId && partsIds.Contains(r.SparePartId));
                foreach(var dealerRetailOrderDetail in dealerPartsRetailOrder.DealerRetailOrderDetails) {
                    var dealerPartsStockSubDealer = dealerPartsStockSubDealers.FirstOrDefault(r => r.SparePartId == dealerRetailOrderDetail.PartsId);
                    var dbPartDeleaveInformation = ObjectContext.PartDeleaveInformations.SingleOrDefault(c => c.OldPartId == dealerRetailOrderDetail.PartsId && c.PartsSalesCategoryId == dealerPartsRetailOrder.PartsSalesCategoryId && c.BranchId == dealerPartsRetailOrder.BranchId && c.Status == (int)DcsBaseDataStatus.有效);
                    if(dealerPartsStockSubDealer != null) {
                        if(dbPartDeleaveInformation != null) {
                            dealerPartsStockSubDealer.Quantity += dealerRetailOrderDetail.Quantity * dbPartDeleaveInformation.DeleaveAmount;
                        }else {
                            dealerPartsStockSubDealer.Quantity += dealerRetailOrderDetail.Quantity;
                        }
                        DomainService.UpdateDealerPartsStock(dealerPartsStockSubDealer);
                    }else {
                        var newDealerPartsStock = new DealerPartsStock {
                            DealerId = dealerPartsRetailOrder.DealerId,
                            DealerCode = dealerPartsRetailOrder.DealerCode,
                            DealerName = dealerPartsRetailOrder.DealerName,
                            SubDealerId = dealerPartsRetailOrder.SubDealerId,
                            BranchId = dealerPartsRetailOrder.BranchId,
                            SalesCategoryId = dealerPartsRetailOrder.PartsSalesCategoryId,
                            SalesCategoryName = dealerPartsRetailOrder.SalesCategoryName,
                            SparePartId = dealerRetailOrderDetail.PartsId,
                            SparePartCode = dealerRetailOrderDetail.PartsCode,
                            Quantity = dbPartDeleaveInformation != null ? dealerRetailOrderDetail.Quantity * dbPartDeleaveInformation.DeleaveAmount : dealerRetailOrderDetail.Quantity
                        };
                        InsertToDatabase(newDealerPartsStock);
                        new DealerPartsStockAch(this.DomainService).InsertDealerPartsStockValidate(newDealerPartsStock);
                    }
                }
            }
        }

        public void 服务站审批服务站配件零售订单(DealerPartsRetailOrder dealerPartsRetailOrder) {
            var dbDealerPartsRetailOrder = ObjectContext.DealerPartsRetailOrders.Where(r => r.Id == dealerPartsRetailOrder.Id && r.Status == (int)DcsWorkflowOfSimpleApprovalStatus.新建).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbDealerPartsRetailOrder == null)
                throw new ValidationException(ErrorStrings.DealerPartsRetailOrder_Validation3);
            var partsSalesCategory = ObjectContext.PartsSalesCategories.SingleOrDefault(r => r.Id == dealerPartsRetailOrder.PartsSalesCategoryId && r.Status == (int)DcsBaseDataStatus.有效);
            if(partsSalesCategory == null) {
                throw new ValidationException("品牌不存在");
            }
            UpdateToDatabase(dealerPartsRetailOrder);
            var userInfo = Utils.GetCurrentUserInfo();
            dealerPartsRetailOrder.ApproverId = userInfo.Id;
            dealerPartsRetailOrder.ApproverName = userInfo.Name;
            dealerPartsRetailOrder.ApproveTime = DateTime.Now;
            dealerPartsRetailOrder.Status = (int)DcsWorkflowOfSimpleApprovalStatus.已审核;
            this.UpdateDealerPartsRetailOrderValidate(dealerPartsRetailOrder);
            var partsIds = dealerPartsRetailOrder.DealerRetailOrderDetails.Select(r => r.PartsId).ToArray();
            var dealerPartsStocks = ObjectContext.DealerPartsStocks.Where(r => r.DealerId == dealerPartsRetailOrder.DealerId && r.SalesCategoryId == dealerPartsRetailOrder.PartsSalesCategoryId && partsIds.Contains(r.SparePartId) && r.SubDealerId == -1);
            foreach(var dealerRetailOrderDetail in dealerPartsRetailOrder.DealerRetailOrderDetails) {
                var dealerPartsStock = dealerPartsStocks.FirstOrDefault(r => r.SparePartId == dealerRetailOrderDetail.PartsId);
                //var dbPartDeleaveInformation = ObjectContext.PartDeleaveInformations.SingleOrDefault(c => c.OldPartId == dealerRetailOrderDetail.PartsId && c.PartsSalesCategoryId == dealerPartsRetailOrder.PartsSalesCategoryId && c.BranchId == dealerPartsRetailOrder.BranchId && c.Status == (int)DcsBaseDataStatus.有效);
                if(dealerPartsStock == null)
                    throw new ValidationException(string.Format(ErrorStrings.DealerPartsRetailOrder_Validation2, dealerRetailOrderDetail.PartsCode));
                //if(dbPartDeleaveInformation != null) {
                //    if(dealerPartsStock.Quantity < dealerRetailOrderDetail.Quantity * dbPartDeleaveInformation.DeleaveAmount) {
                //        throw new ValidationException(ErrorStrings.DealerPartsRetailOrder_Validation9);
                //    }
                //    dealerPartsStock.Quantity -= dealerRetailOrderDetail.Quantity * dbPartDeleaveInformation.DeleaveAmount;
                //}else {
                if(dealerPartsStock.Quantity < dealerRetailOrderDetail.Quantity) {
                    throw new ValidationException(ErrorStrings.DealerPartsRetailOrder_Validation9);
                }
                dealerPartsStock.Quantity -= dealerRetailOrderDetail.Quantity;
                // }
            }
            if(dealerPartsRetailOrder.RetailOrderType == (int)DcsDealerPartsRetailOrderRetailOrderType.二级站) {
                var dealerPartsStockSubDealers = ObjectContext.DealerPartsStocks.Where(r => r.DealerId == dealerPartsRetailOrder.DealerId && r.SalesCategoryId == dealerPartsRetailOrder.PartsSalesCategoryId && r.SubDealerId == dealerPartsRetailOrder.SubDealerId && partsIds.Contains(r.SparePartId));
                foreach(var dealerRetailOrderDetail in dealerPartsRetailOrder.DealerRetailOrderDetails) {
                    var dealerPartsStockSubDealer = dealerPartsStockSubDealers.FirstOrDefault(r => r.SparePartId == dealerRetailOrderDetail.PartsId);
                    var dbPartDeleaveInformation = ObjectContext.PartDeleaveInformations.SingleOrDefault(c => c.OldPartId == dealerRetailOrderDetail.PartsId && c.PartsSalesCategoryId == dealerPartsRetailOrder.PartsSalesCategoryId && c.BranchId == dealerPartsRetailOrder.BranchId && c.Status == (int)DcsBaseDataStatus.有效);
                    if(dealerPartsStockSubDealer != null) {
                        if(dbPartDeleaveInformation != null) {
                            dealerPartsStockSubDealer.Quantity += dealerRetailOrderDetail.Quantity * dbPartDeleaveInformation.DeleaveAmount;
                        }else {
                            dealerPartsStockSubDealer.Quantity += dealerRetailOrderDetail.Quantity;
                        }
                        DomainService.UpdateDealerPartsStock(dealerPartsStockSubDealer);
                    }else {
                        var newDealerPartsStock = new DealerPartsStock {
                            DealerId = dealerPartsRetailOrder.DealerId,
                            DealerCode = dealerPartsRetailOrder.DealerCode,
                            DealerName = dealerPartsRetailOrder.DealerName,
                            SubDealerId = dealerPartsRetailOrder.SubDealerId,
                            BranchId = dealerPartsRetailOrder.BranchId,
                            SalesCategoryId = dealerPartsRetailOrder.PartsSalesCategoryId,
                            SalesCategoryName = dealerPartsRetailOrder.SalesCategoryName,
                            SparePartId = dealerRetailOrderDetail.PartsId,
                            SparePartCode = dealerRetailOrderDetail.PartsCode,
                            Quantity = dbPartDeleaveInformation != null ? dealerRetailOrderDetail.Quantity * dbPartDeleaveInformation.DeleaveAmount : dealerRetailOrderDetail.Quantity
                        };
                        InsertToDatabase(newDealerPartsStock);
                        new DealerPartsStockAch(this.DomainService).InsertDealerPartsStockValidate(newDealerPartsStock);
                    }
                }
            }
             //记录追溯信息 
            var inTraces = ObjectContext.AccurateTraces.Where(t => t.CompanyId == dealerPartsRetailOrder.DealerId && t.Type == (int)DCSAccurateTraceType.入库 && (t.OutQty == null || t.InQty != t.OutQty) && t.Status == (int)DCSAccurateTraceStatus.有效).ToArray();
            //出库
            var tracOldOut = ObjectContext.AccurateTraces.Where(t => t.Status == (int)DCSAccurateTraceStatus.有效 && t.CompanyId == dealerPartsRetailOrder.DealerId && t.Type == (int)DCSAccurateTraceType.出库 && t.SIHLabelCode != null).ToArray();
            foreach(var item in dealerPartsRetailOrder.DealerRetailOrderDetails) { 
                        if(item.TraceProperty.HasValue && !string.IsNullOrEmpty(item.SIHLabelCode)) { 
                            var traceQty = 0; 
                            var traces = item.SIHLabelCode.Split(new Char[] { ';' }, StringSplitOptions.RemoveEmptyEntries); 
                            foreach(var code in traces) {                                
                                var inTrace = inTraces.Where(t => t.BoxCode == code).ToArray(); 
                                if(code.EndsWith("J")) 
                                    inTrace = inTraces.Where(t => t.SIHLabelCode == code).ToArray(); 
                                if(inTrace.Count() == 0) { 
                                    throw new ValidationException(code + "标签码已出库完成"); 
                                } 
                                foreach(var trace in inTrace) {
                                    if(traceQty == item.Quantity) { 
                                        continue; 
                                    }
                                    var currentQty = 0; 
                                    if(item.TraceProperty.Value == (int)DCSTraceProperty.精确追溯) { 
                                        traceQty++; 
                                        currentQty = 1; 
                                    } else { 
                                        if(((trace.InQty - (trace.OutQty ?? 0))) > (item.Quantity - traceQty)) {
                                            traceQty = item.Quantity;
                                            currentQty = item.Quantity - traceQty; 
                                        } else { 
                                            traceQty = traceQty + (trace.InQty.Value - (trace.OutQty ?? 0)); 
                                            currentQty = (trace.InQty.Value - (trace.OutQty ?? 0)); 
                                        } 
                                    }                                    
                                    var outAccurateTrace = new AccurateTrace { 
                                        Type = (int)DCSAccurateTraceType.出库, 
                                        CompanyType =  (int)DCSAccurateTraceCompanyType.服务站,
                                        CompanyId = dealerPartsRetailOrder.DealerId,
                                        SourceBillId = dealerPartsRetailOrder.Id, 
                                        PartId = item.PartsId, 
                                        TraceCode = trace.TraceCode, 
                                        SIHLabelCode = trace.SIHLabelCode, 
                                        Status = (int)DCSAccurateTraceStatus.有效, 
                                        TraceProperty = trace.TraceProperty, 
                                        BoxCode = trace.BoxCode, 
                                        InQty = currentQty, 
                                        OutQty = currentQty 
                                    }; 
                                    new AccurateTraceAch(this.DomainService).ValidataAccurateTrace(outAccurateTrace); 
                                    trace.OutQty = trace.OutQty ?? 0 + currentQty; 
                                    trace.ModifierId = userInfo.Id; 
                                    trace.ModifierName = userInfo.Name; 
                                    trace.ModifyTime = DateTime.Now; 
                                    UpdateToDatabase(trace);
                                    if(item.TraceProperty.Value == (int)DCSTraceProperty.精确追溯) {
                                        var outTraceLs = tracOldOut.Where(t => t.SourceBillId != dealerPartsRetailOrder.Id && t.TraceCode == trace.TraceCode && t.Status == (int)DCSAccurateTraceStatus.有效).ToArray();
                                        foreach(var outTrace in outTraceLs) {
                                            outTrace.ModifierId = userInfo.Id;
                                            outTrace.ModifierName = userInfo.Name;
                                            outTrace.ModifyTime = DateTime.Now;
                                            outTrace.Status = (int)DCSAccurateTraceStatus.作废;
                                            UpdateToDatabase(outTrace);
                                        }
                                    } 
                                } 
                            } 
                        } 
                    }  
        }

        public void 审批二级站配件零售订单(DealerPartsRetailOrder dealerPartsRetailOrder) {
            var dbDealerPartsRetailOrder = ObjectContext.DealerPartsRetailOrders.Where(r => r.Id == dealerPartsRetailOrder.Id && r.Status == (int)DcsWorkflowOfSimpleApprovalStatus.新建).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbDealerPartsRetailOrder == null)
                throw new ValidationException(ErrorStrings.DealerPartsRetailOrder_Validation3);
            UpdateToDatabase(dealerPartsRetailOrder);
            dealerPartsRetailOrder.Status = (int)DcsWorkflowOfSimpleApprovalStatus.已审核;
            this.UpdateDealerPartsRetailOrderValidate(dealerPartsRetailOrder);
            var partsIds = dealerPartsRetailOrder.DealerRetailOrderDetails.Select(r => r.PartsId).ToArray();
            var dealerPartsStocks = ObjectContext.DealerPartsStocks.Where(r => r.DealerId == dealerPartsRetailOrder.DealerId && r.SalesCategoryId == dealerPartsRetailOrder.PartsSalesCategoryId && partsIds.Contains(r.SparePartId) && r.SubDealerId == dealerPartsRetailOrder.SubDealerId);
            foreach(var dealerRetailOrderDetail in dealerPartsRetailOrder.DealerRetailOrderDetails) {
                var dealerPartsStock = dealerPartsStocks.FirstOrDefault(r => r.SparePartId == dealerRetailOrderDetail.PartsId);
                //var dbPartDeleaveInformation = ObjectContext.PartDeleaveInformations.SingleOrDefault(c => c.OldPartId == dealerRetailOrderDetail.PartsId && c.PartsSalesCategoryId == dealerPartsRetailOrder.PartsSalesCategoryId && c.BranchId == dealerPartsRetailOrder.BranchId && c.Status == (int)DcsBaseDataStatus.有效);
                if(dealerPartsStock == null)
                    throw new ValidationException(string.Format(ErrorStrings.DealerPartsRetailOrder_Validation2, dealerRetailOrderDetail.PartsCode));
                //if(dbPartDeleaveInformation != null) {
                //    if(dealerPartsStock.Quantity < dealerRetailOrderDetail.Quantity * dbPartDeleaveInformation.DeleaveAmount) {
                //        throw new ValidationException(ErrorStrings.DealerPartsRetailOrder_Validation9);
                //    }
                //    dealerPartsStock.Quantity -= dealerRetailOrderDetail.Quantity * dbPartDeleaveInformation.DeleaveAmount;
                //}else {
                if(dealerPartsStock.Quantity < dealerRetailOrderDetail.Quantity) {
                    throw new ValidationException(ErrorStrings.DealerPartsRetailOrder_Validation9);
                }
                dealerPartsStock.Quantity -= dealerRetailOrderDetail.Quantity;
                //}
            }
        }

        public void 服务站审批二级站配件零售订单(DealerPartsRetailOrder dealerPartsRetailOrder) {
            var dbDealerPartsRetailOrder = ObjectContext.DealerPartsRetailOrders.Where(r => r.Id == dealerPartsRetailOrder.Id && r.Status == (int)DcsWorkflowOfSimpleApprovalStatus.新建).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbDealerPartsRetailOrder == null)
                throw new ValidationException(ErrorStrings.DealerPartsRetailOrder_Validation3);
            UpdateToDatabase(dealerPartsRetailOrder);
            dealerPartsRetailOrder.Status = (int)DcsWorkflowOfSimpleApprovalStatus.已审核;
            this.UpdateDealerPartsRetailOrderValidate(dealerPartsRetailOrder);
            var partsIds = dealerPartsRetailOrder.DealerRetailOrderDetails.Select(r => r.PartsId).ToArray();
            var dealerPartsStocks = ObjectContext.DealerPartsStocks.Where(r => r.DealerId == dealerPartsRetailOrder.DealerId && r.SalesCategoryId == dealerPartsRetailOrder.PartsSalesCategoryId && partsIds.Contains(r.SparePartId) && r.SubDealerId == dealerPartsRetailOrder.SubDealerId);
            foreach(var dealerRetailOrderDetail in dealerPartsRetailOrder.DealerRetailOrderDetails) {
                var dealerPartsStock = dealerPartsStocks.FirstOrDefault(r => r.SparePartId == dealerRetailOrderDetail.PartsId);
                //var dbPartDeleaveInformation = ObjectContext.PartDeleaveInformations.SingleOrDefault(c => c.OldPartId == dealerRetailOrderDetail.PartsId && c.PartsSalesCategoryId == dealerPartsRetailOrder.PartsSalesCategoryId && c.BranchId == dealerPartsRetailOrder.BranchId && c.Status == (int)DcsBaseDataStatus.有效);
                if(dealerPartsStock == null)
                    throw new ValidationException(string.Format(ErrorStrings.DealerPartsRetailOrder_Validation2, dealerRetailOrderDetail.PartsCode));
                //if(dbPartDeleaveInformation != null) {
                //    if(dealerPartsStock.Quantity < dealerRetailOrderDetail.Quantity * dbPartDeleaveInformation.DeleaveAmount) {
                //        throw new ValidationException(ErrorStrings.DealerPartsRetailOrder_Validation9);
                //    }
                //    dealerPartsStock.Quantity -= dealerRetailOrderDetail.Quantity * dbPartDeleaveInformation.DeleaveAmount;
                //}else {
                if(dealerPartsStock.Quantity < dealerRetailOrderDetail.Quantity) {
                    throw new ValidationException(ErrorStrings.DealerPartsRetailOrder_Validation9);
                }
                dealerPartsStock.Quantity -= dealerRetailOrderDetail.Quantity;
                // }
            }
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:11:57
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 作废服务站配件零售订单(DealerPartsRetailOrder dealerPartsRetailOrder) {
            new DealerPartsRetailOrderAch(this).作废服务站配件零售订单(dealerPartsRetailOrder);
        }
        
        [Update(UsingCustomMethod = true)]
        public void 生成服务站配件零售订单(DealerPartsRetailOrder dealerPartsRetailOrder) {
            new DealerPartsRetailOrderAch(this).生成服务站配件零售订单(dealerPartsRetailOrder);
        }
        
        [Update(UsingCustomMethod = true)]
        public void 修改服务站配件零售订单(DealerPartsRetailOrder dealerPartsRetailOrder) {
            new DealerPartsRetailOrderAch(this).修改服务站配件零售订单(dealerPartsRetailOrder);
        }
        
        [Update(UsingCustomMethod = true)]
        public void 审批服务站配件零售订单(DealerPartsRetailOrder dealerPartsRetailOrder) {
            new DealerPartsRetailOrderAch(this).审批服务站配件零售订单(dealerPartsRetailOrder);
        }
        
        [Update(UsingCustomMethod = true)]
        public void 服务站审批服务站配件零售订单(DealerPartsRetailOrder dealerPartsRetailOrder) {
            new DealerPartsRetailOrderAch(this).服务站审批服务站配件零售订单(dealerPartsRetailOrder);
        }
        
        [Update(UsingCustomMethod = true)]
        public void 审批二级站配件零售订单(DealerPartsRetailOrder dealerPartsRetailOrder) {
            new DealerPartsRetailOrderAch(this).审批二级站配件零售订单(dealerPartsRetailOrder);
        }
        
        [Update(UsingCustomMethod = true)]
        public void 服务站审批二级站配件零售订单(DealerPartsRetailOrder dealerPartsRetailOrder) {
            new DealerPartsRetailOrderAch(this).服务站审批二级站配件零售订单(dealerPartsRetailOrder);
        }
    }
}
