﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class DealerPartsSalesReturnBillAch : DcsSerivceAchieveBase {
        public DealerPartsSalesReturnBillAch(DcsDomainService domainService)
            : base(domainService) {
        }

        public DealerPartsSalesReturnBill GetPDealerPartsSalesReturnBillDetailsById(int id) {
            var dbPartsRetailReturnBill = ObjectContext.DealerPartsSalesReturnBills.SingleOrDefault(entity => entity.Id == id);
            if(dbPartsRetailReturnBill != null) {
                var partsRetailReturnBillDetails = ObjectContext.DealerRetailReturnBillDetails.Where(r => r.DealerPartsSalesReturnBillId == id).ToArray();
            }
            return dbPartsRetailReturnBill;
        }
        //验证标签码  
        private void ValidateDealerTrace(DealerPartsSalesReturnBill partsRetailReturnBill) {
            var outTraces = ObjectContext.AccurateTraces.Where(t => t.CompanyId == partsRetailReturnBill.DealerId && t.Type == (int)DCSAccurateTraceType.出库 && t.Status == (int)DCSAccurateTraceStatus.有效).ToArray();
            foreach(var item in partsRetailReturnBill.DealerRetailReturnBillDetails) {
                if(item.TraceProperty.HasValue && !string.IsNullOrEmpty(item.SIHLabelCode)) {
                    var traces = item.SIHLabelCode.Split(new Char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                    foreach(var code in traces) {
                        var inTrace = outTraces.Where(t => t.BoxCode == code).ToArray();
                        if(code.EndsWith("J"))
                            inTrace = outTraces.Where(t => t.SIHLabelCode == code).ToArray();
                        if(inTrace.Count() == 0) {
                            throw new ValidationException(code + "标签码不是当前服务站的标签码");
                        }
                        foreach(var trace in inTrace) {
                            if(trace.PartId != item.PartsId) {
                                throw new ValidationException(code + "不是配件" + item.PartsCode + "的标签码");
                            }
                        }
                    }
                }
            }
        }
        public void 生成服务站配件零售退货单(DealerPartsSalesReturnBill partsRetailReturnBill) {
            //验证标签码
            this.ValidateDealerTrace(partsRetailReturnBill);
            var partsOutboundBillDetails = ObjectContext.DealerRetailOrderDetails.Where(r => ObjectContext.DealerPartsRetailOrders.Any(v => v.Id == partsRetailReturnBill.SourceId && v.Code == partsRetailReturnBill.SourceCode && v.Id==r.DealerPartsRetailOrderId)).GroupBy(r => r.PartsId).Select(r => new {
                SparePartId = r.Key,
                OutboundAmount = r.Sum(v => v.Quantity)
            }).ToArray();
            var originalPartsRetailReturnBillDetails = ObjectContext.DealerRetailReturnBillDetails.Where(r => ObjectContext.DealerPartsSalesReturnBills.Any(v => v.SourceId == partsRetailReturnBill.SourceId && v.Status != (int)DcsPartsRetailReturnBillStatus.作废 && v.Id == r.DealerPartsSalesReturnBillId)).GroupBy(r => r.PartsId).Select(r => new {
                SparePartId = r.Key,
                Quantity = r.Sum(v => v.Quantity)
            }).ToArray();
            var afferentPartsRetailReturnBillDetails = partsRetailReturnBill.DealerRetailReturnBillDetails.GroupBy(r => r.PartsId).Select(r => new {
                SparePartId = r.Key,
                Quantity = r.Sum(v => v.Quantity)
            }).ToArray();
            var returnPartsRetailReturnBillDetails = originalPartsRetailReturnBillDetails.Concat(afferentPartsRetailReturnBillDetails).GroupBy(r => r.SparePartId).Select(r => new {
                SparePartId = r.Key,
                Quantity = r.Sum(v => v.Quantity)
            }).ToArray();
            if(!returnPartsRetailReturnBillDetails.All(r => partsOutboundBillDetails.Any(v => v.SparePartId == r.SparePartId)))
                throw new ValidationException(ErrorStrings.DealerPartsSalesReturnBill_Validation1);
            if(partsOutboundBillDetails.Any(r => returnPartsRetailReturnBillDetails.Any(v => v.SparePartId == r.SparePartId && v.Quantity > r.OutboundAmount)))
                throw new ValidationException(ErrorStrings.DealerPartsSalesReturnBill_Validation2);
        }
        public void 修改服务站配件零售退货单(DealerPartsSalesReturnBill partsRetailReturnBill) {
            //验证标签码
            this.ValidateDealerTrace(partsRetailReturnBill);
            var partsOutboundBillDetails = ObjectContext.DealerRetailOrderDetails.Where(r => ObjectContext.DealerPartsRetailOrders.Any(v => v.Id == partsRetailReturnBill.SourceId && v.Code == partsRetailReturnBill.SourceCode && v.Id == r.DealerPartsRetailOrderId)).GroupBy(r => r.PartsId).Select(r => new {
                SparePartId = r.Key,
                OutboundAmount = r.Sum(v => v.Quantity)
            }).ToArray();
            var originalPartsRetailReturnBillDetails = ObjectContext.DealerRetailReturnBillDetails.Where(r => ObjectContext.DealerPartsSalesReturnBills.Any(v => v.SourceId == partsRetailReturnBill.SourceId && v.Status != (int)DcsPartsRetailReturnBillStatus.作废 && v.Id == r.DealerPartsSalesReturnBillId && v.Id != partsRetailReturnBill.Id)).GroupBy(r => r.PartsId).Select(r => new {
                SparePartId = r.Key,
                Quantity = r.Sum(v => v.Quantity)
            }).ToArray();
            var afferentPartsRetailReturnBillDetails = partsRetailReturnBill.DealerRetailReturnBillDetails.GroupBy(r => r.PartsId).Select(r => new {
                SparePartId = r.Key,
                Quantity = r.Sum(v => v.Quantity)
            }).ToArray();
            var returnPartsRetailReturnBillDetails = originalPartsRetailReturnBillDetails.Concat(afferentPartsRetailReturnBillDetails).GroupBy(r => r.SparePartId).Select(r => new {
                SparePartId = r.Key,
                Quantity = r.Sum(v => v.Quantity)
            }).ToArray();
            if(!returnPartsRetailReturnBillDetails.All(r => partsOutboundBillDetails.Any(v => v.SparePartId == r.SparePartId)))
                throw new ValidationException(ErrorStrings.DealerPartsSalesReturnBill_Validation1);
            if(partsOutboundBillDetails.Any(r => returnPartsRetailReturnBillDetails.Any(v => v.SparePartId == r.SparePartId && v.Quantity > r.OutboundAmount)))
                throw new ValidationException(ErrorStrings.DealerPartsSalesReturnBill_Validation2);
        }
        public void 审批服务站配件零售退货单(DealerPartsSalesReturnBill partsRetailReturnBill) {
            var partsRetailReturnBillOld = ObjectContext.DealerPartsSalesReturnBills.Include("DealerRetailReturnBillDetails").Where(r => r.Id == partsRetailReturnBill.Id).FirstOrDefault();
            if(partsRetailReturnBillOld.Status != (int)DcsPartsRetailReturnBillStatus.新建) {
                throw new ValidationException(ErrorStrings.DealerPartsSalesReturnBill_Validation3);
            }
            var userInfo = Utils.GetCurrentUserInfo();
            var details = partsRetailReturnBillOld.DealerRetailReturnBillDetails.ToArray();
            var spId = details.Select(r => r.PartsId).ToArray();
            var outTraces = ObjectContext.AccurateTraces.Where(t => t.CompanyId == partsRetailReturnBill.DealerId && t.Type == (int)DCSAccurateTraceType.出库 && t.Status == (int)DCSAccurateTraceStatus.有效).ToArray();
            var tracOldIn = ObjectContext.AccurateTraces.Where(t => t.Status == (int)DCSAccurateTraceStatus.有效 && t.CompanyId == partsRetailReturnBill.DealerId && t.Type == (int)DCSAccurateTraceType.入库 && t.SIHLabelCode != null).ToArray(); 

            //查询配件的服务站库存
            var dealerStock = ObjectContext.DealerPartsStocks.Where(r => spId.Contains(r.SparePartId) && r.DealerId == partsRetailReturnBillOld.DealerId && r.SalesCategoryId == partsRetailReturnBill.SalesCategoryId && r.SubDealerId == -1).ToArray();
            foreach(var item in details) {
                var stock = dealerStock.Where(r => r.SparePartId == item.PartsId).FirstOrDefault();
                if(null==stock){
                   //未找到库存时，新增服务站库存
                    DealerPartsStock newDealerStock= new DealerPartsStock {
                        DealerId=userInfo.EnterpriseId,
                        DealerCode=userInfo.EnterpriseCode,
                        DealerName=userInfo.EnterpriseName,
                        BranchId = partsRetailReturnBill.BranchId,
                        SalesCategoryId=partsRetailReturnBill.SalesCategoryId,
                        SalesCategoryName=partsRetailReturnBill.SalesCategoryName,
                        SparePartId=item.PartsId,
                        SparePartCode=item.PartsCode,
                        Quantity=item.Quantity,
                        SubDealerId=-1,
                    };
                    InsertToDatabase(newDealerStock);
                    new DealerPartsStockAch(this.DomainService).InsertDealerPartsStockValidate(newDealerStock);
                } else {
                    stock.Quantity = stock.Quantity + item.Quantity;
                    DomainService.UpdateDealerPartsStock(stock);
                }
                if(item.TraceProperty.HasValue && !string.IsNullOrEmpty(item.SIHLabelCode)) {
                    var traces = item.SIHLabelCode.Split(new Char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                    var traceQty = 0;
                    foreach(var code in traces) {
                        var inTrace = outTraces.Where(t => t.BoxCode == code).ToArray();
                        if(code.EndsWith("J"))
                            inTrace = outTraces.Where(t => t.SIHLabelCode == code).ToArray();
                        if(inTrace.Count() == 0) {
                            throw new ValidationException(code + "标签码不是当前服务站的标签码");
                        }
                        foreach(var trace in inTrace) {
                            var currentQty = 0; 
                            if(traceQty == item.Quantity) {
                                continue;
                            } 
                            if(trace.PartId != item.PartsId) {
                                throw new ValidationException(code + "不是配件" + item.PartsCode + "的标签码");
                            }
                            if(item.TraceProperty.Value == (int)DCSTraceProperty.精确追溯) {
                                traceQty++;
                                currentQty = 1;
                            } else {
                                if(((trace.InQty - (trace.OutQty ?? 0))) > (item.Quantity - traceQty)) {
                                    traceQty = item.Quantity;
                                    currentQty = item.Quantity - traceQty;
                                } else {
                                    traceQty = traceQty + (trace.InQty.Value - (trace.OutQty ?? 0));
                                    currentQty = (trace.InQty.Value - (trace.OutQty ?? 0));
                                }
                            }
                            var outAccurateTrace = new AccurateTrace {
                                Type = (int)DCSAccurateTraceType.入库,
                                CompanyType = (int)DCSAccurateTraceCompanyType.服务站,
                                CompanyId = partsRetailReturnBill.DealerId,
                                SourceBillId = partsRetailReturnBill.Id,
                                PartId = item.PartsId,
                                TraceCode = trace.TraceCode,
                                SIHLabelCode = trace.SIHLabelCode,
                                Status = (int)DCSAccurateTraceStatus.有效,
                                TraceProperty = trace.TraceProperty,
                                BoxCode = trace.BoxCode,
                                InQty = currentQty,
                            };
                            new AccurateTraceAch(this.DomainService).ValidataAccurateTrace(outAccurateTrace);
                            if(item.TraceProperty.Value == (int)DCSTraceProperty.精确追溯) {
                                var outTraceLs = tracOldIn.Where(t => t.SourceBillId != partsRetailReturnBill.Id && t.TraceCode == trace.TraceCode && t.Status == (int)DCSAccurateTraceStatus.有效 && t.InQty == t.OutQty).ToArray();
                                foreach(var outTrace in outTraceLs) {
                                    outTrace.ModifierId = userInfo.Id;
                                    outTrace.ModifierName = userInfo.Name;
                                    outTrace.ModifyTime = DateTime.Now;
                                    outTrace.Status = (int)DCSAccurateTraceStatus.作废;
                                    UpdateToDatabase(outTrace);
                                }
                            }  
                        }
                    }
                }
            }
            partsRetailReturnBillOld.Status = (int)DcsPartsRetailReturnBillStatus.审批;
            partsRetailReturnBillOld.ApproverId = userInfo.Id;
            partsRetailReturnBillOld.ApproverName = userInfo.Name;
            partsRetailReturnBillOld.ApproveTime = DateTime.Now;
            DomainService.UpdateDealerPartsSalesReturnBill(partsRetailReturnBillOld);
            ObjectContext.SaveChanges();
        }
    }
    partial class DcsDomainService {
        public DealerPartsSalesReturnBill GetPDealerPartsSalesReturnBillDetailsById(int id) {
            return new DealerPartsSalesReturnBillAch(this).GetPDealerPartsSalesReturnBillDetailsById(id);
        }
         [Update(UsingCustomMethod = true)]
        public void 生成服务站配件零售退货单(DealerPartsSalesReturnBill partsRetailReturnBill) {
            new DealerPartsSalesReturnBillAch(this).生成服务站配件零售退货单(partsRetailReturnBill);
        }
         [Update(UsingCustomMethod = true)]
         public void 修改服务站配件零售退货单(DealerPartsSalesReturnBill partsRetailReturnBill) {
             new DealerPartsSalesReturnBillAch(this).修改服务站配件零售退货单(partsRetailReturnBill);
         }
        [Update(UsingCustomMethod = true)]
         public void 审批服务站配件零售退货单(DealerPartsSalesReturnBill partsRetailReturnBill) {
             new DealerPartsSalesReturnBillAch(this).审批服务站配件零售退货单(partsRetailReturnBill);
         }
    }
}
