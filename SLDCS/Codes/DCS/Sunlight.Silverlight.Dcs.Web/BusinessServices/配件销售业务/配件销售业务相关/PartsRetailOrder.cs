﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsRetailOrderAch : DcsSerivceAchieveBase {
        public PartsRetailOrderAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 生成配件零售订单(PartsRetailOrder partsRetailOrder) {
            var salesUnit = ObjectContext.SalesUnits.FirstOrDefault(r => ObjectContext.SalesUnitAffiWarehouses.Any(v => v.WarehouseId == partsRetailOrder.WarehouseId && r.Id == v.SalesUnitId));
            if(salesUnit == null)
                throw new ValidationException(ErrorStrings.PartsRetailOrder_Validation1);
            var branch = ObjectContext.Branches.FirstOrDefault(r => r.Id == salesUnit.BranchId && r.Status == (int)DcsMasterDataStatus.有效);
            if(branch == null) {
                throw new ValidationException(ErrorStrings.Branch_Validation5);
            }
            partsRetailOrder.BranchId = branch.Id;
            partsRetailOrder.BranchCode = branch.Code;
            partsRetailOrder.BranchName = branch.Name;
            partsRetailOrder.SalesUnitId = salesUnit.Id;
            partsRetailOrder.SalesUnitCode = salesUnit.Code;
            partsRetailOrder.SalesUnitName = salesUnit.Name;
        }

        public void 审批配件零售订单(PartsRetailOrder partsRetailOrder) {
            var dbPartsRetailOrder = ObjectContext.PartsRetailOrders.Where(r => r.Id == partsRetailOrder.Id && r.Status == (int)DcsPartsSalesOrderStatus.新增).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbPartsRetailOrder == null)
                throw new ValidationException(ErrorStrings.PartsRetailOrder_Validation2);
            var company = ObjectContext.Companies.SingleOrDefault(r => r.Id == partsRetailOrder.SalesUnitOwnerCompanyId && r.Status == (int)DcsMasterDataStatus.有效);
            if(company == null)
                throw new ValidationException(ErrorStrings.PartsRetailOrder_Validation4);
            var warehouse = ObjectContext.Warehouses.FirstOrDefault(r => r.Id == partsRetailOrder.WarehouseId && r.Status != (int)DcsBaseDataStatus.作废);
            var tempSalesUnit = ObjectContext.SalesUnits.SingleOrDefault(r => r.Id == partsRetailOrder.SalesUnitId);
            if(tempSalesUnit == null) {
                throw new ValidationException(ErrorStrings.PartsRetailOrder_Validation5);
            }
            if(warehouse == null)
                throw new ValidationException(ErrorStrings.DealerPartsRetailOrder_Validation8);
            var partsOutboundPlan = new PartsOutboundPlan {
                WarehouseId = partsRetailOrder.WarehouseId,
                WarehouseCode = partsRetailOrder.WarehouseCode,
                WarehouseName = partsRetailOrder.WarehouseName,
                StorageCompanyId = partsRetailOrder.SalesUnitOwnerCompanyId,
                StorageCompanyCode = partsRetailOrder.SalesUnitOwnerCompanyCode,
                StorageCompanyName = partsRetailOrder.SalesUnitOwnerCompanyName,
                StorageCompanyType = company.Type,
                BranchId = partsRetailOrder.BranchId,
                BranchCode = partsRetailOrder.BranchCode,
                BranchName = partsRetailOrder.BranchName,
                CounterpartCompanyId = 100000,
                CounterpartCompanyCode = "零售",
                CounterpartCompanyName = "零售",
                ReceivingCompanyId = 100000,
                ReceivingCompanyCode = "零售",
                ReceivingCompanyName = "零售",
                SourceId = partsRetailOrder.Id,
                SourceCode = partsRetailOrder.Code,
                OutboundType = (int)DcsPartsOutboundType.配件零售,
                CustomerAccountId = partsRetailOrder.CustomerAccountId,
                Status = (int)DcsPartsOutboundPlanStatus.新建,
                OriginalRequirementBillId = partsRetailOrder.Id,
                OriginalRequirementBillCode = partsRetailOrder.Code,
                OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.配件零售订单,
                Remark = partsRetailOrder.Remark,
                PartsSalesCategoryId = tempSalesUnit.PartsSalesCategoryId,
                IfWmsInterface = warehouse.WmsInterface
            };
            foreach(var partsRetailOrderDetails in partsRetailOrder.PartsRetailOrderDetails) {
                partsOutboundPlan.PartsOutboundPlanDetails.Add(new PartsOutboundPlanDetail {
                    SparePartId = partsRetailOrderDetails.SparePartId,
                    SparePartCode = partsRetailOrderDetails.SparePartCode,
                    SparePartName = partsRetailOrderDetails.SparePartName,
                    PlannedAmount = partsRetailOrderDetails.Quantity,
                    OutboundFulfillment = null,
                    Price = partsRetailOrderDetails.DiscountedPrice,
                    Remark = partsRetailOrderDetails.Remark
                });
            }
            DomainService.生成配件出库计划只校验保管区库存(partsOutboundPlan);
            UpdateToDatabase(partsRetailOrder);
            partsRetailOrder.Status = (int)DcsPartsRetailOrderStatus.审批;
            var userInfo = Utils.GetCurrentUserInfo();
            partsRetailOrder.ApproverId = userInfo.Id;
            partsRetailOrder.ApproverName = userInfo.Name;
            partsRetailOrder.ApproveTime = DateTime.Now;
        }


        public void 作废配件零售订单(PartsRetailOrder partsRetailOrder) {
            var userInfo = Utils.GetCurrentUserInfo();
            var dbPartsRetailOrder = ObjectContext.PartsRetailOrders.Where(r => r.Id == partsRetailOrder.Id && r.Status == (int)DcsPartsRetailOrderStatus.新建).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbPartsRetailOrder == null) {
                throw new ValidationException(ErrorStrings.PartsRetailOrder_Validation6);
            }
            partsRetailOrder.Status = (int)DcsPartsRetailOrderStatus.作废;
            partsRetailOrder.ModifierId = userInfo.Id;
            partsRetailOrder.ModifierName = userInfo.Name;
            partsRetailOrder.ModifyTime = DateTime.Now;
            partsRetailOrder.AbandonerId = userInfo.Id;
            partsRetailOrder.AbandonerName = userInfo.Name;
            partsRetailOrder.AbandonTime = DateTime.Now;
            UpdateToDatabase(partsRetailOrder);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:11:57
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 生成配件零售订单(PartsRetailOrder partsRetailOrder) {
            new PartsRetailOrderAch(this).生成配件零售订单(partsRetailOrder);
        }
        
        [Update(UsingCustomMethod = true)]
        public void 审批配件零售订单(PartsRetailOrder partsRetailOrder) {
            new PartsRetailOrderAch(this).审批配件零售订单(partsRetailOrder);
        }

        [Update(UsingCustomMethod = true)]
        public void 作废配件零售订单(PartsRetailOrder partsRetailOrder) {
            new PartsRetailOrderAch(this).作废配件零售订单(partsRetailOrder);
        }
    }
}