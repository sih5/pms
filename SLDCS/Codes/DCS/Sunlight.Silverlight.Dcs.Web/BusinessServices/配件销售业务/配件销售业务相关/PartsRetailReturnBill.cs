﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsRetailReturnBillAch : DcsSerivceAchieveBase {
        public PartsRetailReturnBillAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 生成配件零售退货单(PartsRetailReturnBill partsRetailReturnBill) {
            var partsOutboundBillDetails = ObjectContext.PartsOutboundBillDetails.Where(r => ObjectContext.PartsOutboundBills.Any(v => v.OriginalRequirementBillType == (int)DcsOriginalRequirementBillType.配件零售订单 && v.OriginalRequirementBillId == partsRetailReturnBill.SourceId && v.Id == r.PartsOutboundBillId)).GroupBy(r => r.SparePartId).Select(r => new {
                SparePartId = r.Key,
                OutboundAmount = r.Sum(v => v.OutboundAmount)
            }).ToArray();
            var originalPartsRetailReturnBillDetails = ObjectContext.PartsRetailReturnBillDetails.Where(r => ObjectContext.PartsRetailReturnBills.Any(v => v.SourceId == partsRetailReturnBill.SourceId && v.Status != (int)DcsPartsRetailReturnBillStatus.作废 && v.Id == r.PartsRetailReturnBillId)).GroupBy(r => r.SparePartId).Select(r => new {
                SparePartId = r.Key,
                Quantity = r.Sum(v => v.Quantity)
            }).ToArray();
            var afferentPartsRetailReturnBillDetails = partsRetailReturnBill.PartsRetailReturnBillDetails.GroupBy(r => r.SparePartId).Select(r => new {
                SparePartId = r.Key,
                Quantity = r.Sum(v => v.Quantity)
            }).ToArray();
            var returnPartsRetailReturnBillDetails = originalPartsRetailReturnBillDetails.Concat(afferentPartsRetailReturnBillDetails).GroupBy(r => r.SparePartId).Select(r => new {
                SparePartId = r.Key,
                Quantity = r.Sum(v => v.Quantity)
            }).ToArray();
            if(!returnPartsRetailReturnBillDetails.All(r => partsOutboundBillDetails.Any(v => v.SparePartId == r.SparePartId)))
                throw new ValidationException(ErrorStrings.PartsRetailReturnBill_Validation1);
            if(partsOutboundBillDetails.Any(r => returnPartsRetailReturnBillDetails.Any(v => v.SparePartId == r.SparePartId && v.Quantity > r.OutboundAmount)))
                throw new ValidationException(ErrorStrings.PartsRetailReturnBill_Validation2);
        }

        public void 修改配件零售退货单(PartsRetailReturnBill partsRetailReturnBill) {
            var partsOutboundBillDetails = ObjectContext.PartsOutboundBillDetails.Where(r => ObjectContext.PartsOutboundBills.Any(v => v.OriginalRequirementBillType == (int)DcsOriginalRequirementBillType.配件零售订单 && v.OriginalRequirementBillId == partsRetailReturnBill.SourceId && v.Id == r.PartsOutboundBillId)).GroupBy(r => r.SparePartId).Select(r => new {
                SparePartId = r.Key,
                OutboundAmount = r.Sum(v => v.OutboundAmount)
            }).ToArray();
            var originalPartsRetailReturnBillDetails = ObjectContext.PartsRetailReturnBillDetails.Where(r => ObjectContext.PartsRetailReturnBills.Any(v => v.SourceId == partsRetailReturnBill.SourceId && v.Status != (int)DcsPartsRetailReturnBillStatus.作废 && v.Id != partsRetailReturnBill.Id && v.Id == r.PartsRetailReturnBillId)).GroupBy(r => r.SparePartId).Select(r => new {
                SparePartId = r.Key,
                Quantity = r.Sum(v => v.Quantity)
            }).ToArray();
            var afferentPartsRetailReturnBillDetails = partsRetailReturnBill.PartsRetailReturnBillDetails.GroupBy(r => r.SparePartId).Select(r => new {
                SparePartId = r.Key,
                Quantity = r.Sum(v => v.Quantity)
            }).ToArray();
            var returnPartsRetailReturnBillDetails = originalPartsRetailReturnBillDetails.Concat(afferentPartsRetailReturnBillDetails).GroupBy(r => r.SparePartId).Select(r => new {
                SparePartId = r.Key,
                Quantity = r.Sum(v => v.Quantity)
            }).ToArray();
            if(!returnPartsRetailReturnBillDetails.All(r => partsOutboundBillDetails.Any(v => v.SparePartId == r.SparePartId)))
                throw new ValidationException(ErrorStrings.PartsRetailReturnBill_Validation1);
            if(partsOutboundBillDetails.Any(r => returnPartsRetailReturnBillDetails.Any(v => v.SparePartId == r.SparePartId && v.Quantity > r.OutboundAmount)))
                throw new ValidationException(ErrorStrings.PartsRetailReturnBill_Validation2);
        }

        public void 审批配件零售退货单(PartsRetailReturnBill partsRetailReturnBill) {
            var company = ObjectContext.Companies.SingleOrDefault(r => r.Id == partsRetailReturnBill.SalesUnitOwnerCompanyId && r.Status == (int)DcsMasterDataStatus.有效);
            if(company == null)
                throw new ValidationException(ErrorStrings.PartsRetailReturnBill_Validation3);
            var warehouse = ObjectContext.Warehouses.SingleOrDefault(r => r.Id == partsRetailReturnBill.WarehouseId && r.Status != (int)DcsBaseDataStatus.作废);
            if(warehouse == null)
                throw new ValidationException(ErrorStrings.PartsRetailReturnBill_Validation4);
            UpdateToDatabase(partsRetailReturnBill);
            var userInfo = Utils.GetCurrentUserInfo();
            partsRetailReturnBill.ApproverId = userInfo.Id;
            partsRetailReturnBill.ApproverName = userInfo.Name;
            partsRetailReturnBill.ApproveTime = DateTime.Now;
            partsRetailReturnBill.Status = (int)DcsPartsRetailReturnBillStatus.审批;
            this.UpdatePartsRetailReturnBillValidate(partsRetailReturnBill);
            var tempSalesUnit = ObjectContext.SalesUnits.SingleOrDefault(r => r.Id == partsRetailReturnBill.SalesUnitId);
            if(tempSalesUnit == null) {
                throw new ValidationException(ErrorStrings.PartsRetailReturnBill_Validation5);
            }
            var partsRetailReturnBillDetails = partsRetailReturnBill.PartsRetailReturnBillDetails;
            var partsInboundPlan = new PartsInboundPlan {
                WarehouseId = partsRetailReturnBill.WarehouseId,
                WarehouseCode = partsRetailReturnBill.WarehouseCode,
                WarehouseName = partsRetailReturnBill.WarehouseName,
                StorageCompanyId = partsRetailReturnBill.SalesUnitOwnerCompanyId,
                StorageCompanyCode = partsRetailReturnBill.SalesUnitOwnerCompanyCode,
                StorageCompanyName = partsRetailReturnBill.SalesUnitOwnerCompanyName,
                StorageCompanyType = company.Type,
                BranchId = partsRetailReturnBill.BranchId,
                BranchCode = partsRetailReturnBill.BranchCode,
                BranchName = partsRetailReturnBill.BranchName,
                CounterpartCompanyId = partsRetailReturnBill.RetailCustomerCompanyId ?? 0,
                CounterpartCompanyCode = partsRetailReturnBill.RetailCustomerCompanyCode,
                CounterpartCompanyName = partsRetailReturnBill.RetailCustomerCompanyName,
                SourceId = partsRetailReturnBill.Id,
                SourceCode = partsRetailReturnBill.Code,
                InboundType = (int)DcsPartsInboundType.配件零售退货,
                CustomerAccountId = partsRetailReturnBill.CustomerAccountId,
                OriginalRequirementBillId = partsRetailReturnBill.Id,
                OriginalRequirementBillCode = partsRetailReturnBill.Code,
                OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.配件零售退货单,
                Status = (int)DcsPartsInboundPlanStatus.新建,
                PartsSalesCategoryId = tempSalesUnit.PartsSalesCategoryId,
                IfWmsInterface = warehouse.WmsInterface
            };
            foreach(var partsRetailReturnBillDetail in partsRetailReturnBillDetails) {
                partsInboundPlan.PartsInboundPlanDetails.Add(new PartsInboundPlanDetail {
                    SparePartId = partsRetailReturnBillDetail.SparePartId,
                    SparePartCode = partsRetailReturnBillDetail.SparePartCode,
                    SparePartName = partsRetailReturnBillDetail.SparePartName,
                    PlannedAmount = partsRetailReturnBillDetail.Quantity,
                    Price = partsRetailReturnBillDetail.ReturnPrice
                });
            }
            InsertToDatabase(partsInboundPlan);
            new PartsInboundPlanAch(this.DomainService).InsertPartsInboundPlanValidate(partsInboundPlan);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:11:57
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 生成配件零售退货单(PartsRetailReturnBill partsRetailReturnBill) {
            new PartsRetailReturnBillAch(this).生成配件零售退货单(partsRetailReturnBill);
        }
        
        [Update(UsingCustomMethod = true)]
        public void 修改配件零售退货单(PartsRetailReturnBill partsRetailReturnBill) {
            new PartsRetailReturnBillAch(this).修改配件零售退货单(partsRetailReturnBill);
        }
        
        [Update(UsingCustomMethod = true)]
        public void 审批配件零售退货单(PartsRetailReturnBill partsRetailReturnBill) {
            new PartsRetailReturnBillAch(this).审批配件零售退货单(partsRetailReturnBill);
        }
    }
}
