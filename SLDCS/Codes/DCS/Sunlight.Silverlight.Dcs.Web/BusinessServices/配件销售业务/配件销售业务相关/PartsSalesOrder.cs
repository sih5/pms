﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Data;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using System.Text;
using Devart.Data.Oracle;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsSalesOrderAch : DcsSerivceAchieveBase {
        public PartsSalesOrderAch(DcsDomainService domainService)
            : base(domainService) {
        }

        public void 经销商修改配件销售订单(PartsSalesOrder partsSalesOrder) {
            CheckEntityState(partsSalesOrder);
            var dbpartsSalesOrder = ObjectContext.PartsSalesOrders.Where(r => r.Id == partsSalesOrder.Id && r.Status == (int)DcsPartsSalesOrderStatus.新增).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbpartsSalesOrder == null)
                throw new ValidationException(ErrorStrings.PartsSalesOrder_Validation1);
        }

        private PartsSalesOrder CreatePartsSalesOrder(PartsSalesOrder partsSalesOrder) {
            var currentpartsSalesOrder = new PartsSalesOrder();
            currentpartsSalesOrder.Code = GlobalVar.ASSIGNED_BY_SERVER;
            currentpartsSalesOrder.BranchId = partsSalesOrder.BranchId;
            currentpartsSalesOrder.BranchCode = partsSalesOrder.BranchCode;
            currentpartsSalesOrder.BranchName = partsSalesOrder.BranchName;
            currentpartsSalesOrder.SalesCategoryId = partsSalesOrder.SalesCategoryId;
            currentpartsSalesOrder.SalesCategoryName = partsSalesOrder.SalesCategoryName;
            currentpartsSalesOrder.SalesUnitId = partsSalesOrder.SalesUnitId;
            currentpartsSalesOrder.SalesUnitCode = partsSalesOrder.SalesUnitCode;
            currentpartsSalesOrder.SalesUnitName = partsSalesOrder.SalesUnitName;
            currentpartsSalesOrder.SalesUnitOwnerCompanyId = partsSalesOrder.SalesUnitOwnerCompanyId;
            currentpartsSalesOrder.SalesUnitOwnerCompanyCode = partsSalesOrder.SalesUnitOwnerCompanyCode;
            currentpartsSalesOrder.SalesUnitOwnerCompanyName = partsSalesOrder.SalesUnitOwnerCompanyName;
            currentpartsSalesOrder.SourceBillCode = partsSalesOrder.SourceBillCode;
            currentpartsSalesOrder.SourceBillId = partsSalesOrder.SourceBillId;
            currentpartsSalesOrder.CustomerAccountId = partsSalesOrder.CustomerAccountId;
            currentpartsSalesOrder.IsDebt = partsSalesOrder.IsDebt;
            currentpartsSalesOrder.InvoiceReceiveCompanyId = partsSalesOrder.InvoiceReceiveCompanyId;
            currentpartsSalesOrder.InvoiceReceiveCompanyCode = partsSalesOrder.InvoiceReceiveCompanyCode;
            currentpartsSalesOrder.InvoiceReceiveCompanyName = partsSalesOrder.InvoiceReceiveCompanyName;
            currentpartsSalesOrder.InvoiceReceiveCompanyType = partsSalesOrder.InvoiceReceiveCompanyType;
            currentpartsSalesOrder.InvoiceReceiveSaleCateId = partsSalesOrder.InvoiceReceiveSaleCateId;
            currentpartsSalesOrder.InvoiceReceiveSaleCateName = partsSalesOrder.InvoiceReceiveSaleCateName;
            currentpartsSalesOrder.SubmitCompanyId = partsSalesOrder.SubmitCompanyId;
            currentpartsSalesOrder.SubmitCompanyCode = partsSalesOrder.SubmitCompanyCode;
            currentpartsSalesOrder.SubmitCompanyName = partsSalesOrder.SubmitCompanyName;
            currentpartsSalesOrder.WarehouseId = partsSalesOrder.WarehouseId;
            currentpartsSalesOrder.ProvinceID = partsSalesOrder.ProvinceID;
            currentpartsSalesOrder.Province = partsSalesOrder.Province;
            currentpartsSalesOrder.City = partsSalesOrder.City;
            currentpartsSalesOrder.FirstClassStationId = partsSalesOrder.FirstClassStationId;
            currentpartsSalesOrder.FirstClassStationCode = partsSalesOrder.FirstClassStationCode;
            currentpartsSalesOrder.FirstClassStationName = partsSalesOrder.FirstClassStationName;
            currentpartsSalesOrder.PartsSalesOrderTypeId = partsSalesOrder.PartsSalesOrderTypeId;
            currentpartsSalesOrder.PartsSalesOrderTypeName = partsSalesOrder.PartsSalesOrderTypeName;
            currentpartsSalesOrder.IfAgencyService = partsSalesOrder.IfAgencyService;
            currentpartsSalesOrder.SalesActivityDiscountRate = partsSalesOrder.SalesActivityDiscountRate;
            currentpartsSalesOrder.SalesActivityDiscountAmount = partsSalesOrder.SalesActivityDiscountAmount;
            currentpartsSalesOrder.RequestedShippingTime = partsSalesOrder.RequestedShippingTime;
            currentpartsSalesOrder.RequestedDeliveryTime = partsSalesOrder.RequestedDeliveryTime;
            currentpartsSalesOrder.IfDirectProvision = partsSalesOrder.IfDirectProvision;
            currentpartsSalesOrder.CustomerType = partsSalesOrder.CustomerType;
            currentpartsSalesOrder.SecondLevelOrderType = partsSalesOrder.SecondLevelOrderType;
            currentpartsSalesOrder.Remark = partsSalesOrder.Remark;
            currentpartsSalesOrder.ContactPerson = partsSalesOrder.ContactPerson;
            currentpartsSalesOrder.ContactPhone = partsSalesOrder.ContactPhone;
            currentpartsSalesOrder.ReceivingCompanyId = partsSalesOrder.ReceivingCompanyId;
            currentpartsSalesOrder.ReceivingCompanyCode = partsSalesOrder.ReceivingCompanyCode;
            currentpartsSalesOrder.ReceivingCompanyName = partsSalesOrder.ReceivingCompanyName;
            currentpartsSalesOrder.ShippingMethod = partsSalesOrder.ShippingMethod;
            currentpartsSalesOrder.ReceivingWarehouseId = partsSalesOrder.ReceivingWarehouseId;
            currentpartsSalesOrder.ReceivingWarehouseName = partsSalesOrder.ReceivingWarehouseName;
            currentpartsSalesOrder.ReceivingAddress = partsSalesOrder.ReceivingAddress;
            currentpartsSalesOrder.CompanyAddressId = partsSalesOrder.CompanyAddressId;
            return currentpartsSalesOrder;
        }

        public int 校验订货配件起订金额是否满足(PartsSalesOrder partsSalesOrder) {
            var tempDetails = (from a in this.ObjectContext.PartsSalesOrderDetails.Where(r => r.PartsSalesOrderId == partsSalesOrder.Id)
                               join b in this.ObjectContext.PartsSupplierRelations.Where(r => r.IsPrimary == true && r.Status == (int)DcsBaseDataStatus.有效) on a.SparePartId equals b.PartId
                               join c in this.ObjectContext.CustomerSupplyInitialFeeSets.Where(r => r.CustomerId == partsSalesOrder.SubmitCompanyId && r.Status == (int)DcsBaseDataStatus.有效) on b.SupplierId equals c.SupplierId
                               select new {
                                   a.Id,
                                   a.OrderSum,
                                   c.SupplierId,
                                   c.InitialFee
                               }).ToArray();
            if(tempDetails.Any()) {
                var tempGroups = tempDetails.GroupBy(r => new {
                    r.SupplierId
                }).Select(r => new {
                    r.Key.SupplierId,
                    r.First().InitialFee,
                    SumOrderSum = r.Sum(v => v.OrderSum)
                }).ToArray();
                int counts = 0;
                foreach(var item in tempGroups) {
                    if(item.SumOrderSum >= item.InitialFee)
                        continue; //供应商汇总金额，小于起订金额
                    foreach(var tempDetail in tempDetails.Where(r => r.SupplierId == item.SupplierId)) {
                        counts++;
                    }
                }
                if(tempDetails.Count() == counts) { //判断是否清单全部不满足
                    return 1;
                }
                if(counts == 0)
                    return 0;
                if(counts < tempDetails.Count())
                    return 2;
            }
            return 0;
        }

        public void 校验订货配件起订金额(PartsSalesOrder partsSalesOrder, PartsSalesOrderDetail[] partsSalesOrderDetails) {
            var tempDetails = (from a in partsSalesOrderDetails
                               join b in this.ObjectContext.PartsSupplierRelations.Where(r => r.IsPrimary == true && r.Status == (int)DcsBaseDataStatus.有效) on a.SparePartId equals b.PartId
                               join c in this.ObjectContext.CustomerSupplyInitialFeeSets.Where(r => r.CustomerId == partsSalesOrder.SubmitCompanyId && r.Status == (int)DcsBaseDataStatus.有效) on b.SupplierId equals c.SupplierId
                               select new {
                                   a.Id,
                                   a.OrderSum,
                                   a.SparePartCode,
                                   c.SupplierId,
                                   c.SupplierName,
                                   c.InitialFee
                               }).ToArray();
            if(tempDetails.Any()) {
                var tempGroups = tempDetails.GroupBy(r => new {
                    r.SupplierId
                }).Select(r => new {
                    r.Key.SupplierId,
                    r.First().SparePartCode,
                    r.First().SupplierName,
                    r.First().InitialFee,
                    SumOrderSum = r.Sum(v => v.OrderSum)
                }).ToArray();
                foreach(var item in tempGroups) {
                    if(item.SumOrderSum >= item.InitialFee)
                        continue; //供应商汇总金额，小于起订金额
                    foreach(var tempDetail in tempDetails.Where(r => r.SupplierId == item.SupplierId)) {
                        throw new ValidationException(string.Format(ErrorStrings.PartsSalesOrder_Validation13, tempDetail.SparePartCode, tempDetail.SupplierName));
                    }
                }
            }
        }

        public void 提交配件销售订单(PartsSalesOrder partsSalesOrder) {
            var dbpartsSalesOrder = ObjectContext.PartsSalesOrders.Where(r => r.Id == partsSalesOrder.Id && r.Status == (int)DcsPartsSalesOrderStatus.新增).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbpartsSalesOrder == null)
                throw new ValidationException(ErrorStrings.PartsSalesOrder_Validation2);
            var company = ObjectContext.Companies.FirstOrDefault(o => o.Id == partsSalesOrder.ReceivingCompanyId && (o.Type == (int)DcsCompanyType.代理库 || o.Type == (int)DcsCompanyType.服务站兼代理库));
            if(company != null && !partsSalesOrder.ReceivingWarehouseId.HasValue) {
                throw new ValidationException(ErrorStrings.PartsSalesOrder_Validation14);
            }

            var partsSalesOrderDetails = partsSalesOrder.PartsSalesOrderDetails;
            var sparePartIds = partsSalesOrderDetails.Select(r => r.SparePartId).ToArray();
            //是否直供 = 是
            if(partsSalesOrder.IfDirectProvision && partsSalesOrder.PartsSalesOrderTypeName != "紧急订单" && partsSalesOrder.PartsSalesOrderTypeName != "特急订单" && partsSalesOrder.PartsSalesOrderTypeName != "事故订单") {
                var tempDetails = (from a in partsSalesOrderDetails
                                   join b in this.ObjectContext.PartsSupplierRelations.Where(r => r.IsPrimary == true && r.Status == (int)DcsBaseDataStatus.有效) on a.SparePartId equals b.PartId
                                   join c in this.ObjectContext.CustomerSupplyInitialFeeSets.Where(r => r.CustomerId == partsSalesOrder.SubmitCompanyId && r.Status == (int)DcsBaseDataStatus.有效) on b.SupplierId equals c.SupplierId
                                   select new {
                                       a.Id,
                                       a.OrderSum,
                                       c.SupplierId,
                                       c.InitialFee,
                                   }).ToArray();
                if(tempDetails.Any()) {
                    var tempGroups = tempDetails.GroupBy(r => new {
                        r.SupplierId
                    }).Select(r => new {
                        r.Key.SupplierId,
                        r.First().InitialFee,
                        SumOrderSum = r.Sum(v => v.OrderSum)
                    }).ToArray();
                    var currentPartsSalesOrder = this.CreatePartsSalesOrder(partsSalesOrder);
                    foreach(var item in tempGroups) {
                        if(item.SumOrderSum < item.InitialFee) { //供应商汇总金额，小于起订金额
                            foreach(var tempDetail in tempDetails.Where(r => r.SupplierId == item.SupplierId).ToArray()) {
                                var partsSalesOrderDetail = partsSalesOrderDetails.FirstOrDefault(r => r.Id == tempDetail.Id);
                                if(partsSalesOrderDetail != null) {
                                    partsSalesOrderDetails.Remove(partsSalesOrderDetail);
                                    DeleteFromDatabase(partsSalesOrderDetail);
                                    currentPartsSalesOrder.PartsSalesOrderDetails.Add(new PartsSalesOrderDetail() {
                                        SparePartId = partsSalesOrderDetail.SparePartId,
                                        SparePartCode = partsSalesOrderDetail.SparePartCode,
                                        SparePartName = partsSalesOrderDetail.SparePartName,
                                        MeasureUnit = partsSalesOrderDetail.MeasureUnit,
                                        OrderedQuantity = partsSalesOrderDetail.OrderedQuantity,
                                        ApproveQuantity = partsSalesOrderDetail.ApproveQuantity,
                                        OriginalPrice = partsSalesOrderDetail.OriginalPrice,
                                        CustOrderPriceGradeCoefficient = partsSalesOrderDetail.CustOrderPriceGradeCoefficient,
                                        IfCanNewPart = partsSalesOrderDetail.IfCanNewPart,
                                        DiscountedPrice = partsSalesOrderDetail.DiscountedPrice,
                                        OrderPrice = partsSalesOrderDetail.OrderPrice,
                                        OrderSum = partsSalesOrderDetail.OrderSum,
                                        EstimatedFulfillTime = partsSalesOrderDetail.EstimatedFulfillTime,
                                        Remark = partsSalesOrderDetail.Remark,
                                        OverseasPartsFigure = partsSalesOrderDetail.OverseasPartsFigure,
                                        OverseasProjectNumber = partsSalesOrderDetail.OverseasProjectNumber,
                                        PriceTypeName = partsSalesOrderDetail.PriceTypeName,
                                        ABCStrategy = partsSalesOrderDetail.ABCStrategy,
                                        MInSaleingAmount = partsSalesOrderDetail.MInSaleingAmount,
                                        SalesPrice = partsSalesOrderDetail.SalesPrice
                                    });
                                }
                            }
                        }
                    }
                    if(tempDetails.Count() == currentPartsSalesOrder.PartsSalesOrderDetails.Count()) { //判断是否清单全部不满足
                        throw new ValidationException(ErrorStrings.PartsSalesOrder_Validation15);
                    }
                    if(currentPartsSalesOrder.PartsSalesOrderDetails.Any()) {
                        currentPartsSalesOrder.TotalAmount = currentPartsSalesOrder.PartsSalesOrderDetails.Sum(r => r.OrderSum);
                        InsertToDatabase(currentPartsSalesOrder);
                        currentPartsSalesOrder.IfDirectProvision = true;
                        currentPartsSalesOrder.Status = (int)DcsPartsSalesOrderStatus.新增;
                        this.InsertPartsSalesOrderValidate(currentPartsSalesOrder);
                    }
                }
            }

            if(dbpartsSalesOrder.IsExport == 1) {
                if(partsSalesOrder.PriceType != (int)DcsExportCustPriceType.其他) {
                    var virtualPartsSalesPrices = new VirtualPartsSalesPriceAch(this.DomainService).根据配件清单获取销售价格出口使用(partsSalesOrder.PartsSalesOrderTypeId, partsSalesOrder.ReceivingCompanyId, sparePartIds, partsSalesOrder.PriceType ?? 0).ToArray();

                    foreach(var partsSalesOrderDetail in partsSalesOrderDetails) {
                        var virtualPartsSalesPrice = virtualPartsSalesPrices.FirstOrDefault(r => r.SparePartId == partsSalesOrderDetail.SparePartId);
                        if(virtualPartsSalesPrice == null)
                            throw new ValidationException(string.Format(ErrorStrings.PartsSalesOrder_Validation3, partsSalesOrder.PartsSalesOrderTypeName, partsSalesOrderDetail.SparePartCode));
                        partsSalesOrderDetail.OrderPrice = virtualPartsSalesPrice.Price;
                        partsSalesOrderDetail.MeasureUnit = virtualPartsSalesPrice.MeasureUnit;
                        partsSalesOrderDetail.CustOrderPriceGradeCoefficient = virtualPartsSalesPrice.Coefficient;
                        partsSalesOrderDetail.OriginalPrice = virtualPartsSalesPrice.RetailGuidePrice;
                        partsSalesOrderDetail.DiscountedPrice = partsSalesOrderDetail.OriginalPrice - partsSalesOrderDetail.OrderPrice;
                        partsSalesOrderDetail.OrderSum = partsSalesOrderDetail.OrderedQuantity * partsSalesOrderDetail.OrderPrice;
                        partsSalesOrderDetail.PriceTypeName = virtualPartsSalesPrice.PriceTypeName;
                        partsSalesOrderDetail.ABCStrategy = virtualPartsSalesPrice.ABCStrategy;
                        partsSalesOrderDetail.SalesPrice = virtualPartsSalesPrice.SalesPrice;
                    }
                }
            } else {
                var virtualPartsSalesPrices = new VirtualPartsSalesPriceAch(this.DomainService).根据配件清单获取销售价格(partsSalesOrder.PartsSalesOrderTypeId, partsSalesOrder.SubmitCompanyId, sparePartIds).ToArray();

                foreach(var partsSalesOrderDetail in partsSalesOrderDetails) {
                    var virtualPartsSalesPrice = virtualPartsSalesPrices.FirstOrDefault(r => r.SparePartId == partsSalesOrderDetail.SparePartId);
                    if(virtualPartsSalesPrice == null)
                        throw new ValidationException(string.Format(ErrorStrings.PartsSalesOrder_Validation3, partsSalesOrder.PartsSalesOrderTypeName, partsSalesOrderDetail.SparePartCode));
                    if(virtualPartsSalesPrice.PartsTreatyPrice != null)
                        partsSalesOrderDetail.OrderPrice = virtualPartsSalesPrice.PartsTreatyPrice.Value;
                    else
                        partsSalesOrderDetail.OrderPrice = virtualPartsSalesPrice.Price;
                    partsSalesOrderDetail.MeasureUnit = virtualPartsSalesPrice.MeasureUnit;
                    partsSalesOrderDetail.CustOrderPriceGradeCoefficient = virtualPartsSalesPrice.Coefficient;
                    partsSalesOrderDetail.OriginalPrice = virtualPartsSalesPrice.RetailGuidePrice;
                    partsSalesOrderDetail.DiscountedPrice = partsSalesOrderDetail.OriginalPrice - partsSalesOrderDetail.OrderPrice;
                    partsSalesOrderDetail.OrderSum = partsSalesOrderDetail.OrderedQuantity * partsSalesOrderDetail.OrderPrice;
                    partsSalesOrderDetail.PriceTypeName = virtualPartsSalesPrice.PriceTypeName;
                    partsSalesOrderDetail.ABCStrategy = virtualPartsSalesPrice.ABCStrategy;
                    partsSalesOrderDetail.SalesPrice = virtualPartsSalesPrice.SalesPrice;
                }
            }
            // 4
            var allowCustomerCredit = ObjectContext.SalesCenterstrategies.FirstOrDefault(r => r.PartsSalesCategoryId == partsSalesOrder.SalesCategoryId);
            if(allowCustomerCredit == null) {
                throw new ValidationException(ErrorStrings.PartsSalesOrder_Validation16);
            } else if(allowCustomerCredit.AllowCustomerCredit.HasValue && !allowCustomerCredit.AllowCustomerCredit.Value) {
                var customerAccount = DomainService.查询客户可用资金(partsSalesOrder.CustomerAccountId, null, null);
                //由于查询客户可用资金方法查客户账户时只传CustomerAccountId不会查返利金额
                decimal rebateAmout = 0;
                var salesUnit = ObjectContext.SalesUnits.FirstOrDefault(v => v.Id == partsSalesOrder.SalesUnitId && v.Status == (int)DcsMasterDataStatus.有效);
                if(salesUnit == null)
                    throw new ValidationException(string.Format(ErrorStrings.PartsSalesOrderProcess_Validation9, partsSalesOrder.Code));
                var partsRebateAccount = ObjectContext.PartsRebateAccounts.FirstOrDefault(r => r.BranchId == partsSalesOrder.SalesUnitOwnerCompanyId && r.CustomerCompanyId == partsSalesOrder.SubmitCompanyId && r.AccountGroupId == salesUnit.AccountGroupId);
                if(partsRebateAccount != null) {
                    rebateAmout = partsRebateAccount.AccountBalanceAmount;
                }
                if((customerAccount == null) || (customerAccount.UseablePosition + rebateAmout < partsSalesOrder.TotalAmount)) {
                    throw new ValidationException(ErrorStrings.PartsSalesOrder_Validation17);
                }
            }
            //订单类型 =非油品订单、是否直供=是
            //金税分类编码=1的配件拆分为“订单类型=油品订单，是否直供为是“的直供订单
            if(partsSalesOrder.IfDirectProvision && "油品订单" != partsSalesOrder.PartsSalesOrderTypeName) {
                //查询订单配件的金税分类编码
                var spIds = partsSalesOrderDetails.Select(r => r.SparePartId).ToArray();
                var spareparts = ObjectContext.SpareParts.Where(s => spIds.Contains(s.Id)).ToArray();
                var ypPartsSalesOrder = this.CreatePartsSalesOrder(partsSalesOrder);
                if(!spareparts.Any(r => r.GoldenTaxClassifyCode != "1")) {
                    partsSalesOrder.TotalAmount = partsSalesOrderDetails.Sum(r => r.OrderSum);
                    var userInfo = Utils.GetCurrentUserInfo();
                    UpdateToDatabase(partsSalesOrder);
                    partsSalesOrder.Status = (int)DcsPartsSalesOrderStatus.提交;
                    partsSalesOrder.SubmitterId = userInfo.Id;
                    partsSalesOrder.SubmitterName = userInfo.Name;
                    partsSalesOrder.SubmitTime = DateTime.Now;
                    var orderType = ObjectContext.PartsSalesOrderTypes.Where(r => r.Name == "油品订单").FirstOrDefault();
                    partsSalesOrder.PartsSalesOrderTypeId = orderType.Id;
                    partsSalesOrder.PartsSalesOrderTypeName = orderType.Name;
                    this.UpdatePartsSalesOrderValidate(partsSalesOrder);
                    partsSalesOrderDetails = null;
                } else {
                    foreach(var item in spareparts) {
                        if(item.GoldenTaxClassifyCode == "1") {
                            var partsSalesOrderDetail = partsSalesOrderDetails.FirstOrDefault(r => r.SparePartId == item.Id);
                            partsSalesOrderDetails.Remove(partsSalesOrderDetail);
                            DeleteFromDatabase(partsSalesOrderDetail);
                            ypPartsSalesOrder.PartsSalesOrderDetails.Add(new PartsSalesOrderDetail() {
                                SparePartId = partsSalesOrderDetail.SparePartId,
                                SparePartCode = partsSalesOrderDetail.SparePartCode,
                                SparePartName = partsSalesOrderDetail.SparePartName,
                                MeasureUnit = partsSalesOrderDetail.MeasureUnit,
                                OrderedQuantity = partsSalesOrderDetail.OrderedQuantity,
                                ApproveQuantity = partsSalesOrderDetail.ApproveQuantity,
                                OriginalPrice = partsSalesOrderDetail.OriginalPrice,
                                CustOrderPriceGradeCoefficient = partsSalesOrderDetail.CustOrderPriceGradeCoefficient,
                                IfCanNewPart = partsSalesOrderDetail.IfCanNewPart,
                                DiscountedPrice = partsSalesOrderDetail.DiscountedPrice,
                                OrderPrice = partsSalesOrderDetail.OrderPrice,
                                OrderSum = partsSalesOrderDetail.OrderSum,
                                EstimatedFulfillTime = partsSalesOrderDetail.EstimatedFulfillTime,
                                Remark = partsSalesOrderDetail.Remark,
                                OverseasPartsFigure = partsSalesOrderDetail.OverseasPartsFigure,
                                OverseasProjectNumber = partsSalesOrderDetail.OverseasProjectNumber,
                                PriceTypeName = partsSalesOrderDetail.PriceTypeName,
                                ABCStrategy = partsSalesOrderDetail.ABCStrategy,
                                MInSaleingAmount = partsSalesOrderDetail.MInSaleingAmount,
                                SalesPrice = partsSalesOrderDetail.SalesPrice
                            });
                        }
                    }
                    if(ypPartsSalesOrder.PartsSalesOrderDetails.Any()) {
                        ypPartsSalesOrder.TotalAmount = ypPartsSalesOrder.PartsSalesOrderDetails.Sum(r => r.OrderSum);
                        InsertToDatabase(ypPartsSalesOrder);
                        ypPartsSalesOrder.IfDirectProvision = true;
                        ypPartsSalesOrder.Status = (int)DcsPartsSalesOrderStatus.提交;
                        var orderType = ObjectContext.PartsSalesOrderTypes.Where(r => r.Name == "油品订单").FirstOrDefault();
                        ypPartsSalesOrder.PartsSalesOrderTypeId = orderType.Id;
                        ypPartsSalesOrder.PartsSalesOrderTypeName = orderType.Name;
                        this.InsertPartsSalesOrderValidate(ypPartsSalesOrder);
                    }
                }

            }
            if(null != partsSalesOrderDetails && partsSalesOrderDetails.Any()) {
                partsSalesOrder.TotalAmount = partsSalesOrderDetails.Sum(r => r.OrderSum);
                var userInfo = Utils.GetCurrentUserInfo();
                UpdateToDatabase(partsSalesOrder);
                partsSalesOrder.Status = (int)DcsPartsSalesOrderStatus.提交;
                partsSalesOrder.SubmitterId = userInfo.Id;
                partsSalesOrder.SubmitterName = userInfo.Name;
                partsSalesOrder.SubmitTime = DateTime.Now;
                this.UpdatePartsSalesOrderValidate(partsSalesOrder);
            }
        }

        internal string GetMyInsertSql(string tableName, string keyName, string[] fieldNames, string nextval) {
            if(string.IsNullOrEmpty(keyName)) {
                return string.Format("insert into {0}({1}) Values (:{2})", tableName, string.Join(",", fieldNames), string.Join(",:", fieldNames));
            } else {
                return string.Format("insert into {0}({1},{2}) Values ({3},:{4})", tableName, keyName, string.Join(",", fieldNames), nextval ?? (":" + keyName), string.Join(",:", fieldNames));
            }

        }


        public void 撤销配件销售订单(PartsSalesOrder partsSalesOrder) {
            var dbpartsSalesOrder = ObjectContext.PartsSalesOrders.Where(r => r.Id == partsSalesOrder.Id && r.Status == (int)DcsPartsSalesOrderStatus.提交).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbpartsSalesOrder == null)
                throw new ValidationException(ErrorStrings.PartsSalesOrder_Validation4);
            var userInfo = Utils.GetCurrentUserInfo();
            UpdateToDatabase(partsSalesOrder);
            partsSalesOrder.Status = (int)DcsPartsSalesOrderStatus.撤单;
            partsSalesOrder.CancelOperatorId = userInfo.Id;
            partsSalesOrder.CancelOperatorName = userInfo.Name;
            partsSalesOrder.CancelTime = DateTime.Now;
            this.UpdatePartsSalesOrderValidate(partsSalesOrder);
        }


        public void 终止配件销售订单(PartsSalesOrder partsSalesOrder) {
            var dbpartsSalesOrder = ObjectContext.PartsSalesOrders.Where(r => r.Id == partsSalesOrder.Id && ((r.Status == (int)DcsPartsSalesOrderStatus.提交 || r.Status == (int)DcsPartsSalesOrderStatus.部分审批) || r.IsTransSuccess.Value)).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbpartsSalesOrder == null)
                throw new ValidationException(ErrorStrings.PartsSalesOrder_Validation12);
            UpdateToDatabase(partsSalesOrder);
            partsSalesOrder.Status = (int)DcsPartsSalesOrderStatus.终止;
            this.UpdatePartsSalesOrderValidate(partsSalesOrder);
            //new PartsSalesOrderAch(this.DomainService).PMS_SYC_150_SendPMSUpdateOrderStatus_PS(partsSalesOrder);
        }

        public void 作废配件销售订单(PartsSalesOrder partsSalesOrder) {
            var dbpartsSalesOrder = ObjectContext.PartsSalesOrders.Where(r => r.Id == partsSalesOrder.Id && r.Status == (int)DcsPartsSalesOrderStatus.新增).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbpartsSalesOrder == null)
                throw new ValidationException(ErrorStrings.PartsSalesOrder_Validation6);
            var userInfo = Utils.GetCurrentUserInfo();
            UpdateToDatabase(partsSalesOrder);
            partsSalesOrder.Status = (int)DcsPartsSalesOrderStatus.作废;
            partsSalesOrder.AbandonerId = userInfo.Id;
            partsSalesOrder.AbandonerName = userInfo.Name;
            partsSalesOrder.AbandonTime = DateTime.Now;
            this.UpdatePartsSalesOrderValidate(partsSalesOrder);
            //new PartsSalesOrderAch(this.DomainService).PMS_SYC_150_SendPMSUpdateOrderStatus_PS(partsSalesOrder);
        }


        public void 修改销售订单类型(PartsSalesOrder partsSalesOrder) {
            var dbpartsSalesOrder = ObjectContext.PartsSalesOrders.Where(c => c.Id == partsSalesOrder.Id && c.Status == (int)DcsPartsSalesOrderStatus.提交).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbpartsSalesOrder == null)
                throw new ValidationException(ErrorStrings.PartsSalesOrder_Validation5);
            var dbCustomerOrderPriceGrade = ObjectContext.CustomerOrderPriceGrades.SingleOrDefault(c => c.CustomerCompanyId == partsSalesOrder.InvoiceReceiveCompanyId && c.PartsSalesOrderTypeId == partsSalesOrder.PartsSalesOrderTypeId && c.Status == (int)DcsVehicleBaseDataStatus.有效);
            if(dbCustomerOrderPriceGrade == null) {
                throw new ValidationException(ErrorStrings.PartsSalesOrder_Validation9);
            }
            var dbPartsSalesOrderDetails = partsSalesOrder.PartsSalesOrderDetails.ToList();
            if(dbPartsSalesOrderDetails.Count > 0) {
                foreach(var item in dbPartsSalesOrderDetails) {
                    item.CustOrderPriceGradeCoefficient = dbCustomerOrderPriceGrade.Coefficient;
                    item.OrderPrice = item.OriginalPrice - item.DiscountedPrice;
                    item.OrderSum = item.OrderPrice * item.OrderedQuantity;
                    if(item.EntityState == EntityState.Added)
                        InsertToDatabase(item);
                    UpdateToDatabase(item);
                }
                UpdateToDatabase(partsSalesOrder);
                this.UpdatePartsSalesOrderValidate(partsSalesOrder);
            }
        }

        public void 转分公司配件销售订单(PartsSalesOrder partsSalesOrder) {
            var salesUnit = this.ObjectContext.SalesUnits.FirstOrDefault(r => r.PartsSalesCategoryId == partsSalesOrder.SalesCategoryId
                && r.OwnerCompanyId == partsSalesOrder.BranchId);
            if(salesUnit == null)
                throw new ValidationException(ErrorStrings.PartsSalesOrder_Validation10);
            var company = this.ObjectContext.Companies.FirstOrDefault(r => r.Id == salesUnit.OwnerCompanyId);
            var customerAccount = ObjectContext.CustomerAccounts.FirstOrDefault(p => p.CustomerCompanyId == partsSalesOrder.SubmitCompanyId && p.AccountGroupId == salesUnit.AccountGroupId);
            if(customerAccount == null)
                throw new ValidationException(ErrorStrings.PartsSalesOrder_Validation11);
            var newPartsSalesOrder = new PartsSalesOrder {
                BranchId = partsSalesOrder.BranchId,
                BranchCode = partsSalesOrder.BranchCode,
                BranchName = partsSalesOrder.BranchName,
                SalesCategoryId = partsSalesOrder.SalesCategoryId,
                SalesCategoryName = partsSalesOrder.SalesCategoryName,
                SalesUnitId = salesUnit.Id,
                SalesUnitCode = partsSalesOrder.SalesUnitCode,
                SalesUnitName = partsSalesOrder.SalesUnitName,
                SalesUnitOwnerCompanyId = company.Id,
                SalesUnitOwnerCompanyCode = company.Code,
                SalesUnitOwnerCompanyName = company.Name,
                SourceBillCode = partsSalesOrder.SourceBillCode,
                SourceBillId = partsSalesOrder.SourceBillId,
                CustomerAccountId = customerAccount.Id,
                IsDebt = partsSalesOrder.IsDebt,
                InvoiceReceiveCompanyId = partsSalesOrder.InvoiceReceiveCompanyId,
                InvoiceReceiveCompanyCode = partsSalesOrder.InvoiceReceiveCompanyCode,
                InvoiceReceiveCompanyName = partsSalesOrder.InvoiceReceiveCompanyName,
                InvoiceReceiveCompanyType = partsSalesOrder.InvoiceReceiveCompanyType,
                InvoiceReceiveSaleCateId = partsSalesOrder.InvoiceReceiveSaleCateId,
                InvoiceReceiveSaleCateName = partsSalesOrder.InvoiceReceiveSaleCateName,
                SubmitCompanyId = partsSalesOrder.SubmitCompanyId,
                SubmitCompanyCode = partsSalesOrder.SubmitCompanyCode,
                SubmitCompanyName = partsSalesOrder.SubmitCompanyName,
                ProvinceID = partsSalesOrder.ProvinceID,
                Province = partsSalesOrder.Province,
                City = partsSalesOrder.City,
                FirstClassStationId = partsSalesOrder.FirstClassStationId,
                FirstClassStationCode = partsSalesOrder.FirstClassStationCode,
                FirstClassStationName = partsSalesOrder.FirstClassStationName,
                PartsSalesOrderTypeId = partsSalesOrder.PartsSalesOrderTypeId,
                PartsSalesOrderTypeName = partsSalesOrder.PartsSalesOrderTypeName,
                IfAgencyService = false,
                SalesActivityDiscountRate = 1,
                SalesActivityDiscountAmount = 0,
                RequestedShippingTime = partsSalesOrder.RequestedShippingTime,
                RequestedDeliveryTime = partsSalesOrder.RequestedDeliveryTime,
                IfDirectProvision = partsSalesOrder.IfDirectProvision,
                CustomerType = partsSalesOrder.CustomerType,
                SecondLevelOrderType = partsSalesOrder.SecondLevelOrderType,
                Remark = partsSalesOrder.Remark,
                ContactPerson = partsSalesOrder.ContactPerson,
                ContactPhone = partsSalesOrder.ContactPhone,
                ReceivingCompanyId = partsSalesOrder.ReceivingCompanyId,
                ReceivingCompanyCode = partsSalesOrder.ReceivingCompanyCode,
                ReceivingCompanyName = partsSalesOrder.ReceivingCompanyName,
                ShippingMethod = partsSalesOrder.ShippingMethod,
                ReceivingWarehouseId = partsSalesOrder.ReceivingWarehouseId,
                ReceivingWarehouseName = partsSalesOrder.ReceivingWarehouseName,
                ReceivingAddress = partsSalesOrder.ReceivingAddress,
                OriginalRequirementBillId = partsSalesOrder.Id,
                OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.配件销售订单,
                Status = (int)DcsPartsSalesOrderStatus.新增

            };
            foreach(var partsSalesOrderDetail in partsSalesOrder.PartsSalesOrderDetails.Where(p => p.OrderedQuantity != p.ApproveQuantity.Value && p.ApproveQuantity.HasValue)) {
                var newpartsSalesOrderDetail = new PartsSalesOrderDetail {
                    SparePartId = partsSalesOrderDetail.SparePartId,
                    SparePartCode = partsSalesOrderDetail.SparePartCode,
                    SparePartName = partsSalesOrderDetail.SparePartName,
                    MeasureUnit = partsSalesOrderDetail.MeasureUnit,
                    OrderedQuantity = (int)(partsSalesOrderDetail.OrderedQuantity - partsSalesOrderDetail.ApproveQuantity),
                    OriginalPrice = partsSalesOrderDetail.OriginalPrice,
                    CustOrderPriceGradeCoefficient = partsSalesOrderDetail.CustOrderPriceGradeCoefficient,
                    IfCanNewPart = partsSalesOrderDetail.IfCanNewPart,
                    DiscountedPrice = partsSalesOrderDetail.DiscountedPrice,
                    OrderPrice = partsSalesOrderDetail.OrderPrice,
                    OrderSum = (int)(partsSalesOrderDetail.OrderedQuantity - partsSalesOrderDetail.ApproveQuantity) * partsSalesOrderDetail.OrderPrice,
                    EstimatedFulfillTime = partsSalesOrderDetail.EstimatedFulfillTime,
                    Remark = partsSalesOrderDetail.Remark,
                    PriceTypeName = partsSalesOrderDetail.PriceTypeName,
                    ABCStrategy = partsSalesOrderDetail.ABCStrategy,
                    SalesPrice = partsSalesOrderDetail.SalesPrice
                };
                newPartsSalesOrder.PartsSalesOrderDetails.Add(newpartsSalesOrderDetail);
            }

            newPartsSalesOrder.TotalAmount = newPartsSalesOrder.PartsSalesOrderDetails.Sum(r => r.OrderSum);
            var cusAccount = DomainService.查询客户可用资金(newPartsSalesOrder.CustomerAccountId, null, null);
            if(cusAccount == null)
                throw new ValidationException(ErrorStrings.PartsSalesOrder_Validation11);

            if(newPartsSalesOrder.TotalAmount > cusAccount.UseablePosition) {
                newPartsSalesOrder.Remark = newPartsSalesOrder.Remark + ErrorStrings.PartsSalesOrder_Validation18;
            } else {
                提交配件(newPartsSalesOrder);
            }
            InsertToDatabase(newPartsSalesOrder);
            this.InsertPartsSalesOrderValidate1(newPartsSalesOrder);

            partsSalesOrder.Status = (int)DcsPartsSalesOrderStatus.终止;
            UpdateToDatabase(partsSalesOrder);
            this.UpdatePartsSalesOrderValidate(partsSalesOrder);
        }


        public void 修改销售订单类型1(PartsSalesOrder partsSalesOrder, PartsSalesOrderDetail[] partsSalesOrderDetails) {
            var dbpartsSalesOrder = ObjectContext.PartsSalesOrders.Where(c => c.Id == partsSalesOrder.Id && c.Status == (int)DcsPartsSalesOrderStatus.提交).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbpartsSalesOrder == null)
                throw new ValidationException(ErrorStrings.PartsSalesOrder_Validation5);
            var dbCustomerOrderPriceGrade = ObjectContext.CustomerOrderPriceGrades.SingleOrDefault(c => c.CustomerCompanyId == partsSalesOrder.InvoiceReceiveCompanyId && c.PartsSalesOrderTypeId == partsSalesOrder.PartsSalesOrderTypeId && c.Status == (int)DcsVehicleBaseDataStatus.有效);
            if(dbCustomerOrderPriceGrade == null) {
                throw new ValidationException(ErrorStrings.PartsSalesOrder_Validation9);
            }
            var dbPartsSalesOrderDetails = ObjectContext.PartsSalesOrderDetails.Where(r => r.PartsSalesOrderId == partsSalesOrder.Id).SetMergeOption(MergeOption.NoTracking).ToList();
            if(partsSalesOrderDetails.Any()) {
                foreach(var item in partsSalesOrderDetails) {
                    var dbPartsSalesOrderDetail = dbPartsSalesOrderDetails.FirstOrDefault(r => r.Id == item.Id);
                    if(dbPartsSalesOrderDetail == null)
                        continue;
                    dbPartsSalesOrderDetail = item;
                    dbPartsSalesOrderDetail.CustOrderPriceGradeCoefficient = dbCustomerOrderPriceGrade.Coefficient;
                    dbPartsSalesOrderDetail.OrderPrice = item.OrderPrice;
                    dbPartsSalesOrderDetail.OrderSum = item.OrderPrice * item.OrderedQuantity;
                    UpdateToDatabase(dbPartsSalesOrderDetail);
                }
            }
            partsSalesOrder.TotalAmount = dbPartsSalesOrderDetails.Sum(r => r.OrderSum);
            partsSalesOrder.RowVersion = dbpartsSalesOrder.RowVersion;
            UpdateToDatabase(partsSalesOrder);
            this.UpdatePartsSalesOrderValidate(partsSalesOrder);
            ObjectContext.SaveChanges();
        }


        public bool 校验销售订单库存满足(int ownerCompanyId, List<PartsSalesOrderDetail> list) {
            var listSparePartIds = list.Select(r => r.SparePartId).ToArray();
            var lists = list.Select(r => new {
                r.SparePartId,
                r.OrderedQuantity
            }).ToArray();
            var partIds = ObjectContext.PartsBranches.Where(r => listSparePartIds.Contains(r.PartId) && r.IsSalable == true).Select(r => r.PartId).ToArray();
            if(partIds.Length == 0) {
                return false;
            }
            var companyPartsStocks = DomainService.查询企业库存(ownerCompanyId, partIds);
            var checkResult = companyPartsStocks.FirstOrDefault(r => lists.Any(v => v.SparePartId == r.SparePartId && v.OrderedQuantity > r.UsableQuantity));
            return checkResult == null;
        }


        public void 维修单生成紧急销售订单(PartsSalesOrder partsSalesOrder) {
            //这个方法作用是生成配件销售订单以后，回头修改维修单上的一个属性
            var tempId = partsSalesOrder.SourceBillId;
        }


        public void 销售订单提报(PartsSalesOrder partsSalesOrder, int[] preOrderIds) {
            var company = ObjectContext.Companies.FirstOrDefault(o => o.Id == partsSalesOrder.ReceivingCompanyId && (o.Type == (int)DcsCompanyType.代理库 || o.Type == (int)DcsCompanyType.服务站兼代理库));
            if(company != null && !partsSalesOrder.ReceivingWarehouseId.HasValue) {
                throw new ValidationException(ErrorStrings.PartsSalesOrder_Validation14);
            }
            if(preOrderIds.Length != 0) {
                var preOrders = ObjectContext.PreOrders.Where(r => preOrderIds.Contains(r.Id) && r.Status == (int)DcsPreOrderStatus.新建).ToArray();
                foreach(var preOrder in preOrders) {
                    preOrder.Status = (int)DcsPreOrderStatus.已使用;
                    UpdateToDatabase(preOrder);
                }
            }
            if(partsSalesOrder.EntityState == EntityState.Added) {
                InsertToDatabase(partsSalesOrder);
            }
            if(partsSalesOrder.EntityState == EntityState.Modified) {
                UpdateToDatabase(partsSalesOrder);
            }
        }


        public Dictionary<int, int> 校验销售清单品牌库存(int[] ids) {
            var dictionary = new Dictionary<int, int>();
            foreach(var id in ids) {
                var partssalesorder = this.ObjectContext.PartsSalesOrders.FirstOrDefault(r => r.Id == id);
                var storagecompanyId = partssalesorder.SalesUnitOwnerCompanyId;
                //获取清单中的配件ID数组,订单数量
                var detail = this.ObjectContext.PartsSalesOrderDetails.Where(r => r.PartsSalesOrderId == id && (r.OrderedQuantity > (r.ApproveQuantity ?? 0)));
                var partsIds = detail.Select(r => r.SparePartId);
                var salesUnitIds = this.ObjectContext.SalesUnits.Where(r => r.PartsSalesCategoryId == partssalesorder.SalesCategoryId).Select(r => r.Id);
                var salesUnitAffiWarehouses = this.ObjectContext.SalesUnitAffiWarehouses.Where(r => r.SalesUnitId == partssalesorder.SalesUnitId && salesUnitIds.Contains(r.SalesUnitId)).Select(r => r.WarehouseId);
                var warehouse = this.ObjectContext.Warehouses.Where(r => salesUnitAffiWarehouses.Contains(r.Id) || r.IsCentralizedPurchase == true);
                var partsStockGroups = from partstock in ObjectContext.PartsStocks
                                       from warehouseAreaCategory in ObjectContext.WarehouseAreaCategories
                                       where (warehouseAreaCategory.Category == (int)DcsAreaType.检验区 || warehouseAreaCategory.Category == (int)DcsAreaType.保管区) && partstock.StorageCompanyId == storagecompanyId && warehouse.Select(e => e.Id).Contains(partstock.WarehouseId) && partsIds.Contains(partstock.PartId)
                                       && partstock.WarehouseAreaCategoryId == warehouseAreaCategory.Id
                                       group partstock by new {
                                           partstock.StorageCompanyId,
                                           //partstock.WarehouseId,
                                           partstock.BranchId,
                                           partstock.PartId
                                       }
                                           into tempTablePartStrock
                                           select new {
                                               tempTablePartStrock.Key.StorageCompanyId,
                                               //tempTablePartStrock.Key.WarehouseId,
                                               tempTablePartStrock.Key.BranchId,
                                               tempTablePartStrock.Key.PartId,
                                               allQuantity = tempTablePartStrock.Sum(r => r.Quantity)
                                           };
                var wmsCongelationStockViewGroups = from wmsCongelationStockView in ObjectContext.WmsCongelationStockViews //WMS仓库配件冻结库存（视图）
                                                    where wmsCongelationStockView.StorageCompanyId == storagecompanyId && partsIds.Contains(wmsCongelationStockView.SparePartId) && warehouse.Select(e => e.Id).Contains(wmsCongelationStockView.WarehouseId)
                                                    group wmsCongelationStockView by new {
                                                        wmsCongelationStockView.StorageCompanyId,
                                                        //wmsCongelationStockView.WarehouseId,
                                                        wmsCongelationStockView.BranchId,
                                                        wmsCongelationStockView.SparePartId
                                                    }
                                                        into tempWmsCongelationStockViews
                                                        select new {
                                                            tempWmsCongelationStockViews.Key.StorageCompanyId,
                                                            //tempWmsCongelationStockViews.Key.WarehouseId,
                                                            tempWmsCongelationStockViews.Key.BranchId,
                                                            tempWmsCongelationStockViews.Key.SparePartId,
                                                            allDisabledStock = tempWmsCongelationStockViews.Sum(r => r.DisabledStock),
                                                            allCongelationStockQty = tempWmsCongelationStockViews.Sum(r => r.CongelationStockQty)
                                                        };
                var partsLockedStockGroups = from partsLockedStock in ObjectContext.PartsLockedStocks//配件锁定库存
                                             where warehouse.Select(e => e.Id).Contains(partsLockedStock.WarehouseId)
                                             group partsLockedStock by new {
                                                 partsLockedStock.StorageCompanyId,
                                                 //partsLockedStock.WarehouseId,
                                                 partsLockedStock.BranchId,
                                                 partsLockedStock.PartId
                                             }
                                                 into tempPartsLockedStocks
                                                 select new {
                                                     tempPartsLockedStocks.Key.StorageCompanyId,
                                                     //tempPartsLockedStocks.Key.WarehouseId,
                                                     tempPartsLockedStocks.Key.BranchId,
                                                     SparePartId = tempPartsLockedStocks.Key.PartId,
                                                     allLockedQuantity = tempPartsLockedStocks.Sum(r => r.LockedQuantity),
                                                 };
                //2、查询配件库存、查询配件锁定库存、查询WMS配件冻结库存、查询库WMS配件不可用存
                //        配件库存过滤：配件库存.库位Id 库区用途为保管区或检验区 
                //        根据 配件库存.仓储企业Id、配件库存.营销分公司Id、配件库存.配件Id 分组 
                //        返回 sum(配件库存.数量)、sum(配件锁定库存.锁定数量)、sum(WMS仓库配件冻结库存（视图）.冻结库存)、
                //        sum(WMS仓库配件冻结库存（视图）.不可用库存)
                //        配件库存.仓储企业Id=参数 企业Id
                //        配件库存.配件Id in {参数 配件Id数组}
                //        配件库存.仓库id in {步骤1 仓库Id数组}
                //        配件锁定库存.仓库Id=配件库存.仓库Id，配件锁定库存.配件Id=配件库存.配件Id
                //       WMS仓库配件冻结库存（视图）.仓库Id=配件库存.仓库Id，WMS仓库配件冻结库存（视图）.配件Id=配件库存.配件Id
                var tempResultQuery = from partsStockGroup in partsStockGroups
                                      join wmsCongelationStockViewGroup in wmsCongelationStockViewGroups on new {
                                          partId = partsStockGroup.PartId
                                          //warehouseId = partsStockGroup.WarehouseId
                                      } equals new {
                                          partId = wmsCongelationStockViewGroup.SparePartId
                                          //warehouseId = wmsCongelationStockViewGroup.WarehouseId
                                      } into tempTableWmsCongelationStockViewGroups
                                      from a in tempTableWmsCongelationStockViewGroups.DefaultIfEmpty()
                                      join partsLockedStockGroup in partsLockedStockGroups // 配件锁定库存 
                                          on new {
                                              partId = partsStockGroup.PartId,
                                              //warehouseId = partsStockGroup.WarehouseId
                                          } equals new {
                                              partId = partsLockedStockGroup.SparePartId,
                                              //warehouseId = partsLockedStockGroup.WarehouseId
                                          } into tempTablePartsLockedStockGroups
                                      from b in tempTablePartsLockedStockGroups.DefaultIfEmpty()
                                      join sparePart in ObjectContext.SpareParts.Where(r => r.Status == (int)DcsMasterDataStatus.有效) on partsStockGroup.PartId equals sparePart.Id into tempSpareParts
                                      from c in tempSpareParts.DefaultIfEmpty()
                                      select new {
                                          partsStockGroup.StorageCompanyId,
                                          SparePartId = partsStockGroup.PartId,
                                          SparePartCode = c.Code,
                                          SparePartName = c.Name,
                                          Quantity = (int?)partsStockGroup.allQuantity,//sum(配件库存.数量)
                                          AllDisabledStock = (int?)a.allDisabledStock,//sum(WMS仓库配件冻结库存（视图）.不可用库存)
                                          AllCongelationStockQty = (int?)a.allCongelationStockQty,//sum(WMS仓库配件冻结库存（视图）.冻结库存)
                                          AllLockedQuantity = (int?)b.allLockedQuantity,//sum(配件锁定库存.锁定数量)
                                      };
                var result = tempResultQuery.Select(r => new WarehousePartsStock {
                    StorageCompanyId = r.StorageCompanyId,
                    SparePartId = r.SparePartId,
                    SparePartCode = r.SparePartCode,
                    SparePartName = r.SparePartName,
                    Quantity = r.Quantity ?? 0,
                    UsableQuantity = (r.Quantity ?? 0) - (r.AllDisabledStock ?? 0) - (r.AllCongelationStockQty ?? 0) - (r.AllLockedQuantity ?? 0)
                });

                //4、返回数据【选中单据id，是否选项】数组
                //如果步骤3返回的企业库存配件品种中至少有1种不满足配件销售订单清单的需要则返回【选中单据id,1]，
                //企业库存配件品种中所有都不满足配件销售订单清单的则返回【选中单据id,3]
                //否则返回【选中单据id，2】
                //if(detail.Any(a => result.Any(r => r.UsableQuantity >= a.OrderedQuantity - (a.ApproveQuantity ?? 0) && a.SparePartId == r.SparePartId))
                //    && (detail.Any(a => result.Any(r => r.UsableQuantity < a.OrderedQuantity - (a.ApproveQuantity ?? 0) && a.SparePartId == r.SparePartId)) || (detail.Any(a => !result.Any(r => a.SparePartId == r.SparePartId))))) { 
                //    dictionary.Add(id, 1);
                //}else if(!(detail.Any(a => result.Any(r => r.UsableQuantity >= a.OrderedQuantity - (a.ApproveQuantity ?? 0) && a.SparePartId == r.SparePartId)))) {
                //    dictionary.Add(id, 3);
                //}else {
                //    dictionary.Add(id, 2);
                //}

                if(!(detail.Any(a => result.Any(r => r.UsableQuantity > 0 && a.SparePartId == r.SparePartId)))) {
                    dictionary.Add(id, 3); //白
                } else if(detail.All(a => result.Any(r => r.UsableQuantity >= a.OrderedQuantity - (a.ApproveQuantity ?? 0) && a.SparePartId == r.SparePartId))) {
                    dictionary.Add(id, 2); //绿
                } else {
                    dictionary.Add(id, 1); //蓝
                }
                //int blue = 0;
                //int green = 0;
                //foreach(var d in detail) {
                //    foreach(var r in result) {
                //        if(d.SparePartId == r.SparePartId) {
                //            if(r.UsableQuantity - (d.OrderedQuantity - (d.ApproveQuantity ?? 0))>=0) {
                //                green++;
                //            } else if((d.OrderedQuantity - (d.ApproveQuantity ?? 0)) - r.UsableQuantity < (d.OrderedQuantity - (d.ApproveQuantity ?? 0)))
                //            {
                //                blue++;
                //            }
                //        }
                //    }
                //}

                //if(blue > 0) {
                //   dictionary.Add(id, 1); //蓝
                //} else if(green == detail.Count()) {
                //    dictionary.Add(id, 2); //绿
                //} else {
                //    dictionary.Add(id, 3); //白
                //}
            }
            #region
            ////    1、仓库的过滤条件：
            ////    1)本品牌所有仓库（以下获取的仓库ID）：
            ////        配件销售订单.配件销售类型id = 销售组织.配件销售类型id
            ////        配件销售订单.销售组织ID = 销售组织仓库关系.销售组织Id
            ////        销售组织仓库关系.配件仓库Id = 仓库.ID
            ////    2)统购仓库： 
            ////        仓库.是否统购分销=是
            ////    3）以上获取的仓库合计就是所有符合条件的仓库集合
            //var partssalesoreder = list.Select(r => r.PartsSalesOrder).ToArray();
            //foreach(var item in partssalesoreder) {
            //    var salesUnitIds = this.ObjectContext.SalesUnits.Where(r => r.PartsSalesCategoryId == item.PartsSalesOrderTypeId).Select(r => r.Id);
            //    var salesUnitAffiWarehouses = this.ObjectContext.SalesUnitAffiWarehouses.Where(r => r.SalesUnitId == item.SalesUnitId && salesUnitIds.Contains(r.SalesUnitId)).Select(r => r.WarehouseId);
            //    var warehouse = this.ObjectContext.Warehouses.Where(r => salesUnitAffiWarehouses.Contains(r.Id) || r.IsCentralizedPurchase == true);

            //    //获取清单中的配件ID数组
            //    var partsIds = item.PartsSalesOrderDetails.Select(r => r.SparePartId).ToArray();
            //    var partsStockGroups = from partstock in ObjectContext.PartsStocks
            //                           from warehouseAreaCategory in ObjectContext.WarehouseAreaCategories
            //                           where (warehouseAreaCategory.Category == (int)DcsAreaType.检验区 || warehouseAreaCategory.Category == (int)DcsAreaType.保管区) && partstock.StorageCompanyId == storagecompanyId && warehouse.Select(e => e.Id).Contains(partstock.WarehouseId) && partsIds.Contains(partstock.PartId)
            //                           && partstock.WarehouseAreaCategoryId == warehouseAreaCategory.Id
            //                           group partstock by new {
            //                               partstock.StorageCompanyId,
            //                               partstock.WarehouseId,
            //                               partstock.BranchId,
            //                               partstock.PartId
            //                           }
            //                               into tempTablePartStrock
            //                               select new {
            //                                   tempTablePartStrock.Key.StorageCompanyId,
            //                                   tempTablePartStrock.Key.WarehouseId,
            //                                   tempTablePartStrock.Key.BranchId,
            //                                   tempTablePartStrock.Key.PartId,
            //                                   allQuantity = tempTablePartStrock.Sum(r => r.Quantity)
            //                               };
            //    var wmsCongelationStockViewGroups = from wmsCongelationStockView in ObjectContext.WmsCongelationStockViews //WMS仓库配件冻结库存（视图）
            //                                        where wmsCongelationStockView.StorageCompanyId == storagecompanyId && partsIds.Contains(wmsCongelationStockView.SparePartId)
            //                                        group wmsCongelationStockView by new {
            //                                            wmsCongelationStockView.StorageCompanyId,
            //                                            wmsCongelationStockView.WarehouseId,
            //                                            wmsCongelationStockView.BranchId,
            //                                            wmsCongelationStockView.SparePartId
            //                                        }
            //                                            into tempWmsCongelationStockViews
            //                                            select new {
            //                                                tempWmsCongelationStockViews.Key.StorageCompanyId,
            //                                                tempWmsCongelationStockViews.Key.WarehouseId,
            //                                                tempWmsCongelationStockViews.Key.BranchId,
            //                                                tempWmsCongelationStockViews.Key.SparePartId,
            //                                                allDisabledStock = tempWmsCongelationStockViews.Sum(r => r.DisabledStock),
            //                                                allCongelationStockQty = tempWmsCongelationStockViews.Sum(r => r.CongelationStockQty)
            //                                            };
            //    var partsLockedStockGroups = from partsLockedStock in ObjectContext.PartsLockedStocks//配件锁定库存
            //                                 group partsLockedStock by new {
            //                                     partsLockedStock.StorageCompanyId,
            //                                     partsLockedStock.WarehouseId,
            //                                     partsLockedStock.BranchId,
            //                                     partsLockedStock.PartId
            //                                 }
            //                                     into tempPartsLockedStocks
            //                                     select new {
            //                                         tempPartsLockedStocks.Key.StorageCompanyId,
            //                                         tempPartsLockedStocks.Key.WarehouseId,
            //                                         tempPartsLockedStocks.Key.BranchId,
            //                                         SparePartId = tempPartsLockedStocks.Key.PartId,
            //                                         allLockedQuantity = tempPartsLockedStocks.Sum(r => r.LockedQuantity),
            //                                     };
            //    //2、查询配件库存、查询配件锁定库存、查询WMS配件冻结库存、查询库WMS配件不可用存
            //    //        配件库存过滤：配件库存.库位Id 库区用途为保管区或检验区 
            //    //        根据 配件库存.仓储企业Id、配件库存.营销分公司Id、配件库存.配件Id 分组 
            //    //        返回 sum(配件库存.数量)、sum(配件锁定库存.锁定数量)、sum(WMS仓库配件冻结库存（视图）.冻结库存)、
            //    //        sum(WMS仓库配件冻结库存（视图）.不可用库存)
            //    //        配件库存.仓储企业Id=参数 企业Id
            //    //        配件库存.配件Id in {参数 配件Id数组}
            //    //        配件库存.仓库id in {步骤1 仓库Id数组}
            //    //        配件锁定库存.仓库Id=配件库存.仓库Id，配件锁定库存.配件Id=配件库存.配件Id
            //    //       WMS仓库配件冻结库存（视图）.仓库Id=配件库存.仓库Id，WMS仓库配件冻结库存（视图）.配件Id=配件库存.配件Id
            //    var tempResultQuery = from partsStockGroup in partsStockGroups
            //                          join wmsCongelationStockViewGroup in wmsCongelationStockViewGroups on new {
            //                              partId = partsStockGroup.PartId,
            //                              warehouseId = partsStockGroup.WarehouseId
            //                          }equals new {
            //                              partId = wmsCongelationStockViewGroup.SparePartId,
            //                              warehouseId = wmsCongelationStockViewGroup.WarehouseId
            //                          }into tempTableWmsCongelationStockViewGroups
            //                          from a in tempTableWmsCongelationStockViewGroups.DefaultIfEmpty()
            //                          join partsLockedStockGroup in partsLockedStockGroups // 配件锁定库存 
            //                              on new {
            //                                  partId = partsStockGroup.PartId,
            //                                  warehouseId = partsStockGroup.WarehouseId
            //                              }equals new {
            //                                  partId = partsLockedStockGroup.SparePartId,
            //                                  warehouseId = partsLockedStockGroup.WarehouseId
            //                              }into tempTablePartsLockedStockGroups
            //                          from b in tempTablePartsLockedStockGroups.DefaultIfEmpty()
            //                          join sparePart in ObjectContext.SpareParts.Where(r => r.Status == (int)DcsMasterDataStatus.有效) on partsStockGroup.PartId equals sparePart.Id into tempSpareParts
            //                          from c in tempSpareParts.DefaultIfEmpty()
            //                          select new {
            //                              partsStockGroup.StorageCompanyId,
            //                              SparePartId = partsStockGroup.PartId,
            //                              SparePartCode = c.Code,
            //                              SparePartName = c.Name,
            //                              Quantity = partsStockGroup.allQuantity,//sum(配件库存.数量)
            //                              AllDisabledStock = a.allDisabledStock,//sum(WMS仓库配件冻结库存（视图）.不可用库存)
            //                              AllCongelationStockQty = a.allCongelationStockQty,//sum(WMS仓库配件冻结库存（视图）.冻结库存)
            //                              AllLockedQuantity = b.allLockedQuantity,//sum(配件锁定库存.锁定数量)
            //                          };
            //    //3、返回字段：
            //    //        1)企业Id=配件库存.仓储企业Id
            //    //        2)配件Id=配件库存. 配件Id
            //    //        3)配件编号=配件信息. 配件编号（配件信息.Id=配件库存. 配件Id）
            //    //        4)配件名称=配件信息. 配件名称（配件信息.Id=配件库存. 配件Id）
            //    //        5)实际库存= sum(配件库存.数量)
            //    //        6)可用库存= sum(配件库存.数量)-sum(配件锁定库存.锁定数量)-sum(WMS仓库配件冻结库存（视图）.冻结库存)-sum(WMS仓库配件冻结库存（视图）.不可用库存)
            //    var result = tempResultQuery.Select(r => new WarehousePartsStock {
            //        StorageCompanyId = r.StorageCompanyId,
            //        SparePartId = r.SparePartId,
            //        SparePartCode = r.SparePartCode,
            //        SparePartName = r.SparePartName,
            //        Quantity = r != null ? r.Quantity : 0,
            //        UsableQuantity = (r != null ? r.Quantity : 0) - (r != null ? r.AllDisabledStock : 0) - (r != null ? r.AllCongelationStockQty : 0) - (r != null ? r.AllLockedQuantity : 0)
            //    });
            //    //4、如果步骤3返回的企业库存配件品种中至少有1种不满足配件销售订单清单的需要则返回FALSE
            //    foreach(var detail in item.PartsSalesOrderDetails) {
            //        var results = result.Any(r => r.UsableQuantity < detail.OrderedQuantity && r.SparePartId == detail.SparePartId);
            //        return results;
            //    }
            //}
            #endregion
            return dictionary;
        }


        public void 配件公司终止配件销售订单(PartsSalesOrder partsSalesOrder) {
            //1、合法性校验：状态=提交/部分审批
            //1、更新配件销售订单，状态=终止
            var dbpartsSalesOrder = ObjectContext.PartsSalesOrders.Where(r => r.Id == partsSalesOrder.Id && (r.Status == (int)DcsPartsSalesOrderStatus.提交 || r.Status == (int)DcsPartsSalesOrderStatus.部分审批)).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbpartsSalesOrder == null)
                throw new ValidationException(ErrorStrings.PartsSalesOrder_Validation5);
            partsSalesOrder.Status = (int)DcsPartsSalesOrderStatus.终止;
            UpdateToDatabase(partsSalesOrder);
            this.UpdatePartsSalesOrderValidate(partsSalesOrder);
            //查询分公司与数据库关联关系（分公司与数据库关联关系.分公司编码=配件销售订单.收票企业编号）查询到数据
            //更新对应 分公司与数据库关联关系.数据库 数据库对应的配件销售订单（源配件销售订单.编号=配件销售订单.原始销售订单编号） 状态=终止，
            //释放对应配件销售订单 客户账户的审批待发金额（客户账户.id=原配件销售订单.客户账户id) 
            //（如 分公司与数据库关联关系.数据库=dcs 则更新的是DCS库下的的配件销售订单 和 客户账户）释放金额为销售订单清单（订货量-审批量）*订货价格
            var branchDateBaseRel = this.ObjectContext.BranchDateBaseRels.FirstOrDefault(r => r.BranchCode == partsSalesOrder.InvoiceReceiveCompanyCode);
            if(branchDateBaseRel == null)
                throw new ValidationException(string.Format(ErrorStrings.BranchDateBaseRel_Validation1, partsSalesOrder.InvoiceReceiveCompanyCode));
            var money = partsSalesOrder.PartsSalesOrderDetails.Sum(r => (r.OrderedQuantity - (r.ApproveQuantity ?? 0)) * r.OrderPrice);
            var salesOrderBillQuantity = new Devart.Data.Oracle.OracleParameter("SalesOrderBillQuantity", Devart.Data.Oracle.OracleDbType.Integer, System.Data.ParameterDirection.Output);
            ObjectContext.ExecuteStoreCommand("begin :SalesOrderBillQuantity:=UpdateSalesOrderForDataBase(:FSourceBillCode, :FDateBase,:FMoney); end;",
                new Devart.Data.Oracle.OracleParameter("FSourceBillCode", partsSalesOrder.SourceBillCode),
                new Devart.Data.Oracle.OracleParameter("FDateBase", branchDateBaseRel.DateBase.ToUpper()),
                new Devart.Data.Oracle.OracleParameter("FMoney", money),
                salesOrderBillQuantity
                );
        }

        public void 新增配件销售订单代做(PartsSalesOrder partsSalesOrder, List<PartsSalesOrderDetail> partsSalesOrderDetails) {
            if(partsSalesOrder.PartsSalesOrderTypeName != "紧急订单" && partsSalesOrder.PartsSalesOrderTypeName != "特急订单" && partsSalesOrder.PartsSalesOrderTypeName != "事故订单" && partsSalesOrder.IfDirectProvision) {
                this.校验订货配件起订金额(partsSalesOrder, partsSalesOrderDetails.ToArray());
            }
            // 4
            var allowCustomerCredit = ObjectContext.SalesCenterstrategies.FirstOrDefault(r => r.PartsSalesCategoryId == partsSalesOrder.SalesCategoryId);
            if(allowCustomerCredit == null) {
                throw new ValidationException(ErrorStrings.PartsSalesOrder_Validation16);
            } else if(allowCustomerCredit.AllowCustomerCredit.HasValue && !allowCustomerCredit.AllowCustomerCredit.Value) {
                var customerAccount = DomainService.查询客户可用资金(partsSalesOrder.CustomerAccountId, null, null);
                //由于查询客户可用资金方法查客户账户时只传CustomerAccountId不会查返利金额
                decimal rebateAmout = 0;
                var salesUnit = ObjectContext.SalesUnits.FirstOrDefault(v => v.Id == partsSalesOrder.SalesUnitId && v.Status == (int)DcsMasterDataStatus.有效);
                if(salesUnit == null)
                    throw new ValidationException(string.Format(ErrorStrings.PartsSalesOrderProcess_Validation9, partsSalesOrder.Code));
                var partsRebateAccount = ObjectContext.PartsRebateAccounts.FirstOrDefault(r => r.BranchId == partsSalesOrder.SalesUnitOwnerCompanyId && r.CustomerCompanyId == partsSalesOrder.SubmitCompanyId && r.AccountGroupId == salesUnit.AccountGroupId);
                if(partsRebateAccount != null) {
                    rebateAmout = partsRebateAccount.AccountBalanceAmount;
                }
                if((customerAccount == null) || (customerAccount.UseablePosition + rebateAmout < partsSalesOrder.TotalAmount)) {
                    throw new ValidationException(ErrorStrings.PartsSalesOrder_Validation17);
                }
            }

            //订单类型 =非油品订单、是否直供=是
            //金税分类编码=1的配件拆分为“订单类型=油品订单，是否直供为是“的直供订单
            if(partsSalesOrder.IfDirectProvision && "油品订单" != partsSalesOrder.PartsSalesOrderTypeName) {
                //查询订单配件的金税分类编码
                var spIds = partsSalesOrderDetails.Select(r => r.SparePartId).ToArray();
                var spareparts = ObjectContext.SpareParts.Where(s => spIds.Contains(s.Id)).ToArray();
                var ypPartsSalesOrder = this.CreatePartsSalesOrder(partsSalesOrder);
                foreach(var item in spareparts) {
                    if(item.GoldenTaxClassifyCode == "1") {
                        var partsSalesOrderDetail = partsSalesOrderDetails.FirstOrDefault(r => r.SparePartId == item.Id);
                        partsSalesOrderDetails.Remove(partsSalesOrderDetail);
                        ypPartsSalesOrder.PartsSalesOrderDetails.Add(new PartsSalesOrderDetail() {
                            SparePartId = partsSalesOrderDetail.SparePartId,
                            SparePartCode = partsSalesOrderDetail.SparePartCode,
                            SparePartName = partsSalesOrderDetail.SparePartName,
                            MeasureUnit = partsSalesOrderDetail.MeasureUnit,
                            OrderedQuantity = partsSalesOrderDetail.OrderedQuantity,
                            ApproveQuantity = partsSalesOrderDetail.ApproveQuantity,
                            OriginalPrice = partsSalesOrderDetail.OriginalPrice,
                            CustOrderPriceGradeCoefficient = partsSalesOrderDetail.CustOrderPriceGradeCoefficient,
                            IfCanNewPart = partsSalesOrderDetail.IfCanNewPart,
                            DiscountedPrice = partsSalesOrderDetail.DiscountedPrice,
                            OrderPrice = partsSalesOrderDetail.OrderPrice,
                            OrderSum = partsSalesOrderDetail.OrderSum,
                            EstimatedFulfillTime = partsSalesOrderDetail.EstimatedFulfillTime,
                            Remark = partsSalesOrderDetail.Remark,
                            OverseasPartsFigure = partsSalesOrderDetail.OverseasPartsFigure,
                            OverseasProjectNumber = partsSalesOrderDetail.OverseasProjectNumber,
                            PriceTypeName = partsSalesOrderDetail.PriceTypeName,
                            ABCStrategy = partsSalesOrderDetail.ABCStrategy,
                            MInSaleingAmount = partsSalesOrderDetail.MInSaleingAmount,
                            SalesPrice = partsSalesOrderDetail.SalesPrice
                        });
                    }
                }
                if(ypPartsSalesOrder.PartsSalesOrderDetails.Any()) {
                    ypPartsSalesOrder.TotalAmount = ypPartsSalesOrder.PartsSalesOrderDetails.Sum(r => r.OrderSum);
                    ypPartsSalesOrder.IfDirectProvision = true;
                    ypPartsSalesOrder.Status = (int)DcsPartsSalesOrderStatus.提交;
                    var orderType = ObjectContext.PartsSalesOrderTypes.Where(r => r.Name == "油品订单").FirstOrDefault();
                    ypPartsSalesOrder.PartsSalesOrderTypeId = orderType.Id;
                    ypPartsSalesOrder.PartsSalesOrderTypeName = orderType.Name;
                    //  InsertToDatabase(ypPartsSalesOrder);

                    //获取dcs本地数据库销售订单id
                    string dcsconnstrnew = ConfigurationManager.ConnectionStrings["DcsServiceConn"].ConnectionString;
                    OracleConnection dcsoraConnew = new OracleConnection(dcsconnstrnew);
                    string dcsnextvaluesqlnew = "select s_partssalesorder.nextval as nextval from dual";
                    int dcsnextvaluenew = 0;
                    dcsoraConnew.Open();
                    OracleCommand dcscommandnew = new OracleCommand(dcsnextvaluesqlnew, dcsoraConnew);
                    var dcsreadernew = dcscommandnew.ExecuteReader();
                    if(dcsreadernew.Read()) {
                        dcsnextvaluenew = Convert.ToInt32(dcsreadernew["nextval"]);
                    }
                    dcsreadernew.Close();
                    dcsoraConnew.Close();
                    if(dcsnextvaluenew == 0) {
                        throw new ValidationException(ErrorStrings.PartsSalesOrder_Validation19);
                    }
                    ypPartsSalesOrder.Id = dcsnextvaluenew;
                    this.InsertPartsSalesOrderValidate(ypPartsSalesOrder);
                    foreach(var d in ypPartsSalesOrder.PartsSalesOrderDetails) {
                        d.PartsSalesOrderId = dcsnextvaluenew;
                    }
                    insertintosaleorder(ypPartsSalesOrder, ypPartsSalesOrder.PartsSalesOrderDetails.ToList(), ConfigurationManager.ConnectionStrings["DcsServiceConn"].ConnectionString);

                }
            }
            if(partsSalesOrderDetails.Any()) {
                //获取dcs本地数据库销售订单id
                string dcsconnstr = ConfigurationManager.ConnectionStrings["DcsServiceConn"].ConnectionString;
                OracleConnection dcsoraCon = new OracleConnection(dcsconnstr);
                string dcsnextvaluesql = "select s_partssalesorder.nextval as nextval from dual";
                int dcsnextvalue = 0;
                dcsoraCon.Open();
                OracleCommand dcscommand = new OracleCommand(dcsnextvaluesql, dcsoraCon);
                var dcsreader = dcscommand.ExecuteReader();
                if(dcsreader.Read()) {
                    dcsnextvalue = Convert.ToInt32(dcsreader["nextval"]);
                }
                dcsreader.Close();
                dcsoraCon.Close();
                if(dcsnextvalue == 0) {
                    throw new ValidationException(ErrorStrings.PartsSalesOrder_Validation19);
                }
                partsSalesOrder.Id = dcsnextvalue;
                partsSalesOrder.TotalAmount = partsSalesOrderDetails.Sum(r => r.OrderSum);
                partsSalesOrder.Status = (int)DcsPartsSalesOrderStatus.提交;
                this.InsertPartsSalesOrderValidate(partsSalesOrder);
                foreach(var d in partsSalesOrderDetails) {
                    d.PartsSalesOrderId = dcsnextvalue;
                }
                insertintosaleorder(partsSalesOrder, partsSalesOrderDetails, ConfigurationManager.ConnectionStrings["DcsServiceConn"].ConnectionString);
            }


            //如果销售订单.配件销售订单类型名称=急需订单或VOR订单 且销售组织隶属企业Id=营销分公司Id 新增立即执行自动审核订单待处理表
            //立即执行自动审核订单待处理表.订单id=配件销售订单.Id 
            //订单编号=配件销售订单.编号
            //状态=未处理
            //if(partsSalesOrder.SalesUnitOwnerCompanyId == partsSalesOrder.BranchId && (partsSalesOrder.PartsSalesOrderTypeName == "VOR订单" || partsSalesOrder.PartsSalesOrderTypeName == "急需订单")) {
            //    var autoSalesOrderProcess = new AutoSalesOrderProcess();
            //    autoSalesOrderProcess.OrderId = partsSalesOrder.Id;
            //    autoSalesOrderProcess.Code = partsSalesOrder.Code;
            //    autoSalesOrderProcess.Status = (int)DcsSyncStatus.未处理;
            //    InsertToDatabase(autoSalesOrderProcess);
            //}
            this.ObjectContext.SaveChanges();
        }

        internal void insertintosaleorder(PartsSalesOrder newOrder, List<PartsSalesOrderDetail> neworderdetails, String connstr) {
            OracleConnection oraCon = new OracleConnection(connstr);
            oraCon.Open();
            string sqlInsertdetail = new PartsSalesOrderAch(this.DomainService).GetMyInsertSql("PartsSalesOrderDetail", "Id", new[] {
"PartsSalesOrderId","SparePartId","SparePartCode","SparePartName","MeasureUnit","OrderedQuantity","OriginalPrice","CustOrderPriceGradeCoefficient","IfCanNewPart","OrderPrice","OrderSum","EstimatedFulfillTime","DiscountedPrice","Remark","ApproveQuantity","PriceTypeName","ABCStrategy","SalesPrice"
                            }, "S_PartsSalesOrderDetail.Nextval");
            foreach(var item in neworderdetails) {
                var detailcommand = new OracleCommand(sqlInsertdetail, oraCon);
                detailcommand.Parameters.Add(new OracleParameter("PartsSalesOrderId", item.PartsSalesOrderId));
                detailcommand.Parameters.Add(new OracleParameter("SparePartId", item.SparePartId));
                detailcommand.Parameters.Add(new OracleParameter("SparePartCode", item.SparePartCode));
                detailcommand.Parameters.Add(new OracleParameter("SparePartName", item.SparePartName));
                detailcommand.Parameters.Add(new OracleParameter("MeasureUnit", item.MeasureUnit));
                detailcommand.Parameters.Add(new OracleParameter("OrderedQuantity", item.OrderedQuantity));
                detailcommand.Parameters.Add(new OracleParameter("OriginalPrice", item.OriginalPrice));
                detailcommand.Parameters.Add(new OracleParameter("CustOrderPriceGradeCoefficient", item.CustOrderPriceGradeCoefficient));
                detailcommand.Parameters.Add(new OracleParameter("IfCanNewPart", item.IfCanNewPart));
                detailcommand.Parameters.Add(new OracleParameter("OrderPrice", item.OrderPrice));
                detailcommand.Parameters.Add(new OracleParameter("OrderSum", item.OrderSum));
                detailcommand.Parameters.Add(new OracleParameter("EstimatedFulfillTime", item.EstimatedFulfillTime));
                detailcommand.Parameters.Add(new OracleParameter("DiscountedPrice", item.DiscountedPrice));
                detailcommand.Parameters.Add(new OracleParameter("Remark", item.Remark));
                detailcommand.Parameters.Add(new OracleParameter("ApproveQuantity", item.ApproveQuantity));
                detailcommand.Parameters.Add(new OracleParameter("PriceTypeName", item.PriceTypeName));
                detailcommand.Parameters.Add(new OracleParameter("ABCStrategy", item.ABCStrategy));
                detailcommand.Parameters.Add(new OracleParameter("SalesPrice", item.SalesPrice));
                detailcommand.ExecuteNonQuery();
            }
            string sqlInsertM = new PartsSalesOrderAch(this.DomainService).GetMyInsertSql("PartsSalesOrder", null, new[] {"Id","Code","BranchId","BranchCode","BranchName","SalesCategoryId","SalesCategoryName","SalesUnitId","SalesUnitCode","SalesUnitName","SalesUnitOwnerCompanyId","SalesUnitOwnerCompanyCode","SalesUnitOwnerCompanyName","SourceBillCode","SourceBillId",
"CustomerAccountId","IsDebt","InvoiceReceiveCompanyId","InvoiceReceiveCompanyCode","InvoiceReceiveCompanyName","InvoiceReceiveCompanyType","InvoiceReceiveSaleCateId",
"InvoiceReceiveSaleCateName","SubmitCompanyId","SubmitCompanyCode","SubmitCompanyName","ProvinceID","Province","City","FirstClassStationId","FirstClassStationCode",
"FirstClassStationName","PartsSalesOrderTypeId","PartsSalesOrderTypeName","TotalAmount","IfAgencyService","SalesActivityDiscountRate","SalesActivityDiscountAmount",
"RequestedShippingTime","RequestedDeliveryTime","IfDirectProvision","CustomerType ","SecondLevelOrderType","Remark","ContactPerson","ContactPhone","ReceivingCompanyId",
"ReceivingCompanyCode","ReceivingCompanyName","ShippingMethod","ReceivingWarehouseId","ReceivingWarehouseName","ReceivingAddress","CreatorName","CreateTime",
"ModifierName","ModifyTime","SubmitterName","SubmitTime","Status","IsPurDistribution","IsTransSuccess","WarehouseId","CompanyAddressId","ERPSourceOrderCode","InnerSourceId","InnerSourceCode","IfInnerDirectProvision","InvoiceCustomerName","BankAccount","Taxpayeridentification","InvoiceAdress","InvoicePhone","InvoiceType","ReceivingMailbox","EnterpriseType","AutoApproveStatus"
                            }, null);
            var mcommand = new OracleCommand(sqlInsertM, oraCon);
            mcommand.Parameters.Add(new OracleParameter("Id", newOrder.Id));
            mcommand.Parameters.Add(new OracleParameter("Code", newOrder.Code));
            mcommand.Parameters.Add(new OracleParameter("BranchId", newOrder.BranchId));
            mcommand.Parameters.Add(new OracleParameter("BranchCode", newOrder.BranchCode));
            mcommand.Parameters.Add(new OracleParameter("BranchName", newOrder.BranchName));
            mcommand.Parameters.Add(new OracleParameter("SalesCategoryId", newOrder.SalesCategoryId));
            mcommand.Parameters.Add(new OracleParameter("SalesCategoryName", newOrder.SalesCategoryName));
            mcommand.Parameters.Add(new OracleParameter("SalesUnitId", newOrder.SalesUnitId));
            mcommand.Parameters.Add(new OracleParameter("SalesUnitCode", newOrder.SalesUnitCode));
            mcommand.Parameters.Add(new OracleParameter("SalesUnitName", newOrder.SalesUnitName));
            mcommand.Parameters.Add(new OracleParameter("SalesUnitOwnerCompanyId", newOrder.SalesUnitOwnerCompanyId));
            mcommand.Parameters.Add(new OracleParameter("SalesUnitOwnerCompanyCode", newOrder.SalesUnitOwnerCompanyCode));
            mcommand.Parameters.Add(new OracleParameter("SalesUnitOwnerCompanyName", newOrder.SalesUnitOwnerCompanyName));
            mcommand.Parameters.Add(new OracleParameter("SourceBillCode", newOrder.SourceBillCode));
            mcommand.Parameters.Add(new OracleParameter("SourceBillId", newOrder.SourceBillId));
            mcommand.Parameters.Add(new OracleParameter("CustomerAccountId", newOrder.CustomerAccountId));
            mcommand.Parameters.Add(new OracleParameter("IsDebt", newOrder.IsDebt));
            mcommand.Parameters.Add(new OracleParameter("InvoiceReceiveCompanyId", newOrder.InvoiceReceiveCompanyId));
            mcommand.Parameters.Add(new OracleParameter("InvoiceReceiveCompanyCode", newOrder.InvoiceReceiveCompanyCode));
            mcommand.Parameters.Add(new OracleParameter("InvoiceReceiveCompanyName", newOrder.InvoiceReceiveCompanyName));
            mcommand.Parameters.Add(new OracleParameter("InvoiceReceiveCompanyType", newOrder.InvoiceReceiveCompanyType));
            mcommand.Parameters.Add(new OracleParameter("InvoiceReceiveSaleCateId", newOrder.InvoiceReceiveSaleCateId));
            mcommand.Parameters.Add(new OracleParameter("InvoiceReceiveSaleCateName", newOrder.InvoiceReceiveSaleCateName));
            mcommand.Parameters.Add(new OracleParameter("SubmitCompanyId", newOrder.SubmitCompanyId));
            mcommand.Parameters.Add(new OracleParameter("SubmitCompanyCode", newOrder.SubmitCompanyCode));
            mcommand.Parameters.Add(new OracleParameter("SubmitCompanyName", newOrder.SubmitCompanyName));
            mcommand.Parameters.Add(new OracleParameter("ProvinceID", newOrder.ProvinceID));
            mcommand.Parameters.Add(new OracleParameter("Province", newOrder.Province));
            mcommand.Parameters.Add(new OracleParameter("City", newOrder.City));
            mcommand.Parameters.Add(new OracleParameter("FirstClassStationId", newOrder.FirstClassStationId));
            mcommand.Parameters.Add(new OracleParameter("FirstClassStationCode", newOrder.FirstClassStationCode));
            mcommand.Parameters.Add(new OracleParameter("FirstClassStationName", newOrder.FirstClassStationName));
            mcommand.Parameters.Add(new OracleParameter("PartsSalesOrderTypeId", newOrder.PartsSalesOrderTypeId));
            mcommand.Parameters.Add(new OracleParameter("PartsSalesOrderTypeName", newOrder.PartsSalesOrderTypeName));
            mcommand.Parameters.Add(new OracleParameter("TotalAmount", newOrder.TotalAmount));
            mcommand.Parameters.Add(new OracleParameter("IfAgencyService", newOrder.IfAgencyService));
            mcommand.Parameters.Add(new OracleParameter("SalesActivityDiscountRate", newOrder.SalesActivityDiscountRate));
            mcommand.Parameters.Add(new OracleParameter("SalesActivityDiscountAmount", newOrder.SalesActivityDiscountAmount));
            mcommand.Parameters.Add(new OracleParameter("RequestedShippingTime", newOrder.RequestedShippingTime));
            mcommand.Parameters.Add(new OracleParameter("RequestedDeliveryTime", newOrder.RequestedDeliveryTime));
            mcommand.Parameters.Add(new OracleParameter("IfDirectProvision", newOrder.IfDirectProvision));
            mcommand.Parameters.Add(new OracleParameter("CustomerType", newOrder.CustomerType));
            mcommand.Parameters.Add(new OracleParameter("SecondLevelOrderType", newOrder.SecondLevelOrderType));
            mcommand.Parameters.Add(new OracleParameter("Remark", newOrder.Remark));
            mcommand.Parameters.Add(new OracleParameter("ContactPerson", newOrder.ContactPerson));
            mcommand.Parameters.Add(new OracleParameter("ContactPhone", newOrder.ContactPhone));
            mcommand.Parameters.Add(new OracleParameter("ReceivingCompanyId", newOrder.ReceivingCompanyId));
            mcommand.Parameters.Add(new OracleParameter("ReceivingCompanyCode", newOrder.ReceivingCompanyCode));
            mcommand.Parameters.Add(new OracleParameter("ReceivingCompanyName", newOrder.ReceivingCompanyName));
            mcommand.Parameters.Add(new OracleParameter("ShippingMethod", newOrder.ShippingMethod));
            mcommand.Parameters.Add(new OracleParameter("ReceivingWarehouseId", newOrder.ReceivingWarehouseId));
            mcommand.Parameters.Add(new OracleParameter("ReceivingWarehouseName", newOrder.ReceivingWarehouseName));
            mcommand.Parameters.Add(new OracleParameter("ReceivingAddress", newOrder.ReceivingAddress));
            mcommand.Parameters.Add(new OracleParameter("CreatorName", newOrder.CreatorName));
            mcommand.Parameters.Add(new OracleParameter("CreateTime", newOrder.CreateTime));
            mcommand.Parameters.Add(new OracleParameter("ModifierName", newOrder.ModifierName));
            mcommand.Parameters.Add(new OracleParameter("ModifyTime", newOrder.ModifyTime));
            mcommand.Parameters.Add(new OracleParameter("SubmitterName", newOrder.SubmitterName));
            mcommand.Parameters.Add(new OracleParameter("SubmitTime", newOrder.SubmitTime));
            mcommand.Parameters.Add(new OracleParameter("Status", newOrder.Status));
            mcommand.Parameters.Add(new OracleParameter("IsPurDistribution", 0));
            mcommand.Parameters.Add(new OracleParameter("IsTransSuccess", newOrder.IsTransSuccess));
            mcommand.Parameters.Add(new OracleParameter("WarehouseId", newOrder.WarehouseId));
            mcommand.Parameters.Add(new OracleParameter("CompanyAddressId", newOrder.CompanyAddressId));
            mcommand.Parameters.Add(new OracleParameter("ERPSourceOrderCode", newOrder.ERPSourceOrderCode));
            mcommand.Parameters.Add(new OracleParameter("InnerSourceId", newOrder.InnerSourceId));
            mcommand.Parameters.Add(new OracleParameter("InnerSourceCode", newOrder.InnerSourceCode));
            mcommand.Parameters.Add(new OracleParameter("IfInnerDirectProvision", newOrder.IfInnerDirectProvision));
            mcommand.Parameters.Add(new OracleParameter("InvoiceCustomerName", newOrder.InvoiceCustomerName));
            mcommand.Parameters.Add(new OracleParameter("BankAccount", newOrder.BankAccount));
            mcommand.Parameters.Add(new OracleParameter("Taxpayeridentification", newOrder.Taxpayeridentification));
            mcommand.Parameters.Add(new OracleParameter("InvoiceAdress", newOrder.InvoiceAdress));
            mcommand.Parameters.Add(new OracleParameter("InvoicePhone", newOrder.InvoicePhone));
            mcommand.Parameters.Add(new OracleParameter("InvoiceType", newOrder.InvoiceType));
            mcommand.Parameters.Add(new OracleParameter("ReceivingMailbox", newOrder.ReceivingMailbox));
            mcommand.Parameters.Add(new OracleParameter("EnterpriseType", newOrder.EnterpriseType));
            mcommand.Parameters.Add(new OracleParameter("AutoApproveStatus", newOrder.AutoApproveStatus));
            mcommand.ExecuteNonQuery();
            oraCon.Close();
        }


        internal void 提交配件(PartsSalesOrder partsSalesOrder) {
            var partsSalesOrderDetails = partsSalesOrder.PartsSalesOrderDetails;
            var sparePartIds = partsSalesOrderDetails.Select(r => r.SparePartId).ToArray();
            var virtualPartsSalesPrices = new VirtualPartsSalesPriceAch(this.DomainService).根据配件清单获取销售价格(partsSalesOrder.PartsSalesOrderTypeId, partsSalesOrder.SubmitCompanyId, sparePartIds).ToArray();

            foreach(var partsSalesOrderDetail in partsSalesOrderDetails) {
                var virtualPartsSalesPrice = virtualPartsSalesPrices.FirstOrDefault(r => r.SparePartId == partsSalesOrderDetail.SparePartId);
                if(virtualPartsSalesPrice == null)
                    throw new ValidationException(string.Format(ErrorStrings.PartsSalesOrder_Validation3, partsSalesOrder.PartsSalesOrderTypeName, partsSalesOrderDetail.SparePartCode));
                partsSalesOrderDetail.OriginalPrice = virtualPartsSalesPrice.PartsSalesPrice;

                partsSalesOrderDetail.CustOrderPriceGradeCoefficient = virtualPartsSalesPrice.Coefficient;

                partsSalesOrderDetail.OrderPrice = virtualPartsSalesPrice.Price;
                partsSalesOrderDetail.OrderSum = partsSalesOrderDetail.OrderPrice * partsSalesOrderDetail.OrderedQuantity;
            }
            partsSalesOrder.TotalAmount = partsSalesOrderDetails.Sum(r => r.OrderSum);
            var userInfo = Utils.GetCurrentUserInfo();
            partsSalesOrder.Status = (int)DcsPartsSalesOrderStatus.提交;
            partsSalesOrder.SubmitterId = userInfo.Id;
            partsSalesOrder.SubmitterName = userInfo.Name;
            partsSalesOrder.SubmitTime = DateTime.Now;

            // 4
            var allowCustomerCredit = ObjectContext.SalesCenterstrategies.FirstOrDefault(r => r.PartsSalesCategoryId == partsSalesOrder.SalesCategoryId);
            if(allowCustomerCredit == null) {
                throw new ValidationException(ErrorStrings.PartsSalesOrder_Validation16);
            } else if(allowCustomerCredit.AllowCustomerCredit.HasValue && !allowCustomerCredit.AllowCustomerCredit.Value) {
                var customerAccount = DomainService.查询客户可用资金(partsSalesOrder.CustomerAccountId, null, null);
                //由于查询客户可用资金方法查客户账户时只传CustomerAccountId不会查返利金额
                decimal rebateAmout = 0;
                var salesUnit = ObjectContext.SalesUnits.FirstOrDefault(v => v.Id == partsSalesOrder.SalesUnitId && v.Status == (int)DcsMasterDataStatus.有效);
                if(salesUnit == null)
                    throw new ValidationException(string.Format(ErrorStrings.PartsSalesOrderProcess_Validation9, partsSalesOrder.Code));
                var partsRebateAccount = ObjectContext.PartsRebateAccounts.FirstOrDefault(r => r.BranchId == partsSalesOrder.SalesUnitOwnerCompanyId && r.CustomerCompanyId == partsSalesOrder.SubmitCompanyId && r.AccountGroupId == salesUnit.AccountGroupId);
                if(partsRebateAccount != null) {
                    rebateAmout = partsRebateAccount.AccountBalanceAmount;
                }
                if((customerAccount == null) || (customerAccount.UseablePosition + rebateAmout < partsSalesOrder.TotalAmount)) {
                    throw new ValidationException(ErrorStrings.PartsSalesOrder_Validation17);
                }
            }
            //5
            //销售组织隶属企业对应企业.企业类型=分公司 (配件销售订单.销售组织隶属企业Id=企业.企业id)
            //且分公司策略.统购分销策略=纳入统购分销 (分公司策略.分公司id=配件销售订单.销售组织隶属企业Id)
            //查询配件信息（配件信息.配件编号 in 原库.配件销售订单清单（原).配件编号数组）

            var company = ObjectContext.Companies.FirstOrDefault(r => r.Id == partsSalesOrder.SalesUnitOwnerCompanyId);
            if(company.Type == (int)DcsCompanyType.分公司) {
                var branchstrategy = ObjectContext.Branchstrategies.FirstOrDefault(r => r.BranchId == partsSalesOrder.SalesUnitOwnerCompanyId);
                if(branchstrategy.PurDistributionStrategy == 1) {

                    var codes = partsSalesOrderDetails.Select(r => r.SparePartCode).Distinct().ToList();

                    string connstr = ConfigurationManager.ConnectionStrings["SpanDcsServiceConn"].ConnectionString;
                    //string connstr = "Data Source=FTDCS_192.168.17.168;User ID=yxdcs;password=yxdcs;Unicode=True;Direct=False;";
                    OracleConnection oraCon = new OracleConnection(connstr);

                    var sql = new StringBuilder();
                    sql.Append("select id,code,name,status,MeasureUnit from sparepart where status=1 ");
                    sql.Append(" and code in (");
                    for(var i = 0; i < codes.Count; i++) {
                        if(codes.Count == i + 1) {
                            sql.Append("'" + codes[i] + "'");
                        } else {
                            sql.Append("'" + codes[i] + "',");
                        }
                    }
                    sql.Append(")");
                    //新建一个DataAdapter用于填充DataSet
                    OracleDataAdapter oraDap = new OracleDataAdapter(sql.ToString(), oraCon);
                    //新建一个DataSet
                    DataSet ds = new DataSet();
                    //填充DataSet
                    oraDap.Fill(ds);
                    //新建一个DataTable
                    DataTable _table = ds.Tables[0];
                    List<SparePart> yxsparepart = new List<SparePart>();

                    for(int i = 0; i < _table.Rows.Count; i++) {
                        SparePart sp = new SparePart();
                        sp.Id = Convert.ToInt32(_table.Rows[i]["id"]);
                        sp.Code = _table.Rows[i]["code"].ToString();
                        sp.Name = _table.Rows[i]["name"].ToString();
                        sp.Status = Convert.ToInt32(_table.Rows[i]["Status"]);
                        sp.MeasureUnit = _table.Rows[i]["MeasureUnit"].ToString();
                        yxsparepart.Add(sp);
                    }

                    bool IsTransSuccess = true;
                    foreach(var c in codes) {
                        var checkcode = yxsparepart.FirstOrDefault(r => r.Code == c);
                        if(checkcode == null) {
                            IsTransSuccess = false;
                            break;
                        }
                    }
                    if(IsTransSuccess) {
                        partsSalesOrder.IsTransSuccess = true;
                        //生成yxdcs下销售订单

                        //获取nextval
                        string nextvaluesql = "select s_partssalesorder.nextval as nextval from dual";
                        int nextvalue = 0;
                        oraCon.Open();
                        OracleCommand command = new OracleCommand(nextvaluesql, oraCon);
                        var reader = command.ExecuteReader();
                        if(reader.Read()) {
                            nextvalue = Convert.ToInt32(reader["nextval"]);
                        }
                        reader.Close();
                        oraCon.Close();
                        if(nextvalue == 0) {
                            throw new ValidationException(ErrorStrings.PartsSalesOrder_Validation19);
                        }

                        //获取客户账户id
                        string customerAccountsql = @"select id from CustomerAccount t where t.customercompanyid=(select id from company where company.code=:code and company.status=1) and t.AccountGroupId=(select AccountGroupId from SalesUnit where SalesUnit.id=100)";
                        int customerAccountid = 0;
                        oraCon.Open();
                        OracleCommand cacommand = new OracleCommand(customerAccountsql, oraCon);
                        OracleParameter caparameter = new OracleParameter("code", partsSalesOrder.SalesUnitOwnerCompanyCode);
                        cacommand.Parameters.Add(caparameter);
                        var careader = cacommand.ExecuteReader();
                        if(careader.Read()) {
                            customerAccountid = Convert.ToInt32(careader["id"]);
                        }
                        careader.Close();
                        oraCon.Close();

                        //获取企业
                        string csql = "select id,code,name from company where code=:code and status=1";
                        Company c = new Company();
                        oraCon.Open();
                        OracleCommand ccommand = new OracleCommand(csql, oraCon);
                        OracleParameter cparameter = new OracleParameter("code", partsSalesOrder.SalesUnitOwnerCompanyCode);
                        ccommand.Parameters.Add(cparameter);
                        var creader = ccommand.ExecuteReader();
                        if(creader.Read()) {
                            c.Id = Convert.ToInt32(creader["id"]);
                            c.Code = creader["code"].ToString();
                            c.Name = creader["name"].ToString();
                        }
                        creader.Close();
                        oraCon.Close();

                        string c1sql = "select id,code,name from company where code=:code and status=1";
                        Company c1 = new Company();
                        oraCon.Open();
                        OracleCommand c1command = new OracleCommand(c1sql, oraCon);
                        OracleParameter c1parameter = new OracleParameter("code", partsSalesOrder.ReceivingCompanyCode);
                        c1command.Parameters.Add(c1parameter);
                        var c1reader = c1command.ExecuteReader();
                        if(c1reader.Read()) {
                            c1.Id = Convert.ToInt32(c1reader["id"]);
                            c1.Code = c1reader["code"].ToString();
                            c1.Name = c1reader["name"].ToString();
                        }
                        c1reader.Close();
                        oraCon.Close();

                        //获取配件销售订单类型
                        string psql = "select id,name from PartsSalesOrderType where name=:name and PartsSalesCategoryId=6 and status=1";
                        int partsSalesOrderTypeId = 15;
                        string partsSalesOrderTypeName = "储备订单";
                        oraCon.Open();
                        OracleCommand pcommand = new OracleCommand(psql, oraCon);
                        OracleParameter pparameter = new OracleParameter("name", partsSalesOrder.PartsSalesOrderTypeName);
                        pcommand.Parameters.Add(pparameter);
                        var preader = pcommand.ExecuteReader();
                        if(preader.Read()) {
                            partsSalesOrderTypeId = Convert.ToInt32(preader["id"]);
                            partsSalesOrderTypeName = preader["name"].ToString();
                        }
                        preader.Close();
                        oraCon.Close();

                        //获取仓库
                        string wsql = "select id from warehouse where name=:name and status=1";
                        int warehouseid = 0;
                        oraCon.Open();
                        OracleCommand wcommand = new OracleCommand(wsql, oraCon);
                        OracleParameter wparameter = new OracleParameter("name", partsSalesOrder.ReceivingWarehouseName);
                        wcommand.Parameters.Add(wparameter);
                        var wreader = wcommand.ExecuteReader();
                        if(wreader.Read()) {
                            warehouseid = Convert.ToInt32(wreader["id"]);
                        }
                        wreader.Close();
                        oraCon.Close();

                        //获取销售订单编号
                        string codesql = @"select regexp_replace('PSO1101' || TO_CHAR(sysdate, 'yyyymmdd') || '{SERIAL}', '{SERIAL}', lpad(TO_CHAR(GetSerial(1, TRUNC(sysdate, 'DD'), '1101')), 4, '0')) as code from dual";
                        string code = "";
                        oraCon.Open();
                        OracleCommand codecommand = new OracleCommand(codesql, oraCon);
                        var codereader = codecommand.ExecuteReader();
                        if(codereader.Read()) {
                            code = codereader["code"].ToString();
                        }
                        codereader.Close();
                        oraCon.Close();

                        PartsSalesOrder newOrder = new PartsSalesOrder();
                        newOrder.Id = nextvalue;
                        newOrder.Code = code;
                        newOrder.BranchId = 2;
                        newOrder.BranchCode = "1101";
                        newOrder.BranchName = "北汽红岩汽车股份有限公司北京配件销售分公司";
                        newOrder.SalesCategoryId = 6;
                        newOrder.SalesCategoryName = "统购";
                        newOrder.SalesUnitId = 100;
                        newOrder.SalesUnitCode = "TG";
                        newOrder.SalesUnitName = "统购";

                        newOrder.SalesUnitOwnerCompanyId = 2;
                        newOrder.SalesUnitOwnerCompanyCode = "1101";
                        newOrder.SalesUnitOwnerCompanyName = "北汽红岩汽车股份有限公司北京配件销售分公司";

                        newOrder.SourceBillCode = partsSalesOrder.Code;
                        newOrder.SourceBillId = partsSalesOrder.Id;
                        newOrder.CustomerAccountId = customerAccountid;
                        newOrder.IsDebt = false;
                        newOrder.InvoiceReceiveCompanyId = c.Id;
                        newOrder.InvoiceReceiveCompanyCode = partsSalesOrder.SalesUnitOwnerCompanyCode;
                        newOrder.InvoiceReceiveCompanyName = c.Name;
                        newOrder.InvoiceReceiveCompanyType = 1; //猜的 可能是1
                        newOrder.InvoiceReceiveSaleCateId = partsSalesOrder.SalesCategoryId;
                        newOrder.InvoiceReceiveSaleCateName = partsSalesOrder.SalesCategoryName;
                        newOrder.SubmitCompanyId = c.Id;
                        newOrder.SubmitCompanyCode = partsSalesOrder.SalesUnitOwnerCompanyCode;
                        newOrder.SubmitCompanyName = c.Name;
                        newOrder.ProvinceID = partsSalesOrder.ProvinceID;
                        newOrder.Province = partsSalesOrder.Province;
                        newOrder.City = partsSalesOrder.City;
                        newOrder.FirstClassStationId = partsSalesOrder.FirstClassStationId;
                        newOrder.FirstClassStationCode = partsSalesOrder.FirstClassStationCode;
                        newOrder.FirstClassStationName = partsSalesOrder.FirstClassStationName;
                        newOrder.PartsSalesOrderTypeId = partsSalesOrderTypeId;
                        newOrder.PartsSalesOrderTypeName = partsSalesOrderTypeName;
                        //newOrder.TotalAmount =根据清单汇总;
                        newOrder.IfAgencyService = partsSalesOrder.IfAgencyService;
                        newOrder.SalesActivityDiscountRate = 1;
                        newOrder.SalesActivityDiscountAmount = 0;
                        newOrder.RequestedShippingTime = partsSalesOrder.RequestedShippingTime;
                        newOrder.RequestedDeliveryTime = partsSalesOrder.RequestedDeliveryTime;
                        newOrder.IfDirectProvision = partsSalesOrder.IfDirectProvision;
                        newOrder.CustomerType = partsSalesOrder.CustomerType;
                        newOrder.SecondLevelOrderType = partsSalesOrder.SecondLevelOrderType;
                        newOrder.Remark = partsSalesOrder.Remark;
                        newOrder.ContactPerson = partsSalesOrder.ContactPerson;
                        newOrder.ContactPhone = partsSalesOrder.ContactPhone;
                        newOrder.ReceivingCompanyId = c1.Id;
                        newOrder.ReceivingCompanyCode = partsSalesOrder.ReceivingCompanyCode;
                        newOrder.ReceivingCompanyName = partsSalesOrder.ReceivingCompanyName;
                        newOrder.ShippingMethod = partsSalesOrder.ShippingMethod;
                        newOrder.ReceivingWarehouseId = warehouseid;
                        newOrder.ReceivingWarehouseName = partsSalesOrder.ReceivingWarehouseName;
                        newOrder.ReceivingAddress = partsSalesOrder.ReceivingAddress;
                        newOrder.CompanyAddressId = partsSalesOrder.CompanyAddressId;
                        newOrder.CreatorName = partsSalesOrder.CreatorName;
                        newOrder.CreateTime = partsSalesOrder.CreateTime;
                        newOrder.ModifierName = partsSalesOrder.ModifierName;
                        newOrder.ModifyTime = partsSalesOrder.ModifyTime;
                        newOrder.SubmitterName = partsSalesOrder.SubmitterName;
                        newOrder.SubmitTime = partsSalesOrder.SubmitTime;
                        newOrder.Status = (int)DcsPartsSalesOrderStatus.提交;


                        //获取客户订单价格等级 CustomerOrderPriceGrade
                        string copgsql = string.Format("select Coefficient from CustomerOrderPriceGrade t where t.partssalescategoryid=6 and CustomerCompanyId={0}and t.partssalesordertypeid={1}", newOrder.InvoiceReceiveCompanyId, newOrder.PartsSalesOrderTypeId);
                        double coefficient = 1.0;
                        oraCon.Open();
                        OracleCommand copgcommand = new OracleCommand(copgsql, oraCon);
                        var copgreader = copgcommand.ExecuteReader();
                        if(copgreader.Read()) {
                            coefficient = Convert.ToDouble(copgreader["coefficient"]);
                        }
                        copgreader.Close();
                        oraCon.Close();


                        //查询配件销售价格
                        var salepricesql = new StringBuilder();
                        salepricesql.Append(string.Format("select sparepartid,sparepartcode,sparepartname,salesprice from PartsSalesPrice t where t.partssalescategoryid={0}and status=1", newOrder.SalesCategoryId));
                        salepricesql.Append(" and t.sparepartid in (");
                        for(var i = 0; i < yxsparepart.Count; i++) {
                            if(codes.Count == i + 1) {
                                salepricesql.Append(yxsparepart[i].Id.ToString());
                            } else {
                                salepricesql.Append(yxsparepart[i].Id.ToString() + ",");
                            }
                        }
                        salepricesql.Append(")");
                        //新建一个DataAdapter用于填充DataSet
                        OracleDataAdapter salepriceoraDap = new OracleDataAdapter(salepricesql.ToString(), oraCon);
                        //新建一个DataSet
                        DataSet salepriceds = new DataSet();
                        //填充DataSet
                        salepriceoraDap.Fill(salepriceds);
                        //新建一个DataTable
                        DataTable saleprice_table = salepriceds.Tables[0];
                        List<PartsSalesPrice> saleprices = new List<PartsSalesPrice>();

                        for(int i = 0; i < saleprice_table.Rows.Count; i++) {
                            PartsSalesPrice sprice = new PartsSalesPrice();
                            sprice.SparePartId = Convert.ToInt32(saleprice_table.Rows[i]["sparepartid"]);
                            sprice.SparePartCode = saleprice_table.Rows[i]["sparepartcode"].ToString();
                            sprice.SparePartName = saleprice_table.Rows[i]["sparepartname"].ToString();
                            sprice.SalesPrice = Convert.ToDecimal(saleprice_table.Rows[i]["salesprice"]);
                            saleprices.Add(sprice);
                        }

                        //查询配件特殊协议销售价格
                        var treatypricesql = new StringBuilder();
                        treatypricesql.Append(string.Format("select sparepartid,sparepartcode,sparepartname,treatyprice from PartsSpecialTreatyPrice t where t.partssalescategoryid={0}and t.companyid={1}and status=1", newOrder.SalesCategoryId, newOrder.InvoiceReceiveCompanyId));
                        treatypricesql.Append(" and t.sparepartid in (");
                        for(var i = 0; i < yxsparepart.Count; i++) {
                            if(codes.Count == i + 1) {
                                treatypricesql.Append(yxsparepart[i].Id.ToString());
                            } else {
                                treatypricesql.Append(yxsparepart[i].Id.ToString() + ",");
                            }
                        }
                        treatypricesql.Append(")");
                        //新建一个DataAdapter用于填充DataSet
                        OracleDataAdapter treatypriceoraDap = new OracleDataAdapter(treatypricesql.ToString(), oraCon);
                        //新建一个DataSet
                        DataSet treatypriceds = new DataSet();
                        //填充DataSet
                        treatypriceoraDap.Fill(treatypriceds);
                        //新建一个DataTable
                        DataTable treatyprice_table = treatypriceds.Tables[0];
                        List<PartsSpecialTreatyPrice> treatyprices = new List<PartsSpecialTreatyPrice>();

                        for(int i = 0; i < treatyprice_table.Rows.Count; i++) {
                            PartsSpecialTreatyPrice treatyprice = new PartsSpecialTreatyPrice();
                            treatyprice.SparePartId = Convert.ToInt32(treatyprice_table.Rows[i]["sparepartid"]);
                            treatyprice.SparePartCode = treatyprice_table.Rows[i]["sparepartcode"].ToString();
                            treatyprice.SparePartName = treatyprice_table.Rows[i]["sparepartname"].ToString();
                            treatyprice.TreatyPrice = Convert.ToDecimal(treatyprice_table.Rows[i]["treatyprice"]);
                            treatyprices.Add(treatyprice);
                        }

                        //增加清单
                        List<PartsSalesOrderDetail> neworderdetails = new List<PartsSalesOrderDetail>();
                        foreach(var p in partsSalesOrderDetails) {
                            //获取当前配件信息
                            var part = yxsparepart.FirstOrDefault(r => r.Code == p.SparePartCode);
                            var saleprice = saleprices.FirstOrDefault(r => r.SparePartCode == p.SparePartCode);
                            var tprice = treatyprices.FirstOrDefault(r => r.SparePartCode == p.SparePartCode);
                            PartsSalesOrderDetail neworderdetail = new PartsSalesOrderDetail();
                            neworderdetail.PartsSalesOrderId = nextvalue;
                            neworderdetail.SparePartId = part.Id;
                            neworderdetail.SparePartCode = part.Code;
                            neworderdetail.SparePartName = part.Name;
                            neworderdetail.MeasureUnit = part.MeasureUnit;
                            neworderdetail.OrderedQuantity = p.OrderedQuantity;
                            if(tprice != null) {
                                neworderdetail.OriginalPrice = tprice.TreatyPrice;
                            } else if(saleprice != null) {
                                neworderdetail.OriginalPrice = saleprice.SalesPrice * Convert.ToDecimal(coefficient);
                                neworderdetail.SalesPrice = saleprice.SalesPrice;
                            } else {
                                neworderdetail.OriginalPrice = 0;
                            }
                            neworderdetail.CustOrderPriceGradeCoefficient = coefficient;
                            neworderdetail.IfCanNewPart = p.IfCanNewPart;
                            neworderdetail.OrderPrice = neworderdetail.OriginalPrice;
                            neworderdetail.OrderSum = p.OrderedQuantity * neworderdetail.OriginalPrice;
                            neworderdetail.EstimatedFulfillTime = p.EstimatedFulfillTime;
                            neworderdetail.DiscountedPrice = p.DiscountedPrice;
                           
                            neworderdetails.Add(neworderdetail);
                            newOrder.TotalAmount = newOrder.TotalAmount + neworderdetail.OrderSum;
                        }
                        //插入清单与主单信息
                        oraCon.Open();
                        string sqlInsertdetail = new PartsSalesOrderAch(this.DomainService).GetMyInsertSql("PartsSalesOrderDetail", "Id", new[] {
"PartsSalesOrderId","SparePartId","SparePartCode","SparePartName","MeasureUnit","OrderedQuantity","OriginalPrice","CustOrderPriceGradeCoefficient","IfCanNewPart","OrderPrice","OrderSum","EstimatedFulfillTime","DiscountedPrice"
                            }, "S_PartsSalesOrderDetail.Nextval");
                        foreach(var item in neworderdetails) {
                            var detailcommand = new OracleCommand(sqlInsertdetail, oraCon);
                            detailcommand.Parameters.Add(new OracleParameter("PartsSalesOrderId", item.PartsSalesOrderId));
                            detailcommand.Parameters.Add(new OracleParameter("SparePartId", item.SparePartId));
                            detailcommand.Parameters.Add(new OracleParameter("SparePartCode", item.SparePartCode));
                            detailcommand.Parameters.Add(new OracleParameter("SparePartName", item.SparePartName));
                            detailcommand.Parameters.Add(new OracleParameter("MeasureUnit", item.MeasureUnit));
                            detailcommand.Parameters.Add(new OracleParameter("OrderedQuantity", item.OrderedQuantity));
                            detailcommand.Parameters.Add(new OracleParameter("OriginalPrice", item.OriginalPrice));
                            detailcommand.Parameters.Add(new OracleParameter("CustOrderPriceGradeCoefficient", item.CustOrderPriceGradeCoefficient));
                            detailcommand.Parameters.Add(new OracleParameter("IfCanNewPart", item.IfCanNewPart));
                            detailcommand.Parameters.Add(new OracleParameter("OrderPrice", item.OrderPrice));
                            detailcommand.Parameters.Add(new OracleParameter("OrderSum", item.OrderSum));
                            detailcommand.Parameters.Add(new OracleParameter("EstimatedFulfillTime", item.EstimatedFulfillTime));
                            detailcommand.Parameters.Add(new OracleParameter("DiscountedPrice", item.DiscountedPrice));
                            detailcommand.ExecuteNonQuery();
                        }
                        string sqlInsertM = new PartsSalesOrderAch(this.DomainService).GetMyInsertSql("PartsSalesOrder", null, new[] {"Id","Code","BranchId","BranchCode","BranchName","SalesCategoryId","SalesCategoryName","SalesUnitId","SalesUnitCode","SalesUnitName","SalesUnitOwnerCompanyId","SalesUnitOwnerCompanyCode","SalesUnitOwnerCompanyName","SourceBillCode","SourceBillId",
"CustomerAccountId","IsDebt","InvoiceReceiveCompanyId","InvoiceReceiveCompanyCode","InvoiceReceiveCompanyName","InvoiceReceiveCompanyType","InvoiceReceiveSaleCateId",
"InvoiceReceiveSaleCateName","SubmitCompanyId","SubmitCompanyCode","SubmitCompanyName","ProvinceID","Province","City","FirstClassStationId","FirstClassStationCode",
"FirstClassStationName","PartsSalesOrderTypeId","PartsSalesOrderTypeName","TotalAmount","IfAgencyService","SalesActivityDiscountRate","SalesActivityDiscountAmount",
"RequestedShippingTime","RequestedDeliveryTime","IfDirectProvision","CustomerType ","SecondLevelOrderType","Remark","ContactPerson","ContactPhone","ReceivingCompanyId",
"ReceivingCompanyCode","ReceivingCompanyName","ShippingMethod","ReceivingWarehouseId","ReceivingWarehouseName","ReceivingAddress","CreatorName","CreateTime",
"ModifierName","ModifyTime","SubmitterName","SubmitTime","Status","IsPurDistribution","CompanyAddressId"
                            }, null);
                        var mcommand = new OracleCommand(sqlInsertM, oraCon);
                        mcommand.Parameters.Add(new OracleParameter("Id", newOrder.Id));
                        mcommand.Parameters.Add(new OracleParameter("Code", newOrder.Code));
                        mcommand.Parameters.Add(new OracleParameter("BranchId", newOrder.BranchId));
                        mcommand.Parameters.Add(new OracleParameter("BranchCode", newOrder.BranchCode));
                        mcommand.Parameters.Add(new OracleParameter("BranchName", newOrder.BranchName));
                        mcommand.Parameters.Add(new OracleParameter("SalesCategoryId", newOrder.SalesCategoryId));
                        mcommand.Parameters.Add(new OracleParameter("SalesCategoryName", newOrder.SalesCategoryName));
                        mcommand.Parameters.Add(new OracleParameter("SalesUnitId", newOrder.SalesUnitId));
                        mcommand.Parameters.Add(new OracleParameter("SalesUnitCode", newOrder.SalesUnitCode));
                        mcommand.Parameters.Add(new OracleParameter("SalesUnitName", newOrder.SalesUnitName));
                        mcommand.Parameters.Add(new OracleParameter("SalesUnitOwnerCompanyId", newOrder.SalesUnitOwnerCompanyId));
                        mcommand.Parameters.Add(new OracleParameter("SalesUnitOwnerCompanyCode", newOrder.SalesUnitOwnerCompanyCode));
                        mcommand.Parameters.Add(new OracleParameter("SalesUnitOwnerCompanyName", newOrder.SalesUnitOwnerCompanyName));
                        mcommand.Parameters.Add(new OracleParameter("SourceBillCode", newOrder.SourceBillCode));
                        mcommand.Parameters.Add(new OracleParameter("SourceBillId", newOrder.SourceBillId));
                        mcommand.Parameters.Add(new OracleParameter("CustomerAccountId", newOrder.CustomerAccountId));
                        mcommand.Parameters.Add(new OracleParameter("IsDebt", newOrder.IsDebt));
                        mcommand.Parameters.Add(new OracleParameter("InvoiceReceiveCompanyId", newOrder.InvoiceReceiveCompanyId));
                        mcommand.Parameters.Add(new OracleParameter("InvoiceReceiveCompanyCode", newOrder.InvoiceReceiveCompanyCode));
                        mcommand.Parameters.Add(new OracleParameter("InvoiceReceiveCompanyName", newOrder.InvoiceReceiveCompanyName));
                        mcommand.Parameters.Add(new OracleParameter("InvoiceReceiveCompanyType", newOrder.InvoiceReceiveCompanyType));
                        mcommand.Parameters.Add(new OracleParameter("InvoiceReceiveSaleCateId", newOrder.InvoiceReceiveSaleCateId));
                        mcommand.Parameters.Add(new OracleParameter("InvoiceReceiveSaleCateName", newOrder.InvoiceReceiveSaleCateName));
                        mcommand.Parameters.Add(new OracleParameter("SubmitCompanyId", newOrder.SubmitCompanyId));
                        mcommand.Parameters.Add(new OracleParameter("SubmitCompanyCode", newOrder.SubmitCompanyCode));
                        mcommand.Parameters.Add(new OracleParameter("SubmitCompanyName", newOrder.SubmitCompanyName));
                        mcommand.Parameters.Add(new OracleParameter("ProvinceID", newOrder.ProvinceID));
                        mcommand.Parameters.Add(new OracleParameter("Province", newOrder.Province));
                        mcommand.Parameters.Add(new OracleParameter("City", newOrder.City));
                        mcommand.Parameters.Add(new OracleParameter("FirstClassStationId", newOrder.FirstClassStationId));
                        mcommand.Parameters.Add(new OracleParameter("FirstClassStationCode", newOrder.FirstClassStationCode));
                        mcommand.Parameters.Add(new OracleParameter("FirstClassStationName", newOrder.FirstClassStationName));
                        mcommand.Parameters.Add(new OracleParameter("PartsSalesOrderTypeId", newOrder.PartsSalesOrderTypeId));
                        mcommand.Parameters.Add(new OracleParameter("PartsSalesOrderTypeName", newOrder.PartsSalesOrderTypeName));
                        mcommand.Parameters.Add(new OracleParameter("TotalAmount", newOrder.TotalAmount));
                        mcommand.Parameters.Add(new OracleParameter("IfAgencyService", newOrder.IfAgencyService));
                        mcommand.Parameters.Add(new OracleParameter("SalesActivityDiscountRate", newOrder.SalesActivityDiscountRate));
                        mcommand.Parameters.Add(new OracleParameter("SalesActivityDiscountAmount", newOrder.SalesActivityDiscountAmount));
                        mcommand.Parameters.Add(new OracleParameter("RequestedShippingTime", newOrder.RequestedShippingTime));
                        mcommand.Parameters.Add(new OracleParameter("RequestedDeliveryTime", newOrder.RequestedDeliveryTime));
                        mcommand.Parameters.Add(new OracleParameter("IfDirectProvision", newOrder.IfDirectProvision));
                        mcommand.Parameters.Add(new OracleParameter("CustomerType", newOrder.CustomerType));
                        mcommand.Parameters.Add(new OracleParameter("SecondLevelOrderType", newOrder.SecondLevelOrderType));
                        mcommand.Parameters.Add(new OracleParameter("Remark", newOrder.Remark));
                        mcommand.Parameters.Add(new OracleParameter("ContactPerson", newOrder.ContactPerson));
                        mcommand.Parameters.Add(new OracleParameter("ContactPhone", newOrder.ContactPhone));
                        mcommand.Parameters.Add(new OracleParameter("ReceivingCompanyId", newOrder.ReceivingCompanyId));
                        mcommand.Parameters.Add(new OracleParameter("ReceivingCompanyCode", newOrder.ReceivingCompanyCode));
                        mcommand.Parameters.Add(new OracleParameter("ReceivingCompanyName", newOrder.ReceivingCompanyName));
                        mcommand.Parameters.Add(new OracleParameter("ShippingMethod", newOrder.ShippingMethod));
                        mcommand.Parameters.Add(new OracleParameter("ReceivingWarehouseId", newOrder.ReceivingWarehouseId));
                        mcommand.Parameters.Add(new OracleParameter("ReceivingWarehouseName", newOrder.ReceivingWarehouseName));
                        mcommand.Parameters.Add(new OracleParameter("ReceivingAddress", newOrder.ReceivingAddress));
                        mcommand.Parameters.Add(new OracleParameter("CreatorName", newOrder.CreatorName));
                        mcommand.Parameters.Add(new OracleParameter("CreateTime", newOrder.CreateTime));
                        mcommand.Parameters.Add(new OracleParameter("ModifierName", newOrder.ModifierName));
                        mcommand.Parameters.Add(new OracleParameter("ModifyTime", newOrder.ModifyTime));
                        mcommand.Parameters.Add(new OracleParameter("SubmitterName", newOrder.SubmitterName));
                        mcommand.Parameters.Add(new OracleParameter("SubmitTime", newOrder.SubmitTime));
                        mcommand.Parameters.Add(new OracleParameter("Status", newOrder.Status));
                        mcommand.Parameters.Add(new OracleParameter("IsPurDistribution", 1));
                        mcommand.Parameters.Add(new OracleParameter("CompanyAddressId", newOrder.CompanyAddressId));
                        mcommand.ExecuteNonQuery();
                        oraCon.Close();
                    } else {
                        partsSalesOrder.IsTransSuccess = false;
                    }

                }
            }
        }


        [Query(HasSideEffects = true)]
        public IQueryable<PartsSalesOrder> GetPartsSalesOrderByIds(int?[] Ids) {
            return ObjectContext.PartsSalesOrders.Where(r => Ids.Contains(r.Id));
        }
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:11:57
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 经销商修改配件销售订单(PartsSalesOrder partsSalesOrder) {
            new PartsSalesOrderAch(this).经销商修改配件销售订单(partsSalesOrder);
        }

        [Update(UsingCustomMethod = true)]
        public void 提交配件销售订单(PartsSalesOrder partsSalesOrder) {
            new PartsSalesOrderAch(this).提交配件销售订单(partsSalesOrder);
        }

        [Update(UsingCustomMethod = true)]
        public void 撤销配件销售订单(PartsSalesOrder partsSalesOrder) {
            new PartsSalesOrderAch(this).撤销配件销售订单(partsSalesOrder);
        }

        [Update(UsingCustomMethod = true)]
        public void 终止配件销售订单(PartsSalesOrder partsSalesOrder) {
            new PartsSalesOrderAch(this).终止配件销售订单(partsSalesOrder);
        }

        [Update(UsingCustomMethod = true)]
        public void 作废配件销售订单(PartsSalesOrder partsSalesOrder) {
            new PartsSalesOrderAch(this).作废配件销售订单(partsSalesOrder);
        }

        [Update(UsingCustomMethod = true)]
        public void 修改销售订单类型(PartsSalesOrder partsSalesOrder) {
            new PartsSalesOrderAch(this).修改销售订单类型(partsSalesOrder);
        }

        [Update(UsingCustomMethod = true)]
        public void 转分公司配件销售订单(PartsSalesOrder partsSalesOrder) {
            new PartsSalesOrderAch(this).转分公司配件销售订单(partsSalesOrder);
        }

        [Invoke]
        public void 修改销售订单类型1(PartsSalesOrder partsSalesOrder, PartsSalesOrderDetail[] partsSalesOrderDetails) {
            new PartsSalesOrderAch(this).修改销售订单类型1(partsSalesOrder, partsSalesOrderDetails);
        }

        [Invoke]
        public bool 校验销售订单库存满足(int ownerCompanyId, List<PartsSalesOrderDetail> list) {
            return new PartsSalesOrderAch(this).校验销售订单库存满足(ownerCompanyId, list);
        }

        [Update(UsingCustomMethod = true)]
        public void 维修单生成紧急销售订单(PartsSalesOrder partsSalesOrder) {
            new PartsSalesOrderAch(this).维修单生成紧急销售订单(partsSalesOrder);
        }

        [Update(UsingCustomMethod = true)]
        public void 销售订单提报(PartsSalesOrder partsSalesOrder, int[] preOrderIds) {
            new PartsSalesOrderAch(this).销售订单提报(partsSalesOrder, preOrderIds);
        }

        [Invoke]
        public Dictionary<int, int> 校验销售清单品牌库存(int[] ids) {
            return new PartsSalesOrderAch(this).校验销售清单品牌库存(ids);
        }

        [Invoke]
        public int 校验订货配件起订金额是否满足(PartsSalesOrder partsSalesOrder) {
            return new PartsSalesOrderAch(this).校验订货配件起订金额是否满足(partsSalesOrder);
        }

        [Update(UsingCustomMethod = true)]
        public void 配件公司终止配件销售订单(PartsSalesOrder partsSalesOrder) {
            new PartsSalesOrderAch(this).配件公司终止配件销售订单(partsSalesOrder);
        }

        [Invoke]
        public void 新增配件销售订单代做(PartsSalesOrder partsSalesOrder, List<PartsSalesOrderDetail> partsSalesOrderDetails) {
            new PartsSalesOrderAch(this).新增配件销售订单代做(partsSalesOrder, partsSalesOrderDetails);
        }

        [Query(HasSideEffects = true)]
        public IQueryable<PartsSalesOrder> GetPartsSalesOrderByIds(int?[] Ids) {
            return new PartsSalesOrderAch(this).GetPartsSalesOrderByIds(Ids);
        }
    }
}
