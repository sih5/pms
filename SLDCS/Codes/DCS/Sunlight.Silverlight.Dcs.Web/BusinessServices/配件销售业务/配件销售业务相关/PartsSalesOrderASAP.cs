﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsSalesOrderASAPAche : DcsSerivceAchieveBase {
        public PartsSalesOrderASAPAche(DcsDomainService domainService)
            : base(domainService) {
        }

        //[Invoke]
        public void 手工设置海外接口(List<VirtualPartsSalesOrderOutboundPlanHw> virtualPartsSalesOrderOutboundPlanHws) {
            var partsSalesOrders = virtualPartsSalesOrderOutboundPlanHws.Where(v => v.InterfaceId == (int)DcsInterfaceType.销售订单);
            if(partsSalesOrders.Any()) {
                var ids = partsSalesOrders.Select(o => o.CodeId);
                IQueryable<PartsSalesOrderASAP> partsSalesOrderAsaps = this.ObjectContext.PartsSalesOrderASAPs.Where(v => ids.Contains(v.Id));
                foreach(var partsSalesOrderAsap in partsSalesOrderAsaps) {
                    partsSalesOrderAsap.HandleStatus = ((int)DcsSynchronousState.未处理).ToString();
                    partsSalesOrderAsap.Message = null;
                    partsSalesOrderAsap.ModifyTime = null;
                    this.UpdateToDatabase(partsSalesOrderAsap);
                }
            }
            var partsOutboundPlans = virtualPartsSalesOrderOutboundPlanHws.Where(v => v.InterfaceId == (int)DcsInterfaceType.出库计划单);
            //if(partsOutboundPlans.Any()) {
            //    var ids = partsOutboundPlans.Select(o => o.CodeId);
            //    IQueryable<PartsOutboundPlanASAP> partsOutboundPlanAsaps = this.ObjectContext.PartsOutboundPlanASAPs.Where(v => ids.Contains(v.Id));
            //    foreach(var partsOutboundPlanAsap in partsOutboundPlanAsaps) {
            //        partsOutboundPlanAsap.HandleStatus = ((int)DcsSynchronousState.未处理).ToString();
            //        partsOutboundPlanAsap.Message = null;
            //        partsOutboundPlanAsap.ModifyTime = null;
            //        this.UpdateToDatabase(partsOutboundPlanAsap);
            //    }
            //}
            this.ObjectContext.SaveChanges();
        }
    }

    partial class DcsDomainService {
        [Invoke]
        public void 手工设置海外接口(List<VirtualPartsSalesOrderOutboundPlanHw> virtualPartsSalesOrderOutboundPlanHws) {
            new PartsSalesOrderASAPAche(this).手工设置海外接口(virtualPartsSalesOrderOutboundPlanHws);
        }
    }
}
