﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
using System.Configuration;
using System.Data;
using System.Text;
using Devart.Data.Oracle;
using NPOI.SS.Formula.Functions;
using System.Transactions;
using System.Data.Objects.SqlClient;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsSalesOrderOwningAch : DcsSerivceAchieveBase {
        public PartsSalesOrderOwningAch(DcsDomainService domainService)
            : base(domainService) {
        }
        public IEnumerable<PartsSalesOrderOwning> GetPartsSalesOrderOwnings(string submitCompanyName, string referenceCode, string sparePartCode) {
            string SQL = @"select sum(UnApproveQuantity)as UnApproveQuantity,submitcompanycode,submitcompanyname,referencecode,sparepartcode,sparepartname,CompanyName,MarketingDepartmentName from (select     
       po.submitcompanycode,
       po.submitcompanyname,
       sp.referencecode,
       p.sparepartcode,
       p.sparepartname,
       co.name as CompanyName,
       p.OrderedQuantity - nvl(p.ApproveQuantity, 0) as UnApproveQuantity,
       (case
         when cp.type = 3 then
          (select mt.Name
             from MarketingDepartment mt
             join Agency ag
               on mt.id = ag.marketingdepartmentid
            where ag.id = cp.id)
         when cp.type = 2 or cp.type = 7 then
          (select mt.name
             from MarketingDepartment mt
             join DealerServiceInfo ag
               on mt.id = ag.marketingdepartmentid
            where ag.DealerId = cp.id)
         else
          null
       end) as MarketingDepartmentName
  from partssalesorderdetail p
  join partssalesorder po
    on p.partssalesorderid = po.id
  left join warehouse w
    on po.warehouseid = w.id
  join sparepart sp
    on p.sparepartid = sp.id
  join company cp
    on po.SubmitCompanyId = cp.id
  left join SalesUnit su
    on po.SalesUnitId = su.id
 inner join company co
    on su.OwnerCompanyId = co.id
   and co.type != 1
 where po.status = 4
   and p.OrderedQuantity > nvl(p.approvequantity, 0) ";

            if(!string.IsNullOrEmpty(referenceCode)) {
                SQL = SQL + " and LOWER(sp.referenceCode) like '%" + referenceCode.ToLower() + "%'";
            }

            if(!string.IsNullOrEmpty(submitCompanyName)) {
                SQL = SQL + " and LOWER(po.submitCompanyName) like '%" + submitCompanyName.ToLower() + "%'";
            }
            if(!string.IsNullOrEmpty(sparePartCode)) {
                SQL = SQL + " and LOWER(p.sparePartCode) like '%" + sparePartCode.ToLower() + "%'";
            }
            //查询当前登录人所属公司性质，若是代理库，则只查询其下属区域内的订单
            var userInfo = Utils.GetCurrentUserInfo();

            var company = ObjectContext.Companies.Where(r => r.Id == userInfo.EnterpriseId).FirstOrDefault();
            if(company.Type != (int)DcsCompanyType.分公司) {
                if(company.Type == (int)DcsCompanyType.代理库) {
                    SQL = SQL + " and  co.Id=" + userInfo.EnterpriseId;
                } else {
                    SQL = SQL + " and  po.SubmitCompanyId=" + userInfo.EnterpriseId;
                }
            }
            SQL = SQL + "  )tt group by submitcompanycode,submitcompanyname,referencecode,sparepartcode,sparepartname,CompanyName,MarketingDepartmentName";
            var search = ObjectContext.ExecuteStoreQuery<PartsSalesOrderOwning>(SQL).ToList();
            return search;
        }
    }
    partial class DcsDomainService {
        public IEnumerable<PartsSalesOrderOwning> GetPartsSalesOrderOwnings(string submitCompanyName, string referenceCode, string sparePartCode) {
            return new PartsSalesOrderOwningAch(this).GetPartsSalesOrderOwnings(submitCompanyName, referenceCode, sparePartCode);

        }
    }
}
