﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Data;
using System.Data.Objects;
using System.Diagnostics;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using System.Transactions;
using Microsoft.Data.Extensions;
using NLog;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsSalesOrderProcessAch : DcsSerivceAchieveBase {
        public PartsSalesOrderProcessAch(DcsDomainService domainService)
            : base(domainService) {
        }

        internal static Logger logger = LogManager.GetCurrentClassLogger();

        public void 配件销售直供审批服务(PartsSalesOrderProcess partsSalesOrderProcess, bool isTransSap) {
            try {
                var partsSalesOrder = ObjectContext.PartsSalesOrders.SingleOrDefault(r => r.Id == partsSalesOrderProcess.OriginalSalesOrderId && r.Status != (int)DcsPartsSalesOrderStatus.作废);
                if(partsSalesOrder == null)
                    throw new ValidationException(ErrorStrings.PartsSalesOrderProcess_Validation1);
                new PartsSalesOrderAch(this.DomainService).PMS_SYC_150_记录日志用(partsSalesOrder);
                if(partsSalesOrder.Status == (int)DcsPartsSalesOrderStatus.提交) {
                    partsSalesOrder.FirstApproveTime = DateTime.Now;
                }
                var customerAccount = DomainService.查询客户可用资金(partsSalesOrder.CustomerAccountId, null, null);
                if(customerAccount == null)
                    throw new ValidationException(string.Format(ErrorStrings.PartsSalesOrderProcess_Validation2, partsSalesOrder.Code));
                var salesUnit = ObjectContext.SalesUnits.FirstOrDefault(v => v.Id == partsSalesOrder.SalesUnitId && v.Status == (int)DcsMasterDataStatus.有效);
                if(salesUnit == null)
                    throw new ValidationException(string.Format(ErrorStrings.PartsSalesOrderProcess_Validation9, partsSalesOrder.Code));
                decimal rebateAmout = 0;

                var PartsRebateAccount = ObjectContext.PartsRebateAccounts.FirstOrDefault(r => r.BranchId == partsSalesOrder.SalesUnitOwnerCompanyId && r.CustomerCompanyId == partsSalesOrder.SubmitCompanyId && r.AccountGroupId == salesUnit.AccountGroupId);
                if(PartsRebateAccount != null) {
                    rebateAmout = PartsRebateAccount.AccountBalanceAmount;
                }
                if((customerAccount.UseablePosition + rebateAmout) < partsSalesOrderProcess.CurrentFulfilledAmount)
                    //当前账户可用额度不足
                    throw new ValidationException(ErrorStrings.PartsSalesOrderProcess_Validation3);
                var _warehouse = ObjectContext.Warehouses.Where(r => r.Type == (int)DcsWarehouseType.虚拟库 && r.StorageCompanyId == partsSalesOrder.SalesUnitOwnerCompanyId && r.BranchId == partsSalesOrder.BranchId && r.Status == (int)DcsBaseDataStatus.有效);
                var salesUnitAffiWarehouse = from a in _warehouse
                                             join b in ObjectContext.SalesUnitAffiWarehouses on a.Id equals b.WarehouseId
                                             join c in ObjectContext.SalesUnits on b.SalesUnitId equals c.Id
                                             where c.PartsSalesCategoryId == partsSalesOrder.SalesCategoryId
                                             select a.Id;
                var warehouse = _warehouse.FirstOrDefault(r => salesUnitAffiWarehouse.Contains(r.Id));
                if(warehouse == null)
                    //对应仓库查询不到
                    throw new ValidationException(ErrorStrings.PartsSalesOrderProcess_Validation4);

                var partsSalesOrderProcessDetails = partsSalesOrderProcess.PartsSalesOrderProcessDetails;
                var partIds = partsSalesOrderProcessDetails.Select(r => r.SparePartId).Distinct();
                var supplierCompanyIds = partsSalesOrderProcessDetails.Select(r => r.SupplierCompanyId).Distinct();
                var partsSupplierRelations = ObjectContext.PartsSupplierRelations.Where(r => r.BranchId == partsSalesOrder.BranchId && partIds.Contains(r.PartId) && supplierCompanyIds.Contains(r.SupplierId) && r.Status == (int)DcsBaseDataStatus.有效).ToArray();

                var partsSalesOrderDetails = ObjectContext.PartsSalesOrderDetails.Where(r => r.PartsSalesOrderId == partsSalesOrder.Id).ToArray();
                UpdateToDatabase(partsSalesOrder);
                foreach(var partsSalesOrderDetail in partsSalesOrderDetails) {
                    var partsSalesOrderProcessDetail = partsSalesOrderProcessDetails.FirstOrDefault(r => r.SparePartId == partsSalesOrderDetail.SparePartId);
                    if(partsSalesOrderProcessDetail != null) {
                        if((partsSalesOrderDetail.ApproveQuantity ?? 0) + partsSalesOrderProcessDetail.CurrentFulfilledQuantity > partsSalesOrderDetail.OrderedQuantity) {
                            throw new ValidationException(ErrorStrings.PartsSalesOrderProcess_Validation18);
                        }
                        partsSalesOrderDetail.ApproveQuantity = (partsSalesOrderDetail.ApproveQuantity ?? 0) + partsSalesOrderProcessDetail.CurrentFulfilledQuantity;
                    }
                }
                partsSalesOrder.Status = partsSalesOrderDetails.Any(v => v.ApproveQuantity != v.OrderedQuantity) ? partsSalesOrder.Status = (int)DcsPartsSalesOrderStatus.部分审批 : (int)DcsPartsSalesOrderStatus.审批完成;
                var currentUser = Utils.GetCurrentUserInfo();
                partsSalesOrder.ApproverId = currentUser.Id;
                partsSalesOrder.ApproverName = currentUser.Name;
                partsSalesOrder.ApproveTime = DateTime.Now;
                partsSalesOrder.RequestedDeliveryTime = partsSalesOrderProcess.RequestedDeliveryTime;
                partsSalesOrder.Time = (partsSalesOrder.Time ?? 0) + 1;
                new PartsSalesOrderAch(this.DomainService).UpdatePartsSalesOrderValidate(partsSalesOrder);

                InsertToDatabase(partsSalesOrderProcess);
                partsSalesOrderProcess.BillStatusAfterProcess = partsSalesOrder.Status;
                partsSalesOrderProcess.Time = partsSalesOrder.Time;
                this.InsertPartsSalesOrderProcessValidate(partsSalesOrderProcess);

                new CustomerAccountAch(this.DomainService).订单审批过账记录流水(partsSalesOrder, partsSalesOrderProcess.CurrentFulfilledAmount);
                var processDetailsBySupplierCompany = partsSalesOrderProcessDetails.Where(r => r.OrderProcessMethod == (int)DcsPartsSalesOrderProcessDetailProcessMethod.供应商直发).GroupBy(r => new {
                    r.SupplierCompanyId
                });

                var userInfo = Utils.GetCurrentUserInfo();
                var primarySupplier = ObjectContext.PartsSupplierRelations.Where(r => partIds.Contains(r.PartId) && r.BranchId == userInfo.EnterpriseId && (r.IsPrimary ?? false) == true && (r.PartsSupplier.Code == "1000002367" || r.PartsSupplier.Code == "1000003829"));
                //已与设计确认，一张销售订单处理单对应的清单配件隶属供应商数量有限（大多只有一个），因此循环查询获取单个供应商的配件采购价
                foreach(var partsSalesOrderProcessDetail in processDetailsBySupplierCompany) {

                    var partsPurchaseOrder = new PartsPurchaseOrder {
                        BranchId = partsSalesOrder.BranchId,
                        BranchCode = partsSalesOrder.BranchCode,
                        BranchName = partsSalesOrder.BranchName,
                        DirectRecWarehouseId = partsSalesOrder.ReceivingWarehouseId,
                        DirectRecWarehouseName = partsSalesOrder.ReceivingWarehouseName,
                        WarehouseId = warehouse.Id,
                        WarehouseName = warehouse.Name,
                        ReceivingCompanyId = partsSalesOrder.SubmitCompanyId,
                        ReceivingCompanyName = partsSalesOrder.SubmitCompanyName,
                        ReceivingAddress = partsSalesOrder.ReceivingAddress,
                        PartsSupplierId = partsSalesOrderProcessDetail.Key.SupplierCompanyId.Value,
                        PartsSupplierCode = partsSalesOrderProcessDetail.First().SupplierCompanyCode,
                        PartsSupplierName = partsSalesOrderProcessDetail.First().SupplierCompanyName,
                        IfDirectProvision = true,
                        OriginalRequirementBillId = partsSalesOrder.Id,
                        OriginalRequirementBillCode = partsSalesOrder.Code,
                        OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.配件销售订单,
                        PartsSalesCategoryId = partsSalesOrder.SalesCategoryId,
                        PartsSalesCategoryName = partsSalesOrder.SalesCategoryName,
                        RequestedDeliveryTime = partsSalesOrder.RequestedDeliveryTime.Value,
                        ShippingMethod = partsSalesOrderProcess.ShippingMethod,
                        Status = (int)DcsPartsPurchaseOrderStatus.提交,
                        InStatus = (int)DcsPurchaseInStatus.未入库,
                        //   PartsPurchaseOrderTypeId = partsPurchaseOrderType.First().Id,
                        Remark = partsSalesOrder.Remark,
                    };

                    if(primarySupplier.Count() > 0 && isTransSap)
                        partsPurchaseOrder.IsTransSap = true;
                    var partsSalesOrderType = ObjectContext.PartsSalesOrderTypes.Where(r => r.PartsSalesCategoryId == partsSalesOrder.SalesCategoryId && r.Status == (int)DcsBaseDataStatus.有效 && r.Name.Contains("特急") && r.Id == partsSalesOrder.PartsSalesOrderTypeId);
                    var partsPurchaseOrderType = ObjectContext.PartsPurchaseOrderTypes.Where(r => r.PartsSalesCategoryId == partsSalesOrder.SalesCategoryId && r.Status == (int)DcsBaseDataStatus.有效);
                    if(partsSalesOrder.PartsSalesOrderTypeName == "正常订单" || partsSalesOrder.PartsSalesOrderTypeName == "油品订单" || partsSalesOrder.PartsSalesOrderTypeName == "集团订单" || partsSalesOrder.PartsSalesOrderTypeName == "6K1" || partsSalesOrder.PartsSalesOrderTypeName == "6K2") {
                        var type = partsPurchaseOrderType.Where(r => r.Name.Contains("正常订单")).FirstOrDefault();
                        if(null == type)
                            throw new ValidationException("未找到类型为正常订单的采购订单类型");
                        partsPurchaseOrder.PartsPurchaseOrderTypeId = type.Id;
                    }else{if(partsSalesOrderType.Any()) {
                        var type = partsPurchaseOrderType.Where(r => r.Name.Contains("特急订单")).FirstOrDefault();
                        if(null == type)
                            throw new ValidationException(ErrorStrings.PartsSalesOrderProcess_Validation19);
                        partsPurchaseOrder.PartsPurchaseOrderTypeId = type.Id;
                    } else {
                        var type = partsPurchaseOrderType.Where(r => r.Name.Contains("紧急订单")).FirstOrDefault();
                        if(null == type)
                            throw new ValidationException(ErrorStrings.PartsSalesOrderProcess_Validation20);
                        partsPurchaseOrder.PartsPurchaseOrderTypeId = type.Id;
                    }
                    }
                    var partsPurchasePricingDetails = DomainService.查询配件采购价格(DateTime.Now, partsSalesOrderProcessDetail.Key.SupplierCompanyId, partsSalesOrderProcessDetail.Select(v => v.SparePartId).ToArray(), partsSalesOrder.BranchId, null, null, partsPurchaseOrder.PartsSalesCategoryId).ToArray();
                    //var partsPurchasePricingDetails =DomainService.GetPurchasePricingsByDate(DateTime.Now, partsSalesOrderProcessDetail.Key.SupplierCompanyId, partsSalesOrderProcessDetail.Select(v => v.SparePartId).ToArray(),partsSalesOrder.BranchId, partsPurchaseOrder.PartsSalesCategoryId).ToArray();
                    var serialNumber = 1;
                    //var branchStrategies = ObjectContext.Branchstrategies.Where(r => r.BranchId == partsPurchaseOrder.BranchId && r.Status == (int)DcsBaseDataStatus.有效).ToArray();
                    //if(branchStrategies.Length != 1) {
                    //    throw new ValidationException("该分公司存在多个有效的分公司策略");
                    //}
                    //var branchstrategy = branchStrategies[0];

                    var salesCenterstrategy = this.ObjectContext.SalesCenterstrategies.SingleOrDefault(r => r.PartsSalesCategoryId == partsSalesOrder.SalesCategoryId && r.Status == (int)DcsBaseDataStatus.有效);
                    if(salesCenterstrategy == null)
                        throw new ValidationException(ErrorStrings.SupplierShippingOrder_Validation31);

                    foreach(var orderDetail in partsSalesOrderProcessDetail) {
                        var partsSupplierRelation = partsSupplierRelations.FirstOrDefault(r => r.PartId == orderDetail.SparePartId);
                        if(partsSupplierRelation == null)
                            throw new ValidationException(string.Format(ErrorStrings.PartsSalesOrderProcess_Validation7, orderDetail.SparePartCode, orderDetail.SupplierCompanyCode));


                        var partsPurchasePricingDetail = partsPurchasePricingDetails.FirstOrDefault(r => r.PartId == orderDetail.SparePartId);
                        if(salesCenterstrategy.PartsPurchasePricingStrategy == (int)DcsBranchstrategyPartsPurchasePricingStrategy.先定价) {
                            if(partsPurchasePricingDetail == null) {
                                throw new ValidationException(string.Format(ErrorStrings.PartsSalesOrderProcess_Validation8, orderDetail.SparePartCode, orderDetail.SupplierCompanyCode));
                            }
                            var partsPurchaseOrderDetail = new PartsPurchaseOrderDetail {
                                SparePartId = orderDetail.SparePartId,
                                SparePartCode = orderDetail.SparePartCode,
                                SparePartName = orderDetail.SparePartName,
                                SupplierPartCode = partsSupplierRelation.SupplierPartCode,
                                UnitPrice = partsPurchasePricingDetail.PurchasePrice,
                                OrderAmount = orderDetail.CurrentFulfilledQuantity,
                                DirectOrderAmount = orderDetail.CurrentFulfilledQuantity,
                                MeasureUnit = orderDetail.MeasureUnit,
                                SerialNumber = serialNumber++,
                                PriceType = partsPurchasePricingDetail.PriceType
                            };
                            partsPurchaseOrder.PartsPurchaseOrderDetails.Add(partsPurchaseOrderDetail);
                        } else {
                            decimal price = 0;
                            if(partsPurchasePricingDetail != null) {
                                price = partsPurchasePricingDetail.PurchasePrice;
                            }
                            var partsPurchaseOrderDetail = new PartsPurchaseOrderDetail {
                                SparePartId = orderDetail.SparePartId,
                                SparePartCode = orderDetail.SparePartCode,
                                SparePartName = orderDetail.SparePartName,
                                SupplierPartCode = partsSupplierRelation.SupplierPartCode,
                                UnitPrice = price,
                                OrderAmount = orderDetail.CurrentFulfilledQuantity,
                                DirectOrderAmount = orderDetail.CurrentFulfilledQuantity,
                                MeasureUnit = orderDetail.MeasureUnit,
                                SerialNumber = serialNumber++
                            };
                            partsPurchaseOrder.PartsPurchaseOrderDetails.Add(partsPurchaseOrderDetail);
                        }
                    }
                    partsPurchaseOrder.TotalAmount = partsPurchaseOrder.PartsPurchaseOrderDetails.Sum(v => v.UnitPrice * v.OrderAmount);
                    DomainService.生成配件采购订单(partsPurchaseOrder);
                }
                new PartsSalesOrderAch(this.DomainService).PMS_SYC_150_SendPMSUpdateOrderStatus_PS(partsSalesOrder);

                var sparePartIds = partsSalesOrderProcessDetails.Select(o => o.SparePartId);
                var spareParts = ObjectContext.SpareParts.Where(o => sparePartIds.Contains(o.Id) && o.Status == (int)DcsMasterDataStatus.有效);
                var assemblyKeyCodes = spareParts.Where(o => o.PartType == (int)DcsSparePartPartType.总成件_领用).Select(o => o.AssemblyKeyCode).ToList();
                var assemblyPartRequisitionLinks = ObjectContext.AssemblyPartRequisitionLinks.Where(o => assemblyKeyCodes.Contains(o.KeyCode) && o.Status == 1).ToList();
                var company = ObjectContext.Companies.FirstOrDefault(o => o.Id == partsSalesOrder.SalesUnitOwnerCompanyId);
                foreach(var detail in partsSalesOrderProcessDetails) {

                if(company.Type == (int)DcsCompanyType.分公司) {
                    int serialNum = 0;
                    var sparePart = spareParts.FirstOrDefault(o => o.Id == detail.SparePartId);
                    if(sparePart.PartType == (int)DcsSparePartPartType.总成件_领用) {
                        //获取当前总成件对应的配件关系
                        var assemblyPartRequisitionLinkNew = assemblyPartRequisitionLinks.Where(o => (o.KeyCode ?? 0) == (sparePart.AssemblyKeyCode ?? 0));
                        if(assemblyPartRequisitionLinkNew.Count() == 0)
                            throw new ValidationException(string.Format(ErrorStrings.AssemblyPartRequisitionLink_Validation3, detail.SparePartCode));
                        //领用仓库
                        var warehous = ObjectContext.Warehouses.Where(t => t.Name == "重庆库" && t.Status == (int)DcsBaseDataStatus.有效).First();
                        if(null == warehous) {
                            throw new ValidationException("生领用单时未找到 重庆库 信息");

                        }                      
                        var assemblySparePartId = assemblyPartRequisitionLinkNew.Select(o => o.PartId);
                        //查询可用库存和企业库存，进行校验
                        var assemblyPartsStocks = ObjectContext.PartsStocks.Where(r => r.WarehouseId == warehous.Id && assemblySparePartId.Contains(r.PartId)).ToArray();
                        var enterprisePartsCosts = ObjectContext.EnterprisePartsCosts.Where(o => o.OwnerCompanyId == partsSalesOrder.BranchId && assemblySparePartId.Contains(o.SparePartId));
                        
                        //生成内部领出单
                        InternalAllocationBill internalAllocationBill = null;
                        foreach(var assemblyPartRequisitionLink in assemblyPartRequisitionLinkNew) {
                            var assemblyPartsStock = assemblyPartsStocks.Where(o => o.PartId == assemblyPartRequisitionLink.PartId).Sum(o => o.Quantity - (o.LockedQty ?? 0));
                            var enterprisePartsCost = enterprisePartsCosts.FirstOrDefault(o => o.SparePartId == assemblyPartRequisitionLink.PartId);
                            if(assemblyPartRequisitionLink.Qty > assemblyPartsStock) {
                                throw new ValidationException(string.Format(ErrorStrings.AssemblyPartRequisitionLink_Validation6, detail.SparePartCode, assemblyPartRequisitionLink.PartCode));
                            } else {
                                var departmentInformation = ObjectContext.DepartmentInformations.FirstOrDefault(o => o.Name.Equals("销售服务组") && o.Status == 1);
                                if(internalAllocationBill == null) {
                                    internalAllocationBill = new InternalAllocationBill();
                                    internalAllocationBill.BranchId = partsSalesOrder.BranchId;
                                    internalAllocationBill.BranchName = partsSalesOrder.BranchName;
                                    internalAllocationBill.WarehouseId = warehous.Id;
                                    internalAllocationBill.WarehouseName = warehous.Name;
                                    internalAllocationBill.DepartmentId = departmentInformation.Id;
                                    internalAllocationBill.DepartmentName = departmentInformation.Name;
                                    internalAllocationBill.TotalAmount = 0;
                                    internalAllocationBill.Status = (int)DcsInternalAllocationBillStatus.已审核;
                                    internalAllocationBill.ApproverId = currentUser.Id;
                                    internalAllocationBill.ApproverName = currentUser.Name;
                                    internalAllocationBill.ApproveTime = DateTime.Now;
                                    internalAllocationBill.ModifierId = currentUser.Id;
                                    internalAllocationBill.ModifierName = currentUser.Name;
                                    internalAllocationBill.ModifyTime = DateTime.Now;
                                    internalAllocationBill.Objid = Guid.NewGuid().ToString();
                                    internalAllocationBill.SourceCode = partsSalesOrder.Code;
                                    internalAllocationBill.Type = (int)DcsInternalAllocationBill_Type.包材;
                                    internalAllocationBill.ZcId = sparePart.Id;
                                    InsertToDatabase(internalAllocationBill);
                                    new InternalAllocationBillAch(this.DomainService).InsertInternalAllocationBillValidate(internalAllocationBill);
                                }
                                var internalAllocationDetail = new InternalAllocationDetail();
                                internalAllocationDetail.InternalAllocationBill = internalAllocationBill;
                                internalAllocationDetail.SerialNumber = serialNum++;
                                internalAllocationDetail.SparePartId = assemblyPartRequisitionLink.PartId;
                                internalAllocationDetail.SparePartCode = assemblyPartRequisitionLink.PartCode;
                                internalAllocationDetail.SparePartName = assemblyPartRequisitionLink.PartName;
                                internalAllocationDetail.MeasureUnit = sparePart.MeasureUnit;
                                internalAllocationDetail.UnitPrice = enterprisePartsCost.CostPrice;
                                internalAllocationDetail.Quantity = (assemblyPartRequisitionLink.Qty ?? 0) * detail.CurrentFulfilledQuantity;
                                internalAllocationBill.InternalAllocationDetails.Add(internalAllocationDetail);
                                internalAllocationBill.TotalAmount += internalAllocationDetail.Quantity * internalAllocationDetail.UnitPrice;
                            }
                        }
                    }
                }
                }
            } catch(OptimisticConcurrencyException) {
                throw new ValidationException(ErrorStrings.PartsSalesOrderProcess_Validation21);
            }
        }
        internal static List<PartsStock> partsStockLists = new List<PartsStock>();

        public void 审批配件销售订单正常订单(PartsSalesOrderProcess partsSalesOrderProcess) {
            try {
                var partsSalesOrder = ObjectContext.PartsSalesOrders.SingleOrDefault(r => r.Id == partsSalesOrderProcess.OriginalSalesOrderId && r.Status != (int)DcsPartsSalesOrderStatus.作废);
                if(partsSalesOrder == null)
                    throw new ValidationException(ErrorStrings.PartsSalesOrderProcess_Validation1);
                var customerAccount = DomainService.查询客户可用资金(partsSalesOrder.CustomerAccountId, null, null);
                if(customerAccount == null)
                    throw new ValidationException(string.Format(ErrorStrings.PartsSalesOrderProcess_Validation2, partsSalesOrder.Code));
                var salesUnit = ObjectContext.SalesUnits.FirstOrDefault(v => v.Id == partsSalesOrder.SalesUnitId && v.Status == (int)DcsMasterDataStatus.有效);
                if(salesUnit == null)
                    throw new ValidationException(string.Format(ErrorStrings.PartsSalesOrderProcess_Validation9, partsSalesOrder.Code));
                decimal rebateAmout = 0;
                var PartsRebateAccount = ObjectContext.PartsRebateAccounts.FirstOrDefault(r => r.BranchId == partsSalesOrder.SalesUnitOwnerCompanyId && r.CustomerCompanyId == partsSalesOrder.SubmitCompanyId && r.AccountGroupId == salesUnit.AccountGroupId);
                if(PartsRebateAccount != null) {
                    rebateAmout = PartsRebateAccount.AccountBalanceAmount;
                }
                if(customerAccount.UseablePosition + rebateAmout < partsSalesOrderProcess.CurrentFulfilledAmount)
                    //当前账户可用额度不足
                    throw new ValidationException(ErrorStrings.PartsSalesOrderProcess_Validation3);
                var invoiceReceiveCompany = ObjectContext.Companies.FirstOrDefault(r => r.Id == partsSalesOrder.InvoiceReceiveCompanyId && r.Status == (int)DcsMasterDataStatus.有效);
                if(invoiceReceiveCompany == null)
                    //对应的企业不存在
                    throw new ValidationException(string.Format(ErrorStrings.PartsSalesOrderProcess_Validation10, partsSalesOrder.Code));


                var partsSalesOrderProcessDetails = partsSalesOrderProcess.PartsSalesOrderProcessDetails;
                var partsSalesOrderDetails = ObjectContext.PartsSalesOrderDetails.Where(r => r.PartsSalesOrderId == partsSalesOrder.Id).ToArray();
                UpdateToDatabase(partsSalesOrder);
                foreach(var partsSalesOrderDetail in partsSalesOrderDetails) {
                    var partsSalesOrderProcessDetail = partsSalesOrderProcessDetails.FirstOrDefault(r => r.SparePartId == partsSalesOrderDetail.SparePartId);
                    if(partsSalesOrderProcessDetail != null) {
                        if((partsSalesOrderDetail.ApproveQuantity ?? 0) + partsSalesOrderProcessDetail.CurrentFulfilledQuantity > partsSalesOrderDetail.OrderedQuantity) {
                            throw new ValidationException(ErrorStrings.PartsSalesOrderProcess_Validation18);
                        }
                        partsSalesOrderDetail.ApproveQuantity = (partsSalesOrderDetail.ApproveQuantity ?? 0) + partsSalesOrderProcessDetail.CurrentFulfilledQuantity;
                    }
                }
                partsSalesOrder.Status = partsSalesOrderDetails.Any(v => v.ApproveQuantity != v.OrderedQuantity) ? partsSalesOrder.Status = (int)DcsPartsSalesOrderStatus.部分审批 : (int)DcsPartsSalesOrderStatus.审批完成;
                var currentUser = Utils.GetCurrentUserInfo();
                partsSalesOrder.ApproverId = currentUser.Id;
                partsSalesOrder.ApproverName = currentUser.Name;
                partsSalesOrder.ApproveTime = DateTime.Now;
                partsSalesOrder.Time = (partsSalesOrder.Time ?? 0) + 1;
                new PartsSalesOrderAch(this.DomainService).UpdatePartsSalesOrderValidate(partsSalesOrder);

                new CustomerAccountAch(this.DomainService).订单审批过账(partsSalesOrder.CustomerAccountId, partsSalesOrderProcess.CurrentFulfilledAmount);
                var processDetailsByWarehouses = partsSalesOrderProcessDetails.Where(r => r.OrderProcessMethod == (int)DcsPartsSalesOrderProcessDetailProcessMethod.本部处理).GroupBy(r => new {
                    r.WarehouseId
                });
                var warehouseIds = partsSalesOrderProcessDetails.Select(r => r.WarehouseId).Distinct().ToArray();
                var warehouses = ObjectContext.Warehouses.Where(r => warehouseIds.Contains(r.Id) && r.Status != (int)DcsBaseDataStatus.作废).ToArray();
                var userInfo = Utils.GetCurrentUserInfo();
                var userInfoCompany = ObjectContext.Companies.SingleOrDefault(r => r.Id == userInfo.EnterpriseId);
                var agencySaleUnits = ObjectContext.SalesUnits.Where(agencySaleUnit => agencySaleUnit.OwnerCompanyId == invoiceReceiveCompany.Id && agencySaleUnit.PartsSalesCategoryId == salesUnit.PartsSalesCategoryId && agencySaleUnit.Status == (int)DcsMasterDataStatus.有效);
                //var agencyWarehouse = ObjectContext.Warehouses.FirstOrDefault(v => ObjectContext.SalesUnitAffiWarehouses.Any(affi => affi.WarehouseId == v.Id && agencySaleUnits.Contains(affi.SalesUnit))); 此处获取错误，应该到销售订单里面获取仓库信息（当有多个仓库时，此方法就会出现错误）
                var agencyWarehouse = ObjectContext.Warehouses.FirstOrDefault(v => v.Id == partsSalesOrder.ReceivingWarehouseId && v.Status == (int)DcsBaseDataStatus.有效);
                foreach(var partsSalesOrderProcessDetail in processDetailsByWarehouses) {
                    var warehouse = warehouses.FirstOrDefault(r => r.Id == partsSalesOrderProcessDetail.Key.WarehouseId);
                    if(warehouse == null)
                        throw new ValidationException(string.Format(ErrorStrings.PartsSalesOrderProcess_Validation13, partsSalesOrderProcessDetail.First().WarehouseCode));
                    var partsOutboundPlan = new PartsOutboundPlan {
                        WarehouseId = (int)partsSalesOrderProcessDetail.Key.WarehouseId,
                        WarehouseName = partsSalesOrderProcessDetail.First().WarehouseName,
                        WarehouseCode = partsSalesOrderProcessDetail.First().WarehouseCode,
                        StorageCompanyId = userInfo.EnterpriseId,
                        StorageCompanyCode = userInfo.EnterpriseCode,
                        StorageCompanyName = userInfo.EnterpriseName,
                        StorageCompanyType = userInfoCompany.Type,
                        BranchId = partsSalesOrder.BranchId,
                        BranchCode = partsSalesOrder.BranchCode,
                        BranchName = partsSalesOrder.BranchName,
                        CounterpartCompanyId = partsSalesOrder.InvoiceReceiveCompanyId,
                        CounterpartCompanyCode = partsSalesOrder.InvoiceReceiveCompanyCode,
                        CounterpartCompanyName = partsSalesOrder.InvoiceReceiveCompanyName,
                        SourceId = partsSalesOrder.Id,
                        SourceCode = partsSalesOrder.Code,
                        OutboundType = (int)DcsPartsOutboundType.配件销售,
                        CustomerAccountId = partsSalesOrder.CustomerAccountId,
                        OriginalRequirementBillId = partsSalesOrder.Id,
                        OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.配件销售订单,
                        ReceivingCompanyId = partsSalesOrder.ReceivingCompanyId,
                        ReceivingCompanyCode = partsSalesOrder.ReceivingCompanyCode,
                        ReceivingCompanyName = partsSalesOrder.ReceivingCompanyName,
                        Status = (int)DcsPartsOutboundPlanStatus.新建,
                        ShippingMethod = partsSalesOrder.ShippingMethod,
                        IfWmsInterface = warehouse.WmsInterface,
                        PartsSalesCategoryId = salesUnit.PartsSalesCategoryId,
                        PartsSalesOrderTypeId = partsSalesOrder.PartsSalesOrderTypeId,
                        PartsSalesOrderTypeName = partsSalesOrder.PartsSalesOrderTypeName,
                        OrderApproveComment = partsSalesOrderProcess.ApprovalComment,
                        ReceivingAddress = partsSalesOrder.ReceivingAddress
                    };
                    if(invoiceReceiveCompany.Type == (int)DcsCompanyType.代理库 || invoiceReceiveCompany.Type == (int)DcsCompanyType.服务站兼代理库) {
                        if(agencyWarehouse == null)
                            throw new ValidationException(ErrorStrings.PartsSalesOrderProcess_Validation11);
                        partsOutboundPlan.ReceivingWarehouseId = agencyWarehouse.Id;
                        partsOutboundPlan.ReceivingWarehouseCode = agencyWarehouse.Code;
                        partsOutboundPlan.ReceivingWarehouseName = agencyWarehouse.Name;
                    }
                    var orderDetails = partsSalesOrderProcessDetail.Select(v => new {
                        SparePartId = v.NewPartId ?? v.SparePartId,
                        SparePartCode = v.NewPartId.HasValue ? v.NewPartCode : v.SparePartCode,
                        SparePartName = v.NewPartId.HasValue ? v.NewPartName : v.SparePartName,
                        PlannedAmount = v.CurrentFulfilledQuantity,
                        Price = v.OrderPrice
                    }).GroupBy(v => v.SparePartId).Select(v => new PartsOutboundPlanDetail {
                        SparePartId = v.Key,
                        SparePartCode = v.First().SparePartCode,
                        SparePartName = v.First().SparePartName,
                        PlannedAmount = v.Sum(v1 => v1.PlannedAmount),
                        Price = v.First().Price
                    });

                    foreach(var orderDetail in orderDetails)
                        partsOutboundPlan.PartsOutboundPlanDetails.Add(orderDetail);
                    DomainService.生成配件出库计划(partsOutboundPlan);
                }

                var processDetailsBySupplierCompany = partsSalesOrderProcessDetails.Where(r => r.OrderProcessMethod == (int)DcsPartsSalesOrderProcessDetailProcessMethod.积压件平台).GroupBy(r => new {
                    r.SupplierCompanyId
                }).ToArray();
                if(processDetailsBySupplierCompany.Length > 0) {
                    var supplierCompanyIds = partsSalesOrderProcessDetails.Where(v => v.SupplierCompanyId.HasValue).Select(v => v.SupplierCompanyId).Distinct().ToArray();
                    var customerAccounts = ObjectContext.CustomerAccounts.Where(r => supplierCompanyIds.Contains(r.CustomerCompanyId) && r.AccountGroupId == salesUnit.AccountGroupId && r.Status == (int)DcsMasterDataStatus.有效).ToArray();
                    foreach(var partsSalesOrderProcessDetail in processDetailsBySupplierCompany) {
                        var supplierCompanyAccount = customerAccounts.FirstOrDefault(r => r.CustomerCompanyId == partsSalesOrderProcessDetail.Key.SupplierCompanyId);
                        if(supplierCompanyAccount == null)
                            throw new ValidationException(string.Format(ErrorStrings.PartsSalesOrderProcess_Validation12, partsSalesOrderProcessDetail.First().SupplierCompanyCode));
                        var overstockPartsAdjustBill = new OverstockPartsAdjustBill {
                            OriginalStorageCompanyId = partsSalesOrderProcessDetail.Key.SupplierCompanyId.Value,
                            OriginalStorageCompanyCode = partsSalesOrderProcessDetail.First().SupplierCompanyCode,
                            OriginalStorageCompanyName = partsSalesOrderProcessDetail.First().SupplierCompanyName,
                            OriginalStorageCoCustAccountId = supplierCompanyAccount.Id,
                            StorageCompanyType = partsSalesOrder.InvoiceReceiveCompanyType,
                            DestStorageCompanyId = partsSalesOrder.InvoiceReceiveCompanyId,
                            DestStorageCompanyCode = partsSalesOrder.InvoiceReceiveCompanyCode,
                            DestStorageCompanyName = partsSalesOrder.InvoiceReceiveCompanyName,
                            BranchId = partsSalesOrder.BranchId,
                            BranchCode = partsSalesOrder.BranchCode,
                            BranchName = partsSalesOrder.BranchName,
                            SalesUnitId = partsSalesOrder.SalesUnitId,
                            SalesUnitName = partsSalesOrder.SalesUnitName,
                            SalesUnitOwnerCompanyId = partsSalesOrder.SalesUnitOwnerCompanyId,
                            TransferReason = string.Format("配件销售积压件调剂（{0}）", partsSalesOrder.Code),
                            SourceId = partsSalesOrder.Id,
                            SourceCode = partsSalesOrder.Code,
                            OriginalRequirementBillId = partsSalesOrder.Id,
                            OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.配件销售订单,
                            Status = (int)DcsOverstockPartsAdjustBillStatus.新建
                        };
                        foreach(var orderDetail in partsSalesOrderProcessDetail) {
                            var overstockPartsAdjustDetail = new OverstockPartsAdjustDetail {
                                SparePartId = orderDetail.SparePartId,
                                SparePartCode = orderDetail.SparePartCode,
                                SparePartName = orderDetail.SparePartName,
                                Quantity = orderDetail.CurrentFulfilledQuantity,
                                TransferOutPrice = orderDetail.TransferPrice,
                                TransferInPrice = orderDetail.OrderPrice
                            };
                            overstockPartsAdjustBill.OverstockPartsAdjustDetails.Add(overstockPartsAdjustDetail);
                        }
                        InsertToDatabase(overstockPartsAdjustBill);
                        new OverstockPartsAdjustBillAch(this.DomainService).InsertOverstockPartsAdjustBillValidate(overstockPartsAdjustBill);
                    }
                }
                InsertToDatabase(partsSalesOrderProcess);
                partsSalesOrderProcess.BillStatusAfterProcess = partsSalesOrder.Status;
                partsSalesOrderProcess.Time = partsSalesOrder.Time;
                this.InsertPartsSalesOrderProcessValidate(partsSalesOrderProcess);
                var branchstrategy = ObjectContext.Branchstrategies.FirstOrDefault(r => r.BranchId == partsSalesOrder.BranchId);
                if(branchstrategy == null)
                    throw new ValidationException(ErrorStrings.PartsSalesOrderProcess_Validation14);
                var salesUnitOwnerCompany = ObjectContext.Companies.SingleOrDefault(r => r.Id == partsSalesOrder.SalesUnitOwnerCompanyId && r.Status != (int)DcsMasterDataStatus.作废);
                if(salesUnitOwnerCompany == null)
                    throw new ValidationException(ErrorStrings.PartsSalesOrderProcess_Validation15);
                if(branchstrategy.EmergencySalesStrategy == 1 && partsSalesOrderDetails.Any(r => r.ApproveQuantity < r.OrderedQuantity) && partsSalesOrder.PartsSalesOrderTypeName.Trim() == "紧急销售订单" && salesUnitOwnerCompany.Type == (int)DcsCompanyType.代理库) {
                    var submitCompanyCustomerAccount = ObjectContext.CustomerAccounts.FirstOrDefault(r => r.CustomerCompanyId == partsSalesOrder.SubmitCompanyId && ObjectContext.AccountGroups.Any(v => v.SalesCompanyId == partsSalesOrder.BranchId && r.AccountGroupId == v.Id && r.Status != (int)DcsMasterDataStatus.作废));
                    if(submitCompanyCustomerAccount == null)
                        throw new ValidationException(ErrorStrings.PartsSalesOrderProcess_Validation16);
                    var newPartsSalesOrder = new PartsSalesOrder {
                        BranchId = partsSalesOrder.BranchId,
                        BranchCode = partsSalesOrder.BranchCode,
                        BranchName = partsSalesOrder.BranchName,
                        SalesUnitId = partsSalesOrder.SalesUnitId,
                        SalesUnitCode = partsSalesOrder.SalesUnitCode,
                        SalesUnitName = partsSalesOrder.SalesUnitName,
                        SalesUnitOwnerCompanyId = partsSalesOrder.SalesUnitOwnerCompanyId,
                        SalesUnitOwnerCompanyCode = partsSalesOrder.SalesUnitOwnerCompanyCode,
                        SalesUnitOwnerCompanyName = partsSalesOrder.SalesUnitOwnerCompanyName,
                        SourceBillCode = partsSalesOrder.Code,
                        SourceBillId = partsSalesOrder.Id,
                        CustomerAccountId = submitCompanyCustomerAccount.Id,
                        InvoiceReceiveCompanyId = partsSalesOrder.InvoiceReceiveCompanyId,
                        InvoiceReceiveCompanyCode = partsSalesOrder.InvoiceReceiveCompanyCode,
                        InvoiceReceiveCompanyName = partsSalesOrder.InvoiceReceiveCompanyName,
                        InvoiceReceiveCompanyType = partsSalesOrder.InvoiceReceiveCompanyType,
                        SubmitCompanyId = partsSalesOrder.SubmitCompanyId,
                        SubmitCompanyCode = partsSalesOrder.SubmitCompanyCode,
                        SubmitCompanyName = partsSalesOrder.SubmitCompanyName,
                        PartsSalesOrderTypeId = partsSalesOrder.PartsSalesOrderTypeId,
                        PartsSalesOrderTypeName = partsSalesOrder.PartsSalesOrderTypeName,
                        IfAgencyService = true,
                        SalesActivityDiscountRate = partsSalesOrder.SalesActivityDiscountRate,
                        SalesActivityDiscountAmount = partsSalesOrder.SalesActivityDiscountAmount,
                        RequestedShippingTime = partsSalesOrder.RequestedShippingTime,
                        RequestedDeliveryTime = partsSalesOrder.RequestedDeliveryTime,
                        IfDirectProvision = partsSalesOrder.IfDirectProvision,
                        SecondLevelOrderType = partsSalesOrder.SecondLevelOrderType,
                        Remark = "转服务站向总部提报的销售订单",
                        ApprovalComment = partsSalesOrder.ApprovalComment,
                        ContactPerson = partsSalesOrder.ContactPerson,
                        ContactPhone = partsSalesOrder.ContactPhone,
                        ReceivingCompanyId = partsSalesOrder.ReceivingCompanyId,
                        ReceivingCompanyCode = partsSalesOrder.ReceivingCompanyCode,
                        ReceivingCompanyName = partsSalesOrder.ReceivingCompanyName,
                        ShippingMethod = partsSalesOrder.ShippingMethod,
                        ReceivingAddress = partsSalesOrder.ReceivingAddress
                        //状态未赋值
                    };
                    var newPartsIds = partsSalesOrderDetails.Where(r => r.ApproveQuantity < r.OrderedQuantity).Select(v => v.SparePartId).ToArray();
                    var virtualPartsSalesPrices = new VirtualPartsSalesPriceAch(this.DomainService).根据配件清单获取销售价格(partsSalesOrder.ReceivingCompanyId, partsSalesOrder.PartsSalesOrderTypeId, newPartsIds);
                    foreach(var partsSalesOrderDetail in partsSalesOrderDetails) {
                        if(partsSalesOrderDetail.ApproveQuantity < partsSalesOrderDetail.OrderedQuantity) {
                            var virtualPartsSalesPrice = virtualPartsSalesPrices.FirstOrDefault(r => r.SparePartId == partsSalesOrderDetail.SparePartId);
                            if(virtualPartsSalesPrice == null)
                                throw new ValidationException(string.Format(ErrorStrings.PartsSalesOrderProcess_Validation17, partsSalesOrderDetail.SparePartCode));
                            var newpartsSalesOrderDetail = new PartsSalesOrderDetail {
                                SparePartId = partsSalesOrderDetail.SparePartId,
                                SparePartCode = partsSalesOrderDetail.SparePartCode,
                                SparePartName = partsSalesOrderDetail.SparePartName,
                                MeasureUnit = partsSalesOrderDetail.MeasureUnit,
                                OrderedQuantity = partsSalesOrderDetail.OrderedQuantity - (partsSalesOrderDetail.ApproveQuantity.HasValue ? partsSalesOrderDetail.ApproveQuantity.Value : 0),
                                ApproveQuantity = 0,
                                OriginalPrice = partsSalesOrderDetail.OriginalPrice,
                                CustOrderPriceGradeCoefficient = virtualPartsSalesPrice.Coefficient,
                                OrderPrice = partsSalesOrderDetail.OriginalPrice * (decimal)virtualPartsSalesPrice.Coefficient,
                                OrderSum = partsSalesOrderDetail.OrderPrice * partsSalesOrderDetail.OrderedQuantity - (partsSalesOrderDetail.ApproveQuantity.HasValue ? partsSalesOrderDetail.ApproveQuantity.Value : 0),
                                EstimatedFulfillTime = partsSalesOrderDetail.EstimatedFulfillTime,
                                PriceTypeName = partsSalesOrderDetail.PriceTypeName,
                                ABCStrategy = partsSalesOrderDetail.ABCStrategy
                            };
                            newPartsSalesOrder.PartsSalesOrderDetails.Add(newpartsSalesOrderDetail);
                        }
                    }
                    newPartsSalesOrder.TotalAmount -= newPartsSalesOrder.PartsSalesOrderDetails.Sum(r => (r.ApproveQuantity.HasValue ? r.ApproveQuantity.Value : 0) * r.OrderPrice);
                    InsertToDatabase(newPartsSalesOrder);
                    new PartsSalesOrderAch(this.DomainService).InsertPartsSalesOrderValidate(newPartsSalesOrder);
                }
                if(branchstrategy.EmergencySalesStrategy == 2 && partsSalesOrderDetails.Any(r => r.ApproveQuantity < r.OrderedQuantity) && partsSalesOrder.PartsSalesOrderTypeName.Trim() == "紧急销售订单" && salesUnitOwnerCompany.Type == (int)DcsCompanyType.代理库) {
                    var submitCompanyCustomerAccount = ObjectContext.CustomerAccounts.FirstOrDefault(r => r.CustomerCompanyId == partsSalesOrder.SubmitCompanyId && ObjectContext.AccountGroups.Any(v => v.SalesCompanyId == partsSalesOrder.BranchId && r.AccountGroupId == v.Id && r.Status != (int)DcsMasterDataStatus.作废));
                    if(submitCompanyCustomerAccount == null)
                        throw new ValidationException(ErrorStrings.PartsSalesOrderProcess_Validation16);
                    var newPartsSalesOrder = new PartsSalesOrder {
                        BranchId = partsSalesOrder.BranchId,
                        BranchCode = partsSalesOrder.BranchCode,
                        BranchName = partsSalesOrder.BranchName,
                        SalesUnitId = partsSalesOrder.SalesUnitId,
                        SalesUnitCode = partsSalesOrder.SalesUnitCode,
                        SalesUnitName = partsSalesOrder.SalesUnitName,
                        SalesUnitOwnerCompanyId = partsSalesOrder.SalesUnitOwnerCompanyId,
                        SalesUnitOwnerCompanyCode = partsSalesOrder.SalesUnitOwnerCompanyCode,
                        SalesUnitOwnerCompanyName = partsSalesOrder.SalesUnitOwnerCompanyName,
                        SourceBillCode = partsSalesOrder.Code,
                        SourceBillId = partsSalesOrder.Id,
                        CustomerAccountId = submitCompanyCustomerAccount.Id,
                        InvoiceReceiveCompanyId = partsSalesOrder.InvoiceReceiveCompanyId,
                        InvoiceReceiveCompanyCode = partsSalesOrder.InvoiceReceiveCompanyCode,
                        InvoiceReceiveCompanyName = partsSalesOrder.InvoiceReceiveCompanyName,
                        InvoiceReceiveCompanyType = (int)DcsCompanyType.代理库,
                        SubmitCompanyId = partsSalesOrder.SubmitCompanyId,
                        SubmitCompanyCode = partsSalesOrder.SubmitCompanyCode,
                        SubmitCompanyName = partsSalesOrder.SubmitCompanyName,
                        PartsSalesOrderTypeId = partsSalesOrder.PartsSalesOrderTypeId,
                        PartsSalesOrderTypeName = partsSalesOrder.PartsSalesOrderTypeName,
                        //TotalAmount = partsSalesOrder.TotalAmount,
                        IfAgencyService = true,
                        SalesActivityDiscountRate = partsSalesOrder.SalesActivityDiscountRate,
                        SalesActivityDiscountAmount = partsSalesOrder.SalesActivityDiscountAmount,
                        RequestedShippingTime = partsSalesOrder.RequestedShippingTime,
                        RequestedDeliveryTime = partsSalesOrder.RequestedDeliveryTime,
                        IfDirectProvision = partsSalesOrder.IfDirectProvision,
                        SecondLevelOrderType = partsSalesOrder.SecondLevelOrderType,
                        Remark = "转服务站向总部提报的销售订单",
                        ApprovalComment = partsSalesOrder.ApprovalComment,
                        ContactPerson = partsSalesOrder.ContactPerson,
                        ContactPhone = partsSalesOrder.ContactPhone,
                        ReceivingCompanyId = partsSalesOrder.ReceivingCompanyId,
                        ReceivingCompanyCode = partsSalesOrder.ReceivingCompanyCode,
                        ReceivingCompanyName = partsSalesOrder.ReceivingCompanyName,
                        ShippingMethod = partsSalesOrder.ShippingMethod,
                        ReceivingAddress = partsSalesOrder.ReceivingAddress,
                        Status = (int)DcsPartsSalesOrderStatus.新增
                    };
                    var newPartsIds = partsSalesOrderDetails.Where(r => r.ApproveQuantity < r.OrderedQuantity).Select(v => v.SparePartId).ToArray();
                    var virtualPartsSalesPrices = new VirtualPartsSalesPriceAch(this.DomainService).根据配件清单获取销售价格(partsSalesOrder.ReceivingCompanyId, partsSalesOrder.PartsSalesOrderTypeId, newPartsIds);
                    foreach(var partsSalesOrderDetail in partsSalesOrderDetails) {
                        if(partsSalesOrderDetail.ApproveQuantity < partsSalesOrderDetail.OrderedQuantity) {
                            var virtualPartsSalesPrice = virtualPartsSalesPrices.FirstOrDefault(r => r.SparePartId == partsSalesOrderDetail.SparePartId);
                            if(virtualPartsSalesPrice == null)
                                throw new ValidationException(ErrorStrings.PartsSalesOrderProcess_Validation17);
                            var newpartsSalesOrderDetail = new PartsSalesOrderDetail {
                                SparePartId = partsSalesOrderDetail.SparePartId,
                                SparePartCode = partsSalesOrderDetail.SparePartCode,
                                SparePartName = partsSalesOrderDetail.SparePartName,
                                MeasureUnit = partsSalesOrderDetail.MeasureUnit,
                                OrderedQuantity = partsSalesOrderDetail.OrderedQuantity - (partsSalesOrderDetail.ApproveQuantity.HasValue ? partsSalesOrderDetail.ApproveQuantity.Value : 0),
                                ApproveQuantity = 0,
                                OriginalPrice = partsSalesOrderDetail.OriginalPrice,
                                CustOrderPriceGradeCoefficient = virtualPartsSalesPrice.Coefficient,
                                OrderPrice = partsSalesOrderDetail.OriginalPrice * (decimal)virtualPartsSalesPrice.Coefficient,
                                OrderSum = partsSalesOrderDetail.OrderPrice * partsSalesOrderDetail.OrderedQuantity - (partsSalesOrderDetail.ApproveQuantity.HasValue ? partsSalesOrderDetail.ApproveQuantity.Value : 0),
                                EstimatedFulfillTime = partsSalesOrderDetail.EstimatedFulfillTime,
                                PriceTypeName = partsSalesOrderDetail.PriceTypeName,
                                ABCStrategy = partsSalesOrderDetail.ABCStrategy
                            };
                            newPartsSalesOrder.PartsSalesOrderDetails.Add(newpartsSalesOrderDetail);
                        }
                    }
                    newPartsSalesOrder.TotalAmount -= newPartsSalesOrder.PartsSalesOrderDetails.Sum(r => (r.ApproveQuantity.HasValue ? r.ApproveQuantity.Value : 0) * r.OrderPrice);
                    InsertToDatabase(newPartsSalesOrder);
                    new PartsSalesOrderAch(this.DomainService).InsertPartsSalesOrderValidate(newPartsSalesOrder);
                }
            } catch(OptimisticConcurrencyException) {
                throw new ValidationException("订单正在被他人审批，请重新查询后审批！");
            }
        }

        public void 电商平台审核销售订单(PartsSalesOrderProcess partsSalesOrderProcess) {
            /*
            1、判断出库仓库是否是 WMS的仓库，若是，则走WMS接口进行操作，若不是，则进行以下操作
            */
            try {
                var currentUser = Utils.GetCurrentUserInfo();
                var partsSalesOrder = ObjectContext.PartsSalesOrders.Include("PartsSalesOrderDetails").Where(r => r.Id == partsSalesOrderProcess.OriginalSalesOrderId && r.Status != (int)DcsPartsSalesOrderStatus.作废).SetMergeOption(MergeOption.OverwriteChanges).SingleOrDefault();
                if(partsSalesOrder == null)
                    throw new ValidationException(ErrorStrings.PartsSalesOrderProcess_Validation1);
                var su = ObjectContext.SalesUnits.FirstOrDefault(v => v.Id == partsSalesOrder.SalesUnitId && v.Status == (int)DcsMasterDataStatus.有效);
                if(su == null)
                    throw new ValidationException(string.Format(ErrorStrings.PartsSalesOrderProcess_Validation9, partsSalesOrder.Code));
                var customerAccount = DomainService.查询客户可用资金(partsSalesOrder.CustomerAccountId, null, null);
                decimal rebateAmout = 0;
                var PartsRebateAccount = ObjectContext.PartsRebateAccounts.FirstOrDefault(r => r.BranchId == partsSalesOrder.SalesUnitOwnerCompanyId && r.CustomerCompanyId == partsSalesOrder.SubmitCompanyId && r.AccountGroupId == su.AccountGroupId);
                if(PartsRebateAccount != null) {
                    rebateAmout = PartsRebateAccount.AccountBalanceAmount;
                }
                if(customerAccount == null)
                    throw new ValidationException(string.Format(ErrorStrings.PartsSalesOrderProcess_Validation2, partsSalesOrder.Code));
                if((customerAccount.UseablePosition + rebateAmout) < partsSalesOrderProcess.CurrentFulfilledAmount)
                    //当前账户可用额度不足
                    throw new ValidationException(ErrorStrings.PartsSalesOrderProcess_Validation3);
                var invoiceReceiveCompany = ObjectContext.Companies.FirstOrDefault(r => r.Id == partsSalesOrder.InvoiceReceiveCompanyId && r.Status == (int)DcsMasterDataStatus.有效);
                if(invoiceReceiveCompany == null)
                    //对应的企业不存在
                    throw new ValidationException(string.Format(ErrorStrings.PartsSalesOrderProcess_Validation10, partsSalesOrder.Code));

                var partIds = partsSalesOrderProcess.PartsSalesOrderProcessDetails.Select(r => r.SparePartId).ToArray();
                var warehouseIds = partsSalesOrderProcess.PartsSalesOrderProcessDetails.Select(r => r.WarehouseId).ToArray();
                var warehouses = this.ObjectContext.Warehouses.Include("Branch").Where(r => warehouseIds.Contains(r.Id)).Distinct().ToArray();
                var destWarehouse = this.ObjectContext.Warehouses.SingleOrDefault(r => r.Id == partsSalesOrder.WarehouseId && r.Status == (int)DcsBaseDataStatus.有效);
                if(destWarehouse == null)
                    throw new ValidationException(string.Format("不存在名称为欧曼电商虚拟仓库的有效仓库数据"));
                var salesUnits = (from a in this.ObjectContext.SalesUnits
                                  join b in this.ObjectContext.SalesUnitAffiWarehouses.Where(r => warehouseIds.Contains(r.WarehouseId) || r.WarehouseId == destWarehouse.Id) on a.Id equals b.SalesUnitId
                                  select new {
                                      a.Id,
                                      a.PartsSalesCategoryId,
                                      b.WarehouseId
                                  }).ToArray();

                //更新销售订单审批数量 
                var partsSalesOrderdetails = partsSalesOrder.PartsSalesOrderDetails.Where(r => partIds.Contains(r.SparePartId)).ToArray();
                foreach(var partsSalesOrderDetail in partsSalesOrderdetails) {
                    var currentFulfilledQuantity = partsSalesOrderProcess.PartsSalesOrderProcessDetails.Where(r => r.SparePartId == partsSalesOrderDetail.SparePartId && r.OrderProcessMethod != (int)DcsPartsSalesOrderProcessDetailProcessMethod.供应商直发).Sum(v => v.CurrentFulfilledQuantity);
                    partsSalesOrderDetail.ApproveQuantity = (partsSalesOrderDetail.ApproveQuantity ?? 0) + currentFulfilledQuantity;
                    if(partsSalesOrderDetail.ApproveQuantity > partsSalesOrderDetail.OrderedQuantity)
                        throw new ValidationException("配件" + partsSalesOrderDetail.SparePartCode + "本次满足数量大于未满足数量");
                }

                var dbPartsSalesPrices = ObjectContext.PartsSalesPrices.Where(r => r.PartsSalesCategoryId == partsSalesOrder.SalesCategoryId && partIds.Contains(r.SparePartId)).ToList();
                var dbpartsPlannedPrices = ObjectContext.PartsPlannedPrices.Where(r => r.PartsSalesCategoryId == partsSalesOrder.SalesCategoryId && partIds.Contains(r.SparePartId)).ToList();

                var company = this.ObjectContext.Companies.SingleOrDefault(r => r.Code == "FT000333" && r.Status == (int)DcsBaseDataStatus.有效);
                var i = 0;
                foreach(var warehouse in warehouses) {
                    var storageCompany = this.ObjectContext.Companies.Single(r => r.Id == warehouse.StorageCompanyId);
                    var partsTransferOrder = new PartsTransferOrder();
                    partsTransferOrder.Id = i++;
                    partsTransferOrder.StorageCompanyId = warehouse.StorageCompanyId;
                    partsTransferOrder.StorageCompanyType = warehouse.StorageCompanyType;
                    partsTransferOrder.OriginalWarehouseId = warehouse.Id;
                    partsTransferOrder.OriginalWarehouseCode = warehouse.Code;
                    partsTransferOrder.OriginalWarehouseName = warehouse.Name;
                    partsTransferOrder.OriginalBillId = partsSalesOrder.Id;
                    partsTransferOrder.OriginalBillCode = partsSalesOrder.Code;
                    partsTransferOrder.DestWarehouseId = destWarehouse.Id;
                    partsTransferOrder.ShippingMethod = partsSalesOrder.ShippingMethod;
                    partsTransferOrder.DestWarehouseCode = destWarehouse.Code;
                    partsTransferOrder.DestWarehouseName = destWarehouse.Name;
                    partsTransferOrder.ERPSourceOrderCode = partsSalesOrder.ERPSourceOrderCode;
                    partsTransferOrder.Type = (int)DcsPartsTransferOrderType.急需调拨;
                    partsTransferOrder.TotalAmount = partsSalesOrder.TotalAmount;
                    partsTransferOrder.EcommerceMoney = partsSalesOrder.EcommerceMoney;
                    partsTransferOrder.Status = (int)DcsPartsTransferOrderStatus.已审批;
                    partsTransferOrder.Remark = partsSalesOrder.Remark;
                    partsTransferOrder.ModifierId = currentUser.Id;
                    partsTransferOrder.ModifierName = currentUser.Name;
                    partsTransferOrder.ModifyTime = DateTime.Now;
                    partsTransferOrder.ApproverId = currentUser.Id;
                    partsTransferOrder.ApproverName = currentUser.Name;
                    partsTransferOrder.ApproveTime = DateTime.Now;


                    //配件出库计划
                    var partsOutboundPlan = new PartsOutboundPlan();
                    partsOutboundPlan.Id = i++;
                    partsOutboundPlan.WarehouseId = partsTransferOrder.OriginalWarehouseId;
                    partsOutboundPlan.WarehouseCode = partsTransferOrder.OriginalWarehouseCode;
                    partsOutboundPlan.WarehouseName = partsTransferOrder.OriginalWarehouseName;
                    partsOutboundPlan.StorageCompanyId = partsTransferOrder.StorageCompanyId;
                    partsOutboundPlan.StorageCompanyName = storageCompany.Name;
                    partsOutboundPlan.StorageCompanyCode = storageCompany.Code;
                    partsOutboundPlan.StorageCompanyType = partsTransferOrder.StorageCompanyType;
                    partsOutboundPlan.BranchId = warehouse.BranchId;
                    partsOutboundPlan.BranchCode = warehouse.Branch.Code;
                    partsOutboundPlan.BranchName = warehouse.Branch.Name;
                    partsOutboundPlan.CounterpartCompanyId = storageCompany.Id;
                    partsOutboundPlan.CounterpartCompanyCode = storageCompany.Code;
                    partsOutboundPlan.CounterpartCompanyName = storageCompany.Name;
                    partsOutboundPlan.SourceId = partsTransferOrder.Id;
                    partsOutboundPlan.CustomerAccountId = partsSalesOrder.CustomerAccountId;
                    partsOutboundPlan.Remark = partsSalesOrder.Remark;

                    var salesUnit = salesUnits.FirstOrDefault(r => r.WarehouseId == partsOutboundPlan.WarehouseId);
                    partsOutboundPlan.PartsSalesCategoryId = salesUnit.PartsSalesCategoryId;
                    partsOutboundPlan.ReceivingCompanyId = storageCompany.Id;
                    partsOutboundPlan.ReceivingCompanyCode = storageCompany.Code;
                    partsOutboundPlan.ReceivingCompanyName = storageCompany.Name;
                    partsOutboundPlan.OutboundType = (int)DcsPartsOutboundType.配件调拨;
                    partsOutboundPlan.OriginalRequirementBillId = (int)partsTransferOrder.OriginalBillId;
                    partsOutboundPlan.OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.配件销售订单;

                    partsOutboundPlan.Status = warehouse.WmsInterface ? (int)DcsPartsOutboundPlanStatus.新建 : (int)DcsPartsOutboundPlanStatus.出库完成;
                    partsOutboundPlan.Remark = partsTransferOrder.Remark;
                    partsOutboundPlan.ReceivingWarehouseId = partsTransferOrder.DestWarehouseId;
                    partsOutboundPlan.ReceivingWarehouseCode = partsTransferOrder.DestWarehouseCode;
                    partsOutboundPlan.ReceivingWarehouseName = partsTransferOrder.DestWarehouseName;
                    partsOutboundPlan.ShippingMethod = (int)DcsPartsShippingMethod.公路物流;
                    partsOutboundPlan.ERPSourceOrderCode = partsTransferOrder.ERPSourceOrderCode;

                    partsOutboundPlan.IfWmsInterface = true;

                    var partsSalesOrderProcessDetails = partsSalesOrderProcess.PartsSalesOrderProcessDetails.Where(r => r.WarehouseId == warehouse.Id).ToArray();

                    var outpartIds = partsSalesOrderProcessDetails.Select(r => r.SparePartId).ToArray();

                    var partsLockeds = new PartsLockedStockAch(this.DomainService).GetPartsLockedStocksWithLock(ObjectContext.PartsLockedStocks.Where(r => r.WarehouseId == partsOutboundPlan.WarehouseId && outpartIds.Contains(r.PartId))).ToArray();


                    foreach(var detail in partsSalesOrderProcessDetails) {

                        var partsSalesPrice = dbPartsSalesPrices.FirstOrDefault(r => r.SparePartId == detail.SparePartId);
                        if(partsSalesPrice == null) {
                            throw new ValidationException(string.Format("配件{0}不存在销售价", detail.SparePartName));
                        }
                        var partsPlannedPrice = dbpartsPlannedPrices.FirstOrDefault(r => r.SparePartId == detail.SparePartId);
                        if(partsPlannedPrice == null) {
                            throw new ValidationException(string.Format("配件{0}不存在计划价", detail.SparePartName));
                        }
                        var partsSalesOrderDetail = partsSalesOrder.PartsSalesOrderDetails.FirstOrDefault(r => r.SparePartId == detail.SparePartId);
                        //调拨单清单
                        var partsTransferOrderDetail = new PartsTransferOrderDetail();
                        partsTransferOrderDetail.PartsTransferOrderId = partsTransferOrderDetail.Id;
                        partsTransferOrderDetail.SparePartId = detail.SparePartId;
                        partsTransferOrderDetail.SparePartCode = detail.SparePartCode;
                        partsTransferOrderDetail.SparePartName = detail.SparePartName;
                        partsTransferOrderDetail.PlannedAmount = detail.CurrentFulfilledQuantity;
                        partsTransferOrderDetail.ConfirmedAmount = detail.CurrentFulfilledQuantity;
                        partsTransferOrderDetail.Price = partsSalesPrice.SalesPrice;
                        partsTransferOrderDetail.PlannPrice = partsPlannedPrice.PlannedPrice;
                        partsTransferOrder.PartsTransferOrderDetails.Add(partsTransferOrderDetail);
                        //出库计划清单
                        var partsOutboundPlanDetail = new PartsOutboundPlanDetail();
                        partsOutboundPlanDetail.PartsOutboundPlanId = partsOutboundPlanDetail.Id;
                        partsOutboundPlanDetail.SparePartId = detail.SparePartId;
                        partsOutboundPlanDetail.SparePartCode = detail.SparePartCode;
                        partsOutboundPlanDetail.OriginalPrice = partsSalesOrderDetail.OriginalPrice;
                        partsOutboundPlanDetail.SparePartName = detail.SparePartName;
                        partsOutboundPlanDetail.PlannedAmount = detail.CurrentFulfilledQuantity;
                        if(partsOutboundPlan.Status == (int)DcsPartsOutboundPlanStatus.出库完成)
                            partsOutboundPlanDetail.OutboundFulfillment = detail.CurrentFulfilledQuantity;
                        partsOutboundPlanDetail.Price = partsPlannedPrice.PlannedPrice;
                        partsOutboundPlan.PartsOutboundPlanDetails.Add(partsOutboundPlanDetail);

                        var partsLockedStock = partsLockeds.FirstOrDefault(r => r.PartId == partsOutboundPlanDetail.SparePartId);
                        if(partsOutboundPlan.Status == (int)DcsPartsOutboundPlanStatus.新建) {
                            if(partsLockedStock == null) {
                                //增加锁定库存
                                var newpartsLockedStock = new PartsLockedStock {
                                    WarehouseId = partsOutboundPlan.WarehouseId,
                                    PartId = partsOutboundPlanDetail.SparePartId,
                                    LockedQuantity = partsOutboundPlanDetail.PlannedAmount,
                                    StorageCompanyId = partsOutboundPlan.StorageCompanyId,
                                    BranchId = partsOutboundPlan.BranchId
                                };
                                InsertToDatabase(newpartsLockedStock);
                                new PartsLockedStockAch(this.DomainService).InsertPartsLockedStockValidate(newpartsLockedStock);
                            } else {
                                partsLockedStock.LockedQuantity += partsOutboundPlanDetail.PlannedAmount;
                                new PartsLockedStockAch(this.DomainService).UpdatePartsLockedStockValidate(partsLockedStock);
                                UpdateToDatabase(partsLockedStock);
                            }
                        }
                    }
                    partsOutboundPlan.EcommerceMoney = partsOutboundPlan.PartsOutboundPlanDetails.Sum(r => r.OriginalPrice * r.PlannedAmount);
                    partsTransferOrder.EcommerceMoney = partsOutboundPlan.EcommerceMoney;
                    partsTransferOrder.TotalAmount = partsTransferOrder.PartsTransferOrderDetails.Sum(r => r.Price * r.PlannedAmount);
                    partsTransferOrder.TotalPlanAmount = partsTransferOrder.PartsTransferOrderDetails.Sum(r => r.PlannPrice * r.PlannedAmount);
                    InsertToDatabase(partsTransferOrder);
                    new PartsTransferOrderAch(this.DomainService).InsertPartsTransferOrderValidate(partsTransferOrder);

                    partsOutboundPlan.SourceCode = partsTransferOrder.Code;
                    partsOutboundPlan.OriginalRequirementBillCode = partsTransferOrder.OriginalBillCode;
                    InsertToDatabase(partsOutboundPlan);
                    new PartsOutboundPlanAch(this.DomainService).InsertPartsOutboundPlanValidate(partsOutboundPlan);
                    if(!warehouse.WmsInterface) {
                        partsOutboundPlan.IfWmsInterface = false;
                        //配件出库单
                        var partsOutboundBill = new PartsOutboundBill();
                        partsOutboundBill.Id = i++;
                        partsOutboundBill.PartsOutboundPlanId = partsOutboundPlan.Id;
                        partsOutboundBill.WarehouseId = partsTransferOrder.OriginalWarehouseId;
                        partsOutboundBill.WarehouseCode = partsTransferOrder.OriginalWarehouseCode;
                        partsOutboundBill.WarehouseName = partsTransferOrder.OriginalWarehouseName;
                        partsOutboundBill.StorageCompanyId = partsTransferOrder.StorageCompanyId;
                        partsOutboundBill.StorageCompanyName = storageCompany.Name;
                        partsOutboundBill.StorageCompanyCode = storageCompany.Code;
                        partsOutboundBill.StorageCompanyType = partsTransferOrder.StorageCompanyType;
                        partsOutboundBill.BranchId = warehouse.BranchId;
                        partsOutboundBill.BranchCode = warehouse.Branch.Code;
                        partsOutboundBill.BranchName = warehouse.Branch.Name;
                        partsOutboundBill.CounterpartCompanyId = storageCompany.Id;
                        partsOutboundBill.CounterpartCompanyCode = storageCompany.Code;
                        partsOutboundBill.CounterpartCompanyName = storageCompany.Name;
                        partsOutboundBill.ReceivingCompanyId = storageCompany.Id;
                        partsOutboundBill.ReceivingCompanyCode = storageCompany.Code;
                        partsOutboundBill.ReceivingCompanyName = storageCompany.Name;
                        partsOutboundBill.OutboundType = (int)DcsPartsOutboundType.配件调拨;
                        partsOutboundBill.OriginalRequirementBillId = (int)partsTransferOrder.OriginalBillId;
                        partsOutboundBill.OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.配件销售订单;

                        partsOutboundBill.Remark = partsTransferOrder.Remark;
                        partsOutboundBill.ReceivingWarehouseId = partsTransferOrder.DestWarehouseId;
                        partsOutboundBill.ReceivingWarehouseCode = partsTransferOrder.DestWarehouseCode;
                        partsOutboundBill.ReceivingWarehouseName = partsTransferOrder.DestWarehouseName;
                        partsOutboundBill.ShippingMethod = (int)DcsPartsShippingMethod.公路物流;
                        partsOutboundBill.ERPSourceOrderCode = partsTransferOrder.ERPSourceOrderCode;

                        partsOutboundBill.PartsSalesCategoryId = salesUnit.PartsSalesCategoryId;
                        partsOutboundBill.SettlementStatus = (int)DcsPartsSettlementStatus.不结算;


                        //配件入库计划
                        var partsInboundPlan = new PartsInboundPlan();
                        partsInboundPlan.Id = i++;
                        partsInboundPlan.WarehouseId = (int)partsOutboundBill.ReceivingWarehouseId;
                        partsInboundPlan.WarehouseCode = partsOutboundBill.ReceivingWarehouseCode;
                        partsInboundPlan.WarehouseName = partsOutboundBill.ReceivingWarehouseName;
                        var w = this.ObjectContext.Warehouses.Include("Branch").SingleOrDefault(r => r.Id == partsInboundPlan.WarehouseId);
                        var wStorageCompany = this.ObjectContext.Companies.Single(r => r.Id == w.StorageCompanyId);
                        partsInboundPlan.StorageCompanyId = w.StorageCompanyId;
                        partsInboundPlan.StorageCompanyCode = wStorageCompany.Code;
                        partsInboundPlan.StorageCompanyName = wStorageCompany.Name;
                        partsInboundPlan.StorageCompanyType = w.StorageCompanyType;
                        partsInboundPlan.BranchId = partsOutboundBill.BranchId;
                        partsInboundPlan.BranchCode = partsOutboundBill.BranchCode;
                        partsInboundPlan.BranchName = partsOutboundBill.BranchName;
                        partsInboundPlan.SourceId = partsTransferOrder.Id; //调拨单
                        partsInboundPlan.CounterpartCompanyId = storageCompany.Id;
                        partsInboundPlan.CounterpartCompanyCode = storageCompany.Code;
                        partsInboundPlan.CounterpartCompanyName = storageCompany.Name;
                        partsInboundPlan.Remark = partsSalesOrder.Remark;

                        partsInboundPlan.Status = (int)DcsPartsInboundPlanStatus.检验完成;
                        partsInboundPlan.ERPSourceOrderCode = partsSalesOrder.ERPSourceOrderCode;
                        partsInboundPlan.EcommerceMoney = partsSalesOrder.EcommerceMoney;
                        partsInboundPlan.InboundType = (int)DcsPartsInboundType.配件调拨;
                        var s = salesUnits.FirstOrDefault(r => r.WarehouseId == partsInboundPlan.WarehouseId);
                        partsInboundPlan.PartsSalesCategoryId = s.PartsSalesCategoryId;
                        partsInboundPlan.OriginalRequirementBillId = partsTransferOrder.Id;
                        partsInboundPlan.OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.配件调拨单;


                        //配件入库检验单
                        var partsInboundCheckBill = new PartsInboundCheckBill();
                        partsInboundCheckBill.Id = i++;
                        partsInboundCheckBill.PartsInboundPlanId = partsInboundPlan.Id;
                        partsInboundCheckBill.WarehouseId = (int)partsOutboundBill.ReceivingWarehouseId;
                        partsInboundCheckBill.WarehouseCode = partsOutboundBill.ReceivingWarehouseCode;
                        partsInboundCheckBill.WarehouseName = partsOutboundBill.ReceivingWarehouseName;
                        partsInboundCheckBill.StorageCompanyId = w.StorageCompanyId;
                        partsInboundCheckBill.StorageCompanyCode = wStorageCompany.Code;
                        partsInboundCheckBill.StorageCompanyName = wStorageCompany.Name;
                        partsInboundCheckBill.StorageCompanyType = w.StorageCompanyType;
                        partsInboundCheckBill.BranchId = partsOutboundBill.BranchId;
                        partsInboundCheckBill.BranchCode = partsOutboundBill.BranchCode;
                        partsInboundCheckBill.CounterpartCompanyId = partsOutboundBill.StorageCompanyId;
                        partsInboundCheckBill.CounterpartCompanyCode = partsOutboundBill.StorageCompanyCode;
                        partsInboundCheckBill.CounterpartCompanyName = partsOutboundBill.StorageCompanyName;
                        partsInboundCheckBill.BranchName = partsOutboundBill.BranchName;
                        partsInboundCheckBill.Status = (int)DcsPartsInboundCheckBillStatus.上架完成;
                        partsInboundCheckBill.ERPSourceOrderCode = partsSalesOrder.ERPSourceOrderCode;
                        partsInboundCheckBill.EcommerceMoney = partsSalesOrder.EcommerceMoney;
                        partsInboundCheckBill.InboundType = (int)DcsPartsInboundType.配件调拨;
                        partsInboundCheckBill.PartsSalesCategoryId = s.PartsSalesCategoryId;
                        partsInboundCheckBill.OriginalRequirementBillId = partsTransferOrder.Id;
                        partsInboundCheckBill.OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.配件调拨单;
                        partsInboundCheckBill.SettlementStatus = (int)DcsPartsSettlementStatus.不结算;
                        //配件出库计划
                        var partsOutboundPlanBranch = new PartsOutboundPlan();
                        partsOutboundPlanBranch.WarehouseId = partsTransferOrder.DestWarehouseId;
                        partsOutboundPlanBranch.WarehouseCode = partsTransferOrder.DestWarehouseCode;
                        partsOutboundPlanBranch.WarehouseName = partsTransferOrder.DestWarehouseName;
                        partsOutboundPlanBranch.StorageCompanyId = partsTransferOrder.StorageCompanyId;
                        partsOutboundPlanBranch.StorageCompanyCode = storageCompany.Code;
                        partsOutboundPlanBranch.StorageCompanyName = storageCompany.Name;
                        partsOutboundPlanBranch.StorageCompanyType = partsTransferOrder.StorageCompanyType;
                        partsOutboundPlanBranch.BranchId = warehouse.Branch.Id;
                        partsOutboundPlanBranch.BranchCode = warehouse.Branch.Code;

                        partsOutboundPlanBranch.BranchName = warehouse.Branch.Name;

                        partsOutboundPlanBranch.ReceivingCompanyId = company.Id;
                        partsOutboundPlanBranch.ReceivingCompanyCode = company.Code;
                        partsOutboundPlanBranch.ReceivingCompanyName = company.Name;
                        partsOutboundPlanBranch.CustomerAccountId = partsSalesOrder.CustomerAccountId;
                        partsOutboundPlanBranch.Remark = partsSalesOrder.Remark;

                        partsOutboundPlanBranch.CounterpartCompanyId = company.Id;
                        partsOutboundPlanBranch.CounterpartCompanyCode = company.Code;
                        partsOutboundPlanBranch.CounterpartCompanyName = company.Name;
                        partsOutboundPlanBranch.SourceCode = partsSalesOrder.Code;
                        partsOutboundPlanBranch.OutboundType = (int)DcsPartsOutboundType.配件销售;
                        partsOutboundPlanBranch.SourceId = partsSalesOrder.Id;

                        partsOutboundPlanBranch.Status = (int)DcsPartsOutboundPlanStatus.新建;
                        partsOutboundPlanBranch.ShippingMethod = (int)DcsPartsShippingMethod.公路物流;
                        partsOutboundPlanBranch.PartsSalesCategoryId = partsSalesOrder.SalesCategoryId;
                        partsOutboundPlanBranch.ERPSourceOrderCode = partsSalesOrder.ERPSourceOrderCode;
                        partsOutboundPlanBranch.EcommerceMoney = partsSalesOrder.EcommerceMoney;

                        partsOutboundPlanBranch.OriginalRequirementBillId = (int)partsTransferOrder.OriginalBillId;
                        partsOutboundPlanBranch.OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.配件销售订单;
                        partsOutboundPlanBranch.PartsSalesOrderTypeId = partsSalesOrder.PartsSalesOrderTypeId;
                        partsOutboundPlanBranch.PartsSalesOrderTypeName = partsSalesOrder.PartsSalesOrderTypeName;


                        var inPartsPlannedPrices = ObjectContext.PartsPlannedPrices.Where(r => r.OwnerCompanyId == partsInboundCheckBill.StorageCompanyId && r.PartsSalesCategoryId == partsInboundCheckBill.PartsSalesCategoryId && outpartIds.Contains(r.SparePartId)).ToList();

                        var dbPartsStocks = new PartsStockAch(this.DomainService).GetPartsStocksWithLock(ObjectContext.PartsStocks.Include("WarehouseArea").Where(r => r.WarehouseId == partsOutboundBill.WarehouseId && outpartIds.Contains(r.PartId) && r.WarehouseAreaCategory.Category == (int)DcsAreaType.保管区)).ToList();
                        var dbPartsPlannedPrices = ObjectContext.PartsPlannedPrices.Where(r => r.OwnerCompanyId == partsOutboundBill.StorageCompanyId && r.PartsSalesCategoryId == partsOutboundBill.PartsSalesCategoryId && outpartIds.Contains(r.SparePartId)).ToList();


                        var inPartsStocks = new PartsStockAch(this.DomainService).GetPartsStocksWithLock(ObjectContext.PartsStocks.Include("WarehouseArea").Where(r => r.WarehouseId == partsInboundPlan.WarehouseId && outpartIds.Contains(r.PartId) && r.WarehouseAreaCategory.Category == (int)DcsAreaType.保管区)).ToList();
                        var warehouseAreaIds = inPartsStocks.Select(r => r.WarehouseAreaId).ToArray();
                        var dbWarehouseAreas = ObjectContext.WarehouseAreas.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && warehouseAreaIds.Contains(r.Id) || (r.WarehouseId == partsInboundCheckBill.WarehouseId && r.WarehouseAreaCategory.Category == (int)DcsAreaType.保管区 && r.AreaKind == (int)DcsAreaKind.库位));
                        var inWarehouseArea = dbWarehouseAreas.FirstOrDefault(r => r.WarehouseId == partsInboundCheckBill.WarehouseId && r.WarehouseAreaCategory.Category == (int)DcsAreaType.保管区 && r.AreaKind == (int)DcsAreaKind.库位);
                        if(inWarehouseArea == null)
                            throw new ValidationException(string.Format("仓库编号:{0}对应的保管区库位不存在", partsInboundCheckBill.WarehouseCode));
                        var partsLockedStocks = new PartsLockedStockAch(this.DomainService).GetPartsLockedStocksWithLock(ObjectContext.PartsLockedStocks.Where(r => r.WarehouseId == partsOutboundPlanBranch.WarehouseId && outpartIds.Contains(r.PartId))).ToArray();

                        //生成各种清单
                        foreach(var detail in partsSalesOrderProcessDetails) {
                            var partsSalesPrice = dbPartsSalesPrices.First(r => r.SparePartId == detail.SparePartId);
                            var partsPlannedPrice = dbpartsPlannedPrices.First(r => r.SparePartId == detail.SparePartId);
                            var partsSalesOrderDetail1 = partsSalesOrder.PartsSalesOrderDetails.FirstOrDefault(r => r.SparePartId == detail.SparePartId);
                            //调拨单清单
                            //var partsTransferOrderDetail = new PartsTransferOrderDetail();
                            //partsTransferOrderDetail.PartsTransferOrderId = partsTransferOrderDetail.Id;
                            //partsTransferOrderDetail.SparePartId = detail.SparePartId;
                            //partsTransferOrderDetail.SparePartCode = detail.SparePartCode;
                            //partsTransferOrderDetail.SparePartName = detail.SparePartName;
                            //partsTransferOrderDetail.PlannedAmount = detail.CurrentFulfilledQuantity;
                            //partsTransferOrderDetail.ConfirmedAmount = detail.CurrentFulfilledQuantity;
                            //partsTransferOrderDetail.Price = partsSalesPrice.SalesPrice;
                            //partsTransferOrderDetail.PlannPrice = partsPlannedPrice.PlannedPrice;
                            //partsTransferOrder.PartsTransferOrderDetails.Add(partsTransferOrderDetail);
                            ////出库计划清单
                            //var partsOutboundPlanDetail = new PartsOutboundPlanDetail();
                            //partsOutboundPlanDetail.PartsOutboundPlanId = partsOutboundPlanDetail.Id;
                            //partsOutboundPlanDetail.SparePartId = detail.SparePartId;
                            //partsOutboundPlanDetail.SparePartCode = detail.SparePartCode;
                            //partsOutboundPlanDetail.SparePartName = detail.SparePartName;
                            //partsOutboundPlanDetail.PlannedAmount = detail.CurrentFulfilledQuantity;
                            //partsOutboundPlanDetail.OutboundFulfillment = detail.CurrentFulfilledQuantity;
                            //partsOutboundPlanDetail.Price = partsSalesPrice.SalesPrice;
                            //partsOutboundPlan.PartsOutboundPlanDetails.Add(partsOutboundPlanDetail);
                            //更新库存减去
                            var partsStock = dbPartsStocks.Where(r => r.PartId == detail.SparePartId).OrderByDescending(r => r.Quantity);
                            if(!partsStock.Any())
                                throw new ValidationException(string.Format(ErrorStrings.PartsTransferOrder_Validation12, detail.SparePartCode));
                            if(partsStock.Sum(r => r.Quantity) < detail.CurrentFulfilledQuantity)
                                throw new ValidationException(string.Format(ErrorStrings.PartsTransferOrder_Validation13, detail.SparePartCode));
                            var dbPartsPlannedPrice = dbPartsPlannedPrices.FirstOrDefault(r => r.SparePartId == detail.SparePartId);
                            var partsOutboundPlanDetail = partsOutboundPlan.PartsOutboundPlanDetails.SingleOrDefault(r => r.SparePartId == detail.SparePartId);
                            foreach(var stock in partsStock.ToList()) {
                                var partsOutboundBillDetail = new PartsOutboundBillDetail();
                                partsOutboundBillDetail.PartsOutboundBillId = partsOutboundBill.Id;
                                if(stock.Quantity >= detail.CurrentFulfilledQuantity) {
                                    stock.Quantity -= detail.CurrentFulfilledQuantity;
                                    partsOutboundBillDetail.SparePartId = detail.SparePartId;
                                    partsOutboundBillDetail.SparePartCode = detail.SparePartCode;
                                    partsOutboundBillDetail.SparePartName = detail.SparePartName;
                                    partsOutboundBillDetail.OutboundAmount = detail.CurrentFulfilledQuantity;
                                    partsOutboundBillDetail.WarehouseAreaId = stock.WarehouseAreaId;
                                    partsOutboundBillDetail.WarehouseAreaCode = stock.WarehouseArea.Code;
                                    partsOutboundBillDetail.SettlementPrice = partsSalesPrice.SalesPrice;
                                    partsOutboundBillDetail.CostPrice = dbPartsPlannedPrice != null ? dbPartsPlannedPrice.PlannedPrice : default(decimal);
                                    partsOutboundBillDetail.OriginalPrice = partsOutboundPlanDetail.OriginalPrice;
                                    partsOutboundBill.PartsOutboundBillDetails.Add(partsOutboundBillDetail);
                                    UpdateToDatabase(stock);
                                    new PartsStockAch(this.DomainService).UpdatePartsStockValidate(stock);
                                    break;
                                }

                                partsOutboundPlanDetail.OutboundFulfillment -= stock.Quantity;
                                partsOutboundBillDetail.OriginalPrice = partsOutboundPlanDetail.OriginalPrice;
                                partsOutboundBillDetail.SparePartId = detail.SparePartId;
                                partsOutboundBillDetail.SparePartCode = detail.SparePartCode;
                                partsOutboundBillDetail.SparePartName = detail.SparePartName;
                                partsOutboundBillDetail.OutboundAmount = stock.Quantity;
                                partsOutboundBillDetail.WarehouseAreaId = stock.WarehouseAreaId;
                                partsOutboundBillDetail.WarehouseAreaCode = stock.WarehouseArea.Code;
                                partsOutboundBillDetail.SettlementPrice = partsSalesPrice.SalesPrice;
                                partsOutboundBillDetail.CostPrice = dbPartsPlannedPrice != null ? dbPartsPlannedPrice.PlannedPrice : default(decimal);
                                partsOutboundBill.PartsOutboundBillDetails.Add(partsOutboundBillDetail);
                                stock.Quantity = 0;
                                UpdateToDatabase(stock);
                                new PartsStockAch(this.DomainService).UpdatePartsStockValidate(stock);
                            }

                            //入库计划清单
                            var partsInboundPlanDetail = new PartsInboundPlanDetail();
                            partsInboundPlanDetail.PartsInboundPlanId = partsInboundPlan.Id;
                            partsInboundPlanDetail.SparePartId = detail.SparePartId;
                            partsInboundPlanDetail.SparePartCode = detail.SparePartCode;
                            partsInboundPlanDetail.SparePartName = detail.SparePartName;
                            partsInboundPlanDetail.PlannedAmount = detail.CurrentFulfilledQuantity;
                            partsInboundPlanDetail.InspectedQuantity = detail.CurrentFulfilledQuantity;
                            partsInboundPlanDetail.Price = partsSalesPrice.SalesPrice;
                            partsInboundPlan.PartsInboundPlanDetails.Add(partsInboundPlanDetail);
                            //入库单清单  更新库存新增
                            int warehouseAreaId;
                            string warehouseAreaCode;
                            if(inPartsStocks.All(r => r.PartId != detail.SparePartId)) {
                                var newPartsStock = new PartsStock();
                                newPartsStock.WarehouseId = partsInboundCheckBill.WarehouseId;
                                newPartsStock.StorageCompanyId = partsInboundCheckBill.StorageCompanyId;
                                newPartsStock.StorageCompanyType = partsInboundCheckBill.StorageCompanyType;
                                newPartsStock.BranchId = partsInboundCheckBill.BranchId;
                                newPartsStock.WarehouseAreaId = inWarehouseArea.Id;
                                newPartsStock.WarehouseAreaCategoryId = inWarehouseArea.AreaCategoryId;
                                newPartsStock.PartId = detail.SparePartId;
                                newPartsStock.Quantity = partsInboundPlanDetail.InspectedQuantity ?? default(int);
                                warehouseAreaId = inWarehouseArea.Id;
                                warehouseAreaCode = inWarehouseArea.Code;
                                InsertToDatabase(newPartsStock);
                                new PartsStockAch(this.DomainService).InsertPartsStockValidate(newPartsStock);
                                partsStockLists.Add(newPartsStock);
                            } else {
                                var inPartsStock = inPartsStocks.First(r => r.PartId == detail.SparePartId);
                                inPartsStock.Quantity += partsInboundPlanDetail.InspectedQuantity ?? default(int);
                                warehouseAreaId = inPartsStock.WarehouseAreaId;
                                warehouseAreaCode = dbWarehouseAreas.First(r => r.Id == inPartsStock.WarehouseAreaId).Code;
                                UpdateToDatabase(inPartsStock);
                                new PartsStockAch(this.DomainService).UpdatePartsStockValidate(inPartsStock);
                            }
                            var costPrice = inPartsPlannedPrices.FirstOrDefault(r => r.SparePartId == detail.SparePartId);
                            var partsInboundCheckBillDetail = new PartsInboundCheckBillDetail();
                            partsInboundCheckBillDetail.PartsInboundCheckBillId = partsInboundCheckBill.Id;
                            partsInboundCheckBillDetail.SparePartId = detail.SparePartId;
                            partsInboundCheckBillDetail.SparePartCode = detail.SparePartCode;
                            partsInboundCheckBillDetail.SparePartName = detail.SparePartName;
                            partsInboundCheckBillDetail.WarehouseAreaId = warehouseAreaId;
                            partsInboundCheckBillDetail.WarehouseAreaCode = warehouseAreaCode;
                            partsInboundCheckBillDetail.InspectedQuantity = partsInboundPlanDetail.InspectedQuantity ?? default(int);
                            partsInboundCheckBillDetail.SettlementPrice = partsInboundPlanDetail.Price;
                            partsInboundCheckBillDetail.CostPrice = costPrice != null ? costPrice.PlannedPrice : default(decimal);
                            partsInboundCheckBill.PartsInboundCheckBillDetails.Add(partsInboundCheckBillDetail);

                            //出库计划清单 
                            var partsOutboundPlanDetailBranch = new PartsOutboundPlanDetail();
                            partsOutboundPlanDetailBranch.SparePartId = detail.SparePartId;
                            partsOutboundPlanDetailBranch.SparePartCode = detail.SparePartCode;
                            partsOutboundPlanDetailBranch.OriginalPrice = partsOutboundPlanDetail.OriginalPrice;
                            partsOutboundPlanDetailBranch.SparePartName = detail.SparePartName;
                            partsOutboundPlanDetailBranch.PlannedAmount = detail.CurrentFulfilledQuantity;
                            partsOutboundPlanDetailBranch.OutboundFulfillment = default(int);
                            partsOutboundPlanDetailBranch.Price = partsSalesPrice.SalesPrice;
                            partsOutboundPlanBranch.PartsOutboundPlanDetails.Add(partsOutboundPlanDetailBranch);

                            var partsLockedStock = partsLockedStocks.FirstOrDefault(r => r.PartId == partsOutboundPlanDetailBranch.SparePartId);

                            if(partsLockedStock == null) {
                                //增加锁定库存
                                var newpartsLockedStock = new PartsLockedStock {
                                    WarehouseId = partsOutboundPlanBranch.WarehouseId,
                                    PartId = partsOutboundPlanDetailBranch.SparePartId,
                                    LockedQuantity = partsOutboundPlanDetailBranch.PlannedAmount,
                                    StorageCompanyId = partsOutboundPlan.StorageCompanyId,
                                    BranchId = partsOutboundPlanBranch.BranchId
                                };
                                InsertToDatabase(newpartsLockedStock);
                                new PartsLockedStockAch(this.DomainService).InsertPartsLockedStockValidate(newpartsLockedStock);
                            } else {
                                partsLockedStock.LockedQuantity += partsOutboundPlanDetailBranch.PlannedAmount;
                                new PartsLockedStockAch(this.DomainService).UpdatePartsLockedStockValidate(partsLockedStock);
                                UpdateToDatabase(partsLockedStock);
                            }
                        }
                        partsOutboundBill.EcommerceMoney = partsOutboundBill.PartsOutboundBillDetails.Sum(r => r.OriginalPrice * r.OutboundAmount);
                        partsOutboundBill.OriginalRequirementBillCode = partsTransferOrder.OriginalBillCode;
                        InsertToDatabase(partsOutboundBill);
                        new PartsOutboundBillAch(this.DomainService).InsertPartsOutboundBillValidate(partsOutboundBill);

                        partsInboundPlan.SourceCode = partsTransferOrder.Code;
                        partsInboundPlan.OriginalRequirementBillCode = partsTransferOrder.Code;
                        InsertToDatabase(partsInboundPlan);
                        new PartsInboundPlanAch(this.DomainService).InsertPartsInboundPlanValidate(partsInboundPlan);

                        partsInboundCheckBill.OriginalRequirementBillCode = partsTransferOrder.Code;
                        InsertToDatabase(partsInboundCheckBill);
                        new PartsInboundCheckBillAch(this.DomainService).InsertPartsInboundCheckBillValidate(partsInboundCheckBill);

                        partsOutboundPlanBranch.OriginalRequirementBillCode = partsTransferOrder.OriginalBillCode;
                        InsertToDatabase(partsOutboundPlanBranch);
                        new PartsOutboundPlanAch(this.DomainService).InsertPartsOutboundPlanValidate(partsOutboundPlanBranch);
                    }
                }
                partsSalesOrder.Time = (partsSalesOrder.Time ?? 0) + 1;
                partsSalesOrder.Status = partsSalesOrder.PartsSalesOrderDetails.Any(v => v.ApproveQuantity != v.OrderedQuantity && v.OrderedQuantity > 0) ? partsSalesOrder.Status = (int)DcsPartsSalesOrderStatus.部分审批 : (int)DcsPartsSalesOrderStatus.审批完成;
                partsSalesOrderProcess.BillStatusAfterProcess = partsSalesOrder.Status;
                partsSalesOrderProcess.Time = partsSalesOrder.Time;
                this.InsertPartsSalesOrderProcessValidate(partsSalesOrderProcess);

                partsSalesOrder.ApproverId = currentUser.Id;
                partsSalesOrder.ApproverName = currentUser.Name;
                partsSalesOrder.ApproveTime = DateTime.Now;
                partsSalesOrder.ModifierId = currentUser.Id;
                partsSalesOrder.ModifierName = currentUser.Name;
                partsSalesOrder.ModifyTime = DateTime.Now;
                new PartsSalesOrderAch(this.DomainService).InsertPartsSalesOrderValidate(partsSalesOrder);
            } catch(OptimisticConcurrencyException) {
                throw new ValidationException(ErrorStrings.PartsSalesOrderProcess_Validation21);
            }
        }

        /// <summary>
        /// 审批配件销售订单正常订单
        /// </summary>
        /// <param name="partsSalesOrderProcess"></param>
        /// <param name="partsSalesOrderTypeId"></param>
        /// <param name="remark"></param>
        /// <param name="salesCategoryId"></param>
        /// <param name="isCheckMinSaleQuantity">是否校验保底库存-审核本部处理</param>
        public void 审批配件销售订单正常订单2(PartsSalesOrderProcess partsSalesOrderProcess, int partsSalesOrderTypeId, string remark, int salesCategoryId, bool isCheckMinSaleQuantity) {
            try {
                var partsSalesCategory = ObjectContext.PartsSalesCategories.FirstOrDefault(v => v.Id == salesCategoryId);
                var partsSalesOrder = ObjectContext.PartsSalesOrders.Include("PartsSalesOrderDetails").Where(r => r.Id == partsSalesOrderProcess.OriginalSalesOrderId && r.Status != (int)DcsPartsSalesOrderStatus.作废).SetMergeOption(MergeOption.OverwriteChanges).SingleOrDefault();
                if(partsSalesOrder == null)
                    throw new ValidationException(ErrorStrings.PartsSalesOrderProcess_Validation1);

                var company = ObjectContext.Companies.FirstOrDefault(o => o.Id == partsSalesOrder.SalesUnitOwnerCompanyId);
                if(partsSalesOrder.Status == (int)DcsPartsSalesOrderStatus.审批完成) {
                    throw new ValidationException(ErrorStrings.PartsSalesOrderProcess_Validation22);
                }
                new PartsSalesOrderAch(this.DomainService).PMS_SYC_150_记录日志用(partsSalesOrder);
                if(partsSalesOrder.Status == (int)DcsPartsSalesOrderStatus.提交) {
                    partsSalesOrder.FirstApproveTime = DateTime.Now;
                }
                //销售订单审批报错根据日志查询 可能是 出库仓库 名称和  编号是null导致
                foreach(var partsSalesOrderProcessDetail in partsSalesOrderProcess.PartsSalesOrderProcessDetails.Where(r => string.IsNullOrEmpty(r.WarehouseCode))) {
                    var warehouse = this.ObjectContext.Warehouses.SingleOrDefault(r => r.Id == partsSalesOrderProcessDetail.WarehouseId);
                    if(warehouse != null) {
                        partsSalesOrderProcessDetail.WarehouseCode = warehouse.Code;
                        partsSalesOrderProcessDetail.WarehouseName = warehouse.Name;
                    }
                }
                var partsSalesOrderDetails = ObjectContext.PartsSalesOrderDetails.Where(r => r.PartsSalesOrderId == partsSalesOrder.Id).SetMergeOption(MergeOption.OverwriteChanges).ToArray();
                var partsSalesOrderProcessDetails = partsSalesOrderProcess.PartsSalesOrderProcessDetails;
                //修改订单类型
                partsSalesOrder.Remark = remark;
                partsSalesOrder.ApprovalComment = partsSalesOrderProcess.ApprovalComment;
                UpdateToDatabase(partsSalesOrder);
                var salesUnit = ObjectContext.SalesUnits.FirstOrDefault(v => v.Id == partsSalesOrder.SalesUnitId && v.Status == (int)DcsMasterDataStatus.有效);
                if(salesUnit == null)
                    throw new ValidationException(string.Format(ErrorStrings.PartsSalesOrderProcess_Validation9, partsSalesOrder.Code));
                var customerAccount = DomainService.查询客户可用资金(partsSalesOrder.CustomerAccountId, null, null);

                //查询客户可用资金该方法中只传第一个参数时并不计算返利余额
                decimal rebateAmout = 0;
                var PartsRebateAccount = ObjectContext.PartsRebateAccounts.FirstOrDefault(r => r.BranchId == partsSalesOrder.SalesUnitOwnerCompanyId && r.CustomerCompanyId == partsSalesOrder.SubmitCompanyId && r.AccountGroupId == salesUnit.AccountGroupId);
                if(PartsRebateAccount != null) {
                    rebateAmout = PartsRebateAccount.AccountBalanceAmount;
                }
                if(customerAccount == null)
                    throw new ValidationException(string.Format(ErrorStrings.PartsSalesOrderProcess_Validation2, partsSalesOrder.Code));
                if(customerAccount.UseablePosition + rebateAmout < partsSalesOrderProcess.CurrentFulfilledAmount)
                    //当前账户可用额度不足
                    throw new ValidationException(ErrorStrings.PartsSalesOrderProcess_Validation3);
                var invoiceReceiveCompany = ObjectContext.Companies.FirstOrDefault(r => r.Id == partsSalesOrder.InvoiceReceiveCompanyId && r.Status == (int)DcsMasterDataStatus.有效);
                if(invoiceReceiveCompany == null)
                    //对应的企业不存在
                    throw new ValidationException(string.Format(ErrorStrings.PartsSalesOrderProcess_Validation10, partsSalesOrder.Code));
                var detailPartIds = partsSalesOrderProcessDetails.Where(r => r.OrderProcessMethod == (int)DcsPartsSalesOrderProcessDetailProcessMethod.本部处理 || r.OrderProcessMethod == (int)DcsPartsSalesOrderProcessDetailProcessMethod.积压件平台 || r.OrderProcessMethod == (int)DcsPartsSalesOrderProcessDetailProcessMethod.供应商直发 || r.OrderProcessMethod == (int)DcsPartsSalesOrderProcessDetailProcessMethod.转中心库 || r.OrderProcessMethod == (int)DcsPartsSalesOrderProcessDetailProcessMethod.转中心库采购).Select(r => r.SparePartId).ToArray();
                var upPartsSalesDetails = partsSalesOrderDetails.Where(r => detailPartIds.Contains(r.SparePartId)).ToArray();
                foreach(var partsSalesOrderDetail in upPartsSalesDetails) {
                    var currentFulfilledQuantity = partsSalesOrderProcessDetails.Where(r => r.SparePartId == partsSalesOrderDetail.SparePartId && r.OrderProcessMethod != (int)DcsPartsSalesOrderProcessDetailProcessMethod.供应商直发).Sum(v => v.CurrentFulfilledQuantity);
                    partsSalesOrderDetail.ApproveQuantity = (partsSalesOrderDetail.ApproveQuantity ?? 0) + currentFulfilledQuantity;
                    if(partsSalesOrderDetail.ApproveQuantity > partsSalesOrderDetail.OrderedQuantity)
                        throw new ValidationException(string.Format(ErrorStrings.PartsSalesOrderProcess_Validation23, partsSalesOrderDetail.SparePartCode));
                }

                var upProcessDetail = partsSalesOrderProcessDetails.Where(v => v.OrderProcessStatus == (int)DcsPartsSalesOrderProcessDetailOrderProcessStatus.未满足 && upPartsSalesDetails.Any(detail => detail.SparePartId == v.SparePartId && detail.ApproveQuantity == detail.OrderedQuantity)).ToArray();
                foreach(var detail in upProcessDetail)
                    detail.OrderProcessStatus = (int)DcsPartsSalesOrderProcessDetailOrderProcessStatus.满足;

                var currentUser = Utils.GetCurrentUserInfo();
                partsSalesOrder.ApproverId = currentUser.Id;
                partsSalesOrder.ApproverName = currentUser.Name;
                partsSalesOrder.ApproveTime = DateTime.Now;
                partsSalesOrder.Time = (partsSalesOrder.Time ?? 0) + 1;
                new PartsSalesOrderAch(this.DomainService).UpdatePartsSalesOrderValidate(partsSalesOrder);
                var passAccountAmount = partsSalesOrderProcessDetails.Where(r => r.OrderProcessMethod != (int)DcsPartsSalesOrderProcessDetailProcessMethod.供应商直发 && r.OrderProcessMethod != (int)DcsPartsSalesOrderProcessDetailProcessMethod.转中心库 && r.OrderProcessMethod != (int)DcsPartsSalesOrderProcessDetailProcessMethod.转中心库采购).Sum(r => r.CurrentFulfilledQuantity * r.OrderPrice);
                var passAccountAmountForCenter = partsSalesOrderProcessDetails.Where(r => r.OrderProcessMethod == (int)DcsPartsSalesOrderProcessDetailProcessMethod.转中心库 || r.OrderProcessMethod == (int)DcsPartsSalesOrderProcessDetailProcessMethod.转中心库采购).Sum(r => r.CurrentFulfilledQuantity * r.OrderPrice);

                #region 处理方式为：积压件平台
                var processDetailsBySupplierCompany = partsSalesOrderProcessDetails.Where(r => r.OrderProcessMethod == (int)DcsPartsSalesOrderProcessDetailProcessMethod.积压件平台).GroupBy(r => new {
                    r.SupplierCompanyId
                }).ToArray();
                if(processDetailsBySupplierCompany.Length > 0) {
                    var supplierCompanyIds = partsSalesOrderProcessDetails.Where(v => v.SupplierCompanyId.HasValue).Select(v => v.SupplierCompanyId).Distinct().ToArray();
                    var customerAccounts = ObjectContext.CustomerAccounts.Where(r => supplierCompanyIds.Contains(r.CustomerCompanyId) && r.AccountGroupId == salesUnit.AccountGroupId && r.Status == (int)DcsMasterDataStatus.有效).ToArray();
                    foreach(var partsSalesOrderProcessDetail in processDetailsBySupplierCompany) {
                        var supplierCompanyAccount = customerAccounts.FirstOrDefault(r => r.CustomerCompanyId == partsSalesOrderProcessDetail.Key.SupplierCompanyId);
                        if(supplierCompanyAccount == null)
                            throw new ValidationException(string.Format(ErrorStrings.PartsSalesOrderProcess_Validation12, partsSalesOrderProcessDetail.First().SupplierCompanyCode));
                        var overstockPartsAdjustBill = new OverstockPartsAdjustBill {
                            OriginalStorageCompanyId = partsSalesOrderProcessDetail.Key.SupplierCompanyId.Value,
                            OriginalStorageCompanyCode = partsSalesOrderProcessDetail.First().SupplierCompanyCode,
                            OriginalStorageCompanyName = partsSalesOrderProcessDetail.First().SupplierCompanyName,
                            OriginalStorageCoCustAccountId = supplierCompanyAccount.Id,
                            StorageCompanyType = partsSalesOrder.InvoiceReceiveCompanyType,
                            DestStorageCompanyId = partsSalesOrder.InvoiceReceiveCompanyId,
                            DestStorageCompanyCode = partsSalesOrder.InvoiceReceiveCompanyCode,
                            DestStorageCompanyName = partsSalesOrder.InvoiceReceiveCompanyName,
                            DestStorageCoCustomerAccountId = partsSalesOrder.CustomerAccountId,
                            BranchId = partsSalesOrder.BranchId,
                            BranchCode = partsSalesOrder.BranchCode,
                            BranchName = partsSalesOrder.BranchName,
                            SalesUnitId = partsSalesOrder.SalesUnitId,
                            SalesUnitName = partsSalesOrder.SalesUnitName,
                            SalesUnitOwnerCompanyId = partsSalesOrder.SalesUnitOwnerCompanyId,
                            SalesUnitOwnerCompanyCode = partsSalesOrder.SalesUnitOwnerCompanyCode,
                            SalesUnitOwnerCompanyName = partsSalesOrder.SalesUnitOwnerCompanyName,
                            TransferReason = string.Format("配件销售积压件调剂（{0}）", partsSalesOrder.Code),
                            SourceId = partsSalesOrder.Id,
                            SourceCode = partsSalesOrder.Code,
                            OriginalRequirementBillId = partsSalesOrder.Id,
                            OriginalRequirementBillCode = partsSalesOrder.Code,
                            OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.配件销售订单,
                            Status = (int)DcsOverstockPartsAdjustBillStatus.生效
                        };
                        foreach(var orderDetail in partsSalesOrderProcessDetail) {
                            var overstockPartsAdjustDetail = new OverstockPartsAdjustDetail {
                                SparePartId = orderDetail.SparePartId,
                                SparePartCode = orderDetail.SparePartCode,
                                SparePartName = orderDetail.SparePartName,
                                Quantity = orderDetail.CurrentFulfilledQuantity,
                                TransferOutPrice = orderDetail.TransferPrice,
                                TransferInPrice = orderDetail.TransferPrice
                            };
                            overstockPartsAdjustBill.OverstockPartsAdjustDetails.Add(overstockPartsAdjustDetail);
                        }
                        InsertToDatabase(overstockPartsAdjustBill);
                        new OverstockPartsAdjustBillAch(this.DomainService).InsertOverstockPartsAdjustBillValidate(overstockPartsAdjustBill);
                    }
                }
                #endregion

                #region 订单处理方式=供应商直发
                var supplierProcessDetails = partsSalesOrderProcessDetails.Where(r => r.OrderProcessMethod == (int)DcsPartsSalesOrderProcessDetailProcessMethod.供应商直发).ToArray();
                if(supplierProcessDetails.Length > 0) {
                    var partsIds = supplierProcessDetails.Select(r => r.SparePartId).ToArray();
                    var newpartsSalesOrder = new PartsSalesOrder {
                        BranchId = partsSalesOrder.BranchId,
                        BranchCode = partsSalesOrder.BranchCode,
                        BranchName = partsSalesOrder.BranchName,
                        SalesCategoryId = partsSalesOrder.SalesCategoryId,
                        SalesCategoryName = partsSalesOrder.SalesCategoryName,
                        SalesUnitId = partsSalesOrder.SalesUnitId,
                        SalesUnitCode = partsSalesOrder.SalesUnitCode,
                        SalesUnitName = partsSalesOrder.SalesUnitName,
                        SalesUnitOwnerCompanyId = partsSalesOrder.SalesUnitOwnerCompanyId,
                        SalesUnitOwnerCompanyCode = partsSalesOrder.SalesUnitOwnerCompanyCode,
                        SalesUnitOwnerCompanyName = partsSalesOrder.SalesUnitOwnerCompanyName,
                        SourceBillCode = partsSalesOrder.Code,
                        SourceBillId = partsSalesOrder.Id,
                        CustomerAccountId = partsSalesOrder.CustomerAccountId,
                        InvoiceReceiveCompanyId = partsSalesOrder.InvoiceReceiveCompanyId,
                        InvoiceReceiveCompanyCode = partsSalesOrder.InvoiceReceiveCompanyCode,
                        InvoiceReceiveCompanyName = partsSalesOrder.InvoiceReceiveCompanyName,
                        InvoiceReceiveCompanyType = partsSalesOrder.InvoiceReceiveCompanyType,
                        InvoiceReceiveSaleCateId = partsSalesOrder.InvoiceReceiveSaleCateId,
                        InvoiceReceiveSaleCateName = partsSalesOrder.InvoiceReceiveSaleCateName,
                        SubmitCompanyId = partsSalesOrder.SubmitCompanyId,
                        SubmitCompanyCode = partsSalesOrder.SubmitCompanyCode,
                        SubmitCompanyName = partsSalesOrder.SubmitCompanyName,
                        FirstClassStationId = partsSalesOrder.FirstClassStationId,
                        FirstClassStationCode = partsSalesOrder.FirstClassStationCode,
                        FirstClassStationName = partsSalesOrder.FirstClassStationName,
                        PartsSalesOrderTypeId = partsSalesOrder.PartsSalesOrderTypeId,
                        PartsSalesOrderTypeName = partsSalesOrder.PartsSalesOrderTypeName,
                        IfAgencyService = partsSalesOrder.IfAgencyService,
                        SalesActivityDiscountRate = partsSalesOrder.SalesActivityDiscountRate,
                        SalesActivityDiscountAmount = partsSalesOrder.SalesActivityDiscountAmount,
                        RequestedShippingTime = partsSalesOrder.RequestedShippingTime,
                        RequestedDeliveryTime = partsSalesOrder.RequestedDeliveryTime,
                        IfDirectProvision = true,
                        SecondLevelOrderType = partsSalesOrder.SecondLevelOrderType,
                        Remark = "订单转直供" + partsSalesOrder.Remark,
                        ApprovalComment = partsSalesOrder.ApprovalComment,
                        ContactPerson = partsSalesOrder.ContactPerson,
                        ContactPhone = partsSalesOrder.ContactPhone,
                        ReceivingCompanyId = partsSalesOrder.ReceivingCompanyId,
                        ReceivingCompanyCode = partsSalesOrder.ReceivingCompanyCode,
                        ReceivingCompanyName = partsSalesOrder.ReceivingCompanyName,
                        ShippingMethod = partsSalesOrder.ShippingMethod,
                        ReceivingWarehouseId = partsSalesOrder.ReceivingWarehouseId,
                        ReceivingWarehouseName = partsSalesOrder.ReceivingWarehouseName,
                        ReceivingAddress = partsSalesOrder.ReceivingAddress,
                        Status = (int)DcsPartsSalesOrderStatus.提交,
                        WarehouseId = partsSalesOrder.WarehouseId,
                        CustomerType = partsSalesOrder.CustomerType,
                        ProvinceID = partsSalesOrder.ProvinceID,
                        Province = partsSalesOrder.Province,
                        City = partsSalesOrder.City
                    };
                    //获取业务代码
                    var partsSalesOrderType = ObjectContext.PartsSalesOrderTypes.Where(v => v.Id == partsSalesOrder.PartsSalesOrderTypeId).FirstOrDefault();
                    if(partsSalesOrderType == null)
                        newpartsSalesOrder.Code = CodeGenerator.Generate("PartsSalesOrder", partsSalesOrder.SalesUnitOwnerCompanyCode);
                    else
                        newpartsSalesOrder.Code = partsSalesOrderType.BusinessCode + CodeGenerator.Generate("PartsSalesOrder", partsSalesOrder.SalesUnitOwnerCompanyCode);
                    partsSalesOrder.Remark = "订单转直供-" + newpartsSalesOrder.Code + partsSalesOrder.Remark;
                    //var virtualPartsSalesPrices = DomainService.根据销售类型查询配件销售价(partsSalesOrder.SalesCategoryId, partsSalesOrder.PartsSalesOrderTypeId, partsSalesOrder.SubmitCompanyId, partsSalesOrder.IfDirectProvision).ToArray();
                    foreach(var orderdetail in partsSalesOrderDetails.Where(r => partsIds.Contains(r.SparePartId)).ToArray()) {
                        //var virtualPartsSalesPrice = virtualPartsSalesPrices.FirstOrDefault(r => r.SparePartId == orderdetail.SparePartId);
                        var tempSupplierProcessDetails = supplierProcessDetails.SingleOrDefault(r => r.SparePartId == orderdetail.SparePartId);
                        Debug.Assert(tempSupplierProcessDetails != null, "tempSupplierProcessDetails != null");
                        var newPartsSalesOrderDetail = new PartsSalesOrderDetail {
                            PartsSalesOrderId = newpartsSalesOrder.Id,
                            SparePartId = orderdetail.SparePartId,
                            SparePartCode = orderdetail.SparePartCode,
                            SparePartName = orderdetail.SparePartName,
                            MeasureUnit = orderdetail.MeasureUnit,
                            OrderedQuantity = tempSupplierProcessDetails.CurrentFulfilledQuantity,
                            ApproveQuantity = 0,
                            DiscountedPrice = orderdetail.DiscountedPrice,
                            OrderSum = tempSupplierProcessDetails.CurrentFulfilledQuantity * orderdetail.OrderPrice,
                            OriginalPrice = orderdetail.OriginalPrice,
                            CustOrderPriceGradeCoefficient = orderdetail.CustOrderPriceGradeCoefficient,
                            OrderPrice = orderdetail.OrderPrice,
                            EstimatedFulfillTime = orderdetail.EstimatedFulfillTime,
                            Remark = orderdetail.Remark,
                            PriceTypeName = orderdetail.PriceTypeName,
                            ABCStrategy = orderdetail.ABCStrategy,
                            SalesPrice = orderdetail.SalesPrice
                        };
                        InsertToDatabase(newPartsSalesOrderDetail);
                        newpartsSalesOrder.PartsSalesOrderDetails.Add(newPartsSalesOrderDetail);
                        orderdetail.OrderedQuantity = orderdetail.OrderedQuantity - newPartsSalesOrderDetail.OrderedQuantity;
                        orderdetail.OrderSum = orderdetail.OrderedQuantity * orderdetail.OrderPrice;
                        orderdetail.Remark = orderdetail.Remark + "转直供数量:" + tempSupplierProcessDetails.CurrentFulfilledQuantity;
                        UpdateToDatabase(orderdetail);
                    }
                    //主单重新计算下总金额
                    if(partsSalesCategory.Name == "随车行") {
                        newpartsSalesOrder.ERPSourceOrderCode = partsSalesOrder.ERPSourceOrderCode;
                        newpartsSalesOrder.InvoiceCustomerName = partsSalesOrder.InvoiceCustomerName;
                        newpartsSalesOrder.BankAccount = partsSalesOrder.BankAccount;
                        newpartsSalesOrder.Taxpayeridentification = partsSalesOrder.Taxpayeridentification;
                        newpartsSalesOrder.InvoiceAdress = partsSalesOrder.InvoiceAdress;
                        newpartsSalesOrder.InvoicePhone = partsSalesOrder.InvoicePhone;
                        newpartsSalesOrder.InvoiceType = partsSalesOrder.InvoiceType;
                    }
                    newpartsSalesOrder.TotalAmount = newpartsSalesOrder.PartsSalesOrderDetails.Sum(r => r.OrderSum);
                    InsertToDatabase(newpartsSalesOrder);
                    new PartsSalesOrderAch(this.DomainService).InsertPartsSalesOrderValidate(newpartsSalesOrder);
                    partsSalesOrder.TotalAmount = partsSalesOrder.PartsSalesOrderDetails.Sum(r => r.OrderSum);
                    if(partsSalesOrder.TotalAmount == 0) {
                        partsSalesOrder.Status = (int)DcsPartsSalesOrderStatus.终止;
                    }
                    UpdateToDatabase(partsSalesOrder);
                }
                #endregion

                #region 订单处理方式=转中心库

                PartsInboundPlan curPartsInboundPlan = null;
                PartsSalesReturnBill curPartsSalesReturnBill = null;
                var insideMarketingProcessDetails = partsSalesOrderProcessDetails.Where(r => r.OrderProcessMethod == (int)DcsPartsSalesOrderProcessDetailProcessMethod.转中心库).ToArray();
                if(insideMarketingProcessDetails.Length > 0) {
                    new PartsSalesOrderProcessForBranchAch(this.DomainService).审批销售订单转中心库(partsSalesOrderProcess, partsSalesOrderProcessDetails.ToList(), partsSalesOrderDetails.ToList(), partsSalesOrder, partsSalesCategory, out curPartsInboundPlan, out curPartsSalesReturnBill);
                }
                #endregion
                //更新销售订单状态
                partsSalesOrder.Status = partsSalesOrderDetails.Any(v => v.ApproveQuantity != v.OrderedQuantity && v.OrderedQuantity > 0) ? partsSalesOrder.Status = (int)DcsPartsSalesOrderStatus.部分审批 : (int)DcsPartsSalesOrderStatus.审批完成;

                #region 处理方式为：本部处理
                var processDetailsByWarehouses = partsSalesOrderProcessDetails.Where(r => r.OrderProcessMethod == (int)DcsPartsSalesOrderProcessDetailProcessMethod.本部处理 || r.OrderProcessMethod == (int)DcsPartsSalesOrderProcessDetailProcessMethod.统购分销).GroupBy(r => new {
                    r.WarehouseId
                });
                var sparePartIds = partsSalesOrderProcessDetails.Where(r => r.OrderProcessMethod == (int)DcsPartsSalesOrderProcessDetailProcessMethod.本部处理 || r.OrderProcessMethod == (int)DcsPartsSalesOrderProcessDetailProcessMethod.统购分销).Select(o => o.SparePartId);

                var bb= partsSalesOrderProcessDetails.Where(r => r.OrderProcessMethod == (int)DcsPartsSalesOrderProcessDetailProcessMethod.本部处理 || r.OrderProcessMethod == (int)DcsPartsSalesOrderProcessDetailProcessMethod.统购分销).FirstOrDefault();
                if(bb!=null ){
                 var   processWarehouseId = bb.WarehouseId;
                 var noReplace = partsSalesOrderProcessDetails.Where(r => (r.OrderProcessMethod == (int)DcsPartsSalesOrderProcessDetailProcessMethod.本部处理 || r.OrderProcessMethod == (int)DcsPartsSalesOrderProcessDetailProcessMethod.统购分销) && r.NewPartId == null).Select(o => o.SparePartId).ToList();
                 var hasReplace = partsSalesOrderProcessDetails.Where(r => (r.OrderProcessMethod == (int)DcsPartsSalesOrderProcessDetailProcessMethod.本部处理 || r.OrderProcessMethod == (int)DcsPartsSalesOrderProcessDetailProcessMethod.统购分销) && r.NewPartId != null).Select(o => o.NewPartId.Value).ToList();
                 var tockSid = noReplace.Concat(hasReplace);
                 //判断没有替换件库存是否足够
                 if(tockSid.ToArray().Count() > 0) {
                     var warehousePartsStocks = DomainService.查询仓库库存销售审核(partsSalesOrder.SalesUnitOwnerCompanyId, processWarehouseId, tockSid.ToArray()).ToArray();
                     foreach(var spid in tockSid) {
                         var appqty = partsSalesOrderProcessDetails.Where(t => (t.SparePartId == spid && t.NewPartId == null) || (t.NewPartId == spid)).Sum(y => y.CurrentFulfilledQuantity);
                         var stock = warehousePartsStocks.Where(t => t.SparePartId == spid).FirstOrDefault();
                         if(stock == null || stock.UsableQuantity < appqty) {
                             throw new ValidationException(stock.SparePartCode + "可用库存不足");
                         }
                     }
                  }         
                }                     
                var spareParts = ObjectContext.SpareParts.Where(o => sparePartIds.Contains(o.Id) && o.Status == (int)DcsMasterDataStatus.有效);
                //获取配件主包装数量
                var partsBranchPackingProps = ObjectContext.PartsBranchPackingProps.Where(o => sparePartIds.Contains(o.SparePartId.Value) && ObjectContext.PartsBranches.Any(r => r.Id == o.PartsBranchId && r.BranchId == partsSalesOrder.BranchId
                                    && r.PartsSalesCategoryId == partsSalesOrder.SalesCategoryId && r.MainPackingType == o.PackingType && r.PartId == o.SparePartId && r.Status == (int)DcsMasterDataStatus.有效));

                var warehouseIds = partsSalesOrderProcessDetails.Select(r => r.WarehouseId ?? 0).Distinct().ToArray();
                if(partsSalesOrderProcessDetails.Any(r => r.OrderProcessMethod == (int)DcsPartsSalesOrderProcessDetailProcessMethod.本部处理) && partsSalesOrder.SalesCategoryName == "随车行" && ObjectContext.Companies.Any(r => r.Id == partsSalesOrder.SalesUnitOwnerCompanyId && r.Type == (int)DcsCompanyType.分公司 && r.Status == (int)DcsBaseDataStatus.有效)) {
                    //销售组织中有当前所属品牌
                    var salesUnitys = this.ObjectContext.SalesUnits.Where(v => partsSalesOrder.SalesCategoryId == v.PartsSalesCategoryId);
                    var warehouseIdsInternal = this.ObjectContext.SalesUnitAffiWarehouses.Where(v => salesUnitys.Any(e => e.Id == v.SalesUnitId)).Select(e => e.WarehouseId).ToArray();
                    var warehouseIdsForHere = partsSalesOrderProcessDetails.Where(r => r.OrderProcessMethod == (int)DcsPartsSalesOrderProcessDetailProcessMethod.本部处理).Select(r => r.WarehouseId ?? 0).Distinct().ToArray();
                    var except = warehouseIdsForHere.Except(warehouseIdsInternal).ToList();
                    if(except.Any())
                        throw new ValidationException(ErrorStrings.PartsSalesOrderProcess_Validation24);
                }

                var warehouses = ObjectContext.Warehouses.Where(r => warehouseIds.Contains(r.Id) && r.Status != (int)DcsBaseDataStatus.作废).ToArray();
                var dbSalesUnit = ObjectContext.SalesUnits.FirstOrDefault(v => v.Id == partsSalesOrder.SalesUnitId);
                if(dbSalesUnit == null)
                    throw new ValidationException(ErrorStrings.PartsSalesOrderProcess_Validation25);
                var userInfoCompany = ObjectContext.Companies.SingleOrDefault(r => r.Id == dbSalesUnit.OwnerCompanyId);
                if(userInfoCompany == null)
                    throw new ValidationException(ErrorStrings.PartsSalesOrderProcess_Validation26);
                var agencySaleUnits = ObjectContext.SalesUnits.Where(agencySaleUnit => agencySaleUnit.OwnerCompanyId == invoiceReceiveCompany.Id && agencySaleUnit.PartsSalesCategoryId == salesUnit.PartsSalesCategoryId && agencySaleUnit.Status == (int)DcsMasterDataStatus.有效);
                var agencyWarehouse = ObjectContext.Warehouses.FirstOrDefault(v => ObjectContext.SalesUnitAffiWarehouses.Any(affi => affi.WarehouseId == v.Id && agencySaleUnits.Contains(affi.SalesUnit)));
                foreach(var partsSalesOrderProcessDetail in processDetailsByWarehouses) {
                    var sparePartId = partsSalesOrderProcessDetail.Select(r => r.SparePartId).ToArray();
                    if(company.Type == (int)DcsCompanyType.分公司) {
                        if(partsSalesOrder.PartsSalesOrderTypeName == "正常订单") {
                            foreach(var partid in sparePartId) {
                                var sparePart = spareParts.FirstOrDefault(o => o.Id == partid);
                                if(sparePart != null && partsSalesOrderProcessDetail.First(r => r.SparePartId == partid).CurrentFulfilledQuantity % sparePart.MInPackingAmount > 0)
                                    throw new ValidationException(string.Format(ErrorStrings.PartsSalesOrderProcess_Validation27, partsSalesOrderProcessDetail.First(r => r.SparePartId == partid).SparePartCode, sparePart.MInPackingAmount));

                            }
                        }
                    }
                    var warehouse = warehouses.FirstOrDefault(r => r.Id == partsSalesOrderProcessDetail.Key.WarehouseId);
                    if(warehouse == null)
                        throw new ValidationException(string.Format(ErrorStrings.PartsSalesOrderProcess_Validation13, partsSalesOrderProcessDetail.First().WarehouseCode));
                    var partsOutboundPlan = new PartsOutboundPlan {
                        WarehouseId = (int)partsSalesOrderProcessDetail.Key.WarehouseId,
                        WarehouseName = partsSalesOrderProcessDetail.First().WarehouseName,
                        WarehouseCode = partsSalesOrderProcessDetail.First().WarehouseCode,
                        StorageCompanyId = userInfoCompany.Id,
                        CompanyAddressId = partsSalesOrder.CompanyAddressId,
                        StorageCompanyCode = userInfoCompany.Code,
                        StorageCompanyName = userInfoCompany.Name,
                        StorageCompanyType = userInfoCompany.Type,
                        BranchId = partsSalesOrder.BranchId,
                        BranchCode = partsSalesOrder.BranchCode,
                        BranchName = partsSalesOrder.BranchName,
                        CounterpartCompanyId = partsSalesOrder.InvoiceReceiveCompanyId,
                        CounterpartCompanyCode = partsSalesOrder.InvoiceReceiveCompanyCode,
                        CounterpartCompanyName = partsSalesOrder.InvoiceReceiveCompanyName,
                        SourceId = partsSalesOrder.Id,
                        SourceCode = partsSalesOrder.Code,
                        OutboundType = (int)DcsPartsOutboundType.配件销售,
                        CustomerAccountId = partsSalesOrder.CustomerAccountId,
                        OriginalRequirementBillId = partsSalesOrder.Id,
                        OriginalRequirementBillCode = partsSalesOrder.Code,
                        OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.配件销售订单,
                        ReceivingCompanyId = partsSalesOrder.ReceivingCompanyId,
                        ReceivingCompanyCode = partsSalesOrder.ReceivingCompanyCode,
                        ReceivingCompanyName = partsSalesOrder.ReceivingCompanyName,
                        Status = (int)DcsPartsOutboundPlanStatus.新建,
                        ShippingMethod = partsSalesOrderProcess.ShippingMethod,
                        IfWmsInterface = warehouse.WmsInterface,
                        PartsSalesCategoryId = salesUnit.PartsSalesCategoryId,
                        PartsSalesOrderTypeId = partsSalesOrder.PartsSalesOrderTypeId,
                        PartsSalesOrderTypeName = partsSalesOrder.PartsSalesOrderTypeName,
                        OrderApproveComment = partsSalesOrderProcess.ApprovalComment,
                        SAPPurchasePlanCode = partsSalesOrder.OverseasDemandSheetNo,
                        Remark = partsSalesOrder.Remark,
                        ERPSourceOrderCode = partsSalesOrder.ERPSourceOrderCode,
                        ReceivingAddress = partsSalesOrder.ReceivingAddress
                    };
                    if(invoiceReceiveCompany.Type == (int)DcsCompanyType.代理库 || invoiceReceiveCompany.Type == (int)DcsCompanyType.服务站兼代理库) {
                        if(agencyWarehouse == null)
                            throw new ValidationException(ErrorStrings.PartsSalesOrderProcess_Validation11);
                        string receivingWarehouseCode = null;
                        if(partsSalesOrder.ReceivingWarehouseId.HasValue) {
                            var receivingWarehouse = ObjectContext.Warehouses.SingleOrDefault(r => r.Id == partsSalesOrder.ReceivingWarehouseId.Value && r.Status == (int)DcsBaseDataStatus.有效);
                            if(receivingWarehouse != null) {
                                receivingWarehouseCode = receivingWarehouse.Code;
                            }
                        }
                        partsOutboundPlan.ReceivingWarehouseId = partsSalesOrder.ReceivingWarehouseId;
                        partsOutboundPlan.ReceivingWarehouseCode = receivingWarehouseCode;
                        partsOutboundPlan.ReceivingWarehouseName = partsSalesOrder.ReceivingWarehouseName;
                    }
                    var orderDetails = partsSalesOrderProcessDetail.Select(v => new {
                        SparePartId = v.NewPartId ?? v.SparePartId,
                        SparePartCode = v.NewPartId.HasValue ? v.NewPartCode : v.SparePartCode,
                        SparePartName = v.NewPartId.HasValue ? v.NewPartName : v.SparePartName,
                        PlannedAmount = v.CurrentFulfilledQuantity,
                        Price = v.OrderPrice
                    }).GroupBy(v => v.SparePartId).Select(v => new PartsOutboundPlanDetail {
                        SparePartId = v.Key,
                        SparePartCode = v.First().SparePartCode,
                        SparePartName = v.First().SparePartName,
                        PlannedAmount = v.Sum(v1 => v1.PlannedAmount),
                        Price = v.First().Price
                    });

                    foreach(var orderDetail in orderDetails) {
                        var partsSalesOrderDetail = partsSalesOrderDetails.FirstOrDefault(r => r.SparePartId == orderDetail.SparePartId);
                        if(partsSalesOrderDetail != null)
                            orderDetail.OriginalPrice = partsSalesOrderDetail.OriginalPrice;
                        partsOutboundPlan.PartsOutboundPlanDetails.Add(orderDetail);
                    }
                    //生成配件出库计划(partsOutboundPlan);
                    var partsOutboundPlanDetails = partsOutboundPlan.PartsOutboundPlanDetails.ToArray();
                    var partsOutboundPlanDetailPartIds = partsOutboundPlanDetails.Select(r => r.SparePartId);
                    var partsLockedStocks = new PartsLockedStockAch(this.DomainService).GetPartsLockedStocksWithLock(ObjectContext.PartsLockedStocks.Where(r => r.WarehouseId == partsOutboundPlan.WarehouseId && partsOutboundPlanDetailPartIds.Contains(r.PartId))).ToArray();
                    var partsStocks1 = ObjectContext.PartsStocks.Where(r => r.WarehouseId == partsOutboundPlan.WarehouseId && partsOutboundPlanDetailPartIds.Contains(r.PartId)).ToList();
                    var partsStocks = partsStocks1.Union(partsStockLists);

                    int serialNum = 0;
                    //获取保底库存
                    var bottomStocks = new BottomStockAch(this.DomainService).查询保底库存表信息(partsSalesOrder.SalesUnitOwnerCompanyId, partsSalesOrder.WarehouseId, partsOutboundPlanDetailPartIds.ToArray());
                    var assemblyKeyCodes = spareParts.Where(o => o.PartType == (int)DcsSparePartPartType.总成件_领用).Select(o => o.AssemblyKeyCode).ToList();
                    var assemblyPartRequisitionLinks = ObjectContext.AssemblyPartRequisitionLinks.Where(o => assemblyKeyCodes.Contains(o.KeyCode) && o.Status == 1).ToList();
                    foreach(var partsOutboundPlanDetail in partsOutboundPlanDetails) {
                        var bottomStock = bottomStocks.FirstOrDefault(r => r.SparePartId == partsOutboundPlanDetail.SparePartId);
                        if(bottomStock == null) {
                            bottomStock = new BottomStock() {
                                StockQty = 0
                            };
                        }
                        var partsLockedStock = partsLockedStocks.FirstOrDefault(r => r.PartId == partsOutboundPlanDetail.SparePartId);
                        var partsStocksAmount = partsStocks.Where(r => r.PartId == partsOutboundPlanDetail.SparePartId).Sum(v => v.Quantity);
                        if(partsLockedStock == null) {
                            if((partsSalesOrderProcessDetails.Any(r => (r.NewPartId ?? r.SparePartId) == partsOutboundPlanDetail.SparePartId && r.OrderProcessMethod == (int)DcsPartsSalesOrderProcessDetailProcessMethod.本部处理)
                                || partsSalesOrderProcessDetails.Any(r => (r.NewPartId ?? r.SparePartId) == partsOutboundPlanDetail.SparePartId && r.OrderProcessMethod == (int)DcsPartsSalesOrderProcessDetailProcessMethod.统购分销))
                                && (partsStocksAmount - partsOutboundPlanDetail.PlannedAmount - (isCheckMinSaleQuantity ? bottomStock.StockQty : 0) < 0))
                                throw new ValidationException(string.Format(ErrorStrings.PartsOutboundPlan_Validation3, partsOutboundPlanDetail.SparePartCode));
                            var newpartsLockedStock = new PartsLockedStock {
                                WarehouseId = partsOutboundPlan.WarehouseId,
                                PartId = partsOutboundPlanDetail.SparePartId,
                                LockedQuantity = partsOutboundPlanDetail.PlannedAmount,
                                StorageCompanyId = partsOutboundPlan.StorageCompanyId,
                                BranchId = partsOutboundPlan.BranchId,
                                CreatorId = currentUser.Id,
                                CreatorName = currentUser.Name,
                                CreateTime = DateTime.Now
                            };
                            InsertToDatabase(newpartsLockedStock);
                            //new PartsLockedStockAch(this.DomainService).InsertPartsLockedStockValidate(newpartsLockedStock);
                        } else {
                            if((partsSalesOrderProcessDetails.Any(r => (r.NewPartId ?? r.SparePartId) == partsOutboundPlanDetail.SparePartId && r.OrderProcessMethod == (int)DcsPartsSalesOrderProcessDetailProcessMethod.本部处理)
                                || partsSalesOrderProcessDetails.Any(r => (r.NewPartId ?? r.SparePartId) == partsOutboundPlanDetail.SparePartId && r.OrderProcessMethod == (int)DcsPartsSalesOrderProcessDetailProcessMethod.统购分销))
                                && (partsStocksAmount - partsLockedStock.LockedQuantity - partsOutboundPlanDetail.PlannedAmount - (isCheckMinSaleQuantity ? bottomStock.StockQty : 0) < 0))
                                throw new ValidationException(string.Format(ErrorStrings.PartsOutboundPlan_Validation3, partsOutboundPlanDetail.SparePartCode));
                            partsLockedStock.LockedQuantity += partsOutboundPlanDetail.PlannedAmount;
                            partsLockedStock.ModifierId = currentUser.Id;
                            partsLockedStock.ModifierName = currentUser.Name;
                            partsLockedStock.ModifyTime = DateTime.Now;
                            //new PartsLockedStockAch(this.DomainService).UpdatePartsLockedStockValidate(partsLockedStock);
                            UpdateToDatabase(partsLockedStock);
                        }
                        if(company.Type == (int)DcsCompanyType.分公司) {
                            var sparePart = spareParts.FirstOrDefault(o => o.Id == partsOutboundPlanDetail.SparePartId);
                            if(sparePart!=null && sparePart.PartType == (int)DcsSparePartPartType.总成件_领用) {
                                //获取当前总成件对应的配件关系
                                var assemblyPartRequisitionLinkNew = assemblyPartRequisitionLinks.Where(o => (o.KeyCode ?? 0) == (sparePart.AssemblyKeyCode ?? 0));
                                if(assemblyPartRequisitionLinkNew.Count() == 0)
                                    throw new ValidationException(string.Format(ErrorStrings.AssemblyPartRequisitionLink_Validation3, partsOutboundPlanDetail.SparePartCode));

                                var assemblySparePartId = assemblyPartRequisitionLinkNew.Select(o => o.PartId);
                                //查询可用库存和企业库存，进行校验
                                var assemblyPartsStocks = ObjectContext.PartsStocks.Where(r => r.WarehouseId == partsOutboundPlan.WarehouseId && assemblySparePartId.Contains(r.PartId)).ToArray();
                                var enterprisePartsCosts = ObjectContext.EnterprisePartsCosts.Where(o => o.OwnerCompanyId == partsSalesOrder.BranchId && assemblySparePartId.Contains(o.SparePartId));
                                //生成内部领出单
                                InternalAllocationBill internalAllocationBill = null;
                                foreach(var assemblyPartRequisitionLink in assemblyPartRequisitionLinkNew) {
                                    var assemblyPartsStock = assemblyPartsStocks.Where(o => o.PartId == assemblyPartRequisitionLink.PartId).Sum(o => o.Quantity - (o.LockedQty ?? 0));
                                    var enterprisePartsCost = enterprisePartsCosts.FirstOrDefault(o => o.SparePartId == assemblyPartRequisitionLink.PartId);
                                    if(assemblyPartRequisitionLink.Qty > assemblyPartsStock) {
                                        throw new ValidationException(string.Format(ErrorStrings.AssemblyPartRequisitionLink_Validation6, partsOutboundPlanDetail.SparePartCode, assemblyPartRequisitionLink.PartCode));
                                    } else {
                                        var departmentInformation = ObjectContext.DepartmentInformations.FirstOrDefault(o => o.Name.Equals("销售服务组") && o.Status == 1);
                                        if(internalAllocationBill == null) {
                                            internalAllocationBill = new InternalAllocationBill();
                                            internalAllocationBill.BranchId = partsSalesOrder.BranchId;
                                            internalAllocationBill.BranchName = partsSalesOrder.BranchName;
                                            internalAllocationBill.WarehouseId = partsOutboundPlan.WarehouseId;
                                            internalAllocationBill.WarehouseName = partsOutboundPlan.WarehouseName;
                                            internalAllocationBill.DepartmentId = departmentInformation.Id;
                                            internalAllocationBill.DepartmentName = departmentInformation.Name;
                                            internalAllocationBill.TotalAmount = 0;
                                            internalAllocationBill.Status = (int)DcsInternalAllocationBillStatus.已审核;
                                            internalAllocationBill.ApproverId = currentUser.Id;
                                            internalAllocationBill.ApproverName = currentUser.Name;
                                            internalAllocationBill.ApproveTime = DateTime.Now;
                                            internalAllocationBill.ModifierId = currentUser.Id;
                                            internalAllocationBill.ModifierName = currentUser.Name;
                                            internalAllocationBill.ModifyTime = DateTime.Now;
                                            internalAllocationBill.Objid = Guid.NewGuid().ToString();
                                            internalAllocationBill.SourceCode = partsSalesOrder.Code;
                                            internalAllocationBill.Type = (int)DcsInternalAllocationBill_Type.包材;
                                            InsertToDatabase(internalAllocationBill);
                                            new InternalAllocationBillAch(this.DomainService).InsertInternalAllocationBillValidate(internalAllocationBill);
                                        }
                                        var internalAllocationDetail = new InternalAllocationDetail();
                                        internalAllocationDetail.InternalAllocationBill = internalAllocationBill;
                                        internalAllocationDetail.SerialNumber = serialNum++;
                                        internalAllocationDetail.SparePartId = assemblyPartRequisitionLink.PartId;
                                        internalAllocationDetail.SparePartCode = assemblyPartRequisitionLink.PartCode;
                                        internalAllocationDetail.SparePartName = assemblyPartRequisitionLink.PartName;
                                        internalAllocationDetail.MeasureUnit = sparePart.MeasureUnit;
                                        internalAllocationDetail.UnitPrice = enterprisePartsCost.CostPrice;
                                        internalAllocationDetail.Quantity = (assemblyPartRequisitionLink.Qty ?? 0) * partsOutboundPlanDetail.PlannedAmount;
                                        internalAllocationBill.InternalAllocationDetails.Add(internalAllocationDetail);
                                        internalAllocationBill.TotalAmount += internalAllocationDetail.Quantity * internalAllocationDetail.UnitPrice;
                                    }
                                }
                            }
                        }
                    }
                    InsertToDatabase(partsOutboundPlan);
                    new PartsOutboundPlanAch(this.DomainService).InsertPartsOutboundPlanValidate(partsOutboundPlan);
                }
                #endregion

                #region 注释 Ly
                /*
                //如果 销售订单.销售组织隶属企业为代理库，代理库.是否自动转化订单=是 且存在配件销售订单清单.审批数量< 配件销售订单清单.订货数量 调用 【配件销售订单.转分公司配件销售订单】
                var company = this.ObjectContext.Companies.FirstOrDefault(r => r.Id == partsSalesOrder.SalesUnitOwnerCompanyId && r.Status == (int)DcsMasterDataStatus.有效);
                if(company == null)
                    throw new ValidationException(string.Format("销售组织隶属企业编号为“{0}”的企业不存在！", partsSalesOrder.SalesUnitOwnerCompanyCode));
                if(company.Type == (int)DcsCompanyType.代理库) {
                    var agency = this.ObjectContext.Agencies.FirstOrDefault(r => r.Id == company.Id);
                    if(agency != null && agency.IsAutoTranslate == true && partsSalesOrder.PartsSalesOrderDetails.Any(r => r.ApproveQuantity < r.OrderedQuantity)) {
                        DomainService.转分公司配件销售订单(partsSalesOrder);
                    }
                }*/
                #endregion

                partsSalesOrder.TotalAmount = partsSalesOrderDetails.Sum(r => r.OrderSum);
                //更新处理单处理后状态
                partsSalesOrderProcess.BillStatusAfterProcess = partsSalesOrder.Status;
                partsSalesOrderProcess.Time = partsSalesOrder.Time;
                this.InsertPartsSalesOrderProcessValidate(partsSalesOrderProcess);
                InsertToDatabase(partsSalesOrderProcess);

                #region 订单处理方式=转中心库采购

                var centerPurchases = partsSalesOrderProcessDetails.Where(r => r.OrderProcessMethod == (int)DcsPartsSalesOrderProcessDetailProcessMethod.转中心库采购).ToArray();
                if(centerPurchases.Length > 0) {
                    审批销售订单转中心库采购(partsSalesOrderProcessDetails.ToList(), partsSalesOrderDetails.ToList(), partsSalesOrder, partsSalesCategory);
                }
                #endregion

                if(passAccountAmount > 0)
                    new CustomerAccountAch(this.DomainService).销售订单审批过账记录流水(partsSalesOrder, passAccountAmount);
                if(passAccountAmountForCenter > 0)
                    new CustomerAccountAch(this.DomainService).销售订单审批过账记录流水2(partsSalesOrder, passAccountAmountForCenter);
                if(curPartsInboundPlan != null && curPartsSalesReturnBill != null && curPartsInboundPlan.Id != 0 && curPartsSalesReturnBill.Id != 0)
                    this.ObjectContext.ExecuteStoreCommand(String.Format("Update PartsInboundPlan Set Sourceid ={0} where Id ={1}", curPartsSalesReturnBill.Id, curPartsInboundPlan.Id));
                var eventInfo = new LogEventInfo(LogLevel.Info, logger.Name, "");
                eventInfo.Properties["id"] = partsSalesOrder.Id;
                eventInfo.Properties["code"] = partsSalesOrder.Code;
                eventInfo.Properties["systemCode"] = "FT";
                eventInfo.Properties["operatorID"] = currentUser.Id;
                eventInfo.Properties["operatorName"] = currentUser.Name;
                eventInfo.Properties["enterpriseCode"] = currentUser.EnterpriseCode;
                eventInfo.Properties["enterpriseName"] = currentUser.EnterpriseName;
                eventInfo.Properties["startTime"] = DateTime.Now;
                eventInfo.Properties["endTime"] = DateTime.Now;
                eventInfo.Properties["type"] = 1;

                logger.Log(eventInfo);

                try {
                    var entities = ObjectContext.ObjectStateManager.GetObjectStateEntries(EntityState.Added | EntityState.Deleted | EntityState.Modified);
                    foreach(var entity in entities)
                        DomainService.logInfo.AddDetailLog(Sunlight.Silverlight.Log.Web.LogDetailType.Param, entity.State.ToString(), entity.Entity, false);
                } catch(Exception ex) {
                    try {
                        var errors = DomainService.GetFullExceptionMessage(ex);
                        DomainService.logInfo.AddDetailLog(Sunlight.Silverlight.Log.Web.LogDetailType.Error, "格式化对象报错:", errors, false);
                    } catch {
                    }
                }
                new PartsSalesOrderAch(this.DomainService).PMS_SYC_150_SendPMSUpdateOrderStatus_PS(partsSalesOrder);
            } catch(OptimisticConcurrencyException) {
                throw new ValidationException(ErrorStrings.PartsSalesOrderProcess_Validation21);
            }
        }

        private void 审批销售订单转中心库采购(List<PartsSalesOrderProcessDetail> marketingProcessDetailProcess, List<PartsSalesOrderDetail> partsSalesOrderDetails, PartsSalesOrder partsSalesOrder, PartsSalesCategory orderPartsSalesCategory) {
            var processDetails = marketingProcessDetailProcess.Where(r => r.OrderProcessMethod == (int)DcsPartsSalesOrderProcessDetailProcessMethod.转中心库采购).ToArray();
            var virtualBranchWarehouse = ObjectContext.Warehouses.FirstOrDefault(r => r.Type == (int)DcsWarehouseType.虚拟库 && r.StorageCompanyType == (int)DcsCompanyType.分公司);

            if(virtualBranchWarehouse == null) {
                throw new ValidationException(ErrorStrings.PartsSalesOrderProcess_Validation28);
            }
            var centerSupplierIdCount = processDetails.Select(r => r.SupplierCompanyId).Distinct().Count();
            if(centerSupplierIdCount > 1) {
                throw new ValidationException(ErrorStrings.PartsSalesOrderProcess_Validation29);
            }
            var purchaseType = ObjectContext.PartsPurchaseOrderTypes.FirstOrDefault(r => r.Name == "中心库采购");
            if(purchaseType == null) {
                throw new ValidationException(ErrorStrings.PartsSalesOrderProcess_Validation30);
            }
            var partIds = processDetails.Select(r => r.SparePartId).Distinct().ToArray();
            var branches = ObjectContext.PartsBranches.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && partIds.Contains(r.PartId));
            var user = Utils.GetCurrentUserInfo();
            var supplierId = processDetails.Select(r => r.SupplierCompanyId).First();
            var supplierCode = processDetails.Select(r => r.SupplierCompanyCode).First();
            var supplierName = processDetails.Select(r => r.SupplierCompanyName).First();
            //销售订单提报单位 收货仓库
            var receivingWarehouse = ObjectContext.Warehouses.FirstOrDefault(r => r.Id == partsSalesOrder.ReceivingWarehouseId);

            #region 1、发运完成的供应商采购订单和采购订单清单
            var newPartsPurchaseOrder = new PartsPurchaseOrder();
            newPartsPurchaseOrder.Code = CodeGenerator.Generate("PartsPurchaseOrder", partsSalesOrder.BranchCode);
            newPartsPurchaseOrder.BranchId = partsSalesOrder.BranchId;
            newPartsPurchaseOrder.BranchCode = partsSalesOrder.BranchCode;
            newPartsPurchaseOrder.BranchName = partsSalesOrder.BranchName;
            newPartsPurchaseOrder.WarehouseId = virtualBranchWarehouse.Id;
            newPartsPurchaseOrder.WarehouseName = virtualBranchWarehouse.Name;
            newPartsPurchaseOrder.PartsSalesCategoryId = orderPartsSalesCategory.Id;
            newPartsPurchaseOrder.PartsSalesCategoryName = orderPartsSalesCategory.Name;
            newPartsPurchaseOrder.ReceivingCompanyId = partsSalesOrder.BranchId;
            newPartsPurchaseOrder.ReceivingCompanyName = partsSalesOrder.BranchName;
            newPartsPurchaseOrder.ReceivingAddress = partsSalesOrder.BranchName;
            newPartsPurchaseOrder.PartsSupplierId = (int)supplierId;
            newPartsPurchaseOrder.PartsSupplierCode = supplierCode;
            newPartsPurchaseOrder.PartsSupplierName = supplierName;
            newPartsPurchaseOrder.IfDirectProvision = false;
            newPartsPurchaseOrder.OriginalRequirementBillId = partsSalesOrder.Id;
            newPartsPurchaseOrder.OriginalRequirementBillCode = partsSalesOrder.Code;
            newPartsPurchaseOrder.OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.配件销售订单;
            newPartsPurchaseOrder.PartsPurchaseOrderTypeId = purchaseType.Id;
            newPartsPurchaseOrder.RequestedDeliveryTime = DateTime.Now.AddDays(5);
            newPartsPurchaseOrder.ShippingMethod = partsSalesOrder.ShippingMethod;
            newPartsPurchaseOrder.Status = (int)DcsPartsPurchaseOrderStatus.发运完毕;
            newPartsPurchaseOrder.Remark = partsSalesOrder.Remark;
            newPartsPurchaseOrder.InStatus = (int)DcsPurchaseInStatus.全部入库完成;
            newPartsPurchaseOrder.CreatorId = partsSalesOrder.ApproverId;
            newPartsPurchaseOrder.CreatorName = partsSalesOrder.ApproverName;
            newPartsPurchaseOrder.CreateTime = partsSalesOrder.ApproveTime;
            newPartsPurchaseOrder.SubmitterId = partsSalesOrder.ApproverId;
            newPartsPurchaseOrder.SubmitterName = partsSalesOrder.ApproverName;
            newPartsPurchaseOrder.SubmitTime = partsSalesOrder.ApproveTime;
            newPartsPurchaseOrder.ApproverId = partsSalesOrder.ApproverId;
            newPartsPurchaseOrder.ApproverName = partsSalesOrder.ApproverName;
            newPartsPurchaseOrder.ApproveTime = partsSalesOrder.ApproveTime;
            newPartsPurchaseOrder.IsTransSap = false;
            //清单
            int i = 1;
            foreach(var item in processDetails) {
                var partssalsedetail = partsSalesOrderDetails.SingleOrDefault(r => r.SparePartId == item.SparePartId);
                var partsBranch = branches.FirstOrDefault(r => r.PartId == item.SparePartId);
                if(partsBranch == null) {
                    throw new ValidationException(string.Format(ErrorStrings.PartsSalesOrderProcess_Validation31, item.SparePartCode));
                }
                var newPartsPurchaseOrderDetail = new PartsPurchaseOrderDetail();
                newPartsPurchaseOrderDetail.SerialNumber = i++;
                newPartsPurchaseOrderDetail.SparePartId = item.SparePartId;
                newPartsPurchaseOrderDetail.SparePartCode = item.SparePartCode;
                newPartsPurchaseOrderDetail.SparePartName = item.SparePartName;
                newPartsPurchaseOrderDetail.MeasureUnit = item.MeasureUnit;
                newPartsPurchaseOrderDetail.SupplierPartCode = item.SparePartCode;
                newPartsPurchaseOrderDetail.PromisedDeliveryTime = DateTime.Now.AddDays(5);
                newPartsPurchaseOrderDetail.UnitPrice = Math.Round(partssalsedetail.OrderPrice / 1.13m, 2);
                newPartsPurchaseOrderDetail.OrderAmount = item.CurrentFulfilledQuantity;
                newPartsPurchaseOrderDetail.ConfirmedAmount = item.CurrentFulfilledQuantity;
                newPartsPurchaseOrderDetail.ShippingAmount = item.CurrentFulfilledQuantity;
                newPartsPurchaseOrderDetail.Remark = partssalsedetail.Remark;
                newPartsPurchaseOrderDetail.ShippingDate = DateTime.Now;
                newPartsPurchaseOrderDetail.PriceType = (int)DcsPurchasePriceType.正式价格;
                //价格类型
                if(newPartsPurchaseOrderDetail.PriceType != null)
                    newPartsPurchaseOrderDetail.PriceTypeName = Enum.GetName(typeof(DcsABCStrategyCategory), newPartsPurchaseOrderDetail.PriceType);
                if(partsBranch.PartABC != null)
                    newPartsPurchaseOrderDetail.ABCStrategy = Enum.GetName(typeof(DcsABCStrategyCategory), partsBranch.PartABC);

                newPartsPurchaseOrder.PartsPurchaseOrderDetails.Add(newPartsPurchaseOrderDetail);
            }
            newPartsPurchaseOrder.TotalAmount = newPartsPurchaseOrder.PartsPurchaseOrderDetails.Sum(r => r.ConfirmedAmount * r.UnitPrice);
            InsertToDatabase(newPartsPurchaseOrder);
            #endregion
            this.ObjectContext.SaveChanges();
            #region 2、收货确认的供应商发运单和发运单清单
            var newSupplierShippingOrder = new SupplierShippingOrder();
            newSupplierShippingOrder.Code = CodeGenerator.Generate("SupplierShippingOrder", partsSalesOrder.BranchCode);
            newSupplierShippingOrder.BranchId = partsSalesOrder.BranchId;
            newSupplierShippingOrder.BranchCode = partsSalesOrder.BranchCode;
            newSupplierShippingOrder.BranchName = partsSalesOrder.BranchName;
            newSupplierShippingOrder.PartsSalesCategoryId = orderPartsSalesCategory.Id;
            newSupplierShippingOrder.PartsSalesCategoryName = orderPartsSalesCategory.Name;
            newSupplierShippingOrder.PartsSupplierId = (int)supplierId;
            newSupplierShippingOrder.PartsSupplierCode = supplierCode;
            newSupplierShippingOrder.PartsSupplierName = supplierName;
            newSupplierShippingOrder.IfDirectProvision = false;
            newSupplierShippingOrder.PartsPurchaseOrderId = newPartsPurchaseOrder.Id;
            newSupplierShippingOrder.PartsPurchaseOrderCode = newPartsPurchaseOrder.Code;
            newSupplierShippingOrder.OriginalRequirementBillId = partsSalesOrder.Id;
            newSupplierShippingOrder.OriginalRequirementBillCode = partsSalesOrder.Code;
            newSupplierShippingOrder.OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.配件销售订单;
            newSupplierShippingOrder.ReceivingWarehouseId = virtualBranchWarehouse.Id;
            newSupplierShippingOrder.ReceivingWarehouseName = virtualBranchWarehouse.Name;
            newSupplierShippingOrder.ReceivingCompanyId = partsSalesOrder.BranchId;
            newSupplierShippingOrder.ReceivingCompanyCode = partsSalesOrder.BranchCode;
            newSupplierShippingOrder.ReceivingCompanyName = partsSalesOrder.BranchName;
            newSupplierShippingOrder.ReceivingAddress = partsSalesOrder.BranchName;
            newSupplierShippingOrder.ShippingMethod = partsSalesOrder.ShippingMethod;
            newSupplierShippingOrder.Status = (int)DcsSupplierShippingOrderStatus.收货确认;
            newSupplierShippingOrder.RequestedDeliveryTime = DateTime.Now.AddDays(5);
            newSupplierShippingOrder.PlanDeliveryTime = DateTime.Now.AddDays(5);
            newSupplierShippingOrder.CreatorId = user.Id;
            newSupplierShippingOrder.CreatorName = user.Name;
            newSupplierShippingOrder.CreateTime = DateTime.Now;
            newSupplierShippingOrder.Remark = newPartsPurchaseOrder.Remark;
            //清单
            var j = 1;
            foreach(var item in processDetails) {
                var partsPurchaseOrderDetail = newPartsPurchaseOrder.PartsPurchaseOrderDetails.SingleOrDefault(r => r.SparePartId == item.SparePartId);
                var partssalsedetail = partsSalesOrderDetails.SingleOrDefault(r => r.SparePartId == item.SparePartId);
                var newSupplierShippingDetail = new SupplierShippingDetail();
                newSupplierShippingDetail.SerialNumber = j++;
                newSupplierShippingDetail.SparePartId = item.SparePartId;
                newSupplierShippingDetail.SparePartCode = item.SparePartCode;
                newSupplierShippingDetail.SparePartName = item.SparePartName;
                newSupplierShippingDetail.SupplierPartCode = item.SparePartCode;
                newSupplierShippingDetail.MeasureUnit = item.MeasureUnit;
                newSupplierShippingDetail.Quantity = item.CurrentFulfilledQuantity;
                newSupplierShippingDetail.ConfirmedAmount = item.CurrentFulfilledQuantity;
                newSupplierShippingDetail.UnitPrice = partssalsedetail.OrderPrice;
                newSupplierShippingDetail.Remark = partsPurchaseOrderDetail.Remark;

                newSupplierShippingOrder.SupplierShippingDetails.Add(newSupplierShippingDetail);
            }
            InsertToDatabase(newSupplierShippingOrder);
            this.ObjectContext.SaveChanges();
            # endregion
            #region 3、上架完成的入库计划单及入库计划单清单
            var newPartsInboundPlan = new PartsInboundPlan();
            newPartsInboundPlan.BranchId = partsSalesOrder.BranchId;
            newPartsInboundPlan.BranchCode = partsSalesOrder.BranchCode;
            newPartsInboundPlan.BranchName = partsSalesOrder.BranchName;
            newPartsInboundPlan.WarehouseId = virtualBranchWarehouse.Id;
            newPartsInboundPlan.WarehouseCode = virtualBranchWarehouse.Code;
            newPartsInboundPlan.WarehouseName = virtualBranchWarehouse.Name;
            newPartsInboundPlan.StorageCompanyId = partsSalesOrder.BranchId;
            newPartsInboundPlan.StorageCompanyCode = partsSalesOrder.BranchCode;
            newPartsInboundPlan.StorageCompanyName = partsSalesOrder.BranchName;
            newPartsInboundPlan.StorageCompanyType = virtualBranchWarehouse.StorageCompanyType;
            newPartsInboundPlan.PartsSalesCategoryId = orderPartsSalesCategory.Id;
            newPartsInboundPlan.PartsSalesCategorieName = orderPartsSalesCategory.Name;
            newPartsInboundPlan.CounterpartCompanyId = newSupplierShippingOrder.PartsSupplierId;
            newPartsInboundPlan.CounterpartCompanyCode = newSupplierShippingOrder.PartsSupplierCode;
            newPartsInboundPlan.CounterpartCompanyName = newSupplierShippingOrder.PartsSupplierName;
            newPartsInboundPlan.SourceId = newSupplierShippingOrder.Id;
            newPartsInboundPlan.SourceCode = newSupplierShippingOrder.Code;
            newPartsInboundPlan.InboundType = (int)DcsPartsInboundType.配件采购;
            newPartsInboundPlan.CustomerAccountId = null;
            newPartsInboundPlan.OriginalRequirementBillId = newPartsPurchaseOrder.Id;
            newPartsInboundPlan.OriginalRequirementBillCode = newPartsPurchaseOrder.Code;
            newPartsInboundPlan.OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.配件采购订单;
            newPartsInboundPlan.PartsPurchaseOrderTypeId = newPartsPurchaseOrder.PartsPurchaseOrderTypeId;
            newPartsInboundPlan.Status = (int)DcsPartsInboundPlanStatus.上架完成;
            newPartsInboundPlan.IfWmsInterface = false;
            newPartsInboundPlan.StorageCompanyCode = partsSalesOrder.BranchCode;
            newPartsInboundPlan.Remark = partsSalesOrder.Remark;
            newPartsInboundPlan.Code = CodeGenerator.Generate("PartsInboundPlan", newPartsInboundPlan.StorageCompanyCode);
            newPartsInboundPlan.CreatorId = user.Id;
            newPartsInboundPlan.CreatorName = user.Name;
            newPartsInboundPlan.CreateTime = DateTime.Now;
            //清单
            foreach(var item in processDetails) {
                var partssalsedetail = partsSalesOrderDetails.SingleOrDefault(r => r.SparePartId == item.SparePartId);
                var newPartsInboundPlanDetail = new PartsInboundPlanDetail();
                newPartsInboundPlanDetail.SparePartId = item.SparePartId;
                newPartsInboundPlanDetail.SparePartCode = item.SparePartCode;
                newPartsInboundPlanDetail.SparePartName = item.SparePartName;
                newPartsInboundPlanDetail.PlannedAmount = item.CurrentFulfilledQuantity;
                newPartsInboundPlanDetail.InspectedQuantity = item.CurrentFulfilledQuantity;
                newPartsInboundPlanDetail.Price = Math.Round(partssalsedetail.OrderPrice / 1.13m, 2);
                newPartsInboundPlanDetail.Remark = partssalsedetail.Remark;
                newPartsInboundPlanDetail.OriginalPrice = partssalsedetail.OriginalPrice;
                newPartsInboundPlan.PartsInboundPlanDetails.Add(newPartsInboundPlanDetail);
            }
            InsertToDatabase(newPartsInboundPlan);
            this.ObjectContext.SaveChanges();
            #endregion
            #region 4、	入库单和入库单清单
            var newPartsInboundCheckBill = new PartsInboundCheckBill();
            newPartsInboundCheckBill.PartsInboundPlanId = newPartsInboundPlan.Id;
            newPartsInboundCheckBill.WarehouseId = newPartsInboundPlan.WarehouseId;
            newPartsInboundCheckBill.WarehouseCode = newPartsInboundPlan.WarehouseCode;
            newPartsInboundCheckBill.WarehouseName = newPartsInboundPlan.WarehouseName;
            newPartsInboundCheckBill.StorageCompanyId = newPartsInboundPlan.StorageCompanyId;
            newPartsInboundCheckBill.StorageCompanyCode = newPartsInboundPlan.StorageCompanyCode;
            newPartsInboundCheckBill.StorageCompanyName = newPartsInboundPlan.StorageCompanyName;
            newPartsInboundCheckBill.StorageCompanyType = newPartsInboundPlan.StorageCompanyType;
            newPartsInboundCheckBill.PartsSalesCategoryId = newPartsInboundPlan.PartsSalesCategoryId;
            newPartsInboundCheckBill.BranchId = newPartsInboundPlan.BranchId;
            newPartsInboundCheckBill.BranchCode = newPartsInboundPlan.BranchCode;
            newPartsInboundCheckBill.BranchName = newPartsInboundPlan.BranchName;
            newPartsInboundCheckBill.CounterpartCompanyId = newPartsInboundPlan.CounterpartCompanyId;
            newPartsInboundCheckBill.CounterpartCompanyCode = newPartsInboundPlan.CounterpartCompanyCode;
            newPartsInboundCheckBill.CounterpartCompanyName = newPartsInboundPlan.CounterpartCompanyName;
            newPartsInboundCheckBill.InboundType = newPartsInboundPlan.InboundType;
            newPartsInboundCheckBill.CustomerAccountId = newPartsInboundPlan.CustomerAccountId;
            newPartsInboundCheckBill.OriginalRequirementBillId = newPartsPurchaseOrder.Id;
            newPartsInboundCheckBill.OriginalRequirementBillCode = newPartsInboundPlan.OriginalRequirementBillCode;
            newPartsInboundCheckBill.OriginalRequirementBillType = newPartsInboundPlan.OriginalRequirementBillType;
            newPartsInboundCheckBill.Status = (int)DcsPartsInboundCheckBillStatus.上架完成;
            newPartsInboundCheckBill.SettlementStatus = (int)DcsPartsSettlementStatus.待结算;
            newPartsInboundCheckBill.Remark = newPartsInboundPlan.Remark;
            newPartsInboundCheckBill.Code = CodeGenerator.Generate("PartsInboundCheckBill", newPartsInboundCheckBill.StorageCompanyCode);
            newPartsInboundCheckBill.CreatorId = user.Id;
            newPartsInboundCheckBill.CreatorName = user.Name;
            newPartsInboundCheckBill.CreateTime = DateTime.Now;
            //清单
            foreach(var item in processDetails) {
                var partssalsedetail = partsSalesOrderDetails.SingleOrDefault(r => r.SparePartId == item.SparePartId);
                var newPartsInboundCheckBillDetail = new PartsInboundCheckBillDetail();
                newPartsInboundCheckBillDetail.SparePartId = item.SparePartId;
                newPartsInboundCheckBillDetail.SparePartCode = item.SparePartCode;
                newPartsInboundCheckBillDetail.SparePartName = item.SparePartName;
                //虚拟库无库位
                newPartsInboundCheckBillDetail.WarehouseAreaId = virtualBranchWarehouse.Id;
                newPartsInboundCheckBillDetail.WarehouseAreaCode = virtualBranchWarehouse.Code;
                newPartsInboundCheckBillDetail.InspectedQuantity = item.CurrentFulfilledQuantity;
                newPartsInboundCheckBillDetail.SettlementPrice = Math.Round(partssalsedetail.OrderPrice / 1.13m, 2);
                newPartsInboundCheckBillDetail.CostPrice = Math.Round(partssalsedetail.OrderPrice / 1.13m, 2);
                newPartsInboundCheckBillDetail.OriginalPrice = partssalsedetail.OriginalPrice;
                newPartsInboundCheckBill.PartsInboundCheckBillDetails.Add(newPartsInboundCheckBillDetail);
            }
            InsertToDatabase(newPartsInboundCheckBill);
            ObjectContext.SaveChanges();
            #endregion

            #region 5、	针对销售订单提报单位的 出库完成的出库计划单和清单
            var newPartsOutboundPlan = new PartsOutboundPlan();
            newPartsOutboundPlan.WarehouseId = virtualBranchWarehouse.Id;
            newPartsOutboundPlan.WarehouseCode = virtualBranchWarehouse.Code;
            newPartsOutboundPlan.WarehouseName = virtualBranchWarehouse.Name;
            newPartsOutboundPlan.BranchId = newPartsInboundCheckBill.BranchId;
            newPartsOutboundPlan.BranchCode = newPartsInboundCheckBill.BranchCode;
            newPartsOutboundPlan.BranchName = newPartsInboundCheckBill.BranchName;
            newPartsOutboundPlan.StorageCompanyId = newPartsInboundCheckBill.StorageCompanyId;
            newPartsOutboundPlan.StorageCompanyCode = newPartsInboundCheckBill.StorageCompanyCode;
            newPartsOutboundPlan.StorageCompanyName = newPartsInboundCheckBill.StorageCompanyName;
            newPartsOutboundPlan.StorageCompanyType = newPartsInboundCheckBill.StorageCompanyType;
            newPartsOutboundPlan.PartsSalesCategoryId = newPartsInboundCheckBill.PartsSalesCategoryId;
            newPartsOutboundPlan.CounterpartCompanyId = partsSalesOrder.SubmitCompanyId;
            newPartsOutboundPlan.CounterpartCompanyCode = partsSalesOrder.SubmitCompanyCode;
            newPartsOutboundPlan.CounterpartCompanyName = partsSalesOrder.SubmitCompanyName;
            newPartsOutboundPlan.ReceivingCompanyId = partsSalesOrder.SubmitCompanyId;
            newPartsOutboundPlan.ReceivingCompanyCode = partsSalesOrder.SubmitCompanyCode;
            newPartsOutboundPlan.ReceivingCompanyName = partsSalesOrder.SubmitCompanyName;
            if(receivingWarehouse != null) {
                newPartsOutboundPlan.ReceivingWarehouseId = receivingWarehouse.Id;
                newPartsOutboundPlan.ReceivingWarehouseCode = receivingWarehouse.Code;
                newPartsOutboundPlan.ReceivingWarehouseName = receivingWarehouse.Name;
            }
            newPartsOutboundPlan.ReceivingAddress = partsSalesOrder.ReceivingAddress;
            newPartsOutboundPlan.PartsSalesOrderTypeId = partsSalesOrder.PartsSalesOrderTypeId;
            newPartsOutboundPlan.PartsSalesOrderTypeName = partsSalesOrder.PartsSalesOrderTypeName;
            newPartsOutboundPlan.SourceId = partsSalesOrder.Id;
            newPartsOutboundPlan.SourceCode = partsSalesOrder.Code;
            newPartsOutboundPlan.OutboundType = (int)DcsPartsOutboundType.配件销售;
            newPartsOutboundPlan.ShippingMethod = (int)DcsPartsShippingMethod.公路物流;
            newPartsOutboundPlan.CustomerAccountId = partsSalesOrder.CustomerAccountId;
            newPartsOutboundPlan.OriginalRequirementBillId = partsSalesOrder.Id;
            newPartsOutboundPlan.OriginalRequirementBillCode = partsSalesOrder.Code;
            newPartsOutboundPlan.OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.配件销售订单;
            newPartsOutboundPlan.Status = (int)DcsPartsOutboundPlanStatus.出库完成;
            newPartsOutboundPlan.IfWmsInterface = false;
            newPartsOutboundPlan.Remark = partsSalesOrder.Remark;
            newPartsOutboundPlan.Code = CodeGenerator.Generate("PartsOutboundPlan", newPartsOutboundPlan.StorageCompanyCode);
            newPartsOutboundPlan.CreatorId = user.Id;
            newPartsOutboundPlan.CreatorName = user.Name;
            newPartsOutboundPlan.CreateTime = DateTime.Now;
            //清单
            foreach(var item in processDetails) {
                var partssalsedetail = partsSalesOrderDetails.SingleOrDefault(r => r.SparePartId == item.SparePartId);
                var newPartsOutboundPlanDetail = new PartsOutboundPlanDetail();
                newPartsOutboundPlanDetail.SparePartId = item.SparePartId;
                newPartsOutboundPlanDetail.SparePartCode = item.SparePartCode;
                newPartsOutboundPlanDetail.SparePartName = item.SparePartName;
                newPartsOutboundPlanDetail.PlannedAmount = item.CurrentFulfilledQuantity;
                newPartsOutboundPlanDetail.OutboundFulfillment = item.CurrentFulfilledQuantity;
                newPartsOutboundPlanDetail.Price = partssalsedetail.OrderPrice;
                newPartsOutboundPlanDetail.Remark = partssalsedetail.Remark;
                newPartsOutboundPlanDetail.OriginalPrice = partssalsedetail.OriginalPrice;
                newPartsOutboundPlan.PartsOutboundPlanDetails.Add(newPartsOutboundPlanDetail);
            }
            InsertToDatabase(newPartsOutboundPlan);
            ObjectContext.SaveChanges();
            #endregion
            #region 6、	针对销售订单提报单位的 已发运状态的发运单和清单
            var newPartsShippingOrder = new PartsShippingOrder();
            newPartsShippingOrder.Type = (int)DcsPartsShippingOrderType.销售;
            newPartsShippingOrder.PartsSalesCategoryId = newPartsOutboundPlan.PartsSalesCategoryId;
            newPartsShippingOrder.BranchId = newPartsOutboundPlan.BranchId;
            newPartsShippingOrder.ShippingCompanyId = newPartsOutboundPlan.StorageCompanyId;
            newPartsShippingOrder.ShippingCompanyCode = newPartsOutboundPlan.StorageCompanyCode;
            newPartsShippingOrder.ShippingCompanyName = newPartsOutboundPlan.StorageCompanyName;
            newPartsShippingOrder.SettlementCompanyId = newPartsOutboundPlan.StorageCompanyId;
            newPartsShippingOrder.SettlementCompanyCode = newPartsOutboundPlan.StorageCompanyCode;
            newPartsShippingOrder.SettlementCompanyName = newPartsOutboundPlan.StorageCompanyName;
            newPartsShippingOrder.ReceivingCompanyId = newPartsOutboundPlan.ReceivingCompanyId;
            newPartsShippingOrder.ReceivingCompanyCode = newPartsOutboundPlan.ReceivingCompanyCode;
            newPartsShippingOrder.ReceivingCompanyName = newPartsOutboundPlan.ReceivingCompanyName;
            newPartsShippingOrder.InvoiceReceiveSaleCateId = newPartsOutboundPlan.PartsSalesOrderTypeId;
            newPartsShippingOrder.InvoiceReceiveSaleCateName = newPartsOutboundPlan.PartsSalesOrderTypeName;
            newPartsShippingOrder.WarehouseId = newPartsOutboundPlan.WarehouseId;
            newPartsShippingOrder.WarehouseCode = newPartsOutboundPlan.WarehouseCode;
            newPartsShippingOrder.WarehouseName = newPartsOutboundPlan.WarehouseName;
            newPartsShippingOrder.ReceivingWarehouseId = newPartsOutboundPlan.ReceivingWarehouseId;
            newPartsShippingOrder.ReceivingWarehouseCode = newPartsOutboundPlan.ReceivingWarehouseCode;
            newPartsShippingOrder.ReceivingWarehouseName = newPartsOutboundPlan.ReceivingWarehouseName;
            newPartsShippingOrder.ReceivingAddress = newPartsOutboundPlan.ReceivingAddress;
            newPartsShippingOrder.OriginalRequirementBillId = newPartsOutboundPlan.OriginalRequirementBillId;
            newPartsShippingOrder.OriginalRequirementBillType = newPartsOutboundPlan.OriginalRequirementBillType;
            newPartsShippingOrder.OriginalRequirementBillCode = newPartsOutboundPlan.OriginalRequirementBillCode;
            newPartsShippingOrder.ShippingMethod = newPartsOutboundPlan.ShippingMethod;
            newPartsShippingOrder.Status = (int)DcsPartsShippingOrderStatus.已发货;
            newPartsShippingOrder.Remark = newPartsOutboundPlan.Remark;
            newPartsShippingOrder.Code = CodeGenerator.Generate("PartsShippingOrder", newPartsShippingOrder.ShippingCompanyCode);
            newPartsShippingOrder.CreatorId = user.Id;
            newPartsShippingOrder.CreatorName = user.Name;
            newPartsShippingOrder.CreateTime = DateTime.Now;
            newPartsShippingOrder.ShippingDate = DateTime.Now;
            newPartsShippingOrder.RequestedArrivalDate = DateTime.Now.AddDays(5);
            //清单
            foreach(var item in processDetails) {
                var partssalsedetail = partsSalesOrderDetails.SingleOrDefault(r => r.SparePartId == item.SparePartId);
                var newPartsShippingOrderDetail = new PartsShippingOrderDetail();
                newPartsShippingOrderDetail.SparePartId = item.SparePartId;
                newPartsShippingOrderDetail.SparePartCode = item.SparePartCode;
                newPartsShippingOrderDetail.SparePartName = item.SparePartName;
                newPartsShippingOrderDetail.ShippingAmount = item.CurrentFulfilledQuantity;
                //newPartsShippingOrderDetail.ConfirmedAmount = item.CurrentFulfilledQuantity;
                newPartsShippingOrderDetail.SettlementPrice = partssalsedetail.OrderPrice;
                newPartsShippingOrderDetail.PartsOutboundPlanId = newPartsOutboundPlan.Id;
                newPartsShippingOrderDetail.PartsOutboundPlanCode = newPartsOutboundPlan.Code;
                newPartsShippingOrderDetail.Remark = partssalsedetail.Remark;

                newPartsShippingOrder.PartsShippingOrderDetails.Add(newPartsShippingOrderDetail);
            }
            var simpleSparePartInfos = newPartsShippingOrder.PartsShippingOrderDetails.Select(r => new {
                r.SparePartId,
                r.ShippingAmount
            }).ToList();
            var sparePartsIds = newPartsShippingOrder.PartsShippingOrderDetails.Select(r => r.SparePartId).ToArray();
            var spareparts = ObjectContext.SpareParts.Where(r => sparePartsIds.Contains(r.Id) && r.Status != (int)DcsMasterDataStatus.作废).ToArray();
            var totalWeight = (from a in simpleSparePartInfos
                               from b in spareparts
                               where a.SparePartId == b.Id
                               select new {
                                   weightOfpart = a.ShippingAmount * b.Weight,
                                   volumeOfPart = a.ShippingAmount * b.Volume,
                               }).ToArray();
            //计算总重量
            newPartsShippingOrder.Weight = totalWeight.Sum(r => r.weightOfpart);
            newPartsShippingOrder.Volume = totalWeight.Sum(r => r.volumeOfPart);

            InsertToDatabase(newPartsShippingOrder);
            ObjectContext.SaveChanges();
            #endregion
            #region 7、	针对销售订单提报单位的 销售出库单和清单
            var newPartsOutboundBill = new PartsOutboundBill();
            newPartsOutboundBill.Code = CodeGenerator.Generate("PartsOutboundBill", newPartsOutboundPlan.StorageCompanyCode);
            newPartsOutboundBill.PartsOutboundPlanId = newPartsOutboundPlan.Id;
            newPartsOutboundBill.WarehouseId = newPartsOutboundPlan.WarehouseId;
            newPartsOutboundBill.WarehouseCode = newPartsOutboundPlan.WarehouseCode;
            newPartsOutboundBill.WarehouseName = newPartsOutboundPlan.WarehouseName;
            newPartsOutboundBill.StorageCompanyId = newPartsOutboundPlan.StorageCompanyId;
            newPartsOutboundBill.StorageCompanyCode = newPartsOutboundPlan.StorageCompanyCode;
            newPartsOutboundBill.StorageCompanyName = newPartsOutboundPlan.StorageCompanyName;
            newPartsOutboundBill.StorageCompanyType = newPartsOutboundPlan.StorageCompanyType;
            newPartsOutboundBill.BranchId = newPartsOutboundPlan.BranchId;
            newPartsOutboundBill.BranchCode = newPartsOutboundPlan.BranchCode;
            newPartsOutboundBill.BranchName = newPartsOutboundPlan.BranchName;
            newPartsOutboundBill.PartsSalesCategoryId = newPartsOutboundPlan.PartsSalesCategoryId;
            newPartsOutboundBill.PartsSalesOrderTypeId = newPartsOutboundPlan.PartsSalesOrderTypeId;
            newPartsOutboundBill.PartsSalesOrderTypeName = newPartsOutboundPlan.PartsSalesOrderTypeName;
            newPartsOutboundBill.CounterpartCompanyId = newPartsOutboundPlan.CounterpartCompanyId;
            newPartsOutboundBill.CounterpartCompanyCode = newPartsOutboundPlan.CounterpartCompanyCode;
            newPartsOutboundBill.CounterpartCompanyName = newPartsOutboundPlan.CounterpartCompanyName;
            newPartsOutboundBill.ReceivingCompanyId = newPartsOutboundPlan.ReceivingCompanyId;
            newPartsOutboundBill.ReceivingCompanyCode = newPartsOutboundPlan.ReceivingCompanyCode;
            newPartsOutboundBill.ReceivingCompanyName = newPartsOutboundPlan.ReceivingCompanyName;
            newPartsOutboundBill.ReceivingWarehouseId = newPartsOutboundPlan.ReceivingWarehouseId;
            newPartsOutboundBill.ReceivingWarehouseCode = newPartsOutboundPlan.ReceivingWarehouseCode;
            newPartsOutboundBill.ReceivingWarehouseName = newPartsOutboundPlan.ReceivingWarehouseName;
            newPartsOutboundBill.OriginalRequirementBillId = newPartsOutboundPlan.OriginalRequirementBillId;
            newPartsOutboundBill.OriginalRequirementBillCode = newPartsOutboundPlan.OriginalRequirementBillCode;
            newPartsOutboundBill.OriginalRequirementBillType = newPartsOutboundPlan.OriginalRequirementBillType;
            newPartsOutboundBill.OutboundType = newPartsOutboundPlan.OutboundType;
            newPartsOutboundBill.ShippingMethod = newPartsOutboundPlan.ShippingMethod;
            newPartsOutboundBill.CustomerAccountId = newPartsOutboundPlan.CustomerAccountId;
            newPartsOutboundBill.SettlementStatus = (int)DcsPartsSettlementStatus.待结算;
            newPartsOutboundBill.PartsSalesOrderTypeId = partsSalesOrder.PartsSalesOrderTypeId;
            newPartsOutboundBill.PartsSalesOrderTypeName = partsSalesOrder.PartsSalesOrderTypeName;
            newPartsOutboundBill.CreatorId = user.Id;
            newPartsOutboundBill.CreatorName = user.Name;
            newPartsOutboundBill.CreateTime = DateTime.Now;
            //清单
            foreach(var item in processDetails) {
                var partssalsedetail = partsSalesOrderDetails.SingleOrDefault(r => r.SparePartId == item.SparePartId);
                var newPartsOutboundBillDetail = new PartsOutboundBillDetail();
                newPartsOutboundBillDetail.SparePartId = item.SparePartId;
                newPartsOutboundBillDetail.SparePartCode = item.SparePartCode;
                newPartsOutboundBillDetail.SparePartName = item.SparePartName;
                newPartsOutboundBillDetail.OutboundAmount = item.CurrentFulfilledQuantity;
                //虚拟库无库位
                newPartsOutboundBillDetail.WarehouseAreaId = virtualBranchWarehouse.Id;
                newPartsOutboundBillDetail.WarehouseAreaCode = virtualBranchWarehouse.Code;
                newPartsOutboundBillDetail.SettlementPrice = partssalsedetail.OrderPrice;
                newPartsOutboundBillDetail.CostPrice = Math.Round(partssalsedetail.OrderPrice / 1.16m, 2);
                newPartsOutboundBillDetail.Remark = partssalsedetail.Remark;
                newPartsOutboundBillDetail.OriginalPrice = partssalsedetail.OriginalPrice;
                newPartsOutboundBill.PartsOutboundBillDetails.Add(newPartsOutboundBillDetail);
            }
            InsertToDatabase(newPartsOutboundBill);
            ObjectContext.SaveChanges();
            #endregion

            #region 配件发运单关联单
            var newPartsShippingOrderRef = new PartsShippingOrderRef {
                PartsOutboundBillId = newPartsOutboundBill.Id,
                PartsShippingOrderId = newPartsShippingOrder.Id
            };
            InsertToDatabase(newPartsShippingOrderRef);
            #endregion
            ObjectContext.SaveChanges();
        }

        internal void 统购分销单元格(List<PartsSalesOrderProcessDetail> marketingProcessDetailProcess, PartsSalesOrder dbpartsSalesOrder) {
            partsStockLists.Clear();
            var i = 0;
            var marketingProcessDetailgroups = marketingProcessDetailProcess.GroupBy(r => r.WarehouseId).ToArray();
            var keyvaluePartsShippingMethod = ObjectContext.KeyValueItems.Where(r => r.Name == "PartsShipping_Method").ToList();
            var shippingMethodKey = keyvaluePartsShippingMethod.Any(r => r.Value == "客户自行运输") ? keyvaluePartsShippingMethod.First(r => r.Value == "客户自行运输").Key : default(int);
            foreach(var details in marketingProcessDetailgroups) {
                var marketingProcessDetail = details.First();
                var orderDetails = marketingProcessDetailProcess.Where(r => r.WarehouseId == details.Key).ToArray();
                if(marketingProcessDetail == null)
                    return;
                var marketingProcessDetails = orderDetails.Select(v => new {
                    SparePartId = v.NewPartId ?? v.SparePartId,
                    SparePartCode = v.NewPartId.HasValue ? v.NewPartCode : v.SparePartCode,
                    SparePartName = v.NewPartId.HasValue ? v.NewPartName : v.SparePartName,
                    v.MeasureUnit,
                    v.OrderedQuantity,
                    v.CurrentFulfilledQuantity,
                    v.OrderPrice
                }).GroupBy(v => v.SparePartId).Select(v => new PartsSalesOrderProcessDetail {
                    SparePartId = v.Key,
                    SparePartCode = v.First().SparePartCode,
                    SparePartName = v.First().SparePartName,
                    MeasureUnit = v.First().MeasureUnit,
                    OrderedQuantity = v.First().OrderedQuantity,
                    CurrentFulfilledQuantity = v.Sum(v1 => v1.CurrentFulfilledQuantity),
                    OrderPrice = v.First().OrderPrice
                });
                var partIds = marketingProcessDetails.Select(r => r.SparePartId).ToArray();
                var warehouse = ObjectContext.Warehouses.Include("Branch").FirstOrDefault(r => r.Id == marketingProcessDetail.WarehouseId);
                var purchaseWarehouse = ObjectContext.Warehouses.Include("Branch").Include("Company").FirstOrDefault(r => r.Id == marketingProcessDetail.PurchaseWarehouseId);
                if(warehouse == null)
                    throw new ValidationException(ErrorStrings.PartsSalesOrderProcess_Validation32);
                if(purchaseWarehouse == null)
                    throw new ValidationException(ErrorStrings.PartsSalesOrderProcess_Validation33);
                var dbSalesUnitAffiWarehouses = ObjectContext.SalesUnitAffiWarehouses.Include("SalesUnit").Where(r => r.WarehouseId == marketingProcessDetail.WarehouseId || r.WarehouseId == marketingProcessDetail.PurchaseWarehouseId).ToList();
                var partsSalesCategoryIds = dbSalesUnitAffiWarehouses.Select(r => r.SalesUnit).Select(r => r.PartsSalesCategoryId).ToArray();
                var dbPartsSalesCategories = ObjectContext.PartsSalesCategories.Where(r => partsSalesCategoryIds.Contains(r.Id)).ToList();
                var companyIds = dbSalesUnitAffiWarehouses.Select(r => r.SalesUnit).Select(r => r.OwnerCompanyId).ToArray();
                var dbCompanies = ObjectContext.Companies.Where(r => companyIds.Contains(r.Id) || r.Id == warehouse.StorageCompanyId || r.Id == purchaseWarehouse.StorageCompanyId || r.Id == dbpartsSalesOrder.SalesUnitOwnerCompanyId).ToList();
                //财务主体不同
                if(warehouse.BranchId != purchaseWarehouse.BranchId) {
                    var purchaseBranch = ObjectContext.Branches.FirstOrDefault(r => r.Id == purchaseWarehouse.BranchId);
                    if(purchaseBranch == null)
                        throw new ValidationException(ErrorStrings.PartsSalesOrderProcess_Validation34);
                    var salesUnit = dbSalesUnitAffiWarehouses.Where(r => r.WarehouseId == marketingProcessDetail.PurchaseWarehouseId).Select(r => r.SalesUnit).FirstOrDefault(r => r.Status == (int)DcsBaseDataStatus.有效);
                    if(salesUnit == null)
                        throw new ValidationException(ErrorStrings.PartsSalesOrderProcess_Validation35);
                    var partsSalesCategory = dbPartsSalesCategories.FirstOrDefault(r => r.Id == salesUnit.PartsSalesCategoryId);
                    if(partsSalesCategory == null)
                        throw new ValidationException(ErrorStrings.PartsSalesOrderProcess_Validation36);
                    var company = dbCompanies.FirstOrDefault(r => r.Id == salesUnit.OwnerCompanyId);
                    if(company == null)
                        throw new ValidationException(ErrorStrings.PartsSalesOrderProcess_Validation37);
                    var salesCenterstrategy = ObjectContext.SalesCenterstrategies.FirstOrDefault(r => r.PartsSalesCategoryId == partsSalesCategory.Id);
                    //if(salesCenterstrategy == null)
                    //    throw new ValidationException(string.Format("品牌{0}对应的销售中心策略不存在", partsSalesCategory.Code));
                    var customerAccount = new CustomerAccountAch(this.DomainService).GetCustomerAccountsWithLock(ObjectContext.CustomerAccounts.Where(r => r.AccountGroupId == salesUnit.AccountGroupId && r.CustomerCompanyId == dbpartsSalesOrder.SalesUnitOwnerCompanyId && r.Status == (int)DcsBaseDataStatus.有效)).ToArray();
                    if(!customerAccount.Any() || customerAccount.Count() > 1)
                        throw new ValidationException(ErrorStrings.PartsSalesOrderProcess_Validation38);
                    var oldCompany = dbCompanies.FirstOrDefault(r => r.Id == dbpartsSalesOrder.SalesUnitOwnerCompanyId);
                    if(oldCompany == null)
                        throw new ValidationException(ErrorStrings.PartsSalesOrderProcess_Validation39);
                    var partsSalesOrderType = ObjectContext.PartsSalesOrderTypes.FirstOrDefault(r => r.PartsSalesCategoryId == partsSalesCategory.Id && r.Name == "急需订单");
                    if(partsSalesOrderType == null)
                        throw new ValidationException(ErrorStrings.PartsSalesOrderProcess_Validation40);

                    #region 生成统购出库仓库的销售订单及清单
                    var partsSalesOrder = new PartsSalesOrder();
                    partsSalesOrder.Id = i++;
                    partsSalesOrder.BranchId = purchaseWarehouse.BranchId;
                    partsSalesOrder.BranchCode = purchaseBranch.Code;
                    partsSalesOrder.BranchName = purchaseBranch.Name;
                    partsSalesOrder.SalesCategoryId = salesUnit.PartsSalesCategoryId;
                    partsSalesOrder.SalesCategoryName = partsSalesCategory.Name;
                    partsSalesOrder.SalesUnitId = salesUnit.Id;
                    partsSalesOrder.SalesUnitCode = salesUnit.Code;
                    partsSalesOrder.SalesUnitName = salesUnit.Name;
                    partsSalesOrder.SalesUnitOwnerCompanyId = salesUnit.OwnerCompanyId;
                    partsSalesOrder.SalesUnitOwnerCompanyCode = company.Code;
                    partsSalesOrder.SalesUnitOwnerCompanyName = company.Name;
                    partsSalesOrder.SourceBillCode = dbpartsSalesOrder.Code;
                    partsSalesOrder.SourceBillId = dbpartsSalesOrder.Id;
                    partsSalesOrder.IsDebt = salesCenterstrategy == null || (!salesCenterstrategy.AllowCustomerCredit.HasValue || salesCenterstrategy.AllowCustomerCredit.Value);
                    partsSalesOrder.CustomerAccountId = customerAccount.First().Id;
                    partsSalesOrder.InvoiceReceiveCompanyId = dbpartsSalesOrder.SalesUnitOwnerCompanyId;
                    partsSalesOrder.InvoiceReceiveCompanyCode = oldCompany.Code;
                    partsSalesOrder.InvoiceReceiveCompanyName = oldCompany.Name;
                    partsSalesOrder.InvoiceReceiveCompanyType = oldCompany.Type;
                    partsSalesOrder.SubmitCompanyId = partsSalesOrder.InvoiceReceiveCompanyId;
                    partsSalesOrder.SubmitCompanyCode = partsSalesOrder.InvoiceReceiveCompanyCode;
                    partsSalesOrder.SubmitCompanyName = partsSalesOrder.InvoiceReceiveCompanyName;
                    partsSalesOrder.PartsSalesOrderTypeId = partsSalesOrderType.Id;
                    partsSalesOrder.PartsSalesOrderTypeName = "急需订单";
                    //partsSalesOrder.TotalAmount = dbpartsSalesOrderDetails.Where(r => partIds.Contains(r.SparePartId)).Sum(r => r.OrderSum);
                    partsSalesOrder.IfAgencyService = true;
                    partsSalesOrder.SalesActivityDiscountRate = 0;
                    partsSalesOrder.SalesActivityDiscountAmount = 0;
                    partsSalesOrder.CustomerType = company.Type;
                    partsSalesOrder.IfDirectProvision = false;
                    partsSalesOrder.ReceivingCompanyId = partsSalesOrder.InvoiceReceiveCompanyId;
                    partsSalesOrder.ReceivingCompanyCode = partsSalesOrder.InvoiceReceiveCompanyCode;
                    partsSalesOrder.ReceivingCompanyName = partsSalesOrder.InvoiceReceiveCompanyName;
                    partsSalesOrder.ShippingMethod = shippingMethodKey;
                    partsSalesOrder.ReceivingWarehouseId = marketingProcessDetail.WarehouseId;
                    partsSalesOrder.ReceivingWarehouseName = warehouse.Name;
                    partsSalesOrder.WarehouseId = marketingProcessDetail.PurchaseWarehouseId;
                    partsSalesOrder.Status = (int)DcsPartsSalesOrderStatus.审批完成;
                    new PartsSalesOrderAch(this.DomainService).InsertPartsSalesOrderValidate(partsSalesOrder);
                    var customerOrderPriceGrade = ObjectContext.CustomerOrderPriceGrades.FirstOrDefault(r => r.PartsSalesCategoryId == partsSalesOrder.SalesCategoryId && r.CustomerCompanyId == partsSalesOrder.InvoiceReceiveCompanyId && r.PartsSalesOrderTypeId == partsSalesOrder.PartsSalesOrderTypeId && r.Status == (int)DcsBaseDataStatus.有效);
                    if(customerOrderPriceGrade == null)
                        throw new ValidationException(ErrorStrings.PartsSalesOrderProcess_Validation41);
                    var dbPartsSalesPrices = ObjectContext.PartsSalesPrices.Where(r => r.PartsSalesCategoryId == partsSalesOrder.SalesCategoryId && partIds.Contains(r.SparePartId)).ToList();
                    var dbPartsSpecialTreatyPrices = ObjectContext.PartsSpecialTreatyPrices.Where(r => r.CompanyId == partsSalesOrder.InvoiceReceiveCompanyId && r.PartsSalesCategoryId == partsSalesOrder.SalesCategoryId && partIds.Contains(r.SparePartId)).ToList();
                    foreach(var detail in marketingProcessDetails) {
                        var partsSpecialTreatyPrice = dbPartsSpecialTreatyPrices.FirstOrDefault(r => r.SparePartId == detail.SparePartId);
                        var partsSalesPrice = dbPartsSalesPrices.FirstOrDefault(r => r.SparePartId == detail.SparePartId);
                        if(partsSpecialTreatyPrice == null && partsSalesPrice == null) {
                            throw new ValidationException(ErrorStrings.PartsSalesOrderProcess_Validation42);
                        }
                        var partsSalesOrderDetail = new PartsSalesOrderDetail();
                        partsSalesOrderDetail.PartsSalesOrderId = partsSalesOrder.Id;
                        partsSalesOrderDetail.SparePartId = detail.SparePartId;
                        partsSalesOrderDetail.SparePartCode = detail.SparePartCode;
                        partsSalesOrderDetail.SparePartName = detail.SparePartName;
                        partsSalesOrderDetail.MeasureUnit = detail.MeasureUnit;
                        partsSalesOrderDetail.OrderedQuantity = detail.CurrentFulfilledQuantity;
                        partsSalesOrderDetail.ApproveQuantity = detail.CurrentFulfilledQuantity;
                        partsSalesOrderDetail.CustOrderPriceGradeCoefficient = customerOrderPriceGrade.Coefficient;
                        partsSalesOrderDetail.OriginalPrice = partsSpecialTreatyPrice != null ? partsSpecialTreatyPrice.TreatyPrice : partsSalesPrice.SalesPrice;
                        partsSalesOrderDetail.IfCanNewPart = false;
                        partsSalesOrderDetail.DiscountedPrice = 0;
                        partsSalesOrderDetail.OrderPrice = partsSalesOrderDetail.OriginalPrice * (decimal)partsSalesOrderDetail.CustOrderPriceGradeCoefficient;
                        partsSalesOrderDetail.OrderSum = partsSalesOrderDetail.OrderPrice * partsSalesOrderDetail.OrderedQuantity;
                        partsSalesOrder.PartsSalesOrderDetails.Add(partsSalesOrderDetail);
                    }
                    partsSalesOrder.TotalAmount = partsSalesOrder.PartsSalesOrderDetails.Sum(r => r.OrderSum);
                    var newPartsSalesOrderProcess = new PartsSalesOrderProcess();
                    newPartsSalesOrderProcess.Id = i++;
                    //newPartsSalesOrderProcess.OriginalSalesOrderId = partsSalesOrder.Id;
                    newPartsSalesOrderProcess.BillStatusBeforeProcess = (int)DcsPartsSalesOrderStatus.提交;
                    newPartsSalesOrderProcess.BillStatusAfterProcess = (int)DcsPartsSalesOrderStatus.审批完成;
                    newPartsSalesOrderProcess.CurrentFulfilledAmount = marketingProcessDetails.Sum(r => r.CurrentFulfilledQuantity * r.OrderPrice);
                    newPartsSalesOrderProcess.ShippingMethod = shippingMethodKey;
                    newPartsSalesOrderProcess.PartsSalesOrder = partsSalesOrder;
                    //this.InsertPartsSalesOrderProcessValidate(newPartsSalesOrderProcess);

                    #region this.InsertPartsSalesOrderProcessValidate的代码搬过来
                    var userInfo = Utils.GetCurrentUserInfo();
                    newPartsSalesOrderProcess.CreatorId = userInfo.Id;
                    newPartsSalesOrderProcess.CreatorName = userInfo.Name;
                    newPartsSalesOrderProcess.CreateTime = DateTime.Now;
                    if(string.IsNullOrWhiteSpace(newPartsSalesOrderProcess.Code) || newPartsSalesOrderProcess.Code == GlobalVar.ASSIGNED_BY_SERVER)
                        newPartsSalesOrderProcess.Code = CodeGenerator.Generate("PartsSalesOrderProcess", userInfo.EnterpriseCode);

                    var branchstrategie = ObjectContext.Branchstrategies.Where(r => r.BranchId == partsSalesOrder.BranchId && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
                    if(branchstrategie == null) {
                        throw new ValidationException(ErrorStrings.PartsSalesOrderProcess_Validation43);
                    }
                    var maxInvoiceAmount = branchstrategie.MaxInvoiceAmount;
                    var popdSumAmount = newPartsSalesOrderProcess.PartsSalesOrderProcessDetails.Sum(r => r.OrderPrice * r.CurrentFulfilledQuantity);
                    if(popdSumAmount > maxInvoiceAmount) {
                        throw new ValidationException(ErrorStrings.PartsSalesOrderProcess_Validation44);
                    }
                    #endregion

                    foreach(var detail in partsSalesOrder.PartsSalesOrderDetails) {
                        var newPartsSalesOrderProcessDetail = new PartsSalesOrderProcessDetail();
                        newPartsSalesOrderProcessDetail.PartsSalesOrderProcessId = newPartsSalesOrderProcess.Id;
                        newPartsSalesOrderProcessDetail.WarehouseId = marketingProcessDetail.PurchaseWarehouseId;
                        newPartsSalesOrderProcessDetail.WarehouseCode = purchaseWarehouse.Code;
                        newPartsSalesOrderProcessDetail.WarehouseName = purchaseWarehouse.Name;
                        newPartsSalesOrderProcessDetail.SparePartId = detail.SparePartId;
                        newPartsSalesOrderProcessDetail.SparePartCode = detail.SparePartCode;
                        newPartsSalesOrderProcessDetail.SparePartName = detail.SparePartName;
                        newPartsSalesOrderProcessDetail.OrderedQuantity = detail.ApproveQuantity.HasValue ? detail.ApproveQuantity.Value : default(int);
                        newPartsSalesOrderProcessDetail.CurrentFulfilledQuantity = detail.ApproveQuantity.HasValue ? detail.ApproveQuantity.Value : default(int);
                        newPartsSalesOrderProcessDetail.OrderPrice = detail.OrderPrice;
                        newPartsSalesOrderProcessDetail.OrderProcessStatus = (int)DcsPartsSalesOrderProcessDetailOrderProcessStatus.满足;
                        newPartsSalesOrderProcessDetail.OrderProcessMethod = (int)DcsPartsSalesOrderProcessDetailProcessMethod.本部处理;
                        newPartsSalesOrderProcess.PartsSalesOrderProcessDetails.Add(newPartsSalesOrderProcessDetail);
                    }
                    InsertToDatabase(newPartsSalesOrderProcess);
                    partsSalesOrder.PartsSalesOrderProcesses.Add(newPartsSalesOrderProcess);

                    #endregion

                    customerAccount.First().ShippedProductValue += partsSalesOrder.TotalAmount;
                    UpdateToDatabase(customerAccount.First());
                    new CustomerAccountAch(this.DomainService).UpdateCustomerAccountValidate(customerAccount.First());

                    #region 生成出库计划单及清单，生成出库单及清单，生成配件发运单及清单
                    var newPartsOutboundPlan = new PartsOutboundPlan();
                    newPartsOutboundPlan.Id = i++;
                    newPartsOutboundPlan.WarehouseId = marketingProcessDetail.PurchaseWarehouseId.Value;
                    newPartsOutboundPlan.WarehouseCode = marketingProcessDetail.PurchaseWarehouseCode;
                    newPartsOutboundPlan.WarehouseName = marketingProcessDetail.PurchaseWarehouseName;
                    newPartsOutboundPlan.StorageCompanyId = purchaseWarehouse.StorageCompanyId;
                    newPartsOutboundPlan.StorageCompanyCode = purchaseWarehouse.Company.Code;
                    newPartsOutboundPlan.StorageCompanyName = purchaseWarehouse.Company.Name;
                    newPartsOutboundPlan.StorageCompanyType = (int)DcsCompanyType.分公司;
                    newPartsOutboundPlan.BranchId = partsSalesOrder.BranchId;
                    newPartsOutboundPlan.BranchCode = partsSalesOrder.BranchCode;
                    newPartsOutboundPlan.BranchName = partsSalesOrder.BranchName;
                    newPartsOutboundPlan.PartsSalesCategoryId = partsSalesOrder.SalesCategoryId;
                    newPartsOutboundPlan.PartsSalesOrderTypeId = partsSalesOrder.PartsSalesOrderTypeId;
                    newPartsOutboundPlan.PartsSalesOrderTypeName = partsSalesOrder.PartsSalesOrderTypeName;
                    newPartsOutboundPlan.CounterpartCompanyId = partsSalesOrder.InvoiceReceiveCompanyId;
                    newPartsOutboundPlan.CounterpartCompanyCode = partsSalesOrder.InvoiceReceiveCompanyCode;
                    newPartsOutboundPlan.CounterpartCompanyName = partsSalesOrder.InvoiceReceiveCompanyName;
                    newPartsOutboundPlan.ReceivingCompanyId = partsSalesOrder.InvoiceReceiveCompanyId;
                    newPartsOutboundPlan.ReceivingCompanyCode = partsSalesOrder.InvoiceReceiveCompanyCode;
                    newPartsOutboundPlan.ReceivingCompanyName = partsSalesOrder.InvoiceReceiveCompanyName;
                    newPartsOutboundPlan.ReceivingWarehouseId = marketingProcessDetail.WarehouseId;
                    newPartsOutboundPlan.ReceivingWarehouseCode = marketingProcessDetail.WarehouseCode;
                    newPartsOutboundPlan.ReceivingWarehouseName = marketingProcessDetail.WarehouseName;
                    newPartsOutboundPlan.SourceId = partsSalesOrder.Id;
                    newPartsOutboundPlan.SourceCode = partsSalesOrder.Code;
                    newPartsOutboundPlan.OutboundType = (int)DcsPartsOutboundType.配件销售;
                    newPartsOutboundPlan.ShippingMethod = shippingMethodKey;
                    newPartsOutboundPlan.CustomerAccountId = partsSalesOrder.CustomerAccountId;
                    newPartsOutboundPlan.OriginalRequirementBillId = partsSalesOrder.Id;
                    newPartsOutboundPlan.OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.配件销售订单;
                    newPartsOutboundPlan.OriginalRequirementBillCode = partsSalesOrder.Code;
                    newPartsOutboundPlan.Status = (int)DcsPartsOutboundPlanStatus.出库完成;
                    newPartsOutboundPlan.IfWmsInterface = false;
                    newPartsOutboundPlan.Remark = "统购分销出库";
                    new PartsOutboundPlanAch(this.DomainService).InsertPartsOutboundPlanValidate(newPartsOutboundPlan);
                    foreach(var detail in marketingProcessDetails) {
                        var newPartsOutboundPlanDetail = new PartsOutboundPlanDetail();
                        newPartsOutboundPlanDetail.PartsOutboundPlanId = newPartsOutboundPlan.Id;
                        newPartsOutboundPlanDetail.SparePartId = detail.SparePartId;
                        newPartsOutboundPlanDetail.SparePartCode = detail.SparePartCode;
                        newPartsOutboundPlanDetail.SparePartName = detail.SparePartName;
                        newPartsOutboundPlanDetail.PlannedAmount = detail.CurrentFulfilledQuantity;
                        newPartsOutboundPlanDetail.OutboundFulfillment = detail.CurrentFulfilledQuantity;
                        newPartsOutboundPlanDetail.Price = detail.OrderPrice;
                        newPartsOutboundPlanDetail.Remark = "统购分销出库";
                        newPartsOutboundPlan.PartsOutboundPlanDetails.Add(newPartsOutboundPlanDetail);
                    }
                    var newPartsOutboundBill = new PartsOutboundBill();
                    newPartsOutboundBill.Id = i++;
                    newPartsOutboundBill.PartsOutboundPlanId = newPartsOutboundPlan.Id;
                    newPartsOutboundBill.WarehouseId = newPartsOutboundPlan.WarehouseId;
                    newPartsOutboundBill.WarehouseCode = newPartsOutboundPlan.WarehouseCode;
                    newPartsOutboundBill.WarehouseName = newPartsOutboundPlan.WarehouseName;
                    newPartsOutboundBill.StorageCompanyId = newPartsOutboundPlan.StorageCompanyId;
                    newPartsOutboundBill.StorageCompanyCode = newPartsOutboundPlan.StorageCompanyCode;
                    newPartsOutboundBill.StorageCompanyName = newPartsOutboundPlan.StorageCompanyName;
                    newPartsOutboundBill.StorageCompanyType = newPartsOutboundPlan.StorageCompanyType;
                    newPartsOutboundBill.BranchId = newPartsOutboundPlan.BranchId;
                    newPartsOutboundBill.BranchCode = newPartsOutboundPlan.BranchCode;
                    newPartsOutboundBill.BranchName = newPartsOutboundPlan.BranchName;
                    newPartsOutboundBill.PartsSalesCategoryId = newPartsOutboundPlan.PartsSalesCategoryId;
                    newPartsOutboundBill.PartsSalesOrderTypeId = newPartsOutboundPlan.PartsSalesOrderTypeId;
                    newPartsOutboundBill.PartsSalesOrderTypeName = newPartsOutboundPlan.PartsSalesOrderTypeName;
                    newPartsOutboundBill.CounterpartCompanyId = newPartsOutboundPlan.CounterpartCompanyId;
                    newPartsOutboundBill.CounterpartCompanyCode = newPartsOutboundPlan.CounterpartCompanyCode;
                    newPartsOutboundBill.CounterpartCompanyName = newPartsOutboundPlan.CounterpartCompanyName;
                    newPartsOutboundBill.ReceivingCompanyId = newPartsOutboundPlan.ReceivingCompanyId;
                    newPartsOutboundBill.ReceivingCompanyCode = newPartsOutboundPlan.ReceivingCompanyCode;
                    newPartsOutboundBill.ReceivingCompanyName = newPartsOutboundPlan.ReceivingCompanyName;
                    newPartsOutboundBill.ReceivingWarehouseId = newPartsOutboundPlan.ReceivingWarehouseId;
                    newPartsOutboundBill.ReceivingWarehouseCode = newPartsOutboundPlan.ReceivingWarehouseCode;
                    newPartsOutboundBill.ReceivingWarehouseName = newPartsOutboundPlan.ReceivingWarehouseName;
                    newPartsOutboundBill.OutboundType = newPartsOutboundPlan.OutboundType;
                    newPartsOutboundBill.ShippingMethod = newPartsOutboundPlan.ShippingMethod;
                    newPartsOutboundBill.CustomerAccountId = newPartsOutboundPlan.CustomerAccountId;
                    newPartsOutboundBill.OriginalRequirementBillId = newPartsOutboundPlan.OriginalRequirementBillId;
                    newPartsOutboundBill.OriginalRequirementBillType = newPartsOutboundPlan.OriginalRequirementBillType;
                    newPartsOutboundBill.OriginalRequirementBillCode = newPartsOutboundPlan.OriginalRequirementBillCode;
                    newPartsOutboundBill.SettlementStatus = (int)DcsPartsSettlementStatus.待结算;
                    newPartsOutboundBill.Remark = "统购分销出库";
                    new PartsOutboundBillAch(this.DomainService).InsertPartsOutboundBillValidate(newPartsOutboundBill);
                    var outpartIds = newPartsOutboundPlan.PartsOutboundPlanDetails.Select(r => r.SparePartId).ToArray();
                    var dbPartsStocks = new PartsStockAch(this.DomainService).GetPartsStocksWithLock(ObjectContext.PartsStocks.Include("WarehouseArea").Where(r => r.WarehouseId == newPartsOutboundBill.WarehouseId && outpartIds.Contains(r.PartId) && r.WarehouseAreaCategory.Category == (int)DcsAreaType.保管区)).ToList();
                    var dbPartsPlannedPrices = ObjectContext.PartsPlannedPrices.Where(r => r.OwnerCompanyId == newPartsOutboundBill.StorageCompanyId && r.PartsSalesCategoryId == newPartsOutboundBill.PartsSalesCategoryId && outpartIds.Contains(r.SparePartId)).ToList();
                    foreach(var detail in newPartsOutboundPlan.PartsOutboundPlanDetails) {
                        var partsStock = dbPartsStocks.Where(r => r.PartId == detail.SparePartId).OrderByDescending(r => r.Quantity);
                        if(!partsStock.Any())
                            throw new ValidationException(string.Format(ErrorStrings.PartsSalesOrderProcess_Validation45, detail.SparePartCode));
                        if(partsStock.Sum(r => r.Quantity) < detail.OutboundFulfillment)
                            throw new ValidationException(string.Format(ErrorStrings.PartsTransferOrder_Validation13, detail.SparePartCode));
                        var partsPlannedPrice = dbPartsPlannedPrices.FirstOrDefault(r => r.SparePartId == detail.SparePartId);
                        foreach(var stock in partsStock.ToList()) {
                            var newPartsOutboundBillDetail = new PartsOutboundBillDetail();
                            if(stock.Quantity >= detail.OutboundFulfillment) {
                                stock.Quantity -= detail.OutboundFulfillment.Value;
                                newPartsOutboundBillDetail.PartsOutboundBillId = newPartsOutboundBill.Id;
                                newPartsOutboundBillDetail.SparePartId = detail.SparePartId;
                                newPartsOutboundBillDetail.SparePartCode = detail.SparePartCode;
                                newPartsOutboundBillDetail.SparePartName = detail.SparePartName;
                                newPartsOutboundBillDetail.OutboundAmount = detail.OutboundFulfillment.Value;
                                newPartsOutboundBillDetail.WarehouseAreaId = stock.WarehouseAreaId;
                                newPartsOutboundBillDetail.WarehouseAreaCode = stock.WarehouseArea.Code;
                                newPartsOutboundBillDetail.SettlementPrice = detail.Price;
                                newPartsOutboundBillDetail.CostPrice = partsPlannedPrice != null ? partsPlannedPrice.PlannedPrice : default(decimal);
                                newPartsOutboundBill.PartsOutboundBillDetails.Add(newPartsOutboundBillDetail);
                                UpdateToDatabase(stock);
                                new PartsStockAch(this.DomainService).UpdatePartsStockValidate(stock);
                                break;
                            }
                            detail.OutboundFulfillment -= stock.Quantity;
                            newPartsOutboundBillDetail.PartsOutboundBillId = newPartsOutboundBill.Id;
                            newPartsOutboundBillDetail.SparePartId = detail.SparePartId;
                            newPartsOutboundBillDetail.SparePartCode = detail.SparePartCode;
                            newPartsOutboundBillDetail.SparePartName = detail.SparePartName;
                            newPartsOutboundBillDetail.OutboundAmount = stock.Quantity;
                            newPartsOutboundBillDetail.WarehouseAreaId = stock.WarehouseAreaId;
                            newPartsOutboundBillDetail.WarehouseAreaCode = stock.WarehouseArea.Code;
                            newPartsOutboundBillDetail.SettlementPrice = detail.Price;
                            newPartsOutboundBillDetail.CostPrice = partsPlannedPrice != null ? partsPlannedPrice.PlannedPrice : default(decimal);
                            newPartsOutboundBill.PartsOutboundBillDetails.Add(newPartsOutboundBillDetail);
                            stock.Quantity = 0;
                            UpdateToDatabase(stock);
                            new PartsStockAch(this.DomainService).UpdatePartsStockValidate(stock);
                        }
                    }
                    var newPartsShippingOrder = new PartsShippingOrder();
                    newPartsShippingOrder.Id = i++;
                    newPartsShippingOrder.Type = (int)DcsPartsShippingOrderType.销售;
                    newPartsShippingOrder.PartsSalesCategoryId = partsSalesCategory.Id;
                    newPartsShippingOrder.BranchId = newPartsOutboundBill.BranchId;
                    newPartsShippingOrder.ShippingCompanyId = newPartsOutboundBill.StorageCompanyId;
                    newPartsShippingOrder.ShippingCompanyCode = newPartsOutboundBill.StorageCompanyCode;
                    newPartsShippingOrder.ShippingCompanyName = newPartsOutboundBill.StorageCompanyName;
                    newPartsShippingOrder.SettlementCompanyId = newPartsOutboundBill.StorageCompanyId;
                    newPartsShippingOrder.SettlementCompanyCode = newPartsOutboundBill.StorageCompanyCode;
                    newPartsShippingOrder.SettlementCompanyName = newPartsOutboundBill.StorageCompanyName;
                    newPartsShippingOrder.ReceivingCompanyId = newPartsOutboundBill.ReceivingCompanyId;
                    newPartsShippingOrder.ReceivingCompanyCode = newPartsOutboundBill.ReceivingCompanyCode;
                    newPartsShippingOrder.ReceivingCompanyName = newPartsOutboundBill.ReceivingCompanyName;
                    newPartsShippingOrder.InvoiceReceiveSaleCateId = newPartsOutboundBill.PartsSalesOrderTypeId;
                    newPartsShippingOrder.InvoiceReceiveSaleCateName = newPartsOutboundBill.PartsSalesOrderTypeName;
                    newPartsShippingOrder.WarehouseId = newPartsOutboundBill.WarehouseId;
                    newPartsShippingOrder.WarehouseCode = newPartsOutboundBill.WarehouseCode;
                    newPartsShippingOrder.WarehouseName = newPartsOutboundBill.WarehouseName;
                    newPartsShippingOrder.ReceivingWarehouseId = newPartsOutboundBill.ReceivingWarehouseId;
                    newPartsShippingOrder.ReceivingWarehouseCode = newPartsOutboundBill.ReceivingWarehouseCode;
                    newPartsShippingOrder.ReceivingWarehouseName = newPartsOutboundBill.ReceivingWarehouseName;
                    newPartsShippingOrder.OriginalRequirementBillId = newPartsOutboundBill.OriginalRequirementBillId;
                    newPartsShippingOrder.OriginalRequirementBillType = newPartsOutboundBill.OriginalRequirementBillType;
                    newPartsShippingOrder.OriginalRequirementBillCode = newPartsOutboundBill.OriginalRequirementBillCode;
                    newPartsShippingOrder.ShippingMethod = shippingMethodKey;
                    newPartsShippingOrder.Status = (int)DcsPartsShippingOrderStatus.回执确认;
                    newPartsShippingOrder.Remark = "统购分销发运";
                    DomainService.InsertPartsShippingOrderValidate(newPartsShippingOrder);
                    foreach(var detail in newPartsOutboundBill.PartsOutboundBillDetails) {
                        var newPartsShippingOrderDetail = new PartsShippingOrderDetail();
                        newPartsShippingOrderDetail.PartsShippingOrderId = newPartsShippingOrder.Id;
                        newPartsShippingOrderDetail.SparePartId = detail.SparePartId;
                        newPartsShippingOrderDetail.SparePartCode = detail.SparePartCode;
                        newPartsShippingOrderDetail.SparePartName = detail.SparePartName;
                        newPartsShippingOrderDetail.ShippingAmount = detail.OutboundAmount;
                        newPartsShippingOrderDetail.ConfirmedAmount = detail.OutboundAmount;
                        newPartsShippingOrderDetail.SettlementPrice = detail.SettlementPrice;
                        newPartsShippingOrder.PartsShippingOrderDetails.Add(newPartsShippingOrderDetail);
                    }
                    var newPartsShippingOrderRef = new PartsShippingOrderRef {
                        PartsOutboundBillId = newPartsOutboundBill.Id,
                        PartsShippingOrderId = newPartsShippingOrder.Id
                    };
                    newPartsShippingOrder.PartsShippingOrderRefs.Add(newPartsShippingOrderRef);
                    InsertToDatabase(newPartsShippingOrder);
                    newPartsOutboundBill.PartsShippingOrderRefs.Add(newPartsShippingOrderRef);
                    InsertToDatabase(newPartsOutboundBill);
                    newPartsOutboundPlan.PartsOutboundBills.Add(newPartsOutboundBill);
                    InsertToDatabase(newPartsOutboundPlan);
                    partsSalesOrder.PartsOutboundPlans.Add(newPartsOutboundPlan);
                    InsertToDatabase(partsSalesOrder);
                    #endregion

                    #region 生成本品牌采购订单及清单
                    var outsalesUnit = dbSalesUnitAffiWarehouses.Where(r => r.WarehouseId == marketingProcessDetail.WarehouseId).Select(r => r.SalesUnit).FirstOrDefault(r => r.Status == (int)DcsBaseDataStatus.有效);
                    if(outsalesUnit == null)
                        throw new ValidationException(ErrorStrings.PartsSalesOrderProcess_Validation46);
                    var outpartsSalesCategory = dbPartsSalesCategories.FirstOrDefault(r => r.Id == outsalesUnit.PartsSalesCategoryId);
                    if(outpartsSalesCategory == null)
                        throw new ValidationException(ErrorStrings.PartsSalesOrderProcess_Validation47);
                    var outcompany = dbCompanies.FirstOrDefault(r => r.Id == outsalesUnit.OwnerCompanyId);
                    if(outcompany == null)
                        throw new ValidationException(ErrorStrings.PartsSalesOrderProcess_Validation37);
                    var supplierstorageCompany = dbCompanies.FirstOrDefault(r => r.Id == purchaseWarehouse.StorageCompanyId);
                    if(supplierstorageCompany == null)
                        throw new ValidationException(ErrorStrings.PartsSalesOrderProcess_Validation48);
                    var storageCompany = dbCompanies.FirstOrDefault(r => r.Id == warehouse.StorageCompanyId);
                    if(storageCompany == null)
                        throw new ValidationException(ErrorStrings.PartsSalesOrderProcess_Validation49);
                    var outPartsPurchaseOrderType = ObjectContext.PartsPurchaseOrderTypes.FirstOrDefault(r => r.PartsSalesCategoryId == outpartsSalesCategory.Id && r.Name == "急需采购订单");
                    if(outPartsPurchaseOrderType == null)
                        throw new ValidationException(ErrorStrings.PartsSalesOrderProcess_Validation50);
                    var newPartsPurchaseOrder = new PartsPurchaseOrder();
                    newPartsPurchaseOrder.Id = i++;
                    newPartsPurchaseOrder.BranchId = warehouse.BranchId;
                    newPartsPurchaseOrder.BranchCode = warehouse.Branch.Code;
                    newPartsPurchaseOrder.BranchName = warehouse.Branch.Name;
                    newPartsPurchaseOrder.WarehouseId = marketingProcessDetail.WarehouseId.Value;
                    newPartsPurchaseOrder.WarehouseName = marketingProcessDetail.WarehouseName;
                    newPartsPurchaseOrder.PartsSalesCategoryId = dbpartsSalesOrder.SalesCategoryId;
                    newPartsPurchaseOrder.PartsSalesCategoryName = dbpartsSalesOrder.SalesCategoryName;
                    newPartsPurchaseOrder.ReceivingCompanyId = outcompany.Id;
                    newPartsPurchaseOrder.ReceivingCompanyName = outcompany.Name;
                    newPartsPurchaseOrder.PartsSupplierId = purchaseWarehouse.StorageCompanyId;
                    newPartsPurchaseOrder.PartsSupplierCode = supplierstorageCompany.Code;
                    newPartsPurchaseOrder.PartsSupplierName = supplierstorageCompany.Name;
                    newPartsPurchaseOrder.OriginalRequirementBillId = dbpartsSalesOrder.Id;
                    newPartsPurchaseOrder.OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.配件销售订单;
                    newPartsPurchaseOrder.OriginalRequirementBillCode = dbpartsSalesOrder.Code;
                    //newPartsPurchaseOrder.TotalAmount = newPartsOutboundPlan.PartsOutboundPlanDetails.Sum(r => r.PlannedAmount * r.Price);
                    newPartsPurchaseOrder.PartsPurchaseOrderTypeId = outPartsPurchaseOrderType.Id;
                    newPartsPurchaseOrder.RequestedDeliveryTime = DateTime.Now;
                    newPartsPurchaseOrder.Status = (int)DcsPartsPurchaseOrderStatus.发运完毕;
                    newPartsPurchaseOrder.Remark = "统购分销采购";
                    InsertToDatabase(newPartsPurchaseOrder);
                    new PartsPurchaseOrderAch(this.DomainService).InsertPartsPurchaseOrderValidate(newPartsPurchaseOrder);
                    foreach(var detail in marketingProcessDetails) {
                        var partsSpecialTreatyPrice = dbPartsSpecialTreatyPrices.FirstOrDefault(r => r.SparePartId == detail.SparePartId);
                        var partsSalesPrice = dbPartsSalesPrices.FirstOrDefault(r => r.SparePartId == detail.SparePartId);
                        if(partsSpecialTreatyPrice == null && partsSalesPrice == null) {
                            throw new ValidationException(ErrorStrings.PartsSalesOrderProcess_Validation42);
                        }
                        var newPartsPurchaseOrderDetail = new PartsPurchaseOrderDetail();
                        newPartsPurchaseOrderDetail.PartsPurchaseOrderId = newPartsPurchaseOrder.Id;
                        newPartsPurchaseOrderDetail.SparePartId = detail.SparePartId;
                        newPartsPurchaseOrderDetail.SparePartCode = detail.SparePartCode;
                        newPartsPurchaseOrderDetail.SparePartName = detail.SparePartName;
                        newPartsPurchaseOrderDetail.UnitPrice = partsSpecialTreatyPrice != null ? partsSpecialTreatyPrice.TreatyPrice : partsSalesPrice.SalesPrice;
                        newPartsPurchaseOrderDetail.OrderAmount = detail.CurrentFulfilledQuantity;
                        newPartsPurchaseOrderDetail.ConfirmedAmount = detail.CurrentFulfilledQuantity;
                        newPartsPurchaseOrderDetail.ShippingAmount = detail.CurrentFulfilledQuantity;
                        newPartsPurchaseOrder.PartsPurchaseOrderDetails.Add(newPartsPurchaseOrderDetail);
                    }
                    newPartsPurchaseOrder.TotalAmount = newPartsPurchaseOrder.PartsPurchaseOrderDetails.Sum(r => r.OrderAmount * r.UnitPrice);
                    #endregion

                    #region 生成供应商发运单及清单
                    var newSupplierShippingOrder = new SupplierShippingOrder();
                    newSupplierShippingOrder.Id = i++;
                    newSupplierShippingOrder.PartsPurchaseOrderId = newPartsPurchaseOrder.Id;
                    newSupplierShippingOrder.PartsSupplierId = newPartsPurchaseOrder.PartsSupplierId;
                    newSupplierShippingOrder.PartsSupplierCode = newPartsPurchaseOrder.PartsSupplierCode;
                    newSupplierShippingOrder.PartsSupplierName = newPartsPurchaseOrder.PartsSupplierName;
                    newSupplierShippingOrder.IfDirectProvision = false;
                    newSupplierShippingOrder.BranchId = newPartsPurchaseOrder.BranchId;
                    newSupplierShippingOrder.BranchCode = newPartsPurchaseOrder.BranchCode;
                    newSupplierShippingOrder.BranchName = newPartsPurchaseOrder.BranchName;
                    newSupplierShippingOrder.PartsSalesCategoryId = newPartsPurchaseOrder.PartsSalesCategoryId;
                    newSupplierShippingOrder.PartsSalesCategoryName = newPartsPurchaseOrder.PartsSalesCategoryName;
                    newSupplierShippingOrder.PartsPurchaseOrderCode = newPartsPurchaseOrder.Code;
                    newSupplierShippingOrder.OriginalRequirementBillId = dbpartsSalesOrder.OriginalRequirementBillId.HasValue ? dbpartsSalesOrder.OriginalRequirementBillId.Value : dbpartsSalesOrder.Id;
                    newSupplierShippingOrder.OriginalRequirementBillType = dbpartsSalesOrder.OriginalRequirementBillType.HasValue ? dbpartsSalesOrder.OriginalRequirementBillType.Value : (int)DcsOriginalRequirementBillType.配件销售订单;
                    newSupplierShippingOrder.OriginalRequirementBillCode = dbpartsSalesOrder.OriginalRequirementBillId.HasValue ? "" : dbpartsSalesOrder.Code;
                    newSupplierShippingOrder.ReceivingWarehouseId = newPartsPurchaseOrder.WarehouseId;
                    newSupplierShippingOrder.ReceivingWarehouseName = newPartsPurchaseOrder.WarehouseName;
                    newSupplierShippingOrder.ReceivingCompanyId = newPartsPurchaseOrder.ReceivingCompanyId;
                    newSupplierShippingOrder.ReceivingCompanyName = newPartsPurchaseOrder.ReceivingCompanyName;
                    newSupplierShippingOrder.ReceivingCompanyCode = outcompany.Code;
                    newSupplierShippingOrder.ReceivingAddress = newPartsPurchaseOrder.ReceivingAddress;
                    newSupplierShippingOrder.ShippingMethod = shippingMethodKey;
                    newSupplierShippingOrder.Status = (int)DcsSupplierShippingOrderStatus.收货确认;
                    newSupplierShippingOrder.RequestedDeliveryTime = newPartsPurchaseOrder.RequestedDeliveryTime;
                    newSupplierShippingOrder.PlanDeliveryTime = newPartsPurchaseOrder.RequestedDeliveryTime;
                    newSupplierShippingOrder.Remark = "统购分销供应商发运单";
                    InsertToDatabase(newSupplierShippingOrder);
                    new SupplierShippingOrderAch(this.DomainService).InsertSupplierShippingOrderValidate(newSupplierShippingOrder);
                    foreach(var detail in newPartsPurchaseOrder.PartsPurchaseOrderDetails) {
                        var newSupplierShippingDetail = new SupplierShippingDetail();
                        newSupplierShippingDetail.SupplierShippingOrderId = newSupplierShippingOrder.Id;
                        newSupplierShippingDetail.SparePartId = detail.SparePartId;
                        newSupplierShippingDetail.SparePartCode = detail.SparePartCode;
                        newSupplierShippingDetail.SparePartName = detail.SparePartName;
                        newSupplierShippingDetail.Quantity = detail.OrderAmount;
                        newSupplierShippingDetail.ConfirmedAmount = detail.OrderAmount;
                        newSupplierShippingDetail.UnitPrice = detail.UnitPrice;
                        newSupplierShippingOrder.SupplierShippingDetails.Add(newSupplierShippingDetail);
                    }
                    newPartsPurchaseOrder.SupplierShippingOrders.Add(newSupplierShippingOrder);
                    #endregion

                    #region 新增配件物流批次，单据清单，配件清单
                    var newPartsLogisticBatch = new PartsLogisticBatch();
                    newPartsLogisticBatch.Id = i++;
                    newPartsLogisticBatch.CompanyId = newSupplierShippingOrder.BranchId;
                    newPartsLogisticBatch.Status = (int)DcsPartsLogisticBatchStatus.完成;
                    newPartsLogisticBatch.ShippingStatus = (int)DcsPartsLogisticBatchShippingStatus.供应商发运;
                    newPartsLogisticBatch.SourceId = newPartsOutboundPlan.Id;
                    newPartsLogisticBatch.SourceType = (int)DcsPartsLogisticBatchSourceType.配件采购订单;
                    newPartsOutboundPlan.PartsLogisticBatches.Add(newPartsLogisticBatch);
                    InsertToDatabase(newPartsLogisticBatch);
                    new PartsLogisticBatchAch(this.DomainService).InsertPartsLogisticBatchValidate(newPartsLogisticBatch);
                    var partsLogisticBatchBillDetail = new PartsLogisticBatchBillDetail {
                        PartsLogisticBatchId = newPartsLogisticBatch.Id,
                        BillId = newSupplierShippingOrder.Id,
                        BillType = (int)DcsPartsLogisticBatchBillDetailBillType.供应商发运单,
                        CreatorId = newSupplierShippingOrder.CreatorId,
                        CreatorName = newSupplierShippingOrder.CreatorName,
                        CreateTime = newSupplierShippingOrder.CreateTime
                    };
                    newSupplierShippingOrder.PartsLogisticBatchBillDetails.Add(partsLogisticBatchBillDetail);
                    var partsLogisticBatchBillDetail1 = new PartsLogisticBatchBillDetail {
                        PartsLogisticBatchId = newPartsLogisticBatch.Id,
                        BillId = newPartsOutboundBill.Id,
                        BillType = (int)DcsPartsLogisticBatchBillDetailBillType.配件出库单,
                        CreatorId = newPartsOutboundBill.CreatorId,
                        CreatorName = newPartsOutboundBill.CreatorName,
                        CreateTime = newPartsOutboundBill.CreateTime
                    };
                    newPartsOutboundBill.PartsLogisticBatchBillDetails.Add(partsLogisticBatchBillDetail1);
                    newPartsLogisticBatch.PartsLogisticBatchBillDetails.Add(partsLogisticBatchBillDetail);
                    newPartsLogisticBatch.PartsLogisticBatchBillDetails.Add(partsLogisticBatchBillDetail1);
                    foreach(var detail in newSupplierShippingOrder.SupplierShippingDetails) {
                        var newPartsLogisticBatchItemDetail = new PartsLogisticBatchItemDetail();
                        newPartsLogisticBatchItemDetail.PartsLogisticBatchId = newPartsLogisticBatch.Id;
                        newPartsLogisticBatchItemDetail.CounterpartCompanyId = newSupplierShippingOrder.PartsSupplierId;
                        newPartsLogisticBatchItemDetail.ReceivingCompanyId = newSupplierShippingOrder.ReceivingCompanyId.Value;
                        newPartsLogisticBatchItemDetail.ShippingCompanyId = newSupplierShippingOrder.PartsSupplierId;
                        newPartsLogisticBatchItemDetail.SparePartId = detail.SparePartId;
                        newPartsLogisticBatchItemDetail.SparePartCode = detail.SparePartCode;
                        newPartsLogisticBatchItemDetail.SparePartName = detail.SparePartName;
                        newPartsLogisticBatchItemDetail.OutboundAmount = detail.ConfirmedAmount.Value;
                        newPartsLogisticBatchItemDetail.InboundAmount = detail.ConfirmedAmount;
                        newPartsLogisticBatch.PartsLogisticBatchItemDetails.Add(newPartsLogisticBatchItemDetail);
                    }
                    #endregion

                    #region 生成入库计划单及清单，生成配件入库检验单及清单
                    var newPartsInboundPlan = new PartsInboundPlan();
                    newPartsInboundPlan.Id = i++;
                    newPartsInboundPlan.WarehouseId = marketingProcessDetail.WarehouseId.Value;
                    newPartsInboundPlan.WarehouseCode = marketingProcessDetail.WarehouseCode;
                    newPartsInboundPlan.WarehouseName = marketingProcessDetail.WarehouseName;
                    newPartsInboundPlan.StorageCompanyId = warehouse.StorageCompanyId;
                    newPartsInboundPlan.StorageCompanyCode = storageCompany.Code;
                    newPartsInboundPlan.StorageCompanyName = storageCompany.Name;
                    newPartsInboundPlan.StorageCompanyType = storageCompany.Type;
                    newPartsInboundPlan.BranchId = newPartsPurchaseOrder.BranchId;
                    newPartsInboundPlan.BranchCode = newPartsPurchaseOrder.BranchCode;
                    newPartsInboundPlan.BranchName = newPartsPurchaseOrder.BranchName;
                    newPartsInboundPlan.PartsSalesCategoryId = newPartsPurchaseOrder.PartsSalesCategoryId;
                    newPartsInboundPlan.CounterpartCompanyId = newPartsPurchaseOrder.PartsSupplierId;
                    newPartsInboundPlan.CounterpartCompanyCode = newPartsPurchaseOrder.PartsSupplierCode;
                    newPartsInboundPlan.CounterpartCompanyName = newPartsPurchaseOrder.PartsSupplierName;
                    newPartsInboundPlan.SourceId = newSupplierShippingOrder.Id;
                    newPartsInboundPlan.SourceCode = newSupplierShippingOrder.Code;
                    newPartsInboundPlan.InboundType = (int)DcsPartsInboundType.配件采购;
                    newPartsInboundPlan.OriginalRequirementBillId = newSupplierShippingOrder.OriginalRequirementBillId;
                    newPartsInboundPlan.OriginalRequirementBillType = newSupplierShippingOrder.OriginalRequirementBillType;
                    newPartsInboundPlan.OriginalRequirementBillCode = newSupplierShippingOrder.OriginalRequirementBillCode;
                    newPartsInboundPlan.Status = (int)DcsPartsInboundPlanStatus.检验完成;
                    newPartsInboundPlan.IfWmsInterface = false;
                    newPartsInboundPlan.Remark = "统购分销入库计划";
                    newSupplierShippingOrder.PartsInboundPlans.Add(newPartsInboundPlan);
                    InsertToDatabase(newPartsInboundPlan);
                    new PartsInboundPlanAch(this.DomainService).InsertPartsInboundPlanValidate(newPartsInboundPlan);
                    foreach(var detail in newPartsPurchaseOrder.PartsPurchaseOrderDetails) {
                        var newPartsInboundPlanDetail = new PartsInboundPlanDetail();
                        newPartsInboundPlanDetail.PartsInboundPlanId = newPartsInboundPlan.Id;
                        newPartsInboundPlanDetail.SparePartId = detail.SparePartId;
                        newPartsInboundPlanDetail.SparePartCode = detail.SparePartCode;
                        newPartsInboundPlanDetail.SparePartName = detail.SparePartName;
                        newPartsInboundPlanDetail.PlannedAmount = detail.OrderAmount;
                        newPartsInboundPlanDetail.InspectedQuantity = detail.OrderAmount;
                        newPartsInboundPlanDetail.Price = detail.UnitPrice;
                        newPartsInboundPlan.PartsInboundPlanDetails.Add(newPartsInboundPlanDetail);
                    }
                    var newPartsInboundCheckBill = new PartsInboundCheckBill();
                    newPartsInboundCheckBill.Id = i++;
                    newPartsInboundCheckBill.PartsInboundPlanId = newPartsInboundPlan.Id;
                    newPartsInboundCheckBill.WarehouseId = newPartsInboundPlan.WarehouseId;
                    newPartsInboundCheckBill.WarehouseCode = newPartsInboundPlan.WarehouseCode;
                    newPartsInboundCheckBill.WarehouseName = newPartsInboundPlan.WarehouseName;
                    newPartsInboundCheckBill.StorageCompanyId = newPartsInboundPlan.StorageCompanyId;
                    newPartsInboundCheckBill.StorageCompanyCode = newPartsInboundPlan.StorageCompanyCode;
                    newPartsInboundCheckBill.StorageCompanyName = newPartsInboundPlan.StorageCompanyName;
                    newPartsInboundCheckBill.StorageCompanyType = newPartsInboundPlan.StorageCompanyType;
                    newPartsInboundCheckBill.PartsSalesCategoryId = newPartsInboundPlan.PartsSalesCategoryId;
                    newPartsInboundCheckBill.BranchId = newPartsInboundPlan.BranchId;
                    newPartsInboundCheckBill.BranchCode = newPartsInboundPlan.BranchCode;
                    newPartsInboundCheckBill.BranchName = newPartsInboundPlan.BranchName;
                    newPartsInboundCheckBill.CounterpartCompanyId = newPartsInboundPlan.CounterpartCompanyId;
                    newPartsInboundCheckBill.CounterpartCompanyCode = newPartsInboundPlan.CounterpartCompanyCode;
                    newPartsInboundCheckBill.CounterpartCompanyName = newPartsInboundPlan.CounterpartCompanyName;
                    newPartsInboundCheckBill.InboundType = (int)DcsPartsInboundType.配件采购;
                    newPartsInboundCheckBill.OriginalRequirementBillId = newPartsInboundPlan.OriginalRequirementBillId;
                    newPartsInboundCheckBill.OriginalRequirementBillType = newPartsInboundPlan.OriginalRequirementBillType;
                    newPartsInboundCheckBill.OriginalRequirementBillCode = newPartsInboundPlan.OriginalRequirementBillCode;
                    newPartsInboundCheckBill.Status = (int)DcsPartsInboundCheckBillStatus.上架完成;
                    newPartsInboundCheckBill.SettlementStatus = (int)DcsPartsSettlementStatus.待结算;
                    newPartsInboundPlan.PartsInboundCheckBills.Add(newPartsInboundCheckBill);
                    InsertToDatabase(newPartsInboundCheckBill);
                    new PartsInboundCheckBillAch(this.DomainService).InsertPartsInboundCheckBillValidate(newPartsInboundCheckBill);
                    var inpartIds = newPartsInboundPlan.PartsInboundPlanDetails.Select(r => r.SparePartId).ToArray();
                    var inPartsStocks = new PartsStockAch(this.DomainService).GetPartsStocksWithLock(ObjectContext.PartsStocks.Include("WarehouseArea").Where(r => r.WarehouseId == newPartsInboundPlan.WarehouseId && inpartIds.Contains(r.PartId) && r.WarehouseAreaCategory.Category == (int)DcsAreaType.保管区)).ToList();
                    var warehouseAreaIds = inPartsStocks.Select(r => r.WarehouseAreaId).ToArray();
                    var dbWarehouseAreas = ObjectContext.WarehouseAreas.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && warehouseAreaIds.Contains(r.Id) || (r.WarehouseId == newPartsInboundCheckBill.WarehouseId && r.WarehouseAreaCategory.Category == (int)DcsAreaType.保管区 && r.AreaKind == (int)DcsAreaKind.库位));
                    var inWarehouseArea = dbWarehouseAreas.FirstOrDefault(r => r.WarehouseId == newPartsInboundCheckBill.WarehouseId && r.WarehouseAreaCategory.Category == (int)DcsAreaType.保管区 && r.AreaKind == (int)DcsAreaKind.库位);
                    if(inWarehouseArea == null)
                        throw new ValidationException(string.Format(ErrorStrings.PartsSalesOrderProcess_Validation51, newPartsInboundCheckBill.WarehouseCode));
                    var inPartsPlannedPrices = ObjectContext.PartsPlannedPrices.Where(r => r.OwnerCompanyId == newPartsInboundCheckBill.StorageCompanyId && r.PartsSalesCategoryId == newPartsInboundCheckBill.PartsSalesCategoryId && outpartIds.Contains(r.SparePartId)).ToList();
                    foreach(var detail in newPartsInboundPlan.PartsInboundPlanDetails) {
                        var warehouseAreaId = 0;
                        var warehouseAreaCode = "";
                        if(inPartsStocks.All(r => r.PartId != detail.SparePartId)) {
                            var newPartsStock = new PartsStock();
                            newPartsStock.WarehouseId = newPartsInboundCheckBill.WarehouseId;
                            newPartsStock.StorageCompanyId = newPartsInboundCheckBill.StorageCompanyId;
                            newPartsStock.StorageCompanyType = newPartsInboundCheckBill.StorageCompanyType;
                            newPartsStock.BranchId = newPartsInboundCheckBill.BranchId;
                            newPartsStock.WarehouseAreaId = inWarehouseArea.Id;
                            newPartsStock.WarehouseAreaCategoryId = inWarehouseArea.AreaCategoryId;
                            newPartsStock.PartId = detail.SparePartId;
                            newPartsStock.Quantity = detail.InspectedQuantity.HasValue ? detail.InspectedQuantity.Value : default(int);
                            warehouseAreaId = inWarehouseArea.Id;
                            warehouseAreaCode = inWarehouseArea.Code;
                            InsertToDatabase(newPartsStock);
                            new PartsStockAch(this.DomainService).InsertPartsStockValidate(newPartsStock);
                        } else {
                            var inPartsStock = inPartsStocks.First(r => r.PartId == detail.SparePartId);
                            inPartsStock.Quantity += detail.InspectedQuantity.HasValue ? detail.InspectedQuantity.Value : default(int);
                            warehouseAreaId = inPartsStock.WarehouseAreaId;
                            warehouseAreaCode = dbWarehouseAreas.First(r => r.Id == inPartsStock.WarehouseAreaId).Code;
                            UpdateToDatabase(inPartsStock);
                            new PartsStockAch(this.DomainService).UpdatePartsStockValidate(inPartsStock);
                        }
                        var costPrice = inPartsPlannedPrices.FirstOrDefault(r => r.SparePartId == detail.SparePartId);
                        var newPartsInboundCheckBillDetail = new PartsInboundCheckBillDetail();
                        newPartsInboundCheckBillDetail.PartsInboundCheckBillId = newPartsInboundCheckBill.Id;
                        newPartsInboundCheckBillDetail.SparePartId = detail.SparePartId;
                        newPartsInboundCheckBillDetail.SparePartCode = detail.SparePartCode;
                        newPartsInboundCheckBillDetail.SparePartName = detail.SparePartName;
                        newPartsInboundCheckBillDetail.WarehouseAreaId = warehouseAreaId;
                        newPartsInboundCheckBillDetail.WarehouseAreaCode = warehouseAreaCode;
                        newPartsInboundCheckBillDetail.InspectedQuantity = detail.InspectedQuantity.HasValue ? detail.InspectedQuantity.Value : default(int);
                        newPartsInboundCheckBillDetail.SettlementPrice = detail.Price;
                        newPartsInboundCheckBillDetail.CostPrice = costPrice != null ? costPrice.PlannedPrice : default(decimal);
                        newPartsInboundCheckBillDetail.POCode = detail.POCode;
                        newPartsInboundCheckBill.PartsInboundCheckBillDetails.Add(newPartsInboundCheckBillDetail);
                    }
                    var partsLogisticBatchBillDetail2 = new PartsLogisticBatchBillDetail {
                        PartsLogisticBatchId = newPartsLogisticBatch.Id,
                        BillId = newPartsInboundPlan.Id,
                        BillType = (int)DcsPartsLogisticBatchBillDetailBillType.配件入库计划,
                        CreatorId = newPartsInboundPlan.CreatorId,
                        CreatorName = newPartsInboundPlan.CreatorName,
                        CreateTime = newPartsInboundPlan.CreateTime
                    };
                    newPartsInboundPlan.PartsLogisticBatchBillDetails.Add(partsLogisticBatchBillDetail2);
                    newPartsLogisticBatch.PartsLogisticBatchBillDetails.Add(partsLogisticBatchBillDetail2);
                    #endregion
                }
                //财务主体相同
                if(warehouse.BranchId == purchaseWarehouse.BranchId) {
                    var salesUnit = dbSalesUnitAffiWarehouses.Where(r => r.WarehouseId == marketingProcessDetail.PurchaseWarehouseId).Select(r => r.SalesUnit).FirstOrDefault(r => r.Status == (int)DcsBaseDataStatus.有效);
                    if(salesUnit == null)
                        throw new ValidationException(ErrorStrings.PartsSalesOrderProcess_Validation35);
                    var partsPlannedPrices = ObjectContext.PartsPlannedPrices.Where(r => partIds.Contains(r.SparePartId)).Where(r => r.PartsSalesCategoryId == dbpartsSalesOrder.SalesCategoryId || r.PartsSalesCategoryId == salesUnit.PartsSalesCategoryId).ToList();
                    foreach(var detail in marketingProcessDetails) {
                        var partPrice1 = partsPlannedPrices.FirstOrDefault(r => r.SparePartId == detail.SparePartId && r.PartsSalesCategoryId == dbpartsSalesOrder.SalesCategoryId);
                        var partPrice2 = partsPlannedPrices.FirstOrDefault(r => r.SparePartId == detail.SparePartId && r.PartsSalesCategoryId == salesUnit.PartsSalesCategoryId);
                        if(partPrice1 == null)
                            throw new ValidationException("订单品牌计划价不存在");
                        if(partPrice2 == null)
                            throw new ValidationException("统购出库仓库计划价不存在");
                        if(partPrice1.PlannedPrice != partPrice2.PlannedPrice)
                            throw new ValidationException("计划价不相同，不允许生成调拨单");
                    }

                    var dbPartsSalesPrices = ObjectContext.PartsSalesPrices.Where(r => r.PartsSalesCategoryId == dbpartsSalesOrder.SalesCategoryId && partIds.Contains(r.SparePartId)).ToList();

                    var dbpartsPlannedPrices = ObjectContext.PartsPlannedPrices.Where(r => r.PartsSalesCategoryId == dbpartsSalesOrder.SalesCategoryId && partIds.Contains(r.SparePartId)).ToList();

                    #region 生成调拨单及清单
                    var nowTime = DateTime.Now;
                    var userInfo = Utils.GetCurrentUserInfo();
                    var newPartsTransferOrder = new PartsTransferOrder();
                    newPartsTransferOrder.Id = i++;
                    newPartsTransferOrder.OriginalBillId = dbpartsSalesOrder.Id;
                    newPartsTransferOrder.OriginalBillCode = dbpartsSalesOrder.Code;
                    newPartsTransferOrder.OriginalWarehouseId = marketingProcessDetail.PurchaseWarehouseId.Value;
                    newPartsTransferOrder.OriginalWarehouseCode = marketingProcessDetail.PurchaseWarehouseCode;
                    newPartsTransferOrder.OriginalWarehouseName = marketingProcessDetail.PurchaseWarehouseName;
                    newPartsTransferOrder.StorageCompanyId = purchaseWarehouse.StorageCompanyId;
                    newPartsTransferOrder.StorageCompanyType = purchaseWarehouse.StorageCompanyType;
                    newPartsTransferOrder.DestWarehouseId = marketingProcessDetail.WarehouseId.Value;
                    newPartsTransferOrder.DestWarehouseCode = marketingProcessDetail.WarehouseCode;
                    newPartsTransferOrder.DestWarehouseName = marketingProcessDetail.WarehouseName;
                    newPartsTransferOrder.Status = (int)DcsPartsTransferOrderStatus.已审批;
                    newPartsTransferOrder.ModifierId = userInfo.Id;
                    newPartsTransferOrder.ModifierName = userInfo.Name;
                    newPartsTransferOrder.ModifyTime = nowTime;
                    newPartsTransferOrder.ApproverId = userInfo.Id;
                    newPartsTransferOrder.ApproverName = userInfo.Name;
                    newPartsTransferOrder.ApproveTime = nowTime;
                    newPartsTransferOrder.Type = (int)DcsPartsTransferOrderType.急需调拨;
                    InsertToDatabase(newPartsTransferOrder);
                    new PartsTransferOrderAch(this.DomainService).InsertPartsTransferOrderValidate(newPartsTransferOrder);
                    foreach(var detail in marketingProcessDetails) {
                        var partsSalesPrice = dbPartsSalesPrices.First(r => r.SparePartId == detail.SparePartId);
                        var partsPlannedPrice = dbpartsPlannedPrices.First(r => r.SparePartId == detail.SparePartId);
                        if(dbpartsPlannedPrices == null)
                            throw new ValidationException("配件" + detail.SparePartCode + "不存在计划价");
                        var newPartsTransferOrderDetail = new PartsTransferOrderDetail();
                        newPartsTransferOrderDetail.PartsTransferOrderId = newPartsTransferOrder.Id;
                        newPartsTransferOrderDetail.SparePartId = detail.SparePartId;
                        newPartsTransferOrderDetail.SparePartCode = detail.SparePartCode;
                        newPartsTransferOrderDetail.SparePartName = detail.SparePartName;
                        newPartsTransferOrderDetail.PlannedAmount = detail.CurrentFulfilledQuantity;
                        newPartsTransferOrderDetail.ConfirmedAmount = detail.CurrentFulfilledQuantity;
                        newPartsTransferOrderDetail.Price = partsSalesPrice.SalesPrice;
                        newPartsTransferOrderDetail.PlannPrice = partsPlannedPrice.PlannedPrice;
                        newPartsTransferOrder.PartsTransferOrderDetails.Add(newPartsTransferOrderDetail);
                    }
                    newPartsTransferOrder.TotalAmount = newPartsTransferOrder.PartsTransferOrderDetails.Sum(r => r.Price * r.PlannedAmount);
                    newPartsTransferOrder.TotalPlanAmount = newPartsTransferOrder.PartsTransferOrderDetails.Sum(r => r.PlannPrice * r.PlannedAmount);
                    #endregion

                    #region 生成出库计划单及清单，生成出库单及清单，生成发运单及清单
                    var company = dbCompanies.FirstOrDefault(r => r.Id == purchaseWarehouse.StorageCompanyId);
                    if(company == null)
                        throw new ValidationException("配件调拨仓储企业不存在");
                    var partsSalesCategoryId = dbSalesUnitAffiWarehouses.Where(r => r.WarehouseId == marketingProcessDetail.PurchaseWarehouseId).Select(r => r.SalesUnit).Select(r => r.PartsSalesCategoryId).ToArray();
                    var partsSalesCategory = dbPartsSalesCategories.FirstOrDefault(r => partsSalesCategoryId.Contains(r.Id));
                    if(partsSalesCategory == null)
                        throw new ValidationException("配件销售类型不存在");
                    var newPartsOutboundPlan = new PartsOutboundPlan();
                    newPartsOutboundPlan.Id = i++;
                    newPartsOutboundPlan.WarehouseId = newPartsTransferOrder.OriginalWarehouseId;
                    newPartsOutboundPlan.WarehouseCode = newPartsTransferOrder.OriginalWarehouseCode;
                    newPartsOutboundPlan.WarehouseName = newPartsTransferOrder.OriginalWarehouseName;
                    newPartsOutboundPlan.StorageCompanyId = newPartsTransferOrder.StorageCompanyId;
                    newPartsOutboundPlan.StorageCompanyCode = company.Code;
                    newPartsOutboundPlan.StorageCompanyName = company.Name;
                    newPartsOutboundPlan.StorageCompanyType = newPartsTransferOrder.StorageCompanyType;
                    newPartsOutboundPlan.BranchId = purchaseWarehouse.BranchId;
                    newPartsOutboundPlan.BranchCode = purchaseWarehouse.Branch.Code;
                    newPartsOutboundPlan.BranchName = purchaseWarehouse.Branch.Name;
                    newPartsOutboundPlan.CounterpartCompanyId = newPartsTransferOrder.StorageCompanyId;
                    newPartsOutboundPlan.CounterpartCompanyCode = company.Code;
                    newPartsOutboundPlan.CounterpartCompanyName = company.Name;
                    newPartsOutboundPlan.ReceivingCompanyId = newPartsOutboundPlan.StorageCompanyId;
                    newPartsOutboundPlan.ReceivingCompanyCode = newPartsOutboundPlan.StorageCompanyCode;
                    newPartsOutboundPlan.ReceivingCompanyName = newPartsOutboundPlan.StorageCompanyName;
                    newPartsOutboundPlan.SourceId = newPartsTransferOrder.Id;
                    newPartsOutboundPlan.SourceCode = newPartsTransferOrder.Code;
                    newPartsOutboundPlan.OutboundType = (int)DcsPartsOutboundType.配件调拨;
                    newPartsOutboundPlan.CustomerAccountId = default(int?);
                    newPartsOutboundPlan.OriginalRequirementBillId = dbpartsSalesOrder.Id;
                    newPartsOutboundPlan.OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.配件销售订单;
                    newPartsOutboundPlan.OriginalRequirementBillCode = dbpartsSalesOrder.Code;
                    newPartsOutboundPlan.Status = (int)DcsPartsOutboundPlanStatus.出库完成;
                    newPartsOutboundPlan.Remark = newPartsTransferOrder.Remark;
                    newPartsOutboundPlan.ReceivingWarehouseId = newPartsTransferOrder.DestWarehouseId;
                    newPartsOutboundPlan.ReceivingWarehouseCode = newPartsTransferOrder.DestWarehouseCode;
                    newPartsOutboundPlan.ReceivingWarehouseName = newPartsTransferOrder.DestWarehouseName;
                    newPartsOutboundPlan.ShippingMethod = newPartsTransferOrder.ShippingMethod;
                    newPartsOutboundPlan.PartsSalesCategoryId = partsSalesCategory.Id;
                    newPartsOutboundPlan.PartsSalesOrderTypeId = dbpartsSalesOrder.PartsSalesOrderTypeId;
                    newPartsOutboundPlan.PartsSalesOrderTypeName = dbpartsSalesOrder.PartsSalesOrderTypeName;
                    newPartsTransferOrder.PartsOutboundPlans.Add(newPartsOutboundPlan);
                    //dbpartsSalesOrder.PartsOutboundPlans.Add(newPartsOutboundPlan);
                    InsertToDatabase(newPartsOutboundPlan);
                    new PartsOutboundPlanAch(this.DomainService).InsertPartsOutboundPlanValidate(newPartsOutboundPlan);
                    foreach(var detail in newPartsTransferOrder.PartsTransferOrderDetails.ToList()) {
                        var newPartsOutboundPlanDetail = new PartsOutboundPlanDetail();
                        newPartsOutboundPlanDetail.PartsOutboundPlanId = newPartsOutboundPlan.Id;
                        newPartsOutboundPlanDetail.SparePartId = detail.SparePartId;
                        newPartsOutboundPlanDetail.SparePartCode = detail.SparePartCode;
                        newPartsOutboundPlanDetail.SparePartName = detail.SparePartName;
                        newPartsOutboundPlanDetail.PlannedAmount = detail.PlannedAmount;
                        newPartsOutboundPlanDetail.OutboundFulfillment = detail.PlannedAmount;
                        newPartsOutboundPlanDetail.Price = detail.Price;
                        newPartsOutboundPlanDetail.Remark = detail.Remark;
                        newPartsOutboundPlan.PartsOutboundPlanDetails.Add(newPartsOutboundPlanDetail);
                    }
                    var newPartsOutboundBill = new PartsOutboundBill();
                    newPartsOutboundBill.Id = i++;
                    newPartsOutboundBill.PartsOutboundPlanId = newPartsOutboundPlan.Id;
                    newPartsOutboundBill.WarehouseId = newPartsOutboundPlan.WarehouseId;
                    newPartsOutboundBill.WarehouseCode = newPartsOutboundPlan.WarehouseCode;
                    newPartsOutboundBill.WarehouseName = newPartsOutboundPlan.WarehouseName;
                    newPartsOutboundBill.StorageCompanyId = newPartsOutboundPlan.StorageCompanyId;
                    newPartsOutboundBill.StorageCompanyCode = newPartsOutboundPlan.StorageCompanyCode;
                    newPartsOutboundBill.StorageCompanyName = newPartsOutboundPlan.StorageCompanyName;
                    newPartsOutboundBill.StorageCompanyType = newPartsOutboundPlan.StorageCompanyType;
                    newPartsOutboundBill.BranchId = newPartsOutboundPlan.BranchId;
                    newPartsOutboundBill.BranchCode = newPartsOutboundPlan.BranchCode;
                    newPartsOutboundBill.BranchName = newPartsOutboundPlan.BranchName;
                    newPartsOutboundBill.PartsSalesCategoryId = newPartsOutboundPlan.PartsSalesCategoryId;
                    newPartsOutboundBill.PartsSalesOrderTypeId = newPartsOutboundPlan.PartsSalesOrderTypeId;
                    newPartsOutboundBill.PartsSalesOrderTypeName = newPartsOutboundPlan.PartsSalesOrderTypeName;
                    newPartsOutboundBill.CounterpartCompanyId = newPartsOutboundPlan.CounterpartCompanyId;
                    newPartsOutboundBill.CounterpartCompanyCode = newPartsOutboundPlan.CounterpartCompanyCode;
                    newPartsOutboundBill.CounterpartCompanyName = newPartsOutboundPlan.CounterpartCompanyName;
                    newPartsOutboundBill.ReceivingCompanyId = newPartsOutboundPlan.ReceivingCompanyId;
                    newPartsOutboundBill.ReceivingCompanyCode = newPartsOutboundPlan.ReceivingCompanyCode;
                    newPartsOutboundBill.ReceivingCompanyName = newPartsOutboundPlan.ReceivingCompanyName;
                    newPartsOutboundBill.ReceivingWarehouseId = newPartsOutboundPlan.ReceivingWarehouseId;
                    newPartsOutboundBill.ReceivingWarehouseCode = newPartsOutboundPlan.ReceivingWarehouseCode;
                    newPartsOutboundBill.ReceivingWarehouseName = newPartsOutboundPlan.ReceivingWarehouseName;
                    newPartsOutboundBill.OutboundType = newPartsOutboundPlan.OutboundType;
                    newPartsOutboundBill.ShippingMethod = newPartsOutboundPlan.ShippingMethod;
                    newPartsOutboundBill.CustomerAccountId = newPartsOutboundPlan.CustomerAccountId;
                    newPartsOutboundBill.OriginalRequirementBillId = newPartsOutboundPlan.OriginalRequirementBillId;
                    newPartsOutboundBill.OriginalRequirementBillType = newPartsOutboundPlan.OriginalRequirementBillType;
                    newPartsOutboundBill.OriginalRequirementBillCode = newPartsOutboundPlan.OriginalRequirementBillCode;
                    newPartsOutboundBill.SettlementStatus = (int)DcsPartsSettlementStatus.待结算;
                    newPartsOutboundBill.Remark = "统购分销出库";
                    newPartsOutboundPlan.PartsOutboundBills.Add(newPartsOutboundBill);
                    InsertToDatabase(newPartsOutboundBill);
                    new PartsOutboundBillAch(this.DomainService).InsertPartsOutboundBillValidate(newPartsOutboundBill);
                    var outpartIds = newPartsOutboundPlan.PartsOutboundPlanDetails.Select(r => r.SparePartId).ToArray();
                    var dbPartsStocks = new PartsStockAch(this.DomainService).GetPartsStocksWithLock(ObjectContext.PartsStocks.Include("WarehouseArea").Where(r => r.WarehouseId == newPartsOutboundBill.WarehouseId && outpartIds.Contains(r.PartId) && r.WarehouseAreaCategory.Category == (int)DcsAreaType.保管区)).ToList();
                    var dbPartsPlannedPrices = ObjectContext.PartsPlannedPrices.Where(r => r.OwnerCompanyId == newPartsOutboundBill.StorageCompanyId && r.PartsSalesCategoryId == newPartsOutboundBill.PartsSalesCategoryId && outpartIds.Contains(r.SparePartId)).ToList();
                    foreach(var detail in newPartsOutboundPlan.PartsOutboundPlanDetails) {
                        var partsStock = dbPartsStocks.Where(r => r.PartId == detail.SparePartId).OrderByDescending(r => r.Quantity);
                        if(!partsStock.Any())
                            throw new ValidationException(string.Format("配件编号:{0}对应的配件库存不存在", detail.SparePartCode));
                        if(partsStock.Sum(r => r.Quantity) < detail.OutboundFulfillment)
                            throw new ValidationException(string.Format("配件编号:{0}对应的配件库存数量不足", detail.SparePartCode));
                        var partsPlannedPrice = dbPartsPlannedPrices.FirstOrDefault(r => r.SparePartId == detail.SparePartId);
                        foreach(var stock in partsStock.ToList()) {
                            var newPartsOutboundBillDetail = new PartsOutboundBillDetail();
                            if(stock.Quantity >= detail.OutboundFulfillment) {
                                stock.Quantity -= detail.OutboundFulfillment.Value;
                                newPartsOutboundBillDetail.PartsOutboundBillId = newPartsOutboundBill.Id;
                                newPartsOutboundBillDetail.SparePartId = detail.SparePartId;
                                newPartsOutboundBillDetail.SparePartCode = detail.SparePartCode;
                                newPartsOutboundBillDetail.SparePartName = detail.SparePartName;
                                newPartsOutboundBillDetail.OutboundAmount = detail.OutboundFulfillment.Value;
                                newPartsOutboundBillDetail.WarehouseAreaId = stock.WarehouseAreaId;
                                newPartsOutboundBillDetail.WarehouseAreaCode = stock.WarehouseArea.Code;
                                newPartsOutboundBillDetail.SettlementPrice = detail.Price;
                                newPartsOutboundBillDetail.CostPrice = partsPlannedPrice != null ? partsPlannedPrice.PlannedPrice : default(decimal);
                                newPartsOutboundBill.PartsOutboundBillDetails.Add(newPartsOutboundBillDetail);
                                UpdateToDatabase(stock);
                                new PartsStockAch(this.DomainService).UpdatePartsStockValidate(stock);
                                break;
                            }
                            detail.OutboundFulfillment -= stock.Quantity;
                            newPartsOutboundBillDetail.PartsOutboundBillId = newPartsOutboundBill.Id;
                            newPartsOutboundBillDetail.SparePartId = detail.SparePartId;
                            newPartsOutboundBillDetail.SparePartCode = detail.SparePartCode;
                            newPartsOutboundBillDetail.SparePartName = detail.SparePartName;
                            newPartsOutboundBillDetail.OutboundAmount = stock.Quantity;
                            newPartsOutboundBillDetail.WarehouseAreaId = stock.WarehouseAreaId;
                            newPartsOutboundBillDetail.WarehouseAreaCode = stock.WarehouseArea.Code;
                            newPartsOutboundBillDetail.SettlementPrice = detail.Price;
                            newPartsOutboundBillDetail.CostPrice = partsPlannedPrice != null ? partsPlannedPrice.PlannedPrice : default(decimal);
                            newPartsOutboundBill.PartsOutboundBillDetails.Add(newPartsOutboundBillDetail);
                            stock.Quantity = 0;
                            UpdateToDatabase(stock);
                            new PartsStockAch(this.DomainService).UpdatePartsStockValidate(stock);
                        }
                    }
                    var newPartsShippingOrder = new PartsShippingOrder();
                    newPartsShippingOrder.Id = i++;
                    newPartsShippingOrder.Type = (int)DcsPartsShippingOrderType.调拨;
                    newPartsShippingOrder.PartsSalesCategoryId = partsSalesCategory.Id;
                    newPartsShippingOrder.BranchId = newPartsOutboundBill.BranchId;
                    newPartsShippingOrder.ShippingCompanyId = newPartsOutboundBill.StorageCompanyId;
                    newPartsShippingOrder.ShippingCompanyCode = newPartsOutboundBill.StorageCompanyCode;
                    newPartsShippingOrder.ShippingCompanyName = newPartsOutboundBill.StorageCompanyName;
                    newPartsShippingOrder.SettlementCompanyId = newPartsOutboundBill.StorageCompanyId;
                    newPartsShippingOrder.SettlementCompanyCode = newPartsOutboundBill.StorageCompanyCode;
                    newPartsShippingOrder.SettlementCompanyName = newPartsOutboundBill.StorageCompanyName;
                    newPartsShippingOrder.ReceivingCompanyId = newPartsOutboundBill.ReceivingCompanyId;
                    newPartsShippingOrder.ReceivingCompanyCode = newPartsOutboundBill.ReceivingCompanyCode;
                    newPartsShippingOrder.ReceivingCompanyName = newPartsOutboundBill.ReceivingCompanyName;
                    newPartsShippingOrder.InvoiceReceiveSaleCateId = newPartsOutboundBill.PartsSalesOrderTypeId;
                    newPartsShippingOrder.InvoiceReceiveSaleCateName = newPartsOutboundBill.PartsSalesOrderTypeName;
                    newPartsShippingOrder.WarehouseId = newPartsOutboundBill.WarehouseId;
                    newPartsShippingOrder.WarehouseCode = newPartsOutboundBill.WarehouseCode;
                    newPartsShippingOrder.WarehouseName = newPartsOutboundBill.WarehouseName;
                    newPartsShippingOrder.ReceivingWarehouseId = newPartsOutboundBill.ReceivingWarehouseId;
                    newPartsShippingOrder.ReceivingWarehouseCode = newPartsOutboundBill.ReceivingWarehouseCode;
                    newPartsShippingOrder.ReceivingWarehouseName = newPartsOutboundBill.ReceivingWarehouseName;
                    newPartsShippingOrder.OriginalRequirementBillId = newPartsOutboundBill.OriginalRequirementBillId;
                    newPartsShippingOrder.OriginalRequirementBillType = newPartsOutboundBill.OriginalRequirementBillType;
                    newPartsShippingOrder.OriginalRequirementBillCode = newPartsOutboundBill.OriginalRequirementBillCode;
                    newPartsShippingOrder.ShippingMethod = shippingMethodKey;
                    newPartsShippingOrder.Status = (int)DcsPartsShippingOrderStatus.回执确认;
                    newPartsShippingOrder.Remark = "统购分销发运";
                    DomainService.InsertPartsShippingOrderValidate(newPartsShippingOrder);
                    foreach(var detail in newPartsTransferOrder.PartsTransferOrderDetails.ToList()) {
                        var newPartsShippingOrderDetail = new PartsShippingOrderDetail();
                        newPartsShippingOrderDetail.PartsShippingOrderId = newPartsShippingOrder.Id;
                        newPartsShippingOrderDetail.SparePartId = detail.SparePartId;
                        newPartsShippingOrderDetail.SparePartCode = detail.SparePartCode;
                        newPartsShippingOrderDetail.SparePartName = detail.SparePartName;
                        newPartsShippingOrderDetail.ShippingAmount = detail.ConfirmedAmount.Value;
                        newPartsShippingOrderDetail.ConfirmedAmount = detail.ConfirmedAmount;
                        newPartsShippingOrderDetail.SettlementPrice = detail.Price;
                        newPartsShippingOrder.PartsShippingOrderDetails.Add(newPartsShippingOrderDetail);
                    }
                    var newPartsShippingOrderRef = new PartsShippingOrderRef {
                        PartsOutboundBillId = newPartsOutboundBill.Id,
                        PartsShippingOrderId = newPartsShippingOrder.Id
                    };
                    newPartsShippingOrder.PartsShippingOrderRefs.Add(newPartsShippingOrderRef);
                    InsertToDatabase(newPartsShippingOrder);
                    newPartsOutboundBill.PartsShippingOrderRefs.Add(newPartsShippingOrderRef);
                    InsertToDatabase(newPartsOutboundBill);
                    newPartsOutboundPlan.PartsOutboundBills.Add(newPartsOutboundBill);
                    InsertToDatabase(newPartsOutboundPlan);
                    #endregion

                    #region 新增配件物流批次，单据清单
                    var newPartsLogisticBatch = new PartsLogisticBatch();
                    newPartsLogisticBatch.Id = i++;
                    newPartsLogisticBatch.CompanyId = newPartsShippingOrder.BranchId;
                    newPartsLogisticBatch.Status = (int)DcsPartsLogisticBatchStatus.完成;
                    newPartsLogisticBatch.ShippingStatus = (int)DcsPartsLogisticBatchShippingStatus.发运;
                    newPartsLogisticBatch.SourceId = newPartsOutboundPlan.Id;
                    newPartsLogisticBatch.SourceType = (int)DcsPartsLogisticBatchSourceType.配件出库计划;
                    newPartsOutboundPlan.PartsLogisticBatches.Add(newPartsLogisticBatch);
                    InsertToDatabase(newPartsLogisticBatch);
                    new PartsLogisticBatchAch(this.DomainService).InsertPartsLogisticBatchValidate(newPartsLogisticBatch);
                    var partsLogisticBatchBillDetail = new PartsLogisticBatchBillDetail {
                        PartsLogisticBatchId = newPartsLogisticBatch.Id,
                        BillId = newPartsShippingOrder.Id,
                        BillType = (int)DcsPartsLogisticBatchBillDetailBillType.配件发运单,
                        CreatorId = newPartsShippingOrder.CreatorId,
                        CreatorName = newPartsShippingOrder.CreatorName,
                        CreateTime = newPartsShippingOrder.CreateTime
                    };
                    newPartsShippingOrder.PartsLogisticBatchBillDetails.Add(partsLogisticBatchBillDetail);
                    var partsLogisticBatchBillDetail1 = new PartsLogisticBatchBillDetail {
                        PartsLogisticBatchId = newPartsLogisticBatch.Id,
                        BillId = newPartsOutboundBill.Id,
                        BillType = (int)DcsPartsLogisticBatchBillDetailBillType.配件出库单,
                        CreatorId = newPartsOutboundBill.CreatorId,
                        CreatorName = newPartsOutboundBill.CreatorName,
                        CreateTime = newPartsOutboundBill.CreateTime
                    };
                    newPartsOutboundBill.PartsLogisticBatchBillDetails.Add(partsLogisticBatchBillDetail1);
                    newPartsLogisticBatch.PartsLogisticBatchBillDetails.Add(partsLogisticBatchBillDetail);
                    newPartsLogisticBatch.PartsLogisticBatchBillDetails.Add(partsLogisticBatchBillDetail1);
                    #endregion

                    #region 生成入库计划单及清单，生成配件入库检验单及清单
                    var newPartsInboundPlan = new PartsInboundPlan();
                    newPartsInboundPlan.Id = i++;
                    newPartsInboundPlan.WarehouseId = newPartsShippingOrder.ReceivingWarehouseId.Value;
                    newPartsInboundPlan.WarehouseCode = newPartsShippingOrder.ReceivingWarehouseCode;
                    newPartsInboundPlan.WarehouseName = newPartsShippingOrder.ReceivingWarehouseName;
                    newPartsInboundPlan.StorageCompanyId = warehouse.StorageCompanyId;
                    newPartsInboundPlan.StorageCompanyCode = company.Code;
                    newPartsInboundPlan.StorageCompanyName = company.Name;
                    newPartsInboundPlan.StorageCompanyType = company.Type;
                    newPartsInboundPlan.BranchId = newPartsShippingOrder.BranchId;
                    newPartsInboundPlan.BranchCode = newPartsOutboundBill.BranchCode;
                    newPartsInboundPlan.BranchName = newPartsOutboundBill.BranchName;
                    newPartsInboundPlan.PartsSalesCategoryId = dbpartsSalesOrder.SalesCategoryId;
                    newPartsInboundPlan.CounterpartCompanyId = newPartsShippingOrder.SettlementCompanyId;
                    newPartsInboundPlan.CounterpartCompanyCode = newPartsShippingOrder.SettlementCompanyCode;
                    newPartsInboundPlan.CounterpartCompanyName = newPartsShippingOrder.SettlementCompanyName;
                    newPartsInboundPlan.SourceId = newPartsShippingOrder.Id;
                    newPartsInboundPlan.SourceCode = newPartsShippingOrder.Code;
                    newPartsInboundPlan.InboundType = (int)DcsPartsInboundType.配件调拨;
                    newPartsInboundPlan.OriginalRequirementBillId = newPartsShippingOrder.OriginalRequirementBillId.Value;
                    newPartsInboundPlan.OriginalRequirementBillType = newPartsShippingOrder.OriginalRequirementBillType.Value;
                    newPartsInboundPlan.OriginalRequirementBillCode = newPartsShippingOrder.OriginalRequirementBillCode;
                    newPartsInboundPlan.Status = (int)DcsPartsInboundPlanStatus.检验完成;
                    newPartsInboundPlan.Remark = newPartsShippingOrder.Remark;
                    newPartsShippingOrder.PartsInboundPlans.Add(newPartsInboundPlan);
                    InsertToDatabase(newPartsInboundPlan);
                    new PartsInboundPlanAch(this.DomainService).InsertPartsInboundPlanValidate(newPartsInboundPlan);
                    foreach(var detail in newPartsTransferOrder.PartsTransferOrderDetails.ToList()) {
                        var newPartsInboundPlanDetail = new PartsInboundPlanDetail();
                        newPartsInboundPlanDetail.PartsInboundPlanId = newPartsInboundPlan.Id;
                        newPartsInboundPlanDetail.SparePartId = detail.SparePartId;
                        newPartsInboundPlanDetail.SparePartCode = detail.SparePartCode;
                        newPartsInboundPlanDetail.SparePartName = detail.SparePartName;
                        newPartsInboundPlanDetail.PlannedAmount = detail.ConfirmedAmount.HasValue ? detail.ConfirmedAmount.Value : default(int);
                        newPartsInboundPlanDetail.InspectedQuantity = detail.ConfirmedAmount;
                        newPartsInboundPlanDetail.Price = detail.Price;
                        newPartsInboundPlanDetail.Remark = detail.Remark;
                        newPartsInboundPlan.PartsInboundPlanDetails.Add(newPartsInboundPlanDetail);
                    }
                    var newPartsInboundCheckBill = new PartsInboundCheckBill();
                    newPartsInboundCheckBill.Id = i++;
                    newPartsInboundCheckBill.PartsInboundPlanId = newPartsInboundPlan.Id;
                    newPartsInboundCheckBill.WarehouseId = newPartsInboundPlan.WarehouseId;
                    newPartsInboundCheckBill.WarehouseCode = newPartsInboundPlan.WarehouseCode;
                    newPartsInboundCheckBill.WarehouseName = newPartsInboundPlan.WarehouseName;
                    newPartsInboundCheckBill.StorageCompanyId = newPartsInboundPlan.StorageCompanyId;
                    newPartsInboundCheckBill.StorageCompanyCode = newPartsInboundPlan.StorageCompanyCode;
                    newPartsInboundCheckBill.StorageCompanyName = newPartsInboundPlan.StorageCompanyName;
                    newPartsInboundCheckBill.StorageCompanyType = newPartsInboundPlan.StorageCompanyType;
                    newPartsInboundCheckBill.PartsSalesCategoryId = newPartsInboundPlan.PartsSalesCategoryId;
                    newPartsInboundCheckBill.BranchId = newPartsInboundPlan.BranchId;
                    newPartsInboundCheckBill.BranchCode = newPartsInboundPlan.BranchCode;
                    newPartsInboundCheckBill.BranchName = newPartsInboundPlan.BranchName;
                    newPartsInboundCheckBill.CounterpartCompanyId = newPartsInboundPlan.CounterpartCompanyId;
                    newPartsInboundCheckBill.CounterpartCompanyCode = newPartsInboundPlan.CounterpartCompanyCode;
                    newPartsInboundCheckBill.CounterpartCompanyName = newPartsInboundPlan.CounterpartCompanyName;
                    newPartsInboundCheckBill.InboundType = (int)DcsPartsInboundType.配件调拨;
                    newPartsInboundCheckBill.OriginalRequirementBillId = newPartsInboundPlan.OriginalRequirementBillId;
                    newPartsInboundCheckBill.OriginalRequirementBillType = newPartsInboundPlan.OriginalRequirementBillType;
                    newPartsInboundCheckBill.OriginalRequirementBillCode = newPartsInboundPlan.OriginalRequirementBillCode;
                    newPartsInboundCheckBill.Status = (int)DcsPartsInboundCheckBillStatus.上架完成;
                    newPartsInboundCheckBill.SettlementStatus = (int)DcsPartsSettlementStatus.待结算;
                    newPartsInboundPlan.PartsInboundCheckBills.Add(newPartsInboundCheckBill);
                    InsertToDatabase(newPartsInboundCheckBill);
                    new PartsInboundCheckBillAch(this.DomainService).InsertPartsInboundCheckBillValidate(newPartsInboundCheckBill);
                    var inpartIds = newPartsInboundPlan.PartsInboundPlanDetails.Select(r => r.SparePartId).ToArray();
                    var inPartsStocks = new PartsStockAch(this.DomainService).GetPartsStocksWithLock(ObjectContext.PartsStocks.Include("WarehouseArea").Where(r => r.WarehouseId == newPartsInboundPlan.WarehouseId && inpartIds.Contains(r.PartId) && r.WarehouseAreaCategory.Category == (int)DcsAreaType.保管区)).ToList();
                    var warehouseAreaIds = inPartsStocks.Select(r => r.WarehouseAreaId).ToArray();
                    var dbWarehouseAreas = ObjectContext.WarehouseAreas.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && warehouseAreaIds.Contains(r.Id) || (r.WarehouseId == newPartsInboundCheckBill.WarehouseId && r.WarehouseAreaCategory.Category == (int)DcsAreaType.保管区 && r.AreaKind == (int)DcsAreaKind.库位));
                    var inWarehouseArea = dbWarehouseAreas.FirstOrDefault(r => r.WarehouseId == newPartsInboundCheckBill.WarehouseId && r.WarehouseAreaCategory.Category == (int)DcsAreaType.保管区 && r.AreaKind == (int)DcsAreaKind.库位);
                    if(inWarehouseArea == null)
                        throw new ValidationException(string.Format("仓库编号:{0}对应的保管区库位不存在", newPartsInboundCheckBill.WarehouseCode));
                    var inPartsPlannedPrices = ObjectContext.PartsPlannedPrices.Where(r => r.OwnerCompanyId == newPartsInboundCheckBill.StorageCompanyId && r.PartsSalesCategoryId == newPartsInboundCheckBill.PartsSalesCategoryId && outpartIds.Contains(r.SparePartId)).ToList();
                    foreach(var detail in newPartsInboundPlan.PartsInboundPlanDetails) {
                        int warehouseAreaId;
                        string warehouseAreaCode;
                        if(inPartsStocks.All(r => r.PartId != detail.SparePartId)) {
                            var newPartsStock = new PartsStock();
                            newPartsStock.WarehouseId = newPartsInboundCheckBill.WarehouseId;
                            newPartsStock.StorageCompanyId = newPartsInboundCheckBill.StorageCompanyId;
                            newPartsStock.StorageCompanyType = newPartsInboundCheckBill.StorageCompanyType;
                            newPartsStock.BranchId = newPartsInboundCheckBill.BranchId;
                            newPartsStock.WarehouseAreaId = inWarehouseArea.Id;
                            newPartsStock.WarehouseAreaCategoryId = inWarehouseArea.AreaCategoryId;
                            newPartsStock.PartId = detail.SparePartId;
                            newPartsStock.Quantity = detail.InspectedQuantity.HasValue ? detail.InspectedQuantity.Value : default(int);
                            warehouseAreaId = inWarehouseArea.Id;
                            warehouseAreaCode = inWarehouseArea.Code;
                            InsertToDatabase(newPartsStock);
                            new PartsStockAch(this.DomainService).InsertPartsStockValidate(newPartsStock);
                            partsStockLists.Add(newPartsStock);
                        } else {
                            var inPartsStock = inPartsStocks.First(r => r.PartId == detail.SparePartId);
                            inPartsStock.Quantity += detail.InspectedQuantity.HasValue ? detail.InspectedQuantity.Value : default(int);
                            warehouseAreaId = inPartsStock.WarehouseAreaId;
                            warehouseAreaCode = dbWarehouseAreas.First(r => r.Id == inPartsStock.WarehouseAreaId).Code;
                            UpdateToDatabase(inPartsStock);
                            new PartsStockAch(this.DomainService).UpdatePartsStockValidate(inPartsStock);
                        }
                        var costPrice = inPartsPlannedPrices.FirstOrDefault(r => r.SparePartId == detail.SparePartId);
                        var newPartsInboundCheckBillDetail = new PartsInboundCheckBillDetail();
                        newPartsInboundCheckBillDetail.PartsInboundCheckBillId = newPartsInboundCheckBill.Id;
                        newPartsInboundCheckBillDetail.SparePartId = detail.SparePartId;
                        newPartsInboundCheckBillDetail.SparePartCode = detail.SparePartCode;
                        newPartsInboundCheckBillDetail.SparePartName = detail.SparePartName;
                        newPartsInboundCheckBillDetail.WarehouseAreaId = warehouseAreaId;
                        newPartsInboundCheckBillDetail.WarehouseAreaCode = warehouseAreaCode;
                        newPartsInboundCheckBillDetail.InspectedQuantity = detail.InspectedQuantity.HasValue ? detail.InspectedQuantity.Value : default(int);
                        newPartsInboundCheckBillDetail.SettlementPrice = detail.Price;
                        newPartsInboundCheckBillDetail.CostPrice = costPrice != null ? costPrice.PlannedPrice : default(decimal);
                        newPartsInboundCheckBillDetail.POCode = detail.POCode;
                        newPartsInboundCheckBill.PartsInboundCheckBillDetails.Add(newPartsInboundCheckBillDetail);
                    }
                    var partsLogisticBatchBillDetail2 = new PartsLogisticBatchBillDetail {
                        PartsLogisticBatchId = newPartsLogisticBatch.Id,
                        BillId = newPartsInboundPlan.Id,
                        BillType = (int)DcsPartsLogisticBatchBillDetailBillType.配件入库计划,
                        CreatorId = newPartsInboundPlan.CreatorId,
                        CreatorName = newPartsInboundPlan.CreatorName,
                        CreateTime = newPartsInboundPlan.CreateTime
                    };
                    newPartsInboundPlan.PartsLogisticBatchBillDetails.Add(partsLogisticBatchBillDetail2);
                    newPartsLogisticBatch.PartsLogisticBatchBillDetails.Add(partsLogisticBatchBillDetail2);
                    #endregion
                }
            }
        }

        /// <summary>
        /// 批量审核配件销售订单
        /// </summary>
        /// <param name="ids">配件销售订单id数组</param>
        /// <returns>是否部分审批</returns>

        public bool 批量审核配件销售订单(int[] ids) {
            try {
                var partsOutBoundPlans = new List<PartsOutboundPlan>();
                var userInfo = Utils.GetCurrentUserInfo();
                var partsSalesOrders = ObjectContext.PartsSalesOrders.Where(r => ids.Contains(r.Id) && r.Status != (int)DcsPartsSalesOrderStatus.作废);
                var isPartApproval = false;
                foreach(var partsSalesOrder in partsSalesOrders) {
                    new PartsSalesOrderAch(this.DomainService).PMS_SYC_150_记录日志用(partsSalesOrder);
                    var checkPartIds = partsSalesOrder.PartsSalesOrderDetails.Select(r => r.SparePartId).ToArray();
                    var latestPriceSalesOrderDetails = new VirtualPartsSalesPriceAch(this.DomainService).根据配件清单获取销售价格(partsSalesOrder.PartsSalesOrderTypeId, partsSalesOrder.SubmitCompanyId, checkPartIds);
                    var warehousePartsStocks = DomainService.查询仓库库存销售审核(partsSalesOrder.SalesUnitOwnerCompanyId, partsSalesOrder.WarehouseId, partsSalesOrder.PartsSalesOrderDetails.Select(r => r.SparePartId).ToArray()).ToArray();

                    var partsSalesOrderProcess = new PartsSalesOrderProcess {
                        OriginalSalesOrderId = partsSalesOrder.Id,
                        BillStatusBeforeProcess = partsSalesOrder.Status,
                        RequestedShippingTime = partsSalesOrder.RequestedShippingTime,
                        RequestedDeliveryTime = partsSalesOrder.RequestedDeliveryTime,
                        ShippingMethod = partsSalesOrder.ShippingMethod,
                        Time = (partsSalesOrder.Time ?? 0) + 1
                    };

                    if(string.IsNullOrWhiteSpace(partsSalesOrderProcess.Code) || partsSalesOrderProcess.Code == GlobalVar.ASSIGNED_BY_SERVER)
                        partsSalesOrderProcess.Code = CodeGenerator.Generate("PartsSalesOrderProcess", userInfo.EnterpriseCode);

                    ObjectContext.AddToPartsSalesOrderProcesses(partsSalesOrderProcess);

                    foreach(var partsSalesOrderDetail in partsSalesOrder.PartsSalesOrderDetails) {
                        if((partsSalesOrderDetail.ApproveQuantity ?? 0) < partsSalesOrderDetail.OrderedQuantity) {
                            var warehouse = ObjectContext.Warehouses.SingleOrDefault(r => r.Id == partsSalesOrder.WarehouseId);
                            if(warehouse == null) {
                                throw new ValidationException(string.Format("Id为{0}仓库的不存在", partsSalesOrder.WarehouseId));
                            }
                            var warehousePartsStock = warehousePartsStocks.SingleOrDefault(r => r.SparePartId == partsSalesOrderDetail.SparePartId);
                            var current = (warehousePartsStock == null ? 0 : warehousePartsStock.UsableQuantity) > partsSalesOrderDetail.OrderedQuantity - (partsSalesOrderDetail.ApproveQuantity ?? 0) ? partsSalesOrderDetail.OrderedQuantity - (partsSalesOrderDetail.ApproveQuantity ?? 0) : (warehousePartsStock == null ? 0 : warehousePartsStock.UsableQuantity);
                            if(current > 0) {

                                var partsSalesOrderProcessDetail = new PartsSalesOrderProcessDetail {
                                    OrderProcessMethod = (int)DcsPartsSalesOrderProcessDetailProcessMethod.本部处理,
                                    WarehouseId = partsSalesOrder.WarehouseId,
                                    WarehouseCode = warehouse.Code,
                                    WarehouseName = warehouse.Name,
                                    SparePartId = partsSalesOrderDetail.SparePartId,
                                    SparePartCode = partsSalesOrderDetail.SparePartCode,
                                    SparePartName = partsSalesOrderDetail.SparePartName,
                                    MeasureUnit = partsSalesOrderDetail.MeasureUnit,
                                    IfDirectSupply = false,
                                    OrderedQuantity = partsSalesOrderDetail.OrderedQuantity,
                                    //OrderPrice = virtualPartsSalesPrice.Price,
                                    OrderProcessStatus = (warehousePartsStock == null ? 0 : warehousePartsStock.UsableQuantity) >= partsSalesOrderDetail.OrderedQuantity - (partsSalesOrderDetail.ApproveQuantity ?? 0) ? (int)DcsPartsSalesOrderProcessDetailOrderProcessStatus.满足 : (int)DcsPartsSalesOrderProcessDetailOrderProcessStatus.未满足,
                                    CurrentFulfilledQuantity = current,
                                };
                                if(partsSalesOrder.PartsSalesCategory.Name == "随车行") {
                                    partsSalesOrderProcessDetail.OrderPrice = partsSalesOrderDetail.OrderPrice;
                                } else {
                                    var virtualPartsSalesPrice = latestPriceSalesOrderDetails.FirstOrDefault(r => r.SparePartId == partsSalesOrderDetail.SparePartId);
                                    if(virtualPartsSalesPrice == null) {
                                        throw new ValidationException(string.Format("未找到配件编号{0}对应的销售价格", partsSalesOrderDetail.SparePartCode));
                                    }
                                    partsSalesOrderProcessDetail.OrderPrice = virtualPartsSalesPrice.Price;
                                }
                                partsSalesOrderProcess.PartsSalesOrderProcessDetails.Add(partsSalesOrderProcessDetail);
                            }

                        }
                        if(partsSalesOrder.PartsSalesCategory.Name != "随车行") {
                            var virtualPartsSalesPrice = latestPriceSalesOrderDetails.FirstOrDefault(r => r.SparePartId == partsSalesOrderDetail.SparePartId);
                            if(virtualPartsSalesPrice == null) {
                                throw new ValidationException(string.Format("未找到配件编号{0}对应的销售价格", partsSalesOrderDetail.SparePartCode));
                            }
                            partsSalesOrderDetail.OrderPrice = virtualPartsSalesPrice.Price;
                            partsSalesOrderDetail.OrderSum = virtualPartsSalesPrice.Price * partsSalesOrderDetail.OrderedQuantity;
                        }
                    }

                    foreach(var partsSalesOrderDetail in partsSalesOrder.PartsSalesOrderDetails) {
                        partsSalesOrderDetail.ApproveQuantity = partsSalesOrderProcess.PartsSalesOrderProcessDetails.Where(r => r.SparePartId == partsSalesOrderDetail.SparePartId).Sum(r => r.CurrentFulfilledQuantity) + (partsSalesOrderDetail.ApproveQuantity ?? 0);
                    }

                    partsSalesOrderProcess.CurrentFulfilledAmount = partsSalesOrderProcess.PartsSalesOrderProcessDetails.Sum(r => r.CurrentFulfilledQuantity * r.OrderPrice);

                    if(partsSalesOrderProcess.PartsSalesOrderProcessDetails.Any()) {
                        if(partsSalesOrderProcess.PartsSalesOrderProcessDetails.Any(r => r.OrderProcessStatus == (int)DcsPartsSalesOrderProcessDetailOrderProcessStatus.未满足)) {
                            partsSalesOrderProcess.BillStatusAfterProcess = (int)DcsPartsSalesOrderStatus.部分审批;
                            if(!isPartApproval) {
                                isPartApproval = true;
                            }
                        } else {
                            partsSalesOrderProcess.BillStatusAfterProcess = (int)DcsPartsSalesOrderStatus.审批完成;
                        }
                    } else
                        //当前没有任何处理清单
                        continue;

                    var customerAccount = DomainService.查询客户可用资金(partsSalesOrder.CustomerAccountId, null, null);

                    if(customerAccount == null)
                        throw new ValidationException(string.Format(ErrorStrings.PartsSalesOrderProcess_Validation2, partsSalesOrder.Code));

                    var salesUnit = ObjectContext.SalesUnits.FirstOrDefault(v => v.Id == partsSalesOrder.SalesUnitId && v.Status == (int)DcsMasterDataStatus.有效);
                    if(salesUnit == null)
                        throw new ValidationException(string.Format(ErrorStrings.PartsSalesOrderProcess_Validation9, partsSalesOrder.Code));
                    decimal rebateAmout = 0;

                    var PartsRebateAccount = ObjectContext.PartsRebateAccounts.FirstOrDefault(r => r.BranchId == partsSalesOrder.SalesUnitOwnerCompanyId && r.CustomerCompanyId == partsSalesOrder.SubmitCompanyId && r.AccountGroupId == salesUnit.AccountGroupId);
                    if(PartsRebateAccount != null) {
                        rebateAmout = PartsRebateAccount.AccountBalanceAmount;
                    }
                    if((customerAccount.UseablePosition + rebateAmout) < partsSalesOrderProcess.CurrentFulfilledAmount)
                        //当前账户可用额度不足
                        continue;

                    new CustomerAccountAch(this.DomainService).订单审批过账(partsSalesOrder.CustomerAccountId, partsSalesOrderProcess.CurrentFulfilledAmount);

                    var warehouseIds = partsSalesOrderProcess.PartsSalesOrderProcessDetails.Select(r => r.WarehouseId).Distinct().ToArray();
                    var warehouses = ObjectContext.Warehouses.Where(r => warehouseIds.Contains(r.Id) && r.Status != (int)DcsBaseDataStatus.作废).ToArray();
                    var invoiceReceiveCompany = ObjectContext.Companies.FirstOrDefault(r => r.Id == partsSalesOrder.InvoiceReceiveCompanyId && r.Status == (int)DcsMasterDataStatus.有效);
                    if(invoiceReceiveCompany == null)
                        //对应的企业不存在
                        throw new ValidationException(string.Format(ErrorStrings.PartsSalesOrderProcess_Validation10, partsSalesOrder.Code));

                    var agencySaleUnits = ObjectContext.SalesUnits.Where(r => r.OwnerCompanyId == invoiceReceiveCompany.Id && r.PartsSalesCategoryId == salesUnit.PartsSalesCategoryId && r.Status == (int)DcsMasterDataStatus.有效);
                    //var agencyWarehouse = ObjectContext.Warehouses.FirstOrDefault(v => ObjectContext.SalesUnitAffiWarehouses.Any(affi => affi.WarehouseId == v.Id && agencySaleUnits.Contains(affi.SalesUnit))); 此处获取错误，应该到销售订单里面获取仓库信息（当有多个仓库时，此方法就会出现错误）
                    var agencyWarehouse = ObjectContext.Warehouses.FirstOrDefault(v => v.Id == partsSalesOrder.ReceivingWarehouseId && v.Status == (int)DcsBaseDataStatus.有效);
                    var processDetailsByWarehouses = partsSalesOrderProcess.PartsSalesOrderProcessDetails.Where(r => r.OrderProcessMethod == (int)DcsPartsSalesOrderProcessDetailProcessMethod.本部处理).GroupBy(r => new {
                        r.WarehouseId
                    });
                    //生成出库计划单
                    foreach(var partsSalesOrderProcessDetail in processDetailsByWarehouses) {
                        var warehouse = warehouses.FirstOrDefault(r => r.Id == partsSalesOrderProcessDetail.Key.WarehouseId);
                        if(warehouse == null)
                            throw new ValidationException(string.Format(ErrorStrings.PartsSalesOrderProcess_Validation13, partsSalesOrderProcessDetail.First().WarehouseCode));
                        var userInfoCompany = ObjectContext.Companies.SingleOrDefault(r => r.Id == userInfo.EnterpriseId);
                        if(userInfoCompany == null)
                            throw new ValidationException(string.Format("Id为{0}的企业不存在", userInfo.EnterpriseId));

                        var partsOutboundPlan = new PartsOutboundPlan {
                            WarehouseId = partsSalesOrderProcessDetail.Key.WarehouseId ?? 0,
                            WarehouseName = partsSalesOrderProcessDetail.First().WarehouseName,
                            WarehouseCode = partsSalesOrderProcessDetail.First().WarehouseCode,
                            Status = (int)DcsPartsOutboundPlanStatus.新建,
                            StorageCompanyId = userInfo.EnterpriseId,
                            StorageCompanyCode = userInfo.EnterpriseCode,
                            StorageCompanyName = userInfo.EnterpriseName,
                            StorageCompanyType = userInfoCompany.Type,
                            BranchId = partsSalesOrder.BranchId,
                            BranchCode = partsSalesOrder.BranchCode,
                            BranchName = partsSalesOrder.BranchName,
                            CounterpartCompanyId = partsSalesOrder.InvoiceReceiveCompanyId,
                            CounterpartCompanyCode = partsSalesOrder.InvoiceReceiveCompanyCode,
                            CounterpartCompanyName = partsSalesOrder.InvoiceReceiveCompanyName,
                            ReceivingCompanyId = partsSalesOrder.ReceivingCompanyId,
                            ReceivingCompanyCode = partsSalesOrder.ReceivingCompanyCode,
                            ReceivingCompanyName = partsSalesOrder.ReceivingCompanyName,
                            PartsSalesCategoryId = salesUnit.PartsSalesCategoryId,
                            SourceId = partsSalesOrder.Id,
                            SourceCode = partsSalesOrder.Code,
                            OutboundType = (int)DcsPartsOutboundType.配件销售,
                            CustomerAccountId = partsSalesOrder.CustomerAccountId,
                            OriginalRequirementBillId = partsSalesOrder.Id,
                            OriginalRequirementBillCode = partsSalesOrder.Code,
                            OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.配件销售订单,
                            OrderApproveComment = partsSalesOrder.ApprovalComment,
                            ShippingMethod = partsSalesOrder.ShippingMethod,
                            IfWmsInterface = warehouse.WmsInterface,
                            PartsSalesOrderTypeId = partsSalesOrder.PartsSalesOrderTypeId,
                            PartsSalesOrderTypeName = partsSalesOrder.PartsSalesOrderTypeName,
                            SAPPurchasePlanCode = partsSalesOrder.OverseasDemandSheetNo,
                            ReceivingAddress = partsSalesOrder.ReceivingAddress
                        };

                        //企业类型=代理库,收货仓库相关三个字段赋值
                        if(invoiceReceiveCompany.Type == (int)DcsCompanyType.代理库 || invoiceReceiveCompany.Type == (int)DcsCompanyType.服务站兼代理库) {
                            if(agencyWarehouse == null)
                                throw new ValidationException(ErrorStrings.PartsSalesOrderProcess_Validation11);
                            partsOutboundPlan.ReceivingWarehouseId = agencyWarehouse.Id;
                            partsOutboundPlan.ReceivingWarehouseCode = agencyWarehouse.Code;
                            partsOutboundPlan.ReceivingWarehouseName = agencyWarehouse.Name;
                        }

                        var orderDetails = partsSalesOrderProcessDetail.Select(v => new {
                            SparePartId = v.NewPartId ?? v.SparePartId,
                            SparePartCode = v.NewPartId.HasValue ? v.NewPartCode : v.SparePartCode,
                            SparePartName = v.NewPartId.HasValue ? v.NewPartName : v.SparePartName,
                            PlannedAmount = v.CurrentFulfilledQuantity,
                            Price = v.OrderPrice
                        }).GroupBy(v => v.SparePartId).Select(v => new PartsOutboundPlanDetail {
                            SparePartId = v.Key,
                            SparePartCode = v.First().SparePartCode,
                            SparePartName = v.First().SparePartName,
                            PlannedAmount = v.Sum(v1 => v1.PlannedAmount),
                            Price = v.First().Price
                        });
                        foreach(var orderDetail in orderDetails)
                            partsOutboundPlan.PartsOutboundPlanDetails.Add(orderDetail);

                        partsOutBoundPlans.Add(partsOutboundPlan);
                        var temp = partsOutboundPlan.PartsOutboundPlanDetails.Any(p => p.PlannedAmount > 0);
                        if(temp) {
                            ObjectContext.AddToPartsOutboundPlans(partsOutboundPlan);
                        }

                    }
                    partsSalesOrder.TotalAmount = partsSalesOrder.PartsSalesOrderDetails.Sum(r => r.OrderSum);
                    partsSalesOrder.Status = partsSalesOrderProcess.BillStatusAfterProcess;

                    //isPartApproval ? (int)DcsPartsSalesOrderStatus.部分审批 : (int)DcsPartsSalesOrderStatus.审批完成;
                    partsSalesOrder.FirstApproveTime = DateTime.Now;
                    partsSalesOrder.Time = (partsSalesOrder.Time ?? 0) + 1;
                    if(partsSalesOrder.Status == (int)DcsPartsSalesOrderStatus.审批完成) {
                        partsSalesOrder.ApproverId = userInfo.Id;
                        partsSalesOrder.ApproverName = userInfo.Name;
                        partsSalesOrder.ApproveTime = partsSalesOrder.FirstApproveTime;
                    }
                    new PartsSalesOrderAch(this.DomainService).PMS_SYC_150_SendPMSUpdateOrderStatus_PS(partsSalesOrder);
                }
                DomainService.批量生成配件出库计划(partsOutBoundPlans.ToArray());
                ObjectContext.SaveChanges();
                return isPartApproval;
            } catch(OptimisticConcurrencyException) {
                throw new ValidationException("订单正在被他人审批，请重新查询后审批！");
            }
        }
        public void 校验直供清单(PartsSalesOrderProcess partsSalesOrderProcess) {
            // 查询仓库 id in 销售订单处理清单.出库仓库id数组且  销售订单处理清单.处理方式=供应商直供
            // 如果 仓库.是否wms接口=是 抛错，xxx仓库与wms接口，无法直供完成。
            //查询配件营销信息（配件id in  销售订单处理清单.配件id数组 销售订单处理清单.处理方式=供应商直供，配件销售类型id=配件销售订单.配件销售类型id，
            //配件销售订单.id=配件销售订单处理.原始销售订单ID，配件营销信息.状态=有效）
            //如果返回结果中配件营销信息.是否可直供=否 抛错“xxx配件信息不可直供，请维护其营销信息”，
            //如果配件处理单清单中配件不存在营销信息抛错：xxx配件营销信息不存在
            //查询配件计划价（配件id in  销售订单处理清单.配件id数组 销售订单处理清单.处理方式=供应商直供，配件销售类型id=配件销售订单.配件销售类型id，配件销售订单.id=配件销售订单处理.原始销售订单ID）
            //如果  销售订单处理清单查询的计划价存在空的情况，且处理方式=供应商直供 ，提示 xxx配件计划价不存在
            //查询配件与供应商关系（配件id in  销售订单处理清单.配件id数组 销售订单处理清单.处理方式=供应商直供，配件销售类型id=配件销售订单.配件销售类型id，配件销售订单.id=配件销售订单处理.原始销售订单ID，配件与供应商关系.状态=有效，配件与供应商关系.是否首选供应商=是）
            //如果配件处理清单中存在未查询到结果的数据，提示xxx配件不存在首选供应商
            var partsSalesOrder = this.ObjectContext.PartsSalesOrders.FirstOrDefault(r => r.Id == partsSalesOrderProcess.OriginalSalesOrderId);
            // var warehouseIds = partsSalesOrderProcess.PartsSalesOrderProcessDetails.Select(r => r.WarehouseId).ToArray();
            // var warehouses = this.ObjectContext.Warehouses.Where(r => warehouseIds.Contains(r.Id)).ToArray();
            var partsIds = partsSalesOrderProcess.PartsSalesOrderProcessDetails.Select(r => r.SparePartId).ToArray();
            var partsBranchs = this.ObjectContext.PartsBranches.Where(r => partsIds.Contains(r.PartId) && r.PartsSalesCategoryId == partsSalesOrder.SalesCategoryId).ToArray();
            var partsPlannedPrices = this.ObjectContext.PartsPlannedPrices.Where(r => partsIds.Contains(r.SparePartId) && r.PartsSalesCategoryId == partsSalesOrder.SalesCategoryId).ToArray();
            var partsSupplierRelations = this.ObjectContext.PartsSupplierRelations.Where(r => partsIds.Contains(r.PartId) && r.Status == (int)DcsBaseDataStatus.有效 && r.PartsSalesCategoryId == partsSalesOrder.SalesCategoryId && r.IsPrimary == true);
            foreach(var detail in partsSalesOrderProcess.PartsSalesOrderProcessDetails) {
                if(detail.OrderProcessMethod == (int)DcsPartsSalesOrderProcessDetailProcessMethod.供应商直发) {
                    //var warehouse = warehouses.FirstOrDefault(r => r.Id == detail.WarehouseId && r.WmsInterface == true);
                    //if(warehouse != null && warehouse.WmsInterface == true)
                    //    throw new ValidationException(string.Format("编号为{0}的仓库与wms接口，无法直供完成", warehouse.Code));
                    var partsBranch = partsBranchs.FirstOrDefault(r => r.PartId == detail.SparePartId && r.Status == (int)DcsBaseDataStatus.有效);
                    if(partsBranch == null) {
                        throw new ValidationException(string.Format("{0}配件营销信息不存在", detail.SparePartCode));
                    } else {
                        if(partsBranch != null && partsBranch.IsDirectSupply == false)
                            throw new ValidationException(string.Format("{0}配件信息不可直供，请维护其营销信息", partsBranch.PartCode));
                    }
                    var partsPlannedPrice = partsPlannedPrices.FirstOrDefault(r => r.SparePartId == detail.SparePartId);
                    if(partsPlannedPrice == null)
                        throw new ValidationException(string.Format("{0}配件计划价不存在", detail.SparePartCode));
                    var partsSupplierRelation = partsSupplierRelations.FirstOrDefault(r => r.PartId == detail.SparePartId);
                    if(partsSupplierRelation == null)
                        throw new ValidationException(string.Format("配件不存在首选供应商", detail.SparePartCode));
                }
            }
        }

        public void 审批配件公司销售订单(PartsSalesOrderProcess partsSalesOrderProcess) {
            using(var transaction = new TransactionScope()) {
                //重新获取价格，调用服务【虚拟配件销售价.根据配件清单获取销售价格】   
                //（参数：虚拟配件销售价.订单类型Id=配件销售订单.配件销售订单类型Id,虚拟配件销售价.客户Id=配件销售订单.提报单位Id,虚拟配件销售价.配件Id数组=配件销售订单处理清单.配件Id 数组）   
                //根据返回的价格更新订单处理清单.订货价格 ,同时更新配件销售订单清单.订货价格   
                int i = 0;
                Warehouse virtualWarehouse = null;
                PartsPurchaseOrderType partsPurchaseOrderType = null;
                //PartsSupplierRelation[] partsSupplierRelation = null;
                PartsSupplierRelation[] partsSupplierRelation1 = null;
                //检验区库区库位
                //WarehouseArea[] warehouseAreaes = null;
                //保管区库区库位
                //WarehouseArea[] warehouseAreaesNew = null;
                //PartsPlannedPrice[] partsPlannedPrices = null;
                string receivingCompanyCode = null;
                Company[] companys = null;
                Company newcompany = null;
                var partsSalesOrder = this.ObjectContext.PartsSalesOrders.Include("PartsSalesOrderDetails").FirstOrDefault(r => r.Id == partsSalesOrderProcess.OriginalSalesOrderId);//partsSalesOrderProcess.PartsSalesOrder;
                if(partsSalesOrder == null)
                    throw new ValidationException(string.Format("配件销售订单处理编号为{0}的单据不存在对应的配件销售订单", partsSalesOrderProcess.Code));
                var customerAccountnew = new CustomerAccountAch(this.DomainService).GetCustomerAccountsWithLock(ObjectContext.CustomerAccounts.Where(r => r.Id == partsSalesOrder.CustomerAccountId && r.Status != (int)DcsMasterDataStatus.作废)).ToArray();
                if(!customerAccountnew.Any() || customerAccountnew.Count() > 1)
                    throw new ValidationException(ErrorStrings.CustomerAccount_Validation3);
                var warehouseIds = partsSalesOrderProcess.PartsSalesOrderProcessDetails.Select(r => r.WarehouseId.HasValue ? r.WarehouseId.Value : 0).ToArray();
                var receivingWarehouse = this.ObjectContext.Warehouses.FirstOrDefault(r => r.Id == partsSalesOrder.ReceivingWarehouseId);
                //if(receivingWarehouse == null)
                //    throw new ValidationException("收货仓库不存在！");
                var sparePartIds = partsSalesOrderProcess.PartsSalesOrderProcessDetails.Select(r => r.SparePartId).ToArray();
                var sparepartsIds = partsSalesOrder.PartsSalesOrderDetails.Select(r => r.SparePartId).ToArray();
                var supplierCompanyIds = partsSalesOrderProcess.PartsSalesOrderProcessDetails.Select(r => r.SupplierCompanyId).ToArray();
                if(partsSalesOrderProcess.PartsSalesOrderProcessDetails.Any(r => r.OrderProcessMethod == (int)DcsPartsSalesOrderProcessDetailProcessMethod.供应商直发)) {
                    virtualWarehouse = this.ObjectContext.Warehouses.FirstOrDefault(r => r.Type == (int)DcsWarehouseType.虚拟库 && r.StorageCompanyId == partsSalesOrder.SalesUnitOwnerCompanyId && r.BranchId == partsSalesOrder.BranchId && r.Status == (int)DcsBaseDataStatus.有效);
                    if(virtualWarehouse == null)
                        throw new ValidationException(string.Format("分公司编号为{0}的企业不存在有效的虚拟库", partsSalesOrder.BranchCode));
                    var temp = from detail in partsSalesOrderProcess.PartsSalesOrderProcessDetails.Where(r => r.OrderProcessMethod == (int)DcsPartsSalesOrderProcessDetailProcessMethod.供应商直发)
                               join partsPlannedPrice in this.ObjectContext.PartsPlannedPrices.Where(r => r.OwnerCompanyId == virtualWarehouse.StorageCompanyId && r.PartsSalesCategoryId == 6) on detail.SparePartId equals partsPlannedPrice.SparePartId
                               join partsSupplierRelation2 in this.ObjectContext.PartsSupplierRelations.Where(r => r.PartsSalesCategoryId == partsSalesOrder.SalesCategoryId && r.Status == (int)DcsBaseDataStatus.有效 && r.IsPrimary == true) on detail.SparePartId equals partsSupplierRelation2.PartId
                               select new {
                                   detail.SparePartId,
                                   partsPlannedPrice.PlannedPrice,
                                   partsSupplierRelation2.SupplierId
                               };
                    var a = temp.ToArray();
                    newcompany = this.ObjectContext.Companies.FirstOrDefault(r => r.Id == virtualWarehouse.StorageCompanyId);
                    ////配件采购订单类型（名称=紧急采购订单 配件销售类型Id=6）查询不到报错：对应品牌xxx的紧急采购订单类型不存在
                    var partssalescategory = this.ObjectContext.PartsSalesCategories.FirstOrDefault(r => r.Id == 6);
                    partsPurchaseOrderType = this.ObjectContext.PartsPurchaseOrderTypes.FirstOrDefault(r => r.Name == "紧急采购订单" && r.PartsSalesCategoryId == 6 && r.Status == (int)DcsBaseDataStatus.有效);
                    if(partsPurchaseOrderType == null)
                        throw new ValidationException(string.Format("对应品牌{0}的紧急采购订单类型不存在", partssalescategory.Name));
                    var supplierIds = a.Select(r => r.SupplierId).ToArray();
                    companys = this.ObjectContext.Companies.Where(r => supplierIds.Contains(r.Id)).ToArray();
                    //var sparePartProcessMethodIds = partsSalesOrderProcess.PartsSalesOrderProcessDetails.Where(r => r.OrderProcessMethod == (int)DcsPartsSalesOrderProcessDetailProcessMethod.供应商直发).Select(r => r.SparePartId).ToArray();
                    //partsSupplierRelation = this.ObjectContext.PartsSupplierRelations.Where(r => sparePartProcessMethodIds.Contains(r.PartId) && r.PartsSalesCategoryId == partsSalesOrder.SalesCategoryId && r.Status == (int)DcsBaseDataStatus.有效 && r.IsPrimary == true).ToArray();
                    //var supplierIds = partsSupplierRelation.Select(r => r.SupplierId).ToArray();
                    //companys = this.ObjectContext.Companies.Where(r => supplierIds.Contains(r.Id)).ToArray();
                    ////查询 配件与供应商关系，条件:配件与供应商关系.营销分公司id=配件销售订单.营销分公司Id & 
                    ////                                                    配件与供应商关系.配件Id=配件销售订单处理清单.配件Id &
                    ////                                                    配件与供应商关系.供应商Id=配件销售订单处理清单.供货单位Id &配件销售类型Id=6
                    partsSupplierRelation1 = this.ObjectContext.PartsSupplierRelations.Where(r => r.BranchId == partsSalesOrder.BranchId && sparePartIds.Contains(r.PartId) && supplierCompanyIds.Contains(r.SupplierId) && r.PartsSalesCategoryId == 6).ToArray();
                    receivingCompanyCode = this.ObjectContext.Companies.FirstOrDefault(r => r.Id == partsSalesOrder.SubmitCompanyId).Code;
                    //库区库位.库区库位类型=库位，库区用途=检验区，仓库ID=配件入库检验单.配件仓库Id
                    var warehouseAreaCategoryIds = this.ObjectContext.WarehouseAreaCategories.Where(r => r.Category == (int)DcsAreaType.检验区).Select(r => r.Id).ToArray();
                    var warehouseAreaes = this.ObjectContext.WarehouseAreas.Where(r => r.AreaKind == (int)DcsAreaKind.库位 && warehouseAreaCategoryIds.Contains(r.AreaCategoryId ?? 0) && r.WarehouseId == virtualWarehouse.Id).ToArray();
                    if(!warehouseAreaes.Any())
                        throw new ValidationException(string.Format("编号为{0}的仓库库区库位信息不存在对应的检验区", virtualWarehouse.Code));
                    //库区库位.库区库位类型=库位，库区用途=保管区，仓库ID=配件入库检验单.配件仓库Id
                    var warehouseAreaCategoryIdNews = this.ObjectContext.WarehouseAreaCategories.Where(r => r.Category == (int)DcsAreaType.保管区).Select(r => r.Id).ToArray();
                    var warehouseAreaesNew = this.ObjectContext.WarehouseAreas.Where(r => r.AreaKind == (int)DcsAreaKind.库位 && warehouseAreaCategoryIdNews.Contains(r.AreaCategoryId ?? 0) && r.WarehouseId == virtualWarehouse.Id).ToArray();
                    if(!warehouseAreaesNew.Any())
                        throw new ValidationException(string.Format("编号为{0}的仓库库区库位信息不存在对应的保管区", virtualWarehouse.Code));
                    ////查询成本价（隶属企业Id=配件入库检验单.仓储企业id，配件销售类型Id=配件入库检验单.配件销售类型Id,配件Id=配件入库检验单清单.配件Id）
                    ////获取生成的配件入库计划清单中配件的所有成本价
                    //partsPlannedPrices = this.ObjectContext.PartsPlannedPrices.Where(r => r.OwnerCompanyId == virtualWarehouse.StorageCompanyId && r.PartsSalesCategoryId == 6 && sparePartProcessMethodIds.Contains(r.SparePartId)).ToArray();

                    var processDetailWarehouse = this.ObjectContext.Warehouses.Where(r => warehouseIds.Contains(r.Id)).ToArray();
                    var virtualPartsSalesPrices = new VirtualPartsSalesPriceAch(this.DomainService).根据配件清单获取销售价格(partsSalesOrder.PartsSalesOrderTypeId, partsSalesOrder.SubmitCompanyId, sparepartsIds).ToArray();
                    foreach(var detail in partsSalesOrderProcess.PartsSalesOrderProcessDetails) {
                        var virtualPartsSalesPrice = virtualPartsSalesPrices.FirstOrDefault(r => r.SparePartId == detail.SparePartId);
                        if(virtualPartsSalesPrice == null)
                            throw new ValidationException(string.Format(ErrorStrings.PartsSalesOrder_Validation3, partsSalesOrder.PartsSalesOrderTypeName, detail.SparePartCode));
                        detail.OrderPrice = virtualPartsSalesPrice.Price;
                    }
                    foreach(var detail in partsSalesOrder.PartsSalesOrderDetails) {
                        var virtualPartsSalesPrice = virtualPartsSalesPrices.FirstOrDefault(r => r.SparePartId == detail.SparePartId);
                        if(virtualPartsSalesPrice == null)
                            throw new ValidationException(string.Format(ErrorStrings.PartsSalesOrder_Validation3, partsSalesOrder.PartsSalesOrderTypeName, detail.SparePartCode));
                        detail.OrderPrice = virtualPartsSalesPrice.Price;
                    }
                    //1）调用【客户账户.查客户可用资金】
                    //    条件：客户账户.Id=配件销售订单.客户账户Id
                    //    配件销售订单处理.本次满足金额小于客户账户.可用额度
                    //    系统提示：当前账户可用额度不足
                    var customerAccount = DomainService.查询客户可用资金(partsSalesOrder.CustomerAccountId, null, null);
                    if(partsSalesOrderProcess.CurrentFulfilledAmount > customerAccount.UseablePosition)
                        throw new ValidationException("当前账户可用额度不足");
                    //2）更新配件销售订单,同时更新配件销售订单处理的处理后状态字段
                    //  a.配件销售订单赋值：
                    //    针对配件销售订单处理清单中的订单处理方式 in （本部处理，供应商直发）的行，逐个更改配件销售订单清单的审批数量
                    //    配件销售订单清单.审批数量=配件销售订单清单.审批数量+配件销售处理清单. 本次满足数量
                    //    如果所有配件销售订单清单.审批数量=配件销售订单清单.订货数量        配件销售订单.状态=审批完成  否则  配件销售订单.状态=部分审批 
                    //  b.配件销售订单处理.处理后单据状态=配件销售订单.状态；
                    int[] methods = new int[]{
                        (int)DcsPartsSalesOrderProcessDetailProcessMethod.本部处理,
                        (int)DcsPartsSalesOrderProcessDetailProcessMethod.供应商直发
                      };
                    var partsSalesOrderProcessDetails = partsSalesOrderProcess.PartsSalesOrderProcessDetails.Where(r => methods.Contains(r.OrderProcessMethod)).ToArray();
                    foreach(var detail in partsSalesOrder.PartsSalesOrderDetails) {
                        var partsSalesOrderProcessDetail = partsSalesOrderProcessDetails.FirstOrDefault(r => r.SparePartId == detail.SparePartId);
                        if(partsSalesOrderProcessDetail == null)
                            continue;
                        detail.ApproveQuantity = (detail.ApproveQuantity ?? 0) + partsSalesOrderProcessDetail.CurrentFulfilledQuantity;
                    }
                    if(partsSalesOrder.PartsSalesOrderDetails.Any(r => r.ApproveQuantity != r.OrderedQuantity)) {
                        partsSalesOrder.Status = (int)DcsPartsSalesOrderStatus.部分审批;
                    } else {
                        partsSalesOrder.Status = (int)DcsPartsSalesOrderStatus.审批完成;
                    }
                    partsSalesOrderProcess.BillStatusAfterProcess = partsSalesOrder.Status;
                    InsertToDatabase(partsSalesOrderProcess);
                    this.InsertPartsSalesOrderProcessValidate(partsSalesOrderProcess);
                    //UpdateToDatabase(partsSalesOrderProcess);
                    UpdateToDatabase(partsSalesOrder);
                    //UpdatePartsSalesOrderProcessValidate(partsSalesOrderProcess);
                    new PartsSalesOrderAch(this.DomainService).UpdatePartsSalesOrderValidate(partsSalesOrder);

                    //3）
                    //更新客户账户（客户账户.id=配件销售订单.客户账户id）审批待发金额=审批待发金额+sum（配件销售订单处理清单.订货价格*配件销售订单处理清单.本次满足数量 ，配件销售订单处理清单.订单处理方式=本部满足）
                    customerAccountnew.First().PendingAmount = customerAccountnew.First().PendingAmount + partsSalesOrderProcess.PartsSalesOrderProcessDetails.Where(ex => ex.OrderProcessMethod == (int)DcsPartsSalesOrderProcessDetailProcessMethod.本部处理).Sum(r => r.OrderPrice * r.CurrentFulfilledQuantity);
                    UpdateToDatabase(customerAccountnew.First());
                    //4）根据配件销售订单处理清单订单处理方式，进行数据拆分。
                    //    a)订单处理方式=未处理    不做任何处理
                    #region 订单处理方式=供应商直供
                    foreach(var detail in partsSalesOrderProcess.PartsSalesOrderProcessDetails.Where(r => r.OrderProcessMethod == (int)DcsPartsSalesOrderProcessDetailProcessMethod.供应商直发)) {
                        var company = companys.FirstOrDefault(r => r.Id == a.FirstOrDefault(ex => ex.SparePartId == detail.SparePartId).SupplierId);
                        detail.SupplierCompanyId = company.Id;
                        detail.SupplierCompanyCode = company.Code;
                        detail.SupplierCompanyName = company.Name;
                    }
                    foreach(var detail in partsSalesOrderProcess.PartsSalesOrderProcessDetails.Where(r => r.OrderProcessMethod == (int)DcsPartsSalesOrderProcessDetailProcessMethod.供应商直发).GroupBy(r => r.SupplierCompanyId)) {
                        #region 生成采购订单
                        PartsPurchaseOrder partsPurchaseOrder = new PartsPurchaseOrder();
                        partsPurchaseOrder.Id = i++;
                        partsPurchaseOrder.Code = GlobalVar.ASSIGNED_BY_SERVER;
                        partsPurchaseOrder.BranchId = partsSalesOrder.BranchId;
                        partsPurchaseOrder.BranchCode = partsSalesOrder.BranchCode;
                        partsPurchaseOrder.BranchName = partsSalesOrder.BranchName;
                        partsPurchaseOrder.WarehouseId = virtualWarehouse.Id;
                        partsPurchaseOrder.DirectRecWarehouseId = partsSalesOrder.ReceivingWarehouseId;
                        partsPurchaseOrder.DirectRecWarehouseName = partsSalesOrder.ReceivingWarehouseName;
                        partsPurchaseOrder.WarehouseName = virtualWarehouse.Name;
                        partsPurchaseOrder.ReceivingCompanyId = partsSalesOrder.SubmitCompanyId;
                        partsPurchaseOrder.ReceivingCompanyName = partsSalesOrder.SubmitCompanyName;
                        partsPurchaseOrder.ReceivingAddress = partsSalesOrder.ReceivingAddress;
                        partsPurchaseOrder.PartsSupplierId = detail.Key.Value;
                        partsPurchaseOrder.PartsSupplierCode = detail.FirstOrDefault().SupplierCompanyCode;
                        partsPurchaseOrder.PartsSupplierName = detail.FirstOrDefault().SupplierCompanyName;
                        partsPurchaseOrder.IfDirectProvision = true;
                        partsPurchaseOrder.OriginalRequirementBillId = partsSalesOrder.Id;
                        partsPurchaseOrder.OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.配件销售订单;
                        partsPurchaseOrder.OriginalRequirementBillCode = partsSalesOrder.Code;
                        partsPurchaseOrder.PartsPurchaseOrderTypeId = partsPurchaseOrderType.Id;
                        partsPurchaseOrder.PartsSalesCategoryId = 6;
                        partsPurchaseOrder.RequestedDeliveryTime = partsSalesOrder.RequestedDeliveryTime ?? DateTime.Now;
                        partsPurchaseOrder.ShippingMethod = partsSalesOrderProcess.ShippingMethod;
                        partsPurchaseOrder.Status = (int)DcsPartsPurchaseOrderStatus.发运完毕;
                        partsPurchaseOrder.ConfirmationRemark = " ";
                        partsPurchaseOrder.Remark = partsSalesOrder.Remark + "联系人：" + partsSalesOrder.ContactPerson + ",联系人电话：" + partsSalesOrder.ContactPhone;
                        var prices = DomainService.查询配件采购价格按天(DateTime.Now, detail.Key.Value, sparePartIds, 6);
                        foreach(var item in detail) {
                            if(prices.FirstOrDefault(r => r.PartId == item.SparePartId) == null)
                                throw new ValidationException(string.Format("编号为{0}配件，查询配件采购价格（按天）的单据不存在,请维护！"));
                            PartsPurchaseOrderDetail partsPurchaseOrderDetail = new PartsPurchaseOrderDetail();
                            partsPurchaseOrderDetail.PartsPurchaseOrderId = partsPurchaseOrder.Id;
                            partsPurchaseOrderDetail.SparePartId = item.SparePartId;
                            partsPurchaseOrderDetail.SparePartCode = item.SparePartCode;
                            partsPurchaseOrderDetail.SparePartName = item.SparePartName;
                            partsPurchaseOrderDetail.SupplierPartCode = partsSupplierRelation1.FirstOrDefault(r => r.PartId == item.SparePartId && r.SupplierId == item.SupplierCompanyId) == null ? string.Empty : partsSupplierRelation1.FirstOrDefault(r => r.PartId == item.SparePartId && r.SupplierId == item.SupplierCompanyId).SupplierPartCode;
                            partsPurchaseOrderDetail.PromisedDeliveryTime = null;
                            partsPurchaseOrderDetail.UnitPrice = prices.FirstOrDefault(r => r.PartId == item.SparePartId).Price;
                            partsPurchaseOrderDetail.OrderAmount = item.CurrentFulfilledQuantity;
                            partsPurchaseOrderDetail.ConfirmedAmount = item.CurrentFulfilledQuantity;
                            partsPurchaseOrderDetail.ShippingAmount = item.CurrentFulfilledQuantity;
                            partsPurchaseOrderDetail.MeasureUnit = null;
                            partsPurchaseOrderDetail.PackingAmount = null;
                            partsPurchaseOrderDetail.PackingSpecification = null;
                            partsPurchaseOrderDetail.Remark = null;
                            partsPurchaseOrderDetail.ConfirmationRemark = null;
                            partsPurchaseOrderDetail.DirectOrderAmount = item.CurrentFulfilledQuantity;
                            partsPurchaseOrder.PartsPurchaseOrderDetails.Add(partsPurchaseOrderDetail);
                        }
                        partsPurchaseOrder.TotalAmount = partsPurchaseOrder.PartsPurchaseOrderDetails.Sum(r => r.UnitPrice * r.OrderAmount);
                        InsertToDatabase(partsPurchaseOrder);
                        new PartsPurchaseOrderAch(this.DomainService).InsertPartsPurchaseOrderValidate(partsPurchaseOrder);
                        #endregion
                        #region 生成供应商发运单
                        SupplierShippingOrder supplierShippingOrder = new SupplierShippingOrder();
                        supplierShippingOrder.Id = i++;
                        supplierShippingOrder.Code = GlobalVar.ASSIGNED_BY_SERVER;
                        supplierShippingOrder.PartsSupplierId = partsPurchaseOrder.PartsSupplierId;
                        supplierShippingOrder.PartsSupplierCode = partsPurchaseOrder.PartsSupplierCode;
                        supplierShippingOrder.PartsSupplierName = partsPurchaseOrder.PartsSupplierName;
                        supplierShippingOrder.IfDirectProvision = true;
                        supplierShippingOrder.BranchId = partsPurchaseOrder.BranchId;
                        supplierShippingOrder.BranchCode = partsPurchaseOrder.BranchCode;
                        supplierShippingOrder.BranchName = partsPurchaseOrder.BranchName;
                        supplierShippingOrder.PartsSalesCategoryId = partsPurchaseOrder.PartsSalesCategoryId;
                        supplierShippingOrder.PartsSalesCategoryName = partssalescategory.Name;
                        supplierShippingOrder.OriginalRequirementBillId = partsPurchaseOrder.OriginalRequirementBillId ?? 0;
                        supplierShippingOrder.OriginalRequirementBillType = partsPurchaseOrder.OriginalRequirementBillType ?? 0;
                        supplierShippingOrder.OriginalRequirementBillCode = partsPurchaseOrder.OriginalRequirementBillCode;
                        supplierShippingOrder.ReceivingWarehouseId = partsPurchaseOrder.WarehouseId;
                        supplierShippingOrder.ReceivingWarehouseName = partsPurchaseOrder.WarehouseName;
                        supplierShippingOrder.ReceivingCompanyId = partsPurchaseOrder.ReceivingCompanyId;
                        supplierShippingOrder.ReceivingCompanyName = partsPurchaseOrder.ReceivingCompanyName;
                        supplierShippingOrder.ReceivingCompanyCode = receivingCompanyCode;
                        supplierShippingOrder.ShippingMethod = partsPurchaseOrder.ShippingMethod ?? 0;
                        supplierShippingOrder.Status = (int)DcsSupplierShippingOrderStatus.收货确认;
                        supplierShippingOrder.RequestedDeliveryTime = partsPurchaseOrder.RequestedDeliveryTime;
                        supplierShippingOrder.PlanDeliveryTime = partsPurchaseOrder.RequestedDeliveryTime;
                        supplierShippingOrder.Remark = partsPurchaseOrder.Remark;
                        foreach(var item in partsPurchaseOrder.PartsPurchaseOrderDetails) {
                            SupplierShippingDetail supplierShippingDetail = new SupplierShippingDetail();
                            supplierShippingDetail.Id = supplierShippingOrder.Id;
                            supplierShippingDetail.SparePartId = item.SparePartId;
                            supplierShippingDetail.SparePartCode = item.SparePartCode;
                            supplierShippingDetail.SparePartName = item.SparePartName;
                            supplierShippingDetail.Quantity = item.OrderAmount;
                            supplierShippingDetail.UnitPrice = item.UnitPrice;
                            supplierShippingOrder.SupplierShippingDetails.Add(supplierShippingDetail);
                        }
                        InsertToDatabase(supplierShippingOrder);
                        new SupplierShippingOrderAch(this.DomainService).InsertSupplierShippingOrderValidate(supplierShippingOrder);
                        #endregion
                        #region 新增配件物流批次、配件物流批次单据清单、配件物流批次配件清单
                        foreach(var item in partsPurchaseOrder.PartsPurchaseOrderDetails) {
                            PartsLogisticBatch partsLogisticBatch = new PartsLogisticBatch();
                            partsLogisticBatch.Id = i++;
                            partsLogisticBatch.CompanyId = supplierShippingOrder.BranchId;
                            partsLogisticBatch.Status = (int)DcsPartsLogisticBatchStatus.完成;
                            partsLogisticBatch.ShippingStatus = (int)DcsPartsLogisticBatchShippingStatus.供应商发运;
                            partsLogisticBatch.SourceId = item.Id;
                            partsLogisticBatch.SourceType = (int)DcsPartsLogisticBatchSourceType.配件采购订单;
                            var partsLogisticBatchBillDetail = new PartsLogisticBatchBillDetail {
                                PartsLogisticBatchId = partsLogisticBatch.Id,
                                BillId = supplierShippingOrder.Id,
                                BillType = (int)DcsPartsLogisticBatchBillDetailBillType.供应商发运单
                            };
                            partsLogisticBatch.PartsLogisticBatchBillDetails.Add(partsLogisticBatchBillDetail);
                            foreach(var itemnew in supplierShippingOrder.SupplierShippingDetails) {
                                var partsLogisticBatchItemDetail = new PartsLogisticBatchItemDetail {
                                    PartsLogisticBatchId = partsLogisticBatch.Id,
                                    CounterpartCompanyId = supplierShippingOrder.PartsSupplierId,
                                    ReceivingCompanyId = supplierShippingOrder.ReceivingCompanyId ?? 0,
                                    ShippingCompanyId = supplierShippingOrder.PartsSupplierId,
                                    SparePartId = itemnew.SparePartId,
                                    SparePartCode = itemnew.SparePartCode,
                                    SparePartName = itemnew.SparePartName,
                                    InboundAmount = itemnew.ConfirmedAmount
                                };
                                partsLogisticBatch.PartsLogisticBatchItemDetails.Add(partsLogisticBatchItemDetail);
                            }
                            InsertToDatabase(partsLogisticBatch);
                            new PartsLogisticBatchAch(this.DomainService).InsertPartsLogisticBatchValidate(partsLogisticBatch);
                        }
                        #endregion
                        #region 生成入库计划单、生成入库计划单清单
                        #region 生成入库计划单
                        #endregion
                        PartsInboundPlan partsInboundPlan = new PartsInboundPlan();
                        partsInboundPlan.Id = i++;
                        partsInboundPlan.Code = GlobalVar.ASSIGNED_BY_SERVER;
                        partsInboundPlan.WarehouseId = partsPurchaseOrder.WarehouseId;
                        partsInboundPlan.WarehouseName = partsPurchaseOrder.WarehouseName;
                        partsInboundPlan.WarehouseCode = virtualWarehouse.Code;
                        partsInboundPlan.StorageCompanyId = virtualWarehouse.StorageCompanyId;
                        partsInboundPlan.StorageCompanyCode = newcompany.Code;
                        partsInboundPlan.StorageCompanyName = newcompany.Name;
                        partsInboundPlan.StorageCompanyType = newcompany.Type;
                        partsInboundPlan.BranchId = partsPurchaseOrder.BranchId;
                        partsInboundPlan.BranchCode = partsPurchaseOrder.BranchCode;
                        partsInboundPlan.BranchName = partsPurchaseOrder.BranchName;
                        partsInboundPlan.PartsSalesCategoryId = partsPurchaseOrder.PartsSalesCategoryId;
                        partsInboundPlan.CounterpartCompanyId = partsPurchaseOrder.PartsSupplierId;
                        partsInboundPlan.CounterpartCompanyCode = partsPurchaseOrder.PartsSupplierCode;
                        partsInboundPlan.CounterpartCompanyName = partsPurchaseOrder.PartsSupplierName;
                        partsInboundPlan.SourceId = supplierShippingOrder.Id;
                        partsInboundPlan.SourceCode = supplierShippingOrder.Code;
                        partsInboundPlan.InboundType = (int)DcsPartsInboundType.配件采购;
                        partsInboundPlan.OriginalRequirementBillId = supplierShippingOrder.OriginalRequirementBillId;
                        partsInboundPlan.OriginalRequirementBillType = supplierShippingOrder.OriginalRequirementBillType;
                        partsInboundPlan.OriginalRequirementBillCode = supplierShippingOrder.OriginalRequirementBillCode;
                        partsInboundPlan.Status = (int)DcsPartsInboundPlanStatus.检验完成;
                        partsInboundPlan.IfWmsInterface = virtualWarehouse.WmsInterface;
                        partsInboundPlan.Remark = partsPurchaseOrder.Remark;
                        foreach(var item in partsPurchaseOrder.PartsPurchaseOrderDetails) {
                            PartsInboundPlanDetail partsInboundPlanDetail = new PartsInboundPlanDetail();
                            partsInboundPlanDetail.PartsInboundPlanId = partsInboundPlan.Id;
                            partsInboundPlanDetail.SparePartId = item.SparePartId;
                            partsInboundPlanDetail.SparePartCode = item.SparePartCode;
                            partsInboundPlanDetail.SparePartName = item.SparePartName;
                            partsInboundPlanDetail.PlannedAmount = item.OrderAmount;
                            partsInboundPlanDetail.InspectedQuantity = item.OrderAmount;
                            partsInboundPlanDetail.Price = item.UnitPrice;
                            partsInboundPlan.PartsInboundPlanDetails.Add(partsInboundPlanDetail);
                        }
                        InsertToDatabase(partsInboundPlan);
                        new PartsInboundPlanAch(this.DomainService).InsertPartsInboundPlanValidate(partsInboundPlan);
                        #endregion
                        #region 生成配件入库检验单、配件入库检验单清单 赋值逻辑
                        PartsInboundCheckBill partsInboundCheckBill = new PartsInboundCheckBill();
                        partsInboundCheckBill.Id = i++;
                        partsInboundCheckBill.Code = GlobalVar.ASSIGNED_BY_SERVER;
                        partsInboundCheckBill.PartsInboundPlanId = partsInboundPlan.Id;
                        partsInboundCheckBill.WarehouseId = partsInboundPlan.WarehouseId;
                        partsInboundCheckBill.WarehouseCode = partsInboundPlan.WarehouseCode;
                        partsInboundCheckBill.WarehouseName = partsInboundPlan.WarehouseName;
                        partsInboundCheckBill.StorageCompanyId = partsInboundPlan.StorageCompanyId;
                        partsInboundCheckBill.StorageCompanyCode = partsInboundPlan.StorageCompanyCode;
                        partsInboundCheckBill.StorageCompanyName = partsInboundPlan.StorageCompanyName;
                        partsInboundCheckBill.StorageCompanyType = partsInboundPlan.StorageCompanyType;
                        partsInboundCheckBill.PartsSalesCategoryId = partsInboundPlan.PartsSalesCategoryId;
                        partsInboundCheckBill.BranchId = partsInboundPlan.BranchId;
                        partsInboundCheckBill.BranchCode = partsInboundPlan.BranchCode;
                        partsInboundCheckBill.BranchName = partsInboundPlan.BranchName;
                        partsInboundCheckBill.CounterpartCompanyId = partsInboundPlan.CounterpartCompanyId;
                        partsInboundCheckBill.CounterpartCompanyCode = partsInboundPlan.CounterpartCompanyCode;
                        partsInboundCheckBill.CounterpartCompanyName = partsInboundPlan.CounterpartCompanyName;
                        partsInboundCheckBill.InboundType = (int)DcsPartsInboundType.配件采购;
                        partsInboundCheckBill.OriginalRequirementBillId = partsInboundPlan.OriginalRequirementBillId;
                        partsInboundCheckBill.OriginalRequirementBillType = partsInboundPlan.OriginalRequirementBillType;
                        partsInboundCheckBill.OriginalRequirementBillCode = partsInboundPlan.OriginalRequirementBillCode;
                        partsInboundCheckBill.Status = (int)DcsPartsInboundCheckBillStatus.上架完成;
                        partsInboundCheckBill.SettlementStatus = (int)DcsPartsSettlementStatus.待结算;
                        var warehouseAreae = warehouseAreaes.OrderBy(r => Guid.NewGuid()).FirstOrDefault(r => r.WarehouseId == partsInboundPlan.WarehouseId);
                        foreach(var item in partsInboundPlan.PartsInboundPlanDetails) {
                            PartsInboundCheckBillDetail partsInboundCheckBillDetail = new PartsInboundCheckBillDetail();
                            partsInboundCheckBillDetail.PartsInboundCheckBillId = partsInboundCheckBill.Id;
                            partsInboundCheckBillDetail.SparePartId = item.SparePartId;
                            partsInboundCheckBillDetail.SparePartCode = item.SparePartCode;
                            partsInboundCheckBillDetail.SparePartName = item.SparePartName;
                            partsInboundCheckBillDetail.WarehouseAreaId = warehouseAreae.Id;
                            partsInboundCheckBillDetail.WarehouseAreaCode = warehouseAreae.Code;
                            partsInboundCheckBillDetail.InspectedQuantity = item.InspectedQuantity ?? 0;
                            partsInboundCheckBillDetail.SettlementPrice = item.Price;
                            partsInboundCheckBillDetail.CostPrice = a.FirstOrDefault(r => r.SparePartId == item.SparePartId).PlannedPrice;
                            partsInboundCheckBillDetail.POCode = item.POCode;
                            partsInboundCheckBill.PartsInboundCheckBillDetails.Add(partsInboundCheckBillDetail);
                        }
                        InsertToDatabase(partsInboundCheckBill);
                        new PartsInboundCheckBillAch(this.DomainService).InsertPartsInboundCheckBillValidate(partsInboundCheckBill);
                        #endregion
                        #region  生成配件移库单、配件移库单清单 赋值逻辑
                        PartsShiftOrder partsShiftOrder = new PartsShiftOrder();
                        partsShiftOrder.Id = i++;
                        partsShiftOrder.Code = GlobalVar.ASSIGNED_BY_SERVER;
                        partsShiftOrder.BranchId = partsInboundCheckBill.BranchId;
                        partsShiftOrder.WarehouseId = partsInboundCheckBill.WarehouseId;
                        partsShiftOrder.WarehouseCode = partsInboundCheckBill.WarehouseCode;
                        partsShiftOrder.WarehouseName = partsInboundCheckBill.WarehouseName;
                        partsShiftOrder.StorageCompanyId = partsInboundCheckBill.StorageCompanyId;
                        partsShiftOrder.StorageCompanyCode = partsInboundCheckBill.StorageCompanyCode;
                        partsShiftOrder.StorageCompanyName = partsInboundCheckBill.StorageCompanyName;
                        partsShiftOrder.StorageCompanyType = partsInboundCheckBill.StorageCompanyType;
                        partsShiftOrder.Status = (int)DcsPartsShiftOrderStatus.生效;
                        partsShiftOrder.Type = (int)DcsPartsShiftOrderType.上架;
                        var warehouseAreae1 = warehouseAreaesNew.OrderBy(r => Guid.NewGuid()).FirstOrDefault(r => r.WarehouseId == partsInboundCheckBill.WarehouseId);
                        foreach(var item in partsInboundCheckBill.PartsInboundCheckBillDetails) {
                            PartsShiftOrderDetail partsShiftOrderDetail = new PartsShiftOrderDetail();
                            partsShiftOrderDetail.PartsShiftOrderId = partsShiftOrder.Id;
                            partsShiftOrderDetail.SparePartId = item.SparePartId;
                            partsShiftOrderDetail.SparePartCode = item.SparePartCode;
                            partsShiftOrderDetail.SparePartName = item.SparePartName;
                            partsShiftOrderDetail.OriginalWarehouseAreaId = item.WarehouseAreaId;
                            partsShiftOrderDetail.OriginalWarehouseAreaCode = item.WarehouseAreaCode;
                            partsShiftOrderDetail.OriginalWarehouseAreaCategory = (int)DcsAreaType.检验区;
                            partsShiftOrderDetail.DestWarehouseAreaId = warehouseAreae1.Id;
                            partsShiftOrderDetail.DestWarehouseAreaCode = warehouseAreae1.Code;
                            partsShiftOrderDetail.DestWarehouseAreaCategory = (int)DcsAreaType.保管区;
                            partsShiftOrderDetail.Quantity = item.InspectedQuantity;
                            partsShiftOrder.PartsShiftOrderDetails.Add(partsShiftOrderDetail);
                        }
                        InsertToDatabase(partsShiftOrder);
                        new PartsShiftOrderAch(this.DomainService).InsertPartsShiftOrderValidate(partsShiftOrder);
                        #endregion
                        #region 出库计划主清单赋值 逻辑
                        PartsOutboundPlan partsOutboundPlan = new PartsOutboundPlan();
                        partsOutboundPlan.Id = i++;
                        partsOutboundPlan.Code = GlobalVar.ASSIGNED_BY_SERVER;
                        partsOutboundPlan.WarehouseId = partsInboundPlan.WarehouseId;
                        partsOutboundPlan.WarehouseCode = partsInboundPlan.WarehouseCode;
                        partsOutboundPlan.WarehouseName = partsInboundPlan.WarehouseName;
                        partsOutboundPlan.StorageCompanyId = partsInboundPlan.StorageCompanyId;
                        partsOutboundPlan.StorageCompanyCode = partsInboundPlan.StorageCompanyCode;
                        partsOutboundPlan.StorageCompanyName = partsInboundPlan.StorageCompanyName;
                        partsOutboundPlan.StorageCompanyType = partsInboundPlan.StorageCompanyType;
                        partsOutboundPlan.BranchId = partsInboundPlan.BranchId;
                        partsOutboundPlan.BranchCode = partsInboundPlan.BranchCode;
                        partsOutboundPlan.BranchName = partsInboundPlan.BranchName;
                        partsOutboundPlan.PartsSalesCategoryId = partsInboundPlan.PartsSalesCategoryId;
                        partsOutboundPlan.PartsSalesOrderTypeId = partsSalesOrder.PartsSalesOrderTypeId;
                        partsOutboundPlan.PartsSalesOrderTypeName = partsSalesOrder.PartsSalesOrderTypeName;
                        partsOutboundPlan.CounterpartCompanyId = partsSalesOrder.InvoiceReceiveCompanyId;
                        partsOutboundPlan.CounterpartCompanyCode = partsSalesOrder.InvoiceReceiveCompanyCode;
                        partsOutboundPlan.CounterpartCompanyName = partsSalesOrder.InvoiceReceiveCompanyName;
                        partsOutboundPlan.ReceivingCompanyId = partsSalesOrder.ReceivingCompanyId;
                        partsOutboundPlan.ReceivingCompanyCode = partsSalesOrder.ReceivingCompanyCode;
                        partsOutboundPlan.ReceivingCompanyName = partsSalesOrder.ReceivingCompanyName;
                        partsOutboundPlan.ReceivingWarehouseId = partsSalesOrder.ReceivingWarehouseId;
                        partsOutboundPlan.ReceivingWarehouseCode = (partsSalesOrder.ReceivingWarehouseId.HasValue && partsSalesOrder.ReceivingWarehouseId != 0) ? receivingWarehouse.Code : string.Empty;
                        partsOutboundPlan.ReceivingWarehouseName = partsSalesOrder.ReceivingWarehouseName;
                        partsOutboundPlan.SourceId = partsSalesOrder.Id;
                        partsOutboundPlan.SourceCode = partsSalesOrder.Code;
                        partsOutboundPlan.OutboundType = (int)DcsPartsOutboundType.配件销售;
                        partsOutboundPlan.ShippingMethod = partsSalesOrder.ShippingMethod;
                        partsOutboundPlan.CustomerAccountId = partsSalesOrder.CustomerAccountId;
                        partsOutboundPlan.OriginalRequirementBillId = partsSalesOrder.SourceBillId ?? 0;
                        partsOutboundPlan.OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.配件销售订单;
                        partsOutboundPlan.OriginalRequirementBillCode = partsSalesOrder.SourceBillCode;
                        partsOutboundPlan.Status = (int)DcsPartsOutboundPlanStatus.新建;
                        partsOutboundPlan.IfWmsInterface = virtualWarehouse.WmsInterface;
                        partsOutboundPlan.OrderApproveComment = partsSalesOrder.ApprovalComment;
                        partsOutboundPlan.Remark = partsSalesOrder.Remark;
                        partsOutboundPlan.IsPurDistribution = true;
                        foreach(var item in detail) {
                            PartsOutboundPlanDetail partsOutboundPlanDetail = new PartsOutboundPlanDetail();
                            partsOutboundPlanDetail.Id = i++;
                            partsOutboundPlanDetail.PartsOutboundPlanId = partsOutboundPlan.Id;
                            partsOutboundPlanDetail.SparePartId = item.SparePartId;
                            partsOutboundPlanDetail.SparePartCode = item.SparePartCode;
                            partsOutboundPlanDetail.SparePartName = item.SparePartName;
                            partsOutboundPlanDetail.PlannedAmount = item.CurrentFulfilledQuantity;
                            partsOutboundPlanDetail.OutboundFulfillment = item.CurrentFulfilledQuantity;
                            partsOutboundPlanDetail.Price = item.OrderPrice;
                            partsOutboundPlan.PartsOutboundPlanDetails.Add(partsOutboundPlanDetail);
                        }
                        InsertToDatabase(partsOutboundPlan);
                        new PartsOutboundPlanAch(this.DomainService).InsertPartsOutboundPlanValidate(partsOutboundPlan);
                        #endregion
                        #region 生成出库单、生成出库单清单 逻辑
                        PartsOutboundBill partsOutboundBill = new PartsOutboundBill();
                        partsOutboundBill.Id = i++;
                        partsOutboundBill.Code = GlobalVar.ASSIGNED_BY_SERVER;
                        partsOutboundBill.PartsOutboundPlanId = partsOutboundPlan.Id;
                        partsOutboundBill.WarehouseId = partsOutboundPlan.WarehouseId;
                        partsOutboundBill.WarehouseCode = partsOutboundPlan.WarehouseCode;
                        partsOutboundBill.WarehouseName = partsOutboundPlan.WarehouseName;
                        partsOutboundBill.StorageCompanyId = partsOutboundPlan.StorageCompanyId;
                        partsOutboundBill.StorageCompanyCode = partsOutboundPlan.StorageCompanyCode;
                        partsOutboundBill.StorageCompanyName = partsOutboundPlan.StorageCompanyName;
                        partsOutboundBill.StorageCompanyType = partsOutboundPlan.StorageCompanyType;
                        partsOutboundBill.BranchId = partsOutboundPlan.BranchId;
                        partsOutboundBill.BranchCode = partsOutboundPlan.BranchCode;
                        partsOutboundBill.BranchName = partsOutboundPlan.BranchName;
                        partsOutboundBill.PartsSalesCategoryId = partsOutboundPlan.PartsSalesCategoryId;
                        partsOutboundBill.PartsSalesOrderTypeId = partsOutboundPlan.PartsSalesOrderTypeId;
                        partsOutboundBill.PartsSalesOrderTypeName = partsOutboundPlan.PartsSalesOrderTypeName;
                        partsOutboundBill.CounterpartCompanyId = partsOutboundPlan.CounterpartCompanyId;
                        partsOutboundBill.CounterpartCompanyCode = partsOutboundPlan.CounterpartCompanyCode;
                        partsOutboundBill.CounterpartCompanyName = partsOutboundPlan.CounterpartCompanyName;
                        partsOutboundBill.ReceivingCompanyId = partsOutboundPlan.ReceivingCompanyId;
                        partsOutboundBill.ReceivingCompanyCode = partsOutboundPlan.ReceivingCompanyCode;
                        partsOutboundBill.ReceivingCompanyName = partsOutboundPlan.ReceivingCompanyName;
                        partsOutboundBill.ReceivingWarehouseId = partsOutboundPlan.ReceivingWarehouseId;
                        partsOutboundBill.ReceivingWarehouseCode = partsOutboundPlan.ReceivingWarehouseCode;
                        partsOutboundBill.ReceivingWarehouseName = partsOutboundPlan.ReceivingWarehouseName;
                        partsOutboundBill.OutboundType = partsOutboundPlan.OutboundType;
                        partsOutboundBill.ShippingMethod = partsOutboundPlan.ShippingMethod;
                        partsOutboundBill.CustomerAccountId = partsOutboundPlan.CustomerAccountId;
                        partsOutboundBill.OriginalRequirementBillId = partsOutboundPlan.OriginalRequirementBillId;
                        partsOutboundBill.OriginalRequirementBillType = partsOutboundPlan.OriginalRequirementBillType;
                        partsOutboundBill.OriginalRequirementBillCode = partsOutboundPlan.OriginalRequirementBillCode;
                        partsOutboundBill.SettlementStatus = (int)DcsPartsSettlementStatus.待结算;
                        foreach(var item in partsOutboundPlan.PartsOutboundPlanDetails) {
                            PartsOutboundBillDetail partsOutboundBillDetail = new PartsOutboundBillDetail();
                            partsOutboundBillDetail.PartsOutboundBillId = partsOutboundBill.Id;
                            partsOutboundBillDetail.SparePartId = item.SparePartId;
                            partsOutboundBillDetail.SparePartCode = item.SparePartCode;
                            partsOutboundBillDetail.SparePartName = item.SparePartName;
                            partsOutboundBillDetail.OutboundAmount = item.OutboundFulfillment ?? 0;
                            partsOutboundBillDetail.WarehouseAreaId = partsShiftOrder.PartsShiftOrderDetails.FirstOrDefault(r => r.SparePartId == item.SparePartId).DestWarehouseAreaId;
                            partsOutboundBillDetail.WarehouseAreaCode = partsShiftOrder.PartsShiftOrderDetails.FirstOrDefault(r => r.SparePartId == item.SparePartId).DestWarehouseAreaCode;
                            partsOutboundBillDetail.SettlementPrice = item.Price;
                            partsOutboundBillDetail.CostPrice = a.FirstOrDefault(r => r.SparePartId == item.SparePartId).PlannedPrice;
                            partsOutboundBill.PartsOutboundBillDetails.Add(partsOutboundBillDetail);
                        }
                        InsertToDatabase(partsOutboundBill);
                        new PartsOutboundBillAch(this.DomainService).InsertPartsOutboundBillValidate(partsOutboundBill);
                        #endregion
                    }
                    #endregion
                } else {
                    var processDetailWarehouseerror = warehouseIds.Any(r => this.ObjectContext.Warehouses.All(e => e.Id != r));// this.ObjectContext.Warehouses.Any(r => warehouseIds.Contains(r.Id));
                    if(processDetailWarehouseerror == true)
                        throw new ValidationException("处理清单中存在出库仓库不在系统中的数据！");
                    var processDetailWarehouse = this.ObjectContext.Warehouses.Where(r => warehouseIds.Contains(r.Id)).ToArray();
                    var virtualPartsSalesPrices = new VirtualPartsSalesPriceAch(this.DomainService).根据配件清单获取销售价格(partsSalesOrder.PartsSalesOrderTypeId, partsSalesOrder.SubmitCompanyId, sparepartsIds).ToArray();
                    foreach(var detail in partsSalesOrderProcess.PartsSalesOrderProcessDetails) {
                        var virtualPartsSalesPrice = virtualPartsSalesPrices.FirstOrDefault(r => r.SparePartId == detail.SparePartId);
                        if(virtualPartsSalesPrice == null)
                            throw new ValidationException(string.Format(ErrorStrings.PartsSalesOrder_Validation3, partsSalesOrder.PartsSalesOrderTypeName, detail.SparePartCode));
                        detail.OrderPrice = virtualPartsSalesPrice.Price;
                    }
                    foreach(var detail in partsSalesOrder.PartsSalesOrderDetails) {
                        var virtualPartsSalesPrice = virtualPartsSalesPrices.FirstOrDefault(r => r.SparePartId == detail.SparePartId);
                        if(virtualPartsSalesPrice == null)
                            throw new ValidationException(string.Format(ErrorStrings.PartsSalesOrder_Validation3, partsSalesOrder.PartsSalesOrderTypeName, detail.SparePartCode));
                        detail.OrderPrice = virtualPartsSalesPrice.Price;
                    }
                    //1）调用【客户账户.查客户可用资金】
                    //    条件：客户账户.Id=配件销售订单.客户账户Id
                    //    配件销售订单处理.本次满足金额小于客户账户.可用额度
                    //    系统提示：当前账户可用额度不足
                    var customerAccount = DomainService.查询客户可用资金(partsSalesOrder.CustomerAccountId, null, null);
                    if(partsSalesOrderProcess.CurrentFulfilledAmount > customerAccount.UseablePosition)
                        throw new ValidationException("当前账户可用额度不足");
                    //2）更新配件销售订单,同时更新配件销售订单处理的处理后状态字段
                    //  a.配件销售订单赋值：
                    //    针对配件销售订单处理清单中的订单处理方式 in （本部处理，供应商直发）的行，逐个更改配件销售订单清单的审批数量
                    //    配件销售订单清单.审批数量=配件销售订单清单.审批数量+配件销售处理清单. 本次满足数量
                    //    如果所有配件销售订单清单.审批数量=配件销售订单清单.订货数量        配件销售订单.状态=审批完成  否则  配件销售订单.状态=部分审批 
                    //  b.配件销售订单处理.处理后单据状态=配件销售订单.状态；
                    int[] methods = new int[]{
                        (int)DcsPartsSalesOrderProcessDetailProcessMethod.本部处理,
                        (int)DcsPartsSalesOrderProcessDetailProcessMethod.供应商直发
                    };
                    var partsSalesOrderProcessDetails = partsSalesOrderProcess.PartsSalesOrderProcessDetails.Where(r => methods.Contains(r.OrderProcessMethod)).ToArray();
                    foreach(var detail in partsSalesOrder.PartsSalesOrderDetails) {
                        var partsSalesOrderProcessDetail = partsSalesOrderProcessDetails.FirstOrDefault(r => r.SparePartId == detail.SparePartId);
                        if(partsSalesOrderProcessDetail == null)
                            continue;
                        detail.ApproveQuantity = (detail.ApproveQuantity ?? 0) + partsSalesOrderProcessDetail.CurrentFulfilledQuantity;
                    }
                    if(partsSalesOrder.PartsSalesOrderDetails.Any(r => r.ApproveQuantity != r.OrderedQuantity)) {
                        partsSalesOrder.Status = (int)DcsPartsSalesOrderStatus.部分审批;
                    } else {
                        partsSalesOrder.Status = (int)DcsPartsSalesOrderStatus.审批完成;
                    }
                    partsSalesOrderProcess.BillStatusAfterProcess = partsSalesOrder.Status;
                    InsertToDatabase(partsSalesOrderProcess);
                    this.InsertPartsSalesOrderProcessValidate(partsSalesOrderProcess);
                    //UpdateToDatabase(partsSalesOrderProcess);
                    UpdateToDatabase(partsSalesOrder);
                    //UpdatePartsSalesOrderProcessValidate(partsSalesOrderProcess);
                    new PartsSalesOrderAch(this.DomainService).UpdatePartsSalesOrderValidate(partsSalesOrder);

                    //3）
                    //更新客户账户（客户账户.id=配件销售订单.客户账户id）审批待发金额=审批待发金额+sum（配件销售订单处理清单.订货价格*配件销售订单处理清单.本次满足数量 ，配件销售订单处理清单.订单处理方式=本部满足）
                    customerAccountnew.First().PendingAmount = customerAccountnew.First().PendingAmount + partsSalesOrderProcess.PartsSalesOrderProcessDetails.Where(ex => ex.OrderProcessMethod == (int)DcsPartsSalesOrderProcessDetailProcessMethod.本部处理).Sum(r => r.OrderPrice * r.CurrentFulfilledQuantity);
                    UpdateToDatabase(customerAccountnew.First());
                    #region 订单处理方式=本部处理
                    foreach(var detail in partsSalesOrderProcess.PartsSalesOrderProcessDetails.Where(r => r.OrderProcessMethod == (int)DcsPartsSalesOrderProcessDetailProcessMethod.本部处理).GroupBy(r => r.WarehouseId)) {
                        PartsOutboundPlan partsOutboundPlan = new PartsOutboundPlan();
                        partsOutboundPlan.Id = i++;
                        partsOutboundPlan.WarehouseId = detail.FirstOrDefault().WarehouseId ?? 0;
                        partsOutboundPlan.WarehouseCode = detail.FirstOrDefault().WarehouseCode;
                        partsOutboundPlan.WarehouseName = detail.FirstOrDefault().WarehouseName;
                        partsOutboundPlan.StorageCompanyId = 2;
                        partsOutboundPlan.StorageCompanyCode = "1101";
                        partsOutboundPlan.StorageCompanyName = "北汽红岩汽车股份有限公司北京配件销售分公司";
                        partsOutboundPlan.StorageCompanyType = (int)DcsCompanyType.分公司;
                        partsOutboundPlan.BranchId = 2;
                        partsOutboundPlan.BranchCode = "1101";
                        partsOutboundPlan.BranchName = "北汽红岩汽车股份有限公司北京配件销售分公司";
                        partsOutboundPlan.PartsSalesCategoryId = partsSalesOrder.SalesCategoryId;
                        partsOutboundPlan.PartsSalesOrderTypeId = partsSalesOrder.PartsSalesOrderTypeId;
                        partsOutboundPlan.PartsSalesOrderTypeName = partsSalesOrder.PartsSalesOrderTypeName;
                        partsOutboundPlan.CounterpartCompanyId = partsSalesOrder.InvoiceReceiveCompanyId;
                        partsOutboundPlan.CounterpartCompanyCode = partsSalesOrder.InvoiceReceiveCompanyCode;
                        partsOutboundPlan.CounterpartCompanyName = partsSalesOrder.InvoiceReceiveCompanyName;
                        partsOutboundPlan.ReceivingCompanyId = partsSalesOrder.ReceivingCompanyId;
                        partsOutboundPlan.ReceivingCompanyCode = partsSalesOrder.ReceivingCompanyCode;
                        partsOutboundPlan.ReceivingCompanyName = partsSalesOrder.ReceivingCompanyName;
                        partsOutboundPlan.ReceivingWarehouseId = partsSalesOrder.ReceivingWarehouseId;
                        partsOutboundPlan.ReceivingWarehouseCode = (partsSalesOrder.ReceivingWarehouseId.HasValue && partsSalesOrder.ReceivingWarehouseId != 0) ? receivingWarehouse.Code : string.Empty;
                        partsOutboundPlan.ReceivingWarehouseName = partsSalesOrder.ReceivingWarehouseName;
                        partsOutboundPlan.SourceId = partsSalesOrder.Id;
                        partsOutboundPlan.SourceCode = partsSalesOrder.Code;
                        partsOutboundPlan.OutboundType = (int)DcsPartsOutboundType.配件销售;
                        partsOutboundPlan.ShippingMethod = partsSalesOrder.ShippingMethod;
                        partsOutboundPlan.CustomerAccountId = partsSalesOrder.CustomerAccountId;
                        partsOutboundPlan.OriginalRequirementBillId = partsSalesOrder.SourceBillId ?? 0;
                        partsOutboundPlan.OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.配件销售订单;
                        partsOutboundPlan.OriginalRequirementBillCode = partsSalesOrder.SourceBillCode;
                        partsOutboundPlan.Status = (int)DcsPartsOutboundPlanStatus.新建;
                        partsOutboundPlan.IfWmsInterface = processDetailWarehouse.FirstOrDefault(r => r.Id == detail.FirstOrDefault().WarehouseId).WmsInterface;
                        partsOutboundPlan.OrderApproveComment = partsSalesOrder.ApprovalComment;
                        partsOutboundPlan.Remark = partsSalesOrder.Remark;
                        partsOutboundPlan.IsPurDistribution = true;
                        partsOutboundPlan.ReceivingAddress = partsSalesOrder.ReceivingAddress;
                        #region 配件出库计划清单赋值
                        foreach(var item in detail) {
                            PartsOutboundPlanDetail partsOutboundPlanDetail = new PartsOutboundPlanDetail();
                            partsOutboundPlanDetail.PartsOutboundPlanId = partsOutboundPlan.Id;
                            partsOutboundPlanDetail.SparePartId = item.NewPartId.HasValue ? item.NewPartId.Value : item.SparePartId;
                            partsOutboundPlanDetail.SparePartCode = item.NewPartId.HasValue ? item.NewPartCode : item.SparePartCode;
                            partsOutboundPlanDetail.SparePartName = item.NewPartId.HasValue ? item.NewPartName : item.SparePartName;
                            if(partsOutboundPlan.PartsOutboundPlanDetails.FirstOrDefault(r => r.SparePartId == partsOutboundPlanDetail.SparePartId) != null) {
                                partsOutboundPlanDetail.PlannedAmount += item.CurrentFulfilledQuantity;
                            } else {
                                partsOutboundPlanDetail.PlannedAmount = item.CurrentFulfilledQuantity;
                            }
                            partsOutboundPlanDetail.Price = item.OrderPrice;
                            partsOutboundPlan.PartsOutboundPlanDetails.Add(partsOutboundPlanDetail);
                        }
                        //生成配件出库计划
                        var partsOutboundPlanDetails = partsOutboundPlan.PartsOutboundPlanDetails.ToArray();
                        var partsOutboundPlanDetailPartIds = partsOutboundPlanDetails.Select(r => r.SparePartId);
                        var partsLockedStocks = new PartsLockedStockAch(this.DomainService).GetPartsLockedStocksWithLock(ObjectContext.PartsLockedStocks.Where(r => r.WarehouseId == partsOutboundPlan.WarehouseId && partsOutboundPlanDetailPartIds.Contains(r.PartId))).ToArray();
                        var partsStocks = ObjectContext.PartsStocks.Where(r => r.WarehouseId == partsOutboundPlan.WarehouseId && partsOutboundPlanDetailPartIds.Contains(r.PartId)).ToArray();
                        var wmsCongelationStockViews = ObjectContext.WmsCongelationStockViews.Where(r => r.PartsSalesCategoryId == partsOutboundPlan.PartsSalesCategoryId && r.WarehouseId == partsOutboundPlan.WarehouseId && partsOutboundPlanDetailPartIds.Contains(r.SparePartId)).ToArray();
                        foreach(var partsOutboundPlanDetail in partsOutboundPlanDetails) {
                            var partsLockedStock = partsLockedStocks.FirstOrDefault(r => r.PartId == partsOutboundPlanDetail.SparePartId);
                            var partsStocksAmount = partsStocks.Where(r => r.PartId == partsOutboundPlanDetail.SparePartId).Sum(v => v.Quantity);
                            var congelationStockQtyAmount = wmsCongelationStockViews.Where(r => r.SparePartId == partsOutboundPlanDetail.SparePartId).Sum(v => v.CongelationStockQty);
                            var disabledStockAmount = wmsCongelationStockViews.Where(r => r.SparePartId == partsOutboundPlanDetail.SparePartId).Sum(v => v.DisabledStock);
                            if(partsLockedStock == null) {
                                if(partsSalesOrderProcessDetails.Any(r => (r.NewPartId ?? r.SparePartId) == partsOutboundPlanDetail.SparePartId && r.OrderProcessMethod == (int)DcsPartsSalesOrderProcessDetailProcessMethod.本部处理) && (partsStocksAmount - partsOutboundPlanDetail.PlannedAmount - congelationStockQtyAmount - disabledStockAmount < 0))
                                    throw new ValidationException(string.Format(ErrorStrings.PartsOutboundPlan_Validation3, partsOutboundPlanDetail.SparePartCode));
                                var newpartsLockedStock = new PartsLockedStock {
                                    WarehouseId = partsOutboundPlan.WarehouseId,
                                    PartId = partsOutboundPlanDetail.SparePartId,
                                    LockedQuantity = partsOutboundPlanDetail.PlannedAmount,
                                    StorageCompanyId = partsOutboundPlan.StorageCompanyId,
                                    BranchId = partsOutboundPlan.BranchId
                                };
                                InsertToDatabase(newpartsLockedStock);
                                new PartsLockedStockAch(this.DomainService).InsertPartsLockedStockValidate(newpartsLockedStock);
                            } else {
                                if(partsSalesOrderProcessDetails.Any(r => (r.NewPartId ?? r.SparePartId) == partsOutboundPlanDetail.SparePartId && r.OrderProcessMethod == (int)DcsPartsSalesOrderProcessDetailProcessMethod.本部处理) && (partsStocksAmount - partsLockedStock.LockedQuantity - partsOutboundPlanDetail.PlannedAmount - congelationStockQtyAmount - disabledStockAmount < 0))
                                    throw new ValidationException(string.Format(ErrorStrings.PartsOutboundPlan_Validation3, partsOutboundPlanDetail.SparePartCode));
                                partsLockedStock.LockedQuantity += partsOutboundPlanDetail.PlannedAmount;
                                new PartsLockedStockAch(this.DomainService).UpdatePartsLockedStockValidate(partsLockedStock);
                                UpdateToDatabase(partsLockedStock);
                            }
                        }
                        #endregion
                        InsertToDatabase(partsOutboundPlan);
                        new PartsOutboundPlanAch(this.DomainService).InsertPartsOutboundPlanValidate(partsOutboundPlan);
                    }
                    #endregion
                }
                #region
                //根据数据库连接字符串获取user id段的值
                var userInfo = Utils.GetCurrentUserInfo();
                var entityConnStr = ConfigurationManager.ConnectionStrings["DcsEntities"].ConnectionString;
                var conn = entityConnStr.ToUpper();
                var ss = conn.Split(';');
                var value = ss[3];
                var ww = value.Split('=');
                var value1 = ww[1].Trim();
                //根据销售订单.收票单位编号 查询分公司与数据库关联关系 获取要更新的数据库
                var branchDateBaseRel = this.ObjectContext.BranchDateBaseRels.FirstOrDefault(r => r.BranchCode == partsSalesOrder.InvoiceReceiveCompanyCode);
                if(branchDateBaseRel == null)
                    throw new ValidationException(string.Format(ErrorStrings.BranchDateBaseRel_Validation1, partsSalesOrder.InvoiceReceiveCompanyCode));

                this.ObjectContext.SaveChanges();


                var salesOrderBillQuantity = new Devart.Data.Oracle.OracleParameter("SalesOrderBillQuantity", Devart.Data.Oracle.OracleDbType.Integer, System.Data.ParameterDirection.Output);
                ObjectContext.ExecuteStoreCommand("begin :SalesOrderBillQuantity:=GenerateOldSalesOrderProcess(:FOldDateBase, :FNewDateBase,:FBillSourceBillId,:FProcessBillId,:FEnterpriseCode); end;",
                    new Devart.Data.Oracle.OracleParameter("FOldDateBase", value1),
                    new Devart.Data.Oracle.OracleParameter("FNewDateBase", branchDateBaseRel.DateBase.ToUpper()),
                    new Devart.Data.Oracle.OracleParameter("FBillSourceBillId", partsSalesOrder.SourceBillId),
                    new Devart.Data.Oracle.OracleParameter("FProcessBillId", partsSalesOrderProcess.Id),
                    new Devart.Data.Oracle.OracleParameter("FEnterpriseCode", userInfo.EnterpriseCode),
                    salesOrderBillQuantity
                    );
                #endregion
                this.ObjectContext.SaveChanges();
                transaction.Complete();
            }
        }

    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:11:57
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 配件销售直供审批服务(PartsSalesOrderProcess partsSalesOrderProcess, bool isTransSap) {
            new PartsSalesOrderProcessAch(this).配件销售直供审批服务(partsSalesOrderProcess, isTransSap);
        }


        [Update(UsingCustomMethod = true)]
        public void 审批配件销售订单正常订单(PartsSalesOrderProcess partsSalesOrderProcess) {
            new PartsSalesOrderProcessAch(this).审批配件销售订单正常订单(partsSalesOrderProcess);
        }


        [Update(UsingCustomMethod = true)]
        public void 审批配件销售订单正常订单2(PartsSalesOrderProcess partsSalesOrderProcess, int partsSalesOrderTypeId, string remark, int salesCategoryId, bool isCheckMinSaleQuantity) {
            new PartsSalesOrderProcessAch(this).审批配件销售订单正常订单2(partsSalesOrderProcess, partsSalesOrderTypeId, remark, salesCategoryId, isCheckMinSaleQuantity);
        }
        /// <summary>
        /// 批量审核配件销售订单
        /// </summary>
        /// <param name="ids">配件销售订单id数组</param>
        /// <returns>是否部分审批</returns>
        [Invoke(HasSideEffects = true)]
        public bool 批量审核配件销售订单(int[] ids) {
            return new PartsSalesOrderProcessAch(this).批量审核配件销售订单(ids);
        }

        [Update(UsingCustomMethod = true)]
        public void 电商平台审核销售订单(PartsSalesOrderProcess partsSalesOrderProcess) {
            new PartsSalesOrderProcessAch(this).电商平台审核销售订单(partsSalesOrderProcess);
        }

        [Invoke]
        public void 校验直供清单(PartsSalesOrderProcess partsSalesOrderProcess) {
            new PartsSalesOrderProcessAch(this).校验直供清单(partsSalesOrderProcess);
        }


        [Update(UsingCustomMethod = true)]
        public void 审批配件公司销售订单(PartsSalesOrderProcess partsSalesOrderProcess) {
            new PartsSalesOrderProcessAch(this).审批配件公司销售订单(partsSalesOrderProcess);
        }
    }
}


