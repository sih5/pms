﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Devart.Data.Oracle;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsSalesOrderProcessForBranchAch : DcsSerivceAchieveBase {
        public PartsSalesOrderProcessForBranchAch(DcsDomainService domainService)
            : base(domainService) {
        }

        internal void 审批销售订单内转直供生成雷萨数据(PartsPurchaseOrder partsPurchaseOrder, PartsSalesOrder orderPartsSalesOrder) {
            var partsSalesOrder = new PartsSalesOrder {
                Code = GlobalVar.ASSIGNED_BY_SERVER,
                BranchCode = "2450",
                Status = (int)DcsPartsPurchaseOrderStatus.提交,
                IfAgencyService = false,
                SalesActivityDiscountRate = 0,
                SalesActivityDiscountAmount = 0,
                SalesCategoryName = "欧曼保外",
                SubmitCompanyCode = "FT000333",
                SalesUnitName = "欧曼保外销售组织",
                InvoiceReceiveCompanyCode = "FT000333",
                PartsSalesOrderTypeName = "急需订单",
                RequestedDeliveryTime = DateTime.Now.AddDays(2),
                IfDirectProvision = false,
                CustomerType = orderPartsSalesOrder.CustomerType,
                ReceivingCompanyCode = "FT000333",
                ShippingMethod = (int)DcsPartsShippingMethod.公路物流,
                ERPOrderCode = orderPartsSalesOrder.ERPOrderCode,
                ERPSourceOrderCode = orderPartsSalesOrder.ERPSourceOrderCode,
                IfInnerDirectProvision = true,
                InnerSourceId = orderPartsSalesOrder.Id,
                InnerSourceCode = orderPartsSalesOrder.Code,
                Remark = partsPurchaseOrder.Remark
            };

            string fdConnString = ConfigurationManager.ConnectionStrings["DcsFDServiceConn"].ConnectionString;
            OracleConnection fdConn = new OracleConnection(fdConnString);
            string dcsnextvaluesql = "select s_partssalesorder.nextval as nextval from dual";
            setAttributeValue(fdConn, dcsnextvaluesql, (command) => {
                int dcsnextvalue = 0;
                var dataRead = command.ExecuteReader();
                if(dataRead.Read()) {
                    dcsnextvalue = Convert.ToInt32(dataRead["nextval"]);
                }
                if(dcsnextvalue == 0) {
                    throw new ValidationException("序列号异常，请检查");
                }
                partsSalesOrder.Id = dcsnextvalue;
            });
            //设置分公司信息
            setAttributeValue(fdConn, "select id,name from branch where code=:code and status=1 ", (command) => {
                command.Parameters.Add(new OracleParameter("code", partsSalesOrder.BranchCode));
                var dataRead = command.ExecuteReader();
                if(dataRead.Read()) {
                    partsSalesOrder.BranchId = Convert.ToInt32(dataRead["id"]);
                    partsSalesOrder.BranchName = dataRead["name"].ToString();
                }
            });
            //获取品牌
            setAttributeValue(fdConn, "select id from PartsSalesCategory where name=:name and status=1 ", (command) => {
                command.Parameters.Add(new OracleParameter("name", partsSalesOrder.SalesCategoryName));
                var dataRead = command.ExecuteReader();
                if(dataRead.Read()) {
                    partsSalesOrder.SalesCategoryId = Convert.ToInt32(dataRead["id"]);
                }
            });

            //获取提报单位信息  收货单位信息
            setAttributeValue(fdConn, "select id,name from dealer where code=:code and status=1 ", (command) => {
                command.Parameters.Add(new OracleParameter("code", partsSalesOrder.SubmitCompanyCode));
                var dataRead = command.ExecuteReader();
                if(dataRead.Read()) {
                    partsSalesOrder.SubmitCompanyId = Convert.ToInt32(dataRead["id"]);
                    partsSalesOrder.SubmitCompanyName = dataRead["name"].ToString();
                    partsSalesOrder.ReceivingCompanyId = Convert.ToInt32(dataRead["id"]);
                    partsSalesOrder.ReceivingCompanyName = dataRead["name"].ToString();
                }
            });

            //获取销售组织 和隶属企业id
            setAttributeValue(fdConn, "select id,code,ownerCompanyId from SalesUnit  where name=:name and status=1 ", (command) => {
                command.Parameters.Add(new OracleParameter("name", partsSalesOrder.SalesUnitName));
                var dataRead = command.ExecuteReader();
                if(!dataRead.HasRows) {
                    throw new ValidationException("不存在名称为欧曼保外的销售组织!");
                }
                if(dataRead.Read()) {
                    partsSalesOrder.SalesUnitId = Convert.ToInt32(dataRead["id"]);
                    partsSalesOrder.SalesUnitCode = dataRead["code"].ToString();
                    partsSalesOrder.SalesUnitOwnerCompanyId = Convert.ToInt32(dataRead["ownerCompanyId"]);
                }
            });
            //获取隶属企业信息
            setAttributeValue(fdConn, "select code,name from Company  where id=:id and status=1 ", (command) => {
                command.Parameters.Add(new OracleParameter("id", partsSalesOrder.SalesUnitOwnerCompanyId));
                var dataRead = command.ExecuteReader();
                if(dataRead.Read()) {
                    partsSalesOrder.SalesUnitOwnerCompanyCode = dataRead["code"].ToString();
                    partsSalesOrder.SalesUnitOwnerCompanyName = dataRead["name"].ToString();
                }
            });
            //获取客户账户
            setAttributeValue(fdConn, "select id,accountBalance from  CustomerAccount  where AccountGroupId =(select AccountGroupId from SalesUnit where OwnerCompanyId=:ownerCompanyId and partssalescategoryid=:partssalescategoryid and BranchId=:branchId  and status=1) and CustomerCompanyId=:submitCompanyId", (command) => {
                command.Parameters.Add(new OracleParameter("ownerCompanyId", partsSalesOrder.SalesUnitOwnerCompanyId));
                command.Parameters.Add(new OracleParameter("branchId", partsSalesOrder.BranchId));
                command.Parameters.Add(new OracleParameter("partssalescategoryid", partsSalesOrder.SalesCategoryId));
                command.Parameters.Add(new OracleParameter("submitCompanyId", partsSalesOrder.SubmitCompanyId));
                var dataRead = command.ExecuteReader();
                if(dataRead.Read()) {
                    partsSalesOrder.CustomerAccountId = Convert.ToInt32(dataRead["id"]);
                    var accountBalance = Convert.ToDecimal(dataRead["accountBalance"]);
                    if(accountBalance > 0) {
                        partsSalesOrder.IsDebt = false;
                    } else {
                        partsSalesOrder.IsDebt = true;
                    }
                }
            });
            //收票单位信息
            setAttributeValue(fdConn, "select id,name,type from Company  where code=:code and status=1 ", (command) => {
                command.Parameters.Add(new OracleParameter("code", partsSalesOrder.InvoiceReceiveCompanyCode));
                var dataRead = command.ExecuteReader();
                if(dataRead.Read()) {
                    partsSalesOrder.InvoiceReceiveCompanyId = Convert.ToInt32(dataRead["id"]);
                    partsSalesOrder.InvoiceReceiveCompanyName = dataRead["name"].ToString();
                    partsSalesOrder.InvoiceReceiveCompanyType = Convert.ToInt32(dataRead["type"]);
                }
            });

            //销售订单类型
            setAttributeValue(fdConn, "select id from PartsSalesOrderType where branchid=:branchid and PartsSalesCategoryId=:partsSalesCategoryId and Name='急需订单'", (command) => {
                command.Parameters.Add(new OracleParameter("branchid", partsSalesOrder.BranchId));
                command.Parameters.Add(new OracleParameter("partsSalesCategoryId", partsSalesOrder.SalesCategoryId));
                var dataRead = command.ExecuteReader();
                if(!dataRead.HasRows) {
                    throw new ValidationException("该企业不存在内部直供销售订单类型。");
                }
                if(dataRead.Read()) {
                    partsSalesOrder.PartsSalesOrderTypeId = Convert.ToInt32(dataRead["id"]);
                }
            });

            //获取企业收货地址
            setAttributeValue(fdConn, "select id,detailAddress from CompanyAddress where CompanyId=(select id from Company where code=:code)", (command) => {
                command.Parameters.Add(new OracleParameter("code", partsSalesOrder.ReceivingCompanyCode));
                var dataRead = command.ExecuteReader();
                if(dataRead.Read()) {
                    partsSalesOrder.CompanyAddressId = Convert.ToInt32(dataRead["id"]);
                    partsSalesOrder.ReceivingAddress = dataRead["detailAddress"].ToString();
                }
            });
            //获取订货仓库 保外密云配件总库（搬迁）
            setAttributeValue(fdConn, "select id from Warehouse where name=:name and status=1", (command) => {
                command.Parameters.Add(new OracleParameter("name", "保外密云配件总库（搬迁）"));
                var dataRead = command.ExecuteReader();
                if(!dataRead.HasRows) {
                    throw new ValidationException("未找到有效的保外密云配件总库(搬迁)仓库信,请维护!");
                }
                if(dataRead.Read()) {
                    partsSalesOrder.WarehouseId = Convert.ToInt32(dataRead["id"]);
                }
            });
            //	新配件销售订单清单.	订货价格	=	查询配件特殊协议价.协议价格（配件销售类型Id，提报企业Id，配件Id）	
            //新配件销售订单清单.	订货金额	=	订货价格*订货数量	
            //根据配件id获取配件特殊协议价

            var sql = new StringBuilder("select sp.id,sp.code,sp.name,ps.TreatyPrice from PartsSpecialTreatyPrice  ps,SparePart sp where  ps.PartsSalesCategoryId=:partsSalesCategoryId and ps.CompanyId=:companyId and ps.status=1  and ps.sparePartId =sp.id and sp.code in (");
            var sparePartIds = partsPurchaseOrder.PartsPurchaseOrderDetails.Select(r => r.SparePartCode).Distinct().ToArray();
            for(var i = 0; i < sparePartIds.Length; i++) {
                sql.Append("'" + sparePartIds[i] + "'");
                if(i != sparePartIds.Length - 1) {
                    sql.Append(" ,");
                }
            }
            sql.Append(" )");
            List<PartsSalesOrderDetail> lists = new List<PartsSalesOrderDetail>();
            setAttributeValue(fdConn, sql.ToString(), (command) => {
                command.Parameters.Add(new OracleParameter("partsSalesCategoryId", partsSalesOrder.SalesCategoryId));
                command.Parameters.Add(new OracleParameter("companyId", partsSalesOrder.SubmitCompanyId));
                var dataRead = command.ExecuteReader();
                while(dataRead.Read()) {
                    lists.Add(new PartsSalesOrderDetail {
                        SparePartId = Convert.ToInt32(dataRead["id"]),
                        SparePartCode = dataRead["code"].ToString(),
                        SparePartName = dataRead["name"].ToString(),
                        OrderPrice = Convert.ToDecimal(dataRead["TreatyPrice"])
                    })
                ;
                }
            });

            new PartsSalesOrderAch(this.DomainService).InsertPartsSalesOrderValidate(partsSalesOrder);
            var neworderdetails = new List<PartsSalesOrderDetail>();
            foreach(var detail in partsPurchaseOrder.PartsPurchaseOrderDetails) {
                var partsSpecialTreatyPrice = lists.FirstOrDefault(r => r.SparePartCode == detail.SparePartCode);
                if(partsSpecialTreatyPrice == null) {
                    throw new ValidationException(String.Format("未找到配件:{0},的特殊协议价!", detail.SparePartCode));
                }
                var newPartsSalesOrderDetail = new PartsSalesOrderDetail {
                    PartsSalesOrderId = partsSalesOrder.Id,
                    SparePartId = partsSpecialTreatyPrice.SparePartId,
                    SparePartCode = partsSpecialTreatyPrice.SparePartCode,
                    SparePartName = partsSpecialTreatyPrice.SparePartName,
                    OrderedQuantity = detail.OrderAmount,
                    ApproveQuantity = 0,
                    OriginalPrice = detail.UnitPrice,
                    CustOrderPriceGradeCoefficient = 1,
                    IfCanNewPart = false,
                    DiscountedPrice = 0,
                    OrderPrice = partsSpecialTreatyPrice.OrderPrice,
                    OrderSum = partsSpecialTreatyPrice.OrderPrice * detail.OrderAmount
                };
                neworderdetails.Add(newPartsSalesOrderDetail);
            }
            partsSalesOrder.TotalAmount = neworderdetails.Sum(r => r.OrderedQuantity * r.OrderPrice);
            new PartsSalesOrderAch(this.DomainService).insertintosaleorder(partsSalesOrder, neworderdetails, fdConnString);
        }

        internal void setAttributeValue(OracleConnection conncnConnection, String sql, Action<OracleCommand> action) {
            try {
                conncnConnection.Open();
                var command = new OracleCommand(sql, conncnConnection);
                action(command);
            } catch(Exception) {
                throw;
            } finally {
                conncnConnection.Close();
            }

        }

        //系统暂无转内部直供逻辑，字典项改为转中心库 -LY
        internal void 审批销售订单内转直供(PartsSalesOrderProcess orderPartsSalesOrderProcess, List<PartsSalesOrderProcessDetail> marketingProcessDetailProcess, List<PartsSalesOrderDetail> partsSalesOrderDetails, PartsSalesOrder partsSalesOrder, PartsSalesCategory orderPartsSalesCategory) {
            var userInfo = Utils.GetCurrentUserInfo();
            var insideMarketingProcessDetails = marketingProcessDetailProcess.Where(r => r.OrderProcessMethod == (int)DcsPartsSalesOrderProcessDetailProcessMethod.转中心库).ToArray();
            if(insideMarketingProcessDetails.Length > 0) {
                var partsIds = insideMarketingProcessDetails.Select(r => r.SparePartId).ToArray();
                var warehouse = ObjectContext.Warehouses.FirstOrDefault(r => r.Name == "随车行北京CDC仓库" && r.Status == (int)DcsBaseDataStatus.有效);
                //生成销售订单
                var newpartsSalesOrder = new PartsSalesOrder {
                    BranchId = partsSalesOrder.BranchId,
                    BranchCode = partsSalesOrder.BranchCode,
                    BranchName = partsSalesOrder.BranchName,
                    SalesCategoryId = partsSalesOrder.SalesCategoryId,
                    SalesCategoryName = partsSalesOrder.SalesCategoryName,
                    SalesUnitId = partsSalesOrder.SalesUnitId,
                    SalesUnitCode = partsSalesOrder.SalesUnitCode,
                    SalesUnitName = partsSalesOrder.SalesUnitName,
                    SalesUnitOwnerCompanyId = partsSalesOrder.SalesUnitOwnerCompanyId,
                    SalesUnitOwnerCompanyCode = partsSalesOrder.SalesUnitOwnerCompanyCode,
                    SalesUnitOwnerCompanyName = partsSalesOrder.SalesUnitOwnerCompanyName,
                    SourceBillCode = partsSalesOrder.SourceBillCode,
                    SourceBillId = partsSalesOrder.SourceBillId,
                    CustomerAccountId = partsSalesOrder.CustomerAccountId,
                    InvoiceReceiveCompanyId = partsSalesOrder.InvoiceReceiveCompanyId,
                    InvoiceReceiveCompanyCode = partsSalesOrder.InvoiceReceiveCompanyCode,
                    InvoiceReceiveCompanyName = partsSalesOrder.InvoiceReceiveCompanyName,
                    InvoiceReceiveCompanyType = partsSalesOrder.InvoiceReceiveCompanyType,
                    InvoiceReceiveSaleCateId = partsSalesOrder.InvoiceReceiveSaleCateId,
                    InvoiceReceiveSaleCateName = partsSalesOrder.InvoiceReceiveSaleCateName,
                    SubmitCompanyId = partsSalesOrder.SubmitCompanyId,
                    SubmitCompanyCode = partsSalesOrder.SubmitCompanyCode,
                    SubmitCompanyName = partsSalesOrder.SubmitCompanyName,
                    FirstClassStationId = partsSalesOrder.FirstClassStationId,
                    FirstClassStationCode = partsSalesOrder.FirstClassStationCode,
                    FirstClassStationName = partsSalesOrder.FirstClassStationName,
                    PartsSalesOrderTypeId = partsSalesOrder.PartsSalesOrderTypeId,
                    PartsSalesOrderTypeName = partsSalesOrder.PartsSalesOrderTypeName,
                    IfAgencyService = partsSalesOrder.IfAgencyService,
                    SalesActivityDiscountRate = partsSalesOrder.SalesActivityDiscountRate,
                    SalesActivityDiscountAmount = partsSalesOrder.SalesActivityDiscountAmount,
                    RequestedShippingTime = partsSalesOrder.RequestedShippingTime,
                    RequestedDeliveryTime = partsSalesOrder.RequestedDeliveryTime,
                    IfDirectProvision = partsSalesOrder.IfDirectProvision,
                    SecondLevelOrderType = partsSalesOrder.SecondLevelOrderType,
                    Remark = "随车行$" + partsSalesOrder.Remark,
                    ApprovalComment = partsSalesOrder.ApprovalComment,
                    ContactPerson = partsSalesOrder.ContactPerson,
                    ContactPhone = partsSalesOrder.ContactPhone,
                    ReceivingCompanyId = partsSalesOrder.ReceivingCompanyId,
                    ReceivingCompanyCode = partsSalesOrder.ReceivingCompanyCode,
                    ReceivingCompanyName = partsSalesOrder.ReceivingCompanyName,
                    ShippingMethod = partsSalesOrder.ShippingMethod,
                    ReceivingWarehouseId = partsSalesOrder.ReceivingWarehouseId,
                    ReceivingWarehouseName = partsSalesOrder.ReceivingWarehouseName,
                    ReceivingAddress = partsSalesOrder.ReceivingAddress,
                    WarehouseId = warehouse.Id,
                    CustomerType = partsSalesOrder.CustomerType,
                    ProvinceID = partsSalesOrder.ProvinceID,
                    Province = partsSalesOrder.Province,
                    City = partsSalesOrder.City,
                    EnterpriseType = partsSalesOrder.EnterpriseType,
                    ReceivingMailbox = partsSalesOrder.ReceivingMailbox,
                    ERPSourceOrderCode = partsSalesOrder.ERPSourceOrderCode,
                    InvoiceCustomerName = partsSalesOrder.InvoiceCustomerName,
                    BankAccount = partsSalesOrder.BankAccount,
                    Taxpayeridentification = partsSalesOrder.Taxpayeridentification,
                    InvoiceAdress = partsSalesOrder.InvoiceAdress,
                    InvoicePhone = partsSalesOrder.InvoicePhone,
                    InvoiceType = partsSalesOrder.InvoiceType,
                    Status = (int)DcsPartsSalesOrderStatus.审批完成,
                    InnerSourceId = partsSalesOrder.Id,
                    InnerSourceCode = partsSalesOrder.Code,
                    IfInnerDirectProvision = true,
                    ERPOrderCode = partsSalesOrder.ERPOrderCode
                };
                //获取业务代码
                var partsSalesOrderType = ObjectContext.PartsSalesOrderTypes.Where(v => v.Id == partsSalesOrder.PartsSalesOrderTypeId).FirstOrDefault();
                if(partsSalesOrderType == null)
                    newpartsSalesOrder.Code = CodeGenerator.Generate("PartsSalesOrder", newpartsSalesOrder.SalesUnitOwnerCompanyCode);
                else
                    newpartsSalesOrder.Code = partsSalesOrderType.BusinessCode + CodeGenerator.Generate("PartsSalesOrder", newpartsSalesOrder.SalesUnitOwnerCompanyCode);



                string yxConnString = ConfigurationManager.ConnectionStrings["DcsServiceConn"].ConnectionString;
                OracleConnection fdConn = new OracleConnection(yxConnString);
                string dcsnextvaluesql = "select s_partssalesorder.nextval as nextval from dual";
                int dcsnextvalue = 0;
                setAttributeValue(fdConn, dcsnextvaluesql, (command) => {
                    var dataRead = command.ExecuteReader();
                    if(dataRead.Read()) {
                        dcsnextvalue = Convert.ToInt32(dataRead["nextval"]);
                    }
                    if(dcsnextvalue == 0) {
                        throw new ValidationException("序列号异常，请检查");
                    }
                    newpartsSalesOrder.Id = dcsnextvalue;
                });
                //生成销售订单处理单
                var newPartsSalesOrderProcess = new PartsSalesOrderProcess {
                    OriginalSalesOrderId = dcsnextvalue,
                    Code = CodeGenerator.Generate("PartsSalesOrderProcess", userInfo.EnterpriseCode),
                    BillStatusBeforeProcess = orderPartsSalesOrderProcess.BillStatusBeforeProcess,
                    BillStatusAfterProcess = (int)DcsPartsSalesOrderStatus.审批完成,
                    RequestedShippingTime = orderPartsSalesOrderProcess.RequestedShippingTime,
                    RequestedDeliveryTime = orderPartsSalesOrderProcess.RequestedDeliveryTime,
                    PartsSalesActivityName = orderPartsSalesOrderProcess.PartsSalesActivityName,
                    CurrentFulfilledAmount = orderPartsSalesOrderProcess.CurrentFulfilledAmount,
                    ApprovalComment = orderPartsSalesOrderProcess.ApprovalComment,
                    ShippingMethod = orderPartsSalesOrderProcess.ShippingMethod,
                    ShippingInformationRemark = orderPartsSalesOrderProcess.ShippingInformationRemark,
                    Remark = orderPartsSalesOrderProcess.Remark,
                    CreatorId = userInfo.Id,
                    CreatorName = userInfo.Name,
                    CreateTime = DateTime.Now,
                };
                //  newpartsSalesOrder.PartsSalesOrderProcesses.Add(newPartsSalesOrderProcess);
                var supplierCompanyIds = insideMarketingProcessDetails.Select(r => r.SupplierCompanyId).Distinct();
                //订单审批过账(newpartsSalesOrder.CustomerAccountId, newPartsSalesOrderProcess.CurrentFulfilledAmount);

                //var _warehouse = ObjectContext.Warehouses.Where(r => r.Type == (int)DcsWarehouseType.虚拟库 && r.StorageCompanyId == newpartsSalesOrder.SalesUnitOwnerCompanyId && r.BranchId == newpartsSalesOrder.BranchId && r.Status == (int)DcsBaseDataStatus.有效);
                //var salesUnitAffiWarehouse = from a in _warehouse join b in ObjectContext.SalesUnitAffiWarehouses on a.Id equals b.WarehouseId join c in ObjectContext.SalesUnits on b.SalesUnitId equals c.Id where c.PartsSalesCategoryId == partsSalesOrder.SalesCategoryId select a.Id;
                //var warehouse = _warehouse.FirstOrDefault(r => salesUnitAffiWarehouse.Contains(r.Id));
                //if(warehouse == null)
                //    //对应仓库查询不到
                //    throw new ValidationException(ErrorStrings.PartsSalesOrderProcess_Validation4);
                var partsSalesCategory = this.ObjectContext.PartsSalesCategories.FirstOrDefault(r => r.Name == "随车行" && r.Status == (int)DcsBaseDataStatus.有效);
                if(partsSalesCategory == null)
                    throw new ValidationException("未找到有效的随车行品牌信息,请维护！");
                var partsPurchaseOrderType = this.ObjectContext.PartsPurchaseOrderTypes.FirstOrDefault(r => r.BranchId == newpartsSalesOrder.BranchId && r.PartsSalesCategoryId == partsSalesCategory.Id && r.Status == (int)DcsBaseDataStatus.有效);
                if(partsPurchaseOrderType == null)
                    throw new ValidationException("未找到有效的内部直供采购订单的订单类型,请维护!");
                var partsSupplier = this.ObjectContext.PartsSuppliers.FirstOrDefault(r => r.Code == "FT001078" && r.Status == (int)DcsBaseDataStatus.有效);
                if(partsSupplier == null) {
                    throw new ValidationException("未找有效的到欧曼虚拟供应商FT001078的信息,请维护!");
                }
                //生成配件采购订单
                var newPartsPurchaseOrder = new PartsPurchaseOrder {
                    BranchId = newpartsSalesOrder.BranchId,
                    BranchCode = newpartsSalesOrder.BranchCode,
                    BranchName = newpartsSalesOrder.BranchName,
                    DirectRecWarehouseId = newpartsSalesOrder.ReceivingWarehouseId,
                    DirectRecWarehouseName = newpartsSalesOrder.ReceivingWarehouseName,
                    WarehouseId = warehouse.Id,
                    WarehouseName = warehouse.Name,
                    PartsSalesCategoryId = partsSalesCategory.Id,
                    PartsSalesCategoryName = "随车行",
                    ReceivingCompanyId = newpartsSalesOrder.SubmitCompanyId,
                    ReceivingCompanyName = newpartsSalesOrder.SubmitCompanyName,
                    ReceivingAddress = newpartsSalesOrder.ReceivingAddress,
                    PartsPurchaseOrderTypeId = partsPurchaseOrderType.Id,
                    RequestedDeliveryTime = newpartsSalesOrder.RequestedDeliveryTime.Value,
                    ShippingMethod = newpartsSalesOrder.ShippingMethod,
                    Status = (int)DcsPartsPurchaseOrderStatus.发运完毕,
                    PartsSupplierId = partsSupplier.Id,
                    PartsSupplierCode = partsSupplier.Code,
                    PartsSupplierName = partsSupplier.Name,
                    IfDirectProvision = false,
                    IfInnerDirectProvision = true,
                    OriginalRequirementBillId = dcsnextvalue,
                    OriginalRequirementBillCode = newpartsSalesOrder.Code,
                    OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.配件销售订单,
                    InStatus = (int)DcsPurchaseInStatus.未入库,
                    Remark = newpartsSalesOrder.Remark + (string.IsNullOrWhiteSpace(newpartsSalesOrder.ContactPerson) ? "" : ("   联系人:" + newpartsSalesOrder.ContactPerson)) + (string.IsNullOrWhiteSpace(newpartsSalesOrder.ContactPhone) ? "" : ("   联系人电话:" + newpartsSalesOrder.ContactPhone))
                };
                newPartsPurchaseOrder.Code = CodeGenerator.Generate("PartsPurchaseOrder", newPartsPurchaseOrder.BranchCode);

                //生成供应商发运单
                var newSupplierShippingOrder = new SupplierShippingOrder {
                    PartsSupplierId = newPartsPurchaseOrder.PartsSupplierId,
                    PartsSupplierCode = newPartsPurchaseOrder.PartsSupplierCode,
                    PartsSupplierName = newPartsPurchaseOrder.PartsSupplierName,
                    DirectRecWarehouseId = newPartsPurchaseOrder.DirectRecWarehouseId,
                    DirectRecWarehouseName = newPartsPurchaseOrder.DirectRecWarehouseName,
                    IfDirectProvision = false,
                    BranchId = newPartsPurchaseOrder.BranchId,
                    BranchCode = newPartsPurchaseOrder.BranchCode,
                    BranchName = newPartsPurchaseOrder.BranchName,
                    PartsPurchaseOrderId = newPartsPurchaseOrder.Id,
                    PartsSalesCategoryId = newPartsPurchaseOrder.PartsSalesCategoryId,
                    PartsSalesCategoryName = newPartsPurchaseOrder.PartsSalesCategoryName,
                    PartsPurchaseOrderCode = newPartsPurchaseOrder.Code,
                    OriginalRequirementBillId = dcsnextvalue,
                    OriginalRequirementBillType = (int)newPartsPurchaseOrder.OriginalRequirementBillType,
                    OriginalRequirementBillCode = newPartsPurchaseOrder.OriginalRequirementBillCode,
                    ReceivingWarehouseId = newPartsPurchaseOrder.WarehouseId,
                    ReceivingWarehouseName = newPartsPurchaseOrder.WarehouseName,
                    ReceivingCompanyId = newPartsPurchaseOrder.ReceivingCompanyId,
                    ReceivingCompanyName = newPartsPurchaseOrder.ReceivingCompanyName,
                    ReceivingAddress = newPartsPurchaseOrder.ReceivingAddress,
                    ShippingMethod = (int)newPartsPurchaseOrder.ShippingMethod,
                    Status = (int)DcsSupplierShippingOrderStatus.收货确认,
                    RequestedDeliveryTime = newPartsPurchaseOrder.RequestedDeliveryTime,
                    PlanDeliveryTime = newPartsPurchaseOrder.RequestedDeliveryTime,
                    Remark = newPartsPurchaseOrder.Remark,
                    GPMSPurOrderCode = newPartsPurchaseOrder.GPMSPurOrderCode,
                    PlanSource = newPartsPurchaseOrder.PlanSource,
                };
                var partsSupplierRelations = ObjectContext.PartsSupplierRelations.Where(r => r.BranchId == newpartsSalesOrder.BranchId && partsIds.Contains(r.PartId) && supplierCompanyIds.Contains(r.SupplierId) && r.Status == (int)DcsBaseDataStatus.有效).ToArray();

                var partsPurchasePricingDetails = DomainService.查询配件采购价格(DateTime.Now, newPartsPurchaseOrder.PartsSupplierId, partsIds, newpartsSalesOrder.BranchId, null, null, newPartsPurchaseOrder.PartsSalesCategoryId).ToArray();
                var salesCenterstrategy = this.ObjectContext.SalesCenterstrategies.SingleOrDefault(r => r.PartsSalesCategoryId == newpartsSalesOrder.SalesCategoryId && r.Status == (int)DcsBaseDataStatus.有效);
                if(salesCenterstrategy == null)
                    throw new ValidationException("未找到对应的销售中心策略");
                var spareParts = this.ObjectContext.SpareParts.Where(r => partsIds.Contains(r.Id)).ToArray();
                var serialNumber = 1;
                var neworderdetails = new List<PartsSalesOrderDetail>();
                foreach(var orderdetail in partsSalesOrderDetails.Where(r => partsIds.Contains(r.SparePartId)).ToArray()) {
                    var tempSupplierProcessDetails = insideMarketingProcessDetails.SingleOrDefault(r => r.SparePartId == orderdetail.SparePartId);
                    var partsSupplierRelation = partsSupplierRelations.FirstOrDefault(r => r.PartId == orderdetail.SparePartId);
                    if(partsSupplierRelation == null)
                        throw new ValidationException(string.Format(ErrorStrings.PartsSalesOrderProcess_Validation7, orderdetail.SparePartCode, tempSupplierProcessDetails.SupplierCompanyCode));
                    var sparePart = spareParts.FirstOrDefault(r => r.Id == orderdetail.SparePartId);
                    Debug.Assert(tempSupplierProcessDetails != null, "tempSupplierProcessDetails != null");
                    //生成销售订单清单
                    var newPartsSalesOrderDetail = new PartsSalesOrderDetail {
                        PartsSalesOrderId = dcsnextvalue,
                        SparePartId = orderdetail.SparePartId,
                        SparePartCode = orderdetail.SparePartCode,
                        SparePartName = orderdetail.SparePartName,
                        MeasureUnit = sparePart.MeasureUnit,
                        OrderedQuantity = tempSupplierProcessDetails.CurrentFulfilledQuantity,
                        ApproveQuantity = tempSupplierProcessDetails.CurrentFulfilledQuantity,
                        DiscountedPrice = orderdetail.DiscountedPrice,
                        OriginalPrice = orderdetail.OriginalPrice,
                        OrderPrice = orderdetail.OrderPrice,
                        CustOrderPriceGradeCoefficient = orderdetail.CustOrderPriceGradeCoefficient,
                        OrderSum = tempSupplierProcessDetails.CurrentFulfilledQuantity * orderdetail.OrderPrice,
                        EstimatedFulfillTime = orderdetail.EstimatedFulfillTime,
                        Remark = "原销售订单转内部直供"
                    };
                    neworderdetails.Add(newPartsSalesOrderDetail);
                    orderdetail.OrderedQuantity = orderdetail.OrderedQuantity - newPartsSalesOrderDetail.OrderedQuantity;
                    orderdetail.OrderSum = orderdetail.OrderedQuantity * orderdetail.OrderPrice;
                    orderdetail.Remark = orderdetail.Remark + "转内部直供数量:" + tempSupplierProcessDetails.CurrentFulfilledQuantity;
                    UpdateToDatabase(orderdetail);
                    //生成销售订单处理清单
                    var newPartsSalesOrderProcessDetail = new PartsSalesOrderProcessDetail {
                        WarehouseId = tempSupplierProcessDetails.WarehouseId,
                        WarehouseCode = tempSupplierProcessDetails.WarehouseCode,
                        WarehouseName = tempSupplierProcessDetails.WarehouseName,
                        PurchaseWarehouseId = tempSupplierProcessDetails.PurchaseWarehouseId,
                        PurchaseWarehouseCode = tempSupplierProcessDetails.PurchaseWarehouseCode,
                        PurchaseWarehouseName = tempSupplierProcessDetails.PurchaseWarehouseName,
                        SparePartId = tempSupplierProcessDetails.SparePartId,
                        SparePartCode = tempSupplierProcessDetails.SparePartCode,
                        SparePartName = tempSupplierProcessDetails.SparePartName,
                        NewPartId = tempSupplierProcessDetails.NewPartId,
                        NewPartCode = tempSupplierProcessDetails.NewPartCode,
                        NewPartName = tempSupplierProcessDetails.NewPartName,
                        IfDirectSupply = tempSupplierProcessDetails.IfDirectSupply,
                        MeasureUnit = sparePart.MeasureUnit,
                        OrderedQuantity = newPartsSalesOrderDetail.OrderedQuantity,
                        CurrentFulfilledQuantity = newPartsSalesOrderDetail.OrderedQuantity,
                        OrderPrice = tempSupplierProcessDetails.OrderPrice,
                        AdjustPrice = tempSupplierProcessDetails.AdjustPrice,
                        OrderProcessStatus = tempSupplierProcessDetails.OrderProcessStatus,
                        OrderProcessMethod = tempSupplierProcessDetails.OrderProcessMethod,
                        SupplierCompanyId = tempSupplierProcessDetails.SupplierCompanyId,
                        SupplierCompanyCode = tempSupplierProcessDetails.SupplierCompanyCode,
                        SupplierCompanyName = tempSupplierProcessDetails.SupplierCompanyName,
                        SupplierWarehouseId = tempSupplierProcessDetails.SupplierWarehouseId,
                        SupplierWarehouseCode = tempSupplierProcessDetails.SupplierWarehouseCode,
                        SupplierWarehouseName = tempSupplierProcessDetails.SupplierWarehouseName,
                        CreateTime = tempSupplierProcessDetails.CreateTime,
                        CreatorId = tempSupplierProcessDetails.CreatorId,
                        CreatorName = tempSupplierProcessDetails.CreatorName
                    };
                    newPartsSalesOrderProcess.PartsSalesOrderProcessDetails.Add(newPartsSalesOrderProcessDetail);
                    //生成采购订单清单
                    var partsPurchasePricingDetail = partsPurchasePricingDetails.FirstOrDefault(r => r.PartId == orderdetail.SparePartId);
                    var partsPurchaseOrderDetail = new PartsPurchaseOrderDetail {
                        SparePartId = tempSupplierProcessDetails.SparePartId,
                        SparePartCode = tempSupplierProcessDetails.SparePartCode,
                        SparePartName = tempSupplierProcessDetails.SparePartName,
                        MeasureUnit = sparePart.MeasureUnit,
                        Specification = sparePart.Specification,
                        SupplierPartCode = partsSupplierRelation.SupplierPartCode,
                        OrderAmount = tempSupplierProcessDetails.CurrentFulfilledQuantity,
                        ConfirmedAmount = tempSupplierProcessDetails.CurrentFulfilledQuantity,
                        ShippingAmount = tempSupplierProcessDetails.CurrentFulfilledQuantity,
                        DirectOrderAmount = tempSupplierProcessDetails.CurrentFulfilledQuantity,
                        SerialNumber = serialNumber++,
                    };
                    if(salesCenterstrategy.PartsPurchasePricingStrategy == (int)DcsBranchstrategyPartsPurchasePricingStrategy.先定价) {
                        if(partsPurchasePricingDetail == null) {
                            throw new ValidationException(string.Format(ErrorStrings.PartsSalesOrderProcess_Validation8, tempSupplierProcessDetails.SparePartCode, tempSupplierProcessDetails.SupplierCompanyCode));
                        }
                        partsPurchaseOrderDetail.UnitPrice = partsPurchasePricingDetail.PurchasePrice;
                    } else {
                        decimal price = 0;
                        if(partsPurchasePricingDetail != null) {
                            price = partsPurchasePricingDetail.PurchasePrice;
                        }
                        partsPurchaseOrderDetail.UnitPrice = price;
                    }
                    newPartsPurchaseOrder.PartsPurchaseOrderDetails.Add(partsPurchaseOrderDetail);
                    //生成供应商发运单清单
                    var supplierShippingDetail = new SupplierShippingDetail {
                        SerialNumber = partsPurchaseOrderDetail.SerialNumber,
                        SparePartId = partsPurchaseOrderDetail.SparePartId,
                        SparePartCode = partsPurchaseOrderDetail.SparePartCode,
                        SparePartName = partsPurchaseOrderDetail.SparePartName,
                        SupplierPartCode = partsPurchaseOrderDetail.SupplierPartCode,
                        MeasureUnit = sparePart.MeasureUnit,
                        Quantity = partsPurchaseOrderDetail.OrderAmount,
                        ConfirmedAmount = partsPurchaseOrderDetail.ConfirmedAmount,
                        UnitPrice = partsPurchaseOrderDetail.UnitPrice,
                        SpareOrderRemark = partsPurchaseOrderDetail.Remark
                    };
                    newSupplierShippingOrder.SupplierShippingDetails.Add(supplierShippingDetail);
                }

                newpartsSalesOrder.TotalAmount = neworderdetails.Sum(r => r.OrderSum);
                // InsertToDatabase(newpartsSalesOrder);
                new PartsSalesOrderAch(this.DomainService).InsertPartsSalesOrderValidate(newpartsSalesOrder);
                new PartsSalesOrderAch(this.DomainService).insertintosaleorder(newpartsSalesOrder, neworderdetails, yxConnString);
                newPartsPurchaseOrder.TotalAmount = newPartsPurchaseOrder.PartsPurchaseOrderDetails.Sum(r => r.UnitPrice * r.OrderAmount);
                InsertToDatabase(newPartsPurchaseOrder);
                new PartsPurchaseOrderAch(this.DomainService).InsertPartsPurchaseOrderValidate(newPartsPurchaseOrder);

                InsertToDatabase(newPartsSalesOrderProcess);
                // InsertPartsSalesOrderProcessValidate(newPartsSalesOrderProcess);

                InsertToDatabase(newSupplierShippingOrder);
                new SupplierShippingOrderAch(this.DomainService).InsertSupplierShippingOrderValidate(newSupplierShippingOrder);
                partsSalesOrder.TotalAmount = partsSalesOrder.PartsSalesOrderDetails.Sum(r => r.OrderSum);

                partsSalesOrder.Status = (int)DcsPartsSalesOrderStatus.审批完成;
                UpdateToDatabase(partsSalesOrder);
                审批销售订单内转直供生成雷萨数据(newPartsPurchaseOrder, newpartsSalesOrder);
            }
        }

        internal void 审批销售订单转中心库(PartsSalesOrderProcess orderPartsSalesOrderProcess, List<PartsSalesOrderProcessDetail> marketingProcessDetailProcess, List<PartsSalesOrderDetail> partsSalesOrderDetails, PartsSalesOrder partsSalesOrder, PartsSalesCategory orderPartsSalesCategory, out PartsInboundPlan curPartsInboundPlan, out PartsSalesReturnBill curPartsSalesReturnBill) {
            var insideMarketingProcessDetails = marketingProcessDetailProcess.Where(r => r.OrderProcessMethod == (int)DcsPartsSalesOrderProcessDetailProcessMethod.转中心库).ToArray();
            #region 业务判断
            var userInfo = Utils.GetCurrentUserInfo();
            var partsIds = insideMarketingProcessDetails.Select(r => r.SparePartId).ToArray();
            var warehouse = this.ObjectContext.Warehouses.SingleOrDefault(r => r.Id == partsSalesOrder.WarehouseId);
            if(warehouse == null)
                throw new ValidationException(string.Format("Id为{0}仓库不存在", partsSalesOrder.WarehouseId));
            var salesUnit = this.ObjectContext.SalesUnits.FirstOrDefault(r => r.Id == partsSalesOrder.SalesUnitId && r.Status != (int)DcsMasterDataStatus.作废);
            if(salesUnit == null)
                throw new ValidationException(string.Format("Id为{0}销售组织不存在", partsSalesOrder.SalesUnitId));
            var userInfoCompany = this.ObjectContext.Companies.SingleOrDefault(r => r.Id == salesUnit.OwnerCompanyId);
            if(userInfoCompany == null)
                throw new ValidationException("销售订单所属销售组织隶属企业不存在");
            var accountGroup = this.ObjectContext.AccountGroups.FirstOrDefault(r => r.Id == salesUnit.AccountGroupId && r.Status != (int)DcsMasterDataStatus.作废);
            if(accountGroup == null)
                throw new ValidationException(string.Format("Id为{0}账户组不存在", salesUnit.AccountGroupId));
            var insideMarketingProcessDetail = insideMarketingProcessDetails.First();
            var customerAccount = this.ObjectContext.CustomerAccounts.FirstOrDefault(r => r.AccountGroupId == accountGroup.Id && r.CustomerCompanyId == insideMarketingProcessDetail.SupplierCompanyId && r.Status != (int)DcsMasterDataStatus.作废);
            if(customerAccount == null)
                throw new ValidationException(string.Format("账户组Id为{0}客户账户不存在", accountGroup.Id));
            #endregion
            var sparePartId = marketingProcessDetailProcess.Select(o => o.SparePartId);

            //判断库存是否足够
            if(sparePartId.ToArray().Count() > 0) {
                var warehousePartsStocks = DomainService.查询仓库库存销售审核(insideMarketingProcessDetail.SupplierCompanyId.Value, insideMarketingProcessDetail.SupplierWarehouseId, sparePartId.ToArray()).ToArray();
                foreach(var spid in sparePartId) {
                    var appqty = marketingProcessDetailProcess.Where(t => t.SparePartId == spid).Sum(y => y.CurrentFulfilledQuantity);
                    var stock = warehousePartsStocks.Where(t => t.SparePartId == spid).FirstOrDefault();
                    if(stock == null || stock.UsableQuantity < appqty) {
                        throw new ValidationException(stock.SparePartCode + "中心库可用库存不足");
                    }
                }
            }
            #region 中心库向总部的配件销售退货单赋值
            //中心库向总部的配件销售退货单赋值
            var partsSalesReturnBill = new PartsSalesReturnBill();
            partsSalesReturnBill.Code = CodeGenerator.Generate("PartsSalesReturnBill", partsSalesOrder.SalesUnitOwnerCompanyCode);
            partsSalesReturnBill.ReturnCompanyId = insideMarketingProcessDetail.SupplierCompanyId ?? 0;
            partsSalesReturnBill.ReturnCompanyCode = insideMarketingProcessDetail.SupplierCompanyCode;
            partsSalesReturnBill.ReturnCompanyName = insideMarketingProcessDetail.SupplierCompanyName;
            partsSalesReturnBill.InvoiceReceiveCompanyId = partsSalesReturnBill.ReturnCompanyId;
            partsSalesReturnBill.InvoiceReceiveCompanyCode = partsSalesReturnBill.ReturnCompanyCode;
            partsSalesReturnBill.InvoiceReceiveCompanyName = partsSalesReturnBill.ReturnCompanyName;
            partsSalesReturnBill.SubmitCompanyId = partsSalesReturnBill.ReturnCompanyId;
            partsSalesReturnBill.SubmitCompanyCode = partsSalesReturnBill.ReturnCompanyCode;
            partsSalesReturnBill.SubmitCompanyName = partsSalesReturnBill.ReturnCompanyName;
            partsSalesReturnBill.ShippingMethod = (int)DcsPartsShippingMethod.公路物流;
            partsSalesReturnBill.SalesUnitId = partsSalesOrder.SalesUnitId;
            partsSalesReturnBill.SalesUnitName = partsSalesOrder.SalesUnitName;
            partsSalesReturnBill.SalesUnitOwnerCompanyId = partsSalesOrder.SalesUnitOwnerCompanyId;
            partsSalesReturnBill.SalesUnitOwnerCompanyCode = partsSalesOrder.SalesUnitOwnerCompanyCode;
            partsSalesReturnBill.SalesUnitOwnerCompanyName = partsSalesOrder.SalesUnitOwnerCompanyName;
            partsSalesReturnBill.CustomerAccountId = customerAccount.Id;
            partsSalesReturnBill.BranchId = partsSalesOrder.SalesUnitOwnerCompanyId;
            partsSalesReturnBill.BranchCode = partsSalesOrder.SalesUnitOwnerCompanyCode;
            partsSalesReturnBill.BranchName = partsSalesOrder.SalesUnitOwnerCompanyName;
            partsSalesReturnBill.InvoiceRequirement = (int)DcsPartsSalesReturnBillInvoiceRequirement.开红字发票;
            partsSalesReturnBill.ReturnWarehouseId = insideMarketingProcessDetail.SupplierWarehouseId;
            partsSalesReturnBill.ReturnWarehouseCode = insideMarketingProcessDetail.SupplierWarehouseCode;
            partsSalesReturnBill.ReturnWarehouseName = insideMarketingProcessDetail.SupplierWarehouseName;
            partsSalesReturnBill.WarehouseId = partsSalesOrder.WarehouseId;
            partsSalesReturnBill.WarehouseCode = warehouse.Code;
            partsSalesReturnBill.WarehouseName = warehouse.Name;
            partsSalesReturnBill.Status = (int)DcsPartsSalesReturnBillStatus.审批通过;
            partsSalesReturnBill.ReturnType = (int)DcsPartsSalesReturnBillReturnType.特殊退货;
            partsSalesReturnBill.ReturnReason = "转单" + partsSalesOrder.Code;
            partsSalesReturnBill.FinalApproverId = userInfo.Id;
            partsSalesReturnBill.FinalApproverName = userInfo.Name;
            partsSalesReturnBill.FinalApproveTime = DateTime.Now;
            partsSalesReturnBill.Remark = partsSalesOrder.Remark;
            partsSalesReturnBill.IsTurn = true;
            #endregion

            int id = 0;
            #region 添加配件出库计划单
            var partsOutboundPlan = new PartsOutboundPlan {
                Id = id++,
                WarehouseId = partsSalesReturnBill.ReturnWarehouseId ?? 0,
                WarehouseCode = partsSalesReturnBill.ReturnWarehouseCode,
                WarehouseName = partsSalesReturnBill.ReturnWarehouseName,
                StorageCompanyId = partsSalesReturnBill.ReturnCompanyId,
                StorageCompanyCode = partsSalesReturnBill.ReturnCompanyCode,
                StorageCompanyName = partsSalesReturnBill.ReturnCompanyName,
                StorageCompanyType = (int)DcsCompanyType.代理库,
                BranchId = partsSalesOrder.SalesUnitOwnerCompanyId,
                BranchCode = partsSalesOrder.SalesUnitOwnerCompanyCode,
                BranchName = partsSalesOrder.SalesUnitOwnerCompanyName,
                PartsSalesCategoryId = salesUnit.PartsSalesCategoryId,
                PartsSalesOrderTypeId = partsSalesOrder.PartsSalesOrderTypeId,
                PartsSalesOrderTypeName = partsSalesOrder.PartsSalesOrderTypeName,
                CounterpartCompanyId = partsSalesOrder.SalesUnitOwnerCompanyId,
                CounterpartCompanyCode = partsSalesOrder.SalesUnitOwnerCompanyCode,
                CounterpartCompanyName = partsSalesOrder.SalesUnitOwnerCompanyName,
                ReceivingCompanyId = partsSalesOrder.SalesUnitOwnerCompanyId,
                ReceivingCompanyCode = partsSalesOrder.SalesUnitOwnerCompanyCode,
                ReceivingCompanyName = partsSalesOrder.SalesUnitOwnerCompanyName,
                ReceivingWarehouseId = partsSalesOrder.WarehouseId,
                ReceivingWarehouseCode = warehouse.Code,
                ReceivingWarehouseName = warehouse.Name,
                OutboundType = (int)DcsPartsOutboundType.采购退货,
                SourceId = partsSalesReturnBill.Id,
                SourceCode = partsSalesReturnBill.Code,
                CustomerAccountId = partsSalesReturnBill.CustomerAccountId,
                OriginalRequirementBillId = partsSalesReturnBill.Id,
                OriginalRequirementBillCode = partsSalesReturnBill.Code,
                OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.配件销售订单,
                Status = (int)DcsPartsOutboundPlanStatus.新建,
                IfWmsInterface = false,
                OrderTypeId = partsSalesOrder.PartsSalesOrderTypeId,
                OrderTypeName = partsSalesOrder.PartsSalesOrderTypeName,
                Remark ="转单" +partsSalesOrder.Code,
                IsTurn=true,
				ReceivingAddress=partsSalesOrder.ReceivingAddress,
				CompanyAddressId=partsSalesOrder.CompanyAddressId
            };
            #endregion

            #region 添加配件出库单
            //var partsOutboundBill = new PartsOutboundBill {
            //    Id = id++,
            //    //PartsOutboundPlanId = partsOutboundPlan.Id,
            //    WarehouseId = partsOutboundPlan.WarehouseId,
            //    WarehouseCode = partsOutboundPlan.WarehouseCode,
            //    WarehouseName = partsOutboundPlan.WarehouseName,
            //    StorageCompanyId = partsOutboundPlan.StorageCompanyId,
            //    StorageCompanyCode = partsOutboundPlan.StorageCompanyCode,
            //    StorageCompanyName = partsOutboundPlan.StorageCompanyName,
            //    StorageCompanyType = partsOutboundPlan.StorageCompanyType,
            //    BranchId = partsOutboundPlan.BranchId,
            //    BranchCode = partsOutboundPlan.BranchCode,
            //    BranchName = partsOutboundPlan.BranchName,
            //    PartsSalesCategoryId = partsOutboundPlan.PartsSalesCategoryId,
            //    PartsSalesOrderTypeId = partsOutboundPlan.PartsSalesOrderTypeId,
            //    PartsSalesOrderTypeName = partsOutboundPlan.PartsSalesOrderTypeName,
            //    CounterpartCompanyId = partsOutboundPlan.CounterpartCompanyId,
            //    CounterpartCompanyCode = partsOutboundPlan.CounterpartCompanyCode,
            //    CounterpartCompanyName = partsOutboundPlan.CounterpartCompanyName,
            //    ReceivingCompanyId = partsOutboundPlan.ReceivingCompanyId,
            //    ReceivingCompanyCode = partsOutboundPlan.ReceivingCompanyCode,
            //    ReceivingCompanyName = partsOutboundPlan.ReceivingCompanyName,
            //    ReceivingWarehouseId = partsOutboundPlan.ReceivingWarehouseId,
            //    ReceivingWarehouseCode = partsOutboundPlan.ReceivingWarehouseCode,
            //    ReceivingWarehouseName = partsOutboundPlan.ReceivingWarehouseName,
            //    OutboundType = partsOutboundPlan.OutboundType,
            //    CustomerAccountId = partsOutboundPlan.CustomerAccountId,
            //    OriginalRequirementBillId = partsOutboundPlan.OriginalRequirementBillId,
            //    OriginalRequirementBillCode = partsOutboundPlan.OriginalRequirementBillCode,
            //    OriginalRequirementBillType = partsOutboundPlan.OriginalRequirementBillType,
            //    SettlementStatus = (int)DcsPartsSettlementStatus.待结算,
            //    Remark = partsSalesOrder.Remark,              
            //};
            #endregion

            #region 新增总部入库检验单
            //var partsInboundCheckBill = new PartsInboundCheckBill {
            //    WarehouseId = partsSalesOrder.WarehouseId ?? 0,
            //    WarehouseCode = warehouse.Code,
            //    WarehouseName = warehouse.Name,
            //    StorageCompanyId = partsSalesOrder.SalesUnitOwnerCompanyId,
            //    StorageCompanyCode = partsSalesOrder.SalesUnitOwnerCompanyCode,
            //    StorageCompanyName = partsSalesOrder.SalesUnitOwnerCompanyName,
            //    StorageCompanyType = (int)DcsCompanyType.分公司,
            //    BranchId = partsSalesOrder.BranchId,
            //    BranchCode = partsSalesOrder.BranchCode,
            //    BranchName = partsSalesOrder.BranchName,
            //    CounterpartCompanyId = insideMarketingProcessDetail.SupplierCompanyId ?? 0,
            //    CounterpartCompanyCode = insideMarketingProcessDetail.SupplierCompanyCode,
            //    CounterpartCompanyName = insideMarketingProcessDetail.SupplierCompanyName,
            //    InboundType = (int)DcsPartsInboundType.销售退货,
            //    CustomerAccountId = customerAccount.Id,
            //    OriginalRequirementBillId = partsSalesReturnBill.Id,
            //    OriginalRequirementBillCode = partsSalesReturnBill.Code,
            //    OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.配件销售退货单,
            //    Status = (int)DcsPartsInboundCheckBillStatus.上架完成,
            //    SettlementStatus = (int)DcsPartsSettlementStatus.待结算,
            //    PartsSalesCategoryId = partsSalesOrder.SalesCategoryId,
            //    Remark = partsSalesOrder.Remark
            //};
            #endregion

            #region 总部出库计划单
            //string receivingWarehouseCode = null;
            //if(partsSalesOrder.ReceivingWarehouseId.HasValue) {
            //    var receivingWarehouse = ObjectContext.Warehouses.SingleOrDefault(r => r.Id == partsSalesOrder.ReceivingWarehouseId.Value && r.Status == (int)DcsBaseDataStatus.有效);
            //    if(receivingWarehouse != null) {
            //        receivingWarehouseCode = receivingWarehouse.Code;
            //    }
            //}
            //var zbPartsOutboundPlan = new PartsOutboundPlan {
            //    Id = id++,
            //    WarehouseId = partsSalesOrder.WarehouseId ?? 0,
            //    WarehouseName = warehouse.Name,
            //    WarehouseCode = warehouse.Code,
            //    StorageCompanyId = userInfoCompany.Id,
            //    CompanyAddressId = partsSalesOrder.CompanyAddressId,
            //    StorageCompanyCode = userInfoCompany.Code,
            //    StorageCompanyName = userInfoCompany.Name,
            //    StorageCompanyType = userInfoCompany.Type,
            //    BranchId = partsSalesOrder.BranchId,
            //    BranchCode = partsSalesOrder.BranchCode,
            //    BranchName = partsSalesOrder.BranchName,
            //    CounterpartCompanyId = partsSalesOrder.InvoiceReceiveCompanyId,
            //    CounterpartCompanyCode = partsSalesOrder.InvoiceReceiveCompanyCode,
            //    CounterpartCompanyName = partsSalesOrder.InvoiceReceiveCompanyName,
            //    SourceId = partsSalesOrder.Id,
            //    SourceCode = partsSalesOrder.Code,
            //    OutboundType = (int)DcsPartsOutboundType.配件销售,
            //    CustomerAccountId = partsSalesOrder.CustomerAccountId,
            //    OriginalRequirementBillId = partsSalesOrder.Id,
            //    OriginalRequirementBillCode = partsSalesOrder.Code,
            //    OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.配件销售订单,
            //    ReceivingCompanyId = partsSalesOrder.ReceivingCompanyId,
            //    ReceivingCompanyCode = partsSalesOrder.ReceivingCompanyCode,
            //    ReceivingCompanyName = partsSalesOrder.ReceivingCompanyName,
            //    Status = (int)DcsPartsOutboundPlanStatus.出库完成,
            //    ShippingMethod = partsSalesOrder.ShippingMethod,
            //    IfWmsInterface = warehouse.WmsInterface,
            //    PartsSalesCategoryId = salesUnit.PartsSalesCategoryId,
            //    PartsSalesOrderTypeId = partsSalesOrder.PartsSalesOrderTypeId,
            //    PartsSalesOrderTypeName = partsSalesOrder.PartsSalesOrderTypeName,
            //    OrderApproveComment = orderPartsSalesOrderProcess.ApprovalComment,
            //    SAPPurchasePlanCode = partsSalesOrder.OverseasDemandSheetNo,
            //    Remark = partsSalesOrder.Remark,
            //    ERPSourceOrderCode = partsSalesOrder.ERPSourceOrderCode,
            //    ReceivingAddress = partsSalesOrder.ReceivingAddress,
            //    ReceivingWarehouseId = partsSalesOrder.ReceivingWarehouseId,
            //    ReceivingWarehouseCode = receivingWarehouseCode,
            //    ReceivingWarehouseName = partsSalesOrder.ReceivingWarehouseName
            //};
            #endregion

            #region 总部出库单
            //var zbPartsOutboundBill = new PartsOutboundBill {
            //    Id = id++,
            //    PartsOutboundPlanId = zbPartsOutboundPlan.Id,
            //    WarehouseId = zbPartsOutboundPlan.WarehouseId,
            //    WarehouseCode = zbPartsOutboundPlan.WarehouseCode,
            //    WarehouseName = zbPartsOutboundPlan.WarehouseName,
            //    StorageCompanyId = zbPartsOutboundPlan.StorageCompanyId,
            //    StorageCompanyCode = zbPartsOutboundPlan.StorageCompanyCode,
            //    StorageCompanyName = zbPartsOutboundPlan.StorageCompanyName,
            //    StorageCompanyType = zbPartsOutboundPlan.StorageCompanyType,
            //    BranchId = zbPartsOutboundPlan.BranchId,
            //    BranchCode = zbPartsOutboundPlan.BranchCode,
            //    BranchName = zbPartsOutboundPlan.BranchName,
            //    PartsSalesCategoryId = zbPartsOutboundPlan.PartsSalesCategoryId,
            //    PartsSalesOrderTypeId = zbPartsOutboundPlan.PartsSalesOrderTypeId,
            //    PartsSalesOrderTypeName = zbPartsOutboundPlan.PartsSalesOrderTypeName,
            //    CounterpartCompanyId = zbPartsOutboundPlan.CounterpartCompanyId,
            //    CounterpartCompanyCode = zbPartsOutboundPlan.CounterpartCompanyCode,
            //    CounterpartCompanyName = zbPartsOutboundPlan.CounterpartCompanyName,
            //    ReceivingCompanyId = zbPartsOutboundPlan.ReceivingCompanyId,
            //    ReceivingCompanyCode = zbPartsOutboundPlan.ReceivingCompanyCode,
            //    ReceivingCompanyName = zbPartsOutboundPlan.ReceivingCompanyName,
            //    ReceivingWarehouseId = zbPartsOutboundPlan.ReceivingWarehouseId,
            //    ReceivingWarehouseCode = zbPartsOutboundPlan.ReceivingWarehouseCode,
            //    ReceivingWarehouseName = zbPartsOutboundPlan.ReceivingWarehouseName,
            //    OutboundType = zbPartsOutboundPlan.OutboundType,
            //    CustomerAccountId = zbPartsOutboundPlan.CustomerAccountId,
            //    OriginalRequirementBillId = zbPartsOutboundPlan.OriginalRequirementBillId,
            //    OriginalRequirementBillCode = zbPartsOutboundPlan.OriginalRequirementBillCode,
            //    OriginalRequirementBillType = zbPartsOutboundPlan.OriginalRequirementBillType,
            //    SettlementStatus = (int)DcsPartsSettlementStatus.待结算,
            //    Remark = partsSalesOrder.Remark
            //};
            #endregion

            #region 总部发运单
            //var newPartsShippingOrder = new PartsShippingOrder();
            //newPartsShippingOrder.Type = (int)DcsPartsShippingOrderType.销售;
            //newPartsShippingOrder.PartsSalesCategoryId = zbPartsOutboundPlan.PartsSalesCategoryId;
            //newPartsShippingOrder.BranchId = zbPartsOutboundPlan.BranchId;
            //newPartsShippingOrder.ShippingCompanyId = zbPartsOutboundBill.StorageCompanyId;
            //newPartsShippingOrder.ShippingCompanyCode = zbPartsOutboundBill.StorageCompanyCode;
            //newPartsShippingOrder.ShippingCompanyName = zbPartsOutboundBill.StorageCompanyName;
            //newPartsShippingOrder.SettlementCompanyId = zbPartsOutboundBill.StorageCompanyId;
            //newPartsShippingOrder.SettlementCompanyCode = zbPartsOutboundBill.StorageCompanyCode;
            //newPartsShippingOrder.SettlementCompanyName = zbPartsOutboundBill.StorageCompanyName;
            //newPartsShippingOrder.ReceivingCompanyId = zbPartsOutboundBill.ReceivingCompanyId;
            //newPartsShippingOrder.ReceivingCompanyCode = zbPartsOutboundBill.ReceivingCompanyCode;
            //newPartsShippingOrder.ReceivingCompanyName = zbPartsOutboundBill.ReceivingCompanyName;
            //newPartsShippingOrder.InvoiceReceiveSaleCateId = zbPartsOutboundBill.PartsSalesOrderTypeId;
            //newPartsShippingOrder.InvoiceReceiveSaleCateName = zbPartsOutboundBill.PartsSalesOrderTypeName;
            //newPartsShippingOrder.WarehouseId = zbPartsOutboundBill.WarehouseId;
            //newPartsShippingOrder.WarehouseCode = zbPartsOutboundBill.WarehouseCode;
            //newPartsShippingOrder.WarehouseName = zbPartsOutboundBill.WarehouseName;
            //newPartsShippingOrder.ReceivingWarehouseId = zbPartsOutboundBill.ReceivingWarehouseId;
            //newPartsShippingOrder.ReceivingWarehouseCode = zbPartsOutboundBill.ReceivingWarehouseCode;
            //newPartsShippingOrder.ReceivingWarehouseName = zbPartsOutboundBill.ReceivingWarehouseName;
            //newPartsShippingOrder.OriginalRequirementBillId = zbPartsOutboundBill.OriginalRequirementBillId;
            //newPartsShippingOrder.OriginalRequirementBillType = zbPartsOutboundBill.OriginalRequirementBillType;
            //newPartsShippingOrder.OriginalRequirementBillCode = zbPartsOutboundBill.OriginalRequirementBillCode;
            //newPartsShippingOrder.GPMSPurOrderCode = zbPartsOutboundBill.GPMSPurOrderCode;
            //newPartsShippingOrder.SAPPurchasePlanCode = zbPartsOutboundBill.SAPPurchasePlanCode;
            //newPartsShippingOrder.Status = (int)DcsPartsShippingOrderStatus.已发货;
            //newPartsShippingOrder.Remark = partsSalesOrder.Remark;
            #endregion

        //    zbPartsOutboundBill.PartsShippingOrderId = newPartsShippingOrder.Id;

            #region 配件发运单关联单
            //var newPartsShippingOrderRef = new PartsShippingOrderRef {
            //    PartsOutboundBillId = zbPartsOutboundBill.Id,
            //    PartsShippingOrderId = newPartsShippingOrder.Id
            //};
            #endregion

            #region 总部入库计划单
            //var partsInboundPlan = new PartsInboundPlan {
            //    WarehouseId = partsSalesOrder.WarehouseId ?? 0,
            //    WarehouseCode = warehouse.Code,
            //    WarehouseName = warehouse.Name,
            //    StorageCompanyId = partsSalesOrder.SalesUnitOwnerCompanyId,
            //    StorageCompanyCode = partsSalesOrder.SalesUnitOwnerCompanyCode,
            //    StorageCompanyName = partsSalesOrder.SalesUnitOwnerCompanyName,
            //    StorageCompanyType = (int)DcsCompanyType.分公司,
            //    BranchId = partsSalesOrder.BranchId,
            //    BranchCode = partsSalesOrder.BranchCode,
            //    BranchName = partsSalesOrder.BranchName,
            //    PartsSalesCategoryId = partsSalesOrder.SalesCategoryId,
            //    CounterpartCompanyId = insideMarketingProcessDetail.SupplierCompanyId ?? 0,
            //    CounterpartCompanyCode = insideMarketingProcessDetail.SupplierCompanyCode,
            //    CounterpartCompanyName = insideMarketingProcessDetail.SupplierCompanyName,
            //    //SourceId = partsSalesReturnBill.Id,
            //    SourceCode = partsSalesReturnBill.Code,
            //    InboundType = (int)DcsPartsInboundType.销售退货,
            //    CustomerAccountId = customerAccount.Id,
            //    OriginalRequirementBillId = partsSalesReturnBill.Id,
            //    OriginalRequirementBillCode = partsSalesReturnBill.Code,
            //    OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.配件销售退货单,
            //    Status = (int)DcsPartsInboundPlanStatus.上架完成,
            //    IfWmsInterface = false,
            //    ArrivalDate = DateTime.Now,
            //    PackingFinishTime = DateTime.Now,
            //    ShelvesFinishTime = DateTime.Now,
            //    ReceivingFinishTime = DateTime.Now,
            //    Remark = partsSalesOrder.Remark
            //};
            #endregion

            //var result = ObjectContext.PartsStocks.Where(r => r.Quantity > 0 && partsIds.Contains(r.PartId) && r.WarehouseId == partsOutboundBill.WarehouseId);
            //var virtualPartsStocks = (from r in result
            //                          join partsStockBatchDetail in ObjectContext.PartsStockBatchDetails on r.Id equals partsStockBatchDetail.PartsStockId into details
            //                          from v in details.DefaultIfEmpty()
            //                          join sp in ObjectContext.SpareParts on r.PartId equals sp.Id
            //                          join wh in ObjectContext.Warehouses on r.WarehouseId equals wh.Id
            //                          join wa in ObjectContext.WarehouseAreas on r.WarehouseAreaId equals wa.Id
            //                          join wac in ObjectContext.WarehouseAreaCategories on r.WarehouseAreaCategoryId equals wac.Id
            //                          where wac.Category == (int)DcsAreaType.保管区
            //                          select new VirtualPartsStock {
            //                              PartsStockId = r.Id,
            //                              WarehouseId = r.WarehouseId,
            //                              WarehouseCode = wh.Code,
            //                              WarehouseName = wh.Name,
            //                              StorageCompanyId = r.StorageCompanyId,
            //                              StorageCompanyType = r.StorageCompanyType,
            //                              BranchId = r.BranchId,
            //                              SparePartId = sp.Id,
            //                              SparePartCode = sp.Code,
            //                              SparePartName = sp.Name,
            //                              MeasureUnit = sp.MeasureUnit,
            //                              WarehouseAreaId = r.WarehouseAreaId,
            //                              WarehouseAreaCode = wa.Code,
            //                              Quantity = r.Quantity,
            //                              UsableQuantity = (int?)v.Quantity ?? r.Quantity,//此处不能改为v.Quantity 因join 可能导致v.Quantity为Null 所以强制转换有效
            //                              BatchNumber = v.BatchNumber ?? "",
            //                              InboundTime = v.InboundTime,
            //                          }).ToArray();


            ////根据检验单仓库id查找库位Id
            //var warehouseArea = this.ObjectContext.WarehouseAreas.FirstOrDefault(r => r.Status == (int)DcsBaseDataStatus.有效 && r.WarehouseId == partsSalesOrder.WarehouseId && r.AreaKind == (int)DcsAreaKind.库位 && this.ObjectContext.WarehouseAreaCategories.Any(w => w.Id == r.AreaCategoryId && w.Category == (int)DcsAreaType.检验区));
            //if(warehouseArea == null)
            //    throw new ValidationException(string.Format("仓库{0}，未找到对应的检验区库存！", warehouse.Code));
            //#region 涉及清单添加
            ////销售退货逻辑改为取时间最远的满足退货条件的销售单退货
            var partSaleOrderStatus = new[] {
                (int)DcsPartsSalesOrderStatus.部分审批, (int)DcsPartsSalesOrderStatus.审批完成, (int)DcsPartsSalesOrderStatus.终止
            };
           var sparePartIds = insideMarketingProcessDetails.Select(t => t.SparePartId).Distinct().ToArray();
           var returnDetail = (from a in ObjectContext.PartsSalesReturnBillDetails
                               join b in ObjectContext.PartsSalesReturnBills on a.PartsSalesReturnBillId equals b.Id
                               where b.Status != (int)DcsPartsSalesReturnBillStatus.终止 && b.Status != (int)DcsPartsSalesReturnBillStatus.作废 && sparePartIds.Contains(a.SparePartId) && b.ReturnCompanyId == partsSalesReturnBill.ReturnCompanyId
                               group a by new {
                                   a.SparePartId,
                                   a.PartsSalesOrderId
                               } into g
                               select new {
                                   PartsSalesOrderId = g.Key.PartsSalesOrderId,
                                   ReturnedQuantity = g.Sum(t => t.ReturnedQuantity),
                                   SparePartId = g.Key.SparePartId
                               }).ToArray();

           var outboundBillDetails = from a in this.ObjectContext.PartsOutboundBillDetails
                                     join b in this.ObjectContext.PartsOutboundBills.Where(r => r.OutboundType != (int)DcsPartsOutboundType.配件调拨 && r.OriginalRequirementBillType == (int)DcsOriginalRequirementBillType.配件销售订单) on a.PartsOutboundBillId equals b.Id
                                     select new {
                                         a.SparePartId,
                                         b.OriginalRequirementBillId,
                                         a.OutboundAmount
                                     };
           //此处ApproveQuantity取值改为 取出库单中的实际出库数量
           var oldSales = (from a in ObjectContext.PartsSalesOrders
                           join b in ObjectContext.PartsSalesOrderDetails on a.Id equals b.PartsSalesOrderId

                           where a.SubmitCompanyId == partsSalesReturnBill.ReturnCompanyId
                              && sparePartIds.Contains(b.SparePartId)
                              && partSaleOrderStatus.Contains(a.Status)
                              && a.Id != partsSalesOrder.Id
                              && b.ApproveQuantity > 0
                           select new {
                               a.Id,
                               b.SparePartId,
                               ApproveQuantity = outboundBillDetails.Where(d => d.SparePartId == b.SparePartId && d.OriginalRequirementBillId == a.Id).Sum(d => d.OutboundAmount),
                               a.ApproveTime,
                               b.OrderPrice,
                               a.Code
                           }).ToArray();
           var returnOrder = from a in oldSales
                             join b in returnDetail on new {
                                 a.SparePartId,
                                 a.Id
                             } equals new {
                                 b.SparePartId,
                                 Id = b.PartsSalesOrderId.Value
                             } into bb
                             from c in bb.DefaultIfEmpty()
                             select new {
                                 a.Id,
                                 a.SparePartId,
                                 ApproveQuantity = a.ApproveQuantity - (c == null ? 0 : c.ReturnedQuantity),
                                 a.ApproveTime,
                                 a.Code,
                                 a.OrderPrice
                             };
           var orders = returnOrder.Where(y => y.ApproveQuantity > 0).OrderBy(t => t.ApproveTime).Distinct().ToArray();
           if(orders.Count() == 0) {
               throw new ValidationException("转单失败");
           }
            var  locked=ObjectContext.PartsLockedStocks.Where(t=>sparePartIds.Contains(t.PartId) && t.WarehouseId==partsOutboundPlan.WarehouseId).ToArray();
            foreach(var item in insideMarketingProcessDetails) {
                var partsSalesOrderDetail = partsSalesOrderDetails.First(r => r.SparePartId == item.SparePartId);
                int? orderAmount = 0;
                var cOrdes = orders.Where(t => t.SparePartId == item.SparePartId);
                if(cOrdes.Count() == 0 || cOrdes.Sum(s => s.ApproveQuantity) == 0 ||cOrdes.Sum(s => s.ApproveQuantity)<item.CurrentFulfilledQuantity) {
                    throw new ValidationException("转单失败," + item.SparePartCode + "没有足够的销售单退货");
                }
                foreach(var detail in cOrdes) {
                    if(orderAmount == item.CurrentFulfilledQuantity) {
                        continue;
                    }
                    var ReturnedQuantity = 0;
                    int? ApproveQuantity = 0;
                    if(orderAmount + detail.ApproveQuantity < item.CurrentFulfilledQuantity) {
                        orderAmount += detail.ApproveQuantity;
                        ReturnedQuantity = detail.ApproveQuantity;
                        ApproveQuantity = detail.ApproveQuantity;
                    } else {
                        //销售退货清单赋值                          
                        ReturnedQuantity = item.CurrentFulfilledQuantity - orderAmount.Value;
                        ApproveQuantity = item.CurrentFulfilledQuantity - orderAmount;
                        orderAmount = item.CurrentFulfilledQuantity;
                    }

                    //销售退货清单赋值
                    var partsSalesReturnBillDetail = new PartsSalesReturnBillDetail {
                        SparePartId = item.SparePartId,
                        SparePartCode = item.SparePartCode,
                        SparePartName = item.SparePartName,
                        MeasureUnit = item.MeasureUnit,
                        ReturnedQuantity = ReturnedQuantity,
                        ApproveQuantity = ApproveQuantity,
                        OriginalOrderPrice = detail.OrderPrice,
                        ReturnPrice = detail.OrderPrice,
                        OriginalPrice = partsSalesOrderDetail.OriginalPrice,
                        PartsSalesOrderId = detail.Id,
                        OriginalRequirementBillId = detail.Id,
                        OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.配件销售订单,
                        PartsSalesOrderCode = detail.Code
                    };
                    partsSalesReturnBill.PartsSalesReturnBillDetails.Add(partsSalesReturnBillDetail);
					//添加出库计划单
                    var PartsOutboundPlanDetail = partsOutboundPlan.PartsOutboundPlanDetails.FirstOrDefault(r => r.SparePartId == item.SparePartId && r.Price == detail.OrderPrice);
                    if (PartsOutboundPlanDetail == null) {
                        partsOutboundPlan.PartsOutboundPlanDetails.Add(new PartsOutboundPlanDetail {
                           SparePartId = item.SparePartId,
						   SparePartCode = item.SparePartCode,
						   SparePartName = item.SparePartName,
						   PlannedAmount = ReturnedQuantity,
                           Price = detail.OrderPrice,
						   OriginalPrice = partsSalesOrderDetail.OrderPrice,
                        });
                    } else {
                        PartsOutboundPlanDetail.PlannedAmount += ReturnedQuantity;
                    }
                }

                
                //增加审单锁定量
                var lockQty = locked.Where(t=>t.PartId==item.SparePartId).FirstOrDefault();
                if(lockQty==null) {
                    var newLock = new PartsLockedStock {
                      WarehouseId=partsOutboundPlan.WarehouseId,
                      StorageCompanyId = partsOutboundPlan.StorageCompanyId,
                      BranchId = partsOutboundPlan.BranchId,
                      PartId=item.SparePartId,
                      LockedQuantity = item.CurrentFulfilledQuantity,
                      CreateTime=DateTime.Now,
                      CreatorId=userInfo.Id,
                      CreatorName=userInfo.Name
                    };
                    new PartsLockedStockAch(this.DomainService).InsertPartsLockedStockValidate(newLock);
                    InsertToDatabase(newLock);
                } else {
                    lockQty.LockedQuantity += item.CurrentFulfilledQuantity;
                    lockQty.ModifierId = userInfo.Id;
                    lockQty.ModifierName = userInfo.Name;
                    lockQty.ModifyTime = DateTime.Now;
                    UpdateToDatabase(lockQty);
                }
                //var virtualPartsStock = virtualPartsStocks.Where(r => r.SparePartId == item.SparePartId);
                //if(!virtualPartsStock.Any())
                //    throw new ValidationException(string.Format("编号为{0}的配件，未找到对应的保管区库存！", item.SparePartCode));
                //foreach(var partsStock in virtualPartsStock) {
                //    //添加配件出库单清单
                //    var partsOutboundBillDetail = new PartsOutboundBillDetail {
                //        SparePartId = item.SparePartId,
                //        SparePartCode = item.SparePartCode,
                //        SparePartName = item.SparePartName,
                //        SettlementPrice = item.OrderPrice,
                //        //CostPrice = EnterprisePartsCost.成本价 ,
                //        WarehouseAreaId = partsStock.WarehouseAreaId,
                //        WarehouseAreaCode = partsStock.WarehouseAreaCode,
                //        BatchNumber = partsStock.BatchNumber,
                //        OriginalPrice = partsSalesOrderDetail.OriginalPrice
                //    };
                //    //剩余出库数量
                //    var currentOutboundAmount = partsOutboundPlanDetail.PlannedAmount - (partsOutboundPlanDetail.OutboundFulfillment ?? 0);
                //    if(currentOutboundAmount == 0)
                //        break;
                //    partsOutboundBillDetail.OutboundAmount = currentOutboundAmount > partsOutboundPlanDetail.PlannedAmount ? partsOutboundPlanDetail.PlannedAmount : currentOutboundAmount;
                //    partsOutboundPlanDetail.OutboundFulfillment = (partsOutboundPlanDetail.OutboundFulfillment ?? 0) + partsOutboundBillDetail.OutboundAmount;
                //    partsOutboundBill.PartsOutboundBillDetails.Add(partsOutboundBillDetail);
                //}
                

                ////添加配件入库计划单清单
                //var partsInboundPlanDetail = new PartsInboundPlanDetail {
                //    SparePartId = item.SparePartId,
                //    SparePartCode = item.SparePartCode,
                //    SparePartName = item.SparePartName,
                //    PlannedAmount = item.CurrentFulfilledQuantity,
                //    InspectedQuantity = item.CurrentFulfilledQuantity,
                //    Price = item.OrderPrice,
                //    OriginalPrice = partsSalesOrderDetail.OriginalPrice
                //};
                //partsInboundPlan.PartsInboundPlanDetails.Add(partsInboundPlanDetail);

                ////新增入库检验单清单
                //partsInboundCheckBill.PartsInboundCheckBillDetails.Add(new PartsInboundCheckBillDetail {
                //    SparePartId = item.SparePartId,
                //    SparePartCode = item.SparePartCode,
                //    SparePartName = item.SparePartName,
                //    WarehouseAreaId = warehouseArea.Id,
                //    WarehouseAreaCode = warehouseArea.Code,
                //    InspectedQuantity = item.CurrentFulfilledQuantity,
                //    SettlementPrice = item.OrderPrice,
                //    OriginalPrice = partsSalesOrderDetail.OriginalPrice
                //});

                ////总部出库计划单清单
                //zbPartsOutboundPlan.PartsOutboundPlanDetails.Add(new PartsOutboundPlanDetail {
                //    SparePartId = item.SparePartId,
                //    SparePartCode = item.SparePartCode,
                //    SparePartName = item.SparePartName,
                //    PlannedAmount = item.CurrentFulfilledQuantity,
                //    Price = item.OrderPrice,
                //    OutboundFulfillment = item.CurrentFulfilledQuantity,
                //    OriginalPrice = partsSalesOrderDetail.OriginalPrice
                //});

                ////总部出库计划单清单
                //zbPartsOutboundBill.PartsOutboundBillDetails.Add(new PartsOutboundBillDetail {
                //    SparePartId = item.SparePartId,
                //    SparePartCode = item.SparePartCode,
                //    SparePartName = item.SparePartName,
                //    SettlementPrice = item.OrderPrice,
                //    OutboundAmount = item.CurrentFulfilledQuantity,
                //    OriginalPrice = partsSalesOrderDetail.OriginalPrice
                //});

                ////总部配件发运单清单
                //newPartsShippingOrder.PartsShippingOrderDetails.Add(new PartsShippingOrderDetail {
                //    PartsShippingOrderId = newPartsShippingOrder.Id,
                //    SparePartId = item.SparePartId,
                //    SparePartCode = item.SparePartCode,
                //    SparePartName = item.SparePartName,
                //    ShippingAmount = item.CurrentFulfilledQuantity,
                //    ConfirmedAmount = item.CurrentFulfilledQuantity,
                //    SettlementPrice = item.OrderPrice,
                //    PartsOutboundPlanId = zbPartsOutboundPlan.Id
                //});
            }
         //   #endregion

            //var warehouseAreaIds = partsOutboundBill.PartsOutboundBillDetails.Select(r => r.WarehouseAreaId).Distinct().ToArray();
            //var partsStocks = new PartsStockAch(this.DomainService).GetPartsStocksWithLock(this.ObjectContext.PartsStocks.Where(r => r.WarehouseId == partsOutboundBill.WarehouseId && partsIds.Contains(r.PartId) && warehouseAreaIds.Contains(r.WarehouseAreaId))).ToArray();
            //var enterprisePartsCosts = new EnterprisePartsCostAch(this.DomainService).GetEnterprisePartsCostsWithLock(this.ObjectContext.EnterprisePartsCosts.Where(r => r.OwnerCompanyId == partsOutboundPlan.StorageCompanyId && partsIds.Contains(r.SparePartId))).ToList();
            ////中心库成本价
            //var partsAmountCenters = new Dictionary<int, decimal>();
            //partsAmountCenters = (from a in ObjectContext.EnterprisePartsCosts.Where(r => r.OwnerCompanyId == partsOutboundBill.StorageCompanyId && r.PartsSalesCategoryId == partsOutboundBill.PartsSalesCategoryId && partsIds.Contains(r.SparePartId)).ToArray()
            //                      select a).ToDictionary(r => r.SparePartId, r => r.CostPrice);
            ////总部成本价
            //var partsAmountDetails = new Dictionary<int, decimal>();
            //partsAmountDetails = (from a in ObjectContext.EnterprisePartsCosts.Where(r => r.OwnerCompanyId == zbPartsOutboundBill.StorageCompanyId && r.PartsSalesCategoryId == zbPartsOutboundBill.PartsSalesCategoryId && partsIds.Contains(r.SparePartId)).ToArray()
            //                      select a).ToDictionary(r => r.SparePartId, r => r.CostPrice);
            //foreach(var partsOutboundBillDetail in partsOutboundBill.PartsOutboundBillDetails) {
            //    //减少待发区数量
            //    var partsStock = partsStocks.FirstOrDefault(r => r.WarehouseAreaId == partsOutboundBillDetail.WarehouseAreaId && r.PartId == partsOutboundBillDetail.SparePartId);
            //    if(partsStock == null)
            //        throw new ValidationException(string.Format(ErrorStrings.PartsOutboundBill_Validation6, partsOutboundBillDetail.SparePartCode, partsOutboundBillDetail.WarehouseAreaCode));
            //    if(partsOutboundBillDetail.OutboundAmount > partsStock.Quantity)
            //        throw new ValidationException(string.Format("编号为{0}的配件,在编号为{1}的库位上库存不足", partsOutboundBillDetail.SparePartCode, partsOutboundBillDetail.WarehouseAreaCode));

            //    partsStock.Quantity -= partsOutboundBillDetail.OutboundAmount;
            //    new PartsStockAch(this.DomainService).UpdatePartsStockValidate(partsStock);

            //    //减少企业成本库存
            //    var enterprisePartsCost = enterprisePartsCosts == null ? null : enterprisePartsCosts.FirstOrDefault(r => r.SparePartId == partsOutboundBillDetail.SparePartId);
            //    if(enterprisePartsCost != null) {
            //        enterprisePartsCost.Quantity -= partsOutboundBillDetail.OutboundAmount;
            //        enterprisePartsCost.CostAmount -= partsOutboundBillDetail.OutboundAmount * enterprisePartsCost.CostPrice;
            //        UpdateToDatabase(enterprisePartsCost);
            //    } else {
            //        throw new ValidationException(string.Format(ErrorStrings.PartsOutboundBill_Validation8, partsOutboundBillDetail.SparePartCode));
            //    }

            //    //填充配件出库单清单
            //    if(!partsAmountCenters.ContainsKey(partsOutboundBillDetail.SparePartId)) {
            //        throw new ValidationException(string.Format(ErrorStrings.PartsOutboundBill_Validation8, partsOutboundBillDetail.SparePartCode));
            //    } else {
            //        partsOutboundBillDetail.CostPrice = partsAmountCenters[partsOutboundBillDetail.SparePartId];
            //    }
            //    if(partsOutboundBillDetail.CostPrice == 0) {
            //        throw new ValidationException(string.Format("编号为“{0}”的配件的成本价等于0", partsOutboundBillDetail.SparePartCode));
            //    }
            //}

            //foreach(var partsInboundCheckBillDetail in partsInboundCheckBill.PartsInboundCheckBillDetails) {
            //    //填充配件出库单清单
            //    if(!partsAmountDetails.ContainsKey(partsInboundCheckBillDetail.SparePartId)) {
            //        throw new ValidationException(string.Format(ErrorStrings.PartsOutboundBill_Validation8, partsInboundCheckBillDetail.SparePartCode));
            //    } else {
            //        partsInboundCheckBillDetail.CostPrice = partsAmountDetails[partsInboundCheckBillDetail.SparePartId];
            //    }
            //    if(partsInboundCheckBillDetail.CostPrice == 0) {
            //        throw new ValidationException(string.Format("编号为“{0}”的配件的成本价等于0", partsInboundCheckBillDetail.SparePartCode));
            //    }
            //}

            //foreach(var partsOutboundBillDetail in zbPartsOutboundBill.PartsOutboundBillDetails) {
            //    //填充配件出库单清单
            //    if(!partsAmountDetails.ContainsKey(partsOutboundBillDetail.SparePartId)) {
            //        throw new ValidationException(string.Format(ErrorStrings.PartsOutboundBill_Validation8, partsOutboundBillDetail.SparePartCode));
            //    } else {
            //        partsOutboundBillDetail.CostPrice = partsAmountDetails[partsOutboundBillDetail.SparePartId];
            //    }
            //    if(partsOutboundBillDetail.CostPrice == 0) {
            //        throw new ValidationException(string.Format("编号为“{0}”的配件的成本价等于0", partsOutboundBillDetail.SparePartCode));
            //    }
            //}
            partsSalesReturnBill.TotalAmount = partsSalesReturnBill.PartsSalesReturnBillDetails.Sum(r => r.ReturnPrice * r.ApproveQuantity.Value);

            partsOutboundPlan.PartsSalesReturnBill = partsSalesReturnBill;
          //  partsInboundPlan.PartsSalesReturnBill = partsSalesReturnBill;

           // partsOutboundPlan.PartsOutboundBills.Add(partsOutboundBill);
            //partsInboundPlan.PartsInboundCheckBills.Add(partsInboundCheckBill);
            //newPartsShippingOrder.PartsShippingOrderRefs.Add(newPartsShippingOrderRef);
            //zbPartsOutboundBill.PartsShippingOrderRefs.Add(newPartsShippingOrderRef);
            InsertToDatabase(partsSalesReturnBill);
            new PartsSalesReturnBillAch(this.DomainService).InsertPartsSalesReturnBillValidate(partsSalesReturnBill);
            InsertToDatabase(partsOutboundPlan);
            new PartsOutboundPlanAch(this.DomainService).InsertPartsOutboundPlanValidate(partsOutboundPlan);
            //InsertToDatabase(partsOutboundBill);
            //new PartsOutboundBillAch(this.DomainService).InsertPartsOutboundBillValidate(partsOutboundBill);
            //InsertToDatabase(partsInboundCheckBill);
            //new PartsInboundCheckBillAch(this.DomainService).InsertPartsInboundCheckBillValidate(partsInboundCheckBill);
            //InsertToDatabase(zbPartsOutboundPlan);
            //new PartsOutboundPlanAch(this.DomainService).InsertPartsOutboundPlanValidate(zbPartsOutboundPlan);
            //InsertToDatabase(zbPartsOutboundBill);
            //new PartsOutboundBillAch(this.DomainService).InsertPartsOutboundBillValidate(zbPartsOutboundBill);
            //foreach(var partsShippingOrderDetail in newPartsShippingOrder.PartsShippingOrderDetails)
            //    partsShippingOrderDetail.PartsOutboundPlanCode = zbPartsOutboundPlan.Code;
            //InsertToDatabase(newPartsShippingOrder);
            //DomainService.InsertPartsShippingOrderValidate(newPartsShippingOrder);
            //InsertToDatabase(partsInboundPlan);
            //new PartsInboundPlanAch(this.DomainService).InsertPartsInboundPlanValidate(partsInboundPlan);
            partsSalesOrder.TotalAmount = partsSalesOrder.PartsSalesOrderDetails.Sum(r => r.OrderSum);
            partsSalesOrder.Status = (int)DcsPartsSalesOrderStatus.审批完成;
            curPartsInboundPlan = null;
            curPartsSalesReturnBill = partsSalesReturnBill;
        }
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:11:57
        //原分布类的函数全部转移到Ach                                                               
        //使用工具生成后没有任何共有方法 NoPublic
    }
}
