﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using System.Transactions;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsSalesReturnBillAch : DcsSerivceAchieveBase {
        public PartsSalesReturnBillAch(DcsDomainService domainService)
            : base(domainService) {
        }
        //验证标签码
        private void ValidateDealerTrace(PartsSalesReturnBill partsSalesReturnBill) {
            var inTraces = ObjectContext.AccurateTraces.Where(t => t.CompanyId == partsSalesReturnBill.ReturnCompanyId && t.Type == (int)DCSAccurateTraceType.入库 && (t.OutQty == null || t.InQty != t.OutQty) && t.Status == (int)DCSAccurateTraceStatus.有效).ToArray();
            foreach(var item in partsSalesReturnBill.PartsSalesReturnBillDetails) {
                if(item.TraceProperty.HasValue && !string.IsNullOrEmpty(item.SIHLabelCode)) {
                    var traceQty = 0;

                    var traces = item.SIHLabelCode.Split(new Char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                    foreach(var code in traces) {
                        var inTrace = inTraces.Where(t => t.BoxCode == code).ToArray();
                        if(code.EndsWith("J"))
                            inTrace = inTraces.Where(t => t.SIHLabelCode == code).ToArray();
                        if(inTrace.Count() == 0) {
                            throw new ValidationException(code + "标签码不存在,或已出库完成");
                        }
                        foreach(var trace in inTrace) {
                            if(trace.PartId != item.SparePartId) {
                                throw new ValidationException(code + "不是配件" + item.SparePartCode + "的标签码");
                            }
                            if(item.TraceProperty.Value == (int)DCSTraceProperty.精确追溯) {
                                traceQty++;
                            }
                        }
                    }

                    //if(item.TraceProperty.Value == (int)DCSTraceProperty.精确追溯 && traceQty > 0 && traceQty > item.ReturnedQuantity) {
                    //  //  throw new ValidationException(item.SparePartCode + "配件的标签码数量大于退货数量");
                    //}
                }
            }
        }
        public void 生成配件销售退货单(PartsSalesReturnBill partsSalesReturnBill) {
            var company = ObjectContext.Companies.SingleOrDefault(r => r.Id == partsSalesReturnBill.ReturnCompanyId && r.Status == (int)DcsMasterDataStatus.有效);
            if(company == null)
                throw new ValidationException(ErrorStrings.PartsSalesReturnBill_Validation5);
            var partIds = partsSalesReturnBill.PartsSalesReturnBillDetails.Select(r => r.SparePartId).ToArray();
            var partCodes =  partsSalesReturnBill.PartsSalesReturnBillDetails.Select(r => r.SparePartCode).ToArray();
            var salesUnit = ObjectContext.SalesUnits.SingleOrDefault(r => r.Id == partsSalesReturnBill.SalesUnitId && r.Status == (int)DcsBaseDataStatus.有效);
            if(salesUnit == null) {
                throw new ValidationException("未找到对应的销售组织");
            }
            //验证标签码
            if(company.Type == (int)DcsCompanyType.服务站 || company.Type == (int)DcsCompanyType.集团企业) {
                this.ValidateDealerTrace(partsSalesReturnBill);
            }
            
            var partsSalesOrderIds = partsSalesReturnBill.PartsSalesReturnBillDetails.Select(r => r.PartsSalesOrderId).ToArray();

            var businessTypes = this.ObjectContext.PartsSalesOrderTypes.Where(r => this.ObjectContext.PartsSalesOrders.Any(p => p.PartsSalesOrderTypeId == r.Id && partsSalesOrderIds.Contains(p.Id))).Select(r => r.BusinessType).Distinct();
            if(businessTypes.Count() > 1) {
                throw new ValidationException("销售订单的业务类型存在不一致！");
            }
            //	新增、修改、初审、审核退货单，是否质保可编辑， 修改是否质保=是退货入库仓库默认为 仓库信息.是否质量件= 是的仓库（如果有多条，默认一条 可修改）
            if(partsSalesReturnBill.IsWarrantyReturn.HasValue && partsSalesReturnBill.IsWarrantyReturn.Value) {
                var zlWarehous = ObjectContext.Warehouses.Where(t => t.IsQualityWarehouse == true && t.Status == (int)DcsBaseDataStatus.有效 && t.StorageCompanyId == partsSalesReturnBill.SalesUnitOwnerCompanyId).FirstOrDefault();
                if(zlWarehous==null) {
                    throw new ValidationException("未找到质量件仓库信息");
                }
                partsSalesReturnBill.WarehouseId = zlWarehous.Id;
                partsSalesReturnBill.WarehouseCode = zlWarehous.Code;
                partsSalesReturnBill.WarehouseName = zlWarehous.Name;
            }
            //当填写折扣时
            //特殊退货不重新计算
            //退货总金额
            if(partsSalesReturnBill.ReturnType == (int)DcsPartsSalesReturn_ReturnType.特殊退货) {
                Decimal totalAmount = Decimal.Zero;
                foreach(var item in partsSalesReturnBill.PartsSalesReturnBillDetails) {
                    if(null != partsSalesReturnBill.DiscountRate) {
                        item.ReturnPrice = Math.Round(Decimal.Parse((item.OriginalOrderPrice * (partsSalesReturnBill.DiscountRate)).ToString()), 2);
                    }
                    totalAmount += item.ReturnPrice * item.ReturnedQuantity;
                }
                partsSalesReturnBill.TotalAmount = totalAmount;
            } else if(partsSalesReturnBill.ReturnType == (int)DcsPartsSalesReturn_ReturnType.正常退货){
                Decimal totalAmount = Decimal.Zero;
                var partsSalesOrders = ObjectContext.PartsSalesOrders.Where(r => partsSalesOrderIds.Contains(r.Id)).ToArray();
                var returnStrategies = ObjectContext.CenterSaleReturnStrategies.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.ReturnType == (int)DcsPartsSalesReturn_ReturnType.正常退货).ToArray();
                foreach (var item in partsSalesReturnBill.PartsSalesReturnBillDetails) {
                    var partsSalesOrder = partsSalesOrders.FirstOrDefault(r => r.Id == item.PartsSalesOrderId);
                    if(partsSalesOrder.SubmitTime==null) {
                        partsSalesOrder.SubmitTime = partsSalesOrder.CreateTime;
                    }
                    var timeSlot = (DateTime.Now.Date - partsSalesOrder.SubmitTime.Value.Date).Days;
                    var returnDays = returnStrategies.Where(r => timeSlot > r.TimeFrom.Value && timeSlot <= r.TimeTo.Value).SingleOrDefault();
                    if (returnDays == null || returnDays.DiscountRate == 0) {
                        throw new ValidationException("购入时间" + timeSlot + "天的不予退换");
                    } else {
                        item.DiscountRate = returnDays.DiscountRate;
                    }

                    if(null != item.DiscountRate) {
                      //  item.ReturnPrice = Decimal.Parse((item.OriginalOrderPrice * (partsSalesReturnBill.DiscountRate)).ToString());
                       item.ReturnPrice =Math.Round(Decimal.Parse((item.OriginalOrderPrice * Decimal.Parse(item.DiscountRate.ToString())).ToString()), 2);
                    }
                   // item.ReturnPrice = Math.Round(item.ReturnPrice * Math.Round((decimal)item.DiscountRate, 2), 2);
                    totalAmount += item.ReturnPrice * item.ReturnedQuantity;
                }
                partsSalesReturnBill.TotalAmount = totalAmount;

            } else if (partsSalesReturnBill.ReturnType == (int)DcsPartsSalesReturn_ReturnType.保底退货) {
                //原逻辑不需要
                //var submitCompany = ObjectContext.Companies.Single(r => r.Id == partsSalesReturnBill.SubmitCompanyId);
                //int? warehouseId = 0;
                //if (submitCompany.Type == (int)DcsCompanyType.代理库 || submitCompany.Type == (int)DcsCompanyType.服务站兼代理库) {
                //    warehouseId = partsSalesReturnBill.ReturnWarehouseId;
                //}
                //var bottomStocks = ObjectContext.BottomStocks.Where(r => r.CompanyID == submitCompany.Id && partIds.Contains(r.SparePartId) && (r.WarehouseID ?? 0) == warehouseId).ToArray();
                //var returnStrategies = ObjectContext.CenterSaleReturnStrategies.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.ReturnType == (int)DcsPartsSalesReturn_ReturnType.保底退货).ToArray();
                //Decimal totalAmount = Decimal.Zero;
                //foreach (var item in partsSalesReturnBill.PartsSalesReturnBillDetails) {
                //    var bts = bottomStocks.Where(r => r.SparePartId == item.SparePartId).ToArray();
                //    if (bts == null || !bts.Any()) { 
                //        throw new ValidationException("配件" + item.SparePartCode + "不存在保底库存");
                //    }
                //    if (bts.Any(r => r.Status == (int)DcsBaseDataStatus.作废) && !bts.Any(r => r.Status == (int)DcsBaseDataStatus.有效)) {
                //        var abandonTime = bts.Where(r => r.Status == (int)DcsBaseDataStatus.作废).Min(x => x.AbandonTime);
                //        var timeSlot = (DateTime.Now.Date - abandonTime.Value.Date).Days;
                //        var returnDays = returnStrategies.Where(r => timeSlot > r.TimeFrom.Value && timeSlot <= r.TimeTo.Value).SingleOrDefault();
                //        if (returnDays == null || returnDays.DiscountRate == 0) {
                //            throw new ValidationException("配件" + item.SparePartCode + "不允许做保底退货");
                //        } else {
                //            item.DiscountRate = returnDays.DiscountRate;
                //        }

                //        if (null != partsSalesReturnBill.DiscountRate) {
                //            item.ReturnPrice = Decimal.Parse((item.OriginalOrderPrice * (partsSalesReturnBill.DiscountRate)).ToString());
                //        }
                //        item.ReturnPrice = Math.Round(item.ReturnPrice * Math.Round((decimal)item.DiscountRate, 2), 2);
                //        totalAmount += item.ReturnPrice * item.ReturnedQuantity;
                //    } else { 
                //        throw new ValidationException("配件" + item.SparePartCode + "不允许做保底退货");
                //    }
                //}
                //	销售退货，退货类型为“保底退货”，○1若存在有效的<配件保底库存明细集合版本号>，不允许退货，
                //○2无 有效 <配件保底库存明细集合版本号>时，退货时间不能超过最后一个失效时间+2年，且退货数量不能大于合计版本号中的强制储备的最大值
                var submitCompany = ObjectContext.Companies.Single(r => r.Id == partsSalesReturnBill.SubmitCompanyId);
                var bottomStocks = ObjectContext.BottomStockColVersions.Where(r => r.CompanyId == submitCompany.Id && partIds.Contains(r.SparePartId)).ToArray();
                var returnStrategies = ObjectContext.CenterSaleReturnStrategies.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.ReturnType == (int)DcsPartsSalesReturn_ReturnType.保底退货).ToArray();
                Decimal totalAmount = Decimal.Zero;
                foreach (var item in partsSalesReturnBill.PartsSalesReturnBillDetails) {
                    var bts = bottomStocks.Where(r => r.SparePartId == item.SparePartId).FirstOrDefault();
                    if (bts == null) { 
                        throw new ValidationException("配件" + item.SparePartCode + "不存在保底库存");
                    }
                    if (bts.Status == (int)DcsBaseDataStatus.作废) {
                        var abandonTime = bts.EndTime;
                        var timeSlot = (DateTime.Now.Date - abandonTime.Value.Date).Days;
                        var returnDays = returnStrategies.Where(r => timeSlot > r.TimeFrom.Value && timeSlot <= r.TimeTo.Value).SingleOrDefault();
                        if(returnDays == null || timeSlot > 730 || returnDays.DiscountRate == 0 || item.ReturnedQuantity > bts.ReserveQty) {
                            throw new ValidationException("配件" + item.SparePartCode + "不允许做保底退货");
                        } else {
                            item.DiscountRate = returnDays.DiscountRate;
                        }

                        if(null != item.DiscountRate) {
                            //  item.ReturnPrice = Decimal.Parse((item.OriginalOrderPrice * (partsSalesReturnBill.DiscountRate)).ToString());
                            item.ReturnPrice = Math.Round(Decimal.Parse((item.OriginalOrderPrice * Decimal.Parse(item.DiscountRate.ToString())).ToString()), 2);
                        }
                      //  item.ReturnPrice = Math.Round(item.ReturnPrice * Math.Round((decimal)item.DiscountRate, 2), 2);
                        totalAmount += item.ReturnPrice * item.ReturnedQuantity;
                    } else { 
                        throw new ValidationException("配件" + item.SparePartCode + "不允许做保底退货");
                    }
                }
                partsSalesReturnBill.TotalAmount = totalAmount;
            } else { 
                throw new ValidationException("未找到对应的退货类型");
            }

            var customerAccount = this.ObjectContext.CustomerAccounts.Where(r => r.AccountGroupId == salesUnit.AccountGroupId && r.CustomerCompanyId == partsSalesReturnBill.ReturnCompanyId && r.Status == (int)DcsMasterDataStatus.有效).FirstOrDefault();
            if(customerAccount == null) {
                throw new ValidationException("未找到对应的客户账户");
            } else
                partsSalesReturnBill.CustomerAccountId = customerAccount.Id;
            if(company.Type == (int)DcsCompanyType.代理库 || company.Type == (int)DcsCompanyType.服务站兼代理库) {
                if(!partsSalesReturnBill.ReturnWarehouseId.HasValue) {
                    throw new ValidationException("代理库退货提报，退货仓库应必填!");
                }
                //校验代理库库存
                //获取清单中配件的可用库存
                var warehousePartsStocks = DomainService.查询仓库库存(partsSalesReturnBill.ReturnCompanyId, partsSalesReturnBill.ReturnWarehouseId, partIds).ToList();
                if(warehousePartsStocks.Count <= 0) {
                    throw new ValidationException(ErrorStrings.WarehousePartsStock_Validation1);
                }
                //校验退货量是否大于可用库存
                foreach(var partsSalesReturnBillDetail in partsSalesReturnBill.PartsSalesReturnBillDetails) {
                    var returnQuantity = partsSalesReturnBill.PartsSalesReturnBillDetails.Where(r => r.SparePartId == partsSalesReturnBillDetail.SparePartId).Sum(r => r.ReturnedQuantity);
                    var checkPartsStock = warehousePartsStocks.SingleOrDefault(r => r.SparePartId == partsSalesReturnBillDetail.SparePartId);
                    if(checkPartsStock == null || returnQuantity > checkPartsStock.UsableQuantity) {
                        throw new ValidationException(string.Format("配件编号为{0}的库存不足", partsSalesReturnBillDetail.SparePartCode));
                    }
                }
            } else if(company.Type != (int)DcsCompanyType.集团企业) { // 如果退货单位企业类型 = 集团企业，不校验库存是否足够
                //校验服务站库存

                //获取退货单清单中相关配件的库存
                var dealerPartsStrocks = ObjectContext.DealerPartsStocks.Where(r => r.DealerId == partsSalesReturnBill.ReturnCompanyId && r.SubDealerId == -1 && r.SalesCategoryId == salesUnit.PartsSalesCategoryId && partIds.Contains(r.SparePartId)).ToArray();
                foreach(var partsSalesReturnBillDetail in partsSalesReturnBill.PartsSalesReturnBillDetails) {
                    var returnQuantity = partsSalesReturnBill.PartsSalesReturnBillDetails.Where(r => r.SparePartId == partsSalesReturnBillDetail.SparePartId).Sum(r => r.ReturnedQuantity);
                    var dealerPartsStock = dealerPartsStrocks.SingleOrDefault(r => r.SparePartId == partsSalesReturnBillDetail.SparePartId);
                    if(dealerPartsStock != null) {
                        if(dealerPartsStock.Quantity < returnQuantity) {
                            throw new ValidationException(string.Format("配件编号为{0}的库存不足", dealerPartsStock.SparePartCode));
                        }
                    } else {
                        throw new ValidationException(string.Format("配件编号为{0}的库存不足", partsSalesReturnBillDetail.SparePartCode));
                    }
                }
            }
            //本次退货品种数量
            var afferentPartsSalesReturnBillDetails = partsSalesReturnBill.PartsSalesReturnBillDetails.GroupBy(r => new {
                r.SparePartId,
                r.SparePartCode,
                r.PartsSalesOrderId
            }).Select(r => new {
                SparePartId = r.Key.SparePartId,
                SparePartCode = r.Key.SparePartCode,
                PartsSalesOrderId=r.Key.PartsSalesOrderId,
                ReturnedQuantity = r.Sum(v => v.ReturnedQuantity)
            }).ToArray();
            ////历史出库品种数量
            //var partsOutboundBillDetails = ObjectContext.PartsOutboundBillDetails.Where(r => ObjectContext.PartsOutboundBills.Any(v => partsSalesOrderIds.Contains(v.OriginalRequirementBillId) && v.OriginalRequirementBillType == (int)DcsOriginalRequirementBillType.配件销售订单 && v.Id == r.PartsOutboundBillId)).GroupBy(r => new { r.SparePartId, r.SparePartCode }).Select(r => new {
            //    SparePartId = r.Key.SparePartId,
            //    SparePartCode = r.Key.SparePartCode,
            //    OutboundAmount = r.Sum(v => v.OutboundAmount)
            //}).ToArray();
            ////历史退货品种数量
            ////var originalPartsSalesReturnBillDetails = ObjectContext.PartsSalesReturnBillDetails.Where(r => partsSalesOrderIds.Contains(r.PartsSalesOrderId) && partCodes.Contains(r.SparePartCode) && ObjectContext.PartsSalesReturnBills.Any(v => v.Status != (int)DcsPartsSalesReturnBillStatus.作废 && v.Status != (int)DcsPartsSalesReturnBillStatus.终止 && v.Id == r.PartsSalesReturnBillId)).GroupBy(r => new { r.SparePartId, r.SparePartCode }).Select(r => new {
            ////    SparePartId = r.Key.SparePartId,
            ////    SparePartCode = r.Key.SparePartCode,
            ////    ReturnedQuantity = r.Sum(v => v.ApproveQuantity??0)
            ////}).ToArray();
            //var originalPartsSalesReturnBillDetails = (from c in ObjectContext.PartsSalesReturnBillDetails.Where(r => partsSalesOrderIds.Contains(r.PartsSalesOrderId) && partCodes.Contains(r.SparePartCode) && ObjectContext.PartsSalesReturnBills.Any(v => v.Status != (int)DcsPartsSalesReturnBillStatus.作废 && v.Status != (int)DcsPartsSalesReturnBillStatus.终止 && v.Id == r.PartsSalesReturnBillId))                                                       
            //                                            join b in ObjectContext.PartsSalesReturnBills.Where(v => v.Status != (int)DcsPartsSalesReturnBillStatus.作废 && v.Status != (int)DcsPartsSalesReturnBillStatus.终止 && !ObjectContext.PartsOutboundPlans.Any(p=>p.OriginalRequirementBillId==v.Id && p.OriginalRequirementBillCode==v.Code && p.Status ==(int)DcsPartsOutboundPlanStatus.终止)) on c.PartsSalesReturnBillId equals b.Id
            //                                            select new {
            //                                                c.SparePartCode,
            //                                                c.SparePartId,
            //                                                ReturnedQuantity = b.Status == (int)DcsPartsSalesReturnBillStatus.审批通过 ? c.ApproveQuantity.Value : c.ReturnedQuantity
            //                                            }).GroupBy(r => new {
            //                                                r.SparePartCode,
            //                                                r.SparePartId
            //                                            }).Select(r => new {
            //                                                SparePartId=r.Key.SparePartId,
            //                                                SparePartCode=r.Key.SparePartCode,
            //                                                ReturnedQuantity = r.Sum(t => t.ReturnedQuantity)
            //                                            }).ToArray();
            ////本次退货量+历史退货量
            //var returnPartsSalesReturnBillDetails = originalPartsSalesReturnBillDetails.Concat(afferentPartsSalesReturnBillDetails).GroupBy(r => new { r.SparePartId, r.SparePartCode }).Select(r => new {
            //    SparePartId = r.Key.SparePartId,
            //    SparePartCode = r.Key.SparePartCode,
            //    ReturnedQuantity = r.Sum(v => v.ReturnedQuantity)
            //}).ToArray();

            //foreach (var item in returnPartsSalesReturnBillDetails) {
            //    if (!partsOutboundBillDetails.Any(v => v.SparePartId == item.SparePartId)) {
            //        throw new ValidationException("配件"+item.SparePartCode+"在出库订单中不存在");
            //    }
            ////}
            var sparePartIds = partsSalesReturnBill.PartsSalesReturnBillDetails.Select(t => t.SparePartId).Distinct().ToArray();
            var canReturnDetail = new PartsOutboundBillDetailAch(this.DomainService).退货节点选取配件销售单校验(partsSalesOrderIds, sparePartIds);
            if(canReturnDetail == null || canReturnDetail.Count() == 0) {
                throw new ValidationException(string.Format("未找到可退货的配件"));
            }
            foreach(var item in afferentPartsSalesReturnBillDetails) {
                var outboundAmount = canReturnDetail.Where(r => r.SparePartId == item.SparePartId && r.PartsSalesOrderId == item.PartsSalesOrderId).FirstOrDefault();
                if(outboundAmount == null || outboundAmount.ReturnedQuantity < item.ReturnedQuantity) {
                    var returnQty = 0;
                    if(outboundAmount!=null) {
                        returnQty = outboundAmount.ReturnedQuantity;
                    }
                    throw new ValidationException(string.Format("配件{0}可退货数量是{1}，退货数量大于可退货数量", item.SparePartCode, returnQty));
                }
            }
            //if(!returnPartsSalesReturnBillDetails.All(r => partsOutboundBillDetails.Any(v => v.SparePartId == r.SparePartId)))
            //    throw new ValidationException(ErrorStrings.PartsSalesReturnBill_Validation1);
            
            //if(partsOutboundBillDetails.Any(r => returnPartsSalesReturnBillDetails.Any(v => v.SparePartId == r.SparePartId && v.ReturnedQuantity > r.OutboundAmount)))
            //    throw new ValidationException(ErrorStrings.PartsSalesReturnBill_Validation2);
        }


        public void 修改配件销售退货单(PartsSalesReturnBill partsSalesReturnBill) {
            CheckEntityState(partsSalesReturnBill);
            var company = ObjectContext.Companies.SingleOrDefault(r => r.Id == partsSalesReturnBill.ReturnCompanyId && r.Status == (int)DcsMasterDataStatus.有效);
            if(company == null)
                throw new ValidationException(ErrorStrings.PartsSalesReturnBill_Validation5);
            //验证标签码
            if(company.Type == (int)DcsCompanyType.服务站 || company.Type == (int)DcsCompanyType.集团企业) {
                this.ValidateDealerTrace(partsSalesReturnBill);
            }
            var partIds = partsSalesReturnBill.PartsSalesReturnBillDetails.Select(r => r.SparePartId).ToArray();
            var partCodes =  partsSalesReturnBill.PartsSalesReturnBillDetails.Select(r => r.SparePartCode).ToArray();

            var salesUnit = ObjectContext.SalesUnits.SingleOrDefault(r => r.Id == partsSalesReturnBill.SalesUnitId && r.Status == (int)DcsBaseDataStatus.有效);
            if(salesUnit == null) {
                throw new ValidationException("未找到对应的销售组织");
            }
            var partsSalesOrderIds = partsSalesReturnBill.PartsSalesReturnBillDetails.Select(r => r.PartsSalesOrderId).ToArray();
            var businessTypes = this.ObjectContext.PartsSalesOrderTypes.Where(r => this.ObjectContext.PartsSalesOrders.Any(p => p.PartsSalesOrderTypeId == r.Id && partsSalesOrderIds.Contains(p.Id))).Select(r => r.BusinessType).Distinct();
            if(businessTypes.Count() > 1) {
                throw new ValidationException("销售订单的业务类型存在不一致！");
            }
            //	新增、修改、初审、审核退货单，是否质保可编辑， 修改是否质保=是退货入库仓库默认为 仓库信息.是否质量件= 是的仓库（如果有多条，默认一条 可修改）
            if(partsSalesReturnBill.IsWarrantyReturn.HasValue && partsSalesReturnBill.IsWarrantyReturn.Value) {
                var zlWarehous = ObjectContext.Warehouses.Where(t => t.IsQualityWarehouse == true && t.Status == (int)DcsBaseDataStatus.有效 && t.StorageCompanyId == partsSalesReturnBill.SalesUnitOwnerCompanyId).FirstOrDefault();
                if(zlWarehous == null) {
                    throw new ValidationException("未找到质量件仓库信息");
                }
                partsSalesReturnBill.WarehouseId = zlWarehous.Id;
                partsSalesReturnBill.WarehouseCode = zlWarehous.Code;
                partsSalesReturnBill.WarehouseName = zlWarehous.Name;
            } else if(partsSalesReturnBill.IsWarrantyReturn.HasValue && !partsSalesReturnBill.IsWarrantyReturn.Value) {
                var reDetail=partsSalesReturnBill.PartsSalesReturnBillDetails.First();
                var inWarehouse = ObjectContext.Warehouses.Where(t => ObjectContext.PartsSalesOrders.Any(r => r.Id == reDetail.PartsSalesOrderId && r.WarehouseId == t.Id)).FirstOrDefault();
                if(inWarehouse != null) {
                    partsSalesReturnBill.WarehouseId = inWarehouse.Id;
                    partsSalesReturnBill.WarehouseCode = inWarehouse.Code;
                    partsSalesReturnBill.WarehouseName = inWarehouse.Name;
                }
            }
            //当填写折扣时
            //特殊退货不重新计算//2018.12.06 所有的退货类型都要重新计算价格
            //退货总金额
            if(partsSalesReturnBill.ReturnType == (int)DcsPartsSalesReturn_ReturnType.特殊退货) {
                Decimal totalAmount = Decimal.Zero;
                foreach(var item in partsSalesReturnBill.PartsSalesReturnBillDetails) {
                    if(null != partsSalesReturnBill.DiscountRate) {
                        item.ReturnPrice = Math.Round(Decimal.Parse((item.OriginalOrderPrice * (partsSalesReturnBill.DiscountRate)).ToString()), 2);
                    }
                    totalAmount += item.ReturnPrice * item.ReturnedQuantity;
                }
                partsSalesReturnBill.TotalAmount = totalAmount;
            } else if(partsSalesReturnBill.ReturnType == (int)DcsPartsSalesReturn_ReturnType.正常退货){
                Decimal totalAmount = Decimal.Zero;
                var partsSalesOrders = ObjectContext.PartsSalesOrders.Where(r => partsSalesOrderIds.Contains(r.Id)).ToArray();
                var returnStrategies = ObjectContext.CenterSaleReturnStrategies.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.ReturnType == (int)DcsPartsSalesReturn_ReturnType.正常退货).ToArray();
                foreach (var item in partsSalesReturnBill.PartsSalesReturnBillDetails) {
                    var partsSalesOrder = partsSalesOrders.FirstOrDefault(r => r.Id == item.PartsSalesOrderId);
                    if(partsSalesOrder.SubmitTime == null) {
                        partsSalesOrder.SubmitTime = partsSalesOrder.CreateTime;
                    }
                    var timeSlot = (DateTime.Now.Date - partsSalesOrder.SubmitTime.Value.Date).Days;
                    var returnDays = returnStrategies.Where(r => timeSlot > r.TimeFrom.Value && timeSlot <= r.TimeTo.Value).SingleOrDefault();
                    if (returnDays == null || returnDays.DiscountRate == 0) {
                        throw new ValidationException("购入时间" + timeSlot + "天的不予退换");
                    } else {
                        item.DiscountRate = returnDays.DiscountRate;
                    }

                    if(null != item.DiscountRate) {
                        //  item.ReturnPrice = Decimal.Parse((item.OriginalOrderPrice * (partsSalesReturnBill.DiscountRate)).ToString());
                        item.ReturnPrice = Math.Round(Decimal.Parse((item.OriginalOrderPrice * Decimal.Parse(item.DiscountRate.ToString())).ToString()), 2);
                    }
                   // item.ReturnPrice = Math.Round(item.ReturnPrice * Math.Round((decimal)item.DiscountRate, 2), 2);
                    totalAmount += item.ReturnPrice * item.ReturnedQuantity;
                }
                partsSalesReturnBill.TotalAmount = totalAmount;

            } else if (partsSalesReturnBill.ReturnType == (int)DcsPartsSalesReturn_ReturnType.保底退货) {
                var submitCompany = ObjectContext.Companies.Single(r => r.Id == partsSalesReturnBill.SubmitCompanyId);
                int? warehouseId = 0;
                if (submitCompany.Type == (int)DcsCompanyType.代理库 || submitCompany.Type == (int)DcsCompanyType.服务站兼代理库) {
                    warehouseId = partsSalesReturnBill.ReturnWarehouseId;
                }
                var bottomStocks = ObjectContext.BottomStocks.Where(r => r.CompanyID == submitCompany.Id && partIds.Contains(r.SparePartId) && (r.WarehouseID ?? 0) == warehouseId).ToArray();
                var returnStrategies = ObjectContext.CenterSaleReturnStrategies.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.ReturnType == (int)DcsPartsSalesReturn_ReturnType.保底退货).ToArray();
                Decimal totalAmount = Decimal.Zero;
                foreach (var item in partsSalesReturnBill.PartsSalesReturnBillDetails) {
                    var bts = bottomStocks.Where(r => r.SparePartId == item.SparePartId).ToArray();
                    if (bts == null || !bts.Any()) { 
                        throw new ValidationException("配件" + item.SparePartCode + "不存在保底库存");
                    }
                    if (bts.Any(r => r.Status == (int)DcsBaseDataStatus.作废) && !bts.Any(r => r.Status == (int)DcsBaseDataStatus.有效)) {
                        var abandonTime = bts.Where(r => r.Status == (int)DcsBaseDataStatus.作废).Min(x => x.AbandonTime);
                        var timeSlot = (DateTime.Now.Date - abandonTime.Value.Date).Days;
                        var returnDays = returnStrategies.Where(r => timeSlot > r.TimeFrom.Value && timeSlot <= r.TimeTo.Value).SingleOrDefault();
                        if (returnDays == null || returnDays.DiscountRate == 0) {
                            throw new ValidationException("配件" + item.SparePartCode + "不允许做保底退货");
                        } else {
                            item.DiscountRate = returnDays.DiscountRate;
                        }

                        if(null != item.DiscountRate) {
                            //  item.ReturnPrice = Decimal.Parse((item.OriginalOrderPrice * (partsSalesReturnBill.DiscountRate)).ToString());
                            item.ReturnPrice = Math.Round(Decimal.Parse((item.OriginalOrderPrice * Decimal.Parse(item.DiscountRate.ToString())).ToString()), 2);
                        }
                     //   item.ReturnPrice = Math.Round(item.ReturnPrice * Math.Round((decimal)item.DiscountRate, 2), 2);
                        totalAmount += item.ReturnPrice * item.ReturnedQuantity;
                    } else { 
                        throw new ValidationException("配件" + item.SparePartCode + "不允许做保底退货");
                    }
                }
                partsSalesReturnBill.TotalAmount = totalAmount;
            } else { 
                throw new ValidationException("未找到对应的退货类型");
            }

            //查询客户账户重新赋值（FTDCS_ISS20171227004）

            var customerAccount = this.ObjectContext.CustomerAccounts.Where(r => r.AccountGroupId == salesUnit.AccountGroupId && r.CustomerCompanyId == partsSalesReturnBill.ReturnCompanyId && r.Status == (int)DcsMasterDataStatus.有效).FirstOrDefault();
            if(customerAccount == null) {
                throw new ValidationException("未找到对应的客户账户");
            } else
                partsSalesReturnBill.CustomerAccountId = customerAccount.Id;



            if(company.Type == (int)DcsCompanyType.代理库 || company.Type == (int)DcsCompanyType.服务站兼代理库) {
                if(!partsSalesReturnBill.ReturnWarehouseId.HasValue) {
                    throw new ValidationException("代理库退货提报，退货仓库应必填!");
                }
                //校验代理库库存
                //获取清单中配件的可用库存
                var warehousePartsStocks = DomainService.查询仓库库存(partsSalesReturnBill.ReturnCompanyId, partsSalesReturnBill.ReturnWarehouseId, partIds).ToList();
                if(warehousePartsStocks.Count <= 0) {
                    throw new ValidationException(ErrorStrings.WarehousePartsStock_Validation1);
                }
                //校验退货量是否大于可用库存
                foreach(var partsSalesReturnBillDetail in partsSalesReturnBill.PartsSalesReturnBillDetails) {
                    var returnQuantity = partsSalesReturnBill.PartsSalesReturnBillDetails.Where(r => r.SparePartId == partsSalesReturnBillDetail.SparePartId).Sum(r => r.ReturnedQuantity);
                    var checkPartsStock = warehousePartsStocks.SingleOrDefault(r => r.SparePartId == partsSalesReturnBillDetail.SparePartId);
                    if(checkPartsStock == null || returnQuantity > checkPartsStock.UsableQuantity) {
                        throw new ValidationException(string.Format("配件编号为{0}的库存不足", partsSalesReturnBillDetail.SparePartCode));
                    }
                }
            } else if(company.Type != (int)DcsCompanyType.集团企业) { // 如果退货单位企业类型 = 集团企业，不校验库存是否足够
                //校验服务站库存

                //获取退货单清单中相关配件的库存
                var dealerPartsStrocks = ObjectContext.DealerPartsStocks.Where(r => r.DealerId == partsSalesReturnBill.ReturnCompanyId && r.SubDealerId == -1 && r.SalesCategoryId == salesUnit.PartsSalesCategoryId && partIds.Contains(r.SparePartId)).ToArray();
                foreach(var partsSalesReturnBillDetail in partsSalesReturnBill.PartsSalesReturnBillDetails) {
                    var dealerPartsStock = dealerPartsStrocks.SingleOrDefault(r => r.SparePartId == partsSalesReturnBillDetail.SparePartId);
                    //校验拆散件信息
                    var dbPartDeleaveInformation = ObjectContext.PartDeleaveInformations.SingleOrDefault(c => c.OldPartId == partsSalesReturnBillDetail.SparePartId && c.PartsSalesCategoryId == salesUnit.PartsSalesCategoryId && c.BranchId == partsSalesReturnBill.BranchId && c.Status == (int)DcsBaseDataStatus.有效);
                    if(dealerPartsStock != null) {
                        var returnQuantity = partsSalesReturnBill.PartsSalesReturnBillDetails.Where(r => r.SparePartId == partsSalesReturnBillDetail.SparePartId).Sum(r => r.ReturnedQuantity);
                        if(dealerPartsStock.Quantity < returnQuantity) {
                            throw new ValidationException(string.Format("配件编号为{0}的库存不足", dealerPartsStock.SparePartCode));
                        }
                    } else {
                        throw new ValidationException(string.Format("配件编号为{0}的库存不足", partsSalesReturnBillDetail.SparePartCode));
                    }
                }
            }
            var partsOutboundBillDetails = ObjectContext.PartsOutboundBillDetails.Where(r => ObjectContext.PartsOutboundBills.Any(v => partsSalesOrderIds.Contains(v.OriginalRequirementBillId) && v.OriginalRequirementBillType == (int)DcsOriginalRequirementBillType.配件销售订单 && v.Id == r.PartsOutboundBillId)).GroupBy(r =>new { r.SparePartId,r.SparePartCode}).Select(r => new {
                SparePartId = r.Key.SparePartId,
                SparePartCode = r.Key.SparePartCode,
                OutboundAmount = r.Sum(v => v.OutboundAmount)
            }).ToArray();
            //var originalPartsSalesReturnBillDetails = ObjectContext.PartsSalesReturnBillDetails.Where(r => partsSalesOrderIds.Contains(r.PartsSalesOrderId)&& partCodes.Contains(r.SparePartCode) && ObjectContext.PartsSalesReturnBills.Any(v => v.Status != (int)DcsPartsSalesReturnBillStatus.作废 && v.Status != (int)DcsPartsSalesReturnBillStatus.终止 && v.Id != partsSalesReturnBill.Id && v.Id == r.PartsSalesReturnBillId)).GroupBy(r => new { r.SparePartId,r.SparePartCode}).Select(r => new {
            //    SparePartId = r.Key.SparePartId,
            //    SparePartCode = r.Key.SparePartCode,
            //    ReturnedQuantity = r.Sum(v => v.ApproveQuantity??0)
            //}).ToArray();
            var originalPartsSalesReturnBillDetails = (from c in ObjectContext.PartsSalesReturnBillDetails.Where(r => partsSalesOrderIds.Contains(r.PartsSalesOrderId) && partCodes.Contains(r.SparePartCode) && ObjectContext.PartsSalesReturnBills.Any(v => v.Status != (int)DcsPartsSalesReturnBillStatus.作废 && v.Status != (int)DcsPartsSalesReturnBillStatus.终止 && v.Id != partsSalesReturnBill.Id && v.Id == r.PartsSalesReturnBillId))
                                                       join b in ObjectContext.PartsSalesReturnBills.Where(v => v.Status != (int)DcsPartsSalesReturnBillStatus.作废 && v.Status != (int)DcsPartsSalesReturnBillStatus.终止 && !ObjectContext.PartsOutboundPlans.Any(p => p.OriginalRequirementBillId == v.Id && p.OriginalRequirementBillCode == v.Code && p.Status == (int)DcsPartsOutboundPlanStatus.终止)) on c.PartsSalesReturnBillId equals b.Id
                                                       select new {
                                                           c.SparePartCode,
                                                           c.SparePartId,
                                                           ReturnedQuantity = b.Status == (int)DcsPartsSalesReturnBillStatus.审批通过 ? c.ApproveQuantity.Value : c.ReturnedQuantity
                                                       }).GroupBy(r => new {
                                                           r.SparePartCode,
                                                           r.SparePartId
                                                       }).Select(r => new {
                                                           SparePartId = r.Key.SparePartId,
                                                           SparePartCode = r.Key.SparePartCode,
                                                           ReturnedQuantity = r.Sum(t => t.ReturnedQuantity)
                                                       }).ToArray();
            var afferentPartsSalesReturnBillDetails = partsSalesReturnBill.PartsSalesReturnBillDetails.GroupBy(r => new { r.SparePartId,r.SparePartCode}).Select(r => new {
                SparePartId = r.Key.SparePartId,
                SparePartCode = r.Key.SparePartCode,
                ReturnedQuantity = r.Sum(v => v.ReturnedQuantity)
            }).ToArray();
            var returnPartsSalesReturnBillDetails = originalPartsSalesReturnBillDetails.Concat(afferentPartsSalesReturnBillDetails).GroupBy(r => new { r.SparePartId,r.SparePartCode}).Select(r => new {
                SparePartId = r.Key.SparePartId,
                SparePartCode = r.Key.SparePartCode,
                ReturnedQuantity = r.Sum(v => v.ReturnedQuantity)
            }).ToArray();

            foreach (var item in returnPartsSalesReturnBillDetails) {
                if (!partsOutboundBillDetails.Any(v => v.SparePartId == item.SparePartId)) {
                    throw new ValidationException("配件"+item.SparePartCode+"在出库订单中不存在");
                }
            }
            foreach (var item in returnPartsSalesReturnBillDetails) { 
                var outboundAmount = partsOutboundBillDetails.First(r => r.SparePartId == item.SparePartId).OutboundAmount;
                if (item.ReturnedQuantity > outboundAmount) {
                    throw new ValidationException(string.Format("配件{0}出库数量是{1}，总退货数量大于出库数量",item.SparePartCode,outboundAmount));
                }
            }
            //if(!returnPartsSalesReturnBillDetails.All(r => partsOutboundBillDetails.Any(v => v.SparePartId == r.SparePartId)))
            //    throw new ValidationException(ErrorStrings.PartsSalesReturnBill_Validation1);
            //if(partsOutboundBillDetails.Any(r => returnPartsSalesReturnBillDetails.Any(v => v.SparePartId == r.SparePartId && v.ReturnedQuantity > r.OutboundAmount)))
            //    throw new ValidationException(ErrorStrings.PartsSalesReturnBill_Validation2);
        }

        public void 提交配件销售退货单(PartsSalesReturnBill partsSalesReturnBill) {
            var partsSalesReturnBillDetails = ObjectContext.PartsSalesReturnBillDetails.Where(r => r.PartsSalesReturnBillId == partsSalesReturnBill.Id).ToArray();
            var partIds = partsSalesReturnBillDetails.Select(r => r.SparePartId).ToArray();
            var partsSalesOrderIds = partsSalesReturnBillDetails.Select(r => r.PartsSalesOrderId).ToArray();
           
            //当填写折扣时
            //特殊退货不重新计算
            //退货总金额
            if (partsSalesReturnBill.ReturnType == (int)DcsPartsSalesReturn_ReturnType.特殊退货) {
                Decimal totalAmount = Decimal.Zero;
                foreach (var item in partsSalesReturnBill.PartsSalesReturnBillDetails) {
                    if(null != partsSalesReturnBill.DiscountRate) {
                        item.ReturnPrice = Math.Round(Decimal.Parse((item.OriginalOrderPrice * (partsSalesReturnBill.DiscountRate)).ToString()), 2);
                    }
                    totalAmount += item.ReturnPrice * item.ReturnedQuantity;
                }
                partsSalesReturnBill.TotalAmount = totalAmount;
            } else if(partsSalesReturnBill.ReturnType == (int)DcsPartsSalesReturn_ReturnType.正常退货){
                Decimal totalAmount = Decimal.Zero;
                var partsSalesOrders = ObjectContext.PartsSalesOrders.Where(r => partsSalesOrderIds.Contains(r.Id)).ToArray();
                var returnStrategies = ObjectContext.CenterSaleReturnStrategies.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.ReturnType == (int)DcsPartsSalesReturn_ReturnType.正常退货).ToArray();
                foreach (var item in partsSalesReturnBill.PartsSalesReturnBillDetails) {
                    var partsSalesOrder = partsSalesOrders.FirstOrDefault(r => r.Id == item.PartsSalesOrderId);
                    if(partsSalesOrder.SubmitTime == null) {
                        partsSalesOrder.SubmitTime = partsSalesOrder.CreateTime;
                    }
                    var timeSlot = (DateTime.Now.Date - partsSalesOrder.SubmitTime.Value.Date).Days;
                    var returnDays = returnStrategies.Where(r => timeSlot > r.TimeFrom.Value && timeSlot <= r.TimeTo.Value).SingleOrDefault();
                    if (returnDays == null || returnDays.DiscountRate == 0) {
                        throw new ValidationException("购入时间" + timeSlot + "天的不予退换");
                    } else {
                        item.DiscountRate = returnDays.DiscountRate;
                    }
                    
                    if (null != item.DiscountRate) {
                        item.ReturnPrice = Math.Round(Decimal.Parse((item.OriginalOrderPrice * Decimal.Parse(item.DiscountRate.ToString())).ToString()), 2);
                    }
                   // item.ReturnPrice = Math.Round(item.ReturnPrice * Math.Round((decimal)item.DiscountRate, 2), 2);
                    totalAmount += item.ReturnPrice * item.ReturnedQuantity;
                }
                partsSalesReturnBill.TotalAmount = totalAmount;

            } else if (partsSalesReturnBill.ReturnType == (int)DcsPartsSalesReturn_ReturnType.保底退货) {
                var submitCompany = ObjectContext.Companies.Single(r => r.Id == partsSalesReturnBill.SubmitCompanyId);
                int? warehouseId = 0;
                if (submitCompany.Type == (int)DcsCompanyType.代理库 || submitCompany.Type == (int)DcsCompanyType.服务站兼代理库) {
                    warehouseId = partsSalesReturnBill.ReturnWarehouseId;
                }
                var bottomStocks = ObjectContext.BottomStocks.Where(r => r.CompanyID == submitCompany.Id && partIds.Contains(r.SparePartId) && (r.WarehouseID ?? 0) == warehouseId).ToArray();
                var returnStrategies = ObjectContext.CenterSaleReturnStrategies.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.ReturnType == (int)DcsPartsSalesReturn_ReturnType.保底退货).ToArray();
                Decimal totalAmount = Decimal.Zero;
                foreach (var item in partsSalesReturnBill.PartsSalesReturnBillDetails) {
                    var bts = bottomStocks.Where(r => r.SparePartId == item.SparePartId).ToArray();
                    if (bts == null || !bts.Any()) { 
                        throw new ValidationException("配件" + item.SparePartCode + "不存在保底库存");
                    }
                    if (bts.Any(r => r.Status == (int)DcsBaseDataStatus.作废) && !bts.Any(r => r.Status == (int)DcsBaseDataStatus.有效)) {
                        var abandonTime = bts.Where(r => r.Status == (int)DcsBaseDataStatus.作废).Min(x => x.AbandonTime);
                        var timeSlot = (DateTime.Now.Date - abandonTime.Value.Date).Days;
                        var returnDays = returnStrategies.Where(r => timeSlot > r.TimeFrom.Value && timeSlot <= r.TimeTo.Value).SingleOrDefault();
                        if (returnDays == null || returnDays.DiscountRate == 0) {
                            throw new ValidationException("配件" + item.SparePartCode + "不允许做保底退货");
                        } else {
                            item.DiscountRate = returnDays.DiscountRate;
                        }

                        if(null != item.DiscountRate) {
                            item.ReturnPrice = Math.Round(Decimal.Parse((item.OriginalOrderPrice * Decimal.Parse(item.DiscountRate.ToString())).ToString()), 2);
                        }
                       // item.ReturnPrice = Math.Round(item.ReturnPrice * Math.Round((decimal)item.DiscountRate, 2), 2);
                        totalAmount += item.ReturnPrice * item.ReturnedQuantity;
                    } else { 
                        throw new ValidationException("配件" + item.SparePartCode + "不允许做保底退货");
                    }
                }
                partsSalesReturnBill.TotalAmount = totalAmount;
            } else { 
                throw new ValidationException("未找到对应的退货类型");
            }
            var company = ObjectContext.Companies.SingleOrDefault(r => r.Id == partsSalesReturnBill.ReturnCompanyId && r.Status == (int)DcsMasterDataStatus.有效);
            if(company == null)
                throw new ValidationException(ErrorStrings.PartsSalesReturnBill_Validation5);
            var salesUnit = ObjectContext.SalesUnits.SingleOrDefault(r => r.Id == partsSalesReturnBill.SalesUnitId && r.Status == (int)DcsBaseDataStatus.有效);
            if(salesUnit == null) {
                throw new ValidationException("未找到对应的销售组织");
            }
            if (company.Type == (int)DcsCompanyType.代理库 || company.Type == (int)DcsCompanyType.服务站兼代理库) {
                if (!partsSalesReturnBill.ReturnWarehouseId.HasValue) {
                    throw new ValidationException("代理库退货提报，退货仓库应必填!");
                }
                //校验代理库库存
                //获取清单中配件的可用库存
                var warehousePartsStocks = DomainService.查询仓库库存(partsSalesReturnBill.ReturnCompanyId, partsSalesReturnBill.ReturnWarehouseId, partIds).ToList();
                if (warehousePartsStocks.Count <= 0) {
                    throw new ValidationException(ErrorStrings.WarehousePartsStock_Validation1);
                }
                //校验退货量是否大于可用库存
                foreach (var partsSalesReturnBillDetail in partsSalesReturnBill.PartsSalesReturnBillDetails) {
                    var returnQuantity = partsSalesReturnBill.PartsSalesReturnBillDetails.Where(r => r.SparePartId == partsSalesReturnBillDetail.SparePartId).Sum(r => r.ReturnedQuantity);
                    var checkPartsStock = warehousePartsStocks.SingleOrDefault(r => r.SparePartId == partsSalesReturnBillDetail.SparePartId);
                    if (checkPartsStock == null || returnQuantity > checkPartsStock.UsableQuantity) {
                        throw new ValidationException(string.Format(ErrorStrings.CompanyTransferOrder_Validation11, partsSalesReturnBillDetail.SparePartCode));
                    }
                }
            } else if (company.Type != (int)DcsCompanyType.集团企业) { // 如果退货单位企业类型 = 集团企业，不校验库存是否足够
                //校验服务站库存

                //获取退货单清单中相关配件的库存
                var dealerPartsStrocks = ObjectContext.DealerPartsStocks.Where(r => r.DealerId == partsSalesReturnBill.ReturnCompanyId && r.SubDealerId == -1 && r.SalesCategoryId == salesUnit.PartsSalesCategoryId && partIds.Contains(r.SparePartId)).ToArray();
                foreach (var partsSalesReturnBillDetail in partsSalesReturnBill.PartsSalesReturnBillDetails) {
                    var dealerPartsStock = dealerPartsStrocks.SingleOrDefault(r => r.SparePartId == partsSalesReturnBillDetail.SparePartId);
                    //校验拆散件信息
                    var dbPartDeleaveInformation = ObjectContext.PartDeleaveInformations.SingleOrDefault(c => c.OldPartId == partsSalesReturnBillDetail.SparePartId && c.PartsSalesCategoryId == salesUnit.PartsSalesCategoryId && c.BranchId == partsSalesReturnBill.BranchId && c.Status == (int)DcsBaseDataStatus.有效);
                    if (dealerPartsStock != null) {
                        var returnQuantity = partsSalesReturnBill.PartsSalesReturnBillDetails.Where(r => r.SparePartId == partsSalesReturnBillDetail.SparePartId).Sum(r => r.ReturnedQuantity);
                        if (dealerPartsStock.Quantity < returnQuantity) {
                            throw new ValidationException(string.Format(ErrorStrings.CompanyTransferOrder_Validation11, dealerPartsStock.SparePartCode));
                        }
                    } else {
                        throw new ValidationException(string.Format(ErrorStrings.CompanyTransferOrder_Validation11, partsSalesReturnBillDetail.SparePartCode));
                    }
                }
            } else if(company.Type == (int)DcsCompanyType.服务站 || company.Type == (int)DcsCompanyType.集团企业) {
                //验证标签码
                    this.ValidateDealerTrace(partsSalesReturnBill);
                }
            var user = Utils.GetCurrentUserInfo();
            partsSalesReturnBill.Status = (int)DcsPartsSalesReturnBillStatus.提交;
            partsSalesReturnBill.ModifierId = user.Id;
            partsSalesReturnBill.ModifierName = user.Name;
            partsSalesReturnBill.ModifyTime = DateTime.Now;
            UpdateToDatabase(partsSalesReturnBill);
        }
        public void 审批配件销售退货单(PartsSalesReturnBill partsSalesReturnBill) {
            UpdateToDatabase(partsSalesReturnBill);
            var userInfo = Utils.GetCurrentUserInfo();
            partsSalesReturnBill.ApproverId = userInfo.Id;
            partsSalesReturnBill.ApproverName = userInfo.Name;
            partsSalesReturnBill.ApproveTime = DateTime.Now;
            var uscompany = ObjectContext.Companies.Where(r => r.Id == userInfo.EnterpriseId).FirstOrDefault();
            partsSalesReturnBill.Status = (int)DcsPartsSalesReturnBillStatus.审核通过;
            //	新增、修改、初审、审核退货单，是否质保可编辑， 修改是否质保=是退货入库仓库默认为 仓库信息.是否质量件= 是的仓库（如果有多条，默认一条 可修改）
            if(partsSalesReturnBill.IsWarrantyReturn.HasValue && partsSalesReturnBill.IsWarrantyReturn.Value) {
                var zlWarehous = ObjectContext.Warehouses.Where(t => t.IsQualityWarehouse == true && t.Status == (int)DcsBaseDataStatus.有效 && t.StorageCompanyId == partsSalesReturnBill.SalesUnitOwnerCompanyId).FirstOrDefault();
                if(zlWarehous == null) {
                    throw new ValidationException("未找到质量件仓库信息");
                }
                partsSalesReturnBill.WarehouseId = zlWarehous.Id;
                partsSalesReturnBill.WarehouseCode = zlWarehous.Code;
                partsSalesReturnBill.WarehouseName = zlWarehous.Name;
            } else if(partsSalesReturnBill.IsWarrantyReturn.HasValue && !partsSalesReturnBill.IsWarrantyReturn.Value && partsSalesReturnBill.WarehouseName.Contains("质量")) {
                var reDetail = partsSalesReturnBill.PartsSalesReturnBillDetails.First();
                var inWarehouse = ObjectContext.Warehouses.Where(t => ObjectContext.PartsSalesOrders.Any(r => r.Id == reDetail.PartsSalesOrderId && r.WarehouseId == t.Id)).FirstOrDefault();
                if(inWarehouse != null) {
                    partsSalesReturnBill.WarehouseId = inWarehouse.Id;
                    partsSalesReturnBill.WarehouseCode = inWarehouse.Code;
                    partsSalesReturnBill.WarehouseName = inWarehouse.Name;
                }
            }
            //当填写折扣时，重新计算金额
            if(null != partsSalesReturnBill.DiscountRate) {
                this.upDataByDiscountRate(partsSalesReturnBill);
            }
            var company = ObjectContext.Companies.Where(t => t.Id == partsSalesReturnBill.ReturnCompanyId).First();
            //验证标签码
            if(company.Type == (int)DcsCompanyType.服务站 || company.Type == (int)DcsCompanyType.集团企业) {
                this.ValidateDealerTrace(partsSalesReturnBill);
            }
            this.UpdatePartsSalesReturnBillValidate(partsSalesReturnBill);
            ObjectContext.SaveChanges();
        }

        public void 驳回配件销售退货单(PartsSalesReturnBill partsSalesReturnBill) {
            var dbPartsSalesReturnBill = ObjectContext.PartsSalesReturnBills.Where(r => r.Id == partsSalesReturnBill.Id && r.Status == (int)DcsPartsSalesReturnBillStatus.提交).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbPartsSalesReturnBill == null) {
                throw new ValidationException("只能驳回提交状态的单据");
            }
            partsSalesReturnBill.Status = (int)DcsPartsSalesReturnBillStatus.新增;
            var userinfo = Utils.GetCurrentUserInfo();
            partsSalesReturnBill.RejecterId = userinfo.Id;
            partsSalesReturnBill.RejecterName = userinfo.Name;
            partsSalesReturnBill.RejectTime = DateTime.Now;
            UpdateToDatabase(partsSalesReturnBill);
            this.UpdatePartsSalesReturnBillValidate(partsSalesReturnBill);
        }


        public void 作废配件销售退货单(PartsSalesReturnBill partsSalesReturnBill) {
            var dbPartsSalesReturnBill = ObjectContext.PartsSalesReturnBills.Where(r => r.Id == partsSalesReturnBill.Id && r.Status == (int)DcsPartsSalesReturnBillStatus.新增).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbPartsSalesReturnBill == null) {
                throw new ValidationException("只能作废新建状态的单据");
            }
            partsSalesReturnBill.Status = (int)DcsPartsSalesReturnBillStatus.作废;
            var userInfo = Utils.GetCurrentUserInfo();
            partsSalesReturnBill.ModifierId = userInfo.Id;
            partsSalesReturnBill.ModifierName = userInfo.Name;
            partsSalesReturnBill.ModifyTime = DateTime.Now;
            partsSalesReturnBill.AbandonerId = userInfo.Id;
            partsSalesReturnBill.AbandonerName = userInfo.Name;
            partsSalesReturnBill.AbandonTime = DateTime.Now;
            UpdateToDatabase(partsSalesReturnBill);


        }


        public void 终止配件销售退货单(PartsSalesReturnBill partsSalesReturnBill) {
            var dbPartsSalesReturnBill = ObjectContext.PartsSalesReturnBills.Where(r => r.Id == partsSalesReturnBill.Id && r.Status == (int)DcsPartsSalesReturnBillStatus.提交).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbPartsSalesReturnBill == null) {
                throw new ValidationException("只能终止提交状态的单据");
            }
            var userInfo = Utils.GetCurrentUserInfo();
            partsSalesReturnBill.Status = (int)DcsPartsSalesReturnBillStatus.终止;
            partsSalesReturnBill.StopId = userInfo.Id;
            partsSalesReturnBill.StopName = userInfo.Name;
            partsSalesReturnBill.StopTime = DateTime.Now;
            UpdateToDatabase(partsSalesReturnBill);
        }

        public void 上传销售退货单附件(PartsSalesReturnBill partsSalesReturnBill) {
            var userInfo = Utils.GetCurrentUserInfo();
            partsSalesReturnBill.ModifierId = userInfo.Id;
            partsSalesReturnBill.ModifierName = userInfo.Name;
            partsSalesReturnBill.ModifyTime = DateTime.Now;
            partsSalesReturnBill.Path = partsSalesReturnBill.Path;
            UpdateToDatabase(partsSalesReturnBill);
        }
        public void 销售订单退货初审(PartsSalesReturnBill partsSalesReturnBill) {  
            var userInfo = Utils.GetCurrentUserInfo();
            partsSalesReturnBill.ModifierId = userInfo.Id;
            partsSalesReturnBill.ModifierName = userInfo.Name;
            partsSalesReturnBill.ModifyTime = DateTime.Now;
            partsSalesReturnBill.InitialApproverId = userInfo.Id;
            partsSalesReturnBill.InitialApproverName = userInfo.Name;
            partsSalesReturnBill.InitialApproveTime = DateTime.Now;
            if(partsSalesReturnBill.ReturnType == (int)DcsPartsSalesReturn_ReturnType.正常退货 || partsSalesReturnBill.ReturnType == (int)DcsPartsSalesReturn_ReturnType.保底退货) {
                partsSalesReturnBill.Status = (int)DcsPartsSalesReturnBillStatus.审批通过;
            } else {
                partsSalesReturnBill.Status = (int)DcsPartsSalesReturnBillStatus.初审通过;
            }
            UpdateToDatabase(partsSalesReturnBill);
            //	新增、修改、初审、审核退货单，是否质保可编辑， 修改是否质保=是退货入库仓库默认为 仓库信息.是否质量件= 是的仓库（如果有多条，默认一条 可修改）
            if(partsSalesReturnBill.IsWarrantyReturn.HasValue && partsSalesReturnBill.IsWarrantyReturn.Value) {
                var zlWarehous = ObjectContext.Warehouses.Where(t => t.IsQualityWarehouse == true && t.Status == (int)DcsBaseDataStatus.有效 && t.StorageCompanyId == partsSalesReturnBill.SalesUnitOwnerCompanyId).FirstOrDefault();
                if(zlWarehous == null) {
                    throw new ValidationException("未找到质量件仓库信息");
                }
                partsSalesReturnBill.WarehouseId = zlWarehous.Id;
                partsSalesReturnBill.WarehouseCode = zlWarehous.Code;
                partsSalesReturnBill.WarehouseName = zlWarehous.Name;
            } else if(partsSalesReturnBill.IsWarrantyReturn.HasValue && !partsSalesReturnBill.IsWarrantyReturn.Value && partsSalesReturnBill.WarehouseName.Contains("质量")) {
                var reDetail = partsSalesReturnBill.PartsSalesReturnBillDetails.First();
                var inWarehouse = ObjectContext.Warehouses.Where(t => ObjectContext.PartsSalesOrders.Any(r => r.Id == reDetail.PartsSalesOrderId && r.WarehouseId == t.Id)).FirstOrDefault();
                if(inWarehouse != null) {
                    partsSalesReturnBill.WarehouseId = inWarehouse.Id;
                    partsSalesReturnBill.WarehouseCode = inWarehouse.Code;
                    partsSalesReturnBill.WarehouseName = inWarehouse.Name;
                }
            }

            //当填写折扣时
            //退货总金额
            //当填写折扣时，重新计算金额
            if(null != partsSalesReturnBill.DiscountRate) {
                this.upDataByDiscountRate(partsSalesReturnBill);
            }
            //正常退货和保底退货只有初审
            if(partsSalesReturnBill.ReturnType == (int)DcsPartsSalesReturn_ReturnType.特殊退货) {
                return;
            }
            var partsSalesReturnBillDetails = partsSalesReturnBill.PartsSalesReturnBillDetails.OrderByDescending(r => r.PartsSalesOrderId);
            var company = ObjectContext.Companies.SingleOrDefault(r => r.Id == partsSalesReturnBill.ReturnCompanyId && r.Status == (int)DcsMasterDataStatus.有效);
            if(company == null)
                throw new ValidationException(ErrorStrings.PartsSalesReturnBill_Validation5);
            var receCompany = ObjectContext.Companies.SingleOrDefault(t => t.Id == partsSalesReturnBill.SalesUnitOwnerCompanyId && t.Status == (int)DcsMasterDataStatus.有效);
            if(receCompany == null)
                throw new ValidationException("销售退货对应的收货单位不存在");
            var dbSalesUnit = ObjectContext.SalesUnits.SingleOrDefault(r => r.Id == partsSalesReturnBill.SalesUnitId && r.Status != (int)DcsMasterDataStatus.作废);
            if(dbSalesUnit == null)
                throw new ValidationException(ErrorStrings.PartsSalesReturnBill_Validation11);

            if(company.Type == (int)DcsCompanyType.代理库 || company.Type == (int)DcsCompanyType.服务站兼代理库) {
                if(partsSalesReturnBillDetails.All(r => !r.ApproveQuantity.HasValue || r.ApproveQuantity.Value == 0))
                    throw new ValidationException(ErrorStrings.PartsSalesReturnBill_Validation6);

                if(!partsSalesReturnBill.ReturnWarehouseId.HasValue) {
                    throw new ValidationException("代理库退货提报，退货仓库应必填!");
                }
                var returnWarehouse = ObjectContext.Warehouses.SingleOrDefault(r => r.Id == partsSalesReturnBill.ReturnWarehouseId);


                var partsOutboundPlan = new PartsOutboundPlan {
                    WarehouseId = partsSalesReturnBill.ReturnWarehouseId.Value,
                    WarehouseCode = partsSalesReturnBill.ReturnWarehouseCode,
                    WarehouseName = partsSalesReturnBill.ReturnWarehouseName,
                    StorageCompanyId = partsSalesReturnBill.ReturnCompanyId,
                    StorageCompanyCode = partsSalesReturnBill.ReturnCompanyCode,
                    StorageCompanyName = partsSalesReturnBill.ReturnCompanyName,
                    CustomerAccountId = partsSalesReturnBill.CustomerAccountId,
                    StorageCompanyType = (int)DcsCompanyType.代理库,
                    BranchId = partsSalesReturnBill.BranchId,
                    BranchCode = partsSalesReturnBill.BranchCode,
                    BranchName = partsSalesReturnBill.BranchName,
                    CounterpartCompanyId = partsSalesReturnBill.SalesUnitOwnerCompanyId,
                    CounterpartCompanyCode = partsSalesReturnBill.SalesUnitOwnerCompanyCode,
                    CounterpartCompanyName = partsSalesReturnBill.SalesUnitOwnerCompanyName,
                    ReceivingCompanyId = partsSalesReturnBill.SalesUnitOwnerCompanyId,
                    ReceivingCompanyCode = partsSalesReturnBill.SalesUnitOwnerCompanyCode,
                    ReceivingCompanyName = partsSalesReturnBill.SalesUnitOwnerCompanyName,
                    ReceivingWarehouseId = partsSalesReturnBill.WarehouseId,
                    ReceivingWarehouseCode = partsSalesReturnBill.WarehouseCode,
                    ReceivingWarehouseName = partsSalesReturnBill.WarehouseName,
                    OriginalRequirementBillId = partsSalesReturnBill.Id,
                    OriginalRequirementBillCode = partsSalesReturnBill.Code,
                    OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.配件销售退货单,
                    SourceId = partsSalesReturnBill.Id,
                    SourceCode = partsSalesReturnBill.Code,
                    OutboundType = (int)DcsPartsOutboundType.采购退货,
                    Status = (int)DcsPartsOutboundPlanStatus.新建,
                    PartsSalesCategoryId = dbSalesUnit.PartsSalesCategoryId,
                    IfWmsInterface = returnWarehouse.WmsInterface,
                    ERPSourceOrderCode = partsSalesReturnBill.ERPSourceOrderCode
                };
                var partIds = partsSalesReturnBillDetails.Select(r => r.SparePartId).ToArray();
                //校验代理库库存
                //获取清单中配件的可用库存
                var warehousePartsStocks = DomainService.查询仓库库存(partsSalesReturnBill.ReturnCompanyId, partsSalesReturnBill.ReturnWarehouseId, partIds).ToList();
                if(warehousePartsStocks.Count <= 0) {
                    throw new ValidationException(ErrorStrings.WarehousePartsStock_Validation1);
                }
                foreach(var partsSalesReturnBillDetail in partsSalesReturnBillDetails) {
                    if(!partsSalesReturnBillDetail.ApproveQuantity.HasValue)
                        throw new ValidationException(string.Format(ErrorStrings.PartsSalesReturnBill_Validation8, partsSalesReturnBillDetail.SparePartCode));
                    if(partsSalesReturnBillDetail.ApproveQuantity == 0)
                        continue;
                    
                    //校验退货量是否大于可用库存 
                    //当可用库存小于审核数量时，赋值可用数量
                    var checkPartsStock = warehousePartsStocks.SingleOrDefault(r => r.SparePartId == partsSalesReturnBillDetail.SparePartId);
                    if(checkPartsStock == null) {
                        throw new ValidationException(string.Format("配件编号为{0}的库存不存在", partsSalesReturnBillDetail.SparePartCode));
                    }
                    if (checkPartsStock.UsableQuantity < partsSalesReturnBillDetail.ApproveQuantity.Value) {
                            if (checkPartsStock.UsableQuantity > 0) {
                                partsSalesReturnBillDetail.ApproveQuantity = checkPartsStock.UsableQuantity;
                                checkPartsStock.UsableQuantity = 0;
                            } else {
                                partsSalesReturnBillDetail.ApproveQuantity = 0;
                                continue;
                            }
                        } else {
                            checkPartsStock.UsableQuantity = checkPartsStock.UsableQuantity - partsSalesReturnBillDetail.ApproveQuantity.Value;
                        }

                    var PartsOutboundPlanDetail = partsOutboundPlan.PartsOutboundPlanDetails.FirstOrDefault(r => r.SparePartId == partsSalesReturnBillDetail.SparePartId && r.Price == partsSalesReturnBillDetail.ReturnPrice);
                    if (PartsOutboundPlanDetail == null) {
                        partsOutboundPlan.PartsOutboundPlanDetails.Add(new PartsOutboundPlanDetail {
                            SparePartId = partsSalesReturnBillDetail.SparePartId,
                            SparePartCode = partsSalesReturnBillDetail.SparePartCode,
                            SparePartName = partsSalesReturnBillDetail.SparePartName,
                            PlannedAmount = partsSalesReturnBillDetail.ApproveQuantity.Value,
                            Price = partsSalesReturnBillDetail.ReturnPrice,
                            OriginalPrice = partsSalesReturnBillDetail.OriginalPrice
                        });
                    } else {
                        PartsOutboundPlanDetail.PlannedAmount += partsSalesReturnBillDetail.ApproveQuantity.Value;
                    }
                }
                if (partsSalesReturnBillDetails.Any(r => r.ApproveQuantity.HasValue && r.ApproveQuantity.Value > 0))
                    DomainService.生成配件出库计划(partsOutboundPlan);
            }

            if(company.Type == (int)DcsCompanyType.服务站) {
                var partsIds = partsSalesReturnBill.PartsSalesReturnBillDetails.Select(r => r.SparePartId).ToArray();
                var dealerPartsStocks = ObjectContext.DealerPartsStocks.Where(r => r.SubDealerId == -1 && r.DealerId == partsSalesReturnBill.ReturnCompanyId && partsIds.Contains(r.SparePartId) && r.SalesCategoryId == dbSalesUnit.PartsSalesCategoryId).ToArray();
                if(!partsSalesReturnBill.WarehouseId.HasValue || string.IsNullOrEmpty(partsSalesReturnBill.WarehouseCode) || string.IsNullOrEmpty(partsSalesReturnBill.WarehouseName))
                    throw new ValidationException(ErrorStrings.PartsSalesReturnBill_Validation7);
                var dbcompany = ObjectContext.Companies.SingleOrDefault(r => r.Id == partsSalesReturnBill.SalesUnitOwnerCompanyId && r.Status == (int)DcsMasterDataStatus.有效);
                if(dbcompany == null)
                    throw new ValidationException(ErrorStrings.PartsSalesReturnBill_Validation9);
                var partsInboundPlan = new PartsInboundPlan {
                    WarehouseId = partsSalesReturnBill.WarehouseId.Value,
                    WarehouseCode = partsSalesReturnBill.WarehouseCode,
                    WarehouseName = partsSalesReturnBill.WarehouseName,
                    StorageCompanyId = partsSalesReturnBill.SalesUnitOwnerCompanyId,
                    StorageCompanyCode = partsSalesReturnBill.SalesUnitOwnerCompanyCode,
                    StorageCompanyName = partsSalesReturnBill.SalesUnitOwnerCompanyName,
                    StorageCompanyType = dbcompany.Type,
                    BranchId = partsSalesReturnBill.BranchId,
                    BranchCode = partsSalesReturnBill.BranchCode,
                    BranchName = partsSalesReturnBill.BranchName,
                    CounterpartCompanyId = partsSalesReturnBill.ReturnCompanyId,
                    CounterpartCompanyCode = partsSalesReturnBill.ReturnCompanyCode,
                    CounterpartCompanyName = partsSalesReturnBill.ReturnCompanyName,
                    SourceId = partsSalesReturnBill.Id,
                    SourceCode = partsSalesReturnBill.Code,
                    InboundType = (int)DcsPartsInboundType.销售退货,
                    CustomerAccountId = partsSalesReturnBill.CustomerAccountId,
                    OriginalRequirementBillId = partsSalesReturnBill.Id,
                    OriginalRequirementBillCode = partsSalesReturnBill.Code,
                    OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.配件销售退货单,
                    Status = (int)DcsPartsInboundPlanStatus.新建,
                    PartsSalesCategoryId = dbSalesUnit.PartsSalesCategoryId,
                    ERPSourceOrderCode = partsSalesReturnBill.ERPSourceOrderCode
                };
                foreach(var partsSalesReturnBillDetail in partsSalesReturnBillDetails) {
                    if(!partsSalesReturnBillDetail.ApproveQuantity.HasValue)
                        throw new ValidationException(string.Format(ErrorStrings.PartsSalesReturnBill_Validation8, partsSalesReturnBillDetail.SparePartCode));
                    if(partsSalesReturnBillDetail.ApproveQuantity == 0)
                        continue;
                    var dealerPartsStock = dealerPartsStocks.FirstOrDefault(r => r.SparePartId == partsSalesReturnBillDetail.SparePartId);
                    if(dealerPartsStock == null)
                        throw new ValidationException("经销商配件库存不存在");
                    //if (dealerPartsStock.Quantity <= 0) { 
                    //    throw new ValidationException(string.Format("配件编号为{0}的经销商配件库存为0", dealerPartsStock.SparePartCode));
                    //}
                    if (dealerPartsStock.Quantity < partsSalesReturnBillDetail.ApproveQuantity.Value) { 
                        partsSalesReturnBillDetail.ApproveQuantity = dealerPartsStock.Quantity;
                    }
                    //如果服务站库存等于0，不生成入库计划单
                    if (dealerPartsStock.Quantity <= 0) { 
                        partsSalesReturnBillDetail.ApproveQuantity = 0;
                        continue;
                    }
                    dealerPartsStock.Quantity -= partsSalesReturnBillDetail.ApproveQuantity.Value;
                    UpdateToDatabase(dealerPartsStock);
                    new DealerPartsStockAch(this.DomainService).UpdateDealerPartsStockValidate(dealerPartsStock);

                    var partsInboundPlanDetail = partsInboundPlan.PartsInboundPlanDetails.FirstOrDefault(o => o.SparePartId == partsSalesReturnBillDetail.SparePartId && o.Price == partsSalesReturnBillDetail.ReturnPrice);
                    if(partsInboundPlanDetail != null) {
                        partsInboundPlanDetail.PlannedAmount += partsSalesReturnBillDetail.ApproveQuantity.Value;
                    } else {
                        partsInboundPlan.PartsInboundPlanDetails.Add(new PartsInboundPlanDetail {
                            SparePartId = partsSalesReturnBillDetail.SparePartId,
                            SparePartCode = partsSalesReturnBillDetail.SparePartCode,
                            SparePartName = partsSalesReturnBillDetail.SparePartName,
                            PlannedAmount = partsSalesReturnBillDetail.ApproveQuantity.Value,
                            Price = partsSalesReturnBillDetail.ReturnPrice,
                            POCode = partsSalesReturnBillDetail.SettlementPurchaseOrder,
                            OriginalPrice = partsSalesReturnBillDetail.OriginalPrice
                        });
                    }
                }
                if (partsSalesReturnBillDetails.Any(r => r.ApproveQuantity.HasValue && r.ApproveQuantity.Value > 0)) {
                    InsertToDatabase(partsInboundPlan);
                    new PartsInboundPlanAch(this.DomainService).InsertPartsInboundPlanValidate(partsInboundPlan);
                    ObjectContext.SaveChanges();
                    //记录追溯信息
                    var inTraces = ObjectContext.AccurateTraces.Where(t => t.CompanyId == partsSalesReturnBill.ReturnCompanyId && t.Type == (int)DCSAccurateTraceType.入库 && (t.OutQty == null || t.InQty != t.OutQty) && t.Status == (int)DCSAccurateTraceStatus.有效).ToArray();
                    var abandInTrace = ObjectContext.AccurateTraces.Where(t => t.CompanyId == receCompany.Id && t.Type == (int)DCSAccurateTraceType.入库 && (t.InQty == (t.OutQty ?? 0) && t.InQty > 0)).ToArray();
                    //出库
                    var tracOldOut = ObjectContext.AccurateTraces.Where(t => t.Status == (int)DCSAccurateTraceStatus.有效 && t.CompanyId == partsSalesReturnBill.ReturnCompanyId && t.Type == (int)DCSAccurateTraceType.出库 && t.SIHLabelCode != null).ToArray();
                    foreach(var item in partsSalesReturnBill.PartsSalesReturnBillDetails) {
                        if(item.TraceProperty.HasValue && !string.IsNullOrEmpty(item.SIHLabelCode)) {
                            var traceQty = 0;
                            var traces = item.SIHLabelCode.Split(new Char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                            foreach(var code in traces) {
                                
                                var inTrace = inTraces.Where(t => t.BoxCode == code).ToArray();
                                if(code.EndsWith("J"))
                                    inTrace = inTraces.Where(t => t.SIHLabelCode == code).ToArray();
                                if(inTrace.Count() == 0) {
                                    throw new ValidationException(code + "标签码已出库完成");
                                }
                                foreach(var trace in inTrace) {
                                    if(traceQty == item.ReturnedQuantity) {
                                        continue;
                                    }
                                    var currentQty = 0;
                                    if(item.TraceProperty.Value == (int)DCSTraceProperty.精确追溯) {
                                        traceQty++;
                                        currentQty = 1;
                                    } else {
                                        if(((trace.InQty - (trace.OutQty ?? 0))) > (item.ReturnedQuantity - traceQty)) {
                                            traceQty = item.ReturnedQuantity;
                                            currentQty = item.ReturnedQuantity - traceQty;
                                        } else {
                                            traceQty = traceQty + (trace.InQty.Value - (trace.OutQty ?? 0));
                                            currentQty = (trace.InQty.Value - (trace.OutQty ?? 0));
                                        }
                                    }
                                    var newAccurateTrace = new AccurateTrace {
                                        Type = (int)DCSAccurateTraceType.入库,
                                        CompanyType = receCompany.Type == (int)DcsCompanyType.分公司 ? (int)DCSAccurateTraceCompanyType.配件中心 : (int)DCSAccurateTraceCompanyType.中心库,
                                        CompanyId = receCompany.Id,
                                        SourceBillId = partsInboundPlan.Id,
                                        PartId = item.SparePartId,
                                        TraceCode = trace.TraceCode,
                                        SIHLabelCode = trace.SIHLabelCode,
                                        Status = (int)DCSAccurateTraceStatus.有效,
                                        TraceProperty = trace.TraceProperty,
                                        BoxCode = trace.BoxCode,
                                        InQty = currentQty
                                    };
                                    if(receCompany.Type == (int)DcsCompanyType.分公司) {
                                        newAccurateTrace.BoxCode = null;
                                        newAccurateTrace.SIHLabelCode = null;
                                        newAccurateTrace.InQty = null;
                                    }
                                    new AccurateTraceAch(this.DomainService).ValidataAccurateTrace(newAccurateTrace);
                                    var aband = abandInTrace.Where(t => t.TraceCode == trace.TraceCode).FirstOrDefault();
                                    if(aband != null) {
                                        aband.Status = (int)DCSAccurateTraceStatus.作废;
                                        aband.ModifierId = userInfo.Id;
                                        aband.ModifierName = userInfo.Name;
                                        aband.ModifyTime = DateTime.Now;
                                        UpdateToDatabase(aband);
                                    }
                                    var outAccurateTrace = new AccurateTrace {
                                        Type = (int)DCSAccurateTraceType.出库,
                                        CompanyType =  (int)DCSAccurateTraceCompanyType.服务站,
                                        CompanyId = company.Id,
                                        SourceBillId = partsSalesReturnBill.Id,
                                        PartId = item.SparePartId,
                                        TraceCode = trace.TraceCode,
                                        SIHLabelCode = trace.SIHLabelCode,
                                        Status = (int)DCSAccurateTraceStatus.有效,
                                        TraceProperty = trace.TraceProperty,
                                        BoxCode = trace.BoxCode,
                                        InQty = currentQty,
                                        OutQty = currentQty
                                    };
                                    new AccurateTraceAch(this.DomainService).ValidataAccurateTrace(outAccurateTrace);
                                    trace.OutQty = trace.OutQty ?? 0 + currentQty;
                                    trace.ModifierId = userInfo.Id;
                                    trace.ModifierName = userInfo.Name;
                                    trace.ModifyTime = DateTime.Now;
                                    UpdateToDatabase(trace);
                                    if(item.TraceProperty.Value == (int)DCSTraceProperty.精确追溯) {
                                        var outTraceLs = tracOldOut.Where(t => t.SourceBillId != partsSalesReturnBill.Id && t.TraceCode == trace.TraceCode && t.Status == (int)DCSAccurateTraceStatus.有效).ToArray();
                                        foreach(var outTrace in outTraceLs) {
                                            outTrace.ModifierId = userInfo.Id;
                                            outTrace.ModifierName = userInfo.Name;
                                            outTrace.ModifyTime = DateTime.Now;
                                            outTrace.Status = (int)DCSAccurateTraceStatus.作废;
                                            UpdateToDatabase(outTrace);
                                        }
                                    } 
                                }
                            }
                        }
                    }
                }              
            }
            //集团客户和服务站一样。只是不扣库存
            if(company.Type == (int)DcsCompanyType.集团企业) {
                var partsIds = partsSalesReturnBill.PartsSalesReturnBillDetails.Select(r => r.SparePartId).ToArray();
                var dealerPartsStocks = ObjectContext.DealerPartsStocks.Where(r => r.SubDealerId == -1 && r.DealerId == partsSalesReturnBill.ReturnCompanyId && partsIds.Contains(r.SparePartId) && r.SalesCategoryId == dbSalesUnit.PartsSalesCategoryId).ToArray();
                if(!partsSalesReturnBill.WarehouseId.HasValue || string.IsNullOrEmpty(partsSalesReturnBill.WarehouseCode) || string.IsNullOrEmpty(partsSalesReturnBill.WarehouseName))
                    throw new ValidationException(ErrorStrings.PartsSalesReturnBill_Validation7);
                var dbcompany = ObjectContext.Companies.SingleOrDefault(r => r.Id == partsSalesReturnBill.SalesUnitOwnerCompanyId && r.Status == (int)DcsMasterDataStatus.有效);
                if(dbcompany == null)
                    throw new ValidationException(ErrorStrings.PartsSalesReturnBill_Validation9);
                var partsInboundPlan = new PartsInboundPlan {
                    WarehouseId = partsSalesReturnBill.WarehouseId.Value,
                    WarehouseCode = partsSalesReturnBill.WarehouseCode,
                    WarehouseName = partsSalesReturnBill.WarehouseName,
                    StorageCompanyId = partsSalesReturnBill.SalesUnitOwnerCompanyId,
                    StorageCompanyCode = partsSalesReturnBill.SalesUnitOwnerCompanyCode,
                    StorageCompanyName = partsSalesReturnBill.SalesUnitOwnerCompanyName,
                    StorageCompanyType = dbcompany.Type,
                    BranchId = partsSalesReturnBill.BranchId,
                    BranchCode = partsSalesReturnBill.BranchCode,
                    BranchName = partsSalesReturnBill.BranchName,
                    CounterpartCompanyId = partsSalesReturnBill.ReturnCompanyId,
                    CounterpartCompanyCode = partsSalesReturnBill.ReturnCompanyCode,
                    CounterpartCompanyName = partsSalesReturnBill.ReturnCompanyName,
                    SourceId = partsSalesReturnBill.Id,
                    SourceCode = partsSalesReturnBill.Code,
                    InboundType = (int)DcsPartsInboundType.销售退货,
                    CustomerAccountId = partsSalesReturnBill.CustomerAccountId,
                    OriginalRequirementBillId = partsSalesReturnBill.Id,
                    OriginalRequirementBillCode = partsSalesReturnBill.Code,
                    OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.配件销售退货单,
                    Status = (int)DcsPartsInboundPlanStatus.新建,
                    PartsSalesCategoryId = dbSalesUnit.PartsSalesCategoryId,
                    ERPSourceOrderCode = partsSalesReturnBill.ERPSourceOrderCode
                };
                foreach(var partsSalesReturnBillDetail in partsSalesReturnBillDetails) {
                    if(!partsSalesReturnBillDetail.ApproveQuantity.HasValue)
                        throw new ValidationException(string.Format(ErrorStrings.PartsSalesReturnBill_Validation8, partsSalesReturnBillDetail.SparePartCode));
                    if(partsSalesReturnBillDetail.ApproveQuantity == 0)
                        continue;
                    var partsInboundPlanDetail = partsInboundPlan.PartsInboundPlanDetails.FirstOrDefault(o => o.SparePartId == partsSalesReturnBillDetail.SparePartId && o.Price == partsSalesReturnBillDetail.ReturnPrice);
                    if (partsInboundPlanDetail != null) {
                        partsInboundPlanDetail.PlannedAmount += partsSalesReturnBillDetail.ApproveQuantity.Value;
                    } else {
                        partsInboundPlan.PartsInboundPlanDetails.Add(new PartsInboundPlanDetail {
                            SparePartId = partsSalesReturnBillDetail.SparePartId,
                            SparePartCode = partsSalesReturnBillDetail.SparePartCode,
                            SparePartName = partsSalesReturnBillDetail.SparePartName,
                            PlannedAmount = partsSalesReturnBillDetail.ApproveQuantity.Value,
                            Price = partsSalesReturnBillDetail.ReturnPrice,
                            POCode = partsSalesReturnBillDetail.SettlementPurchaseOrder,
                            OriginalPrice = partsSalesReturnBillDetail.OriginalPrice
                        });
                    }
                }
                InsertToDatabase(partsInboundPlan);
                new PartsInboundPlanAch(this.DomainService).InsertPartsInboundPlanValidate(partsInboundPlan);
                ObjectContext.SaveChanges();
                //记录追溯信息
                var inTraces = ObjectContext.AccurateTraces.Where(t => t.CompanyId == partsSalesReturnBill.ReturnCompanyId && t.Type == (int)DCSAccurateTraceType.入库 && (t.OutQty == null || t.InQty != t.OutQty) && t.Status == (int)DCSAccurateTraceStatus.有效).ToArray();
                var abandInTrace = ObjectContext.AccurateTraces.Where(t => t.CompanyId == receCompany.Id && t.Type == (int)DCSAccurateTraceType.入库 && (t.InQty == (t.OutQty ?? 0) && t.InQty > 0)).ToArray();
                //出库
                var tracOldOut = ObjectContext.AccurateTraces.Where(t => t.Status == (int)DCSAccurateTraceStatus.有效 && t.CompanyId == partsSalesReturnBill.ReturnCompanyId && t.Type == (int)DCSAccurateTraceType.出库 && t.SIHLabelCode != null).ToArray();
                foreach(var item in partsSalesReturnBill.PartsSalesReturnBillDetails) {
                    if(item.TraceProperty.HasValue && !string.IsNullOrEmpty(item.SIHLabelCode)) {
                        var traceQty = 0;
                        var traces = item.SIHLabelCode.Split(new Char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                        foreach(var code in traces) {
                            
                            var inTrace = inTraces.Where(t => t.BoxCode == code).ToArray();
                            if(code.EndsWith("J"))
                                inTrace = inTraces.Where(t => t.SIHLabelCode == code).ToArray();
                            if(inTrace.Count() == 0) {
                                throw new ValidationException(code + "标签码已出库完成");
                            }
                            foreach(var trace in inTrace) {
                                if(traceQty == item.ReturnedQuantity) {
                                    continue;
                                }
                                var currentQty = 0;
                                if(item.TraceProperty.Value == (int)DCSTraceProperty.精确追溯) {
                                    traceQty++;
                                    currentQty = 1;
                                } else {
                                    if(((trace.InQty - (trace.OutQty ?? 0))) > (item.ReturnedQuantity - traceQty)) {
                                        traceQty = item.ReturnedQuantity;
                                        currentQty = item.ReturnedQuantity - traceQty;
                                    } else {
                                        traceQty = traceQty + (trace.InQty.Value - (trace.OutQty ?? 0));
                                        currentQty = (trace.InQty.Value - (trace.OutQty ?? 0));
                                    }
                                }
                                var newAccurateTrace = new AccurateTrace {
                                    Type = (int)DCSAccurateTraceType.入库,
                                    CompanyType = receCompany.Type == (int)DcsCompanyType.分公司 ? (int)DCSAccurateTraceCompanyType.配件中心 : (int)DCSAccurateTraceCompanyType.中心库,
                                    CompanyId = receCompany.Id,
                                    SourceBillId = partsInboundPlan.Id,
                                    PartId = item.SparePartId,
                                    TraceCode = trace.TraceCode,
                                    SIHLabelCode = trace.SIHLabelCode,
                                    Status = (int)DCSAccurateTraceStatus.有效,
                                    TraceProperty = trace.TraceProperty,
                                    BoxCode = trace.BoxCode,
                                    InQty = currentQty
                                };
                                if(receCompany.Type == (int)DcsCompanyType.分公司) {
                                    newAccurateTrace.BoxCode = null;
                                    newAccurateTrace.SIHLabelCode = null;
                                    newAccurateTrace.InQty = null;
                                }
                                new AccurateTraceAch(this.DomainService).ValidataAccurateTrace(newAccurateTrace);
                                var aband = abandInTrace.Where(t => t.TraceCode == trace.TraceCode).FirstOrDefault();
                                if(aband != null) {
                                    aband.Status = (int)DCSAccurateTraceStatus.作废;
                                    aband.ModifierId = userInfo.Id;
                                    aband.ModifierName = userInfo.Name;
                                    aband.ModifyTime = DateTime.Now;
                                    UpdateToDatabase(aband);
                                }
                                var outAccurateTrace = new AccurateTrace {
                                    Type = (int)DCSAccurateTraceType.出库,
                                    CompanyType = (int)DCSAccurateTraceCompanyType.服务站,
                                    CompanyId = company.Id,
                                    SourceBillId = partsSalesReturnBill.Id,
                                    PartId = item.SparePartId,
                                    TraceCode = trace.TraceCode,
                                    SIHLabelCode = trace.SIHLabelCode,
                                    Status = (int)DCSAccurateTraceStatus.有效,
                                    TraceProperty = trace.TraceProperty,
                                    BoxCode = trace.BoxCode,
                                    InQty = currentQty,
                                    OutQty = currentQty
                                };
                                new AccurateTraceAch(this.DomainService).ValidataAccurateTrace(outAccurateTrace);
                                trace.OutQty = trace.OutQty ?? 0 + currentQty;
                                trace.ModifierId = userInfo.Id;
                                trace.ModifierName = userInfo.Name;
                                trace.ModifyTime = DateTime.Now;
                                UpdateToDatabase(trace);
                                if(item.TraceProperty.Value == (int)DCSTraceProperty.精确追溯) {
                                    var outTraceLs = tracOldOut.Where(t => t.SourceBillId != partsSalesReturnBill.Id && t.TraceCode == trace.TraceCode && t.Status == (int)DCSAccurateTraceStatus.有效).ToArray();
                                    foreach(var outTrace in outTraceLs) {
                                        outTrace.ModifierId = userInfo.Id;
                                        outTrace.ModifierName = userInfo.Name;
                                        outTrace.ModifyTime = DateTime.Now;
                                        outTrace.Status = (int)DCSAccurateTraceStatus.作废;
                                        UpdateToDatabase(outTrace);
                                    }
                                } 
                            }
                        }
                    }
                }
            }
            //2“是否换货”字段为“是”的销售退货单审核通过的同时，生成同种配件图号、同样价格和数量的“提交”状态的销售订单，将退货单中的“平台单号”赋值给销售订单的“平台单号”字段
            //根据销售退货单的销售订单号 生成新的销售订单
            if(partsSalesReturnBill.IsBarter == true) {
                foreach(var salesReturnDetail in partsSalesReturnBill.PartsSalesReturnBillDetails) {
                    var partsSalesOrder = this.ObjectContext.PartsSalesOrders.Where(r => r.Code == salesReturnDetail.PartsSalesOrderCode).FirstOrDefault();
                    var partsSalesOrderType = this.ObjectContext.PartsSalesOrderTypes.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.BranchId == partsSalesReturnBill.BranchId && r.Name == "急需订单").FirstOrDefault();
                    var warehouse = this.ObjectContext.Warehouses.Where(r => r.BranchId == partsSalesReturnBill.BranchId && r.Name == "随车行北京CDC仓库" && r.Status == (int)DcsBaseDataStatus.有效).FirstOrDefault();
                    if(partsSalesOrder != null) {
                        var newPartsSalesOrder = new PartsSalesOrder {
                            BranchId = partsSalesOrder.BranchId,
                            BranchCode = partsSalesOrder.BranchCode,
                            BranchName = partsSalesOrder.BranchName,
                            SalesCategoryId = partsSalesOrder.SalesCategoryId,
                            SalesCategoryName = partsSalesOrder.SalesCategoryName,
                            SalesUnitId = partsSalesOrder.SalesUnitId,
                            SalesUnitCode = partsSalesOrder.SalesUnitCode,
                            SalesUnitName = partsSalesOrder.SalesUnitName,
                            SalesUnitOwnerCompanyId = partsSalesOrder.SalesUnitOwnerCompanyId,
                            SalesUnitOwnerCompanyCode = partsSalesOrder.SalesUnitOwnerCompanyCode,
                            SalesUnitOwnerCompanyName = partsSalesOrder.SalesUnitOwnerCompanyName,
                            SourceBillCode = partsSalesOrder.SourceBillCode,
                            SourceBillId = partsSalesOrder.SourceBillId,
                            CustomerAccountId = partsSalesOrder.Id,
                            IsDebt = partsSalesOrder.IsDebt,
                            InvoiceReceiveCompanyId = partsSalesOrder.InvoiceReceiveCompanyId,
                            InvoiceReceiveCompanyCode = partsSalesOrder.InvoiceReceiveCompanyCode,
                            InvoiceReceiveCompanyName = partsSalesOrder.InvoiceReceiveCompanyName,
                            InvoiceReceiveCompanyType = partsSalesOrder.InvoiceReceiveCompanyType,
                            InvoiceReceiveSaleCateId = partsSalesOrder.InvoiceReceiveSaleCateId,
                            InvoiceReceiveSaleCateName = partsSalesOrder.InvoiceReceiveSaleCateName,
                            SubmitCompanyId = partsSalesOrder.SubmitCompanyId,
                            SubmitCompanyCode = partsSalesOrder.SubmitCompanyCode,
                            SubmitCompanyName = partsSalesOrder.SubmitCompanyName,
                            ProvinceID = partsSalesOrder.ProvinceID,
                            Province = partsSalesOrder.Province,
                            City = partsSalesOrder.City,
                            FirstClassStationId = partsSalesOrder.FirstClassStationId,
                            FirstClassStationCode = partsSalesOrder.FirstClassStationCode,
                            FirstClassStationName = partsSalesOrder.FirstClassStationName,
                            PartsSalesOrderTypeId = partsSalesOrderType != null ? partsSalesOrderType.Id : 0,
                            PartsSalesOrderTypeName = partsSalesOrderType != null ? partsSalesOrderType.Name : "",
                            IfAgencyService = partsSalesOrder.IfAgencyService,
                            SalesActivityDiscountRate = partsSalesOrder.SalesActivityDiscountRate,
                            SalesActivityDiscountAmount = partsSalesOrder.SalesActivityDiscountAmount,
                            RequestedShippingTime = partsSalesOrder.RequestedShippingTime,
                            RequestedDeliveryTime = partsSalesOrder.RequestedDeliveryTime,
                            IfDirectProvision = partsSalesOrder.IfDirectProvision,
                            CustomerType = partsSalesOrder.CustomerType,
                            SecondLevelOrderType = partsSalesOrder.SecondLevelOrderType,
                            Remark = partsSalesOrder.Remark,
                            ContactPerson = partsSalesOrder.ContactPerson,
                            ContactPhone = partsSalesOrder.ContactPhone,
                            ReceivingCompanyId = partsSalesOrder.ReceivingCompanyId,
                            ReceivingCompanyCode = partsSalesOrder.ReceivingCompanyCode,
                            ReceivingCompanyName = partsSalesOrder.ReceivingCompanyName,
                            ShippingMethod = (int)DcsPartsShippingMethod.公路物流,
                            ReceivingWarehouseId = partsSalesOrder.ReceivingWarehouseId,
                            ReceivingWarehouseName = partsSalesOrder.ReceivingWarehouseName,
                            ReceivingAddress = partsSalesOrder.ReceivingAddress,
                            OriginalRequirementBillId = partsSalesOrder.Id,
                            OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.配件销售订单,
                            WarehouseId = warehouse != null ? warehouse.Id : 0,
                            ERPSourceOrderCode = partsSalesOrder.ERPSourceOrderCode,
                            ERPOrderCode = partsSalesOrder.ERPOrderCode,
                            Status = (int)DcsPartsSalesOrderStatus.提交
                        };

                        foreach(var partsSalesOrderDetail in partsSalesOrder.PartsSalesOrderDetails) {
                            var newpartsSalesOrderDetail = new PartsSalesOrderDetail {
                                SparePartId = partsSalesOrderDetail.SparePartId,
                                SparePartCode = partsSalesOrderDetail.SparePartCode,
                                SparePartName = partsSalesOrderDetail.SparePartName,
                                MeasureUnit = partsSalesOrderDetail.MeasureUnit,
                                OrderedQuantity = partsSalesOrderDetail.OrderedQuantity,
                                OriginalPrice = partsSalesOrderDetail.OriginalPrice,
                                CustOrderPriceGradeCoefficient = partsSalesOrderDetail.CustOrderPriceGradeCoefficient,
                                IfCanNewPart = partsSalesOrderDetail.IfCanNewPart,
                                DiscountedPrice = partsSalesOrderDetail.DiscountedPrice,
                                OrderPrice = partsSalesOrderDetail.OrderPrice,
                                OrderSum = partsSalesOrderDetail.OrderedQuantity * partsSalesOrderDetail.OrderPrice,
                                EstimatedFulfillTime = partsSalesOrderDetail.EstimatedFulfillTime,
                                Remark = partsSalesOrderDetail.Remark,
                                PriceTypeName = partsSalesOrderDetail.PriceTypeName,
                                ABCStrategy = partsSalesOrderDetail.ABCStrategy
                            };
                            newPartsSalesOrder.PartsSalesOrderDetails.Add(newpartsSalesOrderDetail);
                        }
                        newPartsSalesOrder.TotalAmount = newPartsSalesOrder.PartsSalesOrderDetails.Sum(r => r.OrderSum);

                        new PartsSalesOrderAch(this.DomainService).InsertPartsSalesOrderValidate1(newPartsSalesOrder);
                        InsertToDatabase(newPartsSalesOrder);
                    } else {
                        throw new ValidationException("未找到对应的销售订单,请确认");
                    }
                }
            }
            ObjectContext.SaveChanges();
        }
       
        public void 销售订单退货终审(PartsSalesReturnBill partsSalesReturnBill) {
            var userInfo = Utils.GetCurrentUserInfo();
            partsSalesReturnBill.ModifierId = userInfo.Id;
            partsSalesReturnBill.ModifierName = userInfo.Name;
            partsSalesReturnBill.ModifyTime = DateTime.Now;
            partsSalesReturnBill.FinalApproverId = userInfo.Id;
            partsSalesReturnBill.FinalApproverName = userInfo.Name;
            partsSalesReturnBill.FinalApproveTime = DateTime.Now;
            partsSalesReturnBill.Status = (int)DcsPartsSalesReturnBillStatus.审批通过;
            //当填写折扣时，重新计算金额
            if(null != partsSalesReturnBill.DiscountRate) {
                this.upDataByDiscountRate(partsSalesReturnBill);
            }
            UpdateToDatabase(partsSalesReturnBill);
            if(partsSalesReturnBill.ReturnType == (int)DcsPartsSalesReturn_ReturnType.特殊退货) {
                var partsSalesReturnBillDetails = partsSalesReturnBill.PartsSalesReturnBillDetails.OrderByDescending(r => r.PartsSalesOrderId).ToList();
                var company = ObjectContext.Companies.SingleOrDefault(r => r.Id == partsSalesReturnBill.ReturnCompanyId && r.Status == (int)DcsMasterDataStatus.有效);
                if(company == null)
                    throw new ValidationException(ErrorStrings.PartsSalesReturnBill_Validation5);
                var receCompany=ObjectContext.Companies.SingleOrDefault(t=>t.Id==partsSalesReturnBill.SalesUnitOwnerCompanyId && t.Status == (int)DcsMasterDataStatus.有效);
                if(receCompany == null)
                    throw new ValidationException("销售退货对应的收货单位不存在");
                var dbSalesUnit = ObjectContext.SalesUnits.SingleOrDefault(r => r.Id == partsSalesReturnBill.SalesUnitId && r.Status != (int)DcsMasterDataStatus.作废);
                if(dbSalesUnit == null)
                    throw new ValidationException(ErrorStrings.PartsSalesReturnBill_Validation11);

                if (company.Type == (int)DcsCompanyType.代理库 || company.Type == (int)DcsCompanyType.服务站兼代理库) {
                    if (partsSalesReturnBillDetails.All(r => !r.ApproveQuantity.HasValue || r.ApproveQuantity.Value == 0))
                        throw new ValidationException(ErrorStrings.PartsSalesReturnBill_Validation6);

                    if (!partsSalesReturnBill.ReturnWarehouseId.HasValue) {
                        throw new ValidationException("代理库退货提报，退货仓库应必填!");
                    }
                    var returnWarehouse = ObjectContext.Warehouses.SingleOrDefault(r => r.Id == partsSalesReturnBill.ReturnWarehouseId);

                    var partsOutboundPlan = new PartsOutboundPlan {
                        WarehouseId = partsSalesReturnBill.ReturnWarehouseId.Value,
                        WarehouseCode = partsSalesReturnBill.ReturnWarehouseCode,
                        WarehouseName = partsSalesReturnBill.ReturnWarehouseName,
                        StorageCompanyId = partsSalesReturnBill.ReturnCompanyId,
                        StorageCompanyCode = partsSalesReturnBill.ReturnCompanyCode,
                        StorageCompanyName = partsSalesReturnBill.ReturnCompanyName,
                        CustomerAccountId = partsSalesReturnBill.CustomerAccountId,
                        StorageCompanyType = (int)DcsCompanyType.代理库,
                        BranchId = partsSalesReturnBill.BranchId,
                        BranchCode = partsSalesReturnBill.BranchCode,
                        BranchName = partsSalesReturnBill.BranchName,
                        CounterpartCompanyId = partsSalesReturnBill.SalesUnitOwnerCompanyId,
                        CounterpartCompanyCode = partsSalesReturnBill.SalesUnitOwnerCompanyCode,
                        CounterpartCompanyName = partsSalesReturnBill.SalesUnitOwnerCompanyName,
                        ReceivingCompanyId = partsSalesReturnBill.SalesUnitOwnerCompanyId,
                        ReceivingCompanyCode = partsSalesReturnBill.SalesUnitOwnerCompanyCode,
                        ReceivingCompanyName = partsSalesReturnBill.SalesUnitOwnerCompanyName,
                        ReceivingWarehouseId = partsSalesReturnBill.WarehouseId,
                        ReceivingWarehouseCode = partsSalesReturnBill.WarehouseCode,
                        ReceivingWarehouseName = partsSalesReturnBill.WarehouseName,
                        OriginalRequirementBillId = partsSalesReturnBill.Id,
                        OriginalRequirementBillCode = partsSalesReturnBill.Code,
                        OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.配件销售退货单,
                        SourceId = partsSalesReturnBill.Id,
                        SourceCode = partsSalesReturnBill.Code,
                        OutboundType = (int)DcsPartsOutboundType.采购退货,
                        Status = (int)DcsPartsOutboundPlanStatus.新建,
                        PartsSalesCategoryId = dbSalesUnit.PartsSalesCategoryId,
                        IfWmsInterface = returnWarehouse.WmsInterface,
                        ERPSourceOrderCode = partsSalesReturnBill.ERPSourceOrderCode
                    };
                    var partIds = partsSalesReturnBillDetails.Select(r => r.SparePartId).ToArray();
                    //校验代理库库存
                    //获取清单中配件的可用库存
                    var warehousePartsStocks = DomainService.查询仓库库存(partsSalesReturnBill.ReturnCompanyId, partsSalesReturnBill.ReturnWarehouseId, partIds).ToList();
                    if (warehousePartsStocks.Count <= 0) {
                        throw new ValidationException(ErrorStrings.WarehousePartsStock_Validation1);
                    }
                    foreach (var partsSalesReturnBillDetail in partsSalesReturnBillDetails) {
                        if (!partsSalesReturnBillDetail.ApproveQuantity.HasValue)
                            throw new ValidationException(string.Format(ErrorStrings.PartsSalesReturnBill_Validation8, partsSalesReturnBillDetail.SparePartCode));
                        if (partsSalesReturnBillDetail.ApproveQuantity == 0)
                            continue;
                        //校验退货量是否大于可用库存 
                        //当可用库存小于审核数量时，赋值可用数量
                        var checkPartsStock = warehousePartsStocks.SingleOrDefault(r => r.SparePartId == partsSalesReturnBillDetail.SparePartId);
                        if (checkPartsStock == null) {
                            throw new ValidationException(string.Format("配件编号为{0}的库存不存在", partsSalesReturnBillDetail.SparePartCode));
                        }

                        if (checkPartsStock.UsableQuantity < partsSalesReturnBillDetail.ApproveQuantity.Value) {
                            if (checkPartsStock.UsableQuantity > 0) {
                                partsSalesReturnBillDetail.ApproveQuantity = checkPartsStock.UsableQuantity;
                                checkPartsStock.UsableQuantity = 0;
                            } else {
                                partsSalesReturnBillDetail.ApproveQuantity = 0;
                                continue;
                            }
                        } else {
                            checkPartsStock.UsableQuantity = checkPartsStock.UsableQuantity - partsSalesReturnBillDetail.ApproveQuantity.Value;
                        }

                        var PartsOutboundPlanDetail = partsOutboundPlan.PartsOutboundPlanDetails.FirstOrDefault(r => r.SparePartId == partsSalesReturnBillDetail.SparePartId && r.Price == partsSalesReturnBillDetail.ReturnPrice);
                        if (PartsOutboundPlanDetail == null) {
                            partsOutboundPlan.PartsOutboundPlanDetails.Add(new PartsOutboundPlanDetail {
                                SparePartId = partsSalesReturnBillDetail.SparePartId,
                                SparePartCode = partsSalesReturnBillDetail.SparePartCode,
                                SparePartName = partsSalesReturnBillDetail.SparePartName,
                                PlannedAmount = partsSalesReturnBillDetail.ApproveQuantity.Value,
                                Price = partsSalesReturnBillDetail.ReturnPrice,
                                OriginalPrice = partsSalesReturnBillDetail.OriginalPrice
                            });
                        } else {
                            PartsOutboundPlanDetail.PlannedAmount += partsSalesReturnBillDetail.ApproveQuantity.Value;
                        }
                    }
                    if (partsSalesReturnBillDetails.Any(r => r.ApproveQuantity.HasValue && r.ApproveQuantity.Value > 0))
                        DomainService.生成配件出库计划(partsOutboundPlan);
                }

                if(company.Type == (int)DcsCompanyType.服务站) {
                    var partsIds = partsSalesReturnBill.PartsSalesReturnBillDetails.Select(r => r.SparePartId).ToArray();
                    var dealerPartsStocks = ObjectContext.DealerPartsStocks.Where(r => r.SubDealerId == -1 && r.DealerId == partsSalesReturnBill.ReturnCompanyId && partsIds.Contains(r.SparePartId) && r.SalesCategoryId == dbSalesUnit.PartsSalesCategoryId).ToArray();
                    if(!partsSalesReturnBill.WarehouseId.HasValue || string.IsNullOrEmpty(partsSalesReturnBill.WarehouseCode) || string.IsNullOrEmpty(partsSalesReturnBill.WarehouseName))
                        throw new ValidationException(ErrorStrings.PartsSalesReturnBill_Validation7);
                    var dbcompany = ObjectContext.Companies.SingleOrDefault(r => r.Id == partsSalesReturnBill.SalesUnitOwnerCompanyId && r.Status == (int)DcsMasterDataStatus.有效);
                    if(dbcompany == null)
                        throw new ValidationException(ErrorStrings.PartsSalesReturnBill_Validation9);
                    var partsInboundPlan = new PartsInboundPlan {
                        WarehouseId = partsSalesReturnBill.WarehouseId.Value,
                        WarehouseCode = partsSalesReturnBill.WarehouseCode,
                        WarehouseName = partsSalesReturnBill.WarehouseName,
                        StorageCompanyId = partsSalesReturnBill.SalesUnitOwnerCompanyId,
                        StorageCompanyCode = partsSalesReturnBill.SalesUnitOwnerCompanyCode,
                        StorageCompanyName = partsSalesReturnBill.SalesUnitOwnerCompanyName,
                        StorageCompanyType = dbcompany.Type,
                        BranchId = partsSalesReturnBill.BranchId,
                        BranchCode = partsSalesReturnBill.BranchCode,
                        BranchName = partsSalesReturnBill.BranchName,
                        CounterpartCompanyId = partsSalesReturnBill.ReturnCompanyId,
                        CounterpartCompanyCode = partsSalesReturnBill.ReturnCompanyCode,
                        CounterpartCompanyName = partsSalesReturnBill.ReturnCompanyName,
                        SourceId = partsSalesReturnBill.Id,
                        SourceCode = partsSalesReturnBill.Code,
                        InboundType = (int)DcsPartsInboundType.销售退货,
                        CustomerAccountId = partsSalesReturnBill.CustomerAccountId,
                        OriginalRequirementBillId = partsSalesReturnBill.Id,
                        OriginalRequirementBillCode = partsSalesReturnBill.Code,
                        OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.配件销售退货单,
                        Status = (int)DcsPartsInboundPlanStatus.新建,
                        PartsSalesCategoryId = dbSalesUnit.PartsSalesCategoryId,
                        ERPSourceOrderCode = partsSalesReturnBill.ERPSourceOrderCode
                    };
                    foreach(var partsSalesReturnBillDetail in partsSalesReturnBillDetails) {
                        if(!partsSalesReturnBillDetail.ApproveQuantity.HasValue)
                            throw new ValidationException(string.Format(ErrorStrings.PartsSalesReturnBill_Validation8, partsSalesReturnBillDetail.SparePartCode));
                        if(partsSalesReturnBillDetail.ApproveQuantity == 0)
                            continue;

                        var dealerPartsStock = dealerPartsStocks.FirstOrDefault(r => r.SparePartId == partsSalesReturnBillDetail.SparePartId);
                        if(dealerPartsStock == null)
                            throw new ValidationException("经销商配件库存不存在");
                        //if (dealerPartsStock.Quantity <= 0) {
                        //    throw new ValidationException(string.Format("配件编号为{0}的经销商配件库存为0", dealerPartsStock.SparePartCode));
                        //}
                        if (dealerPartsStock.Quantity < partsSalesReturnBillDetail.ApproveQuantity.Value) {
                            if (dealerPartsStock.Quantity <= 0) {
                                partsSalesReturnBillDetail.ApproveQuantity = 0;
                                continue;
                            } else {
                                partsSalesReturnBillDetail.ApproveQuantity = dealerPartsStock.Quantity;
                            }
                        }
                        dealerPartsStock.Quantity -= partsSalesReturnBillDetail.ApproveQuantity.Value;
                        UpdateToDatabase(dealerPartsStock);
                        new DealerPartsStockAch(this.DomainService).UpdateDealerPartsStockValidate(dealerPartsStock);

                        var partsInboundPlanDetail = partsInboundPlan.PartsInboundPlanDetails.FirstOrDefault(o => o.SparePartId == partsSalesReturnBillDetail.SparePartId && o.Price == partsSalesReturnBillDetail.ReturnPrice);
                        if (partsInboundPlanDetail != null) {
                            partsInboundPlanDetail.PlannedAmount += partsSalesReturnBillDetail.ApproveQuantity.Value;
                        } else {
                            partsInboundPlan.PartsInboundPlanDetails.Add(new PartsInboundPlanDetail {
                                SparePartId = partsSalesReturnBillDetail.SparePartId,
                                SparePartCode = partsSalesReturnBillDetail.SparePartCode,
                                SparePartName = partsSalesReturnBillDetail.SparePartName,
                                PlannedAmount = partsSalesReturnBillDetail.ApproveQuantity.Value,
                                Price = partsSalesReturnBillDetail.ReturnPrice,
                                POCode = partsSalesReturnBillDetail.SettlementPurchaseOrder,
                                OriginalPrice = partsSalesReturnBillDetail.OriginalPrice
                            });
                        }
                    }
                    if (partsSalesReturnBillDetails.Any(r => r.ApproveQuantity.HasValue && r.ApproveQuantity > 0)) {
                        InsertToDatabase(partsInboundPlan);
                        new PartsInboundPlanAch(this.DomainService).InsertPartsInboundPlanValidate(partsInboundPlan);
                        ObjectContext.SaveChanges();
                        //记录追溯信息
                        var inTraces = ObjectContext.AccurateTraces.Where(t => t.CompanyId == partsSalesReturnBill.ReturnCompanyId && t.Type == (int)DCSAccurateTraceType.入库 && (t.OutQty == null || t.InQty != t.OutQty) && t.Status == (int)DCSAccurateTraceStatus.有效).ToArray();
                        var abandInTrace = ObjectContext.AccurateTraces.Where(t => t.CompanyId == receCompany.Id && t.Type == (int)DCSAccurateTraceType.入库 && (t.InQty == (t.OutQty ?? 0) && t.InQty > 0)).ToArray();
                        //出库
                        var tracOldOut = ObjectContext.AccurateTraces.Where(t => t.Status == (int)DCSAccurateTraceStatus.有效 && t.CompanyId == partsSalesReturnBill.ReturnCompanyId && t.Type == (int)DCSAccurateTraceType.出库 && t.SIHLabelCode != null).ToArray();
                        foreach(var item in partsSalesReturnBill.PartsSalesReturnBillDetails) {
                            if(!string.IsNullOrEmpty(item.SIHLabelCode)) {
                                var traceQty = 0;                               
                                var traces = item.SIHLabelCode.Split(new Char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                                foreach(var code in traces) {
                                    if(!item.TraceProperty.HasValue) {
                                        item.TraceProperty = 0;
                                   }
                                    var inTrace = inTraces.Where(t => t.BoxCode == code).ToArray();                                   
                                    if(code.EndsWith("J"))
                                        inTrace = inTraces.Where(t => t.SIHLabelCode == code).ToArray();
                                    if(inTrace.Count() == 0) {
                                        throw new ValidationException(code + "标签码已出库完成");
                                    }
                                    foreach(var trace in inTrace) {
                                        if(traceQty == item.ReturnedQuantity) {
                                            continue;
                                        }
                                        var currentQty = 0;
                                        if(item.TraceProperty.Value == (int)DCSTraceProperty.精确追溯) {
                                            traceQty++;
                                            currentQty = 1;
                                        } else {
                                            if(((trace.InQty - (trace.OutQty ?? 0))) > (item.ReturnedQuantity - traceQty)) {
                                                traceQty = item.ReturnedQuantity;
                                                currentQty = item.ReturnedQuantity - traceQty;
                                            } else {
                                                traceQty = traceQty + (trace.InQty.Value - (trace.OutQty ?? 0));
                                                currentQty = (trace.InQty.Value - (trace.OutQty ?? 0));
                                            }
                                        }
                                        var newAccurateTrace = new AccurateTrace {
                                            Type = (int)DCSAccurateTraceType.入库,
                                            CompanyType = receCompany.Type == (int)DcsCompanyType.分公司 ? (int)DCSAccurateTraceCompanyType.配件中心 : (int)DCSAccurateTraceCompanyType.中心库,
                                            CompanyId = receCompany.Id,
                                            SourceBillId = partsInboundPlan.Id,
                                            PartId = item.SparePartId,
                                            TraceCode = trace.TraceCode,
                                            SIHLabelCode = trace.SIHLabelCode,
                                            Status = (int)DCSAccurateTraceStatus.有效,
                                            TraceProperty = trace.TraceProperty,
                                            BoxCode = trace.BoxCode,
                                            InQty = currentQty
                                        };
                                        if(receCompany.Type == (int)DcsCompanyType.分公司) {
                                            newAccurateTrace.BoxCode = null;
                                            newAccurateTrace.SIHLabelCode = null;
                                            newAccurateTrace.InQty = null;
                                        }
                                        new AccurateTraceAch(this.DomainService).ValidataAccurateTrace(newAccurateTrace);
                                        if(!string.IsNullOrWhiteSpace(trace.TraceCode)) {
                                            var aband = abandInTrace.Where(t => t.TraceCode == trace.TraceCode).FirstOrDefault();
                                            if(aband != null) {
                                                aband.Status = (int)DCSAccurateTraceStatus.作废;
                                                aband.ModifierId = userInfo.Id;
                                                aband.ModifierName = userInfo.Name;
                                                aband.ModifyTime = DateTime.Now;
                                                UpdateToDatabase(aband);
                                            }
                                        }
                                       
                                        var outAccurateTrace = new AccurateTrace {
                                            Type = (int)DCSAccurateTraceType.出库,
                                            CompanyType = (int)DCSAccurateTraceCompanyType.服务站,
                                            CompanyId = company.Id,
                                            SourceBillId = partsSalesReturnBill.Id,
                                            PartId = item.SparePartId,
                                            TraceCode = trace.TraceCode,
                                            SIHLabelCode = trace.SIHLabelCode,
                                            Status = (int)DCSAccurateTraceStatus.有效,
                                            TraceProperty = trace.TraceProperty,
                                            BoxCode = trace.BoxCode,
                                            InQty = currentQty,
                                            OutQty = currentQty
                                        };
                                        new AccurateTraceAch(this.DomainService).ValidataAccurateTrace(outAccurateTrace);
                                        trace.OutQty = trace.OutQty ?? 0 + currentQty;
                                        trace.ModifierId = userInfo.Id;
                                        trace.ModifierName = userInfo.Name;
                                        trace.ModifyTime = DateTime.Now;
                                        UpdateToDatabase(trace);
                                        if(item.TraceProperty.Value == (int)DCSTraceProperty.精确追溯 &&string.IsNullOrEmpty(trace.TraceCode)) {
                                            var outTraceLs = tracOldOut.Where(t => t.SourceBillId != partsSalesReturnBill.Id && t.TraceCode == trace.TraceCode && t.Status == (int)DCSAccurateTraceStatus.有效).ToArray();
                                            foreach(var outTrace in outTraceLs) {
                                                outTrace.ModifierId = userInfo.Id;
                                                outTrace.ModifierName = userInfo.Name;
                                                outTrace.ModifyTime = DateTime.Now;
                                                outTrace.Status = (int)DCSAccurateTraceStatus.作废;
                                                UpdateToDatabase(outTrace);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                //集团客户和服务站一样。只是不扣库存
                if(company.Type == (int)DcsCompanyType.集团企业) {
                    var partsIds = partsSalesReturnBill.PartsSalesReturnBillDetails.Select(r => r.SparePartId).ToArray();
                    var dealerPartsStocks = ObjectContext.DealerPartsStocks.Where(r => r.SubDealerId == -1 && r.DealerId == partsSalesReturnBill.ReturnCompanyId && partsIds.Contains(r.SparePartId) && r.SalesCategoryId == dbSalesUnit.PartsSalesCategoryId).ToArray();
                    if(!partsSalesReturnBill.WarehouseId.HasValue || string.IsNullOrEmpty(partsSalesReturnBill.WarehouseCode) || string.IsNullOrEmpty(partsSalesReturnBill.WarehouseName))
                        throw new ValidationException(ErrorStrings.PartsSalesReturnBill_Validation7);
                    var dbcompany = ObjectContext.Companies.SingleOrDefault(r => r.Id == partsSalesReturnBill.SalesUnitOwnerCompanyId && r.Status == (int)DcsMasterDataStatus.有效);
                    if(dbcompany == null)
                        throw new ValidationException(ErrorStrings.PartsSalesReturnBill_Validation9);
                    var partsInboundPlan = new PartsInboundPlan {
                        WarehouseId = partsSalesReturnBill.WarehouseId.Value,
                        WarehouseCode = partsSalesReturnBill.WarehouseCode,
                        WarehouseName = partsSalesReturnBill.WarehouseName,
                        StorageCompanyId = partsSalesReturnBill.SalesUnitOwnerCompanyId,
                        StorageCompanyCode = partsSalesReturnBill.SalesUnitOwnerCompanyCode,
                        StorageCompanyName = partsSalesReturnBill.SalesUnitOwnerCompanyName,
                        StorageCompanyType = dbcompany.Type,
                        BranchId = partsSalesReturnBill.BranchId,
                        BranchCode = partsSalesReturnBill.BranchCode,
                        BranchName = partsSalesReturnBill.BranchName,
                        CounterpartCompanyId = partsSalesReturnBill.ReturnCompanyId,
                        CounterpartCompanyCode = partsSalesReturnBill.ReturnCompanyCode,
                        CounterpartCompanyName = partsSalesReturnBill.ReturnCompanyName,
                        SourceId = partsSalesReturnBill.Id,
                        SourceCode = partsSalesReturnBill.Code,
                        InboundType = (int)DcsPartsInboundType.销售退货,
                        CustomerAccountId = partsSalesReturnBill.CustomerAccountId,
                        OriginalRequirementBillId = partsSalesReturnBill.Id,
                        OriginalRequirementBillCode = partsSalesReturnBill.Code,
                        OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.配件销售退货单,
                        Status = (int)DcsPartsInboundPlanStatus.新建,
                        PartsSalesCategoryId = dbSalesUnit.PartsSalesCategoryId,
                        ERPSourceOrderCode = partsSalesReturnBill.ERPSourceOrderCode
                    };
                    foreach(var partsSalesReturnBillDetail in partsSalesReturnBillDetails) {
                        if(!partsSalesReturnBillDetail.ApproveQuantity.HasValue)
                            throw new ValidationException(string.Format(ErrorStrings.PartsSalesReturnBill_Validation8, partsSalesReturnBillDetail.SparePartCode));
                        if(partsSalesReturnBillDetail.ApproveQuantity == 0)
                            continue;
                        var partsInboundPlanDetail = partsInboundPlan.PartsInboundPlanDetails.FirstOrDefault(o => o.SparePartId == partsSalesReturnBillDetail.SparePartId && o.Price == partsSalesReturnBillDetail.ReturnPrice);
                        if (partsInboundPlanDetail != null) {
                            partsInboundPlanDetail.PlannedAmount += partsSalesReturnBillDetail.ApproveQuantity.Value;
                        } else {
                            partsInboundPlan.PartsInboundPlanDetails.Add(new PartsInboundPlanDetail {
                                SparePartId = partsSalesReturnBillDetail.SparePartId,
                                SparePartCode = partsSalesReturnBillDetail.SparePartCode,
                                SparePartName = partsSalesReturnBillDetail.SparePartName,
                                PlannedAmount = partsSalesReturnBillDetail.ApproveQuantity.Value,
                                Price = partsSalesReturnBillDetail.ReturnPrice,
                                POCode = partsSalesReturnBillDetail.SettlementPurchaseOrder,
                                OriginalPrice = partsSalesReturnBillDetail.OriginalPrice
                            });
                        }
                    }
                    InsertToDatabase(partsInboundPlan);
                    new PartsInboundPlanAch(this.DomainService).InsertPartsInboundPlanValidate(partsInboundPlan);
                    ObjectContext.SaveChanges();
                    //记录追溯信息
                    var inTraces = ObjectContext.AccurateTraces.Where(t => t.CompanyId == partsSalesReturnBill.ReturnCompanyId && t.Type == (int)DCSAccurateTraceType.入库 && (t.OutQty == null || t.InQty != t.OutQty) && t.Status == (int)DCSAccurateTraceStatus.有效).ToArray();
                    var abandInTrace = ObjectContext.AccurateTraces.Where(t => t.CompanyId == receCompany.Id && t.Type == (int)DCSAccurateTraceType.入库 && (t.InQty == (t.OutQty ?? 0) && t.InQty > 0)).ToArray();
                    //出库
                    var tracOldOut = ObjectContext.AccurateTraces.Where(t => t.Status == (int)DCSAccurateTraceStatus.有效 && t.CompanyId == partsSalesReturnBill.ReturnCompanyId && t.Type == (int)DCSAccurateTraceType.出库 && t.SIHLabelCode != null).ToArray();
                    foreach(var item in partsSalesReturnBill.PartsSalesReturnBillDetails) {
                        if(item.TraceProperty.HasValue && !string.IsNullOrEmpty(item.SIHLabelCode)) {
                            var traceQty = 0;
                            var traces = item.SIHLabelCode.Split(new Char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                            foreach(var code in traces) {
                                
                                var inTrace = inTraces.Where(t => t.BoxCode == code).ToArray();
                                if(code.EndsWith("J"))
                                    inTrace = inTraces.Where(t => t.SIHLabelCode == code).ToArray();
                                if(inTrace.Count() == 0) {
                                    throw new ValidationException(code + "标签码已出库完成");
                                }
                                foreach(var trace in inTrace) {
                                    if(traceQty == item.ReturnedQuantity) {
                                        continue;
                                    }
                                    var currentQty = 0;
                                    if(item.TraceProperty.Value == (int)DCSTraceProperty.精确追溯) {
                                        traceQty++;
                                        currentQty = 1;
                                    } else {
                                        if(((trace.InQty - (trace.OutQty ?? 0))) > (item.ReturnedQuantity - traceQty)) {
                                            traceQty = item.ReturnedQuantity;
                                            currentQty = item.ReturnedQuantity - traceQty;
                                        } else {
                                            traceQty = traceQty + (trace.InQty.Value - (trace.OutQty ?? 0));
                                            currentQty = (trace.InQty.Value - (trace.OutQty ?? 0));
                                        }
                                    }
                                    var newAccurateTrace = new AccurateTrace {
                                        Type = (int)DCSAccurateTraceType.入库,
                                        CompanyType = receCompany.Type == (int)DcsCompanyType.分公司 ? (int)DCSAccurateTraceCompanyType.配件中心 : (int)DCSAccurateTraceCompanyType.中心库,
                                        CompanyId = receCompany.Id,
                                        SourceBillId = partsInboundPlan.Id,
                                        PartId = item.SparePartId,
                                        TraceCode = trace.TraceCode,
                                        SIHLabelCode = trace.SIHLabelCode,
                                        Status = (int)DCSAccurateTraceStatus.有效,
                                        TraceProperty = trace.TraceProperty,
                                        BoxCode = trace.BoxCode,
                                        InQty = currentQty
                                    };
                                    if(receCompany.Type == (int)DcsCompanyType.分公司) {
                                        newAccurateTrace.BoxCode = null;
                                        newAccurateTrace.SIHLabelCode = null;
                                        newAccurateTrace.InQty = null;
                                    }
                                    new AccurateTraceAch(this.DomainService).ValidataAccurateTrace(newAccurateTrace);
                                    var aband = abandInTrace.Where(t => t.TraceCode == trace.TraceCode).FirstOrDefault();
                                    if(aband != null) {
                                        aband.Status = (int)DCSAccurateTraceStatus.作废;
                                        aband.ModifierId = userInfo.Id;
                                        aband.ModifierName = userInfo.Name;
                                        aband.ModifyTime = DateTime.Now;
                                        UpdateToDatabase(aband);
                                    }
                                    var outAccurateTrace = new AccurateTrace {
                                        Type = (int)DCSAccurateTraceType.出库,
                                        CompanyType = (int)DCSAccurateTraceCompanyType.服务站,
                                        CompanyId = company.Id,
                                        SourceBillId = partsSalesReturnBill.Id,
                                        PartId = item.SparePartId,
                                        TraceCode = trace.TraceCode,
                                        SIHLabelCode = trace.SIHLabelCode,
                                        Status = (int)DCSAccurateTraceStatus.有效,
                                        TraceProperty = trace.TraceProperty,
                                        BoxCode = trace.BoxCode,
                                        InQty = currentQty,
                                        OutQty = currentQty
                                    };
                                    new AccurateTraceAch(this.DomainService).ValidataAccurateTrace(outAccurateTrace);
                                    trace.OutQty = trace.OutQty ?? 0 + currentQty;
                                    trace.ModifierId = userInfo.Id;
                                    trace.ModifierName = userInfo.Name;
                                    trace.ModifyTime = DateTime.Now;
                                    UpdateToDatabase(trace);
                                    if(item.TraceProperty.Value == (int)DCSTraceProperty.精确追溯) {
                                        var outTraceLs = tracOldOut.Where(t => t.SourceBillId != partsSalesReturnBill.Id && t.TraceCode == trace.TraceCode  && t.Status == (int)DCSAccurateTraceStatus.有效).ToArray();
                                        foreach(var outTrace in outTraceLs) {
                                            outTrace.ModifierId = userInfo.Id;
                                            outTrace.ModifierName = userInfo.Name;
                                            outTrace.ModifyTime = DateTime.Now;
                                            outTrace.Status = (int)DCSAccurateTraceStatus.作废;
                                            UpdateToDatabase(outTrace);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                //2“是否换货”字段为“是”的销售退货单审核通过的同时，生成同种配件图号、同样价格和数量的“提交”状态的销售订单，将退货单中的“平台单号”赋值给销售订单的“平台单号”字段
                //根据销售退货单的销售订单号 生成新的销售订单
                if(partsSalesReturnBill.IsBarter == true) {
                    foreach(var salesReturnDetail in partsSalesReturnBill.PartsSalesReturnBillDetails) {
                        var partsSalesOrder = this.ObjectContext.PartsSalesOrders.Where(r => r.Code == salesReturnDetail.PartsSalesOrderCode).FirstOrDefault();
                        var partsSalesOrderType = this.ObjectContext.PartsSalesOrderTypes.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.BranchId == partsSalesReturnBill.BranchId && r.Name == "急需订单").FirstOrDefault();
                        var warehouse = this.ObjectContext.Warehouses.Where(r => r.BranchId == partsSalesReturnBill.BranchId && r.Name == "随车行北京CDC仓库" && r.Status == (int)DcsBaseDataStatus.有效).FirstOrDefault();
                        if(partsSalesOrder != null) {
                            var newPartsSalesOrder = new PartsSalesOrder {
                                BranchId = partsSalesOrder.BranchId,
                                BranchCode = partsSalesOrder.BranchCode,
                                BranchName = partsSalesOrder.BranchName,
                                SalesCategoryId = partsSalesOrder.SalesCategoryId,
                                SalesCategoryName = partsSalesOrder.SalesCategoryName,
                                SalesUnitId = partsSalesOrder.SalesUnitId,
                                SalesUnitCode = partsSalesOrder.SalesUnitCode,
                                SalesUnitName = partsSalesOrder.SalesUnitName,
                                SalesUnitOwnerCompanyId = partsSalesOrder.SalesUnitOwnerCompanyId,
                                SalesUnitOwnerCompanyCode = partsSalesOrder.SalesUnitOwnerCompanyCode,
                                SalesUnitOwnerCompanyName = partsSalesOrder.SalesUnitOwnerCompanyName,
                                SourceBillCode = partsSalesOrder.SourceBillCode,
                                SourceBillId = partsSalesOrder.SourceBillId,
                                CustomerAccountId = partsSalesOrder.Id,
                                IsDebt = partsSalesOrder.IsDebt,
                                InvoiceReceiveCompanyId = partsSalesOrder.InvoiceReceiveCompanyId,
                                InvoiceReceiveCompanyCode = partsSalesOrder.InvoiceReceiveCompanyCode,
                                InvoiceReceiveCompanyName = partsSalesOrder.InvoiceReceiveCompanyName,
                                InvoiceReceiveCompanyType = partsSalesOrder.InvoiceReceiveCompanyType,
                                InvoiceReceiveSaleCateId = partsSalesOrder.InvoiceReceiveSaleCateId,
                                InvoiceReceiveSaleCateName = partsSalesOrder.InvoiceReceiveSaleCateName,
                                SubmitCompanyId = partsSalesOrder.SubmitCompanyId,
                                SubmitCompanyCode = partsSalesOrder.SubmitCompanyCode,
                                SubmitCompanyName = partsSalesOrder.SubmitCompanyName,
                                ProvinceID = partsSalesOrder.ProvinceID,
                                Province = partsSalesOrder.Province,
                                City = partsSalesOrder.City,
                                FirstClassStationId = partsSalesOrder.FirstClassStationId,
                                FirstClassStationCode = partsSalesOrder.FirstClassStationCode,
                                FirstClassStationName = partsSalesOrder.FirstClassStationName,
                                PartsSalesOrderTypeId = partsSalesOrderType != null ? partsSalesOrderType.Id : 0,
                                PartsSalesOrderTypeName = partsSalesOrderType != null ? partsSalesOrderType.Name : "",
                                IfAgencyService = partsSalesOrder.IfAgencyService,
                                SalesActivityDiscountRate = partsSalesOrder.SalesActivityDiscountRate,
                                SalesActivityDiscountAmount = partsSalesOrder.SalesActivityDiscountAmount,
                                RequestedShippingTime = partsSalesOrder.RequestedShippingTime,
                                RequestedDeliveryTime = partsSalesOrder.RequestedDeliveryTime,
                                IfDirectProvision = partsSalesOrder.IfDirectProvision,
                                CustomerType = partsSalesOrder.CustomerType,
                                SecondLevelOrderType = partsSalesOrder.SecondLevelOrderType,
                                Remark = partsSalesOrder.Remark,
                                ContactPerson = partsSalesOrder.ContactPerson,
                                ContactPhone = partsSalesOrder.ContactPhone,
                                ReceivingCompanyId = partsSalesOrder.ReceivingCompanyId,
                                ReceivingCompanyCode = partsSalesOrder.ReceivingCompanyCode,
                                ReceivingCompanyName = partsSalesOrder.ReceivingCompanyName,
                                ShippingMethod = (int)DcsPartsShippingMethod.公路物流,
                                ReceivingWarehouseId = partsSalesOrder.ReceivingWarehouseId,
                                ReceivingWarehouseName = partsSalesOrder.ReceivingWarehouseName,
                                ReceivingAddress = partsSalesOrder.ReceivingAddress,
                                OriginalRequirementBillId = partsSalesOrder.Id,
                                OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.配件销售订单,
                                WarehouseId = warehouse != null ? warehouse.Id : 0,
                                ERPSourceOrderCode = partsSalesOrder.ERPSourceOrderCode,
                                ERPOrderCode = partsSalesOrder.ERPOrderCode,
                                Status = (int)DcsPartsSalesOrderStatus.提交
                            };

                            foreach(var partsSalesOrderDetail in partsSalesOrder.PartsSalesOrderDetails) {
                                var newpartsSalesOrderDetail = new PartsSalesOrderDetail {
                                    SparePartId = partsSalesOrderDetail.SparePartId,
                                    SparePartCode = partsSalesOrderDetail.SparePartCode,
                                    SparePartName = partsSalesOrderDetail.SparePartName,
                                    MeasureUnit = partsSalesOrderDetail.MeasureUnit,
                                    OrderedQuantity = partsSalesOrderDetail.OrderedQuantity,
                                    OriginalPrice = partsSalesOrderDetail.OriginalPrice,
                                    CustOrderPriceGradeCoefficient = partsSalesOrderDetail.CustOrderPriceGradeCoefficient,
                                    IfCanNewPart = partsSalesOrderDetail.IfCanNewPart,
                                    DiscountedPrice = partsSalesOrderDetail.DiscountedPrice,
                                    OrderPrice = partsSalesOrderDetail.OrderPrice,
                                    OrderSum = partsSalesOrderDetail.OrderedQuantity * partsSalesOrderDetail.OrderPrice,
                                    EstimatedFulfillTime = partsSalesOrderDetail.EstimatedFulfillTime,
                                    Remark = partsSalesOrderDetail.Remark,
                                    PriceTypeName = partsSalesOrderDetail.PriceTypeName,
                                    ABCStrategy = partsSalesOrderDetail.ABCStrategy
                                };
                                newPartsSalesOrder.PartsSalesOrderDetails.Add(newpartsSalesOrderDetail);
                            }

                            newPartsSalesOrder.TotalAmount = newPartsSalesOrder.PartsSalesOrderDetails.Sum(r => r.OrderSum);

                            new PartsSalesOrderAch(this.DomainService).InsertPartsSalesOrderValidate1(newPartsSalesOrder);
                            InsertToDatabase(newPartsSalesOrder);
                        } else {
                            throw new ValidationException("未找到对应的销售订单,请确认");
                        }
                    }
                    ObjectContext.SaveChanges();
                }
            }
        }
        public void upDataByDiscountRate(PartsSalesReturnBill partsSalesReturnBill) {
            var partsSalesReturnBillDetails = ObjectContext.PartsSalesReturnBillDetails.Where(r => r.PartsSalesReturnBillId == partsSalesReturnBill.Id).ToArray();
            //当填写折扣时
            //特殊退货不重新计算
            //退货总金额
            if(partsSalesReturnBill.ReturnType == (int)DcsPartsSalesReturn_ReturnType.特殊退货) {
                Decimal totalAmount = Decimal.Zero;
                foreach(var item in partsSalesReturnBillDetails) {
                    if(null != partsSalesReturnBill.DiscountRate) {
                        item.ReturnPrice = Math.Round(Decimal.Parse((item.OriginalOrderPrice * (partsSalesReturnBill.DiscountRate)).ToString()), 2);
                    }
                    totalAmount += item.ReturnPrice * item.ReturnedQuantity;
                }
                partsSalesReturnBill.TotalAmount = totalAmount;
            } else if(partsSalesReturnBill.ReturnType == (int)DcsPartsSalesReturn_ReturnType.正常退货){
                Decimal totalAmount = Decimal.Zero;
                foreach (var item in partsSalesReturnBillDetails) {
                    //if (null != partsSalesReturnBill.DiscountRate) { 
                    //    item.ReturnPrice = Decimal.Parse((item.OriginalOrderPrice * (partsSalesReturnBill.DiscountRate)).ToString());
                    //}
                    if(null != item.DiscountRate) {
                        item.ReturnPrice = Math.Round(Decimal.Parse((item.OriginalOrderPrice * Decimal.Parse(item.DiscountRate.ToString())).ToString()), 2);
                    }
                    totalAmount += item.ReturnPrice * item.ReturnedQuantity;
                }
                partsSalesReturnBill.TotalAmount = totalAmount;

            } else if (partsSalesReturnBill.ReturnType == (int)DcsPartsSalesReturn_ReturnType.保底退货) {
                Decimal totalAmount = Decimal.Zero;
                foreach (var item in partsSalesReturnBillDetails) {
                    //if (null != partsSalesReturnBill.DiscountRate) {
                    //    item.ReturnPrice = Decimal.Parse((item.OriginalOrderPrice * (partsSalesReturnBill.DiscountRate)).ToString());
                    //}
                    if(null != item.DiscountRate) {
                        item.ReturnPrice = Math.Round(Decimal.Parse((item.OriginalOrderPrice * Decimal.Parse(item.DiscountRate.ToString())).ToString()), 2);
                    }
                    totalAmount += item.ReturnPrice * item.ReturnedQuantity;
                }
                partsSalesReturnBill.TotalAmount = totalAmount;
            } else { 
                throw new ValidationException("未找到对应的退货类型");
            }

        }
        public void 初审不通过(PartsSalesReturnBill partsSalesReturnBill) {
            var userInfo = Utils.GetCurrentUserInfo();
            partsSalesReturnBill.ModifierId = userInfo.Id;
            partsSalesReturnBill.ModifierName = userInfo.Name;
            partsSalesReturnBill.ModifyTime = DateTime.Now;
            partsSalesReturnBill.InitialApproverId = null;
            partsSalesReturnBill.InitialApproverName = null;
            partsSalesReturnBill.InitialApproveTime = null;
            partsSalesReturnBill.ApproverId = null;
            partsSalesReturnBill.ApproverName = null;
            partsSalesReturnBill.ApproveTime = null;
            partsSalesReturnBill.FinalApproverId = null;
            partsSalesReturnBill.FinalApproverName = null;
            partsSalesReturnBill.FinalApproveTime = null;
            partsSalesReturnBill.Status = (int)DcsPartsSalesReturnBillStatus.新增;
            partsSalesReturnBill.RejecterId = userInfo.Id; 
            partsSalesReturnBill.RejecterName = userInfo.Name; 
            partsSalesReturnBill.RejectTime = DateTime.Now; 

            UpdateToDatabase(partsSalesReturnBill);
            ObjectContext.SaveChanges();
        }
        public void 终审不通过(PartsSalesReturnBill partsSalesReturnBill) {
            var userInfo = Utils.GetCurrentUserInfo();
            partsSalesReturnBill.ModifierId = userInfo.Id;
            partsSalesReturnBill.ModifierName = userInfo.Name;
            partsSalesReturnBill.ModifyTime = DateTime.Now;
            partsSalesReturnBill.InitialApproverId = null;
            partsSalesReturnBill.InitialApproverName = null;
            partsSalesReturnBill.InitialApproveTime = null;
            partsSalesReturnBill.ApproverId = null;
            partsSalesReturnBill.ApproverName = null;
            partsSalesReturnBill.ApproveTime = null;
            partsSalesReturnBill.FinalApproverId = null;
            partsSalesReturnBill.FinalApproverName = null;
            partsSalesReturnBill.FinalApproveTime = null;
            partsSalesReturnBill.Status = (int)DcsPartsSalesReturnBillStatus.新增;
            partsSalesReturnBill.RejecterId = userInfo.Id; 
            partsSalesReturnBill.RejecterName = userInfo.Name; 
            partsSalesReturnBill.RejectTime = DateTime.Now; 

            UpdateToDatabase(partsSalesReturnBill);
            ObjectContext.SaveChanges();
        }
        public void 审核不通过(PartsSalesReturnBill partsSalesReturnBill) {
            var userInfo = Utils.GetCurrentUserInfo();
            partsSalesReturnBill.ModifierId = userInfo.Id;
            partsSalesReturnBill.ModifierName = userInfo.Name;
            partsSalesReturnBill.ModifyTime = DateTime.Now;
            partsSalesReturnBill.InitialApproverId = null;
            partsSalesReturnBill.InitialApproverName = null;
            partsSalesReturnBill.InitialApproveTime = null;
            partsSalesReturnBill.ApproverId = null;
            partsSalesReturnBill.ApproverName = null;
            partsSalesReturnBill.ApproveTime = null;
            partsSalesReturnBill.FinalApproverId = null;
            partsSalesReturnBill.FinalApproverName = null;
            partsSalesReturnBill.FinalApproveTime = null;
            partsSalesReturnBill.Status = (int)DcsPartsSalesReturnBillStatus.新增;
            partsSalesReturnBill.RejecterId = userInfo.Id; 
            partsSalesReturnBill.RejecterName = userInfo.Name; 
            partsSalesReturnBill.RejectTime = DateTime.Now; 

            UpdateToDatabase(partsSalesReturnBill);
            ObjectContext.SaveChanges();
        }
        public void 批量审核退货单(int[] ids) {
            foreach(var id in ids) {

                var returnBill = ObjectContext.PartsSalesReturnBills.Include("PartsSalesReturnBillDetails").Where(r => r.Id == id).FirstOrDefault();
                foreach(var item in returnBill.PartsSalesReturnBillDetails) {
                    item.ApproveQuantity = item.ReturnedQuantity;
                }
                if(returnBill.WarehouseName.Contains("虚拟")) {
                    throw new ValidationException(returnBill.Code+"入库仓库不能是虚拟库，请单独初审");
                }
                this.销售订单退货初审(returnBill);
                ObjectContext.SaveChanges();
            }
        }

    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:11:57
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 生成配件销售退货单(PartsSalesReturnBill partsSalesReturnBill) {
            new PartsSalesReturnBillAch(this).生成配件销售退货单(partsSalesReturnBill);
        }


        [Update(UsingCustomMethod = true)]
        public void 修改配件销售退货单(PartsSalesReturnBill partsSalesReturnBill) {
            new PartsSalesReturnBillAch(this).修改配件销售退货单(partsSalesReturnBill);
        }


        [Update(UsingCustomMethod = true)]
        public void 审批配件销售退货单(PartsSalesReturnBill partsSalesReturnBill) {
            new PartsSalesReturnBillAch(this).审批配件销售退货单(partsSalesReturnBill);
        }


        [Update(UsingCustomMethod = true)]
        public void 驳回配件销售退货单(PartsSalesReturnBill partsSalesReturnBill) {
            new PartsSalesReturnBillAch(this).驳回配件销售退货单(partsSalesReturnBill);
        }


        [Update(UsingCustomMethod = true)]
        public void 作废配件销售退货单(PartsSalesReturnBill partsSalesReturnBill) {
            new PartsSalesReturnBillAch(this).作废配件销售退货单(partsSalesReturnBill);
        }


        [Update(UsingCustomMethod = true)]
        public void 终止配件销售退货单(PartsSalesReturnBill partsSalesReturnBill) {
            new PartsSalesReturnBillAch(this).终止配件销售退货单(partsSalesReturnBill);
        }
        [Update(UsingCustomMethod = true)]
        public void 上传销售退货单附件(PartsSalesReturnBill partsSalesReturnBill) {
            new PartsSalesReturnBillAch(this).上传销售退货单附件(partsSalesReturnBill);
        }
        [Update(UsingCustomMethod = true)]
        public void 销售订单退货初审(PartsSalesReturnBill partsSalesReturnBill) {
            new PartsSalesReturnBillAch(this).销售订单退货初审(partsSalesReturnBill);
        }
        [Update(UsingCustomMethod = true)]
        public void 销售订单退货终审(PartsSalesReturnBill partsSalesReturnBill) {
            new PartsSalesReturnBillAch(this).销售订单退货终审(partsSalesReturnBill);
        }
        [Update(UsingCustomMethod = true)]
        public void 初审不通过(PartsSalesReturnBill partsSalesReturnBill) {
            new PartsSalesReturnBillAch(this).初审不通过(partsSalesReturnBill);
        }
        [Update(UsingCustomMethod = true)]
        public void 终审不通过(PartsSalesReturnBill partsSalesReturnBill) {
            new PartsSalesReturnBillAch(this).终审不通过(partsSalesReturnBill);
        }
        [Update(UsingCustomMethod = true)]
        public void 审核不通过(PartsSalesReturnBill partsSalesReturnBill) { 
            new PartsSalesReturnBillAch(this).审核不通过(partsSalesReturnBill);
        }
        [Invoke(HasSideEffects = true)]
        public void 批量审核退货单(int[] ids) {
            new PartsSalesReturnBillAch(this).批量审核退货单(ids);
        }
        [Update(UsingCustomMethod = true)]

        public void 提交配件销售退货单(PartsSalesReturnBill partsSalesReturnBill) {
            new PartsSalesReturnBillAch(this).提交配件销售退货单(partsSalesReturnBill);
        }
    }
}
