﻿using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class PersonSalesCenterLinkAch : DcsSerivceAchieveBase {
        public PersonSalesCenterLinkAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 作废人员与销售中心关系(PersonSalesCenterLink personSalesCenterLink) {
            var status = ObjectContext.PersonSalesCenterLinks.Where(r => r.Id == personSalesCenterLink.Id && r.Status == (int)DcsMasterDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(status == null)
                throw new ValidationException(ErrorStrings.PersonSalesCenterLink_Validation1);
            UpdateToDatabase(personSalesCenterLink);
            personSalesCenterLink.Status = (int)DcsMasterDataStatus.作废;
            this.UpdatePersonSalesCenterLinkValidate(personSalesCenterLink);
        }

        public void 删除人员与销售中心关系(PersonSalesCenterLink personSalesCenterLink) {
            var status = ObjectContext.PersonSalesCenterLinks.Where(r => r.Id == personSalesCenterLink.Id && r.Status == (int)DcsMasterDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(status == null)
                throw new ValidationException(ErrorStrings.PersonSalesCenterLink_Validation3);
            DeleteFromDatabase(personSalesCenterLink);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:11:57
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 作废人员与销售中心关系(PersonSalesCenterLink personSalesCenterLink) {
            new PersonSalesCenterLinkAch(this).作废人员与销售中心关系(personSalesCenterLink);
        }
        
        [Update(UsingCustomMethod = true)]
        public void 删除人员与销售中心关系(PersonSalesCenterLink personSalesCenterLink) {
            new PersonSalesCenterLinkAch(this).删除人员与销售中心关系(personSalesCenterLink);
        }
    }
}
