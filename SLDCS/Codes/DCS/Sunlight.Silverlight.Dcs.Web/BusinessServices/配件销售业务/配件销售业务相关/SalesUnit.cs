﻿using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class SalesUnitAch : DcsSerivceAchieveBase {
        public SalesUnitAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 生成销售组织(SalesUnit salesUnit, int[] warehouseIds) {
            var salesUnits = ObjectContext.SalesUnits.Where(r => r.PartsSalesCategoryId == salesUnit.PartsSalesCategoryId && r.OwnerCompanyId == salesUnit.OwnerCompanyId);
            if(salesUnits.Count() != 0) {
                throw new ValidationException(ErrorStrings.SalesUnit_Validation6);
            }
            if((from warehouseId in warehouseIds
                let dbWarehouseIds = ObjectContext.SalesUnitAffiWarehouses.Select(r => r.WarehouseId)
                where dbWarehouseIds.Contains(warehouseId)
                select warehouseId).Any()) {
                throw new ValidationException(ErrorStrings.SalesUnit_Validation7);
            }
            var warehouses = ObjectContext.Warehouses.Where(r => r.StorageCompanyId == salesUnit.OwnerCompanyId && r.BranchId == salesUnit.BranchId && r.Type == (int)DcsWarehouseType.虚拟库 && r.Status == (int)DcsBaseDataStatus.有效).ToArray();
            if(warehouses.Count() > 1)
                throw new ValidationException(ErrorStrings.SalesUnit_Validation1);
            
            if(salesUnit.BranchId != salesUnit.OwnerCompanyId) {
                var dbsalesUnit = ObjectContext.SalesUnits.Where(r => r.OwnerCompanyId == salesUnit.OwnerCompanyId && r.PartsSalesCategoryId == salesUnit.PartsSalesCategoryId && r.Status == (int)DcsMasterDataStatus.有效).ToArray();
                if(dbsalesUnit.Any())
                    throw new ValidationException(ErrorStrings.SalesUnit_Validation2);
            }
            if(!warehouses.Any()) {
                var warehouse = new Warehouse {
                    Type = (int)DcsWarehouseType.虚拟库,
                    Status = (int)DcsBaseDataStatus.有效,
                    StorageStrategy = (int)DcsStorageStrategy.随机存储,
                    BranchId = salesUnit.BranchId,
                    StorageCompanyId = salesUnit.OwnerCompanyId,
                    StorageCompanyType = salesUnit.BranchId == salesUnit.OwnerCompanyId ? (int)DcsCompanyType.分公司 : (int)DcsCompanyType.代理库
                };
                InsertToDatabase(warehouse);
                new WarehouseAch(this.DomainService).InsertWarehouseValidate(warehouse);
            }
        }

        public void 修改销售组织(SalesUnit salesUnit, int[] warehouseIds) {
            if(warehouseIds.Any()) {
                if((from warehouseId in warehouseIds
                    let dbWarehouseIds = ObjectContext.SalesUnitAffiWarehouses.Where(r => r.SalesUnitId != salesUnit.Id).Select(r => r.WarehouseId)
                    where dbWarehouseIds.Contains(warehouseId)
                    select warehouseId).Any()) {
                    throw new ValidationException(ErrorStrings.SalesUnit_Validation7);
                }
            }

            var dbSalesUnit = ObjectContext.SalesUnits.Where(r => r.Id == salesUnit.Id && r.Status == (int)DcsMasterDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbSalesUnit == null)
                throw new ValidationException(ErrorStrings.SalesUnit_Validation3);
            var warehouses = ObjectContext.Warehouses.Where(r => r.StorageCompanyId == salesUnit.OwnerCompanyId && r.BranchId == salesUnit.BranchId && r.Type == (int)DcsWarehouseType.虚拟库 && r.Status == (int)DcsBaseDataStatus.有效).ToArray();
            if(warehouses.Count() > 1)
                throw new ValidationException(ErrorStrings.SalesUnit_Validation1);
            
            if(salesUnit.BranchId != salesUnit.OwnerCompanyId) {
                var dbsalesUnit = ObjectContext.SalesUnits.Where(r => r.Id != salesUnit.Id && r.OwnerCompanyId == salesUnit.OwnerCompanyId && r.PartsSalesCategoryId == salesUnit.PartsSalesCategoryId && r.Status == (int)DcsMasterDataStatus.有效).ToArray();
                if(dbsalesUnit.Any())
                    throw new ValidationException(ErrorStrings.SalesUnit_Validation2);
            }
            if(!warehouses.Any()) {
                var warehouse = new Warehouse {
                    Type = (int)DcsWarehouseType.虚拟库,
                    Status = (int)DcsBaseDataStatus.有效,
                    StorageStrategy = (int)DcsStorageStrategy.随机存储,
                    BranchId = salesUnit.BranchId,
                    StorageCompanyId = salesUnit.OwnerCompanyId,
                    StorageCompanyType = salesUnit.BranchId == salesUnit.OwnerCompanyId ? (int)DcsCompanyType.分公司 : (int)DcsCompanyType.代理库
                };
                InsertToDatabase(warehouse);
                new WarehouseAch(this.DomainService).InsertWarehouseValidate(warehouse);
            }
            UpdateToDatabase(salesUnit);
            UpdateSalesUnit(salesUnit);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:11:57
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 生成销售组织(SalesUnit salesUnit, int[] warehouseIds) {
            new SalesUnitAch(this).生成销售组织(salesUnit,warehouseIds);
        }
        
        [Update(UsingCustomMethod = true)]
        public void 修改销售组织(SalesUnit salesUnit, int[] warehouseIds) {
            new SalesUnitAch(this).修改销售组织(salesUnit,warehouseIds);
        }
    }
}