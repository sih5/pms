﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class SecondClassStationPlanAch : DcsSerivceAchieveBase {
        public SecondClassStationPlanAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 提交二级站需求计划(SecondClassStationPlan secondClassStationPlan) {
            var dbsecondClassStationPlan = ObjectContext.SecondClassStationPlans.Where(c => c.Id == secondClassStationPlan.Id && c.Status == (int)DcsSecondClassStationPlanStatus.新建).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbsecondClassStationPlan == null) {
                throw new ValidationException(ErrorStrings.SecondClassStationPlan_Validation1);
            }
            secondClassStationPlan.Status = (int)DcsSecondClassStationPlanStatus.提交;
            var userInfo = Utils.GetCurrentUserInfo();
            secondClassStationPlan.SubmitterId = userInfo.Id;
            secondClassStationPlan.SubmitterName = userInfo.Name;
            secondClassStationPlan.SubmitTime = DateTime.Now;

        }


        public void 作废二级站需求计划(SecondClassStationPlan secondClassStationPlan) {
            var dbsecondClassStationPlan = ObjectContext.SecondClassStationPlans.Where(c => c.Id == secondClassStationPlan.Id && c.Status == (int)DcsSecondClassStationPlanStatus.新建).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbsecondClassStationPlan == null) {
                throw new ValidationException(ErrorStrings.SecondClassStationPlan_Validation1);
            }
            secondClassStationPlan.Status = (int)DcsSecondClassStationPlanStatus.作废;
            var userInfo = Utils.GetCurrentUserInfo();
            secondClassStationPlan.AbandonerId = userInfo.Id;
            secondClassStationPlan.AbandonerName = userInfo.Name;
            secondClassStationPlan.AbandonTime = DateTime.Now;
        }

        public void 审核二级站需求计划(SecondClassStationPlan secondClassStationPlan) {
            var dbsecondClassStationPlan = ObjectContext.SecondClassStationPlans.Where(c => c.Id == secondClassStationPlan.Id && c.Status == (int)DcsSecondClassStationPlanStatus.提交).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbsecondClassStationPlan == null) {
                throw new ValidationException(ErrorStrings.SecondClassStationPlan_Validation3);
            }
            secondClassStationPlan.Status = (int)DcsSecondClassStationPlanStatus.审核通过;
            var userInfo = Utils.GetCurrentUserInfo();
            secondClassStationPlan.ApproverId = userInfo.Id;
            secondClassStationPlan.ApproverName = userInfo.Name;
            secondClassStationPlan.ApproveTime = DateTime.Now;
            var secondClassStationPlanDetails = secondClassStationPlan.SecondClassStationPlanDetails.Where(r => r.ProcessMode == (int)DcsProcessMode.本企业满足).ToArray();
            if(secondClassStationPlanDetails.Length > 0) {
                var i = 1;
                decimal totalAmount = 0;
                var partsSalesCategory = ObjectContext.PartsSalesCategories.SingleOrDefault(r => r.Id == secondClassStationPlan.PartsSalesCategoryId && r.Status == (int)DcsBaseDataStatus.有效);
                if(partsSalesCategory == null) {
                    throw new ValidationException("未找到对应的品牌或者品牌已经被作废");
                }

                var branch = ObjectContext.Branches.SingleOrDefault(r => r.Id == partsSalesCategory.BranchId && r.Status == (int)DcsBaseDataStatus.有效);
                if(branch == null) {
                    throw new ValidationException("未找到对应的分公司");
                }
                var sparePartIds = secondClassStationPlanDetails.Select(r => r.SparePartId).ToArray();
                var sparePartPrices = DomainService.查询经销商配件零售价(secondClassStationPlan.FirstClassStationId, secondClassStationPlan.PartsSalesCategoryId, sparePartIds, -1).ToArray();

                var dealerPartsRetailOrder = new DealerPartsRetailOrder();
                dealerPartsRetailOrder.RetailOrderType = (int)DcsDealerPartsRetailOrderRetailOrderType.二级站;
                dealerPartsRetailOrder.BranchId = partsSalesCategory.BranchId;
                dealerPartsRetailOrder.BranchName = branch.Name;
                dealerPartsRetailOrder.DealerId = secondClassStationPlan.FirstClassStationId;
                dealerPartsRetailOrder.DealerCode = secondClassStationPlan.FirstClassStationCode;
                dealerPartsRetailOrder.DealerName = secondClassStationPlan.FirstClassStationName;
                dealerPartsRetailOrder.SubDealerId = secondClassStationPlan.SecondClassStationId;
                dealerPartsRetailOrder.SubDealerCode = secondClassStationPlan.SecondClassStationCode;
                dealerPartsRetailOrder.SubDealerName = secondClassStationPlan.SecondClassStationName;
                dealerPartsRetailOrder.SubDealerlOrderType = 0;
                dealerPartsRetailOrder.PartsSalesCategoryId = secondClassStationPlan.PartsSalesCategoryId;
                dealerPartsRetailOrder.SalesCategoryName = partsSalesCategory.Name;
                dealerPartsRetailOrder.Remark = partsSalesCategory.Remark;
                dealerPartsRetailOrder.CreatorId = secondClassStationPlan.ModifierId;
                dealerPartsRetailOrder.CreatorName = secondClassStationPlan.ModifierName;
                dealerPartsRetailOrder.CreateTime = secondClassStationPlan.ModifyTime;
                dealerPartsRetailOrder.Status = (int)DcsWorkflowOfSimpleApprovalStatus.新建;
                InsertToDatabase(dealerPartsRetailOrder);
                foreach(var secondClassStationPlanDetail in secondClassStationPlanDetails) {
                    var dealerPartsStock = ObjectContext.DealerPartsStocks.SingleOrDefault(r => r.SalesCategoryId == secondClassStationPlan.PartsSalesCategoryId && r.SparePartId == secondClassStationPlanDetail.SparePartId && r.DealerId == secondClassStationPlan.FirstClassStationId && r.SubDealerId == -1);
                    int tempQuantity = 0;
                    if(dealerPartsStock != null) {
                        tempQuantity = dealerPartsStock.Quantity;
                    }
                    var tempPrices = sparePartPrices.Where(r => r.SparePartId == secondClassStationPlanDetail.SparePartId).ToArray();
                    if(secondClassStationPlanDetail.Quantity.HasValue && secondClassStationPlanDetail.Quantity.Value > tempQuantity) {
                        throw new ValidationException("配件" + secondClassStationPlanDetail.SparePartCode + "不支持本企业满足处理方式，请使用转销售订单处理方式");
                    }
                    if(tempPrices.Length > 1) {
                        throw new ValidationException(ErrorStrings.ExportColumn_PartCode + secondClassStationPlanDetail.SparePartCode + "在经销商" + secondClassStationPlan.FirstClassStationCode + "的品牌" + partsSalesCategory.Name + "下存在多个建议售价");
                    }
                    if(tempPrices.Length == 0) {
                        throw new ValidationException(ErrorStrings.ExportColumn_PartCode + secondClassStationPlanDetail.SparePartCode + "在经销商" + secondClassStationPlan.FirstClassStationCode + "的品牌" + partsSalesCategory.Name + "下未找到建议售价");
                    }

                    var price = tempPrices[0].RetailGuidePrice;
                    var dealerRetailOrderDetail = new DealerRetailOrderDetail();
                    InsertToDatabase(dealerRetailOrderDetail);
                    dealerRetailOrderDetail.SerialNumber = i;
                    dealerRetailOrderDetail.PartsId = secondClassStationPlanDetail.SparePartId;
                    dealerRetailOrderDetail.PartsCode = secondClassStationPlanDetail.SparePartCode;
                    dealerRetailOrderDetail.PartsName = secondClassStationPlanDetail.SparePartName;
                    dealerRetailOrderDetail.Quantity = secondClassStationPlanDetail.Quantity ?? 0;
                    dealerRetailOrderDetail.Price = price;
                    dealerRetailOrderDetail.Remark = secondClassStationPlanDetail.Remark;
                    dealerPartsRetailOrder.DealerRetailOrderDetails.Add(dealerRetailOrderDetail);
                    totalAmount = totalAmount + (dealerRetailOrderDetail.Quantity * dealerRetailOrderDetail.Price);
                    i++;
                }
                dealerPartsRetailOrder.TotalAmount = totalAmount;
                new DealerPartsRetailOrderAch(this.DomainService).InsertDealerPartsRetailOrderValidate(dealerPartsRetailOrder);
            }


        }

        public void 修改二级站需求计划(SecondClassStationPlan secondClassStationPlan) {
            UpdateToDatabase(secondClassStationPlan);
            var dbsecondClassStationPlan = ObjectContext.SecondClassStationPlans.Where(c => c.Id == secondClassStationPlan.Id && c.Status == (int)DcsSecondClassStationPlanStatus.新建).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbsecondClassStationPlan == null) {
                throw new ValidationException(ErrorStrings.SecondClassStationPlan_Validation1);
            }
            this.UpdateSecondClassStationPlanValidate(secondClassStationPlan);

        }


        public void 终止二级站需求计划(SecondClassStationPlan secondClassStationPlan) {
            var dbsecondClassStationPlan = ObjectContext.SecondClassStationPlans.Where(c => c.Id == secondClassStationPlan.Id && c.Status == (int)DcsSecondClassStationPlanStatus.提交).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbsecondClassStationPlan == null)
                throw new ValidationException(ErrorStrings.SecondClassStationPlan_Validation2);
            secondClassStationPlan.Status = (int)DcsSecondClassStationPlanStatus.终止;
            UpdateToDatabase(secondClassStationPlan);
        }


    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:11:57
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 提交二级站需求计划(SecondClassStationPlan secondClassStationPlan) {
            new SecondClassStationPlanAch(this).提交二级站需求计划(secondClassStationPlan);
        }


        [Update(UsingCustomMethod = true)]
        public void 作废二级站需求计划(SecondClassStationPlan secondClassStationPlan) {
            new SecondClassStationPlanAch(this).作废二级站需求计划(secondClassStationPlan);
        }

        [Update(UsingCustomMethod = true)]
        public void 审核二级站需求计划(SecondClassStationPlan secondClassStationPlan) {
            new SecondClassStationPlanAch(this).审核二级站需求计划(secondClassStationPlan);
        }

        [Update(UsingCustomMethod = true)]
        public void 修改二级站需求计划(SecondClassStationPlan secondClassStationPlan) {
            new SecondClassStationPlanAch(this).修改二级站需求计划(secondClassStationPlan);
        }


        [Update(UsingCustomMethod = true)]
        public void 终止二级站需求计划(SecondClassStationPlan secondClassStationPlan) {
            new SecondClassStationPlanAch(this).终止二级站需求计划(secondClassStationPlan);
        }
    }
}
