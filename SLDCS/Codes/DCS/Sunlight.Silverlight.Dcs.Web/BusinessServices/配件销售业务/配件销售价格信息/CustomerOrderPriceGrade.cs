﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class CustomerOrderPriceGradeAch : DcsSerivceAchieveBase {
        public CustomerOrderPriceGradeAch(DcsDomainService domainService)
            : base(domainService) {
        }

        /// <summary>
        /// 仅供客户端使用
        /// </summary>

        public void 修改客户企业订单价格等级(CustomerOrderPriceGrade customerOrderPriceGrade) {
            CheckEntityState(customerOrderPriceGrade);
            var originalCustomerOrderPriceGrade = ChangeSet.GetOriginal(customerOrderPriceGrade);
            if(originalCustomerOrderPriceGrade == null)
                return;
            var customerOrderPriceGradeHistory = new CustomerOrderPriceGradeHistory {
                CustomerCompanyId = originalCustomerOrderPriceGrade.CustomerCompanyId,
                CustomerCompanyCode = originalCustomerOrderPriceGrade.CustomerCompanyCode,
                CustomerCompanyName = originalCustomerOrderPriceGrade.CustomerCompanyName,
                PartsSalesOrderTypeId = originalCustomerOrderPriceGrade.PartsSalesOrderTypeId,
                PartsSalesOrderTypeCode = originalCustomerOrderPriceGrade.PartsSalesOrderTypeCode,
                PartsSalesOrderTypeName = originalCustomerOrderPriceGrade.PartsSalesOrderTypeName,
                Coefficient = originalCustomerOrderPriceGrade.Coefficient,
                Status = originalCustomerOrderPriceGrade.Status,
                Remark = originalCustomerOrderPriceGrade.Remark,
            };
            InsertToDatabase(customerOrderPriceGradeHistory);
            new CustomerOrderPriceGradeHistoryAch(this.DomainService).InsertCustomerOrderPriceGradeHistoryValidate(customerOrderPriceGradeHistory);
        }

        public void 作废客户企业订单价格等级(CustomerOrderPriceGrade customerOrderPriceGrade) {
            var dbcustomerOrderPriceGrade = ObjectContext.CustomerOrderPriceGrades.Where(r => r.Id == customerOrderPriceGrade.Id && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbcustomerOrderPriceGrade == null)
                throw new ValidationException(ErrorStrings.CustomerOrderPriceGrade_Validation2);
            UpdateToDatabase(customerOrderPriceGrade);
            customerOrderPriceGrade.Status = (int)DcsBaseDataStatus.作废;
            var customerOrderPriceGradeHistory = new CustomerOrderPriceGradeHistory {
                CustomerCompanyId = dbcustomerOrderPriceGrade.CustomerCompanyId,
                CustomerCompanyCode = dbcustomerOrderPriceGrade.CustomerCompanyCode,
                CustomerCompanyName = dbcustomerOrderPriceGrade.CustomerCompanyName,
                PartsSalesOrderTypeId = dbcustomerOrderPriceGrade.PartsSalesOrderTypeId,
                PartsSalesOrderTypeCode = dbcustomerOrderPriceGrade.PartsSalesOrderTypeCode,
                PartsSalesOrderTypeName = dbcustomerOrderPriceGrade.PartsSalesOrderTypeName,
                Coefficient = dbcustomerOrderPriceGrade.Coefficient,
                Status = dbcustomerOrderPriceGrade.Status,
                Remark = dbcustomerOrderPriceGrade.Remark,
            };
            InsertToDatabase(customerOrderPriceGradeHistory);
            new CustomerOrderPriceGradeHistoryAch(this.DomainService).InsertCustomerOrderPriceGradeHistoryValidate(customerOrderPriceGradeHistory);
            var userInfo = Utils.GetCurrentUserInfo();
            customerOrderPriceGrade.AbandonerId = userInfo.Id;
            customerOrderPriceGrade.AbandonerName = userInfo.Name;
            customerOrderPriceGrade.AbandonTime = DateTime.Now;
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:11:57
        //原分布类的函数全部转移到Ach                                                               
        /// <summary>
        /// 仅供客户端使用
        /// </summary>
        [Update(UsingCustomMethod = true)]
        public void 修改客户企业订单价格等级(CustomerOrderPriceGrade customerOrderPriceGrade) {
            new CustomerOrderPriceGradeAch(this).修改客户企业订单价格等级(customerOrderPriceGrade);
        }
        
        [Update(UsingCustomMethod = true)]
        public void 作废客户企业订单价格等级(CustomerOrderPriceGrade customerOrderPriceGrade) {
            new CustomerOrderPriceGradeAch(this).作废客户企业订单价格等级(customerOrderPriceGrade);
        }
    }
}
