﻿using Sunlight.Silverlight.Web;
using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using Microsoft.Data.Extensions;using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DealerPartsSalesPriceAch : DcsSerivceAchieveBase {
        public DealerPartsSalesPriceAch(DcsDomainService domainService)
            : base(domainService) {
        }

        public void 修改服务站批发价(DealerPartsSalesPrice dealerPartsSalesPrice) {
            //加履历
            var userInfo = Utils.GetCurrentUserInfo();
            DealerPartsSalesPriceHistory dealerPartsSalesPriceHistory = new DealerPartsSalesPriceHistory();
            dealerPartsSalesPriceHistory.DealerPartsSalesPriceId = dealerPartsSalesPrice.Id;
            dealerPartsSalesPriceHistory.BranchId = dealerPartsSalesPrice.BranchId;
            dealerPartsSalesPriceHistory.PartsSalesCategoryId = dealerPartsSalesPrice.PartsSalesCategoryId;
            dealerPartsSalesPriceHistory.PartsSalesCategoryCode = dealerPartsSalesPrice.PartsSalesCategoryCode;
            dealerPartsSalesPriceHistory.PartsSalesCategoryName = dealerPartsSalesPrice.PartsSalesCategoryName;
            dealerPartsSalesPriceHistory.SparePartId = dealerPartsSalesPrice.SparePartId;
            dealerPartsSalesPriceHistory.SparePartCode = dealerPartsSalesPrice.SparePartCode;
            dealerPartsSalesPriceHistory.SparePartName = dealerPartsSalesPrice.SparePartName;
            dealerPartsSalesPriceHistory.DealerSalesPrice = dealerPartsSalesPrice.DealerSalesPrice;
            dealerPartsSalesPriceHistory.Status = dealerPartsSalesPrice.Status;
            dealerPartsSalesPriceHistory.Remark = dealerPartsSalesPrice.Remark;
            dealerPartsSalesPriceHistory.CreatorId = userInfo.Id;
            dealerPartsSalesPriceHistory.CreatorName = userInfo.Name;
            dealerPartsSalesPriceHistory.CreateTime = DateTime.Now;
            DomainService.InsertDealerPartsSalesPriceHistory(dealerPartsSalesPriceHistory);
        }
        public void 作废服务站批发价(DealerPartsSalesPrice dealerPartsSalesPrice) {
            var dbDealerPartsSalesPrice = ObjectContext.DealerPartsSalesPrices.Where(r => r.Id == dealerPartsSalesPrice.Id && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbDealerPartsSalesPrice == null) {
                throw new ValidationException(ErrorStrings.DealerPartsSalesPrice_Validation1);
            }
            dealerPartsSalesPrice.Status = (int)DcsBaseDataStatus.作废;
            var userInfo = Utils.GetCurrentUserInfo();
            dealerPartsSalesPrice.AbandonerId = userInfo.Id;
            dealerPartsSalesPrice.AbandonerName = userInfo.Name;
            dealerPartsSalesPrice.AbandonTime = DateTime.Now;
            UpdateToDatabase(dealerPartsSalesPrice);
            //加履历
            DealerPartsSalesPriceHistory dealerPartsSalesPriceHistory = new DealerPartsSalesPriceHistory();
            dealerPartsSalesPriceHistory.DealerPartsSalesPriceId = dealerPartsSalesPrice.Id;
            dealerPartsSalesPriceHistory.BranchId = dealerPartsSalesPrice.BranchId;
            dealerPartsSalesPriceHistory.PartsSalesCategoryId = dealerPartsSalesPrice.PartsSalesCategoryId;
            dealerPartsSalesPriceHistory.PartsSalesCategoryCode = dealerPartsSalesPrice.PartsSalesCategoryCode;
            dealerPartsSalesPriceHistory.PartsSalesCategoryName = dealerPartsSalesPrice.PartsSalesCategoryName;
            dealerPartsSalesPriceHistory.SparePartId = dealerPartsSalesPrice.SparePartId;
            dealerPartsSalesPriceHistory.SparePartCode = dealerPartsSalesPrice.SparePartCode;
            dealerPartsSalesPriceHistory.SparePartName = dealerPartsSalesPrice.SparePartName;
            dealerPartsSalesPriceHistory.DealerSalesPrice = dealerPartsSalesPrice.DealerSalesPrice;
            dealerPartsSalesPriceHistory.Status = dealerPartsSalesPrice.Status;
            dealerPartsSalesPriceHistory.Remark = dealerPartsSalesPrice.Remark;
            dealerPartsSalesPriceHistory.CreatorId = userInfo.Id;
            dealerPartsSalesPriceHistory.CreatorName = userInfo.Name;
            dealerPartsSalesPriceHistory.CreateTime = DateTime.Now;
            DomainService.InsertDealerPartsSalesPriceHistory(dealerPartsSalesPriceHistory);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:11:57
        //原分布类的函数全部转移到Ach                                                               

        public void 修改服务站批发价(DealerPartsSalesPrice dealerPartsSalesPrice) {
            new DealerPartsSalesPriceAch(this).修改服务站批发价(dealerPartsSalesPrice);
        }

        public void 作废服务站批发价(DealerPartsSalesPrice dealerPartsSalesPrice) {
            new DealerPartsSalesPriceAch(this).作废服务站批发价(dealerPartsSalesPrice);
        }
    }
}
