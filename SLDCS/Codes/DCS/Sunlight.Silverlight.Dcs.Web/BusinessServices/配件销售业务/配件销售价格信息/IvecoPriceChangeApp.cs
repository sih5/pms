﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
using System.Configuration;
using System.Collections.Generic;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class IvecoPriceChangeAppAch : DcsSerivceAchieveBase {
        //public IvecoPriceChangeAppAch(DcsDomainService domainService)
        //    : base(domainService) {
        //}

        /// <summary>
        /// 仅供客户端使用
        /// </summary>

        public void 初审依维柯销售价变更申请(IvecoPriceChangeApp ivecoPriceChangeApp) {
            var dbIvecoPriceChangeApp = ObjectContext.IvecoPriceChangeApps.Where(r => r.Id == ivecoPriceChangeApp.Id && r.Status == (int)DcsIvecoPriceAppStatus.新增).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbIvecoPriceChangeApp == null)
                throw new ValidationException(ErrorStrings.IvecoPriceChangeApp_Validation2);
            var userinfo = Utils.GetCurrentUserInfo();
            UpdateToDatabase(ivecoPriceChangeApp);
            ivecoPriceChangeApp.InitialApproverId = userinfo.Id;
            ivecoPriceChangeApp.InitialApproverName = userinfo.Name;
            ivecoPriceChangeApp.InitialApproveTime = System.DateTime.Now;
            ivecoPriceChangeApp.Status = (int)DcsIvecoPriceAppStatus.初审通过;
            this.UpdateIvecoPriceChangeAppValidate(ivecoPriceChangeApp);
        }


        public void 批量初审依维柯销售价变更申请(int[] ids) {
            var dbIvecoPriceChangeApps = ObjectContext.IvecoPriceChangeApps.Where(r => ids.Contains(r.Id) && r.Status == (int)DcsIvecoPriceAppStatus.新增).SetMergeOption(MergeOption.NoTracking);
            if(dbIvecoPriceChangeApps == null)
                throw new ValidationException(ErrorStrings.IvecoPriceChangeApp_Validation2);
            var userinfo = Utils.GetCurrentUserInfo();
            foreach(var ivecoPriceChangeApp in dbIvecoPriceChangeApps) {
                UpdateToDatabase(ivecoPriceChangeApp);
                ivecoPriceChangeApp.InitialApproverId = userinfo.Id;
                ivecoPriceChangeApp.InitialApproverName = userinfo.Name;
                ivecoPriceChangeApp.InitialApproveTime = System.DateTime.Now;
                ivecoPriceChangeApp.Status = (int)DcsIvecoPriceAppStatus.初审通过;
                this.UpdateIvecoPriceChangeAppValidate(ivecoPriceChangeApp);
            }
            ObjectContext.SaveChanges();
        }


        public void 终审依维柯销售价变更申请(IvecoPriceChangeApp ivecoPriceChangeApp) {
            var dbIvecoPriceChangeApp = ObjectContext.IvecoPriceChangeApps.Where(r => r.Id == ivecoPriceChangeApp.Id && r.Status == (int)DcsIvecoPriceAppStatus.初审通过).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbIvecoPriceChangeApp == null)
                throw new ValidationException(ErrorStrings.IvecoPriceChangeApp_Validation3);
            var userinfo = Utils.GetCurrentUserInfo();
            UpdateToDatabase(ivecoPriceChangeApp);
            ivecoPriceChangeApp.FinalApproverId = userinfo.Id;
            ivecoPriceChangeApp.FinalApproverName = userinfo.Name;
            ivecoPriceChangeApp.FinalApproveTime = System.DateTime.Now;
            ivecoPriceChangeApp.Status = (int)DcsIvecoPriceAppStatus.终审通过;
            this.UpdateIvecoPriceChangeAppValidate(ivecoPriceChangeApp);
            var ivecoPriceChangeAppDetails = ivecoPriceChangeApp.IvecoPriceChangeAppDetails.ToArray();
            var partsIds = ivecoPriceChangeAppDetails.Select(r => r.SparePartId).ToArray();
            var ivecoSalesPrices = ObjectContext.IvecoSalesPrices.Where(r => r.PartsSalesCategoryId == ivecoPriceChangeApp.PartsSalesCategoryId && partsIds.Contains(r.SparePartId)).ToArray();
            foreach(var ivecoPriceChangeAppDetail in ivecoPriceChangeAppDetails) {
                //新增或更新配件销售价。
                var ivecoSalesPrice = ivecoSalesPrices.FirstOrDefault(r => r.SparePartId == ivecoPriceChangeAppDetail.SparePartId);
                if(ivecoSalesPrice == null) {
                    var newIvecoSalesPrice = new IvecoSalesPrice {
                        PartsSalesCategoryId = ivecoPriceChangeApp.PartsSalesCategoryId,
                        PartsSalesCategoryName = ivecoPriceChangeApp.PartsSalesCategory.Name,
                        SparePartId = ivecoPriceChangeAppDetail.SparePartId,
                        SparePartCode = ivecoPriceChangeAppDetail.SparePartCode,
                        SparePartName = ivecoPriceChangeAppDetail.SparePartName,
                        SalesPrice = ivecoPriceChangeAppDetail.IvecoPrice,
                        CreatorId = ivecoPriceChangeApp.CreatorId,
                        CreatorName = ivecoPriceChangeApp.CreatorName,
                        CreateTime = ivecoPriceChangeApp.CreateTime,
                        ModifierId = ivecoPriceChangeApp.ModifierId,
                        ModifierName = ivecoPriceChangeApp.ModifierName,
                        ModifyTime = ivecoPriceChangeApp.ModifyTime
                    };
                    InsertToDatabase(newIvecoSalesPrice);
                } else {
                    ivecoSalesPrice.SalesPrice = ivecoPriceChangeAppDetail.IvecoPrice;
                    ivecoSalesPrice.ModifierId = ivecoPriceChangeApp.ModifierId;
                    ivecoSalesPrice.ModifierName = ivecoPriceChangeApp.ModifierName;
                    ivecoSalesPrice.ModifyTime = ivecoPriceChangeApp.ModifyTime;
                    new IvecoSalesPriceAch(this.DomainService).UpdateIvecoSalesPriceValidate(ivecoSalesPrice);
                }
                #region 2018-05-09 注释
                //新增配件销售价变更履历
                //var partsSalesPriceHistory = new PartsSalesPriceHistory {
                //    BranchId = ivecoPriceChangeApp.BranchId,
                //    PartsSalesCategoryId = ivecoPriceChangeApp.PartsSalesCategoryId,
                //    PartsSalesCategoryCode = ivecoPriceChangeApp.PartsSalesCategoryCode,
                //    PartsSalesCategoryName = ivecoPriceChangeApp.PartsSalesCategoryName,
                //    SparePartId = ivecoPriceChangeAppDetail.SparePartId,
                //    SparePartCode = ivecoPriceChangeAppDetail.SparePartCode,
                //    SparePartName = ivecoPriceChangeAppDetail.SparePartName,
                //    IfClaim = ivecoPriceChangeApp.IfClaim,
                //    SalesPrice = ivecoPriceChangeAppDetail.SalesPrice,
                //    PriceType = ivecoPriceChangeAppDetail.PriceType,
                //    Status = (int)DcsBaseDataStatus.有效,
                //    ValidationTime = ivecoPriceChangeAppDetail.ValidationTime,
                //    ExpireTime = ivecoPriceChangeAppDetail.ExpireTime,
                //    Remark = ivecoPriceChangeAppDetail.Remark
                //};
                //InsertToDatabase(partsSalesPriceHistory);
                //new PartsSalesPriceHistoryAch(this.DomainService).InsertPartsSalesPriceHistoryValidate(partsSalesPriceHistory);

                //只有价格大于0 才新增更新价格及其履历
                //if(ivecoPriceChangeAppDetail.RetailGuidePrice > 0) {
                //    //新增或更新配件零售指导价
                //    var partsRetailGuidePrice = partsRetailGuidePrices.FirstOrDefault(r => r.SparePartId == ivecoPriceChangeAppDetail.SparePartId);
                //    if(partsRetailGuidePrice == null) {
                //        var newPartsRetailGuidePrice = new PartsRetailGuidePrice {
                //            BranchId = ivecoPriceChangeApp.BranchId,
                //            PartsSalesCategoryId = ivecoPriceChangeApp.PartsSalesCategoryId,
                //            PartsSalesCategoryCode = ivecoPriceChangeApp.PartsSalesCategoryCode,
                //            PartsSalesCategoryName = ivecoPriceChangeApp.PartsSalesCategoryName,
                //            SparePartId = ivecoPriceChangeAppDetail.SparePartId,
                //            SparePartCode = ivecoPriceChangeAppDetail.SparePartCode,
                //            SparePartName = ivecoPriceChangeAppDetail.SparePartName,
                //            RetailGuidePrice = ivecoPriceChangeAppDetail.RetailGuidePrice != null ? ivecoPriceChangeAppDetail.RetailGuidePrice.Value : 0,
                //            ValidationTime = ivecoPriceChangeAppDetail.ValidationTime,
                //            ExpireTime = ivecoPriceChangeAppDetail.ExpireTime,
                //            Status = (int)DcsBaseDataStatus.有效,
                //            Remark = ivecoPriceChangeAppDetail.Remark
                //        };
                //        InsertToDatabase(newPartsRetailGuidePrice);
                //        new PartsRetailGuidePriceAch(this.DomainService).InsertPartsRetailGuidePriceValidate(newPartsRetailGuidePrice);
                //    } else {
                //        partsRetailGuidePrice.RetailGuidePrice = ivecoPriceChangeAppDetail.RetailGuidePrice ?? 0;
                //        partsRetailGuidePrice.ValidationTime = ivecoPriceChangeAppDetail.ValidationTime;
                //        partsRetailGuidePrice.ExpireTime = ivecoPriceChangeAppDetail.ExpireTime;
                //        new PartsRetailGuidePriceAch(this.DomainService).UpdatePartsRetailGuidePriceValidate(partsRetailGuidePrice);
                //    }
                //    //新增配件零售指导价变更履历
                //    var partsRetailGuidePriceHistory = new PartsRetailGuidePriceHistory {
                //        BranchId = ivecoPriceChangeApp.BranchId,
                //        PartsSalesCategoryId = ivecoPriceChangeApp.PartsSalesCategoryId,
                //        PartsSalesCategoryCode = ivecoPriceChangeApp.PartsSalesCategoryCode,
                //        PartsSalesCategoryName = ivecoPriceChangeApp.PartsSalesCategoryName,
                //        SparePartId = ivecoPriceChangeAppDetail.SparePartId,
                //        SparePartCode = ivecoPriceChangeAppDetail.SparePartCode,
                //        SparePartName = ivecoPriceChangeAppDetail.SparePartName,
                //        RetailGuidePrice = ivecoPriceChangeAppDetail.RetailGuidePrice.HasValue ? ivecoPriceChangeAppDetail.RetailGuidePrice.Value : 0,
                //        ValidationTime = ivecoPriceChangeAppDetail.ValidationTime,
                //        ExpireTime = ivecoPriceChangeAppDetail.ExpireTime,
                //        Status = (int)DcsBaseDataStatus.有效,
                //        Remark = ivecoPriceChangeAppDetail.Remark
                //    };
                //    InsertToDatabase(partsRetailGuidePriceHistory);
                //    new PartsRetailGuidePriceHistoryAch(this.DomainService).InsertPartsRetailGuidePriceHistoryValidate(partsRetailGuidePriceHistory);
                //}
               
                //只有价格大于0 才新增更新价格及其履历
                //if(ivecoPriceChangeAppDetail.DealerSalesPrice > 0) {
                //    //新增或更新服务站配件批发价
                //    var dealerPartsSalesPrice = dealerPartsSalesPrices.FirstOrDefault(r => r.SparePartId == ivecoPriceChangeAppDetail.SparePartId);
                //    if(dealerPartsSalesPrice == null) {
                //        var newDealerPartsSalesPrice = new DealerPartsSalesPrice {
                //            BranchId = ivecoPriceChangeApp.BranchId,
                //            PartsSalesCategoryId = ivecoPriceChangeApp.PartsSalesCategoryId,
                //            PartsSalesCategoryCode = ivecoPriceChangeApp.PartsSalesCategoryCode,
                //            PartsSalesCategoryName = ivecoPriceChangeApp.PartsSalesCategoryName,
                //            SparePartId = ivecoPriceChangeAppDetail.SparePartId,
                //            SparePartCode = ivecoPriceChangeAppDetail.SparePartCode,
                //            SparePartName = ivecoPriceChangeAppDetail.SparePartName,
                //            DealerSalesPrice = ivecoPriceChangeAppDetail.DealerSalesPrice ?? 0,
                //            Status = (int)DcsBaseDataStatus.有效,
                //            Remark = ivecoPriceChangeAppDetail.Remark
                //        };
                //        InsertToDatabase(newDealerPartsSalesPrice);
                //        new DealerPartsSalesPriceAch(this.DomainService).InsertDealerPartsSalesPriceValidate(newDealerPartsSalesPrice);
                //    } else {
                //        dealerPartsSalesPrice.DealerSalesPrice = ivecoPriceChangeAppDetail.DealerSalesPrice ?? 0;
                //        new DealerPartsSalesPriceAch(this.DomainService).UpdateDealerPartsSalesPriceValidate(dealerPartsSalesPrice);
                //    }
                //    //新增服务站配件批发价变更履历
                //    var dealerPartsSalesPriceHistory = new DealerPartsSalesPriceHistory {
                //        BranchId = ivecoPriceChangeApp.BranchId,
                //        PartsSalesCategoryId = ivecoPriceChangeApp.PartsSalesCategoryId,
                //        PartsSalesCategoryCode = ivecoPriceChangeApp.PartsSalesCategoryCode,
                //        PartsSalesCategoryName = ivecoPriceChangeApp.PartsSalesCategoryName,
                //        SparePartId = ivecoPriceChangeAppDetail.SparePartId,
                //        SparePartCode = ivecoPriceChangeAppDetail.SparePartCode,
                //        SparePartName = ivecoPriceChangeAppDetail.SparePartName,
                //        DealerSalesPrice = ivecoPriceChangeAppDetail.DealerSalesPrice ?? 0,
                //        Status = (int)DcsBaseDataStatus.有效,
                //        Remark = ivecoPriceChangeAppDetail.Remark
                //    };
                //    InsertToDatabase(dealerPartsSalesPriceHistory);
                //    new DealerPartsSalesPriceHistoryAch(this.DomainService).InsertDealerPartsSalesPriceHistoryValidate(dealerPartsSalesPriceHistory);
                //}
                #endregion
            }
        }


        public void 驳回依维柯销售价变更申请(IvecoPriceChangeApp ivecoPriceChangeApp) {
            var dbIvecoPriceChangeApp = ObjectContext.IvecoPriceChangeApps.Where(r => r.Id == ivecoPriceChangeApp.Id && (r.Status == (int)DcsIvecoPriceAppStatus.初审通过 || r.Status == (int)DcsIvecoPriceAppStatus.新增)).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbIvecoPriceChangeApp == null)
                throw new ValidationException(ErrorStrings.IvecoPriceChangeApp_Validation4);
            var userinfo = Utils.GetCurrentUserInfo();
            UpdateToDatabase(ivecoPriceChangeApp);
            ivecoPriceChangeApp.Status = (int)DcsIvecoPriceAppStatus.新增;
            this.UpdateIvecoPriceChangeAppValidate(ivecoPriceChangeApp);
        }


        public void 作废依维柯销售价变更申请(IvecoPriceChangeApp ivecoPriceChangeApp) {
            var dbIvecoPriceChangeApp = ObjectContext.IvecoPriceChangeApps.Where(r => r.Id == ivecoPriceChangeApp.Id && (r.Status == (int)DcsIvecoPriceAppStatus.初审通过 || r.Status == (int)DcsIvecoPriceAppStatus.新增)).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbIvecoPriceChangeApp == null)
                throw new ValidationException(ErrorStrings.IvecoPriceChangeApp_Validation5);
            var userinfo = Utils.GetCurrentUserInfo();
            UpdateToDatabase(ivecoPriceChangeApp);
            ivecoPriceChangeApp.Status = (int)DcsIvecoPriceAppStatus.作废;
            this.UpdateIvecoPriceChangeAppValidate(ivecoPriceChangeApp);
        }


        public void 修改依维柯销售价变更申请(IvecoPriceChangeApp ivecoPriceChangeApp) {
            CheckEntityState(ivecoPriceChangeApp);
            var dbIvecoPriceChangeApp = ObjectContext.IvecoPriceChangeApps.Where(r => r.Id == ivecoPriceChangeApp.Id && r.Status == (int)DcsIvecoPriceAppStatus.新增).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbIvecoPriceChangeApp == null)
                throw new ValidationException(ErrorStrings.IvecoPriceChangeApp_Validation1);
            var ivecoPriceChangeAppdetailcheck = ivecoPriceChangeApp.IvecoPriceChangeAppDetails.Where(r => (!ObjectContext.PartsBranches.Any(b => r.SparePartId == b.PartId && b.PartsSalesCategoryId == ivecoPriceChangeApp.PartsSalesCategoryId && b.Status == (int)DcsBaseDataStatus.有效)));
            if(ivecoPriceChangeAppdetailcheck.Any()) {
                string partscodes = " ";
                foreach(var c in ivecoPriceChangeAppdetailcheck) {
                    partscodes = partscodes + c.SparePartCode + " ";
                }
                throw new ValidationException(string.Format(ErrorStrings.IvecoPriceChangeApp_Validation6,partscodes));
            }

        }
        #region 2018-05-09 注释
        //public void 生成配件销售价变更申请(IvecoPriceChangeApp ivecoPriceChangeApp, List<IvecoPriceChangeAppDetail> ivecoPriceChangeAppDetails) {
        //    //var ivecoPriceChangeAppdetailcheck = ivecoPriceChangeAppDetails.Where(r => (!ObjectContext.PartsBranches.Any(b => r.SparePartId == b.PartId && b.PartsSalesCategoryId == ivecoPriceChangeApp.PartsSalesCategoryId && b.Status == (int)DcsBaseDataStatus.有效)));
        //    //if(ivecoPriceChangeAppdetailcheck.Any()) {
        //    //    string partscodes = " ";
        //    //    foreach(var c in ivecoPriceChangeAppdetailcheck) {
        //    //        partscodes = partscodes + c.SparePartCode + " ";
        //    //    }
        //    //    throw new ValidationException("清单中" + partscodes + "配件 不存在配件营销信息");
        //    //}
        //    var partsSalesCategory = ivecoPriceChangeAppDetails.GroupBy(r => r.PartsSalesCategoryName);
        //    foreach(var item in partsSalesCategory) {
        //        var details = ivecoPriceChangeAppDetails.Where(r => r.PartsSalesCategoryName == item.Key);
        //        var newIvecoPriceChangeApp = new IvecoPriceChangeApp();
        //        newIvecoPriceChangeApp.Code = ivecoPriceChangeApp.Code;
        //        newIvecoPriceChangeApp.Status = ivecoPriceChangeApp.Status;
        //        newIvecoPriceChangeApp.PartsSalesCategoryId = details.FirstOrDefault().PartsSalesCategoryId;
        //        InsertIvecoPriceChangeAppValidate(newIvecoPriceChangeApp);
        //        foreach(var detail in details) {
        //            var ivecoPriceChangeAppDetail = new IvecoPriceChangeAppDetail();
        //            ivecoPriceChangeAppDetail.SparePartId = detail.SparePartId;
        //            ivecoPriceChangeAppDetail.SparePartCode = detail.SparePartCode;
        //            ivecoPriceChangeAppDetail.SparePartName = detail.SparePartName;
        //            ivecoPriceChangeAppDetail.SalesPrice = detail.SalesPrice;
        //            ivecoPriceChangeAppDetail.RetailGuidePrice = detail.RetailGuidePrice;
        //            ivecoPriceChangeAppDetail.DealerSalesPrice = detail.DealerSalesPrice;
        //            ivecoPriceChangeAppDetail.PriceType = detail.PriceType;
        //            ivecoPriceChangeAppDetail.ValidationTime = detail.ValidationTime;
        //            ivecoPriceChangeAppDetail.ExpireTime = detail.ExpireTime;
        //            ivecoPriceChangeAppDetail.Remark = detail.Remark;
        //            ivecoPriceChangeAppDetail.RowVersion = detail.RowVersion;
        //            ivecoPriceChangeAppDetail.CategoryCode = detail.CategoryCode;
        //            ivecoPriceChangeAppDetail.CategoryName = detail.CategoryName;
        //            ivecoPriceChangeAppDetail.SalesPriceFluctuationRatio = detail.SalesPriceFluctuationRatio;
        //            ivecoPriceChangeAppDetail.RetailPriceFluctuationRatio = detail.RetailPriceFluctuationRatio;
        //            ivecoPriceChangeAppDetail.MaxSalesPriceFloating = detail.MaxSalesPriceFloating;
        //            ivecoPriceChangeAppDetail.MinSalesPriceFloating = detail.MinSalesPriceFloating;
        //            ivecoPriceChangeAppDetail.MaxRetailOrderPriceFloating = detail.MaxRetailOrderPriceFloating;
        //            ivecoPriceChangeAppDetail.MinRetailOrderPriceFloating = detail.MinRetailOrderPriceFloating;
        //            ivecoPriceChangeAppDetail.MaxPurchasePricing = detail.MaxPurchasePricing;
        //            ivecoPriceChangeAppDetail.IsUpsideDown = detail.IsUpsideDown;
        //            ivecoPriceChangeAppDetail.MaxExchangeSalePrice = detail.MaxExchangeSalePrice;
        //            newIvecoPriceChangeApp.IvecoPriceChangeAppDetails.Add(detail);
        //        }
        //        InsertToDatabase(newIvecoPriceChangeApp);
        //    }
        //    this.ObjectContext.SaveChanges();
        //}
        #endregion
        public void 新增依维柯销售价变更申请(IvecoPriceChangeApp ivecoPriceChangeApp) {
            var ivecoPriceChangeAppdetailcheck = ivecoPriceChangeApp.IvecoPriceChangeAppDetails.Where(r => (!ObjectContext.PartsBranches.Any(b => r.SparePartId == b.PartId && b.PartsSalesCategoryId == ivecoPriceChangeApp.PartsSalesCategoryId && b.Status == (int)DcsBaseDataStatus.有效)));
            if(ivecoPriceChangeAppdetailcheck.Any()) {
                string partscodes = " ";
                foreach(var c in ivecoPriceChangeAppdetailcheck) {
                    partscodes = partscodes + c.SparePartCode + " ";
                }
                throw new ValidationException(string.Format(ErrorStrings.IvecoPriceChangeApp_Validation6,partscodes));
            }
            InsertIvecoPriceChangeApp(ivecoPriceChangeApp);
        }


        public void 批量终审依维柯销售价变更申请(int[] ids) {
            var dbIvecoPriceChangeApps = ObjectContext.IvecoPriceChangeApps.Where(r => ids.Contains(r.Id) && r.Status == (int)DcsIvecoPriceAppStatus.初审通过).SetMergeOption(MergeOption.NoTracking);
            if(dbIvecoPriceChangeApps == null)
                throw new ValidationException(ErrorStrings.IvecoPriceChangeApp_Validation3);
            var userinfo = Utils.GetCurrentUserInfo();
            foreach(var ivecoPriceChangeApp in dbIvecoPriceChangeApps) {
                UpdateToDatabase(ivecoPriceChangeApp);
                ivecoPriceChangeApp.FinalApproverId = userinfo.Id;
                ivecoPriceChangeApp.FinalApproverName = userinfo.Name;
                ivecoPriceChangeApp.FinalApproveTime = System.DateTime.Now;
                ivecoPriceChangeApp.Status = (int)DcsIvecoPriceAppStatus.终审通过;
                this.UpdateIvecoPriceChangeAppValidate(ivecoPriceChangeApp);
                var ivecoPriceChangeAppDetails = ivecoPriceChangeApp.IvecoPriceChangeAppDetails.ToArray();
                var partsIds = ivecoPriceChangeAppDetails.Select(r => r.SparePartId).ToArray();
                var ivecoSalesPrices = ObjectContext.IvecoSalesPrices.Where(r => r.PartsSalesCategoryId == ivecoPriceChangeApp.PartsSalesCategoryId && partsIds.Contains(r.SparePartId)).ToArray();
                foreach(var ivecoPriceChangeAppDetail in ivecoPriceChangeAppDetails) {
                    //新增或更新配件销售价。
                    var ivecoSalesPrice = ivecoSalesPrices.FirstOrDefault(r => r.SparePartId == ivecoPriceChangeAppDetail.SparePartId);
                    if(ivecoSalesPrice == null) {
                        var newIvecoSalesPrice = new IvecoSalesPrice {
                            PartsSalesCategoryId = ivecoPriceChangeApp.PartsSalesCategoryId ?? 0,
                            PartsSalesCategoryName = ivecoPriceChangeApp.PartsSalesCategory.Name,
                            SparePartId = ivecoPriceChangeAppDetail.SparePartId ?? 0,
                            SparePartCode = ivecoPriceChangeAppDetail.SparePartCode,
                            SparePartName = ivecoPriceChangeAppDetail.SparePartName,
                            SalesPrice = ivecoPriceChangeAppDetail.IvecoPrice,
                            CreatorId = ivecoPriceChangeApp.CreatorId,
                            CreatorName = ivecoPriceChangeApp.CreatorName,
                            CreateTime = ivecoPriceChangeApp.CreateTime,
                            ModifierId = ivecoPriceChangeApp.ModifierId,
                            ModifierName = ivecoPriceChangeApp.ModifierName,
                            ModifyTime = ivecoPriceChangeApp.ModifyTime
                        };
                        InsertToDatabase(newIvecoSalesPrice);
                    } else {
                        ivecoSalesPrice.SalesPrice = ivecoPriceChangeAppDetail.IvecoPrice;
                        new IvecoSalesPriceAch(this.DomainService).UpdateIvecoSalesPriceValidate(ivecoSalesPrice);
                    }
                    #region 2018-05-09 注释
                    //新增配件销售价变更履历
                    //var partsSalesPriceHistory = new PartsSalesPriceHistory {
                    //    BranchId = ivecoPriceChangeApp.BranchId,
                    //    PartsSalesCategoryId = ivecoPriceChangeApp.PartsSalesCategoryId,
                    //    PartsSalesCategoryCode = ivecoPriceChangeApp.PartsSalesCategoryCode,
                    //    PartsSalesCategoryName = ivecoPriceChangeApp.PartsSalesCategoryName,
                    //    SparePartId = ivecoPriceChangeAppDetail.SparePartId,
                    //    SparePartCode = ivecoPriceChangeAppDetail.SparePartCode,
                    //    SparePartName = ivecoPriceChangeAppDetail.SparePartName,
                    //    IfClaim = ivecoPriceChangeApp.IfClaim,
                    //    SalesPrice = ivecoPriceChangeAppDetail.SalesPrice,
                    //    PriceType = ivecoPriceChangeAppDetail.PriceType,
                    //    Status = (int)DcsBaseDataStatus.有效,
                    //    ValidationTime = ivecoPriceChangeAppDetail.ValidationTime,
                    //    ExpireTime = ivecoPriceChangeAppDetail.ExpireTime,
                    //    Remark = ivecoPriceChangeAppDetail.Remark
                    //};
                    //InsertToDatabase(partsSalesPriceHistory);
                    //new PartsSalesPriceHistoryAch(this.DomainService).InsertPartsSalesPriceHistoryValidate(partsSalesPriceHistory);

                    ////只有价格大于0 才新增更新价格及其履历
                    //if(ivecoPriceChangeAppDetail.RetailGuidePrice > 0) {
                    //    //新增或更新配件零售指导价
                    //    var partsRetailGuidePrice = partsRetailGuidePrices.FirstOrDefault(r => r.SparePartId == ivecoPriceChangeAppDetail.SparePartId);
                    //    if(partsRetailGuidePrice == null) {
                    //        var newPartsRetailGuidePrice = new PartsRetailGuidePrice {
                    //            BranchId = ivecoPriceChangeApp.BranchId,
                    //            PartsSalesCategoryId = ivecoPriceChangeApp.PartsSalesCategoryId,
                    //            PartsSalesCategoryCode = ivecoPriceChangeApp.PartsSalesCategoryCode,
                    //            PartsSalesCategoryName = ivecoPriceChangeApp.PartsSalesCategoryName,
                    //            SparePartId = ivecoPriceChangeAppDetail.SparePartId,
                    //            SparePartCode = ivecoPriceChangeAppDetail.SparePartCode,
                    //            SparePartName = ivecoPriceChangeAppDetail.SparePartName,
                    //            RetailGuidePrice = ivecoPriceChangeAppDetail.RetailGuidePrice != null ? ivecoPriceChangeAppDetail.RetailGuidePrice.Value : 0,
                    //            ValidationTime = ivecoPriceChangeAppDetail.ValidationTime,
                    //            ExpireTime = ivecoPriceChangeAppDetail.ExpireTime,
                    //            Status = (int)DcsBaseDataStatus.有效,
                    //            Remark = ivecoPriceChangeAppDetail.Remark
                    //        };
                    //        InsertToDatabase(newPartsRetailGuidePrice);
                    //        new PartsRetailGuidePriceAch(this.DomainService).InsertPartsRetailGuidePriceValidate(newPartsRetailGuidePrice);
                    //    } else {
                    //        partsRetailGuidePrice.RetailGuidePrice = ivecoPriceChangeAppDetail.RetailGuidePrice ?? 0;
                    //        partsRetailGuidePrice.ValidationTime = ivecoPriceChangeAppDetail.ValidationTime;
                    //        partsRetailGuidePrice.ExpireTime = ivecoPriceChangeAppDetail.ExpireTime;
                    //        new PartsRetailGuidePriceAch(this.DomainService).UpdatePartsRetailGuidePriceValidate(partsRetailGuidePrice);
                    //    }
                    //    //新增配件零售指导价变更履历
                    //    var partsRetailGuidePriceHistory = new PartsRetailGuidePriceHistory {
                    //        BranchId = ivecoPriceChangeApp.BranchId,
                    //        PartsSalesCategoryId = ivecoPriceChangeApp.PartsSalesCategoryId,
                    //        PartsSalesCategoryCode = ivecoPriceChangeApp.PartsSalesCategoryCode,
                    //        PartsSalesCategoryName = ivecoPriceChangeApp.PartsSalesCategoryName,
                    //        SparePartId = ivecoPriceChangeAppDetail.SparePartId,
                    //        SparePartCode = ivecoPriceChangeAppDetail.SparePartCode,
                    //        SparePartName = ivecoPriceChangeAppDetail.SparePartName,
                    //        RetailGuidePrice = ivecoPriceChangeAppDetail.RetailGuidePrice.HasValue ? ivecoPriceChangeAppDetail.RetailGuidePrice.Value : 0,
                    //        ValidationTime = ivecoPriceChangeAppDetail.ValidationTime,
                    //        ExpireTime = ivecoPriceChangeAppDetail.ExpireTime,
                    //        Status = (int)DcsBaseDataStatus.有效,
                    //        Remark = ivecoPriceChangeAppDetail.Remark
                    //    };
                    //    InsertToDatabase(partsRetailGuidePriceHistory);
                    //    new PartsRetailGuidePriceHistoryAch(this.DomainService).InsertPartsRetailGuidePriceHistoryValidate(partsRetailGuidePriceHistory);
                    //}

                    ////只有价格大于0 才新增更新价格及其履历
                    //if(ivecoPriceChangeAppDetail.DealerSalesPrice > 0) {
                    //    //新增或更新服务站配件批发价
                    //    var dealerPartsSalesPrice = dealerPartsSalesPrices.FirstOrDefault(r => r.SparePartId == ivecoPriceChangeAppDetail.SparePartId);
                    //    if(dealerPartsSalesPrice == null) {
                    //        var newDealerPartsSalesPrice = new DealerPartsSalesPrice {
                    //            BranchId = ivecoPriceChangeApp.BranchId,
                    //            PartsSalesCategoryId = ivecoPriceChangeApp.PartsSalesCategoryId,
                    //            PartsSalesCategoryCode = ivecoPriceChangeApp.PartsSalesCategoryCode,
                    //            PartsSalesCategoryName = ivecoPriceChangeApp.PartsSalesCategoryName,
                    //            SparePartId = ivecoPriceChangeAppDetail.SparePartId,
                    //            SparePartCode = ivecoPriceChangeAppDetail.SparePartCode,
                    //            SparePartName = ivecoPriceChangeAppDetail.SparePartName,
                    //            DealerSalesPrice = ivecoPriceChangeAppDetail.DealerSalesPrice ?? 0,
                    //            Status = (int)DcsBaseDataStatus.有效,
                    //            Remark = ivecoPriceChangeAppDetail.Remark
                    //        };
                    //        InsertToDatabase(newDealerPartsSalesPrice);
                    //        new DealerPartsSalesPriceAch(this.DomainService).InsertDealerPartsSalesPriceValidate(newDealerPartsSalesPrice);
                    //    } else {
                    //        dealerPartsSalesPrice.DealerSalesPrice = ivecoPriceChangeAppDetail.DealerSalesPrice ?? 0;
                    //        new DealerPartsSalesPriceAch(this.DomainService).UpdateDealerPartsSalesPriceValidate(dealerPartsSalesPrice);
                    //    }
                    //    //新增服务站配件批发价变更履历
                    //    var dealerPartsSalesPriceHistory = new DealerPartsSalesPriceHistory {
                    //        BranchId = ivecoPriceChangeApp.BranchId,
                    //        PartsSalesCategoryId = ivecoPriceChangeApp.PartsSalesCategoryId,
                    //        PartsSalesCategoryCode = ivecoPriceChangeApp.PartsSalesCategoryCode,
                    //        PartsSalesCategoryName = ivecoPriceChangeApp.PartsSalesCategoryName,
                    //        SparePartId = ivecoPriceChangeAppDetail.SparePartId,
                    //        SparePartCode = ivecoPriceChangeAppDetail.SparePartCode,
                    //        SparePartName = ivecoPriceChangeAppDetail.SparePartName,
                    //        DealerSalesPrice = ivecoPriceChangeAppDetail.DealerSalesPrice ?? 0,
                    //        Status = (int)DcsBaseDataStatus.有效,
                    //        Remark = ivecoPriceChangeAppDetail.Remark
                    //    };
                    //    InsertToDatabase(dealerPartsSalesPriceHistory);
                    //    new DealerPartsSalesPriceHistoryAch(this.DomainService).InsertDealerPartsSalesPriceHistoryValidate(dealerPartsSalesPriceHistory);
                    //}
                    #endregion
                }
                ObjectContext.SaveChanges();
            }

        }

        public IQueryable<GetAllPartsSalesPrice> 查询依维柯销售价品牌(string partsSalesCategoryName) {
            IQueryable<GetAllPartsSalesPrice> partsSalesPrices = ObjectContext.GetAllPartsSalesPrices;
            if(!string.IsNullOrEmpty(partsSalesCategoryName))
                return partsSalesPrices = partsSalesPrices.Where(r => r.PartsSalesCategoryName == partsSalesCategoryName && r.Status != (int)DcsBaseDataStatus.作废);
            return partsSalesPrices = partsSalesPrices.Where(r => r.Status != (int)DcsBaseDataStatus.作废);
        }

        //public IvecoPriceChangeApp 查询依维柯销售价申请单详单附带虚拟字段(int id) {
        //    var dbPrtsSalesPriceChange = ObjectContext.IvecoPriceChangeApps.SingleOrDefault(e => e.Id == id);
        //    if(dbPrtsSalesPriceChange != null) {
        //        var dbDetails = ObjectContext.IvecoPriceChangeAppDetails.Where(ex => ex.ParentId == dbPrtsSalesPriceChange.Id).ToArray();
        //        var sparePartIds = dbDetails.Select(r => r.SparePartId).ToArray();
        //        if(dbDetails.Any()) {
        //            var partsPurchasePricings = ObjectContext.PartsPurchasePricings.Where(r => sparePartIds.Contains(r.PartId) && r.Status == (int)DcsPartsPurchasePricingStatus.生效 && r.PartsSalesCategoryId == dbPrtsSalesPriceChange.PartsSalesCategoryId).ToArray();

        //            var partsSalesPrices = ObjectContext.PartsSalesPrices.Where(r => sparePartIds.Contains(r.SparePartId) && r.Status == (int)DcsBaseDataStatus.有效 && r.PartsSalesCategoryId == dbPrtsSalesPriceChange.PartsSalesCategoryId && r.BranchId == dbPrtsSalesPriceChange.BranchId).ToArray();
        //            var partsRetailGuidePrices = ObjectContext.PartsRetailGuidePrices.Where(r => sparePartIds.Contains(r.SparePartId) && r.Status == (int)DcsBaseDataStatus.有效 && r.PartsSalesCategoryId == dbPrtsSalesPriceChange.PartsSalesCategoryId && r.BranchId == dbPrtsSalesPriceChange.BranchId).ToArray();

        //            foreach(var ivecoPriceChangeAppDetail in dbDetails) {
        //                var partsPurchasePricing = partsPurchasePricings.FirstOrDefault(r => r.PartId == ivecoPriceChangeAppDetail.SparePartId);
        //                var partsSalesPrice = partsSalesPrices.FirstOrDefault(r => r.SparePartId == ivecoPriceChangeAppDetail.SparePartId);
        //                var partsRetailGuidePrice = partsRetailGuidePrices.FirstOrDefault(r => r.SparePartId == ivecoPriceChangeAppDetail.SparePartId);
        //                ivecoPriceChangeAppDetail.PurchasePrice = partsPurchasePricing != null ? partsPurchasePricing.PurchasePrice : 0;
        //                ivecoPriceChangeAppDetail.IncreaseRateSale = (partsPurchasePricing == null || partsPurchasePricing.PurchasePrice == 0 || ivecoPriceChangeAppDetail.SalesPrice == 0) ? 0 : ivecoPriceChangeAppDetail.SalesPrice / ivecoPriceChangeAppDetail.PurchasePrice - 1;
        //                ivecoPriceChangeAppDetail.IncreaseRateRetail = (ivecoPriceChangeAppDetail.SalesPrice == 0 || ivecoPriceChangeAppDetail.RetailGuidePrice == null) ? 0 : ivecoPriceChangeAppDetail.RetailGuidePrice.Value / ivecoPriceChangeAppDetail.SalesPrice - 1;
        //                ivecoPriceChangeAppDetail.IncreaseRateDealerSalesPrice = (ivecoPriceChangeAppDetail.SalesPrice == 0 || ivecoPriceChangeAppDetail.DealerSalesPrice == null || ivecoPriceChangeAppDetail.DealerSalesPrice == 0) ? 0 : ivecoPriceChangeAppDetail.DealerSalesPrice.Value / ivecoPriceChangeAppDetail.SalesPrice - 1;
        //                ivecoPriceChangeAppDetail.PartsSalesPrice_SalePrice = partsSalesPrice != null ? partsSalesPrice.SalesPrice : 0;
        //                ivecoPriceChangeAppDetail.PartsRetailGuidePrice_RetailGuidePrice = partsRetailGuidePrice != null ? partsRetailGuidePrice.RetailGuidePrice : 0;
        //                ivecoPriceChangeAppDetail.SalesPriceFluctuationRatio = (ivecoPriceChangeAppDetail.SalesPrice == 0 || ivecoPriceChangeAppDetail.PartsRetailGuidePrice_RetailGuidePrice == 0) ? 0 : Convert.ToDouble(ivecoPriceChangeAppDetail.SalesPrice / ivecoPriceChangeAppDetail.PartsRetailGuidePrice_RetailGuidePrice) - 1;
        //                ivecoPriceChangeAppDetail.RetailPriceFluctuationRatio = (ivecoPriceChangeAppDetail.RetailGuidePrice == 0 || ivecoPriceChangeAppDetail.PartsRetailGuidePrice_RetailGuidePrice == 0) ? 0 : Convert.ToDouble(ivecoPriceChangeAppDetail.RetailGuidePrice / ivecoPriceChangeAppDetail.PartsRetailGuidePrice_RetailGuidePrice) - 1;
        //                var partsPriceIncreaseRate = ObjectContext.PartsPriceIncreaseRates.FirstOrDefault(r => r.BrandId == dbPrtsSalesPriceChange.BranchId && r.CategoryCode == ivecoPriceChangeAppDetail.CategoryCode && r.Status == (int)DcsBaseDataStatus.有效);
        //                if(partsPriceIncreaseRate != null) {
        //                    ivecoPriceChangeAppDetail.MaxSalesPriceFloating = partsPriceIncreaseRate.MaxSalesPriceFloating;
        //                    ivecoPriceChangeAppDetail.MinSalesPriceFloating = partsPriceIncreaseRate.MinSalesPriceFloating;
        //                    ivecoPriceChangeAppDetail.MaxRetailOrderPriceFloating = partsPriceIncreaseRate.MaxRetailOrderPriceFloating;
        //                    ivecoPriceChangeAppDetail.MinRetailOrderPriceFloating = partsPriceIncreaseRate.MinRetailOrderPriceFloating;
        //                }
        //            }
        //        }
        //    }
        //    return dbPrtsSalesPriceChange;
        //}

    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:11:57
        //原分布类的函数全部转移到Ach                                                               
        /// <summary>
        /// 仅供客户端使用
        /// </summary>
        [Update(UsingCustomMethod = true)]
        public void 初审依维柯销售价变更申请(IvecoPriceChangeApp ivecoPriceChangeApp) {
            new IvecoPriceChangeAppAch(this).初审依维柯销售价变更申请(ivecoPriceChangeApp);
        }


        [Invoke(HasSideEffects = true)]
        public void 批量初审依维柯销售价变更申请(int[] ids) {
            new IvecoPriceChangeAppAch(this).批量初审依维柯销售价变更申请(ids);
        }


        [Update(UsingCustomMethod = true)]
        public void 终审依维柯销售价变更申请(IvecoPriceChangeApp ivecoPriceChangeApp) {
            new IvecoPriceChangeAppAch(this).终审依维柯销售价变更申请(ivecoPriceChangeApp);
        }


        [Update(UsingCustomMethod = true)]
        public void 驳回依维柯销售价变更申请(IvecoPriceChangeApp ivecoPriceChangeApp) {
            new IvecoPriceChangeAppAch(this).驳回依维柯销售价变更申请(ivecoPriceChangeApp);
        }


        [Update(UsingCustomMethod = true)]
        public void 作废依维柯销售价变更申请(IvecoPriceChangeApp ivecoPriceChangeApp) {
            new IvecoPriceChangeAppAch(this).作废依维柯销售价变更申请(ivecoPriceChangeApp);
        }


        [Update(UsingCustomMethod = true)]
        public void 修改依维柯销售价变更申请(IvecoPriceChangeApp ivecoPriceChangeApp) {
            new IvecoPriceChangeAppAch(this).修改依维柯销售价变更申请(ivecoPriceChangeApp);
        }


        [Update(UsingCustomMethod = true)]
        public void 新增依维柯销售价变更申请(IvecoPriceChangeApp ivecoPriceChangeApp) {
            new IvecoPriceChangeAppAch(this).新增依维柯销售价变更申请(ivecoPriceChangeApp);
        }

        //[Invoke]
        //public void 生成配件销售价变更申请(IvecoPriceChangeApp ivecoPriceChangeApp, List<IvecoPriceChangeAppDetail> ivecoPriceChangeAppDetails) {
        //    new IvecoPriceChangeAppAch(this).生成配件销售价变更申请(ivecoPriceChangeApp, ivecoPriceChangeAppDetails);
        //}

        [Invoke(HasSideEffects = true)]
        public void 批量终审依维柯销售价变更申请(int[] ids) {
            new IvecoPriceChangeAppAch(this).批量终审依维柯销售价变更申请(ids);
        }

        //public IQueryable<GetAllPartsSalesPrice> 查询依维柯销售价品牌(string partsSalesCategoryName) {
        //    return new IvecoPriceChangeAppAch(this).查询依维柯销售价品牌(partsSalesCategoryName);
        //}

        //public IvecoPriceChangeApp 查询依维柯销售价申请单详单附带虚拟字段(int id) {
        //    return new IvecoPriceChangeAppAch(this).查询依维柯销售价申请单详单附带虚拟字段(id);
        //}
    }
}
