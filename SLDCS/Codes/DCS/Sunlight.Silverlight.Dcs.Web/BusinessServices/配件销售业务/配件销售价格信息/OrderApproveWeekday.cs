﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class OrderApproveWeekdayAch : DcsSerivceAchieveBase {
        public OrderApproveWeekdayAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 作废储备订单自动审核日与省份关系(OrderApproveWeekday orderApproveWeekday) {
            var dborderApproveWeekday = ObjectContext.OrderApproveWeekdays.Where(r => r.Id == orderApproveWeekday.Id && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dborderApproveWeekday == null)
                throw new ValidationException(ErrorStrings.OrderApproveWeekday_Validation1);
            UpdateToDatabase(orderApproveWeekday);
            orderApproveWeekday.Status = (int)DcsMasterDataStatus.作废;
            var userInfo = Utils.GetCurrentUserInfo();
            orderApproveWeekday.AbandonerId = userInfo.Id;
            orderApproveWeekday.AbandonerName = userInfo.Name;
            orderApproveWeekday.AbandonTime = DateTime.Now;
            this.UpdateOrderApproveWeekdayValidate(orderApproveWeekday);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:11:57
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 作废储备订单自动审核日与省份关系(OrderApproveWeekday orderApproveWeekday) {
            new OrderApproveWeekdayAch(this).作废储备订单自动审核日与省份关系(orderApproveWeekday);
        }
    }
}
