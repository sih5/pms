﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsRetailGuidePriceAch : DcsSerivceAchieveBase {
        public PartsRetailGuidePriceAch(DcsDomainService domainService)
            : base(domainService) {
        }

        /// <summary>
        /// 仅供客户端使用
        /// </summary>

        public void 修改配件零售指导价(PartsRetailGuidePrice partsRetailGuidePrice) {
            CheckEntityState(partsRetailGuidePrice);
            var dbpartsRetailGuidePrice = ChangeSet.GetOriginal(partsRetailGuidePrice);
            if(dbpartsRetailGuidePrice == null)
                return;
            if(dbpartsRetailGuidePrice.Status != (int)DcsBaseDataStatus.有效)
                throw new ValidationException(ErrorStrings.PartsRetailGuidePrice_Validation2);
            CreatePartsRetailGuidePriceHistory(dbpartsRetailGuidePrice);
        }

        public void 作废配件零售指导价(PartsRetailGuidePrice partsRetailGuidePrice) {
            var dbpartsRetailGuidePrice = ObjectContext.PartsRetailGuidePrices.Where(r => r.Id == partsRetailGuidePrice.Id && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbpartsRetailGuidePrice == null)
                throw new ValidationException(ErrorStrings.PartsRetailGuidePrice_Validation3);
            CreatePartsRetailGuidePriceHistory(dbpartsRetailGuidePrice);
            UpdateToDatabase(partsRetailGuidePrice);
            partsRetailGuidePrice.Status = (int)DcsBaseDataStatus.作废;
            var userInfo = Utils.GetCurrentUserInfo();
            partsRetailGuidePrice.AbandonerId = userInfo.Id;
            partsRetailGuidePrice.AbandonerName = userInfo.Name;
            partsRetailGuidePrice.AbandonTime = DateTime.Now;
            this.UpdatePartsRetailGuidePriceValidate(partsRetailGuidePrice);
        }
        internal void CreatePartsRetailGuidePriceHistory(PartsRetailGuidePrice partsRetailGuidePrice) {
            var partsRetailGuidePriceHistory = new PartsRetailGuidePriceHistory {
                SparePartId = partsRetailGuidePrice.SparePartId,
                SparePartCode = partsRetailGuidePrice.SparePartCode,
                SparePartName = partsRetailGuidePrice.SparePartName,
                RetailGuidePrice = partsRetailGuidePrice.RetailGuidePrice,
                ValidationTime = partsRetailGuidePrice.ValidationTime,
                ExpireTime = partsRetailGuidePrice.ExpireTime,
                Status = partsRetailGuidePrice.Status,
                Remark = partsRetailGuidePrice.Remark
            };
            InsertToDatabase(partsRetailGuidePriceHistory);
            new PartsRetailGuidePriceHistoryAch(this.DomainService).InsertPartsRetailGuidePriceHistoryValidate(partsRetailGuidePriceHistory);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:11:57
        //原分布类的函数全部转移到Ach                                                               
        /// <summary>
        /// 仅供客户端使用
        /// </summary>
        [Update(UsingCustomMethod = true)]
        public void 修改配件零售指导价(PartsRetailGuidePrice partsRetailGuidePrice) {
            new PartsRetailGuidePriceAch(this).修改配件零售指导价(partsRetailGuidePrice);
        }
        
        [Update(UsingCustomMethod = true)]
        public void 作废配件零售指导价(PartsRetailGuidePrice partsRetailGuidePrice) {
            new PartsRetailGuidePriceAch(this).作废配件零售指导价(partsRetailGuidePrice);
        }
    }
}
