﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Web;using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsSalePriceIncreaseRateAch : DcsSerivceAchieveBase {
        public PartsSalePriceIncreaseRateAch(DcsDomainService domainService)
            : base(domainService) {
        }

        public void 作废配件销售加价率(PartsSalePriceIncreaseRate pspir) {
            var dbpspir = this.ObjectContext.PartsSalePriceIncreaseRates.Where(v => v.Id == pspir.Id && v.status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbpspir == null)
                throw new ValidationException(ErrorStrings.PartsSalePriceIncreaseRate_Validation1);
            UpdateToDatabase(pspir);
            pspir.status = (int)DcsBaseDataStatus.作废;
            var userInfo = Utils.GetCurrentUserInfo();
            pspir.AbandonerId = userInfo.Id;
            pspir.AbandonerName = userInfo.Name;
            pspir.AbandonTime = DateTime.Now;
            this.UpdatePartsSalePriceIncreaseRateValidate(pspir);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:11:57
        //原分布类的函数全部转移到Ach                                                               

        public void 作废配件销售加价率(PartsSalePriceIncreaseRate pspir) {
            new PartsSalePriceIncreaseRateAch(this).作废配件销售加价率(pspir);
        }
    }
}
