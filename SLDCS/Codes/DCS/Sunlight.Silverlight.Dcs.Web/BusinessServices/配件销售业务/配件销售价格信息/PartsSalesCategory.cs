﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsSalesCategoryAch : DcsSerivceAchieveBase {
        public PartsSalesCategoryAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 作废配件销售类型(PartsSalesCategory partsSalesCategory) {
            var dbpartsSalesCategory = ObjectContext.PartsSalesCategories.Where(r => r.Id == partsSalesCategory.Id && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbpartsSalesCategory == null)
                throw new ValidationException(ErrorStrings.PartsSalesCategory_Validation3);
            UpdateToDatabase(partsSalesCategory);
            dbpartsSalesCategory.Status = (int)DcsBaseDataStatus.作废;
            var userInfo = Utils.GetCurrentUserInfo();
            partsSalesCategory.AbandonerId = userInfo.Id;
            partsSalesCategory.AbandonerName = userInfo.Name;
            partsSalesCategory.AbandonTime = DateTime.Now;
            this.UpdatePartsSalesCategoryValidate(partsSalesCategory);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:11:57
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 作废配件销售类型(PartsSalesCategory partsSalesCategory) {
            new PartsSalesCategoryAch(this).作废配件销售类型(partsSalesCategory);
        }
    }
}
