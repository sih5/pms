﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsSalesOrderTypeAch : DcsSerivceAchieveBase {
        public PartsSalesOrderTypeAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 作废配件销售订单类型(PartsSalesOrderType partsSalesOrderType) {
            var dbpartsSalesOrderType = ObjectContext.PartsSalesOrderTypes.Where(r => r.Id == partsSalesOrderType.Id && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbpartsSalesOrderType == null)
                throw new ValidationException(ErrorStrings.PartsSalesOrderType_Validation3);
            UpdateToDatabase(partsSalesOrderType);
            partsSalesOrderType.Status = (int)DcsBaseDataStatus.作废;
            this.UpdatePartsSalesOrderTypeValidate(partsSalesOrderType);
            var userInfo = Utils.GetCurrentUserInfo();
            partsSalesOrderType.AbandonerId = userInfo.Id;
            partsSalesOrderType.AbandonerName = userInfo.Name;
            partsSalesOrderType.AbandonTime = DateTime.Now;
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:11:57
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 作废配件销售订单类型(PartsSalesOrderType partsSalesOrderType) {
            new PartsSalesOrderTypeAch(this).作废配件销售订单类型(partsSalesOrderType);
        }
    }
}
