﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsSalesPriceAch : DcsSerivceAchieveBase {
        public PartsSalesPriceAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 作废配件销售价(PartsSalesPrice partsSalesPrice) {
            var dbpartsSalesPrice = ObjectContext.PartsSalesPrices.Where(r => r.Id == partsSalesPrice.Id && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbpartsSalesPrice == null)
                throw new ValidationException(ErrorStrings.PartsSalesPrice_Validation3);
            var partsSalesPriceHistory = new PartsSalesPriceHistory {
                PartsSalesCategoryId = dbpartsSalesPrice.PartsSalesCategoryId,
                PartsSalesCategoryCode = dbpartsSalesPrice.PartsSalesCategoryCode,
                PartsSalesCategoryName = dbpartsSalesPrice.PartsSalesCategoryName,
                SparePartId = dbpartsSalesPrice.SparePartId,
                SparePartCode = dbpartsSalesPrice.SparePartCode,
                SparePartName = dbpartsSalesPrice.SparePartName,
                SalesPrice = dbpartsSalesPrice.SalesPrice,
                PriceType = dbpartsSalesPrice.PriceType,
                Status = dbpartsSalesPrice.Status,
                ValidationTime = dbpartsSalesPrice.ValidationTime,
                ExpireTime = dbpartsSalesPrice.ExpireTime,
                Remark = dbpartsSalesPrice.Remark
            };
            InsertToDatabase(partsSalesPriceHistory);
            new PartsSalesPriceHistoryAch(this.DomainService).InsertPartsSalesPriceHistoryValidate(partsSalesPriceHistory);
            UpdateToDatabase(partsSalesPrice);
            partsSalesPrice.Status = (int)DcsMasterDataStatus.作废;
            var userInfo = Utils.GetCurrentUserInfo();
            partsSalesPrice.AbandonerId = userInfo.Id;
            partsSalesPrice.AbandonerName = userInfo.Name;
            partsSalesPrice.AbandonTime = DateTime.Now;
            this.UpdatePartsSalesPriceValidate(partsSalesPrice);
        }
        /// <summary>
        /// 仅允许客户端调度
        /// </summary>
        /// <param name="partsSalesPrice"></param>

        public void 修改配件销售价(PartsSalesPrice partsSalesPrice) {
            CheckEntityState(partsSalesPrice);
            var originalpartsSalesPrice = ChangeSet.GetOriginal(partsSalesPrice);
            if(originalpartsSalesPrice == null)
                return;
            if(originalpartsSalesPrice.Status != (int)DcsBaseDataStatus.有效)
                throw new ValidationException(ErrorStrings.PartsSalesPrice_Validation2);
            var partsSalesPriceHistory = new PartsSalesPriceHistory {
                PartsSalesCategoryId = originalpartsSalesPrice.PartsSalesCategoryId,
                PartsSalesCategoryCode = originalpartsSalesPrice.PartsSalesCategoryCode,
                PartsSalesCategoryName = originalpartsSalesPrice.PartsSalesCategoryName,
                SparePartId = originalpartsSalesPrice.SparePartId,
                SparePartCode = originalpartsSalesPrice.SparePartCode,
                SparePartName = originalpartsSalesPrice.SparePartName,
                SalesPrice = originalpartsSalesPrice.SalesPrice,
                PriceType = originalpartsSalesPrice.PriceType,
                Status = originalpartsSalesPrice.Status,
                ValidationTime = originalpartsSalesPrice.ValidationTime,
                ExpireTime = originalpartsSalesPrice.ExpireTime,
                Remark = originalpartsSalesPrice.Remark
            };
            InsertToDatabase(partsSalesPriceHistory);
            new PartsSalesPriceHistoryAch(this.DomainService).InsertPartsSalesPriceHistoryValidate(partsSalesPriceHistory);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:11:57
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 作废配件销售价(PartsSalesPrice partsSalesPrice) {
            new PartsSalesPriceAch(this).作废配件销售价(partsSalesPrice);
        }
        /// <summary>
        /// 仅允许客户端调度
        /// </summary>
        /// <param name="partsSalesPrice"></param>
        [Update(UsingCustomMethod = true)]
        public void 修改配件销售价(PartsSalesPrice partsSalesPrice) {
            new PartsSalesPriceAch(this).修改配件销售价(partsSalesPrice);
        }
    }
}
