﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsSalesPriceChangeAch : DcsSerivceAchieveBase {
        public PartsSalesPriceChangeAch(DcsDomainService domainService)
            : base(domainService) {
        }

        /// <summary>
        /// 仅供客户端使用
        /// </summary>

        public void 初审配件销售价格变更申请(PartsSalesPriceChange partsSalesPriceChange) {
            var dbPartsSalesPriceChange = ObjectContext.PartsSalesPriceChanges.Include("PartsSalesPriceChangeDetails").Where(r => r.Id == partsSalesPriceChange.Id && r.Status == (int)DcsPartsSalesPriceChangeStatus.提交).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbPartsSalesPriceChange == null)
                throw new ValidationException(ErrorStrings.PartsSalesPriceChange_Validation2);
            // //校验价格
            this.validatePrice(dbPartsSalesPriceChange.PartsSalesPriceChangeDetails.ToList());
            var userinfo = Utils.GetCurrentUserInfo();
            if(userinfo.Id==0){
                throw new ValidationException(ErrorStrings.User_Invalid);
            }
            UpdateToDatabase(partsSalesPriceChange);
            partsSalesPriceChange.InitialApproverId = userinfo.Id;
            partsSalesPriceChange.InitialApproverName = userinfo.Name;
            partsSalesPriceChange.InitialApproveTime = System.DateTime.Now;
            partsSalesPriceChange.Status = (int)DcsPartsSalesPriceChangeStatus.初审通过;
            this.UpdatePartsSalesPriceChangeValidate(partsSalesPriceChange);
        }

        public void 审核配件销售价格变更申请(PartsSalesPriceChange partsSalesPriceChange) {
            var dbPartsSalesPriceChange = ObjectContext.PartsSalesPriceChanges.Include("PartsSalesPriceChangeDetails").Where(r => r.Id == partsSalesPriceChange.Id && r.Status == (int)DcsPartsSalesPriceChangeStatus.初审通过).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbPartsSalesPriceChange == null)
                throw new ValidationException("只能审核处于“初审通过”状态下的单据");
            // //校验价格
            this.validatePrice(dbPartsSalesPriceChange.PartsSalesPriceChangeDetails.ToList());
            var userinfo = Utils.GetCurrentUserInfo();
            if(userinfo.Id == 0) {
                throw new ValidationException(ErrorStrings.PartsPurchasePricingChange_Approve_Init);
            }
            UpdateToDatabase(partsSalesPriceChange);
            partsSalesPriceChange.ApproverId = userinfo.Id;
            partsSalesPriceChange.ApproverName = userinfo.Name;
            partsSalesPriceChange.ApproveTime = System.DateTime.Now;
            partsSalesPriceChange.Status = (int)DcsPartsSalesPriceChangeStatus.审核通过;
            this.UpdatePartsSalesPriceChangeValidate(partsSalesPriceChange);
        }

        public void 批量初审配件销售价格变更申请(int[] ids) {
            var dbPartsSalesPriceChanges = ObjectContext.PartsSalesPriceChanges.Where(r => ids.Contains(r.Id) && r.Status == (int)DcsPartsSalesPriceChangeStatus.提交).SetMergeOption(MergeOption.NoTracking);
            if(dbPartsSalesPriceChanges == null)
                throw new ValidationException(ErrorStrings.PartsSalesPriceChange_Validation2);
            var userinfo = Utils.GetCurrentUserInfo();
            if(userinfo.Id == 0) {
                throw new ValidationException(ErrorStrings.User_Invalid);
            }
            // //校验价格
            var changeIds = dbPartsSalesPriceChanges.Select(r => r.Id).ToArray();
            var detail = ObjectContext.PartsSalesPriceChangeDetails.Where(r => changeIds.Contains(r.ParentId)).ToList();
            this.validatePrice(detail);
            foreach(var partsSalesPriceChange in dbPartsSalesPriceChanges) {
                UpdateToDatabase(partsSalesPriceChange);
                partsSalesPriceChange.InitialApproverId = userinfo.Id;
                partsSalesPriceChange.InitialApproverName = userinfo.Name;
                partsSalesPriceChange.InitialApproveTime = System.DateTime.Now;
                partsSalesPriceChange.Status = (int)DcsPartsSalesPriceChangeStatus.初审通过;
                this.UpdatePartsSalesPriceChangeValidate(partsSalesPriceChange);
            }
            ObjectContext.SaveChanges();
        }


        public void 终审配件销售价格变更申请(PartsSalesPriceChange partsSalesPriceChange) {
            var dbPartsSalesPriceChange = ObjectContext.PartsSalesPriceChanges.Where(r => r.Id == partsSalesPriceChange.Id && r.Status == (int)DcsPartsSalesPriceChangeStatus.审核通过).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbPartsSalesPriceChange == null)
                throw new ValidationException(ErrorStrings.PartsSalesPriceChange_Validation3);
            var userinfo = Utils.GetCurrentUserInfo();
            if(userinfo.Id == 0) {
                throw new ValidationException(ErrorStrings.User_Invalid);
            }
            UpdateToDatabase(partsSalesPriceChange);
            partsSalesPriceChange.FinalApproverId = userinfo.Id;
            partsSalesPriceChange.FinalApproverName = userinfo.Name;
            partsSalesPriceChange.FinalApproveTime = System.DateTime.Now;
            partsSalesPriceChange.Status = (int)DcsPartsSalesPriceChangeStatus.生效;
            this.UpdatePartsSalesPriceChangeValidate(partsSalesPriceChange);
            var partsSalesPriceChangeDetails = partsSalesPriceChange.PartsSalesPriceChangeDetails.ToArray();
            // //校验价格
            this.validatePrice(partsSalesPriceChange.PartsSalesPriceChangeDetails.ToList());

            var partsIds = partsSalesPriceChangeDetails.Select(r => r.SparePartId).ToArray();
            var priceTypes = partsSalesPriceChangeDetails.Select(r => r.PriceType).ToArray();
            var partsSalesPrices = ObjectContext.PartsSalesPrices.Where(r => r.PartsSalesCategoryId == partsSalesPriceChange.PartsSalesCategoryId && partsIds.Contains(r.SparePartId) && r.Status != (int)DcsBaseDataStatus.作废).ToArray();
            var partsRetailGuidePrices = ObjectContext.PartsRetailGuidePrices.Where(r => r.PartsSalesCategoryId == partsSalesPriceChange.PartsSalesCategoryId && partsIds.Contains(r.SparePartId) && r.Status != (int)DcsBaseDataStatus.作废).ToArray();
            var dealerPartsSalesPrices = ObjectContext.DealerPartsSalesPrices.Where(r => r.PartsSalesCategoryId == partsSalesPriceChange.PartsSalesCategoryId && partsIds.Contains(r.SparePartId) && r.Status != (int)DcsBaseDataStatus.作废).ToArray();
            foreach(var partsSalesPriceChangeDetail in partsSalesPriceChangeDetails) {
                //新增或更新配件销售价。
                var partsSalesPrice = partsSalesPrices.FirstOrDefault(r => r.SparePartId == partsSalesPriceChangeDetail.SparePartId);
                decimal? oldCenterprise = null;
                decimal? oldSalePrice = null;
                if(partsSalesPrice == null) {
                    var newPartsSalesPrice = new PartsSalesPrice {
                        BranchId = partsSalesPriceChange.BranchId,
                        PartsSalesCategoryId = partsSalesPriceChange.PartsSalesCategoryId,
                        PartsSalesCategoryCode = partsSalesPriceChange.PartsSalesCategoryCode,
                        PartsSalesCategoryName = partsSalesPriceChange.PartsSalesCategoryName,
                        SparePartId = partsSalesPriceChangeDetail.SparePartId,
                        SparePartCode = partsSalesPriceChangeDetail.SparePartCode,
                        SparePartName = partsSalesPriceChangeDetail.SparePartName,
                        IfClaim = partsSalesPriceChange.IfClaim,
                        SalesPrice = partsSalesPriceChangeDetail.SalesPrice,
                        CenterPrice = partsSalesPriceChangeDetail.CenterPrice,
                        PriceType = partsSalesPriceChangeDetail.PriceType,
                        Status = (int)DcsBaseDataStatus.有效,
                        ValidationTime = DateTime.Now,
                        ExpireTime = partsSalesPriceChangeDetail.ExpireTime,
                        CreatorId = partsSalesPriceChange.CreatorId,
                        CreatorName = partsSalesPriceChange.CreatorName,
                        CreateTime = partsSalesPriceChange.CreateTime,
                        Remark = partsSalesPriceChangeDetail.Remark,
                        ModifierId = partsSalesPriceChange.ModifierId,
                        ModifierName = partsSalesPriceChange.ModifierName,
                        ModifyTime = partsSalesPriceChange.ModifyTime,
                        ApproverId = partsSalesPriceChange.FinalApproverId,
                        ApproverName = partsSalesPriceChange.FinalApproverName,
                        ApproveTime = partsSalesPriceChange.FinalApproveTime
                    };
                    InsertToDatabase(newPartsSalesPrice);
                } else {
                    oldCenterprise = partsSalesPrice.CenterPrice;
                    oldSalePrice = partsSalesPrice.SalesPrice;

                    partsSalesPrice.CenterPrice = partsSalesPriceChangeDetail.CenterPrice;
                    partsSalesPrice.SalesPrice = partsSalesPriceChangeDetail.SalesPrice;
                    partsSalesPrice.ValidationTime = DateTime.Now;
                    partsSalesPrice.ExpireTime = partsSalesPriceChangeDetail.ExpireTime;
                    partsSalesPrice.IfClaim = partsSalesPriceChange.IfClaim;
                    partsSalesPrice.PriceType = partsSalesPriceChangeDetail.PriceType;
                    partsSalesPrice.ApproverId = partsSalesPriceChange.FinalApproverId;
                    partsSalesPrice.ApproverName = partsSalesPriceChange.FinalApproverName;
                    partsSalesPrice.ApproveTime = partsSalesPriceChange.FinalApproveTime;
                    new PartsSalesPriceAch(this.DomainService).UpdatePartsSalesPriceValidate(partsSalesPrice);
                }
                //新增配件销售价变更履历
                var partsSalesPriceHistory = new PartsSalesPriceHistory {
                    BranchId = partsSalesPriceChange.BranchId,
                    PartsSalesCategoryId = partsSalesPriceChange.PartsSalesCategoryId,
                    PartsSalesCategoryCode = partsSalesPriceChange.PartsSalesCategoryCode,
                    PartsSalesCategoryName = partsSalesPriceChange.PartsSalesCategoryName,
                    SparePartId = partsSalesPriceChangeDetail.SparePartId,
                    SparePartCode = partsSalesPriceChangeDetail.SparePartCode,
                    SparePartName = partsSalesPriceChangeDetail.SparePartName,
                    IfClaim = partsSalesPriceChange.IfClaim,
                    SalesPrice = partsSalesPriceChangeDetail.SalesPrice,
                    CenterPrice = partsSalesPriceChangeDetail.CenterPrice,
                    RetailGuidePrice = partsSalesPriceChangeDetail.RetailGuidePrice,
                    PriceType = partsSalesPriceChangeDetail.PriceType,
                    Status = (int)DcsBaseDataStatus.有效,
                    ValidationTime = partsSalesPriceChangeDetail.ValidationTime,
                    ExpireTime = partsSalesPriceChangeDetail.ExpireTime,
                    Remark = partsSalesPriceChangeDetail.Remark,
                    PriceTypeName = partsSalesPriceChangeDetail.PriceTypeName,
                    OldSalesPrice = oldSalePrice,
                    OldCenterPrice = oldCenterprise
                };
                InsertToDatabase(partsSalesPriceHistory);
                new PartsSalesPriceHistoryAch(this.DomainService).InsertPartsSalesPriceHistoryValidate(partsSalesPriceHistory);

                //只有价格大于0 才新增更新价格及其履历
                if(partsSalesPriceChangeDetail.RetailGuidePrice > 0) {
                    //新增或更新配件零售指导价
                    var partsRetailGuidePrice = partsRetailGuidePrices.FirstOrDefault(r => r.SparePartId == partsSalesPriceChangeDetail.SparePartId);
                    if(partsRetailGuidePrice == null) {
                        var newPartsRetailGuidePrice = new PartsRetailGuidePrice {
                            BranchId = partsSalesPriceChange.BranchId,
                            PartsSalesCategoryId = partsSalesPriceChange.PartsSalesCategoryId,
                            PartsSalesCategoryCode = partsSalesPriceChange.PartsSalesCategoryCode,
                            PartsSalesCategoryName = partsSalesPriceChange.PartsSalesCategoryName,
                            SparePartId = partsSalesPriceChangeDetail.SparePartId,
                            SparePartCode = partsSalesPriceChangeDetail.SparePartCode,
                            SparePartName = partsSalesPriceChangeDetail.SparePartName,
                            RetailGuidePrice = partsSalesPriceChangeDetail.RetailGuidePrice != null ? partsSalesPriceChangeDetail.RetailGuidePrice.Value : 0,
                            ValidationTime = partsSalesPriceChangeDetail.ValidationTime,
                            ExpireTime = partsSalesPriceChangeDetail.ExpireTime,
                            Status = (int)DcsBaseDataStatus.有效,
                            Remark = partsSalesPriceChangeDetail.Remark
                        };
                        InsertToDatabase(newPartsRetailGuidePrice);
                        new PartsRetailGuidePriceAch(this.DomainService).InsertPartsRetailGuidePriceValidate(newPartsRetailGuidePrice);
                    } else {
                        partsRetailGuidePrice.RetailGuidePrice = partsSalesPriceChangeDetail.RetailGuidePrice ?? 0;
                        partsRetailGuidePrice.ValidationTime = partsSalesPriceChangeDetail.ValidationTime;
                        partsRetailGuidePrice.ExpireTime = partsSalesPriceChangeDetail.ExpireTime;
                        new PartsRetailGuidePriceAch(this.DomainService).UpdatePartsRetailGuidePriceValidate(partsRetailGuidePrice);
                    }
                    //新增配件零售指导价变更履历
                    var partsRetailGuidePriceHistory = new PartsRetailGuidePriceHistory {
                        BranchId = partsSalesPriceChange.BranchId,
                        PartsSalesCategoryId = partsSalesPriceChange.PartsSalesCategoryId,
                        PartsSalesCategoryCode = partsSalesPriceChange.PartsSalesCategoryCode,
                        PartsSalesCategoryName = partsSalesPriceChange.PartsSalesCategoryName,
                        SparePartId = partsSalesPriceChangeDetail.SparePartId,
                        SparePartCode = partsSalesPriceChangeDetail.SparePartCode,
                        SparePartName = partsSalesPriceChangeDetail.SparePartName,
                        RetailGuidePrice = partsSalesPriceChangeDetail.RetailGuidePrice.HasValue ? partsSalesPriceChangeDetail.RetailGuidePrice.Value : 0,
                        ValidationTime = partsSalesPriceChangeDetail.ValidationTime,
                        ExpireTime = partsSalesPriceChangeDetail.ExpireTime,
                        Status = (int)DcsBaseDataStatus.有效,
                        Remark = partsSalesPriceChangeDetail.Remark
                    };
                    InsertToDatabase(partsRetailGuidePriceHistory);
                    new PartsRetailGuidePriceHistoryAch(this.DomainService).InsertPartsRetailGuidePriceHistoryValidate(partsRetailGuidePriceHistory);
                }

                //只有价格大于0 才新增更新价格及其履历
                if(partsSalesPriceChangeDetail.DealerSalesPrice > 0) {
                    //新增或更新服务站配件批发价
                    var dealerPartsSalesPrice = dealerPartsSalesPrices.FirstOrDefault(r => r.SparePartId == partsSalesPriceChangeDetail.SparePartId);
                    if(dealerPartsSalesPrice == null) {
                        var newDealerPartsSalesPrice = new DealerPartsSalesPrice {
                            BranchId = partsSalesPriceChange.BranchId,
                            PartsSalesCategoryId = partsSalesPriceChange.PartsSalesCategoryId,
                            PartsSalesCategoryCode = partsSalesPriceChange.PartsSalesCategoryCode,
                            PartsSalesCategoryName = partsSalesPriceChange.PartsSalesCategoryName,
                            SparePartId = partsSalesPriceChangeDetail.SparePartId,
                            SparePartCode = partsSalesPriceChangeDetail.SparePartCode,
                            SparePartName = partsSalesPriceChangeDetail.SparePartName,
                            DealerSalesPrice = partsSalesPriceChangeDetail.DealerSalesPrice ?? 0,
                            Status = (int)DcsBaseDataStatus.有效,
                            Remark = partsSalesPriceChangeDetail.Remark
                        };
                        InsertToDatabase(newDealerPartsSalesPrice);
                        new DealerPartsSalesPriceAch(this.DomainService).InsertDealerPartsSalesPriceValidate(newDealerPartsSalesPrice);
                    } else {
                        dealerPartsSalesPrice.DealerSalesPrice = partsSalesPriceChangeDetail.DealerSalesPrice ?? 0;
                        new DealerPartsSalesPriceAch(this.DomainService).UpdateDealerPartsSalesPriceValidate(dealerPartsSalesPrice);
                    }
                    //新增服务站配件批发价变更履历
                    var dealerPartsSalesPriceHistory = new DealerPartsSalesPriceHistory {
                        BranchId = partsSalesPriceChange.BranchId,
                        PartsSalesCategoryId = partsSalesPriceChange.PartsSalesCategoryId,
                        PartsSalesCategoryCode = partsSalesPriceChange.PartsSalesCategoryCode,
                        PartsSalesCategoryName = partsSalesPriceChange.PartsSalesCategoryName,
                        SparePartId = partsSalesPriceChangeDetail.SparePartId,
                        SparePartCode = partsSalesPriceChangeDetail.SparePartCode,
                        SparePartName = partsSalesPriceChangeDetail.SparePartName,
                        DealerSalesPrice = partsSalesPriceChangeDetail.DealerSalesPrice ?? 0,
                        Status = (int)DcsBaseDataStatus.有效,
                        Remark = partsSalesPriceChangeDetail.Remark
                    };
                    InsertToDatabase(dealerPartsSalesPriceHistory);
                    new DealerPartsSalesPriceHistoryAch(this.DomainService).InsertDealerPartsSalesPriceHistoryValidate(dealerPartsSalesPriceHistory);
                }

                //增加逻辑：销售价变更申请审核通过后，倒冲 采购价格变动情况记录
                //依据配件编号 倒冲 销售价维护时间字段为空，并且状态=有效 的记录（可能有多条）。 倒冲中心库价、经销价（服务站价）、建议售价、销售价维护时间、状态=作废、修改人、修改时间
                var purchasePricingChangeHises = this.ObjectContext.PurchasePricingChangeHis.Where(r => r.SparePartId == partsSalesPriceChangeDetail.SparePartId && !r.MaintenanceTime.HasValue && r.Status == (int)DcsMasterDataStatus.有效).ToArray();
                foreach(var item in purchasePricingChangeHises) {
                    item.CenterPrice = partsSalesPriceChangeDetail.CenterPrice;
                    item.DistributionPrice = partsSalesPriceChangeDetail.SalesPrice;
                    item.RetailPrice = partsSalesPriceChangeDetail.RetailGuidePrice;
                    item.MaintenanceTime = DateTime.Now;
                    item.Status = (int)DcsMasterDataStatus.作废;
                    item.ModifierId = userinfo.Id;
                    item.ModifierName = userinfo.Name;
                    item.ModifyTime = DateTime.Now;
                    UpdateToDatabase(item);
                }

                //终审通过后，价格类型字段的值需覆盖配件分品牌信息管理中的价格类型
                var partsSalePriceIncreaseRate = ObjectContext.PartsSalePriceIncreaseRates.FirstOrDefault(o => o.status == (int)DcsBaseDataStatus.有效 && o.GroupName == partsSalesPriceChangeDetail.PriceTypeName);
                var partsBranch = ObjectContext.PartsBranches.FirstOrDefault(b => partsSalesPriceChangeDetail.SparePartId == b.PartId && b.PartsSalesCategoryId == partsSalesPriceChange.PartsSalesCategoryId && b.Status == (int)DcsBaseDataStatus.有效);
                if(partsBranch != null && partsSalePriceIncreaseRate != null) {
                    partsBranch.IncreaseRateGroupId = partsSalePriceIncreaseRate.Id;
                    new PartsBranchAch(this.DomainService).UpdatePartsBranchValidate(partsBranch);
                }
            }
        }

        public void 驳回配件销售价格变更申请(PartsSalesPriceChange partsSalesPriceChange) {
            var dbPartsSalesPriceChange = ObjectContext.PartsSalesPriceChanges.Where(r => r.Id == partsSalesPriceChange.Id && (r.Status == (int)DcsPartsSalesPriceChangeStatus.初审通过 || r.Status == (int)DcsPartsSalesPriceChangeStatus.提交 || r.Status == (int)DcsPartsSalesPriceChangeStatus.审核通过)).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbPartsSalesPriceChange == null)
                throw new ValidationException(ErrorStrings.PartsSalesPriceChange_Reject);
            var userinfo = Utils.GetCurrentUserInfo();
            if(userinfo.Id == 0) {
                throw new ValidationException(ErrorStrings.User_Invalid);
            }
            UpdateToDatabase(partsSalesPriceChange);
            partsSalesPriceChange.RejectId = userinfo.Id;
            partsSalesPriceChange.Rejecter = userinfo.Name;
            partsSalesPriceChange.RejectTime = System.DateTime.Now;
            if(partsSalesPriceChange.Status == (int)DcsPartsSalesPriceChangeStatus.提交) {
                partsSalesPriceChange.InitialApproverId = userinfo.Id;
                partsSalesPriceChange.InitialApproverName = userinfo.Name;
                partsSalesPriceChange.InitialApproveTime = System.DateTime.Now;
            } else if(partsSalesPriceChange.Status == (int)DcsPartsSalesPriceChangeStatus.初审通过) {
                partsSalesPriceChange.ApproverId = userinfo.Id;
                partsSalesPriceChange.ApproverName = userinfo.Name;
                partsSalesPriceChange.ApproveTime = System.DateTime.Now;
            } else {
                partsSalesPriceChange.FinalApproverId = userinfo.Id;
                partsSalesPriceChange.FinalApproverName = userinfo.Name;
                partsSalesPriceChange.FinalApproveTime = System.DateTime.Now;
            }
            partsSalesPriceChange.Status = (int)DcsPartsSalesPriceChangeStatus.新建;
            this.UpdatePartsSalesPriceChangeValidate(partsSalesPriceChange);
        }


        public void 作废配件销售价格变更申请(PartsSalesPriceChange partsSalesPriceChange) {
            var dbPartsSalesPriceChange = ObjectContext.PartsSalesPriceChanges.Where(r => r.Id == partsSalesPriceChange.Id && ( r.Status == (int)DcsPartsSalesPriceChangeStatus.新建)).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbPartsSalesPriceChange == null)
                throw new ValidationException(ErrorStrings.PartsSalesPriceChange_Validation5);
            var userinfo = Utils.GetCurrentUserInfo();
            if(userinfo.Id == 0) {
                throw new ValidationException(ErrorStrings.User_Invalid);
            }
            UpdateToDatabase(partsSalesPriceChange);
            partsSalesPriceChange.AbandonerId = userinfo.Id;
            partsSalesPriceChange.AbandonerName = userinfo.Name;
            partsSalesPriceChange.AbandonTime = System.DateTime.Now;
            partsSalesPriceChange.Status = (int)DcsPartsSalesPriceChangeStatus.作废;
            this.UpdatePartsSalesPriceChangeValidate(partsSalesPriceChange);
        }


        public void 修改配件销售价变更申请(PartsSalesPriceChange partsSalesPriceChange) {
            CheckEntityState(partsSalesPriceChange);
            var dbPartsSalesPriceChange = ObjectContext.PartsSalesPriceChanges.Where(r => r.Id == partsSalesPriceChange.Id && r.Status == (int)DcsPartsSalesPriceChangeStatus.新建).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbPartsSalesPriceChange == null)
                throw new ValidationException(ErrorStrings.PartsSalesPriceChange_Validation1);
            var partsSalesPriceChangedetailcheck = partsSalesPriceChange.PartsSalesPriceChangeDetails.Where(r => (!ObjectContext.PartsBranches.Any(b => r.SparePartId == b.PartId && b.PartsSalesCategoryId == partsSalesPriceChange.PartsSalesCategoryId && b.Status == (int)DcsBaseDataStatus.有效)));
            if(partsSalesPriceChangedetailcheck.Any()) {
                string partscodes = " ";
                foreach(var c in partsSalesPriceChangedetailcheck) {
                    partscodes = partscodes + c.SparePartCode + " ";
                }
                throw new ValidationException(string.Format(ErrorStrings.IvecoPriceChangeApp_Validation6,partscodes));
            }
            //校验价格
            this.validatePrice(partsSalesPriceChange.PartsSalesPriceChangeDetails.ToList());
            //计算变更比率
            var partsPurchasePricings = this.GetPartsPurchasePricingByPartIds(partsSalesPriceChange.PartsSalesPriceChangeDetails.Select(r => r.SparePartId).ToArray());//获取配件采购价格
            foreach(var detail in partsSalesPriceChange.PartsSalesPriceChangeDetails) {
                var pricing = partsPurchasePricings.FirstOrDefault(r => r.PartId == detail.SparePartId);//获取配件采购价格
                if(pricing != null && pricing.PurchasePrice != 0) {
                    //变更比率有误，需更改公式为：（*价格-采购价）/采购价*100%
                    //计算变动比率时，采购价使用含税价计算。 采购价*1.13
                    detail.MaxPurchasePricing = pricing.PurchasePrice;//采购价格
                    detail.SalesPriceFluctuationRatio = Convert.ToDouble(detail.SalesPrice / (pricing.PurchasePrice * 1.13m));//服务站价变更比率
                    detail.CenterPriceRatio = Convert.ToDouble(detail.CenterPrice / (pricing.PurchasePrice * 1.13m)); //中心库价变更比率
                    detail.RetailPriceFluctuationRatio = Convert.ToDouble(detail.RetailGuidePrice / (pricing.PurchasePrice * 1.13m)); //零售价变更比率
                }
            }
        }

        public void 生成配件销售价变更申请(PartsSalesPriceChange partsSalesPriceChange, List<PartsSalesPriceChangeDetail> partsSalesPriceChangeDetails) {
            //var partsSalesPriceChangedetailcheck = partsSalesPriceChangeDetails.Where(r => (!ObjectContext.PartsBranches.Any(b => r.SparePartId == b.PartId && b.PartsSalesCategoryId == partsSalesPriceChange.PartsSalesCategoryId && b.Status == (int)DcsBaseDataStatus.有效)));
            //if(partsSalesPriceChangedetailcheck.Any()) {
            //    string partscodes = " ";
            //    foreach(var c in partsSalesPriceChangedetailcheck) {
            //        partscodes = partscodes + c.SparePartCode + " ";
            //    }
            //    throw new ValidationException("清单中" + partscodes + "配件 不存在配件营销信息");
            //}
            var partsSalesCategory = partsSalesPriceChangeDetails.GroupBy(r => r.PartsSalesCategoryName);
            foreach(var item in partsSalesCategory) {
                var details = partsSalesPriceChangeDetails.Where(r => r.PartsSalesCategoryName == item.Key);
                var newPartsSalesPriceChange = new PartsSalesPriceChange();
                newPartsSalesPriceChange.Code = partsSalesPriceChange.Code;
                newPartsSalesPriceChange.IfClaim = partsSalesPriceChange.IfClaim;
                newPartsSalesPriceChange.Status = partsSalesPriceChange.Status;
                newPartsSalesPriceChange.BranchId = partsSalesPriceChange.BranchId;
                newPartsSalesPriceChange.PartsSalesCategoryId = details.FirstOrDefault().PartsSalesCategoryId;
                newPartsSalesPriceChange.PartsSalesCategoryCode = details.FirstOrDefault().PartsSalesCategoryCode;
                newPartsSalesPriceChange.PartsSalesCategoryName = details.FirstOrDefault().PartsSalesCategoryName;
                newPartsSalesPriceChange.Remark = partsSalesPriceChange.Remark;
                InsertPartsSalesPriceChangeValidate(newPartsSalesPriceChange);
                foreach(var detail in details) {
                    var partsSalesPriceChangeDetail = new PartsSalesPriceChangeDetail();
                    partsSalesPriceChangeDetail.SparePartId = detail.SparePartId;
                    partsSalesPriceChangeDetail.SparePartCode = detail.SparePartCode;
                    partsSalesPriceChangeDetail.SparePartName = detail.SparePartName;
                    partsSalesPriceChangeDetail.SalesPrice = detail.SalesPrice;
                    partsSalesPriceChangeDetail.RetailGuidePrice = detail.RetailGuidePrice;
                    partsSalesPriceChangeDetail.DealerSalesPrice = detail.DealerSalesPrice;
                    partsSalesPriceChangeDetail.PriceType = detail.PriceType;
                    partsSalesPriceChangeDetail.ValidationTime = detail.ValidationTime;
                    partsSalesPriceChangeDetail.ExpireTime = detail.ExpireTime;
                    partsSalesPriceChangeDetail.Remark = detail.Remark;
                    partsSalesPriceChangeDetail.RowVersion = detail.RowVersion;
                    partsSalesPriceChangeDetail.CategoryCode = detail.CategoryCode;
                    partsSalesPriceChangeDetail.CategoryName = detail.CategoryName;
                    partsSalesPriceChangeDetail.SalesPriceFluctuationRatio = detail.SalesPriceFluctuationRatio;
                    partsSalesPriceChangeDetail.RetailPriceFluctuationRatio = detail.RetailPriceFluctuationRatio;
                    partsSalesPriceChangeDetail.MaxSalesPriceFloating = detail.MaxSalesPriceFloating;
                    partsSalesPriceChangeDetail.MinSalesPriceFloating = detail.MinSalesPriceFloating;
                    partsSalesPriceChangeDetail.MaxRetailOrderPriceFloating = detail.MaxRetailOrderPriceFloating;
                    partsSalesPriceChangeDetail.MinRetailOrderPriceFloating = detail.MinRetailOrderPriceFloating;
                    partsSalesPriceChangeDetail.MaxPurchasePricing = detail.MaxPurchasePricing;
                    partsSalesPriceChangeDetail.IsUpsideDown = detail.IsUpsideDown;
                    partsSalesPriceChangeDetail.MaxExchangeSalePrice = detail.MaxExchangeSalePrice;
                    newPartsSalesPriceChange.PartsSalesPriceChangeDetails.Add(detail);
                }
                InsertToDatabase(newPartsSalesPriceChange);
            }
            this.ObjectContext.SaveChanges();
        }

        public void 新增配件销售价变更申请(PartsSalesPriceChange partsSalesPriceChange) {
            var partsSalesPriceChangedetailcheck = partsSalesPriceChange.PartsSalesPriceChangeDetails.Where(r => (!ObjectContext.PartsBranches.Any(b => r.SparePartId == b.PartId && b.PartsSalesCategoryId == partsSalesPriceChange.PartsSalesCategoryId && b.Status == (int)DcsBaseDataStatus.有效)));
            if(partsSalesPriceChangedetailcheck.Any()) {
                string partscodes = " ";
                foreach(var c in partsSalesPriceChangedetailcheck) {
                    partscodes = partscodes + c.SparePartCode + " ";
                }
                throw new ValidationException(string.Format(ErrorStrings.PartsSalePriceIncreaseRate_Validation2,partscodes));
            }
            //校验价格
            this.validatePrice(partsSalesPriceChange.PartsSalesPriceChangeDetails.ToList());
            var partsPurchasePricings = this.GetPartsPurchasePricingByPartIds(partsSalesPriceChange.PartsSalesPriceChangeDetails.Select(r => r.SparePartId).ToArray());//获取配件采购价格
            foreach(var detail in partsSalesPriceChange.PartsSalesPriceChangeDetails) {
                var pricing = partsPurchasePricings.FirstOrDefault(r => r.PartId == detail.SparePartId);//获取配件采购价格
                if(pricing == null || pricing.PurchasePrice == 0)
                    continue;
                //变更比率有误，需更改公式为：（*价格-采购价）/采购价*100%
                //计算变动比率时，采购价使用含税价计算。 采购价*1.16
                detail.MaxPurchasePricing = pricing.PurchasePrice;//采购价格
                detail.SalesPriceFluctuationRatio = Convert.ToDouble(detail.SalesPrice / (pricing.PurchasePrice * 1.13m));//服务站价变更比率
                detail.CenterPriceRatio = Convert.ToDouble(detail.CenterPrice / (pricing.PurchasePrice * 1.13m)); //中心库价变更比率
                detail.RetailPriceFluctuationRatio = Convert.ToDouble(detail.RetailGuidePrice / (pricing.PurchasePrice * 1.13m)); //零售价变更比率
            }
            InsertPartsSalesPriceChange(partsSalesPriceChange);
        }

        /// <summary>
        /// 根据备件查询首选供应商对应的采购价格
        /// </summary>
        /// <param name="partIds">备件Id</param>
        /// <returns></returns>
        public IQueryable<PartsPurchasePricing> GetPartsPurchasePricingByPartIds(int[] partIds) {
            var result = from a in this.ObjectContext.PartsPurchasePricings.Where(r => r.ValidFrom <= DateTime.Now && r.ValidTo > DateTime.Now)
                         join b in this.ObjectContext.PartsSupplierRelations.Where(r => partIds.Contains(r.PartId) && r.IsPrimary == true && r.Status != (int)DcsBaseDataStatus.作废)
                            on new {
                                a.PartId,
                                SupplierId = a.PartsSupplierId
                            } equals new {
                                b.PartId,
                                b.SupplierId
                            }
                         select new {
                             a
                         };
            return result.Select(r => r.a);
        }

        public void 批量终审配件销售价格变更申请(int[] ids) {
            var dbPartsSalesPriceChanges = ObjectContext.PartsSalesPriceChanges.Where(r => ids.Contains(r.Id) && r.Status == (int)DcsPartsSalesPriceChangeStatus.初审通过).SetMergeOption(MergeOption.NoTracking);
            if(dbPartsSalesPriceChanges == null)
                throw new ValidationException(ErrorStrings.PartsSalesPriceChange_Validation3);
            var userinfo = Utils.GetCurrentUserInfo();
            foreach(var partsSalesPriceChange in dbPartsSalesPriceChanges) {
                UpdateToDatabase(partsSalesPriceChange);
                partsSalesPriceChange.FinalApproverId = userinfo.Id;
                partsSalesPriceChange.FinalApproverName = userinfo.Name;
                partsSalesPriceChange.FinalApproveTime = System.DateTime.Now;
                partsSalesPriceChange.Status = (int)DcsPartsSalesPriceChangeStatus.生效;
                this.UpdatePartsSalesPriceChangeValidate(partsSalesPriceChange);
                var partsSalesPriceChangeDetails = partsSalesPriceChange.PartsSalesPriceChangeDetails.ToArray();
                // //校验价格
                this.validatePrice(partsSalesPriceChange.PartsSalesPriceChangeDetails.ToList());
                var partsIds = partsSalesPriceChangeDetails.Select(r => r.SparePartId).ToArray();
                var priceTypes = partsSalesPriceChangeDetails.Select(r => r.PriceType).ToArray();
                var partsSalesPrices = ObjectContext.PartsSalesPrices.Where(r => r.PartsSalesCategoryId == partsSalesPriceChange.PartsSalesCategoryId && partsIds.Contains(r.SparePartId) && r.Status != (int)DcsBaseDataStatus.作废).ToArray();
                var partsRetailGuidePrices = ObjectContext.PartsRetailGuidePrices.Where(r => r.PartsSalesCategoryId == partsSalesPriceChange.PartsSalesCategoryId && partsIds.Contains(r.SparePartId) && r.Status != (int)DcsBaseDataStatus.作废).ToArray();
                var dealerPartsSalesPrices = ObjectContext.DealerPartsSalesPrices.Where(r => r.PartsSalesCategoryId == partsSalesPriceChange.PartsSalesCategoryId && partsIds.Contains(r.SparePartId) && r.Status != (int)DcsBaseDataStatus.作废).ToArray();
                foreach(var partsSalesPriceChangeDetail in partsSalesPriceChangeDetails) {
                    //新增或更新配件销售价。
                    var partsSalesPrice = partsSalesPrices.FirstOrDefault(r => r.SparePartId == partsSalesPriceChangeDetail.SparePartId);
                    if(partsSalesPrice == null) {
                        var newPartsSalesPrice = new PartsSalesPrice {
                            BranchId = partsSalesPriceChange.BranchId,
                            PartsSalesCategoryId = partsSalesPriceChange.PartsSalesCategoryId,
                            PartsSalesCategoryCode = partsSalesPriceChange.PartsSalesCategoryCode,
                            PartsSalesCategoryName = partsSalesPriceChange.PartsSalesCategoryName,
                            SparePartId = partsSalesPriceChangeDetail.SparePartId,
                            SparePartCode = partsSalesPriceChangeDetail.SparePartCode,
                            SparePartName = partsSalesPriceChangeDetail.SparePartName,
                            IfClaim = partsSalesPriceChange.IfClaim,
                            SalesPrice = partsSalesPriceChangeDetail.SalesPrice,
                            CenterPrice = partsSalesPriceChangeDetail.CenterPrice,
                            PriceType = partsSalesPriceChangeDetail.PriceType,
                            Status = (int)DcsBaseDataStatus.有效,
                            ValidationTime = partsSalesPriceChangeDetail.ValidationTime,
                            ExpireTime = partsSalesPriceChangeDetail.ExpireTime,
                            CreatorId = partsSalesPriceChange.CreatorId,
                            CreatorName = partsSalesPriceChange.CreatorName,
                            CreateTime = partsSalesPriceChange.CreateTime,
                            Remark = partsSalesPriceChangeDetail.Remark,
                            ModifierId = partsSalesPriceChange.ModifierId,
                            ModifierName = partsSalesPriceChange.ModifierName,
                            ModifyTime = partsSalesPriceChange.ModifyTime,
                            ApproverId = partsSalesPriceChange.FinalApproverId,
                            ApproverName = partsSalesPriceChange.FinalApproverName,
                            ApproveTime = partsSalesPriceChange.FinalApproveTime,
                        };
                        InsertToDatabase(newPartsSalesPrice);
                    } else {
                        partsSalesPrice.CenterPrice = partsSalesPriceChangeDetail.CenterPrice;
                        partsSalesPrice.SalesPrice = partsSalesPriceChangeDetail.SalesPrice;
                        partsSalesPrice.ValidationTime = partsSalesPriceChangeDetail.ValidationTime;
                        partsSalesPrice.ExpireTime = partsSalesPriceChangeDetail.ExpireTime;
                        partsSalesPrice.IfClaim = partsSalesPriceChange.IfClaim;
                        partsSalesPrice.PriceType = partsSalesPriceChangeDetail.PriceType;
                        partsSalesPrice.ApproverId = partsSalesPriceChange.FinalApproverId;
                        partsSalesPrice.ApproverName = partsSalesPriceChange.FinalApproverName;
                        partsSalesPrice.ApproveTime = partsSalesPriceChange.FinalApproveTime;
                        new PartsSalesPriceAch(this.DomainService).UpdatePartsSalesPriceValidate(partsSalesPrice);
                    }
                    //新增配件销售价变更履历
                    var partsSalesPriceHistory = new PartsSalesPriceHistory {
                        BranchId = partsSalesPriceChange.BranchId,
                        PartsSalesCategoryId = partsSalesPriceChange.PartsSalesCategoryId,
                        PartsSalesCategoryCode = partsSalesPriceChange.PartsSalesCategoryCode,
                        PartsSalesCategoryName = partsSalesPriceChange.PartsSalesCategoryName,
                        SparePartId = partsSalesPriceChangeDetail.SparePartId,
                        SparePartCode = partsSalesPriceChangeDetail.SparePartCode,
                        SparePartName = partsSalesPriceChangeDetail.SparePartName,
                        IfClaim = partsSalesPriceChange.IfClaim,
                        SalesPrice = partsSalesPriceChangeDetail.SalesPrice,
                        CenterPrice = partsSalesPriceChangeDetail.CenterPrice,
                        RetailGuidePrice = partsSalesPriceChangeDetail.RetailGuidePrice,
                        PriceType = partsSalesPriceChangeDetail.PriceType,
                        Status = (int)DcsBaseDataStatus.有效,
                        ValidationTime = partsSalesPriceChangeDetail.ValidationTime,
                        ExpireTime = partsSalesPriceChangeDetail.ExpireTime,
                        Remark = partsSalesPriceChangeDetail.Remark
                    };
                    InsertToDatabase(partsSalesPriceHistory);
                    new PartsSalesPriceHistoryAch(this.DomainService).InsertPartsSalesPriceHistoryValidate(partsSalesPriceHistory);

                    //只有价格大于0 才新增更新价格及其履历
                    if(partsSalesPriceChangeDetail.RetailGuidePrice > 0) {
                        //新增或更新配件零售指导价
                        var partsRetailGuidePrice = partsRetailGuidePrices.FirstOrDefault(r => r.SparePartId == partsSalesPriceChangeDetail.SparePartId);
                        if(partsRetailGuidePrice == null) {
                            var newPartsRetailGuidePrice = new PartsRetailGuidePrice {
                                BranchId = partsSalesPriceChange.BranchId,
                                PartsSalesCategoryId = partsSalesPriceChange.PartsSalesCategoryId,
                                PartsSalesCategoryCode = partsSalesPriceChange.PartsSalesCategoryCode,
                                PartsSalesCategoryName = partsSalesPriceChange.PartsSalesCategoryName,
                                SparePartId = partsSalesPriceChangeDetail.SparePartId,
                                SparePartCode = partsSalesPriceChangeDetail.SparePartCode,
                                SparePartName = partsSalesPriceChangeDetail.SparePartName,
                                RetailGuidePrice = partsSalesPriceChangeDetail.RetailGuidePrice != null ? partsSalesPriceChangeDetail.RetailGuidePrice.Value : 0,
                                ValidationTime = partsSalesPriceChangeDetail.ValidationTime,
                                ExpireTime = partsSalesPriceChangeDetail.ExpireTime,
                                Status = (int)DcsBaseDataStatus.有效,
                                Remark = partsSalesPriceChangeDetail.Remark
                            };
                            InsertToDatabase(newPartsRetailGuidePrice);
                            new PartsRetailGuidePriceAch(this.DomainService).InsertPartsRetailGuidePriceValidate(newPartsRetailGuidePrice);
                        } else {
                            partsRetailGuidePrice.RetailGuidePrice = partsSalesPriceChangeDetail.RetailGuidePrice ?? 0;
                            partsRetailGuidePrice.ValidationTime = partsSalesPriceChangeDetail.ValidationTime;
                            partsRetailGuidePrice.ExpireTime = partsSalesPriceChangeDetail.ExpireTime;
                            new PartsRetailGuidePriceAch(this.DomainService).UpdatePartsRetailGuidePriceValidate(partsRetailGuidePrice);
                        }
                        //新增配件零售指导价变更履历
                        var partsRetailGuidePriceHistory = new PartsRetailGuidePriceHistory {
                            BranchId = partsSalesPriceChange.BranchId,
                            PartsSalesCategoryId = partsSalesPriceChange.PartsSalesCategoryId,
                            PartsSalesCategoryCode = partsSalesPriceChange.PartsSalesCategoryCode,
                            PartsSalesCategoryName = partsSalesPriceChange.PartsSalesCategoryName,
                            SparePartId = partsSalesPriceChangeDetail.SparePartId,
                            SparePartCode = partsSalesPriceChangeDetail.SparePartCode,
                            SparePartName = partsSalesPriceChangeDetail.SparePartName,
                            RetailGuidePrice = partsSalesPriceChangeDetail.RetailGuidePrice.HasValue ? partsSalesPriceChangeDetail.RetailGuidePrice.Value : 0,
                            ValidationTime = partsSalesPriceChangeDetail.ValidationTime,
                            ExpireTime = partsSalesPriceChangeDetail.ExpireTime,
                            Status = (int)DcsBaseDataStatus.有效,
                            Remark = partsSalesPriceChangeDetail.Remark
                        };
                        InsertToDatabase(partsRetailGuidePriceHistory);
                        new PartsRetailGuidePriceHistoryAch(this.DomainService).InsertPartsRetailGuidePriceHistoryValidate(partsRetailGuidePriceHistory);
                    }

                    //只有价格大于0 才新增更新价格及其履历
                    if(partsSalesPriceChangeDetail.DealerSalesPrice > 0) {
                        //新增或更新服务站配件批发价
                        var dealerPartsSalesPrice = dealerPartsSalesPrices.FirstOrDefault(r => r.SparePartId == partsSalesPriceChangeDetail.SparePartId);
                        if(dealerPartsSalesPrice == null) {
                            var newDealerPartsSalesPrice = new DealerPartsSalesPrice {
                                BranchId = partsSalesPriceChange.BranchId,
                                PartsSalesCategoryId = partsSalesPriceChange.PartsSalesCategoryId,
                                PartsSalesCategoryCode = partsSalesPriceChange.PartsSalesCategoryCode,
                                PartsSalesCategoryName = partsSalesPriceChange.PartsSalesCategoryName,
                                SparePartId = partsSalesPriceChangeDetail.SparePartId,
                                SparePartCode = partsSalesPriceChangeDetail.SparePartCode,
                                SparePartName = partsSalesPriceChangeDetail.SparePartName,
                                DealerSalesPrice = partsSalesPriceChangeDetail.DealerSalesPrice ?? 0,
                                Status = (int)DcsBaseDataStatus.有效,
                                Remark = partsSalesPriceChangeDetail.Remark
                            };
                            InsertToDatabase(newDealerPartsSalesPrice);
                            new DealerPartsSalesPriceAch(this.DomainService).InsertDealerPartsSalesPriceValidate(newDealerPartsSalesPrice);
                        } else {
                            dealerPartsSalesPrice.DealerSalesPrice = partsSalesPriceChangeDetail.DealerSalesPrice ?? 0;
                            new DealerPartsSalesPriceAch(this.DomainService).UpdateDealerPartsSalesPriceValidate(dealerPartsSalesPrice);
                        }
                        //新增服务站配件批发价变更履历
                        var dealerPartsSalesPriceHistory = new DealerPartsSalesPriceHistory {
                            BranchId = partsSalesPriceChange.BranchId,
                            PartsSalesCategoryId = partsSalesPriceChange.PartsSalesCategoryId,
                            PartsSalesCategoryCode = partsSalesPriceChange.PartsSalesCategoryCode,
                            PartsSalesCategoryName = partsSalesPriceChange.PartsSalesCategoryName,
                            SparePartId = partsSalesPriceChangeDetail.SparePartId,
                            SparePartCode = partsSalesPriceChangeDetail.SparePartCode,
                            SparePartName = partsSalesPriceChangeDetail.SparePartName,
                            DealerSalesPrice = partsSalesPriceChangeDetail.DealerSalesPrice ?? 0,
                            Status = (int)DcsBaseDataStatus.有效,
                            Remark = partsSalesPriceChangeDetail.Remark
                        };
                        InsertToDatabase(dealerPartsSalesPriceHistory);
                        new DealerPartsSalesPriceHistoryAch(this.DomainService).InsertDealerPartsSalesPriceHistoryValidate(dealerPartsSalesPriceHistory);
                    }

                }
                ObjectContext.SaveChanges();
            }

        }

        public IQueryable<GetAllPartsSalesPrice> 查询配件销售价格品牌(string partsSalesCategoryName) {
            IQueryable<GetAllPartsSalesPrice> partsSalesPrices = ObjectContext.GetAllPartsSalesPrices;
            if(!string.IsNullOrEmpty(partsSalesCategoryName))
                return partsSalesPrices = partsSalesPrices.Where(r => r.PartsSalesCategoryName == partsSalesCategoryName && r.Status != (int)DcsBaseDataStatus.作废);
            return partsSalesPrices = partsSalesPrices.Where(r => r.Status != (int)DcsBaseDataStatus.作废);
        }

        public PartsSalesPriceChange 查询配件销售价格申请单详单附带虚拟字段(int id) {
            var dbPrtsSalesPriceChange = ObjectContext.PartsSalesPriceChanges.SingleOrDefault(e => e.Id == id);
            if(dbPrtsSalesPriceChange != null) {
                var dbDetails = ObjectContext.PartsSalesPriceChangeDetails.Where(ex => ex.ParentId == dbPrtsSalesPriceChange.Id).ToArray();
                var sparePartIds = dbDetails.Select(r => r.SparePartId).ToArray();
                if(dbDetails.Any()) {
                    var partsPurchasePricings = ObjectContext.PartsPurchasePricings.Where(r => sparePartIds.Contains(r.PartId) && r.Status == (int)DcsPartsPurchasePricingStatus.生效 && r.PartsSalesCategoryId == dbPrtsSalesPriceChange.PartsSalesCategoryId).ToArray();

                    var partsSalesPrices = ObjectContext.PartsSalesPrices.Where(r => sparePartIds.Contains(r.SparePartId) && r.Status == (int)DcsBaseDataStatus.有效 && r.PartsSalesCategoryId == dbPrtsSalesPriceChange.PartsSalesCategoryId && r.BranchId == dbPrtsSalesPriceChange.BranchId).ToArray();
                    var partsRetailGuidePrices = ObjectContext.PartsRetailGuidePrices.Where(r => sparePartIds.Contains(r.SparePartId) && r.Status == (int)DcsBaseDataStatus.有效 && r.PartsSalesCategoryId == dbPrtsSalesPriceChange.PartsSalesCategoryId && r.BranchId == dbPrtsSalesPriceChange.BranchId).ToArray();

                    foreach(var partsSalesPriceChangeDetail in dbDetails) {
                        var partsPurchasePricing = partsPurchasePricings.FirstOrDefault(r => r.PartId == partsSalesPriceChangeDetail.SparePartId);
                        var partsSalesPrice = partsSalesPrices.FirstOrDefault(r => r.SparePartId == partsSalesPriceChangeDetail.SparePartId);
                        var partsRetailGuidePrice = partsRetailGuidePrices.FirstOrDefault(r => r.SparePartId == partsSalesPriceChangeDetail.SparePartId);
                        partsSalesPriceChangeDetail.PurchasePrice = partsPurchasePricing != null ? partsPurchasePricing.PurchasePrice : 0;
                        partsSalesPriceChangeDetail.IncreaseRateSale = (partsPurchasePricing == null || partsPurchasePricing.PurchasePrice == 0 || partsSalesPriceChangeDetail.SalesPrice == 0) ? 0 : partsSalesPriceChangeDetail.SalesPrice / partsSalesPriceChangeDetail.PurchasePrice - 1;
                        partsSalesPriceChangeDetail.IncreaseRateRetail = (partsSalesPriceChangeDetail.SalesPrice == 0 || partsSalesPriceChangeDetail.RetailGuidePrice == null) ? 0 : partsSalesPriceChangeDetail.RetailGuidePrice.Value / partsSalesPriceChangeDetail.SalesPrice - 1;
                        partsSalesPriceChangeDetail.IncreaseRateDealerSalesPrice = (partsSalesPriceChangeDetail.SalesPrice == 0 || partsSalesPriceChangeDetail.DealerSalesPrice == null || partsSalesPriceChangeDetail.DealerSalesPrice == 0) ? 0 : partsSalesPriceChangeDetail.DealerSalesPrice.Value / partsSalesPriceChangeDetail.SalesPrice - 1;
                        partsSalesPriceChangeDetail.PartsSalesPrice_SalePrice = partsSalesPrice != null ? partsSalesPrice.SalesPrice : 0;
                        partsSalesPriceChangeDetail.PartsRetailGuidePrice_RetailGuidePrice = partsRetailGuidePrice != null ? partsRetailGuidePrice.RetailGuidePrice : 0;
                        partsSalesPriceChangeDetail.SalesPriceFluctuationRatio = (partsSalesPriceChangeDetail.SalesPrice == 0 || partsSalesPriceChangeDetail.PartsRetailGuidePrice_RetailGuidePrice == 0) ? 0 : Convert.ToDouble(partsSalesPriceChangeDetail.SalesPrice / partsSalesPriceChangeDetail.PartsRetailGuidePrice_RetailGuidePrice) - 1;
                        partsSalesPriceChangeDetail.RetailPriceFluctuationRatio = (partsSalesPriceChangeDetail.RetailGuidePrice == 0 || partsSalesPriceChangeDetail.PartsRetailGuidePrice_RetailGuidePrice == 0) ? 0 : Convert.ToDouble(partsSalesPriceChangeDetail.RetailGuidePrice / partsSalesPriceChangeDetail.PartsRetailGuidePrice_RetailGuidePrice) - 1;
                        var partsPriceIncreaseRate = ObjectContext.PartsPriceIncreaseRates.FirstOrDefault(r => r.BrandId == dbPrtsSalesPriceChange.BranchId && r.CategoryCode == partsSalesPriceChangeDetail.CategoryCode && r.Status == (int)DcsBaseDataStatus.有效);
                        if(partsPriceIncreaseRate != null) {
                            partsSalesPriceChangeDetail.MaxSalesPriceFloating = partsPriceIncreaseRate.MaxSalesPriceFloating;
                            partsSalesPriceChangeDetail.MinSalesPriceFloating = partsPriceIncreaseRate.MinSalesPriceFloating;
                            partsSalesPriceChangeDetail.MaxRetailOrderPriceFloating = partsPriceIncreaseRate.MaxRetailOrderPriceFloating;
                            partsSalesPriceChangeDetail.MinRetailOrderPriceFloating = partsPriceIncreaseRate.MinRetailOrderPriceFloating;
                        }
                    }
                }
            }
            return dbPrtsSalesPriceChange;
        }
        // 增加校验：中心库价大于等于采购价，服务站价大于等于中心库价，建议售价大于等于服务站价才可以保存申请单
        public void validatePrice(List<PartsSalesPriceChangeDetail> detailList) {
            var spareIds = detailList.Select(r => r.SparePartId).ToArray();
            //查询采购价格
            var artsPurchasePricing = ObjectContext.PartsPurchasePricings.Where(r => r.ValidFrom <= DateTime.Now && r.ValidTo >= DateTime.Now && spareIds.Contains(r.PartId) && r.Status == (int)DcsPartsPurchasePricingStatus.生效
                && ObjectContext.PartsSupplierRelations.Any(d => d.SupplierId == r.PartsSupplierId && d.PartId == r.PartId && d.IsPrimary == true)).ToArray();
            foreach(var item in detailList) {
                var purPrice = artsPurchasePricing.Where(r => r.PartId == item.SparePartId).FirstOrDefault();
                if(null == purPrice) {
                    throw new ValidationException(string.Format(ErrorStrings.PartsSalePriceChange_Validation1,item.SparePartCode));
                }
                if(item.RetailGuidePrice < item.SalesPrice || item.SalesPrice < item.CenterPrice || item.CenterPrice < purPrice.PurchasePrice) {
                    throw new ValidationException(string.Format(ErrorStrings.PartsSalePriceChange_Validation2,item.SparePartCode));
                }
            }
        }
        public void 提交配件销售价变更申请(PartsSalesPriceChange partsSalesPriceChange) {
            var dbPartsSalesPriceChange = ObjectContext.PartsSalesPriceChanges.Include("PartsSalesPriceChangeDetails").Where(r => r.Id == partsSalesPriceChange.Id && r.Status == (int)DcsPartsSalesPriceChangeStatus.新建).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbPartsSalesPriceChange == null)
                throw new ValidationException(ErrorStrings.PartsSalesPriceChange_Validation2);
            // //校验价格
            this.validatePrice(dbPartsSalesPriceChange.PartsSalesPriceChangeDetails.ToList());
            var userinfo = Utils.GetCurrentUserInfo();
            if(userinfo.Id == 0) {
                throw new ValidationException(ErrorStrings.User_Invalid);
            }
            UpdateToDatabase(partsSalesPriceChange);
            partsSalesPriceChange.InitialApproverId = userinfo.Id;
            partsSalesPriceChange.InitialApproverName = userinfo.Name;
            partsSalesPriceChange.InitialApproveTime = System.DateTime.Now;
            partsSalesPriceChange.Status = (int)DcsPartsSalesPriceChangeStatus.提交;
            this.UpdatePartsSalesPriceChangeValidate(partsSalesPriceChange);
        }
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:11:57
        //原分布类的函数全部转移到Ach                                                               
        /// <summary>
        /// 仅供客户端使用
        /// </summary>
        [Update(UsingCustomMethod = true)]
        public void 初审配件销售价格变更申请(PartsSalesPriceChange partsSalesPriceChange) {
            new PartsSalesPriceChangeAch(this).初审配件销售价格变更申请(partsSalesPriceChange);
        }

        [Update(UsingCustomMethod = true)]
        public void 审核配件销售价格变更申请(PartsSalesPriceChange partsSalesPriceChange) {
            new PartsSalesPriceChangeAch(this).审核配件销售价格变更申请(partsSalesPriceChange);
        }

        [Invoke(HasSideEffects = true)]
        public void 批量初审配件销售价格变更申请(int[] ids) {
            new PartsSalesPriceChangeAch(this).批量初审配件销售价格变更申请(ids);
        }


        [Update(UsingCustomMethod = true)]
        public void 终审配件销售价格变更申请(PartsSalesPriceChange partsSalesPriceChange) {
            new PartsSalesPriceChangeAch(this).终审配件销售价格变更申请(partsSalesPriceChange);
        }


        [Update(UsingCustomMethod = true)]
        public void 驳回配件销售价格变更申请(PartsSalesPriceChange partsSalesPriceChange) {
            new PartsSalesPriceChangeAch(this).驳回配件销售价格变更申请(partsSalesPriceChange);
        }


        [Update(UsingCustomMethod = true)]
        public void 作废配件销售价格变更申请(PartsSalesPriceChange partsSalesPriceChange) {
            new PartsSalesPriceChangeAch(this).作废配件销售价格变更申请(partsSalesPriceChange);
        }


        [Update(UsingCustomMethod = true)]
        public void 修改配件销售价变更申请(PartsSalesPriceChange partsSalesPriceChange) {
            new PartsSalesPriceChangeAch(this).修改配件销售价变更申请(partsSalesPriceChange);
        }


        [Update(UsingCustomMethod = true)]
        public void 新增配件销售价变更申请(PartsSalesPriceChange partsSalesPriceChange) {
            new PartsSalesPriceChangeAch(this).新增配件销售价变更申请(partsSalesPriceChange);
        }

        [Invoke]
        public void 生成配件销售价变更申请(PartsSalesPriceChange partsSalesPriceChange, List<PartsSalesPriceChangeDetail> partsSalesPriceChangeDetails) {
            new PartsSalesPriceChangeAch(this).生成配件销售价变更申请(partsSalesPriceChange, partsSalesPriceChangeDetails);
        }

        [Invoke(HasSideEffects = true)]
        public void 批量终审配件销售价格变更申请(int[] ids) {
            new PartsSalesPriceChangeAch(this).批量终审配件销售价格变更申请(ids);
        }

        public IQueryable<GetAllPartsSalesPrice> 查询配件销售价格品牌(string partsSalesCategoryName) {
            return new PartsSalesPriceChangeAch(this).查询配件销售价格品牌(partsSalesCategoryName);
        }

        public PartsSalesPriceChange 查询配件销售价格申请单详单附带虚拟字段(int id) {
            return new PartsSalesPriceChangeAch(this).查询配件销售价格申请单详单附带虚拟字段(id);
        }
         [Update(UsingCustomMethod = true)]
         public void 提交配件销售价变更申请(PartsSalesPriceChange partsSalesPriceChange) {
             new PartsSalesPriceChangeAch(this).提交配件销售价变更申请(partsSalesPriceChange);
             }
    }
}
