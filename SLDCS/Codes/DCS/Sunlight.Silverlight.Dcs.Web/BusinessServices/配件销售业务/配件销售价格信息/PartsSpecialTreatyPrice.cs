﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsSpecialTreatyPriceAch : DcsSerivceAchieveBase {
        public PartsSpecialTreatyPriceAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 作废配件特殊协议价(PartsSpecialTreatyPrice partsSpecialTreatyPrice) {
            var dbpartsSpecialTreatyPrice = ObjectContext.PartsSpecialTreatyPrices.Where(r => r.Id == partsSpecialTreatyPrice.Id && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbpartsSpecialTreatyPrice == null)
                throw new ValidationException(ErrorStrings.PartsSpecialTreatyPrice_Validation2);
            UpdateToDatabase(partsSpecialTreatyPrice);
            partsSpecialTreatyPrice.Status = (int)DcsBaseDataStatus.作废;
            var userInfo = Utils.GetCurrentUserInfo();
            partsSpecialTreatyPrice.AbandonerId = userInfo.Id;
            partsSpecialTreatyPrice.AbandonerName = userInfo.Name;
            partsSpecialTreatyPrice.AbandonTime = DateTime.Now;
            this.UpdatePartsSpecialTreatyPriceValidate(partsSpecialTreatyPrice);
        }
        public void 更新配件特殊协议价(PartsSpecialTreatyPrice partsSpecialTreatyPrice) {
            var dbpartsSpecialTreatyPrice = ObjectContext.PartsSpecialTreatyPrices.Where(c => c.Id == partsSpecialTreatyPrice.Id && c.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbpartsSpecialTreatyPrice == null) {
                throw new ValidationException(ErrorStrings.PartsSpecialTreatyPrice_Validation3);
            }
            var dbpartsSpecialTreatyPrices = ObjectContext.PartsSpecialTreatyPrices.Where(c => c.Id == partsSpecialTreatyPrice.Id && c.PartsSalesCategoryId == partsSpecialTreatyPrice.PartsSalesCategoryId && c.CompanyId == partsSpecialTreatyPrice.CompanyId && c.SparePartId == partsSpecialTreatyPrice.SparePartId).SetMergeOption(MergeOption.NoTracking);
            if(dbpartsSpecialTreatyPrices.Count() == 1) {
                throw new ValidationException(ErrorStrings.PartsSpecialTreatyPrice_Validation4);
            }
        }
        public void 新增配件特殊协议价(PartsSpecialTreatyPrice partsSpecialTreatyPrice) {
            var dbpartsSpecialTreatyPrices = ObjectContext.PartsSpecialTreatyPrices.Where(c => c.Id == partsSpecialTreatyPrice.Id && c.PartsSalesCategoryId == partsSpecialTreatyPrice.PartsSalesCategoryId && c.CompanyId == partsSpecialTreatyPrice.CompanyId && c.SparePartId == partsSpecialTreatyPrice.SparePartId).SetMergeOption(MergeOption.NoTracking);
            if(dbpartsSpecialTreatyPrices.Count() == 1) {
                throw new ValidationException(ErrorStrings.PartsSpecialTreatyPrice_Validation4);
            }
        }

        public void 批量作废配件特殊协议价(int[] ids) {
            var partsSpecialTreatyPrices = ObjectContext.PartsSpecialTreatyPrices.Where(r => ids.Contains(r.Id) && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking);
            
            if(partsSpecialTreatyPrices.Any(r => r.Status != (int)DcsBaseDataStatus.有效)) {
                throw new ValidationException(ErrorStrings.PartsSpecialTreatyPrice_Validation2);
            }
            foreach(var partsSpecialTreatyPrice in partsSpecialTreatyPrices) {
                partsSpecialTreatyPrice.Status = (int)DcsBaseDataStatus.作废;
                var userInfo = Utils.GetCurrentUserInfo();
                partsSpecialTreatyPrice.AbandonerId = userInfo.Id;
                partsSpecialTreatyPrice.AbandonerName = userInfo.Name;
                partsSpecialTreatyPrice.AbandonTime = DateTime.Now;
                this.UpdatePartsSpecialTreatyPriceValidate(partsSpecialTreatyPrice);
                UpdateToDatabase(partsSpecialTreatyPrice);
            }
            ObjectContext.SaveChanges();
        }
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:11:57
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 作废配件特殊协议价(PartsSpecialTreatyPrice partsSpecialTreatyPrice) {
            new PartsSpecialTreatyPriceAch(this).作废配件特殊协议价(partsSpecialTreatyPrice);
        }

        public void 更新配件特殊协议价(PartsSpecialTreatyPrice partsSpecialTreatyPrice) {
            new PartsSpecialTreatyPriceAch(this).更新配件特殊协议价(partsSpecialTreatyPrice);
        }

        public void 新增配件特殊协议价(PartsSpecialTreatyPrice partsSpecialTreatyPrice) {
            new PartsSpecialTreatyPriceAch(this).新增配件特殊协议价(partsSpecialTreatyPrice);
        }
         [Invoke]
        public void 批量作废配件特殊协议价(int[] ids) {
            new PartsSpecialTreatyPriceAch(this).批量作废配件特殊协议价(ids);
        }
    }
}
