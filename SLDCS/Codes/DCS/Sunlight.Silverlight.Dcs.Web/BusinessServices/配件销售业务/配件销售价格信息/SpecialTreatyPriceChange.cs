﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Data.Objects.DataClasses;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.ServiceModel.DomainServices.Server;
using System.Configuration;
using Microsoft.Data.Extensions;
using NPOI.SS.Formula.Functions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
using Devart.Data.Oracle;
using System.Data;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class SpecialTreatyPriceChangeAch : DcsSerivceAchieveBase {
        public SpecialTreatyPriceChangeAch(DcsDomainService domainService)
            : base(domainService) {
        }

        /// <summary>
        /// 仅供客户端使用
        /// </summary>

        public void 审核配件配件特殊协议价格变更申请(SpecialTreatyPriceChange specialTreatyPriceChange) {
            #region
            var dbSpecialTreatyPriceChange = ObjectContext.SpecialTreatyPriceChanges.Where(r => r.Id == specialTreatyPriceChange.Id && r.Status == (int)DcsSpecialTreatyPriceChangeStatus.审核通过).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbSpecialTreatyPriceChange == null)
                throw new ValidationException(ErrorStrings.SpecialTreatyPriceChange_Validation1);
            var userinfo = Utils.GetCurrentUserInfo();
            UpdateToDatabase(specialTreatyPriceChange);
            specialTreatyPriceChange.CheckerId = userinfo.Id;
            specialTreatyPriceChange.CheckerName = userinfo.Name;
            specialTreatyPriceChange.CheckerTime = System.DateTime.Now;
            if(specialTreatyPriceChange.Status.Equals(66)) {
                specialTreatyPriceChange.Status = (int)DcsSpecialTreatyPriceChangeStatus.已审核;

                this.UpdateSpecialTreatyPriceChangeValidate(specialTreatyPriceChange);
                var specialPriceChangeLists = specialTreatyPriceChange.SpecialPriceChangeLists.ToArray();
                var partsIds = specialPriceChangeLists.Select(r => r.SparePartId).ToArray();
                var partsSpecialTreatyPrices = ObjectContext.PartsSpecialTreatyPrices.Where(r => r.PartsSalesCategoryId == specialTreatyPriceChange.BrandId && partsIds.Contains(r.SparePartId) && r.Status == (int)DcsBaseDataStatus.有效 && r.CompanyId == specialTreatyPriceChange.CorporationId.Value).ToArray();
                var partsSalesCategory = ObjectContext.PartsSalesCategories.Where(r => r.Id == specialTreatyPriceChange.BrandId).FirstOrDefault();
                if(partsSalesCategory == null) {
                    throw new ValidationException(string.Format(ErrorStrings.SpecialTreatyPriceChange_Validation2, specialTreatyPriceChange.BrandName));
                }
                foreach(var specialPriceChangeList in specialPriceChangeLists) {
                    //新增或更新配件销售价。
                    var partsSpecialTreatyPrice = partsSpecialTreatyPrices.FirstOrDefault(r => r.SparePartId == specialPriceChangeList.SparePartId);
                    if(partsSpecialTreatyPrice == null) {
                        partsSpecialTreatyPrice = new PartsSpecialTreatyPrice {
                            BranchId = partsSalesCategory.BranchId,
                            PartsSalesCategoryId = partsSalesCategory.Id,
                            PartsSalesCategoryCode = partsSalesCategory.Code,
                            PartsSalesCategoryName = partsSalesCategory.Name,
                            SparePartId = specialPriceChangeList.SparePartId.Value,
                            SparePartCode = specialPriceChangeList.SparePartCode,
                            SparePartName = specialPriceChangeList.SparePartName,
                            CompanyId = specialTreatyPriceChange.CorporationId.Value,
                            TreatyPrice = specialPriceChangeList.SpecialTreatyPrice.Value,
                            Status = (int)DcsBaseDataStatus.有效,
                            ValidationTime = specialTreatyPriceChange.ValidationTime,
                            ExpireTime = specialTreatyPriceChange.ExpireTime,
                            CreatorId = specialTreatyPriceChange.CreatorId,
                            CreatorName = specialTreatyPriceChange.CreatorName,
                            CreateTime = specialTreatyPriceChange.CreateTime,
                            Remark = specialTreatyPriceChange.Remark,
                            ModifierId = specialTreatyPriceChange.ModifierId,
                            ModifierName = specialTreatyPriceChange.ModifierName,
                            ModifyTime = specialTreatyPriceChange.ModifyTime,
                            AbandonerId = specialTreatyPriceChange.AbandonerId,
                            AbandonerName = specialTreatyPriceChange.AbandonerName,
                            AbandonTime = specialTreatyPriceChange.AbandonerTime
                        };
                        InsertToDatabase(partsSpecialTreatyPrice);
                    } else {
                        partsSpecialTreatyPrice.BranchId = partsSalesCategory.BranchId;
                        partsSpecialTreatyPrice.PartsSalesCategoryId = partsSalesCategory.Id;
                        partsSpecialTreatyPrice.PartsSalesCategoryCode = partsSalesCategory.Code;
                        partsSpecialTreatyPrice.PartsSalesCategoryName = partsSalesCategory.Name;
                        partsSpecialTreatyPrice.SparePartId = specialPriceChangeList.SparePartId.Value;
                        partsSpecialTreatyPrice.SparePartCode = specialPriceChangeList.SparePartCode;
                        partsSpecialTreatyPrice.SparePartName = specialPriceChangeList.SparePartName;
                        partsSpecialTreatyPrice.CompanyId = specialTreatyPriceChange.CorporationId.Value;
                        partsSpecialTreatyPrice.TreatyPrice = specialPriceChangeList.SpecialTreatyPrice.Value;
                        partsSpecialTreatyPrice.Remark = specialPriceChangeList.Remark;
                        partsSpecialTreatyPrice.ValidationTime = specialTreatyPriceChange.ValidationTime;
                        partsSpecialTreatyPrice.ExpireTime = specialTreatyPriceChange.ExpireTime;
                        new PartsSpecialTreatyPriceAch(this.DomainService).UpdatePartsSpecialTreatyPriceValidate(partsSpecialTreatyPrice);
                        UpdateToDatabase(partsSpecialTreatyPrice);

                        //新增配件销售价变更履历
                        var partsSpecialPriceHistory = new PartsSpecialPriceHistory {
                            PartsSpecialTreatyPriceId = partsSpecialTreatyPrice.Id,
                            BranchId = partsSalesCategory.BranchId,
                            PartsSalesCategoryId = partsSalesCategory.Id,
                            PartsSalesCategoryCode = partsSalesCategory.Code,
                            PartsSalesCategoryName = partsSalesCategory.Name,
                            SparePartId = specialPriceChangeList.SparePartId.Value,
                            SparePartCode = specialPriceChangeList.SparePartCode,
                            SparePartName = specialPriceChangeList.SparePartName,
                            CompanyId = specialTreatyPriceChange.CorporationId.Value,
                            TreatyPrice = specialPriceChangeList.SpecialTreatyPrice.Value,
                            Status = (int)DcsBaseDataStatus.有效,
                            CreatorId = specialTreatyPriceChange.CreatorId,
                            CreatorName = specialTreatyPriceChange.CreatorName,
                            CreateTime = specialTreatyPriceChange.CreateTime,
                            Remark = specialTreatyPriceChange.Remark
                        };
                        InsertToDatabase(partsSpecialPriceHistory);
                        new PartsSpecialPriceHistoryAch(this.DomainService).InsertPartsSpecialPriceHistoryValidate(partsSpecialPriceHistory);
                    }
                }
            } else if(specialTreatyPriceChange.Status.Equals(77)) {
                specialTreatyPriceChange.Status = (int)DcsSpecialTreatyPriceChangeStatus.新建;
            }
            ObjectContext.SaveChanges();
            #endregion
        }

        public void 批量审核配件配件特殊协议价格变更申请(List<SpecialTreatyPriceChange> specialTreatyPriceChanges) {
            specialTreatyPriceChanges.ForEach(r => 审核配件配件特殊协议价格变更申请(r));
        }

        public void 初审配件特殊协议价变更申请(SpecialTreatyPriceChange specialTreatyPriceChange) {
            var dbSpecialTreatyPriceChange = ObjectContext.SpecialTreatyPriceChanges.Where(r => r.Id == specialTreatyPriceChange.Id && r.Status == (int)DcsSpecialTreatyPriceChangeStatus.已提交).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbSpecialTreatyPriceChange == null)
                throw new ValidationException(ErrorStrings.SpecialTreatyPriceChange_Validation3);
            var userinfo = Utils.GetCurrentUserInfo();
            UpdateToDatabase(specialTreatyPriceChange);
            specialTreatyPriceChange.InitialApproverId = userinfo.Id;
            specialTreatyPriceChange.InitialApproverName = userinfo.Name;
            specialTreatyPriceChange.InitialApproveTime = System.DateTime.Now;
            if(specialTreatyPriceChange.Status.Equals(66)) {
                specialTreatyPriceChange.Status = (int)DcsSpecialTreatyPriceChangeStatus.初审通过;
            } else if(specialTreatyPriceChange.Status.Equals(77)) {
                specialTreatyPriceChange.Status = (int)DcsSpecialTreatyPriceChangeStatus.已提交;
            }

            this.UpdateSpecialTreatyPriceChangeValidate(specialTreatyPriceChange);

        }

        public void 审核配件特殊协议价变更申请(SpecialTreatyPriceChange specialTreatyPriceChange) {
            var dbSpecialTreatyPriceChange = ObjectContext.SpecialTreatyPriceChanges.Where(r => r.Id == specialTreatyPriceChange.Id && r.Status == (int)DcsSpecialTreatyPriceChangeStatus.初审通过).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbSpecialTreatyPriceChange == null)
                throw new ValidationException("只能审核处于“初审通过”状态下的申请单");
            var userinfo = Utils.GetCurrentUserInfo();
            UpdateToDatabase(specialTreatyPriceChange);
            specialTreatyPriceChange.ApproverId = userinfo.Id;
            specialTreatyPriceChange.ApproverName = userinfo.Name;
            specialTreatyPriceChange.ApproveTime = System.DateTime.Now;
            if(specialTreatyPriceChange.Status.Equals(66)) {
                specialTreatyPriceChange.Status = (int)DcsSpecialTreatyPriceChangeStatus.审核通过;
            } else if(specialTreatyPriceChange.Status.Equals(77)) {
                specialTreatyPriceChange.Status = (int)DcsSpecialTreatyPriceChangeStatus.已提交;
            }

            this.UpdateSpecialTreatyPriceChangeValidate(specialTreatyPriceChange);
        }

        public void 驳回配件特殊协议价变更申请(SpecialTreatyPriceChange specialTreatyPriceChange) {
            var dbSpecialTreatyPriceChange = ObjectContext.SpecialTreatyPriceChanges.Where(r => r.Id == specialTreatyPriceChange.Id && (r.Status == (int)DcsSpecialTreatyPriceChangeStatus.已提交 || r.Status == (int)DcsSpecialTreatyPriceChangeStatus.初审通过 || r.Status == (int)DcsSpecialTreatyPriceChangeStatus.审核通过)).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbSpecialTreatyPriceChange == null)
                throw new ValidationException(ErrorStrings.SpecialTreatyPriceChange_Validation4);
            var userinfo = Utils.GetCurrentUserInfo();
            if(specialTreatyPriceChange.Status == (int)DcsSpecialTreatyPriceChangeStatus.已提交) {
                specialTreatyPriceChange.InitialApproverId = userinfo.Id;
                specialTreatyPriceChange.InitialApproverName = userinfo.Name;
                specialTreatyPriceChange.InitialApproveTime = System.DateTime.Now;
            } else {
                specialTreatyPriceChange.CheckerId = userinfo.Id;
                specialTreatyPriceChange.CheckerName = userinfo.Name;
                specialTreatyPriceChange.CheckerTime = System.DateTime.Now;                
            }
            specialTreatyPriceChange.Status = (int)DcsSpecialTreatyPriceChangeStatus.新建;
            specialTreatyPriceChange.ModifierId = userinfo.Id;
            specialTreatyPriceChange.ModifierName = userinfo.Name;
            specialTreatyPriceChange.ModifyTime = System.DateTime.Now;
            UpdateToDatabase(specialTreatyPriceChange);
        }

        public void 作废配件配件特殊协议价格变更申请(SpecialTreatyPriceChange specialTreatyPriceChange) {
            var dbSpecialTreatyPriceChange = ObjectContext.SpecialTreatyPriceChanges.Where(r => r.Id == specialTreatyPriceChange.Id && r.Status == (int)DcsSpecialTreatyPriceChangeStatus.新建).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbSpecialTreatyPriceChange == null)
                throw new ValidationException(ErrorStrings.SpecialTreatyPriceChange_Validation5);
            var userinfo = Utils.GetCurrentUserInfo();
            UpdateToDatabase(specialTreatyPriceChange);
            specialTreatyPriceChange.AbandonerId = userinfo.Id;
            specialTreatyPriceChange.AbandonerName = userinfo.Name;
            specialTreatyPriceChange.AbandonerTime = System.DateTime.Now;
            specialTreatyPriceChange.Status = (int)DcsSpecialTreatyPriceChangeStatus.作废;
            this.UpdateSpecialTreatyPriceChangeValidate(specialTreatyPriceChange);
        }

        public void 修改配件特殊协议价变更申请(SpecialTreatyPriceChange specialTreatyPriceChange) {
            CheckEntityState(specialTreatyPriceChange);
            var dbSpecialTreatyPriceChange = ObjectContext.SpecialTreatyPriceChanges.Where(r => r.Id == specialTreatyPriceChange.Id && r.Status == (int)DcsSpecialTreatyPriceChangeStatus.新建).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbSpecialTreatyPriceChange == null)
                throw new ValidationException(ErrorStrings.SpecialTreatyPriceChange_Validation6);
            var specialPriceChangeListCheck = specialTreatyPriceChange.SpecialPriceChangeLists.Where(r => (!ObjectContext.PartsBranches.Any(b => r.SparePartId == b.PartId && b.PartsSalesCategoryId == specialTreatyPriceChange.BrandId && b.Status == (int)DcsBaseDataStatus.有效)));
            if(specialPriceChangeListCheck.Any()) {
                string partscodes = " ";
                foreach(var c in specialPriceChangeListCheck) {
                    partscodes = partscodes + c.SparePartCode + " ";
                }
                throw new ValidationException(string.Format(ErrorStrings.PartsSalePriceIncreaseRate_Validation2,partscodes));
            }

        }


        public void 新增配件特殊协议价变更申请(SpecialTreatyPriceChange specialTreatyPriceChange) {
            var specialPriceChangeListcheck = specialTreatyPriceChange.SpecialPriceChangeLists.Where(r => (!ObjectContext.PartsBranches.Any(b => r.SparePartId == b.PartId && b.PartsSalesCategoryId == specialTreatyPriceChange.BrandId && b.Status == (int)DcsBaseDataStatus.有效)));
            if(specialPriceChangeListcheck.Any()) {
                string partscodes = " ";
                foreach(var c in specialPriceChangeListcheck) {
                    partscodes = partscodes + c.SparePartCode + " ";
                }
                throw new ValidationException(string.Format(ErrorStrings.IvecoPriceChangeApp_Validation6,partscodes));
            }
            InsertSpecialTreatyPriceChange(specialTreatyPriceChange);
        }
        public void 提交配件配件特殊协议价格变更申请(SpecialTreatyPriceChange specialTreatyPriceChange) {
            var dbSpecialTreatyPriceChange = ObjectContext.SpecialTreatyPriceChanges.Where(r => r.Id == specialTreatyPriceChange.Id && r.Status == (int)DcsSpecialTreatyPriceChangeStatus.新建).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbSpecialTreatyPriceChange == null)
                throw new ValidationException(ErrorStrings.SpecialTreatyPriceChange_Validation5);
            var userinfo = Utils.GetCurrentUserInfo();
            UpdateToDatabase(specialTreatyPriceChange);
            specialTreatyPriceChange.Status = (int)DcsSpecialTreatyPriceChangeStatus.已提交;
            this.UpdateSpecialTreatyPriceChangeValidate(specialTreatyPriceChange);
        }

    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:11:58
        //原分布类的函数全部转移到Ach                                                               
        /// <summary>
        /// 仅供客户端使用
        /// </summary>
        [Update(UsingCustomMethod = true)]
        public void 审核配件配件特殊协议价格变更申请(SpecialTreatyPriceChange specialTreatyPriceChange) {
            new SpecialTreatyPriceChangeAch(this).审核配件配件特殊协议价格变更申请(specialTreatyPriceChange);
        }

        [Update(UsingCustomMethod = true)]
        public void 审核配件特殊协议价变更申请(SpecialTreatyPriceChange specialTreatyPriceChange) {
            new SpecialTreatyPriceChangeAch(this).审核配件特殊协议价变更申请(specialTreatyPriceChange);
        }

        public void 批量审核配件配件特殊协议价格变更申请(List<SpecialTreatyPriceChange> specialTreatyPriceChanges) {
            new SpecialTreatyPriceChangeAch(this).批量审核配件配件特殊协议价格变更申请(specialTreatyPriceChanges);
        }


        [Update(UsingCustomMethod = true)]
        public void 作废配件配件特殊协议价格变更申请(SpecialTreatyPriceChange specialTreatyPriceChange) {
            new SpecialTreatyPriceChangeAch(this).作废配件配件特殊协议价格变更申请(specialTreatyPriceChange);
        }

        public void 修改配件特殊协议价变更申请(SpecialTreatyPriceChange specialTreatyPriceChange) {
            new SpecialTreatyPriceChangeAch(this).修改配件特殊协议价变更申请(specialTreatyPriceChange);
        }


        [Update(UsingCustomMethod = true)]
        public void 新增配件特殊协议价变更申请(SpecialTreatyPriceChange specialTreatyPriceChange) {
            new SpecialTreatyPriceChangeAch(this).新增配件特殊协议价变更申请(specialTreatyPriceChange);
        }


        [Update(UsingCustomMethod = true)]
        public void 初审配件特殊协议价变更申请(SpecialTreatyPriceChange specialTreatyPriceChange) {
            new SpecialTreatyPriceChangeAch(this).初审配件特殊协议价变更申请(specialTreatyPriceChange);
        }
        [Update(UsingCustomMethod = true)]
        public void 驳回配件特殊协议价变更申请(SpecialTreatyPriceChange specialTreatyPriceChange) {
            new SpecialTreatyPriceChangeAch(this).驳回配件特殊协议价变更申请(specialTreatyPriceChange);
        }
        [Update(UsingCustomMethod = true)]
        public void 提交配件配件特殊协议价格变更申请(SpecialTreatyPriceChange specialTreatyPriceChange) {
            new SpecialTreatyPriceChangeAch(this).提交配件配件特殊协议价格变更申请(specialTreatyPriceChange);
        }
    }
}
