﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class WarehouseSequenceAch : DcsSerivceAchieveBase {
        public WarehouseSequenceAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 作废出库仓库优先顺序(WarehouseSequence warehouseSequence) {
            var dbwarehouseSequence = ObjectContext.WarehouseSequences.Where(r => r.Id == warehouseSequence.Id && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbwarehouseSequence == null)
                throw new ValidationException(ErrorStrings.OrderApproveWeekday_Validation1);
            UpdateToDatabase(warehouseSequence);
            warehouseSequence.Status = (int)DcsMasterDataStatus.作废;
            var userInfo = Utils.GetCurrentUserInfo();
            warehouseSequence.AbandonerId = userInfo.Id;
            warehouseSequence.AbandonerName = userInfo.Name;
            warehouseSequence.AbandonTime = DateTime.Now;
            this.UpdateWarehouseSequenceValidate(warehouseSequence);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:11:58
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 作废出库仓库优先顺序(WarehouseSequence warehouseSequence) {
            new WarehouseSequenceAch(this).作废出库仓库优先顺序(warehouseSequence);
        }
    }
}
