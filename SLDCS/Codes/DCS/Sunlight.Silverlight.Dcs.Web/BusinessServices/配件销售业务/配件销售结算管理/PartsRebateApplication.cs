﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Web;using Sunlight.Silverlight.Dcs.Web.Resources;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsRebateApplicationAch : DcsSerivceAchieveBase {
        public PartsRebateApplicationAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 作废配件返利申请单(PartsRebateApplication partsRebateApplication) {
            var status = ObjectContext.PartsRebateApplications.Where(r => r.Id == partsRebateApplication.Id && r.Status == (int)DcsWorkflowOfSimpleApprovalStatus.新建).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(status == null)
                throw new ValidationException(ErrorStrings.PartsRebateApplication_Validation1);
            UpdateToDatabase(partsRebateApplication);
            var userinfo = Utils.GetCurrentUserInfo();
            partsRebateApplication.Status = (int)DcsMasterDataStatus.作废;
            partsRebateApplication.AbandonerId = userinfo.Id;
            partsRebateApplication.AbandonerName = userinfo.Name;
            partsRebateApplication.AbandonTime = DateTime.Now;
            this.UpdatePartsRebateApplicationValidate(partsRebateApplication);
        }

        public void 作废配件返利申请单单据(int id) {
            var partsRebateApplication = ObjectContext.PartsRebateApplications.Where(r => r.Id == id && r.Status == (int)DcsWorkflowOfSimpleApprovalStatus.新建).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(partsRebateApplication == null)
                throw new ValidationException(ErrorStrings.PartsRebateApplication_Validation1);
            UpdateToDatabase(partsRebateApplication);
            var userinfo = Utils.GetCurrentUserInfo();
            partsRebateApplication.Status = (int)DcsMasterDataStatus.作废;
            partsRebateApplication.AbandonerId = userinfo.Id;
            partsRebateApplication.AbandonerName = userinfo.Name;
            partsRebateApplication.AbandonTime = DateTime.Now;
            this.UpdatePartsRebateApplicationValidate(partsRebateApplication);
            this.ObjectContext.SaveChanges();
        }

        public void 审核配件返利申请单(PartsRebateApplication partsRebateApplication) {
            var partsRebateAccount = ObjectContext.PartsRebateAccounts.SingleOrDefault(r => r.CustomerCompanyId == partsRebateApplication.CustomerCompanyId && r.AccountGroupId == partsRebateApplication.AccountGroupId);
            if(partsRebateAccount != null) {
                var partsRebateChangeDetail = new PartsRebateChangeDetail();
                partsRebateChangeDetail.SourceId = partsRebateApplication.Id;
                partsRebateChangeDetail.SourceCode = partsRebateApplication.Code;
                partsRebateChangeDetail.SourceType = (int)DcsPartsRebateChangeDetailSourceType.返利申请单;
                partsRebateChangeDetail.PartsRebateAccountId = partsRebateAccount.Id;
                partsRebateChangeDetail.BranchId = partsRebateApplication.BranchId;
                partsRebateChangeDetail.CustomerCompanyId = partsRebateAccount.CustomerCompanyId;
                partsRebateChangeDetail.AccountGroupId = partsRebateAccount.AccountGroupId;
                partsRebateChangeDetail.CreatorId = partsRebateApplication.ApproverId;
                partsRebateChangeDetail.CreatorName = partsRebateApplication.ApproverName;
                partsRebateChangeDetail.CreateTime = DateTime.Now;
                if(partsRebateApplication.RebateDirection == (int)DcsPartsRebateDirection.增加) {
                    //更新配件返利账户.结存金额=配件返利账户.结存金额+配件返利申请单.金额，并记录修改人，修改时间。
                    partsRebateAccount.AccountBalanceAmount += partsRebateApplication.Amount;
                    partsRebateAccount.ModifierId = partsRebateApplication.ApproverId;
                    partsRebateAccount.ModifierName = partsRebateApplication.ApproverName;
                    partsRebateAccount.ModifyTime = DateTime.Now;
                    partsRebateChangeDetail.Amount = partsRebateApplication.Amount;
                }
                if(partsRebateApplication.RebateDirection == (int)DcsPartsRebateDirection.扣减) {
                    //更新配件返利账户.结存金额=配件返利账户.结存金额-配件返利申请单.金额，并记录修改人，修改时间。
                    partsRebateAccount.AccountBalanceAmount -= partsRebateApplication.Amount;
                    partsRebateAccount.ModifierId = partsRebateApplication.ApproverId;
                    partsRebateAccount.ModifierName = partsRebateApplication.ApproverName;
                    partsRebateAccount.ModifyTime = DateTime.Now;
                    partsRebateChangeDetail.Amount = (-1) * partsRebateApplication.Amount;
                }
                InsertToDatabase(partsRebateChangeDetail);
                new PartsRebateChangeDetailAch(this.DomainService).InsertPartsRebateChangeDetailValidate(partsRebateChangeDetail);
                UpdateToDatabase(partsRebateAccount);
                new PartsRebateAccountAch(this.DomainService).UpdatePartsRebateAccountValidate(partsRebateAccount);
            }else {
                var partsRebateAccountNew = new PartsRebateAccount();
                InsertToDatabase(partsRebateAccountNew);
                partsRebateAccountNew.BranchId = partsRebateApplication.BranchId;
                partsRebateAccountNew.AccountGroupId = partsRebateApplication.AccountGroupId;
                partsRebateAccountNew.CustomerCompanyId = partsRebateApplication.CustomerCompanyId;
                partsRebateAccountNew.CustomerCompanyCode = partsRebateApplication.CustomerCompanyCode;
                partsRebateAccountNew.CustomerCompanyName = partsRebateApplication.CustomerCompanyName;
                if(partsRebateApplication.RebateDirection == (int)DcsPartsRebateDirection.增加) {
                    partsRebateAccountNew.AccountBalanceAmount = partsRebateApplication.Amount;
                }
                if(partsRebateApplication.RebateDirection == (int)DcsPartsRebateDirection.扣减) {
                    partsRebateAccountNew.AccountBalanceAmount = -partsRebateApplication.Amount;
                }
                partsRebateAccountNew.Status = (int)DcsMasterDataStatus.有效;
                partsRebateAccountNew.CreatorId = partsRebateApplication.ApproverId;
                partsRebateAccountNew.CreatorName = partsRebateApplication.ApproverName;
                partsRebateAccountNew.CreateTime = DateTime.Now;
                var partsRebateChangeDetail = new PartsRebateChangeDetail();
                partsRebateChangeDetail.BranchId = partsRebateApplication.BranchId;
                partsRebateChangeDetail.SourceId = partsRebateApplication.Id;
                partsRebateChangeDetail.SourceCode = partsRebateApplication.Code;
                partsRebateChangeDetail.SourceType = (int)DcsPartsRebateChangeDetailSourceType.返利申请单;
                partsRebateChangeDetail.PartsRebateAccountId = partsRebateAccountNew.Id;
                partsRebateChangeDetail.CustomerCompanyId = partsRebateAccountNew.CustomerCompanyId;
                partsRebateChangeDetail.AccountGroupId = partsRebateAccountNew.AccountGroupId;
                partsRebateChangeDetail.Amount = partsRebateAccountNew.AccountBalanceAmount;
                partsRebateChangeDetail.CreatorId = partsRebateApplication.ApproverId;
                partsRebateChangeDetail.CreatorName = partsRebateApplication.ApproverName;
                partsRebateChangeDetail.CreateTime = DateTime.Now;
                InsertToDatabase(partsRebateChangeDetail);
                new PartsRebateChangeDetailAch(this.DomainService).InsertPartsRebateChangeDetailValidate(partsRebateChangeDetail);
                InsertToDatabase(partsRebateAccountNew);
                new PartsRebateAccountAch(this.DomainService).InsertPartsRebateAccountValidate(partsRebateAccountNew);
            }
            partsRebateApplication.Status = (int)DcsWorkflowOfSimpleApprovalStatus.已审核;
            UpdateToDatabase(partsRebateApplication);
            var userInfo = Utils.GetCurrentUserInfo();
            partsRebateApplication.ApproverId = userInfo.Id;
            partsRebateApplication.ApproverName = userInfo.Name;
            partsRebateApplication.ApproveTime = DateTime.Now;

        }

        public void 批量审核配件返利申请(int[] ids, string approvalComment) {
            try {
                #region 逻辑
                int tempId = -1;
                var userInfo = Utils.GetCurrentUserInfo();
                var partsRebateApplications = ObjectContext.PartsRebateApplications.Where(r => ids.Contains(r.Id));
                var haveAddedAccount = new List<PartsRebateAccount>();
                foreach(var partsRebateApplication in partsRebateApplications) {
                    var partsRebateAccount = ObjectContext.PartsRebateAccounts.SingleOrDefault(r => r.CustomerCompanyId == partsRebateApplication.CustomerCompanyId && r.AccountGroupId == partsRebateApplication.AccountGroupId && r.Status == (int)DcsMasterDataStatus.有效);
                    if(partsRebateAccount != null) {
                        var partsRebateChangeDetail = new PartsRebateChangeDetail();
                        partsRebateChangeDetail.SourceId = partsRebateApplication.Id;
                        partsRebateChangeDetail.SourceCode = partsRebateApplication.Code;
                        partsRebateChangeDetail.SourceType = (int)DcsPartsRebateChangeDetailSourceType.返利申请单;
                        partsRebateChangeDetail.PartsRebateAccountId = partsRebateAccount.Id;
                        partsRebateChangeDetail.BranchId = partsRebateApplication.BranchId;
                        partsRebateChangeDetail.CustomerCompanyId = partsRebateAccount.CustomerCompanyId;
                        partsRebateChangeDetail.AccountGroupId = partsRebateAccount.AccountGroupId;
                        partsRebateChangeDetail.CreatorId = partsRebateApplication.ApproverId;
                        partsRebateChangeDetail.CreatorName = partsRebateApplication.ApproverName;
                        partsRebateChangeDetail.CreateTime = DateTime.Now;
                        if(partsRebateApplication.RebateDirection == (int)DcsPartsRebateDirection.增加) {
                            //更新配件返利账户.结存金额=配件返利账户.结存金额+配件返利申请单.金额，并记录修改人，修改时间。
                            partsRebateAccount.AccountBalanceAmount += partsRebateApplication.Amount;
                            partsRebateAccount.ModifierId = partsRebateApplication.ApproverId;
                            partsRebateAccount.ModifierName = partsRebateApplication.ApproverName;
                            partsRebateAccount.ModifyTime = DateTime.Now;
                            partsRebateChangeDetail.Amount = partsRebateApplication.Amount;
                        }
                        if(partsRebateApplication.RebateDirection == (int)DcsPartsRebateDirection.扣减) {
                            //更新配件返利账户.结存金额=配件返利账户.结存金额-配件返利申请单.金额，并记录修改人，修改时间。
                            partsRebateAccount.AccountBalanceAmount -= partsRebateApplication.Amount;
                            partsRebateAccount.ModifierId = partsRebateApplication.ApproverId;
                            partsRebateAccount.ModifierName = partsRebateApplication.ApproverName;
                            partsRebateAccount.ModifyTime = DateTime.Now;
                            partsRebateChangeDetail.Amount = (-1) * partsRebateApplication.Amount;
                        }
                        InsertToDatabase(partsRebateChangeDetail);
                        new PartsRebateChangeDetailAch(this.DomainService).InsertPartsRebateChangeDetailValidate(partsRebateChangeDetail);
                        UpdateToDatabase(partsRebateAccount);
                        new PartsRebateAccountAch(this.DomainService).UpdatePartsRebateAccountValidate(partsRebateAccount);
                    }else {
                        var partsRebateAccountNew = haveAddedAccount.SingleOrDefault(r => r.CustomerCompanyId == partsRebateApplication.CustomerCompanyId && r.AccountGroupId == partsRebateApplication.AccountGroupId && r.Status == (int)DcsMasterDataStatus.有效);
                        if(partsRebateAccountNew == null) {
                            partsRebateAccountNew = new PartsRebateAccount();
                            InsertToDatabase(partsRebateAccountNew);
                            partsRebateAccountNew.Id = tempId;
                            tempId--;
                            partsRebateAccountNew.BranchId = partsRebateApplication.BranchId;
                            partsRebateAccountNew.AccountGroupId = partsRebateApplication.AccountGroupId;
                            partsRebateAccountNew.CustomerCompanyId = partsRebateApplication.CustomerCompanyId;
                            partsRebateAccountNew.CustomerCompanyCode = partsRebateApplication.CustomerCompanyCode;
                            partsRebateAccountNew.CustomerCompanyName = partsRebateApplication.CustomerCompanyName;
                            if(partsRebateApplication.RebateDirection == (int)DcsPartsRebateDirection.增加) {
                                partsRebateAccountNew.AccountBalanceAmount = partsRebateApplication.Amount;
                            }
                            if(partsRebateApplication.RebateDirection == (int)DcsPartsRebateDirection.扣减) {
                                partsRebateAccountNew.AccountBalanceAmount = -partsRebateApplication.Amount;
                            }
                            partsRebateAccountNew.Status = (int)DcsMasterDataStatus.有效;
                            partsRebateAccountNew.CreatorId = partsRebateApplication.ApproverId;
                            partsRebateAccountNew.CreatorName = partsRebateApplication.ApproverName;
                            partsRebateAccountNew.CreateTime = DateTime.Now;
                            InsertToDatabase(partsRebateAccountNew);
                            new PartsRebateAccountAch(this.DomainService).InsertPartsRebateAccountValidate(partsRebateAccountNew);
                            haveAddedAccount.Add(partsRebateAccountNew);
                        }
                        var partsRebateChangeDetail = new PartsRebateChangeDetail();
                        partsRebateChangeDetail.BranchId = partsRebateApplication.BranchId;
                        partsRebateChangeDetail.SourceId = partsRebateApplication.Id;
                        partsRebateChangeDetail.SourceCode = partsRebateApplication.Code;
                        partsRebateChangeDetail.SourceType = (int)DcsPartsRebateChangeDetailSourceType.返利申请单;
                        partsRebateChangeDetail.PartsRebateAccountId = partsRebateAccountNew.Id;
                        partsRebateChangeDetail.CustomerCompanyId = partsRebateAccountNew.CustomerCompanyId;
                        partsRebateChangeDetail.AccountGroupId = partsRebateAccountNew.AccountGroupId;
                        partsRebateChangeDetail.Amount = partsRebateAccountNew.AccountBalanceAmount;
                        partsRebateChangeDetail.CreatorId = partsRebateApplication.ApproverId;
                        partsRebateChangeDetail.CreatorName = partsRebateApplication.ApproverName;
                        partsRebateChangeDetail.CreateTime = DateTime.Now;
                        InsertToDatabase(partsRebateChangeDetail);
                        new PartsRebateChangeDetailAch(this.DomainService).InsertPartsRebateChangeDetailValidate(partsRebateChangeDetail);
                    }
                    partsRebateApplication.Status = (int)DcsWorkflowOfSimpleApprovalStatus.已审核;
                    UpdateToDatabase(partsRebateApplication);
                    partsRebateApplication.ApproverId = userInfo.Id;
                    partsRebateApplication.ApproverName = userInfo.Name;
                    partsRebateApplication.ApproveTime = DateTime.Now;
                    partsRebateApplication.ApprovalComment = approvalComment;
                    ObjectContext.SaveChanges();
                }
                #endregion
            }catch(Exception ex) {
                throw new ValidationException(ex.Message);
            }

        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:11:58
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 作废配件返利申请单(PartsRebateApplication partsRebateApplication) {
            new PartsRebateApplicationAch(this).作废配件返利申请单(partsRebateApplication);
        }

        [Invoke]
        public void 作废配件返利申请单单据(int id) {
            new PartsRebateApplicationAch(this).作废配件返利申请单单据(id);
        }

        [Update(UsingCustomMethod = true)]
        public void 审核配件返利申请单(PartsRebateApplication partsRebateApplication) {
            new PartsRebateApplicationAch(this).审核配件返利申请单(partsRebateApplication);
        }
        
        [Invoke(HasSideEffects = true)]
        public void 批量审核配件返利申请(int[] ids, string approvalComment) {
            new PartsRebateApplicationAch(this).批量审核配件返利申请(ids,approvalComment);
        }
    }
}
