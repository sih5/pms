﻿using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsRebateTypeAch : DcsSerivceAchieveBase {
        public PartsRebateTypeAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 作废配件返利类型(PartsRebateType partsRebateType) {
            var dbPartsRebateType = ObjectContext.PartsRebateTypes.Where(r => r.Id == partsRebateType.Id && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbPartsRebateType == null)
                throw new ValidationException(ErrorStrings.PartsRebateType_Validation2);
            UpdateToDatabase(partsRebateType);
            var userinfo = Utils.GetCurrentUserInfo();
            partsRebateType.Status = (int)DcsBaseDataStatus.作废;
            partsRebateType.AbandonerId = userinfo.Id;
            partsRebateType.AbandonerName = userinfo.Name;
            partsRebateType.AbandonTime = System.DateTime.Now;
            this.UpdatePartsRebateTypeValidate(partsRebateType);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:11:58
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 作废配件返利类型(PartsRebateType partsRebateType) {
            new PartsRebateTypeAch(this).作废配件返利类型(partsRebateType);
        }
    }
}
