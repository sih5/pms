﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsSalesRtnSettlementAch : DcsSerivceAchieveBase {
        public PartsSalesRtnSettlementAch(DcsDomainService domainService)
            : base(domainService) {
        }

        public void 验证过账日期是否有效(DateTime? invoiceDate) {
            string year = invoiceDate.Value.Year.ToString();
            string month = invoiceDate.Value.Month.ToString();
            var accountPeriod = ObjectContext.AccountPeriods.Where(r => r.Month == month && r.Year == year).SingleOrDefault();
            if(null == accountPeriod) {
                throw new ValidationException(ErrorStrings.AccountPeriod_Validation2);
            } else if((int)DcsAccountPeriodStatus.已关闭 == accountPeriod.Status) {
                throw new ValidationException(ErrorStrings.AccountPeriod_Validation3);
            }
        }
        public void 生成配件销售退货结算单(PartsSalesRtnSettlement partsSalesRtnSettlement) {
            if(null != partsSalesRtnSettlement.InvoiceDate.Value) {
                //当过帐日期不为空时，验证过帐日期是否有效
                this.验证过账日期是否有效(partsSalesRtnSettlement.InvoiceDate);
            }
            decimal? inDiscount = 0;
            var partsSalesRtnSettlementRefs = partsSalesRtnSettlement.PartsSalesRtnSettlementRefs;
            var sourceIds = partsSalesRtnSettlementRefs.Select(r => r.SourceId);
            var partsInboundCheckBills = ObjectContext.PartsInboundCheckBills.Where(r => sourceIds.Contains(r.Id));
            foreach(var partsSalesRtnSettlementRef in partsSalesRtnSettlementRefs) {
                var partsInboundCheckBill = partsInboundCheckBills.SingleOrDefault(r => r.Id == partsSalesRtnSettlementRef.SourceId && r.SettlementStatus == (int)DcsPartsSettlementStatus.待结算);
                if(partsInboundCheckBill == null)
                    throw new ValidationException(ErrorStrings.PartsSalesRtnSettlement_Validation1);
                partsInboundCheckBill.SettlementStatus = (int)DcsPartsSettlementStatus.已结算;
                new PartsInboundCheckBillAch(this.DomainService).UpdatePartsInboundCheckBillValidate(partsInboundCheckBill);
                //查询入库检验单的清单
                var inDetails = ObjectContext.PartsInboundCheckBillDetails.Where(r => r.PartsInboundCheckBillId == partsInboundCheckBill.Id).ToArray();
                foreach(var item in inDetails) {
                    inDiscount += ((item.OriginalPrice ?? 0) - (item.SettlementPrice)) * item.InspectedQuantity;
                }
            }
            //折扣金额改为汇总统计：（(入库清单.零售价- 入库单清单.结算价) *数量））/（1+税率）
            partsSalesRtnSettlement.Discount = inDiscount / Convert.ToDecimal((1 + partsSalesRtnSettlement.TaxRate));
            if(partsSalesRtnSettlement.PartsSalesRtnSettlementDetails.Sum(r => r.SettlementAmount) != partsSalesRtnSettlement.TotalSettlementAmount) {
                throw new ValidationException(ErrorStrings.PartsSalesRtnSettlement_Validation11);
            }
            if(partsSalesRtnSettlement.PartsSalesRtnSettlementRefs.GroupBy(r => r.SourceCode).Any(r => r.Count() > 1)) {
                throw new ValidationException(ErrorStrings.PartsSalesRtnSettlement_Validation12);
            }
        }


        public void 修改配件销售退货结算单(PartsSalesRtnSettlement partsSalesRtnSettlement) {
            CheckEntityState(partsSalesRtnSettlement);
            var dbpartsSalesRtnSettlement = ObjectContext.PartsSalesRtnSettlements.Where(r => r.Id == partsSalesRtnSettlement.Id && r.Status == (int)DcsPartsSalesRtnSettlementStatus.新建).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbpartsSalesRtnSettlement == null)
                throw new ValidationException(ErrorStrings.PartsSalesRtnSettlement_Validation2);
            if(null != partsSalesRtnSettlement.InvoiceDate.Value) {
                //当过帐日期不为空时，验证过帐日期是否有效
                this.验证过账日期是否有效(partsSalesRtnSettlement.InvoiceDate);
            }
            decimal? inDiscount = 0;
            var partsSalesRtnSettlementRefs = ChangeSet.GetAssociatedChanges(partsSalesRtnSettlement, r => r.PartsSalesRtnSettlementRefs).Cast<PartsSalesRtnSettlementRef>().ToArray();
            var insertPartsSalesRtnSettlementRefs = partsSalesRtnSettlementRefs.Where(r => ChangeSet.GetChangeOperation(r) == ChangeOperation.Insert && partsSalesRtnSettlementRefs.Any(v => ChangeSet.GetChangeOperation(v) == ChangeOperation.Delete && v.SourceId == r.SourceId)).ToArray();
            var deletePartsSalesRtnSettlementRefs = partsSalesRtnSettlementRefs.Where(r => ChangeSet.GetChangeOperation(r) == ChangeOperation.Delete && partsSalesRtnSettlementRefs.Any(v => ChangeSet.GetChangeOperation(v) == ChangeOperation.Insert && v.SourceId == r.SourceId)).ToArray();

            //待处理的关联单
            var pendingPartsSalesRtnSettlementRefs = partsSalesRtnSettlementRefs.Except(insertPartsSalesRtnSettlementRefs).Except(deletePartsSalesRtnSettlementRefs).ToArray();
            var sourceIds = pendingPartsSalesRtnSettlementRefs.Select(r => r.SourceId);
            var partsInboundCheckBills = ObjectContext.PartsInboundCheckBills.Where(r => sourceIds.Contains(r.Id)).ToArray();
            foreach(var partsSalesRtnSettlementRef in pendingPartsSalesRtnSettlementRefs) {
                switch(ChangeSet.GetChangeOperation(partsSalesRtnSettlementRef)) {
                    case ChangeOperation.Insert:
                        var pendingSettlementPartsInboundCheckBill = partsInboundCheckBills.SingleOrDefault(r => r.Id == partsSalesRtnSettlementRef.SourceId && r.SettlementStatus == (int)DcsPartsSettlementStatus.已结算);
                        if(pendingSettlementPartsInboundCheckBill == null)
                            throw new ValidationException(ErrorStrings.PartsSalesRtnSettlement_Validation13);
                        new PartsInboundCheckBillAch(this.DomainService).UpdatePartsInboundCheckBillValidate(pendingSettlementPartsInboundCheckBill);
                        //查询入库检验单的清单
                        var inDetails = ObjectContext.PartsInboundCheckBillDetails.Where(r => r.PartsInboundCheckBillId == pendingSettlementPartsInboundCheckBill.Id).ToArray();
                        foreach(var item in inDetails) {
                            inDiscount += (item.OriginalPrice - item.SettlementPrice) * item.InspectedQuantity;
                        }
                        break;
                    case ChangeOperation.Delete:
                        var partsInboundCheckBill = partsInboundCheckBills.SingleOrDefault(r => r.Id == partsSalesRtnSettlementRef.SourceId);
                        if(partsInboundCheckBill == null)
                            throw new ValidationException(string.Format(ErrorStrings.PartsSalesRtnSettlement_Validation3, partsSalesRtnSettlementRef.SourceCode));
                        partsInboundCheckBill.SettlementStatus = (int)DcsPartsSettlementStatus.待结算;
                        new PartsInboundCheckBillAch(this.DomainService).UpdatePartsInboundCheckBillValidate(partsInboundCheckBill);
                        break;

                }
            }
            if(pendingPartsSalesRtnSettlementRefs.Count() == 0) {
                var rtnref = ObjectContext.PartsSalesRtnSettlementRefs.Where(r => r.PartsSalesRtnSettlementId == partsSalesRtnSettlement.Id).ToArray();
                var sourceIdsRef = rtnref.Select(r => r.SourceId);
                var partsInboundCheckBillsRef = ObjectContext.PartsInboundCheckBills.Where(r => sourceIdsRef.Contains(r.Id)).ToArray();
                foreach(var rtn in rtnref) {
                    var partsInboundCheckBillNone = partsInboundCheckBillsRef.SingleOrDefault(r => r.Id == rtn.SourceId);
                    if(partsInboundCheckBillNone == null)
                        throw new ValidationException(string.Format(ErrorStrings.PartsSalesRtnSettlement_Validation3, rtn.SourceCode));
                    //查询入库检验单的清单
                    var inNoneDetails = ObjectContext.PartsInboundCheckBillDetails.Where(r => r.PartsInboundCheckBillId == partsInboundCheckBillNone.Id).ToArray();
                    foreach(var item in inNoneDetails) {
                        inDiscount += ((item.OriginalPrice ?? 0) - (item.SettlementPrice)) * item.InspectedQuantity;
                    }
                }
            }
            //折扣金额改为汇总统计：（(入库清单.零售价- 入库单清单.结算价) *数量））/（1+税率）
            partsSalesRtnSettlement.Discount = inDiscount / Convert.ToDecimal((1 + partsSalesRtnSettlement.TaxRate));
            UpdateToDatabase(partsSalesRtnSettlement);
        }


        public void 审批配件销售退货结算单(PartsSalesRtnSettlement partsSalesRtnSettlement) {
            var dbpartsSalesRtnSettlement = ObjectContext.PartsSalesRtnSettlements.Where(r => r.Id == partsSalesRtnSettlement.Id && r.Status == (int)DcsPartsPurchaseSettleStatus.新建).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbpartsSalesRtnSettlement == null)
                throw new ValidationException(ErrorStrings.PartsSalesRtnSettlement_Validation4);
            if(null != dbpartsSalesRtnSettlement.InvoiceDate.Value) {
                //当过帐日期不为空时，验证过帐日期是否有效
                this.验证过账日期是否有效(dbpartsSalesRtnSettlement.InvoiceDate);
            }
            UpdateToDatabase(partsSalesRtnSettlement);
            partsSalesRtnSettlement.Status = (int)DcsPartsPurchaseSettleStatus.已审批;
            var userInfo = Utils.GetCurrentUserInfo();
            partsSalesRtnSettlement.ApproverId = userInfo.Id;
            partsSalesRtnSettlement.ApproverName = userInfo.Name;
            partsSalesRtnSettlement.ApproveTime = DateTime.Now;
            this.UpdatePartsSalesRtnSettlementValidate(partsSalesRtnSettlement);
            new CustomerAccountAch(this.DomainService).销售退货结算根据结算金额过账(partsSalesRtnSettlement);
            if(partsSalesRtnSettlement.PartsSalesRtnSettlementDetails.Sum(r => r.SettlementAmount) != partsSalesRtnSettlement.TotalSettlementAmount) {
                throw new ValidationException(ErrorStrings.PartsSalesRtnSettlement_Validation11);
            }
            if(partsSalesRtnSettlement.PartsSalesRtnSettlementRefs.GroupBy(r => r.SourceCode).Any(r => r.Count() > 1)) {
                throw new ValidationException(ErrorStrings.PartsSalesRtnSettlement_Validation12);
            }
        }


        public void 审批虚拟配件销售退货结算单(PartsSalesRtnSettlementEx partsSalesRtnSettlementEx) {
            var partsSalesRtnSettlement = ObjectContext.PartsSalesRtnSettlements.Where(r => r.Id == partsSalesRtnSettlementEx.Id).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(partsSalesRtnSettlement == null)
                throw new ValidationException(ErrorStrings.PartsSalesRtnSettlement_Validation14);
            var dbpartsSalesRtnSettlement = ObjectContext.PartsSalesRtnSettlements.Where(r => r.Id == partsSalesRtnSettlement.Id && r.Status == (int)DcsPartsPurchaseSettleStatus.新建).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbpartsSalesRtnSettlement == null)
                throw new ValidationException(ErrorStrings.PartsSalesRtnSettlement_Validation4);
            if(null != partsSalesRtnSettlement.InvoiceDate.Value) {
                //当过帐日期不为空时，验证过帐日期是否有效
                this.验证过账日期是否有效(partsSalesRtnSettlement.InvoiceDate);
            }
            UpdateToDatabase(partsSalesRtnSettlement);
            var userInfo = Utils.GetCurrentUserInfo();
            if(Convert.ToDecimal(partsSalesRtnSettlement.TotalSettlementAmount) == 0) {
                partsSalesRtnSettlement.Status = (int)DcsPartsPurchaseSettleStatus.发票登记;
                partsSalesRtnSettlement.InvoiceRegistrationOperatorId = userInfo.Id;
                partsSalesRtnSettlement.InvoiceRegistrationOperator = UserInfo.Name;
                partsSalesRtnSettlement.InvoiceRegistrationTime = DateTime.Now;
            } else {
                partsSalesRtnSettlement.Status = (int)DcsPartsPurchaseSettleStatus.已审批;
            }
            partsSalesRtnSettlement.ApproverId = userInfo.Id;
            partsSalesRtnSettlement.ApproverName = userInfo.Name;
            partsSalesRtnSettlement.ApproveTime = DateTime.Now;
            this.UpdatePartsSalesRtnSettlementValidate(partsSalesRtnSettlement);
            var company = ObjectContext.Companies.SingleOrDefault(r => r.Id == userInfo.EnterpriseId && r.Status == (int)DcsMasterDataStatus.有效);
            if(company == null) {
                throw new ValidationException(ErrorStrings.PartsSalesRtnSettlement_Validation15);
            }
            new CustomerAccountAch(this.DomainService).销售退货结算根据结算金额过账(partsSalesRtnSettlement);
            if(partsSalesRtnSettlement.PartsSalesRtnSettlementDetails.Sum(r => r.SettlementAmount) != partsSalesRtnSettlement.TotalSettlementAmount) {
                throw new ValidationException(ErrorStrings.PartsSalesRtnSettlement_Validation16);
            }
            if(partsSalesRtnSettlement.PartsSalesRtnSettlementRefs.GroupBy(r => r.SourceCode).Any(r => r.Count() > 1)) {
                throw new ValidationException(ErrorStrings.PartsSalesRtnSettlement_Validation12);
            }


        }



        //public void 反结算配件销售退货结算单(PartsSalesRtnSettlement partsSalesRtnSettlement) {
        public void 反结算配件销售退货结算单(PartsSalesRtnSettlementEx partsSalesRtnSettlementEx) {
            var partsSalesRtnSettlement = ObjectContext.PartsSalesRtnSettlements.Where(r => r.Id == partsSalesRtnSettlementEx.Id).SetMergeOption(MergeOption.OverwriteChanges).SingleOrDefault();
            if(partsSalesRtnSettlement == null)
                throw new ValidationException(ErrorStrings.PartsSalesRtnSettlement_Validation14);

            var dbpartsSalesRtnSettlement = ObjectContext.PartsSalesRtnSettlements.Where(r => r.Id == partsSalesRtnSettlement.Id && (r.Status == (int)DcsPartsPurchaseSettleStatus.发票登记 || r.Status == (int)DcsPartsPurchaseSettleStatus.已审批)).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbpartsSalesRtnSettlement == null)
                throw new ValidationException(ErrorStrings.PartsSalesRtnSettlement_Validation5);
            var userInfo = Utils.GetCurrentUserInfo();
            var company = ObjectContext.Companies.SingleOrDefault(r => r.Id == userInfo.EnterpriseId && r.Status == (int)DcsMasterDataStatus.有效);
            if(company == null) {
                throw new ValidationException(ErrorStrings.PartsSalesRtnSettlement_Validation15);
            }
            //if(company.Type == (int)DcsCompanyType.分公司) {
            //    if(dbpartsSalesRtnSettlement.Status == (int)DcsPartsPurchaseSettleStatus.发票登记) {
            //        new CustomerAccountAch(this.DomainService).销售退货结算过账(partsSalesRtnSettlement);
            //    }
            //}else {
            partsSalesRtnSettlement.TotalSettlementAmount = -1 * partsSalesRtnSettlement.TotalSettlementAmount;
            new CustomerAccountAch(this.DomainService).销售退货结算根据结算金额过账(partsSalesRtnSettlement);
            partsSalesRtnSettlement.TotalSettlementAmount = -1 * partsSalesRtnSettlement.TotalSettlementAmount;
            //}

            if(partsSalesRtnSettlement.Status == (int)DcsPartsPurchaseSettleStatus.发票登记) {
                var salesRtnSettleInvoiceRels = ObjectContext.SalesRtnSettleInvoiceRels.Where(r => r.PartsSalesRtnSettlementId == partsSalesRtnSettlement.Id).ToArray();
                if(!salesRtnSettleInvoiceRels.Any()) {
                    throw new ValidationException(ErrorStrings.PartsSalesRtnSettlement_Validation17);
                }
                var invoiceIds = salesRtnSettleInvoiceRels.Select(r => r.InvoiceId).ToArray();
                var partsSalesRtnSettlements = (from a in ObjectContext.SalesRtnSettleInvoiceRels.Where(r => invoiceIds.Contains(r.InvoiceId))
                                                join b in ObjectContext.PartsSalesRtnSettlements on a.PartsSalesRtnSettlementId equals b.Id
                                                select b).ToArray();
                var invoiceInformations = ObjectContext.InvoiceInformations.Where(r => invoiceIds.Contains(r.Id) && r.SourceType == (int)DcsInvoiceInformationSourceType.配件销售退货结算单 && r.Status != (int)DcsInvoiceInformationStatus.作废);
                foreach(var invoiceInformation in invoiceInformations) {
                    invoiceInformation.Status = (int)DcsInvoiceInformationStatus.作废;
                    new InvoiceInformationAch(this.DomainService).UpdateInvoiceInformationValidate(invoiceInformation);
                }
                foreach(var item in partsSalesRtnSettlements) {
                    CancelPartsSalesRtnSettlementBill(item);
                }
            } else if(partsSalesRtnSettlement.Status == (int)DcsPartsPurchaseSettleStatus.已审批) {
                CancelPartsSalesRtnSettlementBill(partsSalesRtnSettlement);
            }

            if(partsSalesRtnSettlement.PartsSalesRtnSettlementDetails.Sum(r => r.SettlementAmount) != partsSalesRtnSettlement.TotalSettlementAmount) {
                throw new ValidationException(ErrorStrings.PartsSalesRtnSettlement_Validation11);
            }
            if(partsSalesRtnSettlement.PartsSalesRtnSettlementRefs.GroupBy(r => r.SourceCode).Any(r => r.Count() > 1)) {
                throw new ValidationException(ErrorStrings.PartsSalesRtnSettlement_Validation12);
            }

        }

        internal void CancelPartsSalesRtnSettlementBill(PartsSalesRtnSettlement partsSalesRtnSettlement) {
            var partsSalesRtnSettlementRefs = ObjectContext.PartsSalesRtnSettlementRefs.Where(r => r.PartsSalesRtnSettlementId == partsSalesRtnSettlement.Id).ToArray();
            var sourceIds = partsSalesRtnSettlementRefs.Select(r => r.SourceId);
            var partsInboundCheckBills = ObjectContext.PartsInboundCheckBills.Where(r => sourceIds.Contains(r.Id)).ToArray();
            foreach(var partsSalesRtnSettlementRef in partsSalesRtnSettlementRefs) {
                var partsInboundCheckBill = partsInboundCheckBills.SingleOrDefault(r => r.Id == partsSalesRtnSettlementRef.SourceId);
                if(partsInboundCheckBill == null)
                    throw new ValidationException(string.Format(ErrorStrings.PartsSalesRtnSettlement_Validation3, partsSalesRtnSettlementRef.SourceCode));
                partsInboundCheckBill.SettlementStatus = (int)DcsPartsSettlementStatus.待结算;
                new PartsInboundCheckBillAch(this.DomainService).UpdatePartsInboundCheckBillValidate(partsInboundCheckBill);
            }
            var offsettedPartsSalesRtnSettlement = new PartsSalesRtnSettlement {
                SettlementPath = (int)DcsPartsPurchaseSettleBillSettlementPath.反冲结算,
                SalesCompanyId = partsSalesRtnSettlement.SalesCompanyId,
                SalesCompanyCode = partsSalesRtnSettlement.SalesCompanyCode,
                SalesCompanyName = partsSalesRtnSettlement.SalesCompanyName,
                PartsSalesCategoryId = partsSalesRtnSettlement.PartsSalesCategoryId,
                PartsSalesCategoryName = partsSalesRtnSettlement.PartsSalesCategoryName,
                CustomerCompanyId = partsSalesRtnSettlement.CustomerCompanyId,
                CustomerCompanyCode = partsSalesRtnSettlement.CustomerCompanyCode,
                CustomerCompanyName = partsSalesRtnSettlement.CustomerCompanyName,
                AccountGroupId = partsSalesRtnSettlement.AccountGroupId,
                AccountGroupCode = partsSalesRtnSettlement.AccountGroupCode,
                AccountGroupName = partsSalesRtnSettlement.AccountGroupName,
                CustomerAccountId = partsSalesRtnSettlement.CustomerAccountId,
                TotalSettlementAmount = -partsSalesRtnSettlement.TotalSettlementAmount,
                InvoiceAmountDifference = -partsSalesRtnSettlement.InvoiceAmountDifference,
                OffsettedSettlementBillId = partsSalesRtnSettlement.Id,
                OffsettedSettlementBillCode = partsSalesRtnSettlement.Code,
                TaxRate = partsSalesRtnSettlement.TaxRate,
                Tax = -partsSalesRtnSettlement.Tax,
                InvoicePath = partsSalesRtnSettlement.InvoicePath,
                Status = (int)DcsPartsSalesRtnSettlementStatus.已审批,
            };
            InsertToDatabase(offsettedPartsSalesRtnSettlement);
            this.InsertPartsSalesRtnSettlementValidate(offsettedPartsSalesRtnSettlement);

            UpdateToDatabase(partsSalesRtnSettlement);
            partsSalesRtnSettlement.Status = (int)DcsPartsPurchaseSettleStatus.已反冲;
            this.UpdatePartsSalesRtnSettlementValidate(partsSalesRtnSettlement);
        }


        //public void 作废配件销售退货结算单(PartsSalesRtnSettlement partsSalesRtnSettlement) {
        public void 作废配件销售退货结算单(PartsSalesRtnSettlementEx partsSalesRtnSettlementEx) {
            var partsSalesRtnSettlement = ObjectContext.PartsSalesRtnSettlements.Where(r => r.Id == partsSalesRtnSettlementEx.Id).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(partsSalesRtnSettlement == null)
                throw new ValidationException(ErrorStrings.PartsSalesRtnSettlement_Validation14);

            var dbpartsSalesRtnSettlement = ObjectContext.PartsSalesRtnSettlements.Where(r => r.Id == partsSalesRtnSettlement.Id && r.Status == (int)DcsPartsPurchaseSettleStatus.新建).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbpartsSalesRtnSettlement == null)
                throw new ValidationException(ErrorStrings.PartsSalesRtnSettlement_Validation6);
            var partsSalesRtnSettlementRefs = partsSalesRtnSettlement.PartsSalesRtnSettlementRefs;
            var sourceIds = partsSalesRtnSettlementRefs.Select(r => r.SourceId);
            var partsInboundCheckBills = ObjectContext.PartsInboundCheckBills.Where(r => sourceIds.Contains(r.Id));
            foreach(var partsSalesRtnSettlementRef in partsSalesRtnSettlementRefs) {
                var partsInboundCheckBill = partsInboundCheckBills.SingleOrDefault(r => r.Id == partsSalesRtnSettlementRef.SourceId);
                if(partsInboundCheckBill == null)
                    throw new ValidationException(string.Format(ErrorStrings.PartsSalesRtnSettlement_Validation3, partsSalesRtnSettlementRef.SourceCode));
                partsInboundCheckBill.SettlementStatus = (int)DcsPartsSettlementStatus.待结算;
                new PartsInboundCheckBillAch(this.DomainService).UpdatePartsInboundCheckBillValidate(partsInboundCheckBill);
            }
            UpdateToDatabase(partsSalesRtnSettlement);
            partsSalesRtnSettlement.Status = (int)DcsPartsPurchaseSettleStatus.作废;
            var userInfo = Utils.GetCurrentUserInfo();
            partsSalesRtnSettlement.AbandonerId = userInfo.Id;
            partsSalesRtnSettlement.AbandonerName = userInfo.Name;
            partsSalesRtnSettlement.AbandonTime = DateTime.Now;
            this.UpdatePartsSalesRtnSettlementValidate(partsSalesRtnSettlement);
        }


        public void 配件销售退货结算单发票登记(PartsSalesRtnSettlement partsSalesRtnSettlement, decimal invoiceAmount) {
            if(invoiceAmount <= 0)
                throw new ValidationException(ErrorStrings.PartsSalesRtnSettlement_Validation7);
            var dbpartsSalesRtnSettlement = ObjectContext.PartsSalesRtnSettlements.Where(r => r.Id == partsSalesRtnSettlement.Id && r.Status == (int)DcsPartsPurchaseSettleStatus.已审批).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbpartsSalesRtnSettlement == null)
                throw new ValidationException(ErrorStrings.PartsSalesRtnSettlement_Validation8);
            UpdateToDatabase(partsSalesRtnSettlement);
            partsSalesRtnSettlement.Status = (int)DcsPartsPurchaseSettleStatus.发票登记;
            partsSalesRtnSettlement.InvoiceAmountDifference = partsSalesRtnSettlement.TotalSettlementAmount - invoiceAmount;
            var userInfo = Utils.GetCurrentUserInfo();
            partsSalesRtnSettlement.InvoiceRegistrationOperatorId = userInfo.Id;
            partsSalesRtnSettlement.InvoiceRegistrationOperator = userInfo.Name;
            partsSalesRtnSettlement.InvoiceRegistrationTime = DateTime.Now;
            this.UpdatePartsSalesRtnSettlementValidate(partsSalesRtnSettlement);
            if(partsSalesRtnSettlement.PartsSalesRtnSettlementDetails.Sum(r => r.SettlementAmount) != partsSalesRtnSettlement.TotalSettlementAmount) {
                throw new ValidationException(ErrorStrings.PartsSalesRtnSettlement_Validation11);
            }
            if(partsSalesRtnSettlement.PartsSalesRtnSettlementRefs.GroupBy(r => r.SourceCode).Any(r => r.Count() > 1)) {
                throw new ValidationException(ErrorStrings.PartsSalesRtnSettlement_Validation12);
            }
        }

    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:11:58
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 生成配件销售退货结算单(PartsSalesRtnSettlement partsSalesRtnSettlement) {
            new PartsSalesRtnSettlementAch(this).生成配件销售退货结算单(partsSalesRtnSettlement);
        }


        [Update(UsingCustomMethod = true)]
        public void 修改配件销售退货结算单(PartsSalesRtnSettlement partsSalesRtnSettlement) {
            new PartsSalesRtnSettlementAch(this).修改配件销售退货结算单(partsSalesRtnSettlement);
        }


        [Update(UsingCustomMethod = true)]
        public void 审批配件销售退货结算单(PartsSalesRtnSettlement partsSalesRtnSettlement) {
            new PartsSalesRtnSettlementAch(this).审批配件销售退货结算单(partsSalesRtnSettlement);
        }


        [Update(UsingCustomMethod = true)]
        public void 审批虚拟配件销售退货结算单(PartsSalesRtnSettlementEx partsSalesRtnSettlementEx) {
            new PartsSalesRtnSettlementAch(this).审批虚拟配件销售退货结算单(partsSalesRtnSettlementEx);
        }
        public void 反结算配件销售退货结算单(PartsSalesRtnSettlementEx partsSalesRtnSettlementEx) {
            new PartsSalesRtnSettlementAch(this).反结算配件销售退货结算单(partsSalesRtnSettlementEx);
        }
        public void 作废配件销售退货结算单(PartsSalesRtnSettlementEx partsSalesRtnSettlementEx) {
            new PartsSalesRtnSettlementAch(this).作废配件销售退货结算单(partsSalesRtnSettlementEx);
        }


        [Update(UsingCustomMethod = true)]
        public void 配件销售退货结算单发票登记(PartsSalesRtnSettlement partsSalesRtnSettlement, decimal invoiceAmount) {
            new PartsSalesRtnSettlementAch(this).配件销售退货结算单发票登记(partsSalesRtnSettlement, invoiceAmount);
        }
    }
}