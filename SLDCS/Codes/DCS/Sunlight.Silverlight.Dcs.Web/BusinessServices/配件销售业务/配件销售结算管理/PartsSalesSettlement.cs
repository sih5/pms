﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using System.Transactions;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsSalesSettlementAch : DcsSerivceAchieveBase {
        public PartsSalesSettlementAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 生成配件销售结算单(PartsSalesSettlement partsSalesSettlement) {
            using(var transaction = new TransactionScope()) {
                if (null != partsSalesSettlement.InvoiceDate.Value)
                {
                    //当过帐日期不为空时，验证过帐日期是否有效
                    this.验证过账日期是否有效(partsSalesSettlement.InvoiceDate);
                }
                var checkdetail = partsSalesSettlement.PartsSalesSettlementDetails.Any(r => r.QuantityToSettle < 0);
                if(checkdetail) {
                    throw new ValidationException(ErrorStrings.PartsSalesSettlement_Validation17);
                }
                //if(partsSalesSettlement.RebateMethod == (int)DcsPartsSalesSettlementRebateMethod.返利池 && partsSalesSettlement.RebateAmount > 0) {
                //    var partsRebateAccounts = ObjectContext.PartsRebateAccounts.Where(r => r.AccountGroupId == partsSalesSettlement.AccountGroupId && r.CustomerCompanyId == partsSalesSettlement.CustomerCompanyId && r.Status == (int)DcsMasterDataStatus.有效).ToArray();
                //    if(partsRebateAccounts.Length > 1)
                //        throw new ValidationException(ErrorStrings.PartsSalesSettlement_Validation1);
                //    if(partsRebateAccounts.Length == 0)
                //        throw new ValidationException(ErrorStrings.PartsSalesSettlement_Validation2);
                //    var sumRebateAmount = ObjectContext.PartsSalesSettlements.Where(r => r.CustomerAccountId == partsSalesSettlement.CustomerAccountId && r.Status == (int)DcsPartsSalesSettlementStatus.新建).ToArray().Sum(r => r.RebateAmount);
                //    if(partsSalesSettlement.RebateAmount > partsRebateAccounts.Single().AccountBalanceAmount - sumRebateAmount)
                //        throw new ValidationException(ErrorStrings.PartsSalesSettlement_Validation3);
                //}
                var partsSalesSettlementRefs = partsSalesSettlement.PartsSalesSettlementRefs;
                var partsOutboundBillIds = partsSalesSettlementRefs.Where(r => r.SourceType == (int)DcsPartsSalesSettlementRefSourceType.配件出库单).Select(r => r.SourceId);
                var partsInboundCheckBillIds = partsSalesSettlementRefs.Where(r => r.SourceType == (int)DcsPartsSalesSettlementRefSourceType.配件入库检验单).Select(r => r.SourceId);
                var partsOutboundBills = ObjectContext.PartsOutboundBills.Where(r => partsOutboundBillIds.Contains(r.Id)).ToArray();
                var partsInboundCheckBills = ObjectContext.PartsInboundCheckBills.Where(r => partsInboundCheckBillIds.Contains(r.Id)).ToArray();
                /**
                  * 销售结算单生成、修改、审核通过后，计算 折让不含税金额= （(（销售出库单清单.原始价格- 出库单清单.结算价）*数量)  - ((入库清单.零售价- 入库单清单.结算价) *数量））/（1+税率）
                 **/
                decimal? outRebateAmount = 0;
                decimal? inRebateAmount = 0;
                foreach(var partsSalesSettlementRef in partsSalesSettlementRefs) {
                    switch(partsSalesSettlementRef.SourceType) {
                        case (int)DcsPartsSalesSettlementRefSourceType.配件出库单:
                            var partsOutboundBill = partsOutboundBills.SingleOrDefault(r => r.Id == partsSalesSettlementRef.SourceId && r.SettlementStatus == (int)DcsPartsSettlementStatus.待结算);
                            if(partsOutboundBill == null)
                                throw new ValidationException(ErrorStrings.PartsSalesSettlement_Validation4);
                            partsOutboundBill.SettlementStatus = (int)DcsPartsSettlementStatus.已结算;
                            new PartsOutboundBillAch(this.DomainService).UpdatePartsOutboundBillValidate(partsOutboundBill);
                            //查询出库清单
                            var outDetails = ObjectContext.PartsOutboundBillDetails.Where(r=>r.PartsOutboundBillId==partsOutboundBill.Id).ToArray();
                            foreach(var item in outDetails){
                                if(item.OriginalPrice > item.SettlementPrice) {
                                    outRebateAmount += (item.OriginalPrice - item.SettlementPrice) * item.OutboundAmount;
                                }
                            }
                            break;
                        case (int)DcsPartsSalesSettlementRefSourceType.配件入库检验单:
                            var partsInboundCheckBill = partsInboundCheckBills.SingleOrDefault(r => r.Id == partsSalesSettlementRef.SourceId && r.SettlementStatus == (int)DcsPartsSettlementStatus.待结算);
                            if(partsInboundCheckBill == null)
                                throw new ValidationException(ErrorStrings.PartsSalesSettlement_Validation5);
                            partsInboundCheckBill.SettlementStatus = (int)DcsPartsSettlementStatus.已结算;
                            new PartsInboundCheckBillAch(this.DomainService).UpdatePartsInboundCheckBillValidate(partsInboundCheckBill);
                            //查询入库清单
                            var inDetails = ObjectContext.PartsInboundCheckBillDetails.Where(r => r.PartsInboundCheckBillId == partsInboundCheckBill.Id).ToArray();
                            foreach(var item in inDetails){
                                if(item.OriginalPrice > item.SettlementPrice) {
                                    inRebateAmount += (item.OriginalPrice - item.SettlementPrice) * item.InspectedQuantity;
                                }
                            }
                            break;
                    }
                }
                //折让不含税金额= （(（销售出库单清单.原始价格- 出库单清单.结算价）*数量)  - ((入库清单.零售价- 入库单清单.结算价) *数量））/（1+税率）
                partsSalesSettlement.RebateAmount = (outRebateAmount - inRebateAmount) / Convert.ToDecimal((1 + partsSalesSettlement.TaxRate));

                this.ObjectContext.SaveChanges();
                var codes = partsSalesSettlement.PartsSalesSettlementRefs.Where(r => r.InvoiceType == (int)DcsPartsSalesSettlementRefSourceType.配件出库单).Select(r => r.SourceCode).ToArray();
                var ecommerceFreightDisposals = this.ObjectContext.EcommerceFreightDisposals.Where(r => codes.Contains(r.PartsOutboundBillCode));
                foreach(var ecommerceFreightDisposal in ecommerceFreightDisposals) {
                    ecommerceFreightDisposal.PartsSalesSettlementId = partsSalesSettlement.Id;
                    ecommerceFreightDisposal.PartsSalesSettlementCode = partsSalesSettlement.Code;
                    ecommerceFreightDisposal.SalesSettlementStatus = partsSalesSettlement.Status;
                    new EcommerceFreightDisposalAch(this.DomainService).UpdateEcommerceFreightDisposal(ecommerceFreightDisposal);
                }
                transaction.Complete();
            }
        }

        public void 生成配件销售结算单2(PartsSalesSettlement partsSalesSettlement, List<PartsSalesSettlementRef> partsSalesSettlementRefs, List<PartsSalesSettlementDetail> partsSalesSettlementDetails) {
            using(var transaction = new TransactionScope()) {
                if(null != partsSalesSettlement.InvoiceDate.Value) {
                    //当过帐日期不为空时，验证过帐日期是否有效
                    this.验证过账日期是否有效(partsSalesSettlement.InvoiceDate);
                }
                var checkdetail = partsSalesSettlement.PartsSalesSettlementDetails.Any(r => r.QuantityToSettle < 0);
                if(checkdetail) {
                    throw new ValidationException(ErrorStrings.PartsSalesSettlement_Validation17);
                }
                decimal line = 1000000;
                var partsOutboundBillIds = partsSalesSettlementRefs.Where(r => r.SourceType == (int)DcsPartsSalesSettlementRefSourceType.配件出库单).Select(r => r.SourceId);
                var partsInboundCheckBillIds = partsSalesSettlementRefs.Where(r => r.SourceType == (int)DcsPartsSalesSettlementRefSourceType.配件入库检验单).Select(r => r.SourceId);
                var partsOutboundBills = ObjectContext.PartsOutboundBills.Where(r => partsOutboundBillIds.Contains(r.Id)).ToArray();
                var partsInboundCheckBills = ObjectContext.PartsInboundCheckBills.Where(r => partsInboundCheckBillIds.Contains(r.Id)).ToArray();
                /**
                 * 销售结算单生成、修改、审核通过后，计算 折让不含税金额= （(（销售出库单清单.原始价格- 出库单清单.结算价）*数量)  - ((入库清单.零售价- 入库单清单.结算价) *数量））/（1+税率）
                **/
                    var orderDetails = partsSalesSettlementDetails.ToArray();
                     var times = partsSalesSettlement.TotalSettlementAmount / line + 1;
                     for(int i = 0; i < times; i++) {
                         if(partsSalesSettlementRefs.Any(t => t.IsSett == false)) {
                             var newPartsSalesSettlement = this.setNewSettlement(partsSalesSettlement);
                             decimal? totalSettlementAmount = 0;
                             decimal? outRebateAmount = 0;
                             decimal? inRebateAmount = 0;
                             foreach(var partsSalesSettlementRef in partsSalesSettlementRefs.Where(r => r.IsSett == false).OrderByDescending(t => t.SettlementAmount)) {
                                 if(totalSettlementAmount + partsSalesSettlementRef.SettlementAmount <= line || totalSettlementAmount == 0) {
                                     newPartsSalesSettlement.PartsSalesSettlementRefs.Add(partsSalesSettlementRef);
                                     totalSettlementAmount += partsSalesSettlementRef.SettlementAmount;
                                     partsSalesSettlementRef.IsSett = true;
                                     switch(partsSalesSettlementRef.SourceType) {
                                         case (int)DcsPartsSalesSettlementRefSourceType.配件出库单:
                                             var partsOutboundBill = partsOutboundBills.SingleOrDefault(r => r.Id == partsSalesSettlementRef.SourceId && r.SettlementStatus == (int)DcsPartsSettlementStatus.待结算);
                                             if(partsOutboundBill == null)
                                                 throw new ValidationException(ErrorStrings.PartsSalesSettlement_Validation4);
                                             partsOutboundBill.SettlementStatus = (int)DcsPartsSettlementStatus.已结算;
                                             new PartsOutboundBillAch(this.DomainService).UpdatePartsOutboundBillValidate(partsOutboundBill);
                                             //查询出库清单
                                             var outDetails = ObjectContext.PartsOutboundBillDetails.Where(r => r.PartsOutboundBillId == partsOutboundBill.Id).ToArray();
                                             foreach(var item in outDetails) {
                                                 if(item.OriginalPrice > item.SettlementPrice) {
                                                     outRebateAmount += (item.OriginalPrice - item.SettlementPrice) * item.OutboundAmount;
                                                 }
                                             }
                                             break;
                                         case (int)DcsPartsSalesSettlementRefSourceType.配件入库检验单:
                                             var partsInboundCheckBill = partsInboundCheckBills.SingleOrDefault(r => r.Id == partsSalesSettlementRef.SourceId && r.SettlementStatus == (int)DcsPartsSettlementStatus.待结算);
                                             if(partsInboundCheckBill == null)
                                                 throw new ValidationException(ErrorStrings.PartsSalesSettlement_Validation5);
                                             partsInboundCheckBill.SettlementStatus = (int)DcsPartsSettlementStatus.已结算;
                                             new PartsInboundCheckBillAch(this.DomainService).UpdatePartsInboundCheckBillValidate(partsInboundCheckBill);
                                             //查询入库清单
                                             var inDetails = ObjectContext.PartsInboundCheckBillDetails.Where(r => r.PartsInboundCheckBillId == partsInboundCheckBill.Id).ToArray();
                                             foreach(var item in inDetails) {
                                                 if(item.OriginalPrice > item.SettlementPrice) {
                                                     inRebateAmount += (item.OriginalPrice - item.SettlementPrice) * item.InspectedQuantity;
                                                 }
                                             }
                                             break;
                                     }
                                     var details = orderDetails.Where(r => r.BillCode == partsSalesSettlementRef.SourceCode);
                                     foreach(var detail in details) {
                                         newPartsSalesSettlement.PartsSalesSettlementDetails.Add(detail);
                                     }
                                 }
                             }
                             newPartsSalesSettlement.TotalSettlementAmount = newPartsSalesSettlement.PartsSalesSettlementDetails.Sum(t => t.SettlementAmount);
                             //折让不含税金额= （(（销售出库单清单.原始价格- 出库单清单.结算价）*数量)  - ((入库清单.零售价- 入库单清单.结算价) *数量））/（1+税率）
                             newPartsSalesSettlement.RebateAmount = (outRebateAmount - inRebateAmount) / Convert.ToDecimal((1 + newPartsSalesSettlement.TaxRate));
                             newPartsSalesSettlement.Tax = Math.Round((newPartsSalesSettlement.TotalSettlementAmount / (decimal)(1 + newPartsSalesSettlement.TaxRate)) * (decimal)newPartsSalesSettlement.TaxRate, 2);
                             InsertPartsSalesSettlementValidate(newPartsSalesSettlement);
                             InsertToDatabase(newPartsSalesSettlement);

                         }
                     }

                this.ObjectContext.SaveChanges();
                transaction.Complete();
            }
        }

        private PartsSalesSettlement setNewSettlement(PartsSalesSettlement oldpartsSalesSettlement) {
            var newSettlement = new PartsSalesSettlement() {
                SettlementPath=oldpartsSalesSettlement.SettlementPath,
                SalesCompanyId=oldpartsSalesSettlement.SalesCompanyId,
                SalesCompanyCode=oldpartsSalesSettlement.SalesCompanyCode,
                SalesCompanyName=oldpartsSalesSettlement.SalesCompanyName,
                WarehouseId=oldpartsSalesSettlement.WarehouseId,
                PartsSalesCategoryId=oldpartsSalesSettlement.PartsSalesCategoryId,
                PartsSalesCategoryName=oldpartsSalesSettlement.PartsSalesCategoryName,
                AccountGroupId=oldpartsSalesSettlement.AccountGroupId,
                AccountGroupCode=oldpartsSalesSettlement.AccountGroupCode,
                AccountGroupName=oldpartsSalesSettlement.AccountGroupName,
                CustomerAccountId=oldpartsSalesSettlement.CustomerAccountId,
                CustomerCompanyCode=oldpartsSalesSettlement.CustomerCompanyCode,
                CustomerCompanyId=oldpartsSalesSettlement.CustomerCompanyId,
                CustomerCompanyName=oldpartsSalesSettlement.CustomerCompanyName,
                InvoiceAmountDifference=oldpartsSalesSettlement.InvoiceAmountDifference,
                Tax=oldpartsSalesSettlement.Tax,
                TaxRate=oldpartsSalesSettlement.TaxRate,
                Status=oldpartsSalesSettlement.Status,
                Remark=oldpartsSalesSettlement.Remark,
                IsUnifiedSettle=oldpartsSalesSettlement.IsUnifiedSettle,
                InvoiceDate=oldpartsSalesSettlement.InvoiceDate,
                SettleType=oldpartsSalesSettlement.SettleType,
                BusinessType=oldpartsSalesSettlement.BusinessType,
                RebateMethod=oldpartsSalesSettlement.RebateMethod,
            };
            return newSettlement;
        }     
        public void 验证过账日期是否有效(DateTime? invoiceDate)
        {
            string year = invoiceDate.Value.Year.ToString();
            string month = invoiceDate.Value.Month.ToString();
            var accountPeriod = ObjectContext.AccountPeriods.Where(r => r.Month == month && r.Year == year).SingleOrDefault();
            if (null == accountPeriod)
            {
                throw new ValidationException(ErrorStrings.AccountPeriod_Validation2);
            }
            else if ((int)DcsAccountPeriodStatus.已关闭 == accountPeriod.Status)
            {
                throw new ValidationException(ErrorStrings.AccountPeriod_Validation3);
            }
        }

        public void 修改配件销售结算单(PartsSalesSettlement partsSalesSettlement) {
            if (null != partsSalesSettlement.InvoiceDate.Value)
            {
                //当过帐日期不为空时，验证过帐日期是否有效
                this.验证过账日期是否有效(partsSalesSettlement.InvoiceDate);
               
            }
            CheckEntityState(partsSalesSettlement);
            var dbpartsSalesSettlement = ObjectContext.PartsSalesSettlements.Where(r => r.Id == partsSalesSettlement.Id && r.Status == (int)DcsPartsSalesSettlementStatus.新建).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbpartsSalesSettlement == null)
                throw new ValidationException(ErrorStrings.PartsSalesSettlement_Validation6);
            if(partsSalesSettlement.RebateMethod == (int)DcsPartsSalesSettlementRebateMethod.返利池 && partsSalesSettlement.RebateAmount > 0) {
                var partsRebateAccounts = ObjectContext.PartsRebateAccounts.Where(r => r.AccountGroupId == partsSalesSettlement.AccountGroupId && r.CustomerCompanyId == partsSalesSettlement.CustomerCompanyId && r.Status == (int)DcsMasterDataStatus.有效).ToArray();
                if(partsRebateAccounts.Length > 1)
                    throw new ValidationException(ErrorStrings.PartsSalesSettlement_Validation1);
                if(partsRebateAccounts.Length == 0)
                    throw new ValidationException(ErrorStrings.PartsSalesSettlement_Validation2);
                var sumRebateAmount = ObjectContext.PartsSalesSettlements.Where(r => r.CustomerAccountId == partsSalesSettlement.CustomerAccountId && r.Status == (int)DcsPartsSalesSettlementStatus.新建 && r.Id != partsSalesSettlement.Id).ToArray().Sum(r => r.RebateAmount);
                if(partsSalesSettlement.RebateAmount > partsRebateAccounts.Single().AccountBalanceAmount - sumRebateAmount)
                    throw new ValidationException(ErrorStrings.PartsSalesSettlement_Validation3);
            }
            var partsSalesSettlementRefs = ChangeSet.GetAssociatedChanges(partsSalesSettlement, r => r.PartsSalesSettlementRefs).Cast<PartsSalesSettlementRef>().ToArray();
            var insertPartsSalesSettlementRefs = partsSalesSettlementRefs.Where(r => ChangeSet.GetChangeOperation(r) == ChangeOperation.Insert && partsSalesSettlementRefs.Any(v => ChangeSet.GetChangeOperation(v) == ChangeOperation.Delete && v.SourceId == r.SourceId && v.SourceType == r.SourceType)).ToArray();
            var deletePartsSalesSettlementRefs = partsSalesSettlementRefs.Where(r => ChangeSet.GetChangeOperation(r) == ChangeOperation.Delete && partsSalesSettlementRefs.Any(v => ChangeSet.GetChangeOperation(v) == ChangeOperation.Insert && v.SourceId == r.SourceId && v.SourceType == r.SourceType)).ToArray();
            var nonePartsSalesSettlementRefs = partsSalesSettlementRefs.Where(r => ChangeSet.GetChangeOperation(r) == ChangeOperation.None && partsSalesSettlementRefs.Any(v => ChangeSet.GetChangeOperation(v) == ChangeOperation.Insert && v.SourceId == r.SourceId && v.SourceType == r.SourceType)).ToArray();
            //待处理的关联单
            var pendingPartsSalesSettlementRefs = partsSalesSettlementRefs.Except(insertPartsSalesSettlementRefs).Except(deletePartsSalesSettlementRefs).Except(nonePartsSalesSettlementRefs).ToArray();
            var partsOutboundBillIds = pendingPartsSalesSettlementRefs.Where(r => r.SourceType == (int)DcsPartsSalesSettlementRefSourceType.配件出库单).Select(r => r.SourceId);
            var partsInboundCheckBillIds = pendingPartsSalesSettlementRefs.Where(r => r.SourceType == (int)DcsPartsSalesSettlementRefSourceType.配件入库检验单).Select(r => r.SourceId);
            var partsOutboundBills = ObjectContext.PartsOutboundBills.Where(r => partsOutboundBillIds.Contains(r.Id)).ToArray();
            var partsInboundCheckBills = ObjectContext.PartsInboundCheckBills.Where(r => partsInboundCheckBillIds.Contains(r.Id)).ToArray();
            var codes = pendingPartsSalesSettlementRefs.Where(r => r.InvoiceType == (int)DcsPartsSalesSettlementRefSourceType.配件出库单).Select(r => r.SourceCode).ToArray();
            var ecommerceFreightDisposals = this.ObjectContext.EcommerceFreightDisposals.Where(r => codes.Contains(r.PartsOutboundBillCode));
            /**
                * 销售结算单生成、修改、审核通过后，计算 折让不含税金额= （(（销售出库单清单.原始价格- 出库单清单.结算价）*数量)  - ((入库清单.零售价- 入库单清单.结算价) *数量））/（1+税率）
               **/
            decimal? outRebateAmount = 0;
            decimal? inRebateAmount = 0;
            foreach(var partsSalesSettlementRef in pendingPartsSalesSettlementRefs) {
                var ecommerceFreightDisposal = ecommerceFreightDisposals.SingleOrDefault(r => r.Code == partsSalesSettlementRef.SourceCode);
                switch(ChangeSet.GetChangeOperation(partsSalesSettlementRef)) {
                    case ChangeOperation.Insert:
                        switch(partsSalesSettlementRef.SourceType) {
                            case (int)DcsPartsSalesSettlementRefSourceType.配件出库单:
                                var pendingSettlementPartsOutboundBill = partsOutboundBills.SingleOrDefault(r => r.Id == partsSalesSettlementRef.SourceId && r.SettlementStatus == (int)DcsPartsSettlementStatus.待结算);
                                if(pendingSettlementPartsOutboundBill == null)
                                    throw new ValidationException(ErrorStrings.PartsSalesSettlement_Validation4);
                                pendingSettlementPartsOutboundBill.SettlementStatus = (int)DcsPartsSettlementStatus.已结算;
                                new PartsOutboundBillAch(this.DomainService).UpdatePartsOutboundBillValidate(pendingSettlementPartsOutboundBill);
                                if(ecommerceFreightDisposal != null) {
                                    ecommerceFreightDisposal.PartsSalesSettlementId = partsSalesSettlement.Id;
                                    ecommerceFreightDisposal.PartsSalesSettlementCode = partsSalesSettlement.Code;
                                    ecommerceFreightDisposal.SalesSettlementStatus = partsSalesSettlement.Status;
                                    new EcommerceFreightDisposalAch(this.DomainService).UpdateEcommerceFreightDisposal(ecommerceFreightDisposal);
                                }
                                //查询出库清单
                                var outDetails = ObjectContext.PartsOutboundBillDetails.Where(r => r.PartsOutboundBillId == pendingSettlementPartsOutboundBill.Id).ToArray();
                                foreach(var item in outDetails) {
                                    if(item.OriginalPrice > item.SettlementPrice) {
                                        outRebateAmount += (item.OriginalPrice - item.SettlementPrice) * item.OutboundAmount;
                                    }
                                }
                                break;
                            case (int)DcsPartsSalesSettlementRefSourceType.配件入库检验单:
                                var pendingSettlementPartsInboundCheckBill = partsInboundCheckBills.SingleOrDefault(r => r.Id == partsSalesSettlementRef.SourceId && r.SettlementStatus == (int)DcsPartsSettlementStatus.待结算);
                                if(pendingSettlementPartsInboundCheckBill == null)
                                    throw new ValidationException(ErrorStrings.PartsSalesSettlement_Validation5);
                                pendingSettlementPartsInboundCheckBill.SettlementStatus = (int)DcsPartsSettlementStatus.已结算;
                                new PartsInboundCheckBillAch(this.DomainService).UpdatePartsInboundCheckBillValidate(pendingSettlementPartsInboundCheckBill);
                                //查询入库清单
                                var inDetails = ObjectContext.PartsInboundCheckBillDetails.Where(r => r.PartsInboundCheckBillId == pendingSettlementPartsInboundCheckBill.Id).ToArray();
                                foreach(var item in inDetails) {
                                    if(item.OriginalPrice > item.SettlementPrice) {
                                        inRebateAmount += (item.OriginalPrice - item.SettlementPrice) * item.InspectedQuantity;
                                    }
                                }
                                break;
                        }
                        break;
                    case ChangeOperation.Delete:
                        switch(partsSalesSettlementRef.SourceType) {
                            case (int)DcsPartsSalesSettlementRefSourceType.配件出库单:
                                var partsOutboundBill = partsOutboundBills.SingleOrDefault(r => r.Id == partsSalesSettlementRef.SourceId);
                                if(partsOutboundBill == null)
                                    throw new ValidationException(string.Format(ErrorStrings.PartsSalesSettlement_Validation9, partsSalesSettlementRef.SourceCode));
                                partsOutboundBill.SettlementStatus = (int)DcsPartsSettlementStatus.待结算;
                                new PartsOutboundBillAch(this.DomainService).UpdatePartsOutboundBillValidate(partsOutboundBill);
                                if(ecommerceFreightDisposal != null) {
                                    ecommerceFreightDisposal.PartsSalesSettlementId = null;
                                    ecommerceFreightDisposal.PartsSalesSettlementCode = null;
                                    ecommerceFreightDisposal.SalesSettlementStatus = null;
                                    new EcommerceFreightDisposalAch(this.DomainService).UpdateEcommerceFreightDisposal(ecommerceFreightDisposal);
                                }
                                break;
                            case (int)DcsPartsSalesSettlementRefSourceType.配件入库检验单:
                                var partsInboundCheckBill = partsInboundCheckBills.SingleOrDefault(r => r.Id == partsSalesSettlementRef.SourceId);
                                if(partsInboundCheckBill == null)
                                    throw new ValidationException(string.Format(ErrorStrings.PartsSalesSettlement_Validation10, partsSalesSettlementRef.SourceCode));
                                partsInboundCheckBill.SettlementStatus = (int)DcsPartsSettlementStatus.待结算;
                                new PartsInboundCheckBillAch(this.DomainService).UpdatePartsInboundCheckBillValidate(partsInboundCheckBill);
                                break;
                        }
                        break;
                    case ChangeOperation.None:
                          switch(partsSalesSettlementRef.SourceType) {
                            case (int)DcsPartsSalesSettlementRefSourceType.配件出库单:
                                var pendingSettlementPartsOutboundBill = partsOutboundBills.SingleOrDefault(r => r.Id == partsSalesSettlementRef.SourceId && r.SettlementStatus == (int)DcsPartsSettlementStatus.已结算);
                                if(pendingSettlementPartsOutboundBill == null)
                                    throw new ValidationException(ErrorStrings.PartsSalesSettlement_Validation4);                         
                                //查询出库清单
                                var outDetails = ObjectContext.PartsOutboundBillDetails.Where(r => r.PartsOutboundBillId == pendingSettlementPartsOutboundBill.Id).ToArray();
                                foreach(var item in outDetails) {
                                    outRebateAmount += (item.OriginalPrice - item.SettlementPrice) * item.OutboundAmount;
                                }
                                break;
                            case (int)DcsPartsSalesSettlementRefSourceType.配件入库检验单:
                                var pendingSettlementPartsInboundCheckBill = partsInboundCheckBills.SingleOrDefault(r => r.Id == partsSalesSettlementRef.SourceId && r.SettlementStatus == (int)DcsPartsSettlementStatus.已结算);
                                if(pendingSettlementPartsInboundCheckBill == null)
                                    throw new ValidationException(ErrorStrings.PartsSalesSettlement_Validation5);                             
                                //查询入库清单
                                var inDetails = ObjectContext.PartsInboundCheckBillDetails.Where(r => r.PartsInboundCheckBillId == pendingSettlementPartsInboundCheckBill.Id).ToArray();
                                foreach(var item in inDetails) {
                                    inRebateAmount += (item.OriginalPrice - item.SettlementPrice) * item.InspectedQuantity;
                                }
                                break;
                        }
                        break;
                }
            }
            //折让不含税金额= （(（销售出库单清单.原始价格- 出库单清单.结算价）*数量)  - ((入库清单.零售价- 入库单清单.结算价) *数量））/（1+税率）
            partsSalesSettlement.RebateAmount = (outRebateAmount - inRebateAmount) / Convert.ToDecimal((1 + partsSalesSettlement.TaxRate));
        }


        public void 审批配件销售结算单(PartsSalesSettlement partsSalesSettlement) {
            var dbpartsSalesSettlement = ObjectContext.PartsSalesSettlements.Where(r => r.Id == partsSalesSettlement.Id && r.Status == (int)DcsPartsSalesSettlementStatus.新建).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbpartsSalesSettlement == null)
                throw new ValidationException(ErrorStrings.PartsSalesSettlement_Validation11);
            if (null != dbpartsSalesSettlement.InvoiceDate.Value)
            { 
                //当过帐日期不为空时，验证过帐日期是否有效
                this.验证过账日期是否有效(dbpartsSalesSettlement.InvoiceDate);
            }

            var userInfo = Utils.GetCurrentUserInfo();
            if(partsSalesSettlement.RebateMethod == (int)DcsPartsSalesSettlementRebateMethod.返利池) {
                var partsRebateAccounts = ObjectContext.PartsRebateAccounts.Where(r => r.AccountGroupId == partsSalesSettlement.AccountGroupId && r.CustomerCompanyId == partsSalesSettlement.CustomerCompanyId && r.Status == (int)DcsMasterDataStatus.有效).ToArray();
                if(partsRebateAccounts.Length > 1)
                    throw new ValidationException(ErrorStrings.PartsSalesSettlement_Validation1);
                if(partsRebateAccounts.Length == 0)
                    throw new ValidationException(ErrorStrings.PartsSalesSettlement_Validation2);
                if(partsSalesSettlement.SettlementPath == (int)DcsPartsPurchaseSettleBillSettlementPath.正常结算) {
                    partsRebateAccounts.Single().AccountBalanceAmount -= partsSalesSettlement.RebateAmount.HasValue ? partsSalesSettlement.RebateAmount.Value : 0;
                } else {
                    partsRebateAccounts.Single().AccountBalanceAmount += partsSalesSettlement.RebateAmount.HasValue ? partsSalesSettlement.RebateAmount.Value : 0;
                }

            }
            if(partsSalesSettlement.TotalSettlementAmount == 0) {
                partsSalesSettlement.Status = (int)DcsPartsSalesSettlementStatus.发票登记;
                partsSalesSettlement.InvoiceRegistrationTime = DateTime.Now;
                partsSalesSettlement.InvoiceRegistrationOperatorId = userInfo.Id;
                partsSalesSettlement.InvoiceRegistrationOperator = userInfo.Name;
            } else {
                new CustomerAccountAch(this.DomainService).销售结算过账(partsSalesSettlement);
                partsSalesSettlement.Status = (int)DcsPartsSalesSettlementStatus.已审批;               
            }
            partsSalesSettlement.ApproverId = userInfo.Id;
            partsSalesSettlement.ApproverName = userInfo.Name;
            partsSalesSettlement.ApproveTime = DateTime.Now;
            this.UpdatePartsSalesSettlementValidate(partsSalesSettlement);
        }


        public void 审批虚拟配件销售结算单(PartsSalesSettlementEx partsSalesSettlementEx) {
            var partsSalesSettlement = ObjectContext.PartsSalesSettlements.Where(r => r.Id == partsSalesSettlementEx.Id).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(partsSalesSettlement == null)
                throw new ValidationException(ErrorStrings.PartsSalesRtnSettlement_Validation14);
            var dbpartsSalesSettlement = ObjectContext.PartsSalesSettlements.Where(r => r.Id == partsSalesSettlement.Id && r.Status == (int)DcsPartsSalesSettlementStatus.新建).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbpartsSalesSettlement == null)
                throw new ValidationException(ErrorStrings.PartsSalesSettlement_Validation11);
            var detailcheck = ObjectContext.PartsSalesSettlementDetails.Any(r => r.PartsSalesSettlementId == partsSalesSettlementEx.Id && r.QuantityToSettle < 0);
            if(detailcheck) {
                throw new ValidationException(ErrorStrings.PartsSalesSettlement_Validation18);
            }
            if (null != partsSalesSettlement.InvoiceDate.Value)
            {
                //当过帐日期不为空时，验证过帐日期是否有效
                this.验证过账日期是否有效(partsSalesSettlement.InvoiceDate);
            }
            var userInfo = Utils.GetCurrentUserInfo();
            var nowTime = DateTime.Now;
            if(partsSalesSettlement.RebateMethod == (int)DcsPartsSalesSettlementRebateMethod.返利池) {
                var partsRebateAccounts = ObjectContext.PartsRebateAccounts.Where(r => r.AccountGroupId == partsSalesSettlement.AccountGroupId && r.CustomerCompanyId == partsSalesSettlement.CustomerCompanyId && r.Status == (int)DcsMasterDataStatus.有效).ToArray();
                if(partsRebateAccounts.Length > 1)
                    throw new ValidationException(ErrorStrings.PartsSalesSettlement_Validation1);
                if(partsRebateAccounts.Length == 0)
                    throw new ValidationException(ErrorStrings.PartsSalesSettlement_Validation2);
                if(partsSalesSettlement.SettlementPath == (int)DcsPartsPurchaseSettleBillSettlementPath.正常结算) {
                    partsRebateAccounts.Single().AccountBalanceAmount -= partsSalesSettlement.RebateAmount.HasValue ? partsSalesSettlement.RebateAmount.Value : 0;
                } else {
                    partsRebateAccounts.Single().AccountBalanceAmount += partsSalesSettlement.RebateAmount.HasValue ? partsSalesSettlement.RebateAmount.Value : 0;
                }

            }
            if(partsSalesSettlement.TotalSettlementAmount == 0) {
                partsSalesSettlement.Status = (int)DcsPartsSalesSettlementStatus.发票登记;
                partsSalesSettlement.InvoiceRegistrationTime = nowTime;
                partsSalesSettlement.InvoiceRegistrationOperatorId = userInfo.Id;
                partsSalesSettlement.InvoiceRegistrationOperator = userInfo.Name;
            } else {
                partsSalesSettlement.Status = (int)DcsPartsSalesSettlementStatus.已审批;
            }

            new CustomerAccountAch(this.DomainService).销售结算过账(partsSalesSettlement);


            UpdateToDatabase(partsSalesSettlement);
            partsSalesSettlement.ApproverId = userInfo.Id;
            partsSalesSettlement.ApproverName = userInfo.Name;
            partsSalesSettlement.ApproveTime = nowTime;
            this.UpdatePartsSalesSettlementValidate(partsSalesSettlement);
        }


        public void 反结算配件销售结算单(PartsSalesSettlementEx partsSalesSettlementEx) {
            var partsSalesSettlement = ObjectContext.PartsSalesSettlements.Where(r => r.Id == partsSalesSettlementEx.Id).SetMergeOption(MergeOption.OverwriteChanges).SingleOrDefault();
            if(partsSalesSettlement == null)
                throw new ValidationException(ErrorStrings.PartsSalesRtnSettlement_Validation14);
            var dbpartsSalesSettlement = ObjectContext.PartsSalesSettlements.Where(r => r.Id == partsSalesSettlement.Id && (r.Status == (int)DcsPartsSalesSettlementStatus.发票登记 || r.Status == (int)DcsPartsSalesSettlementStatus.已审批)).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbpartsSalesSettlement == null)
                throw new ValidationException(ErrorStrings.PartsSalesSettlement_Validation12);
            if(partsSalesSettlement.RebateMethod == (int)DcsPartsSalesSettlementRebateMethod.返利池) {
                var partsRebateAccounts = ObjectContext.PartsRebateAccounts.Where(r => r.AccountGroupId == partsSalesSettlement.AccountGroupId && r.CustomerCompanyId == partsSalesSettlement.CustomerCompanyId && r.Status == (int)DcsMasterDataStatus.有效).ToArray();
                if(partsRebateAccounts.Length > 1)
                    throw new ValidationException(ErrorStrings.PartsSalesSettlement_Validation1);
                if(partsRebateAccounts.Length == 0)
                    throw new ValidationException(ErrorStrings.PartsSalesSettlement_Validation2);
                partsRebateAccounts.Single().AccountBalanceAmount += partsSalesSettlement.RebateAmount.HasValue ? partsSalesSettlement.RebateAmount.Value : 0;
            }
            var partsSalesSettlementRefs = partsSalesSettlement.PartsSalesSettlementRefs;
            var partsOutboundBillIds = partsSalesSettlementRefs.Where(r => r.SourceType == (int)DcsPartsSalesSettlementRefSourceType.配件出库单).Select(r => r.SourceId);
            var partsInboundCheckBillIds = partsSalesSettlementRefs.Where(r => r.SourceType == (int)DcsPartsSalesSettlementRefSourceType.配件入库检验单).Select(r => r.SourceId);
            var partsOutboundBills = ObjectContext.PartsOutboundBills.Where(r => partsOutboundBillIds.Contains(r.Id)).ToArray();
            var partsInboundCheckBills = ObjectContext.PartsInboundCheckBills.Where(r => partsInboundCheckBillIds.Contains(r.Id)).ToArray();
            foreach(var partsSalesSettlementRef in partsSalesSettlementRefs) {
                switch(partsSalesSettlementRef.SourceType) {
                    case (int)DcsPartsSalesSettlementRefSourceType.配件出库单:
                        var partsOutboundBill = partsOutboundBills.SingleOrDefault(r => r.Id == partsSalesSettlementRef.SourceId);
                        if(partsOutboundBill == null)
                            throw new ValidationException(string.Format(ErrorStrings.PartsPurchaseSettleBill_Validation8, partsSalesSettlementRef.SourceCode));
                        partsOutboundBill.SettlementStatus = (int)DcsPartsSettlementStatus.待结算;
                        new PartsOutboundBillAch(this.DomainService).UpdatePartsOutboundBillValidate(partsOutboundBill);
                        break;
                    case (int)DcsPartsSalesSettlementRefSourceType.配件入库检验单:
                        var partsInboundCheckBill = partsInboundCheckBills.SingleOrDefault(r => r.Id == partsSalesSettlementRef.SourceId);
                        if(partsInboundCheckBill == null)
                            throw new ValidationException(string.Format(ErrorStrings.PartsPurchaseSettleBill_Validation9, partsSalesSettlementRef.SourceCode));
                        partsInboundCheckBill.SettlementStatus = (int)DcsPartsSettlementStatus.待结算;
                        new PartsInboundCheckBillAch(this.DomainService).UpdatePartsInboundCheckBillValidate(partsInboundCheckBill);
                        break;
                }
            }
            var offsettedPartsSalesSettlement = new PartsSalesSettlement {
                SettlementPath = (int)DcsPartsPurchaseSettleBillSettlementPath.反冲结算,
                SalesCompanyId = partsSalesSettlement.SalesCompanyId,
                SalesCompanyCode = partsSalesSettlement.SalesCompanyCode,
                SalesCompanyName = partsSalesSettlement.SalesCompanyName,
                AccountGroupId = partsSalesSettlement.AccountGroupId,
                AccountGroupCode = partsSalesSettlement.AccountGroupCode,
                PartsSalesCategoryId = partsSalesSettlement.PartsSalesCategoryId,
                PartsSalesCategoryName = partsSalesSettlement.PartsSalesCategoryName,
                AccountGroupName = partsSalesSettlement.AccountGroupName,
                CustomerCompanyId = partsSalesSettlement.CustomerCompanyId,
                CustomerCompanyCode = partsSalesSettlement.CustomerCompanyCode,
                CustomerCompanyName = partsSalesSettlement.CustomerCompanyName,
                CustomerAccountId = partsSalesSettlement.CustomerAccountId,
                TotalSettlementAmount = -partsSalesSettlement.TotalSettlementAmount,
                RebateMethod = partsSalesSettlement.RebateMethod,
                RebateAmount = partsSalesSettlement.RebateAmount,
                InvoiceAmountDifference = -partsSalesSettlement.InvoiceAmountDifference,
                OffsettedSettlementBillId = partsSalesSettlement.Id,
                OffsettedSettlementBillCode = partsSalesSettlement.Code,
                TaxRate = partsSalesSettlement.TaxRate,
                Tax = partsSalesSettlement.Tax,
                Status = (int)DcsPartsSalesSettlementStatus.已审批,
            };
            InsertToDatabase(offsettedPartsSalesSettlement);
            this.InsertPartsSalesSettlementValidate(offsettedPartsSalesSettlement);
            var sumSettlementAmount = partsSalesSettlement.PartsSalesSettlementRefs.Sum(r => r.SettlementAmount);
            new CustomerAccountAch(this.DomainService).销售结算过账(offsettedPartsSalesSettlement);
            if(partsSalesSettlement.Status == (int)DcsPartsSalesSettlementStatus.发票登记) {
                var invoiceInformations = ObjectContext.InvoiceInformations.Where(r => r.SourceId == partsSalesSettlement.Id && r.SourceType == (int)DcsInvoiceInformationSourceType.配件销售结算单 && r.Status != (int)DcsInvoiceInformationStatus.作废);
                foreach(var invoiceInformation in invoiceInformations) {
                    invoiceInformation.Status = (int)DcsInvoiceInformationStatus.作废;
                    new InvoiceInformationAch(this.DomainService).UpdateInvoiceInformationValidate(invoiceInformation);
                }
            }
            UpdateToDatabase(partsSalesSettlement);
            partsSalesSettlement.Status = (int)DcsPartsSalesSettlementStatus.已反冲;
            this.UpdatePartsSalesSettlementValidate(partsSalesSettlement);
        }


        public void 作废配件销售结算单(PartsSalesSettlementEx partsSalesSettlementEx) {
            var partsSalesSettlement = ObjectContext.PartsSalesSettlements.Where(r => r.Id == partsSalesSettlementEx.Id).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(partsSalesSettlement == null)
                throw new ValidationException(ErrorStrings.PartsSalesRtnSettlement_Validation14);
            var dbpartsSalesSettlement = ObjectContext.PartsSalesSettlements.Where(r => r.Id == partsSalesSettlement.Id && r.Status == (int)DcsPartsSalesSettlementStatus.新建).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbpartsSalesSettlement == null)
                throw new ValidationException(ErrorStrings.PartsSalesSettlement_Validation13);
            var partsSalesSettlementRefs = partsSalesSettlement.PartsSalesSettlementRefs;
            var partsOutboundBillIds = partsSalesSettlementRefs.Where(r => r.SourceType == (int)DcsPartsSalesSettlementRefSourceType.配件出库单).Select(r => r.SourceId);
            var partsInboundCheckBillIds = partsSalesSettlementRefs.Where(r => r.SourceType == (int)DcsPartsSalesSettlementRefSourceType.配件入库检验单).Select(r => r.SourceId);
            var partsOutboundBills = ObjectContext.PartsOutboundBills.Where(r => partsOutboundBillIds.Contains(r.Id)).ToArray();
            var partsInboundCheckBills = ObjectContext.PartsInboundCheckBills.Where(r => partsInboundCheckBillIds.Contains(r.Id)).ToArray();
            foreach(var partsSalesSettlementRef in partsSalesSettlementRefs) {
                switch(partsSalesSettlementRef.SourceType) {
                    case (int)DcsPartsSalesSettlementRefSourceType.配件出库单:
                        var partsOutboundBill = partsOutboundBills.SingleOrDefault(r => r.Id == partsSalesSettlementRef.SourceId);
                        if(partsOutboundBill == null)
                            throw new ValidationException(string.Format(ErrorStrings.PartsSalesSettlement_Validation9, partsSalesSettlementRef.SourceCode));
                        partsOutboundBill.SettlementStatus = (int)DcsPartsSettlementStatus.待结算;
                        new PartsOutboundBillAch(this.DomainService).UpdatePartsOutboundBillValidate(partsOutboundBill);
                        break;
                    case (int)DcsPartsSalesSettlementRefSourceType.配件入库检验单:
                        var partsInboundCheckBill = partsInboundCheckBills.SingleOrDefault(r => r.Id == partsSalesSettlementRef.SourceId);
                        if(partsInboundCheckBill == null)
                            throw new ValidationException(string.Format(ErrorStrings.PartsSalesSettlement_Validation10, partsSalesSettlementRef.SourceCode));
                        partsInboundCheckBill.SettlementStatus = (int)DcsPartsSettlementStatus.待结算;
                        new PartsInboundCheckBillAch(this.DomainService).UpdatePartsInboundCheckBillValidate(partsInboundCheckBill);
                        break;
                }
            }
            UpdateToDatabase(partsSalesSettlement);
            partsSalesSettlement.Status = (int)DcsPartsSalesSettlementStatus.作废;
            var userInfo = Utils.GetCurrentUserInfo();
            partsSalesSettlement.AbandonerId = userInfo.Id;
            partsSalesSettlement.AbandonerName = userInfo.Name;
            partsSalesSettlement.AbandonTime = DateTime.Now;
            this.UpdatePartsSalesSettlementValidate(partsSalesSettlement);

            //var codes = partsSalesSettlementRefs.Where(r => r.InvoiceType == (int)DcsPartsSalesSettlementRefSourceType.配件出库单).Select(r => r.SourceCode).ToArray();
            //var ecommerceFreightDisposals = this.ObjectContext.EcommerceFreightDisposals.Where(r => codes.Contains(r.PartsOutboundBillCode));
            //foreach(var ecommerceFreightDisposal in ecommerceFreightDisposals) {
            //    ecommerceFreightDisposal.PartsSalesSettlementId = null;
            //    ecommerceFreightDisposal.PartsSalesSettlementCode = null;
            //    ecommerceFreightDisposal.SalesSettlementStatus = null;
            //    new EcommerceFreightDisposalAch(this.DomainService).UpdateEcommerceFreightDisposal(ecommerceFreightDisposal);
            //}
        }


        public void 登记发票(PartsSalesSettlement partsSalesSettlement, decimal invoiceAmount) {
            if(invoiceAmount < 0)
                throw new ValidationException(ErrorStrings.PartsSalesSettlement_Validation14);
            var dbpartsSalesSettlement = ObjectContext.PartsSalesSettlements.Where(r => r.Id == partsSalesSettlement.Id && r.Status == (int)DcsPartsSalesSettlementStatus.已审批).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbpartsSalesSettlement == null)
                throw new ValidationException(ErrorStrings.PartsSalesSettlement_Validation15);
            UpdateToDatabase(partsSalesSettlement);
            partsSalesSettlement.Status = (int)DcsPartsSalesSettlementStatus.发票登记;
            partsSalesSettlement.InvoiceAmountDifference = partsSalesSettlement.TotalSettlementAmount - invoiceAmount;
            var userInfo = Utils.GetCurrentUserInfo();
            partsSalesSettlement.InvoiceRegistrationOperatorId = userInfo.Id;
            partsSalesSettlement.InvoiceRegistrationOperator = userInfo.Name;
            partsSalesSettlement.InvoiceRegistrationTime = DateTime.Now;
            this.UpdatePartsSalesSettlementValidate(partsSalesSettlement);
        }

    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:11:58
        //原分布类的函数全部转移到Ach                                                               

        [Update(UsingCustomMethod = true)]
        public void 生成配件销售结算单(PartsSalesSettlement partsSalesSettlement) {
            new PartsSalesSettlementAch(this).生成配件销售结算单(partsSalesSettlement);
        }


        [Update(UsingCustomMethod = true)]
        public void 修改配件销售结算单(PartsSalesSettlement partsSalesSettlement) {
            new PartsSalesSettlementAch(this).修改配件销售结算单(partsSalesSettlement);
        }


        [Update(UsingCustomMethod = true)]
        public void 审批配件销售结算单(PartsSalesSettlement partsSalesSettlement) {
            new PartsSalesSettlementAch(this).审批配件销售结算单(partsSalesSettlement);
        }


        [Update(UsingCustomMethod = true)]
        public void 审批虚拟配件销售结算单(PartsSalesSettlementEx partsSalesSettlementEx) {
            new PartsSalesSettlementAch(this).审批虚拟配件销售结算单(partsSalesSettlementEx);
        }


        [Update(UsingCustomMethod = true)]
        public void 反结算配件销售结算单(PartsSalesSettlementEx partsSalesSettlementEx) {
            new PartsSalesSettlementAch(this).反结算配件销售结算单(partsSalesSettlementEx);
        }


        [Update(UsingCustomMethod = true)]
        public void 作废配件销售结算单(PartsSalesSettlementEx partsSalesSettlementEx) {
            new PartsSalesSettlementAch(this).作废配件销售结算单(partsSalesSettlementEx);
        }


        [Update(UsingCustomMethod = true)]
        public void 登记发票(PartsSalesSettlement partsSalesSettlement, decimal invoiceAmount) {
            new PartsSalesSettlementAch(this).登记发票(partsSalesSettlement, invoiceAmount);
        }
         [Invoke]
        public void 生成配件销售结算单2(PartsSalesSettlement partsSalesSettlement, List<PartsSalesSettlementRef> partsSalesSettlementRefs, List<PartsSalesSettlementDetail> partsSalesSettlementDetails) {
            new PartsSalesSettlementAch(this).生成配件销售结算单2(partsSalesSettlement, partsSalesSettlementRefs, partsSalesSettlementDetails);
        }
    }
}
