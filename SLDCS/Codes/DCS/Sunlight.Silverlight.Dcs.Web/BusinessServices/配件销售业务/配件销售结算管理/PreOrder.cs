﻿using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class PreOrderAch : DcsSerivceAchieveBase {
        public PreOrderAch(DcsDomainService domainService)
            : base(domainService) {
        }


        public void 作废预订单(int[] PreOrderids) {
            var DBPreOrders = ObjectContext.PreOrders.Where(r => PreOrderids.Contains(r.Id));
            foreach(var o in DBPreOrders) {
                if(o.Status == (int)DcsPreOrderStatus.新建) {
                    o.Status = (int)DcsPreOrderStatus.作废;
                    UpdateToDatabase(o);
                }else {
                    throw new ValidationException("只允许作废新建状态的预订单");
                }
            }
            ObjectContext.SaveChanges();
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:11:58
        //原分布类的函数全部转移到Ach                                                               

        [Invoke]
        public void 作废预订单(int[] PreOrderids) {
            new PreOrderAch(this).作废预订单(PreOrderids);
        }
    }
}
