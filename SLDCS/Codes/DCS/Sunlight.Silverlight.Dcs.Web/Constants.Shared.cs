﻿using System;

namespace Sunlight.Silverlight.Dcs.Web {

    public static class GlobalVar {
        //最大分类层次
        public const int PRODUCTCATEGORY_MAX_LEVEL = 4;

        //该值由服务端填充
        public const string ASSIGNED_BY_SERVER = "<尚未生成>";

        public const int ASSIGNED_BY_SERVER_INT = Int32.MinValue;

        //虚拟维修项目编号
        public const string DEFAULT_WARRANTYPOLICY_CODE = "9999999999";

        //人员默认头像的 URL
        public const string DEFAULT_AVATAR_URL = "Client/Images/DefaultAvatar.jpg";
        //服务端文件下载 URL 前缀
        public const string DOWNLOAD_FILE_URL_PREFIX = "FileDownloadHandler.ashx?Path=";
        //服务端上传文件处理路径
        public const string UPLOAD_FILE_URL_PERFIX = "FileUploadHandler.ashx";
        //产品图片存放目录
        public const string UPLOAD_PRODUCT_DIR = "Products";
        //导入文件存放目录
        public const string UPLOAD_IMPORTFILE_DIR = "ImportedFiles";
        //导出文件存放目录T
        public const string DOWNLOAD_EXPORTFILE_DIR = "ExportedFiles";

        /// <summary>
        /// 虚拟维修项目编号
        /// </summary>
        public const string VIRTUAL_REPAIRITEM_CODE = "9999999999";

        public const string VEHICLEINFORMATION_SALESDATE_NAME = "销售日期";

        public const string VEHICLEINFORMATION_PRODUCTCODE_NAME = "产品编号";

        public const string VEHICLEINFORMATION_OUTOFFACTORYDATE_NAME = "出厂日期";

        public const int UNREAL_WAREHOUSEAREA_ID = 0;

        public const string UNREAL_WAREHOUSEAREA_CODE = "0";

        public const int RETAIL_CUSTOMER_ID = 100000;
        public const string RETAIL_CUSTOMER_CODE = "RETAIL100000";
        public const string RETAIL_CUSTOMER_NAME = "零售客户";
        public const string RETAIL_PARTSSALESCATEGORY_NAME = "配件零售";

        public const string VIRTUAL_WAREHOUSE_NAME_PREFIX = "虚拟库";
        public const string CULTURE_INFO_HEAD_LOCAL_NAME = "_CultureInfo";
        public const string CULTYRE_INFO_HEADER_NAMESPACE = "http://sunlight.bz/excelservice/";
        //整车
        public const int DEFAULT_DEALERVEHICLEMONTHQUOTA_VEHICLESALESORGID = 1;
        public const string DEFAULT_DEALERVEHICLEMONTHQUOTA_VEHICLESALESORGCODE = "CAM";
        public const string DEFAULT_DEALERVEHICLEMONTHQUOTA_VEHICLESALESORGNAME = "CAM";

        public const int DEFAULT_OEMVEHICLEMONTHQUOTA_VEHICLESALESORGID = 1;
        public const string DEFAULT_OEMVEHICLEMONTHQUOTA_VEHICLESALESORGCODE = "CAM";
        public const string DEFAULT_OEMVEHICLEMONTHQUOTA_VEHICLESALESORGNAME = "CAM";

        public const int DEFAULT_VEHICLEINFORMATION_BRANCHID = 1;
        public const string DEFAULT_VEHICLEINFORMATION_BRANCHCODE = "CAM";
        public const string DEFAULT_VEHICLEINFORMATION_BRANCHNAME = "CAM";

        public const int DEFAULT_VEHICLEAVAILABLERESOURCE_BRANCHID = 1;
        public const string DEFAULT_VEHICLEAVAILABLERESOURCE_BRANCHCODE = "CAM";
        public const string DEFAULT_VEHICLEAVAILABLERESOURCE_BRANCHNAME = "CAM";

        public const int DEFAULT_VEHICLESHIPPINGORDERCHANGEREC_VEHICLESALESORGID = 1;
        public const string DEFAULT_VEHICLESHIPPINGORDERCHANGEREC_VEHICLESALESORGCODE = "CAM";
        public const string DEFAULT_VEHICLESHIPPINGORDERCHANGEREC_VEHICLESALESORGNAME = "CAM";

        public const int DEFAULT_VEHICLESHIPPINGORDER_VEHICLESALESORGID = 1;
        public const string DEFAULT_VEHICLESHIPPINGORDER_VEHICLESALESORGCODE = "CAM";
        public const string DEFAULT_VEHICLESHIPPINGORDER_VEHICLESALESORGNAME = "CAM";

        public const int DEFAULT_VEHICLECUSTOMERACCOUNTVEHICLE_ACCOUNTGROUPID = 1;

        public const int DEFAULT_VEHICLEORDERPLANSUMMARY_BRANCHID = 1;
        public const string DEFAULT_VEHICLEORDERPLANSUMMARY_BRANCHCODE = "CAM";
        public const string DEFAULT_VEHICLEORDERPLANSUMMARY_BRANCHNAME = "CAM";

        public const int DEFAULT_VEHICLECATEGORY_ID = 1;
        public const string DEFAULT_VEHICLECATEGORY_CODE = "CAM";
        public const string DEFAULT_VEHICLECATEGORY_NAME = "CAM";

        public const string VEHICLEWAREHOUSE_NAME_FINISHED_WHOLESALE_CENTER = "成品批发中心";
    }
}