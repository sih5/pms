﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Devart.Data.Oracle;
using Sunlight.Silverlight.Dcs.Web.Dashboard.Models.Charts;

namespace Sunlight.Silverlight.Dcs.Web.Dashboard {
    public partial class Dashboard {
        #region 整车
        public List<VehicleModelCount> GetVehicleStocks() {
            const string sql = "SELECT VehicleModelCode, VehicleModelName, COUNT(*) AS CNT FROM Stock WHERE InventoryStatus <> 2 GROUP BY VehicleModelCode, VehicleModelName";
            var result = new List<VehicleModelCount>(100);
            using(var conn = new SqlConnection(DcsConnStr)) {
                var cmd = new SqlCommand(sql, conn);
                conn.Open();
                using(var reader = cmd.ExecuteReader()) {
                    while(reader.Read())
                        result.Add(new VehicleModelCount {
                            VehicleModelCode = Convert.ToString(reader["VehicleModelCode"]),
                            VehicleModelName = Convert.ToString(reader["VehicleModelName"]),
                            Count = Convert.ToInt32(reader["CNT"])
                        });
                }
            }
            return result;
        }

        public List<VehicleModelCount> GetDeliveryVehiclesByDay() {
            const string sql = @"SELECT VehicleModelCode, VehicleModelName, COUNT(*) AS CNT FROM DeliveryOrderDetail
INNER JOIN DeliveryOrder ON DeliveryOrderDetail.DeliveryOrderId = DeliveryOrder.Id
WHERE DATEDIFF(DAY, DeliveryOrder.CreateTime, GETDATE()) = 0
GROUP BY VehicleModelCode, VehicleModelName";
            var result = new List<VehicleModelCount>(100);
            using(var conn = new SqlConnection(DcsConnStr)) {
                var cmd = new SqlCommand(sql, conn);
                conn.Open();
                using(var reader = cmd.ExecuteReader()) {
                    while(reader.Read())
                        result.Add(new VehicleModelCount {
                            VehicleModelCode = Convert.ToString(reader["VehicleModelCode"]),
                            VehicleModelName = Convert.ToString(reader["VehicleModelName"]),
                            Count = Convert.ToInt32(reader["CNT"])
                        });
                }
            }
            return result;
        }

        public List<VehicleModelCount> GetDeliveryVehiclesByMonth() {
            const string sql = @"SELECT VehicleModelCode, VehicleModelName, COUNT(*) AS CNT FROM DeliveryOrderDetail
INNER JOIN DeliveryOrder ON DeliveryOrderDetail.DeliveryOrderId = DeliveryOrder.Id
WHERE DATEDIFF(MONTH, DeliveryOrder.CreateTime, GETDATE()) = 0
GROUP BY VehicleModelCode, VehicleModelName";
            var result = new List<VehicleModelCount>(100);
            using(var conn = new SqlConnection(DcsConnStr)) {
                var cmd = new SqlCommand(sql, conn);
                conn.Open();
                using(var reader = cmd.ExecuteReader()) {
                    while(reader.Read())
                        result.Add(new VehicleModelCount {
                            VehicleModelCode = Convert.ToString(reader["VehicleModelCode"]),
                            VehicleModelName = Convert.ToString(reader["VehicleModelName"]),
                            Count = Convert.ToInt32(reader["CNT"])
                        });
                }
            }
            return result;
        }
        #endregion

        #region 配件
        public Dictionary<string, decimal> GetSparePartStocks() {
            const string sql = @"SELECT SUM(StockCost) AS StockCostSum, ISNULL(ABCCategory, '其他')+ '(' + CONVERT(VARCHAR,SUM(StockCost)) + ')' AS ABCCategory FROM PartsStockCost
INNER JOIN SparePart ON SparePart.Id = PartsStockCost.SparePartId
GROUP BY SparePart.ABCCategory;";
            var result = new Dictionary<string, decimal>(10);
            using(var conn = new SqlConnection(DcsConnStr)) {
                var cmd = new SqlCommand(sql, conn);
                conn.Open();
                using(var reader = cmd.ExecuteReader()) {
                    while(reader.Read())
                        result.Add(Convert.ToString(reader["ABCCategory"]), Convert.ToDecimal(reader["StockCostSum"]));
                }
            }
            return result;
        }
        #endregion

        #region 服务
        public decimal GetRepairContractBookingPercent() {
            const string sql = @"
SELECT CAST(COUNT(CASE WHEN RepairBookingOrderId IS NOT NULL THEN 1 ELSE NULL END) * 1. / CASE COUNT(*) WHEN 0 THEN 1 ELSE COUNT(*) END * 1. AS Numeric(9,4)) AS Percentage
FROM RepairContract WHERE Status != 0";
            decimal result;
            using(var conn = new SqlConnection(DcsConnStr)) {
                var cmd = new SqlCommand(sql, conn);
                conn.Open();
                using(var reader = cmd.ExecuteReader()) {
                    reader.Read();
                    result = Convert.ToDecimal(reader["Percentage"]);
                }
            }
            return result;
        }

        public List<decimal> GetRepairContractDeliverPrecent() {
            const string deliverSql = @"
SELECT COUNT(CASE WHEN ActualDeliveryTime <= ExpectedFinishTime THEN 1 ELSE NULL END) AS DeliverCount, COUNT(*) AS TotalCount
FROM RepairContract
WHERE DATEDIFF(MONTH, ActualDeliveryTime, GETDATE()) = 0";
            const string repairSql = @"
SELECT COUNT(*) AS RepairCount
FROM RepairContract
WHERE DATEDIFF(MONTH, ActualDeliveryTime, GETDATE()) = 0 AND EXISTS(
	SELECT Id FROM RepairContractItem
	WHERE RepairContractItem.RepairContractId = RepairContract.Id AND RepairContractItem.AccountingProperty != 5)";
            decimal deliverCount;
            decimal totalCount;
            decimal repairCount;
            using(var conn = new SqlConnection(DcsConnStr)) {
                var deliverCmd = new SqlCommand(deliverSql, conn);
                conn.Open();
                using(var reader = deliverCmd.ExecuteReader()) {
                    reader.Read();
                    deliverCount = Convert.ToInt32(reader["DeliverCount"]);
                    totalCount = Convert.ToInt32(reader["TotalCount"]);
                }

                var repairCmd = new SqlCommand(repairSql, conn);
                using(var reader = repairCmd.ExecuteReader()) {
                    reader.Read();
                    repairCount = Convert.ToInt32(reader["RepairCount"]);
                }
            }
            var result = new List<decimal>();
            result.Add(Math.Round(deliverCount / Math.Max(totalCount, 1), 4));
            result.Add(Math.Round(repairCount / Math.Max(totalCount, 1), 4));
            return result;
        }

        public Dictionary<string, int> GetRepairProportionsByAdvisor() {
            const string sql = @"
SELECT * FROM (
	SELECT ServiceAdvisorName + '(' + CONVERT(VARCHAR, COUNT(*)) + ')' AS Advisor, COUNT(*) AS CNT FROM RepairContract
	WHERE Status <> 0
        AND DATEDIFF(MONTH, CreateTime, GETDATE()) = 0
	GROUP BY ServiceAdvisorId, ServiceAdvisorName) AS TMP
WHERE CNT > 0";
            var result = new Dictionary<string, int>(50);
            using(var conn = new SqlConnection(DcsConnStr)) {
                var cmd = new SqlCommand(sql, conn);
                conn.Open();
                using(var reader = cmd.ExecuteReader()) {
                    while(reader.Read())
                        result.Add(Convert.ToString(reader["Advisor"]), Convert.ToInt32(reader["CNT"]));
                }
            }
            return result;
        }
        #endregion

        #region CRM
        public Dictionary<string, int> GetCustomerProportionsByLevel() {
            const string sql = @"
SELECT COUNT(*) AS CNT, KeyValueItem.Value + '(' + CONVERT(VARCHAR, COUNT(*)) + ')'  AS LevelOfIntent
FROM PotentialCustomer
LEFT OUTER JOIN Customer ON PotentialCustomer.CustomerId = Customer.Id
LEFT OUTER JOIN KeyValueItem ON PotentialCustomer.LevelOfIntent = KeyValueItem.[Key]
	AND KeyValueItem.[Status] = 2 AND KeyValueItem.Category = 'DMS' AND KeyValueItem.Name = 'Condition_LevelOfIntent'
WHERE Customer.[Status] IN (1, 2)
GROUP BY PotentialCustomer.LevelOfIntent, KeyValueItem.Value";
            var result = new Dictionary<string, int>(50);
            using(var conn = new SqlConnection(DcsConnStr)) {
                var cmd = new SqlCommand(sql, conn);
                conn.Open();
                using(var reader = cmd.ExecuteReader()) {
                    while(reader.Read())
                        result.Add(Convert.ToString(reader["LevelOfIntent"]), Convert.ToInt32(reader["CNT"]));
                }
            }
            return result;
        }

        public Dictionary<DateTime, int> GetHallVisitRegistrationsByDay() {
            const string sql = @"
SELECT CONVERT(VARCHAR(10), BeginTime, 111) AS BeginDate, COUNT(*) AS CNT
FROM HallVisitRegistration
WHERE Status <> 0 AND DATEDIFF(MONTH, BeginTime, GETDATE()) = 0
GROUP BY CONVERT(VARCHAR(10), BeginTime, 111)";
            var result = new Dictionary<DateTime, int>(31);
            using(var conn = new SqlConnection(DcsConnStr)) {
                var cmd = new SqlCommand(sql, conn);
                conn.Open();
                using(var reader = cmd.ExecuteReader()) {
                    while(reader.Read())
                        result.Add(Convert.ToDateTime(reader["BeginDate"]), Convert.ToInt32(reader["CNT"]));
                }
            }
            return result;
        }

        public Dictionary<DateTime, int> GetCallRegistrationsByDay() {
            const string sql = @"
SELECT CONVERT(VARCHAR(10), BeginTime, 111) AS BeginDate, COUNT(*) AS CNT
FROM CallRegistration
WHERE Status <> 0 AND DATEDIFF(MONTH, BeginTime, GETDATE()) = 0
GROUP BY CONVERT(VARCHAR(10), BeginTime, 111)";
            var result = new Dictionary<DateTime, int>(31);
            using(var conn = new SqlConnection(DcsConnStr)) {
                var cmd = new SqlCommand(sql, conn);
                conn.Open();
                using(var reader = cmd.ExecuteReader()) {
                    while(reader.Read())
                        result.Add(Convert.ToDateTime(reader["BeginDate"]), Convert.ToInt32(reader["CNT"]));
                }
            }
            return result;
        }

        public Dictionary<DateTime, int> GetTestDriveRecordsByDay() {
            const string sql = @"
SELECT CONVERT(VARCHAR(10), TestDriveDate, 111) AS TestDriveDate, COUNT(*) AS CNT
FROM TestDriveRecord
WHERE Status <> 0 AND DATEDIFF(MONTH, TestDriveDate, GETDATE()) = 0
GROUP BY CONVERT(VARCHAR(10), TestDriveDate, 111)";
            var result = new Dictionary<DateTime, int>(31);
            using(var conn = new SqlConnection(DcsConnStr)) {
                var cmd = new SqlCommand(sql, conn);
                conn.Open();
                using(var reader = cmd.ExecuteReader()) {
                    while(reader.Read())
                        result.Add(Convert.ToDateTime(reader["TestDriveDate"]), Convert.ToInt32(reader["CNT"]));
                }
            }
            return result;
        }
        #endregion

        #region 测试Demo
        public Dictionary<string, int> GetPartsStocksByWarehouse() {
            const string sql = @"
SELECT * FROM (
	SELECT b.name || '(' || COUNT(*)|| ')' AS Name, COUNT(*) AS CNT from PartsStock a 
join Warehouse b on a.warehouseid =b.id where a.StorageCompanyType =1 group by b.name) TMP
WHERE CNT > 0";
            var result = new Dictionary<string, int>(50);
            using(var conn = new OracleConnection(DcsConnStr)) {
                var cmd = new OracleCommand(sql, conn);
                conn.Open();
                using(var reader = cmd.ExecuteReader()) {
                    while(reader.Read())
                        result.Add(Convert.ToString(reader["Name"]), Convert.ToInt32(reader["CNT"]));
                }
            }
            return result;
        }
        #endregion
    }
}
