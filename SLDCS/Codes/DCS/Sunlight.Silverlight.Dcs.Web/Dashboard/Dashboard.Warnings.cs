﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using Devart.Data.Oracle;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web.Dashboard {
    public partial class Dashboard {

        private readonly Dictionary<int, string> warningItemsSql = new Dictionary<int, string> {
            {11101, @"select Count(*) from notificationlimit   inner join notification on  notification.id= notificationlimit.noticeid inner join Company on Company.Type = notificationlimit.descompanytpye "},

            #region 待办事项
            // 采购价申请待初审
            {11144, @"SELECT COUNT(*) FROM partspurchasepricingchange where status=4 "},
            // 采购价申请待审核
            {11105, @"SELECT COUNT(*) FROM partspurchasepricingchange where status=3 "},
            // 采购价申请待审批
            {11106, @"SELECT COUNT(*) FROM partspurchasepricingchange where status=5 "},
            // 销售价申请待初审
            {11145, @"SELECT COUNT(*) FROM PartsSalesPriceChange where status=4 "},
            // 销售价申请待审核
            {11107, @"SELECT COUNT(*) FROM PartsSalesPriceChange where status=2 "},
            // 销售价申请待审批
            {11108, @"SELECT COUNT(*) FROM PartsSalesPriceChange where status=5 "},
            // 包装设置待审核
            {11109, @"SELECT COUNT(*) FROM PartsPackingPropertyApp where status=4 "},
            // 特殊协议价待初审
            {11146, @"SELECT COUNT(*) FROM SpecialTreatyPriceChange where status=5 "},
            // 特殊协议价待审核
            {11110, @"SELECT COUNT(*) FROM SpecialTreatyPriceChange where status=3 "},
            // 特殊协议价待审批
            {11111, @"SELECT COUNT(*) FROM SpecialTreatyPriceChange where status=4 "},
            // 采购计划待初审
            {11143, @"SELECT COUNT(*) FROM PartsPurchasePlan where status=2 "},
            // 采购计划待审核
            {11112, @"SELECT COUNT(*) FROM PartsPurchasePlan where status=3 "},
            // 采购计划待审批 
            {11113, @"SELECT COUNT(*) FROM PartsPurchasePlan where status=5 "},           
            // 信用申请待审核
            {11114, @"SELECT COUNT(*) FROM CredenceApplication where status=5 and CreditType=1"},
            // 信用申请待审批
            {11115, @"SELECT COUNT(*) FROM CredenceApplication where status=7 and CreditType=1"},

            // 信用申请待初审 临时授信
            {11149, @"SELECT COUNT(*) FROM CredenceApplication where status=6  and CreditType=2 "},
             // 信用申请待审核
            {11148, @"SELECT COUNT(*) FROM CredenceApplication where status=5 and CreditType=2"},
            // 信用申请待审批
            {11150, @"SELECT COUNT(*) FROM CredenceApplication where status=7 and CreditType=2"},
             // 信用申请待高级审核
            {11151, @"SELECT COUNT(*) FROM CredenceApplication where status=2 and CreditType=2"},
            // 信用申请待高级审批
            {11152, @"SELECT COUNT(*) FROM CredenceApplication where status=8 and CreditType=2"},

            // 采购退货待审核
            {11116, @"SELECT COUNT(*) FROM PartsPurReturnOrder where status=1 "},
            // 采购退货待审批
            {11117, @"SELECT COUNT(*) FROM PartsPurReturnOrder where status=3 "},
            // 销售退货待审核
            {11118, @"SELECT COUNT(*) FROM PartsSalesReturnBill where status=5 "},
            // 销售退货待审批
            {11119, @"SELECT COUNT(*) FROM PartsSalesReturnBill where status=3 "},
             // 配件外采待确认
            {16407, @"SELECT COUNT(*) FROM PartsOuterPurchaseChange popc where popc.status=2 and (exists(select 1 from  DealerServiceInfo e
                INNER JOIN MarketDptPersonnelRelation f ON e.MarketingDepartmentId = f.MarketDepartmentId and f.personnelid = "+ Utils.GetCurrentUserInfo().Id +" where f.status<>99 and popc.CustomerCompanyId = e.DealerId AND popc.PartsSalesCategoryrId = e.PartsSalesCategoryId) or not exists (select id from MarketDptPersonnelRelation where personnelid = "+ Utils.GetCurrentUserInfo().Id +" and status <> 99))"},
            // 配件外采待审核
            {11120, @"SELECT COUNT(*) FROM PartsOuterPurchaseChange popc where popc.status=4 and (exists(select 1 from  DealerServiceInfo e
                INNER JOIN MarketDptPersonnelRelation f ON e.MarketingDepartmentId = f.MarketDepartmentId and f.personnelid = "+ Utils.GetCurrentUserInfo().Id +" where f.status<>99 and popc.CustomerCompanyId = e.DealerId AND popc.PartsSalesCategoryrId = e.PartsSalesCategoryId) or not exists (select id from MarketDptPersonnelRelation where personnelid = "+ Utils.GetCurrentUserInfo().Id +" and status <> 99))"},
            // 配件外采待审批
            {11121, @"SELECT COUNT(*) FROM PartsOuterPurchaseChange popc where popc.status=5 and (exists(select 1 from  DealerServiceInfo e
                INNER JOIN MarketDptPersonnelRelation f ON e.MarketingDepartmentId = f.MarketDepartmentId and f.personnelid = "+ Utils.GetCurrentUserInfo().Id +" where f.status<>99 and popc.CustomerCompanyId = e.DealerId AND popc.PartsSalesCategoryrId = e.PartsSalesCategoryId) or not exists (select id from MarketDptPersonnelRelation where personnelid = "+ Utils.GetCurrentUserInfo().Id +" and status <> 99))"},                                                                                                             
            // 配件外采待高级审核
            {16408, @"SELECT COUNT(*) FROM PartsOuterPurchaseChange popc where popc.status=7 and (exists(select 1 from  DealerServiceInfo e
                INNER JOIN MarketDptPersonnelRelation f ON e.MarketingDepartmentId = f.MarketDepartmentId and f.personnelid = "+ Utils.GetCurrentUserInfo().Id +" where f.status<>99 and popc.CustomerCompanyId = e.DealerId AND popc.PartsSalesCategoryrId = e.PartsSalesCategoryId) or not exists (select id from MarketDptPersonnelRelation where personnelid = "+ Utils.GetCurrentUserInfo().Id +" and status <> 99))"},
            // 配件外采待高级审批
            {16409, @"SELECT COUNT(*) FROM PartsOuterPurchaseChange popc where popc.status=8 and (exists(select 1 from  DealerServiceInfo e
                INNER JOIN MarketDptPersonnelRelation f ON e.MarketingDepartmentId = f.MarketDepartmentId and f.personnelid = "+ Utils.GetCurrentUserInfo().Id +" where f.status<>99 and popc.CustomerCompanyId = e.DealerId AND popc.PartsSalesCategoryrId = e.PartsSalesCategoryId) or not exists (select id from MarketDptPersonnelRelation where personnelid = "+ Utils.GetCurrentUserInfo().Id +" and status <> 99))"},
            

            // 中心库盘点待审核
            {11122, @"SELECT COUNT(*) FROM PartsInventoryBill where status=5 and storagecompanytype!=1 "},
            // 中心库盘点待审批
            {11123, @"SELECT COUNT(*) FROM PartsInventoryBill where status=4 and storagecompanytype!=1 "},
            // 服务站盘点待审核
            {11124, @"SELECT COUNT(*) FROM DealerPartsInventoryBill dpib where dpib.status=3 and (exists(select 1 from  DealerServiceInfo e
                INNER JOIN MarketDptPersonnelRelation f ON e.MarketingDepartmentId = f.MarketDepartmentId and f.personnelid = "+ Utils.GetCurrentUserInfo().Id +" where f.status<>99 and dpib.StorageCompanyId = e.DealerId AND dpib.SalesCategoryId = e.PartsSalesCategoryId) or not exists (select id from MarketDptPersonnelRelation where personnelid = "+ Utils.GetCurrentUserInfo().Id +" and status <> 99))"},
            // 服务站盘点待审批
            {11125, @"SELECT COUNT(*) FROM DealerPartsInventoryBill dpib where dpib.status=2 and (exists(select 1 from  DealerServiceInfo e
                INNER JOIN MarketDptPersonnelRelation f ON e.MarketingDepartmentId = f.MarketDepartmentId and f.personnelid = "+ Utils.GetCurrentUserInfo().Id +" where f.status<>99 and dpib.StorageCompanyId = e.DealerId AND dpib.SalesCategoryId = e.PartsSalesCategoryId) or not exists (select id from MarketDptPersonnelRelation where personnelid = "+ Utils.GetCurrentUserInfo().Id +" and status <> 99))"},
            // 服务站盘点待高级审核
            {11147, @"SELECT COUNT(*) FROM DealerPartsInventoryBill dpib where dpib.status=4 and (exists(select 1 from  DealerServiceInfo e
                INNER JOIN MarketDptPersonnelRelation f ON e.MarketingDepartmentId = f.MarketDepartmentId and f.personnelid = "+ Utils.GetCurrentUserInfo().Id +" where f.status<>99 and dpib.StorageCompanyId = e.DealerId AND dpib.SalesCategoryId = e.PartsSalesCategoryId) or not exists (select id from MarketDptPersonnelRelation where personnelid = "+ Utils.GetCurrentUserInfo().Id +" and status <> 99))"},
             //服务站盘点待高级审批                                                                                                            
            {16404, @"SELECT COUNT(*) FROM DealerPartsInventoryBill dpib where dpib.status=6 and (exists(select 1 from  DealerServiceInfo e
                INNER JOIN MarketDptPersonnelRelation f ON e.MarketingDepartmentId = f.MarketDepartmentId and f.personnelid = "+ Utils.GetCurrentUserInfo().Id +" where f.status<>99 and dpib.StorageCompanyId = e.DealerId AND dpib.SalesCategoryId = e.PartsSalesCategoryId) or not exists (select id from MarketDptPersonnelRelation where personnelid = "+ Utils.GetCurrentUserInfo().Id +" and status <> 99))"},
            // SIH盘点待初审
            {11153, @"SELECT COUNT(*) FROM PartsInventoryBill where status=2  and StorageCompanyType=1"},
            // SIH盘点待审核
            {11126, @"SELECT COUNT(*) FROM PartsInventoryBill where status=5 and StorageCompanyType=1 "},
            // SIH盘点待审批
            {11127, @"SELECT COUNT(*) FROM PartsInventoryBill where status=4  and StorageCompanyType=1 "},
            // 三包索赔待审核
            {11128, @"SELECT COUNT(*) FROM PartsClaimOrderNew where status=13 "},
            // 三包索赔待审批
            {11129, @"SELECT COUNT(*) FROM PartsClaimOrderNew where status=14 "},
            // 调拨待审核
            {11130, @"SELECT COUNT(*) FROM PartsTransferOrder where status=1 "},
            // 调拨待审批
            {11131, @"SELECT COUNT(*) FROM PartsTransferOrder where status=3 "},
            // 直供发运单待完成
            {11132, @"SELECT COUNT(*) FROM SupplierShippingOrder where status=2 and IfDirectProvision=1 and nvl(DirectProvisionFinished,0)=0 "},       
            // 借用待审核
            {11133, @"SELECT COUNT(*) FROM BorrowBill where status=2 "},
            // 借用待审批
            {11134, @"SELECT COUNT(*) FROM BorrowBill where status=3 "},

            // 内部领出待审核
            {11135, @"SELECT COUNT(*) FROM InternalAllocationBill where status=1 "},
            // 内部领出待审批
            {11136, @"SELECT COUNT(*) FROM InternalAllocationBill where status=3 "},
             // 借用待归还
            {11137, @"SELECT COUNT(*) FROM BorrowBill where status in(5,7) "},
            // 借用归还待确认
            {11138, @"SELECT COUNT(*) FROM BorrowBill where status=6 "},
             // 服务站盘点待初审
            {11139, @"SELECT COUNT(*) FROM DealerPartsInventoryBill dpib where status=1 and (exists(select 1 from  DealerServiceInfo e
                INNER JOIN MarketDptPersonnelRelation f ON e.MarketingDepartmentId = f.MarketDepartmentId and f.personnelid = "+ Utils.GetCurrentUserInfo().Id +" where f.status<>99 and dpib.StorageCompanyId = e.DealerId AND dpib.SalesCategoryId = e.PartsSalesCategoryId) or not exists (select id from MarketDptPersonnelRelation where personnelid = "+ Utils.GetCurrentUserInfo().Id +" and status <> 99))"},
             // 中心库盘点待初审
            {11140, @"SELECT COUNT(*) FROM PartsInventoryBill where status=2 and storagecompanytype!=1 "},
              // 中心库盘点待高级审核
            {16406, @"SELECT COUNT(*) FROM PartsInventoryBill where status=6 and storagecompanytype!=1 "},
             // 配件外采待初审
            {11141, @"SELECT COUNT(*) FROM PartsOuterPurchaseChange popc where status=6 and (exists(select 1 from  DealerServiceInfo e
                INNER JOIN MarketDptPersonnelRelation f ON e.MarketingDepartmentId = f.MarketDepartmentId and f.personnelid = "+ Utils.GetCurrentUserInfo().Id +" where f.status<>99 and popc.CustomerCompanyId = e.DealerId AND popc.PartsSalesCategoryrId = e.PartsSalesCategoryId) or not exists (select id from MarketDptPersonnelRelation where personnelid = "+ Utils.GetCurrentUserInfo().Id +" and status <> 99))"},
            // 采购价格超期提醒
            {11142, @"select sum(tt.Warning) from (select count(1) as Warning from partspurchasepricing  a
                  inner  join  partssupplierrelation  b on  a.partid  =  b.partid and  a.partssupplierid  =  b.supplierid
                  join  sparepart  sp on  a.partid  =  sp.id
                  join  PartsSupplier  pr on  a.partssupplierid  =  pr.id
                  where trunc(a.validfrom)  <=  trunc(sysdate) and  trunc(a.validto)  >=  trunc(sysdate) 
                  and (trunc(a.validto)  =  trunc(sysdate)  +  15  or
                  trunc(a.validto)  =  trunc(sysdate)  +  7  or
                  trunc(a.validto)  =  trunc(sysdate)  +  3  or
                  trunc(a.validto)  =  trunc(sysdate)  +  1)
                  and  not  exists (select  1 from  partspurchasepricing  tmp
                                  where  trunc(tmp.validfrom)  =  trunc(a.validto)  +  1 and  trunc(tmp.validfrom)  >  trunc(sysdate))
                  union  all
                  select  count(1)  as  Warning  from  partspurchasepricing  a
                  inner  join  partssupplierrelation  b  on  a.partid  =  b.partid and  a.partssupplierid  =  b.supplierid
                  join  sparepart  sp on  a.partid  =  sp.id
                  join  PartsSupplier  pr on  a.partssupplierid  =  pr.id
                  where  trunc(a.validfrom)  >  trunc(sysdate)
                      and  trunc(a.validto)  >  trunc(sysdate)
                      and  (trunc(a.validto)  =  trunc(sysdate)  +  15  or
                            trunc(a.validto)  =  trunc(sysdate)  +  7  or
                            trunc(a.validto)  =  trunc(sysdate)  +  3  or
                            trunc(a.validto)  =  trunc(sysdate)  +  1))  tt"},
               //中心库强制储备待审核
                {11154, @"SELECT COUNT(*) FROM ForceReserveBill where status=2 and companytype=3 "},
               //中心库强制储备待审批
                {11155, @"SELECT COUNT(*) FROM ForceReserveBill where status=3 and companytype=3"},
                //服务商强制储备待审核 中心库
                {11156, @"SELECT COUNT(*) FROM ForceReserveBill fr "},
                //服务商强制储备待审核 配件中心
                {11157, @"SELECT COUNT(*) FROM ForceReserveBill fr "},                
                 //服务商强制储备待确认
                {11158, @"SELECT COUNT(*) FROM ForceReserveBill fr "},               
                 //服务商强制储备待审批
                {11159, @"SELECT COUNT(*) FROM ForceReserveBill fr "},
                  //服务商强制储备待审批(配件公司)
                {11160, @"SELECT COUNT(*) FROM ForceReserveBill fr  where fr.status=3  and fr.ReserveType='T'"},
                //待审核供应商零件永久性标识条码修正
                {11161, @"SELECT COUNT(*) FROM SupplierTraceCode fr  where fr.status=2  "},
                 //待审批供应商零件永久性标识条码修正
                {11162, @"SELECT COUNT(*) FROM SupplierTraceCode fr  where fr.status=3  "},
                //SIH授信（服务商）待初审
                {11163, @"SELECT COUNT(*) FROM SIHCreditInfo fr  where fr.status=2 and fr.type=1  "},
                //SIH授信（服务商）待审核
                {11164, @"SELECT COUNT(*) FROM SIHCreditInfo fr  where fr.status=3  and fr.type=1 "},
                //SIH授信（服务商）待审批
                {11165, @"SELECT COUNT(*) FROM SIHCreditInfo fr  where fr.status=4  and fr.type=1 "},
                //SIH授信（服务商）待高级审核
                {11166, @"SELECT COUNT(*) FROM SIHCreditInfo fr  where fr.status=5 and fr.type=1 "},
                 //SIH授信（服务商）-分销中心  待审核
                {11167, @"SELECT COUNT(*) FROM SIHCreditInfo fr  where fr.status=2  and fr.type=2"},
                 //SIH授信（服务商）-分销中心  待审批
                {11168, @"SELECT COUNT(*) FROM SIHCreditInfo fr  where fr.status=4 and fr.type=2 "},
                 //供应商到货确认，待审核
                {11169, @"SELECT COUNT(*) FROM SupplierShippingOrder fr  where fr.ModifyStatus=1 "},
                 //临时订单待复核待办事项--待复核
                {11176, @"SELECT COUNT(*) FROM TemPurchasePlanOrder fr  where fr.Status=3 "},
                 //临时订单待复核待办事项--待确认
                {11177, @"SELECT COUNT(*) FROM TemPurchasePlanOrder fr  where fr.Status=4 "},
                //临时订单待提交
                {11178, @"SELECT COUNT(*) FROM TemPurchaseOrder fr where fr.Status =1  and (not exists (select 1 from PersonnelSupplierRelation ps   where ps.personid = " + Utils.GetCurrentUserInfo().Id + @"
                       and ps.status = 1) or exists  (select 1   from PersonnelSupplierRelation ps   where ps.personid = " + Utils.GetCurrentUserInfo().Id + @" and ps.status = 1  and ps.supplierid = fr.suplierid))"},
                 //临时订单审核待提交
                {11179, @"SELECT COUNT(*) FROM TemPurchaseOrder fr where fr.ApproveStatus =1 
                     and  (not exists (select 1 from PersonnelSupplierRelation ps   where ps.personid = " + Utils.GetCurrentUserInfo().Id + @"
                       and ps.status = 1) or exists  (select 1   from PersonnelSupplierRelation ps   where ps.personid = " + Utils.GetCurrentUserInfo().Id + @" and ps.status = 1  and ps.supplierid = fr.suplierid))"},
                //临时订单待审核
                {11180, @"SELECT COUNT(*) FROM TemPurchaseOrder fr where fr.ApproveStatus =2  and (not exists (select 1 from PersonnelSupplierRelation ps   where ps.personid = " + Utils.GetCurrentUserInfo().Id + @"
                       and ps.status = 1) or exists  (select 1   from PersonnelSupplierRelation ps   where ps.personid = " + Utils.GetCurrentUserInfo().Id + @" and ps.status = 1  and ps.supplierid = fr.suplierid))"},
                //临时订单待审批                                                                                                      
                {11181, @"SELECT COUNT(*) FROM TemPurchaseOrder fr where fr.ApproveStatus =3 and (not exists (select 1 from PersonnelSupplierRelation ps   where ps.personid = " + Utils.GetCurrentUserInfo().Id + @"
                       and ps.status = 1) or exists  (select 1   from PersonnelSupplierRelation ps   where ps.personid = " + Utils.GetCurrentUserInfo().Id + @" and ps.status = 1  and ps.supplierid = fr.suplierid))"},
                //临时订单未完成                                                                                               
                {11182, @"SELECT COUNT(*) FROM TemPurchaseOrder fr where fr.PartsSalesOrderCode is null and (not exists (select 1 from PersonnelSupplierRelation ps   where ps.personid = " + Utils.GetCurrentUserInfo().Id + @"
                       and ps.status = 1) or exists  (select 1   from PersonnelSupplierRelation ps   where ps.personid = " + Utils.GetCurrentUserInfo().Id + @" and ps.status = 1  and ps.supplierid = fr.suplierid))"},
               //	临时订单无采购价格
               {11183,@"select count(1)
                      from TemPurchaseOrder tho
                      join TemPurchaseOrderDetail tod
                        on tho.id = tod.TemPurchaseOrderId
                      left join PartsPurchasePricing ppp
                        on tho.suplierid = ppp.partssupplierid
                       and tod.sparepartid = ppp.partid
                       and ppp.status = 2
                       and trunc(ppp.validfrom) <= trunc(sysdate)
                       and trunc(ppp.validto) >= trunc(sysdate)
                      left join partssalesprice psp
                        on tod.sparepartid = psp.sparepartid
                       and psp.status = 1
                     where tho.status <> 7
                       and (not exists
                            (select 1
                               from PartsSupplierRelation pr
                              where pr.supplierid = tho.suplierid
                                and tod.sparepartid = pr.partid
                                and pr.status = 1) or ppp.id is null or psp.id is null)
                       and ppp.id is null "},
               //	临时订单无销售价格
               {11184,@"select count(1)
                      from TemPurchaseOrder tho
                      join TemPurchaseOrderDetail tod
                        on tho.id = tod.TemPurchaseOrderId
                      left join PartsPurchasePricing ppp
                        on tho.suplierid = ppp.partssupplierid
                       and tod.sparepartid = ppp.partid
                       and ppp.status = 2
                       and trunc(ppp.validfrom) <= trunc(sysdate)
                       and trunc(ppp.validto) >= trunc(sysdate)
                      left join partssalesprice psp
                        on tod.sparepartid = psp.sparepartid
                       and psp.status = 1
                     where tho.status <> 7
                       and (not exists
                            (select 1
                               from PartsSupplierRelation pr
                              where pr.supplierid = tho.suplierid
                                and tod.sparepartid = pr.partid
                                and pr.status = 1) or ppp.id is null or psp.id is null)
                       and psp.id is null "},
                 //【上架回退待初审】，当【上架回退】单据状态为=提交时显示。
                {11170, @"select count(*) from PartsDifferenceBackBill where Status = 2  and Type = 1"},
                 //【包装回退待初审】，当【包装回退】单据状态=提交时显示。
                {11171, @"  select count(*) from PartsDifferenceBackBill where Status = 2  and Type = 2"},
                //【包装回退待审核】，当【包装回退】单据状态=初审通过时显示。
                {11172, @" select count(*) from PartsDifferenceBackBill where Status = 5  and Type = 2 "},
                 //【入库回退待初审】，当【入库回退】单据状态=提交时显示。
                {11173, @" select count(*) from PartsDifferenceBackBill where Status = 2  and Type = 3 "},
                 //【入库回退待审核】，当【入库回退】单据状态=初审通过时显示。
                {11174, @" select count(*) from PartsDifferenceBackBill where Status = 5  and Type = 3  "},
                 //【入库回退待审批】，当【入库回退】单据状态=审核通过时显示。
                {11175, @"  select count(*) from PartsDifferenceBackBill where Status = 3  and Type = 3 "},
                //	移库待审核
                {11185, @"  select count(*) from partsshiftorder where Status = 4  and  StorageCompanyId="+ Utils.GetCurrentUserInfo().EnterpriseId},
                 //	移库待审批
                {11186, @"  select count(*) from partsshiftorder where Status = 3  and  StorageCompanyId="+ Utils.GetCurrentUserInfo().EnterpriseId},
                 //	中心库收货差异处理 待判定
                {11195, @"  select count(*) from AgencyDifferenceBackBill where Status = 2   " },
                //	中心库收货差异处理 待确认
                {11196, @"  select count(*) from AgencyDifferenceBackBill where Status = 3   " },
                 //	中心库收货差异处理 待处理
                {11197, @"  select count(*) from AgencyDifferenceBackBill ad where ad.Status = 4   " },
                 //	中心库收货差异处理 待审核
                {11198, @"  select count(*) from AgencyDifferenceBackBill where Status = 5   " },
                 // 中心库收货差异处理 待审批
                {11199, @"  select count(*) from AgencyDifferenceBackBill where Status = 6   " },
                 //	跨区域销售备案待初审
                {11190, @"  select count(*) from CrossSalesOrder where Status = 2  "},
                 //跨区域销售备案待审核
                {11191, @"  select count(*) from CrossSalesOrder where Status = 4  "},
                //跨区域销售备案待审批
                {11192, @"  select count(*) from CrossSalesOrder where Status = 5  "},
                //跨区域销售备案待高级审核
                {11193, @"  select count(*) from CrossSalesOrder where Status = 6  "},
                //跨区域销售备案待完善资料
                {11194, @"  select count(*) from CrossSalesOrder where Status = 7  "},
            #endregion
        };

        public Dictionary<int, string> GetWarnings(int[] ids) {
            var result = new Dictionary<int, string>(ids.Count());
            var validWarningIds = warningItemsSql.Keys.Intersect(ids).ToList();
            if(validWarningIds.Any())
                using(var conn = new OracleConnection(DcsConnStr)) {
                    conn.Open();
                    foreach(var warningItemSql in warningItemsSql.Where(kv => validWarningIds.Contains(kv.Key))) {
                        string Sql = warningItemSql.Value;
                        //当为公告时，特殊处理
                        if(warningItemSql.Key == 11101)
                            GetNotificationSQL(ref Sql, conn);
                        if(warningItemSql.Key == 11114 || warningItemSql.Key == 11115)
                            Sql += " and SalesCompanyId=" + CurrentUser.EnterpriseId;
                        if(warningItemSql.Key == 11118 || warningItemSql.Key == 11119)
                            Sql += " and SalesUnitOwnerCompanyId=" + CurrentUser.EnterpriseId;
                        if(warningItemSql.Key == 11122 || warningItemSql.Key == 11123)
                            Sql += " and StorageCompanyType in(" + (int)DcsCompanyType.代理库 + "," + (int)DcsCompanyType.服务站兼代理库 + ")";
                        if(warningItemSql.Key == 11126 || warningItemSql.Key == 11127)
                            Sql += " and StorageCompanyType =" + (int)DcsCompanyType.分公司;
                        if(warningItemSql.Key == 11130 || warningItemSql.Key == 11131)
                            Sql += " and StorageCompanyId=" + CurrentUser.EnterpriseId;

                        if(warningItemSql.Key == 11159) {
                            Sql += " join AgencyDealerRelation ar on fr.companyid= ar.DealerId and ar.status=1 where fr.status=2   and  fr.ReserveType!='T' and  fr.companyType =" + (int)DcsCompanyType.服务站 + " and ar.AgencyId=" + CurrentUser.EnterpriseId;
                        }
                        if(warningItemSql.Key == 11156) {
                            Sql += " where fr.companyType=" + (int)DcsCompanyType.服务站 + " and fr.ReserveType='T' and fr.status=2";
                        }
                        if(warningItemSql.Key == 11157) {
                            Sql += " where fr.status=3 and fr.ReserveType!='T' and fr.companyId=" + CurrentUser.EnterpriseId;
                        }
                        if(warningItemSql.Key == 11158) {
                            Sql += " join AgencyDealerRelation ar on fr.companyid= ar.DealerId and ar.status=1 join Agency ag on ar.AgencyId =ag.id join MarketDptPersonnelRelation md on ag.MarketingDepartmentId=md.MarketDepartmentId and md.status=1 where fr.status=5  and fr.ReserveType!='T' and  md.PersonnelId=" + CurrentUser.EnterpriseId;
                        }

                        if(warningItemSql.Key == 11167) {
                            Sql = @" select sum(countS)
                                  from (select count(1) countS
                                          from SIHCreditInfo so
                                         where so.status = 2
                                           and so.type = 2
                                           and exists (select 1
                                                  from SIHCreditInfoDetail sd
                                                  join MarketDptPersonnelRelation mk
                                                    on sd.marketingdepartmentid = mk.MarketDepartmentId
                                                   and mk.status = 1
                                                 where sd.sihcreditinfoid = so.id and mk.personnelid= " + CurrentUser.Id + @")
                                        union
                                        select (1) countS
                                          from SIHCreditInfo so
                                         where so.status = 2
                                           and so.type = 2
                                           and not exists (select 1
                                                  from MarketDptPersonnelRelation mk
                                                 where mk.status = 1 and mk.personnelid= " + CurrentUser.Id + @" ))";
                        }
                        if(warningItemSql.Key == 11168) {
                            Sql = @" select sum(countS)
                                  from (select count(1) countS
                                          from SIHCreditInfo so
                                         where so.status = 4
                                           and so.type = 2
                                           and exists (select 1
                                                  from SIHCreditInfoDetail sd
                                                  join MarketDptPersonnelRelation mk
                                                    on sd.marketingdepartmentid = mk.MarketDepartmentId
                                                   and mk.status = 1
                                                 where sd.sihcreditinfoid = so.id and mk.personnelid= " + CurrentUser.Id + @")
                                        union
                                        select (1) countS
                                          from SIHCreditInfo so
                                         where so.status = 4
                                           and so.type = 2
                                           and not exists (select 1
                                                  from MarketDptPersonnelRelation mk
                                                 where mk.status = 1 and mk.personnelid= " + CurrentUser.Id + @" ))";
                        }
                        if(warningItemSql.Key == 16407)//配件外采待确认
                        {
                            if(getCompanyType(conn) == (int)DcsCompanyType.代理库) {
                                Sql = @" SELECT COUNT(*) FROM PartsOuterPurchaseChange popc where popc.status=2 and   popc.CustomerCompanyId in(  select DealerId from agencydealerrelation where AgencyCode='" + Utils.GetCurrentUserInfo().EnterpriseCode + "')";
                            }

                        }
                        if(warningItemSql.Key == 11197) {
                            Sql += @"  
                               and (not exists (select 1
                                              from ResponsibleDetail re
                                              join ResponsibleMembers rt
                                                on re.responmemberid = rt.id
                                               and rt.status = 1
                                             where re.personelid = " + CurrentUser.Id + @"
                                        ) or exists
                                            (select 1
                                               from ResRelationship rr
                                               join ResponsibleMembers rt
                                                 on rr.restem = rt.id
                                               join ResponsibleDetail re
                                                 on re.responmemberid = rt.id
                                              where rr.id = ad.resrelationshipid
                                                and re.personelid = " + CurrentUser.Id + @" ))";
                        }
                        var cmd = new OracleCommand(Sql, conn);
                        try {
                            var text = string.IsNullOrEmpty(Sql) ? string.Empty : cmd.ExecuteScalar();
                            if(text != null && text != DBNull.Value)
                                result[warningItemSql.Key] = text.ToString();
                        } catch(SqlException) {
                            // 某SQL语句执行失败时，不影响其他语句执行。
                            // TODO：应记录至日志中
                        }
                    }
                }
            return result;
        }

        private int getCompanyType(OracleConnection conn) {
            var companyCmd = new OracleCommand(@"select Type from company where id=:CompanyId", conn);
            companyCmd.Parameters.AddWithValue(":CompanyId", CurrentUser.EnterpriseId);
            int CompanyType = 0;
            try {
                var text = companyCmd.ExecuteScalar();
                if(text != null && text != DBNull.Value)
                    CompanyType = (int)text;
            } catch(SqlException) {
                // 某SQL语句执行失败时，不影响其他语句执行。
                // TODO：应记录至日志中
            }
            return CompanyType;
        }

        /// <summary>
        /// 当为公告时，根据当前登录企业拼接Sql
        /// </summary>
        /// <param name="Sql">查询语句</param>
        /// <param name="conn">数据库连接</param>
        private void GetNotificationSQL(ref string Sql, OracleConnection conn) {
            var companyCmd = new OracleCommand(@"select Type from company where id=:CompanyId", conn);
            companyCmd.Parameters.AddWithValue(":CompanyId", CurrentUser.EnterpriseId);
            int CompanyType = 0;
            try {
                var text = companyCmd.ExecuteScalar();
                if(text != null && text != DBNull.Value)
                    CompanyType = (int)text;
            } catch(SqlException) {
                // 某SQL语句执行失败时，不影响其他语句执行。
                // TODO：应记录至日志中
            }
            /*  企业类型
            1	分公司
            2	服务站
            3	代理库
            4	物流公司
            5	责任单位
            6	配件供应商
            7	服务站兼代理库
            8	集团企业
            */
            string WhereSql = @" where notification.createtime >= to_date(to_char(sysdate,'YYYY/MM/DD'),'yyyy-mm-dd hh24:mi:ss') and Company.Id = " + CurrentUser.EnterpriseId;
            switch(CompanyType) {
                case 1:
                    WhereSql += @" and notification.companyid = " + CurrentUser.EnterpriseId;
                    Sql += WhereSql;
                    break;
                case 2:
                    WhereSql += @" and CompanyId in (select BranchId from DealerServiceInfo where DealerID = " + CurrentUser.EnterpriseId + ")";
                    Sql += WhereSql;
                    break;
                case 3:
                    WhereSql += @" and CompanyId in (select BranchId from Agencyaffibranch where AgencyId = " + CurrentUser.EnterpriseId + ")";
                    Sql += WhereSql;
                    break;
                case 4:
                    WhereSql += @" and CompanyId in (select BranchId from LogisticCompanyServiceRange where LogisticCompanyId = " + CurrentUser.EnterpriseId + ")";
                    Sql += WhereSql;
                    break;
                case 5:
                    WhereSql += @" and CompanyId in (select BranchId from ResponsibleUnitBranch where ResponsibleUnitId = " + CurrentUser.EnterpriseId + ")";
                    Sql += WhereSql;
                    break;
                case 6:
                    WhereSql += @" and CompanyId in (select BranchId from BranchSupplierRelation where SupplierId = " + CurrentUser.EnterpriseId;
                    Sql += WhereSql;
                    break;
                case 7:
                    WhereSql += @" and CompanyId in (select BranchId from DealerServiceInfo where DealerID = " + CurrentUser.EnterpriseId + " union all select BranchId from Agencyaffibranch where AgencyId = " + CurrentUser.EnterpriseId + ")";
                    Sql += WhereSql;
                    break;
            }
        }
    }
}
