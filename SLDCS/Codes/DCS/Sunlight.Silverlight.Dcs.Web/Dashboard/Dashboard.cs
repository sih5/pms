﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.ServiceModel.Activation;
using Devart.Data.Oracle;
using Sunlight.Silverlight.Dcs.Web.Dashboard.Models;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web.Dashboard {
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public partial class Dashboard : IDashboard {
        private const string GET_PERSONNEL_PHOTO_SQL = "SELECT Photo FROM Personnel WHERE Id = :PersonnelId";

        private const string GET_AUTHORIZED_PAGES_SQL = @"SELECT Page.Id, Page.Name, Page.Type, Page.PageType, Page.Parameter AS Uri, Page.Icon, DashboardSetting.Sequence, Page.Description
FROM DashboardSetting
LEFT OUTER JOIN Page ON Page.Id = DashboardSetting.PageId
WHERE DashboardSetting.PersonnelId = :PersonnelId AND Page.Type = 2 AND Page.PageType IN ('Chart', 'Warning')
    AND DashboardSetting.PageId IN (
        SELECT Node.CategoryId FROM RolePersonnel
        LEFT OUTER JOIN Rule ON Rule.RoleId = RolePersonnel.RoleId
        LEFT OUTER JOIN Node ON Rule.NodeId = Node.Id AND Node.CategoryType = 0
        WHERE RolePersonnel.PersonnelId = DashboardSetting.PersonnelId
    )";

        private const string GET_PAGES_SQL = @"SELECT distinct(Page.Id), Page.Name, Page.Type, Page.PageType, Page.Parameter AS Uri, Page.Icon, Page.ParentId + Page.Sequence AS Sequence, Page.Description
FROM RolePersonnel
LEFT OUTER JOIN Rule ON Rule.RoleId = RolePersonnel.RoleId
LEFT OUTER JOIN Node ON Rule.NodeId = Node.Id AND Node.CategoryType = 0
LEFT OUTER JOIN Page ON Page.Id = Node.CategoryId AND Node.CategoryType = 0
WHERE RolePersonnel.PersonnelId = :PersonnelId AND Page.PageType IN ('Chart', 'Warning')";

        private string SecurityConnStr {
            get {
                return ConfigurationManager.ConnectionStrings["SecurityServiceConn"].ConnectionString;
            }
        }

        private string DcsConnStr {
            get {
                return ConfigurationManager.ConnectionStrings["DcsServiceConn"].ConnectionString;
            }
        }

        private Utils.UserInfo CurrentUser {
            get {
                return Utils.GetCurrentUserInfo();
            }
        }

        public DashboardSetting GetDashboardSetting() {
            var setting = new DashboardSetting {
                PersonnelName = CurrentUser.Name,
                LastLoginDate = DateTime.Now,
                AuthorizedPages = new List<SecurityPage>(100),
                CompanyCode = CurrentUser.EnterpriseCode,
                CompanyName = CurrentUser.EnterpriseName
            };

            var personnelId = CurrentUser.Id;
            using(var connection = new OracleConnection(SecurityConnStr)) {
                var photoCmd = new OracleCommand(GET_PERSONNEL_PHOTO_SQL, connection);
                photoCmd.Parameters.AddWithValue(":PersonnelId", personnelId);

                var authorizedPagesCmd = new OracleCommand(GET_AUTHORIZED_PAGES_SQL, connection);
                authorizedPagesCmd.Parameters.AddWithValue(":PersonnelId", personnelId);

                connection.Open();
                var photo = photoCmd.ExecuteScalar();
                //setting.PhotoUri = this
                setting.PhotoUri = photo != null && photo != DBNull.Value ? photo.ToString() : GlobalVar.DEFAULT_AVATAR_URL;

                using(var reader = authorizedPagesCmd.ExecuteReader()) {
                    while(reader != null && reader.Read())
                        setting.AuthorizedPages.Add(new SecurityPage {
                            Id = Convert.ToInt32(reader["Id"]),
                            DisplayName = Convert.ToString(reader["Name"]),
                            Description = Convert.ToString(reader["Description"]),
                            Type = Convert.ToInt32(reader["Type"]),
                            PageType = Convert.ToString(reader["PageType"]),
                            PageUri = reader["Uri"] != DBNull.Value ? new Uri(Convert.ToString(reader["Uri"]), UriKind.Relative) : null,
                            IconUri = reader["Icon"] != DBNull.Value ? new Uri(Convert.ToString(reader["Icon"]), UriKind.Relative) : null,
                            Sequence = Convert.ToInt32(reader["Sequence"])
                        });
                }
                if(setting.AuthorizedPages.Count == 0) {
                    var pagesCmd = new OracleCommand(GET_PAGES_SQL, connection);
                    pagesCmd.Parameters.AddWithValue(":PersonnelId", personnelId);
                    using(var reader = pagesCmd.ExecuteReader()) {
                        while(reader != null && reader.Read())
                            setting.AuthorizedPages.Add(new SecurityPage {
                                Id = Convert.ToInt32(reader["Id"]),
                                DisplayName = Convert.ToString(reader["Name"]),
                                Description = Convert.ToString(reader["Description"]),
                                Type = Convert.ToInt32(reader["Type"]),
                                PageType = Convert.ToString(reader["PageType"]),
                                PageUri = reader["Uri"] != DBNull.Value ? new Uri(Convert.ToString(reader["Uri"]), UriKind.Relative) : null,
                                IconUri = reader["Icon"] != DBNull.Value ? new Uri(Convert.ToString(reader["Icon"]), UriKind.Relative) : null,
                                Sequence = Convert.ToInt32(reader["Sequence"])
                            });
                    }
                }
            }
            return setting;
        }

        public List<SecurityPage> GetAllAuthorizedPages() {
            var result = new List<SecurityPage>();
            using(var connection = new OracleConnection(SecurityConnStr)) {
                var pagesCmd = new OracleCommand(GET_PAGES_SQL, connection);
                pagesCmd.Parameters.AddWithValue(":PersonnelId", CurrentUser.Id);
                connection.Open();
                using(var reader = pagesCmd.ExecuteReader()) {
                    while(reader != null && reader.Read())
                        result.Add(new SecurityPage {
                            Id = Convert.ToInt32(reader["Id"]),
                            DisplayName = Convert.ToString(reader["Name"]),
                            Description = Convert.ToString(reader["Description"]),
                            Type = Convert.ToInt32(reader["Type"]),
                            PageType = Convert.ToString(reader["PageType"]),
                            PageUri = reader["Uri"] != DBNull.Value ? new Uri(Convert.ToString(reader["Uri"]), UriKind.Relative) : null,
                            IconUri = reader["Icon"] != DBNull.Value ? new Uri(Convert.ToString(reader["Icon"]), UriKind.Relative) : null,
                            Sequence = Convert.ToInt32(reader["Sequence"])
                        });
                }
            }
            return result;
        }

        public void SaveDashboardSetting(List<SecurityPage> pages) {
            var authorizedPages = GetAllAuthorizedPages();
            var securityPageComparer = new SecurityPageComparer();
            var personnelId = CurrentUser.Id;
            using(var connection = new OracleConnection(SecurityConnStr)) {
                connection.Open();
                using(var transaction = connection.BeginTransaction()) {
                    var pagesCmd = new OracleCommand("DELETE FROM DashboardSetting WHERE PersonnelId = :PersonnelId;",
                        connection, transaction);
                    pagesCmd.Parameters.AddWithValue(":PersonnelId", personnelId);
                    pagesCmd.ExecuteNonQuery();
                    //var dataTable = new DataTable();
                    //dataTable.Columns.Add("PersonnelId", typeof(int));
                    //dataTable.Columns.Add("PageId", typeof(int));
                    //dataTable.Columns.Add("Sequence", typeof(int));

                    //var bulkCopy = new OracleBulkCopy(connection, SqlBulkCopyOptions.Default, transaction);
                    //foreach(DataColumn column in dataTable.Columns)
                    //    bulkCopy.ColumnMappings.Add(column.ColumnName, column.ColumnName);

                    //dataTable.TableName = "DashboardSetting";
                    //foreach(var page in pages.Intersect(authorizedPages, securityPageComparer)) {
                    //    var row = dataTable.NewRow();
                    //    row["PersonnelId"] = personnelId;
                    //    row["PageId"] = page.Id;
                    //    row["Sequence"] = page.Sequence;
                    //    dataTable.Rows.Add(row);
                    //}
                    //bulkCopy.DestinationTableName = "DashboardSetting";
                    //bulkCopy.WriteToServer(dataTable);
                    foreach(var page in pages.Intersect(authorizedPages, securityPageComparer)) {
                        var pagesInsert = new OracleCommand("Innert into DashboardSetting values (S_DashboardSetting.Nextval,:PersonnelId,:PageId,:Sequence)");
                        pagesInsert.Parameters.AddWithValue(":PersonnelId", personnelId);
                        pagesInsert.Parameters.AddWithValue(":PageId", page.Id);
                        pagesInsert.Parameters.AddWithValue(":Sequence", page.Sequence);
                        pagesCmd.ExecuteNonQuery();
                    }
                    transaction.Commit();
                }
            }
        }
    }
}
