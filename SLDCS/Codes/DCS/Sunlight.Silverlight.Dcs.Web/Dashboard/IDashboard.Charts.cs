﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using Sunlight.Silverlight.Dcs.Web.Dashboard.Models.Charts;

namespace Sunlight.Silverlight.Dcs.Web.Dashboard {
    public partial interface IDashboard {
        #region 整车
        /// <summary>
        /// 当前整车库存结构
        /// </summary>
        /// <returns>以车型为分组的车辆数</returns>
        [OperationContract]
        List<VehicleModelCount> GetVehicleStocks();
        /// <summary>
        /// 当日累计整车销售出库量
        /// </summary>
        /// <returns>以车型为分组的车辆数</returns>
        [OperationContract]
        List<VehicleModelCount> GetDeliveryVehiclesByDay();
        /// <summary>
        /// 当月累计整车销售出库量
        /// </summary>
        /// <returns>以车型为分组的车辆数</returns>
        [OperationContract]
        List<VehicleModelCount> GetDeliveryVehiclesByMonth();
        #endregion

        #region 配件
        /// <summary>
        /// 当前配件库存结构
        /// </summary>
        /// <returns>以ABC属性为分组的配件数</returns>
        [OperationContract]
        Dictionary<string, decimal> GetSparePartStocks();
        #endregion

        #region 服务
        /// <summary>
        /// 预约维修率
        /// </summary>
        /// <returns>预约维修的百分比</returns>
        [OperationContract]
        decimal GetRepairContractBookingPercent();
        /// <summary>
        /// 当月及时交车率/一次修复率
        /// </summary>
        /// <returns>数量为2的List，第一个为及时交车率，第二个为一次修复率</returns>
        [OperationContract]
        List<decimal> GetRepairContractDeliverPrecent();
        /// <summary>
        /// 当月个人维修接待业务占比
        /// </summary>
        /// <returns>以维修顾问为分组的维修合同数</returns>
        [OperationContract]
        Dictionary<string, int> GetRepairProportionsByAdvisor();
        #endregion

        #region CRM
        /// <summary>
        /// H/A/B/C客户的占比
        /// </summary>
        /// <returns>以有望确度为分组的潜在客户数</returns>
        [OperationContract]
        Dictionary<string, int> GetCustomerProportionsByLevel();
        /// <summary>
        /// 当月来店客户数量
        /// </summary>
        /// <returns>以日期为分组的展厅来访登记数</returns>
        [OperationContract]
        Dictionary<DateTime, int> GetHallVisitRegistrationsByDay();
        /// <summary>
        /// 当月来电客户数量
        /// </summary>
        /// <returns>以日期为分组的来电登记数</returns>
        [OperationContract]
        Dictionary<DateTime, int> GetCallRegistrationsByDay();
        /// <summary>
        /// 当月试乘试驾数量
        /// </summary>
        /// <returns>以日期为分组的试乘试驾数</returns>
        [OperationContract]
        Dictionary<DateTime, int> GetTestDriveRecordsByDay();
        #endregion

        #region 测试Demo
        [OperationContract]
        Dictionary<string, int> GetPartsStocksByWarehouse();
        #endregion
    }
}
