﻿using System.Collections.Generic;
using System.ServiceModel;
using Sunlight.Silverlight.Dcs.Web.Dashboard.Models;

namespace Sunlight.Silverlight.Dcs.Web.Dashboard {
    /// <summary>
    /// 主控台服务
    /// </summary>
    [ServiceContract(Namespace = "http://sunlight.bz/dashboard/")]
    public partial interface IDashboard {
        /// <summary>
        /// 获取当前用户的主控台配置
        /// </summary>
        /// <returns>主控台配置</returns>
        [OperationContract]
        DashboardSetting GetDashboardSetting();

        /// <summary>
        /// 获取当前用户被授权的警告信息和统计图表集合
        /// </summary>
        /// <returns>被授权的警告信息和统计图表集合</returns>
        [OperationContract]
        List<SecurityPage> GetAllAuthorizedPages();

        /// <summary>
        /// 保存当前用户的主控台配置信息
        /// </summary>
        /// <param name="pages"></param>
        [OperationContract]
        void SaveDashboardSetting(List<SecurityPage> pages);

        #region 统计信息项
        /// <summary>
        /// 获取指定的统计信息项的结果集合
        /// </summary>
        /// <param name="ids">统计信息项的 Id 集合</param>
        /// <returns>统计信息项的结果集合</returns>
        [OperationContract]
        Dictionary<int, string> GetWarnings(int[] ids);
        #endregion
    }
}
