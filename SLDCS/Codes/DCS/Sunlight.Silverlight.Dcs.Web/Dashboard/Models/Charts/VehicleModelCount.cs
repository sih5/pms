﻿using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web.Dashboard.Models.Charts {
    [DataContract(Namespace = "http://sunlight.bz/dashboard/")]
    public class VehicleModelCount {
        [DataMember]
        public int Count;

        [DataMember]
        public string VehicleModelCode;

        [DataMember]
        public string VehicleModelName;
    }
}