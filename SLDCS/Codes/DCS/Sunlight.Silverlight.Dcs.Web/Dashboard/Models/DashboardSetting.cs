﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web.Dashboard.Models {
    /// <summary>
    /// 主控台配置
    /// </summary>
    [DataContract(Namespace = "http://sunlight.bz/dashboard/")]
    public class DashboardSetting {
        /// <summary>
        /// 当前登录用户的姓名
        /// </summary>
        [DataMember]
        public string PersonnelName;

        /// <summary>
        /// 最近一次登录系统的时间
        /// </summary>
        [DataMember]
        public DateTime LastLoginDate;

        /// <summary>
        /// 当前登录用户的头像图片路径
        /// </summary>
        [DataMember]
        public string PhotoUri;

        /// <summary>
        /// 被授权的统计信息项和图表项集合
        /// </summary>
        [DataMember]
        public List<SecurityPage> AuthorizedPages;

        /// <summary>
        /// 当前登录企业编号
        /// </summary>
        [DataMember]
        public string CompanyCode;

        /// <summary>
        /// 当前登录企业名称
        /// </summary>
        [DataMember]
        public string CompanyName;
    }
}
