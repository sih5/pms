﻿using System;
using System.Runtime.Serialization;

namespace Sunlight.Silverlight.Dcs.Web.Dashboard.Models {
    [DataContract(Namespace = "http://sunlight.bz/dashboard/")]
    public class SecurityPage {
        [DataMember]
        public int Id;

        [DataMember]
        public string DisplayName;

        [DataMember]
        public string Description;

        [DataMember]
        public int Type;

        [DataMember]
        public Uri PageUri;

        [DataMember]
        public string PageType;

        [DataMember]
        public Uri IconUri;

        [DataMember]
        public int Sequence;
    }
}
