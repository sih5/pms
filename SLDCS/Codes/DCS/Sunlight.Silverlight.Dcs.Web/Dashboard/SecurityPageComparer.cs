﻿using System.Collections.Generic;
using Sunlight.Silverlight.Dcs.Web.Dashboard.Models;

namespace Sunlight.Silverlight.Dcs.Web.Dashboard {
    public class SecurityPageComparer : IEqualityComparer<SecurityPage> {
        public bool Equals(SecurityPage x, SecurityPage y) {
            return x.Id == y.Id;
        }

        public int GetHashCode(SecurityPage obj) {
            return obj.Id;
        }
    }
}
