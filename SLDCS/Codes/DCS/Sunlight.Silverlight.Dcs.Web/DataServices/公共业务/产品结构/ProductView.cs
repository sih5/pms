﻿using System.Linq;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class ProductViewAch : DcsSerivceAchieveBase {
        public ProductViewAch(DcsDomainService domainService)
            : base(domainService) {
        }

        public IQueryable<ProductView> GetProductViewWithServiceProductLineView() {
            var productViews = ObjectContext.ProductViews;
            var serviceProductLineViews = ObjectContext.ServiceProductLineViews.Where(r => productViews.Select(v => v.ServiceProductLineId).Contains(r.ProductLineId)).Select(r => new {
                r.ProductLineId,
                r.ProductLineName
            }).ToList();
            foreach(var productView in productViews.ToList()) {
                productView.ServiceProductLineName = serviceProductLineViews.FirstOrDefault(r => r.ProductLineId == productView.ServiceProductLineId) != null ? serviceProductLineViews.First(r => r.ProductLineId == productView.ServiceProductLineId).ProductLineName : string.Empty;
            }
            return productViews.OrderBy(e => e.Id);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:29
        //原分布类的函数全部转移到Ach                                                               

        public IQueryable<ProductView> GetProductViewWithServiceProductLineView() {
            return new ProductViewAch(this).GetProductViewWithServiceProductLineView();
        }
    }
}
