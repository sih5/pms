﻿using System.Linq;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Web;using System.ServiceModel.DomainServices.Server;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class ServiceProductLineViewAch : DcsSerivceAchieveBase {
        public ServiceProductLineViewAch(DcsDomainService domainService)
            : base(domainService) {
        }

        public IQueryable<ServiceProductLineView> GetServiceProductLineViewWithDetail() {
            return ObjectContext.ServiceProductLineViews.Include("EngineProductLine").Include("ServiceProductLine").Include("Branch").OrderBy(e => e.ProductLineId);
        }
        public ServiceProductLineView GetServiceProductLineViewByProductId(int productId) {
            var servProdLineAffiProduct = this.ObjectContext.ServProdLineAffiProducts.SingleOrDefault(r => r.ProductId == productId);
            if(servProdLineAffiProduct != null) {
                return this.ObjectContext.ServiceProductLineViews.SingleOrDefault(r => r.ProductLineId == servProdLineAffiProduct.ServiceProductLineId);
            }
            return null;
        }
        [Query(HasSideEffects = true)]
        public IQueryable<ServiceProductLineView> GetServiceProductLineViewByServiceProductLineIds(int[] serviceProductLineIds){
            return this.ObjectContext.ServiceProductLineViews.Where(r => serviceProductLineIds.Contains(r.ProductLineId));
        }
        public IQueryable<ServiceProductLineView> GetServiceProductLineViewWithPartsSalesCategory() {
            var usrinfo = Utils.GetCurrentUserInfo();
            var serviceProductLineView = ObjectContext.ServiceProductLineViews.Where(r => this.ObjectContext.PersonSalesCenterLinks.Any(x => x.PartsSalesCategoryId == r.PartsSalesCategoryId && x.PersonId == usrinfo.Id && x.Status == (int)DcsBaseDataStatus.有效)).Include("PartsSalesCategory");
            return serviceProductLineView.OrderBy(v => v.ProductLineId);
        }

    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:29
        //原分布类的函数全部转移到Ach                                                               

        public IQueryable<ServiceProductLineView> GetServiceProductLineViewWithDetail() {
            return new ServiceProductLineViewAch(this).GetServiceProductLineViewWithDetail();
        }                                                  

        public IQueryable<ServiceProductLineView> GetServiceProductLineViewWithPartsSalesCategory() {
            return new ServiceProductLineViewAch(this).GetServiceProductLineViewWithPartsSalesCategory();
        }

        public ServiceProductLineView GetServiceProductLineViewByProductId(int productId) {
            return new ServiceProductLineViewAch(this).GetServiceProductLineViewByProductId(productId);
        }
        
        [Query(HasSideEffects = true)]
        public IQueryable<ServiceProductLineView> GetServiceProductLineViewByServiceProductLineIds(int[] serviceProductLineIds){
            return new ServiceProductLineViewAch(this).GetServiceProductLineViewByServiceProductLineIds(serviceProductLineIds);
        }
    }
}
