﻿using System.Linq;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class VehModelRetailSuggestedPriceAch : DcsSerivceAchieveBase {
        public VehModelRetailSuggestedPriceAch(DcsDomainService domainService)
            : base(domainService) {
        }

        public IQueryable<VehModelRetailSuggestedPrice> 查询销售车辆产品以及价格(string productCategoryCode, string productCategoryName) {
            IQueryable<VehModelRetailSuggestedPrice> vehModelRetailSuggestedPrices = ObjectContext.VehModelRetailSuggestedPrices;
            if(!string.IsNullOrEmpty(productCategoryCode))
                vehModelRetailSuggestedPrices = vehModelRetailSuggestedPrices.Where(r => r.ProductCategoryCode.ToLower().Contains(productCategoryCode.ToLower()));
            if(!string.IsNullOrEmpty(productCategoryName))
                vehModelRetailSuggestedPrices = vehModelRetailSuggestedPrices.Where(r => r.ProductCategoryName.ToLower().Contains(productCategoryName.ToLower()));
            return vehModelRetailSuggestedPrices.OrderBy(e => e.ProductCategoryId);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:29
        //原分布类的函数全部转移到Ach                                                               

        public IQueryable<VehModelRetailSuggestedPrice> 查询销售车辆产品以及价格(string productCategoryCode, string productCategoryName) {
            return new VehModelRetailSuggestedPriceAch(this).查询销售车辆产品以及价格(productCategoryCode,productCategoryName);
        }
    }
}