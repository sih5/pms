﻿using System;
using System.Linq;
using Sunlight.Silverlight.Web;
using System.Collections.Generic;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class AgencyAch : DcsSerivceAchieveBase {
        internal void InsertAgencyValidate(Agency agency) {
            var userInfo = Utils.GetCurrentUserInfo();
            agency.CreatorId = userInfo.Id;
            agency.CreatorName = userInfo.Name;
            agency.CreateTime = DateTime.Now;
        }

        internal void UpdateAgencyValidate(Agency agency) {
            var userInfo = Utils.GetCurrentUserInfo();
            agency.ModifierId = userInfo.Id;
            agency.ModifierName = userInfo.Name;
            agency.ModifyTime = DateTime.Now;
        }

        public void InsertAgency(Agency agency) {
            InsertToDatabase(agency);
            this.InsertAgencyValidate(agency);
        }

        public void UpdateAgency(Agency agency) {
            UpdateToDatabase(agency);
            this.UpdateAgencyValidate(agency);
        }

        public Agency GetAgencyWithCompanyAndAgencyAffiBranch(int id) {
            var dbAgency = ObjectContext.Agencies.SingleOrDefault(r => r.Id == id);
            if(dbAgency != null) {
                var company = ObjectContext.Companies.SingleOrDefault(r => r.Id == dbAgency.Id);
                var agencyAffiBranches = ObjectContext.AgencyAffiBranches.Where(r => r.AgencyId == dbAgency.Id).ToArray();
                var marketingDepartment = ObjectContext.MarketingDepartments.SingleOrDefault(r => r.Id == dbAgency.MarketingDepartmentId);
                var centerAuthorizedProvinces = ObjectContext.CenterAuthorizedProvinces.Where(r => r.AgencyId == dbAgency.Id).ToArray();
            }
            return dbAgency;
        }
        public IQueryable<Agency> GetAgencyCompany() {
            return ObjectContext.Agencies.Include("Company").Include("MarketingDepartment").OrderBy(v => v.Id);
        }
    
        //根据中心库/服务站编号 查询customerinfo_tmp 表中的名称
        public VirtualIdNameForTmp getCustomerNameByCodeForTmp(string code) {
            if (string.IsNullOrEmpty(code))
                return null;
            string SQL = @"select Id,custcode as Code,custname as Name from customerinfo_tmp where custcode = '" + code + "'";
            var search = ObjectContext.ExecuteStoreQuery<VirtualIdNameForTmp>(SQL).ToList();
            return search.FirstOrDefault();
        }

        public IEnumerable<VirtualIdNameForTmp> getCustomerTmp(string code, string name) {
            string SQL = @"select Id,custcode as Code,custname as Name,createttime as createTime from customerinfo_tmp where 1=1";
            if (!string.IsNullOrEmpty(code))
                SQL += " and  custcode like '%" + code + "%'";
            if (!string.IsNullOrEmpty(name))
                SQL += " and  custname like '%" + name + "%'";
            var search = ObjectContext.ExecuteStoreQuery<VirtualIdNameForTmp>(SQL).ToList();
            return search;
        }

        public IEnumerable<VirtualIdNameForTmp> getSupplierTmp(string code, string name) {
            string SQL = @"select Id,suppcode as Code,suppname as Name,createttime as createTime from supplier_tmp where 1=1";
            if (!string.IsNullOrEmpty(code))
                SQL += " and  suppcode like '%" + code + "%'";
            if (!string.IsNullOrEmpty(name))
                SQL += " and  suppname like '%" + name + "%'";
            var search = ObjectContext.ExecuteStoreQuery<VirtualIdNameForTmp>(SQL).ToList();
            return search;
        }
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:29
        //原分布类的函数全部转移到Ach                                                               

        public void InsertAgency(Agency agency) {
            new AgencyAch(this).InsertAgency(agency);
        }

        public void UpdateAgency(Agency agency) {
            new AgencyAch(this).UpdateAgency(agency);
        }

        public Agency GetAgencyWithCompanyAndAgencyAffiBranch(int id) {
            return new AgencyAch(this).GetAgencyWithCompanyAndAgencyAffiBranch(id);
        }
        public IQueryable<Agency> GetAgencyCompany() {
            return new AgencyAch(this).GetAgencyCompany();
        }

        public VirtualIdNameForTmp getCustomerNameByCodeForTmp(string code) {
            return new AgencyAch(this).getCustomerNameByCodeForTmp(code);
        }

        public IEnumerable<VirtualIdNameForTmp> getCustomerTmp(string code,string name) {
            return new AgencyAch(this).getCustomerTmp(code,name);
        }
        public IEnumerable<VirtualIdNameForTmp> getSupplierTmp(string code, string name) {
            return new AgencyAch(this).getSupplierTmp(code, name);
        }
    }
}
