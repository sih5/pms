﻿using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class AgencyAffiBranchAch : DcsSerivceAchieveBase {
        public AgencyAffiBranchAch(DcsDomainService domainService)
            : base(domainService) {
        }

        internal void InsertAgencyAffiBranchValidate(AgencyAffiBranch agencyAffiBranch) {
            var dbagencyAffiBranch = ObjectContext.AgencyAffiBranches.Where(r => r.AgencyId == agencyAffiBranch.AgencyId && r.BranchId == agencyAffiBranch.BranchId).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbagencyAffiBranch != null)
                throw new ValidationException(ErrorStrings.AgencyAffiBranch_Validation1);
        }
        public void InsertAgencyAffiBranch(AgencyAffiBranch agencyAffiBranch) {
            InsertToDatabase(agencyAffiBranch);
            this.InsertAgencyAffiBranchValidate(agencyAffiBranch);
        }
        public void UpdateAgencyAffiBranch(AgencyAffiBranch agencyAffiBranch) {
            //客户端需要提供空的Update方法作更新操作
        }
        public void DeleteAgencyAffiBranch(AgencyAffiBranch agencyAffiBranch) {
            DeleteFromDatabase(agencyAffiBranch);
        }
        public IQueryable<AgencyAffiBranch> 根据营销分公司查询代理库(int branchId) {
            return ObjectContext.AgencyAffiBranches.Where(r => r.BranchId == branchId).Include("Agency.Company").Include("Branch").OrderBy(r => r.Id);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:29
        //原分布类的函数全部转移到Ach                                                               

        public void InsertAgencyAffiBranch(AgencyAffiBranch agencyAffiBranch) {
            new AgencyAffiBranchAch(this).InsertAgencyAffiBranch(agencyAffiBranch);
        }

        public void UpdateAgencyAffiBranch(AgencyAffiBranch agencyAffiBranch) {
            new AgencyAffiBranchAch(this).UpdateAgencyAffiBranch(agencyAffiBranch);
        }

        public void DeleteAgencyAffiBranch(AgencyAffiBranch agencyAffiBranch) {
            new AgencyAffiBranchAch(this).DeleteAgencyAffiBranch(agencyAffiBranch);
        }

        public IQueryable<AgencyAffiBranch> 根据营销分公司查询代理库(int branchId) {
            return new AgencyAffiBranchAch(this).根据营销分公司查询代理库(branchId);
        }
    }
}
