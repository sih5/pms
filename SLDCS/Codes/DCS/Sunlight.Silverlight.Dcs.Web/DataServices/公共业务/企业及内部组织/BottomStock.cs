﻿using System;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class BottomStockAch : DcsSerivceAchieveBase {
        internal void InsertBottomStockValidate(BottomStock bottomStock) {
            var userInfo = Utils.GetCurrentUserInfo();
            bottomStock.CreatorId = userInfo.Id;
            bottomStock.CreatorName = userInfo.Name;
            bottomStock.CreateTime = DateTime.Now;
        }

        internal void UpdateBottomStockValidate(BottomStock bottomStock) {
            var userInfo = Utils.GetCurrentUserInfo();
            bottomStock.ModifierId = userInfo.Id;
            bottomStock.ModifierName = userInfo.Name;
            bottomStock.ModifyTime = DateTime.Now;
        }

        public void InserteBottomStock(BottomStock bottomStock) {
            InsertToDatabase(bottomStock);
            this.InsertBottomStockValidate(bottomStock);
        }

        public void UpdateBottomStock(BottomStock bottomStock) {
            UpdateToDatabase(bottomStock);
            this.UpdateBottomStockValidate(bottomStock);
        }
        public void DeleteBottomStock(BottomStock bottomStock) {
            DeleteFromDatabase(bottomStock);
        }
        public IQueryable<BottomStock> QueryBottomStocks(string sparePartCodes) {
            var result = ObjectContext.BottomStocks.Include("PartsSalesCategory").Include("Company").Include("SparePart").Include("Warehouse").AsQueryable();
            if (!string.IsNullOrEmpty(sparePartCodes)) {
                var spareparts = sparePartCodes.Split(',');
                if (spareparts.Length == 1) {
                    var sparepartcode = spareparts[0];
                    result = result.Where(r => r.SparePartCode.Contains(sparepartcode));
                } else { 
                    result = result.Where(r => spareparts.Contains(r.SparePartCode));
                }
            }
            return result.OrderBy(r => r.Id);

        }

        public IQueryable<BottomStockColVersion> 查询保底库存表信息2(int? companyId, int? warehouseId, int[] partsId) {
            var result = ObjectContext.BottomStockColVersions.Where(r => r.CompanyId == companyId);         
            if (partsId != null && partsId.Length > 0)
                result = result.Where(r => partsId.Contains(r.SparePartId));
            return result.OrderBy(r => r.Id);
        }

        public IQueryable<BottomStock> 查询保底库存表信息(int? companyId, int? warehouseId, int[] partsId) {
            var result = ObjectContext.BottomStocks.Where(r => r.Status != (int)DcsBaseDataStatus.作废);
            if(companyId.HasValue)
                result = result.Where(r => r.CompanyID == companyId.Value);
            if(warehouseId.HasValue)
                result = result.Where(r => r.WarehouseID == warehouseId.Value);
            if(partsId != null && partsId.Length > 0)
                result = result.Where(r => partsId.Contains(r.SparePartId));
            return result.OrderBy(r => r.Id);
        }
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:34
        //原分布类的函数全部转移到Ach                                                               

        public void InserteBottomStock(BottomStock bottomStock) {
            new BottomStockAch(this).InserteBottomStock(bottomStock);
        }

        public void UpdateBottomStock(BottomStock bottomStock) {
            new BottomStockAch(this).UpdateBottomStock(bottomStock);
        }

        public void DeleteBottomStock(BottomStock bottomStock) {
            new BottomStockAch(this).DeleteBottomStock(bottomStock);
        }

        public IQueryable<BottomStock> QueryBottomStocks(string sparePartCodes) {
            return new BottomStockAch(this).QueryBottomStocks(sparePartCodes);
        }

        [Query(HasSideEffects = true)]
        public IQueryable<BottomStock> 查询保底库存表信息(int? companyId, int? warehouseId, int[] partsId) {
            return new BottomStockAch(this).查询保底库存表信息(companyId, warehouseId, partsId);
        }
        [Query(HasSideEffects = true)]
        public IQueryable<BottomStockColVersion> 查询保底库存表信息2(int? companyId, int? warehouseId, int[] partsId) {
            return new BottomStockAch(this).查询保底库存表信息2(companyId, warehouseId, partsId);
        }
    }
}
