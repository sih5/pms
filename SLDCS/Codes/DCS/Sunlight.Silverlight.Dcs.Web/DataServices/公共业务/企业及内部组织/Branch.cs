﻿using System;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class BranchAch : DcsSerivceAchieveBase {
        internal void InsertBranchValidate(Branch branch) {
            var userInfo = Utils.GetCurrentUserInfo();
            branch.CreatorId = userInfo.Id;
            branch.CreatorName = userInfo.Name;
            branch.CreateTime = DateTime.Now;
        }

        internal void UpdateBranchValidate(Branch branch) {
            var userInfo = Utils.GetCurrentUserInfo();
            branch.ModifierId = userInfo.Id;
            branch.ModifierName = userInfo.Name;
            branch.ModifyTime = DateTime.Now;
        }

        public void InsertBranch(Branch branch) {
            InsertToDatabase(branch);
            this.InsertBranchValidate(branch);
        }

        public void UpdateBranch(Branch branch) {
            UpdateToDatabase(branch);
            this.UpdateBranchValidate(branch);
        }

        public IQueryable<Branch> GetBranchWithCompany() {
            return ObjectContext.Branches.Include("Company").OrderBy(v => v.Id);
        }

        [Query(HasSideEffects = true)]
        public IQueryable<Branch> GetBranchesByIds(int[] ids) {
            return ObjectContext.Branches.Where(e => ids.Contains(e.Id) && e.Status == (int)DcsMasterDataStatus.有效).OrderBy(r => r.Id);
        }

        public IQueryable<Branch> 根据企业查询营销分公司(int companyId) {
            var company = ObjectContext.Companies.SingleOrDefault(r => r.Id == companyId);
            if(company == null)
                return null;
            switch(company.Type) {
                case (int)DcsCompanyType.代理库:
                    return ObjectContext.Branches.Where(r => ObjectContext.AgencyAffiBranches.Any(v => v.AgencyId == companyId && v.BranchId == r.Id)).OrderBy(r => r.Id);
                case (int)DcsCompanyType.服务站兼代理库:
                    return ObjectContext.Branches.Where(r => ObjectContext.AgencyAffiBranches.Any(v => v.AgencyId == companyId && v.BranchId == r.Id)).OrderBy(r => r.Id);
                case (int)DcsCompanyType.服务站:
                    return ObjectContext.Branches.Where(r => ObjectContext.DealerServiceInfoes.Any(v => v.DealerId == companyId && v.BranchId == r.Id)).OrderBy(r => r.Id);
                default:
                    return null;
            }
        }

        public IQueryable<Branch> GetBranchByCustomerInformation() {
            var userInfo = Utils.GetCurrentUserInfo();
            return ObjectContext.Branches.Where(r => ObjectContext.CustomerInformations.Any(e => e.CustomerCompanyId == userInfo.EnterpriseId && e.SalesCompanyId == r.Id && r.Status != (int)DcsMasterDataStatus.作废)).OrderBy(r => r.Id);
        }
        public IQueryable<Branch> GetBranchByDealerServiceInfo() {
            var userInfo = Utils.GetCurrentUserInfo();
            return ObjectContext.Branches.Where(r => ObjectContext.DealerServiceInfoes.Any(e => e.DealerId == userInfo.EnterpriseId && e.BranchId == r.Id && r.Status == (int)DcsMasterDataStatus.有效 && e.Status == (int)DcsMasterDataStatus.有效)).OrderBy(r => r.Id);
        }
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:29
        //原分布类的函数全部转移到Ach                                                               

        public void InsertBranch(Branch branch) {
            new BranchAch(this).InsertBranch(branch);
        }

        public void UpdateBranch(Branch branch) {
            new BranchAch(this).UpdateBranch(branch);
        }

        public IQueryable<Branch> GetBranchWithCompany() {
            return new BranchAch(this).GetBranchWithCompany();
        }
        public IQueryable<Branch> GetBranchByDealerServiceInfo() {
            return new BranchAch(this).GetBranchByDealerServiceInfo();
        }


        [Query(HasSideEffects = true)]
        public IQueryable<Branch> GetBranchesByIds(int[] ids) {
            return new BranchAch(this).GetBranchesByIds(ids);
        }

        public IQueryable<Branch> 根据企业查询营销分公司(int companyId) {
            return new BranchAch(this).根据企业查询营销分公司(companyId);
        }

        public IQueryable<Branch> GetBranchByCustomerInformation() {
            return new BranchAch(this).GetBranchByCustomerInformation();
        }
    }
}
