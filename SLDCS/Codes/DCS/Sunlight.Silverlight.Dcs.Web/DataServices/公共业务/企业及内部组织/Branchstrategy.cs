﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
//请注意进行Using移除和排序
namespace Sunlight.Silverlight.Dcs.Web {
    partial class BranchstrategyAch : DcsSerivceAchieveBase {
        internal void InsertBranchstrategyValidate(Branchstrategy branchstrategy) {
            var dbBranchstrategy = ObjectContext.Branchstrategies.Where(r => r.BranchId == branchstrategy.BranchId && r.PartsPurchasePricingStrategy == branchstrategy.PartsPurchasePricingStrategy && r.Status != (int)DcsBaseDataStatus.作废).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbBranchstrategy != null)
                throw new ValidationException(ErrorStrings.Branchstrategy_Validation1);
            var dbBranchstrategyUsed = ObjectContext.Branchstrategies.Where(r => r.BranchId == branchstrategy.BranchId && r.UsedPartsReturnStrategy == branchstrategy.UsedPartsReturnStrategy && r.Status != (int)DcsBaseDataStatus.作废).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbBranchstrategyUsed != null)
                throw new ValidationException(ErrorStrings.Branchstrategy_Validation2);
            var userInfo = Utils.GetCurrentUserInfo();
            branchstrategy.CreatorId = userInfo.Id;
            branchstrategy.CreatorName = userInfo.Name;
            branchstrategy.CreateTime = DateTime.Now;
        }
        internal void UpdateBranchstrategyValidate(Branchstrategy branchstrategy) {
            var dbBranchstrategy = ObjectContext.Branchstrategies.Where(r => r.Id != branchstrategy.Id && r.BranchId == branchstrategy.BranchId && r.PartsPurchasePricingStrategy == branchstrategy.PartsPurchasePricingStrategy && r.Status != (int)DcsBaseDataStatus.作废).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbBranchstrategy != null)
                throw new ValidationException(ErrorStrings.Branchstrategy_Validation1);
            var dbBranchstrategyUsed = ObjectContext.Branchstrategies.Where(r => r.Id != branchstrategy.Id && r.BranchId == branchstrategy.BranchId && r.UsedPartsReturnStrategy == branchstrategy.UsedPartsReturnStrategy && r.Status != (int)DcsBaseDataStatus.作废).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbBranchstrategyUsed != null)
                throw new ValidationException(ErrorStrings.Branchstrategy_Validation2);
            var userInfo = Utils.GetCurrentUserInfo();
            branchstrategy.ModifierId = userInfo.Id;
            branchstrategy.ModifierName = userInfo.Name;
            branchstrategy.ModifyTime = DateTime.Now;
        }
        public void InsertBranchstrategy(Branchstrategy branchstrategy) {
            InsertToDatabase(branchstrategy);
            this.InsertBranchstrategyValidate(branchstrategy);
        }
        public void UpdateBranchstrategy(Branchstrategy branchstrategy) {
            UpdateToDatabase(branchstrategy);
            this.UpdateBranchstrategyValidate(branchstrategy);
        }
        public Branchstrategy GetBranchstrategyByBranchId(int branchId) {
            var branchstrategy = ObjectContext.Branchstrategies.SingleOrDefault(r => r.BranchId == branchId);
            if(branchstrategy == null) {
                throw new ValidationException(ErrorStrings.SupplierShippingOrder_Validation29);
            }
            return branchstrategy;
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:29
        //原分布类的函数全部转移到Ach                                                               

        public void InsertBranchstrategy(Branchstrategy branchstrategy) {
            new BranchstrategyAch(this).InsertBranchstrategy(branchstrategy);
        }

        public void UpdateBranchstrategy(Branchstrategy branchstrategy) {
            new BranchstrategyAch(this).UpdateBranchstrategy(branchstrategy);
        }

        public Branchstrategy GetBranchstrategyByBranchId(int branchId) {
            return new BranchstrategyAch(this).GetBranchstrategyByBranchId(branchId);
        }
    }
}
