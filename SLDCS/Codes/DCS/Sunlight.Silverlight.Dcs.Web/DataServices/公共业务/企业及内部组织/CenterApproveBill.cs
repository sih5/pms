﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class CenterApproveBillAch : DcsSerivceAchieveBase {
        public CenterApproveBillAch(DcsDomainService domainService)
            : base(domainService) {
        }

        public void Insert(CenterApproveBill centerApproveBill) {
        }
        public void Update(CenterApproveBill centerApproveBill) {
        }

        public IQueryable<CenterApproveBill> GetCenterApproveBillWithMarket() {
            return ObjectContext.CenterApproveBills.Include("MarketingDepartment").OrderBy(v => v.Id);
        }
        private CenterApproveBill convertWeek(CenterApproveBill centerApproveBill) {
            if (centerApproveBill.ApproveWeek == "1") {
                centerApproveBill.ApproveWeek = ErrorStrings.CenterApproveBill_Text_Monday;
            }else if (centerApproveBill.ApproveWeek == "2") {
                centerApproveBill.ApproveWeek =ErrorStrings.CenterApproveBill_Text_Tuesday;
            }else if (centerApproveBill.ApproveWeek == "3") {
                centerApproveBill.ApproveWeek = ErrorStrings.CenterApproveBill_Text_Wednesday;
            }else if (centerApproveBill.ApproveWeek == "4") {
                centerApproveBill.ApproveWeek = ErrorStrings.CenterApproveBill_Text_Thursday;
            }else if (centerApproveBill.ApproveWeek == "5") {
                centerApproveBill.ApproveWeek = ErrorStrings.CenterApproveBill_Text_Friday;
            }
            return centerApproveBill;
        }
        public void InsertCenterApproveBill(CenterApproveBill centerApproveBill) {
            var dbcenterApproveBill = ObjectContext.CenterApproveBills.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.CompanyCode == centerApproveBill.CompanyCode && r.WarehouseId==centerApproveBill.WarehouseId).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if (dbcenterApproveBill != null) {
                throw new ValidationException(ErrorStrings.CenterApproveBill_Validation1);
            }
            var agency = ObjectContext.Agencies.FirstOrDefault(r => r.Id == centerApproveBill.CompanyId);
            if (agency != null) {
                centerApproveBill.MarketingDepartmentId = agency.MarketingDepartmentId;
            }
            convertWeek(centerApproveBill);
            var user = Utils.GetCurrentUserInfo();
            centerApproveBill.Status = (int)DcsBaseDataStatus.有效;
            centerApproveBill.CreatorId = user.Id;
            centerApproveBill.CreatorName = user.Name;
            centerApproveBill.CreateTime = DateTime.Now;
            InsertToDatabase(centerApproveBill);
        }

        public void UpdateCenterApproveBill(CenterApproveBill centerApproveBill) {
            var dbcenterApproveBill = ObjectContext.CenterApproveBills.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.Id == centerApproveBill.Id).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if (dbcenterApproveBill == null) {
                throw new ValidationException(ErrorStrings.CenterApproveBill_Validation2);
            }
            var dbcenterApproveBills = ObjectContext.CenterApproveBills.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.Id != centerApproveBill.Id && r.CompanyCode == centerApproveBill.CompanyCode && r.WarehouseId == centerApproveBill.WarehouseId).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if (dbcenterApproveBills != null) {
                throw new ValidationException(ErrorStrings.CenterApproveBill_Validation1);
            }
            var agency = ObjectContext.Agencies.FirstOrDefault(r => r.Id == centerApproveBill.CompanyId);
            if (agency != null) {
                centerApproveBill.MarketingDepartmentId = agency.MarketingDepartmentId;
            }
            convertWeek(centerApproveBill);
            var user = Utils.GetCurrentUserInfo();
            centerApproveBill.ModifierId = user.Id;
            centerApproveBill.ModifierName = user.Name;
            centerApproveBill.ModifyTime = DateTime.Now;
            UpdateToDatabase(centerApproveBill);
        }

        public void AbandonCenterApproveBill(CenterApproveBill centerApproveBill) {
            var user = Utils.GetCurrentUserInfo();
            centerApproveBill.Status = (int)DcsBaseDataStatus.作废;
            centerApproveBill.ModifierId = user.Id;
            centerApproveBill.ModifierName = user.Name;
            centerApproveBill.ModifyTime = DateTime.Now;
            UpdateToDatabase(centerApproveBill);
        }
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:09:49
        //原分布类的函数全部转移到Ach                                                               
        public void Insert(CenterApproveBill centerApproveBill) {
            new CenterApproveBillAch(this).Insert(centerApproveBill);
        }
        public void Update(CenterApproveBill centerApproveBill) {
            new CenterApproveBillAch(this).Update(centerApproveBill);
        }
        public IQueryable<CenterApproveBill> GetCenterApproveBillWithMarket() {
            return new CenterApproveBillAch(this).GetCenterApproveBillWithMarket();
        }
        [Update(UsingCustomMethod = true)]
        public void InsertCenterApproveBill(CenterApproveBill centerApproveBill) {
             new CenterApproveBillAch(this).InsertCenterApproveBill(centerApproveBill);
        }

        [Update(UsingCustomMethod = true)]
        public void UpdateCenterApproveBill(CenterApproveBill centerApproveBill) {
            new CenterApproveBillAch(this).UpdateCenterApproveBill(centerApproveBill);
        }
        [Update(UsingCustomMethod = true)]

        public void AbandonCenterApproveBill(CenterApproveBill centerApproveBill) {
            new CenterApproveBillAch(this).AbandonCenterApproveBill(centerApproveBill);
        }
    }
}
