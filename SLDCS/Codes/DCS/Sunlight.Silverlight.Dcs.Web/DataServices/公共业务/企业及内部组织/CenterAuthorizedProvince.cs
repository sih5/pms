﻿using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class CenterAuthorizedProvinceAch : DcsSerivceAchieveBase {
        public CenterAuthorizedProvinceAch(DcsDomainService domainService)
            : base(domainService) {
        }

        public void InsertCenterAuthorizedProvince(CenterAuthorizedProvince centerAuthorizedProvince) {
            InsertToDatabase(centerAuthorizedProvince);
        }
        public void UpdateCenterAuthorizedProvince(CenterAuthorizedProvince centerAuthorizedProvince) {
            //客户端需要提供空的Update方法作更新操作
        }
        public void DeleteCenterAuthorizedProvince(CenterAuthorizedProvince centerAuthorizedProvince) {
            DeleteFromDatabase(centerAuthorizedProvince);
        }

        public IQueryable<CenterAuthorizedProvince> getCenterAuthorizedProvincesForAgency(int agencyId) {
            return ObjectContext.CenterAuthorizedProvinces.Where(r => r.AgencyId == agencyId);
        }
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:29
        //原分布类的函数全部转移到Ach                                                               

        public void InsertCenterAuthorizedProvince(CenterAuthorizedProvince centerAuthorizedProvince) {
            new CenterAuthorizedProvinceAch(this).InsertCenterAuthorizedProvince(centerAuthorizedProvince);
        }

        public void UpdateCenterAuthorizedProvince(CenterAuthorizedProvince centerAuthorizedProvince) {
            new CenterAuthorizedProvinceAch(this).UpdateCenterAuthorizedProvince(centerAuthorizedProvince);
        }

        public void DeleteCenterAuthorizedProvince(CenterAuthorizedProvince centerAuthorizedProvince) {
            new CenterAuthorizedProvinceAch(this).DeleteCenterAuthorizedProvince(centerAuthorizedProvince);
        }

        public IQueryable<CenterAuthorizedProvince> getCenterAuthorizedProvincesForAgency(int agencyId) {
            return new CenterAuthorizedProvinceAch(this).getCenterAuthorizedProvincesForAgency(agencyId);
        }
    }
}
