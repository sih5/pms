﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class CenterSaleReturnStrategyAch : DcsSerivceAchieveBase {
        public CenterSaleReturnStrategyAch(DcsDomainService domainService)
            : base(domainService) {
        }

        public void Insert(CenterSaleReturnStrategy centerSaleReturnStrategy) {
        }
        public void Update(CenterSaleReturnStrategy centerSaleReturnStrategy) {
        }

        public void InsertCenterSaleReturnStrategy(CenterSaleReturnStrategy centerSaleReturnStrategy) {
            var user = Utils.GetCurrentUserInfo();
            centerSaleReturnStrategy.Status = (int)DcsBaseDataStatus.有效;
            centerSaleReturnStrategy.CreatorId = user.Id;
            centerSaleReturnStrategy.CreatorName = user.Name;
            centerSaleReturnStrategy.CreateTime = DateTime.Now;
            var centerSaleReturnStrategies = ObjectContext.CenterSaleReturnStrategies.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.ReturnType == centerSaleReturnStrategy.ReturnType).ToArray();
            if (centerSaleReturnStrategies.Any()) {
                var strategies = centerSaleReturnStrategies.Where(r => (centerSaleReturnStrategy.TimeFrom >= r.TimeFrom && centerSaleReturnStrategy.TimeFrom < r.TimeTo) || (centerSaleReturnStrategy.TimeTo > r.TimeFrom && centerSaleReturnStrategy.TimeTo < r.TimeTo) || (centerSaleReturnStrategy.TimeFrom <= r.TimeFrom && centerSaleReturnStrategy.TimeTo >= r.TimeTo));
                if(strategies != null && strategies.Count() != 0) {
                    throw new ValidationException(ErrorStrings.CenterSaleReturnStrategy_Validation1);
                }
            }
            InsertToDatabase(centerSaleReturnStrategy);
        }

        public void UpdateCenterSaleReturnStrategy(CenterSaleReturnStrategy centerSaleReturnStrategy) {
            var user = Utils.GetCurrentUserInfo();
            centerSaleReturnStrategy.ModifierId = user.Id;
            centerSaleReturnStrategy.ModifierName = user.Name;
            centerSaleReturnStrategy.ModifyTime = DateTime.Now;
            var centerSaleReturnStrategies = ObjectContext.CenterSaleReturnStrategies.Where(r => centerSaleReturnStrategy.Id != r.Id && r.Status == (int)DcsBaseDataStatus.有效 && r.ReturnType == centerSaleReturnStrategy.ReturnType).ToArray();
            if (centerSaleReturnStrategies.Any()) {
                var strategies = centerSaleReturnStrategies.Where(r => (centerSaleReturnStrategy.TimeFrom >= r.TimeFrom && centerSaleReturnStrategy.TimeFrom < r.TimeTo) || (centerSaleReturnStrategy.TimeTo > r.TimeFrom && centerSaleReturnStrategy.TimeTo < r.TimeTo) || (centerSaleReturnStrategy.TimeFrom <= r.TimeFrom && centerSaleReturnStrategy.TimeTo >= r.TimeTo));
                if (strategies != null && strategies.Count() != 0) {
                    throw new ValidationException(ErrorStrings.CenterSaleReturnStrategy_Validation1);
                }
            }
            UpdateToDatabase(centerSaleReturnStrategy);
        }

        public void AbandonCenterSaleReturnStrategy(CenterSaleReturnStrategy centerSaleReturnStrategy) {
            var user = Utils.GetCurrentUserInfo();

            var dbcenterSaleReturnStrategy = ObjectContext.CenterSaleReturnStrategies.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.Id == centerSaleReturnStrategy.Id).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbcenterSaleReturnStrategy == null)
                throw new ValidationException(ErrorStrings.CenterSaleReturnStrategy_Validation2);
            centerSaleReturnStrategy.Status = (int)DcsBaseDataStatus.作废;
            centerSaleReturnStrategy.ModifierId = user.Id;
            centerSaleReturnStrategy.ModifierName = user.Name;
            centerSaleReturnStrategy.ModifyTime = DateTime.Now;
            UpdateToDatabase(centerSaleReturnStrategy);
        }
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:09:49
        //原分布类的函数全部转移到Ach                                                               
        public void Insert(CenterSaleReturnStrategy centerSaleReturnStrategy) {
            new CenterSaleReturnStrategyAch(this).Insert(centerSaleReturnStrategy);
        }
        public void Update(CenterSaleReturnStrategy centerSaleReturnStrategy) {
            new CenterSaleReturnStrategyAch(this).Update(centerSaleReturnStrategy);
        }
        [Update(UsingCustomMethod = true)]
        public void InsertCenterSaleReturnStrategy(CenterSaleReturnStrategy centerSaleReturnStrategy) {
             new CenterSaleReturnStrategyAch(this).InsertCenterSaleReturnStrategy(centerSaleReturnStrategy);
        }

        [Update(UsingCustomMethod = true)]
        public void UpdateCenterSaleReturnStrategy(CenterSaleReturnStrategy centerSaleReturnStrategy) {
            new CenterSaleReturnStrategyAch(this).UpdateCenterSaleReturnStrategy(centerSaleReturnStrategy);
        }
        [Update(UsingCustomMethod = true)]

        public void AbandonCenterSaleReturnStrategy(CenterSaleReturnStrategy centerSaleReturnStrategy) {
            new CenterSaleReturnStrategyAch(this).AbandonCenterSaleReturnStrategy(centerSaleReturnStrategy);
        }
    }
}
