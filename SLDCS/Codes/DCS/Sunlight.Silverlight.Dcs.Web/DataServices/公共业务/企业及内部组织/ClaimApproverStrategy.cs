﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class ClaimApproverStrategyAch : DcsSerivceAchieveBase {
        public ClaimApproverStrategyAch(DcsDomainService domainService)
            : base(domainService) {
        }

        public IQueryable<ClaimApproverStrategy> 查询三包索赔单审批策略() {
            return ObjectContext.ClaimApproverStrategies.Where(r => r.Id != default(int)).OrderBy(r =>r.FeeTo);
        }

        public ClaimApproverStrategy getClaimApproverStrategyById(int id)
        {
            return ObjectContext.ClaimApproverStrategies.Where(r => r.Id == id).SingleOrDefault();
        }

        public void Insert(ClaimApproverStrategy claimApproverStrategy) {
        }
        public void Update(ClaimApproverStrategy claimApproverStrategy) {
        }

        public void InsertClaimApproverStrategy(ClaimApproverStrategy claimApproverStrategy) {
            var user = Utils.GetCurrentUserInfo();
           
            claimApproverStrategy.Branchid = user.EnterpriseId;
            claimApproverStrategy.BranchCode = user.EnterpriseCode;
            claimApproverStrategy.BranchName = user.EnterpriseName;
            claimApproverStrategy.CreatorId = user.Id;
            claimApproverStrategy.CreatorName = user.Name;
            claimApproverStrategy.CreateTime = DateTime.Now;

            //校验金额区间是否重叠
            var claimApproverStrategies = ObjectContext.ClaimApproverStrategies.Where(r => r.Branchid == claimApproverStrategy.Branchid);
            if(claimApproverStrategies != null && claimApproverStrategies.Count() != 0) {
                var strategies = claimApproverStrategies.Where(r => (claimApproverStrategy.FeeFrom >= r.FeeFrom && claimApproverStrategy.FeeFrom < r.FeeTo) || (claimApproverStrategy.FeeTo > r.FeeFrom && claimApproverStrategy.FeeTo < r.FeeTo) || (claimApproverStrategy.FeeFrom <= r.FeeFrom && claimApproverStrategy.FeeTo >= r.FeeTo));
                if(strategies != null && strategies.Count() != 0) {
                    throw new ValidationException(ErrorStrings.ClaimApproverStrategy_Validation1);
                }
            }
            InsertToDatabase(claimApproverStrategy);
        }

        public void UpdateClaimApproverStrategy(ClaimApproverStrategy claimApproverStrategy) {
            var user = Utils.GetCurrentUserInfo();
            claimApproverStrategy.ModifierId = user.Id;
            claimApproverStrategy.ModifierName = user.Name;
            claimApproverStrategy.ModifyTime = DateTime.Now;
            //校验金额区间是否重叠
            var claimApproverStrategies = ObjectContext.ClaimApproverStrategies.Where(r => r.Branchid == claimApproverStrategy.Branchid);
            if(claimApproverStrategies != null && claimApproverStrategies.Count() != 0) {
                var strategies = claimApproverStrategies.Where(r => claimApproverStrategy.Id != r.Id && ((claimApproverStrategy.FeeFrom >= r.FeeFrom && claimApproverStrategy.FeeFrom < r.FeeTo) || (claimApproverStrategy.FeeTo > r.FeeFrom && claimApproverStrategy.FeeTo < r.FeeTo) || (claimApproverStrategy.FeeFrom <= r.FeeFrom && claimApproverStrategy.FeeTo >= r.FeeTo)));
                if(strategies != null && strategies.Count() != 0) {
                    throw new ValidationException(ErrorStrings.ClaimApproverStrategy_Validation1);
                }
            }
            UpdateToDatabase(claimApproverStrategy);
        }

    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:09:49
        //原分布类的函数全部转移到Ach                                                               
        public IQueryable<ClaimApproverStrategy> 查询三包索赔单审批策略()
        {
            return new ClaimApproverStrategyAch(this).查询三包索赔单审批策略();
        }

        public ClaimApproverStrategy getClaimApproverStrategyById(int id) {
            return new ClaimApproverStrategyAch(this).getClaimApproverStrategyById(id);
        }
        public void Insert(ClaimApproverStrategy claimApproverStrategy) {
            new ClaimApproverStrategyAch(this).Insert(claimApproverStrategy);
        }
        public void Update(ClaimApproverStrategy claimApproverStrategy) {
            new ClaimApproverStrategyAch(this).Update(claimApproverStrategy);
        }
        [Update(UsingCustomMethod = true)]
        public void InsertClaimApproverStrategy(ClaimApproverStrategy claimApproverStrategy) {
             new ClaimApproverStrategyAch(this).InsertClaimApproverStrategy(claimApproverStrategy);
        }

        [Update(UsingCustomMethod = true)]
        public void UpdateClaimApproverStrategy(ClaimApproverStrategy claimApproverStrategy) {
            new ClaimApproverStrategyAch(this).UpdateClaimApproverStrategy(claimApproverStrategy);
        }
    }
}
