﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class CompanyAch : DcsSerivceAchieveBase {
        public CompanyAch(DcsDomainService domainService)
            : base(domainService) {
        }

        internal void InsertCompanyValidate(Company company) {
            var companyCode = ObjectContext.Companies.Where(r => r.Code.ToLower() == company.Code.ToLower()).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(companyCode != null)
                throw new ValidationException(string.Format(ErrorStrings.Company_Validation1, company.Code));
            //var companyName = ObjectContext.Companies.Where(r => r.Name.ToLower() == company.Name.ToLower()).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            //if(companyName != null)
            //    throw new ValidationException(string.Format(ErrorStrings.Company_Validation2, company.Name));
            var userInfo = Utils.GetCurrentUserInfo();
            company.CreatorId = userInfo.Id;
            company.CreatorName = userInfo.Name;
            company.CreateTime = DateTime.Now;
        }
        internal void UpdateCompanyValidate(Company company) {
            var companyCode = ObjectContext.Companies.Where(r => r.Id != company.Id && r.Code.ToLower() == company.Code.ToLower()).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(companyCode != null)
                throw new ValidationException(string.Format(ErrorStrings.Company_Validation1, company.Code));
            //var companyName = ObjectContext.Companies.Where(r => r.Id != company.Id && r.Name.ToLower() == company.Name.ToLower()).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            //if(companyName != null)
            //    throw new ValidationException(string.Format(ErrorStrings.Company_Validation2, company.Name));
            var userInfo = Utils.GetCurrentUserInfo();
            company.ModifierId = userInfo.Id;
            company.ModifierName = userInfo.Name;
            company.ModifyTime = DateTime.Now;
        }
        public void InsertCompany(Company company) {
            InsertToDatabase(company);
            this.InsertCompanyValidate(company);
        }
        public void UpdateCompany(Company company) {
            UpdateToDatabase(company);
            this.UpdateCompanyValidate(company);
        }
        public IQueryable<Company> GetCompaniesAndAgenciesByLoginCompany() {
            var userInfo = Utils.GetCurrentUserInfo();
            return ObjectContext.Companies.Where(r => (r.Id == userInfo.EnterpriseId || ObjectContext.AgencyAffiBranches.Any(v => r.Id == v.AgencyId && v.BranchId == userInfo.EnterpriseId)) && r.Status == (int)DcsMasterDataStatus.有效).OrderBy(r => r.Id);
        }
        public IQueryable<Company> GetCompanyWithDetails() {
            var userInfo = Utils.GetCurrentUserInfo();
            return ObjectContext.Companies.Where(r => r.Id == userInfo.EnterpriseId).Include("Dealer").Include("Dealer.DealerServiceInfoes").Include("Agency").Include("Agency.AgencyAffiBranches").Include("PartsSupplier").Include("PartsSupplier.BranchSupplierRelations").OrderBy(r => r.Id);
        }
        public IQueryable<Company> GetCompaniesNotContainedIn() {
            return ObjectContext.Companies.Where(r => !r.Name.Contains("集团企业")).OrderBy(r => r.Id);
        }
        public IQueryable<Company> GetCompaniesByType() {
            return ObjectContext.Companies.Where(r => r.Type == (int)DcsCompanyType.服务站 || r.Type == (int)DcsCompanyType.代理库 || r.Type == (int)DcsCompanyType.服务站兼代理库).OrderBy(r => r.Id);
        }

        public IQueryable<Company> GetCompanyWithNoSupplier() {
            return ObjectContext.Companies.Where(r => r.Type != (int)DcsCompanyType.出口客户 && r.Type != (int)DcsCompanyType.配件供应商).OrderBy(r => r.Id);
        }
        public IQueryable<Company> GetCompanyWithBranchIds() {
            var userInfo = Utils.GetCurrentUserInfo();
            return ObjectContext.Companies.Where(r => r.Id == userInfo.EnterpriseId).OrderBy(r => r.Id);
        }

        public IQueryable<Company> GetCompanyById(int id)
        {
            return ObjectContext.Companies.Where(r => r.Id == id);
        }

        public IQueryable<Company> GetCompaniesForApproveBill() {
            return ObjectContext.Companies.Where(r => r.Status != (int)DcsBaseDataStatus.作废).OrderBy(r => r.Id);
        }
        public IQueryable<Company> 查询可新增的智能企业() {
            return ObjectContext.Companies.Where(r => r.Status != (int)DcsBaseDataStatus.作废 && !ObjectContext.SmartCompanies.Any(t => t.CompanyId == r.Id && t.Status == (int)DcsBaseDataStatus.有效)).OrderBy(r => r.Id);
        }
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:29
        //原分布类的函数全部转移到Ach                                                               

        public void InsertCompany(Company company) {
            new CompanyAch(this).InsertCompany(company);
        }

        public void UpdateCompany(Company company) {
            new CompanyAch(this).UpdateCompany(company);
        }

        public IQueryable<Company> GetCompaniesAndAgenciesByLoginCompany() {
            return new CompanyAch(this).GetCompaniesAndAgenciesByLoginCompany();
        }

        public IQueryable<Company> GetCompanyWithDetails() {
            return new CompanyAch(this).GetCompanyWithDetails();
        }

        public IQueryable<Company> GetCompaniesNotContainedIn() {
            return new CompanyAch(this).GetCompaniesNotContainedIn();
        }

        public IQueryable<Company> GetCompaniesByType() {
            return new CompanyAch(this).GetCompaniesByType();
        }

        public IQueryable<Company> GetCompanyWithBranchIds() {
            return new CompanyAch(this).GetCompanyWithBranchIds();
        }

        public IQueryable<Company> GetCompanyWithNoSupplier() {
            return new CompanyAch(this).GetCompanyWithNoSupplier();
        }

        public IQueryable<Company> GetCompanyById(int id) {
            return new CompanyAch(this).GetCompanyById(id);
        }

        public IQueryable<Company> GetCompaniesForApproveBill() {
            return new CompanyAch(this).GetCompaniesForApproveBill();
        }
        public IQueryable<Company> 查询可新增的智能企业() {
            return new CompanyAch(this).查询可新增的智能企业();
        }
    }
}
