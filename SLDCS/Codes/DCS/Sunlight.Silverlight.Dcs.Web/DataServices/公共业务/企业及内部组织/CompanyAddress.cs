﻿using System;
using System.Linq;
using Sunlight.Silverlight.Web;


namespace Sunlight.Silverlight.Dcs.Web {
    partial class CompanyAddressAch : DcsSerivceAchieveBase {
        public CompanyAddressAch(DcsDomainService domainService)
            : base(domainService) {
        }

        internal void InsertCompanyAddressValidate(CompanyAddress companyAddress) {
            var userInfo = Utils.GetCurrentUserInfo();
            companyAddress.CreatorId = userInfo.Id;
            companyAddress.CreatorName = userInfo.Name;
            companyAddress.CreateTime = DateTime.Now;
        }

        internal void UpdateCompanyAddressValidate(CompanyAddress companyAddress) {
            var userInfo = Utils.GetCurrentUserInfo();
            companyAddress.ModifierId = userInfo.Id;
            companyAddress.ModifierName = userInfo.Name;
            companyAddress.ModifyTime = DateTime.Now;
        }

        public void InsertCompanyAddress(CompanyAddress companyAddress) {
            InsertToDatabase(companyAddress);
            this.InsertCompanyAddressValidate(companyAddress);
        }

        public void UpdateCompanyAddress(CompanyAddress companyAddress) {
            UpdateToDatabase(companyAddress);
            this.UpdateCompanyAddressValidate(companyAddress);
        }

        public IQueryable<CompanyAddress> GetCompanyAddressWithRegion() {
            return ObjectContext.CompanyAddresses.Include("TiledRegion").OrderBy(e => e.Id);

        }

        public IQueryable<CompanyAddress> GetCompanyAddressWithDetail() {
            return ObjectContext.CompanyAddresses.Include("TiledRegion").Include("Company").OrderBy(e => e.Id);

        }

        public CompanyAddress GetCompanyAddressById(int id) {
            return this.ObjectContext.CompanyAddresses.Include("Company").First(r => r.Id == id);
        }

        //加载本企业，和本企业下属企业的收货地址
        public IQueryable<CompanyAddress> GetCompanyAddressWithAgencyDealerRelations(int salesCategoryId,int companyId) {
            var agencyDealerRelation = this.ObjectContext.AgencyDealerRelations.Where(r => r.AgencyId == companyId && r.Status == (int)DcsBaseDataStatus.有效 && r.PartsSalesOrderTypeId == salesCategoryId);
            var result = from a in this.ObjectContext.CompanyAddresses.Where(r => r.Status == (int)DcsMasterDataStatus.有效)
                         join b in agencyDealerRelation on a.CompanyId equals b.DealerId
                         select new {
                             a
                         }; //下属企业
            var bResult = this.ObjectContext.CompanyAddresses.Where(r => r.Status == (int)DcsMasterDataStatus.有效 && r.CompanyId == companyId); //本企业的

            return result.Select(r => r.a).Union(bResult).OrderBy(e => e.Id);
        }
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:29
        //原分布类的函数全部转移到Ach                                                               

        public void InsertCompanyAddress(CompanyAddress companyAddress) {
            new CompanyAddressAch(this).InsertCompanyAddress(companyAddress);
        }

        public void UpdateCompanyAddress(CompanyAddress companyAddress) {
            new CompanyAddressAch(this).UpdateCompanyAddress(companyAddress);
        }

        public IQueryable<CompanyAddress> GetCompanyAddressWithRegion() {
            return new CompanyAddressAch(this).GetCompanyAddressWithRegion();
        }

        public IQueryable<CompanyAddress> GetCompanyAddressWithDetail() {
            return new CompanyAddressAch(this).GetCompanyAddressWithDetail();
        }

        public CompanyAddress GetCompanyAddressById(int id) {
            return new CompanyAddressAch(this).GetCompanyAddressById(id);
        }

        public IQueryable<CompanyAddress> GetCompanyAddressWithAgencyDealerRelations(int salesCategoryId, int companyId) {
            return new CompanyAddressAch(this).GetCompanyAddressWithAgencyDealerRelations(salesCategoryId, companyId);
        }
    }
}
