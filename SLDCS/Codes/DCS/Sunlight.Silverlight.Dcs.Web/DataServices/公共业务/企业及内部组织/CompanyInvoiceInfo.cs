﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class CompanyInvoiceInfoAch : DcsSerivceAchieveBase {
        internal void InsertCompanyInvoiceInfoValidate(CompanyInvoiceInfo companyInvoiceInfo) {
            if(companyInvoiceInfo.IsVehicleSalesInvoice) {
                var dbcompanyInvoiceInfo = ObjectContext.CompanyInvoiceInfoes.Where(r => r.IsVehicleSalesInvoice && r.InvoiceCompanyId == companyInvoiceInfo.InvoiceCompanyId && r.CompanyId == companyInvoiceInfo.CompanyId && r.Status != (int)DcsMasterDataStatus.作废).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
                if(dbcompanyInvoiceInfo != null)
                    throw new ValidationException(ErrorStrings.CompanyInvoiceInfo_Validation1);
            }
            if(companyInvoiceInfo.IsPartsSalesInvoice) {
                var dbcompanyInvoiceInfo = ObjectContext.CompanyInvoiceInfoes.Where(r => r.IsPartsSalesInvoice && r.InvoiceCompanyId == companyInvoiceInfo.InvoiceCompanyId && r.CompanyId == companyInvoiceInfo.CompanyId && r.Status != (int)DcsMasterDataStatus.作废).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
                if(dbcompanyInvoiceInfo != null)
                    throw new ValidationException(ErrorStrings.CompanyInvoiceInfo_Validation2);
            }
            var userInfo = Utils.GetCurrentUserInfo();
            companyInvoiceInfo.CreatorId = userInfo.Id;
            companyInvoiceInfo.CreatorName = userInfo.Name;
            companyInvoiceInfo.CreateTime = DateTime.Now;
        }
        internal void UpdateCompanyInvoiceInfoValidate(CompanyInvoiceInfo companyInvoiceInfo) {
            if(companyInvoiceInfo.IsVehicleSalesInvoice) {
                var dbcompanyInvoiceInfo = ObjectContext.CompanyInvoiceInfoes.Where(r => r.Id != companyInvoiceInfo.Id && r.IsVehicleSalesInvoice && r.InvoiceCompanyId == companyInvoiceInfo.InvoiceCompanyId && r.CompanyId == companyInvoiceInfo.CompanyId && r.Status != (int)DcsMasterDataStatus.作废).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
                if(dbcompanyInvoiceInfo != null)
                    throw new ValidationException(ErrorStrings.CompanyInvoiceInfo_Validation1);
            }
            if(companyInvoiceInfo.IsPartsSalesInvoice) {
                var dbcompanyInvoiceInfo = ObjectContext.CompanyInvoiceInfoes.Where(r => r.Id != companyInvoiceInfo.Id && r.IsPartsSalesInvoice && r.InvoiceCompanyId == companyInvoiceInfo.InvoiceCompanyId && r.CompanyId == companyInvoiceInfo.CompanyId && r.Status != (int)DcsMasterDataStatus.作废).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
                if(dbcompanyInvoiceInfo != null)
                    throw new ValidationException(ErrorStrings.CompanyInvoiceInfo_Validation2);
            }
            var userInfo = Utils.GetCurrentUserInfo();
            companyInvoiceInfo.ModifierId = userInfo.Id;
            companyInvoiceInfo.ModifierName = userInfo.Name;
            companyInvoiceInfo.ModifyTime = DateTime.Now;
        }
        public void InsertCompanyInvoiceInfo(CompanyInvoiceInfo companyInvoiceInfo) {
            InsertToDatabase(companyInvoiceInfo);
            this.InsertCompanyInvoiceInfoValidate(companyInvoiceInfo);
        }
        public void UpdateCompanyInvoiceInfo(CompanyInvoiceInfo companyInvoiceInfo) {
            UpdateToDatabase(companyInvoiceInfo);
            this.UpdateCompanyInvoiceInfoValidate(companyInvoiceInfo);
        }
        public void DeleteCompanyInvoiceInfo(CompanyInvoiceInfo companyInvoiceInfo) {
            DeleteFromDatabase(companyInvoiceInfo);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:29
        //原分布类的函数全部转移到Ach                                                               

        public void InsertCompanyInvoiceInfo(CompanyInvoiceInfo companyInvoiceInfo) {
            new CompanyInvoiceInfoAch(this).InsertCompanyInvoiceInfo(companyInvoiceInfo);
        }

        public void UpdateCompanyInvoiceInfo(CompanyInvoiceInfo companyInvoiceInfo) {
            new CompanyInvoiceInfoAch(this).UpdateCompanyInvoiceInfo(companyInvoiceInfo);
        }

        public void DeleteCompanyInvoiceInfo(CompanyInvoiceInfo companyInvoiceInfo) {
            new CompanyInvoiceInfoAch(this).DeleteCompanyInvoiceInfo(companyInvoiceInfo);
        }
    }
}