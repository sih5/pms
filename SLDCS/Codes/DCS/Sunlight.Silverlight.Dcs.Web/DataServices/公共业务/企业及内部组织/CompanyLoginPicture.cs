﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class CompanyLoginPictureAch : DcsSerivceAchieveBase {
        public CompanyLoginPictureAch(DcsDomainService domainService)
            : base(domainService) {
        }

        internal void InsertCompanyLoginPictureValidate(CompanyLoginPicture companyLoginPicture) {
            var dbCompanyLoginPicture = ObjectContext.CompanyLoginPictures.Where(r => r.CompanyId == companyLoginPicture.CompanyId).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbCompanyLoginPicture != null)
                throw new ValidationException(ErrorStrings.CompanyLoginPicture_Validation1);
            var userInfo = Utils.GetCurrentUserInfo();
            companyLoginPicture.CreatorId = userInfo.Id;
            companyLoginPicture.CreatorName = userInfo.Name;
            companyLoginPicture.CreateTime = DateTime.Now;
        }
        internal void UpdateCompanyLoginPictureValidate(CompanyLoginPicture companyLoginPicture) {
            var userInfo = Utils.GetCurrentUserInfo();
            companyLoginPicture.ModifierId = userInfo.Id;
            companyLoginPicture.ModifierName = userInfo.Name;
            companyLoginPicture.ModifyTime = DateTime.Now;
        }
        public void InsertCompanyLoginPicture(CompanyLoginPicture companyLoginPicture) {
            InsertToDatabase(companyLoginPicture);
            this.InsertCompanyLoginPictureValidate(companyLoginPicture);
        }
        public void UpdateCompanyLoginPicture(CompanyLoginPicture companyLoginPicture) {
            UpdateToDatabase(companyLoginPicture);
            this.UpdateCompanyLoginPictureValidate(companyLoginPicture);
        }
        public IQueryable<CompanyLoginPicture> GetCompanyLoginPictureWithCompany() {
            return ObjectContext.CompanyLoginPictures.Include("Company").OrderBy(v => v.Id);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:29
        //原分布类的函数全部转移到Ach                                                               

        public void InsertCompanyLoginPicture(CompanyLoginPicture companyLoginPicture) {
            new CompanyLoginPictureAch(this).InsertCompanyLoginPicture(companyLoginPicture);
        }

        public void UpdateCompanyLoginPicture(CompanyLoginPicture companyLoginPicture) {
            new CompanyLoginPictureAch(this).UpdateCompanyLoginPicture(companyLoginPicture);
        }

        public IQueryable<CompanyLoginPicture> GetCompanyLoginPictureWithCompany() {
            return new CompanyLoginPictureAch(this).GetCompanyLoginPictureWithCompany();
        }
    }
}