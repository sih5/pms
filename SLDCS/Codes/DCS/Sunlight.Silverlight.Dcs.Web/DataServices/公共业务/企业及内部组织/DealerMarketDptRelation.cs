﻿using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class DealerMarketDptRelationAch : DcsSerivceAchieveBase {
        internal void InsertDealerMarketDptRelationValidate(DealerMarketDptRelation dealerMarketDptRelation) {
            dealerMarketDptRelation.Status = (int)DcsBaseDataStatus.有效;
        }
        internal void UpdateDealerMarketDptRelationValidate(DealerMarketDptRelation dealerMarketDptRelation) {
            var dbdealerMarketDptRelation = ObjectContext.DealerMarketDptRelations.Where(r => r.Id == dealerMarketDptRelation.Id && r.BranchId == dealerMarketDptRelation.BranchId && r.MarketId == dealerMarketDptRelation.MarketId && r.DealerId == dealerMarketDptRelation.DealerId).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbdealerMarketDptRelation == null)
                throw new ValidationException(ErrorStrings.DealerMarketDptRelation_Validation1);
        }
        public void InsertDealerMarketDptRelation(DealerMarketDptRelation dealerMarketDptRelation) {
            InsertToDatabase(dealerMarketDptRelation);
            this.InsertDealerMarketDptRelationValidate(dealerMarketDptRelation);
        }
        public void UpdateDealerMarketDptRelation(DealerMarketDptRelation dealerMarketDptRelation) {
            UpdateToDatabase(dealerMarketDptRelation);
            this.UpdateDealerMarketDptRelationValidate(dealerMarketDptRelation);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:29
        //原分布类的函数全部转移到Ach                                                               

        public void InsertDealerMarketDptRelation(DealerMarketDptRelation dealerMarketDptRelation) {
            new DealerMarketDptRelationAch(this).InsertDealerMarketDptRelation(dealerMarketDptRelation);
        }

        public void UpdateDealerMarketDptRelation(DealerMarketDptRelation dealerMarketDptRelation) {
            new DealerMarketDptRelationAch(this).UpdateDealerMarketDptRelation(dealerMarketDptRelation);
        }
    }
}
