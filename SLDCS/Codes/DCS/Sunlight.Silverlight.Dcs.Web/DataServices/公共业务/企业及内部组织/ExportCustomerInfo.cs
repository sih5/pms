﻿using System;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class ExportCustomerInfoAch : DcsSerivceAchieveBase {
        public ExportCustomerInfoAch(DcsDomainService domainService)
            : base(domainService) {
        }

        internal void InsertExportCustomerInfoValidate(ExportCustomerInfo exportCustomerInfo) {
            var userInfo = Utils.GetCurrentUserInfo();
            exportCustomerInfo.CreatorId = userInfo.Id;
            exportCustomerInfo.CreatorName = userInfo.Name;
            exportCustomerInfo.CreateTime = DateTime.Now;
        }

        internal void UpdateExportCustomerInfoValidate(ExportCustomerInfo exportCustomerInfo) {
            var userInfo = Utils.GetCurrentUserInfo();
            exportCustomerInfo.ModifierId = userInfo.Id;
            exportCustomerInfo.ModifierName = userInfo.Name;
            exportCustomerInfo.ModifyTime = DateTime.Now;
        }

        public void InsertExportCustomerInfo(ExportCustomerInfo exportCustomerInfo) {
            InsertToDatabase(exportCustomerInfo);
            this.InsertExportCustomerInfoValidate(exportCustomerInfo);
        }

        public void UpdateExportCustomerInfo(ExportCustomerInfo exportCustomerInfo) {
            UpdateToDatabase(exportCustomerInfo);
            this.UpdateExportCustomerInfoValidate(exportCustomerInfo);
        }
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:29
        //原分布类的函数全部转移到Ach                                                               

        public void InsertExportCustomerInfo(ExportCustomerInfo exportCustomerInfo) {
            new ExportCustomerInfoAch(this).InsertExportCustomerInfo(exportCustomerInfo);
        }

        public void UpdateBranch(ExportCustomerInfo exportCustomerInfo) {
            new ExportCustomerInfoAch(this).UpdateExportCustomerInfo(exportCustomerInfo);
        }

    }
}
