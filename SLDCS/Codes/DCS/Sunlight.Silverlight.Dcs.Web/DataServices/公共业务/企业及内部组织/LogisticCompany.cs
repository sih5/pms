﻿using System;
using System.Linq;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class LogisticCompanyAch : DcsSerivceAchieveBase {
        internal void InsertLogisticCompanyValidate(LogisticCompany logisticCompany) {
            var userInfo = Utils.GetCurrentUserInfo();
            logisticCompany.CreatorId = userInfo.Id;
            logisticCompany.CreatorName = userInfo.Name;
            logisticCompany.CreateTime = DateTime.Now;
        }

        internal void UpdateLogisticCompanyValidate(LogisticCompany logisticCompany) {
            var userInfo = Utils.GetCurrentUserInfo();
            logisticCompany.ModifierId = userInfo.Id;
            logisticCompany.ModifierName = userInfo.Name;
            logisticCompany.ModifyTime = DateTime.Now;
        }

        public void InsertLogisticCompany(LogisticCompany logisticCompany) {
            InsertToDatabase(logisticCompany);
            this.InsertLogisticCompanyValidate(logisticCompany);
        }

        public void UpdateLogisticCompany(LogisticCompany logisticCompany) {
            UpdateToDatabase(logisticCompany);
            this.UpdateLogisticCompanyValidate(logisticCompany);
        }

        public LogisticCompany GetLogisticCompanyWithDetailsById(int id) {
            var dbLogisticCompany = ObjectContext.LogisticCompanies.SingleOrDefault(e => e.Id == id && e.Status == (int)DcsMasterDataStatus.有效);
            if(dbLogisticCompany != null) {
                var ranges = ObjectContext.LogisticCompanyServiceRanges.Where(e => e.LogisticCompanyId == dbLogisticCompany.Id).ToArray();
                foreach (var range in ranges) {
                    dbLogisticCompany.LogisticCompanyServiceRanges.Add(range);
                }
                var company = ObjectContext.Companies.SingleOrDefault(e => e.Id == dbLogisticCompany.Id && e.Status == (int)DcsMasterDataStatus.有效);
                dbLogisticCompany.Company = company;
            }
            return dbLogisticCompany;
        }
    
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:29
        //原分布类的函数全部转移到Ach                                                               

        public void InsertLogisticCompany(LogisticCompany logisticCompany) {
            new LogisticCompanyAch(this).InsertLogisticCompany(logisticCompany);
        }

        public void UpdateLogisticCompany(LogisticCompany logisticCompany) {
            new LogisticCompanyAch(this).UpdateLogisticCompany(logisticCompany);
        }

        public LogisticCompany GetLogisticCompanyWithDetailsById(int id) {
            return new LogisticCompanyAch(this).GetLogisticCompanyWithDetailsById(id);
        }
    }
}
