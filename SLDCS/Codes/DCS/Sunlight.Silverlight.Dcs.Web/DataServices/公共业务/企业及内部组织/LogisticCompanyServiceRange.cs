﻿using System.Linq;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class LogisticCompanyServiceRangeAch : DcsSerivceAchieveBase {
        public LogisticCompanyServiceRangeAch(DcsDomainService domainService)
            : base(domainService) {
        }

        internal void InsertLogisticCompanyServiceRangeValidate(LogisticCompanyServiceRange logisticCompanyServiceRange) {
        }
        internal void UpdateLogisticCompanyServiceRangeValidate(LogisticCompanyServiceRange logisticCompanyServiceRange) {
        }
        public void InsertLogisticCompanyServiceRange(LogisticCompanyServiceRange logisticCompanyServiceRange) {
            InsertToDatabase(logisticCompanyServiceRange);
            this.InsertLogisticCompanyServiceRangeValidate(logisticCompanyServiceRange);
        }
        public void UpdateLogisticCompanyServiceRange(LogisticCompanyServiceRange logisticCompanyServiceRange) {
            UpdateToDatabase(logisticCompanyServiceRange);
            this.UpdateLogisticCompanyServiceRangeValidate(logisticCompanyServiceRange);
        }
        public void DeleteLogisticCompanyServiceRange(LogisticCompanyServiceRange logisticCompanyServiceRange) {
            DeleteFromDatabase(logisticCompanyServiceRange);
        }
        public IQueryable<LogisticCompanyServiceRange> GetLogisticCompanyServiceRangesWithLogisticCompany() {
            return ObjectContext.LogisticCompanyServiceRanges.Include("LogisticCompany").OrderBy(e => e.Id);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:29
        //原分布类的函数全部转移到Ach                                                               

        public void InsertLogisticCompanyServiceRange(LogisticCompanyServiceRange logisticCompanyServiceRange) {
            new LogisticCompanyServiceRangeAch(this).InsertLogisticCompanyServiceRange(logisticCompanyServiceRange);
        }

        public void UpdateLogisticCompanyServiceRange(LogisticCompanyServiceRange logisticCompanyServiceRange) {
            new LogisticCompanyServiceRangeAch(this).UpdateLogisticCompanyServiceRange(logisticCompanyServiceRange);
        }

        public void DeleteLogisticCompanyServiceRange(LogisticCompanyServiceRange logisticCompanyServiceRange) {
            new LogisticCompanyServiceRangeAch(this).DeleteLogisticCompanyServiceRange(logisticCompanyServiceRange);
        }

        public IQueryable<LogisticCompanyServiceRange> GetLogisticCompanyServiceRangesWithLogisticCompany() {
            return new LogisticCompanyServiceRangeAch(this).GetLogisticCompanyServiceRangesWithLogisticCompany();
        }
    }
}
