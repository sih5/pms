﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class MarketDptPersonnelRelationAch : DcsSerivceAchieveBase {
        internal void InsertMarketDptPersonnelRelationValidate(MarketDptPersonnelRelation marketDptPersonnelRelation) {
            var dbMarketDptPersonnelRelation = ObjectContext.MarketDptPersonnelRelations.Where(r => r.BranchId == marketDptPersonnelRelation.BranchId && r.MarketDepartmentId == marketDptPersonnelRelation.MarketDepartmentId && r.PersonnelId == marketDptPersonnelRelation.PersonnelId && r.Status != (int)DcsBaseDataStatus.作废).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbMarketDptPersonnelRelation != null)
                throw new ValidationException(ErrorStrings.MarketDptPersonnelRelation_Validation1);
            var userInfo = Utils.GetCurrentUserInfo();
            marketDptPersonnelRelation.CreatorId = userInfo.Id;
            marketDptPersonnelRelation.CreatorName = userInfo.Name;
            marketDptPersonnelRelation.CreateTime = DateTime.Now;
        }
        internal void UpdateMarketDptPersonnelRelationValidate(MarketDptPersonnelRelation marketDptPersonnelRelation) {
            var dbMarketDptPersonnelRelation = ObjectContext.MarketDptPersonnelRelations.Where(r => r.Id != marketDptPersonnelRelation.Id && r.BranchId == marketDptPersonnelRelation.BranchId && r.MarketDepartmentId == marketDptPersonnelRelation.MarketDepartmentId && r.PersonnelId == marketDptPersonnelRelation.PersonnelId && r.Status != (int)DcsBaseDataStatus.作废).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbMarketDptPersonnelRelation != null)
                throw new ValidationException(ErrorStrings.MarketDptPersonnelRelation_Validation2);
            var userInfo = Utils.GetCurrentUserInfo();
            marketDptPersonnelRelation.ModifierId = userInfo.Id;
            marketDptPersonnelRelation.ModifierName = userInfo.Name;
            marketDptPersonnelRelation.ModifyTime = DateTime.Now;
        }
        public void InsertMarketDptPersonnelRelation(MarketDptPersonnelRelation marketDptPersonnelRelation) {
            InsertToDatabase(marketDptPersonnelRelation);
            this.InsertMarketDptPersonnelRelationValidate(marketDptPersonnelRelation);
        }
        public void UpdateMarketDptPersonnelRelation(MarketDptPersonnelRelation marketDptPersonnelRelation) {
            UpdateToDatabase(marketDptPersonnelRelation);
            this.UpdateMarketDptPersonnelRelationValidate(marketDptPersonnelRelation);
        }
        public IQueryable<MarketDptPersonnelRelation> GetMarketDptPersonnelRelationDetail() {
            var userInfo = Utils.GetCurrentUserInfo();
            var companyType = ObjectContext.Companies.First(r => r.Id == userInfo.EnterpriseId).Type;
            var query = from a in this.ObjectContext.MarketDptPersonnelRelations
                        select a;
            if(companyType == (int)DcsCompanyType.分公司) {
                query = from a in query
                        join b in ObjectContext.MarketingDepartments on a.MarketDepartmentId equals b.Id
                        join d in ObjectContext.PersonSalesCenterLinks.Where(r => r.PersonId == userInfo.Id) on b.PartsSalesCategoryId equals d.PartsSalesCategoryId
                        select a;
            }
            return query.Include("Personnel").Include("MarketingDepartment").OrderBy(entity => entity.Id);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:29
        //原分布类的函数全部转移到Ach                                                               

        public void InsertMarketDptPersonnelRelation(MarketDptPersonnelRelation marketDptPersonnelRelation) {
            new MarketDptPersonnelRelationAch(this).InsertMarketDptPersonnelRelation(marketDptPersonnelRelation);
        }

        public void UpdateMarketDptPersonnelRelation(MarketDptPersonnelRelation marketDptPersonnelRelation) {
            new MarketDptPersonnelRelationAch(this).UpdateMarketDptPersonnelRelation(marketDptPersonnelRelation);
        }

        public IQueryable<MarketDptPersonnelRelation> GetMarketDptPersonnelRelationDetail() {
            return new MarketDptPersonnelRelationAch(this).GetMarketDptPersonnelRelationDetail();
        }
    }
}
