﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class MarketingDepartmentAch : DcsSerivceAchieveBase {
        internal void InsertMarketingDepartmentValidate(MarketingDepartment marketingDepartment) {
            var dbmarketingDepartment = ObjectContext.MarketingDepartments.Where(r => r.BranchId == marketingDepartment.BranchId && r.PartsSalesCategoryId == marketingDepartment.PartsSalesCategoryId && r.Code.ToLower() == marketingDepartment.Code.ToLower() && r.BusinessType == marketingDepartment.BusinessType && r.Status != (int)DcsBaseDataStatus.作废).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbmarketingDepartment != null)
                throw new ValidationException(string.Format(ErrorStrings.MarketingDepartment_Validation1, marketingDepartment.Code, Enum.GetName(typeof(DcsMarketingDepartmentType), marketingDepartment.BusinessType)));

            var userInfo = Utils.GetCurrentUserInfo();
            marketingDepartment.CreatorId = userInfo.Id;
            marketingDepartment.CreatorName = userInfo.Name;
            marketingDepartment.CreateTime = DateTime.Now;
        }

        internal void UpdateMarketingDepartmentValidate(MarketingDepartment marketingDepartment) {
            var dbmarketingDepartment = ObjectContext.MarketingDepartments.Where(r => r.Id != marketingDepartment.Id && r.BranchId == marketingDepartment.BranchId && r.PartsSalesCategoryId == marketingDepartment.PartsSalesCategoryId && r.Code.ToLower() == marketingDepartment.Code.ToLower() && r.BusinessType == marketingDepartment.BusinessType && r.Status != (int)DcsBaseDataStatus.作废).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbmarketingDepartment != null)
                throw new ValidationException(string.Format(ErrorStrings.MarketingDepartment_Validation1, marketingDepartment.Code, Enum.GetName(typeof(DcsMarketingDepartmentType), marketingDepartment.BusinessType)));
            var dbmarketingDepartments = ObjectContext.MarketingDepartments.Where(r => r.Id == marketingDepartment.Id && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbmarketingDepartments == null)
                throw new ValidationException(ErrorStrings.MarketingDepartment_Validation3);

            var userInfo = Utils.GetCurrentUserInfo();
            marketingDepartment.ModifierId = userInfo.Id;
            marketingDepartment.ModifierName = userInfo.Name;
            marketingDepartment.ModifyTime = DateTime.Now;
        }

        public void InsertMarketingDepartment(MarketingDepartment marketingDepartment) {
            InsertToDatabase(marketingDepartment);
            this.InsertMarketingDepartmentValidate(marketingDepartment);
        }

        public void UpdateMarketingDepartment(MarketingDepartment marketingDepartment) {
            UpdateToDatabase(marketingDepartment);
            this.UpdateMarketingDepartmentValidate(marketingDepartment);
        }

        public IQueryable<MarketingDepartment> GetMarketingDepartmentsWithDetails() {
            return ObjectContext.MarketingDepartments.Include("PartsSalesCategory").Include("TiledRegion").Include("Branch").OrderBy(entity => entity.Id);
        }

        public IQueryable<MarketingDepartment> GetMarketingDepartmentsForCreditInfo() {
            var user = Utils.GetCurrentUserInfo();
            var company = ObjectContext.Companies.Where(t=>t.Id==user.EnterpriseId).First();
            var marketDptPersonnelRelations = ObjectContext.MarketDptPersonnelRelations.Where(o => o.PersonnelId == user.Id && o.Status == (int)DcsBaseDataStatus.有效).ToList();
            var marketDepartmentIds = marketDptPersonnelRelations.Select(o => o.MarketDepartmentId).Distinct().ToList();
            if(company.Type==(int)DcsCompanyType.分公司) {
                if(marketDptPersonnelRelations.Any())
                    return ObjectContext.MarketingDepartments.Include("PartsSalesCategory").Include("TiledRegion").Include("Branch")
                        .Where(o => marketDepartmentIds.Contains(o.Id)).OrderBy(entity => entity.Id);
                else
                    return ObjectContext.MarketingDepartments.Include("PartsSalesCategory").Include("TiledRegion").Include("Branch").OrderBy(entity => entity.Id);
            } else {
                return ObjectContext.MarketingDepartments.Where(t=>ObjectContext.Agencies.Any(e=>e.MarketingDepartmentId==t.Id && e.Id==company.Id)).Include("PartsSalesCategory").Include("TiledRegion").Include("Branch").OrderBy(entity => entity.Id);
            }
           
        }

        public MarketingDepartment GetMarketingDepartmentsWithRelationsAndDetails(int departmentId, int branchId) {
            var result = ObjectContext.MarketingDepartments.SingleOrDefault(e => e.Id == departmentId && e.BranchId == branchId && e.Status == (int)DcsBaseDataStatus.有效);
            if(result != null) {
                var tiledRegion = ObjectContext.TiledRegions.SingleOrDefault(r => r.Id == result.RegionId);
                var branch = ObjectContext.Branches.SingleOrDefault(r => r.Id == result.BranchId && r.Status == (int)DcsMasterDataStatus.有效);
                var relations = ObjectContext.DealerMarketDptRelations.Where(e => e.Status == (int)DcsBaseDataStatus.有效 && e.MarketId == departmentId && e.BranchId == branchId).ToArray();
                if(relations.Length > 0) {
                    var dealerIds = relations.Select(r => r.DealerId).ToArray();
                    var dealers = ObjectContext.Dealers.Where(r => dealerIds.Contains(r.Id) && r.Status == (int)DcsBaseDataStatus.有效).ToArray();
                }
            }
            return result;
        }
        public IQueryable<MarketingDepartment> GetMarketingDepartmentByPersonel() {
            var userinfo = Utils.GetCurrentUserInfo();
            var marketDepartmentId = ObjectContext.MarketDptPersonnelRelations.Where(ex => ex.PersonnelId == userinfo.EnterpriseId).Select(ex => ex.MarketDepartmentId);
            return ObjectContext.MarketingDepartments.Where(ex => marketDepartmentId.Contains(ex.Id) && ex.Status == (int)DcsBaseDataStatus.有效).OrderBy(v => v.Id);
        }

    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:29
        //原分布类的函数全部转移到Ach                                                               

        public void InsertMarketingDepartment(MarketingDepartment marketingDepartment) {
            new MarketingDepartmentAch(this).InsertMarketingDepartment(marketingDepartment);
        }

        public void UpdateMarketingDepartment(MarketingDepartment marketingDepartment) {
            new MarketingDepartmentAch(this).UpdateMarketingDepartment(marketingDepartment);
        }

        public IQueryable<MarketingDepartment> GetMarketingDepartmentsWithDetails() {
            return new MarketingDepartmentAch(this).GetMarketingDepartmentsWithDetails();
        }

        public IQueryable<MarketingDepartment> GetMarketingDepartmentsForCreditInfo() {
            return new MarketingDepartmentAch(this).GetMarketingDepartmentsForCreditInfo();
        }

        public MarketingDepartment GetMarketingDepartmentsWithRelationsAndDetails(int departmentId, int branchId) {
            return new MarketingDepartmentAch(this).GetMarketingDepartmentsWithRelationsAndDetails(departmentId, branchId);
        }
        public IQueryable<MarketingDepartment> GetMarketingDepartmentByPersonel() {
            return new MarketingDepartmentAch(this).GetMarketingDepartmentByPersonel();
        }
    }
}
