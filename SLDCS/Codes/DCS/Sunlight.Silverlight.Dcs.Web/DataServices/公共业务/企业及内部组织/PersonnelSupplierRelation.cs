﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class PersonnelSupplierRelationAch : DcsSerivceAchieveBase {
        internal void InsertPersonnelSupplierRelationValidate(PersonnelSupplierRelation personnelSupplierRelation) {
            var userInfo = Utils.GetCurrentUserInfo();
            personnelSupplierRelation.CreatorId = userInfo.Id;
            personnelSupplierRelation.CreatorName = userInfo.Name;
            personnelSupplierRelation.CreateTime = DateTime.Now;
        }
        internal void UpdatePersonnelSupplierRelationValidate(PersonnelSupplierRelation personnelSupplierRelation) {
            var userInfo = Utils.GetCurrentUserInfo();
            personnelSupplierRelation.ModifierId = userInfo.Id;
            personnelSupplierRelation.ModifierName = userInfo.Name;
            personnelSupplierRelation.ModifyTime = DateTime.Now;
        }
        public void InsertePersonnelSupplierRelation(PersonnelSupplierRelation personnelSupplierRelation) {
            InsertToDatabase(personnelSupplierRelation);
            this.InsertPersonnelSupplierRelationValidate(personnelSupplierRelation);
        }
        public void UpdatePersonnelSupplierRelation(PersonnelSupplierRelation personnelSupplierRelation) {
            UpdateToDatabase(personnelSupplierRelation);
            this.UpdatePersonnelSupplierRelationValidate(personnelSupplierRelation);
        }

        internal void DeletePersonnelSupplierRelationValidate(PersonnelSupplierRelation personnelSupplierRelation)
        {
        }

        public void DeletePersonnelSupplierRelation(PersonnelSupplierRelation personnelSupplierRelation)
        {
            DeleteFromDatabase(personnelSupplierRelation);
            this.DeletePersonnelSupplierRelationValidate(personnelSupplierRelation);
        }
        public IQueryable<PersonnelSupplierRelation> QueryPersonnelSupplierRelations()
        {
            return ObjectContext.PersonnelSupplierRelations.Include("Personnel").Include("PartsSalesCategory").Include("PartsSupplier").OrderBy(r => r.Id);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:34
        //原分布类的函数全部转移到Ach                                                               

        public void InsertePersonnelSupplierRelation(PersonnelSupplierRelation personnelSupplierRelation)
        {
            new PersonnelSupplierRelationAch(this).InsertePersonnelSupplierRelation(personnelSupplierRelation);
        }

        public void UpdatePersonnelSupplierRelation(PersonnelSupplierRelation personnelSupplierRelation)
        {
            new PersonnelSupplierRelationAch(this).UpdatePersonnelSupplierRelation(personnelSupplierRelation);
        }

        public void DeletePersonnelSupplierRelation(PersonnelSupplierRelation personnelSupplierRelation)
        {
            new PersonnelSupplierRelationAch(this).DeletePersonnelSupplierRelation(personnelSupplierRelation);
        }
        public IQueryable<PersonnelSupplierRelation> QueryPersonnelSupplierRelations()
        {
            return new PersonnelSupplierRelationAch(this).QueryPersonnelSupplierRelations();
        }
    }
}
