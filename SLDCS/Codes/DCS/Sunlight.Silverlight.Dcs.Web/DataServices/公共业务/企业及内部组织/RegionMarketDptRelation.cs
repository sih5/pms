﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class RegionMarketDptRelationAch : DcsSerivceAchieveBase {
        internal void InsertRegionMarketDptRelationValidate(RegionMarketDptRelation regionMarketDptRelation) {
            var dbRegionMarketDptRelation = ObjectContext.RegionMarketDptRelations.Where(r => r.BranchId == regionMarketDptRelation.BranchId && r.SalesRegionId == regionMarketDptRelation.SalesRegionId && r.MarketDepartmentId == regionMarketDptRelation.MarketDepartmentId && r.Status != (int)DcsBaseDataStatus.作废).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbRegionMarketDptRelation != null)
                throw new ValidationException(ErrorStrings.RegionMarketDptRelation_Validation1);
            var userInfo = Utils.GetCurrentUserInfo();
            regionMarketDptRelation.CreatorId = userInfo.Id;
            regionMarketDptRelation.CreatorName = userInfo.Name;
            regionMarketDptRelation.CreateTime = DateTime.Now;
        }
        internal void UpdateRegionMarketDptRelationValidate(RegionMarketDptRelation regionMarketDptRelation) {
            var dbRegionMarketDptRelation = ObjectContext.RegionMarketDptRelations.Where(r => r.Id != regionMarketDptRelation.Id && r.BranchId == regionMarketDptRelation.BranchId && r.SalesRegionId == regionMarketDptRelation.SalesRegionId && r.MarketDepartmentId == regionMarketDptRelation.MarketDepartmentId && r.Status != (int)DcsBaseDataStatus.作废).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbRegionMarketDptRelation != null)
                throw new ValidationException(ErrorStrings.RegionMarketDptRelation_Validation2);
            var userInfo = Utils.GetCurrentUserInfo();
            regionMarketDptRelation.ModifierId = userInfo.Id;
            regionMarketDptRelation.ModifierName = userInfo.Name;
            regionMarketDptRelation.ModifyTime = DateTime.Now;
        }
        public void InsertRegionMarketDptRelation(RegionMarketDptRelation regionMarketDptRelation) {
            InsertToDatabase(regionMarketDptRelation);
            this.InsertRegionMarketDptRelationValidate(regionMarketDptRelation);
        }
        public void UpdateRegionMarketDptRelation(RegionMarketDptRelation regionMarketDptRelation) {
            UpdateToDatabase(regionMarketDptRelation);
            this.UpdateRegionMarketDptRelationValidate(regionMarketDptRelation);
        }
        public IQueryable<RegionMarketDptRelation> GetSaleRegionMarketingWithMarket() {
            return ObjectContext.RegionMarketDptRelations.Include("SalesRegion").Include("MarketingDepartment").OrderBy(entity => entity.Id);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:29
        //原分布类的函数全部转移到Ach                                                               

        public void InsertRegionMarketDptRelation(RegionMarketDptRelation regionMarketDptRelation) {
            new RegionMarketDptRelationAch(this).InsertRegionMarketDptRelation(regionMarketDptRelation);
        }

        public void UpdateRegionMarketDptRelation(RegionMarketDptRelation regionMarketDptRelation) {
            new RegionMarketDptRelationAch(this).UpdateRegionMarketDptRelation(regionMarketDptRelation);
        }
                public IQueryable<RegionMarketDptRelation> GetSaleRegionMarketingWithMarket() {
            return new RegionMarketDptRelationAch(this).GetSaleRegionMarketingWithMarket();
        }
    }
}
