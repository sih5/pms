﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class RegionPersonnelRelationAch : DcsSerivceAchieveBase {
        internal void InsertRegionPersonnelRelationValidate(RegionPersonnelRelation regionPersonnelRelation) {
            var dbRegionPersonnelRelation = ObjectContext.RegionPersonnelRelations.Where(r => r.BranchId == regionPersonnelRelation.BranchId && r.SalesRegionId == regionPersonnelRelation.SalesRegionId && r.PersonnelId == regionPersonnelRelation.PersonnelId && r.Status != (int)DcsBaseDataStatus.作废).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbRegionPersonnelRelation != null)
                throw new ValidationException(ErrorStrings.RegionPersonnelRelation_Validation1);
            var userInfo = Utils.GetCurrentUserInfo();
            regionPersonnelRelation.CreatorId = userInfo.Id;
            regionPersonnelRelation.CreatorName = userInfo.Name;
            regionPersonnelRelation.CreateTime = DateTime.Now;
        }
        internal void UpdateRegionPersonnelRelationValidate(RegionPersonnelRelation regionPersonnelRelation) {
            var dbRegionPersonnelRelation = ObjectContext.RegionPersonnelRelations.Where(r => r.Id != regionPersonnelRelation.Id && r.BranchId == regionPersonnelRelation.BranchId && r.SalesRegionId == regionPersonnelRelation.SalesRegionId && r.PersonnelId == regionPersonnelRelation.PersonnelId && r.Status != (int)DcsBaseDataStatus.作废).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbRegionPersonnelRelation != null)
                throw new ValidationException(ErrorStrings.RegionPersonnelRelation_Validation2);
            var userInfo = Utils.GetCurrentUserInfo();
            regionPersonnelRelation.ModifierId = userInfo.Id;
            regionPersonnelRelation.ModifierName = userInfo.Name;
            regionPersonnelRelation.ModifyTime = DateTime.Now;
        }
        public void InsertRegionPersonnelRelation(RegionPersonnelRelation regionPersonnelRelation) {
            InsertToDatabase(regionPersonnelRelation);
            this.InsertRegionPersonnelRelationValidate(regionPersonnelRelation);
        }
        public void UpdateRegionPersonnelRelation(RegionPersonnelRelation regionPersonnelRelation) {
            UpdateToDatabase(regionPersonnelRelation);
            this.UpdateRegionPersonnelRelationValidate(regionPersonnelRelation);
        }
        public IQueryable<RegionPersonnelRelation> GetRegionPersonnelRelationWithDetail() {
            return ObjectContext.RegionPersonnelRelations.Include("Personnel").Include("SalesRegion").OrderBy(entity => entity.Id);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:29
        //原分布类的函数全部转移到Ach                                                               

        public void InsertRegionPersonnelRelation(RegionPersonnelRelation regionPersonnelRelation) {
            new RegionPersonnelRelationAch(this).InsertRegionPersonnelRelation(regionPersonnelRelation);
        }

        public void UpdateRegionPersonnelRelation(RegionPersonnelRelation regionPersonnelRelation) {
            new RegionPersonnelRelationAch(this).UpdateRegionPersonnelRelation(regionPersonnelRelation);
        }
                public IQueryable<RegionPersonnelRelation> GetRegionPersonnelRelationWithDetail() {
            return new RegionPersonnelRelationAch(this).GetRegionPersonnelRelationWithDetail();
        }
    }
}
