﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;


namespace Sunlight.Silverlight.Dcs.Web {
    partial class ResRelationshipAch : DcsSerivceAchieveBase {
        public ResRelationshipAch(DcsDomainService domainService)
            : base(domainService) {
        }
        internal void InsertResRelationshipValidate(ResRelationship resRelationship) {
            var old = ObjectContext.ResRelationships.Where(t => t.ResType == resRelationship.ResType && t.Status == (int)DcsBaseDataStatus.有效).Count();
            if(old>0) {
                throw new ValidationException("已存在相同的责任类型");

            }
            var userInfo = Utils.GetCurrentUserInfo();
            resRelationship.CreatorId = userInfo.Id;
            resRelationship.CreatorName = userInfo.Name;
            resRelationship.CreateTime = DateTime.Now;
        }
        internal void UpdateResRelationshipValidate(ResRelationship resRelationship) {
            var userInfo = Utils.GetCurrentUserInfo();
            resRelationship.ModifierId = userInfo.Id;
            resRelationship.ModifierName = userInfo.Name;
            resRelationship.ModifyTime = DateTime.Now;
            UpdateToDatabase(resRelationship);
        }


        public void InsertResRelationship(ResRelationship resRelationship) {
            InsertToDatabase(resRelationship);
          
            this.InsertResRelationshipValidate(resRelationship);
        }

        public void UpdateResRelationship(ResRelationship resRelationship) {
            var dbagency = ObjectContext.ResRelationships.Where(r => r.Id == resRelationship.Id && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbagency == null)
                throw new ValidationException("只能修改有效的数据");
           
            this.UpdateResRelationshipValidate(resRelationship);

        }
        public void 作废责任关系绑定(ResRelationship resRelationship){
            var dbagency = ObjectContext.ResRelationships.Where(r => r.Id == resRelationship.Id && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbagency == null)
                throw new ValidationException("只能作废有效的数据");
            UpdateToDatabase(resRelationship);
            var userInfo = Utils.GetCurrentUserInfo();
            resRelationship.Status = (int)DcsBaseDataStatus.作废;
            resRelationship.AbandonerId = userInfo.Id;
            resRelationship.AbandonerName = userInfo.Name;
            resRelationship.AbandonTime = DateTime.Now;
            this.UpdateResRelationshipValidate(resRelationship);
        }
    }
    partial class DcsDomainService {
        // 已经使用工具进行处理 2017/4/29 12:12:34 
        // 原分布类的函数全部转移到Ach                                                                
        public void InsertResRelationship(ResRelationship resRelationship) {
            new ResRelationshipAch(this).InsertResRelationship(resRelationship);
        }
        public void UpdateResRelationship(ResRelationship resRelationship) {
            new ResRelationshipAch(this).UpdateResRelationship(resRelationship);
        }
        public void 作废责任关系绑定(ResRelationship resRelationship) {
            new ResRelationshipAch(this).作废责任关系绑定(resRelationship);
        }
    }
}
