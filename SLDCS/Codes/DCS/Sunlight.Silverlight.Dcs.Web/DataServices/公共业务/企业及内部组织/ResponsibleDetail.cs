﻿

namespace Sunlight.Silverlight.Dcs.Web {
    partial class ResponsibleDetailAch : DcsSerivceAchieveBase {
        public ResponsibleDetailAch(DcsDomainService domainService)
            : base(domainService) {
        }
        public void InsertResponsibleDetail(ResponsibleDetail responsibleDetail) {

        }

        public void UpdateResponsibleDetail(ResponsibleDetail responsibleDetail) {
        }
    }
    partial class DcsDomainService {
        // 已经使用工具进行处理 2017/4/29 12:12:34 
        //原分布类的函数全部转移到Ach                                                                
        public void InsertResponsibleDetail(ResponsibleDetail responsibleDetail) {

            new ResponsibleDetailAch(this).InsertResponsibleDetail(responsibleDetail);
        }
        public void UpdateResponsibleDetail(ResponsibleDetail responsibleDetail) {
            new ResponsibleDetailAch(this).UpdateResponsibleDetail(responsibleDetail);
        }
    }
}

