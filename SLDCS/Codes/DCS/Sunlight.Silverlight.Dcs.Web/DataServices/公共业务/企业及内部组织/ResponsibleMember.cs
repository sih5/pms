﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;


namespace Sunlight.Silverlight.Dcs.Web {
    partial class ResponsibleMemberAch : DcsSerivceAchieveBase {
        public ResponsibleMemberAch(DcsDomainService domainService)
            : base(domainService) {
        }
        internal void InsertResponsibleMemberValidate(ResponsibleMember responsibleMember) {
            var old = ObjectContext.ResponsibleMembers.Where(t => t.ResTem == responsibleMember.ResTem && t.Status == (int)DcsBaseDataStatus.有效).Count();
            if(old>0) {
                throw new ValidationException("已存在相同的责任组");

            }
            var userInfo = Utils.GetCurrentUserInfo();
            responsibleMember.CreatorId = userInfo.Id;
            responsibleMember.CreatorName = userInfo.Name;
            responsibleMember.CreateTime = DateTime.Now;
        }
        internal void UpdateResponsibleMemberValidate(ResponsibleMember responsibleMember) {
            var userInfo = Utils.GetCurrentUserInfo();
            responsibleMember.ModifierId = userInfo.Id;
            responsibleMember.ModifierName = userInfo.Name;
            responsibleMember.ModifyTime = DateTime.Now;

        }


        public void InsertResponsibleMember(ResponsibleMember responsibleMember) {
            InsertToDatabase(responsibleMember);
            var responsibleMemberDetails = ChangeSet.GetAssociatedChanges(responsibleMember, v => v.ResponsibleDetails, ChangeOperation.Insert);
            foreach(ResponsibleDetail responsibleMemberDetail in responsibleMemberDetails) {
                InsertToDatabase(responsibleMemberDetail);
            }
            this.InsertResponsibleMemberValidate(responsibleMember);
        }

        public void UpdateResponsibleMember(ResponsibleMember responsibleMember) {
            responsibleMember.ResponsibleDetails.Clear();
            UpdateToDatabase(responsibleMember);
            var responsibleMemberDetails = ChangeSet.GetAssociatedChanges(responsibleMember, v => v.ResponsibleDetails);
            foreach(ResponsibleDetail responsibleMemberDetail in responsibleMemberDetails) {
                switch(ChangeSet.GetChangeOperation(responsibleMemberDetail)) {
                    case ChangeOperation.Insert:
                        InsertToDatabase(responsibleMemberDetail);
                        break;
                    case ChangeOperation.Update:
                        UpdateToDatabase(responsibleMemberDetail);
                        break;
                    case ChangeOperation.Delete:
                        DeleteFromDatabase(responsibleMemberDetail);
                        break;
                }
            }
            this.UpdateResponsibleMemberValidate(responsibleMember);
        }
        public ResponsibleMember GetResponsibleMembersWithDetailsById(int id) {
            var dbCrossSalesOrder = ObjectContext.ResponsibleMembers.Include("ResponsibleDetails").SingleOrDefault(entity => entity.Id == id && entity.Status != (int)DcsBaseDataStatus.作废);
            return dbCrossSalesOrder;
        } 
    }
    partial class DcsDomainService {
        // 已经使用工具进行处理 2017/4/29 12:12:34 
        // 原分布类的函数全部转移到Ach                                                                
        public void InsertResponsibleMember(ResponsibleMember responsibleMember) {
            new ResponsibleMemberAch(this).InsertResponsibleMember(responsibleMember);
        }
        public void UpdateResponsibleMember(ResponsibleMember responsibleMember) {
            new ResponsibleMemberAch(this).UpdateResponsibleMember(responsibleMember);
        }
        public ResponsibleMember GetResponsibleMembersWithDetailsById(int id) {
            return new ResponsibleMemberAch(this).GetResponsibleMembersWithDetailsById(id);
        }
    }
}
