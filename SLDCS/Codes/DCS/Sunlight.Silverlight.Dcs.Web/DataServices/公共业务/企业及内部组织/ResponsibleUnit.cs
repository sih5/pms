﻿using System;
using System.Linq;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class ResponsibleUnitAch : DcsSerivceAchieveBase {
        internal void InsertResponsibleUnitValidate(ResponsibleUnit responsibleUnit) {
            var userInfo = Utils.GetCurrentUserInfo();
            responsibleUnit.CreatorId = userInfo.Id;
            responsibleUnit.CreatorName = userInfo.Name;
            responsibleUnit.CreateTime = DateTime.Now;
        }

        internal void UpdateResponsibleUnitValidate(ResponsibleUnit responsibleUnit) {
            var userInfo = Utils.GetCurrentUserInfo();
            responsibleUnit.ModifierId = userInfo.Id;
            responsibleUnit.ModifierName = userInfo.Name;
            responsibleUnit.ModifyTime = DateTime.Now;
        }

        public void InsertResponsibleUnit(ResponsibleUnit responsibleUnit) {
            InsertToDatabase(responsibleUnit);
            this.InsertResponsibleUnitValidate(responsibleUnit);
        }

        public void UpdateResponsibleUnit(ResponsibleUnit responsibleUnit) {
            UpdateToDatabase(responsibleUnit);
            this.UpdateResponsibleUnitValidate(responsibleUnit);
        }
        public IQueryable<ResponsibleUnit> GetResponsibleUnitWithDetailsByBrandName(string brandName) {
            if(string.IsNullOrEmpty(brandName))
                return null;
            var getResponsibleUnitIds = from v in ObjectContext.PartsSalesCategories.Where(r => r.Name == brandName)
                                        join r in ObjectContext.ResponsibleUnitBranches
                                        on v.BranchId equals r.BranchId
                                        select r.ResponsibleUnitId;
            var dbResponsibleUnit = ObjectContext.ResponsibleUnits.Where(r => getResponsibleUnitIds.Contains(r.Id));
            return dbResponsibleUnit;
        }
        public ResponsibleUnit GetResponsibleUnitWithDetailsById(int id) {
            var dbResponsibleUnit = ObjectContext.ResponsibleUnits.SingleOrDefault(entity => entity.Id == id);
            if(dbResponsibleUnit != null) {
                var dbCompany = ObjectContext.Companies.SingleOrDefault(entity => entity.Id == dbResponsibleUnit.Id);
                var dbResponsibleUnitProductDetails = ObjectContext.ResponsibleUnitProductDetails.Where(entity => entity.ResponsibleUnitId == dbResponsibleUnit.Id).ToArray();
                if(dbResponsibleUnitProductDetails.Length > 0) {
                    var productIds = dbResponsibleUnitProductDetails.Select(r => r.ProductId);
                    //var products = ObjectContext.Products.Where(r => productIds.Contains(r.Id) && r.Status == (int)DcsMasterDataStatus.有效).ToArray();
                }
                //var dbResponsibleUnitBranch = ObjectContext.ResponsibleUnitBranches.SingleOrDefault(entity => entity.ResponsibleUnitId == dbResponsibleUnit.Id);
                var dbResponsibleUnitBranchs = ObjectContext.ResponsibleUnitBranches.Where(entity => entity.ResponsibleUnitId == dbResponsibleUnit.Id).ToArray();

            }
            return dbResponsibleUnit;
        }

    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:29
        //原分布类的函数全部转移到Ach                                                               

        public void InsertResponsibleUnit(ResponsibleUnit responsibleUnit) {
            new ResponsibleUnitAch(this).InsertResponsibleUnit(responsibleUnit);
        }

        public void UpdateResponsibleUnit(ResponsibleUnit responsibleUnit) {
            new ResponsibleUnitAch(this).UpdateResponsibleUnit(responsibleUnit);
        }

        public ResponsibleUnit GetResponsibleUnitWithDetailsById(int id) {
            return new ResponsibleUnitAch(this).GetResponsibleUnitWithDetailsById(id);
        }

        public IQueryable<ResponsibleUnit> GetResponsibleUnitWithDetailsByBrandName(string brandName) {
            return new ResponsibleUnitAch(this).GetResponsibleUnitWithDetailsByBrandName(brandName);
        }
    }
}
