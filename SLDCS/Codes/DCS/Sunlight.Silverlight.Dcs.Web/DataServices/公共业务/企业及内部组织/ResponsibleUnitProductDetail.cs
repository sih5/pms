﻿
namespace Sunlight.Silverlight.Dcs.Web {
    partial class ResponsibleUnitProductDetailAch : DcsSerivceAchieveBase {

        internal void InsertResponsibleUnitProductDetailValidate(ResponsibleUnitProductDetail responsibleUnitProductDetail) {
        }

        public void InsertResponsibleUnitProductDetail(ResponsibleUnitProductDetail responsibleUnitProductDetail) {
            InsertToDatabase(responsibleUnitProductDetail);
            this.InsertResponsibleUnitProductDetailValidate(responsibleUnitProductDetail);
        }

        public void UpdateResponsibleUnitProductDetail(ResponsibleUnitProductDetail responsibleUnitProductDetail) {
            //客户端需要提供空的Update方法作更新操作
        }

        public void DeleteResponsibleUnitProductDetail(ResponsibleUnitProductDetail responsibleUnitProductDetail) {
            DeleteFromDatabase(responsibleUnitProductDetail);
        }
    
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:29
        //原分布类的函数全部转移到Ach                                                               

        public void InsertResponsibleUnitProductDetail(ResponsibleUnitProductDetail responsibleUnitProductDetail) {
            new ResponsibleUnitProductDetailAch(this).InsertResponsibleUnitProductDetail(responsibleUnitProductDetail);
        }

        public void UpdateResponsibleUnitProductDetail(ResponsibleUnitProductDetail responsibleUnitProductDetail) {
            new ResponsibleUnitProductDetailAch(this).UpdateResponsibleUnitProductDetail(responsibleUnitProductDetail);
        }

        public void DeleteResponsibleUnitProductDetail(ResponsibleUnitProductDetail responsibleUnitProductDetail) {
            new ResponsibleUnitProductDetailAch(this).DeleteResponsibleUnitProductDetail(responsibleUnitProductDetail);
        }
    }
}
