﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.EntityClient;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Devart.Data.Oracle;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class RolePersonnelReportAch : DcsSerivceAchieveBase {
        public RolePersonnelReportAch(DcsDomainService domainService)
            : base(domainService) {
        }
        public IEnumerable<RolePersonnelReport> 获取角色人员清单(string roleName, string loginId, string name) {
            string SQL = @"select a.name as RoleName, c.loginid as LoginId, c.name as Name,Rownum
                          from role a
                         inner join rolepersonnel b
                            on a.id = b.roleid
                         inner join personnel c
                            on c.id = b.personnelid
                         where a.enterpriseid = 12730
                            and a.status=1  and c.Status = 1
                           and a.name <> '晨阑测试' ";

            if(!string.IsNullOrEmpty(roleName)) {
                SQL = SQL + " and a.name like '%" + roleName + "%'";
            }

            if(!string.IsNullOrEmpty(loginId)) {
                SQL = SQL + " and c.loginId like'%" + loginId + "%'";
            }
            if(!string.IsNullOrEmpty(name)) {
                SQL = SQL + " and c.name like '%" + name + "%'";
            }

            SQL = SQL + "   order by a.name";

            var entityConnection = new EntityConnectionStringBuilder(SecurityConnStr).ProviderConnectionString;

            var db = DbHelper.GetDbHelp(entityConnection);
            var result = new List<RolePersonnelReport>(100);
            using(var conn = db.CreateDbConnection()) {
                conn.Open();
                try {
                    var commandTemplate = db.CreateDbCommand(string.Format(SQL, db.ParamMark), conn, null);
                    var reader = commandTemplate.ExecuteReader();
                    while(reader.Read())
                        result.Add(new RolePersonnelReport {
                            RoleName = reader[0].ToString(),
                            LoginId = reader[1].ToString(),
                            Rownum = reader[3].ToString(),
                            Name = reader[2].ToString()
                        });
                } catch(Exception ex) {
                    if(conn.State == ConnectionState.Open)
                        conn.Close();
                    conn.Dispose();
                    throw ex;
                } finally {
                    if(conn.State == ConnectionState.Open)
                        conn.Close();
                    conn.Dispose();
                }
            }
            return result;
        }
        private string SecurityConnStr {
            get {
                return ConfigurationManager.ConnectionStrings["SecurityEntities"].ConnectionString;
            }
        }
        public IEnumerable<RolePersonnelReport> 获取企业权限模板(string name, string lV1, string actionName, string lV2, string lV3) {
            string SQL = @"select Name, LV1, LV2, LV3,ActionName, Rownum
                          from (select d.id   pageid,
                                       f.name LV1,
                                       e.name LV2,
                                       d.name LV3,
                                       a.name as name
                                  from entnodetemplate a
                                 inner join entnodetemplatedetail b
                                    on a.id = b.entnodetemplateid
                                 inner join node c
                                    on c.id = b.nodeid
                                   and c.categorytype = 0
                                 inner join page d
                                    on d.id = c.categoryid
                                 inner join page e
                                    on e.id = d.parentid
                                 inner join page f
                                    on f.id = e.parentid
                                  where a.status=2 and c.status=2 and d.status=2 and e.status=2 and f.status=2 ) a
                          left join (select D.Pageid as actionid, d.name as ActionName
                                       from entnodetemplate a
                                      inner join entnodetemplatedetail b
                                         on a.id = b.entnodetemplateid
                                      inner join node c
                                         on c.id = b.nodeid
                                        and c.categorytype = 1
                                      inner join action d
                                         on d.id = c.categoryid where a.status=2 and c.status=2 and d.status=2 and rownum=1) b
                            on b.actionid = a.pageid
                            where 1=1  ";

            if(!string.IsNullOrEmpty(name)) {
                SQL = SQL + " and a.name like '%" + name + "%'";
            }

            if(!string.IsNullOrEmpty(lV1)) {
                SQL = SQL + " and a.LV1 like'%" + lV1 + "%'";
            }
            if(!string.IsNullOrEmpty(lV2)) {
                SQL = SQL + " and a.LV2 like'%" + lV2 + "%'";
            }
            if(!string.IsNullOrEmpty(lV3)) {
                SQL = SQL + " and a.LV3 like'%" + lV3 + "%'";
            }
            if(!string.IsNullOrEmpty(actionName)) {
                SQL = SQL + " and b.actionName like '%" + actionName + "%'";
            }

            SQL = SQL + "  order by a.Name, a.LV1, a.lv2, a.lv3 ";

            var entityConnection = new EntityConnectionStringBuilder(SecurityConnStr).ProviderConnectionString;

            var db = DbHelper.GetDbHelp(entityConnection);
            var result = new List<RolePersonnelReport>(100);
            using(var conn = db.CreateDbConnection()) {
                conn.Open();
                try {
                    var commandTemplate = db.CreateDbCommand(string.Format(SQL, db.ParamMark), conn, null);
                    var reader = commandTemplate.ExecuteReader();
                    while(reader.Read())
                        result.Add(new RolePersonnelReport {
                            Name = reader[0] == null ? "" : reader[0].ToString(),
                            LV1 = reader[1] == null ? "" : reader[1].ToString(),
                            LV3 = reader[3] == null ? "" : reader[3].ToString(),
                            LV2 = reader[2] == null ? "" : reader[2].ToString(),
                            ActionName = reader[4] == null ? "" : reader[4].ToString(),
                            Rownum = reader[5].ToString(),
                        });
                } catch(Exception ex) {
                    if(conn.State == ConnectionState.Open)
                        conn.Close();
                    conn.Dispose();
                    throw ex;
                } finally {
                    if(conn.State == ConnectionState.Open)
                        conn.Close();
                    conn.Dispose();
                }
            }
            return result;
        }
        public IEnumerable<RolePersonnelReport> 获取角色权限清单(string name, string actionName, string lV1, string lV2, string lV3) {
            string SQL = @"select  Rownum,mm.* from (select tt.LV1,
                           tt.LV2,
                           tt.LV3,
                           tt.ActionName,
                           decode(sum(AccessoriesCenter),0,null,1),
                           decode(sum(BussinessMananger),0,null,1),
                           decode(sum(AssistantDirector),0,null,1),
                           decode(sum(SalesMananger),0,null,1),
                           decode(sum(SalesFilmMananger),0,null,1),
                           decode(sum(OutSalesMananger),0,null,1),
                           decode(sum(ExtraMananger),0,null,1),
                           decode(sum(ServiceMananger),0,null,1),
                           decode(sum(BigMananger),0,null,1),
                           decode(sum(PurchaseMananger),0,null,1),
                           decode(sum(PlanMananger),0,null,1),
                           decode(sum(AllocationMananger),0,null,1),
                           decode(sum(Buyer),0,null,1),
                           decode(sum(MarketMananger),0,null,1),
                           decode(sum(PriceMananger),0,null,1),
                           decode(sum(CommercialMananger),0,null,1),
                           decode(sum(NetMananger),0,null,1),
                           decode(sum(MarketCommissioner),0,null,1),
                           decode(sum(IntegratedHead),0,null,1),
                           decode(sum(SystemAdministrator),0,null,1),
                           decode(sum(AssistantSystem),0,null,1),
                           decode(sum(PurchaseCommissioner),0,null,1),
                           decode(sum(GeneralCommissioner),0,null,1),
                           decode(sum(AccessoryTechnician),0,null,1),
                           decode( sum(LogisticsManager),0,null,1),
                           decode(sum(LogisticsFuManager),0,null,1),
                           decode( sum(InBoundManager),0,null,1),
                           decode( sum(InBoundHead),0,null,1),
                           decode( sum(InBoundMember),0,null,1),
                           decode(sum(PackingManager),0,null,1),
                           decode(sum(PackingHead),0,null,1),
                           decode( sum(PackingMember),0,null,1),
                           decode(sum(PackingEngineer),0,null,1),
                           decode(sum(ReturnSales),0,null,1),
                           decode(sum(WarehouseManagement),0,null,1),
                           decode(sum(WarehouseHead),0,null,1),
                           decode(sum(WarehouseMember),0,null,1),
                           decode(sum(WarehouseManager),0,null,1),
                           decode(sum(SupervisorLogistics),0,null,1),
                           decode(sum(LogistManager),0,null,1),
                           decode(sum(InventoryHead),0,null,1),
                           decode(sum(InventoryMember),0,null,1),
                           decode(sum(LogisticsWarehouse),0,null,1),
                           decode(sum(Treasurer),0,null,1),
                           decode(sum(TreasurerMember),0,null,1),
                           decode(sum(ServiceManager),0,null,1)
                      from (select a.name as LV1,
                                   b.name as LV2,
                                   c.name as LV3,
                                   d.name as ActionName, tmp.name,
                                   sum(case
                                         when tmp.name = 'KD' || '&' || '配件中心总监' and
                                              nvl(tmp.nodeid, 0) > 1 then
                                          1
                                         else
                                          0
                                       end) AccessoriesCenter, --KD&配件中心总监
                                   sum(case
                                         when tmp.name = '配件业务处经理' and nvl(tmp.nodeid, 0) > 1 then
                                          1
                                         else
                                          0
                                       end) BussinessMananger, --配件业务处经理
                                   sum(case
                                         when tmp.name = '总监助理' and nvl(tmp.nodeid, 0) > 1 then
                                          1
                                         else
                                          0
                                       end) AssistantDirector, --总监助理
                                   sum(case
                                         when tmp.name = '销售服务组主管' and nvl(tmp.nodeid, 0) > 1 then
                                          1
                                         else
                                          0
                                       end) SalesMananger, --销售服务组主管
                                   sum(case
                                         when tmp.name = '销售片管' and nvl(tmp.nodeid, 0) > 1 then
                                          1
                                         else
                                          0
                                       end) SalesFilmMananger, --销售片管
                                   sum(case
                                         when tmp.name = '出口专员' and nvl(tmp.nodeid, 0) > 1 then
                                          1
                                         else
                                          0
                                       end) OutSalesMananger, --出口专员
                                   sum(case
                                         when tmp.name = '特急专员' and nvl(tmp.nodeid, 0) > 1 then
                                          1
                                         else
                                          0
                                       end) ExtraMananger, --特急专员
                                   sum(case
                                         when tmp.name = '服务商管理员' and nvl(tmp.nodeid, 0) > 1 then
                                          1
                                         else
                                          0
                                       end) ServiceMananger, --服务商管理员
                                   sum(case
                                         when tmp.name = '三包、大客户、销售员' and nvl(tmp.nodeid, 0) > 1 then
                                          1
                                         else
                                          0
                                       end) BigMananger, --三包、大客户、销售员
                                   sum(case
                                         when tmp.name = '采购计划组主管' and nvl(tmp.nodeid, 0) > 1 then
                                          1
                                         else
                                          0
                                       end) PurchaseMananger, --采购计划组主管
                                   sum(case
                                         when tmp.name = '计划专员' and nvl(tmp.nodeid, 0) > 1 then
                                          1
                                         else
                                          0
                                       end) PlanMananger, --计划专员
                                   sum(case
                                         when tmp.name = '调拨专员' and nvl(tmp.nodeid, 0) > 1 then
                                          1
                                         else
                                          0
                                       end) AllocationMananger, --调拨专员
                                   sum(case
                                         when tmp.name = '采购专员' and nvl(tmp.nodeid, 0) > 1 then
                                          1
                                         else
                                          0
                                       end) Buyer, --采购专员
                                   sum(case
                                         when tmp.name = '市场投放组主管' and nvl(tmp.nodeid, 0) > 1 then
                                          1
                                         else
                                          0
                                       end) MarketMananger, --市场投放组主管
                                   sum(case
                                         when tmp.name = '价格管理' and nvl(tmp.nodeid, 0) > 1 then
                                          1
                                         else
                                          0
                                       end) PriceMananger, --价格管理
                                   sum(case
                                         when tmp.name = '商务专员' and nvl(tmp.nodeid, 0) > 1 then
                                          1
                                         else
                                          0
                                       end) CommercialMananger, --商务专员
                                   sum(case
                                         when tmp.name = '网络销售员' and nvl(tmp.nodeid, 0) > 1 then
                                          1
                                         else
                                          0
                                       end) NetMananger, --网络销售员
                                   sum(case
                                         when tmp.name = '市场投放专员' and nvl(tmp.nodeid, 0) > 1 then
                                          1
                                         else
                                          0
                                       end) MarketCommissioner, --市场投放专员
                                   sum(case
                                         when tmp.name = '综合组主管' and nvl(tmp.nodeid, 0) > 1 then
                                          1
                                         else
                                          0
                                       end) IntegratedHead, --综合组主管
                                   sum(case
                                         when tmp.name = '系统管理员' and nvl(tmp.nodeid, 0) > 1 then
                                          1
                                         else
                                          0
                                       end) SystemAdministrator, --系统管理员
                                   sum(case
                                         when tmp.name = '系统管理员助理' and nvl(tmp.nodeid, 0) > 1 then
                                          1
                                         else
                                          0
                                       end) AssistantSystem, --系统管理员助理
                                   sum(case
                                         when tmp.name = '采购结算专员' and nvl(tmp.nodeid, 0) > 1 then
                                          1
                                         else
                                          0
                                       end) PurchaseCommissioner, --采购结算专员
                                   sum(case
                                         when tmp.name = '综合专员' and nvl(tmp.nodeid, 0) > 1 then
                                          1
                                         else
                                          0
                                       end) GeneralCommissioner, --综合专员
                                   sum(case
                                         when tmp.name = '配件技术员' and nvl(tmp.nodeid, 0) > 1 then
                                          1
                                         else
                                          0
                                       end) AccessoryTechnician, --配件技术员
                                   sum(case
                                         when tmp.name = '物流管理处经理' and nvl(tmp.nodeid, 0) > 1 then
                                          1
                                         else
                                          0
                                       end) LogisticsManager, --物流管理处经理
                                   sum(case
                                         when tmp.name = '物流管理处副经理' and nvl(tmp.nodeid, 0) > 1 then
                                          1
                                         else
                                          0
                                       end) LogisticsFuManager, --物流管理处副经理
                                   sum(case
                                         when tmp.name = '入库组主管' and nvl(tmp.nodeid, 0) > 1 then
                                          1
                                         else
                                          0
                                       end) InBoundManager, --入库组主管
                                   sum(case
                                         when tmp.name = '入库组组长' and nvl(tmp.nodeid, 0) > 1 then
                                          1
                                         else
                                          0
                                       end) InBoundHead, --入库组组长
                                   sum(case
                                         when tmp.name = '入库组组员' and nvl(tmp.nodeid, 0) > 1 then
                                          1
                                         else
                                          0
                                       end) InBoundMember, --入库组组员
                                   sum(case
                                         when tmp.name = '包装组主管' and nvl(tmp.nodeid, 0) > 1 then
                                          1
                                         else
                                          0
                                       end) PackingManager, --包装组主管
                                   sum(case
                                         when tmp.name = '包装组组长' and nvl(tmp.nodeid, 0) > 1 then
                                          1
                                         else
                                          0
                                       end) PackingHead, --包装组组长
                                   sum(case
                                         when tmp.name = '包装组组员' and nvl(tmp.nodeid, 0) > 1 then
                                          1
                                         else
                                          0
                                       end) PackingMember, --包装组组员
                                   sum(case
                                         when tmp.name = '包装工程师' and nvl(tmp.nodeid, 0) > 1 then
                                          1
                                         else
                                          0
                                       end) PackingEngineer, --包装工程师
                                   sum(case
                                         when tmp.name = '销售退货专员' and nvl(tmp.nodeid, 0) > 1 then
                                          1
                                         else
                                          0
                                       end) ReturnSales, --销售退货专员
                                   sum(case
                                         when tmp.name = '库管组主管' and nvl(tmp.nodeid, 0) > 1 then
                                          1
                                         else
                                          0
                                       end) WarehouseManagement, --库管组主管
                                   sum(case
                                         when tmp.name = '库管组组长' and nvl(tmp.nodeid, 0) > 1 then
                                          1
                                         else
                                          0
                                       end) WarehouseHead, --库管组组长
                                   sum(case
                                         when tmp.name = '库管组组员' and nvl(tmp.nodeid, 0) > 1 then
                                          1
                                         else
                                          0
                                       end) WarehouseMember, --库管组组员
                                   sum(case
                                         when tmp.name = '仓库管理员(马钢库、重庆库)' and nvl(tmp.nodeid, 0) > 1 then
                                          1
                                         else
                                          0
                                       end) WarehouseManager, --仓库管理员(马钢库、重庆库)
                                   sum(case
                                         when tmp.name = '物流组主管' and nvl(tmp.nodeid, 0) > 1 then
                                          1
                                         else
                                          0
                                       end) SupervisorLogistics, --物流组主管
                                   sum(case
                                         when tmp.name = '物流管理工' and nvl(tmp.nodeid, 0) > 1 then
                                          1
                                         else
                                          0
                                       end) LogistManager, --物流管理工
                                   sum(case
                                         when tmp.name = '清点组组长' and nvl(tmp.nodeid, 0) > 1 then
                                          1
                                         else
                                          0
                                       end) InventoryHead, --清点组组长
                                   sum(case
                                         when tmp.name = '清点组组员' and nvl(tmp.nodeid, 0) > 1 then
                                          1
                                         else
                                          0
                                       end) InventoryMember, --清点组组员
                                   sum(case
                                         when tmp.name = '库内物流工程师' and nvl(tmp.nodeid, 0) > 1 then
                                          1
                                         else
                                          0
                                       end) LogisticsWarehouse, --库内物流工程师
                                   sum(case
                                         when tmp.name = '财务主管' and nvl(tmp.nodeid, 0) > 1 then
                                          1
                                         else
                                          0
                                       end) Treasurer, --财务主管
                                   sum(case
                                         when tmp.name = '财务人员' and nvl(tmp.nodeid, 0) > 1 then
                                          1
                                         else
                                          0
                                       end) TreasurerMember, --财务人员
                                   sum(case
                                         when tmp.name = '服务经理' and nvl(tmp.nodeid, 0) > 1 then
                                          1
                                         else
                                          0
                                       end) ServiceManager --服务经理
                              from page a
                             inner join page b
                                on a.id = b.parentid
                             inner join page c
                                on c.parentid = b.id
                             inner join action d
                                on d.pageid = c.id
                             inner join node
                                on node.categoryid = d.id
                               and node.categorytype = 1
                              left join (select rule.nodeid, role.name
                                          from rule
                                         inner join role
                                            on role.id = rule.roleid
                                         where role.enterpriseid = 12730
                                           and role.status = 1) tmp
                                on tmp.nodeid = node.id
                             group by a.name, b.name, c.name, d.name,tmp.name 
                            union
                select a.name as LV1,
                        b.name as LV2,
                        c.name as LV3,
                        cast( '查询' as varchar2(32)) as ActionName,
                        tmp.name,
                        sum(case
                              when tmp.name = 'KD' || '&' || '配件中心总监' and
                                   nvl(tmp.nodeid, 0) > 1 then
                               1
                              else
                               0
                            end) AccessoriesCenter, --KD&配件中心总监
                        sum(case
                              when tmp.name = '配件业务处经理' and nvl(tmp.nodeid, 0) > 1 then
                               1
                              else
                               0
                            end) BussinessMananger, --配件业务处经理
                        sum(case
                              when tmp.name = '总监助理' and nvl(tmp.nodeid, 0) > 1 then
                               1
                              else
                               0
                            end) AssistantDirector, --总监助理
                        sum(case
                              when tmp.name = '销售服务组主管' and nvl(tmp.nodeid, 0) > 1 then
                               1
                              else
                               0
                            end) SalesMananger, --销售服务组主管
                        sum(case
                              when tmp.name = '销售片管' and nvl(tmp.nodeid, 0) > 1 then
                               1
                              else
                               0
                            end) SalesFilmMananger, --销售片管
                        sum(case
                              when tmp.name = '出口专员' and nvl(tmp.nodeid, 0) > 1 then
                               1
                              else
                               0
                            end) OutSalesMananger, --出口专员
                        sum(case
                              when tmp.name = '特急专员' and nvl(tmp.nodeid, 0) > 1 then
                               1
                              else
                               0
                            end) ExtraMananger, --特急专员
                        sum(case
                              when tmp.name = '服务商管理员' and nvl(tmp.nodeid, 0) > 1 then
                               1
                              else
                               0
                            end) ServiceMananger, --服务商管理员
                        sum(case
                              when tmp.name = '三包、大客户、销售员' and
                                   nvl(tmp.nodeid, 0) > 1 then
                               1
                              else
                               0
                            end) BigMananger, --三包、大客户、销售员
                        sum(case
                              when tmp.name = '采购计划组主管' and nvl(tmp.nodeid, 0) > 1 then
                               1
                              else
                               0
                            end) PurchaseMananger, --采购计划组主管
                        sum(case
                              when tmp.name = '计划专员' and nvl(tmp.nodeid, 0) > 1 then
                               1
                              else
                               0
                            end) PlanMananger, --计划专员
                        sum(case
                              when tmp.name = '调拨专员' and nvl(tmp.nodeid, 0) > 1 then
                               1
                              else
                               0
                            end) AllocationMananger, --调拨专员
                        sum(case
                              when tmp.name = '采购专员' and nvl(tmp.nodeid, 0) > 1 then
                               1
                              else
                               0
                            end) Buyer, --采购专员
                        sum(case
                              when tmp.name = '市场投放组主管' and nvl(tmp.nodeid, 0) > 1 then
                               1
                              else
                               0
                            end) MarketMananger, --市场投放组主管
                        sum(case
                              when tmp.name = '价格管理' and nvl(tmp.nodeid, 0) > 1 then
                               1
                              else
                               0
                            end) PriceMananger, --价格管理
                        sum(case
                              when tmp.name = '商务专员' and nvl(tmp.nodeid, 0) > 1 then
                               1
                              else
                               0
                            end) CommercialMananger, --商务专员
                        sum(case
                              when tmp.name = '网络销售员' and nvl(tmp.nodeid, 0) > 1 then
                               1
                              else
                               0
                            end) NetMananger, --网络销售员
                        sum(case
                              when tmp.name = '市场投放专员' and nvl(tmp.nodeid, 0) > 1 then
                               1
                              else
                               0
                            end) MarketCommissioner, --市场投放专员
                        sum(case
                              when tmp.name = '综合组主管' and nvl(tmp.nodeid, 0) > 1 then
                               1
                              else
                               0
                            end) IntegratedHead, --综合组主管
                        sum(case
                              when tmp.name = '系统管理员' and nvl(tmp.nodeid, 0) > 1 then
                               1
                              else
                               0
                            end) SystemAdministrator, --系统管理员
                        sum(case
                              when tmp.name = '系统管理员助理' and nvl(tmp.nodeid, 0) > 1 then
                               1
                              else
                               0
                            end) AssistantSystem, --系统管理员助理
                        sum(case
                              when tmp.name = '采购结算专员' and nvl(tmp.nodeid, 0) > 1 then
                               1
                              else
                               0
                            end) PurchaseCommissioner, --采购结算专员
                        sum(case
                              when tmp.name = '综合专员' and nvl(tmp.nodeid, 0) > 1 then
                               1
                              else
                               0
                            end) GeneralCommissioner, --综合专员
                        sum(case
                              when tmp.name = '配件技术员' and nvl(tmp.nodeid, 0) > 1 then
                               1
                              else
                               0
                            end) AccessoryTechnician, --配件技术员
                        sum(case
                              when tmp.name = '物流管理处经理' and nvl(tmp.nodeid, 0) > 1 then
                               1
                              else
                               0
                            end) LogisticsManager, --物流管理处经理
                        sum(case
                              when tmp.name = '物流管理处副经理' and nvl(tmp.nodeid, 0) > 1 then
                               1
                              else
                               0
                            end) LogisticsFuManager, --物流管理处副经理
                        sum(case
                              when tmp.name = '入库组主管' and nvl(tmp.nodeid, 0) > 1 then
                               1
                              else
                               0
                            end) InBoundManager, --入库组主管
                        sum(case
                              when tmp.name = '入库组组长' and nvl(tmp.nodeid, 0) > 1 then
                               1
                              else
                               0
                            end) InBoundHead, --入库组组长
                        sum(case
                              when tmp.name = '入库组组员' and nvl(tmp.nodeid, 0) > 1 then
                               1
                              else
                               0
                            end) InBoundMember, --入库组组员
                        sum(case
                              when tmp.name = '包装组主管' and nvl(tmp.nodeid, 0) > 1 then
                               1
                              else
                               0
                            end) PackingManager, --包装组主管
                        sum(case
                              when tmp.name = '包装组组长' and nvl(tmp.nodeid, 0) > 1 then
                               1
                              else
                               0
                            end) PackingHead, --包装组组长
                        sum(case
                              when tmp.name = '包装组组员' and nvl(tmp.nodeid, 0) > 1 then
                               1
                              else
                               0
                            end) PackingMember, --包装组组员
                        sum(case
                              when tmp.name = '包装工程师' and nvl(tmp.nodeid, 0) > 1 then
                               1
                              else
                               0
                            end) PackingEngineer, --包装工程师
                        sum(case
                              when tmp.name = '销售退货专员' and nvl(tmp.nodeid, 0) > 1 then
                               1
                              else
                               0
                            end) ReturnSales, --销售退货专员
                        sum(case
                              when tmp.name = '库管组主管' and nvl(tmp.nodeid, 0) > 1 then
                               1
                              else
                               0
                            end) WarehouseManagement, --库管组主管
                        sum(case
                              when tmp.name = '库管组组长' and nvl(tmp.nodeid, 0) > 1 then
                               1
                              else
                               0
                            end) WarehouseHead, --库管组组长
                        sum(case
                              when tmp.name = '库管组组员' and nvl(tmp.nodeid, 0) > 1 then
                               1
                              else
                               0
                            end) WarehouseMember, --库管组组员
                        sum(case
                              when tmp.name = '仓库管理员(马钢库、重庆库)' and
                                   nvl(tmp.nodeid, 0) > 1 then
                               1
                              else
                               0
                            end) WarehouseManager, --仓库管理员(马钢库、重庆库)
                        sum(case
                              when tmp.name = '物流组主管' and nvl(tmp.nodeid, 0) > 1 then
                               1
                              else
                               0
                            end) SupervisorLogistics, --物流组主管
                        sum(case
                              when tmp.name = '物流管理工' and nvl(tmp.nodeid, 0) > 1 then
                               1
                              else
                               0
                            end) LogistManager, --物流管理工
                        sum(case
                              when tmp.name = '清点组组长' and nvl(tmp.nodeid, 0) > 1 then
                               1
                              else
                               0
                            end) InventoryHead, --清点组组长
                        sum(case
                              when tmp.name = '清点组组员' and nvl(tmp.nodeid, 0) > 1 then
                               1
                              else
                               0
                            end) InventoryMember, --清点组组员
                        sum(case
                              when tmp.name = '库内物流工程师' and nvl(tmp.nodeid, 0) > 1 then
                               1
                              else
                               0
                            end) LogisticsWarehouse, --库内物流工程师
                        sum(case
                              when tmp.name = '财务主管' and nvl(tmp.nodeid, 0) > 1 then
                               1
                              else
                               0
                            end) Treasurer, --财务主管
                        sum(case
                              when tmp.name = '财务人员' and nvl(tmp.nodeid, 0) > 1 then
                               1
                              else
                               0
                            end) TreasurerMember, --财务人员
                        sum(case
                              when tmp.name = '服务经理' and nvl(tmp.nodeid, 0) > 1 then
                               1
                              else
                               0
                            end) ServiceManager --服务经理
                   from page a
                  inner join page b
                     on a.id = b.parentid
                  inner join page c
                     on c.parentid = b.id
                 /*inner join action d
                    on d.pageid = c.id*/
                 inner join node
                    on node.categoryid = c.id
                   left join (select rule.nodeid, role.name
                               from rule
                              inner join role
                                 on role.id = rule.roleid
                              where role.enterpriseid = 12730
                                and role.status = 1) tmp
                     on tmp.nodeid = node.id
                  group by a.name, b.name, c.name, tmp.name) tt where 1=1 
                    ";

            if(!string.IsNullOrEmpty(name)) {
                SQL = SQL + " and tt.name like '%" + name + "%'";
            }

            if(!string.IsNullOrEmpty(lV1)) {
                SQL = SQL + " and tt.LV1 like'%" + lV1 + "%'";
            }
            if(!string.IsNullOrEmpty(lV2)) {
                SQL = SQL + " and tt.LV2 like'%" + lV2 + "%'";
            }
            if(!string.IsNullOrEmpty(lV3)) {
                SQL = SQL + " and tt.LV3 like'%" + lV3 + "%'";
            }
            if(!string.IsNullOrEmpty(actionName)) {
                SQL = SQL + " and tt.actionName like '%" + actionName + "%'";
            }

            SQL = SQL + "   group by tt.lV1, tt.lV2, tt.lV3, tt.ActionName   order by tt.lV1, tt.lV2, tt.lV3, tt.ActionName ) mm ";

            var entityConnection = new EntityConnectionStringBuilder(SecurityConnStr).ProviderConnectionString;

            var db = DbHelper.GetDbHelp(entityConnection);
            var result = new List<RolePersonnelReport>(100);
            using(var conn = db.CreateDbConnection()) {
                conn.Open();
                try {
                    var commandTemplate = db.CreateDbCommand(string.Format(SQL, db.ParamMark), conn, null);
                    var reader = commandTemplate.ExecuteReader();
                    while(reader.Read()) {
                        result.Add(new RolePersonnelReport {
                            LV1 = reader[1] == null ? "" : reader[1].ToString(),
                            LV2 = reader[2] == null ? "" : reader[2].ToString(),
                            LV3 = reader[3] == null ? "" : reader[3].ToString(),
                            ActionName = reader[4] == null ? "" : reader[4].ToString(),
                            Rownum = reader[0].ToString(),
                            AccessoriesCenter = reader[5] == null ? "" : reader[5].ToString(),
                            BussinessMananger = reader[6] == null ? "" : reader[6].ToString(),
                            AssistantDirector = reader[7] == null ? "" : reader[7].ToString(),
                            SalesMananger = reader[8] == null ? "" : reader[8].ToString(),
                            SalesFilmMananger = reader[9] == null ? "" : reader[9].ToString(),
                            OutSalesMananger = reader[10] == null ? "" : reader[10].ToString(),
                            ExtraMananger = reader[11] == null ? "" : reader[11].ToString(),
                            ServiceMananger = reader[12] == null ? "" : reader[12].ToString(),
                            BigMananger = reader[13] == null ? "" : reader[13].ToString(),
                            PurchaseMananger = reader[14] == null ? "" : reader[14].ToString(),
                            PlanMananger = reader[15] == null ? "" : reader[15].ToString(),
                            AllocationMananger = reader[16] == null ? "" : reader[16].ToString(),
                            Buyer = reader[18] == null ? "" : reader[17].ToString(),
                            MarketMananger = reader[18] == null ? "" : reader[18].ToString(),
                            PriceMananger = reader[19] == null ? "" : reader[19].ToString(),
                            CommercialMananger = reader[20] == null ? "" : reader[20].ToString(),
                            NetMananger = reader[21] == null ? "" : reader[21].ToString(),
                            MarketCommissioner = reader[22] == null ? "" : reader[22].ToString(),
                            IntegratedHead = reader[23] == null ? "" : reader[23].ToString(),
                            SystemAdministrator = reader[24] == null ? "" : reader[24].ToString(),
                            AssistantSystem = reader[25] == null ? "" : reader[25].ToString(),
                            PurchaseCommissioner = reader[26] == null ? "" : reader[26].ToString(),
                            GeneralCommissioner = reader[27] == null ? "" : reader[27].ToString(),
                            AccessoryTechnician = reader[28] == null ? "" : reader[28].ToString(),
                            LogisticsManager = reader[29] == null ? "" : reader[29].ToString(),
                            LogisticsFuManager = reader[30] == null ? "" : reader[30].ToString(),
                            InBoundManager = reader[31] == null ? "" : reader[31].ToString(),
                            InBoundHead = reader[32] == null ? "" : reader[32].ToString(),
                            InBoundMember = reader[33] == null ? "" : reader[33].ToString(),
                            PackingManager = reader[34] == null ? "" : reader[34].ToString(),
                            PackingHead = reader[35] == null ? "" : reader[35].ToString(),
                            PackingMember = reader[36] == null ? "" : reader[36].ToString(),
                            PackingEngineer = reader[37] == null ? "" : reader[37].ToString(),
                            ReturnSales = reader[38] == null ? "" : reader[38].ToString(),
                            WarehouseManagement = reader[39] == null ? "" : reader[39].ToString(),
                            WarehouseHead = reader[40] == null ? "" : reader[40].ToString(),
                            WarehouseMember = reader[41] == null ? "" : reader[41].ToString(),
                            WarehouseManager = reader[42] == null ? "" : reader[42].ToString(),
                            SupervisorLogistics = reader[43] == null ? "" : reader[43].ToString(),
                            LogistManager = reader[44] == null ? "" : reader[44].ToString(),
                            InventoryHead = reader[45] == null ? "" : reader[45].ToString(),
                            InventoryMember = reader[46] == null ? "" : reader[46].ToString(),
                            LogisticsWarehouse = reader[47] == null ? "" : reader[47].ToString(),
                            Treasurer = reader[48] == null ? "" : reader[48].ToString(),
                            TreasurerMember = reader[49] == null ? "" : reader[49].ToString(),
                            ServiceManager = reader[50] == null ? "" : reader[50].ToString(),
                        });
                    }
                } catch(Exception ex) {
                    if(conn.State == ConnectionState.Open)
                        conn.Close();
                    conn.Dispose();
                    throw ex;
                } finally {
                    if(conn.State == ConnectionState.Open)
                        conn.Close();
                    conn.Dispose();
                }
            }
            return result;
        }
    }
    partial class DcsDomainService {
        public IEnumerable<RolePersonnelReport> 获取角色人员清单(string roleName, string loginId, string name) {
            return new RolePersonnelReportAch(this).获取角色人员清单(roleName, loginId, name);

        }
        public IEnumerable<RolePersonnelReport> 获取企业权限模板(string name, string lV1, string actionName, string lV2, string lV3) {
            return new RolePersonnelReportAch(this).获取企业权限模板(name, lV1, actionName, lV2, lV3);

        }
        public IEnumerable<RolePersonnelReport> 获取角色权限清单(string name, string actionName, string lV1, string lV2, string lV3) {
            return new RolePersonnelReportAch(this).获取角色权限清单(name, actionName, lV1, lV2, lV3);
        }
    }
}
