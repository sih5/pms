﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class SalesCenterstrategyAch : DcsSerivceAchieveBase {
        internal void InsertSalesCenterstrategyValidate(SalesCenterstrategy salesCenterstrategy) {
            var userInfo = Utils.GetCurrentUserInfo();
            salesCenterstrategy.CreatorId = userInfo.Id;
            salesCenterstrategy.CreatorName = userInfo.Name;
            salesCenterstrategy.CreateTime = DateTime.Now;
        }
        internal void UpdateSalesCenterstrategyValidate(SalesCenterstrategy salesCenterstrategy) {
            var dbSalesCenterstrategy = ObjectContext.SalesCenterstrategies.Where(r => r.Id == salesCenterstrategy.Id && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbSalesCenterstrategy == null) {
                throw new ValidationException(ErrorStrings.SalesCenterstrategy_Validation1);
            }
            var userInfo = Utils.GetCurrentUserInfo();
            salesCenterstrategy.ModifierId = userInfo.Id;
            salesCenterstrategy.ModifierName = userInfo.Name;
            salesCenterstrategy.ModifyTime = DateTime.Now;
        }
        public void InsertSalesCenterstrategy(SalesCenterstrategy salesCenterstrategy) {
            InsertToDatabase(salesCenterstrategy);
            this.InsertSalesCenterstrategyValidate(salesCenterstrategy);
        }
        public void UpdateSalesCenterstrategy(SalesCenterstrategy salesCenterstrategy) {
            UpdateToDatabase(salesCenterstrategy);
            this.UpdateSalesCenterstrategyValidate(salesCenterstrategy);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:29
        //原分布类的函数全部转移到Ach                                                               

        public void InsertSalesCenterstrategy(SalesCenterstrategy salesCenterstrategy) {
            new SalesCenterstrategyAch(this).InsertSalesCenterstrategy(salesCenterstrategy);
        }

        public void UpdateSalesCenterstrategy(SalesCenterstrategy salesCenterstrategy) {
            new SalesCenterstrategyAch(this).UpdateSalesCenterstrategy(salesCenterstrategy);
        }
    }
}
