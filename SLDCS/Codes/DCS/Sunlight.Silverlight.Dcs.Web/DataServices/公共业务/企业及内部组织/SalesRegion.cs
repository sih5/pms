﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class SalesRegionAch : DcsSerivceAchieveBase {
        internal void InsertSalesRegionValidate(SalesRegion salesRegion) {
            var dbSalesRegion = ObjectContext.SalesRegions.Where(r => r.BranchId == salesRegion.BranchId && r.PartsSalesCategoryId == salesRegion.PartsSalesCategoryId && r.RegionCode.ToLower() == salesRegion.RegionCode.ToLower() && r.Status != (int)DcsBaseDataStatus.作废).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbSalesRegion != null)
                throw new ValidationException(string.Format(ErrorStrings.SalesRegion_Validation1, dbSalesRegion.RegionCode));
            var userInfo = Utils.GetCurrentUserInfo();
            salesRegion.CreatorId = userInfo.Id;
            salesRegion.CreatorName = userInfo.Name;
            salesRegion.CreateTime = DateTime.Now;
        }
        internal void UpdateSalesRegionValidate(SalesRegion salesRegion) {
            var dbSalesRegion = ObjectContext.SalesRegions.Where(r => r.Id == salesRegion.Id && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbSalesRegion == null)
                throw new ValidationException(ErrorStrings.SalesRegion_Validation2);
            var userInfo = Utils.GetCurrentUserInfo();
            salesRegion.ModifierId = userInfo.Id;
            salesRegion.ModifierName = userInfo.Name;
            salesRegion.ModifyTime = DateTime.Now;
        }
        public void InsertSalesRegion(SalesRegion salesRegion) {
            InsertToDatabase(salesRegion);
            this.InsertSalesRegionValidate(salesRegion);
        }
        public void UpdateSalesRegion(SalesRegion salesRegion) {
            UpdateToDatabase(salesRegion);
            this.UpdateSalesRegionValidate(salesRegion);
        }
        public IQueryable<SalesRegion> GetSalesRegionWithPartsSalesCategory() {
            return ObjectContext.SalesRegions.Include("PartsSalesCategory").OrderBy(v => v.Id);
        }
        public IQueryable<SalesRegion> GetSalesRegionsByPersonel() {
            var userinfo = Utils.GetCurrentUserInfo();
            var salesResgionIds = ObjectContext.RegionPersonnelRelations.Where(ex => ex.PersonnelId == userinfo.EnterpriseId).Select(ex => ex.SalesRegionId);
            return ObjectContext.SalesRegions.Where(ex => salesResgionIds.Contains(ex.Id) && ex.Status == (int)DcsBaseDataStatus.有效).OrderBy(v => v.Id);
        }
        public SalesRegion GetSalesRegionByMarketDepartmentId(int MarketDepartmentId) {
            var regionMarketDptRelation = ObjectContext.RegionMarketDptRelations.FirstOrDefault(ex => ex.MarketDepartmentId == MarketDepartmentId && ex.Status == (int)DcsBaseDataStatus.有效);
            if(regionMarketDptRelation == null)
                return null;
            return ObjectContext.SalesRegions.SingleOrDefault(e => e.Id == regionMarketDptRelation.SalesRegionId && e.Status == (int)DcsBaseDataStatus.有效);
        }

    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:29
        //原分布类的函数全部转移到Ach                                                               

        public void InsertSalesRegion(SalesRegion salesRegion) {
            new SalesRegionAch(this).InsertSalesRegion(salesRegion);
        }

        public void UpdateSalesRegion(SalesRegion salesRegion) {
            new SalesRegionAch(this).UpdateSalesRegion(salesRegion);
        }

        public IQueryable<SalesRegion> GetSalesRegionWithPartsSalesCategory() {
            return new SalesRegionAch(this).GetSalesRegionWithPartsSalesCategory();
        }

        public IQueryable<SalesRegion> GetSalesRegionsByPersonel() {
            return new SalesRegionAch(this).GetSalesRegionsByPersonel();
        }

        public SalesRegion GetSalesRegionByMarketDepartmentId(int MarketDepartmentId) {
            return new SalesRegionAch(this).GetSalesRegionByMarketDepartmentId(MarketDepartmentId);
        }
    }
}
