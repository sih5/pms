﻿
namespace Sunlight.Silverlight.Dcs.Web {
    partial class CompanyDetailAch : DcsSerivceAchieveBase {
        public CompanyDetailAch(DcsDomainService domainService)
            : base(domainService) {
        }

        public void InsertCompanyDetail(CompanyDetail companyDetail) {
            InsertToDatabase(companyDetail);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:29
        //原分布类的函数全部转移到Ach                                                               

        public void InsertCompanyDetail(CompanyDetail companyDetail) {
            new CompanyDetailAch(this).InsertCompanyDetail(companyDetail);
        }
    }
}
