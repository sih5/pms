﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class CountersignatureAch : DcsSerivceAchieveBase {
        internal void InsertCountersignatureValiate(Countersignature countersignature) {
            var dbCountersignature = ObjectContext.Countersignatures.Where(r => r.SourceId == countersignature.SourceId && r.SourceType == countersignature.SourceType).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbCountersignature != null) {
                throw new ValidationException("会签信息已经存在");
            }
            var userInfo = Utils.GetCurrentUserInfo();
            countersignature.CreatorId = userInfo.Id;
            countersignature.CreatorName = userInfo.Name;
            countersignature.CreateTime = DateTime.Now;
        }
        internal void UpdateCountersignatureValiate(Countersignature countersignature) {
        }
        public void InsertCountersignature(Countersignature countersignature) {
            InsertToDatabase(countersignature);
            var countersignatureDetails = ChangeSet.GetAssociatedChanges(countersignature, r => r.CountersignatureDetails, ChangeOperation.Insert);
            foreach(CountersignatureDetail countersignatureDetail in countersignatureDetails) {
                InsertToDatabase(countersignatureDetail);
            }
            InsertCountersignatureValiate(countersignature);
        }
        public void UpdateCountersignature(Countersignature countersignature) {
            countersignature.CountersignatureDetails.Clear();
            UpdateToDatabase(countersignature);
            var countersignatureDetails = ChangeSet.GetAssociatedChanges(countersignature, v => v.CountersignatureDetails);
            foreach(CountersignatureDetail countersignatureDetail in countersignatureDetails) {
                switch(ChangeSet.GetChangeOperation(countersignatureDetail)) {
                    case ChangeOperation.Insert:
                        InsertToDatabase(countersignatureDetail);
                        break;
                    case ChangeOperation.Update:
                        UpdateToDatabase(countersignatureDetail);
                        break;
                    case ChangeOperation.Delete:
                        DeleteFromDatabase(countersignatureDetail);
                        break;
                }
            }
            UpdateCountersignatureValiate(countersignature);
        }


    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:29
        //原分布类的函数全部转移到Ach                                                               

        public void InsertCountersignature(Countersignature countersignature) {
            new CountersignatureAch(this).InsertCountersignature(countersignature);
        }

        public void UpdateCountersignature(Countersignature countersignature) {
            new CountersignatureAch(this).UpdateCountersignature(countersignature);
        }
    }
}
