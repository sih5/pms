﻿
using System.Linq;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class CountersignatureDetailAch : DcsSerivceAchieveBase {
        public CountersignatureDetailAch(DcsDomainService domainService)
            : base(domainService) {
        }

        public IQueryable<CountersignatureDetail> GetCountersignatureDetailsById(string sourceCode, int sourceType, int tpye) {
            var countersignature = ObjectContext.Countersignatures.SingleOrDefault(e => e.SourceCode == sourceCode && e.SourceType == sourceType && e.Tpye == tpye);
            if(countersignature == null)
                return null;
            return ObjectContext.CountersignatureDetails.Where(e => e.CountersignatureId == countersignature.Id).OrderBy(r => r.Id);
        }
        internal void InsertCountersignatureDetailValidate(CountersignatureDetail countersignatureDetail) {
        }
        public void InsertCountersignatureDetail(CountersignatureDetail countersignatureDetail) {
            InsertToDatabase(countersignatureDetail);
            this.InsertCountersignatureDetailValidate(countersignatureDetail);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:29
        //原分布类的函数全部转移到Ach                                                               

        public IQueryable<CountersignatureDetail> GetCountersignatureDetailsById(string sourceCode, int sourceType, int tpye) {
            return new CountersignatureDetailAch(this).GetCountersignatureDetailsById(sourceCode,sourceType,tpye);
        }

        public void InsertCountersignatureDetail(CountersignatureDetail countersignatureDetail) {
            new CountersignatureDetailAch(this).InsertCountersignatureDetail(countersignatureDetail);
        }
    }
}
