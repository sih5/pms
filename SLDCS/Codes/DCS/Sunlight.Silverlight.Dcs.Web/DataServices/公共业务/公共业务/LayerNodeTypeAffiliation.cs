﻿using System.Linq;
using Microsoft.Data.Extensions;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class LayerNodeTypeAffiliationAch : DcsSerivceAchieveBase {
        public LayerNodeTypeAffiliationAch(DcsDomainService domainService)
            : base(domainService) {
        }
        
        public IQueryable<LayerNodeTypeAffiliation> GetLayerNodeTypeAffiliationsWithSubLayerNodeType(int? structureId, int? parentId, bool? isTopLevelNode) {
            IQueryable<LayerNodeTypeAffiliation> result = ObjectContext.LayerNodeTypeAffiliations;
            if(structureId.HasValue)
                result = result.Where(entity => entity.LayerStructureId == structureId.Value);
            if(parentId.HasValue)
                result = result.Where(entity => entity.ParentLayerNodeTypeId == parentId.Value);
            if(isTopLevelNode.HasValue)
                result = result.Where(entity => isTopLevelNode.Value);
            return result.Include("SubLayerNodeType").OrderBy(e => e.Id);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:29
        //原分布类的函数全部转移到Ach                                                               
        
        public IQueryable<LayerNodeTypeAffiliation> GetLayerNodeTypeAffiliationsWithSubLayerNodeType(int? structureId, int? parentId, bool? isTopLevelNode) {
            return new LayerNodeTypeAffiliationAch(this).GetLayerNodeTypeAffiliationsWithSubLayerNodeType(structureId,parentId,isTopLevelNode);
        }
    }
}
