using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using System.Text;
using System.Threading.Tasks;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
using System.Data.Objects;
using Microsoft.Data.Extensions;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class MultiLevelApproveConfigAch : DcsSerivceAchieveBase {
        public MultiLevelApproveConfigAch(DcsDomainService domainService)
            : base(domainService) {
        }
        public void 新增多级审核配置(MultiLevelApproveConfig config) {
            config.ApproverId = config.ApproverIds.Value;
            config.ApproverName = config.ApproverNames;
            this.validationConfig(config);
            var userInfo = Utils.GetCurrentUserInfo();
            config.CreateTime = DateTime.Now;
            config.CreatorId = userInfo.Id;
            config.CreatorName = userInfo.Name;
            config.Status = (int)DcsBaseDataStatus.有效;
            InsertToDatabase(config);
        }

        private void validationConfig(MultiLevelApproveConfig config) {
            var all = ObjectContext.MultiLevelApproveConfigs.Where(r => r.Type == config.Type && r.Id != config.Id && r.Status == (int)DcsBaseDataStatus.有效).ToList();
            var same = ObjectContext.MultiLevelApproveConfigs.Where(r =>  r.Id != config.Id &&r.Type == config.Type&& r.MinApproveFee == config.MinApproveFee && r.MaxApproveFee == config.MaxApproveFee && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).ToList();
            if (same.Count() == 0) {
                var strategies = all.Where(r => (config.MinApproveFee >= r.MinApproveFee && config.MinApproveFee < r.MaxApproveFee)
                || (config.MaxApproveFee > r.MinApproveFee && config.MaxApproveFee < r.MaxApproveFee)
                || (config.MinApproveFee <= r.MinApproveFee && config.MaxApproveFee >= r.MaxApproveFee));
                if (strategies != null && strategies.Count() != 0) {
                    throw new ValidationException("区间不允许重叠");
                }
            } else {
                if (same.Any(r => r.ApproverId == config.ApproverId)) {
                    throw new ValidationException(string.Format("该区间已存在审核人{0}",config.ApproverName));
                }
                if (!same.All(r => r.IsFinished == config.IsFinished)) {
                    throw new ValidationException("是否结束错误");
                }
            }

        }

        public void 更新多级审核配置(MultiLevelApproveConfig config) {
            config.ApproverId = config.ApproverIds.Value;
            config.ApproverName = config.ApproverNames;
            this.validationConfig(config);
            var userInfo = Utils.GetCurrentUserInfo();
            config.ModifyTime = DateTime.Now;
            config.ModifierId = userInfo.Id;
            config.ModifierName = userInfo.Name;            
            UpdateToDatabase(config);
        }

        public void 作废多级审核配置(MultiLevelApproveConfig config) {
            config.Status = (int)DcsBaseDataStatus.作废;
            var userInfo = Utils.GetCurrentUserInfo();
            config.ModifyTime = DateTime.Now;
            config.ModifierId = userInfo.Id;
            config.ModifierName = userInfo.Name;
            UpdateToDatabase(config);
        }

        public void InsertMultiLevelApproveConfig(MultiLevelApproveConfig config) {
        }

        public void UpdateMultiLevelApproveConfig(MultiLevelApproveConfig config) {
        }

    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:09:49
        //原分布类的函数全部转移到Ach                                                               
        [Update(UsingCustomMethod = true)]
       public void  新增多级审核配置(MultiLevelApproveConfig config){
            new MultiLevelApproveConfigAch(this).新增多级审核配置(config);
        }
        [Update(UsingCustomMethod = true)]
        public void 更新多级审核配置(MultiLevelApproveConfig config){
            new MultiLevelApproveConfigAch(this).更新多级审核配置(config);
        }
        [Update(UsingCustomMethod = true)]
        public void 作废多级审核配置(MultiLevelApproveConfig config){
            new MultiLevelApproveConfigAch(this).作废多级审核配置(config);
        }
        public void UpdateMultiLevelApproveConfig(MultiLevelApproveConfig config) {
            new MultiLevelApproveConfigAch(this).UpdateMultiLevelApproveConfig(config);
        }

        public void InsertMultiLevelApproveConfig(MultiLevelApproveConfig config) {
            new MultiLevelApproveConfigAch(this).InsertMultiLevelApproveConfig(config);
        }
    }
}
