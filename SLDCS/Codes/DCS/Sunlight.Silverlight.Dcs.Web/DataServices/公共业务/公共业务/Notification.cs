﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class NotificationAch : DcsSerivceAchieveBase {
        public NotificationAch(DcsDomainService domainService)
            : base(domainService) {
        }

        internal void InsertNotificationValidate(Notification notification) {
            var dbNotification = ObjectContext.Notifications.Where(r => r.Code == notification.Code && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbNotification != null) {
                throw new ValidationException(ErrorStrings.Notification_Validation1);
            }
            if(string.IsNullOrWhiteSpace(notification.Code) || notification.Code == GlobalVar.ASSIGNED_BY_SERVER)
                notification.Code = CodeGenerator.Generate("Notification", notification.CompanyCode);
            var userInfo = Utils.GetCurrentUserInfo();
            notification.CreatorId = userInfo.Id;
            notification.CreatorName = userInfo.Name;
            notification.CreateTime = DateTime.Now;
        }

        internal void UpdateNotificationValidate(Notification notification) {
            var dbNotification = ObjectContext.Notifications.Where(r => r.Id != notification.Id && r.Code == notification.Code && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbNotification != null) {
                throw new ValidationException(ErrorStrings.Notification_Validation1);
            }
            var userInfo = Utils.GetCurrentUserInfo();
            notification.ModifierId = userInfo.Id;
            notification.ModifierName = userInfo.Name;
            notification.ModifyTime = DateTime.Now;
        }

        public void InsertNotification(Notification notification) {
            if(notification.Top.HasValue ? (Convert.ToBoolean(notification.Top)) : false) {
                var dbothernotification = ObjectContext.Notifications.Where(r => r.Id != notification.Id && r.Top == true).FirstOrDefault();
                if(dbothernotification != null) {
                    dbothernotification.Top = false;
                    UpdateToDatabase(dbothernotification);
                    this.UpdateNotificationValidate(dbothernotification);
                }
            }
            InsertToDatabase(notification);
            var notificationLimits = ChangeSet.GetAssociatedChanges(notification, v => v.NotificationLimits, ChangeOperation.Insert);
            foreach(NotificationLimit notificationLimit in notificationLimits) {
                InsertToDatabase(notificationLimit);
            }
            //企业清单
            var companyDetails = ChangeSet.GetAssociatedChanges(notification, v => v.CompanyDetails, ChangeOperation.Insert);
            foreach(CompanyDetail companyDetail in companyDetails) {
                InsertToDatabase(companyDetail);
            }
            this.InsertNotificationValidate(notification);
        }

        public void UpdateNotification(Notification notification) {
            notification.NotificationLimits.Clear();
            notification.CompanyDetails.Clear();
            var userInfo = Utils.GetCurrentUserInfo();
            ObjectContext.ExecuteStoreCommand(String.Format("delete NotificationReply where NotificationId ={0} and CompanyId = {1}", notification.Id, userInfo.EnterpriseId));

            this.UpdateToDatabase(notification);
            var notificationLimits = ChangeSet.GetAssociatedChanges(notification, v => v.NotificationLimits);
            foreach(NotificationLimit notificationLimit in notificationLimits) {
                switch(ChangeSet.GetChangeOperation(notificationLimit)) {
                    case ChangeOperation.Insert:
                        this.InsertToDatabase(notificationLimit);
                        break;
                    case ChangeOperation.Update:
                        this.UpdateToDatabase(notificationLimit);
                        break;
                    case ChangeOperation.Delete:
                        this.DeleteFromDatabase(notificationLimit);
                        break;
                }
            }
            var companyDetails = ChangeSet.GetAssociatedChanges(notification, v => v.CompanyDetails);
            foreach(CompanyDetail companyDetail in companyDetails) {
                switch(ChangeSet.GetChangeOperation(companyDetail)) {
                    case ChangeOperation.Insert:
                        InsertToDatabase(companyDetail);
                        break;
                    case ChangeOperation.Update:
                        UpdateToDatabase(companyDetail);
                        break;
                    case ChangeOperation.Delete:
                        DeleteFromDatabase(companyDetail);
                        break;
                }
            }
            this.UpdateNotificationValidate(notification);

            if(notification.Top.HasValue ? (Convert.ToBoolean(notification.Top)) : false) {
                var dbothernotification = ObjectContext.Notifications.Where(r => r.Id != notification.Id && r.Top == true).FirstOrDefault();
                if(dbothernotification != null) {
                    dbothernotification.Top = false;
                    UpdateToDatabase(dbothernotification);
                    this.UpdateNotificationValidate(dbothernotification);
                }
            }
            var notificationReply = new NotificationReply() {
                NotificationId = notification.Id,
                NotificationCode = notification.Code,
                CompanyId = userInfo.EnterpriseId,
                CompanyCode = userInfo.EnterpriseCode,
                CompanyName = userInfo.EnterpriseName,
                //CompanyName = userInfo.EnterpriseName
                ReplyChannel = 0,
                ReplyContent = notification.ReplyContent,
                ReplyFilePath = notification.ReplyFilePath,
                ReplyDate = DateTime.Now
            };
            this.InsertToDatabase(notificationReply);

        }

        public IQueryable<Notification> GetNotificationWithNotificationLimits(bool? isHasFile, int? companyType) {
            var tempNotifications = from a in ObjectContext.Notifications.Where(r => r.Status == (int)DcsBaseDataStatus.有效)
                                    select a;
            if(companyType.HasValue)
                tempNotifications = tempNotifications.Where(r => r.NotificationLimits.Any(x => x.NoticeId == r.Id && x.DesCompanyTpye == companyType));
            if(isHasFile.HasValue) {
                tempNotifications = isHasFile.Value ? tempNotifications.Where(r => r.Path != null) : tempNotifications.Where(r => r.Path == null);
                return tempNotifications.OrderByDescending(r => r.Top).ThenByDescending(r => r.TopTime).ThenByDescending(r => r.CreateTime);
            }
            return tempNotifications.OrderByDescending(r => r.Top).ThenByDescending(r => r.TopTime).ThenByDescending(r => r.CreateTime);
        }

        public IQueryable<Notification> 目标企业查询公告(int desCompanyId, int desCompanyTpye, bool? isHasFile) {
            var tempNotifications = from a in ObjectContext.Notifications.Where(r => r.Status == (int)DcsBaseDataStatus.有效)
                                    from b in ObjectContext.NotificationLimits.Where(r => r.DesCompanyTpye == desCompanyTpye)
                                    where a.Id == b.NoticeId
                                    select a;
            int tempDesCompanyId1;
            if(isHasFile.HasValue) {
                tempNotifications = isHasFile.Value ? tempNotifications.Where(r => r.Path != null) : tempNotifications.Where(r => r.Path == null);
            }
            if(desCompanyTpye == (int)DcsCompanyType.分公司) {
                tempDesCompanyId1 = desCompanyId;
                tempNotifications = tempNotifications.Where(r => r.CompanyId == tempDesCompanyId1);
                tempNotifications = CompanyDetailFilter(tempNotifications, desCompanyId);
            } else if(desCompanyTpye == (int)DcsCompanyType.服务站) {
                var dealerServiceInfo = ObjectContext.DealerServiceInfoes.Where(r => r.DealerId == desCompanyId && r.Status == (int)DcsMasterDataStatus.有效);
                if(!dealerServiceInfo.Any()) {
                    return null;
                }
                if(dealerServiceInfo.Count() == 1) {
                    tempDesCompanyId1 = dealerServiceInfo.Single().BranchId;
                    tempNotifications = tempNotifications.Where(r => r.CompanyId == tempDesCompanyId1);
                    tempNotifications = CompanyDetailFilter(tempNotifications, desCompanyId);
                } else {
                    tempNotifications = tempNotifications.Where(r => dealerServiceInfo.Select(v => v.BranchId).Distinct().Contains(r.CompanyId));
                    tempNotifications = CompanyDetailFilter(tempNotifications, desCompanyId);
                }
            } else if(desCompanyTpye == (int)DcsCompanyType.代理库) {
                var agencyAffiBranche = ObjectContext.AgencyAffiBranches.Where(r => r.AgencyId == desCompanyId).ToArray();
                if(!agencyAffiBranche.Any()) {
                    return null;
                }
                var tempDesCompanyIds = agencyAffiBranche.Select(r => r.BranchId);
                tempNotifications = tempNotifications.Where(r => tempDesCompanyIds.Contains(r.CompanyId));
                tempNotifications = CompanyDetailFilter(tempNotifications, desCompanyId);
            } else if(desCompanyTpye == (int)DcsCompanyType.服务站兼代理库) {
                var agencyAffiBranche = ObjectContext.AgencyAffiBranches.Where(r => r.AgencyId == desCompanyId).ToArray();
                var dealerServiceInfo = ObjectContext.DealerServiceInfoes.Where(r => r.DealerId == desCompanyId && r.Status == (int)DcsMasterDataStatus.有效).ToArray();
                if(!dealerServiceInfo.Any() || !agencyAffiBranche.Any())
                    return null;
                var agencyAffiBrancheIds = agencyAffiBranche.Select(r => r.BranchId);
                var tempDesCompanyId2s = dealerServiceInfo.Select(r => r.BranchId);
                tempNotifications = tempNotifications.Where(r => agencyAffiBrancheIds.Contains(r.CompanyId) || tempDesCompanyId2s.Contains(r.CompanyId));
                tempNotifications = CompanyDetailFilter(tempNotifications, desCompanyId);
            } else if(desCompanyTpye == (int)DcsCompanyType.物流公司) {
                var logisticCompanyServiceRange = ObjectContext.LogisticCompanyServiceRanges.FirstOrDefault(r => r.LogisticCompanyId == desCompanyId);
                if(logisticCompanyServiceRange == null) {
                    return null;
                }
                tempDesCompanyId1 = logisticCompanyServiceRange.BranchId;
                tempNotifications = tempNotifications.Where(r => r.CompanyId == tempDesCompanyId1);
                tempNotifications = CompanyDetailFilter(tempNotifications, desCompanyId);
            } else if(desCompanyTpye == (int)DcsCompanyType.责任单位) {
                var responsibleUnitBranch = ObjectContext.ResponsibleUnitBranches.FirstOrDefault(r => r.ResponsibleUnitId == desCompanyId);
                if(responsibleUnitBranch == null) {
                    return null;
                }
                tempDesCompanyId1 = responsibleUnitBranch.BranchId;
                tempNotifications = tempNotifications.Where(r => r.CompanyId == tempDesCompanyId1);
                tempNotifications = CompanyDetailFilter(tempNotifications, desCompanyId);
            } else if(desCompanyTpye == (int)DcsCompanyType.配件供应商) {
                var branchSupplierRelation = ObjectContext.BranchSupplierRelations.FirstOrDefault(r => r.SupplierId == desCompanyId && r.Status == (int)DcsBaseDataStatus.有效);
                if(branchSupplierRelation == null) {
                    return null;
                }
                tempDesCompanyId1 = branchSupplierRelation.BranchId;
                tempNotifications = tempNotifications.Where(r => r.CompanyId == tempDesCompanyId1);
                tempNotifications = CompanyDetailFilter(tempNotifications, desCompanyId);
            }
            return tempNotifications.Distinct().Include("NotificationLimits").OrderByDescending(r => r.Top).ThenByDescending(r => r.TopTime).ThenByDescending(r => r.CreateTime);
        }

        public IQueryable<Notification> GetNotificationDetail() {
            return ObjectContext.Notifications.Include("NotificationLimits").Include("CompanyDetails").Include("NotificationReply").OrderBy(e => e.Id);
        }

        internal IQueryable<Notification> CompanyDetailFilter(IQueryable<Notification> tempNotifications, int desCompanyId) {
            var tempNotificationstemp = tempNotifications;
            var hasCompanyDetail = from a in tempNotifications
                                   join b in ObjectContext.CompanyDetails on a.Id equals b.NoticeId
                                   select b;
            if(hasCompanyDetail.Any()) {
                var noticeIds2 = hasCompanyDetail.Where(r => r.CompanyId == desCompanyId).Select(r => r.NoticeId);
                var noticeIds = hasCompanyDetail.Select(r => r.NoticeId);
                var tempNotifications1 = tempNotifications.Where(r => noticeIds2.Contains(r.Id));
                var tempNotifications2 = tempNotificationstemp.Where(r => !noticeIds.Contains(r.Id));
                tempNotifications = tempNotifications1.Union(tempNotifications2);
            }
            return tempNotifications;
        }

    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:29
        //原分布类的函数全部转移到Ach                                                               

        public void InsertNotification(Notification notification) {
            new NotificationAch(this).InsertNotification(notification);
        }

        public void UpdateNotification(Notification notification) {
            new NotificationAch(this).UpdateNotification(notification);
        }

        public IQueryable<Notification> GetNotificationWithNotificationLimits(bool? isHasFile, int? companyType) {
            return new NotificationAch(this).GetNotificationWithNotificationLimits(isHasFile, companyType);
        }

        public IQueryable<Notification> 目标企业查询公告(int desCompanyId, int desCompanyTpye, bool? isHasFile) {
            return new NotificationAch(this).目标企业查询公告(desCompanyId, desCompanyTpye, isHasFile);
        }

        public IQueryable<Notification> GetNotificationDetail() {
            return new NotificationAch(this).GetNotificationDetail();
        }
    }
}
