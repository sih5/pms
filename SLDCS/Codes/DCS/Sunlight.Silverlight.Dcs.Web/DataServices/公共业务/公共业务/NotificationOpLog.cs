﻿using System;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class NotificationOpLogAch : DcsSerivceAchieveBase {
        public NotificationOpLogAch(DcsDomainService domainService)
            : base(domainService) {
        }

        internal void InsertNotificationOpLogValidate(NotificationOpLog notificationOpLog) {
            var userInfo = Utils.GetCurrentUserInfo();
            notificationOpLog.CreatorId = userInfo.Id;
            notificationOpLog.CreatorName = userInfo.Name;
            notificationOpLog.CreateTime = DateTime.Now;
        }
        public void InsertNotificationOpLog(NotificationOpLog notificationOpLog) {
            InsertToDatabase(notificationOpLog);
            this.InsertNotificationOpLogValidate(notificationOpLog);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:29
        //原分布类的函数全部转移到Ach                                                               

        public void InsertNotificationOpLog(NotificationOpLog notificationOpLog) {
            new NotificationOpLogAch(this).InsertNotificationOpLog(notificationOpLog);
        }
    }
}
