﻿
using System.Linq;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class NotificationReplyAch : DcsSerivceAchieveBase {
        public NotificationReplyAch(DcsDomainService domainService)
            : base(domainService) {
        }

        public IQueryable<NotificationReply> GetNotificationRepliesWithSerialNumber() {
            int serialNumber = 1;
            var notificationReplies = ObjectContext.NotificationReplies.OrderBy(e => e.id);
            foreach(var notificationReply in notificationReplies) {
                notificationReply.SerialNumber = serialNumber++;
            }
            return notificationReplies;
        }
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:29
        //原分布类的函数全部转移到Ach                                                               
        public IQueryable<NotificationReply> GetNotificationRepliesWithSerialNumber() {
            return new NotificationReplyAch(this).GetNotificationRepliesWithSerialNumber();
        }
    }
}
