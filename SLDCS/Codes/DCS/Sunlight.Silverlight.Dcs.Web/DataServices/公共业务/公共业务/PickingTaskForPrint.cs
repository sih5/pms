﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class PickingTaskForPrintAch : DcsSerivceAchieveBase {
        public PickingTaskForPrintAch(DcsDomainService domainService)
            : base(domainService)
        {
        }
        //根据人员获取库区信息
        public IQueryable<WarehouseArea> queryWarehouseAreaForPersonel() {
            var userInfo = Utils.GetCurrentUserInfo();
            return ObjectContext.WarehouseAreas.Where(r => ObjectContext.WarehouseAreaManagers.Any(t => t.ManagerId == userInfo.Id && r.TopLevelWarehouseAreaId==t.WarehouseAreaId) && r.Status == (int)DcsBaseDataStatus.有效);

        }
        public IEnumerable<PickingTaskForPrint> GetPickingTaskForPrintId(int id, int? warehouseArea) {
            var userInfo = Utils.GetCurrentUserInfo();
            string SQL = @"select p.code,
                           tw.code as WarehouseAreaName,
                           sum(d.PlanQty-nvl(d.PickingQty,0)) as pickingqty,
                           count(d.id) as Varieties,
                           p.id,
                          tw.id as WarehouseArea
                          from pickingtask p
                         join pickingtaskdetail d
                         on p.id = d.pickingtaskid
                        join warehousearea w
                       on d.warehouseareaid = w.id
                       join WarehouseAreaManager m
                       on w.TopLevelWarehouseAreaId = m.warehouseareaid
                        left join warehousearea tw on tw.id = w.toplevelwarehouseareaid
                       where p.id=" + id + " and m.ManagerId =" + userInfo.Id;
            if(warehouseArea.HasValue) {
                SQL = SQL + " and tw.id ="+warehouseArea;
            }
            SQL = SQL + " group by p.id, p.code, tw.code,tw.id  order by tw.code asc";
            var search = ObjectContext.ExecuteStoreQuery<PickingTaskForPrint>(SQL).ToList();
         
            return search;
        }
    }
    partial class DcsDomainService {
        public IEnumerable<PickingTaskForPrint> GetPickingTaskForPrintId(int id, int? warehouseArea) {
            return new PickingTaskForPrintAch(this).GetPickingTaskForPrintId(id, warehouseArea);
        }
        public IQueryable<WarehouseArea> queryWarehouseAreaForPersonel() {
            return new PickingTaskForPrintAch(this).queryWarehouseAreaForPersonel();
        }
    }
}
