﻿using System;
using System.Linq;

namespace Sunlight.Silverlight.Dcs.Web
{
    partial class RegionAch : DcsSerivceAchieveBase
    {
        public RegionAch(DcsDomainService domainService)
            : base(domainService)
        {
        }

        public IQueryable<Region> 查询存在未结算单据的区域(int companyId, int? warehouseId, DateTime? endTime, int? accountGroupId, int? settleType, int? businessType,bool? isIol)
        {
            IQueryable<PartsOutboundBill> bill = ObjectContext.PartsOutboundBills;
            if(isIol.HasValue) {
                if(isIol.Value) {
                    bill = ObjectContext.PartsOutboundBills.Where(r => ObjectContext.PartsSalesOrderTypes.Any(ps => r.PartsSalesOrderTypeId == ps.Id && ps.Name == "油品订单"));
                } else {
                    bill = ObjectContext.PartsOutboundBills.Where(r => ObjectContext.PartsSalesOrderTypes.Any(ps => r.PartsSalesOrderTypeId == ps.Id && ps.Name != "油品订单"));
                }
            } else {
                bill = ObjectContext.PartsOutboundBills;
            }
            var partsOutboundBills = bill.Where(r => r.SettlementStatus == (int)DcsClaimBillSettlementStatus.待结算 && r.StorageCompanyId == companyId && (warehouseId == null || r.WarehouseId == warehouseId) && (!endTime.HasValue || r.CreateTime <= endTime) && (!accountGroupId.HasValue || ObjectContext.CustomerAccounts.Any(v => v.Id == r.CustomerAccountId && v.AccountGroupId == accountGroupId && ObjectContext.PartsSalesOrderTypes.Any(ps => r.PartsSalesOrderTypeId == ps.Id && ps.SettleType == settleType && ps.BusinessType == businessType)))).Select(r => new
            {
                r.CounterpartCompanyId,
                r.StorageCompanyId
            });
            var tempStruct = partsOutboundBills;
            var regions = ObjectContext.Regions.Where(r => ObjectContext.Companies.Any(v => v.RegionId == r.Id && v.Status == (int)DcsMasterDataStatus.有效 && tempStruct.Any(k => k.CounterpartCompanyId == v.Id && k.StorageCompanyId == companyId)));
            var regionsView = from a in ObjectContext.Regions.Where(r => r.Type == (int)DcsRegionType.省 || r.Type == (int)DcsRegionType.市 || r.Type == (int)DcsRegionType.县_区)
                              join b in ObjectContext.Regions.Where(r => r.Type == (int)DcsRegionType.省 || r.Type == (int)DcsRegionType.市 || r.Type == (int)DcsRegionType.县_区) on a.ParentId equals b.Id into tempTable1
                              from t1 in tempTable1.DefaultIfEmpty()
                              join c in ObjectContext.Regions.Where(r => r.Type == (int)DcsRegionType.省 || r.Type == (int)DcsRegionType.市 || r.Type == (int)DcsRegionType.县_区) on t1.ParentId equals c.Id into tempTable2
                              from t2 in tempTable2.DefaultIfEmpty()
                              select new
                              {
                                  a.Id,
                                  provinceId = a.Type == (int)DcsRegionType.省 ? a.Id : a.Type == (int)DcsRegionType.市 ? t1.Id : a.Type == (int)DcsRegionType.县_区 ? t2.Id : 0,
                                  provinceName = a.Type == (int)DcsRegionType.省 ? a.Name : a.Type == (int)DcsRegionType.市 ? t1.Name : a.Type == (int)DcsRegionType.县_区 ? t2.Name : "",
                              };
            var tempResult = from a in regions
                             from b in regionsView
                             where a.Id == b.Id
                             select new
                             {
                                 b.provinceId,
                                 b.provinceName
                             };
            var result = from a in ObjectContext.Regions
                         from b in tempResult.Distinct()
                         where a.Id == b.provinceId
                         select a;
            return result.Distinct().OrderBy(r => r.Id);
        }

    }

    partial class DcsDomainService
    {// 已经使用工具进行处理 2017/4/29 12:12:29
        //原分布类的函数全部转移到Ach                                                               

        public IQueryable<Region> 查询存在未结算单据的区域(int companyId, int? warehouseId, DateTime? endTime, int? accountGroupId, int? settleType, int? businessType,bool? isIol)
        {
            return new RegionAch(this).查询存在未结算单据的区域(companyId, warehouseId, endTime, accountGroupId, settleType, businessType, isIol);
        }
    }
}
