﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class ReserveFactorMasterOrderAch : DcsSerivceAchieveBase {
        public IQueryable<ReserveFactorMasterOrder> GetReserveFactorMasterOrderWithDetails() {
            return ObjectContext.ReserveFactorMasterOrders.Include("ReserveFactorOrderDetails").OrderBy(r => r.Id);
        }
        public void InsertReserveFactorMasterOrder(ReserveFactorMasterOrder reserveFactorMasterOrder) {
        }
        public void UpdateReserveFactorMasterOrder(ReserveFactorMasterOrder reserveFactorMasterOrder) {           
        }
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:29
        //原分布类的函数全部转移到Ach                                                               

        public IQueryable<ReserveFactorMasterOrder> GetReserveFactorMasterOrderWithDetails() {
            return new ReserveFactorMasterOrderAch(this).GetReserveFactorMasterOrderWithDetails();
        }
        public void InsertReserveFactorMasterOrder(ReserveFactorMasterOrder reserveFactorMasterOrder) {
            new ReserveFactorMasterOrderAch(this).InsertReserveFactorMasterOrder(reserveFactorMasterOrder);
        }
        public void UpdateReserveFactorMasterOrder(ReserveFactorMasterOrder reserveFactorMasterOrder) {
            new ReserveFactorMasterOrderAch(this).UpdateReserveFactorMasterOrder(reserveFactorMasterOrder);
        }
    }
}
