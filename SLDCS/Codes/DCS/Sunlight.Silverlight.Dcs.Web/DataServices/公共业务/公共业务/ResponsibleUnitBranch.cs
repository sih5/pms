﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class ResponsibleUnitBranchAch : DcsSerivceAchieveBase {
        public ResponsibleUnitBranchAch(DcsDomainService domainService)
            : base(domainService) {
        }

        internal void InsertResponsibleUnitBranchValidate(ResponsibleUnitBranch responsibleUnitBranch) {
            var dbResponsibleUnitBranch = ObjectContext.ResponsibleUnitBranches.Where(r => r.BranchId == responsibleUnitBranch.BranchId && r.ResponsibleUnitId == responsibleUnitBranch.ResponsibleUnitId).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbResponsibleUnitBranch != null) {
                throw new ValidationException(ErrorStrings.ResponsibleUnitBranch_Validation1);
            }
        }
        internal void UpdateResponsibleUnitBranchValidate(ResponsibleUnitBranch responsibleUnitBranch) {
            var dbResponsibleUnitBranch = ObjectContext.ResponsibleUnitBranches.Where(r => r.Id != responsibleUnitBranch.Id && r.BranchId == responsibleUnitBranch.BranchId && r.ResponsibleUnitId == responsibleUnitBranch.ResponsibleUnitId).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbResponsibleUnitBranch != null) {
                throw new ValidationException(ErrorStrings.ResponsibleUnitBranch_Validation1);
            }
        }
        public void InsertResponsibleUnitBranch(ResponsibleUnitBranch responsibleUnitBranch) {
            InsertToDatabase(responsibleUnitBranch);
            this.InsertResponsibleUnitBranchValidate(responsibleUnitBranch);
        }
        public void UpdateResponsibleUnitBranch(ResponsibleUnitBranch responsibleUnitBranch) {
            UpdateToDatabase(responsibleUnitBranch);
            this.UpdateResponsibleUnitBranchValidate(responsibleUnitBranch);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:29
        //原分布类的函数全部转移到Ach                                                               

        public void InsertResponsibleUnitBranch(ResponsibleUnitBranch responsibleUnitBranch) {
            new ResponsibleUnitBranchAch(this).InsertResponsibleUnitBranch(responsibleUnitBranch);
        }

        public void UpdateResponsibleUnitBranch(ResponsibleUnitBranch responsibleUnitBranch) {
            new ResponsibleUnitBranchAch(this).UpdateResponsibleUnitBranch(responsibleUnitBranch);
        }
    }
}
