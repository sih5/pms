﻿using System.Linq;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class ScheduleExportStateAch : DcsSerivceAchieveBase {
        public ScheduleExportStateAch(DcsDomainService domainService)
            : base(domainService) {
        }

        public IQueryable<ScheduleExportState> GetScheduleExportStatesFilterEnterpriseCreator() {
            var user = Utils.GetCurrentUserInfo();
            return ObjectContext.ScheduleExportStates.Where(s => s.EnterpriseId == user.EnterpriseId && s.CreatorId == user.Id).OrderByDescending(s => s.ScheduleTime);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:29
        //原分布类的函数全部转移到Ach                                                               

        public IQueryable<ScheduleExportState> GetScheduleExportStatesFilterEnterpriseCreator() {
            return new ScheduleExportStateAch(this).GetScheduleExportStatesFilterEnterpriseCreator();
        }
    }
}