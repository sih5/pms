﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class SmartOrderCalendarAch : DcsSerivceAchieveBase {
        internal void InsertSmartOrderCalendarValidate(SmartOrderCalendar smartOrderCalendar) {
            var dbSmartOrderCalendar = ObjectContext.SmartOrderCalendars.Where(r => r.WarehouseId == smartOrderCalendar.WarehouseId && r.Times == smartOrderCalendar.Times && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbSmartOrderCalendar != null) {
                throw new ValidationException("已存在相同有效的数据");
            }
            var userInfo = Utils.GetCurrentUserInfo();
            smartOrderCalendar.CreatorId = userInfo.Id;
            smartOrderCalendar.CreatorName = userInfo.Name;
            smartOrderCalendar.CreateTime = DateTime.Now;
            InsertToDatabase(smartOrderCalendar);
        }

        internal void UpdateSmartOrderCalendarValidate(SmartOrderCalendar smartOrderCalendar) {           
            var userInfo = Utils.GetCurrentUserInfo();
            smartOrderCalendar.ModifierId = userInfo.Id;
            smartOrderCalendar.ModifierName = userInfo.Name;
            smartOrderCalendar.ModifyTime = DateTime.Now;
        }
        public void InsertSmartOrderCalendar(SmartOrderCalendar smartOrderCalendar) {
            this.InsertSmartOrderCalendarValidate(smartOrderCalendar);
        }
        public void UpdateSmartOrderCalendar(SmartOrderCalendar smartOrderCalendar) {
            var dbSmartOrderCalendar = ObjectContext.SmartOrderCalendars.Where(r => r.Id != smartOrderCalendar.Id && r.WarehouseId == smartOrderCalendar.WarehouseId && r.Times == smartOrderCalendar.Times && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbSmartOrderCalendar != null) {
                throw new ValidationException("已存在相同有效的数据");
            }
            this.UpdateSmartOrderCalendarValidate(smartOrderCalendar);
        }
        public void 作废OrderCalendar(SmartOrderCalendar smartOrderCalendar) {
            var old = ObjectContext.SmartOrderCalendars.Where(t => t.Id == smartOrderCalendar.Id).FirstOrDefault();
            if(old.Status != (int)DcsBaseDataStatus.有效) {
                throw new ValidationException("数据已作废，请重新查询");
            }
            var userInfo = Utils.GetCurrentUserInfo();
            old.Status = (int)DcsBaseDataStatus.作废;
            old.AbandonerId = userInfo.Id;
            old.AbandonerName = userInfo.Name;
            old.AbandonTime = DateTime.Now;
            this.UpdateSmartOrderCalendarValidate(old);
            UpdateToDatabase(old);
            ObjectContext.SaveChanges();
        }
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:29
        //原分布类的函数全部转移到Ach                                                               

        public void InsertSmartOrderCalendar(SmartOrderCalendar smartOrderCalendar) {
            new SmartOrderCalendarAch(this).InsertSmartOrderCalendar(smartOrderCalendar);
        }

        public void UpdateSmartOrderCalendar(SmartOrderCalendar smartOrderCalendar) {
            new SmartOrderCalendarAch(this).UpdateSmartOrderCalendar(smartOrderCalendar);
        }
        [Update(UsingCustomMethod = true)]
        public void 作废OrderCalendar(SmartOrderCalendar smartOrderCalendar) {
            new SmartOrderCalendarAch(this).作废OrderCalendar(smartOrderCalendar);
        }
    }
}
