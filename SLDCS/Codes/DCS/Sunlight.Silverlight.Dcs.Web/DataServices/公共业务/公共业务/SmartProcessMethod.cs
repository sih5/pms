﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class SmartProcessMethodAch : DcsSerivceAchieveBase {
        internal void InsertSmartProcessMethodValidate(SmartProcessMethod smartProcessMethod) {
            var dbsmartProcessMethod = ObjectContext.SmartProcessMethods.Where(r => r.WarehouseId == smartProcessMethod.WarehouseId && r.OrderProcessMethod == smartProcessMethod.OrderProcessMethod && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbsmartProcessMethod != null) {
                throw new ValidationException("已存在相同有效的数据");
            }
            var userInfo = Utils.GetCurrentUserInfo();
            smartProcessMethod.CreatorId = userInfo.Id;
            smartProcessMethod.CreatorName = userInfo.Name;
            smartProcessMethod.CreateTime = DateTime.Now;
            InsertToDatabase(smartProcessMethod);
        }
        public void InsertSmartProcessMethod(SmartProcessMethod smartProcessMethod) {
        }
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:29
        //原分布类的函数全部转移到Ach                                                               

        public void InsertSmartProcessMethod(SmartProcessMethod smartProcessMethod) {
            new SmartProcessMethodAch(this).InsertSmartProcessMethod(smartProcessMethod);
        }
    }
}
