using System;
using System.Collections.Generic;
using System.Linq;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        public IEnumerable<KeyValueItem> GetKeyValueItemsBy(string category, string name, bool? isBuiltIn) {
            return GetKeyValueItems(category, name, isBuiltIn);
        }

        private IEnumerable<KeyValueItem> GetKeyValueItems(string category, string name, bool? isBuiltIn) {
            if(category == null)
                throw new ArgumentNullException("category");
            if(name == null)
                throw new ArgumentNullException("name");

            var result = ObjectContext.KeyValueItems.Where(kv => kv.Status == (int)DcsBaseDataStatus.��Ч && kv.Category == category && kv.Name == name);
            if(isBuiltIn != null)
                result = result.Where(kv => kv.IsBuiltIn == isBuiltIn.Value);
            return result;
        }

        public IEnumerable<KeyValueItem> GetKeyValueItemsByMultipleNames(string category, string[] names, bool? isBuiltIn) {
            if(category == null)
                throw new ArgumentNullException("category");
            if(names == null)
                throw new ArgumentNullException("names");

            switch(names.Length) {
                case 0:
                    return Enumerable.Empty<KeyValueItem>();
                case 1:
                    return GetKeyValueItems(category, names[0], isBuiltIn);
            }

            var result = ObjectContext.KeyValueItems.Where(kv => kv.Status == (int)DcsBaseDataStatus.��Ч && kv.Category == category && names.Contains(kv.Name));
            if(isBuiltIn != null)
                result = result.Where(kv => kv.IsBuiltIn == isBuiltIn.Value);
            return result;
        }
    }
}
