﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web{
    partial class WorkGruopPersonNumberAch : DcsSerivceAchieveBase {
        public WorkGruopPersonNumberAch(DcsDomainService domainService)
            : base(domainService) {
        }
        public void InsertiWorkGruopPersonNumber(WorkGruopPersonNumber workGruopPersonNumber) {
            if(ObjectContext.WorkGruopPersonNumbers.Any(r=>r.WorkGruop==workGruopPersonNumber.WorkGruop )){
                throw new ValidationException(ErrorStrings.Common_Validation1);
            }
            var userInfo = Utils.GetCurrentUserInfo();
            workGruopPersonNumber.CreatorId = userInfo.Id;
            workGruopPersonNumber.CreatorName = userInfo.Name;
            workGruopPersonNumber.CreateTime = DateTime.Now;
            InsertToDatabase(workGruopPersonNumber);
        }
        public void UpdateWorkGruopPersonNumber(WorkGruopPersonNumber workGruopPersonNumber) {
            var userInfo = Utils.GetCurrentUserInfo();
            workGruopPersonNumber.ModifierId = userInfo.Id;
            workGruopPersonNumber.ModifierName = userInfo.Name;
            workGruopPersonNumber.ModifyTime = DateTime.Now;
            UpdateToDatabase(workGruopPersonNumber);
        }
    }
    partial class DcsDomainService {
        public void InsertiWorkGruopPersonNumber(WorkGruopPersonNumber workGruopPersonNumber) {
            new WorkGruopPersonNumberAch(this).InsertiWorkGruopPersonNumber(workGruopPersonNumber);
        }

        public void UpdateWorkGruopPersonNumber(WorkGruopPersonNumber workGruopPersonNumber) {
            new WorkGruopPersonNumberAch(this).UpdateWorkGruopPersonNumber(workGruopPersonNumber);
        }
    }
}
