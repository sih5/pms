﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class ABCStrategyAch : DcsSerivceAchieveBase {
        internal void InsertABCStrategyValidate(ABCStrategy aBCStrategy) {
            var dbsparePart = ObjectContext.ABCStrategies.Where(r => r.BranchId == aBCStrategy.BranchId && r.Category == aBCStrategy.Category && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbsparePart != null)
                throw new ValidationException(string.Format(ErrorStrings.ABCStrategy_Validation1, aBCStrategy.BranchName, Enum.GetName(typeof(DcsABCStrategyCategory), aBCStrategy.Category)));
            var userInfo = Utils.GetCurrentUserInfo();
            aBCStrategy.CreatorId = userInfo.Id;
            aBCStrategy.CreatorName = userInfo.Name;
            aBCStrategy.CreateTime = DateTime.Now;
        }
        internal void UpdateABCStrategyValidate(ABCStrategy aBCStrategy) {
            var userInfo = Utils.GetCurrentUserInfo();
            aBCStrategy.ModifierId = userInfo.Id;
            aBCStrategy.ModifierName = userInfo.Name;
            aBCStrategy.ModifyTime = DateTime.Now;
        }
        public void InsertABCStrategy(ABCStrategy aBCStrategy) {
            InsertToDatabase(aBCStrategy);
            this.InsertABCStrategyValidate(aBCStrategy);
        }
        public void UpdateABCStrategy(ABCStrategy aBCStrategy) {
            UpdateToDatabase(aBCStrategy);
            this.UpdateABCStrategyValidate(aBCStrategy);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:29
        //原分布类的函数全部转移到Ach                                                               

        public void InsertABCStrategy(ABCStrategy aBCStrategy) {
            new ABCStrategyAch(this).InsertABCStrategy(aBCStrategy);
        }

        public void UpdateABCStrategy(ABCStrategy aBCStrategy) {
            new ABCStrategyAch(this).UpdateABCStrategy(aBCStrategy);
        }
    }
}
