﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
using System.Configuration;
using System.Data;
using System.Text;
using Devart.Data.Oracle;
using NPOI.SS.Formula.Functions;
using System.Transactions;
using System.Data.Objects.SqlClient;

namespace Sunlight.Silverlight.Dcs.Web
{
    partial class AllSparePartAch : DcsSerivceAchieveBase
    {
        public AllSparePartAch(DcsDomainService domainService)
            : base(domainService)
        {
        }
        public IEnumerable<AllSparePart> getAllSpareParts(string code, string referenceCode, int? partABC, int? increaseRateGroupId, bool? isSalable, bool? isOrderable, bool? isPrimary, bool? isEffective)
        {
            string SQL = @"SELECT 
                       Extent1.Id,
                       Extent1.Code,
                       Extent1.Name,
                       Extent1.ReferenceCode,
                       Extent4.SupplierPartCode,
                       Extent1.EnglishName,
                       Extent5.PurchasePrice,
                       Extent1.MeasureUnit,
                       Extent6.Code AS PartsSupplierCode,
                       Extent6.Name AS PartsSupplierName,
                       Extent4.IsPrimary,
                       CASE WHEN Extent5.PriceType = 2 THEN 1 ELSE 0 END AS IsZg,
                        Extent2.BreakTime,
                        Extent2.IsAccreditPack,
                        nvl(Extent4.MinBatch,0) as PackingCoefficient,
                        Extent1.MInPackingAmount,
                        Extent4.OrderCycle,
                        Extent7.ArrivalCycle,
                        Extent8.RetailGuidePrice,
                        Extent9.CenterPrice,
                        Extent9.SalesPrice,
                        Extent2.PartABC,
                        Extent2.IsSalable,
                        Extent2.IsOrderable,
                        Extent10.GroupName,
                        Extent1.PartType,
                        Extent2.PurchaseRoute,
                        Extent2.PartsWarrantyLong,
                        to_char(Extent5.ValidFrom,'yyyy/mm/dd ')||'-'||to_char(Extent5.ValidTo,'yyyy/mm/dd ') as Htyxq, 
                        Extent2.Id As PartsBranchId,
                        Extent4.Id AS PartsSupplierRelationId,
                        Extent5.Id AS PartsPurchasePricingId,
                        Extent6.Id AS PartsSupplierId,
                        Extent7.Id AS BranchSupplierRelationId,
                        Extent8.Id AS PartsRetailGuidePriceId,
                        Extent9.Id AS PartsSalesPriceId,
                        Extent10.Id AS PartsSalePriceIncreaseRateId,
                        Extent1.IsSupplierPutIn,Extent4.OrderGoodsCycle
                        FROM  SparePart Extent1
                        left JOIN PartsBranch Extent2 ON Extent2.Status = 1 and Extent1.Id = Extent2.PartId
                        LEFT OUTER JOIN PartsSupplierRelation Extent4 ON (Extent1.Id = Extent4.PartId) AND (Extent4.Status = 1) and Extent2.PartsSalesCategoryId=Extent4.PartsSalesCategoryId
                        LEFT OUTER JOIN PartsPurchasePricing Extent5 ON (((Extent1.Id = Extent5.PartId) AND (Extent4.BranchId = Extent5.BranchId)) AND (Extent5.Status = 2)) AND (Extent4.SupplierId = Extent5.PartsSupplierId) and Extent5.Validfrom<=sysdate and Extent5.Validto>=sysdate and Extent2.PartsSalesCategoryId=Extent5.PartsSalesCategoryId
                        Left JOIN PartsSupplier Extent6 ON Extent4.SupplierId = Extent6.Id
                        LEFT OUTER JOIN BranchSupplierRelation Extent7 ON ((Extent2.BranchId = Extent7.BranchId) AND (Extent7.Status = 1)) AND (Extent4.SupplierId = Extent7.SupplierId)  and Extent2.PartsSalesCategoryId=Extent7.PartsSalesCategoryId
                        LEFT OUTER JOIN PartsRetailGuidePrice Extent8 ON ((Extent1.Id = Extent8.SparePartId) AND (Extent2.BranchId = Extent8.BranchId)) AND (Extent8.Status = 1) and Extent2.PartsSalesCategoryId=Extent8.PartsSalesCategoryId
                        LEFT OUTER JOIN PartsSalesPrice Extent9 ON ((Extent1.Id = Extent9.SparePartId) AND (Extent2.BranchId = Extent9.BranchId)) AND (Extent9.Status = 1) and Extent2.PartsSalesCategoryId=Extent8.PartsSalesCategoryId
                        LEFT OUTER JOIN PartsSalePriceIncreaseRate Extent10 ON Extent2.IncreaseRateGroupId = Extent10.Id
                    WHERE  Extent1.status=1 ";
            if (!string.IsNullOrEmpty(code))
            {
                SQL = SQL + " and LOWER(Extent1.code) like '%" + code.ToLower() + "%'";
            }
            if (!string.IsNullOrEmpty(referenceCode))
            {
                SQL = SQL + " and LOWER(Extent1.referenceCode) like '%" + referenceCode.ToLower() + "%'";
            }
            if (partABC.HasValue)
            {
                SQL = SQL + " and Extent2.partABC=" + partABC.Value;
            }
            if (increaseRateGroupId.HasValue)
            {
                SQL = SQL + " and Extent10.Id=" + increaseRateGroupId.Value;
            }
            if (isSalable.HasValue && isSalable.Value)
            {
                SQL = SQL + " and Extent2.isSalable=" + 1;
            }
            else if (isSalable.HasValue && !isSalable.Value)
            {
                SQL = SQL + " and Extent2.isSalable=" + 0;
            }
            if (isOrderable.HasValue && isOrderable.Value)
            {
                SQL = SQL + " and Extent2.isOrderable=" + 1;
            }
            else if (isOrderable.HasValue && !isOrderable.Value)
            {
                SQL = SQL + " and Extent2.isOrderable=" + 0;
            }
            if (isPrimary.HasValue && isPrimary.Value)
            {
                SQL = SQL + " and Extent4.isPrimary=" + 1;
            }
            else if (isPrimary.HasValue && !isPrimary.Value)
            {
                SQL = SQL + " and Extent4.isPrimary=" + 0;
            }
            if(isEffective.HasValue){
                if(isEffective.Value) {
                    SQL = SQL + " and Extent5.ValidFrom<sysdate and sysdate< Extent5.ValidTo";
                } else {
                    SQL = SQL + " and (Extent5.ValidFrom>sysdate or sysdate>Extent5.ValidTo )";
                }
            }
            var search = ObjectContext.ExecuteStoreQuery<AllSparePart>(SQL).ToList();
            return search;
        }
    }
    partial class DcsDomainService
    {
        public IEnumerable<AllSparePart> getAllSpareParts(string code, string referenceCode, int? partABC, int? increaseRateGroupId, bool? isSalable, bool? isOrderable, bool? isPrimary, bool? isEffective)
        {
            return new AllSparePartAch(this).getAllSpareParts(code, referenceCode, partABC, increaseRateGroupId, isSalable, isOrderable, isPrimary, isEffective);
        }
    }
}
