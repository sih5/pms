﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class AssemblyPartRequisitionLinkAch : DcsSerivceAchieveBase {

        internal void InsertAssemblyPartRequisitionLinkValidate(AssemblyPartRequisitionLink assemblyPartRequisitionLink) {
            var dbAssemblyPartRequisitionLinks = ObjectContext.AssemblyPartRequisitionLinks.Where(r =>r.Status == (int)DcsBaseDataStatus.有效 && r.Id != assemblyPartRequisitionLink.Id && r.PartId == assemblyPartRequisitionLink.PartId && r.KeyCode == assemblyPartRequisitionLink.KeyCode).SetMergeOption(MergeOption.NoTracking);
            if (dbAssemblyPartRequisitionLinks.Any()) {
                throw new ValidationException(ErrorStrings.AssemblyPartRequisitionLink_Validation2);
            }
            var userInfo = Utils.GetCurrentUserInfo();
            assemblyPartRequisitionLink.CreatorId = userInfo.Id;
            assemblyPartRequisitionLink.CreatorName = userInfo.Name;
            assemblyPartRequisitionLink.CreateTime = DateTime.Now;
        }        internal void updateAssemblyPartRequisitionLinkValidate(AssemblyPartRequisitionLink assemblyPartRequisitionLink) {
            var dbAssemblyPartRequisitionLinks = ObjectContext.AssemblyPartRequisitionLinks.Where(r =>r.Status == (int)DcsBaseDataStatus.有效 && r.Id != assemblyPartRequisitionLink.Id && r.PartId == assemblyPartRequisitionLink.PartId && r.KeyCode == assemblyPartRequisitionLink.KeyCode).SetMergeOption(MergeOption.NoTracking);
            if (dbAssemblyPartRequisitionLinks.Any()) {
                throw new ValidationException(ErrorStrings.AssemblyPartRequisitionLink_Validation2);
            }
            var userInfo = Utils.GetCurrentUserInfo();
            assemblyPartRequisitionLink.ModifierId = userInfo.Id;
            assemblyPartRequisitionLink.ModifierName = userInfo.Name;
            assemblyPartRequisitionLink.ModifyTime = DateTime.Now;
        }

        public void InsertAssemblyPartRequisitionLink(AssemblyPartRequisitionLink assemblyPartRequisitionLink) {
            InsertToDatabase(assemblyPartRequisitionLink);
            this.InsertAssemblyPartRequisitionLinkValidate(assemblyPartRequisitionLink);
        }         public void UpdateAssemblyPartRequisitionLink(AssemblyPartRequisitionLink assemblyPartRequisitionLink) {
            UpdateToDatabase(assemblyPartRequisitionLink);
            this.updateAssemblyPartRequisitionLinkValidate(assemblyPartRequisitionLink);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:29
        //原分布类的函数全部转移到Ach                                                               

        public void InsertAssemblyPartRequisitionLink(AssemblyPartRequisitionLink assemblyPartRequisitionLink) {
            new AssemblyPartRequisitionLinkAch(this).InsertAssemblyPartRequisitionLink(assemblyPartRequisitionLink);
        }

        public void UpdateAssemblyPartRequisitionLink(AssemblyPartRequisitionLink assemblyPartRequisitionLink) {
            new AssemblyPartRequisitionLinkAch(this).UpdateAssemblyPartRequisitionLink(assemblyPartRequisitionLink);
        }
    }
}
