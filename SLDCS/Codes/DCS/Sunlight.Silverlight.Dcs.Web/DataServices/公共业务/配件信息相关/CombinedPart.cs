﻿using System.Linq;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class CombinedPartAch : DcsSerivceAchieveBase {
        public CombinedPartAch(DcsDomainService domainService)
            : base(domainService) {
        }

        public IQueryable<CombinedPart> GetCombinedPartsWithChildSparePart() {
            return ObjectContext.CombinedParts.Include("ChildSparePart").OrderBy(entity => entity.Id);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:29
        //原分布类的函数全部转移到Ach                                                               

        public IQueryable<CombinedPart> GetCombinedPartsWithChildSparePart() {
            return new CombinedPartAch(this).GetCombinedPartsWithChildSparePart();
        }
    }
}
