﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class CsbomPartsAch : DcsSerivceAchieveBase {
        public CsbomPartsAch(DcsDomainService domainService)
            : base(domainService) {
        }

         public IEnumerable<VirtualCsbomParts> 查询CSBOM配件信息(string code,string name, string spmName,string englishName,string spmEnglishName,DateTime? bCreateTime, DateTime? eCreateTime)
        {
            string SQL = @"select a.Id,a.Code,
                           a.Name,
                           b.ReferenceCode as SPMReferenceCode,
                           a.ReferenceCode,
                           b.name        AS SPMName,
                           a.EnglishName,
                           b.EnglishName AS SPMEnglishName,
                           a.CreateTime,cast ('是' as varchar2(100)) as IsUpdate 
                            from accessinfo_tmp a
                            inner join sparepart b
                            on a.code = b.code
                            where (a.name <> b.name or a.referencecode <> b.referencecode) ";

            if (!string.IsNullOrEmpty(code))
            {
                SQL = SQL + " and a.Code like '%" + code + "%'";
            }
            if (!string.IsNullOrEmpty(name))
            {
                SQL = SQL + " and a.name like '%" + name + "%'";
            }
            if (!string.IsNullOrEmpty(spmName))
            {
                SQL = SQL + " and b.name like '%" + spmName + "%'";
            }
            if (!string.IsNullOrEmpty(englishName))
            {
                SQL = SQL + " and a.englishName like '%" + englishName + "%'";
            }
            if (!string.IsNullOrEmpty(spmEnglishName))
            {
                SQL = SQL + " and b.englishName like '%" + spmEnglishName + "%'";
            }
            
            if (bCreateTime.HasValue)
            {
                SQL = SQL + " and a.CreateTime>= to_date('" + bCreateTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss')";
            }
            var search = ObjectContext.ExecuteStoreQuery<VirtualCsbomParts>(SQL).ToList();
            return search;
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:29
        //原分布类的函数全部转移到Ach                                                               

        public IEnumerable<VirtualCsbomParts> 查询CSBOM配件信息(string code,string name, string spmName,string englishName,string spmEnglishName,DateTime? bCreateTime, DateTime? eCreateTime) {
            return new CsbomPartsAch(this).查询CSBOM配件信息( code, name,  spmName, englishName, spmEnglishName,bCreateTime,eCreateTime);
        }
    }
}
