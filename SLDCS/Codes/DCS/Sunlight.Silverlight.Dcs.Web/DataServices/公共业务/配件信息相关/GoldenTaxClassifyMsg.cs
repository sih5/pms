﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class GoldenTaxClassifyMsgAch : DcsSerivceAchieveBase {
        public GoldenTaxClassifyMsgAch(DcsDomainService domainService)
            : base(domainService) {
        }

        public void InsertGoldenTaxClassifyMsg(GoldenTaxClassifyMsg goldenTaxClassifyMsg) {
            InsertToDatabase(goldenTaxClassifyMsg);
            this.InsertGoldenTaxClassifyMsgValidate(goldenTaxClassifyMsg);
        }
        internal void InsertGoldenTaxClassifyMsgValidate(GoldenTaxClassifyMsg goldenTaxClassifyMsg) {
            var userInfo = Utils.GetCurrentUserInfo();
            goldenTaxClassifyMsg.CreatorId = userInfo.Id;
            goldenTaxClassifyMsg.CreatorName = userInfo.Name;
            goldenTaxClassifyMsg.CreateTime = DateTime.Now;
        }
        public void UpdateGoldenTaxClassifyMsg(GoldenTaxClassifyMsg goldenTaxClassifyMsg) {
            UpdateToDatabase(goldenTaxClassifyMsg);
            this.UpdateGoldenTaxClassifyMsgValidate(goldenTaxClassifyMsg);
        }
        internal void UpdateGoldenTaxClassifyMsgValidate(GoldenTaxClassifyMsg goldenTaxClassifyMsg) {
            var userInfo = Utils.GetCurrentUserInfo();
            goldenTaxClassifyMsg.ModifierId = userInfo.Id;
            goldenTaxClassifyMsg.ModifierName = userInfo.Name;
            goldenTaxClassifyMsg.ModifyTime = DateTime.Now;
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:29
        //原分布类的函数全部转移到Ach                                                               

        public void InsertGoldenTaxClassifyMsg(GoldenTaxClassifyMsg goldenTaxClassifyMsg) {
            new GoldenTaxClassifyMsgAch(this).InsertGoldenTaxClassifyMsg(goldenTaxClassifyMsg);
        }

        public void UpdateGoldenTaxClassifyMsg(GoldenTaxClassifyMsg goldenTaxClassifyMsg) {
            new GoldenTaxClassifyMsgAch(this).UpdateGoldenTaxClassifyMsg(goldenTaxClassifyMsg);
        }
    }
}
