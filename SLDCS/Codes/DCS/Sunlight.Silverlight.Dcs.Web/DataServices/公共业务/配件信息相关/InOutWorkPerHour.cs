﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class InOutWorkPerHourAch : DcsSerivceAchieveBase {
        public InOutWorkPerHourAch(DcsDomainService domainService)
            : base(domainService) {
        }
        public IQueryable<InOutWorkPerHour> GetInOutWorkPerHoursBy() {
            return ObjectContext.InOutWorkPerHours.OrderBy(r => r.Id);
        }
    }
    partial class DcsDomainService {
        public IQueryable<InOutWorkPerHour> GetInOutWorkPerHoursBy() {
            return new InOutWorkPerHourAch(this).GetInOutWorkPerHoursBy();
        }
    }
}
