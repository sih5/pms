﻿using System;
using System.Linq;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartDeleaveHistoryAch : DcsSerivceAchieveBase {
        public PartDeleaveHistoryAch(DcsDomainService domainService)
            : base(domainService) {
        }

        internal void InsertPartDeleaveHistoryValidate(PartDeleaveHistory partDeleaveHistory) {
            var userInfo = Utils.GetCurrentUserInfo();
            partDeleaveHistory.CreateTime = DateTime.Now;
            partDeleaveHistory.CreatorId = userInfo.Id;
            partDeleaveHistory.CreatorName = userInfo.Name;
        }
        internal void UpdatePartDeleaveHistoryValidate(PartDeleaveHistory partDeleaveHistory) {
        }
        internal void InsertPartDeleaveHistory(PartDeleaveHistory partDeleaveHistory) {
            InsertToDatabase(partDeleaveHistory);
            this.InsertPartDeleaveHistoryValidate(partDeleaveHistory);
        }
        internal void UpdatePartDeleaveHistory(PartDeleaveHistory partDeleaveHistory) {
            UpdateToDatabase(partDeleaveHistory);
            this.UpdatePartDeleaveHistoryValidate(partDeleaveHistory);
        }
        public IQueryable<PartDeleaveHistory> GetPartDeleaveHistoryWithDetail() {
            return ObjectContext.PartDeleaveHistories.Include("Branch").Include("PartsSalesCategory").OrderBy(e => e.Id);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:29
        //原分布类的函数全部转移到Ach                                                               
                public IQueryable<PartDeleaveHistory> GetPartDeleaveHistoryWithDetail() {
            return new PartDeleaveHistoryAch(this).GetPartDeleaveHistoryWithDetail();
        }
    }
}
