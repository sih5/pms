﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartDeleaveInformationAch : DcsSerivceAchieveBase {
        internal void InsertPartDeleaveInformationValidate(PartDeleaveInformation partDeleaveInformation) {
            var dbPartDeleaveInformation = ObjectContext.PartDeleaveInformations.Where(r => r.BranchId == partDeleaveInformation.BranchId && r.OldPartId == partDeleaveInformation.OldPartId && r.PartsSalesCategoryId == partDeleaveInformation.PartsSalesCategoryId && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbPartDeleaveInformation != null) {
                throw new ValidationException(ErrorStrings.PartDeleaveInformation_Validation1);
            }
            var userInfo = Utils.GetCurrentUserInfo();
            partDeleaveInformation.CreateTime = DateTime.Now;
            partDeleaveInformation.CreatorId = userInfo.Id;
            partDeleaveInformation.CreatorName = userInfo.Name;
        }
        internal void UpdatePartDeleaveInformationValidate(PartDeleaveInformation partDeleaveInformation) {
            var dbPartDeleaveInformation = ObjectContext.PartDeleaveInformations.Where(r => r.Id != partDeleaveInformation.Id && r.BranchId == partDeleaveInformation.BranchId && r.OldPartId == partDeleaveInformation.OldPartId && r.PartsSalesCategoryId == partDeleaveInformation.PartsSalesCategoryId && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbPartDeleaveInformation != null) {
                throw new ValidationException(ErrorStrings.PartDeleaveInformation_Validation1);
            }
            var dealerPartsStrocks = ObjectContext.DealerPartsStocks.Where(r => r.SparePartId == partDeleaveInformation.OldPartId && r.BranchId == partDeleaveInformation.BranchId && r.SalesCategoryId == partDeleaveInformation.PartsSalesCategoryId).ToArray();
            if(dealerPartsStrocks.Length > 0 && dealerPartsStrocks.Any(r => r.Quantity > 0)) {
                throw new ValidationException(ErrorStrings.PartDeleaveInformation_Validation2);
            }
            var userInfo = Utils.GetCurrentUserInfo();
            partDeleaveInformation.ModifyTime = DateTime.Now;
            partDeleaveInformation.ModifierId = userInfo.Id;
            partDeleaveInformation.ModifierName = userInfo.Name;
        }
        public void InsertPartDeleaveInformation(PartDeleaveInformation partDeleaveInformation) {
            InsertToDatabase(partDeleaveInformation);
            this.InsertPartDeleaveInformationValidate(partDeleaveInformation);
            var partDeleaveHistory = new PartDeleaveHistory {
                OldPartId = partDeleaveInformation.OldPartId,
                OldPartCode = partDeleaveInformation.OldPartCode,
                DeleavePartCode = partDeleaveInformation.DeleavePartCode,
                DeleavePartName = partDeleaveInformation.DeleavePartName,
                BranchId = partDeleaveInformation.BranchId,
                PartsSalesCategoryId = partDeleaveInformation.PartsSalesCategoryId,
                DeleaveAmount = partDeleaveInformation.DeleaveAmount,
                Remark = partDeleaveInformation.Remark,
            };
            partDeleaveInformation.PartDeleaveHistories.Add(partDeleaveHistory);
            new PartDeleaveHistoryAch(this.DomainService).InsertPartDeleaveHistory(partDeleaveHistory);
        }
        public void UpdatePartDeleaveInformation(PartDeleaveInformation partDeleaveInformation) {
            UpdateToDatabase(partDeleaveInformation);
            this.UpdatePartDeleaveInformationValidate(partDeleaveInformation);
            var partDeleaveHistory = new PartDeleaveHistory {
                PartDeleaveInformationId = partDeleaveInformation.Id,
                OldPartId = partDeleaveInformation.OldPartId,
                OldPartCode = partDeleaveInformation.OldPartCode,
                DeleavePartCode = partDeleaveInformation.DeleavePartCode,
                DeleavePartName = partDeleaveInformation.DeleavePartName,
                BranchId = partDeleaveInformation.BranchId,
                PartsSalesCategoryId = partDeleaveInformation.PartsSalesCategoryId,
                DeleaveAmount = partDeleaveInformation.DeleaveAmount,
                Remark = partDeleaveInformation.Remark,
            };
            new PartDeleaveHistoryAch(this.DomainService).InsertPartDeleaveHistory(partDeleaveHistory);
        }
        public IQueryable<PartDeleaveInformation> GetPartDeleaveInformationsWithDetail() {
            return ObjectContext.PartDeleaveInformations.Include("Branch").Include("PartsSalesCategory").OrderBy(e => e.Id);
        }
        [Query(HasSideEffects = true)]
        public IQueryable<PartDeleaveInformation> GetPartDeleaveInformationsByIds(int[] partIds, int partsSalesCategoryId) {
            return ObjectContext.PartDeleaveInformations.Where(e => partIds.Contains(e.OldPartId) && e.PartsSalesCategoryId == partsSalesCategoryId).OrderBy(r => r.Id).OrderBy(e => e.Id);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:29
        //原分布类的函数全部转移到Ach                                                               

        public void InsertPartDeleaveInformation(PartDeleaveInformation partDeleaveInformation) {
            new PartDeleaveInformationAch(this).InsertPartDeleaveInformation(partDeleaveInformation);
        }

        public void UpdatePartDeleaveInformation(PartDeleaveInformation partDeleaveInformation) {
            new PartDeleaveInformationAch(this).UpdatePartDeleaveInformation(partDeleaveInformation);
        }

        public IQueryable<PartDeleaveInformation> GetPartDeleaveInformationsWithDetail() {
            return new PartDeleaveInformationAch(this).GetPartDeleaveInformationsWithDetail();
        }
        
        [Query(HasSideEffects = true)]
        public IQueryable<PartDeleaveInformation> GetPartDeleaveInformationsByIds(int[] partIds, int partsSalesCategoryId) {
            return new PartDeleaveInformationAch(this).GetPartDeleaveInformationsByIds(partIds,partsSalesCategoryId);
        }
    }
}
