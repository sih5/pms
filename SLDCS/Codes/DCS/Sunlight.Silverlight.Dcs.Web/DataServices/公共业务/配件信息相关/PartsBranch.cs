﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using System.Web.Configuration;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsBranchAch : DcsSerivceAchieveBase {
        internal void InsertPartsBranchValidate(PartsBranch partsBranch) {
            var dbpartsBranch = ObjectContext.PartsBranches.Where(r => r.PartId == partsBranch.PartId && r.BranchId == partsBranch.BranchId && r.PartsSalesCategoryId == partsBranch.PartsSalesCategoryId && r.Status != (int)DcsBaseDataStatus.作废).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbpartsBranch != null)
                throw new ValidationException(string.Format(ErrorStrings.PartsBranch_Validation1, partsBranch.BranchName, partsBranch.PartCode));
            var userInfo = Utils.GetCurrentUserInfo();
            partsBranch.CreatorId = userInfo.Id;
            partsBranch.CreatorName = userInfo.Name;
            partsBranch.CreateTime = DateTime.Now;
        }

        internal void UpdatePartsBranchValidate(PartsBranch partsBranch) {
            var userInfo = Utils.GetCurrentUserInfo();
            partsBranch.ModifierId = userInfo.Id;
            partsBranch.ModifierName = userInfo.Name;
            partsBranch.ModifyTime = DateTime.Now;
        }

        public void InsertPartsBranch(PartsBranch partsBranch) {
            string databaseName = WebConfigurationManager.AppSettings["DataBaseName"];
            if(databaseName == "DCS")
                partsBranch.OMSparePartMark = 0;
            InsertToDatabase(partsBranch);
            this.InsertPartsBranchValidate(partsBranch);
            var newPartsBranchHistory = new PartsBranchHistory {
                PartCode = partsBranch.PartCode,
                PartName = partsBranch.PartName,
                BranchId = partsBranch.BranchId,
                BranchName = partsBranch.BranchName,
                PartsSalesCategoryId = partsBranch.PartsSalesCategoryId,
                PartsSalesCategoryName = partsBranch.PartsSalesCategoryName,
                ABCStrategyId = partsBranch.ABCStrategyId,
                ProductLifeCycle = partsBranch.ProductLifeCycle,
                IsOrderable = partsBranch.IsOrderable,
                PurchaseCycle = partsBranch.PurchaseCycle,
                IsService = partsBranch.IsService,
                IsSalable = partsBranch.IsSalable,
                PartsReturnPolicy = partsBranch.PartsReturnPolicy,
                IsDirectSupply = partsBranch.IsDirectSupply,
                WarrantySupplyStatus = partsBranch.WarrantySupplyStatus,
                MinSaleQuantity = partsBranch.MinSaleQuantity,
                PartABC = partsBranch.PartABC,
                PurchaseRoute = partsBranch.PurchaseRoute,
                PartsWarrantyCategoryId = partsBranch.PartsWarrantyCategoryId,
                PartsWarrantyCategoryCode = partsBranch.PartsWarrantyCategoryCode,
                PartsWarrantyCategoryName = partsBranch.PartsWarrantyCategoryName,
                PartsWarhouseManageGranularity = partsBranch.PartsWarhouseManageGranularity,
                Status = partsBranch.Status,
                CreatorId = partsBranch.CreatorId,
                CreatorName = partsBranch.CreatorName,
                AutoApproveUpLimit = partsBranch.AutoApproveUpLimit,
                CreateTime = partsBranch.CreateTime,
                Remark = partsBranch.Remark,
                RepairMatMinUnit = partsBranch.RepairMatMinUnit,
                ReferenceCode = partsBranch.ReferenceCode,
                LossType = partsBranch.LossType,
                StockMaximum = partsBranch.StockMaximum,
                StockMinimum = partsBranch.StockMinimum,
                PartsWarrantyLong = partsBranch.PartsWarrantyLong,
                PartsMaterialManageCost = partsBranch.PartsMaterialManageCost,
                PartsAttribution = partsBranch.PartsAttribution,
                IncreaseRateGroupId = partsBranch.IncreaseRateGroupId,
                PartId = partsBranch.PartId
            };
            partsBranch.PartsBranchHistories.Add(newPartsBranchHistory);
            InsertToDatabase(newPartsBranchHistory);
            new PartsBranchHistoryAch(this.DomainService).InsertPartsBranchHistoryValidate(newPartsBranchHistory);
        }

        public void UpdatePartsBranch(PartsBranch partsBranch) {
            string databaseName = WebConfigurationManager.AppSettings["DataBaseName"];
            if(databaseName == "DCS")
                partsBranch.OMSparePartMark = 0;
            UpdateToDatabase(partsBranch);
            this.UpdatePartsBranchValidate(partsBranch);
            var newPartsBranchHistory = new PartsBranchHistory {
                PartsBranchId = partsBranch.Id,
                PartCode = partsBranch.PartCode,
                PartName = partsBranch.PartName,
                BranchId = partsBranch.BranchId,
                BranchName = partsBranch.BranchName,
                PartsSalesCategoryId = partsBranch.PartsSalesCategoryId,
                PartsSalesCategoryName = partsBranch.PartsSalesCategoryName,
                ABCStrategyId = partsBranch.ABCStrategyId,
                ProductLifeCycle = partsBranch.ProductLifeCycle,
                IsOrderable = partsBranch.IsOrderable,
                PurchaseCycle = partsBranch.PurchaseCycle,
                IsService = partsBranch.IsService,
                IsSalable = partsBranch.IsSalable,
                PartsReturnPolicy = partsBranch.PartsReturnPolicy,
                IsDirectSupply = partsBranch.IsDirectSupply,
                WarrantySupplyStatus = partsBranch.WarrantySupplyStatus,
                MinSaleQuantity = partsBranch.MinSaleQuantity,
                PartABC = partsBranch.PartABC,
                PurchaseRoute = partsBranch.PurchaseRoute,
                PartsWarrantyCategoryId = partsBranch.PartsWarrantyCategoryId,
                PartsWarrantyCategoryCode = partsBranch.PartsWarrantyCategoryCode,
                PartsWarrantyCategoryName = partsBranch.PartsWarrantyCategoryName,
                PartsWarhouseManageGranularity = partsBranch.PartsWarhouseManageGranularity,
                Status = partsBranch.Status,
                CreatorId = partsBranch.CreatorId,
                CreatorName = partsBranch.CreatorName,
                AutoApproveUpLimit = partsBranch.AutoApproveUpLimit,
                CreateTime = partsBranch.CreateTime,
                Remark = partsBranch.Remark,
                RepairMatMinUnit = partsBranch.RepairMatMinUnit,
                ReferenceCode = partsBranch.ReferenceCode,
                LossType = partsBranch.LossType,
                StockMaximum = partsBranch.StockMaximum,
                StockMinimum = partsBranch.StockMinimum,
                PartsWarrantyLong = partsBranch.PartsWarrantyLong,
                PartsMaterialManageCost = partsBranch.PartsMaterialManageCost,
                PartsAttribution = partsBranch.PartsAttribution,
                IncreaseRateGroupId = partsBranch.IncreaseRateGroupId,
                PartId = partsBranch.PartId
            };
            InsertToDatabase(newPartsBranchHistory);
            new PartsBranchHistoryAch(this.DomainService).InsertPartsBranchHistoryValidate(newPartsBranchHistory);

            //如果是否可采购从是变为否，校验 是否存在非作废/非审批通过的采购计划单
            var dbpartsBranch = ObjectContext.PartsBranches.Where(r => r.Id == partsBranch.Id && r.Status == (int)DcsMasterDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if (dbpartsBranch == null)
                throw new ValidationException(ErrorStrings.PartsBranch_Validation4);
            if (dbpartsBranch.IsOrderable == true && partsBranch.IsOrderable == false) {
                var purchasePlan = ObjectContext.PartsPurchasePlans.Where(r => r.Status != (int)DcsPurchasePlanOrderStatus.作废 && r.Status != (int)DcsPurchasePlanOrderStatus.终审通过 &&  ObjectContext.PartsPurchasePlanDetails.Any(d => d.SparePartId == partsBranch.PartId && d.PurchasePlanId == r.Id));
                if (purchasePlan.Any()) {
                    throw new ValidationException(ErrorStrings.PartsBranch_Validation5);
                }
            }
        }
        [Query(HasSideEffects = true)]
        public IQueryable<PartsBranch> 查询配件营销信息(int? branchId, int[] partsId) {
            var result = ObjectContext.PartsBranches.Where(r => r.Status != (int)DcsBaseDataStatus.作废);
            if(branchId.HasValue)
                result = result.Where(r => r.BranchId == branchId.Value);
            if(partsId != null && partsId.Length > 0)
                result = result.Where(r => partsId.Contains(r.PartId));
            return result.OrderBy(r => r.Id);
        }

        public IQueryable<PartsBranch> GetPartsBranchesRelationWithPartssalescategory() {
            return ObjectContext.PartsBranches.Include("PartsSalesCategory").Include("PartsSalePriceIncreaseRate").Include("SparePart").OrderBy(p => p.Id);
        }

        public IQueryable<VirtualPartsBranch> GetVirtualPartsBranchesRelationWithPartssalescategory() {
            var result = from a in ObjectContext.PartsBranches
                         join b in ObjectContext.PartsSalesCategories on a.PartsSalesCategoryId equals b.Id into tempTable
                         from t in tempTable.DefaultIfEmpty()
                         join c in ObjectContext.SpareParts on a.PartId equals c.Id into tempTable1
                         from t1 in tempTable1.DefaultIfEmpty()
                         join d in ObjectContext.PartsSalePriceIncreaseRates on a.IncreaseRateGroupId equals d.Id into tempTable2
                         from t2 in tempTable2.DefaultIfEmpty()
                         join e in ObjectContext.PartsBranchPackingProps.Where(r => r.PackingType == (int)DcsPackingUnitType.一级包装) on a.Id equals e.PartsBranchId into tempTable3
                         from t3 in tempTable3.DefaultIfEmpty()
                         join f in ObjectContext.PartsBranchPackingProps.Where(r => r.PackingType == (int)DcsPackingUnitType.二级包装) on a.Id equals f.PartsBranchId into tempTable4
                         from t4 in tempTable4.DefaultIfEmpty()
                         join g in ObjectContext.PartsBranchPackingProps.Where(r => r.PackingType == (int)DcsPackingUnitType.三级包装) on a.Id equals g.PartsBranchId into tempTable5
                         from t5 in tempTable5.DefaultIfEmpty()
                         select new VirtualPartsBranch {
                             Id = a.Id,
                             PartId = a.PartId,
                             PartCode = a.PartCode,
                             PartName = a.PartName,
                             BranchId = a.BranchId,
                             BranchName = a.BranchName,
                             PartsSalesCategoryId = a.PartsSalesCategoryId,
                             PartsSalesCategoryName = t.Name,
                             ReferenceCode = t1.ReferenceCode,
                             ExchangeIdentification = t1.ExchangeIdentification,
                             OverseasPartsFigure = t1.OverseasPartsFigure,
                             ProductBrand = t1.ProductBrand,
                             Feature = t1.Feature,
                             EnglishName = t1.EnglishName,
                             OMSparePartMark = t1.OMSparePartMark,
                             IsOrderable = a.IsOrderable,
                             IsSalable = a.IsSalable,
                             IsDirectSupply = a.IsDirectSupply,
                             MinSaleQuantity = a.MinSaleQuantity,
                             ProductLifeCycle = a.ProductLifeCycle,
                             LossType = a.LossType,
                             PartsAttribution = a.PartsAttribution,
                             PartsWarrantyCategoryName = a.PartsWarrantyCategoryName,
                             PartsWarhouseManageGranularity = a.PartsWarhouseManageGranularity,
                             PartsReturnPolicy = a.PartsReturnPolicy,
                             GroupCode = t2.GroupCode,
                             GroupName = t2.GroupName,
                             PartABC = a.PartABC,
                             StockMaximum = a.StockMaximum,
                             StockMinimum = a.StockMinimum,
                             PartsMaterialManageCost = a.PartsMaterialManageCost,
                             PartsWarrantyLong = a.PartsWarrantyLong,
                             PlannedPriceCategory = a.PlannedPriceCategory,
                             AutoApproveUpLimit = a.AutoApproveUpLimit,
                             StandardCode = t1.StandardCode,
                             StandardName = t1.StandardName,
                             Status = a.Status,
                             WarrantySupplyStatus = a.WarrantySupplyStatus,
                             PurchaseRoute = a.PurchaseRoute,
                             Remark = a.Remark,
                             CreatorName = a.CreatorName,
                             CreateTime = a.CreateTime,
                             ModifierName = a.ModifierName,
                             ModifyTime = a.ModifyTime,
                             AbandonerName = a.AbandonerName,
                             AbandonTime = a.AbandonTime,
                             PackingCoefficient1 = t3.PackingCoefficient,
                             PackingCoefficient2 = t4.PackingCoefficient,
                             PackingCoefficient3 = t5.PackingCoefficient,
                             BreakTime = a.BreakTime,
                             IsAccreditPack = a.IsAccreditPack,
                             IsAutoCaclSaleProperty = a.IsAutoCaclSaleProperty,
                             MainPackingType=a.MainPackingType,
                             FirPrin = t3.IsBoxStandardPrint == 1 ? "大标" : t3.IsPrintPartStandard==1?"小标":"",
                             SecPrin = t4.IsBoxStandardPrint == 1 ? "大标" : t4.IsPrintPartStandard == 1 ? "小标" : "",
                             ThidPrin = t5.IsBoxStandardPrint == 1 ? "大标" : t5.IsPrintPartStandard == 1 ? "小标" : "",
                             FirPackingMaterial = t3.PackingMaterial,
                             SecPackingMaterial=t4.PackingMaterial,
                             ThidPackingMaterial=t5.PackingMaterial
                         };
            return result.OrderByDescending(r => r.ModifyTime ?? r.CreateTime);

        }

        public string CheckPartsBranchesByPartIds(int[] partIds, String[] partsSalesCategoryNames) {
            var partsBranches = ObjectContext.PartsBranches.Where(r => partIds.Contains(r.PartId) && !partsSalesCategoryNames.Contains(r.PartsSalesCategoryName) && r.Status == (int)DcsBaseDataStatus.有效).ToArray();
            if(partsBranches.Any()) {
                //导出配件id 然后报错
                return DomainService.导出编辑校验问题配件(partsBranches.Select(r => r.PartCode).ToArray());
            }
            return null;
        }

        public IQueryable<PartsBranch> GetPartsBranchesRelationWithParam(int partsSalesCategoryId, int[] partIds, int branchId) {
            return ObjectContext.PartsBranches.Where(r => partIds.Contains(r.PartId) && r.PartsSalesCategoryId == partsSalesCategoryId && r.IsOrderable && r.BranchId == branchId && r.Status == (int)DcsBaseDataStatus.有效).OrderBy(p => p.Id);
        }

        public IQueryable<PartsBranch> GetPartsBranchesByCompanyId() {
            var userInfo = Utils.GetCurrentUserInfo();
            var companyId = userInfo.EnterpriseId;
            var company = ObjectContext.Companies.SingleOrDefault(r => r.Id == companyId);
            if(company == null) {
                return null;
            }
            if(company.Type == (int)DcsCompanyType.分公司) {
                return ObjectContext.PartsBranches.Include("PartsSalesCategory").OrderBy(p => p.Id);
            }
            var result = ObjectContext.PartsBranches.Where(r => r.IsSalable == true);
            return result.Include("PartsSalesCategory").OrderBy(p => p.Id);
        }

        public IQueryable<VirtualPartsBranch> 查询互换件配件信息() {
            //1、BO关联（全部左联接）配件信息：
            //配件营销信息 依据：配件信息.id = 配件营销信息.配件id and 配件营销信息.状态 = 有效
            //配件销售价 依据：配件信息.id = 配件销售价.配件id and 配件销售价.状态 = 有效 and 配件销售价.配件销售类型id = 参数 配件销售类型id
            //配件零售指导价 依据：配件信息.id = 配件零售指导价.配件id and 配件零售指导价.状态= 有效 and 配件零售指导价.配件销售类型id = 参数 配件销售类型id
            //返回结果字段
            //配件图号 配件名称 品牌 销售价 零售指导价 零部件图号 研究院图号 状态 配件类型 计量单位 规格型号 特征说明
            var result = from partsBranche in ObjectContext.PartsBranches.Where(r => r.Status == (int)DcsBaseDataStatus.有效)
                         join sparePart in ObjectContext.SpareParts on partsBranche.PartId equals sparePart.Id
                          into tempTable1
                         from a in tempTable1.DefaultIfEmpty()
                         join b in ObjectContext.PartsSalesPrices.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on new {
                             SparePartId = a.Id,
                             partsBranche.PartsSalesCategoryId
                         } equals
                          new {
                              b.SparePartId,
                              b.PartsSalesCategoryId
                          }
                          into tempTable2
                         from partsSalesPrice in tempTable2.DefaultIfEmpty()
                         join c in ObjectContext.PartsRetailGuidePrices.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on new {
                             SparePartId = a.Id,
                             partsBranche.PartsSalesCategoryId
                         } equals new {
                             c.SparePartId,
                             c.PartsSalesCategoryId
                         } into tempTable3
                         from partsRetailGuidePrice in tempTable3.DefaultIfEmpty()
                         select new VirtualPartsBranch {
                             Id = partsBranche.Id,
                             PartId = a.Id,
                             PartCode = a.Code,
                             CADCode = a.CADCode,
                             PartName = a.Name,
                             Status = a.Status,
                             PartType = a.PartType,
                             MeasureUnit = a.MeasureUnit,
                             Specification = a.Specification,
                             Feature = a.Feature,
                             ReferenceCode = a.ReferenceCode,
                             CreateTime = a.CreateTime,
                             ModifyTime = a.ModifyTime,
                             PartsSalesCategoryId = partsBranche.PartsSalesCategoryId,
                             PartsSalesCategoryName = partsBranche.PartsSalesCategoryName,
                             RetailGuidePrice = partsRetailGuidePrice.RetailGuidePrice,
                             SalesPrice = partsSalesPrice.SalesPrice
                         };
            return result.OrderBy(r => r.Id);

        }

        public IQueryable<PartsBranchPackingProp> 获取当前企业的配件营销包装属性(int partsSalesCategoryId, int branchId, int[] sparePartIds) {
            var result = ObjectContext.PartsBranchPackingProps.Where(o => ObjectContext.PartsBranches.Any(r => r.MainPackingType == o.PackingType && r.PartsSalesCategoryId == partsSalesCategoryId && r.BranchId == branchId && r.Id == o.PartsBranchId && r.Status == (int)DcsMasterDataStatus.有效));
            if(sparePartIds != null && sparePartIds.Length > 0)
                result = result.Where(r => sparePartIds.Contains(r.SparePartId.Value));
            return result.OrderBy(r => r.Id);
          
        }
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:29
        //原分布类的函数全部转移到Ach                                                               

        public void InsertPartsBranch(PartsBranch partsBranch) {
            new PartsBranchAch(this).InsertPartsBranch(partsBranch);
        }

        public void UpdatePartsBranch(PartsBranch partsBranch) {
            new PartsBranchAch(this).UpdatePartsBranch(partsBranch);
        }

        [Query(HasSideEffects = true)]
        public IQueryable<PartsBranch> 查询配件营销信息(int? branchId, int[] partsId) {
            return new PartsBranchAch(this).查询配件营销信息(branchId, partsId);
        }

        public IQueryable<PartsBranch> GetPartsBranchesRelationWithPartssalescategory() {
            return new PartsBranchAch(this).GetPartsBranchesRelationWithPartssalescategory();
        }
        public string CheckPartsBranchesByPartIds(int[] partIds, String[] partsSalesCategoryNames) {
            return new PartsBranchAch(this).CheckPartsBranchesByPartIds(partIds, partsSalesCategoryNames);
        }

        public IQueryable<PartsBranch> GetPartsBranchesRelationWithParam(int partsSalesCategoryId, int[] partIds, int branchId) {
            return new PartsBranchAch(this).GetPartsBranchesRelationWithParam(partsSalesCategoryId, partIds, branchId);
        }

        public IQueryable<PartsBranch> GetPartsBranchesByCompanyId() {
            return new PartsBranchAch(this).GetPartsBranchesByCompanyId();
        }

        public IQueryable<VirtualPartsBranch> 查询互换件配件信息() {
            return new PartsBranchAch(this).查询互换件配件信息();
        }

        public IQueryable<VirtualPartsBranch> GetVirtualPartsBranchesRelationWithPartssalescategory() {
            return new PartsBranchAch(this).GetVirtualPartsBranchesRelationWithPartssalescategory();
        }

        [Query(HasSideEffects = true)]
        public IQueryable<PartsBranchPackingProp> 获取当前企业的配件营销包装属性(int partsSalesCategoryId, int branchId, int[] sparePartIds) {
            return new PartsBranchAch(this).获取当前企业的配件营销包装属性(partsSalesCategoryId, branchId, sparePartIds);
        }
    }
}

