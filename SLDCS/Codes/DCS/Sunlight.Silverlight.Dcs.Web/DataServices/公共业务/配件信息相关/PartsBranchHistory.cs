﻿using System;
using System.Linq;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsBranchHistoryAch : DcsSerivceAchieveBase {
        public PartsBranchHistoryAch(DcsDomainService domainService)
            : base(domainService) {
        }

        internal void InsertPartsBranchHistoryValidate(PartsBranchHistory partsBranchHistory) {
            var userInfo = Utils.GetCurrentUserInfo();
            partsBranchHistory.CreatorId = userInfo.Id;
            partsBranchHistory.CreatorName = userInfo.Name;
            partsBranchHistory.CreateTime = DateTime.Now;
        }
        public void InsertPartsBranchHistory(PartsBranchHistory partsBranchHistory) {
            InsertToDatabase(partsBranchHistory);
            this.InsertPartsBranchHistoryValidate(partsBranchHistory);
        }
        public IQueryable<PartsBranchHistory> GetPartsBranchHistoryWithPartssalescategory() {
            return ObjectContext.PartsBranchHistories.Include("PartsSalesCategory").Include("PartsSalePriceIncreaseRate").OrderByDescending(p => p.CreateTime);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:29
        //原分布类的函数全部转移到Ach                                                               

        public void InsertPartsBranchHistory(PartsBranchHistory partsBranchHistory) {
            new PartsBranchHistoryAch(this).InsertPartsBranchHistory(partsBranchHistory);
        }

        public IQueryable<PartsBranchHistory> GetPartsBranchHistoryWithPartssalescategory() {
            return new PartsBranchHistoryAch(this).GetPartsBranchHistoryWithPartssalescategory();
        }
    }
}
