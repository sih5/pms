﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Data;
using System.Data.EntityClient;
using System.Data.Objects;
using System.Linq;
using System.Reflection;
using System.ServiceModel.DomainServices.Server;
using System.Text;
using Devart.Common;
using Devart.Data.Oracle;
using Microsoft.Data.Extensions;
using NPOI.SS.Formula.Functions;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Telerik.Reporting;
using Utils = Sunlight.Silverlight.Web.Utils;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsExchangeAch : DcsSerivceAchieveBase {
        public PartsExchangeAch(DcsDomainService domainService)
            : base(domainService) {
        }

        internal void InsertPartsExchangeValidate(PartsExchange partsExchange) {
            var userInfo = Utils.GetCurrentUserInfo();
            partsExchange.CreatorId = userInfo.Id;
            partsExchange.CreatorName = userInfo.Name;
            partsExchange.CreateTime = DateTime.Now;
        }

        internal void UpdatePartsExchangeValdate(PartsExchange partsExchange) {
            var dbPartsExchange = ObjectContext.PartsExchanges.Where(r => r.Id == partsExchange.Id && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).SingleOrDefault();
            if(dbPartsExchange == null) {
                throw new ValidationException(ErrorStrings.PartsExchange_Validation3);
            }

            var userInfo = Utils.GetCurrentUserInfo();
            partsExchange.ModifierId = userInfo.Id;
            partsExchange.ModifierName = userInfo.Name;
            partsExchange.ModifyTime = DateTime.Now;
        }

        public void InsertPartsExchange(PartsExchange partsExchange) {
            InsertToDatabase(partsExchange);
            this.InsertPartsExchangeValidate(partsExchange);
            var newPartExchangeHistory = new PartsExchangeHistory {
                ExchangeCode = partsExchange.ExchangeCode,
                ExchangeName = partsExchange.ExchangeName,
                PartId = partsExchange.PartId,
                Status = (int)DcsBaseDataStatus.有效,
                Remark = partsExchange.Remark
            };
            partsExchange.PartsExchangeHistories.Add(newPartExchangeHistory);
            DomainService.InsertPartsExchangeHistory(newPartExchangeHistory);
        }

        public void UpdatePartsExchange(PartsExchange partsExchange) {
            UpdateToDatabase(partsExchange);
            UpdatePartsExchangeValdate(partsExchange);
            var newPartExchangeHistory = new PartsExchangeHistory {
                PartsExchangeId = partsExchange.Id,
                ExchangeCode = partsExchange.ExchangeCode,
                ExchangeName = partsExchange.ExchangeName,
                PartId = partsExchange.PartId,
                Status = (int)DcsBaseDataStatus.有效,
                Remark = partsExchange.Remark
            };
            DomainService.InsertPartsExchangeHistory(newPartExchangeHistory);
        }

        public IQueryable<PartsExchange> 查询配件互换信息(int? partId) {
            IQueryable<PartsExchange> partsExchanges = ObjectContext.PartsExchanges;
            if(partId.HasValue) {
                partsExchanges = partsExchanges.Where(r => r.PartId == partId);
            }
            var result = ObjectContext.PartsExchanges.Where(r => partsExchanges.Any(v => v.ExchangeCode == r.ExchangeCode));
            return result.Include("SparePart").OrderBy(r => r.Id);
        }

        public IEnumerable<VirtualPartsExchange> 查询配件互换信息带互换组(string partCode, string partName, string exGroupCode, int? status, string exchangeCode, DateTime? beginCreaTime, DateTime? endCreaTime) {
            string connectionString = "";
            StringBuilder sql = new StringBuilder();
            var entityConnStr = ConfigurationManager.ConnectionStrings["DcsEntities"].ConnectionString;
            if(!string.IsNullOrEmpty(entityConnStr)) {
                var entityConnection = new EntityConnectionStringBuilder(entityConnStr);
                connectionString = entityConnection.ProviderConnectionString;
            }
            OracleConnection conn = new OracleConnection(connectionString);
            conn.Open();
            OracleCommand command = new OracleCommand(null, conn);
            sql.Append(@"select rownum as Id,e.Id as PartsExchangeIdId, e.ExchangeCode,g.ExGroupCode,s.Code as PartCode, s.Name as PartName ,e.Status,e.Remark,e.CreatorName,e.CreateTime,e.ModifierName,e.ModifyTime,e.AbandonerName,e.AbandonTime 
                          from PartsExchange e
                         inner join sparepart s
                            on s.id = e.partid
                          left join PartsExchangeGroup g
                            on g.exchangecode = e.exchangecode and g.status = 1
                         where  e.IsVisible=1 
                        ");
            if(status.HasValue) {
                sql.Append(" and e.Status= " + status.Value);
            } else {
                sql.Append(" and e.Status in(1,99)");
            }
            if(beginCreaTime.HasValue) {
                sql.Append(" and e.CreateTime >= to_date('" + beginCreaTime.Value + "','yyyy-MM-dd HH24:mi:ss')");

            }
            if(endCreaTime.HasValue) {
                sql.Append(" and e.CreateTime <= to_date('" + endCreaTime.Value + "','yyyy-MM-dd HH24:mi:ss')");

            }
            if(!string.IsNullOrEmpty(partCode)) {
                sql.Append(@" and (g.exgroupcode in (select exgroupcode
               from PartsExchangeGroup g1
              inner join partsexchange e1
                 on e1.exchangecode = g1.exchangecode
              inner join sparepart s1
                 on s1.id = e1.partid
              where s1.code like '%" + partCode + "%' and e1.status=1 and g1.status=1 ) or s.code  like '%" + partCode + "%' ) ");
            }

            if(!string.IsNullOrEmpty(partName)) {
                sql.Append(" and (s.name like '%" + partName + "%')");
            }

            if(!string.IsNullOrEmpty(exGroupCode)) {
                sql.Append(" and (g.exgroupcode  like '%" + exGroupCode + "%')");
            }

            if(!string.IsNullOrEmpty(exchangeCode)) {
                sql.Append(@" and (g.exgroupcode in (select exgroupcode
                                                   from PartsExchangeGroup g1
                                                  where g1.exchangecode like '%" + exchangeCode + "%' and g1.Status=1) or e.exchangecode  like '%" + exchangeCode + "%')");
            }
            command.CommandText = sql.ToString();
            OracleDataAdapter orda = new OracleDataAdapter(command);
            DataSet ds = new DataSet();
            orda.Fill(ds);

            DataTable dtVirtualPartsExchange = ds.Tables[0];
            List<VirtualPartsExchange> virtualPartsExchanges = new List<VirtualPartsExchange>();
            foreach(DataRow item in dtVirtualPartsExchange.Rows) {
                var salesLead = new VirtualPartsExchange {
                    Id = int.Parse(item["Id"].ToString()),
                    PartsExchangeId = int.Parse(item["PartsExchangeIdId"].ToString()),
                    ExchangeCode = item["ExchangeCode"].ToString(),
                    ExGroupCode = item["ExGroupCode"].ToString(),
                    PartCode = item["PartCode"].ToString(),
                    PartName = item["PartName"].ToString(),
                    Status = int.Parse(item["Status"].ToString()),
                    Remark = item["Remark"].ToString(),
                    CreatorName = item["CreatorName"].ToString(),
                    CreateTime = item["CreateTime"].ToString() == "" ? default(DateTime?) : Convert.ToDateTime(item["CreateTime"].ToString()),
                    ModifierName = item["ModifierName"].ToString(),
                    ModifyTime = item["ModifyTime"].ToString() == "" ? default(DateTime?) : Convert.ToDateTime(item["ModifyTime"].ToString()),
                    AbandonerName = item["AbandonerName"].ToString(),
                    AbandonTime = item["AbandonTime"].ToString() == "" ? default(DateTime?) : Convert.ToDateTime(item["AbandonTime"].ToString()),
                };
                virtualPartsExchanges.Add(salesLead);
            }
            return virtualPartsExchanges.Distinct();
        }

        public List<T> DataSetToList<T>(DataSet ds, int tableIndext) {
            //确认参数有效  
            if(ds == null || ds.Tables.Count <= 0 || tableIndext < 0) {
                return null;
            }
            DataTable dt = ds.Tables[tableIndext]; //取得DataSet里的一个下标为tableIndext的表，然后赋给dt  

            IList<T> list = new List<T>();  //实例化一个list  
            // 在这里写 获取T类型的所有公有属性。 注意这里仅仅是获取T类型的公有属性，不是公有方法，也不是公有字段，当然也不是私有属性                                         
            PropertyInfo[] tMembersAll = typeof(T).GetProperties();

            for(int i = 0; i < dt.Rows.Count; i++) {
                //创建泛型对象。为什么这里要创建一个泛型对象呢？是因为目前我不确定泛型的类型。  
                T t = Activator.CreateInstance<T>();


                //获取t对象类型的所有公有属性。但是我不建议吧这条语句写在for循环里，因为没循环一次就要获取一次，占用资源，所以建议写在外面  
                //PropertyInfo[] tMembersAll = t.GetType().GetProperties();  


                for(int j = 0; j < dt.Columns.Count; j++) {
                    //遍历tMembersAll  
                    foreach(PropertyInfo tMember in tMembersAll) {
                        if(tMember.Name == "Id") {
                            tMember.SetValue(t, i + "" + j, null);
                        }
                        //取dt表中j列的名字，并把名字转换成大写的字母。整条代码的意思是：如果列名和属性名称相同时赋值  
                        if(dt.Columns[j].ColumnName.ToUpper().Equals(tMember.Name.ToUpper())) {
                            //dt.Rows[i][j]表示取dt表里的第i行的第j列；DBNull是指数据库中当一个字段没有被设置值的时候的值，相当于数据库中的“空值”。   
                            if(dt.Rows[i][j] != DBNull.Value) {
                                //SetValue是指：将指定属性设置为指定值。 tMember是T泛型对象t的一个公有成员，整条代码的意思就是：将dt.Rows[i][j]赋值给t对象的tMember成员,参数详情请参照http://msdn.microsoft.com/zh-cn/library/3z2t396t(v=vs.100).aspx/html  

                                tMember.SetValue(t, dt.Rows[i][j].ToString(), null);


                            } else {
                                tMember.SetValue(t, null, null);
                            }
                            break;//注意这里的break是写在if语句里面的，意思就是说如果列名和属性名称相同并且已经赋值了，那么我就跳出foreach循环，进行j+1的下次循环  
                        }

                    }
                }

                list.Add(t);
            }
            return list.ToList();

        }
        [Query(HasSideEffects = true)]
        public IQueryable<PartsExchange> 查询配件互换信息批量(int[] partIds) {
            var result = ObjectContext.PartsExchanges.Where(r => r.Status == (int)DcsBaseDataStatus.有效 & partIds.Contains(r.PartId));
            return result.Include("SparePart").OrderBy(r => r.Id);
        }

      
        public IEnumerable<PartsExchangeWithOtherInfo> GetPartsExchangeWithOtherInfoes(string partCode) {
            //var ExchangeCodeDB =ObjectContext.PartsExchanges.Where(r=>r.PartId== PartId && r.Status == (int)DcsBaseDataStatus.有效);
            //var result = from a in ObjectContext.PartsExchanges.Where(r => r.Status == (int)DcsBaseDataStatus.有效)
            //             join aa in ObjectContext.PartsExchanges.Where(r => r.Status == (int)DcsBaseDataStatus.有效)
            //             on a.ExchangeCode equals aa.ExchangeCode
            //             join b in ObjectContext.SpareParts.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on aa.PartId equals b.Id
            //             join c in ObjectContext.PartsBranches.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on b.Id equals c.PartId into temptable
            //             from t in temptable.DefaultIfEmpty()
            //             join d in ObjectContext.PartsSalesCategories.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on t.PartsSalesCategoryId equals d.Id into t1
            //             from s in t1.DefaultIfEmpty()
            //             where a.PartId != aa.PartId 
            //             select new PartsExchangeWithOtherInfo {
            //                 Id = a.Id,
            //                 PartId = a.PartId,
            //                 SparePartId = aa.PartId,
            //                 Code = b.Code,
            //                 Name = b.Name,
            //                 Feature = b.Feature,
            //                 ExchangeCode = a.ExchangeCode,
            //                 PartsSalesCategoryId = t.PartsSalesCategoryId,
            //                 PartsSalesCategoryName = s.Name,
            //                 IsOrderable = t.IsOrderable,
            //                 IsSalable = t.IsSalable,
            //                 IsDirectSupply = t.IsDirectSupply,
            //                 PartsReturnPolicy = t.PartsReturnPolicy,
            //             };
            //return result.OrderBy(r => r.Code);
            string connectionString = "";
            StringBuilder sql = new StringBuilder();
            var entityConnStr = ConfigurationManager.ConnectionStrings["DcsEntities"].ConnectionString;
            if (!string.IsNullOrEmpty(entityConnStr))
            {
                var entityConnection = new EntityConnectionStringBuilder(entityConnStr);
                connectionString = entityConnection.ProviderConnectionString;
            }
            OracleConnection conn = new OracleConnection(connectionString);
            conn.Open();
            OracleCommand command = new OracleCommand(null, conn);
            sql.Append(@"select rownum as Id, s.Code, s.Name, s.Feature,g.ExGroupCode, p.PartsSalesCategoryId, pc.name as PartsSalesCategoryName, p.IsOrderable,p.IsSalable,p.IsDirectSupply,p.PartsReturnPolicy
                         from PartsExchange e
                         inner join sparepart s on s.id = e.partid
                         left join PartsExchangeGroup g on g.exchangecode = e.exchangecode and g.status = 1
                         LEFT OUTER JOIN PartsBranch p ON (p.Status = 1) AND (s.Id = p.PartId)  
                         LEFT OUTER JOIN PartsSalesCategory pc ON (pc.Status = 1) AND (p.PartsSalesCategoryId = pc.Id)
                         where  e.IsVisible=1 and e.Status=1
                        ");

                sql.Append(@" and (g.exgroupcode in (select exgroupcode
               from PartsExchangeGroup g1
              inner join partsexchange e1
                 on e1.exchangecode = g1.exchangecode
              inner join sparepart s1
                 on s1.id = e1.partid
              where s1.code = '" + partCode + "' and e1.status=1 and g1.status=1 ) or s.code  = '" + partCode + "' ) ");
            command.CommandText = sql.ToString();
            OracleDataAdapter orda = new OracleDataAdapter(command);
            DataSet ds = new DataSet();
            orda.Fill(ds);

            DataTable dtVirtualPartsExchange = ds.Tables[0];
            List<PartsExchangeWithOtherInfo> virtualPartsExchanges = new List<PartsExchangeWithOtherInfo>();
            foreach (DataRow item in dtVirtualPartsExchange.Rows)
            {
                //var salesLead = new PartsExchangeWithOtherInfo
                //{
                PartsExchangeWithOtherInfo salesLead = new PartsExchangeWithOtherInfo();
                salesLead.Id = int.Parse(item["Id"].ToString());
                salesLead.ExchangeCode = item["ExGroupCode"].ToString();
                salesLead.Code = item["Code"].ToString();
                salesLead.Name = item["Name"].ToString();
                salesLead.PartsSalesCategoryName = item["PartsSalesCategoryName"].ToString();
                salesLead.IsOrderable = string.IsNullOrWhiteSpace(item["IsOrderable"].ToString()) ? false : Convert.ToBoolean(item["IsOrderable"]);
                salesLead.IsSalable = string.IsNullOrWhiteSpace(item["IsSalable"].ToString()) ? false : Convert.ToBoolean(item["IsSalable"]);
                salesLead.IsDirectSupply = string.IsNullOrWhiteSpace(item["IsSalable"].ToString()) ? false : Convert.ToBoolean(item["IsDirectSupply"]);
                salesLead.PartsReturnPolicy = string.IsNullOrWhiteSpace(item["IsSalable"].ToString()) ? 0 : int.Parse(item["PartsReturnPolicy"].ToString());
                salesLead.Feature = item["Feature"].ToString();
                //};
                virtualPartsExchanges.Add(salesLead);
            }
            return virtualPartsExchanges.Distinct();
        }
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:29
        //原分布类的函数全部转移到Ach                                                               

        public void InsertPartsExchange(PartsExchange partsExchange) {
            new PartsExchangeAch(this).InsertPartsExchange(partsExchange);
        }

        public void UpdatePartsExchange(PartsExchange partsExchange) {
            new PartsExchangeAch(this).UpdatePartsExchange(partsExchange);
        }

        public IQueryable<PartsExchange> 查询配件互换信息(int? partId) {
            return new PartsExchangeAch(this).查询配件互换信息(partId);
        }

        public IEnumerable<VirtualPartsExchange> 查询配件互换信息带互换组(string partCode, string partName, string exGroupCode, int? status, string exchangeCode, DateTime? beginCreaTime, DateTime? endCreaTime) {
            return new PartsExchangeAch(this).查询配件互换信息带互换组(partCode, partName, exGroupCode, status, exchangeCode, beginCreaTime, endCreaTime);
        }

        [Query(HasSideEffects = true)]
        public IQueryable<PartsExchange> 查询配件互换信息批量(int[] partIds) {
            return new PartsExchangeAch(this).查询配件互换信息批量(partIds);
        }


        [Query(HasSideEffects = true)]
        public IEnumerable<PartsExchangeWithOtherInfo> GetPartsExchangeWithOtherInfoes(string partCode) {
            return new PartsExchangeAch(this).GetPartsExchangeWithOtherInfoes(partCode);
        }
    }
}
