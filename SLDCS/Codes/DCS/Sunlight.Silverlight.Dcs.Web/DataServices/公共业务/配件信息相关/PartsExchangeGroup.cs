﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsExchangeGroupAch : DcsSerivceAchieveBase {

        public PartsExchangeGroupAch(DcsDomainService domainService)
            : base(domainService) {
        }

        public void InsertPartsExchangeGroup(PartsExchangeGroup partsExchangeGroup) {
            if(string.IsNullOrWhiteSpace(partsExchangeGroup.ExGroupCode) || partsExchangeGroup.ExGroupCode == GlobalVar.ASSIGNED_BY_SERVER)
                partsExchangeGroup.ExGroupCode = CodeGenerator.Generate("PartsExchange");
            InsertToDatabase(partsExchangeGroup);
            this.InsertPartsExchangeGroupValidate(partsExchangeGroup);
        }

        internal void InsertPartsExchangeGroupValidate(PartsExchangeGroup partsExchangeGroup) {
            var userInfo = Utils.GetCurrentUserInfo();
            partsExchangeGroup.CreatorId = userInfo.Id;
            partsExchangeGroup.CreatorName = userInfo.Name;
            partsExchangeGroup.CreateTime = DateTime.Now;
        }

        public void UpdatePartsExchangeGroup(PartsExchangeGroup partsExchangeGroup) {
            UpdateToDatabase(partsExchangeGroup);
            this.UpdatePartsExchangeGroupValidate(partsExchangeGroup);
        }

        internal void UpdatePartsExchangeGroupValidate(PartsExchangeGroup partsExchangeGroup) {
            var userInfo = Utils.GetCurrentUserInfo();
            partsExchangeGroup.ModifierId = userInfo.Id;
            partsExchangeGroup.ModifierName = userInfo.Name;
            partsExchangeGroup.ModifyTime = DateTime.Now;
        }
        public IQueryable<VirtualPartsExchangeGroup> GetVirtualPartsExchangeGroups() {
            var result = from a in ObjectContext.PartsExchangeGroups
                         join c in ObjectContext.PartsExchanges.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on a.ExchangeCode equals c.ExchangeCode into temptable1
                         from t in temptable1.DefaultIfEmpty()
                         join b in ObjectContext.SpareParts.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on t.PartId equals b.Id into temptable2
                         from t2 in temptable2.DefaultIfEmpty()
                         select new VirtualPartsExchangeGroup {
                             Id = t2.Id,
                             SparePartCode = t2.Code,
                             SparePartName = t2.Name,
                             ReferenceCode = t2.ReferenceCode,
                             ExchangeCode = a.ExchangeCode,
                             PartsExchangeGroupId = a.Id,
                             CreatorName = t.CreatorName,
                             CreateTime = t.CreateTime
                         };
            return result.OrderBy(r => r.Id);
        }

        public IQueryable<VirtualPartsExchangeGroup> GetVirtualPartsExchangeGroupAndPartsExchanges() {
           
            var partsExchangeGroups =  from a in ObjectContext.PartsExchangeGroups.Where(r => r.Status == (int)DcsBaseDataStatus.有效)
                                       select new VirtualPartsExchangeGroup {
                                           Status = a.Status,
                                           ExchangeCode = a.ExchangeCode,
                                           CreateTime = a.CreateTime ?? DateTime.Now
                                       };
            var partsExchanges = from a in ObjectContext.PartsExchanges.Where(r => r.Status == (int)DcsBaseDataStatus.有效)
                                      select new VirtualPartsExchangeGroup {
                                          Status = a.Status,
                                          ExchangeCode = a.ExchangeCode,
                                          CreateTime = a.CreateTime ?? DateTime.Now
                                      };
            var results = partsExchangeGroups.Union(partsExchanges).ToArray();
            var virtualPartsExchangeGroups = results.GroupBy(v => v.ExchangeCode).Select(o => new VirtualPartsExchangeGroup {
                Status = o.First().Status,
                ExchangeCode = o.Key,
                CreateTime = o.First().CreateTime ?? DateTime.Now,
                Id = 1
            });
            var resultes = virtualPartsExchangeGroups.Distinct().ToArray();

            //foreach(var result in resultes) {
            //    result.Id = id++;
            //}
            return resultes.AsQueryable().OrderByDescending(r => r.CreateTime);
        }
    }

    partial class DcsDomainService {
        public void InsertPartsExchangeGroup(PartsExchangeGroup partsExchangeGroup) {
            new PartsExchangeGroupAch(this).InsertPartsExchangeGroup(partsExchangeGroup);
        }

        public void UpdatePartsExchangeGroup(PartsExchangeGroup partsExchangeGroup) {
            new PartsExchangeGroupAch(this).UpdatePartsExchangeGroup(partsExchangeGroup);
        }

        public IQueryable<VirtualPartsExchangeGroup> GetVirtualPartsExchangeGroups() {
           return  new PartsExchangeGroupAch(this).GetVirtualPartsExchangeGroups();
        }

        public IQueryable<VirtualPartsExchangeGroup> GetVirtualPartsExchangeGroupAndPartsExchanges() {
            return new PartsExchangeGroupAch(this).GetVirtualPartsExchangeGroupAndPartsExchanges();
        }
    }
}
