﻿using System;
using System.Linq;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsExchangeHistoryAch : DcsSerivceAchieveBase {
        public PartsExchangeHistoryAch(DcsDomainService domainService)
            : base(domainService) {
        }

        internal void InsertPartsExchangeHistoryValidate(PartsExchangeHistory partsExchangeHistory) {
            var userInfo = Utils.GetCurrentUserInfo();
            partsExchangeHistory.CreatorId = userInfo.Id;
            partsExchangeHistory.CreatorName = userInfo.Name;
            partsExchangeHistory.CreateTime = DateTime.Now;
        }
        internal void UpdatePartsExchangeHistoryValidate(PartsExchangeHistory partsExchangeHistory) {
        }
        public void InsertPartsExchangeHistory(PartsExchangeHistory partsExchangeHistory) {
            InsertToDatabase(partsExchangeHistory);
            this.InsertPartsExchangeHistoryValidate(partsExchangeHistory);
        }
        public void UpdatePartsExchangeHistory(PartsExchangeHistory partsExchangeHistory) {
            UpdateToDatabase(partsExchangeHistory);
            this.UpdatePartsExchangeHistoryValidate(partsExchangeHistory);
        }
        public IQueryable<PartsExchangeHistory> GetPartsExchangeHistoryWithDetails() {
            return ObjectContext.PartsExchangeHistories.Include("SparePart").OrderBy(e => e.Id);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:29
        //原分布类的函数全部转移到Ach                                                               
                public void InsertPartsExchangeHistory(PartsExchangeHistory partsExchangeHistory) {
            new PartsExchangeHistoryAch(this).InsertPartsExchangeHistory(partsExchangeHistory);
        }
                public void UpdatePartsExchangeHistory(PartsExchangeHistory partsExchangeHistory) {
            new PartsExchangeHistoryAch(this).UpdatePartsExchangeHistory(partsExchangeHistory);
        }
                public IQueryable<PartsExchangeHistory> GetPartsExchangeHistoryWithDetails() {
            return new PartsExchangeHistoryAch(this).GetPartsExchangeHistoryWithDetails();
        }
    }
}
