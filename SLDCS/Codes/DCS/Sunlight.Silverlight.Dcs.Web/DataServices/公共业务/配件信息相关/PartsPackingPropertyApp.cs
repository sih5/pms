﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web{
    partial class PartsPackingPropertyAppAch : DcsSerivceAchieveBase 
    {
        internal void InsertPartsPackingPropertyAppValidate(PartsPackingPropertyApp apps)
        {
            var dbdealer = ObjectContext.PartsPackingPropertyApps.Where(r => r.Code == apps.Code ).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if (dbdealer != null)
                throw new ValidationException(string.Format(ErrorStrings.Dealer_Validation1, apps.Code));

            var userInfo = Utils.GetCurrentUserInfo();
            apps.CreatorId = userInfo.Id;
            apps.CreatorName = userInfo.Name;
            apps.CreateTime = DateTime.Now;
        }

        public void InsertPartsPackingPropertyApp(PartsPackingPropertyApp apps)
        {
            InsertToDatabase(apps);
          //  this.InsertPartsPackingPropertyAppValidate(apps);
        }

        public void UpdatePartsPackingPropertyApp(PartsPackingPropertyApp apps)
        {
            UpdateToDatabase(apps);
          //  this.UpdatePartsPackingPropertyAppValidate(apps);
        }
        internal void UpdatePartsPackingPropertyAppValidate(PartsPackingPropertyApp apps)
        {
            var dbdealer = ObjectContext.PartsPackingPropertyApps.Where(r => r.Id != apps.Id ).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if (dbdealer != null)
                throw new ValidationException(string.Format(ErrorStrings.Dealer_Validation1, apps.Code));

            var userInfo = Utils.GetCurrentUserInfo();
            apps.ModifierId = userInfo.Id;
            apps.ModifierName = userInfo.Name;
            apps.ModifyTime = DateTime.Now;
        }
    }
    partial class DcsDomainService
    {// 已经使用工具进行处理 2017/4/29 12:12:32
        //原分布类的函数全部转移到Ach                                                               

        public void InsertPartsPackingPropertyApp(PartsPackingPropertyApp apps)
        {
            new PartsPackingPropertyAppAch(this).InsertPartsPackingPropertyApp(apps);
        }

        public void UpdatePartsPackingPropertyApp(PartsPackingPropertyApp apps)
        {
            new PartsPackingPropertyAppAch(this).UpdatePartsPackingPropertyApp(apps);
        }
    }
}
