﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsPriceIncreaseRateAch : DcsSerivceAchieveBase {
        public void InsertPartsPriceIncreaseRate(PartsPriceIncreaseRate partsPriceIncreaseRate) {
            InsertToDatabase(partsPriceIncreaseRate);
            this.InsertPartsPriceIncreaseRateValidate(partsPriceIncreaseRate);
        }
        internal void InsertPartsPriceIncreaseRateValidate(PartsPriceIncreaseRate partsPriceIncreaseRate) {
            var userInfo = Utils.GetCurrentUserInfo();
            partsPriceIncreaseRate.CreatorId = userInfo.Id;
            partsPriceIncreaseRate.CreatorName = userInfo.Name;
            partsPriceIncreaseRate.CreateTime = DateTime.Now;
        }
        public void UpdatePartsPriceIncreaseRate(PartsPriceIncreaseRate partsPriceIncreaseRate) {
            UpdateToDatabase(partsPriceIncreaseRate);
            this.UpdatePartsPriceIncreaseRateValidate(partsPriceIncreaseRate);
        }
        internal void UpdatePartsPriceIncreaseRateValidate(PartsPriceIncreaseRate partsPriceIncreaseRate) {
            var userInfo = Utils.GetCurrentUserInfo();
            partsPriceIncreaseRate.ModifierId = userInfo.Id;
            partsPriceIncreaseRate.ModifierName = userInfo.Name;
            partsPriceIncreaseRate.ModifyTime = DateTime.Now;
        }
        public IQueryable<PartsPriceIncreaseRate> 查询配件加价策略() {
            return ObjectContext.PartsPriceIncreaseRates.OrderBy(r => r.Id);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:29
        //原分布类的函数全部转移到Ach                                                               

        public void InsertPartsPriceIncreaseRate(PartsPriceIncreaseRate partsPriceIncreaseRate) {
            new PartsPriceIncreaseRateAch(this).InsertPartsPriceIncreaseRate(partsPriceIncreaseRate);
        }

        public void UpdatePartsPriceIncreaseRate(PartsPriceIncreaseRate partsPriceIncreaseRate) {
            new PartsPriceIncreaseRateAch(this).UpdatePartsPriceIncreaseRate(partsPriceIncreaseRate);
        }

        public IQueryable<PartsPriceIncreaseRate> 查询配件加价策略() {
            return new PartsPriceIncreaseRateAch(this).查询配件加价策略();
        }
    }
}
