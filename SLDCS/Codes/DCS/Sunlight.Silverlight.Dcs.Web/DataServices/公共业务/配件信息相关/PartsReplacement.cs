﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsReplacementAch : DcsSerivceAchieveBase {
        internal void InsertPartsReplacementValidate(PartsReplacement partsReplacement) {
            var dbsparePart = ObjectContext.PartsReplacements.Where(r => r.OldPartId == partsReplacement.OldPartId && r.NewPartId == partsReplacement.NewPartId && r.Status != (int)DcsBaseDataStatus.作废).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbsparePart != null)
                throw new ValidationException(string.Format(ErrorStrings.PartsReplacement_Validation1, partsReplacement.OldPartCode, partsReplacement.NewPartCode));
            var userInfo = Utils.GetCurrentUserInfo();
            partsReplacement.CreatorId = userInfo.Id;
            partsReplacement.CreatorName = userInfo.Name;
            partsReplacement.CreateTime = DateTime.Now;
        }
        internal void UpdatePartsReplacementValidate(PartsReplacement partsReplacement) {
            var dbsparePart = ObjectContext.PartsReplacements.Where(r => r.Id != partsReplacement.Id && r.OldPartId == partsReplacement.OldPartId && r.NewPartId == partsReplacement.NewPartId && r.Status != (int)DcsBaseDataStatus.作废).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbsparePart != null)
                throw new ValidationException(string.Format(ErrorStrings.PartsReplacement_Validation1, partsReplacement.OldPartCode, partsReplacement.NewPartCode));
            var userInfo = Utils.GetCurrentUserInfo();
            partsReplacement.ModifierId = userInfo.Id;
            partsReplacement.ModifierName = userInfo.Name;
            partsReplacement.ModifyTime = DateTime.Now;
        }
        public void InsertPartsReplacement(PartsReplacement partsReplacement) {
            InsertToDatabase(partsReplacement);
            this.InsertPartsReplacementValidate(partsReplacement);
            var newPartsReplacementHistory = new PartsReplacementHistory {
                OldPartId = partsReplacement.OldPartId,
                OldPartCode = partsReplacement.OldPartCode,
                OldPartName = partsReplacement.OldPartName,
                NewPartId = partsReplacement.NewPartId,
                NewPartCode = partsReplacement.NewPartCode,
                NewPartName = partsReplacement.NewPartName,
                Status = partsReplacement.Status,
                Remark = partsReplacement.Remark,
                CreatorId = partsReplacement.CreatorId,
                CreatorName = partsReplacement.CreatorName,
                CreateTime = partsReplacement.CreateTime,
                IsSPM=partsReplacement.IsSPM,
                OldMInAmount = partsReplacement.OldMInAmount,
                RepMInAmount = partsReplacement.RepMInAmount
            };
            partsReplacement.PartsReplacementHistories.Add(newPartsReplacementHistory);
            InsertToDatabase(newPartsReplacementHistory);
            new PartsReplacementHistoryAch(this.DomainService).InsertPartsReplacementHistoryValidate(newPartsReplacementHistory);
        }
        public void UpdatePartsReplacement(PartsReplacement partsReplacement) {
            UpdateToDatabase(partsReplacement);
            this.UpdatePartsReplacementValidate(partsReplacement);
            var newPartsReplacementHistory = new PartsReplacementHistory {
                PartsReplacementId = partsReplacement.Id,
                OldPartId = partsReplacement.OldPartId,
                OldPartCode = partsReplacement.OldPartCode,
                OldPartName = partsReplacement.OldPartName,
                NewPartId = partsReplacement.NewPartId,
                NewPartCode = partsReplacement.NewPartCode,
                NewPartName = partsReplacement.NewPartName,
                Status = partsReplacement.Status,
                Remark = partsReplacement.Remark,
                CreatorId = partsReplacement.CreatorId,
                CreatorName = partsReplacement.CreatorName,
                CreateTime = partsReplacement.CreateTime,
                IsSPM = partsReplacement.IsSPM,
                OldMInAmount = partsReplacement.OldMInAmount,
                RepMInAmount = partsReplacement.RepMInAmount
            };
            InsertToDatabase(newPartsReplacementHistory);
            new PartsReplacementHistoryAch(this.DomainService).InsertPartsReplacementHistoryValidate(newPartsReplacementHistory);
        }
        public IQueryable<PartsReplacement> GetPartsReplacementsByReferenceCode(string referenceCode) {
            if(!string.IsNullOrWhiteSpace(referenceCode)) {
                var res = from a in ObjectContext.PartsReplacements
                          join b in ObjectContext.SpareParts on a.NewPartId equals b.Id
                          join c in ObjectContext.SpareParts on a.OldPartId equals c.Id
                          where b.ReferenceCode.ToUpper().Contains(referenceCode.ToUpper())
                                || c.ReferenceCode.ToUpper().Contains(referenceCode.ToUpper())
                          select a;
                return res.OrderBy(r => r.Id);
            }
            return ObjectContext.PartsReplacements.OrderBy(r => r.Id);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:29
        //原分布类的函数全部转移到Ach                                                               

        public void InsertPartsReplacement(PartsReplacement partsReplacement) {
            new PartsReplacementAch(this).InsertPartsReplacement(partsReplacement);
        }

        public void UpdatePartsReplacement(PartsReplacement partsReplacement) {
            new PartsReplacementAch(this).UpdatePartsReplacement(partsReplacement);
        }

        public IQueryable<PartsReplacement> GetPartsReplacementsByReferenceCode(string referenceCode) {
            return new PartsReplacementAch(this).GetPartsReplacementsByReferenceCode(referenceCode);
        }
    }
}
