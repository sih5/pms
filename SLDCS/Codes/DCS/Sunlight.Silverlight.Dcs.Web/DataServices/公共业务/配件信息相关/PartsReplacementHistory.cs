﻿using System;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsReplacementHistoryAch : DcsSerivceAchieveBase {
        public PartsReplacementHistoryAch(DcsDomainService domainService)
            : base(domainService) {
        }

        internal void InsertPartsReplacementHistoryValidate(PartsReplacementHistory partsReplacementHistory) {
            var userInfo = Utils.GetCurrentUserInfo();
            partsReplacementHistory.CreatorId = userInfo.Id;
            partsReplacementHistory.CreatorName = userInfo.Name;
            partsReplacementHistory.CreateTime = DateTime.Now;
        }
        public void InsertPartsReplacementHistory(PartsReplacementHistory partsReplacementHistory) {
            InsertToDatabase(partsReplacementHistory);
            this.InsertPartsReplacementHistoryValidate(partsReplacementHistory);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:29
        //原分布类的函数全部转移到Ach                                                               

        public void InsertPartsReplacementHistory(PartsReplacementHistory partsReplacementHistory) {
            new PartsReplacementHistoryAch(this).InsertPartsReplacementHistory(partsReplacementHistory);
        }
    }
}
