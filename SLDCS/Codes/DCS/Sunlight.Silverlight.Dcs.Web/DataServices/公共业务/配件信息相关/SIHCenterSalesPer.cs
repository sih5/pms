﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.Text;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class SIHCenterSalesPerAch : DcsSerivceAchieveBase {
        public SIHCenterSalesPerAch(DcsDomainService domainService)
            : base(domainService) {
        }
        public IEnumerable<SIHCenterSalesPer> 获取单中心库销售占比(int? warehouseId,string submitCompanyCode, string submitCompanyName, string sparePartCode, string sparePartName, DateTime? bCreateTime, DateTime? eCreateTime) {
            var userInfo = Utils.GetCurrentUserInfo();
            string SQL = @"select rownum,
						   sparepartcode,
						   sparepartname,
						   submitcompanyname,
						   submitcompanycode,
						   sparepartid,
						   MaxOrderedquantity,
						   sumQty,
						   warehouseid,
						   warehousecode,
						   warehousename,
						   createtime,ReferenceCode,
						   round((MaxOrderedquantity / sumQty) * 100, 2) as perQty
					  from (select ts.sparepartcode,
								   ts.sparepartname,
								   ts.submitcompanyname,
								   ts.submitcompanycode,
								   ts.sparepartid,
								   ts.warehouseid,
								   ts.warehousecode,
								   ts.warehousename, ts.ReferenceCode,
								   max(ts.OrderedQuantityOrder) as MaxOrderedquantity,
								   (select sum(OrderedQuantityOrder)
									  from TemSmartSales t
									 where t.sparepartid = ts.sparepartid
									   and trunc(t.createtime) = trunc(ts.createtime)) as sumQty,
								   trunc(ts.createtime) as createtime
							  from TemSmartSales ts where 1=1 {0}
							 group by ts.sparepartcode,
									  ts.sparepartname,
									  ts.submitcompanyname,
									  ts.submitcompanycode,
									  ts.sparepartid,
										trunc(ts.createtime),
									  ts.warehouseid,
									  ts.warehousecode,ts.ReferenceCode,
									  ts.warehousename)
					 order by warehousecode,submitcompanycode, sparepartcode";
            var serachSQL = new StringBuilder();
           
            if(!string.IsNullOrEmpty(submitCompanyCode)) {
                serachSQL.Append("  and ts.submitCompanyCode like '%" + submitCompanyCode + "%'");
            }
            if(!string.IsNullOrEmpty(submitCompanyName)) {
                serachSQL.Append(" and   ts.submitCompanyName like'%" + submitCompanyName + "%'");
            }
            if(!string.IsNullOrEmpty(sparePartCode)) {
                serachSQL.Append("  and ts.sparePartCode like '%" + sparePartCode + "%'");
            }
            if(!string.IsNullOrEmpty(sparePartName)) {
                serachSQL.Append(" and   ts.sparePartName like'%" + sparePartName + "%'");
            }
            if(bCreateTime.HasValue){

            }
            if(bCreateTime.HasValue) {
                serachSQL.Append(" and trunc(ts.CreateTime)>= trunc(to_date('" + bCreateTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss'))");
            }
            if(eCreateTime.HasValue) {
                serachSQL.Append(" and trunc(ts.CreateTime)<= trunc(to_date('" + eCreateTime.Value.ToString() + "','yyyy-MM-dd HH24:mi:ss'))");
            }
			if(warehouseId.HasValue){
			    serachSQL.Append(" and  ts.warehouseId=" +warehouseId);
			}

            SQL = string.Format(SQL, serachSQL);
            var search = ObjectContext.ExecuteStoreQuery<SIHCenterSalesPer>(SQL).ToList();
            return search;
        }
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:33
        //原分布类的函数全部转移到Ach                                                               
        public IEnumerable<SIHCenterSalesPer> 获取单中心库销售占比(int? warehouseId,string submitCompanyCode, string submitCompanyName, string sparePartCode, string sparePartName, DateTime? bCreateTime, DateTime? eCreateTime) {
            return new SIHCenterSalesPerAch(this).获取单中心库销售占比(warehouseId,submitCompanyCode, submitCompanyName, sparePartCode, sparePartName, bCreateTime, eCreateTime);
        }
    }
}
