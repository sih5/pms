﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using System.Text;
using System.Web.Configuration;
using Microsoft.Data.Extensions;
using Microsoft.VisualBasic;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class SparePartAch : DcsSerivceAchieveBase {
        internal void InsertSparePartValidate(SparePart sparePart) {
            sparePart.Code = Strings.StrConv(sparePart.Code, VbStrConv.Narrow).Replace("【", "[");
            sparePart.Code = strconvsparecode(sparePart.Code);
            var dbsparePart = ObjectContext.SpareParts.Where(r => r.Code.ToLower() == sparePart.Code.ToLower()).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            byte[] bytes = Encoding.Default.GetBytes(sparePart.Code);
            if(bytes.Length > 50) {
                throw new ValidationException(ErrorStrings.SparePart_Validation6);
            }
            if(dbsparePart != null)
                throw new ValidationException(string.Format(ErrorStrings.SparePart_Validation1, sparePart.Code));
            var userInfo = Utils.GetCurrentUserInfo();
            sparePart.CreatorId = userInfo.Id;
            sparePart.CreatorName = userInfo.Name;
            sparePart.CreateTime = DateTime.Now;
        }

        internal string strconvsparecode(string sparecode) {
            string reslut = sparecode.Replace("—", "-").Replace("￥", "$").Replace("……", "^").Replace("×", "*").Replace("】", "]").Replace("＼", @"\").Replace("《", "<").Replace("》", ">").Replace("＃", "#").Replace("｛", "{").Replace("｝", "}");
            return reslut;
        }

        internal void UpdateSparePartValidate(SparePart sparePart) {
            byte[] bytes = Encoding.Default.GetBytes(sparePart.Code);
            if(bytes.Length > 50) {
                throw new ValidationException(ErrorStrings.SparePart_Validation6);
            }
            var userInfo = Utils.GetCurrentUserInfo();
            sparePart.Code = Strings.StrConv(sparePart.Code, VbStrConv.Narrow).Replace("【", "[");
            sparePart.ModifierId = userInfo.Id;
            sparePart.ModifierName = userInfo.Name;
            sparePart.ModifyTime = DateTime.Now;
        }

        public void InsertSparePart(SparePart sparePart) {
            string databaseName = WebConfigurationManager.AppSettings["DataBaseName"];
            if(databaseName == "DCS")
                sparePart.OMSparePartMark = 0;
            InsertToDatabase(sparePart);
            var childCombinedParts = ChangeSet.GetAssociatedChanges(sparePart, v => v.ChildCombinedParts, ChangeOperation.Insert);
            foreach(CombinedPart combinedPart in childCombinedParts) {
                InsertToDatabase(combinedPart);
            }
            this.InsertSparePartValidate(sparePart);
            var sparePartHistory = new SparePartHistory {
                Code = sparePart.Code,
                Name = sparePart.Name,
                EnglishName = sparePart.EnglishName,
                PinyinCode = sparePart.PinyinCode,
                ReferenceCode = sparePart.ReferenceCode,
                IMSCompressionNumber = sparePart.IMSCompressionNumber,
                IMSManufacturerNumber = sparePart.IMSManufacturerNumber,
                ReferenceName = sparePart.ReferenceName,
                CADCode = sparePart.CADCode,
                CADName = sparePart.CADName,
                IsNotWarrantyTransfer = sparePart.IsNotWarrantyTransfer,
                PartType = sparePart.PartType,
                Specification = sparePart.Specification,
                Feature = sparePart.Feature,
                Status = sparePart.Status,
                Length = sparePart.Length,
                OverseasPartsFigure = sparePart.OverseasPartsFigure,
                Width = sparePart.Width,
                Height = sparePart.Height,
                LastSubstitute = sparePart.LastSubstitute,
                NextSubstitute = sparePart.NextSubstitute,
                ShelfLife = sparePart.ShelfLife,
                GroupABCCategory = sparePart.GroupABCCategory,
                Volume = sparePart.Volume,
                Weight = sparePart.Weight,
                Material = sparePart.Material,
                PackingAmount = sparePart.PackingAmount,
                PackingSpecification = sparePart.PackingSpecification,
                PartsOutPackingCode = sparePart.PartsOutPackingCode,
                PartsInPackingCode = sparePart.PartsInPackingCode,
                MeasureUnit = sparePart.MeasureUnit,
                MInPackingAmount = sparePart.MInPackingAmount,
                ExchangeIdentification = sparePart.ExchangeIdentification,
                CreatorId = sparePart.CreatorId,
                CreatorName = sparePart.CreatorName,
                CreateTime = sparePart.CreateTime,
                StandardCode = sparePart.StandardCode,
                StandardName = sparePart.StandardName,
                DeclareElement = sparePart.DeclareElement
            };
            sparePart.SparePartHistories.Add(sparePartHistory);
            InsertToDatabase(sparePartHistory);
            new SparePartHistoryAch(this.DomainService).InsertSparePartHistoryValidate(sparePartHistory);
            var userInfo = Utils.GetCurrentUserInfo();
            if(!string.IsNullOrEmpty(sparePart.ExchangeIdentification)) {
                #region 老版本互换号
                //var spareParts = ObjectContext.SpareParts.Where(r => r.ExchangeIdentification == sparePart.ExchangeIdentification).ToArray();
                ////查找对应的配件信息，若有2个及2个以上有相同识别号，只新增本条互换关系，若只有1个，新增原配件及本条的互换关系
                //if(spareParts.Length >= 2) {
                //    var newPartExchange = new PartsExchange {
                //        ExchangeCode = sparePart.ExchangeIdentification,
                //        ExchangeName = sparePart.ReferenceName,
                //        PartId = sparePart.Id,
                //        Status = (int)DcsBaseDataStatus.有效,
                //        Remark = "由于新增配件互换识别号导致",
                //        CreatorId = userInfo.Id,
                //        CreatorName = userInfo.Name,
                //        CreateTime = DateTime.Now
                //    };
                //    newPartExchange.PartsExchangeHistories.Add(new PartsExchangeHistory {
                //        ExchangeCode = sparePart.ExchangeIdentification,
                //        ExchangeName = sparePart.ReferenceName,
                //        PartId = sparePart.Id,
                //        Status = (int)DcsBaseDataStatus.有效,
                //        Remark = "由于新增配件互换识别号导致",
                //        CreatorId = userInfo.Id,
                //        CreatorName = userInfo.Name,
                //        CreateTime = DateTime.Now
                //    });
                //    InsertToDatabase(newPartExchange);
                //    new PartsExchangeAch(this.DomainService).InsertPartsExchangeValidate(newPartExchange);
                //} else if(spareParts.Length == 1) {
                //    var newPartExchange = new PartsExchange {
                //        ExchangeCode = sparePart.ExchangeIdentification,
                //        ExchangeName = sparePart.ReferenceName,
                //        PartId = sparePart.Id,
                //        Status = (int)DcsBaseDataStatus.有效,
                //        Remark = "由于新增配件互换识别号导致",
                //        CreatorId = userInfo.Id,
                //        CreatorName = userInfo.Name,
                //        CreateTime = DateTime.Now
                //    };
                //    newPartExchange.PartsExchangeHistories.Add(new PartsExchangeHistory {
                //        ExchangeCode = sparePart.ExchangeIdentification,
                //        ExchangeName = sparePart.ReferenceName,
                //        PartId = sparePart.Id,
                //        Status = (int)DcsBaseDataStatus.有效,
                //        Remark = "由于新增配件互换识别号导致",
                //        CreatorId = userInfo.Id,
                //        CreatorName = userInfo.Name,
                //        CreateTime = DateTime.Now
                //    });
                //    InsertToDatabase(newPartExchange);
                //    new PartsExchangeAch(this.DomainService).InsertPartsExchangeValidate(newPartExchange);
                //    var newPartExchangeOld = new PartsExchange {
                //        ExchangeCode = spareParts.FirstOrDefault().ExchangeIdentification,
                //        ExchangeName = spareParts.FirstOrDefault().ReferenceName,
                //        PartId = spareParts.FirstOrDefault().Id,
                //        Status = (int)DcsBaseDataStatus.有效,
                //        Remark = "由于新增配件互换识别号导致",
                //        CreatorId = userInfo.Id,
                //        CreatorName = userInfo.Name,
                //        CreateTime = DateTime.Now
                //    };
                //    newPartExchangeOld.PartsExchangeHistories.Add(new PartsExchangeHistory {
                //        ExchangeCode = spareParts.FirstOrDefault().ExchangeIdentification,
                //        ExchangeName = spareParts.FirstOrDefault().ReferenceName,
                //        PartId = spareParts.FirstOrDefault().Id,
                //        Status = (int)DcsBaseDataStatus.有效,
                //        Remark = "由于新增配件互换识别号导致",
                //        CreatorId = userInfo.Id,
                //        CreatorName = userInfo.Name,
                //        CreateTime = DateTime.Now
                //    });
                //    InsertToDatabase(newPartExchangeOld);
                //    new PartsExchangeAch(this.DomainService).InsertPartsExchangeValidate(newPartExchangeOld);
                //} 
                #endregion
                var newPartExchange = new PartsExchange {
                    ExchangeCode = sparePart.ExchangeIdentification,
                    ExchangeName = sparePart.ReferenceName,
                    PartId = sparePart.Id,
                    Status = (int)DcsBaseDataStatus.有效,
                    Remark = "由于新增配件互换识别号导致",
                    CreatorId = userInfo.Id,
                    CreatorName = userInfo.Name,
                    CreateTime = DateTime.Now
                };
                newPartExchange.PartsExchangeHistories.Add(new PartsExchangeHistory {
                    ExchangeCode = sparePart.ExchangeIdentification,
                    ExchangeName = sparePart.ReferenceName,
                    PartId = sparePart.Id,
                    Status = (int)DcsBaseDataStatus.有效,
                    Remark = "由于新增配件互换识别号导致",
                    CreatorId = userInfo.Id,
                    CreatorName = userInfo.Name,
                    CreateTime = DateTime.Now
                });
                InsertToDatabase(newPartExchange);
                new PartsExchangeAch(this.DomainService).InsertPartsExchangeValidate(newPartExchange);
                var spareParts = ObjectContext.SpareParts.Where(r => r.ExchangeIdentification == sparePart.ExchangeIdentification && r.Status == (int)DcsBaseDataStatus.有效).ToArray();
                var partsExchangeGroups = ObjectContext.PartsExchangeGroups.Where(r => r.ExchangeCode == sparePart.ExchangeIdentification && r.Status == (int)DcsBaseDataStatus.有效).ToArray();
                //查找配件互换号只有1个
                if(spareParts.Length == 1 && !partsExchangeGroups.Any()) {
                    var partsExchangeGroup = new PartsExchangeGroup {
                        ExGroupCode = CodeGenerator.Generate("PartsExchange"),
                        ExchangeCode = sparePart.ExchangeIdentification,
                        Status = (int)DcsBaseDataStatus.有效
                    };
                    InsertToDatabase(partsExchangeGroup);
                    new PartsExchangeGroupAch(this.DomainService).InsertPartsExchangeGroupValidate(partsExchangeGroup);
                }
            }
        }

        public void UpdateSparePart(SparePart sparePart) {
            string databaseName = WebConfigurationManager.AppSettings["DataBaseName"];
            if(databaseName == "DCS")
                sparePart.OMSparePartMark = 0;
            sparePart.ChildCombinedParts.Clear();
            UpdateToDatabase(sparePart);
            var childCombinedParts = ChangeSet.GetAssociatedChanges(sparePart, v => v.ChildCombinedParts);
            foreach(CombinedPart combinedPart in childCombinedParts) {
                switch(ChangeSet.GetChangeOperation(combinedPart)) {
                    case ChangeOperation.Insert:
                        InsertToDatabase(combinedPart);
                        break;
                    case ChangeOperation.Update:
                        UpdateToDatabase(combinedPart);
                        break;
                    case ChangeOperation.Delete:
                        DeleteFromDatabase(combinedPart);
                        break;
                }
            }
            this.UpdateSparePartValidate(sparePart);
            var sparePartHistory = new SparePartHistory {
                Code = sparePart.Code,
                Name = sparePart.Name,
                LastSubstitute = sparePart.LastSubstitute,
                NextSubstitute = sparePart.NextSubstitute,
                ShelfLife = sparePart.ShelfLife,
                GroupABCCategory = sparePart.GroupABCCategory,
                EnglishName = sparePart.EnglishName,
                IMSCompressionNumber = sparePart.IMSCompressionNumber,
                IMSManufacturerNumber = sparePart.IMSManufacturerNumber,
                PinyinCode = sparePart.PinyinCode,
                ReferenceCode = sparePart.ReferenceCode,
                OverseasPartsFigure = sparePart.OverseasPartsFigure,
                ReferenceName = sparePart.ReferenceName,
                CADCode = sparePart.CADCode,
                IsNotWarrantyTransfer = sparePart.IsNotWarrantyTransfer,
                CADName = sparePart.CADName,
                PartType = sparePart.PartType,
                Specification = sparePart.Specification,
                Feature = sparePart.Feature,
                Status = sparePart.Status,
                Length = sparePart.Length,
                Width = sparePart.Width,
                Height = sparePart.Height,
                Volume = sparePart.Volume,
                Weight = sparePart.Weight,
                Material = sparePart.Material,
                PackingAmount = sparePart.PackingAmount,
                PackingSpecification = sparePart.PackingSpecification,
                PartsOutPackingCode = sparePart.PartsOutPackingCode,
                PartsInPackingCode = sparePart.PartsInPackingCode,
                MeasureUnit = sparePart.MeasureUnit,
                MInPackingAmount = sparePart.MInPackingAmount,
                ExchangeIdentification = sparePart.ExchangeIdentification,
                ProductBrand = sparePart.ProductBrand,
                CreatorId = sparePart.CreatorId,
                CreatorName = sparePart.CreatorName,
                CreateTime = sparePart.CreateTime,
                SubstandardName = sparePart.SubstandardName,//配件标准名称						
                TotalNumber = sparePart.TotalNumber,//总成型号						
                Factury = sparePart.Factury,//厂商						
                IsOriginal = sparePart.IsOriginal,//是否原厂件	
                CategoryCode = sparePart.CategoryCode,//分类编码						
                CategoryName = sparePart.CategoryName,//分类名称	
                StandardCode = sparePart.StandardCode,
                StandardName = sparePart.StandardName,
                DeclareElement = sparePart.DeclareElement
            };
            sparePart.SparePartHistories.Add(sparePartHistory);
            InsertToDatabase(sparePartHistory);
            new SparePartHistoryAch(this.DomainService).InsertSparePartHistoryValidate(sparePartHistory);
        }

        public IQueryable<SparePart> GetSparePartsWithParentCombinedParts() {
            return ObjectContext.SpareParts.Include("ParentCombinedParts.ChildSparePart").OrderBy(v => v.Id);
        }

        public SparePart GetSparePartsWithParentCombinedPartsById(int id) {
            var dbSparePart = ObjectContext.SpareParts.SingleOrDefault(e => e.Id == id && e.Status == (int)DcsMasterDataStatus.有效);
            if(dbSparePart != null) {
                var combinedParts = ObjectContext.CombinedParts.Where(e => e.ParentId == id && ObjectContext.SpareParts.Any(entity => entity.Status == (int)DcsMasterDataStatus.有效 && entity.Id == e.PartId)).ToArray();
                var combinedSparePartIds = combinedParts.Select(e => e.PartId).ToArray();
                if(combinedSparePartIds.Any()) {
                    var combinedSpareParts = ObjectContext.SpareParts.Where(e => combinedSparePartIds.Contains(e.Id) && e.Status == (int)DcsMasterDataStatus.有效).ToArray();
                }
            }
            return dbSparePart;
        }
        [Query(HasSideEffects = true)]
        public IQueryable<SparePart> GetSparePartsWithPartsBranchByBranchIdAndPartIds(int branchId, int[] partIds) {
            var partsBranches = ObjectContext.PartsBranches.Where(e => partIds.Contains(e.PartId) && e.BranchId == branchId && e.Status == (int)DcsBaseDataStatus.有效).ToArray();
            if(partsBranches.Any())
                return ObjectContext.SpareParts.Where(e => partIds.Contains(e.Id) && e.Status == (int)DcsMasterDataStatus.有效).OrderBy(r => r.Id);
            return null;
        }

        [Query(HasSideEffects = true)]
        public IQueryable<VirtualSparePart> 查询配件合同价(int branchId, int partsSupplierId, DateTime time, List<int> sparePartIds, int partsSaleCategoryId) {
            var branchStrategy = ObjectContext.Branchstrategies.FirstOrDefault(r => r.BranchId == branchId && r.Status == (int)DcsBaseDataStatus.有效);
            if(branchStrategy == null) {
                throw new ValidationException(ErrorStrings.SparePart_Validation7);
            }

            var salesCenterstrategy = this.ObjectContext.SalesCenterstrategies.SingleOrDefault(r => r.PartsSalesCategoryId == partsSaleCategoryId && r.Status == (int)DcsBaseDataStatus.有效);
            if(salesCenterstrategy == null)
                throw new ValidationException(ErrorStrings.SupplierShippingOrder_Validation31);

            var branch = ObjectContext.Branches.FirstOrDefault(r => r.Id == branchId && r.Status == (int)DcsBaseDataStatus.有效);
            if(branch == null) {
                throw new ValidationException(ErrorStrings.SparePart_Validation8);
            }
            IQueryable<SparePart> tempSparePartQuery;
            if(branchStrategy.PurchaseIsNeedStd.HasValue && branchStrategy.PurchaseIsNeedStd.Value) {
                tempSparePartQuery = ObjectContext.SpareParts.Where(v => v.Status == (int)DcsMasterDataStatus.有效 && ObjectContext.PartsPlannedPrices.Any(r => r.SparePartId == v.Id && r.PartsSalesCategoryId == partsSaleCategoryId));
            } else {
                tempSparePartQuery = ObjectContext.SpareParts.Where(v => v.Status == (int)DcsMasterDataStatus.有效);
            }
            var PartsPurchasePricingStrategyFlag = salesCenterstrategy.PartsPurchasePricingStrategy == (int)DcsBranchstrategyPartsPurchasePricingStrategy.后定价;
            var resultQuery = (from a in tempSparePartQuery
                               join b in ObjectContext.PartsBranches.Where(v => v.PartsSalesCategoryId == partsSaleCategoryId && v.BranchId == branchId && v.Status == (int)DcsBaseDataStatus.有效 && v.IsOrderable) on a.Id equals b.PartId
                               join d in ObjectContext.PartsSupplierRelations.Where(v => v.PartsSalesCategoryId == partsSaleCategoryId && v.SupplierId == partsSupplierId && v.Status == (int)DcsBaseDataStatus.有效) on a.Id equals d.PartId
                               join e in ObjectContext.PartsSuppliers.Where(v => v.Id == partsSupplierId && v.Status == (int)DcsMasterDataStatus.有效) on d.SupplierId equals e.Id
                               join f in ObjectContext.PartsPurchasePricings.Where(v => v.PartsSupplierId == partsSupplierId && v.BranchId == branchId && v.PartsSalesCategoryId == partsSaleCategoryId && v.ValidFrom <= time && v.ValidTo >= time) on a.Id equals f.PartId into t1
                               from g in t1.DefaultIfEmpty()
                               where PartsPurchasePricingStrategyFlag || g != null && g.PurchasePrice != 0
                               select new VirtualSparePart {
                                   Id = a.Id,
                                   Code = a.Code,
                                   Feature = a.Feature,
                                   MeasureUnit = a.MeasureUnit,
                                   Name = a.Name,
                                   PartsBranchName = b.BranchName,
                                   PurchasePrice = g.PurchasePrice,
                                   PartsSalesCategoryName = b.PartsSalesCategoryName,
                                   PartsSupplierCode = e.Code,
                                   PartsSupplierName = e.Name,
                                   ReferenceCode = a.ReferenceCode,
                                   Specification = a.Specification,
                                   Status = a.Status,
                                   PackingAmount = a.PackingAmount,
                                   PackingSpecification = a.PackingSpecification,
                                   SupplierPartCode = d.SupplierPartCode,
                                   LimitQty = g.LimitQty ?? 0,
                                   UsedQty = g.UsedQty ?? 0
                               });
            if(sparePartIds != null && sparePartIds.Count > 1) {
                resultQuery = resultQuery.Where(r => sparePartIds.Contains(r.Id));
            }
            return resultQuery.Distinct().OrderBy(r => r.Id);

        }

        [Query(HasSideEffects = true)]
        public IQueryable<VirtualSparePart> 查询配件合同价采购计划(int branchId, List<int> sparePartIds, int partsSaleCategoryId) {
            //var branchStrategy = ObjectContext.Branchstrategies.FirstOrDefault(r => r.BranchId == branchId && r.Status == (int)DcsBaseDataStatus.有效);
            //if(branchStrategy == null) {
            //    throw new ValidationException("未找到分公司策略");
            //}

         //   var salesCenterstrategy = this.ObjectContext.SalesCenterstrategies.SingleOrDefault(r => r.PartsSalesCategoryId == partsSaleCategoryId && r.Status == (int)DcsBaseDataStatus.有效);
            //if(salesCenterstrategy == null)
            //    throw new ValidationException("未找到对应的销售中心策略");

            //var branch = ObjectContext.Branches.FirstOrDefault(r => r.Id == branchId && r.Status == (int)DcsBaseDataStatus.有效);
            //if(branch == null) {
            //    throw new ValidationException("未找到分公司");
            //}
            //IQueryable<SparePart> tempSparePartQuery;
            //if(branchStrategy.PurchaseIsNeedStd.HasValue && branchStrategy.PurchaseIsNeedStd.Value) {
            //    tempSparePartQuery = ObjectContext.SpareParts.Where(v => v.Status == (int)DcsMasterDataStatus.有效 && ObjectContext.PartsPlannedPrices.Any(r => r.SparePartId == v.Id && r.PartsSalesCategoryId == partsSaleCategoryId));
            //} else {
            //    tempSparePartQuery = ObjectContext.SpareParts.Where(v => v.Status == (int)DcsMasterDataStatus.有效);
            //}
            var time = DateTime.Now;
           // var PartsPurchasePricingStrategyFlag = salesCenterstrategy.PartsPurchasePricingStrategy == (int)DcsBranchstrategyPartsPurchasePricingStrategy.后定价;
            var resultQuery = (from a in ObjectContext.SpareParts
                               join b in ObjectContext.PartsBranches.Where(v => v.PartsSalesCategoryId == partsSaleCategoryId && v.BranchId == branchId && v.Status == (int)DcsBaseDataStatus.有效 && v.IsOrderable) on a.Id equals b.PartId
                               join d in ObjectContext.PartsSupplierRelations.Where(v => v.PartsSalesCategoryId == partsSaleCategoryId && v.Status == (int)DcsBaseDataStatus.有效 && v.IsPrimary == true) on a.Id equals d.PartId
                               join e in ObjectContext.PartsSuppliers.Where(v => v.Status == (int)DcsMasterDataStatus.有效) on d.SupplierId equals e.Id
                               join f in ObjectContext.PartsPurchasePricings.Where(o=>o.PurchasePrice != 0) on new {
                                   PartId=a.Id,
                                   PartsSupplierId= e.Id
                               } equals new {
                                   PartId=f.PartId,
                                   PartsSupplierId= f.PartsSupplierId
                               }
                               join h in ObjectContext.PartsBranchPackingProps on new {
                                   Id = b.Id,
                                   SparePartId = b.PartId,
                                   PackingType = b.MainPackingType
                               } equals new {
                                   Id = h.PartsBranchId.Value,
                                   SparePartId = h.SparePartId.Value,
                                   PackingType = h.PackingType
                               } into l
                               from m in l.DefaultIfEmpty()
                               where  f.ValidFrom <= time && f.ValidTo >= time
                               group a by new {
                                   a.Id,
                                   a.Code,
                                   a.Feature,
                                   a.MeasureUnit,
                                   a.Name,
                                   b.BranchName,
                                   f.PurchasePrice,
                                   f.LimitQty,
                                   f.UsedQty,
                                   b.PartsSalesCategoryName,
                                   a.ReferenceCode,
                                   a.Specification,
                                   a.Status,
                                   a.PackingAmount,
                                   a.PackingSpecification,
                                   PartsSupplierCode = e.Code,
                                   PartsSupplierName = e.Name,
                                   d.SupplierPartCode,
                                   m.PackingCoefficient,
                                   b.PartABC
                               } into ss
                               select new VirtualSparePart {
                                   Id = ss.Key.Id,
                                   Code = ss.Key.Code,
                                   Feature = ss.Key.Feature,
                                   MeasureUnit = ss.Key.MeasureUnit,
                                   Name = ss.Key.Name,
                                   PartsBranchName = ss.Key.BranchName,
                                   PurchasePrice = ss.Key.PurchasePrice,
                                   PartsSalesCategoryName = ss.Key.PartsSalesCategoryName,
                                   PartsSupplierCode = ss.Key.PartsSupplierCode,
                                   PartsSupplierName = ss.Key.PartsSupplierName,
                                   ReferenceCode = ss.Key.ReferenceCode,
                                   Specification = ss.Key.Specification,
                                   Status = ss.Key.Status,
                                   PackingAmount = ss.Key.PackingAmount,
                                   PackingSpecification = ss.Key.PackingSpecification,
                                   SupplierPartCode = ss.Key.SupplierPartCode,
                                   PackingCoefficient = ss.Key.PackingCoefficient,
                                   LimitQty = ss.Key.LimitQty,
                                   UsedQty = ss.Key.UsedQty,
                                   PartABC=ss.Key.PartABC
                               });
            if(sparePartIds != null && sparePartIds.Count > 1) {
                resultQuery = resultQuery.Where(r => sparePartIds.Contains(r.Id) && r.PartsSalesCategoryId == partsSaleCategoryId);
            }
            return resultQuery.Distinct().OrderBy(r => r.Id);

        }
        [Query(HasSideEffects = true)]
        public IQueryable<VirtualSparePart> 跨区域销售查询配件销售价() {
            var result = from a in ObjectContext.SpareParts.Where(t=>t.Status==(int)DcsMasterDataStatus.有效)
                         join b in ObjectContext.PartsBranches.Where(t=>t.Status==(int)DcsMasterDataStatus.有效 && t.IsSalable==true) on a.Id equals b.PartId
                         join c in ObjectContext.PartsSalePriceIncreaseRates on b.IncreaseRateGroupId equals c.Id into cc
                         from s in cc.DefaultIfEmpty()
                         join t in ObjectContext.PartsSalesPrices.Where(t => t.Status == (int)DcsBaseDataStatus.有效) on a.Id equals t.SparePartId
                         join y in ObjectContext.PartsRetailGuidePrices.Where(t=>t.Status==(int)DcsBaseDataStatus.有效) on a.Id equals y.SparePartId
                         select new VirtualSparePart {
                             Id=a.Id,
                             Code=a.Code,
                             Name=a.Name,
                             PartABC=b.PartABC,
                             PriceTypeName = s.GroupName,
                             CenterPrice=t.CenterPrice.Value,
                             SalesPrice=t.SalesPrice,
                             RetailGuidePrice=y.RetailGuidePrice
                         };
            return result;
        }
        [Query(HasSideEffects = true)]
        public IQueryable<SparePart> GetSparePartsByIds(int[] partIds) {
            return ObjectContext.SpareParts.Where(e => partIds.Contains(e.Id)).OrderBy(r => r.Id);
        }
        [Query(HasSideEffects = true)]
        public IEnumerable<SparePart> 查询配件批发价(int partsSalesCategoryId, int[] partsIds) {
            var sparePartSql = ObjectContext.SpareParts.Where(r => r.Status != (int)DcsMasterDataStatus.作废);
            var partsSalePriceSql = ObjectContext.PartsSalesPrices.Where(r => r.Status != (int)DcsBaseDataStatus.作废 && r.PriceType == (int)DcsPartsSalesPricePriceType.基准销售价 && r.PartsSalesCategoryId == partsSalesCategoryId);
            if(partsIds != null && partsIds.Length > 0) {
                sparePartSql = sparePartSql.Where(r => partsIds.Contains(r.Id));
                partsSalePriceSql = partsSalePriceSql.Where(r => partsIds.Contains(r.SparePartId));
            }
            var spareParts = sparePartSql.ToArray();
            var partsSalePrices = partsSalePriceSql.ToArray();
            for(var i = 0; i < spareParts.Count(); i++) {
                var partsSalePrice = partsSalePrices.FirstOrDefault(r => r.SparePartId == spareParts[i].Id);
                if(partsSalePrice != null)
                    spareParts[i].PartsSalePrice = partsSalePrice.SalesPrice;
                else
                    spareParts[i].PartsSalePrice = 0;
            }
            return spareParts.OrderBy(r => r.Id);
        }

        public IEnumerable<SparePart> 查询配件供应商(int? partsId, string supplierCode, string supplierName, int partsSalecategoryId) {
            var sparePartSql = ObjectContext.SpareParts.Where(r => r.Status != (int)DcsMasterDataStatus.作废);
            if(partsId.HasValue)
                sparePartSql = sparePartSql.Where(r => r.Id == partsId.Value);
            var spareParts = sparePartSql.ToArray();
            var partsIds = spareParts.Select(r => r.Id).ToArray();
            var partsSupplierRelation = ObjectContext.PartsSupplierRelations.Where(r => partsIds.Contains(r.PartId) && r.PartsSalesCategoryId == partsSalecategoryId).ToArray();
            var supplierIds = partsSupplierRelation.Select(r => r.SupplierId).ToArray();
            var suppliersSql = ObjectContext.PartsSuppliers.Where(r => supplierIds.Contains(r.Id) && r.Status == (int)DcsMasterDataStatus.有效);
            if(string.IsNullOrEmpty(supplierName)) {
                suppliersSql = suppliersSql.Where(r => supplierName.ToLower().Contains(r.Name.ToLower()));
            }
            if(string.IsNullOrEmpty(supplierCode)) {
                suppliersSql = suppliersSql.Where(r => supplierCode.ToLower().Contains(r.Code.ToLower()));
            }
            var suppliers = suppliersSql.ToArray();
            var partsBranchs = ObjectContext.PartsBranches.Where(r => partsIds.Contains(r.PartId) && r.PartsSalesCategoryId == partsSalecategoryId && r.Status == (int)DcsBaseDataStatus.有效).ToArray();
            return spareParts.OrderBy(r => r.Id);
        }

        public IQueryable<FaultyPart> 查询保外配件供应商(int? partsId, string supplierCode, string supplierName, int partsSalecategoryId) {
            var result = from sparePart in ObjectContext.SpareParts.Where(r => r.Status != (int)DcsMasterDataStatus.作废)
                         join partsSupplierRelation in ObjectContext.PartsSupplierRelations.Where(r => r.PartsSalesCategoryId == partsSalecategoryId) on sparePart.Id equals partsSupplierRelation.PartId
                         into tempTable
                         from t in tempTable.DefaultIfEmpty()
                         join partsSupplier in ObjectContext.PartsSuppliers.Where(r => r.Status == (int)DcsMasterDataStatus.有效) on t.SupplierId equals partsSupplier.Id
                         into tempTable1
                         from t1 in tempTable1.DefaultIfEmpty()
                         join partsBranch in ObjectContext.PartsBranches.Where(r => r.PartsSalesCategoryId == partsSalecategoryId && r.Status == (int)DcsBaseDataStatus.有效) on sparePart.Id equals partsBranch.PartId
                         into tempTable2
                         from t2 in tempTable2.DefaultIfEmpty()
                         where (partsId == null || sparePart.Id == partsId) &&
                         (supplierCode == null || t1.Code == supplierCode) &&
                         (supplierName == null || t1.Name == supplierName)
                         select new FaultyPart {
                             Id = sparePart.Id,
                             SupplierCode = t1.Code,
                             SupplierName = t1.Name,
                             Code = sparePart.Code,
                             Name = sparePart.Name,
                             ReferenceCode = sparePart.ReferenceCode,
                             ReferenceName = sparePart.ReferenceName,
                             Feature = sparePart.Feature,
                             Status = sparePart.Status,
                             MeasureUnit = sparePart.MeasureUnit,
                             PartsSalecategoryId = t.PartsSalesCategoryId,
                             SupplierId = t.SupplierId,
                             PartsWarrantyCategoryId = t2.PartsWarrantyCategoryId
                         };
            return result.OrderBy(r => r.Id);
        }

        public IQueryable<FaultyPart> 查询祸首件(int? partsId, string supplierCode, string supplierName, int partsSalecategoryId, int branchId) {
            var spareParts = ObjectContext.SpareParts.AsQueryable();
            if(partsId.HasValue) {
                spareParts = ObjectContext.SpareParts.Where(r => r.Id == partsId);
            }
            var branchSupplierRelations = ObjectContext.BranchSupplierRelations.AsQueryable();
            if(!string.IsNullOrWhiteSpace(supplierCode)) {
                branchSupplierRelations = ObjectContext.BranchSupplierRelations.Where(r => r.BusinessCode.Contains(supplierCode));
            }
            if(!string.IsNullOrWhiteSpace(supplierName)) {
                branchSupplierRelations = ObjectContext.BranchSupplierRelations.Where(r => r.BusinessName.Contains(supplierName));
            }

            var result = from sparePart in spareParts.Where(r => r.Status == (int)DcsMasterDataStatus.有效)
                         join partsBranch in ObjectContext.PartsBranches.Where(r => r.PartsSalesCategoryId == partsSalecategoryId && r.BranchId == branchId && r.Status == (int)DcsBaseDataStatus.有效) on sparePart.Id equals partsBranch.PartId
                         join partsSupplierRelation in ObjectContext.PartsSupplierRelations.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on sparePart.Id equals partsSupplierRelation.PartId
                         join branchSupplierRelation in branchSupplierRelations.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on
                         new {
                             partsSupplierRelation.SupplierId,
                             partsSupplierRelation.BranchId,
                             partsBranch.PartsSalesCategoryId
                         } equals
                            new {
                                branchSupplierRelation.SupplierId,
                                branchSupplierRelation.BranchId,
                                branchSupplierRelation.PartsSalesCategoryId
                            }
                         where partsBranch.PartsSalesCategoryId == partsSalecategoryId &&
                               sparePart.Id == partsSupplierRelation.PartId &&
                               sparePart.Id == partsBranch.PartId &&
                               partsSupplierRelation.PartsSalesCategoryId == partsSalecategoryId
                         select new FaultyPart {
                             Id = sparePart.Id,
                             SupplierCode = branchSupplierRelation.BusinessCode,
                             SupplierName = branchSupplierRelation.BusinessName,
                             Code = sparePart.Code,
                             Name = sparePart.Name,
                             ReferenceCode = sparePart.ReferenceCode,
                             ReferenceName = sparePart.ReferenceName,
                             Feature = sparePart.Feature,
                             Status = sparePart.Status,
                             MeasureUnit = sparePart.MeasureUnit,
                             PartsSalecategoryId = partsSupplierRelation.PartsSalesCategoryId,
                             SupplierId = partsSupplierRelation.SupplierId,
                             PartsWarrantyCategoryId = partsBranch.PartsWarrantyCategoryId
                         };
            return result.OrderBy(r => r.Id);
        }

        public IQueryable<FaultyPart> 查询祸首件2(int? partsId, string sparePartCode, string sparePartName, string supplierCode, string supplierName, string referenceCode, string referenceName, int partsSalecategoryId, int branchId) {


            var countNumber = 200;
            IQueryable<SparePart> sparepartQuery;


            if(partsId.HasValue || !String.IsNullOrWhiteSpace(sparePartCode) || !String.IsNullOrWhiteSpace(sparePartName) || !String.IsNullOrWhiteSpace(referenceCode) || !String.IsNullOrWhiteSpace(referenceName)) {
                var spareParts = ObjectContext.SpareParts.AsQueryable();
                if(partsId.HasValue) {
                    spareParts = spareParts.Where(r => r.Id == partsId);
                }
                if(!String.IsNullOrWhiteSpace(sparePartCode)) {
                    var UpperSparePartCode = sparePartCode.ToUpper();
                    spareParts = spareParts.Where(r => r.Code.IndexOf(UpperSparePartCode) != -1);
                }
                if(!String.IsNullOrWhiteSpace(sparePartName)) {
                    var UpperSparePartName = sparePartName.ToUpper();
                    spareParts = spareParts.Where(r => r.Name.IndexOf(UpperSparePartName) != -1);
                }
                if(!String.IsNullOrWhiteSpace(referenceCode)) {
                    var UpperReferenceCode = referenceCode.ToUpper();
                    spareParts = spareParts.Where(r => r.ReferenceCode.IndexOf(UpperReferenceCode) != -1);
                }
                if(!String.IsNullOrWhiteSpace(referenceName)) {
                    var UpperReferenceName = referenceName.ToUpper();
                    spareParts = spareParts.Where(r => r.ReferenceName.IndexOf(UpperReferenceName) != -1);
                }
                var tempCount = spareParts.Count();
                if(tempCount < countNumber) {
                    var sparePartIds = spareParts.Select(r => r.Id).ToArray();
                    sparepartQuery = ObjectContext.SpareParts.Where(r => r.Status == (int)DcsMasterDataStatus.有效 && sparePartIds.Contains(r.Id));
                } else {
                    sparepartQuery = spareParts;
                }

            } else {
                sparepartQuery = ObjectContext.SpareParts.Where(r => r.Status == (int)DcsMasterDataStatus.有效);
            }
            var branchSupplierRelations = ObjectContext.BranchSupplierRelations.AsQueryable();
            if(!string.IsNullOrWhiteSpace(supplierCode)) {
                branchSupplierRelations = ObjectContext.BranchSupplierRelations.Where(r => r.BusinessCode.Contains(supplierCode));
            }
            if(!string.IsNullOrWhiteSpace(supplierName)) {
                branchSupplierRelations = ObjectContext.BranchSupplierRelations.Where(r => r.BusinessName.Contains(supplierName));
            }
            var result = from sparePart in sparepartQuery
                         join partsBranch in ObjectContext.PartsBranches.Where(r => r.PartsSalesCategoryId == partsSalecategoryId && r.BranchId == branchId && r.Status == (int)DcsBaseDataStatus.有效) on sparePart.Id equals partsBranch.PartId
                         join partsSupplierRelation in ObjectContext.PartsSupplierRelations.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on sparePart.Id equals partsSupplierRelation.PartId
                         join branchSupplierRelation in branchSupplierRelations.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on
                         new {
                             partsSupplierRelation.SupplierId,
                             partsSupplierRelation.BranchId,
                             partsBranch.PartsSalesCategoryId
                         } equals
                            new {
                                branchSupplierRelation.SupplierId,
                                branchSupplierRelation.BranchId,
                                branchSupplierRelation.PartsSalesCategoryId
                            }
                         where partsBranch.PartsSalesCategoryId == partsSalecategoryId &&
                               sparePart.Id == partsSupplierRelation.PartId &&
                               sparePart.Id == partsBranch.PartId &&
                               partsSupplierRelation.PartsSalesCategoryId == partsSalecategoryId
                         select new FaultyPart {
                             Id = sparePart.Id,
                             SupplierCode = branchSupplierRelation.BusinessCode,
                             SupplierName = branchSupplierRelation.BusinessName,
                             Code = sparePart.Code,
                             Name = sparePart.Name,
                             ReferenceCode = sparePart.ReferenceCode,
                             ReferenceName = sparePart.ReferenceName,
                             Feature = sparePart.Feature,
                             Status = sparePart.Status,
                             MeasureUnit = sparePart.MeasureUnit,
                             PartsSalecategoryId = partsSupplierRelation.PartsSalesCategoryId,
                             SupplierId = partsSupplierRelation.SupplierId,
                             PartsWarrantyCategoryId = partsBranch.PartsWarrantyCategoryId
                         };
            return result.OrderBy(r => r.Id);
        }

        public IQueryable<FaultyPart> 维修索赔查询祸首件(int? partsId, string supplierCode, string supplierName, int partsSalecategoryId) {

            var result = from sparePart in ObjectContext.SpareParts
                         from partsSupplierRelation in ObjectContext.PartsSupplierRelations
                         from branchSupplierRelation in ObjectContext.BranchSupplierRelations
                         from partsBranch in ObjectContext.PartsBranches
                         where sparePart.Status != (int)DcsMasterDataStatus.作废 &&
                         branchSupplierRelation.Status == (int)DcsBaseDataStatus.有效 &&
                         branchSupplierRelation.PartsSalesCategoryId == partsSalecategoryId &&
                         branchSupplierRelation.SupplierId == partsSupplierRelation.SupplierId &&
                         partsBranch.Status == (int)DcsBaseDataStatus.有效 &&
                         partsBranch.PartsSalesCategoryId == partsSalecategoryId &&
                         (partsSupplierRelation.PartsSalesCategoryId == partsSalecategoryId) &&
                         (partsId == null || sparePart.Id == partsId) &&
                         sparePart.Id == partsSupplierRelation.PartId &&
                         sparePart.Id == partsBranch.PartId
                         select new FaultyPart {
                             Id = sparePart.Id,
                             SupplierCode = branchSupplierRelation.BusinessCode,
                             SupplierName = branchSupplierRelation.BusinessName,
                             Code = sparePart.Code,
                             Name = sparePart.Name,
                             ReferenceCode = sparePart.ReferenceCode,
                             ReferenceName = sparePart.ReferenceName,
                             Feature = sparePart.Feature,
                             Status = sparePart.Status,
                             MeasureUnit = sparePart.MeasureUnit,
                             PartsSalecategoryId = partsSupplierRelation.PartsSalesCategoryId,
                             SupplierId = partsSupplierRelation.SupplierId,
                             PartsWarrantyCategoryId = partsBranch.PartsWarrantyCategoryId
                         };
            return result.OrderBy(r => r.Id);

        }

        public IQueryable<PartsInsteadInfo> 查询采购替互换件(int sparePartId, int partsSalesCategoryId, int supplierId) {
            var nowTime = DateTime.Now;
            var partOfResult1 = from a in ObjectContext.PartsReplacements.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.OldPartId == sparePartId)
                                join b in ObjectContext.SpareParts.Where(r => r.Status == (int)DcsMasterDataStatus.有效) on a.NewPartId equals b.Id
                                join c in ObjectContext.PartsBranches.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.PartsSalesCategoryId == partsSalesCategoryId && r.IsOrderable) on a.NewPartId equals c.PartId
                                join d in ObjectContext.PartsSupplierRelations.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.SupplierId == supplierId) on new {
                                    c.PartId,
                                    c.PartsSalesCategoryId
                                } equals new {
                                    d.PartId,
                                    d.PartsSalesCategoryId
                                }
                                join e in ObjectContext.PartsPurchasePricings.Where(r => r.Status == (int)DcsPartsPurchasePricingStatus.生效 && r.ValidFrom <= nowTime && r.ValidTo >= nowTime) on new {
                                    c.PartId,
                                    c.PartsSalesCategoryId
                                } equals new {
                                    e.PartId,
                                    e.PartsSalesCategoryId
                                }
                                select new PartsInsteadInfo {
                                    Id = a.NewPartId,
                                    SparePartCode = b.Code,
                                    SparePartName = b.Name,
                                    InsteadType = (int)DcsPartsReplacementReplacementType.替换,
                                    PurchasePrice = e.PurchasePrice
                                };
            var partOfResult2 = from a in ObjectContext.PartsExchanges.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.PartId == sparePartId)
                                join b in ObjectContext.PartsExchanges.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.PartId != sparePartId) on a.ExchangeCode equals b.ExchangeCode
                                join c in ObjectContext.SpareParts.Where(r => r.Status == (int)DcsMasterDataStatus.有效) on b.PartId equals c.Id
                                join d in ObjectContext.PartsBranches.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.PartsSalesCategoryId == partsSalesCategoryId && r.IsOrderable) on b.PartId equals d.PartId
                                join e in ObjectContext.PartsSupplierRelations.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.SupplierId == supplierId) on new {
                                    d.PartId,
                                    d.PartsSalesCategoryId
                                } equals new {
                                    e.PartId,
                                    e.PartsSalesCategoryId
                                }
                                join f in ObjectContext.PartsPurchasePricings.Where(r => r.Status == (int)DcsPartsPurchasePricingStatus.生效 && r.ValidFrom <= nowTime && r.ValidTo >= nowTime) on new {
                                    d.PartId,
                                    d.PartsSalesCategoryId
                                } equals new {
                                    f.PartId,
                                    f.PartsSalesCategoryId
                                }
                                select new PartsInsteadInfo {
                                    Id = b.PartId,
                                    SparePartCode = c.Code,
                                    SparePartName = c.Name,
                                    InsteadType = (int)DcsPartsReplacementReplacementType.互换,
                                    PurchasePrice = f.PurchasePrice
                                };
            return partOfResult1.Union(partOfResult2);

        }

        [Query(HasSideEffects = true)]
        public IQueryable<SparePart> GetPartsByIds(int[] ids) {
            return ObjectContext.SpareParts.Where(r => ids.Contains(r.Id));
        }

        [Query(HasSideEffects = true)]
        public IQueryable<SparePart> GetSparePartsWithPartsSupplierRelation() {
            var userInfo = Utils.GetCurrentUserInfo();
            return ObjectContext.SpareParts.Where(r => ObjectContext.PartsSupplierRelations.Any(t => t.SupplierId == userInfo.EnterpriseId && t.PartId == r.Id)).OrderBy(r => r.Id);
        }

        public IQueryable<SparePartOuterPurchase> 查询配件带批发价(int partsSalesCategoryId) {
            var partsSalesPrice = from a in ObjectContext.SpareParts
                                  join b in ObjectContext.PartsSalesPrices.Where(r => r.Status == (int)DcsBaseDataStatus.有效 &&  r.PartsSalesCategoryId == partsSalesCategoryId)
                                  on a.Id equals b.SparePartId
                                  select new SparePartOuterPurchase {
                                      Id = b.Id,
                                      PartId = a.Id,
                                      Code = a.Code,
                                      Name = a.Name,
                                      PartsSalesPrice = b.SalesPrice,
                                      CreateTime = a.CreateTime,
                                      ModifyTime = a.ModifyTime,
                                      Status = a.Status,
                                      ReferenceCode = a.ReferenceCode
                                  };
            return partsSalesPrice.OrderBy(r => r.Id);
        }

        public IQueryable<SparePart> GetSparePartsByCode(string[] codes) {
            var partsSalesPrice = ObjectContext.SpareParts.Where(r => codes.Contains(r.Code) && r.Status == (int)DcsMasterDataStatus.有效);
            return partsSalesPrice.OrderBy(r => r.Id);
        }
        public IQueryable<SparePart> GetSparePartsByCode2(string[] codes) {
            var partsSalesPrice = ObjectContext.SpareParts.Where(r => codes.Contains(r.Code));
            return partsSalesPrice.OrderBy(r => r.Id);
        }
        public List<string> GetNumberOfParts(int num, string templateName) {
            List<string> result = new List<string>();
            for(int i = 0; i < num; i++) {
                result.Add(CodeGenerator.Generate(templateName));
            }
            return result;
        }

        [Query(HasSideEffects = true)]
        public IQueryable<VirtualSparePart> GetSparePartWithPartsBranch(int[] partsIds, int partsSaleCategoryId) {
            var resultQuery = (from a in ObjectContext.SpareParts.Where(r => r.Status != (int)DcsMasterDataStatus.作废 && partsIds.Contains(r.Id))
                               join b in ObjectContext.PartsBranches.Where(v => v.PartsSalesCategoryId == partsSaleCategoryId && v.Status == (int)DcsBaseDataStatus.有效) on a.Id equals b.PartId
                               select new VirtualSparePart {
                                   Id = a.Id,
                                   Code = a.Code,
                                   Name = a.Name,
                                   PartsSalesCategoryName = b.PartsSalesCategoryName,
                                   IsSalable = b.IsSalable,
                                   IsOrderable = b.IsOrderable
                               });
            return resultQuery.Distinct().OrderBy(r => r.Id);
        }

        //[Query(HasSideEffects = true)]
        public IQueryable<VirtualSparePart> GetSparePartWithPartsBranch2(int partsSaleCategoryId) {
            var resultQuery = from a in ObjectContext.SpareParts
                              from b in ObjectContext.PartsBranches
                              where a.Id == b.PartId && b.Status == (int)DcsMasterDataStatus.有效 && a.Status == (int)DcsMasterDataStatus.有效 && b.PartsSalesCategoryId == partsSaleCategoryId
                              select new VirtualSparePart {
                                  Id = a.Id,
                                  Name = a.Name,
                                  Code = a.Code,
                                  ReferenceCode = a.ReferenceCode,
                                  OverseasPartsFigure = a.OverseasPartsFigure,
                                  CADCode = a.CADCode,
                                  Status = a.Status,
                                  PartType = a.PartType,
                                  MeasureUnit = a.MeasureUnit,
                                  Specification = a.Specification,
                                  Feature = a.Factury,
                                  PartsSalesCategoryId = b.PartsSalesCategoryId,
                                  CreateTime = a.CreateTime,
                                  ModifyTime = a.ModifyTime
                              };
            return resultQuery.Distinct().OrderBy(r => r.Id);
        }

        public IQueryable<SparePart> GetSparePartWithPartsExchangeGroup(string exGroupCode,string codes) {
            if(string.IsNullOrEmpty(exGroupCode)) {
                var result = from a in ObjectContext.SpareParts
                             select new {
                                 a
                             };
                var result1 = result.Select(q => q.a);
                if (!string.IsNullOrEmpty(codes)) { 
                    var spareparts = codes.Split(','); 
                    if (spareparts.Length == 1) { 
                        var sparepartcode = spareparts[0]; 
                        result1 = result1.Where(r => r.Code.Contains(sparepartcode)); 
                    } else { 
                        result1 = result1.Where(r => spareparts.Contains(r.Code)); 
                    } 
                }
                result1 = result1.OrderByDescending(r => r.ModifyTime ?? r.CreateTime);
                var ids1 = DcsDomainService.QueryComposer.Compose(result1, ParamQueryable).Cast<SparePart>().Select(q => q.Id).ToArray();
                var temp = from q in result.Where(p => ids1.Contains(p.a.Id))
                           join b in ObjectContext.PartsExchangeGroups.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on q.a.ExchangeIdentification equals b.ExchangeCode into tempTable2
                           from exchangeGroup in tempTable2.DefaultIfEmpty()
                           join c in ObjectContext.PartsBranches.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on q.a.Id equals c.PartId into tempTable3
                           from partsBranch in tempTable3.DefaultIfEmpty()
                           select new {
                               q.a,
                               ExGroupCode = exchangeGroup.ExGroupCode,
                               IsSalable = partsBranch.IsSalable,
                               IsOrderable = partsBranch.IsOrderable
                           };
                foreach(var tc in temp) {
                    tc.a.ExGroupCode = tc.ExGroupCode;
                    tc.a.IsSalable = tc.IsSalable;
                    tc.a.IsOrderable = tc.IsOrderable;
                }
                
                return result1;
            } else {
                var result = from a in ObjectContext.SpareParts
                             join b in ObjectContext.PartsExchangeGroups.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.ExGroupCode == exGroupCode)
                             on a.ExchangeIdentification equals b.ExchangeCode
                             join c in ObjectContext.PartsBranches.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on a.Id equals c.PartId into tempTable3
                             from partsBranch in tempTable3.DefaultIfEmpty()
                             select new {
                                 a,
                                 b.ExGroupCode,
                                 IsSalable = partsBranch.IsSalable,
                                 IsOrderable = partsBranch.IsOrderable
                             };
                var result1 = result.Select(q => q.a);
                if (!string.IsNullOrEmpty(codes)) { 
                    var spareparts = codes.Split(','); 
                    if (spareparts.Length == 1) { 
                        var sparepartcode = spareparts[0]; 
                        result1 = result1.Where(r => r.Code.Contains(sparepartcode)); 
                    } else { 
                        result1 = result1.Where(r => spareparts.Contains(r.Code)); 
                    } 
                }
                result1 = result1.OrderByDescending(r => r.ModifyTime ?? r.CreateTime);
                var ids1 = DcsDomainService.QueryComposer.Compose(result1, ParamQueryable).Cast<SparePart>().Select(q => q.Id).ToArray();
                var temp = from q in result.Where(p => ids1.Contains(p.a.Id))
                           select new {
                               q.a,
                               ExGroupCode = q.ExGroupCode,
                               IsSalable = q.IsSalable,
                               IsOrderable = q.IsOrderable
                           };
                foreach(var tc in temp) {
                    tc.a.ExGroupCode = tc.ExGroupCode;
                    tc.a.IsSalable = tc.IsSalable;
                    tc.a.IsOrderable = tc.IsOrderable;
                }
                
                return result1;
            }
        }

        public IQueryable<SparePart> GetSparePartByPartsSalesCategoryId(int? partsSalesCategoryId) {
            if(!partsSalesCategoryId.HasValue)
                return ObjectContext.SpareParts.OrderBy(s => s.Id);
            var result = from s in ObjectContext.SpareParts
                         join p in ObjectContext.PartsBranches.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.PartsSalesCategoryId == partsSalesCategoryId)
                         on s.Id equals p.PartId
                         select s;
            string sql = ((ObjectQuery)result).ToTraceString();
            return result.OrderBy(s => s.Id);
        }

        //查询可直供的配件
        public IQueryable<SparePart> GetSparePartForPartsBranchByDirectSupply() {
            var result = from s in ObjectContext.SpareParts
                         join p in ObjectContext.PartsBranches.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.IsDirectSupply)
                         on s.Id equals p.PartId
                         select s;
            return result.OrderBy(s => s.Id);
        }
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:30
        //原分布类的函数全部转移到Ach                                                               

        public void InsertSparePart(SparePart sparePart) {
            new SparePartAch(this).InsertSparePart(sparePart);
        }


        public void UpdateSparePart(SparePart sparePart) {
            new SparePartAch(this).UpdateSparePart(sparePart);
        }

        public IQueryable<SparePart> GetSparePartsWithParentCombinedParts() {
            return new SparePartAch(this).GetSparePartsWithParentCombinedParts();
        }

        public IQueryable<SparePart> GetSparePartByPartsSalesCategoryId(int? partsSalesCategoryId) {
            return new SparePartAch(this).GetSparePartByPartsSalesCategoryId(partsSalesCategoryId);
        }

        public SparePart GetSparePartsWithParentCombinedPartsById(int id) {
            return new SparePartAch(this).GetSparePartsWithParentCombinedPartsById(id);
        }

        [Query(HasSideEffects = true)]
        public IQueryable<SparePart> GetSparePartsWithPartsBranchByBranchIdAndPartIds(int branchId, int[] partIds) {
            return new SparePartAch(this).GetSparePartsWithPartsBranchByBranchIdAndPartIds(branchId, partIds);
        }


        [Query(HasSideEffects = true)]
        public IQueryable<VirtualSparePart> 查询配件合同价(int branchId, int partsSupplierId, DateTime time, List<int> sparePartIds, int partsSaleCategoryId) {
            return new SparePartAch(this).查询配件合同价(branchId, partsSupplierId, time, sparePartIds, partsSaleCategoryId);
        }

        [Query(HasSideEffects = true)]
        public IQueryable<VirtualSparePart> 查询配件合同价采购计划(int branchId, List<int> sparePartIds, int partsSaleCategoryId) {
            return new SparePartAch(this).查询配件合同价采购计划(branchId, sparePartIds, partsSaleCategoryId);
        }


        [Query(HasSideEffects = true)]
        public IQueryable<SparePart> GetSparePartsByIds(int[] partIds) {
            return new SparePartAch(this).GetSparePartsByIds(partIds);
        }

        [Query(HasSideEffects = true)]
        public IEnumerable<SparePart> 查询配件批发价(int partsSalesCategoryId, int[] partsIds) {
            return new SparePartAch(this).查询配件批发价(partsSalesCategoryId, partsIds);
        }

        public IEnumerable<SparePart> 查询配件供应商(int? partsId, string supplierCode, string supplierName, int partsSalecategoryId) {
            return new SparePartAch(this).查询配件供应商(partsId, supplierCode, supplierName, partsSalecategoryId);
        }

        public IQueryable<FaultyPart> 查询保外配件供应商(int? partsId, string supplierCode, string supplierName, int partsSalecategoryId) {
            return new SparePartAch(this).查询保外配件供应商(partsId, supplierCode, supplierName, partsSalecategoryId);
        }

        public IQueryable<FaultyPart> 查询祸首件(int? partsId, string supplierCode, string supplierName, int partsSalecategoryId, int branchId) {
            return new SparePartAch(this).查询祸首件(partsId, supplierCode, supplierName, partsSalecategoryId, branchId);
        }

        public IQueryable<FaultyPart> 查询祸首件2(int? partsId, string sparePartCode, string sparePartName, string supplierCode, string supplierName, string referenceCode, string referenceName, int partsSalecategoryId, int branchId) {
            return new SparePartAch(this).查询祸首件2(partsId, sparePartCode, sparePartName, supplierCode, supplierName, referenceCode, referenceName, partsSalecategoryId, branchId);
        }

        public IQueryable<FaultyPart> 维修索赔查询祸首件(int? partsId, string supplierCode, string supplierName, int partsSalecategoryId) {
            return new SparePartAch(this).维修索赔查询祸首件(partsId, supplierCode, supplierName, partsSalecategoryId);
        }

        public IQueryable<PartsInsteadInfo> 查询采购替互换件(int sparePartId, int partsSalesCategoryId, int supplierId) {
            return new SparePartAch(this).查询采购替互换件(sparePartId, partsSalesCategoryId, supplierId);
        }

        [Query(HasSideEffects = true)]
        public IQueryable<SparePart> GetPartsByIds(int[] ids) {
            return new SparePartAch(this).GetPartsByIds(ids);
        }

        [Query(HasSideEffects = true)]
        public IQueryable<SparePart> GetSparePartsWithPartsSupplierRelation() {
            return new SparePartAch(this).GetSparePartsWithPartsSupplierRelation();
        }

        public IQueryable<SparePartOuterPurchase> 查询配件带批发价(int partsSalesCategoryId) {
            return new SparePartAch(this).查询配件带批发价(partsSalesCategoryId);
        }

        public IQueryable<SparePart> GetSparePartsByCode(string[] codes) {
            return new SparePartAch(this).GetSparePartsByCode(codes);
        }
        public IQueryable<SparePart> GetSparePartsByCode2(string[] codes) {
            return new SparePartAch(this).GetSparePartsByCode2(codes);
        }
        public List<string> GetNumberOfParts(int num, string templateName) {
            return new SparePartAch(this).GetNumberOfParts(num, templateName);
        }

        [Query(HasSideEffects = true)]
        public IQueryable<VirtualSparePart> GetSparePartWithPartsBranch(int[] partsIds, int partsSaleCategoryId) {
            return new SparePartAch(this).GetSparePartWithPartsBranch(partsIds, partsSaleCategoryId);
        }
        //[Query(HasSideEffects = true)]
        public IQueryable<VirtualSparePart> GetSparePartWithPartsBranch2(int partsSaleCategoryId) {
            return new SparePartAch(this).GetSparePartWithPartsBranch2(partsSaleCategoryId);
        }

        [Query(HasSideEffects = true)]
        public IQueryable<SparePart> GetSparePartWithPartsExchangeGroup(string exGroupCode,string codes) {
            return new SparePartAch(this).GetSparePartWithPartsExchangeGroup(exGroupCode,codes);
        }

        public IQueryable<SparePart> GetSparePartForPartsBranchByDirectSupply() {
            return new SparePartAch(this).GetSparePartForPartsBranchByDirectSupply();
        }
        public IQueryable<VirtualSparePart> 跨区域销售查询配件销售价() {
            return new SparePartAch(this).跨区域销售查询配件销售价();
        }

    }
}
