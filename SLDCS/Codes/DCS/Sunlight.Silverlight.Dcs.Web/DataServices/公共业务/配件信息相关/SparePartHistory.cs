﻿using System;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class SparePartHistoryAch : DcsSerivceAchieveBase {
        public SparePartHistoryAch(DcsDomainService domainService)
            : base(domainService) {
        }

        internal void InsertSparePartHistoryValidate(SparePartHistory sparePartHistory) {
            var userInfo = Utils.GetCurrentUserInfo();
            sparePartHistory.CreatorId = userInfo.Id;
            sparePartHistory.CreatorName = userInfo.Name;
            sparePartHistory.CreateTime = DateTime.Now;
        }
        public void InsertSparePartHistory(SparePartHistory sparePartHistory) {
            InsertToDatabase(sparePartHistory);
            this.InsertSparePartHistoryValidate(sparePartHistory);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:30
        //原分布类的函数全部转移到Ach                                                               

        public void InsertSparePartHistory(SparePartHistory sparePartHistory) {
            new SparePartHistoryAch(this).InsertSparePartHistory(sparePartHistory);
        }
    }
}
