﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class SsUsedPartsDisposalBillAch : DcsSerivceAchieveBase {
        internal void InsertSsUsedPartsDisposalBillValidate(SsUsedPartsDisposalBill ssUsedPartsDisposalBill) {
            var userInfo = Utils.GetCurrentUserInfo();
            ssUsedPartsDisposalBill.CreatorId = userInfo.Id;
            ssUsedPartsDisposalBill.CreatorName = userInfo.Name;
            ssUsedPartsDisposalBill.CreateTime = DateTime.Now;
            if(string.IsNullOrWhiteSpace(ssUsedPartsDisposalBill.Code) || ssUsedPartsDisposalBill.Code == GlobalVar.ASSIGNED_BY_SERVER)
                ssUsedPartsDisposalBill.Code = CodeGenerator.Generate("SsUsedPartsDisposalBill", ssUsedPartsDisposalBill.DealerCode);
        }
        internal void UpdateSsUsedPartsDisposalBillValidate(SsUsedPartsDisposalBill ssUsedPartsDisposalBill) {
            if(ObjectContext.SsUsedPartsDisposalDetails.Any(r => r.SsUsedPartsDisposalBillId == ssUsedPartsDisposalBill.Id && r.UsedPartsReturnPolicy == (int)DcsPartsWarrantyTermReturnPolicy.返回本部))
                throw new ValidationException(ErrorStrings.SsUsedPartsDisposalBill_Validation1);
            var userInfo = Utils.GetCurrentUserInfo();
            ssUsedPartsDisposalBill.ModifierId = userInfo.Id;
            ssUsedPartsDisposalBill.ModifierName = userInfo.Name;
            ssUsedPartsDisposalBill.ModifyTime = DateTime.Now;
        }
        public void InsertSsUsedPartsDisposalBill(SsUsedPartsDisposalBill ssUsedPartsDisposalBill) {
            InsertToDatabase(ssUsedPartsDisposalBill);
            var ssUsedPartsDisposalDetails = ChangeSet.GetAssociatedChanges(ssUsedPartsDisposalBill, r => r.SsUsedPartsDisposalDetails, ChangeOperation.Insert);
            foreach(SsUsedPartsDisposalDetail ssUsedPartsDisposalDetail in ssUsedPartsDisposalDetails)
                InsertToDatabase(ssUsedPartsDisposalDetail);
            this.InsertSsUsedPartsDisposalBillValidate(ssUsedPartsDisposalBill);
        }
        public void UpdateSsUsedPartsDisposalBill(SsUsedPartsDisposalBill ssUsedPartsDisposalBill) {
            ssUsedPartsDisposalBill.SsUsedPartsDisposalDetails.Clear();
            UpdateToDatabase(ssUsedPartsDisposalBill);
            var ssUsedPartsDisposalDetails = ChangeSet.GetAssociatedChanges(ssUsedPartsDisposalBill, r => r.SsUsedPartsDisposalDetails);
            foreach(SsUsedPartsDisposalDetail ssUsedPartsDisposalDetail in ssUsedPartsDisposalDetails) {
                switch(ChangeSet.GetChangeOperation(ssUsedPartsDisposalDetail)) {
                    case ChangeOperation.Insert:
                        InsertToDatabase(ssUsedPartsDisposalDetail);
                        break;
                    case ChangeOperation.Update:
                        UpdateToDatabase(ssUsedPartsDisposalDetail);
                        break;
                    case ChangeOperation.Delete:
                        DeleteFromDatabase(ssUsedPartsDisposalDetail);
                        break;
                }
            }
            this.UpdateSsUsedPartsDisposalBillValidate(ssUsedPartsDisposalBill);
        }
        public SsUsedPartsDisposalBill GetSsUsedPartsDisposalBillsWithDetails(int id) {
            var result = ObjectContext.SsUsedPartsDisposalBills.SingleOrDefault(e => e.Id == id && e.Status != (int)DcsSsUsedPartsDisposalBillStatus.作废);
            if(result != null) {
                var partsSalesCategory = ObjectContext.PartsSalesCategories.SingleOrDefault(e => e.Id == result.PartsSalesCategoryId);
                var details = ObjectContext.SsUsedPartsDisposalDetails.Where(e => e.SsUsedPartsDisposalBillId == id).OrderBy(d => d.SerialNumber).ToArray();
            }
            return result;
        }
        public IQueryable<SsUsedPartsDisposalBill> GetSsUsedPartsDisposalBillsWithPartsSalesCategory() {
            return ObjectContext.SsUsedPartsDisposalBills.Include("PartsSalesCategory").OrderBy(entity => entity.Id);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:30
        //原分布类的函数全部转移到Ach                                                               

        public void InsertSsUsedPartsDisposalBill(SsUsedPartsDisposalBill ssUsedPartsDisposalBill) {
            new SsUsedPartsDisposalBillAch(this).InsertSsUsedPartsDisposalBill(ssUsedPartsDisposalBill);
        }

        public void UpdateSsUsedPartsDisposalBill(SsUsedPartsDisposalBill ssUsedPartsDisposalBill) {
            new SsUsedPartsDisposalBillAch(this).UpdateSsUsedPartsDisposalBill(ssUsedPartsDisposalBill);
        }

        public SsUsedPartsDisposalBill GetSsUsedPartsDisposalBillsWithDetails(int id) {
            return new SsUsedPartsDisposalBillAch(this).GetSsUsedPartsDisposalBillsWithDetails(id);
        }

        public IQueryable<SsUsedPartsDisposalBill> GetSsUsedPartsDisposalBillsWithPartsSalesCategory() {
            return new SsUsedPartsDisposalBillAch(this).GetSsUsedPartsDisposalBillsWithPartsSalesCategory();
        }
    }
}
