﻿using System;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class SsUsedPartsStorageAch : DcsSerivceAchieveBase {
        internal void InsertSsUsedPartsStorageValidate(SsUsedPartsStorage ssUsedPartsStorage) {
            var userInfo = Utils.GetCurrentUserInfo();
            ssUsedPartsStorage.CreatorId = userInfo.Id;
            ssUsedPartsStorage.CreatorName = userInfo.Name;
            ssUsedPartsStorage.CreateTime = DateTime.Now;
            var usedPartsReturnPolicyHistory = new UsedPartsReturnPolicyHistory {
                DealerId = ssUsedPartsStorage.DealerId,
                ClaimBillId = ssUsedPartsStorage.ClaimBillId,
                ClaimBillType = ssUsedPartsStorage.ClaimBillType,
                BranchId = ssUsedPartsStorage.BranchId,
                BranchName = ssUsedPartsStorage.BranchName,
                ClaimBillCode = ssUsedPartsStorage.ClaimBillCode,
                UsedPartsReturnPolicy = ssUsedPartsStorage.UsedPartsReturnPolicy,
                UsedPartsId = ssUsedPartsStorage.UsedPartsId,
                UsedPartsCode = ssUsedPartsStorage.UsedPartsCode,
                UsedPartsName = ssUsedPartsStorage.UsedPartsName,
                UsedPartsBatchNumber = ssUsedPartsStorage.UsedPartsBatchNumber,
                UsedPartsSerialNumber = ssUsedPartsStorage.UsedPartsSerialNumber,
                UsedPartsSupplierId = ssUsedPartsStorage.UsedPartsSupplierId,
                UsedPartsSupplierCode = ssUsedPartsStorage.UsedPartsSupplierCode,
                UsedPartsSupplierName = ssUsedPartsStorage.UsedPartsSupplierCode,
                Quantity = ssUsedPartsStorage.Quantity,
                UnitPrice = ssUsedPartsStorage.UnitPrice,
                PartsManagementCost = ssUsedPartsStorage.PartsManagementCost,
                UsedPartsBarCode = ssUsedPartsStorage.UsedPartsBarCode,
                FaultyPartsId = ssUsedPartsStorage.FaultyPartsId,
                FaultyPartsCode = ssUsedPartsStorage.FaultyPartsCode,
                FaultyPartsName = ssUsedPartsStorage.FaultyPartsName,
                FaultyPartsSupplierId = ssUsedPartsStorage.FaultyPartsSupplierId,
                FaultyPartsSupplierCode = ssUsedPartsStorage.FaultyPartsSupplierCode,
                FaultyPartsSupplierName = ssUsedPartsStorage.FaultyPartsSupplierName,
                IfFaultyParts = ssUsedPartsStorage.IfFaultyParts,
                ResponsibleUnitId = ssUsedPartsStorage.ResponsibleUnitId,
                ResponsibleUnitCode = ssUsedPartsStorage.ResponsibleUnitCode,
                ResponsibleUnitName = ssUsedPartsStorage.ResponsibleUnitName,
                Shipped = ssUsedPartsStorage.Shipped,
                NewPartsId = ssUsedPartsStorage.NewPartsId,
                NewPartsCode = ssUsedPartsStorage.NewPartsCode,
                NewPartsName = ssUsedPartsStorage.NewPartsName,
                NewPartsSupplierId = ssUsedPartsStorage.NewPartsSupplierId,
                NewPartsSupplierCode = ssUsedPartsStorage.NewPartsSupplierCode,
                NewPartsSupplierName = ssUsedPartsStorage.NewPartsSupplierName,
                NewPartsBatchNumber = ssUsedPartsStorage.NewPartsBatchNumber,
                NewPartsSerialNumber = ssUsedPartsStorage.NewPartsSerialNumber,
                NewPartsSecurityNumber = ssUsedPartsStorage.NewPartsSecurityNumber,
                PartsSalesCategoryId = ssUsedPartsStorage.PartsSalesCategoryId
            };
            InsertToDatabase(usedPartsReturnPolicyHistory);
            new UsedPartsReturnPolicyHistoryAch(this.DomainService).InsertUsedPartsReturnPolicyHistoryValidate(usedPartsReturnPolicyHistory);
        }

        internal void UpdateSsUsedPartsStorageValidate(SsUsedPartsStorage ssUsedPartsStorage) {
            var userInfo = Utils.GetCurrentUserInfo();
            ssUsedPartsStorage.ModifierId = userInfo.Id;
            ssUsedPartsStorage.ModifierName = userInfo.Name;
            ssUsedPartsStorage.ModifyTime = DateTime.Now;
            var usedPartsReturnPolicyHistory = new UsedPartsReturnPolicyHistory {
                DealerId = ssUsedPartsStorage.DealerId,
                ClaimBillId = ssUsedPartsStorage.ClaimBillId,
                ClaimBillType = ssUsedPartsStorage.ClaimBillType,
                BranchId = ssUsedPartsStorage.BranchId,
                BranchName = ssUsedPartsStorage.BranchName,
                ClaimBillCode = ssUsedPartsStorage.ClaimBillCode,
                UsedPartsReturnPolicy = ssUsedPartsStorage.UsedPartsReturnPolicy,
                UsedPartsId = ssUsedPartsStorage.UsedPartsId,
                UsedPartsCode = ssUsedPartsStorage.UsedPartsCode,
                UsedPartsName = ssUsedPartsStorage.UsedPartsName,
                UsedPartsBatchNumber = ssUsedPartsStorage.UsedPartsBatchNumber,
                UsedPartsSerialNumber = ssUsedPartsStorage.UsedPartsSerialNumber,
                UsedPartsSupplierId = ssUsedPartsStorage.UsedPartsSupplierId,
                UsedPartsSupplierCode = ssUsedPartsStorage.UsedPartsSupplierCode,
                UsedPartsSupplierName = ssUsedPartsStorage.UsedPartsSupplierCode,
                Quantity = ssUsedPartsStorage.Quantity,
                UnitPrice = ssUsedPartsStorage.UnitPrice,
                PartsManagementCost = ssUsedPartsStorage.PartsManagementCost,
                UsedPartsBarCode = ssUsedPartsStorage.UsedPartsBarCode,
                FaultyPartsId = ssUsedPartsStorage.FaultyPartsId,
                FaultyPartsCode = ssUsedPartsStorage.FaultyPartsCode,
                FaultyPartsName = ssUsedPartsStorage.FaultyPartsName,
                FaultyPartsSupplierId = ssUsedPartsStorage.FaultyPartsSupplierId,
                FaultyPartsSupplierCode = ssUsedPartsStorage.FaultyPartsSupplierCode,
                FaultyPartsSupplierName = ssUsedPartsStorage.FaultyPartsSupplierName,
                IfFaultyParts = ssUsedPartsStorage.IfFaultyParts,
                ResponsibleUnitId = ssUsedPartsStorage.ResponsibleUnitId,
                ResponsibleUnitCode = ssUsedPartsStorage.ResponsibleUnitCode,
                ResponsibleUnitName = ssUsedPartsStorage.ResponsibleUnitName,
                Shipped = ssUsedPartsStorage.Shipped,
                NewPartsId = ssUsedPartsStorage.NewPartsId,
                NewPartsCode = ssUsedPartsStorage.NewPartsCode,
                NewPartsName = ssUsedPartsStorage.NewPartsName,
                NewPartsSupplierId = ssUsedPartsStorage.NewPartsSupplierId,
                NewPartsSupplierCode = ssUsedPartsStorage.NewPartsSupplierCode,
                NewPartsSupplierName = ssUsedPartsStorage.NewPartsSupplierName,
                NewPartsBatchNumber = ssUsedPartsStorage.NewPartsBatchNumber,
                NewPartsSerialNumber = ssUsedPartsStorage.NewPartsSerialNumber,
                NewPartsSecurityNumber = ssUsedPartsStorage.NewPartsSecurityNumber,
                PartsSalesCategoryId = ssUsedPartsStorage.PartsSalesCategoryId
            };
            InsertToDatabase(usedPartsReturnPolicyHistory);
            new UsedPartsReturnPolicyHistoryAch(this.DomainService).InsertUsedPartsReturnPolicyHistoryValidate(usedPartsReturnPolicyHistory);
        }

        public void UpdateSsUsedPartsStorage(SsUsedPartsStorage ssUsedPartsStorage) {
            UpdateToDatabase(ssUsedPartsStorage);
            this.UpdateSsUsedPartsStorageValidate(ssUsedPartsStorage);
        }

        public void InsertSsUsedPartsStorage(SsUsedPartsStorage ssUsedPartsStorage) {
            InsertToDatabase(ssUsedPartsStorage);
            this.UpdateSsUsedPartsStorageValidate(ssUsedPartsStorage);
        }

        public void DeleteSsUsedPartsStorage(SsUsedPartsStorage ssUsedPartsStorage) {
            DeleteFromDatabase(ssUsedPartsStorage);
        }

        public IQueryable<SsUsedPartsStorage> GetSsUsedPartsStoragesWithDealer() {
            return ObjectContext.SsUsedPartsStorages.Include("Dealer").Include("PartsSalesCategory").OrderBy(e => e.Id);
        }

        [Query(HasSideEffects = true)]
        public IQueryable<SsUsedPartsStorage> GetSsUsedPartsStoragesWithDealerByIds(int[] ids) {
            return ObjectContext.SsUsedPartsStorages.Where(e => ids.Contains(e.Id)).Include("Dealer").Include("PartsSalesCategory").OrderBy(e => e.Id);
        }

        public IQueryable<virtualSsUsedPartsStorageWithClaimBillStatus> 查询旧件库存对应索赔单状态() {
            //var ssUsedPartsStorages1 = from a in ObjectContext.SsUsedPartsStorages.Where(r => r.ClaimBillType == (int)DcsClaimBillType.维修索赔 || r.ClaimBillType == (int)DcsClaimBillType.保养索赔 || r.ClaimBillType == (int)DcsClaimBillType.服务活动索赔)
            //                           join b in ObjectContext.RepairClaimBills on a.ClaimBillId equals b.Id into t1
            //                           from c in t1.DefaultIfEmpty()
            //                           join d in ObjectContext.PartsSalesCategories on a.PartsSalesCategoryId equals d.Id into t2
            //                           from e in t2.DefaultIfEmpty()
            //                           select new virtualSsUsedPartsStorageWithClaimBillStatus {
            //                               Id = a.Id,
            //                               ClaimBillId = a.ClaimBillId,
            //                               BranchId = a.BranchId,
            //                               BranchName = a.BranchName,
            //                               DealerId = c.DealerId,
            //                               DealerCode = c.DealerCode,
            //                               DealerName = c.DealerName,
            //                               ClaimBillCode = c.ClaimBillCode,
            //                               ClaimBillType = a.ClaimBillType,
            //                               RepairClaimStatus = c.Status,
            //                               PartsClaimStatus = default(int),
            //                               PartsSalesCategoryId = a.PartsSalesCategoryId,
            //                               PartsSalesCategoryName = e.Name,
            //                               UsedPartsBarCode = a.UsedPartsBarCode,
            //                               UsedPartsCode = a.UsedPartsCode,
            //                               UsedPartsName = a.UsedPartsName,
            //                               UsedPartsId = a.UsedPartsId,
            //                               UsedPartsReturnPolicy = a.UsedPartsReturnPolicy,
            //                               Shipped = a.Shipped,
            //                               ShippedString = a.Shipped ? EntityStrings.Export_Title_Yes : EntityStrings.Export_Title_No,
            //                               UnitPrice = a.UnitPrice,
            //                               IfFaultyParts = a.IfFaultyParts,
            //                               IfFaultyPartsString = a.IfFaultyParts ? EntityStrings.Export_Title_Yes : EntityStrings.Export_Title_No,
            //                               UsedPartsSupplierId = a.UsedPartsSupplierId,
            //                               UsedPartsSupplierCode = a.UsedPartsSupplierCode,
            //                               UsedPartsSupplierName = a.UsedPartsSupplierName,
            //                               FaultyPartsId = a.FaultyPartsId,
            //                               FaultyPartsCode = a.FaultyPartsCode,
            //                               FaultyPartsName = a.FaultyPartsName,
            //                               FaultyPartsSupplierId = a.FaultyPartsSupplierId,
            //                               FaultyPartsSupplierCode = a.FaultyPartsSupplierCode,
            //                               FaultyPartsSupplierName = a.FaultyPartsSupplierName,
            //                               ClaimBillCreateTime = a.ClaimBillCreateTime,
            //                               CreatorName = a.CreatorName,
            //                               CreateTime = a.CreateTime,
            //                               ModifierName = a.ModifierName,
            //                               ModifyTime = a.ModifyTime
            //                           };
            //var ssUsedPartsStorages2 = from a in ObjectContext.SsUsedPartsStorages.Where(r => r.ClaimBillType == (int)DcsClaimBillType.配件索赔)
            //                           join b in ObjectContext.PartsClaimOrders on a.ClaimBillId equals b.Id into t1
            //                           from c in t1.DefaultIfEmpty()
            //                           join d in ObjectContext.PartsSalesCategories on a.PartsSalesCategoryId equals d.Id into t2
            //                           from e in t2.DefaultIfEmpty()
            //                           select new virtualSsUsedPartsStorageWithClaimBillStatus {
            //                               Id = a.Id,
            //                               ClaimBillId = a.ClaimBillId,
            //                               BranchId = a.BranchId,
            //                               BranchName = a.BranchName,
            //                               DealerId = c.DealerId,
            //                               DealerCode = c.DealerCode,
            //                               DealerName = c.DealerName,
            //                               ClaimBillCode = c.Code,
            //                               ClaimBillType = a.ClaimBillType,
            //                               RepairClaimStatus = default(int),
            //                               PartsClaimStatus = c.Status,
            //                               PartsSalesCategoryId = a.PartsSalesCategoryId,
            //                               PartsSalesCategoryName = e.Name,
            //                               UsedPartsBarCode = a.UsedPartsBarCode,
            //                               UsedPartsCode = a.UsedPartsCode,
            //                               UsedPartsName = a.UsedPartsName,
            //                               UsedPartsId = a.UsedPartsId,
            //                               UsedPartsReturnPolicy = a.UsedPartsReturnPolicy,
            //                               Shipped = a.Shipped,
            //                               ShippedString = a.Shipped ? EntityStrings.Export_Title_Yes : EntityStrings.Export_Title_No,
            //                               UnitPrice = a.UnitPrice,
            //                               IfFaultyParts = a.IfFaultyParts,
            //                               IfFaultyPartsString = a.IfFaultyParts ? EntityStrings.Export_Title_Yes : EntityStrings.Export_Title_No,
            //                               UsedPartsSupplierId = a.UsedPartsSupplierId,
            //                               UsedPartsSupplierCode = a.UsedPartsSupplierCode,
            //                               UsedPartsSupplierName = a.UsedPartsSupplierName,
            //                               FaultyPartsId = a.FaultyPartsId,
            //                               FaultyPartsCode = a.FaultyPartsCode,
            //                               FaultyPartsName = a.FaultyPartsName,
            //                               FaultyPartsSupplierId = a.FaultyPartsSupplierId,
            //                               FaultyPartsSupplierCode = a.FaultyPartsSupplierCode,
            //                               FaultyPartsSupplierName = a.FaultyPartsSupplierName,
            //                               ClaimBillCreateTime = a.ClaimBillCreateTime,
            //                               CreatorName = a.CreatorName,
            //                               CreateTime = a.CreateTime,
            //                               ModifierName = a.ModifierName,
            //                               ModifyTime = a.ModifyTime
            //                           };
            //var claimBillIdAndStatus = ssUsedPartsStorages1.Union(ssUsedPartsStorages2);
            //return claimBillIdAndStatus.OrderBy(r => r.Id);
            //return ssUsedPartsStorages1.OrderBy(r => r.Id);
            return null;
        }
    
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:30
        //原分布类的函数全部转移到Ach                                                               

        public void UpdateSsUsedPartsStorage(SsUsedPartsStorage ssUsedPartsStorage) {
            new SsUsedPartsStorageAch(this).UpdateSsUsedPartsStorage(ssUsedPartsStorage);
        }

        public void InsertSsUsedPartsStorage(SsUsedPartsStorage ssUsedPartsStorage) {
            new SsUsedPartsStorageAch(this).InsertSsUsedPartsStorage(ssUsedPartsStorage);
        }

        public void DeleteSsUsedPartsStorage(SsUsedPartsStorage ssUsedPartsStorage) {
            new SsUsedPartsStorageAch(this).DeleteSsUsedPartsStorage(ssUsedPartsStorage);
        }

        public IQueryable<SsUsedPartsStorage> GetSsUsedPartsStoragesWithDealer() {
            return new SsUsedPartsStorageAch(this).GetSsUsedPartsStoragesWithDealer();
        }
        

        [Query(HasSideEffects = true)]
        public IQueryable<SsUsedPartsStorage> GetSsUsedPartsStoragesWithDealerByIds(int[] ids) {
            return new SsUsedPartsStorageAch(this).GetSsUsedPartsStoragesWithDealerByIds(ids);
        }                

        public IQueryable<virtualSsUsedPartsStorageWithClaimBillStatus> 查询旧件库存对应索赔单状态() {
            return new SsUsedPartsStorageAch(this).查询旧件库存对应索赔单状态();
        }
    }
}
