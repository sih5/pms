﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Dcs.Web.Resources;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class UsedPartsDistanceInforAch : DcsSerivceAchieveBase {
        internal void InsertUsedPartsDistanceInforValidate(UsedPartsDistanceInfor usedPartsDistanceInfor) {
            var dbserviceProductLine = ObjectContext.UsedPartsDistanceInfors.Where(r => ((r.UsedPartsDistanceType == (int)DcsUsedPartsDistanceType.服务站_旧件库 && r.UsedPartsWarehouseId == usedPartsDistanceInfor.UsedPartsWarehouseId && r.OriginCompanyId == usedPartsDistanceInfor.OriginCompanyId)
                                                                                            || (r.UsedPartsDistanceType == (int)DcsUsedPartsDistanceType.旧件库_旧件总库 && r.UsedPartsWarehouseId == usedPartsDistanceInfor.UsedPartsWarehouseId && r.OriginUsedPartsWarehouseId == usedPartsDistanceInfor.OriginUsedPartsWarehouseId))
                                                                                        && r.Status != (int)DcsBaseDataStatus.作废).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbserviceProductLine != null)
                throw new ValidationException(ErrorStrings.UsedPartsDistanceInfor_Validation1);
            var userInfo = Utils.GetCurrentUserInfo();
            usedPartsDistanceInfor.CreatorId = userInfo.Id;
            usedPartsDistanceInfor.CreatorName = userInfo.Name;
            usedPartsDistanceInfor.CreateTime = DateTime.Now;
        }
        internal void UpdateServiceProductLineValidate(UsedPartsDistanceInfor usedPartsDistanceInfor) {
            var dbserviceProductLine = ObjectContext.UsedPartsDistanceInfors.Where(r => r.Id != usedPartsDistanceInfor.Id && ((r.UsedPartsDistanceType == (int)DcsUsedPartsDistanceType.服务站_旧件库 && r.UsedPartsWarehouseId == usedPartsDistanceInfor.UsedPartsWarehouseId && r.OriginCompanyId == usedPartsDistanceInfor.OriginCompanyId)
                                                                                            || (r.UsedPartsDistanceType == (int)DcsUsedPartsDistanceType.旧件库_旧件总库 && r.UsedPartsWarehouseId == usedPartsDistanceInfor.UsedPartsWarehouseId && r.OriginUsedPartsWarehouseId == usedPartsDistanceInfor.OriginUsedPartsWarehouseId))
                                                                                        && r.Status == (int)DcsBaseDataStatus.有效).SetMergeOption(MergeOption.NoTracking).FirstOrDefault();
            if(dbserviceProductLine != null)
                throw new ValidationException(ErrorStrings.UsedPartsDistanceInfor_Validation1);
            var userInfo = Utils.GetCurrentUserInfo();
            usedPartsDistanceInfor.ModifierId = userInfo.Id;
            usedPartsDistanceInfor.ModifierName = userInfo.Name;
            usedPartsDistanceInfor.ModifyTime = DateTime.Now;
        }
        public IQueryable<UsedPartsDistanceInfor> GetUsedPartsDistanceInforWithDetails() {
            return ObjectContext.UsedPartsDistanceInfors.Include("Company").Include("UsedPartsWarehouse").Include("Company1").Include("UsedPartsWarehouse1").OrderBy(entity => entity.Id);
        }
        public void InsertUsedPartsDistanceInfor(UsedPartsDistanceInfor usedPartsDistanceInfor) {
            InsertToDatabase(usedPartsDistanceInfor);
            this.InsertUsedPartsDistanceInforValidate(usedPartsDistanceInfor);
        }
        public void UpdateUsedPartsDistanceInfor(UsedPartsDistanceInfor usedPartsDistanceInfor) {
            UpdateToDatabase(usedPartsDistanceInfor);
            UpdateServiceProductLineValidate(usedPartsDistanceInfor);
        }

    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:30
        //原分布类的函数全部转移到Ach                                                               

        public IQueryable<UsedPartsDistanceInfor> GetUsedPartsDistanceInforWithDetails() {
            return new UsedPartsDistanceInforAch(this).GetUsedPartsDistanceInforWithDetails();
        }

        public void InsertUsedPartsDistanceInfor(UsedPartsDistanceInfor usedPartsDistanceInfor) {
            new UsedPartsDistanceInforAch(this).InsertUsedPartsDistanceInfor(usedPartsDistanceInfor);
        }

        public void UpdateUsedPartsDistanceInfor(UsedPartsDistanceInfor usedPartsDistanceInfor) {
            new UsedPartsDistanceInforAch(this).UpdateUsedPartsDistanceInfor(usedPartsDistanceInfor);
        }
    }
}
