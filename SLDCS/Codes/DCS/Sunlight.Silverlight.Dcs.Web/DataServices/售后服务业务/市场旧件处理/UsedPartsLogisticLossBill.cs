﻿using System;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class UsedPartsLogisticLossBillAch : DcsSerivceAchieveBase {
        internal void InsertUsedPartsLogisticLossBillValidate(UsedPartsLogisticLossBill usedPartsLogisticLossBill) {
            var userInfo = Utils.GetCurrentUserInfo();
            usedPartsLogisticLossBill.CreatorId = userInfo.Id;
            usedPartsLogisticLossBill.CreatorName = userInfo.Name;
            usedPartsLogisticLossBill.CreateTime = DateTime.Now;
        }

        internal void UpdateUsedPartsLogisticLossBillValidate(UsedPartsLogisticLossBill usedPartsLogisticLossBill) {
            var dbusedPartsLogisticLossDetails = usedPartsLogisticLossBill.UsedPartsLogisticLossDetails.Where(v => v.ProcessStatus != (int)DcsUsedPartsLogisticLossDetailProcessStatus.未返回).ToArray();
            usedPartsLogisticLossBill.TotalAmount = dbusedPartsLogisticLossDetails.Sum(r => r.Quantity * r.Price + r.PartsManagementCost);
            usedPartsLogisticLossBill.TotalQuantity = dbusedPartsLogisticLossDetails.Sum(r => r.Quantity);

            var userInfo = Utils.GetCurrentUserInfo();
            usedPartsLogisticLossBill.ModifierId = userInfo.Id;
            usedPartsLogisticLossBill.ModifierName = userInfo.Name;
            usedPartsLogisticLossBill.ModifyTime = DateTime.Now;
        }

        public void InsertUsedPartsLogisticLossBill(UsedPartsLogisticLossBill usedPartsLogisticLossBill) {
            InsertToDatabase(usedPartsLogisticLossBill);
            var usedPartsLogisticLossDetails = ChangeSet.GetAssociatedChanges(usedPartsLogisticLossBill, r => r.UsedPartsLogisticLossDetails, ChangeOperation.Insert);
            foreach(UsedPartsLogisticLossDetail usedPartsLogisticLossDetail in usedPartsLogisticLossDetails)
                InsertToDatabase(usedPartsLogisticLossDetail);
            this.InsertUsedPartsLogisticLossBillValidate(usedPartsLogisticLossBill);
        }

        public void UpdateUsedPartsLogisticLossBill(UsedPartsLogisticLossBill usedPartsLogisticLossBill) {
            usedPartsLogisticLossBill.UsedPartsLogisticLossDetails.Clear();
            UpdateToDatabase(usedPartsLogisticLossBill);
            var usedPartsLogisticLossDetails = ChangeSet.GetAssociatedChanges(usedPartsLogisticLossBill, r => r.UsedPartsLogisticLossDetails);
            foreach(UsedPartsLogisticLossDetail usedPartsLogisticLossDetail in usedPartsLogisticLossDetails) {
                switch(ChangeSet.GetChangeOperation(usedPartsLogisticLossDetail)) {
                    case ChangeOperation.Insert:
                        InsertToDatabase(usedPartsLogisticLossDetail);
                        break;
                    case ChangeOperation.Update:
                        UpdateToDatabase(usedPartsLogisticLossDetail);
                        break;
                    case ChangeOperation.Delete:
                        DeleteFromDatabase(usedPartsLogisticLossDetail);
                        break;
                }
            }
            this.UpdateUsedPartsLogisticLossBillValidate(usedPartsLogisticLossBill);
        }

        public IQueryable<UsedPartsLogisticLossBill> GetUsedPartsLogisticLossBillsWithUsedPartsShippingOrder() {
            return ObjectContext.UsedPartsLogisticLossBills.Include("UsedPartsShippingOrder").OrderBy(entity => entity.Id);
        }

        public UsedPartsLogisticLossBill GetUsedPartsLogisticLossBillWithDetails(int id) {
            var dbusedPartsLogisticLossBill = ObjectContext.UsedPartsLogisticLossBills.SingleOrDefault(v => v.Id == id);
            if(dbusedPartsLogisticLossBill != null) {
                var usedPartsLogisticLossDetails = ObjectContext.UsedPartsLogisticLossDetails.Where(r => r.UsedPartsLogisticLossBillId == id).ToArray();
                var usedPartsShippingOrder = ObjectContext.UsedPartsShippingOrders.SingleOrDefault(r => r.Id == dbusedPartsLogisticLossBill.UsedPartsShippingOrderId && r.Status != (int)DcsUsedPartsShippingOrderStatus.作废);
            }
            return dbusedPartsLogisticLossBill;
        }

        public IQueryable<UsedPartsLogisticLossBill> 旧件仓库人员查询物流损失单() {
            var userInfo = Silverlight.Web.Utils.GetCurrentUserInfo();
            var usedPartsWarehouseIds = ObjectContext.UsedPartsWarehouseStaffs.Where(r => r.PersonnelId == userInfo.Id).Select(r => r.UsedPartsWarehouseId);
            var dbusedPartsShippingOrderIds = ObjectContext.UsedPartsShippingOrders.Where(v => usedPartsWarehouseIds.Contains(v.DestinationWarehouseId)).Select(r => r.Id);
            var result = ObjectContext.UsedPartsLogisticLossBills.Include("UsedPartsShippingOrder").Where(r => dbusedPartsShippingOrderIds.Contains(r.UsedPartsShippingOrderId));
            return result;
        }
    
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:30
        //原分布类的函数全部转移到Ach                                                               

        public void InsertUsedPartsLogisticLossBill(UsedPartsLogisticLossBill usedPartsLogisticLossBill) {
            new UsedPartsLogisticLossBillAch(this).InsertUsedPartsLogisticLossBill(usedPartsLogisticLossBill);
        }

        public void UpdateUsedPartsLogisticLossBill(UsedPartsLogisticLossBill usedPartsLogisticLossBill) {
            new UsedPartsLogisticLossBillAch(this).UpdateUsedPartsLogisticLossBill(usedPartsLogisticLossBill);
        }

        public IQueryable<UsedPartsLogisticLossBill> GetUsedPartsLogisticLossBillsWithUsedPartsShippingOrder() {
            return new UsedPartsLogisticLossBillAch(this).GetUsedPartsLogisticLossBillsWithUsedPartsShippingOrder();
        }

        public UsedPartsLogisticLossBill GetUsedPartsLogisticLossBillWithDetails(int id) {
            return new UsedPartsLogisticLossBillAch(this).GetUsedPartsLogisticLossBillWithDetails(id);
        }

        public IQueryable<UsedPartsLogisticLossBill> 旧件仓库人员查询物流损失单() {
            return new UsedPartsLogisticLossBillAch(this).旧件仓库人员查询物流损失单();
        }
    }
}