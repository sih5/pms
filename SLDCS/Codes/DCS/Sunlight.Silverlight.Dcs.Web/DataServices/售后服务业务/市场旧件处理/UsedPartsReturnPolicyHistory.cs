﻿using System;
using System.Linq;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class UsedPartsReturnPolicyHistoryAch : DcsSerivceAchieveBase {
        public UsedPartsReturnPolicyHistoryAch(DcsDomainService domainService)
            : base(domainService) {
        }

        internal void InsertUsedPartsReturnPolicyHistoryValidate(UsedPartsReturnPolicyHistory usedPartsReturnPolicyHistory) {
            var userInfo = Utils.GetCurrentUserInfo();
            usedPartsReturnPolicyHistory.CreatorId = userInfo.Id;
            usedPartsReturnPolicyHistory.CreatorName = userInfo.Name;
            usedPartsReturnPolicyHistory.CreateTime = DateTime.Now;
        }
        public IQueryable<UsedPartsReturnPolicyHistory> GetUsedPartsReturnPolicyHistoriesWithDealer() {
            return ObjectContext.UsedPartsReturnPolicyHistories.Include("Dealer").Include("PartsSalesCategory").OrderBy(e => e.Id);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:30
        //原分布类的函数全部转移到Ach                                                               

        public IQueryable<UsedPartsReturnPolicyHistory> GetUsedPartsReturnPolicyHistoriesWithDealer() {
            return new UsedPartsReturnPolicyHistoryAch(this).GetUsedPartsReturnPolicyHistoriesWithDealer();
        }
    }
}
