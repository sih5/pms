﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DcsDomainService {
        private void InsertUsedPartsShippingOrderValidate(UsedPartsShippingOrder usedPartsShippingOrder) {
            var userInfo = Utils.GetCurrentUserInfo();
            usedPartsShippingOrder.CreatorId = userInfo.Id;
            usedPartsShippingOrder.CreatorName = userInfo.Name;
            usedPartsShippingOrder.CreateTime = DateTime.Now;
            if(string.IsNullOrWhiteSpace(usedPartsShippingOrder.Code) || usedPartsShippingOrder.Code == GlobalVar.ASSIGNED_BY_SERVER)
                usedPartsShippingOrder.Code = CodeGenerator.Generate("UsedPartsShippingOrder", usedPartsShippingOrder.DealerCode);
        }

        internal void UpdateUsedPartsShippingOrderValidate(UsedPartsShippingOrder usedPartsShippingOrder) {
            var userInfo = Utils.GetCurrentUserInfo();
            usedPartsShippingOrder.ModifierId = userInfo.Id;
            usedPartsShippingOrder.ModifierName = userInfo.Name;
            usedPartsShippingOrder.ModifyTime = DateTime.Now;
        }

        public void InsertUsedPartsShippingOrder(UsedPartsShippingOrder usedPartsShippingOrder) {
            InsertToDatabase(usedPartsShippingOrder);
            var usedPartsShippingDetails = ChangeSet.GetAssociatedChanges(usedPartsShippingOrder, r => r.UsedPartsShippingDetails, ChangeOperation.Insert);
            foreach(UsedPartsShippingDetail usedPartsShippingDetail in usedPartsShippingDetails)
                InsertToDatabase(usedPartsShippingDetail);
            InsertUsedPartsShippingOrderValidate(usedPartsShippingOrder);
        }

        public void UpdateUsedPartsShippingOrder(UsedPartsShippingOrder usedPartsShippingOrder) {
            usedPartsShippingOrder.UsedPartsShippingDetails.Clear();
            UpdateToDatabase(usedPartsShippingOrder);
            var usedPartsShippingDetails = ChangeSet.GetAssociatedChanges(usedPartsShippingOrder, r => r.UsedPartsShippingDetails);
            foreach(UsedPartsShippingDetail usedPartsShippingDetail in usedPartsShippingDetails) {
                switch(ChangeSet.GetChangeOperation(usedPartsShippingDetail)) {
                    case ChangeOperation.Insert:
                        InsertToDatabase(usedPartsShippingDetail);
                        break;
                    case ChangeOperation.Update:
                        UpdateToDatabase(usedPartsShippingDetail);
                        break;
                    case ChangeOperation.Delete:
                        DeleteFromDatabase(usedPartsShippingDetail);
                        break;
                }
            }
            UpdateUsedPartsShippingOrderValidate(usedPartsShippingOrder);
        }

        public IQueryable<UsedPartsShippingOrder> GetUsedPartsShippingOrdersWithUsedPartsWarehouse() {
            return ObjectContext.UsedPartsShippingOrders.Include("UsedPartsShippingDetails").Include("UsedPartsWarehouse").Include("PartsSalesCategory").OrderBy(entity => entity.Id);
        }

        public UsedPartsShippingOrder GetUsedPartsShippingOrdersWithDetails(int id) {
            var usedPartsShippingOrder = ObjectContext.UsedPartsShippingOrders.SingleOrDefault(e => e.Id == id);
            if(usedPartsShippingOrder != null) {
                var details = ObjectContext.UsedPartsShippingDetails.Where(e => e.UsedPartsShippingOrderId == usedPartsShippingOrder.Id).ToArray();
            }
            return usedPartsShippingOrder;
        }

        public UsedPartsShippingOrder GetUsedPartsShippingOrderWithDetails(int id) {
            var dbusedPartsShippingOrder = ObjectContext.UsedPartsShippingOrders.SingleOrDefault(v => v.Id == id && v.Status != (int)DcsUsedPartsShippingOrderStatus.作废);
            if(dbusedPartsShippingOrder != null) {
                var dealerServiceInfo = this.ObjectContext.DealerServiceInfoes.SingleOrDefault(r => r.Status == (int)DcsMasterDataStatus.有效 && r.PartsSalesCategoryId == dbusedPartsShippingOrder.PartsSalesCategoryId && r.DealerId == dbusedPartsShippingOrder.DealerId);
                if(dealerServiceInfo != null)
                    dbusedPartsShippingOrder.BusinessCode = dealerServiceInfo.BusinessCode;
                var partsSalesCategory = ObjectContext.PartsSalesCategories.SingleOrDefault(r => r.Id == dbusedPartsShippingOrder.PartsSalesCategoryId);
                var usedPartsShippingDetails = ObjectContext.UsedPartsShippingDetails.Where(r => r.UsedPartsShippingOrderId == id).OrderBy(r => r.UsedPartsName).ThenBy(r => r.ClaimBillId).ToArray();
                var ids = usedPartsShippingDetails.Select(x => x.ClaimBillId);

                //var claimBills = this.ObjectContext.RepairClaimBills.Where(r => ids.Contains(r.Id)).ToArray();
                var usedPartsWarehouse = ObjectContext.UsedPartsWarehouses.SingleOrDefault(r => r.Id == dbusedPartsShippingOrder.DestinationWarehouseId && r.Status == (int)DcsBaseDataStatus.有效);
            }
            return dbusedPartsShippingOrder;
        }


        public IEnumerable<UsedPartsWarehouse> 旧件发运查询旧件仓库(int partsSalesCategoryId) {
            var userInfo = Utils.GetCurrentUserInfo();
            var usedPartsWarehouseIdQuery = from a in ObjectContext.DealerServiceInfoes.Where(r => r.Status == (int)DcsMasterDataStatus.有效)
                                            where a.DealerId == userInfo.EnterpriseId && a.PartsSalesCategoryId == partsSalesCategoryId
                                            select a.UsedPartsWarehouseId;
            var usedPartsWarehouseId = usedPartsWarehouseIdQuery.Distinct().SingleOrDefault();
            if(usedPartsWarehouseId.HasValue) {
                var usedPartsWarehousesQuery = from a in ObjectContext.UsedPartsWarehouses
                                               where a.Id == usedPartsWarehouseId.Value
                                               select a;
                var usedPartsWarehouses = usedPartsWarehousesQuery.ToArray();
                return usedPartsWarehouses;

            } else {
                var usedPartsWarehousesQuery = from a in ObjectContext.UsedPartsWarehouses
                                               where a.PartsSalesCategoryId == partsSalesCategoryId
                                               select a;
                var usedPartsWarehouses = usedPartsWarehousesQuery.ToArray();
                return usedPartsWarehouses;
            }
        }

        public IQueryable<UsedPartsShippingOrderWithOtherInfo> 旧件仓库人员查询发运单() {
            var userInfo = Utils.GetCurrentUserInfo();
            var usedPartsWarehouseIds = ObjectContext.UsedPartsWarehouseStaffs.Where(r => r.PersonnelId == userInfo.Id).Select(r => r.UsedPartsWarehouseId);


            var result = from a in ObjectContext.UsedPartsShippingOrders.Where(v => usedPartsWarehouseIds.Contains(v.DestinationWarehouseId))
                         join b in ObjectContext.DealerServiceInfoes.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on new {
                             a.DealerId,
                             a.PartsSalesCategoryId
                         } equals new {
                             b.DealerId,
                             b.PartsSalesCategoryId
                         } into tempTable0
                         from t0 in tempTable0.DefaultIfEmpty()
                         join c in ObjectContext.UsedPartsWarehouses on a.DestinationWarehouseId equals c.Id into tempTable1
                         from t1 in tempTable1.DefaultIfEmpty()
                         join d in ObjectContext.PartsSalesCategories on a.PartsSalesCategoryId equals d.Id into tempTable2
                         from t2 in tempTable2.DefaultIfEmpty()
                         join e in ObjectContext.Companies.Where(r => r.Status == (int)DcsMasterDataStatus.有效) on a.DealerId equals e.Id into tempTable3
                         from t3 in tempTable3.DefaultIfEmpty()
                         select new UsedPartsShippingOrderWithOtherInfo {
                             UsedPartsShippingOrderId = a.Id,
                             LogisticCompanyCode = a.LogisticCompanyCode,
                             LogisticCompanyName = a.LogisticCompanyName,
                             Operator = a.Operator,
                             OperatorPhone = a.OperatorPhone,
                             TransportationVehicle = a.TransportationVehicle,
                             RequestedArrivalDate = a.RequestedArrivalDate,
                             ActualArrivalDate = a.ActualArrivalDate,
                             ShippingDate = a.ShippingDate,
                             ModifierName = a.ModifierName,
                             ModifyTime = a.ModifyTime,
                             ConfirmorName = a.ConfirmorName,
                             ConfirmationTime = a.ConfirmationTime,
                             UsedPartsWarehouseId = t1 == null ? 0 : t1.Id,
                             PartsSalesCategoryId = a.PartsSalesCategoryId,
                             DealerServiceInfoId = t0 == null ? 0 : t0.Id,
                             DealerId = a.DealerId,
                             DealerCode = t3.Code,
                             DealerName = t3.Name,
                             BussinessCode = t0.BusinessCode,
                             UsedPartsShippingOrderCode = a.Code,
                             BranchId = a.BranchId,
                             BranchCode = a.BranchCode,
                             BranchName = a.BranchName,
                             PartsSalesCategoryName = t2.Name,
                             UsedPartsWarehouseName = t1.Name,
                             ShippingMethod = a.ShippingMethod,
                             Dispatcher = a.Dispatcher,
                             WarehouseContact = a.WarehouseContact,
                             WarehousePhone = a.WarehousePhone,
                             DestinationAddress = a.DestinationAddress,
                             TransportDriver = a.TransportDriver,
                             DriverPhone = a.DriverPhone,
                             Supervisor = a.Supervisor,
                             SupervisorPhone = a.SupervisorPhone,
                             SumUnitPrice = ObjectContext.UsedPartsShippingDetails.Where(r => r.UsedPartsShippingOrderId == a.Id).Sum(r => r.Quantity * r.UnitPrice),
                             DetailCount = ObjectContext.UsedPartsShippingDetails.Count(r => r.UsedPartsShippingOrderId == a.Id),
                             Status = a.Status,
                             Remark = a.Remark,
                             ReceptionRemark = a.ReceptionRemark,
                             CreatorName = a.CreatorName,
                             CreateTime = a.CreateTime
                         };
            return result.OrderBy(e => e.UsedPartsShippingOrderId);
        }




        public IEnumerable<UsedPartsShippingDetail> GetUsedPartsShippingDetailsOrderByUsedPartsBarCodeById(int id) {
            var usedPartsShippingDetails = ObjectContext.UsedPartsShippingDetails.Where(r => r.UsedPartsShippingOrderId == id).OrderBy(r => r.UsedPartsBarCode).ToArray();
            var ids = usedPartsShippingDetails.Select(x => x.ClaimBillId);
            //var claimBills = this.ObjectContext.RepairClaimBills.Where(r => ids.Contains(r.Id)).ToArray();
            return usedPartsShippingDetails.ToArray();
        }
    }
}
