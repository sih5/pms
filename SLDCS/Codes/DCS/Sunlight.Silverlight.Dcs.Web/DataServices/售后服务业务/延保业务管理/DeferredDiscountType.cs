﻿using System;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class DeferredDiscountTypeAch : DcsSerivceAchieveBase {
        public DeferredDiscountTypeAch(DcsDomainService domainService)
            : base(domainService) {
        }
        internal void InsertDeferredDiscountTypeValidate(DeferredDiscountType deferredDiscountType) {
            var userInfo = Utils.GetCurrentUserInfo();
            deferredDiscountType.CreatorId = userInfo.Id;
            deferredDiscountType.CreatorName = userInfo.Name;
            deferredDiscountType.CreatorTime = DateTime.Now;
        }

        internal void UpdateDeferredDiscountTypeValidate(DeferredDiscountType deferredDiscountType) {
            var userInfo = Utils.GetCurrentUserInfo();
            deferredDiscountType.ModifierId = userInfo.Id;
            deferredDiscountType.ModifierName = userInfo.Name;
            deferredDiscountType.ModifierTime = DateTime.Now;
        }

        public void InsertDeferredDiscountType(DeferredDiscountType deferredDiscountType) {
            InsertToDatabase(deferredDiscountType);
            this.InsertDeferredDiscountTypeValidate(deferredDiscountType);
        }

        public void UpdateDeferredDiscountType(DeferredDiscountType deferredDiscountType) {
            UpdateToDatabase(deferredDiscountType);
            this.UpdateDeferredDiscountTypeValidate(deferredDiscountType);
        }

        public IQueryable<DeferredDiscountType> GetValidDeferredDiscountType() {
            return this.ObjectContext.DeferredDiscountTypes.Where(r => (r.Status == (int)DcsBaseDataStatus.有效 && r.EffectiveTime <= DateTime.Now && r.EndTime >= DateTime.Now) || r.Id == -1);
        }
    }

    partial class DcsDomainService {
        public void UpdateIntegralSettleDictate(DeferredDiscountType deferredDiscountType) {
            new DeferredDiscountTypeAch(this).UpdateDeferredDiscountType(deferredDiscountType);
        }

        public void InsertEDeferredDiscountType(DeferredDiscountType deferredDiscountType) {
            new DeferredDiscountTypeAch(this).InsertDeferredDiscountType(deferredDiscountType);
        }

        public IQueryable<DeferredDiscountType> GetValidDeferredDiscountType() {
            return new DeferredDiscountTypeAch(this).GetValidDeferredDiscountType();
        }
    }
}