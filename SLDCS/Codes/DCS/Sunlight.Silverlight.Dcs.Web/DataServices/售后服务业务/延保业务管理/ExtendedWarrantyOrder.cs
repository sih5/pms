﻿using System;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class ExtendedWarrantyOrderAch : DcsSerivceAchieveBase {
        public ExtendedWarrantyOrderAch(DcsDomainService domainService)
            : base(domainService) {
        }
        internal void InsertExtendedWarrantyOrderValidate(ExtendedWarrantyOrder extendedWarrantyOrder) {
            var userInfo = Utils.GetCurrentUserInfo();
            extendedWarrantyOrder.CreatorId = userInfo.Id;
            extendedWarrantyOrder.CreatorName = userInfo.Name;
            extendedWarrantyOrder.CreatorTime = DateTime.Now;
        }

        internal void UpdateExtendedWarrantyOrderValidate(ExtendedWarrantyOrder extendedWarrantyOrder) {
            var userInfo = Utils.GetCurrentUserInfo();
            extendedWarrantyOrder.ModifierId = userInfo.Id;
            extendedWarrantyOrder.ModifierName = userInfo.Name;
            extendedWarrantyOrder.ModifierTime = DateTime.Now;
        }

        public void InsertExtendedWarrantyOrder(ExtendedWarrantyOrder extendedWarrantyOrder) {
            InsertToDatabase(extendedWarrantyOrder);
            var extendedWarrantyOrderLists = ChangeSet.GetAssociatedChanges(extendedWarrantyOrder, v => v.ExtendedWarrantyOrderLists, ChangeOperation.Insert);
            foreach(ExtendedWarrantyOrderList extendedWarrantyOrderList in extendedWarrantyOrderLists) {
                InsertToDatabase(extendedWarrantyOrderList);
            }
            this.InsertExtendedWarrantyOrderValidate(extendedWarrantyOrder);
        }

        public void UpdateExtendedWarrantyOrder(ExtendedWarrantyOrder extendedWarrantyOrder) {
            extendedWarrantyOrder.ExtendedWarrantyOrderLists.Clear();
            UpdateToDatabase(extendedWarrantyOrder);
            var extendedWarrantyOrderLists = ChangeSet.GetAssociatedChanges(extendedWarrantyOrder, v => v.ExtendedWarrantyOrderLists);
            foreach(ExtendedWarrantyOrderList extendedWarrantyOrderList in extendedWarrantyOrderLists) {
                switch(ChangeSet.GetChangeOperation(extendedWarrantyOrderList)) {
                    case ChangeOperation.Insert:
                        InsertToDatabase(extendedWarrantyOrderList);
                        break;
                    case ChangeOperation.Update:
                        UpdateToDatabase(extendedWarrantyOrderList);
                        break;
                    case ChangeOperation.Delete:
                        DeleteFromDatabase(extendedWarrantyOrderList);
                        break;
                }
            }
            this.UpdateExtendedWarrantyOrderValidate(extendedWarrantyOrder);
        }

        public ExtendedWarrantyOrder GetExtendedWarrantyOrderDetailById(int id) {
            var extendedWarrantyOrder = this.ObjectContext.ExtendedWarrantyOrders.Include("RepairObject").SingleOrDefault(r => r.Id == id);
            if(extendedWarrantyOrder != null) {
                var extendedWarrantyOrderLists = this.ObjectContext.ExtendedWarrantyOrderLists.Include("ExtendedWarrantyProduct").Include("DeferredDiscountType").Where(r => r.ExtendedWarrantyOrderId == extendedWarrantyOrder.Id).ToArray();
            }
            return extendedWarrantyOrder;
        }

        public ExtendedWarrantyOrder GetExtendedWarrantyOrderById(int id) {
            return this.ObjectContext.ExtendedWarrantyOrders.SingleOrDefault(r => r.Id == id);
        }

        public IQueryable<ExtendedWarrantyOrder> GetExtendedWarrantyOrderInRepairObject() {
            return this.ObjectContext.ExtendedWarrantyOrders.Include("RepairObject").OrderBy(r => r.Id);
        }

        public IQueryable<ExtendedWarrantyOrderList> GetExtendedWarrantyOrderListInfos() {
            return this.ObjectContext.ExtendedWarrantyOrderLists.Include("ExtendedWarrantyProduct").Include("DeferredDiscountType").OrderBy(r => r.Id);
        }
    }

    partial class DcsDomainService {
        public void UpdateIntegralSettleDictate(ExtendedWarrantyOrder extendedWarrantyOrder) {
            new ExtendedWarrantyOrderAch(this).UpdateExtendedWarrantyOrder(extendedWarrantyOrder);
        }

        public void InsertExtendedWarrantyOrder(ExtendedWarrantyOrder extendedWarrantyOrder) {
            new ExtendedWarrantyOrderAch(this).InsertExtendedWarrantyOrder(extendedWarrantyOrder);
        }
        public ExtendedWarrantyOrder GetExtendedWarrantyOrderDetailById(int id) {
            return new ExtendedWarrantyOrderAch(this).GetExtendedWarrantyOrderDetailById(id);
        }
        public IQueryable<ExtendedWarrantyOrder> GetExtendedWarrantyOrderInRepairObject() {
            return new ExtendedWarrantyOrderAch(this).GetExtendedWarrantyOrderInRepairObject();
        }

        public IQueryable<ExtendedWarrantyOrderList> GetExtendedWarrantyOrderListInfos() {
            return new ExtendedWarrantyOrderAch(this).GetExtendedWarrantyOrderListInfos();
        }

        public ExtendedWarrantyOrder GetExtendedWarrantyOrderById(int id) {
            return new ExtendedWarrantyOrderAch(this).GetExtendedWarrantyOrderById(id);
        }
    }
}