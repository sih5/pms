﻿using Sunlight.Silverlight.Dcs.Web.Entities;
using System.Collections.Generic;
using System.Linq;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class PartsOutboundPlanDetailAch : DcsSerivceAchieveBase {
        public PartsOutboundPlanDetailAch(DcsDomainService domainService)
            : base(domainService) {
        }

        public IQueryable<PartsOutboundPlanDetail> GetPartsOutboundPlanDetailsWithSparePart() {
           return ObjectContext.PartsOutboundPlanDetails.Include("SparePart").OrderBy(r => r.Id);
        }

        public IEnumerable<VirtualPartsOutboundPlanDetail> GetPartsOutboundPlanDetailsWithSparePart2(int partsOutboundPlanId)
        {
            var result = (from a in ObjectContext.PartsOutboundPlanDetails.Where(t => t.PartsOutboundPlanId == partsOutboundPlanId)
                         join b in ObjectContext.SpareParts on a.SparePartId equals b.Id
                         join c in ObjectContext.PartsRetailGuidePrices on new { a.SparePartId,  Status = (int)DcsBaseDataStatus.有效 } equals new { c.SparePartId,c.Status}
                         select new VirtualPartsOutboundPlanDetail
                         {
                             Id = a.Id,
                             SparePartCode = a.SparePartCode,
                             SparePartName = a.SparePartName,
                             SparePartId = a.SparePartId,
                             PlannedAmount = a.PlannedAmount,
                             OutboundFulfillment = a.OutboundFulfillment.Value,
                             MeasureUnit = b.MeasureUnit,
                             Remark = a.Remark,
                             Price=c.RetailGuidePrice,
                             PartsOutboundPlanId = a.PartsOutboundPlanId
                         }).ToArray();
            return result.OrderBy(r=>r.Id);
        }

    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:30
        //原分布类的函数全部转移到Ach                                                               

        public IQueryable<PartsOutboundPlanDetail> GetPartsOutboundPlanDetailsWithSparePart() {
            return new PartsOutboundPlanDetailAch(this).GetPartsOutboundPlanDetailsWithSparePart();
        }
        public IEnumerable<VirtualPartsOutboundPlanDetail> GetPartsOutboundPlanDetailsWithSparePart2(int partsOutboundPlanId)
        {
            return new PartsOutboundPlanDetailAch(this).GetPartsOutboundPlanDetailsWithSparePart2(partsOutboundPlanId);
        }
    }
}
