﻿using System;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class UsedPartsDisposalBillAch : DcsSerivceAchieveBase {
        internal void InsertUsedPartsDisposalBillValidate(UsedPartsDisposalBill usedPartsDisposalBill) {
            var userInfo = Utils.GetCurrentUserInfo();
            usedPartsDisposalBill.CreatorId = userInfo.Id;
            usedPartsDisposalBill.CreatorName = userInfo.Name;
            usedPartsDisposalBill.CreateTime = DateTime.Now;
            if(string.IsNullOrWhiteSpace(usedPartsDisposalBill.Code) || usedPartsDisposalBill.Code == GlobalVar.ASSIGNED_BY_SERVER)
                usedPartsDisposalBill.Code = CodeGenerator.Generate("UsedPartsDisposalBill", usedPartsDisposalBill.UsedPartsWarehouseCode);
        }

        internal void UpdateUsedPartsDisposalBillValidate(UsedPartsDisposalBill usedPartsDisposalBill) {
            var userInfo = Utils.GetCurrentUserInfo();
            usedPartsDisposalBill.ModifierId = userInfo.Id;
            usedPartsDisposalBill.ModifierName = userInfo.Name;
            usedPartsDisposalBill.ModifyTime = DateTime.Now;
        }

        public void InsertUsedPartsDisposalBill(UsedPartsDisposalBill usedPartsDisposalBill) {
            InsertToDatabase(usedPartsDisposalBill);
            var usedPartsDisposalDetails = ChangeSet.GetAssociatedChanges(usedPartsDisposalBill, r => r.UsedPartsDisposalDetails, ChangeOperation.Insert);
            foreach(UsedPartsDisposalDetail usedPartsDisposalDetail in usedPartsDisposalDetails)
                InsertToDatabase(usedPartsDisposalDetail);
            this.InsertUsedPartsDisposalBillValidate(usedPartsDisposalBill);
        }

        public void UpdateUsedPartsDisposalBill(UsedPartsDisposalBill usedPartsDisposalBill) {
            usedPartsDisposalBill.UsedPartsDisposalDetails.Clear();
            UpdateToDatabase(usedPartsDisposalBill);
            var usedPartsDisposalDetails = ChangeSet.GetAssociatedChanges(usedPartsDisposalBill, r => r.UsedPartsDisposalDetails);
            foreach(UsedPartsDisposalDetail usedPartsDisposalDetail in usedPartsDisposalDetails) {
                switch(ChangeSet.GetChangeOperation(usedPartsDisposalDetail)) {
                    case ChangeOperation.Insert:
                        InsertToDatabase(usedPartsDisposalDetail);
                        break;
                    case ChangeOperation.Update:
                        UpdateToDatabase(usedPartsDisposalDetail);
                        break;
                    case ChangeOperation.Delete:
                        DeleteFromDatabase(usedPartsDisposalDetail);
                        break;
                }
            }
            this.UpdateUsedPartsDisposalBillValidate(usedPartsDisposalBill);
        }

        public UsedPartsDisposalBill GetUsedPartsDisposalBillsWithDetails(int id) {
            var dbUsedPartsDisposalBill = ObjectContext.UsedPartsDisposalBills.SingleOrDefault(e => e.Id == id);
            if(dbUsedPartsDisposalBill != null) {
                var details = ObjectContext.UsedPartsDisposalDetails.Where(e => e.UsedPartsDisposalBillId == dbUsedPartsDisposalBill.Id).ToArray();
            }
            return dbUsedPartsDisposalBill;
        }

        public IQueryable<UsedPartsDisposalBill> 旧件仓库人员查询旧件处理单() {
            var userInfo = Silverlight.Web.Utils.GetCurrentUserInfo();
            var usedPartsWarehouseIds = ObjectContext.UsedPartsWarehouseStaffs.Where(r => r.PersonnelId == userInfo.Id).Select(r => r.UsedPartsWarehouseId);
            var result = ObjectContext.UsedPartsDisposalBills.Where(v => usedPartsWarehouseIds.Contains(v.UsedPartsWarehouseId));
            return result;
        }
    
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:30
        //原分布类的函数全部转移到Ach                                                               

        public void InsertUsedPartsDisposalBill(UsedPartsDisposalBill usedPartsDisposalBill) {
            new UsedPartsDisposalBillAch(this).InsertUsedPartsDisposalBill(usedPartsDisposalBill);
        }

        public void UpdateUsedPartsDisposalBill(UsedPartsDisposalBill usedPartsDisposalBill) {
            new UsedPartsDisposalBillAch(this).UpdateUsedPartsDisposalBill(usedPartsDisposalBill);
        }

        public UsedPartsDisposalBill GetUsedPartsDisposalBillsWithDetails(int id) {
            return new UsedPartsDisposalBillAch(this).GetUsedPartsDisposalBillsWithDetails(id);
        }

        public IQueryable<UsedPartsDisposalBill> 旧件仓库人员查询旧件处理单() {
            return new UsedPartsDisposalBillAch(this).旧件仓库人员查询旧件处理单();
        }
    }
}
