﻿using System;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class UsedPartsInboundOrderAch : DcsSerivceAchieveBase {
        internal void InsertUsedPartsInboundOrderValidate(UsedPartsInboundOrder usedPartsInboundOrder) {
            var userInfo = Utils.GetCurrentUserInfo();
            usedPartsInboundOrder.CreatorId = userInfo.Id;
            usedPartsInboundOrder.CreatorName = userInfo.Name;
            usedPartsInboundOrder.CreateTime = DateTime.Now;
            if(string.IsNullOrWhiteSpace(usedPartsInboundOrder.Code) || usedPartsInboundOrder.Code == GlobalVar.ASSIGNED_BY_SERVER)
                usedPartsInboundOrder.Code = CodeGenerator.Generate("UsedPartsInboundOrder", usedPartsInboundOrder.UsedPartsWarehouseCode);
        }

        internal void UpdateUsedPartsInboundOrderValidate(UsedPartsInboundOrder usedPartsInboundOrder) {
            var userInfo = Utils.GetCurrentUserInfo();
            usedPartsInboundOrder.ModifierId = userInfo.Id;
            usedPartsInboundOrder.ModifierName = userInfo.Name;
            usedPartsInboundOrder.ModifyTime = DateTime.Now;
        }

        public void InsertUsedPartsInboundOrder(UsedPartsInboundOrder usedPartsInboundOrder) {
            InsertToDatabase(usedPartsInboundOrder);
            var usedPartsInboundDetails = ChangeSet.GetAssociatedChanges(usedPartsInboundOrder, r => r.UsedPartsInboundDetails, ChangeOperation.Insert);
            foreach(UsedPartsInboundDetail usedPartsInboundDetail in usedPartsInboundDetails)
                InsertToDatabase(usedPartsInboundDetail);
            this.InsertUsedPartsInboundOrderValidate(usedPartsInboundOrder);
        }

        public void UpdateUsedPartsInboundOrder(UsedPartsInboundOrder usedPartsInboundOrder) {
            usedPartsInboundOrder.UsedPartsInboundDetails.Clear();
            UpdateToDatabase(usedPartsInboundOrder);
            var usedPartsInboundDetails = ChangeSet.GetAssociatedChanges(usedPartsInboundOrder, r => r.UsedPartsInboundDetails);
            foreach(UsedPartsInboundDetail usedPartsInboundDetail in usedPartsInboundDetails) {
                switch(ChangeSet.GetChangeOperation(usedPartsInboundDetail)) {
                    case ChangeOperation.Insert:
                        InsertToDatabase(usedPartsInboundDetail);
                        break;
                    case ChangeOperation.Update:
                        UpdateToDatabase(usedPartsInboundDetail);
                        break;
                    case ChangeOperation.Delete:
                        DeleteFromDatabase(usedPartsInboundDetail);
                        break;
                }
            }
            this.UpdateUsedPartsInboundOrderValidate(usedPartsInboundOrder);
        }

        public IQueryable<UsedPartsInboundOrder> 旧件仓库人员查询旧件入库单() {
            var userInfo = Silverlight.Web.Utils.GetCurrentUserInfo();
            var usedPartsWarehouseIds = ObjectContext.UsedPartsWarehouseStaffs.Where(r => r.PersonnelId == userInfo.Id).Select(r => r.UsedPartsWarehouseId);
            var result = ObjectContext.UsedPartsInboundOrders.Where(v => usedPartsWarehouseIds.Contains(v.UsedPartsWarehouseId));
            return result.Include("UsedPartsInboundDetails").OrderBy(r => r.Id);
        }

        public IQueryable<VirtualUsedPartsInboundOrder> 查询旧件入库单() {
            var userInfo = Utils.GetCurrentUserInfo();
            var usedPartsWarehouseIds = ObjectContext.UsedPartsWarehouseStaffs.Where(r => r.PersonnelId == userInfo.Id).Select(r => r.UsedPartsWarehouseId);
            var data = from a in ObjectContext.UsedPartsInboundOrders.Where(x => x.InboundType == (int)DcsUsedPartsInboundOrderInboundType.服务站返件入库 && usedPartsWarehouseIds.Contains(x.UsedPartsWarehouseId))
                       join b in ObjectContext.UsedPartsWarehouses.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on a.UsedPartsWarehouseId equals b.Id
                       join c in ObjectContext.DealerServiceInfoes.Where(r => r.Status == (int)DcsBaseDataStatus.有效) on new {
                           DealerId = (int)a.RelatedCompanyId,
                           b.PartsSalesCategoryId
                       }equals new {
                           c.DealerId,
                           c.PartsSalesCategoryId
                       }into temptable1
                       from t in temptable1.DefaultIfEmpty()
                       select new {
                           Id = a.Id,
                           Code = a.Code,
                           UsedPartsWarehouseCode = a.UsedPartsWarehouseCode,
                           UsedPartsWarehouseName = a.UsedPartsWarehouseName,
                           Businesscode = t.BusinessCode,
                           InboundType = a.InboundType,
                           RelatedCompanyCode = a.RelatedCompanyCode,
                           RelatedCompanyName = a.RelatedCompanyName,
                           TotalAmount = a.TotalAmount,
                           TotalQuantity = a.TotalQuantity,
                           SourceCode = a.SourceCode,
                           UsedPartsEncourageAmount = a.UsedPartsInboundDetails.Sum(r => (r.UsedPartsEncourageAmount ?? 0) * r.Quantity),
                           Operator = a.Operator,
                           Remark = a.Remark,
                           SettlementStatus = a.SettlementStatus,
                           CreatorName = a.CreatorName,
                           CreateTime = a.CreateTime
                       };

            var data2 = from a in ObjectContext.UsedPartsInboundOrders.Where(x => x.InboundType != (int)DcsUsedPartsInboundOrderInboundType.服务站返件入库 && usedPartsWarehouseIds.Contains(x.UsedPartsWarehouseId))
                        select new {
                            Id = a.Id,
                            Code = a.Code,
                            UsedPartsWarehouseCode = a.UsedPartsWarehouseCode,
                            UsedPartsWarehouseName = a.UsedPartsWarehouseName,
                            Businesscode = "",
                            InboundType = a.InboundType,
                            RelatedCompanyCode = a.RelatedCompanyCode,
                            RelatedCompanyName = a.RelatedCompanyName,
                            TotalAmount = a.TotalAmount,
                            TotalQuantity = a.TotalQuantity,
                            SourceCode = a.SourceCode,
                            UsedPartsEncourageAmount = a.UsedPartsInboundDetails.Sum(r => (r.UsedPartsEncourageAmount ?? 0)),
                            Operator = a.Operator,
                            Remark = a.Remark,
                            SettlementStatus = a.SettlementStatus,
                            CreatorName = a.CreatorName,
                            CreateTime = a.CreateTime
                        };
            var data3 = data.Union(data2);
            var result = from a in data3
                         select new VirtualUsedPartsInboundOrder {
                             Id = a.Id,
                             Code = a.Code,
                             UsedPartsWarehouseCode = a.UsedPartsWarehouseCode,
                             UsedPartsWarehouseName = a.UsedPartsWarehouseName,
                             Businesscode = a.Businesscode,
                             InboundType = a.InboundType,

                             RelatedCompanyCode = a.RelatedCompanyCode,
                             RelatedCompanyName = a.RelatedCompanyName,
                             TotalAmount = a.TotalAmount,
                             TotalQuantity = a.TotalQuantity,
                             SourceCode = a.SourceCode,
                             UsedPartsEncourageAmount = a.UsedPartsEncourageAmount,
                             Operator = a.Operator,
                             Remark = a.Remark,
                             SettlementStatus = a.SettlementStatus,
                             CreatorName = a.CreatorName,
                             CreateTime = a.CreateTime
                         };
            return result.OrderBy(x => x.Id);
        }
    
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:30
        //原分布类的函数全部转移到Ach                                                               

        public void InsertUsedPartsInboundOrder(UsedPartsInboundOrder usedPartsInboundOrder) {
            new UsedPartsInboundOrderAch(this).InsertUsedPartsInboundOrder(usedPartsInboundOrder);
        }

        public void UpdateUsedPartsInboundOrder(UsedPartsInboundOrder usedPartsInboundOrder) {
            new UsedPartsInboundOrderAch(this).UpdateUsedPartsInboundOrder(usedPartsInboundOrder);
        }

        public IQueryable<UsedPartsInboundOrder> 旧件仓库人员查询旧件入库单() {
            return new UsedPartsInboundOrderAch(this).旧件仓库人员查询旧件入库单();
        }

        public IQueryable<VirtualUsedPartsInboundOrder> 查询旧件入库单() {
            return new UsedPartsInboundOrderAch(this).查询旧件入库单();
        }
    }
}
