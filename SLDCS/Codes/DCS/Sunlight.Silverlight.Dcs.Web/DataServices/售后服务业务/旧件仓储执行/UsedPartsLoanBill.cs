﻿using System;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class UsedPartsLoanBillAch : DcsSerivceAchieveBase {
        internal void InsertUsedPartsLoanBillValidate(UsedPartsLoanBill usedPartsLoanBill) {
            var userInfo = Utils.GetCurrentUserInfo();
            usedPartsLoanBill.CreatorId = userInfo.Id;
            usedPartsLoanBill.CreatorName = userInfo.Name;
            usedPartsLoanBill.CreateTime = DateTime.Now;
            if(string.IsNullOrWhiteSpace(usedPartsLoanBill.Code) || usedPartsLoanBill.Code == GlobalVar.ASSIGNED_BY_SERVER)
                usedPartsLoanBill.Code = CodeGenerator.Generate("UsedPartsLoanBill", usedPartsLoanBill.UsedPartsWarehouseCode);
        }

        internal void UpdateUsedPartsLoanBillValidate(UsedPartsLoanBill usedPartsLoanBill) {
            var userInfo = Utils.GetCurrentUserInfo();
            usedPartsLoanBill.ModifierId = userInfo.Id;
            usedPartsLoanBill.ModifierName = userInfo.Name;
            usedPartsLoanBill.ModifyTime = DateTime.Now;
        }

        public void InsertUsedPartsLoanBill(UsedPartsLoanBill usedPartsLoanBill) {
            InsertToDatabase(usedPartsLoanBill);
            var usedPartsLoanDetails = ChangeSet.GetAssociatedChanges(usedPartsLoanBill, r => r.UsedPartsLoanDetails, ChangeOperation.Insert);
            foreach(UsedPartsLoanDetail usedPartsLoanDetail in usedPartsLoanDetails)
                InsertToDatabase(usedPartsLoanDetail);
            this.InsertUsedPartsLoanBillValidate(usedPartsLoanBill);
        }

        public void UpdateUsedPartsLoanBill(UsedPartsLoanBill usedPartsLoanBill) {
            usedPartsLoanBill.UsedPartsLoanDetails.Clear();
            UpdateToDatabase(usedPartsLoanBill);
            var usedPartsLoanDetails = ChangeSet.GetAssociatedChanges(usedPartsLoanBill, r => r.UsedPartsLoanDetails);
            foreach(UsedPartsLoanDetail usedPartsLoanDetail in usedPartsLoanDetails) {
                switch(ChangeSet.GetChangeOperation(usedPartsLoanDetail)) {
                    case ChangeOperation.Insert:
                        InsertToDatabase(usedPartsLoanDetail);
                        break;
                    case ChangeOperation.Update:
                        UpdateToDatabase(usedPartsLoanDetail);
                        break;
                    case ChangeOperation.Delete:
                        DeleteFromDatabase(usedPartsLoanDetail);
                        break;
                }
            }
            this.UpdateUsedPartsLoanBillValidate(usedPartsLoanBill);
        }

        public UsedPartsLoanBill GetUsedPartsLoanBillsWithDetailsById(int id) {
            var dbUsedPartsLoanBill = ObjectContext.UsedPartsLoanBills.SingleOrDefault(e => e.Id == id);
            if(dbUsedPartsLoanBill != null) {
                var loanDetails = ObjectContext.UsedPartsLoanDetails.Where(e => e.UsedPartsLoanBillId == dbUsedPartsLoanBill.Id).ToArray();
            }
            return dbUsedPartsLoanBill;
        }

        public IQueryable<UsedPartsLoanBill> 旧件仓库人员查询旧件借用单() {
            var userInfo = Utils.GetCurrentUserInfo();
            var query = ObjectContext.UsedPartsLoanBills.Where(r => ObjectContext.UsedPartsWarehouseStaffs.Any(v => v.PersonnelId == userInfo.Id && v.UsedPartsWarehouseId == r.UsedPartsWarehouseId));
            return query.OrderByDescending(r => r.Id).Include("UsedPartsLoanDetails");
        }








    
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:30
        //原分布类的函数全部转移到Ach                                                               

        public void InsertUsedPartsLoanBill(UsedPartsLoanBill usedPartsLoanBill) {
            new UsedPartsLoanBillAch(this).InsertUsedPartsLoanBill(usedPartsLoanBill);
        }

        public void UpdateUsedPartsLoanBill(UsedPartsLoanBill usedPartsLoanBill) {
            new UsedPartsLoanBillAch(this).UpdateUsedPartsLoanBill(usedPartsLoanBill);
        }

        public UsedPartsLoanBill GetUsedPartsLoanBillsWithDetailsById(int id) {
            return new UsedPartsLoanBillAch(this).GetUsedPartsLoanBillsWithDetailsById(id);
        }

        public IQueryable<UsedPartsLoanBill> 旧件仓库人员查询旧件借用单() {
            return new UsedPartsLoanBillAch(this).旧件仓库人员查询旧件借用单();
        }
    }
}
