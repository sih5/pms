﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class UsedPartsOutboundOrderAch : DcsSerivceAchieveBase {
        internal void InsertUsedPartsOutboundOrderValidate(UsedPartsOutboundOrder usedPartsOutboundOrder) {
            var userInfo = Utils.GetCurrentUserInfo();
            usedPartsOutboundOrder.CreatorId = userInfo.Id;
            usedPartsOutboundOrder.CreatorName = userInfo.Name;
            usedPartsOutboundOrder.CreateTime = DateTime.Now;
            if(string.IsNullOrWhiteSpace(usedPartsOutboundOrder.Code) || usedPartsOutboundOrder.Code == GlobalVar.ASSIGNED_BY_SERVER)
                usedPartsOutboundOrder.Code = CodeGenerator.Generate("UsedPartsOutboundOrder", usedPartsOutboundOrder.UsedPartsWarehouseCode);
        }

        internal void UpdateUsedPartsOutboundOrderValidate(UsedPartsOutboundOrder usedPartsOutboundOrder) {
            var userInfo = Utils.GetCurrentUserInfo();
            usedPartsOutboundOrder.ModifierId = userInfo.Id;
            usedPartsOutboundOrder.ModifierName = userInfo.Name;
            usedPartsOutboundOrder.ModifyTime = DateTime.Now;
        }

        public void InsertUsedPartsOutboundOrder(UsedPartsOutboundOrder usedPartsOutboundOrder) {
            InsertToDatabase(usedPartsOutboundOrder);
            var usedPartsOutboundDetails = ChangeSet.GetAssociatedChanges(usedPartsOutboundOrder, r => r.UsedPartsOutboundDetails, ChangeOperation.Insert);
            foreach(UsedPartsOutboundDetail usedPartsOutboundDetail in usedPartsOutboundDetails)
                InsertToDatabase(usedPartsOutboundDetail);
            this.InsertUsedPartsOutboundOrderValidate(usedPartsOutboundOrder);
        }

        public void UpdateUsedPartsOutboundOrder(UsedPartsOutboundOrder usedPartsOutboundOrder) {
            usedPartsOutboundOrder.UsedPartsOutboundDetails.Clear();
            UpdateToDatabase(usedPartsOutboundOrder);
            var usedPartsOutboundDetails = ChangeSet.GetAssociatedChanges(usedPartsOutboundOrder, r => r.UsedPartsOutboundDetails);
            foreach(UsedPartsOutboundDetail usedPartsOutboundDetail in usedPartsOutboundDetails) {
                switch(ChangeSet.GetChangeOperation(usedPartsOutboundDetail)) {
                    case ChangeOperation.Insert:
                        InsertToDatabase(usedPartsOutboundDetail);
                        break;
                    case ChangeOperation.Update:
                        UpdateToDatabase(usedPartsOutboundDetail);
                        break;
                    case ChangeOperation.Delete:
                        DeleteFromDatabase(usedPartsOutboundDetail);
                        break;
                }
            }
            this.UpdateUsedPartsOutboundOrderValidate(usedPartsOutboundOrder);
        }

        public IQueryable<UsedPartsOutboundOrder> 根据旧件查询旧件出库单(string usedPartsBarCode, string claimBillCode) {
            IQueryable<UsedPartsOutboundDetail> usedPartsOutboundDetails = ObjectContext.UsedPartsOutboundDetails;
            if(!string.IsNullOrEmpty(usedPartsBarCode)) {
                usedPartsOutboundDetails = usedPartsOutboundDetails.Where(r => r.UsedPartsBarCode == usedPartsBarCode);
            }
            if(!string.IsNullOrEmpty(claimBillCode)) {
                usedPartsOutboundDetails = usedPartsOutboundDetails.Where(r => r.ClaimBillCode == claimBillCode);
            }
            return ObjectContext.UsedPartsOutboundOrders.Where(r => usedPartsOutboundDetails.Any(v => v.UsedPartsOutboundOrderId == r.Id));

        }

        public IEnumerable<UsedPartsOutboundOrder> 旧件仓库人员查询旧件出库单(string usedPartsBarCode, string claimBillCode, string businessCode, string relatedCompanyCode, string relatedCompanyName, DateTime? beginCreateTime, DateTime? endCreateTime) {
            var userInfo = Utils.GetCurrentUserInfo();
            var usedPartsWarehouseIds = ObjectContext.UsedPartsWarehouseStaffs.Where(r => r.PersonnelId == userInfo.Id).Select(r => r.UsedPartsWarehouseId).ToArray();
            if(usedPartsWarehouseIds.Length == 0) {
                return null;
            }
            //主单query
            IQueryable<UsedPartsOutboundOrder> usedPartsOutboundOrderQuery = ObjectContext.UsedPartsOutboundOrders.Where(r => usedPartsWarehouseIds.Contains(r.UsedPartsWarehouseId));
            if(!string.IsNullOrWhiteSpace(relatedCompanyCode)) {
                usedPartsOutboundOrderQuery = usedPartsOutboundOrderQuery.Where(r => r.RelatedCompanyCode.Contains(relatedCompanyCode));
            }
            if(!string.IsNullOrWhiteSpace(relatedCompanyName)) {
                usedPartsOutboundOrderQuery = usedPartsOutboundOrderQuery.Where(r => r.RelatedCompanyName.Contains(relatedCompanyName));
            }
            if(beginCreateTime.HasValue) {
                usedPartsOutboundOrderQuery = usedPartsOutboundOrderQuery.Where(r => r.CreateTime >= beginCreateTime);
            }
            if(endCreateTime.HasValue) {
                usedPartsOutboundOrderQuery = usedPartsOutboundOrderQuery.Where(r => r.CreateTime <= endCreateTime);
            }
            //清单query
            IQueryable<UsedPartsOutboundDetail> usedPartsOutboundDetailsQuery = ObjectContext.UsedPartsOutboundDetails;
            if(!string.IsNullOrEmpty(usedPartsBarCode)) {
                usedPartsOutboundDetailsQuery = usedPartsOutboundDetailsQuery.Where(r => r.UsedPartsBarCode.Contains(usedPartsBarCode));
            }

            if(!string.IsNullOrEmpty(claimBillCode)) {
                usedPartsOutboundDetailsQuery = usedPartsOutboundDetailsQuery.Where(r => r.ClaimBillCode.Contains(claimBillCode));
            }
   
            //分公司与供应商关系query
            var branchSupplierRelationQuery = ObjectContext.BranchSupplierRelations.GroupBy(r => new {
                r.BusinessCode,
                r.SupplierId
            }).Select(r => new {
                r.Key.BusinessCode,
                r.Key.SupplierId
            });


            var usedPartsOutboundOrdersWithBussinessCode = (from a in usedPartsOutboundOrderQuery.Where(r => usedPartsOutboundDetailsQuery.Any(v => r.Id == v.UsedPartsOutboundOrderId))
                                                            join b in branchSupplierRelationQuery on a.RelatedCompanyId equals b.SupplierId into temptable
                                                            from t in temptable.DefaultIfEmpty()
                                                            select new {
                                                                UsedPartsOutboundOrderEntity = a,
                                                                BussinessCode = t.BusinessCode ?? ""
                                                            }).ToArray();
            if(usedPartsOutboundOrdersWithBussinessCode.Length == 0) {
                return null;
            }
            int check = 0;
            try {
                for(var i = 0; i < usedPartsOutboundOrdersWithBussinessCode.Length; i++) {
                    check = i;
                    usedPartsOutboundOrdersWithBussinessCode[i].UsedPartsOutboundOrderEntity.BusinessCode = usedPartsOutboundOrdersWithBussinessCode[i].BussinessCode;
                }

                var result = usedPartsOutboundOrdersWithBussinessCode.Select(r => r.UsedPartsOutboundOrderEntity).ToArray();
                if(!string.IsNullOrEmpty(businessCode)) {
                    result = result.Where(r => !string.IsNullOrEmpty(r.BusinessCode) && r.BusinessCode.IndexOf(businessCode) != -1).ToArray();
                }
                return result.OrderBy(r => r.Id);
            }catch(Exception ex) {
                throw new Exception(ex.Message + "_" + check);
            }

        }
    
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:30
        //原分布类的函数全部转移到Ach                                                               

        public void InsertUsedPartsOutboundOrder(UsedPartsOutboundOrder usedPartsOutboundOrder) {
            new UsedPartsOutboundOrderAch(this).InsertUsedPartsOutboundOrder(usedPartsOutboundOrder);
        }

        public void UpdateUsedPartsOutboundOrder(UsedPartsOutboundOrder usedPartsOutboundOrder) {
            new UsedPartsOutboundOrderAch(this).UpdateUsedPartsOutboundOrder(usedPartsOutboundOrder);
        }

        public IQueryable<UsedPartsOutboundOrder> 根据旧件查询旧件出库单(string usedPartsBarCode, string claimBillCode) {
            return new UsedPartsOutboundOrderAch(this).根据旧件查询旧件出库单(usedPartsBarCode,claimBillCode);
        }

        public IEnumerable<UsedPartsOutboundOrder> 旧件仓库人员查询旧件出库单(string usedPartsBarCode, string claimBillCode, string businessCode, string relatedCompanyCode, string relatedCompanyName, DateTime? beginCreateTime, DateTime? endCreateTime) {
            return new UsedPartsOutboundOrderAch(this).旧件仓库人员查询旧件出库单(usedPartsBarCode,claimBillCode,businessCode,relatedCompanyCode,relatedCompanyName,beginCreateTime,endCreateTime);
        }
    }
}
