﻿using System;
using System.ServiceModel.DomainServices.Server;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class UsedPartsRefitBillAch : DcsSerivceAchieveBase {
        internal void InsertUsedPartsRefitBillValidate(UsedPartsRefitBill usedPartsRefitBill) {
            var userInfo = Utils.GetCurrentUserInfo();
            usedPartsRefitBill.CreatorId = userInfo.Id;
            usedPartsRefitBill.CreatorName = userInfo.Name;
            usedPartsRefitBill.CreateTime = DateTime.Now;
            if(string.IsNullOrWhiteSpace(usedPartsRefitBill.Code) || usedPartsRefitBill.Code == GlobalVar.ASSIGNED_BY_SERVER)
                usedPartsRefitBill.Code = CodeGenerator.Generate("UsedPartsRefitBill", usedPartsRefitBill.UsedPartsWarehouseCode);
        }

        internal void UpdateUsedPartsRefitBillValidate(UsedPartsRefitBill usedPartsRefitBill) {
            var userInfo = Utils.GetCurrentUserInfo();
            usedPartsRefitBill.ModifierId = userInfo.Id;
            usedPartsRefitBill.ModifierName = userInfo.Name;
            usedPartsRefitBill.ModifyTime = DateTime.Now;
        }

        public void InsertUsedPartsRefitBill(UsedPartsRefitBill usedPartsRefitBill) {
            InsertToDatabase(usedPartsRefitBill);
            var usedPartsRefitReqDetails = ChangeSet.GetAssociatedChanges(usedPartsRefitBill, r => r.UsedPartsRefitReqDetails, ChangeOperation.Insert);
            foreach(UsedPartsRefitReqDetail usedPartsRefitReqDetail in usedPartsRefitReqDetails)
                InsertToDatabase(usedPartsRefitReqDetail);
            var usedPartsRefitReturnDetails = ChangeSet.GetAssociatedChanges(usedPartsRefitBill, r => r.UsedPartsRefitReturnDetails, ChangeOperation.Insert);
            foreach(UsedPartsRefitReturnDetail usedPartsRefitReturnDetail in usedPartsRefitReturnDetails)
                InsertToDatabase(usedPartsRefitReturnDetail);
            this.InsertUsedPartsRefitBillValidate(usedPartsRefitBill);
        }

        public void UpdateUsedPartsRefitBill(UsedPartsRefitBill usedPartsRefitBill) {
            usedPartsRefitBill.UsedPartsRefitReqDetails.Clear();
            usedPartsRefitBill.UsedPartsRefitReturnDetails.Clear();
            UpdateToDatabase(usedPartsRefitBill);
            var usedPartsRefitReqDetails = ChangeSet.GetAssociatedChanges(usedPartsRefitBill, r => r.UsedPartsRefitReqDetails);
            foreach(UsedPartsRefitReqDetail usedPartsRefitReqDetail in usedPartsRefitReqDetails) {
                switch(ChangeSet.GetChangeOperation(usedPartsRefitReqDetail)) {
                    case ChangeOperation.Insert:
                        InsertToDatabase(usedPartsRefitReqDetail);
                        break;
                    case ChangeOperation.Update:
                        UpdateToDatabase(usedPartsRefitReqDetail);
                        break;
                    case ChangeOperation.Delete:
                        DeleteFromDatabase(usedPartsRefitReqDetail);
                        break;
                }
            }
            var usedPartsRefitReturnDetails = ChangeSet.GetAssociatedChanges(usedPartsRefitBill, r => r.UsedPartsRefitReturnDetails);
            foreach(UsedPartsRefitReturnDetail usedPartsRefitReturnDetail in usedPartsRefitReturnDetails) {
                switch(ChangeSet.GetChangeOperation(usedPartsRefitReturnDetail)) {
                    case ChangeOperation.Insert:
                        InsertToDatabase(usedPartsRefitReturnDetail);
                        break;
                    case ChangeOperation.Update:
                        UpdateToDatabase(usedPartsRefitReturnDetail);
                        break;
                    case ChangeOperation.Delete:
                        DeleteFromDatabase(usedPartsRefitReturnDetail);
                        break;
                }
            }
            this.UpdateUsedPartsRefitBillValidate(usedPartsRefitBill);
        }
    
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:30
        //原分布类的函数全部转移到Ach                                                               

        public void InsertUsedPartsRefitBill(UsedPartsRefitBill usedPartsRefitBill) {
            new UsedPartsRefitBillAch(this).InsertUsedPartsRefitBill(usedPartsRefitBill);
        }

        public void UpdateUsedPartsRefitBill(UsedPartsRefitBill usedPartsRefitBill) {
            new UsedPartsRefitBillAch(this).UpdateUsedPartsRefitBill(usedPartsRefitBill);
        }
    }
}
