﻿using System;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Web;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class UsedPartsReturnOrderAch : DcsSerivceAchieveBase {
        internal void InsertUsedPartsReturnOrderValidate(UsedPartsReturnOrder usedPartsReturnOrder) {
            var userInfo = Utils.GetCurrentUserInfo();
            usedPartsReturnOrder.CreatorId = userInfo.Id;
            usedPartsReturnOrder.CreatorName = userInfo.Name;
            usedPartsReturnOrder.CreateTime = DateTime.Now;
            if(string.IsNullOrWhiteSpace(usedPartsReturnOrder.Code) || usedPartsReturnOrder.Code == GlobalVar.ASSIGNED_BY_SERVER)
                usedPartsReturnOrder.Code = CodeGenerator.Generate("UsedPartsReturnOrder", usedPartsReturnOrder.OutboundWarehouseCode);
        }
        internal void UpdateUsedPartsReturnOrderValidate(UsedPartsReturnOrder usedPartsReturnOrder) {
            var userInfo = Utils.GetCurrentUserInfo();
            usedPartsReturnOrder.ModifierId = userInfo.Id;
            usedPartsReturnOrder.ModifierName = userInfo.Name;
            usedPartsReturnOrder.ModifyTime = DateTime.Now;
        }
        public void InsertUsedPartsReturnOrder(UsedPartsReturnOrder usedPartsReturnOrder) {
            InsertToDatabase(usedPartsReturnOrder);
            var usedPartsReturnOrders = ChangeSet.GetAssociatedChanges(usedPartsReturnOrder, r => r.UsedPartsReturnDetails, ChangeOperation.Insert);
            foreach(UsedPartsReturnDetail usedPartsReturnDetail in usedPartsReturnOrders) {
                InsertToDatabase(usedPartsReturnDetail);
            }
            this.InsertUsedPartsReturnOrderValidate(usedPartsReturnOrder);
        }
        public void UpdateUsedPartsReturnOrdere(UsedPartsReturnOrder usedPartsReturnOrder) {
            usedPartsReturnOrder.UsedPartsReturnDetails.Clear();
            UpdateToDatabase(usedPartsReturnOrder);
            var usedPartsReturnOrders = ChangeSet.GetAssociatedChanges(usedPartsReturnOrder, r => r.UsedPartsReturnDetails);
            foreach(UsedPartsReturnDetail usedPartsReturnDetail in usedPartsReturnOrders) {
                switch(ChangeSet.GetChangeOperation(usedPartsReturnDetail)) {
                    case ChangeOperation.Insert:
                        InsertToDatabase(usedPartsReturnDetail);
                        break;
                    case ChangeOperation.Update:
                        UpdateToDatabase(usedPartsReturnDetail);
                        break;
                    case ChangeOperation.Delete:
                        DeleteFromDatabase(usedPartsReturnDetail);
                        break;
                }
            }
            this.UpdateUsedPartsReturnOrderValidate(usedPartsReturnOrder);
        }
        public UsedPartsReturnOrder GetUsedPartsReturnOrderWithDetailsById(int id) {
            var usedPartsReturnOrder = ObjectContext.UsedPartsReturnOrders.SingleOrDefault(r => r.Id == id && r.Status == (int)DcsUsedPartsReturnOrderStatus.新建);
            if(usedPartsReturnOrder != null) {
                var usedPartsTransferDetails = ObjectContext.UsedPartsReturnDetails.Where(r => r.UsedPartsReturnOrderId == id).ToArray();
            }
            return usedPartsReturnOrder;
        }
        public IQueryable<UsedPartsReturnOrder> 旧件仓库人员查询旧件清退单() {
            var userInfo = Utils.GetCurrentUserInfo();
            var query = ObjectContext.UsedPartsReturnOrders.Where(r => ObjectContext.UsedPartsWarehouseStaffs.Any(v => v.PersonnelId == userInfo.Id && v.UsedPartsWarehouseId == r.OutboundWarehouseId));
            return query.OrderByDescending(r => r.Id).Include("UsedPartsReturnDetails");
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:30
        //原分布类的函数全部转移到Ach                                                               

        public void InsertUsedPartsReturnOrder(UsedPartsReturnOrder usedPartsReturnOrder) {
            new UsedPartsReturnOrderAch(this).InsertUsedPartsReturnOrder(usedPartsReturnOrder);
        }

        public void UpdateUsedPartsReturnOrdere(UsedPartsReturnOrder usedPartsReturnOrder) {
            new UsedPartsReturnOrderAch(this).UpdateUsedPartsReturnOrdere(usedPartsReturnOrder);
        }

        public UsedPartsReturnOrder GetUsedPartsReturnOrderWithDetailsById(int id) {
            return new UsedPartsReturnOrderAch(this).GetUsedPartsReturnOrderWithDetailsById(id);
        }

        public IQueryable<UsedPartsReturnOrder> 旧件仓库人员查询旧件清退单() {
            return new UsedPartsReturnOrderAch(this).旧件仓库人员查询旧件清退单();
        }
    }
}
