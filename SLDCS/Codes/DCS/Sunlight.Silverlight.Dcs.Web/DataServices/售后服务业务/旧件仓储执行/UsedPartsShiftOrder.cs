﻿using System;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class UsedPartsShiftOrderAch : DcsSerivceAchieveBase {
        internal void InsertUsedPartsShiftOrderValidate(UsedPartsShiftOrder usedPartsShiftOrder) {
            var userInfo = Utils.GetCurrentUserInfo();
            usedPartsShiftOrder.CreatorId = userInfo.Id;
            usedPartsShiftOrder.CreatorName = userInfo.Name;
            usedPartsShiftOrder.CreateTime = DateTime.Now;
            if(string.IsNullOrWhiteSpace(usedPartsShiftOrder.Code) || usedPartsShiftOrder.Code == GlobalVar.ASSIGNED_BY_SERVER)
                usedPartsShiftOrder.Code = CodeGenerator.Generate("UsedPartsShiftOrder", usedPartsShiftOrder.UsedPartsWarehouseCode);
        }

        public void InsertUsedPartsShiftOrder(UsedPartsShiftOrder usedPartsShiftOrder) {
            InsertToDatabase(usedPartsShiftOrder);
            var usedPartsShiftDetails = ChangeSet.GetAssociatedChanges(usedPartsShiftOrder, r => r.UsedPartsShiftDetails, ChangeOperation.Insert);
            foreach(UsedPartsShiftDetail usedPartsShiftDetail in usedPartsShiftDetails)
                InsertToDatabase(usedPartsShiftDetail);
            this.InsertUsedPartsShiftOrderValidate(usedPartsShiftOrder);
        }

        public IQueryable<UsedPartsShiftOrder> 旧件仓库人员查询旧件移库单() {
            var userinfo = Utils.GetCurrentUserInfo();
            var usedPartsWarehouseStaffs = ObjectContext.UsedPartsWarehouseStaffs.Where(r => r.PersonnelId == userinfo.Id).Select(r => r.UsedPartsWarehouseId);
            var usedPartsShiftOrders = ObjectContext.UsedPartsShiftOrders.Where(r => usedPartsWarehouseStaffs.Contains(r.UsedPartsWarehouseId));
            return usedPartsShiftOrders.Include("UsedPartsShiftDetails").OrderBy(v => v.Id);
        }
    
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:30
        //原分布类的函数全部转移到Ach                                                               

        public void InsertUsedPartsShiftOrder(UsedPartsShiftOrder usedPartsShiftOrder) {
            new UsedPartsShiftOrderAch(this).InsertUsedPartsShiftOrder(usedPartsShiftOrder);
        }

        public IQueryable<UsedPartsShiftOrder> 旧件仓库人员查询旧件移库单() {
            return new UsedPartsShiftOrderAch(this).旧件仓库人员查询旧件移库单();
        }
    }
}
