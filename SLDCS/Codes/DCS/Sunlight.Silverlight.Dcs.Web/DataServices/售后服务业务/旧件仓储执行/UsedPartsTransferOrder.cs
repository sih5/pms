﻿using System;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Microsoft.Data.Extensions;
using Sunlight.Silverlight.Web;

namespace Sunlight.Silverlight.Dcs.Web {
    partial class UsedPartsTransferOrderAch : DcsSerivceAchieveBase {
        internal void InsertUsedPartsTransferOrderValidate(UsedPartsTransferOrder usedPartsTransferOrder) {
            var userInfo = Utils.GetCurrentUserInfo();
            usedPartsTransferOrder.CreatorId = userInfo.Id;
            usedPartsTransferOrder.CreatorName = userInfo.Name;
            usedPartsTransferOrder.CreateTime = DateTime.Now;
            if(string.IsNullOrWhiteSpace(usedPartsTransferOrder.Code) || usedPartsTransferOrder.Code == GlobalVar.ASSIGNED_BY_SERVER)
                usedPartsTransferOrder.Code = CodeGenerator.Generate("UsedPartsTransferOrder");
        }

        internal void UpdateUsedPartsTransferOrderValidate(UsedPartsTransferOrder usedPartsTransferOrder) {
            var userInfo = Utils.GetCurrentUserInfo();
            usedPartsTransferOrder.ModifierId = userInfo.Id;
            usedPartsTransferOrder.ModifierName = userInfo.Name;
            usedPartsTransferOrder.ModifyTime = DateTime.Now;
        }

        public void InsertUsedPartsTransferOrder(UsedPartsTransferOrder usedPartsTransferOrder) {
            InsertToDatabase(usedPartsTransferOrder);
            var usedPartsTransferDetails = ChangeSet.GetAssociatedChanges(usedPartsTransferOrder, r => r.UsedPartsTransferDetails, ChangeOperation.Insert);
            foreach(UsedPartsTransferDetail usedPartsTransferDetail in usedPartsTransferDetails)
                InsertToDatabase(usedPartsTransferDetail);
            this.InsertUsedPartsTransferOrderValidate(usedPartsTransferOrder);
        }

        public void UpdateUsedPartsTransferOrder(UsedPartsTransferOrder usedPartsTransferOrder) {
            usedPartsTransferOrder.UsedPartsTransferDetails.Clear();
            UpdateToDatabase(usedPartsTransferOrder);
            var usedPartsTransferDetails = ChangeSet.GetAssociatedChanges(usedPartsTransferOrder, r => r.UsedPartsTransferDetails);
            foreach(UsedPartsTransferDetail usedPartsTransferDetail in usedPartsTransferDetails) {
                switch(ChangeSet.GetChangeOperation(usedPartsTransferDetail)) {
                    case ChangeOperation.Insert:
                        InsertToDatabase(usedPartsTransferDetail);
                        break;
                    case ChangeOperation.Update:
                        UpdateToDatabase(usedPartsTransferDetail);
                        break;
                    case ChangeOperation.Delete:
                        DeleteFromDatabase(usedPartsTransferDetail);
                        break;
                }
            }
            this.UpdateUsedPartsTransferOrderValidate(usedPartsTransferOrder);
        }

        public UsedPartsTransferOrder GetUsedPartsTransferOrdersWithDetailsById(int id) {
            var usedPartsTransferOrder = ObjectContext.UsedPartsTransferOrders.SingleOrDefault(r => r.Id == id && r.Status != (int)DcsUsedPartsTransferOrderStatus.作废);
            if(usedPartsTransferOrder != null) {
                var usedPartsTransferDetails = ObjectContext.UsedPartsTransferDetails.Where(r => r.UsedPartsTransferOrderId == id).ToArray();
            }
            return usedPartsTransferOrder;
        }

        public IQueryable<UsedPartsTransferOrder> 根据旧件仓库人员查询旧件调拨单() {
            var userinfo = Utils.GetCurrentUserInfo();
            var usedPartsWarehouseStaffs = ObjectContext.UsedPartsWarehouseStaffs.Where(r => r.PersonnelId == userinfo.Id).Select(r => r.UsedPartsWarehouseId);
            var usedPartsTransferOrders = ObjectContext.UsedPartsTransferOrders.Where(r => usedPartsWarehouseStaffs.Contains(r.OriginWarehouseId) || usedPartsWarehouseStaffs.Contains(r.DestinationWarehouseId));
            return usedPartsTransferOrders.Include("UsedPartsTransferDetails").OrderBy(v => v.Id);
        }
    
    }

    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:30
        //原分布类的函数全部转移到Ach                                                               

        public void InsertUsedPartsTransferOrder(UsedPartsTransferOrder usedPartsTransferOrder) {
            new UsedPartsTransferOrderAch(this).InsertUsedPartsTransferOrder(usedPartsTransferOrder);
        }

        public void UpdateUsedPartsTransferOrder(UsedPartsTransferOrder usedPartsTransferOrder) {
            new UsedPartsTransferOrderAch(this).UpdateUsedPartsTransferOrder(usedPartsTransferOrder);
        }

        public UsedPartsTransferOrder GetUsedPartsTransferOrdersWithDetailsById(int id) {
            return new UsedPartsTransferOrderAch(this).GetUsedPartsTransferOrdersWithDetailsById(id);
        }

        public IQueryable<UsedPartsTransferOrder> 根据旧件仓库人员查询旧件调拨单() {
            return new UsedPartsTransferOrderAch(this).根据旧件仓库人员查询旧件调拨单();
        }
    }
}
