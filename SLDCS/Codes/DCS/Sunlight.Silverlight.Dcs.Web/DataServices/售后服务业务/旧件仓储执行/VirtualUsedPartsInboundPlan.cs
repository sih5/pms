﻿using System;
using System.Collections.Generic;
using System.Linq;
namespace Sunlight.Silverlight.Dcs.Web {
    partial class VirtualUsedPartsInboundPlanAch : DcsSerivceAchieveBase {
        public VirtualUsedPartsInboundPlanAch(DcsDomainService domainService)
            : base(domainService) {
        }

        public IEnumerable<VirtualUsedPartsInboundPlan> 查询虚拟旧件入库计划(string sourceCode, DateTime? sourceBillTimeBegin, DateTime? sourceBillTimeEnd, int? inboundType, int? inboundStatus, string usedPartsWarehouseCode, string usedPartsWarehouseName, int personnelId) {
            var result = Enumerable.Empty<VirtualUsedPartsInboundPlan>();
            var usedPartsWarehouseIds = ObjectContext.UsedPartsWarehouseAreas.Where(r => ObjectContext.UsedPartsWarehouseManagers.Any(v => v.PersonnelId == personnelId && v.UsedPartsWarehouseAreaId == r.Id) && r.Status == (int)DcsBaseDataStatus.有效).Select(r => r.UsedPartsWarehouseId).ToArray();
            if(usedPartsWarehouseIds.Length == 0)
                return result;
            var usedPartsLoanBills = ObjectContext.UsedPartsLoanBills.Where(r => r.Status == (int)DcsUsedPartsLoanBillStatus.生效
                && (r.InboundStatus == (int)DcsUsedPartsInboundStatus.待入库 || r.InboundStatus == (int)DcsUsedPartsInboundStatus.部分入库 || r.InboundStatus == (int)DcsUsedPartsInboundStatus.终止入库)
                && usedPartsWarehouseIds.Contains(r.UsedPartsWarehouseId)).Select(r => new VirtualUsedPartsInboundPlan {
                    SourceId = r.Id,
                    SourceCode = r.Code,
                    SourceBillApprovalTime = r.ApproveTime,
                    InboundType = (int)DcsUsedPartsInboundOrderInboundType.借用归还,
                    InboundStatus = r.InboundStatus,
                    UsedPartsWarehouseCode = r.UsedPartsWarehouseCode,
                    UsedPartsWarehouseName = r.UsedPartsWarehouseName,
                    UsedPartsWarehouseId = r.UsedPartsWarehouseId,
                    RelatedCompanyId = null,
                    RelatedCompanyCode = null,
                    RelatedCompanyName = r.RelatedCompanyName,
                    CompanyCode = null,
                    CompanyName = null
                });
            var usedPartsRefitBills = ObjectContext.UsedPartsRefitBills.Where(r => r.Status == (int)DcsUsedPartsRefitBillStatus.生效
                && (r.InboundStatus == (int)DcsUsedPartsInboundStatus.待入库 || r.InboundStatus == (int)DcsUsedPartsInboundStatus.部分入库 || r.InboundStatus == (int)DcsUsedPartsInboundStatus.终止入库)
                && usedPartsWarehouseIds.Contains(r.UsedPartsWarehouseId)).Select(r => new VirtualUsedPartsInboundPlan {
                    SourceId = r.Id,
                    SourceCode = r.Code,
                    SourceBillApprovalTime = r.ApproveTime,
                    InboundType = (int)DcsUsedPartsInboundOrderInboundType.加工入库,
                    InboundStatus = r.InboundStatus,
                    UsedPartsWarehouseCode = r.UsedPartsWarehouseCode,
                    UsedPartsWarehouseName = r.UsedPartsWarehouseName,
                    UsedPartsWarehouseId = r.UsedPartsWarehouseId,
                    RelatedCompanyId = null,
                    RelatedCompanyCode = null,
                    RelatedCompanyName = r.RelatedCompanyName,
                    CompanyCode = null,
                    CompanyName = null
                });
            var usedPartsTransferOrders = ObjectContext.UsedPartsTransferOrders.Where(r => r.Status == (int)DcsUsedPartsTransferOrderStatus.生效
                && (r.InboundStatus == (int)DcsUsedPartsInboundStatus.待入库 || r.InboundStatus == (int)DcsUsedPartsInboundStatus.部分入库 || r.InboundStatus == (int)DcsUsedPartsInboundStatus.终止入库)
                && usedPartsWarehouseIds.Contains(r.DestinationWarehouseId)).Select(r => new VirtualUsedPartsInboundPlan {
                    //&& usedPartsWarehouseIds.Contains(r.DestinationWarehouseId) && (r.OutboundStatus == (int)DcsUsedPartsOutboundStatus.部分出库 || r.OutboundStatus == (int)DcsUsedPartsOutboundStatus.出库完成)).Select(r => new VirtualUsedPartsInboundPlan {
                    SourceId = r.Id,
                    SourceCode = r.Code,
                    SourceBillApprovalTime = r.ApproveTime,
                    InboundType = (int)DcsUsedPartsInboundOrderInboundType.调拨入库,
                    InboundStatus = r.InboundStatus,
                    UsedPartsWarehouseCode = r.DestinationWarehouseCode,
                    UsedPartsWarehouseName = r.DestinationWarehouseName,
                    UsedPartsWarehouseId = r.DestinationWarehouseId,
                    RelatedCompanyId = r.OriginWarehouseId,
                    RelatedCompanyCode = r.OriginWarehouseCode,
                    RelatedCompanyName = r.OriginWarehouseName,
                    CompanyCode = null,
                    CompanyName = null
                });
            var usedPartsShippingOrders = from a in ObjectContext.UsedPartsShippingOrders.Where(r => r.Status == (int)DcsUsedPartsShippingOrderStatus.已确认 && (r.InboundStatus == (int)DcsUsedPartsInboundStatus.待入库 || r.InboundStatus == (int)DcsUsedPartsInboundStatus.部分入库 || r.InboundStatus == (int)DcsUsedPartsInboundStatus.终止入库) && usedPartsWarehouseIds.Contains(r.DestinationWarehouseId))
                                          from b in ObjectContext.UsedPartsWarehouses
                                          where a.DestinationWarehouseId == b.Id
                                          select new VirtualUsedPartsInboundPlan {
                                              SourceId = a.Id,
                                              SourceCode = a.Code,
                                              SourceBillApprovalTime = a.ConfirmationTime,
                                              InboundType = (int)DcsUsedPartsInboundOrderInboundType.服务站返件入库,
                                              InboundStatus = a.InboundStatus,
                                              UsedPartsWarehouseCode = b.Code,
                                              UsedPartsWarehouseName = b.Name,
                                              UsedPartsWarehouseId = a.DestinationWarehouseId,
                                              RelatedCompanyId = a.DealerId,
                                              RelatedCompanyCode = a.DealerCode,
                                              RelatedCompanyName = a.DealerName,
                                              CompanyCode = a.DealerCode,
                                              CompanyName = a.DealerName
                                          };
            if(inboundType.HasValue) {
                switch(inboundType.Value) {
                    case (int)DcsUsedPartsInboundOrderInboundType.借用归还:
                        result = usedPartsLoanBills;
                        break;
                    case (int)DcsUsedPartsInboundOrderInboundType.加工入库:
                        result = usedPartsRefitBills;
                        break;
                    case (int)DcsUsedPartsInboundOrderInboundType.调拨入库:
                        result = usedPartsTransferOrders;
                        break;
                    case (int)DcsUsedPartsInboundOrderInboundType.服务站返件入库:
                        result = usedPartsShippingOrders;
                        break;
                }
            }else
                result = usedPartsLoanBills.Concat(usedPartsRefitBills.Concat(usedPartsTransferOrders.Concat(usedPartsShippingOrders)));
            if(!string.IsNullOrEmpty(sourceCode))
                result = result.Where(r => r.SourceCode.Contains(sourceCode));
            if(sourceBillTimeBegin.HasValue)
                result = result.Where(r => r.SourceBillApprovalTime >= sourceBillTimeBegin.Value);
            if(sourceBillTimeEnd.HasValue)
                result = result.Where(r => r.SourceBillApprovalTime <= sourceBillTimeEnd.Value);
            if(inboundStatus.HasValue)
                result = result.Where(r => r.InboundStatus == inboundStatus);
            if(!string.IsNullOrEmpty(usedPartsWarehouseCode))
                result = result.Where(r => r.UsedPartsWarehouseCode.Contains(usedPartsWarehouseCode));
            if(!string.IsNullOrEmpty(usedPartsWarehouseName))
                result = result.Where(r => r.UsedPartsWarehouseName.Contains(usedPartsWarehouseName));
            var c = result.ToArray();
            return result.OrderBy(r => r.SourceBillApprovalTime);
        }
    
    }
    partial class DcsDomainService {// 已经使用工具进行处理 2017/4/29 12:12:30
        public IEnumerable<VirtualUsedPartsInboundPlan> 查询虚拟旧件入库计划(string sourceCode, DateTime? sourceBillTimeBegin, DateTime? sourceBillTimeEnd, int? inboundType, int? inboundStatus, string usedPartsWarehouseCode, string usedPartsWarehouseName, int personnelId) {
            return new VirtualUsedPartsInboundPlanAch(this).查询虚拟旧件入库计划(sourceCode,sourceBillTimeBegin,sourceBillTimeEnd,inboundType,inboundStatus,usedPartsWarehouseCode,usedPartsWarehouseName,personnelId);
        }
    }
}
